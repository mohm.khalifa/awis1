<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
echo "\n<style type=text/css> @page { size:landscape; } </style>\n";
?>
</head>
<body>
<?php
include ("ATU_Header.php");	// Kopfzeile

global $awisRSZeilen;

awis_ZeitMessung(0);			// Zeitmessung aktivieren


// DB Init
$con = awisLogon();


$ReifenRechte = awisBenutzerRecht($con,250);

if($ReifenRechte==0)
{
    awisEreignis(3,1000,'Reifen',"$PHP_AUTH_USER",'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}


echo '<form name=frmFahrzeuge action=./reifen_FahrzeugAbfrage.php method=post>';

echo '<table border=1 width=400>';

echo '<tr>';

if(!isset($_POST['txtFahrzeug']))
{
	echo '<td id=FeldBez width=150>Fahrzeughersteller:</td>';
	$SQL = 'SELECT KFZ_HERST.BEZ, KFZ_HERST.KHERNR FROM KFZ_HERST ORDER BY KFZ_HERST.BEZ';
	$rsDaten = awisOpenRecordset($con,$SQL);
	echo '<td ><select name=txtFahrzeug onchange=submit();>';
	echo '<option value=-1>::Bitte w�hlen...::</option>';
	for($j=0;$j < $awisRSZeilen;$j++)
	{
		echo '<option value=' . $rsDaten['KHERNR'][$j];
		echo '>' . $rsDaten['BEZ'][$j] . '</option>';
	}
	echo '</select></td>';
}
elseif(!isset($_POST['txtModell']))
{
	echo '<td id=FeldBez>Hersteller:</td><td>' . $_POST['txtFahrzeug'] . '</td></tr><tr>';
	echo '<td id=FeldBez width=150>Fahrzeugmodell:</td>';
	echo '<td><input type=hidden name=txtFahrzeug value='.$_POST['txtFahrzeug'] . '>';
	
	
	$SQL = 'SELECT DISTINCT ';
	$SQL .= " KFZ_KBANR.KBANRC AS KBANR, KFZ_TYP.BEZ AS TYP,";
	$SQL .= " KFZ_MODELL.BEZ AS MODELL, KFZ_HERST.BEZ AS HERSTELLER,";
	$SQL .= " KFZ_HERST.KHERNR";
	$SQL .= " from KFZ_KBANR INNER JOIN KFZ_TYP ON KFZ_KBANR.KTYPNR=KFZ_TYP.KTYPNR";
	$SQL .= "   INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR";
	$SQL .= "   INNER JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR"; 
	$SQL .= " WHERE KHERNR=" . $_POST['txtFahrzeug'];
	$SQL .= " ORDER BY MODELL";

	$rsDaten = awisOpenRecordset($con,$SQL);
	echo '<select name=txtModell onchange=submit();>';
	echo '<option value=-1>::Bitte w�hlen...::</option>';
	for($j=0;$j < $awisRSZeilen;$j++)
	{
		echo '<option value=' . $rsDaten['KBANR'][$j];
		echo '>' . $rsDaten['MODELL'][$j] . ' - ' . $rsDaten['TYP'][$j] .  ' - ' . $rsDaten['KBANR'][$j] . '</option>';
	}
	echo '</select></td>';
	
	
	echo '</td>';
}
elseif(isset($_POST['txtModell']))
{
	
	$SQL = 'SELECT DISTINCT ';
	//$SQL .= " KFZ_KBANR.KBANRC AS KBANR, KFZ_TYP.BEZ AS TYP, KFZ_TYP.BJVON, KFZ_TYP.BJBIS, KFZ_TYP.PS,";
	$SQL .= " KFZ_MODELL.BEZ AS MODELL, KFZ_HERST.BEZ AS HERSTELLER,";
	$SQL .= " REF_ZIFF21, REF_ZIFF22, REF_ZIFF23, REF_ZIFF33, KFZ_TYP.BJVON, KFZ_TYP.BJBIS ";
	//$SQL .= ", REF_Breite, KFZ_HERST.KHERNR, REF_KBH_ID";
	$SQL .= " from KFZ_KBANR INNER JOIN KFZ_TYP ON KFZ_KBANR.KTYPNR=KFZ_TYP.KTYPNR";
	$SQL .= "   INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR";
	$SQL .= "   INNER JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR"; 
	$SQL .= "   INNER JOIN ReifenFahrzeuge ON KFZ_KBANR.KBANRC=(ReifenFahrzeuge.REF_KBH_ID || ReifenFahrzeuge.REF_KBT_ID) ";
	$SQL .= " WHERE KFZ_KBANR.KBANRC=" . $_POST['txtModell'];
	$rsDaten = awisOpenRecordset($con,$SQL);
	
	
	echo '<font size=4>Fahrzeug: ' . $rsDaten['HERSTELLER'][0] . ' ' . $rsDaten['MODELL'][0] . '</font>'; 
	
	echo '<table border=1>';	
	echo '<tr>';	

	echo '<td id=FeldBez>Ziffer 21</td>';
	echo '<td id=FeldBez>Ziffer 22</td>';
	echo '<td id=FeldBez>Ziffer 23</td>';
	echo '<td id=FeldBez>Ziffer 33</td>';
	echo '<td id=FeldBez>BJ von</td>';
	echo '<td id=FeldBez>BJ bis</td>';
	
	echo '</tr>';

	for($i=0;$i<$awisRSZeilen;$i++)
	{
		echo '<tr>';	

		echo '<td>';
		echo '' . $rsDaten['REF_ZIFF21'][$i];
		echo '</td>';

		echo '<td>';
		echo '' . $rsDaten['REF_ZIFF22'][$i];
		echo '</td>';

		echo '<td>';
		echo '' . $rsDaten['REF_ZIFF23'][$i];
		echo '</td>';

		echo '<td>';
		echo '' . $rsDaten['REF_ZIFF33'][$i];
		echo '</td>';

		echo '<td>';
		echo '' . $rsDaten['BJVON'][$i];
		echo '</td>';

		echo '<td>';
		echo '' . $rsDaten['BJBIS'][$i];
		echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
}

//awis_Debug(1,$rsDaten);
echo '</tr>';


echo '</table>';

echo '</form>';


awisLogoff($con);

echo "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";

echo '<br><font size=2>Ben�tigte Zeit:' . sprintf('%0.6f',awis_ZeitMessung(1)) . ' Sekunden</font>';


?>