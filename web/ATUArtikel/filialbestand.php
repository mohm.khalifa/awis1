<html>
<head>
<title>AWIS - Filialbestand</title>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<?php
require_once('/daten/include/register.inc.php');
require_once('/daten/include/db.inc.php');		// DB-Befehle
require_once('/daten/include/sicherheit.inc.php');
require_once('/daten/include/awisFilialen.inc');
global $AWISBenutzer;
global $awisRSZeilen;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>

</head>

<body>
<?php

include ('/daten/include/ATU_Header.php');	// Kopfzeile

$Warengruppen=array('01','02','04','14','23');

$con=awisLogon();

if($con==FALSE)
{
	awisLogoff($con);
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$FilialBestandRecht=awisBenutzerRecht($con, 406);
if($FilialBestandRecht==0)
{
    awisEreignis(3,1000,'Filialbestand',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

if($_GET['ATUNR']!='')
{
	if (isset($_GET['NurBestand']) AND $_GET['NurBestand'] == 1)
	{
		$ZeigeNurBestaende = true;
	}
	else
	{
		$ZeigeNurBestaende = false;
	}
	
	//*****************************************
	// Nachbarfilialen ermitteln
	//*****************************************
	/*
	$FilDaten=array();
	if(isset($_GET['Nachbar']) AND $_GET['Nachbar']!='')
	{
		$FilDaten = explode(';', $_GET['Nachbar']);
	}
	elseif(isset($_GET['txtFIL_ID']) AND $_GET['txtFIL_ID']!='')
	{
		$SQL = 'SELECT * FROM Filialen WHERE FIL_ID=0' . $_GET['txtFIL_ID'];
		$rsFIL = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen>0)
		{
			$FilDaten = array($rsFIL['FIL_ID'][0],$rsFIL['FIL_PLZ'][0]);
		}
		else
		{
			echo '<span class=	';
		}
	}
		
	if(!empty($FilDaten))
	{
		$Antwort = file_get_contents('http://filialsuche.server.atu.de/filialsuche?plz=' . $FilDaten[1] . '&lkz=D&anzahl=' . awis_BenutzerParameter($con, 'FilialNachbarn', $AWISBenutzer->BenutzerName()));
		if($Antwort!==false)
		{
			$p = xml_parser_create();
			xml_parse_into_struct($p, $Antwort, $vals, $index);
			xml_parser_free($p);
			$Nr = 0;
			foreach($vals as $Daten)
			{
				if($Daten['tag']=='NR')
				{
					$Filialen['NR'][++$Nr]=$Daten['value'];
					$FilListe .= ',' . $Daten['value'];
				}
				elseif($Daten['tag']=='NAME')
				{
					$Filialen['NAME'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='PLZ')
				{
					$Filialen['PLZ'][$Nr]=$Daten['value'];
				}
			}
			echo '<hr>';
		}
		$ZeigeNurBestaende = false;			// Alle Anzeigen
awis_Debug(1,$Filialen,$Antwort);
	}	// Nachbardaten angefordert?
	*/
	
	
	$Filiale = '';
	if (isset($_GET['Nachbar']) AND $_GET['Nachbar']!='')
	{
		$Filiale = $_GET['Nachbar'];
	}
	elseif (isset($_GET['txtFIL_ID']) AND $_GET['txtFIL_ID']!='')
	{
		$Filiale = $_GET['txtFIL_ID'];
	}
	
	$FilListe = '';

	if ($Filiale != '')
	{
		$SQL = 'SELECT DATEN.* ';	
		$SQL .=' FROM(';
		$SQL .= 'SELECT CASE WHEN FEG_FIL_ID_VON = 0'.$Filiale.' THEN FEG_FIL_ID_NACH';
		$SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
		$SQL .= ',FIL_BEZ,FIL_STRASSE,FIL_PLZ,FIL_ORT,FIL_LAGERKZ';
		$SQL .= ' FROM FILIALENENTFERNUNGEN';
		$SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = '.$Filiale.' THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID AND FIL_ID < 900';
		$SQL .= ' WHERE FEG_FIL_ID_VON = 0'.$Filiale;
		$SQL .= ' OR FEG_FIL_ID_NACH = 0'.$Filiale;
		$SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
		$SQL .= ') DATEN ';
		$SQL .= ' WHERE ROWNUM <= 0'.(awis_BenutzerParameter($con, 'FilialNachbarn', $AWISBenutzer->BenutzerName()));
		$SQL .= ' AND (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND FIF_IMQ_ID = 32 AND ROWNUM = 1) IS NOT NULL';
		
		$rsFilialen = awisOpenRecordset($con, $SQL);
		$rsFilialenZeilen = $awisRSZeilen;
		
		for($Zeile=0;$Zeile<$rsFilialenZeilen;$Zeile++)
		{
			$FilListe .= ','. $rsFilialen['FEG_FIL_ID'][$Zeile];		
		}
		$ZeigeNurBestaende = false;			// Alle Anzeigen
	}		
	
	//***************************************************************
	// Protokollieren
	//***************************************************************
	$BindeVariablen=array();
	$BindeVariablen['var_T_ast_atunr']=$_GET['ATUNR'];
	$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
	
	$SQL = 'SELECT count(*) AS ANZ FROM FILIALBESTANDANFRAGEN ';
	$SQL .= ' WHERE FBA_AST_ATUNR=:var_T_ast_atunr';
	$SQL .= ' AND FBA_USER=:var_T_user';
	$SQL .= ' AND FBA_USERDAT > (SYSDATE-(5/1440))';
	$rsFBA = awisOpenRecordset($con, $SQL,true,false,$BindeVariablen);
	if($rsFBA['ANZ'][0]==0)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_T_ast_atunr']=$_GET['ATUNR'];
		$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
	
		$SQL = 'INSERT INTO FILIALBESTANDANFRAGEN';
		$SQL .= '(FBA_AST_ATUNR, FBA_GESAMTMENGE, FBA_USER, FBA_USERDAT)';
		$SQL .= ' VALUES(';
		$SQL .= '  :var_T_ast_atunr';
		$SQL .= ', (SELECT SUM(FIB_BESTAND) AS BESTAND FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr)';
		$SQL .= ', :var_T_user';
		$SQL .= ', SYSDATE';
		$SQL .= ')';

		awisExecute($con,$SQL,true,false,0,$BindeVariablen);
	}

	// Liste mit allen Filialen
	$SQL = 'SELECT F.*';
	$SQL .= ' , (SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=910 AND FIF_FIL_ID=F.FIL_ID AND ROWNUM=1) AS AXFIL';
	$SQL .= " FROM Filialen F";	
	$SQL .= " WHERE FIL_GRUPPE IS NOT NULL";
	if($FilListe!='')
	{
		$SQL .= " AND FIL_ID IN (" . substr($FilListe,1) . ")";
	}
		// ATU-Nr einschr�nken, wenn NUR Filialen mit Bestand gew�nscht sind
	if($_GET['ATUNR']!='' AND $ZeigeNurBestaende)
	{
		$SQL .= " AND FIL_ID IN(SELECT FIB_FIL_ID FROM FilialBestand WHERE FIB_AST_ATUNR " . awisLIKEoderIST($_GET['ATUNR'],1) . " AND FIB_BESTAND > 0)";
	}
	$SQL .= " ORDER BY FIL_ID";

	$rsFIL = awisOpenRecordset($con, $SQL);
	
	$rsFILZeilen = $awisRSZeilen;

	If($rsFILZeilen==0)
	{
		echo '<br><span class=HinweisText>Kein Artikel mit der Nummer in den Filialen gefunden gefunden.</span>';
	}
	
	
	echo '<Form name=frmFilialBestand method=GET action=./filialbestand.php>';
	echo '<table id=DatenTabelle width=500 border=1>';

	$BindeVariablen=array();
	$BindeVariablen['var_T_ast_atunr']=$_GET['ATUNR'];
	
	$rsAST = awisOpenRecordset($con, "SELECT AST_KEY,AST_BEZEICHNUNGWW, AST_KENNUNG, AST_VK, WUG_WGR_ID FROM ArtikelStamm inner join Warenuntergruppen on AST_WUG_KEY = WUG_KEY WHERE AST_ATUNR=:var_T_ast_atunr",true,false,$BindeVariablen);
	
	$WA = false;
	$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);		
	$AnzFilBestand = true;
	if($UserFilialen <> '')
	{
		foreach($Warengruppen as $Warengruppe)
		{
			if ($rsAST['WUG_WGR_ID'][0] == $Warengruppe)
			{
				$WA = true;
			}
		}
		
		$filObj = new awisFilialen();
		$AnzFilBestand = $filObj->AnzFilBestand((int)$UserFilialen,false);
		
	}
	
	//var_dump($con);
	
	//F�r neuen Artikelstamm
	if ($_GET['Zurueck']=='ArtikelInfosNeu' or $_GET['Zurueck']=='ArtikelListeNeu')
	{
		echo '<tr><td id=FeldBez colspan=7><h2>Artikel <a href=/artikelstamm/artikelstamm_Main.php?AST_KEY=' . (isset($rsAST['AST_KEY'][0])?$rsAST['AST_KEY'][0]:'') . '&cmdAktion=Artikelinfo>' . $_GET['ATUNR'] . '</a></h2></td></tr>';
	}
	else
	{
		echo '<tr><td id=FeldBez colspan=7><h2>Artikel <a href=./artikel_Main.php?Key=' . (isset($rsAST['AST_KEY'][0])?$rsAST['AST_KEY'][0]:'') . '&cmdAktion=ArtikelInfos>' . $_GET['ATUNR'] . '</a></h2></td></tr>';
	}

	//echo '<tr><td id=FeldBez colspan=5><h2>Artikel <a href=./artikel_Main.php?Key=' . (isset($rsAST['AST_KEY'][0])?$rsAST['AST_KEY'][0]:'') . '&cmdAktion=ArtikelInfos>' . $_GET['ATUNR'] . '</a></h2></td></tr>';
	echo '<tr><td colspan=7><h4>' . (isset($rsAST['AST_BEZEICHNUNGWW'][0])?$rsAST['AST_BEZEICHNUNGWW'][0]:'') . '</h4></td></tr>';
	echo '<tr><td colspan=7><h4>VK (D): ' . awis_format((isset($rsAST['AST_VK'][0])?$rsAST['AST_VK'][0]:'0'),'Currency') . '&nbsp;&nbsp;';
	echo 'Kennung: ' . (isset($rsAST['AST_KENNUNG'][0])?$rsAST['AST_KENNUNG'][0]:'') . '</h4></td></tr>';

	unset($rsAST);
			// Suchfelder f�r Artikel
	if(($FilialBestandRecht&4)==4)
	{
		echo '<tr><td id=FeldBez colspan=4>Artikelnummer:</td><td colspan=3><input name=ATUNR size=10 value=' . $_GET['ATUNR'] . '>';
		echo "<input name=NurBestand value=1 type=hidden>";
		echo "<input name=Zurueck value='" . $_GET['Zurueck'] . "' type=hidden>";
		echo "<input name=ASTKEY value='" . (isset($_GET['ASTKEY'])?$_GET['ASTKEY']:'') . "' type=hidden>";
		echo '</td></tr>';
		echo '<tr><td id=FeldBez colspan=4>Nachbarfilialen suchen:</td><td colspan=3><input name=txtFIL_ID size=5></td></tr>';

		echo "<tr><td colspan=7><input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
		echo "</td></tr>";
	}

	// Suchfelder f�r Filialen

/*ho,20120223
	$SQL = 'SELECT *';
	$SQL .= " FROM FilialBestand";
	$SQL .= " LEFT JOIN FilialBestandBewegung on FBB_AST_ATUNR = FIB_AST_ATUNR AND FBB_FIL_ID = FIB_FIL_ID";
	$SQL .= " WHERE ";
	$SQL .= " FIB_AST_ATUNR " . awisLIKEoderIST($_GET['ATUNR'],1) . "";
	$SQL .= " ORDER BY FIB_FIL_ID";
*/
	$SQL  = 'SELECT';
	$SQL .= ' NVL(FIB_AST_ATUNR, FBB_AST_ATUNR) AS ATUNR,';
	$SQL .= ' NVL(FIB_FIL_ID, FBB_FIL_ID) AS FIL_ID,';
	$SQL .= ' NVL(FIB_BESTAND, -1) AS BESTAND,';
	$SQL .= ' NVL(FBB_RESERVIERT, -1) AS RESERVIERT,';
	$SQL .= ' NVL(FIB_VK, -1) AS VK';
	$SQL .= ' FROM (';
	$SQL .= '  SELECT *';
	$SQL .= '  FROM ';
	$SQL .= '  (SELECT FIB_AST_ATUNR, FIB_FIL_ID, FIB_BESTAND, FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR ' .awisLIKEoderIST($_GET['ATUNR'],1) .')';
	$SQL .= '  FULL OUTER JOIN ';
	$SQL .= '  (SELECT FBB_AST_ATUNR, FBB_FIL_ID, FBB_RESERVIERT FROM FILIALBESTANDBEWEGUNG WHERE FBB_AST_ATUNR ' .awisLIKEoderIST($_GET['ATUNR'],1) .')';
	$SQL .= '  ON FBB_AST_ATUNR = FIB_AST_ATUNR AND FBB_FIL_ID = FIB_FIL_ID';
	$SQL .= ' ) ';
	$SQL .= 'ORDER BY FIL_ID';	
	
	$rsFIB = awisOpenRecordset($con, $SQL);
	$rsFIBZeilen = $awisRSZeilen;

		// Daten ausgeben
	if($AnzFilBestand or !$WA)
	{
	
	echo '<tr>';
	echo '<td id=FeldBez>Fil-Nr</td>';
	echo '<td id=FeldBez>Fil-Bezeichnung</td>';
	//Feld fuer Lagerkennzeichnung zugef�gt am 19.09.2007 von Wacker (IBM)	
	echo '<td id=FeldBez>Fil-Lager</td>';
	echo '<td id=FeldBez>AX-Filiale</td>';
	
	echo '<td align=center id=FeldBez><a title='.($_GET['NurBestand']==1?'\'Alle Filialen anzeigen\'':'\'Nur Filialen mit Bestand anzeigen\'').' href=./filialbestand.php?ATUNR=' .$_GET['ATUNR'] . ($_GET['NurBestand']==1?'&NurBestand=0':'&NurBestand=1'). "&Zurueck=" . $_GET['Zurueck'] . (isset($_GET['ASTKEY'])?"&ASTKEY=" . $_GET['ASTKEY']:'') . ">Bestand</a></td>";	
		
	echo '<td align=center id=FeldBez>Reserviert</td>';	
	if(($FilialBestandRecht&2)==2)
	{
		echo '<td align=center id=FeldBez>VK</td>';
	}	
	echo '</tr>';

	$GesBestand=0;
	$AnzFil=0;
	for($Zeile=0;$Zeile<$rsFILZeilen;$Zeile++)
	{
		$erg = array_search($rsFIL['FIL_ID'][$Zeile], $rsFIB['FIL_ID']);
		
		echo '<tr>';

		echo '<td class=DatenFeldNormal><a title=\'Filialdetails anzeigen\' href=../filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID=' . $rsFIL['FIL_ID'][$Zeile] . '>' . $rsFIL['FIL_ID'][$Zeile] . '</a></td>';
		if(strpos('123',$rsFIL['FIL_GRUPPE'][$Zeile])===false)
		{
			echo '<td>' . $rsFIL['FIL_BEZ'][$Zeile] . '</td>';

		}
		else
		{
			//echo '<td><a title=\'Nachbarfilialen anzeigen\' href=./filialbestand.php?ATUNR=' .$_GET['ATUNR'] . '&NurBestand=0'. "&Zurueck=" . $_GET['Zurueck'] . (isset($_GET['ASTKEY'])?"&ASTKEY=" . $_GET['ASTKEY']:'') . "&Nachbar=" . $rsFIL['FIL_ID'][$Zeile] . ';' . substr($rsFIL['FIL_PLZ'][$Zeile],0,3) . ">" . $rsFIL['FIL_BEZ'][$Zeile] . '</a></td>';
			echo '<td><a title=\'Nachbarfilialen anzeigen\' href=./filialbestand.php?ATUNR=' .$_GET['ATUNR'] . '&NurBestand=0'. "&Zurueck=" . $_GET['Zurueck'] . (isset($_GET['ASTKEY'])?"&ASTKEY=" . $_GET['ASTKEY']:'') . "&Nachbar=" . $rsFIL['FIL_ID'][$Zeile] . ">" . $rsFIL['FIL_BEZ'][$Zeile] . '</a></td>';
		}

		// Daten fuer Lagerkennzeichnung zugef�gt am 19.09.2007 von Wacker (IBM)
		echo '<td>' . $rsFIL['FIL_LAGERKZ'][$Zeile] . '</td>';

		echo '<td>' . $rsFIL['AXFIL'][$Zeile] . '</td>';
		
		if($erg!==False)
		{
			if ($rsFIB['BESTAND'][$erg]<0) //kein Datensatz in Filialbestand
			{
				echo '<td align=left>- -</td>';			
			}
			else
			{
				echo '<td>' . ($rsFIB['BESTAND'][$erg]==0?'<b>':'') . $rsFIB['BESTAND'][$erg] . ($rsFIB['BESTAND'][$erg]==0?'</b>':'') .'</td>';			
				$GesBestand+=$rsFIB['BESTAND'][$erg];
				if ($rsFIB['BESTAND'][$erg]>0)
				{
					$AnzFil++;
				}
			}
			
			if ($rsFIB['RESERVIERT'][$erg]<0) //kein Datensatz in Filialbestandbewegung
			{
				echo '<td align=left>- -</td>';			
			}
			else
			{
				echo '<td>' . ($rsFIB['RESERVIERT'][$erg]==0?'<b>':'') . $rsFIB['RESERVIERT'][$erg] . ($rsFIB['RESERVIERT'][$erg]==0?'</b>':'') .'</td>';
			}
			
			if(($FilialBestandRecht&2)==2)
			{
				if ($rsFIB['VK'][$erg]<0) //kein Datensatz in Filialbestand
				{
					echo '<td align=right>- -</td>';			
				}
				else
				{
					echo '<td align=right>' . awis_format($rsFIB['VK'][$erg],'Currency') . '</td>';
				}
			}
		}
		else
		{
			echo '<td align=left>- -</td>';
			echo '<td align=left>- -</td>';
			if(($FilialBestandRecht&2)==2)
			{
				echo '<td align=right>- -</td>';
			}
		}		
		echo '</tr>';

	}
	
	echo '<tr><td align=right></td><td colspan=3 align=right>Bestand in ' . ($AnzFil==1?'einer Filiale':$AnzFil . ' Filialen') . ':</td><td align=right>' . $GesBestand . '</td><td></td>';
	}
	else 
	{
		echo "<td colspan=3 align=left><span class=HinweisText>Die Best�nde im AX-WebKat abfragen!</span></td>";
	}
	echo '</table>';
	
	unset($rsFIL);

}
/*********************************************
* Datenstand anzeigen
*********************************************/
//$SQL = "select to_char(max(jou_start), 'DD.MM.YY HH24:MI') Import from journal ";
//$SQL .= "where jou_argument = 'filialbestand'";


$SQL = "SELECT FIB_MAX_DATUM AS IMPORT FROM FILIALBESTANDSTAND";
$rsResult = awisOpenRecordset($con, $SQL);
$rsResultZeilen = $awisRSZeilen;
If($rsResultZeilen==1)
{
	echo '<br><font size=2>Stand der Daten: ' . $rsResult["IMPORT"][0] . '</font>';
}


awislogoff($con);

if(isset($_GET['Zurueck']) AND $_GET['Zurueck']=='ArtikelInfos')
{
	$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
}
elseif(isset($_GET['Zurueck']) AND $_GET['Zurueck']=='ArtikelListe')
{
	$ZurueckLink = '/ATUArtikel/artikel_Main.php?Liste=True&cmdAktion=ArtikelInfos';
}
elseif(isset($_GET['Zurueck']) AND $_GET['Zurueck']=='ArtikelInfosNeu')
{
	$ZurueckLink = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY='.$_GET['ASTKEY'];
}
elseif(isset($_GET['Zurueck']) AND $_GET['Zurueck']=='ArtikelListeNeu')
{
	$ZurueckLink = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Liste=True';
}
else
{
	$ZurueckLink = '/index.php';
}

echo '</form>';

echo "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
echo "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=filialbestand','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

echo '<Script Language=JavaScript>';
echo "document.getElementsByName(\"ATUNR\")[0].focus();";
echo '</Script>';

?>
</body>
</html>

