<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$AWIS_KEY1=$_POST['txtLKK_KEY'];

    if(isset($_POST['txtLKF_FEEDBACK'])){
        $SQL = 'SELECT LKF_KEY FROM LIEFERKONTROLLENFEEDBACK WHERE LKF_LKK_KEY = ' . $AWISDB->WertSetzen('LKF', 'Z', $AWIS_KEY1);
        $rsLKF_Sp = $AWISDB->RecordSetOeffnen($SQL, $AWISDB->Bindevariablen('LKF'));

        if ($rsLKF_Sp->AnzahlDatensaetze() > 0) {
            //Update
            $SQL = "UPDATE LIEFERKONTROLLENFEEDBACK ";
            $SQL .= " SET ";
            $SQL .= " LKF_ZEIT = " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_ZEIT']);
            $SQL .= ", LKF_DATUM = " . $AWISDB->WertSetzen('LKF', 'D', $_POST['txtLKF_DATUM']);
            $SQL .= ", LKF_ZUFRIEDEN = " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_ZUFRIEDEN']);
            $SQL .= ", LKF_TEXT = " . $AWISDB->WertSetzen('LKF', 'T', $_POST['txtLKF_TEXT']);
            $SQL .= ", LKF_SCHADEN = " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_SCHADEN']);
            $SQL .= ", LKF_USER = " . $AWISDB->WertSetzen('LKF', 'T', $AWISBenutzer->BenutzerName());
            $SQL .= ", LKF_USERDAT = SYSDATE";
            $SQL .= " WHERE LKF_LKK_KEY = " . $AWISDB->WertSetzen('LKF', 'Z', $AWIS_KEY1);

            $AWISDB->Ausfuehren($SQL, '', true, $AWISDB->Bindevariablen('LKF'));
        } else {
            //Insert
            $SQL = "INSERT INTO LIEFERKONTROLLENFEEDBACK ";
            $SQL .= " (";
            $SQL .= " LKF_LKK_KEY,";
            $SQL .= " LKF_ZEIT,";
            $SQL .= " LKF_DATUM,";
            $SQL .= " LKF_ZUFRIEDEN,";
            $SQL .= " LKF_TEXT,";
            $SQL .= " LKF_STATUS,";
            $SQL .= " LKF_SCHADEN,";
            $SQL .= " LKF_USER,";
            $SQL .= " LKF_USERDAT";
            $SQL .= " )";
            $SQL .= " VALUES";
            $SQL .= " (";
            $SQL .= " " . $AWISDB->WertSetzen('LKF', 'Z', $AWIS_KEY1);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_ZEIT']);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'D', $_POST['txtLKF_DATUM']);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_ZUFRIEDEN']);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'T', $_POST['txtLKF_TEXT']);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'Z', 0);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'Z', $_POST['txtLKF_SCHADEN']);
            $SQL .= ", " . $AWISDB->WertSetzen('LKF', 'T', $AWISBenutzer->BenutzerName());
            $SQL .= ", SYSDATE";
            $SQL .= " )";

            $AWISDB->Ausfuehren($SQL, '', true, $AWISDB->Bindevariablen('LKF'));
        }
    }
		
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtLIK_MENGEIST',1,1));
	//$Form->DebugAusgabe(1,$Felder);
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,16));
				$SQL = 'UPDATE Lieferkontrollen';
				$SQL .= ' SET LIK_MENGEIST='.intval($_POST[$Feld]);			
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' ,LIK_STATUS = CASE WHEN LIK_MENGESOLL=('.intval($_POST[$Feld]).') THEN 11 ';						
				$SQL .= ' ELSE CASE WHEN LIK_STATUS<10 AND LIK_MENGESOLL<>('.intval($_POST[$Feld]).') THEN 2 ';			
				$SQL .= ' ELSE 12';
				$SQL .= ' END';
				$SQL .= ' END';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				
				if($AWISDB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}				
			}
		}		
	}

    //Wenn Feedbacks gesammelt werden dann nur Abchliessen wenn auch Feedback eingegeben
    $FeedbackOK = true;

    if ($AWISBenutzer->ParameterLesen('LieferkontrollFeedback') == "1") {
        $FeedbackOK = false;
        $SQL = "SELECT LKF_KEY FROM LIEFERKONTROLLENFEEDBACK";
        $SQL .= " WHERE LKF_LKK_KEY = ".$AWISDB->WertSetzen('LKF', 'Z', $AWIS_KEY1);

        $rsFEEDBACK = $AWISDB->RecordSetOeffnen($SQL,$AWISDB->Bindevariablen('LKF'));
        if($rsFEEDBACK->AnzahlDatensaetze()>0){
            $FeedbackOK = true;
        }
    }

    //Wenn keine Differenzen mehr bestehen, LSNR automatisch abschliessen
    $BindeVariablen = array();
    $BindeVariablen['var_N0_lik_lkk_key'] = intval($AWIS_KEY1);

    $SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_MENGEIST<>LIK_MENGESOLL AND LIK_LKK_KEY = :var_N0_lik_lkk_key';
    $rsLIK = $AWISDB->RecordSetOeffnen($SQL, $BindeVariablen);

    if ($rsLIK->FeldInhalt('ANZAHLDIFF') == 0 and $FeedbackOK)
    {
        $SQL = 'UPDATE LIEFERKONTROLLENKOPF SET LKK_STATUS = \'A\' WHERE LKK_KEY = ' . intval($AWIS_KEY1);

        if ($AWISDB->Ausfuehren($SQL) === false) {
            $Form->Fehler_Anzeigen('SpeicherFehler', $awisDBFehler['message'], 'WIEDERHOLEN', 5, '908121043');
            throw new awisException('Fehler beim Speichern', 908121043, $SQL, 2);
        }
    }
	
	
	// Sollbestand
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtLIK_SOLLBESTAND',1,1));	
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,19));
				
				$SQL = 'UPDATE Lieferkontrollen';
				$SQL .= ' SET LIK_SOLLBESTAND='.intval($_POST[$Feld]);
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				
				if($AWISDB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}								
			}	
		}
	}
	
	// Istbestand
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtLIK_ISTBESTAND',1,1));	
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,18));
				
				$SQL = 'UPDATE Lieferkontrollen';
				$SQL .= ' SET LIK_ISTBESTAND='.intval($_POST[$Feld]);
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				
				if($AWISDB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}								
			}
		}
	}

	if(isset($_POST['cmdAdmin_x'])){
	    if(isset($_POST['txtadmin_LIK_LOESCHTYP']) and $_POST['txtadmin_LIK_LOESCHTYP'] != ''){

	        $Delete = ' delete from lieferkontrollen ';
	        $Delete .= ' WHERE lik_mengesoll=0 ';
	        $Delete .= ' AND LIK_LKK_KEY = ' . $AWISDB->WertSetzen('DEL','Z',$_POST['txtLKK_KEY']);

	        $Update = ' update lieferkontrollen ';
            $Update .= ' set lik_mengeist=0, lik_mengescan=0, lik_datumscan=null, lik_status=0 ';
            $Update .= ' WHERE lik_mengesoll=0 and lik_mengeist<>0 ';
            $Update .= ' AND LIK_LKK_KEY = ' . $AWISDB->WertSetzen('UPD','Z',$_POST['txtLKK_KEY']);

            if($_POST['txtadmin_LIK_LOESCHTYP'] == '1'){ //Mit Datum l�schen
                $Delete .= ' and lik_datumscan=' . $AWISDB->WertSetzen('DEL','D',$_POST['txtadmin_LOESCHDATUM']);
                $Update .= ' and lik_datumscan=' . $AWISDB->WertSetzen('UPD','D',$_POST['txtadmin_LOESCHDATUM']);
            }
            
            $AWISDB->Ausfuehren($Delete,'',true,$AWISDB->Bindevariablen('DEL'));
            $AWISDB->Ausfuehren($Update,'',true,$AWISDB->Bindevariablen('UPD'));
                       
        }
    }
}


catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$ex->getSQL());
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
catch (Exception $ex)
{
	
}
?>