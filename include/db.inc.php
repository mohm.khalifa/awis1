<?php
/**
 * DB.INC.PHP
 *
 * Zentrale Include Datei fuer Zugriffe rund um die Datenbank
 *
 * Autor: Sacha Kerres
 *
 *********************************************************************************
 *
 * Aenderungen
 *
 * 18.07.2006	Sacha Kerres		Header mit diesen Zeilen eingefuegt. Bitte alle anderen
 * 									Aenderungen ebenfalls dokumentieren!
 * 18.07.2006	Sacha Kerres		awisErrorMailLink() Funktion fertig programmiert. Liefert jetzt auch
 * 									die Meldung und einen SQL Text bei DB-Fehlern!
 * 19.04.2012	Sacha Kerres		Datenbankverbindungen angepasst. Auf Bindevariablen umgestellt.
 *
 * 25.09.2012	Christian Argauer	�nderungen Bindevariablen. �bergabe L�nge $Var['variable'][XXX], Ausgabe Bindevariablen.
 *
 */
require_once('sicherheit.inc.php');

//******************************************************
// Globale Variablen
//******************************************************

$awisRSInfo=array();			// Infos ueber die Spalten
$awisRSInfoName=array();			// Infos ueber die Spalten(-namen)
$awisRSSpalten = 0;			// Anzahl der eingelesenen Spalten
$awisRSZeilen = 0;			// Anzahl der eingelesenen Zeilen
$awisBenutzerID = 0;		// Aktuelle Benutzer-ID auf dem Server
$awisDBFehler = array('');
$BindeVariablenAusgabe=array();

/******************************************************
* Verwendet den Backup-Server
* ****************************************************/

/**
 * Liest Parameter aus den AWIS - Konfigdateien
 * @param string $INIQuelle
 * @param string $Parameter
 * @return string
 */
function awisLeseAWISParameter($INIQuelle, $Parameter)
{
	$Erg = '';

	// Pruefen, ob es eine Datei mit dem Namen gibt
	if(is_file('/daten/conf/'.$INIQuelle))
	{
		$Zeilen = file('/daten/conf/'. $INIQuelle);
		foreach($Zeilen AS $Zeile)				// Alle Zeilen einlesen
		{
			$Zeile = trim($Zeile);
			if(strpbrk(substr(trim($Zeile),0,1),'#;')===false)	// Kommentarzeile?
			{
				$Inhalt = explode('=',$Zeile);
				if(sizeof($Inhalt)==2)			// Zwei Eintraege  PARAM=WERT
				{
					if(strcasecmp(trim($Inhalt[0]),$Parameter)==0)
					{
						$Erg = trim($Inhalt[1]);// Wert zurueckliefern
					}
				}
			}
		}
	}

	return $Erg;
}

/**
 * Ermittelt den Namen des Backup-DB-Servers
 * @param string $AktVerbindung
 */
Function awisBackupDBServer($AktVerbindung)
{
	// > muss bleiben, wg Fehler in OCILogon()!!
	echo '><span class=Hinweistext>';

	echo 'Es ist kein Datenbank-Server ' . $AktVerbindung . ' verf&uuml;gbar. Bitten wenden Sie sich an den <a href=mailto:edv-helpdesk@de.atu.eu?subject=AWIS>AWIS-Helpdesk</a>.';

	echo '</span>';
 	die();
}


/******************************************************
* Auswahl eines Datenbank-Servers
******************************************************/
Function awisDBServer()
{
	$Level = awisLeseAWISParameter('awis.conf','AWIS_LEVEL');

	switch($Level)
	{
		case 'ENTW':
			$DBServer = "AWISENTWICK_SH";
			break;
		case 'STAG':
			$DBServer = "AWISSTAG_SH";
			break;
		case 'PROD':
			$DBServer = "AWISPROD_SH";
			break;
		case 'SHUT':
			$DBServer = "AWISPROD_SH";
			break;
		default:
			$DBServer = "AWISENTWICK_SH";
			break;
	}

	return $DBServer;
}

/**
* Anmelden am Server
*
* @param boolean $debug
* @return ressource $con
*
******************************************************/
Function awisLogon($Debug=True)
{
	//OCIInternalDebug(-1);		//Ausfuehrliche Meldungen

	$DBServer = awisDBServer();

	$con = @OCILogon("awis","IlOuLua",$DBServer);
	if($con==FALSE)
	{
 		$DBServer = awisBackupDBServer($DBServer);
  		$con = @OCILogon("awis","IlOuLua",$DBServer);
  		if($con==FALSE)
  		{
  			if($Debug)
  			{
  				$DebugText = "<!-- " . system('echo $ORACLE_HOME').'-->';
  			}
  			die("<hr>Keine Verbindung zu " . $DBServer . " m&ouml;glich.");
  		}
 	}

	// Achtung!!! Die Reihenfolge ist nicht beliebig!!!!
	//awisExecute($con, "ALTER SESSION SET NLS_TERRITORY='GERMANY'");
	//awisExecute($con, "ALTER SESSION SET NLS_LANGUAGE='GERMAN'");
	//awisExecute($con, "ALTER SESSION SET NLS_CHARACTERSET='WE8MSWIN1252'");

	//awisExecute($con, "ALTER SESSION SET NLS_LANG='GERMAN_GERMANY.WE8MSWIN1252'");
	awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR'");
	awisExecute($con, "ALTER SESSION SET NLS_SORT='GERMAN'");

 	return $con;
}

//******************************************************
// Anmelden am Server
//******************************************************
Function awisAdminLogon($server="CROSS3",$user='entwick',$passwd='passwd')
{
 $con = OCILogon($user,$passwd,$server);
 if($con==FALSE)
 {
  die("Keine Verbindung moeglich.");
 }

 return $con;
}


//******************************************************
// Anmelden an einem beliebigen Server
//******************************************************
/**
 * Anmeldung am COM Cluster
 *
 * @param string $Modus (DE=Dedicated, SH=Shared)
 * @param bool $Debug (Meldung anzeigen / als Kommentar)
 * @return ressource
 */
function awisLogonComCluster($Modus='DE',$Debug=true)
{
	$Level = awisLeseAWISParameter('awis.conf','AWIS_LEVEL');

	switch($Level)
	{
		case 'ENTW':
			$DBServer = "COM_TEST_".$Modus;	// Testumgebung
			break;
		case 'STAG':
			$DBServer = "COM_TEST_".$Modus;	// Testumgebung
			break;
		case 'PROD':
			$DBServer = "COM_".$Modus;	// Produktivumgebung
			break;
		case 'SHUT':
			$DBServer = "COM_".$Modus;	// Produktivumgebung
			break;
		default:
			$DBServer = "COM_TEST_".$Modus;	// Testumgebung
			break;
	}
	
	$con = @OCILogon("awis","IlOuLua",$DBServer);
	if($con==FALSE)
	{
		echo '-->';
		echo "<hr><span class=HinweisText>Keine Verbindung zu " . $DBServer . " m&ouml;glich.</span>";
		echo '<br>';
		echo 'Wir bitten, diese Unannehmlichkeiten zu entschuldigen. Das System steht in K&uuml;rze wieder zur Verf&uuml;gung.';
		if($Debug)
		{
			echo '<!--'.$DBServer.'-->';
		}
		die();
 	}
 	

	// Achtung!!! Die Reihenfolge ist nicht beliebig!!!!
	awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR'");
	awisExecute($con, "ALTER SESSION SET NLS_SORT='GERMAN'");

 	return $con;
}

/**
* Anmelden am Server
*
* @param boolean $debug
* @return ressource $con
*
******************************************************/
Function awisLogonSchad($Debug=True)
{
	//OCIInternalDebug(-1);		//Ausfuehrliche Meldungen

	$DBServer = awisDBServer();

	$con = @OCILogon("schaddev09","schad",$DBServer);
	if($con==FALSE)
	{
 		$DBServer = awisBackupDBServer($DBServer);
  		$con = @OCILogon("schaddev09","schad",$DBServer);
  		if($con==FALSE)
  		{
  			if($Debug)
  			{
  				$DebugText = "<!--" . system('echo $ORACLE_HOME').'-->';
  			}
  			die("<hr>Keine Verbindung zu " . $DBServer . " m&ouml;glich.");
  		}
 	}

	// Achtung!!! Die Reihenfolge ist nicht beliebig!!!!
	//awisExecute($con, "ALTER SESSION SET NLS_TERRITORY='GERMANY'");
	//awisExecute($con, "ALTER SESSION SET NLS_LANGUAGE='GERMAN'");
	//awisExecute($con, "ALTER SESSION SET NLS_CHARACTERSET='WE8MSWIN1252'");
	//awisExecute($con, "ALTER SESSION SET NLS_LANG='GERMAN_GERMANY.WE8MSWIN1252'");
	awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR'");
	awisExecute($con, "ALTER SESSION SET NLS_SORT='GERMAN'");

 	return $con;
}


//******************************************************
// Abmelden vom Server
//******************************************************

/**
 *  Schliesst eine DB-Verbindung
 *
 * @param DB-connection $con
 * @author Sacha Kerres
 *
 */
Function awisLogoff($con)
{
	OCILogoff($con);
}


//******************************************************
// Bilder vom Server auslesen
//******************************************************
/**
 * Bild aus DB abfragen -> Tabelle AWIS.BILDER
 *
 * @param DB-Connection $con
 * @param string $SQLBefehl
 * @param boolean $Debug
 * @param boolean $Meldung
 * @return Text
 * @author  Christian Argauer
 */
function awisBilder($con, $SQLBefehl,$Debug=TRUE,$Meldung=false)
{
	global $awisDBFehler;

	if(strpos($SQLBefehl,"\\'")!==false)
	{
		$SQLBefehl = str_replace("\\'","''",$SQLBefehl);
	}

	$cmd = OCIParse($con, $SQLBefehl);
	if($cmd==FALSE)
	{
		$awisDBFehler = OCIError();				// Fehler setzen
 		if($Debug == TRUE)
		{
 			echo "\nFehler(1):" . $awisDBFehler['message'] . "-->";
			return FALSE;
		 }
		awisLogoff($con);
 		die("<span class=HinweisText>Fehler beim ausfuehren eines SQL Befehls.</span>\n<!--::$SQLBefehl::-->");
	}

	if(OCIExecute($cmd,OCI_DEFAULT)===false)
	{
		$skawisDBFehler = OCIError($cmd);				// Fehler setzen
		$skawisDBFehler['SQL']=$SQLBefehl;
			// Fehler protokollieren
		awisEreignis(3,1200,$awisDBFehler['message'],str_replace('\'','\\\'',$SQLBefehl));
		if($Meldung==true)
		{
			echo '<table border=1 width=100%><tr><td colspan=2 class=HinweisText>AWIS-FEHLER</td></tr>';
			echo '<tr><td valign=top width=250 class=FeldBez>SQL Anweisung:</td><td>' . substr($SQLBefehl,0,$skawisDBFehler['offset']) . '<font color=#FF0000>' . substr($SQLBefehl,$skawisDBFehler['offset']) . '</font></td>';
			echo '<tr><td class=FeldBez>Zeichen:</td><td>' . $skawisDBFehler['offset'] . '</td>';
			echo '<tr><td class=FeldBez>DB-Fehlercode:</td><td>' . $skawisDBFehler['code'] . '</td>';
			echo '<tr><td valign=top class=FeldBez>DB-Fehlermeldung:</td><td>' . $skawisDBFehler['message'] . '</td>';

			// Hinweis zum Fehler anzeigen
			if(file_exists('/daten/web/FehlerMeldungen/DB_ERROR_' . $skawisDBFehler['code'] . '.html'))
			{
				echo '<tr><td valign=top class=FeldBez>Hinweis:</td><td>' . file_get_contents('/daten/web/FehlerMeldungen/DB_ERROR_' . $awisDBFehler['code'] . '.html') . '</td>';
			}

			echo '</table>';
		}
		else
		{
			echo "<hr><span class=HinweisText>Fehler beim Ausfuehren des SQL Befehls: " . $SQLBefehl . "<br>";
			echo "Die Meldung war: " .$skawisDBFehler['message'] . "</span><hr>";
		}

		if($Debug)
		{
 			echo "<!--\nFehler(2):" . $skawisDBFehler['message'] . "-->";
			//return FALSE;
		}
		OCIFreeStatement($cmd);
		$awisDBFehler = $skawisDBFehler;
		return FALSE;
	}
	$row=null;
	while (ocifetchinto($cmd,$row,OCI_ASSOC))
	{
		$data=$row["BILD"]->load();
	}

	OCIFreeStatement($cmd);

	return $data;
}

//******************************************************
// Befehl ausfuehren
//******************************************************

/**
 * Einen SQL Befehl ausfuehren
 *
 * @param DB-Connection $con
 * @param string $SQLBefehl
 * @param boolean $Debug
 * @param boolean $Meldung
 * @param int $Transaktion(0=ohne (Default), 1=Beginn, 2=Commit)
 * @param array $BindeVariablen
 * @return int
 * @author  Sacha Kerres
 */
Function awisExecute($con, $SQLBefehl, $Debug=TRUE, $Meldung=false, $Transaktion=0, $BindeVariablen=array())
{
	global $awisDBFehler;
	global $BindeVariablenAusgabe;

	$ZeitMessung['Start']=date('c');
	$skawisDBFehler = array();			// Fehler zuruecksetzen
	$skawisDBFehler['message'] = '';

		// Hochkomma kommt u.U. als \' an. Fuer Oracle durch '' ersetzen
	if(strpos($SQLBefehl,"\\'")!==false)
	{
		$SQLBefehl = str_replace("\\'","''",$SQLBefehl);
	}
	$cmd = OCIParse($con, $SQLBefehl);
	$ZeitMessung['Parsen']=date('c');
	if($cmd==FALSE)
	{
		$awisDBFehler = OCIError();				// Fehler setzen
 		if($Debug == TRUE)
		{
 			echo "\nFehler(1):" . $awisDBFehler['message'] . "-->";
			return FALSE;
		 }
		awisLogoff($con);
 		die("<span class=HinweisText>Fehler beim ausfuehren eines SQL Befehls.</span>\n<!--::$SQLBefehl::-->");
	}

	// Falls Variablen mitgegeben werden, diese binden
	// �nderung vom 2012/04
	if(!empty($BindeVariablen))
	{
		foreach($BindeVariablen AS $Name=>$Wert)
		{
			$NamensTeile = explode('_',$Name);

			if (is_array($BindeVariablen[$Name]))
			{
				foreach ($BindeVariablen[$Name] as $Name2 => $Wert2)
				{
					$BindLenght=$Name2;
				}
			}
			else
			{
				$BindLenght=-1;
			}

			if(isset($NamensTeile[1]))
			{
				switch($NamensTeile[1])
				{
					case 'N0':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_INT);
						break;
						/*
					case 'NX':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_NUM);
						break;
						*/
					case 'T':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_CHR);
						break;
					case 'D':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
						break;
					default:
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
				}
			}
			else
			{
				$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
			}
		}
	}

	if(OCIExecute($cmd,($Transaktion==0?OCI_COMMIT_ON_SUCCESS:OCI_DEFAULT))===false)
	{
		$skawisDBFehler = OCIError($cmd);				// Fehler setzen
		$awisDBFehler=$skawisDBFehler;
		$skawisDBFehler['SQL']=$SQLBefehl;
			// Fehler protokollieren
		awisEreignis(3,1200,$awisDBFehler['message'],str_replace('\'','\\\'',$SQLBefehl));
		if($Meldung==true)
		{
			echo '<table border=1 width=100%><tr><td colspan=2 class=HinweisText>AWIS-FEHLER</td></tr>';
			echo '<tr><td valign=top width=250 class=FeldBez>SQL Anweisung:</td><td>' . substr($SQLBefehl,0,$skawisDBFehler['offset']) . '<font color=#FF0000>' . substr($SQLBefehl,$skawisDBFehler['offset']) . '</font></td>';
			echo '<tr><td class=FeldBez>Zeichen:</td><td>' . $skawisDBFehler['offset'] . '</td>';
			echo '<tr><td class=FeldBez>DB-Fehlercode:</td><td>' . $skawisDBFehler['code'] . '</td>';
			echo '<tr><td valign=top class=FeldBez>DB-Fehlermeldung:</td><td>' . $skawisDBFehler['message'] . '</td>';

			// Hinweis zum Fehler anzeigen
			if(file_exists('/daten/web/FehlerMeldungen/DB_ERROR_' . $skawisDBFehler['code'] . '.html'))
			{
				echo '<tr><td valign=top class=FeldBez>Hinweis:</td><td>' . file_get_contents('/daten/web/FehlerMeldungen/DB_ERROR_' . $awisDBFehler['code'] . '.html') . '</td>';
			}

			echo '</table>';
		}
		else
		{
			echo "<hr><span class=HinweisText>Fehler beim Ausf&uuml;hren des SQL Befehls: " . $SQLBefehl . "<br>";
			echo "Die Meldung war: " .$skawisDBFehler['message'] . "</span><hr>";
		}

		if($Debug)
		{
 			echo "<!--\nFehler(2):" . $skawisDBFehler['message'] . "-->";
			//return FALSE;
		}
		OCIFreeStatement($cmd);
		$awisDBFehler = $skawisDBFehler;
		return FALSE;
	}
	else
	{
		if(!empty($BindeVariablen))
		{
			foreach($BindeVariablen AS $Name=>$Wert)
			{
				$BindeVariablenAusgabe[$Name]=$BindeVariablen[$Name];
			}
		}
	}



	$ZeitMessung['Execute']=date('c');
	$Zeilen = OCIRowCount($cmd);
	$ZeitMessung['Ende']=date('c');
	OCIFreeStatement($cmd);

	// Transaktion abschliessen
	if($Transaktion==2)
	{
		ocicommit($con);
	}

	$ZeitMessung['SQL']=str_replace("\n", '', $SQLBefehl);
	$ZeitMessung['SQL']=str_replace("\r", '', $ZeitMessung['SQL']);

	//file_put_contents('/home/awis/awis_db.log', implode("\t", $ZeitMessung).PHP_EOL,FILE_APPEND);

	$awisDBFehler = $skawisDBFehler;		// Fehler zurueckliefern
 	return $Zeilen;
}

/**
 * Recordset in der Datenbank oeffnen
 *
 * @param ressource $con
 * @param string $SQL
 * @param boolean $Debug
 * @param boolean $Meldung
 * @param array $BindeVariablen
 * @param string $FetchType (Default => const OCI_FETCHSTATEMENT_BY_COLUMN)
 * @return unknown
 */
Function awisOpenRecordset($con, $SQL, $Debug=True, $Meldung=false, $BindeVariablen=array(), $FetchType=OCI_FETCHSTATEMENT_BY_COLUMN)
{
	GLOBAL $awisRSZeilen;
	GLOBAL $awisRSSpalten;
	GLOBAL $awisDBFehler;
	global $awisRSInfo;
	global $awisRSInfoName;
	global $BindeVariablenAusgabe;

	$ZeitMessung['Start']=date('c');

	if(!$Debug)
	{
		echo "<!--OpenRec\n";								// Fehler auskommentieren
	}
	if($con===null)
	{
		echo '.';
	}

	$cmd = OCIParse($con, $SQL);
	$ZeitMessung['Parsen']=date('c');

	if($cmd===FALSE)
	{
		$awisDBFehler = OCIError($SQL);				// Fehler setzen
		echo "<hr>AWIS-2606-1054: Fehler beim parsen eines SQL Befehls.<!--" . $SQL . "-->\n";
		if(!$Debug)
		{
			echo "\n";
			print_r($awisDBFehler);
			echo "\n-->";
		}
		return FALSE;
	}
	

	// Falls Variablen mitgegeben werden, diese binden
	// �nderung vom 2012/04
	if(!empty($BindeVariablen))
	{
		foreach($BindeVariablen AS $Name=>$Wert)
		{
			$NamensTeile = explode('_',$Name);

			if (is_array($BindeVariablen[$Name]))
			{
				foreach ($BindeVariablen[$Name] as $Name2 => $Wert2)
				{
					$BindLenght=$Name2;
				}
			}
			else
			{
				$BindLenght=-1;
			}

			if(isset($NamensTeile[1]))
			{
				switch($NamensTeile[1])
				{
					case 'N0':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_INT);
						break;
						/*
					case 'NX':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_NUM);
						break;
						*/
					case 'T':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name],$BindLenght,SQLT_CHR);
						break;
					case 'D':
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
						break;
					default:
						$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
				}
			}
			else
			{
				$Erg = oci_bind_by_name($cmd, $Name, $BindeVariablen[$Name]);
			}
		}
	}


	if(OCIExecute($cmd)===false)
	{
		$awisDBFehler = OCIError($cmd);				// Fehler setzen

		if($Meldung==true)
		{
			echo '<table border=1 width=100%><tr><td colspan=2 class=HinweisText>AWIS-FEHLER</td></tr>';
			echo '<tr><td valign=top width=250 class=FeldBez>SQL Anweisung:</td><td>' . substr($SQL,0,$awisDBFehler['offset']) . '<font color=#FF0000>' . substr($SQL,$awisDBFehler['offset']) . '</font></td>';
			echo '<tr><td class=FeldBez>Zeichen:</td><td>' . $awisDBFehler['offset'] . '</td>';
			echo '<tr><td class=FeldBez>DB-Fehlercode:</td><td>' . $awisDBFehler['code'] . '</td>';
			echo '<tr><td valign=top class=FeldBez>DB-Fehlermeldung:</td><td>' . $awisDBFehler['message'] . '</td>';

			// Hinweis zum Fehler anzeigen
			if(file_exists('/daten/web/FehlerMeldungen/DB_ERROR_' . $awisDBFehler['code'] . '.html'))
			{
				echo '<tr><td valign=top class=FeldBez>Hinweis:</td><td>' . file_get_contents('/daten/web/FehlerMeldungen/DB_ERROR_' . $awisDBFehler['code'] . '.html') . '</td>';
			}

			echo '</table>';
		}
		else
		{
	    	echo "<hr>AWIS-26061055: Fehler beim ausfuehren eines SQL Befehls.<!--" . $SQL . '+++'.$awisDBFehler['message'] . "-->\n";
		}

		if(!$Debug)
		{
			echo "\nERR: ";
			print_r($awisDBFehler);
			echo "\n-->";
		}
		return FALSE;
	}
	else
	{
		if(!empty($BindeVariablen))
		{
			foreach($BindeVariablen AS $Name=>$Wert)
			{
				$BindeVariablenAusgabe[$Name]=$BindeVariablen[$Name];
			}
		}
	}

	$ZeitMessung['Execute']=date('c');

	$RowSet=array();
	$awisRSZeilen = oci_fetch_all($cmd, $RowSet,0,-1,$FetchType);
	$awisDBFehler = oci_error();					// Fehler oder FALSE setzen


	$awisRSSpalten = oci_num_fields($cmd);

		// Infos aufbauen
	//$awisRSInfo[] = '';			// Infos ueber die Spalten
	$awisRSInfo = array();
	for($i=1;$i<=$awisRSSpalten;$i++)
	{
		$awisRSInfo[$i]["Name"] = oci_field_name($cmd, $i);
		$awisRSInfoName[oci_field_name($cmd, $i)]["Nr"] = ($i);
		$awisRSInfo[$i]["Typ"] = oci_field_type($cmd, $i);
		$awisRSInfoName[oci_field_name($cmd, $i)]["Typ"] = oci_field_type($cmd, $i);
		$awisRSInfo[$i]["Groesse"] = oci_field_size($cmd, $i);
		$awisRSInfoName[oci_field_name($cmd, $i)]["Groesse"] = oci_field_size($cmd, $i);
		switch (strtolower(oci_field_type($cmd, $i)))
		{
			case 'varchar':
			case 'varchar2':
			case 'char':
				$awisRSInfo[$i]["TypKZ"] = 'T';
				$awisRSInfoName[oci_field_name($cmd, $i)]["TypKZ"] = 'T';
				break;
			case 'date':
				$awisRSInfo[$i]["TypKZ"] = 'D';
				$awisRSInfoName[oci_field_name($cmd, $i)]["TypKZ"] = 'D';
				break;
			default:
				$awisRSInfo[$i]["TypKZ"] = 'Z';
				$awisRSInfoName[oci_field_name($cmd, $i)]["TypKZ"] = 'Z';
				break;
		}
	}

	reset($RowSet);
	OCIFreeStatement($cmd);

	if(!$Debug)
	{
		echo "\n-->";								// Ende Fehler auskommentieren
	}
	$ZeitMessung['Ende']=date('c');
	$ZeitMessung['SQL']=str_replace("\n", '', $SQL);
	$ZeitMessung['SQL']=str_replace("\r", '', $ZeitMessung['SQL']);

	//file_put_contents('/home/awis/awis_db.log', implode("\t", $ZeitMessung).PHP_EOL,FILE_APPEND);

	return $RowSet;
}

/**
 * Liefert Informationen zu einer SpaltenListe
 *
 * @param array SpaltenInfo
 * @param string $Feldname
 * @param string $Info (Typ, Groesse)
 * @return string
 */
function awisFeldInfo($SpaltenInfo, $Feldname, $Info)
{
	for($i=0;$i<sizeof($SpaltenInfo);$i++)
	{
		if($SpaltenInfo[$i]['Name']==$Feldname)
		{
			return($SpaltenInfo[$i][$Info]);
		}
	}
	return '';
}
//*****************************************************
// Server-Info
//*****************************************************

Function awisServerInfo($con)
{
 	return OCIServerVersion($con);
}

//*****************************************************
// Gibt einen Link mit einer Fehlermeldung an
//*****************************************************

/**
 * @return void
 * @param SeitenName Text
 * @param FehlerTyp Zahl
 * @desc Erzeugt einen Link um eine E-Mail an den AWIS Admin zu versenden
 */
Function awisErrorMailLink($SeitenName, $FehlerTyp, $Nachricht='', $ZusatzInfo='')
{
	global $REMOTE_ADDR;
	global $HTTP_USER_AGENT;
	global $VInfo;
	global $awisDBFehler;
	global $AWISBenutzer;

	echo '<img src=/bilder/kaefer.ico border=0>';
	echo '<br><span class=HinweisText>Es ist ein Fehler aufgetreten (Zeit: ' . date('d.m.Y-h:i') . ')<br><br>';

	echo 'Bitte klicken Sie auf den unten aufgefuehrten Link, um ein Mail mit den notwendigen
		Fehlermeldungen an den Entwickler zu senden. Veraendern Sie die Nachricht bitte nicht,
		da sie automatisch ausgwertet wird.<br><br>';

	echo "<a href=mailto:" . $VInfo["ERRORMAIL"] . "?Subject=AWIS:%20FEHLER%20bei%20$SeitenName";
	echo "&Body=";
	echo "AWIS:" . $VInfo["MODULNAME"];
	echo "~~";
	echo "Zeit:" . date('d.m.Y-h:i');
	echo "~~";
	echo "User:" . $AWISBenutzer->BenutzerName();
	echo "~~";
	echo "Computer:" . @gethostbyaddr($REMOTE_ADDR);
	echo "~~";
	echo "Browser:" . str_replace(" ", "%20", $HTTP_USER_AGENT);
	echo "~~";
	echo "Seite:" . $SeitenName;
	echo "~~";
	echo "Web-Server:" . $_SERVER['SERVER_NAME'];
	echo "~~";
	echo "Fehlertyp:" . $FehlerTyp;
	echo "~~";
	echo "INFO:" . str_replace(" ", "%20",$ZusatzInfo);
	echo "~~";
	if(is_array($Nachricht))
	{
		if(isset($Nachricht['message']))		// DBTyp fehler
		{
			echo "DBERROR:" . str_replace("\n","",str_replace(" ","%20",htmlentities($Nachricht['message'])));
			echo "~~SQL:" . str_replace(" ","%20",htmlentities($Nachricht['SQL']));
		}
		else
		{
			echo "ERROR:" . str_replace("\n","",str_replace(" ","%20",htmlentities(serialize($Nachricht))));
		}
	}
	else
	{
		if($awisDBFehler['message']!='')
		{
			echo "DBERROR:" . str_replace("\n","",str_replace(" ","%20",htmlentities($awisDBFehler['message'])));
			echo "~~SQL:" . str_replace(" ","%20",htmlentities($awisDBFehler['SQL']));
		}
		else
		{
			echo "ERROR:" . str_replace(" ", "%20",$Nachricht);
		}
	}
	echo "~~ENDE~~Fuegen_Sie_hier_bitte_Ihre_Anmerkungen_ein:";
	echo ">";
	echo "AWIS Admin benachrichtigen</a>";

	echo "<br><br>Wir bitten, diese Probleme zu entschuldigen.<br>Wir werden das Problem so schnell wie moeglich beheben.<br></span>";
}


//*******************************************************
// Funtion baut eine Bedingung in Oracle Format um
//
// Parameter:
//
//	ASCIIWort:    0 : nein
//                1 : ASCIIWORT
//                2 : ASCIIWORTOE
//*******************************************************
/**
 * Like oder =
 *
 * @param string $Bedingung
 * @param boolean $toupper
 * @param bollean $KeineUmlaute
 * @param int $SuchWort (0=nein, 1=SUCHWORT(), 2=SUCHWORTOE())
 * @param int $ASCIIWort (0=nein, 1=ASCIIWort, 2=ASCIIWortOE)
 * @return unknown
 */
Function awisLIKEoderIST($Bedingung, $toupper=0, $KeineUmlaute=0, $SuchWort=0, $ASCIIWort=0)
{
	$Erg1 = '';
	$Erg= str_replace('*','%',$Bedingung);
	$Erg= str_replace('?','_',$Erg);

	if($toupper)
	{
		$Erg = mb_strtoupper($Erg);
	}

	if($KeineUmlaute)
	{
		$Erg = str_replace('�','ae',$Erg);
		$Erg = str_replace('�','ue',$Erg);
		$Erg = str_replace('�','oe',$Erg);
		$Erg = str_replace('�','AE',$Erg);
		$Erg = str_replace('�','UE',$Erg);
		$Erg = str_replace('�','OE',$Erg);
		$Erg = str_replace('�','ss',$Erg);
	}

	if($SuchWort===2)
	{
		$Erg = "SUCHWORTOE('" . $Erg . "')";
	}
	if($SuchWort==TRUE OR $SuchWort==1)
	{
		$Erg = "SUCHWORT('" . $Erg . "')";
	}

	if($ASCIIWort==1)			// ASCIIWORT
	{
		$Erg = mb_strtoupper($Erg);
		$Erg = str_replace('O', '0', $Erg);		// O in 0 umwandeln
		$FuehrendeNull=TRUE;

		for($i=0;$i<strlen($Erg);$i++)
		{
			$Zchn = ord($Erg[$i]);
			if(($Zchn > 48 And $Zchn < 58) OR
			   ($Zchn > 63 AND $Zchn < 91) OR
			   ($Zchn > 96 AND $Zchn < 123) OR
			   $Zchn==37 OR
			   $Zchn==181)
			{
				$Erg1 .= $Erg[$i];
				$FuehrendeNull=FALSE;
			}
			If($Zchn == 48 And $FuehrendeNull==FALSE)
			{
				$Erg1 .= '0';
			}
		}
	    $Erg = $Erg1;
	}

	if($ASCIIWort==2)			// ASCIIWORTOE
	{
		$Erg = mb_strtoupper($Erg);
		if(substr($Erg,0,1)=='N')		// Fuehrendes N weg
		{
			$Erg = substr($Erg,1);
		}

		$Erg = str_replace('O', '0', $Erg);		// O in 0 umwandeln
		$FuehrendeNull=TRUE;

		for($i=0;$i<strlen($Erg);$i++)
		{
			$Zchn = ord($Erg[$i]);
			if(($Zchn > 48 And $Zchn < 58) OR
			   ($Zchn > 63 AND $Zchn < 91) OR
			   ($Zchn > 96 AND $Zchn < 123) OR
			   $Zchn==37 OR
			   $Zchn==181)
			{
				if($i==0 AND $Erg[$i]=='A')
				{
					// Keine fuehrenden A
				}
				else
				{
					$Erg1 .= $Erg[$i];
					$FuehrendeNull=FALSE;
				}
			}
			If($Zchn == 48 And $FuehrendeNull==FALSE)
			{
				$Erg1 .= '0';
			}
		}
	    $Erg = $Erg1;
	}



	If(strstr($Erg,'%') || strstr($Erg,'_'))
	{
		If(strstr($Erg,'SUCHWORT('))
		{
			$Erg = "LIKE " . $Erg;
		}
		else
		{
			$Erg = "LIKE '" . $Erg . "'";
		}
	}
	else
	{
		If(strstr($Erg,'SUCHWORT('))
		{
			$Erg = "= " . $Erg;
		}
		else
		{
			$Erg = "='" . $Erg . "'";
		}
	}

	return $Erg;

}


//***************************************************************
// Funktion liefert die BenutzerID
//***************************************************************

/**
 * @param string (optional) LoginName
 * @param bool (optional) AlleAlsListe
 * @return long
 * @desc Funktion liefert die ID des aktuellen Benutzers
 */

Function awisBenutzerID($LoginName="", $AlleAlsListe=false)
{
	global $awisBenutzerID;
	global $awisRSZeilen;
	global $AWISBenutzer;

	if($LoginName=="")
	{
		$LoginName = $AWISBenutzer->BenutzerName();
	}

	if($awisBenutzerID==0)
	{
		$con = awisLogon();
		If($con)
		{
			$BindeVariablen=array();
			$BindeVariablen['var_T_xbl_login']=strtoupper($LoginName);

			$rsBenutzer = awisOpenRecordset($con, "SELECT XBN_KEY FROM MV_BENUTZER_LOGIN WHERE XBL_LOGIN=:var_T_xbl_login",true,false,$BindeVariablen);

			if(!$AlleAlsListe)
			{
				awislogoff($con);
				return $rsBenutzer["XBN_KEY"][0];
			}
			else
			{
				$Erg = array();
				for($i=0;$i<$awisRSZeilen;$i++)
				{
					$Erg[$i] = $rsBenutzer["XBN_KEY"][$i];
				}
				awislogoff($con);
				return $Erg;
			}
		}
	}
	return 0;
}

//***************************************************************
// Funktion liefert den Benutzer-Namen
//***************************************************************

/**
 * @return string
 * @desc Funktion liefert den Namen des aktuellen Benutzers
 */
Function awisBenutzerName()
{
	global $AWISBenutzer;

	return $AWISBenutzer->BenutzerName();
}
/******************************************************************************************
*
* Funktion: string awisListenParameter($Liste)
*
* 	Loest Listen auf, um sie in Abfragen verwenden zu koennen
*
* 	Bsp.: 1-3,10,14-16	0> 1,2,3,10,14,15,16
******************************************************************************************/

Function awisListenParameter($Liste)
{
	$Erg = '';		// Rueckgabewert
	$i = 0;			// Zähler
	$zchn = '';		// Zeichen;
	$pos = 0;		// Position im Text
	$Ende = 0;
	$WertVon = 0;
	$WertBis = 0;
	$Zahl = 0;
	$Trennzeichen = ",";
	$Step = 1;

	If(strpos($Liste,";")!=FALSE)
	{
		$Trennzeichen = ";";
	}


	for($i=0;$i<strlen($Liste);$i++)
	{
		$pos = strpos($Liste,$Trennzeichen,$i);
		if($pos != FALSE)
		{
			$Ende = $pos-$i;
		}
		else
		{
			$Ende = 9999;
		}

			$zchn = substr($Liste,$i,$Ende);

			$i += strlen($zchn);
			if(strpos($zchn,"-") != FALSE)
			{
				$WertVon = substr($zchn,0,strpos($zchn,"-"));
				$WertBis = substr($zchn,strpos($zchn,"-")+1);

				if(ord($WertVon[0])>=47 and ord($WertVon[0])<=58)
				{
					if(strpos($WertBis,"["))
					{
						$Step = substr($WertBis,strpos($WertBis,"[")+1,strlen($WertBis)-strpos($WertBis,"[")-2);
						$Step = doubleval(str_replace(",",".",$Step));
					}
					else
					{
						$Step=1;
					}

					$WertVon = str_replace(",",".",$WertVon);
					$WertBis = str_replace(",",".",$WertBis);

					for($Zahl=doubleval($WertVon);$Zahl<=$WertBis;$Zahl += $Step)
					{
						$Erg .= "," . $Zahl;
					}
				}
				else
				{
					for($Zahl=ord($WertVon);$Zahl<=ord($WertBis);$Zahl++)
					{
						$Erg .= ",'" . chr($Zahl) . "'";
					}
				}
			}
			else
			{
				$Erg .= "," . $zchn;
			}
	}
	return substr($Erg,1);
}

/****************************************************************************************************
*
*
*
****************************************************************************************************/
Function awisImportQuellenText($con, $IMQID, $ZusatzInfo = '')
{
	global $awisRSZeilen;
	$Erg = '';

	$rsIMQ = awisOpenRecordset($con, "SELECT * FROM AWIS.Importquellen ORDER BY IMQ_ID");

	for($_i=0;$_i<$awisRSZeilen;$_i++)
	{
		if(($IMQID & $rsIMQ['IMQ_ID'][$_i])==$rsIMQ['IMQ_ID'][$_i])
		{
			$Erg .= "/ " . $rsIMQ['IMQ_ANZEIGE'][$_i];
		}
	}
	unset($rsIMQ);

	if($ZusatzInfo != '')
	{
		// Username
	 	$Erg = str_replace('%u', $ZusatzInfo , $Erg);
	}

	return substr($Erg,1);
}


/********************************************************************************************************************
*
* _ArtikelInfo(ID, Recordset, ArtikelNummer)
*
*********************************************************************************************************************/

function awis_ArtikelInfo($ID, $rsASI, $ATUNR, $FeldName)
{
	if(substr($FeldName,0,6)=='COUNT:')
	{
		$Erg=0;
	}
	else
	{
		$Erg='';
	}

	for($_i=0;isset($rsASI["ASI_AST_ATUNR"][$_i]) AND $rsASI["ASI_AST_ATUNR"][$_i]!='';$_i++)
	{
//	echo "<br>" . $rsASI["ASI_AST_ATUNR"][$_i] . "/" . $rsASI["ASI_AIT_ID"][$_i] . "=" . $rsASI[$FeldName][$_i] . " ~ $ATUNR/$ID";

		if($rsASI["ASI_AST_ATUNR"][$_i]==$ATUNR AND $rsASI["ASI_AIT_ID"][$_i]==$ID)
		{
			if(substr($FeldName,0,6)=='COUNT:')
			{
				$Erg++;
			}
			elseif(substr($FeldName,0,6)=='LISTE:')
			{
				$Erg .= $rsASI[substr($FeldName,6)][$_i] . '<br>';
			}
			else
			{
				return $rsASI[$FeldName][$_i];
			}
		}
	}
	return($Erg);
}

/**********************************************************************************
*
* awisKennwort($User)
*
* 	Generiert aus einem Text ein Kennwort fuer Oracle
**********************************************************************************/
function awisKennwort($User)
{
	$LetzesZeichen = '';
	$Erg = '';

		// ASCII Werte summieren
	for($i=0;$i<strlen($User);$i++)
	{
		$Sum += ord($User[$i]);
	}
	$Sum = substr($Sum,-1);		// Letzte Stelle

		// Anfangsposition nach 1. Buchstaben
	$User = strtoupper($User);
	$Nr = ord($User[0]);
	if($Nr > 64)
	{
		$Nr-=64;
	}
	else
	{
		$Nr-=48;
	}

		// Name auf 20 Zeichen auffuellen
	$Name = strtoupper(substr($User . substr('oiwe4ghioadsfjce73nbaowfd3sdlfkjrzg538fsadif782rhsiancxskjanxcqgd9',$Nr+$Sum),0,20));
		// Sachas Alphabet
	$Kette = 'LOP5KMJI3N8HU4ZGB9VFTR21ED6C7XSWQA0YLOP5KMJI3N8HU4ZGB9VFTR21ED6C7XSWQA0YLOP5KMJI3N8HU4ZGB9VFTR21ED6C7XSWQA0Y';

		// Daten verschluesseln
	for($i=0;$i<20;$i++)
	{
		$Nr = ord(substr($Name,$i,1));
		if ($Nr > 63)		// Buchstabe
		{
			$Nr-=63;		// Buchstabe -> Position
			$LetzesZeichen = $Nr;
			If($Nr == $LetzesZeichen)		// Auf doppelte Buchstaben testen
			{
				++$Verschieben;
				$Nr+= ++$Verschieben;
			}
			else
			{
				$Verschieben =0;
			}
			$Erg .= $Kette[$Sum+$Nr];
		}
		else
		{
			$Nr = $Nr - 48 + 10;		// Zahlen
			$LetzesZeichen = $Nr;
			If($Nr == $LetzesZeichen)
			{
				$Nr+= ++$Verschieben;
			}
			else
			{
				$Verschieben =0;
			}
			$Erg .= $Kette[$Sum+$Nr];
		}
	}

	return($Erg);
}

/**
 * Wandelt einen Text so um, dass er in der Datenbank gespeichert/gelesen werden kann
 *
 * @param string $Text
 * @return string
 */
function awisDBString($Text)
{
	$Erg = '';

	$Erg = addcslashes($Text,"");

	return $Erg;
}

/**********************************************************************************
*
* DLookup($Felder, $Abfrage, $Bedingung, $_con)
*
* 	Liefert ein Feld aus einer Tabelle
*
**********************************************************************************/
Function DLookup($Felder, $Tabelle, $Bedingung, $_con)
{
	$SQL = "SELECT $Felder FROM $Tabelle";
	if($Bedingung!='')
	{
		$SQL .= " WHERE $Bedingung";
	}
	$rs = awisOpenRecordset($_con, $SQL);

	return($rs[$Felder][0]);
}

/**
 * Verschoeuesselung eines Strings
 *
 * @author Sacha Kerres
 * @version 1.0
 *
 * @param string $Text
 * @return string
 */
function awisCrypt($con,$Text)
{
	global $AWISBenutzer;

    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $key = awis_BenutzerParameter($con,"Mcrypt_Key",$AWISBenutzer->BenutzerName());

    return base64_encode(mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $Text, MCRYPT_MODE_ECB, $iv));
}


/**
 * Verschluesselung eines Strings
 *
 * @author Sacha Kerres
 * @version 1.0
 *
 * @param string $Text
 * @return string
 */
function awisDecrypt($con,$Text)
{
	global $AWISBenutzer;

	if($Text=='')
	{
		return '';
	}
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $key = awis_BenutzerParameter($con,"Mcrypt_Key",$AWISBenutzer->BenutzerName());

    return trim(mcrypt_decrypt(MCRYPT_BLOWFISH, $key, base64_decode($Text), MCRYPT_MODE_ECB, $iv));
}


/**
 * Formatierung von Feldinhalten zur Speicherung in der Datenbank
 * Funktion sollte verwendet werden mit allen INSERT und UPDATE
 * Anweisungen
 *
 * @author Sacha Kerres
 * @version 20070214
 * @name awisDBFeldFormat
 *
 * @param mixed $Wert
 * @param string $Typ (T=Text, TZ=Text(nur Ziffern), D=Datum, Nx=Numerisch (x-Nachkomma))
 * @param boolean $NullErlaubt
 *
 * @example awisDBFeldFormat('1.000,3456','N2', false)
 */
function awisDBFeldFormat($Wert, $Typ = 'T', $NullErlaubt = false)
{
	$Erg = '';
	switch ($Typ[0])
	{
		case 'T':			// Textfelder
			if($Wert==='' AND $NullErlaubt)
			{
				$Erg = 'null';
			}
			else
			{
				$Wert = stripcslashes($Wert);			// Backslash raus
				$Wert = str_replace("'","",$Wert);		// Keine ' erlauben

				if(isset($Typ[0])AND $Typ[0]=='Z')		// Nur Zahlen
				{
					for($i=0;$i<strlen($Wert);$i++)
					{
						if(is_numeric($Wert[$i]))
						{
							$Erg .= $Wert[$i];
						}
					}
					$Erg = "'".$Erg."'";
				}
				else
				{
					$Erg = "'".$Wert."'";
				}
			}
			break;
		case 'D':
			if($Wert==='' AND $NullErlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert==='' AND !$NullErlaubt)
			{
				$Erg = '01.01.1990';
			}
			else
			{
				$Wert = str_replace("-",'.',$Wert);
				$Wert = str_replace("/",'.',$Wert);
				$Wert = str_replace(",",'.',$Wert);

				$DatumsTeile = explode('.',$Wert);
				switch(count($DatumsTeile))
				{
					case 1:			// Nur eine Angabe -> Tag
						$Datum = intval($DatumsTeile[0]).'.'.date('m').'.'.date('Y');
						break;
					case 2:			// Tag.Monat
						$Datum = intval($DatumsTeile[0]).'.'.intval($DatumsTeile[1]).'.'.date('Y');
						break;
					case 3:			// Tag.Monat
						$Datum = intval($DatumsTeile[0]).'.'.intval($DatumsTeile[1]).'.'.intval($DatumsTeile[2]);
						break;
				}
				$Erg = 'TO_DATE(\''.$Datum.'\',\'DD.MM.RRRR\')';
			}
			break;
		case 'N':
			if($Wert==='' AND $NullErlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert==='' AND !$NullErlaubt)
			{
				$Erg = '0';
			}
			else
			{
				if(strstr($Wert,'.')!='' AND strstr($Wert,',')!='')		// Format 1.000,00
				{
					if(strpos($Wert,'.')<strpos($Wert,','))		// Punkt vor Komma (1.000,00)
					{
						$Wert = str_replace('.','',$Wert);
						$Wert = floatval(str_replace(',','.',$Wert));
					}
					else					// Format 1,000.00
					{
						$Wert = floatval(str_replace(',','',$Wert));
					}
				}
				elseif(strstr($Wert,"'")!='' AND strstr($Wert,".")!='')			// Schweiz
				{
					$Wert = floatval(str_replace("'",'',$Wert));
				}
				elseif(strstr($Wert,',')!='')		// Format 1,00
				{
					$Wert = floatval(str_replace(',','.',$Wert));
				}
				elseif(strstr($Wert,'.')!='')		// Format 1.00
				{
					$Wert = floatval($Wert);
				}
				else
				{
					$Wert = floatval($Wert);
				}

				if(isset($Typ[1]))
				{
					$AnzahlNK = intval(substr($Typ,1));
					$Wert = round($Wert,$AnzahlNK);
					$Teil = explode('.',$Wert);
					if(isset($Teil[1]))
					{
						$Wert = $Teil[0].'.'.substr('000000000000000'.$Teil[1],-$AnzahlNK);
					}
					else
					{
						$Wert = $Teil[0].'.'.substr('000000000000000',-$AnzahlNK);
					}
				}

				$Erg = ''.$Wert;
			}
			break;
		default:
			break;
	}

	return $Erg;
}


/**
 * Liefert eine Liste mit Filialen, zu denen ein Benutzer gehoert
 *
 * @author  Sacha Kerres
 * @version 20070510
 * @param resource $con
 * @param string  $BenutzerName
 * @param mixed $Ausgabe (1=Array, 2=Liste)
 * @return mixed
 */
function awisBenutzerFilialen($con, $BenutzerName, $Ausgabe=1)
{
	$BindeVariablen=array();
	$BindeVariablen['var_T_xbl_login'] = strtoupper($BenutzerName);

//	$SQL = 'SELECT XBN_FILIALEN FROM Benutzer ';
//	$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY ';
//	$SQL .= ' WHERE XBL_LOGIN = :var_T_xbl_login';
	
	$SQL = 'SELECT XBN_FILIALEN FROM MV_BENUTZER_LOGIN ';
	$SQL .= ' WHERE XBL_LOGIN = :var_T_xbl_login';

	$rsXBN = awisOpenRecordset($con, $SQL,false,false,$BindeVariablen);
	if($rsXBN['XBN_FILIALEN'][0]=='0')
	{
		$Liste = '';
	}
	else
	{
		$Liste = $rsXBN['XBN_FILIALEN'][0];
	}

	switch($Ausgabe)
	{
		case 1:			// Als ARRAY
			$Erg = explode(',',$Liste);
			break;
		case 2:			// Als Liste
			$Erg = $Liste;
			break;
	}

	return $Erg;
}
?>