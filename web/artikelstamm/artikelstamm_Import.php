<?php

require_once('register.inc.php');
require_once('db.inc.php');
//require_once('register.inc.php');

global $CursorFeld;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWISBenutzer;

ini_set('max_execution_time', 1000);

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('CID', 'CID%');
$TextKonserven[] = array('CIK', '%');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'Dateiname');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Fehler', 'err_keingueltigesDatum');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_aktualisieren');
$TextKonserven[] = array('Wort', 'Datei');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht455 = $AWISBenutzer->HatDasRecht(455);
if ($Recht455 == 0) {
    $Form->Fehler_KeineRechte();
}

$Form->DebugAusgabe(1, $_POST);
if (!isset($_POST['cmdSuche_x']) and !isset($_POST['sucRFF_AST_ATUNR']) and !isset($_POST['cmdDSNeu_x']) and !isset($_POST['cmd_Import_x']) and !isset($_GET['Liste']) and !isset($_POST['Block']) and !isset($_GET['Sort']) and !isset($_GET['Block']) and !isset($_GET['SSort']) and !isset($_GET['CIK_KEY'])) {
    $Form->SchreibeHTMLCode('<form name="frmArtikelstammImport" action="./artikelstamm_Main.php?cmdAktion=Import" method="POST"  enctype="multipart/form-data">');

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_DATEINAME'] . ':', 200);
    $Form->Erstelle_TextFeld('*CIK_DATEINAME', '', 50, 400, true);
    $AWISCursorPosition = 'sucCIK_DATEINAME';
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_STATUS'] . ':', 200);
    $Status = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_STATUS']);
    $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    $Form->Erstelle_SelectFeld('*CIK_STATUS', '', 100, true, '', '', '', '', '', $Status, '');
    $Form->ZeileEnde();

    $Form->Formular_Start();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

    if (($Recht455 & 4) == 4) {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
} elseif (isset($_POST['cmdDSNeu_x'])) {
    $Form->SchreibeHTMLCode('<form name="frmArtikelstammImport" action="./artikelstamm_Main.php?cmdAktion=Import" method="POST"  enctype="multipart/form-data">');

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->SchreibeHTMLCode('<b>Dateiaufbau: Tabstopp-getrennt (TXT): LIEFNR; LIEFARTNR; OENR; HerNr(TecDoc); BEMERKUNG</b>');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->SchreibeHTMLCode('<b>Dateiaufbau: Tabstopp-getrennt (TXT): ATUNR; OENR; OE_HERNR; OE_BEMERKUNG</b>');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->SchreibeHTMLCode('<b>Dateiaufbau: Tabstopp-getrennt (TXT): ATUNR; LIEFNR; DEBITNR; SK (0 = nein / 1 = ja)</b>');
    $Form->ZeileEnde();

    $Form->Trennzeile();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_DATEINAME'] . ':', 150);
    $Form->Erstelle_TextFeld('CIK_DATEINAME', '', 50, 400, true, '', '', '', 'T', 'L');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_IMPORTTYP'] . ':', 150);
    $Importtpen = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_IMPORTTYP']);
    $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    $Form->Erstelle_SelectFeld('CIK_IMPORTTYP', '', 100, true, '', '', '', '', '', $Importtpen, '');
    $Form->ZeileEnde();


    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Datei'] . ':', 150);
    $Form->Erstelle_DateiUpload('Importdatei', 400, 50, 20000000);
    $Form->ZeileEnde();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmd_Import', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $Form->Schaltflaeche('script', 'cmdHilfe',
        "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=import','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png',
        $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form');
} elseif (isset($_FILES['Importdatei'])) {
    if (($fd = fopen($_FILES['Importdatei']['tmp_name'], 'r')) !== false) {
        $Dateiname = $_POST['txtCIK_DATEINAME'];

        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DateiInfo = explode("\t", $Zeile);
        if ($DateiInfo[0] == 'XXLIEFNRZUORDNUNGXX') {
            $Importfunktion = "import_artikel_lartnr";
        } elseif ($DateiInfo[0] == 'XXATUNRZUORDNUNGXX') {
            $Importfunktion = "import_artikel_atunr";
        } elseif ($DateiInfo[0] == 'XXOENRZUORDNUNGXX') {
            $Importfunktion = "import_artikel_oenr";
        } elseif ($DateiInfo[0] == 'XXLIEFARTNRSKXX') {
            $Importfunktion = "import_lartnr_sk";
        } else {
            $Importfunktion = "import_artikel";
        }

        $Erg = $Importfunktion($fd, $Dateiname);

        if ($Erg['Leerzeile'] > 0) {
            $Form->Hinweistext('In der Datei waren ' . $Erg['Leerzeile'] . ' Zeilen ohne OENR vorhanden. Diese wurden nicht mit importiert!!!',awisFormular::HINWEISTEXT_WARNUNG);
        }else{
            $Form->Hinweistext('Datei wurde vollst�ndig importiert.',awisFormular::HINWEISTEXT_OK);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Import&CIK_KEY=' . $DB->FeldInhaltFormat('Z', $Erg['CIK_KEY']);
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmdImport', $Link, '/bilder/cmd_weiter.png');
        $Form->SchaltflaechenEnde();
    }
} else {
    if (isset($_POST['cmdSuche_x']) or isset($_POST['sucRFF_AST_ATUNR'])) {
        $Param['KEY'] = 0;            // Key

        $Param['CIK_DATEINAME'] = $_POST['sucCIK_DATEINAME'];
        $Param['CIK_STATUS'] = $_POST['sucCIK_STATUS'];

        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY'] = '';
        $Param['ORDERBY2'] = '';
        $Param['BLOCK'] = '';
        $Param['KEY'] = 0;

        $AWISBenutzer->ParameterSchreiben("Artikelstammimport_Suche", serialize($Param));
    } elseif (isset($_GET['CIK_KEY'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['CIK_KEY']);
        $Param['KEY'] = $DB->FeldInhaltFormat('N0', $_GET['CIK_KEY']);
        $AWISBenutzer->ParameterSchreiben("Artikelstammimport_Suche", serialize($Param));
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen("Artikelstammimport_Suche"));

    //********************************************************
    // Bedingung erstellen
    //********************************************************	
    $Bedingung = _BedingungErstellen($Param);

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['SSort'])) {
        if (isset($Param['ORDERBY2']) AND $Param['ORDERBY2'] != '') {
            $ORDERBY = $Param['ORDERBY2'];
        } else {
            $ORDERBY = ' CIK_USERDAT DESC';
        }
    } else {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['SSort']);
    }

    //********************************************************
    // Kopfdaten suchen
    //********************************************************								
    $SQL = 'SELECT CIK.*';
    $SQL .= ' FROM CROSSINGIMPORTKOPF CIK';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $SQL .= ' ORDER BY ' . $ORDERBY;

    $rsCIKKopf = $DB->RecordSetOeffnen($SQL);
    $Form->DebugAusgabe(1, $SQL);

    if ($rsCIKKopf->AnzahlDatensaetze() == 0) {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
    } //�bersicht anzeigen
    elseif ($rsCIKKopf->AnzahlDatensaetze() > 1) {
        $Form->ZeileStart();
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=CIK_DATEINAME' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'CIK_DATEINAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CIK']['CIK_DATEINAME'], 500, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=CIK_STATUS' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'CIK_STATUS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CIK']['CIK_STATUS'], 200, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=CIK_USERDAT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'CIK_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CIK']['CIK_USERDAT'], 200, '', $Link);
        $Form->ZeileEnde();

        $CIKZeile = 0;
        $StatusText = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_STATUS']);

        while (!$rsCIKKopf->EOF()) {
            $Form->ZeileStart();
            $Link = './artikelstamm_Main.php?cmdAktion=Import&CIK_KEY=' . $DB->FeldInhaltFormat('Z', $rsCIKKopf->FeldInhalt('CIK_KEY'));
            $Form->Erstelle_ListenFeld('CIK_DATEINAME', $rsCIKKopf->FeldInhalt('CIK_DATEINAME'), 0, 500, false, ($CIKZeile % 2), '', $Link, 'T');

            $Status = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_STATUS']);
            foreach ($StatusText as $Status) {
                $CIKStatus = explode("~", $Status);
                if (intval($CIKStatus[0]) == intval($rsCIKKopf->FeldInhalt('CIK_STATUS'))) {
                    $CIKKOPFStatus = $CIKStatus[1];
                }
            }
            $Form->Erstelle_ListenFeld('CIK_STATUS', $CIKKOPFStatus, 0, 200, false, ($CIKZeile % 2), '', '', 'T', 'L', '');

            $Form->Erstelle_ListenFeld('CIK_USERDAT', $rsCIKKopf->FeldInhalt('CIK_USERDAT'), 0, 200, false, ($CIKZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsCIKKopf->DSWeiter();
            $CIKZeile++;
        }
    } //Liste anzeigen
    else {
        //*****************************************************************
        // Sortierung aufbauen
        //*****************************************************************
        if (!isset($_GET['Sort'])) {
            if (isset($Param['ORDERBY']) AND $Param['ORDERBY'] != '') {
                $ORDERBY = $Param['ORDERBY'];
            } else {
                $ORDERBY = ' CIK_USERDAT DESC';
            }
        } else {
            $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        }

        //********************************************************
        // Daten suchen
        //********************************************************								
        $SQL = 'SELECT CIK.*, CID.* ';
        $SQL .= ', row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
        $SQL .= ' FROM CROSSINGIMPORTKOPF CIK';
        $SQL .= ' LEFT JOIN CROSSINGIMPORTDATEIEN CID ON CID_CIK_KEY = CIK_KEY';

        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }

        $SQL .= ' ORDER BY ' . $ORDERBY;

        //************************************************
        // Aktuellen Datenblock festlegen
        //************************************************
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        } elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '') {
            $Block = intval($Param['BLOCK']);
        }

        //************************************************
        // Zeilen begrenzen
        //************************************************
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

        //*****************************************************************
        // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
        //*****************************************************************
        //if($AWIS_KEY1<=0)
        //{
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
        //}

        $rsCID = $DB->RecordSetOeffnen($SQL);

        $Form->SchreibeHTMLCode('<form name="frmArtikelstammImport" action="./artikelstamm_Main.php?cmdAktion=Import" method="POST"  enctype="multipart/form-data">');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_DATEINAME'] . ':', 100);
        $Form->Erstelle_TextFeld('CIK_DATEINAME', $rsCID->FeldInhalt('CIK_DATEINAME'), 50, 400, false, '', '', '', 'T', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_IMPORTTYP'] . ':', 100);
        $Importtpen = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_IMPORTTYP']);
        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $Form->Erstelle_SelectFeld('CIK_IMPORTTYP', $rsCID->FeldInhalt('CIK_IMPORTTYP'), 100, false, '', '', '', '', '', $Importtpen, '');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['CIK']['CIK_STATUS'] . ':', 100);
        $Status = explode("|", $AWISSprachKonserven['CIK']['lst_CIK_STATUS']);
        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $Form->Erstelle_SelectFeld('CIK_STATUS', $rsCID->FeldInhalt('CIK_STATUS'), 300, false, '', $OptionBitteWaehlen, '', '', '', $Status, '');
        $Form->ZeileEnde();

        //Falls fertig importiert, Protokoll anzeigen
        if ($rsCID->FeldInhalt('CIK_STATUS') == 3) {
            //Protokoll
            $SQL = 'SELECT CIP_ZEILE, CIP_ZEILENINHALT, CIP_MELDUNG, CIP_AST_ATUNR_OEN, CIP_AST_ATUNR, CIP_LAR_LARTNR, CIP_AKTION FROM CROSSINGIMPORTPOS WHERE CIP_CIK_KEY =' . $rsCID->FeldInhalt('CIK_KEY') . ' ORDER BY CIP_ZEILE';
            $rsDaten = $DB->RecordSetOeffnen($SQL);
            $rsDatenZeilen = $rsDaten->AnzahlDatensaetze();

            $DateiName = awis_UserExportDateiName('.csv');
            $fp = fopen($DateiName, 'w+');

            $Zeile = "ZEILE\tINHALT\tMELDUNG\tATUNR\tATUNR\tLARTNR\tAKTION\n";

            fputs($fp, $Zeile);

            while (!$rsDaten->EOF()) {
                $Zeile = $rsDaten->FeldInhalt('CIP_ZEILE') . "\t" . $rsDaten->FeldInhalt('CIP_ZEILENINHALT') . "\t" . $rsDaten->FeldInhalt('CIP_MELDUNG') . "\t" . $rsDaten->FeldInhalt('CIP_AST_ATUNR_OEN') . "\t" . $rsDaten->FeldInhalt('CIP_AST_ATUNR') . "\t" . $rsDaten->FeldInhalt('CIP_LAR_LARTNR') . "\t" . $rsDaten->FeldInhalt('CIP_AKTION') . "\n";
                fputs($fp, $Zeile);
                $rsDaten->DSWeiter();
            }

            $DateiName = pathinfo($DateiName);

            $Form->ZeileStart();
            $Form->SchreibeHTMLCode('<a href=/export/' . $DateiName["basename"] . '>Protokoll �ffnen</a>');
            $Form->ZeileEnde();

            fclose($fp);
        }

        $Form->Trennzeile();

        $Form->ZeileStart();
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_ZEILE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_ZEILE'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_ZEILE'], 80, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_LIE_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_LIE_NR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_LIE_NR'], 80, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_LAR_LARTNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_LAR_LARTNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_LAR_LARTNR'], 130, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_OEN_OENR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_OEN_OENR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_OEN_OENR'], 130, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_OEN_HER_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_OEN_HER_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_OEN_HER_ID'], 130, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Import' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=CID_BEMERKUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CID_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CID']['CID_BEMERKUNG'], 500, '', $Link);
        $Form->ZeileEnde();

        $CIDZeile = 0;
        while (!$rsCID->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('CID_ZEILE', $rsCID->FeldInhalt('CID_ZEILE'), 0, 80, false, ($CIDZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('CID_LIE_NR', $rsCID->FeldInhalt('CID_LIE_NR'), 0, 80, false, ($CIDZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('CID_LAR_LARTNR', $rsCID->FeldInhalt('CID_LAR_LARTNR'), 0, 130, false, ($CIDZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('CID_OEN_OENR', $rsCID->FeldInhalt('CID_OEN_OENR'), 0, 130, false, ($CIDZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('CID_OEN_HER_ID', $rsCID->FeldInhalt('CID_OEN_HER_ID'), 0, 130, false, ($CIDZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('CID_BEMERKUNG', $rsCID->FeldInhalt('CID_BEMERKUNG'), 0, 500, false, ($CIDZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsCID->DSWeiter();
            $CIDZeile++;
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Import&Liste=True' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $DB;

    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0) {
        $Bedingung .= 'AND CIK_KEY = ' . $AWIS_KEY1;

        return $Bedingung;
    }

    if (isset($Param['KEY']) AND $Param['KEY'] != '') {
        $Bedingung .= 'AND CIK_KEY =' . $Param['KEY'];
    }

    if (isset($Param['CIK_STATUS']) AND $Param['CIK_STATUS'] != '-1') {
        $Bedingung .= 'AND CIK_STATUS =' . $Param['CIK_STATUS'];
    }

    if (isset($Param['CIK_DATEINAME']) AND $Param['CIK_DATEINAME'] != '') {
        $Bedingung .= 'AND CIK_DATEINAME ' . $DB->LikeOderIst($Param['CIK_DATEINAME'], 0) . ' ';
    }

    return $Bedingung;
}

/**
 * Artikelstamm Import
 *
 * @param ressource $fd
 * @return array
 */
function import_artikel($fd, $Dateiname)
{
    global $AWISBenutzer;

    $Recht455 = $AWISBenutzer->HatDasRecht(455);

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Erg = array(
        'Code' => 0,
        'Letzte Meldung' => '',
        'Zeile' => 0,
        'Meldungen' => array(),
        'Fehlermeldung' => array(),
        'Anforderung' => '',
        'Meldungen_NEU' => array(),
        'CIK_KEY' => 0,
        'Leerzeile' => 0
    );

    //neuer Eintrag in Tabelle Crossingimportkopf
    $SQL = 'INSERT INTO Crossingimportkopf';
    $SQL .= '(CIK_DATEINAME,CIK_IMPORTTYP, CIK_USER, CIK_USERDAT';
    $SQL .= ')VALUES (';
    $SQL .= $DB->WertSetzen('CIK','T',$Dateiname);
    $SQL .= ' , ' . $DB->WertSetzen('CIK','T',$_POST['txtCIK_IMPORTTYP']);
    $SQL .= ' , ' . $DB->WertSetzen('CIK','T',$AWISBenutzer->BenutzerName());
    $SQL .= ',SYSDATE';
    $SQL .= ')';

    $Form->DebugAusgabe(1, $SQL);

    if ($DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('CIK')) === false) {
        throw new awisException('Fehler beim Speichern', 902261343, $SQL, 2);
    }

    $SQL = 'SELECT seq_CIK_KEY.CurrVal AS KEY FROM DUAL';
    $rsKey = $DB->RecordSetOeffnen($SQL);
    $CIK_KEY = $rsKey->FeldInhalt('KEY');
    $Erg['CIK_KEY'] = $CIK_KEY;

    rewind($fd);
    $Zeile = fgets($fd);
    $Zeile = trim($Zeile);

    $DS = 1;

    $Ueberschriften = array("LIEFNR" => 0, "LIEFARTNR" => 1, "OENR" => 2,"OEN_HER_ID" => 3, "BEMERKUNG" => 4);

    while (!feof($fd)) {
        $Daten = explode("\t", $Zeile);

        if (isset($Daten[$Ueberschriften['OENR']]) AND trim($Daten[$Ueberschriften['OENR']]) != '') {
            //Datei in Importtabelle schreiben
            $SQL = 'INSERT INTO CROSSINGIMPORTDATEIEN (CID_CIK_KEY, CID_ZEILE, CID_LIE_NR, CID_LAR_LARTNR, CID_OEN_OENR,CID_OEN_HER_ID, CID_BEMERKUNG, CID_USER, CID_USERDAT)';
            $SQL .= ' VALUES (';
            $SQL .= '' . $CIK_KEY;
            $SQL .= ',' . $DS;
            $SQL .= ',\'' . trim($Daten[$Ueberschriften['LIEFNR']]) . '\'';
            $SQL .= ',\'' . trim($Daten[$Ueberschriften['LIEFARTNR']]) . '\'';
            $SQL .= ',\'' . trim($Daten[$Ueberschriften['OENR']]) . '\'';
            $SQL .= ',\'' . trim($Daten[$Ueberschriften['OEN_HER_ID']]) . '\'';
            $SQL .= ',\'' . trim($Daten[$Ueberschriften['BEMERKUNG']]) . '\'';
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            $Form->DebugAusgabe(1, $SQL);

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 902261343, $SQL, 2);
            }
        } else {
            $Erg['Leerzeile'] += 1;
        }

        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DS++;
    }

    $SQL = 'UPDATE CROSSINGIMPORTKOPF SET CIK_STATUS = 1 WHERE CIK_KEY = ' . $CIK_KEY;
    if ($DB->Ausfuehren($SQL) === false) {
        throw new awisException('Fehler beim Speichern', 904240912, $SQL, 2);
    }

    return $Erg;
}

/**
 * Artikelstamm Import
 *
 * @param ressource $fd
 * @return array
 */
function import_artikel_lartnr($fd)
{
    global $AWISBenutzer;

    $Recht455 = $AWISBenutzer->HatDasRecht(455);

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Erg = array(
        'Code' => 0,
        'Letzte Meldung' => '',
        'Zeile' => 0,
        'Meldungen' => array(),
        'Fehlermeldung' => array(),
        'Anforderung' => '',
        'Meldungen_NEU' => array(),
        'CIK_KEY' => 0
    );

    //neuer Eintrag in Tabelle Crossingimportkopf
    $SQL = 'INSERT INTO Crossingimportkopf';
    $SQL .= '(CIK_USER, CIK_USERDAT';
    $SQL .= ')VALUES (';
    $SQL .= '\'' . $AWISBenutzer->BenutzerName() . '\'';
    $SQL .= ',SYSDATE';
    $SQL .= ')';


    if ($DB->Ausfuehren($SQL) === false) {
        throw new awisException('Fehler beim Speichern', 902261343, $SQL, 2);
    }

    $SQL = 'SELECT seq_CIK_KEY.CurrVal AS KEY FROM DUAL';
    $rsKey = $DB->RecordSetOeffnen($SQL);
    $CIK_KEY = $rsKey->FeldInhalt('KEY');
    $Erg['CIK_KEY'] = $CIK_KEY;

    $Zeile = fgets($fd);
    $Zeile = trim($Zeile);

    $DS = 1;

    $Ueberschriften = array("LIEFNR" => 0, "LIEFARTNR" => 1, "OENR" => 2, "BEMERKUNG" => 3, "LIEFNR_REFERENZ" => 4, "LIEFARTNR_REFERENZ" => 5);

    while (!feof($fd)) {
        $MELDUNGLIEFARTNR = '';
        $MELDUNGZUORDNUNG = '';
        $MELDUNGIMPORT = '';

        $Daten = explode(";", $Zeile);
        if ($Daten[$Ueberschriften['LIEFARTNR_REFERENZ']] != '') {
            //Pr�fen, bei welchen Artikeln die LIEFARTNR_REFERENZ hinterlegt ist
            $SQL = ' SELECT * ';
            $SQL .= ' FROM Artikelstamm';
            $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $AWISBenutzer->BenutzerSprache() . '\'';
            $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
            $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
            $SQL .= ' INNER JOIN TeileInfos ON (AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
            $SQL .= " AND (TEI_SUCH2) " . $DB->LIKEoderIST($Daten[$Ueberschriften['LIEFARTNR_REFERENZ']],
                    awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . ")";
            $SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
            $SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " . $DB->FeldInhaltFormat('Z', $Daten[$Ueberschriften['LIEFNR_REFERENZ']]) . ")";

            $rsLARREFERENZ = $DB->RecordSetOeffnen($SQL);

            if ($rsLARREFERENZ->AnzahlDatensaetze() >= 1) {
                //LIEFARTNR auch den obigen Artikeln mit zuordnen
                while (!$rsLARREFERENZ->EOF()) {
                    //Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
                    $SQL = ' SELECT * ';
                    $SQL .= ' FROM LIEFERANTENARTIKEL ';
                    $SQL .= " WHERE (LAR_SUCH_LARTNR) " . $DB->LIKEoderIST($Daten[$Ueberschriften['LIEFARTNR']],
                            awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER);
                    $SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " . $DB->FeldInhaltFormat('Z', $Daten[$Ueberschriften['LIEFNR']]);

                    $rsLARTNR = $DB->RecordSetOeffnen($SQL);

                    //Wenn LIEFARTNR + LIEFNR nicht in AWIS
                    if ($rsLARTNR->AnzahlDatensaetze() == 0) {
                        //neue LiefArtNr anlegen
                        $SQL = 'INSERT INTO Lieferantenartikel';
                        $SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
                        $SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
                        $SQL .= ',LAR_USER, LAR_USERDAT';
                        $SQL .= ')VALUES (';
                        $SQL .= ' ' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['LIEFARTNR']], false);
                        $SQL .= ', lpad(' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['LIEFNR']], false) . ',4,0)';
                        $SQL .= ',' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['BEMERKUNG']], true);
                        $SQL .= ',0';
                        $SQL .= ',4';        // Importquelle
                        $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $Daten[$Ueberschriften['LIEFARTNR']], false) . ')';

                        if (($Recht455 & 2) == 2) {
                            $SQL .= ',\'Import\'';
                        } else {
                            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                        }

                        $SQL .= ',SYSDATE';
                        $SQL .= ')';


                        if ($DB->Ausfuehren($SQL) === false) {
                            throw new awisException('Fehler beim Speichern', 906161508, $SQL, 2);
                        }

                        $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
                        $rsKey = $DB->RecordSetOeffnen($SQL);
                        $AWIS_KEY2 = $rsKey->FeldInhalt('KEY');

                        $MELDUNGLIEFARTNR = $Daten[$Ueberschriften['LIEFARTNR']] . ' angelegt';
                    } else {
                        $AWIS_KEY2 = $rsLARTNR->FeldInhalt('LAR_KEY');

                        // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Lieferantenrtikel
                        // nicht durch den Import gel�scht werden kann
                        // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                        $SQL = 'UPDATE Lieferantenartikel SET';

                        if (($Recht455 & 2) == 2) {
                            $SQL .= ' LAR_user=\'Import\'';
                        } else {
                            $SQL .= ' LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                        }

                        $SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
                        $SQL .= ', LAR_userdat=sysdate';
                        $SQL .= ' WHERE LAR_key=0' . $AWIS_KEY2 . ' AND BITAND(LAR_IMQ_ID,4)<>4';

                        if ($DB->Ausfuehren($SQL) === false) {
                            throw new awisException('Fehler beim Speichern', 901161228, $SQL, 2);
                        }

                        $MELDUNGLIEFARTNR = $Daten[$Ueberschriften['LIEFARTNR']] . ' vorhanden';
                    }

                    //Pr�fen, ob ATUNR und LIEFARTNR evtl. schon zugeordnet sind
                    $SQL = ' SELECT * ';
                    $SQL .= ' FROM TeileInfos ';
                    $SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_KEY1 = ' . $rsLARREFERENZ->FeldInhalt('AST_KEY');
                    $SQL .= ' AND TEI_ITY_ID2 = \'LAR\' AND TEI_KEY2 = ' . $AWIS_KEY2;

                    $rsTei = $DB->RecordSetOeffnen($SQL);

                    if ($rsTei->AnzahlDatensaetze() == 0) {
                        //LiefArtNr der ATUNR zuordnen
                        $SQL = 'INSERT INTO TeileInfos';
                        $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                        $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                        $SQL .= 'VALUES(';
                        $SQL .= ' \'AST\'';
                        $SQL .= ',' . $rsLARREFERENZ->FeldInhalt('AST_KEY');
                        $SQL .= ',suchwort(\'' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . '\')';
                        $SQL .= ',\'' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . '\'';
                        $SQL .= ',\'LAR\'';
                        $SQL .= ',' . $AWIS_KEY2;
                        $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $Daten[$Ueberschriften['LIEFARTNR']], false) . ')';
                        $SQL .= ',\'' . $Daten[$Ueberschriften['LIEFARTNR']] . '\'';

                        if (($Recht455 & 2) == 2) {
                            $SQL .= ',\'Import\'';
                        } else {
                            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                        }

                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                        if ($DB->Ausfuehren($SQL) === false) {
                            throw new awisException('Fehler beim Speichern', 901161226, $SQL, 2);
                        }

                        // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
                        // nicht durch den Import gel�scht werden kann
                        // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                        $SQL = 'UPDATE Artikelstamm SET';

                        if (($Recht455 & 2) == 2) {
                            $SQL .= ' AST_user=\'Import\'';
                        } else {
                            $SQL .= ' AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                        }
                        $SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
                        $SQL .= ', AST_userdat=sysdate';
                        $SQL .= ' WHERE AST_key=0' . $rsLARREFERENZ->FeldInhalt('AST_KEY') . ' AND BITAND(AST_IMQ_ID,4)<>4';

                        if ($DB->Ausfuehren($SQL) === false) {
                            throw new awisException('Fehler beim Speichern', 901161227, $SQL, 2);
                        }

                        $MELDUNGZUORDNUNG = $Daten[$Ueberschriften['LIEFARTNR']] . ' wurde den Artikel ' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . ' zugeordnet.';
                    } else {
                        $MELDUNGZUORDNUNG = 'Zuordnung zu Artikel ' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . ' vorhanden.';
                    }

                    $Erg['Meldungen'][] = 'Zeile ' . $DS . ': LIEFARTNR_REFERENZ ' . $Daten[$Ueberschriften['LIEFARTNR_REFERENZ']] . ' vorhanden + Zuordnung zu Artikel ' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . ' vorhanden. LIEFARTNR ' . $MELDUNGLIEFARTNR . ' + ' . $MELDUNGZUORDNUNG;

                    $MELDUNGIMPORT = 'Zeile ' . $DS . ': LIEFARTNR_REFERENZ ' . $Daten[$Ueberschriften['LIEFARTNR_REFERENZ']] . ' vorhanden + Zuordnung zu Artikel ' . $rsLARREFERENZ->FeldInhalt('AST_ATUNR') . ' vorhanden. LIEFARTNR ' . $MELDUNGLIEFARTNR . ' + ' . $MELDUNGZUORDNUNG;
                    $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= '' . $CIK_KEY;
                    $SQL .= ',' . $DS;
                    $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';
                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 906161522, $SQL, 2);
                    }

                    $rsLARREFERENZ->DSWeiter();
                }
            } else {
                $Erg['Meldungen'][] = 'Zeile ' . $DS . ': LIEFARTNR_REFERENZ bei keinem Artikel zugeordnet.';

                $MELDUNGIMPORT = 'Zeile ' . $DS . ': LIEFARTNR_REFERENZ bei keinem Artikel zugeordnet.';
                $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
                $SQL .= 'VALUES(';
                $SQL .= '' . $CIK_KEY;
                $SQL .= ',' . $DS;
                $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 906171334, $SQL, 2);
                }
            }
        } else {
            $Erg['Meldungen'][] = 'Zeile ' . $DS . ': Keine LIEFARTNR_REFERENZ im Datensatz vorhanden.';

            $MELDUNGIMPORT = 'Zeile ' . $DS . ': Keine LIEFARTNR_REFERENZ im Datensatz vorhanden.';
            $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
            $SQL .= 'VALUES(';
            $SQL .= '' . $CIK_KEY;
            $SQL .= ',' . $DS;
            $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 906161522, $SQL, 2);
            }
        }

        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DS++;
    }

    return $Erg;
}

/**
 * Artikelstamm Import
 *
 * @param ressource $fd
 * @return array
 */
function import_artikel_atunr($fd)
{
    global $AWISBenutzer;

    $Recht455 = $AWISBenutzer->HatDasRecht(455);

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Erg = array(
        'Code' => 0,
        'Letzte Meldung' => '',
        'Zeile' => 0,
        'Meldungen' => array(),
        'Fehlermeldung' => array(),
        'Anforderung' => '',
        'Meldungen_NEU' => array(),
        'CIK_KEY' => 0
    );


    //neuer Eintrag in Tabelle Crossingimportkopf
    $SQL = 'INSERT INTO Crossingimportkopf';
    $SQL .= '(CIK_USER, CIK_USERDAT';
    $SQL .= ')VALUES (';
    $SQL .= '\'' . $AWISBenutzer->BenutzerName() . '\'';
    $SQL .= ',SYSDATE';
    $SQL .= ')';

    if ($DB->Ausfuehren($SQL) === false) {
        throw new awisException('Fehler beim Speichern', 902261343, $SQL, 2);
    }

    $SQL = 'SELECT seq_CIK_KEY.CurrVal AS KEY FROM DUAL';
    $rsKey = $DB->RecordSetOeffnen($SQL);
    $CIK_KEY = $rsKey->FeldInhalt('KEY');
    $Erg['CIK_KEY'] = $CIK_KEY;

    $Zeile = fgets($fd);
    $Zeile = trim($Zeile);

    $DS = 1;

    $Ueberschriften = array("ATUNR" => 0, "LIEFNR" => 1, "LIEFARTNR" => 2, "OENR" => 3, "BEMERKUNG" => 4);

    while (!feof($fd)) {
        $MELDUNGLIEFARTNR = '';
        $MELDUNGZUORDNUNG = '';
        $MELDUNGIMPORT = '';

        $Daten = explode(";", $Zeile);
        if ($Daten[$Ueberschriften['ATUNR']] != '') {

            //Pr�fen, ob die ATUNR vorhanden ist 
            $SQL = ' SELECT * ';
            $SQL .= ' FROM Artikelstamm';
            $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $AWISBenutzer->BenutzerSprache() . '\'';
            $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
            $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
            $SQL .= " WHERE AST_ATUNR " . $DB->LikeOderIst($Daten[$Ueberschriften['ATUNR']], awisDatenbank::AWIS_LIKE_UPPER);

            $Form->DebugAusgabe(1, $SQL);
            $rsATUNR = $DB->RecordSetOeffnen($SQL);

            //Wenn Artikel vorhanden ist, pr�fen, die Zuordnung evtl. schon vorhanden ist
            if ($rsATUNR->AnzahlDatensaetze() >= 1) {
                //LIEFARTNR auch den obigen Artikeln mit zuordnen				
                //Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
                $SQL = ' SELECT * ';
                $SQL .= ' FROM LIEFERANTENARTIKEL ';
                $SQL .= " WHERE (LAR_SUCH_LARTNR) " . $DB->LIKEoderIST($Daten[$Ueberschriften['LIEFARTNR']], awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER);
                $SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " . $DB->FeldInhaltFormat('Z', $Daten[$Ueberschriften['LIEFNR']]);

                $Form->DebugAusgabe(1, $SQL);

                $rsLARTNR = $DB->RecordSetOeffnen($SQL);

                //Wenn LIEFARTNR + LIEFNR nicht in AWIS
                if ($rsLARTNR->AnzahlDatensaetze() == 0) {
                    //neue LiefArtNr anlegen
                    $SQL = 'INSERT INTO Lieferantenartikel';
                    $SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
                    $SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
                    $SQL .= ',LAR_USER, LAR_USERDAT';
                    $SQL .= ')VALUES (';
                    $SQL .= ' ' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['LIEFARTNR']], false);
                    $SQL .= ', lpad(' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['LIEFNR']], false) . ',4,0)';
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['BEMERKUNG']], true);
                    $SQL .= ',0';
                    $SQL .= ',4';        // Importquelle
                    $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $Daten[$Ueberschriften['LIEFARTNR']], false) . ')';

                    if (($Recht455 & 2) == 2) {
                        $SQL .= ',\'Import\'';
                    } else {
                        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    }

                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 906161508, $SQL, 2);
                    }

                    $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
                    $rsKey = $DB->RecordSetOeffnen($SQL);
                    $AWIS_KEY2 = $rsKey->FeldInhalt('KEY');

                    $MELDUNGLIEFARTNR = $Daten[$Ueberschriften['LIEFARTNR']] . ' angelegt';
                } else {
                    $AWIS_KEY2 = $rsLARTNR->FeldInhalt('LAR_KEY');

                    // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Lieferantenrtikel
                    // nicht durch den Import gel�scht werden kann
                    // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                    $SQL = 'UPDATE Lieferantenartikel SET';
                    $SQL .= ' LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || chr(13)||chr(10)||' . $DB->FeldInhaltFormat('T', $Daten[$Ueberschriften['BEMERKUNG']], true);

                    if (($Recht455 & 2) == 2) {
                        $SQL .= ', LAR_user=\'Import\'';
                    } else {
                        $SQL .= ', LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    }

                    $SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
                    $SQL .= ', LAR_userdat=sysdate';
                    $SQL .= ' WHERE LAR_key=0' . $AWIS_KEY2;

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901161228, $SQL, 2);
                    }

                    $MELDUNGLIEFARTNR = $Daten[$Ueberschriften['LIEFARTNR']] . ' vorhanden';
                }

                //Pr�fen, ob ATUNR und LIEFARTNR evtl. schon zugeordnet sind
                $SQL = ' SELECT * ';
                $SQL .= ' FROM TeileInfos ';
                $SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_KEY1 = ' . $rsATUNR->FeldInhalt('AST_KEY');
                $SQL .= ' AND TEI_ITY_ID2 = \'LAR\' AND TEI_KEY2 = ' . $AWIS_KEY2;
                $SQL .= ' AND TEI_USER <> \'WWS\'';
                
                $rsTei = $DB->RecordSetOeffnen($SQL);

                if ($rsTei->AnzahlDatensaetze() == 0) {
                    //LiefArtNr der ATUNR zuordnen
                    $SQL = 'INSERT INTO TeileInfos';
                    $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                    $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= ' \'AST\'';
                    $SQL .= ',' . $rsATUNR->FeldInhalt('AST_KEY');
                    $SQL .= ',suchwort(\'' . $rsATUNR->FeldInhalt('AST_ATUNR') . '\')';
                    $SQL .= ',\'' . $rsATUNR->FeldInhalt('AST_ATUNR') . '\'';
                    $SQL .= ',\'LAR\'';
                    $SQL .= ',' . $AWIS_KEY2;
                    $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $Daten[$Ueberschriften['LIEFARTNR']], false) . ')';
                    $SQL .= ',\'' . $Daten[$Ueberschriften['LIEFARTNR']] . '\'';

                    if (($Recht455 & 2) == 2) {
                        $SQL .= ',\'Import\'';
                    } else {
                        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    }

                    $SQL .= ',SYSDATE';
                    $SQL .= ')';


                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901161226, $SQL, 2);
                    }

                    // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
                    // nicht durch den Import gel�scht werden kann
                    // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                    $SQL = 'UPDATE Artikelstamm SET';

                    if (($Recht455 & 2) == 2) {
                        $SQL .= ' AST_user=\'Import\'';
                    } else {
                        $SQL .= ' AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    }
                    $SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
                    $SQL .= ', AST_userdat=sysdate';
                    $SQL .= ' WHERE AST_key=0' . $rsATUNR->FeldInhalt('AST_KEY') . ' AND BITAND(AST_IMQ_ID,4)<>4';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901161227, $SQL, 2);
                    }

                    $MELDUNGZUORDNUNG = $Daten[$Ueberschriften['LIEFARTNR']] . ' wurde den Artikel ' . $rsATUNR->FeldInhalt('AST_ATUNR') . ' zugeordnet.';
                } else {
                    $MELDUNGZUORDNUNG = 'Zuordnung zu Artikel ' . $rsATUNR->FeldInhalt('AST_ATUNR') . ' vorhanden.';
                }

                $Erg['Meldungen'][] = 'Zeile ' . $DS . ': ATUNR ' . $Daten[$Ueberschriften['ATUNR']] . ' vorhanden. LIEFARTNR ' . $MELDUNGLIEFARTNR . ' + ' . $MELDUNGZUORDNUNG;

                $MELDUNGIMPORT = 'Zeile ' . $DS . ': ATUNR ' . $Daten[$Ueberschriften['ATUNR']] . ' vorhanden. LIEFARTNR ' . $MELDUNGLIEFARTNR . ' + ' . $MELDUNGZUORDNUNG;
                $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
                $SQL .= 'VALUES(';
                $SQL .= '' . $CIK_KEY;
                $SQL .= ',' . $DS;
                $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 906161522, $SQL, 2);
                }

                $rsATUNR->DSWeiter();
            } else {
                $Erg['Meldungen'][] = 'Zeile ' . $DS . ': ATUNR ist nicht vorhanden.';

                $MELDUNGIMPORT = 'Zeile ' . $DS . ': ATUNR ist nicht vorhanden.';
                $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
                $SQL .= 'VALUES(';
                $SQL .= '' . $CIK_KEY;
                $SQL .= ',' . $DS;
                $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 906171334, $SQL, 2);
                }
            }
        } else {
            $Erg['Meldungen'][] = 'Zeile ' . $DS . ': Keine ATUNR im Datensatz vorhanden.';

            $MELDUNGIMPORT = 'Zeile ' . $DS . ': Keine ATUNR im Datensatz vorhanden.';
            $SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_MELDUNG, CIP_USER, CIP_USERDAT)';
            $SQL .= 'VALUES(';
            $SQL .= '' . $CIK_KEY;
            $SQL .= ',' . $DS;
            $SQL .= ',\'' . $MELDUNGIMPORT . '\'';
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 906161522, $SQL, 2);
            }
        }

        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DS++;
    }

    return $Erg;
}

/**
 * OE-Nummern Import
 *
 * @param ressource $fd
 * @return array
 */
function import_artikel_oenr ($fd, $Dateiname) {

    global $AWISBenutzer;

    $Recht455 = $AWISBenutzer->HatDasRecht(455);

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Erg = array(
        'Code' => 0,
        'Letzte Meldung' => '',
        'Zeile' => 0,
        'Meldungen' => array(),
        'Fehlermeldung' => array(),
        'Anforderung' => '',
        'Meldungen_NEU' => array(),
        'CIK_KEY' => 0,
        'Leerzeile' => 0
    );

    //neuer Eintrag in Tabelle Crossingimportkopf
    $SQL = '  INSERT INTO crossingimportkopf';
    $SQL .= ' (';
    $SQL .= '   CIK_USER';
    $SQL .= '  ,CIK_USERDAT';
    $SQL .= '  ,CIK_DATEINAME';
    $SQL .= ' )';
    $SQL .= ' VALUES';
    $SQL .= ' (';
    $SQL .= '    '.$DB->WertSetzen('CIK','T',$AWISBenutzer->BenutzerName());
    $SQL .= '   ,SYSDATE';
    $SQL .= '   ,'.$DB->WertSetzen('CIK','T',$Dateiname);
    $SQL .= ' )';

    $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('CIK'));

    $SQL = 'SELECT seq_CIK_KEY.CurrVal AS KEY FROM DUAL';
    $rsKey = $DB->RecordSetOeffnen($SQL);
    $CIK_KEY = $rsKey->FeldInhalt('KEY');
    $Erg['CIK_KEY'] = $CIK_KEY;

    $Zeile = fgets($fd);
    $Zeile = trim($Zeile);

    $DS = 1;

    $Ueberschriften = array("ATUNR" => 0, "OENR" => 1, "OE_HERNR" => 2, "OE_BERMERKUNG" => 3);

    while (!feof($fd)) {
        $MELDUNGOENR = '';
        $MELDUNGZUORDNUNG = '';
        $MELDUNGIMPORT = '';

        $Daten = explode("\t", $Zeile);

        if ($Daten[$Ueberschriften['ATUNR']] != '') {

            //Pr�fen, ob die ATUNR vorhanden ist
            $SQL = ' SELECT * ';
            $SQL .= ' FROM Artikelstamm';
            $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = ' . $DB->WertSetzen('AST','T',$AWISBenutzer->BenutzerSprache());
            $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
            $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
            $SQL .= " WHERE AST_ATUNR " . $DB->LikeOderIst($Daten[$Ueberschriften['ATUNR']], awisDatenbank::AWIS_LIKE_UPPER,'AST');

            //$Form->DebugAusgabe(1, $SQL);
            $Form->DebugAusgabe(1, $DB->LetzterSQL());
            $rsATUNR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('AST'));

            if($rsATUNR->AnzahlDatensaetze() == 1) {

                // Pr�fen, ob die OENR vorhanden ist
                $SQL = '  SELECT *';
                $SQL .= ' FROM oenummern';
                $SQL .= ' WHERE oen_suchnummer = asciiwortoe(' . $DB->WertSetzen('OEN','T',$Daten[$Ueberschriften['OENR']]).')';
                $SQL .= ' AND oen_her_id =                   ' . $DB->WertSetzen('OEN','Z',$Daten[$Ueberschriften['OE_HERNR']]);

                $Form->DebugAusgabe(1, $SQL);
                $rsOENR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('OEN'));

                if($rsOENR->AnzahlDatensaetze() == 0) {
                    // OE-Nummer anlegen

                    $SQL = 'SELECT seq_global_key.NextVal AS OEN_KEY FROM DUAL';
                    $rsOEKey = $DB->RecordSetOeffnen($SQL);
                    $OEKey = $rsOEKey->FeldInhalt('OEN_KEY');

                    $SQL = '  INSERT INTO oenummern';
                    $SQL .= ' (';
                    $SQL .= '    OEN_KEY';
                    $SQL .= '   ,OEN_NUMMER';
                    $SQL .= '   ,OEN_HER_ID';
                    $SQL .= '   ,OEN_SUCHNUMMER';
                    $SQL .= '   ,OEN_BEMERKUNG';
                    $SQL .= '   ,OEN_MUSTER';
                    $SQL .= '   ,OEN_IMQ_ID';
                    $SQL .= '   ,OEN_USER';
                    $SQL .= '   ,OEN_USERDAT';
                    $SQL .= ' )';
                    $SQL .= ' VALUES';
                    $SQL .= ' (';
                    $SQL .= '    ' . $DB->WertSetzen('OEN','Z',$OEKey);
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','T',$Daten[$Ueberschriften['OENR']]);
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','T',$Daten[$Ueberschriften['OE_HERNR']]);
                    $SQL .= '   ,asciiwortoe(' . $DB->WertSetzen('OEN','T',$Daten[$Ueberschriften['OENR']]).')';
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','T',$Daten[$Ueberschriften['OE_BEMERKUNG']]);
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','Z',0);
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','Z',4);
                    $SQL .= '   ,' . $DB->WertSetzen('OEN','T',$AWISBenutzer->BenutzerName());
                    $SQL .= '   ,SYSDATE';
                    $SQL .= ' )';

                    $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('OEN'));

                    $MELDUNGOENR = $Daten[$Ueberschriften['OENR']] . ' angelegt';

                } else {
                    $OEKey = $rsOENR->FeldInhalt('OEN_KEY');
                    $MELDUNGOENR = $Daten[$Ueberschriften['OENR']] . ' vorhanden';
                }

                // Pr�fen, ob eine Zuordnung in TeileInfos vorhanden ist
                $SQL = '  SELECT *';
                $SQL .= ' FROM teileinfos';
                $SQL .= ' WHERE tei_ity_id1 = ' . $DB->WertSetzen('TEI','T','AST');
                $SQL .= ' AND   tei_key1 = '    . $DB->WertSetzen('TEI','Z',$rsATUNR->FeldInhalt('AST_KEY'));
                $SQL .= ' AND   tei_ity_id2 ='  . $DB->WertSetzen('TEI','T','OEN');
                $SQL .= ' AND   tei_key2 = '    . $DB->WertSetzen('TEI','Z',$OEKey);

                $Form->DebugAusgabe(1, $SQL);
                $rsTeileInfos = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TEI'));

                if($rsTeileInfos->AnzahlDatensaetze() == 0) {

                    $SQL = 'INSERT INTO teileinfos';
                    $SQL .= ' (';
                    $SQL .= '    TEI_ITY_ID1';
                    $SQL .= '   ,TEI_KEY1';
                    $SQL .= '   ,TEI_SUCH1';
                    $SQL .= '   ,TEI_WERT1';
                    $SQL .= '   ,TEI_ITY_ID2';
                    $SQL .= '   ,TEI_KEY2';
                    $SQL .= '   ,TEI_SUCH2';
                    $SQL .= '   ,TEI_WERT2';
                    $SQL .= '   ,TEI_USER';
                    $SQL .= '   ,TEI_USERDAT';
                    $SQL .= ' )';
                    $SQL .= ' VALUES';
                    $SQL .= ' (';
                    $SQL .= '    ' . $DB->WertSetzen('TEI','T','AST');
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','Z',$rsATUNR->FeldInhalt('AST_KEY'));
                    $SQL .= '   ,suchwort(' . $DB->WertSetzen('TEI','T',$rsATUNR->FeldInhalt('AST_ATUNR')).')';
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','T',$rsATUNR->FeldInhalt('AST_ATUNR'));
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','T','OEN');
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','Z',$OEKey);
                    $SQL .= '   ,asciiwortoe(' . $DB->WertSetzen('TEI','T',$Daten[$Ueberschriften['OENR']]).')';
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','T',$Daten[$Ueberschriften['OENR']]);
                    $SQL .= '   ,' . $DB->WertSetzen('TEI','T',$AWISBenutzer->BenutzerName());
                    $SQL .= '   ,SYSDATE';
                    $SQL .= ' )';

                    $DB->Ausfuehren($SQL,'', false, $DB->Bindevariablen('TEI'));

                    $MELDUNGZUORDNUNG = $Daten[$Ueberschriften['OENR']] . ' wurde den Artikel ' . $Daten[$Ueberschriften['ATUNR']] . ' zugeordnet.';

                } else {
                    $MELDUNGZUORDNUNG = 'Zuordnung zu Artikel ' . $Daten[$Ueberschriften['ATUNR']] . ' vorhanden.';
                }
                $Erg['Meldungen'][] = 'Zeile ' . $DS . ': ATUNR ' . $Daten[$Ueberschriften['ATUNR']] . ' vorhanden. OENR ' . $MELDUNGOENR . ' + ' . $MELDUNGZUORDNUNG;

                $MELDUNGIMPORT = 'Zeile ' . $DS . ': ATUNR ' . $Daten[$Ueberschriften['ATUNR']] . ' vorhanden. OENR ' . $MELDUNGOENR . ' + ' . $MELDUNGZUORDNUNG;

                $SQL = 'INSERT INTO crossingimportpos';
                $SQL .= ' (';
                $SQL .= '    CIP_CIK_KEY';
                $SQL .= '   ,CIP_ZEILE';
                $SQL .= '   ,CIP_MELDUNG';
                $SQL .= '   ,CIP_USER';
                $SQL .= '   ,CIP_USERDAT';
                $SQL .= ' )';
                $SQL .= ' VALUES';
                $SQL .= ' (';
                $SQL .= '    ' . $DB->WertSetzen('CIP','Z',$CIK_KEY);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','Z',$DS);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$MELDUNGIMPORT);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$AWISBenutzer->BenutzerName());
                $SQL .= '   ,SYSDATE';
                $SQL .= ' )';

                $DB->Ausfuehren($SQL,'', false, $DB->Bindevariablen('CIP'));

            } else {

                $Erg['Meldungen'][] = 'Zeile ' . $DS . ': ATUNR ist nicht vorhanden.';

                $MELDUNGIMPORT = 'Zeile ' . $DS . ': ATUNR ist nicht vorhanden.';

                $SQL = 'INSERT INTO crossingimportpos';
                $SQL .= ' (';
                $SQL .= '    CIP_CIK_KEY';
                $SQL .= '   ,CIP_ZEILE';
                $SQL .= '   ,CIP_MELDUNG';
                $SQL .= '   ,CIP_USER';
                $SQL .= '   ,CIP_USERDAT';
                $SQL .= ' )';
                $SQL .= ' VALUES';
                $SQL .= ' (';
                $SQL .= '    ' . $DB->WertSetzen('CIP','Z',$CIK_KEY);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','Z',$DS);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$MELDUNGIMPORT);
                $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$AWISBenutzer->BenutzerName());
                $SQL .= '   ,SYSDATE';
                $SQL .= ' )';

                $DB->Ausfuehren($SQL,'', false, $DB->Bindevariablen('CIP'));
            }
        } else {
            $Erg['Meldungen'][] = 'Zeile ' . $DS . ': Keine ATUNR im Datensatz vorhanden.';

            $MELDUNGIMPORT = 'Zeile ' . $DS . ': Keine ATUNR im Datensatz vorhanden.';

            $SQL = 'INSERT INTO crossingimportpos';
            $SQL .= ' (';
            $SQL .= '    CIP_CIK_KEY';
            $SQL .= '   ,CIP_ZEILE';
            $SQL .= '   ,CIP_MELDUNG';
            $SQL .= '   ,CIP_USER';
            $SQL .= '   ,CIP_USERDAT';
            $SQL .= ' )';
            $SQL .= ' VALUES';
            $SQL .= ' (';
            $SQL .= '    ' . $DB->WertSetzen('CIP','Z',$CIK_KEY);
            $SQL .= '   ,' . $DB->WertSetzen('CIP','Z',$DS);
            $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$MELDUNGIMPORT);
            $SQL .= '   ,' . $DB->WertSetzen('CIP','T',$AWISBenutzer->BenutzerName());
            $SQL .= '   ,SYSDATE';
            $SQL .= ' )';

            $DB->Ausfuehren($SQL,'', false, $DB->Bindevariablen('CIP'));
        }

        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DS++;
    }

    return $Erg;
}

/**
 * Import Lieferantenartikel-SK-Haken
 *
 * @param ressource $fd
 * @return array
 */
function import_lartnr_sk ($fd, $Dateiname) {
    global $AWISBenutzer;

    $Recht455 = $AWISBenutzer->HatDasRecht(455);

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Erg = array(
        'Code' => 0,
        'Letzte Meldung' => '',
        'Zeile' => 0,
        'Meldungen' => array(),
        'Fehlermeldung' => array(),
        'Anforderung' => '',
        'Meldungen_NEU' => array(),
        'CIK_KEY' => 0,
        'Leerzeile' => 0
    );

    //neuer Eintrag in Tabelle Crossingimportkopf
    $SQL = '  INSERT INTO crossingimportkopf';
    $SQL .= ' (';
    $SQL .= '   CIK_USER';
    $SQL .= '  ,CIK_USERDAT';
    $SQL .= '  ,CIK_DATEINAME';
    $SQL .= ' )';
    $SQL .= ' VALUES';
    $SQL .= ' (';
    $SQL .= '    '.$DB->WertSetzen('CIK','T',$AWISBenutzer->BenutzerName());
    $SQL .= '   ,SYSDATE';
    $SQL .= '   ,'.$DB->WertSetzen('CIK','T',$Dateiname);
    $SQL .= ' )';

    $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('CIK'));

    $SQL = 'SELECT seq_CIK_KEY.CurrVal AS KEY FROM DUAL';
    $rsKey = $DB->RecordSetOeffnen($SQL);
    $CIK_KEY = $rsKey->FeldInhalt('KEY');
    $Erg['CIK_KEY'] = $CIK_KEY;

    $Zeile = fgets($fd);
    $Zeile = trim($Zeile);

    $DS = 1;

    $Ueberschriften = array("ATUNR" => 0, "LIEFNR" => 1, "DEBINR" => 2, "SK" => 3);

    while (!feof($fd)) {

        $Daten = explode("\t", $Zeile);
        $ATUNR = $Daten[$Ueberschriften['ATUNR']];
        $LIEFNR = $Daten[$Ueberschriften['LIEFNR']];
        $LIEFARTNR = $Daten[$Ueberschriften['DEBINR']];
        $ONOFF = $Daten[$Ueberschriften['SK']];

        $SQL = " SELECT";
        $SQL.= " tei.TEI_KEY,";
        $SQL.= " tii.TII_WERT";
        $SQL.= " FROM lieferantenartikel lar INNER JOIN teileinfos tei";
        $SQL.= " ON lar.LAR_KEY = tei.TEI_KEY2";
        $SQL.= " AND tei.TEI_ITY_ID2 = 'LAR'";
        $SQL.= " AND tei.TEI_ITY_ID1 = 'AST'";
        $SQL.= " LEFT JOIN teileinfosinfos tii";
        $SQL.= " ON tei.TEI_KEY = tii.TII_TEI_KEY";
        $SQL.= " WHERE lar.LAR_SUCH_LARTNR = asciiwort(". $DB->WertSetzen('TEI', 'T', $LIEFARTNR).")";
        $SQL.= " AND lar.LAR_LIE_NR = ". $DB->WertSetzen('TEI', 'T', $LIEFNR);
        $SQL.= " AND tei.TEI_SUCH1 = ". $DB->WertSetzen('TEI', 'T', $ATUNR);

        $rsTei = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TEI'));
        $anzDS = $rsTei->AnzahlDatensaetze();

        if($anzDS > 0) {
            while (!$rsTei->EOF()) {
                if ($ONOFF == 0 and ($rsTei->FeldInhalt('TII_WERT') === '1' or $rsTei->FeldInhalt('TII_WERT') === '0')) {
                    // loeschen da SK-Haken gesetzt ist oder ein TEILEINFOSINFOS-DS mit 0 in der Tabelle steht
                    $SQL = " DELETE FROM teileinfosinfos";
                    $SQL .= " WHERE tii_tei_key = " . $DB->WertSetzen('TII', 'N0', $rsTei->FeldInhalt('TEI_KEY'));

                    $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('TII'));
                }

                if ($ONOFF == 1 && $rsTei->FeldInhalt('TII_WERT') === '0') {
                    $SQL = " UPDATE teileinfosinfos";
                    $SQL .= " SET tii_wert = " . $DB->WertSetzen('TII', 'N0', 1);
                    $SQL .= " WHERE tii_ity_key = " . $DB->WertSetzen('TII', 'N0', 318);
                    $SQL .= " AND tii_tei_key = " . $DB->WertSetzen('TII', 'N0', $rsTei->FeldInhalt('TEI_KEY'));

                    $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('TII'));
                }

                if ($ONOFF == 1 && $rsTei->FeldInhalt('TII_WERT') === '') {
                    $SQL = " INSERT INTO teileinfosinfos";
                    $SQL .= " (";
                    $SQL .= "  tii_tei_key";
                    $SQL .= " ,tii_ity_key";
                    $SQL .= " ,tii_wert";
                    $SQL .= " ,tii_user";
                    $SQL .= " ,tii_userdat";
                    $SQL .= " )";
                    $SQL .= " VALUES";
                    $SQL .= " (";
                    $SQL .= "   " . $DB->WertSetzen('TII', 'N0', $rsTei->FeldInhalt('TEI_KEY'));
                    $SQL .= "  ," . $DB->WertSetzen('TII', 'N0', 318);
                    $SQL .= "  ," . $DB->WertSetzen('TII', 'T', '1');
                    $SQL .= "  ,'AWIS'";
                    $SQL .= "  ,SYSDATE";
                    $SQL .= " )";

                    $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('TII'));
                }
                $rsTei->DSWeiter();
            }
        }
        $Zeile = fgets($fd);
        $Zeile = trim($Zeile);
        $DS++;
    }
    return $Erg;
}
?>
