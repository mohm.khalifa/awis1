<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php

$Rechtestufe = awisBenutzerRecht($con, 3300);
		//  1=Einsehen
		//	2=Bearbeiten
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schadens-DB',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

echo "<form name=frmSchaden method=post action=./schaden_Main.php?cmdAktion=Liste>";

$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
if ($Fil_User=='fil-test')
{
	$Fil_Nr='0090';//=substr($AWISBenutzer->BenutzerName(),4,4);
}
else
{
	$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);
}

// Beschr�nkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
awis_Debug(1,$UserFilialen);
// Tabelle aufbauen
if ($Fil_User=='fil-' or $UserFilialen != '')
{	
	echo "<table id=DatenTabelle><tr>";
	echo '<tr><td width=320 ><b>Bearbeitungsnummer</b></td>';
	//echo '<td><input type=text name=txtBearbeitungsnummer size=30></td>';
	echo '<td><input type=text name=BEARB_ID size=30></td>';
	echo '</tr>';
	echo '<tr><td width=320><b>Filial-ID</b><font size=2> (offene KD-Beschwerden anzeigen)</font></td>';
	echo '<td><input type=text name=txtFilialNrOffene size=10></td>';		
	echo '<td><input type=hidden name=txtFilialNr value='.$Fil_Nr.'></td>';
	//echo "<td><input type=hidden name=txtAuftragsart value='S'></td>";
	echo "<td><input type=hidden name=ART value='S'></td>";
	//echo "<td><input type=hidden name=cmdAnzeigen_x value='1'></td>";
	echo '</tr>';
	echo "</table>";	
}
else 
{	
	echo "<table id=DatenTabelle><tr>";
	//echo '<tr><td width=250 ><b>Auftragsart</b></td>';
	//echo "<td><select name=txtAuftragsart><option value='S' selected='selected'>Schaden</option><option value='B'>Beschwerde</option><option value='A'>::ALLE::</option></select></td>";
	//echo '</tr>';	
	
	//TR 06.03.08
	//Es werden nur noch Schadensf�lle angezeigt, da die Beschwerdetabelle nicht mehr benutzt wird!!!!
	//Suche nach Jahr und Offene F�lle funktioniert auch nur bei Sch�den!!!!	
	$mydate = date("Y"); 	
	echo '<tr><td width=250 ><b>Jahr</b></td>';
	echo '<td><input type=text name=txtJahr size=10 value='.$mydate.'></td>';
	echo '</tr>';	
	echo '<tr><td width=250 ><b>ATU Filial-Nummer</b></td>';
	echo '<td><input type=text name=txtFilialNr size=10></td>';
	echo '</tr>';	
	echo '<tr><td width=250 ><b>Status</b></td>';
	echo "<td><select name=txtStatus><option value='A' selected='selected'>Alle</option><option value='O'>Offen</option><option value='G'>Geschlossen</option></select></td>";
	echo "<td><input type=hidden name=txtAuftragsart value='S'></td>";	
	echo '</tr>';	
	echo '<tr><td width=250 ><b>Bearbeitungsnummer</b></td>';
	echo '<td><input type=text name=txtBearbeitungsnummer size=30></td>';
	echo '</tr>';
	echo '<tr><td colspan=2><hr></td></tr>';		
	echo '<tr><td width=250 ><b>Kundenname</b></td>';
	echo '<td><input type=text name=txtKundenname size=30></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>Strasse</b></td>';
	echo '<td><input type=text name=txtStrasse size=30></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>PLZ</b></td>';
	echo '<td><input type=text name=txtPLZ size=10></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>Ort</b></td>';
	echo '<td><input type=text name=txtOrt size=30></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>Telefon</b></td>';
	echo '<td><input type=text name=txtTelefon size=30></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>E-Mail</b></td>';
	echo '<td><input type=text name=txtEMail size=30></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>ATU-Card-Nummer</b></td>';
	echo '<td><input type=text name=txtPAN size=20></td>';
	echo '</tr>';
	echo '<tr><td width=250 ><b>Sachbearbeiter</b></td>';
	echo '<td><input type=text name=txtSachbearbeiter size=30></td>';
	echo '</tr>';
	echo "</table>";
}

print "<hr>&nbsp;<input accesskey=w tabindex=1 type=image src=/bilder/eingabe_ok.png alt='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren>";
print "&nbsp;<img accesskey=r tabindex=2 src=/bilder/radierer.png alt='Formularinhalt l�schen (Alt+r)' name=cmdReset onclick=location.href='./schaden_Main.php?Reset=True';>";

if(($Rechtestufe&4)==4)
{
	print "&nbsp;<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf&uuml;gen (Alt+N)' onclick=location.href='./schaden_Main.php?cmdAktion=Liste'>";
	echo "<input type=hidden name=txtAuftragsart_NEU value='S'>";
	
	/*echo "<hr>";	
	echo "<table id=DatenTabelle><tr>";
	echo "<td>";
	echo '<b>Erstelle: </b>';
	echo "<select name=txtAuftragsart_NEU><option value='S' selected='selected'>Schaden</option><option value='B'>Beschwerde</option></select>";	
	echo "&nbsp;<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf&uuml;gen (Alt+N)' onclick=location.href='./schaden_Main.php?cmdAktion=Liste'>";	
	echo "</td>";
	echo "</tr>";
	echo "</table>";
	*/

}

print "</form>";


// Cursor in das erste Feld setzen
print "<Script Language=JavaScript>";
print "document.getElementsByName(\"txtFilialNr\")[0].focus();";
echo '</SCRIPT>';



?>
</body>
</html>