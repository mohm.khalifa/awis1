<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
include_once('awisFirstglasImport_neu.php');
include_once('awisFirstglasStatus.php');
include_once('firstglas_Shuttle.php');
include_once('firstglas_Migration.php');



global $AWISCursorPosition;		// Aus AWISFormular

ini_set('max_execution_time', 0);

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_FirstGlas');
$TextKonserven[]=array('VJS','*');
$TextKonserven[]=array('VJI','*');
$TextKonserven[]=array('VEP','*');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_FirstGlas'].'</title>';

?>
</head>
<body>
<?

try
{
	$Form = new awisFormular();
        $AWISBenutzer = awisBenutzer::Init();

        if($AWISBenutzer->HatDasRecht(9100)==64)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"200904291315");
		die();
	}


    $Form->Formular_Start();
    $Form->Erstelle_Liste_Ueberschrift("IMPORTCHECK", 1252);

    
    $Shuttle = new firstglasShuttle();
    $Shuttle->pruefeDWHGeladen();
    $Shuttle->Shuttle();
    
    
    /*
    $Migration = FirstGlasMigration::getInstance();
    $Migration->starteImport();
    */

    /*
    $Datum = "20.08.2009 00:00:00";

    $wandleDatum = substr($Datum, 0, 10);
    echo $Jahr = substr($wandleDatum,6,10);
    echo "<br>";
    echo $Monat = substr($wandleDatum,3,2);
    echo "<br>";
    echo $Tag = substr($wandleDatum,0,2);
    $gewDatum = $Jahr."-".$Monat."-".$Tag;
    echo "<br>";
    echo $gewDatum;
    */




    $Form->Formular_Ende();

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200904291312");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}



?>

</body>
</html>