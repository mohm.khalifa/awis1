<?php
require_once('/daten/include/awisZukauf_Import.inc');

try
{
    $Benutzer = 'awis_jobs';
    $DebugLevel = 0;
    $Datei = '';

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--datei':
                            $Datei = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
		            echo ' Optionen:'.PHP_EOL;
		            echo ' --benutzer <Login>                 Anmeldename fuer den Job.'.PHP_EOL;
		            echo ' --debuglevel <Nr>                  Zu verwendender DebugLevel fuer Ausgaben.'.PHP_EOL;
		            echo ' --datei <Dateiname>                Zu importierende Datei.'.PHP_EOL;
                            die();
            }
    }

    if($Datei == '')
    {
        echo 'Keine Datei angegeben';
        die();
    }

    $ZUK = new awisZukauf_Import('awis_jobs');

    $ZUK->DebugLevel($DebugLevel);
    $ZUK->ImportArtikelstammDatei($Datei);
}
catch (Exception $ex)
{
        echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
        echo 'SQL:'.$ex->getSQL().PHP_EOL;
}
?>

