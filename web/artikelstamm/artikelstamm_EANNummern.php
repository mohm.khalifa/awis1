<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('EAN', '%');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht404 = $AWISBenutzer->HatDasRecht(404);            // Recht f�r die Lieferantenartikel
    if ($Recht404 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT *';
    $SQL .= ' FROM EANNUMMERN';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY2 = EAN_KEY AND TEI_ITY_ID2 = \'EAN\'';
    $SQL .= ' INNER JOIN artikelstamm ON TEI_KEY1 = AST_KEY';
    $SQL .= ' WHERE AST_KEY = 0' . $AWIS_KEY1;

    if (isset($_GET['EANSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['EANSort']);
    } else {
        $SQL .= ' ORDER BY EAN_NUMMER DESC';
    }

    $rsEAN = $DB->RecordSetOeffnen($SQL);

    $Form->Formular_Start();
    $Form->ZeileStart();

    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EAN_NUMMER';
    $Link .= '&EANSort=EAN_NUMMER' . ((isset($_GET['EANSort']) AND ($_GET['EANSort'] == 'EAN_NUMMER'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['EAN']['EAN_NUMMER'], 190, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EAN_TYP';
    $Link .= '&EANSort=EAN_TYP' . ((isset($_GET['EANSort']) AND ($_GET['EANSort'] == 'EAN_TYP'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['EAN']['EAN_TYP'], 350, '', $Link);

    $Form->ZeileEnde();

    $DS = 0;
    $EANTypen = $AWISSprachKonserven['EAN']['lst_EAN_TYP'];
    while (!$rsEAN->EOF()) {
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('*EAN_NUMMER', $rsEAN->FeldInhalt('EAN_NUMMER'), 10, 190, false, ($DS % 2), '', '', 'T', 'C');
        $Typ = explode('|', $Form->WerteListe($EANTypen, $rsEAN->FeldInhalt('EAN_TYP')));
        $Form->Erstelle_ListenFeld('*EAN_TYP', (isset($Typ[0])?$Typ[0]:'?'), 10, 350, false, ($DS % 2), '', '', 'T', 'C');

        $Form->ZeileEnde();
        $DS++;

        $rsEAN->DSWeiter();
    }
    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201320");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201321");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>