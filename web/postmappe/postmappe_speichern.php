<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();

    if(isset($_POST['cmdSpeichern_x'])) {

        $SQL = 'INSERT INTO POSTMAPPE(';
        $SQL .= 'PMP_LAG_KZ, PMP_MAP_KZ, PMP_FIL_ID,PMP_XBN_KEY, PMP_USER, PMP_USERDAT';
        $SQL .= ') VALUES (';
        $SQL .= ' ' . $DB->WertSetzen('PMP', 'T', $_POST['txtLAGER'], false);
        $SQL .= ',' . $DB->WertSetzen('PMP', 'T', $_POST['txtMAPPE'], false);

        if ($_POST['txtFILART'] == 2) {
            $SQL .= ',' . $DB->WertSetzen('PMP', 'T', serialize(array('Alle')), true);
        } else {
            $SQL .= ',' . $DB->WertSetzen('PMP', 'T', serialize($_POST['txtFIL_ID']), true);
        }

        $SQL .= ',' . $DB->WertSetzen('PMP', 'N0', $AWISBenutzer->BenutzerID(), false);
        $SQL .= ',' . $DB->WertSetzen('PMP', 'T', $AWISBenutzer->BenutzerName(), true);
        $SQL .= ', SYSDATE)';

        $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('PMP'));
    } elseif(isset($_POST['cmdLoeschen_x'])) {
        $SQL = 'DELETE FROM POSTMAPPE WHERE PMP_XBN_KEY =' . $DB->WertSetzen('PMP','Z',$AWISBenutzer->BenutzerID());

        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('PMP'));
    }
?>
