<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
	//$TextKonserven[]=array('QMM','*');
	$TextKonserven[]=array('BES','*');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	


	$Recht40008 = $AWISBenutzer->HatDasRecht(40008);

	if($Recht40008==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

		$Form->SchreibeHTMLCode('<form name="frmHRTsuche" action="./sachbearbeiter_Main.php?cmdAktion=Details" method="POST"  >');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SBB'));

		$Form->Formular_Start();
		
		/**
		 *  Nachname
		 */
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBNAME'].':',200);
		$Form->Erstelle_TextFeld('*BES_SBNAME',($Param['SPEICHERN']=='on'?$Param['BES_SBNAME']:''),30,180,true,'','','','T','L','','',30);
		$Form->ZeileEnde();
		
		$AWISCursorPosition = 'sucBES_SBNAME';
		
		/**
		 * SBVORNAME
		 */
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBVORNAME'].':',200);
		$Form->Erstelle_TextFeld('*BES_SBVORNAME',($Param['SPEICHERN']=='on'?$Param['BES_SBVORNAME']:''),30,180,true,'','','','T','L','','',30);
		$Form->ZeileEnde();
		
		/**
		 *  Aktiv/Inaktiv
		 */
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',200);
		$Daten = explode('|',$AWISSprachKonserven['BES']['SBA_LST_AKTIV']);
		$Form->Erstelle_SelectFeld('*BES_AKTIV',($Param['SPEICHERN']=='on'?$Param['BES_AKTIV']:''),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		
		// Sammelpostfach
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_SAMMEL'].':',200);
		$Form->Erstelle_Checkbox('*BES_SAMMEL',($Param['SPEICHERN']=='on'?$Param['BES_SAMMEL']:''),20,true,1);
		$Form->ZeileEnde();
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/schaden_neu/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		if(($Recht40008&4)==4)
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		
			
		
		
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
		$Form->SchreibeHTMLCode('</form>');
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>