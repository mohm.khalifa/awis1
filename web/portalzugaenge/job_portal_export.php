<?php
/**
 * Job_export.php
 * 
 * Mit diesem Job werden die ge�nderten Zug�nge aus AWIS exportiert
 * 
 * @author Sacha Kerres
 * @copyright ATU Auto-Teile-Unger
 * @version 20091216
 * @package PORTAL
 */

require_once 'awis_portalzugaenge.inc';
require_once 'awisJobVerwaltung.inc';

try
{
	$Job = new awisJobProtokoll(10,'Starte Export.');
	$Portal = new awis_Portalzugaenge('awis_jobs');
	
	$Job->SchreibeLog(awisJobProtokoll::TYP_MELDUNG,0,'Suche Daten');	
	$Portal->DebugLevel(10);
	
	// Aktuellen Stand exportieren f�r die Verarbeitung
	$DS = $Portal->PersonalExport();
	$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,0,'Export abgeschlossen. '.$DS.' Daten exportiert.');	
	
}
catch (Exception $ex)
{
	if(isset($Job))
	{
		$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	}
}
?>