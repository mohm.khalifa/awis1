<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$RechteStufe = awisBenutzerRecht($con, 1505);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Schluessel',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

awis_Debug('entwick',0,false,$_REQUEST);

// Ausgabe Schluesselanzahl unterschiedlich zur Filialschicht

$SQL="select sln_fil_id, fif_wert, schluesselgruppen.slg_bezeichnung, count(schluesselgruppen.slg_bezeichnung) as anzahl, slg_schichtsuche ";
$SQL.="from schluesselvergaben, schluesselnummern, schluesselgruppen, schluesseluntergruppen, filialinfos ";
$SQL.="where ( (schluesselvergaben.slv_sln_key = schluesselnummern.sln_key(+)) and (schluesselnummern.sln_slg_key=schluesselgruppen.slg_key(+)) ";
$SQL.="and (schluesselnummern.sln_slu_key=schluesseluntergruppen.slu_key(+))) ";
$SQL.="and (schluesselnummern.sln_fil_id=filialinfos.fif_fil_id(+)) ";
$SQL.="and fif_fit_id=20 and fif_imq_id=8 and fif_wert is not null ";
$SQL.="and slu_bezeichnung is null and sln_fil_id is not null and slg_schichtsuche is not null and slv_sls_key=2";
$SQL.="group by sln_fil_id, fif_wert, schluesselgruppen.slg_bezeichnung, slg_schichtsuche ";
$SQL.="order by sln_fil_id, slg_bezeichnung ";

//Filial-Schicht => Haupt,Filial,Werkstatt,Tresor1,Tresor2,Alarmanlage,Smartkey
$Schluessel_Menge= array("1" => array("2","2","2","2","2","3","3"),"2" => array("3","3","2","2","2","3","3"));

awis_Debug(2,$Schluessel_Menge);

$rsSchluesselSchicht = awisOpenRecordset($con,$SQL);
$rsSchluesselSchichtZeilen = $awisRSZeilen;

if($rsSchluesselSchichtZeilen!=0)
{
	echo "<hr>";
	
	echo "<center><h4>Schl&uuml;ssel-Vergabe unterschiedlich zur Filial-Schicht</h4></center>";
	
	echo "<table  width=100% id=DatenTabelle border=1><tr>";
	echo "<td id=FeldBez width=150>Filiale-Nr.</td>";
	echo "<td id=FeldBez width=150>Schicht</td>";
	echo "<td id=FeldBez width=150>Schl&uuml;ssel-Gruppe</td>";	
	echo "<td id=FeldBez width=150>Anzahl</td>";
	echo "</tr>";
	
	for($i=0;$i<$rsSchluesselSchichtZeilen;$i++)
	{
		
		if($rsSchluesselSchicht['ANZAHL'][$i]>$Schluessel_Menge[$rsSchluesselSchicht['FIF_WERT'][$i]][$rsSchluesselSchicht['SLG_SCHICHTSUCHE'][$i]])
		{
			echo "<tr>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><a title=Filial-Details anzeigen href=./schluessel_Main.php?cmdAktion=Vergaben&Anzeigen=TRUE&txtFIL_ID=".$rsSchluesselSchicht['SLN_FIL_ID'][$i].">".$rsSchluesselSchicht['SLN_FIL_ID'][$i]."</a></td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['FIF_WERT'][$i]."</td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['SLG_BEZEICHNUNG'][$i]."</td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['ANZAHL'][$i]."</td>";
			echo "</tr>";
		}
	}
	
	echo "</table>";	
}
?>
</body>
</html>
