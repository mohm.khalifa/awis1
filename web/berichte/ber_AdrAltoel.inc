<?php
require_once 'awisBerichte.inc';

class ber_AdrAltoel
	extends awisBerichte
{
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		//var_dump($Parameter);
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_AdrAuswertungen'));
		
		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = $this->PDFBedingungErstellen($Param,$BindeVariablen);
		
		//********************************************************
		// Order erstellen
		//********************************************************
		$ORDERBY = ' ADK_FIL_ID ASC, adk.adk_datumabschluss DESC';
		
		//*****************************************
		// Daten f�r den Bericht laden
		//*****************************************
        $SQL  = ' SELECT';
        $SQL .= ' ADK_FIL_ID,';
        $SQL .= ' ADK_DATUMABSCHLUSS,';
        $SQL .= ' ADK_LAGERKZ,';
        $SQL .= ' ADS_EWCCODE,';
        $SQL .= ' ADS_EWCBEZ,';
        $SQL .= ' ADP_MENGE,';
        $SQL .= ' FIL_BEZ,';
        $SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILENNR';
        $SQL .= ' FROM ADRKOPF adk';
        $SQL .= ' INNER JOIN ADRPOS adp';
        $SQL .= ' ON adp.adp_adk_key=adk.adk_key';
        $SQL .= ' INNER JOIN ADRSTAMM ads';
        $SQL .= ' ON ads.ads_key=adp.adp_ads_key';
        $SQL .= ' INNER JOIN FILIALEN fil';
        $SQL .= ' ON adk.adk_fil_id=fil.fil_id';
        if($Bedingung!='')
        {
        	$SQL .= ' WHERE ' . substr($Bedingung,4);
        }		
        $SQL .= ' ORDER BY ' .$ORDERBY;
				
//var_dump($SQL);
//die();	
		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		
		//*****************************************
		// �berschriften laden
		//*****************************************
		$BindeVariablen = array();
		$SQL = "SELECT ADS_EWCBEZ, ADS_LAGABEZ FROM ADRSTAMM WHERE ADS_EWCCODE='130205*'";
		$SQL .= ' AND ADS_ADRNR=:var_Z_ADS_ADRNR';
		$BindeVariablen['var_Z_ADS_ADRNR'] = $this->_Form->Format('Z',$Param['ALTOEL'],false);
		$rsADS = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		$Param['ADS_LAGABEZ'] = $this->_Form->Format('T',$rsADS->FeldInhalt('ADS_LAGABEZ'));
		$Param['ADS_EWCBEZ'] = $this->_Form->Format('T',$rsADS->FeldInhalt('ADS_EWCBEZ'));
		$this->_AWISBenutzer->ParameterSchreiben('Formular_AdrAuswertungen',serialize($Param));
		
		//*****************************************
		// Alle Textbausteine laden
		//*****************************************
	    $TextKonserven = array();
        $TextKonserven[]=array('ADR','*');
	    $TextKonserven[]=array('ADK','*');
        $TextKonserven[]=array('ADP','*');
        $TextKonserven[]=array('ADS','*');
        $TextKonserven[]=array('FIL','*');
        
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','DatumVom');
		$TextKonserven[]=array('Wort','DatumBis');
		$TextKonserven[]=array('Liste','lst_ALLE_0');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('Wort','Soll');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eigentliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetFillColor(210,210,210);
		
		// Seite erstellen
		$this->NeueSeite();
		
		while (!$this->_rec->EOF())
		{
			$this->SetXY($this->_Spalte,$this->_Zeile);

			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(15,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ADK_FIL_ID')),1,0,'R');
			$this->Cell(60,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('FIL_BEZ')),1,0,'L');
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ADS_EWCCODE')),1,0,'L');
			$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ADK_DATUMABSCHLUSS')),1,0,'R');
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ADP_MENGE')),1,0,'R');
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ADK_LAGERKZ')),1,0,'R');
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            
			$this->_rec->DSWeiter();
		}

		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
	}
	

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if ($this->PageNo()==1)
		{
			$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_AdrAuswertungen'));
		}
		
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(($this->PageNo()==1?1:2));		// Erste oder zweite Seite importieren
			$this->useTemplate($pdf_vorlage);
		}
		
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Kopfzeile'.($this->PageNo()==1?'1':'2').'-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// Berichtsparameter anzeigen
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Ueberschrift-Groesse']);
		//$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ADR']['ber_AdrAltoel']),0,0,'L','');
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$Param['ADS_LAGABEZ']),0,0,'L','');
		$this->_Zeile+=5;

		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$Param['ADS_EWCBEZ']),0,0,'L','');
		$this->_Zeile+=10;

		if ($this->PageNo()==1)
		{
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['DatumVom']).':',0,0,'L','');
			$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$Param['DATUM_VOM']),0,0,'L','');
			
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['DatumBis']).':',0,0,'L','');
			$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$Param['DATUM_BIS']),0,0,'L','');

			$this->_Zeile+=5;
		}
		
		// Trennlinie
/*
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->Line($this->_Spalte,$this->_Zeile,$this->SeitenBreite()-self::RECHTERRAND,$this->_Zeile);
		$this->_Zeile+=2;
*/		
		$this->Ueberschrift();
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
	}
	
	private function Ueberschrift()
	{
		//�berschrift
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['TabelleUeberschrift-Groesse']*1);
		$this->Cell(15,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_FIL_ID']),1,0,'R',1);
		$this->Cell(60,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['FIL']['FIL_BEZ']),1,0,'L',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADS']['ADS_EWCCODE_kurz']),1,0,'L',1);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_DATUMABSCHLUSS']),1,0,'R',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADP']['ADP_MENGE']),1,0,'R',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_LAGERKZ']),1,0,'R',1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*1.4;		
	}
	
	private function PDFBedingungErstellen($Param, &$BindeVariablen)
	{
    	$Bedingung = '';
    	if(isset($Param['ADK_FIL_ID']) AND $Param['ADK_FIL_ID']!='')
    	{
    		$Bedingung .= ' AND ADK_FIL_ID = :var_N0_ADK_FIL_ID';
    		$BindeVariablen['var_N0_ADK_FIL_ID'] = $this->_Form->Format('N0',$Param['ADK_FIL_ID'],false);
    	}
    	
    	if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
    	{
    		$Bedingung .= ' AND TRUNC(adk_datumabschluss) >= :var_D_DATUM_VOM';
    		$BindeVariablen['var_D_DATUM_VOM'] = $this->_Form->Format('D',$Param['DATUM_VOM'],false);
    	}
    	
    	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
    	{
    		$Bedingung .= ' AND TRUNC(adk_datumabschluss) <= :var_D_DATUM_BIS';
    		$BindeVariablen['var_D_DATUM_BIS'] = $this->_Form->Format('D',$Param['DATUM_BIS'],false);
    	}
    
    	if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
    	{
    		$Bedingung .= ' AND ADK_LAGERKZ = :var_T_ADK_LAGERKZ';
    		$BindeVariablen['var_T_ADK_LAGERKZ'] = $this->_Form->Format('T',$Param['ADK_LAGERKZ'],false);
    	}
    	
		$Bedingung .= " AND ADS_EWCCODE = '130205*'";
		$Bedingung .= ' AND ADS_ADRNR = :var_Z_ADS_ADRNR';
		$BindeVariablen['var_Z_ADS_ADRNR'] = $this->_Form->Format('Z',$Param['ALTOEL'],false);

    	return $Bedingung;
	}
	
}
?>