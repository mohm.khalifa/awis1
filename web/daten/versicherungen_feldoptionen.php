<?php
//**************************************************************
// Liefert Tabellenfelder f�r Versweltfeld
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('HRK', '%');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

if (isset($_GET['OptionsTyp']) and $_GET['OptionsTyp'] == 1) {
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Tabelle: ', 180);
    $SQL = 'SELECT object_name,';
    $SQL .= '   object_name';
    $SQL .= ' FROM';
    $SQL .= '   (SELECT object_id,';
    $SQL .= '     object_name';
    $SQL .= '   FROM SYS.ALL_OBJECTS';
    $SQL .= '   WHERE object_type IN (\'TABLE\',\'VIEW\')';
    $SQL .= '   AND (OBJECT_NAME LIKE \'V\\_VERS%\' escape \'\\\'';
    $SQL .= '   AND OBJECT_NAME NOT LIKE \'V\\_VERS\\_%\' escape \'\\\')';
    $SQL .= '   AND owner IN (\'AWIS\',\'SOA\')';
    $SQL .= '   ORDER BY object_name';
    $SQL .= '   )';
    $Form->Erstelle_SelectFeld('TABELLE', '', 200, true, $SQL, '::Bitte w�hlen::', '', '', '', array(), '', '', array(), '', 'AWIS', 'onChange="ladeTabellenFelder();"');
    $Form->ZeileEnde();

    $Form->ZeileStart('', 'Tabellenfeld_Box');
    $Form->Erstelle_TextLabel('TABELLENFELD' . ':', 160);
    $Form->Erstelle_SelectFeld('TABELLENFELD', '', '345:320', true,
        '***VSF_Spalten;txtTABELLE;txtTABELLE=*txtTABELLE&ZUSATZ=~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] . '&VVE_VERSNR=' . $AWIS_KEY1,
        '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', 'onChange="uebernehmeTabellenfeld();"');
    

    $Form->ZeileEnde();
}

?>
