<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(isset($_GET['Del']))
	{
		$Tabelle = 'PEI';
		$Key=$_GET['Del'];

		$SQL = 'SELECT PEI_KEY, PEI_DATUM, XTZ_TAGESZEIT';
		$SQL .= ' FROM Personaleinsaetze ';
		$SQL .= ' INNER JOIN Tageszeiten ON PEI_VONXTZ_ID = XTZ_ID';
		$SQL .= ' WHERE PEI_KEY=0'.$Key;

		$rsDaten = $DB->RecordsetOeffnen($SQL);

		$PEIKey = $rsDaten->FeldInhalt('PBM_PEI_KEY');

		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('PEI','PEI_DATUM'),$rsDaten->FeldInhalt('PEI_DATUM'));
		$Felder[]=array($Form->LadeTextBaustein('PEI','PEI_VONXTZ_ID'),$rsDaten->FeldInhalt('XTZ_TAGESZEIT'));
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'PEI':
				$SQL = 'DELETE FROM Personaleinsaetze WHERE PEI_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('personaleinsaetze_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./personaleinsaetze_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('PEIKey',$PEIKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>