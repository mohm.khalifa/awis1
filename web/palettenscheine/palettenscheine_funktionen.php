<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

class palettenscheine_funktionen {

    private $_AWISSprachKonverven = '';

    private $_Form = null;

    private $_DB = null;

    private $_Benutzer = null;
 
    public function __construct() {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init();
        $this->_Form = new awisFormular();

        $TextKonserven = array();
        $TextKonserven[] = array('FIL', '%');
        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);
    }
    
    public function erstelleKopfAjax($FIL) {
        $SQL = 'select * from filialen ';
        $SQL .= ' where ';

        if (intval($FIL)) {
            $SQL .= ' FIL_ID = ' . $this->_DB->WertSetzen('FIL','N0',$FIL);
        }else{
            $SQL .= ' lower(FIL_BEZ) like ' . $this->_DB->WertSetzen('FIL','T','%'.mb_strtolower($FIL).'%');
        }

        $rsFil = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FIL',true));

        $FeldBreiten['Labels'] = 150;
        $FeldBreiten['Werte'] = 200;
        $FeldBreiten['FIL_PLZ'] = 100;
        $FeldBreiten['FIL_ORT'] = 150;

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FIL']['FIL_ID'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ID'),$FeldBreiten['Werte']);
        $this->_Form->Erstelle_HiddenFeld('FIL_ID',$rsFil->FeldInhalt('FIL_ID'));
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FIL']['FIL_BEZ'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_BEZ'),$FeldBreiten['Werte']);
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FIL']['FIL_STRASSE'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_STRASSE'),$FeldBreiten['Werte']);
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FIL']['FIL_PLZ'].'/'.$this->_AWISSprachKonserven['FIL']['FIL_ORT'].':',$FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_PLZ'),$FeldBreiten['FIL_PLZ']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ORT'),$FeldBreiten['FIL_ORT']);
        $this->_Form->ZeileEnde();
    }

}