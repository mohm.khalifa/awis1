<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_Matching
{

private $_Funktionen;
private $_DB;
protected $_AWISBenutzer;		
protected $preisListe = '';

public function __construct()
{
  $this->_AWISBenutzer = awisBenutzer::Init();
  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  $this->_DB->Oeffnen();
}	

public function setPreisListe($PreisListe)
{
	$this->preisListe = $PreisListe;
}

public function starteMatching()
{
	
	$SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_WG IS NOT NULL AND GKZ_SG IS NOT NULL AND GKZ_AUTO='j'";
	$SQL .= " AND GKZ_GKP_KEY=".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
	
	$rsRSL = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsRSL->EOF())
	{
		$SQL =  'SELECT AST_ATUNR FROM V_ARTIKELSTAMMWGR WHERE AST_WGR_ID='.$this->_DB->FeldInhaltFormat('T',$rsRSL->FeldInhalt('GKZ_WG'));
		$SQL .= ' AND AST_WUG_ID='.$this->_DB->FeldInhaltFormat('T',$rsRSL->FeldInhalt('GKZ_SG'));		
		
		$rsInsert = $this->_DB->RecordSetOeffnen($SQL);
		
		while(!$rsInsert->EOF())
		{
			$SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES (';		
			$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsRSL->FeldInhalt('GKZ_KEY'));
			$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsInsert->FeldInhalt('AST_ATUNR'));
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
   			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			if($this->_DB->Ausfuehren($SQL)===false) {
   			}
   			
   			$rsInsert->DSWeiter();
		}
	
		$rsRSL->DSWeiter();
	}	
	
	$SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_AUTO='j' AND GKZ_WG IS NULL AND GKZ_SG IS NULL";
	$SQL .= " AND GKZ_GKP_KEY=".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
	$SQL .= " order by gkz_rx";
	
	$rsReifen = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsReifen->EOF())
	{
		$SucheR1 = false;
		$SucheR2 = false;
		
		$SQL  = "Select REI_AST_ATUNR FROM V_REIFENZUSATZINFOS WHERE";
		$SQL .= " REI_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_BREITE'));
		$SQL .= " AND REI_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_QUERSCHNITT'));
		$SQL .= " AND REI_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_BAUART'));
		$SQL .= " AND REI_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_INNENDURCHMESSER'));
		$SQL .= " AND REI_LOADINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_LOADINDEX1'));
		if($rsReifen->FeldInhalt('GKZ_LOADINDEX2') != '')
		{
			$SQL .= " AND REI_LOADINDEX2=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_LOADINDEX2'));
		}
		else 
		{
			$SQL .= " AND REI_LOADINDEX2 is null";
		}
		$SQL .= " AND REI_SPI_SYMBOL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SPEEDINDEX'));
		if($rsReifen->FeldInhalt('GKZ_GWS') != '')
		{
			$SQL .= " AND GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_GWS'));
		}
		else 
		{
			$SQL .= " AND GWS is null";
		}
		$SQL .= " AND RX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_RX'));
		if($rsReifen->FeldInhalt('GKZ_ROF') != '')
		{
			$SQL .= " AND ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_ROF'));
		}
		else 
		{
			$SQL .= " AND ROF is null";
		}
		if($rsReifen->FeldInhalt('GKZ_SEAL') != '')
		{
			$SQL .= " AND SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SEAL'));
		}
		else 
		{
			$SQL .= " AND SEAL is null";
		}
		
		$rsZuordnung = $this->_DB->RecordSetOeffnen($SQL);
		
		if ($rsReifen->FeldInhalt('GKZ_RX')=='R3' and $rsZuordnung->AnzahlDatensaetze()==0)
		{
			$SucheR1 = true;
			
			//Pr�fen, ob Reifen mit R1 vorhanden ist
			$SQL  = "Select REI_AST_ATUNR FROM V_REIFENZUSATZINFOS WHERE";
			$SQL .= " REI_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_BREITE'));
			$SQL .= " AND REI_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_QUERSCHNITT'));
			$SQL .= " AND REI_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_BAUART'));
			$SQL .= " AND REI_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_INNENDURCHMESSER'));
			$SQL .= " AND REI_LOADINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_LOADINDEX1'));
			if($rsReifen->FeldInhalt('GKZ_LOADINDEX2') != '')
			{
				$SQL .= " AND REI_LOADINDEX2=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_LOADINDEX2'));
			}
			else 
			{
				$SQL .= " AND REI_LOADINDEX2 is null";
			}
			$SQL .= " AND REI_SPI_SYMBOL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SPEEDINDEX'));
			if($rsReifen->FeldInhalt('GKZ_GWS') != '')
			{
				$SQL .= " AND GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_GWS'));
			}
			else 
			{
				$SQL .= " AND GWS is null";
			}
			$SQL .= " AND RX=".$this->_DB->FeldInhaltFormat('T','R1');
			if($rsReifen->FeldInhalt('GKZ_ROF') != '')
			{
				$SQL .= " AND ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_ROF'));
			}
			else 
			{
				$SQL .= " AND ROF is null";
			}
			if($rsReifen->FeldInhalt('GKZ_SEAL') != '')
			{
				$SQL .= " AND SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SEAL'));
			}
			else 
			{
				$SQL .= " AND SEAL is null";
			}
			
			$rsZuordnung = $this->_DB->RecordSetOeffnen($SQL);
			
			if ($rsZuordnung->AnzahlDatensaetze()==0)
			{
				$SucheR2 = true;
				
				//Pr�fen, ob Reifen mit R1 vorhanden ist
				$SQL  = "Select REI_AST_ATUNR FROM V_REIFENZUSATZINFOS WHERE";
				$SQL .= " REI_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_BREITE'));
				$SQL .= " AND REI_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_QUERSCHNITT'));
				$SQL .= " AND REI_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_BAUART'));
				$SQL .= " AND REI_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_INNENDURCHMESSER'));
				$SQL .= " AND REI_LOADINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_LOADINDEX1'));
				if($rsReifen->FeldInhalt('GKZ_LOADINDEX2') != '')
				{
					$SQL .= " AND REI_LOADINDEX2=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_LOADINDEX2'));
				}
				else 
				{
					$SQL .= " AND REI_LOADINDEX2 is null";
				}
				$SQL .= " AND REI_SPI_SYMBOL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SPEEDINDEX'));
				if($rsReifen->FeldInhalt('GKZ_GWS') != '')
				{
					$SQL .= " AND GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_GWS'));
				}
				else 
				{
					$SQL .= " AND GWS is null";
				}
				
				$SQL .= " AND RX=".$this->_DB->FeldInhaltFormat('T','R2');
				
				if($rsReifen->FeldInhalt('GKZ_ROF') != '')
				{
					$SQL .= " AND ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_ROF'));
				}
				else 
				{
					$SQL.= " AND ROF is null";
				}
				if($rsReifen->FeldInhalt('GKZ_SEAL') != '')
				{
					$SQL .= " AND SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SEAL'));
				}
				else 
				{
					$SQL .= " AND SEAL is null";
				}
				
				$rsZuordnung = $this->_DB->RecordSetOeffnen($SQL);
			}			
		}
		
		while(!$rsZuordnung->EOF())
		{	
			$Vorhanden = false;		
			
			if ($SucheR1 == true or $SucheR2 == true)
			{							
				//Pr�fen, ob Artikel troztdem schon zugordnet ist
				$SQL = "SELECT * FROM gkpreislistezuord";
				$SQL.= " inner join gkatunrzuord on gkz_key = gka_gkz_key and gka_ast_atunr = ".$this->_DB->FeldInhaltFormat('T',$rsZuordnung->FeldInhalt('REI_AST_ATUNR'));
				$SQL.= " where gkz_gkp_key = ".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);				
				
				$rsGKA = $this->_DB->RecordSetOeffnen($SQL);

				if ($rsGKA->AnzahlDatensaetze()>0)
				{
					echo 'Bereits vorhanden'.PHP_EOL;
					$Vorhanden = true;												
				}
			}
			
			if (($SucheR1 == false and $SucheR2 == false) or $Vorhanden == false)
			{
				$SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES (';		
				$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_KEY'));
				$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsZuordnung->FeldInhalt('REI_AST_ATUNR'));
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
	   			$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				if($this->_DB->Ausfuehren($SQL)===false) 
				{
	   			}	
			}
			$rsZuordnung->DSWeiter(); 		
		}		
		$rsReifen->DSWeiter();
	}
}

public function starteAnreicherung()
{
	//Reifen mit Kenn 1 = E, die nicht ermittelt werden konnten	
	$SQL = 'select * ';
	$SQL.= ' from artikelstamm aa';
	$SQL.= ' inner join v_artikelstammwgr a on a.ast_atunr = aa.ast_atunr and (a.ast_wgr_id = \'01\' or a.ast_wgr_id = \'02\' or (a.ast_wgr_id = \'23\' and (a.ast_wug_id = 200 or a.ast_wug_id = 201)))';
	$SQL.= ' left join gkatunrzuord g on G.GKA_AST_ATUNR = AA.AST_ATUNR';
	$SQL.= ' and G.GKA_GKZ_KEY in';
	$SQL.= ' (select p.gkz_key';
	$SQL.= ' from gkpreislistezuord p';
	$SQL.= ' inner join gkpreisliste l on L.GKP_KEY = P.GKZ_GKP_KEY';
	$SQL.= ' inner join gkkunde k on K.GKD_KEY = 1';
	$SQL.= ' where GKZ_WG IS NOT NULL AND GKZ_SG IS NOT NULL AND GKZ_AUTO=\'j\' and GKZ_GKP_KEY='.$this->_DB->FeldInhaltFormat('NO',$this->preisListe).')';
	$SQL.= ' where aa.ast_kennung = \'E\' and G.GKA_KEY is null';
	//echo $SQL;

	$rsAST = $this->_DB->RecordSetOeffnen($SQL);

	while (!$rsAST->EOF())
	{
		//Letzten g�ltigen Preis von dem Artikel vom COM-Server holen
		$SQL = 'select * ';
		$SQL.= ' from experian_atu.EXP_FESTPREISLISTEN_TAB@com_de.atu.de';
		$SQL.= ' where id_fplb = 23 and artnr='.$this->_DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_ATUNR'));
		$SQL.= ' order by transaktionsid desc';

		$rsEXP = $this->_DB->RecordSetOeffnen($SQL);
		
		if (!$rsEXP->EOF())
		{
			$SQL = 'INSERT INTO GKATUNRANREICHERUNG (GKR_GKP_KEY,GKR_AST_ATUNR,GKR_PREIS,GKR_GUELTIGAB,GKR_GULTIGBIS,GKR_TRANSAKTIONSID,GKR_USER,GKR_USERDAT)';
			$SQL .= ' VALUES(';
			$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
			$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsEXP->FeldInhalt('ARTNR'));
			$SQL .= ','.$this->_DB->FeldInhaltFormat('N2',$rsEXP->FeldInhalt('PREIS'));
			$SQL .= ','.$this->_DB->FeldInhaltFormat('D',$rsEXP->FeldInhalt('GUELTIG_VON'));
			$SQL .= ','.$this->_DB->FeldInhaltFormat('D',$rsEXP->FeldInhalt('GUELTIG_BIS'));
			$SQL .= ','.$this->_DB->FeldInhaltFormat('NO',$rsEXP->FeldInhalt('TRANSAKTIONSID'));
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
   			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($this->_DB->Ausfuehren($SQL)===false) 
			{
   			}			
		}
		
		$rsAST->DSWeiter();		
		
	}		
}

}


?>