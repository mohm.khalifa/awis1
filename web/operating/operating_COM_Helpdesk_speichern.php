<?php
global $AWISBenutzer;
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $comcon;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Operating','OCH_%');


$HDF_KEY = $_REQUEST['HDF_KEY'];
$HDK_KEY = $_REQUEST['HDK_KEY'];


$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(intval($HDF_KEY)===0)		// Neuer Eintrag
{

}
else 					// gešnderter Errlog-Eintrag
{
	$Felder = explode(';',awis_NameInArray($_POST, 'txt',1,1));
	$FehlerListe = array();
	$UpdateFelder = '';

	$SQL='SELECT * FROM COMOP.HELPDESK_FELDER WHERE HDF_KEY=' . $HDF_KEY . '';
	$rsHDF = awisOpenRecordset($comcon,$SQL);
	awis_debug(1,$rsHDF);
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
			// Alten und neuen Wert umformatieren!!
			$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsHDF[$FeldName][0],true);
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		
		$Meldung = str_replace('%1',$rsHDF['HDF_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE COMOP.HELPDESK_FELDER SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', HDF_USER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', HDF_USERDAT=sysdate';
		$SQL .= ' WHERE HDF_KEY=0' . $HDF_KEY . '';

		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('Operating_COM_Helpdesk',1,'Fehler beim Speichern',$SQL);
		}
	}

	$HDFKEY=$HDF_KEY;
awis_Debug(1,$FeldListe,$SQL,$Felder);
}

?>