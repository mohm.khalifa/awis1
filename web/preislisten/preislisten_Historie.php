<?php
global $con;
global $RechteStufe;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con,2001);		// Preislistenadministration
if($RechteStufe==0)
{
	awisLogoff($con);
	die();
}

if(($RechteStufe&16)!=16)		// Rechte f�r Historie anzeigen
{
    awisEreignis(3,1000,'Preislistenhistorie',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$comcon = awisLogonComCluster('DE',false);


echo '<form name=frmHistorie method=POST>';

//*********************************************
// Preislisten - Auswahl
//*********************************************

$SQL = 'SELECT * FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB';
if(isset($_POST['txtID_FPLB']))
{
	$SQL .= ' WHERE ID_FPLB=0' . $_POST['txtID_FPLB'];
}
$SQL .= ' ORDER BY ID_FPLB';

$rsPLI = awisOpenRecordset($comcon,$SQL);
$rsPLIZeilen = $awisRSZeilen;

if(!isset($_POST['txtID_FPLB']))
{
	echo '<table border=0>';
	echo '<tr><td>Preisli<u>s</u>te</td><td><select accesskey=s name=txtID_FPLB onchange=form.submit();>';
	echo '<option value=0>::Bitte w�hlen::</option>';
	for($PLIZeile=0;$PLIZeile<$rsPLIZeilen;$PLIZeile++)
	{
		echo '<option ';

		echo ' value=' . $rsPLI['ID_FPLB'][$PLIZeile] . '>';
		echo $rsPLI['BEZEICHNUNG'][$PLIZeile];
		echo '</option>';
	}

	echo '</select></td></tr>';
	echo '</table>';
}
else 		// Preisliste gew�hlt
{

	echo '<input type=hidden name=txtID_FPLB value='.$_POST['txtID_FPLB'].'>';
	echo '<h3>Transaktionshistorie f�r '. $rsPLI['BEZEICHNUNG'][0] . '</h3><hr>';

	$TID = awis_NameInArray($_POST,'cmdDetails_',0);
	if($TID!='')
	{
		$TID = substr($TID,11);
		$TID = substr($TID,0,strlen($TID)-2);
	}

	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' WHERE PTS_ID_FPLB=0'.$rsPLI['ID_FPLB'][0];
	if($TID!='')
	{
		$SQL .= ' AND PTS_TRANSAKTIONSID=0'.$TID;
	}
	$SQL .= ' ORDER BY PTS_TRANSAKTIONSID DESC';

	$rsPTS = awisOpenRecordset($comcon,$SQL);
	$rsPTSZeilen = $awisRSZeilen;

	if($TID=='')
	{

		echo '<table class=DatenTabelle border=1>';
		echo '<tr>';
		echo '<td class=FeldBez>&nbsp;</td>';
		echo '<td class=FeldBez>Transaktions-ID</td>';
		echo '<td class=FeldBez>Status</td>';
		echo '<td class=FeldBez>Bemerkung</td>';
		echo '<td class=FeldBez>Benutzer</td>';
		echo '<td class=FeldBez>�nderung</td>';
		echo '<td class=FeldBez>G�ltig von</td>';
		echo '<td class=FeldBez>G�ltig bis</td>';
		echo '</tr>';

		for($PTSZeile=0;$PTSZeile<$rsPTSZeilen;$PTSZeile++)
		{
			echo '<tr>';

			echo '<td>';
			echo '<input type=image title=\'Details anzeigen\' name=cmdDetails_'.$rsPTS['PTS_TRANSAKTIONSID'][$PTSZeile].' src=/bilder/info.png>';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_TRANSAKTIONSID'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PST_STATUS'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_BEMERKUNG'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_USER'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_USERDAT'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_GUELTIG_VON'][$PTSZeile].'';
			echo '</td>';

			echo '<td>';
			echo ''.$rsPTS['PTS_GUELTIG_BIS'][$PTSZeile].'';
			echo '</td>';


			echo '</tr>';
		}

		echo '</table>';
	}
	else
	{
		//************************************************************
		// Weitere Details zeigen
		//************************************************************
		if($TID!='')
		{
			awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");


			echo '<br><b>Folgende Pr�fungen wurden f�r die Transaktion '.$TID.' durchgef�hrt:</b><br><br>';
			// Alle Pr�fungen anzeigen
			echo '<table border=1 width=100%>';
			$SQL = 'SELECT  PTA_BEMERKUNG, PTA_BEZEICHNUNG, PPH_STATUS, PPH_ENDE , PPH_USER, PPH_KEY';
			$SQL .= ',(SELECT COUNT(*) FROM EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT WHERE PAU_PPH_KEY=PPH.PPH_KEY) AS AUFF';
			$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN PLP';
			$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN PTA ON PLP_PTA_ID=PTA_ID  ';
			$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST PPH ON PPH_PLP_KEY =  PLP_Key';
			$SQL .= ' WHERE PPH_TRANSAKTIONSID=0' . $TID;
			$SQL .= ' ORDER BY PLP_ReihenFolge';

			$rsPTA = awisOpenRecordset($comcon,$SQL);
			$rsPTAZeilen = $awisRSZeilen;


			echo '<tr><td class=FeldBez width=200>Pr�fung</td>';
			echo '<td class=FeldBez>Ergebnis</td>';
			echo '<td class=FeldBez>Auff�lligkeiten</td>';
			echo '<td class=FeldBez>Pr�fungsende</td>';
			echo '<td class=FeldBez>Gepr�ft durch</td>';
			echo '</tr>';

			for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
			{
				echo '<tr>';
				echo '<td title=\'' . $rsPTA['PTA_BEMERKUNG'][$PTAZeile] . '\'><b>' . $rsPTA['PTA_BEZEICHNUNG'][$PTAZeile] . '</b></td>';
				echo '<td>' . PPHStatusText($rsPTA['PPH_STATUS'][$PTAZeile]) . '</td>';

				if($rsPTA['AUFF'][$PTAZeile]>0)
				{
					echo "<td title='Klicken Sie auf die Zahl, um die Details anzuzeigen'><a onclick=window.open('./preislisten_Auffaelligkeiten.php?PPHKEY=" . $rsPTA['PPH_KEY'][$PTAZeile] . "','Preislistenpr�fungdetails','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>" . $rsPTA['AUFF'][$PTAZeile] . '</a></td>';
				}
				else
				{
					echo '<td>' . $rsPTA['AUFF'][$PTAZeile] . '</td>';
				}

				echo '<td>' . $rsPTA['PPH_ENDE'][$PTAZeile] . '</td>';
				echo '<td>' . $rsPTA['PPH_USER'][$PTAZeile] . '</td>';

				echo '</tr>';

			}
			echo '</table>';


			$SQL = 'SELECT PST_STATUS, PTH_BEMERKUNG, PTH_USER, PTH_USERDAT ';
			$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUSHISTORIE';
			$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTH_PST_ID = PST_ID';
			$SQL .= ' WHERE PTH_PTS_TRANSAKTIONSID=0'.$TID;
			$SQL .= ' ORDER BY PTH_KEY DESC';

			echo '<br><b>Status�nderungen</b><br><br>';
			echo '<table class=DatenTabelle border=1>';
			echo '<tr>';
			echo '<td class=FeldBez>Status</td>';
			echo '<td class=FeldBez>Bemerkung</td>';
			echo '<td class=FeldBez>Benutzer</td>';
			echo '<td class=FeldBez>Datum</td>';
			echo '<tr>';

			$rsPTH = awisOpenRecordset($comcon,$SQL);
			$rsPTHZeilen = $awisRSZeilen;
			for($PTHZeile=0;$PTHZeile<$rsPTHZeilen;$PTHZeile++)
			{
				echo '<tr>';

				echo '<td>' . $rsPTH['PST_STATUS'][$PTHZeile] . '</td>';
				echo '<td>' . $rsPTH['PTH_BEMERKUNG'][$PTHZeile] . '</td>';
				echo '<td>' . $rsPTH['PTH_USER'][$PTHZeile] . '</td>';
				echo '<td>' . $rsPTH['PTH_USERDAT'][$PTHZeile] . '</td>';

				echo '<tr>';




			}

			echo '</table>';



		}
	}

	if($TID=='')
	{
		echo "<br><img src=/bilder/NeueListe.png accesskey='T' title='�bersicht (Alt+T)' onclick=location.href='./preislisten_Main.php?cmdAktion=Historie';>";
	}
	else
	{
		echo '<br><input accesskey=T type=image title=\'Zur�ck zur Liste (Alt+T)\' name=cmdUebersicht src=/bilder/NeueListe.png>';
	}
}

awisLogoff($comcon);
?>