create or replace package body zahl_vers
is
     function aend_zahlungen_vers_new
	 (pa_anzahl number,
	 pa_zkey varchar2,
	 pa_bearbeitungsnr varchar2,
	 pa_bid varchar2,
	 pa_betrag varchar2,
	 pa_verwendungszweck varchar2,
	 pa_datum varchar2,
	 pa_vs_id varchar2,
	 pa_schadensnr varchar2,
	 pa_art varchar2,
	 pa_standid varchar2,
	 pa_vzweckleer number,
   pa_zusatzinfo varchar2,
   pa_zileer number,
   pa_user varchar2,
   pa_userdat date)
	 return varchar2
     IS

	   	   v_meldung varchar2(1000);
	 	   doppelte_versnr exception;
	 	   v_doppelt_bearbnr varchar2(256) :='';

     	   procedure eintrag_zahlungen(
  	 	   p_zkey varchar2,
  	 	   p_bid varchar2,
  	 	   p_betrag varchar2,
  	 	   p_verwendungszweck varchar2,
  	 	   p_datum varchar2,
	 	   p_vzweckleer number,
       p_zusatzinfo varchar2,
       p_zileer number,
       p_user varchar2,
       p_userdat date)
	 	   is
  	            v_upd_zahlungen varchar2(500) := 'update zahlungen set ';
	 	   begin
	   	        if p_bid is not null then
	 	       	     v_upd_zahlungen := v_upd_zahlungen||'bid='||p_bid||',';
	 	        end if;

	 	        if p_betrag is not null then
	 	             v_upd_zahlungen := v_upd_zahlungen||'betrag='||p_betrag||',';
	 	        end if;

	 	        if p_verwendungszweck is not null or p_vzweckleer = 1 then
		             v_upd_zahlungen := v_upd_zahlungen||'verwendungszweck='''||p_verwendungszweck||''',';
	 	        end if;

	 	        if p_datum is not null then
	                 v_upd_zahlungen := v_upd_zahlungen||'datum=to_date('''||p_datum||''',''dd.mm.yy''),';
	 	        end if;

            if p_zusatzinfo is not null or p_zileer = 1 then
		             v_upd_zahlungen := v_upd_zahlungen||'zusatzinfo='''||p_zusatzinfo||''',';
	 	        end if;
            if p_user is not null or p_zileer = 1 then
		             v_upd_zahlungen := v_upd_zahlungen||'z_user='''||p_user||''',';
	 	        end if;
            if p_userdat is not null or p_zileer = 1 then
		             v_upd_zahlungen := v_upd_zahlungen||'z_userdat='''||p_userdat||''',';
	 	        end if;

  	  	        v_upd_zahlungen := rtrim(v_upd_zahlungen,',')||' where zkey = '||p_zkey;

	 	        EXECUTE IMMEDIATE v_upd_zahlungen;

           end eintrag_zahlungen;

           procedure eintrag_vers_new(
	 	   p_vs_id varchar2,
	 	   p_bearbeitungsnr varchar2,
	       p_schadensnr varchar2,
	 	   p_art varchar2,
	 	   p_standid varchar2)
	 	   is
	   	        v_upd_vers_new varchar2(500) := 'update vers_new set ';
	       begin

              /*
               --ermittelt ob bereits Datens�tze mit der �bergebenen Vers-Nr. in der Datenbank vorhanden sind,
	 	       --welche nicht mit der aktuellen Bearbeitungsnr zusammenh�ngen! In die Variable v_doppelt_bearbnr
	 	       --werden die Bearbeitungsnummer der gefundenen Datens�tze geschrieben


		       if p_schadensnr is not null then
		   	        for vers_rec in(
               	         SELECT a.bearbeitungsnr FROM
            	  	  	 zahlungen a,
            	  	  	 vers_new b
            	  	  	 WHERE
            	  	  	 a.bearbeitungsnr != p_bearbeitungsnr
    	  		  	  	 AND
				  	  	 a.zuvsid = b.vs_id(+)
    	  		  	  	 AND b.schadensnr = p_schadensnr)
			        loop
                         v_doppelt_bearbnr := to_char(v_doppelt_bearbnr||vers_rec.bearbeitungsnr||'\n');
                    end loop;
	            end if;

                if v_doppelt_bearbnr is not null then								   -- es sind doppelte vorhanden

                     v_doppelt_bearbnr := rtrim(v_doppelt_bearbnr,'\n');
		             raise doppelte_versnr;

		        else
                */
	   	             if p_schadensnr is not null then
	 	                 v_upd_vers_new := v_upd_vers_new||'schadensnr='''||p_schadensnr||''',';
	 	             end if;

	 	             if p_art is not null then
	 	                  v_upd_vers_new := v_upd_vers_new||'art='''||p_art||''',';
	 	             end if;

	 	             if p_standid is not null then
		                  v_upd_vers_new := v_upd_vers_new||'standid='||p_standid||',';
	 	             end if;

  	  	             v_upd_vers_new := rtrim(v_upd_vers_new,',')||' where vs_id = '||p_vs_id;

	 	             EXECUTE IMMEDIATE v_upd_vers_new;

	            /*
                end if;
                */

           end eintrag_vers_new;

	       procedure neu_zahlungen(
  	 	   p_bearbeitungsnr varchar2,
  	 	   p_bid varchar2,
  	 	   p_betrag varchar2,
  	 	   p_verwendungszweck varchar2,
  	 	   p_datum varchar2,
	 	   p_zuvsid varchar2,
       p_zusatzinfo varchar2)
	 	   is
  	            v_ins_zahlungen varchar2(500);
	 	   begin

		        v_ins_zahlungen := 'insert into zahlungen(zkey, bearbeitungsnr, bid,betrag, verwendungszweck, datum, zuvsid, zusatzinfos) values(zkey_zahlungen.nextval,'''||
		  		p_bearbeitungsnr||''','||p_bid||','||p_betrag||','''||p_verwendungszweck||''',to_date('''||
		  		p_datum||''',''dd.mm.yy''),'||p_zuvsid||''''||p_zusatzinfo||''')';

	 	  		EXECUTE IMMEDIATE v_ins_zahlungen;

           end neu_zahlungen;

     BEGIN

	       if pa_vs_id is null then				--Es existiert kein Eintrag in der Tabelle Vers_New

  	 	        eintrag_zahlungen(pa_zkey,pa_bid,pa_betrag,pa_verwendungszweck,pa_datum,pa_vzweckleer,pa_zusatzinfo,pa_zileer,pa_user,pa_userdat);



  	 	   elsif pa_zkey is null then		--Es existiert ein Eintrag in der Tabelle Vers_New jedoch nicht in Zahlungen

		        if pa_bid is not null or pa_betrag is not null or
			    pa_verwendungszweck is not null or pa_datum is not null then 	   	  	  	   --�nderungen an Zahlungen
				     neu_zahlungen(pa_bearbeitungsnr,pa_bid,pa_betrag,pa_verwendungszweck,pa_datum,pa_vs_id,pa_zusatzinfo);

			    else
			   	     neu_zahlungen(pa_bearbeitungsnr,'2',NULL,NULL,NULL,pa_vs_id,NULL);
			    end if;

			    if pa_schadensnr is not null or pa_art is not null or pa_standid is not null then --�nderungen an Vers_New
			         eintrag_vers_new(pa_vs_id,pa_bearbeitungsnr,pa_schadensnr,pa_art,pa_standid);
			    end if;

		   else 	  	   	  	   				--Es existieren Eintr�ge in den Tabellen Vers_new und Zahlungen

	 	        if pa_schadensnr is not null or pa_art is not null or pa_standid is not null then --�nderungen an Vers_New
			         eintrag_vers_new(pa_vs_id,pa_bearbeitungsnr,pa_schadensnr,pa_art,pa_standid);
			    end if;

			    if pa_bid is not null or pa_betrag is not null or
			    pa_verwendungszweck is not null or pa_zusatzinfo is not null or pa_datum is not null or pa_vzweckleer = 1 or pa_zileer = 1 then --�nderungen an Zahlungen
					 eintrag_zahlungen(pa_zkey,pa_bid,pa_betrag,pa_verwendungszweck,pa_datum,pa_vzweckleer,pa_zusatzinfo,pa_zileer,pa_user,pa_userdat);
	 		    end if;



	       end if;

           commit;

	       if pa_anzahl > 1 then
	            v_meldung := pa_anzahl||' Datenfelder aktualisiert!';
	       else
	 	        v_meldung := '1 Datenfeld aktualisiert!';
	       end if;

	       return v_meldung;


     EXCEPTION
           /*
           WHEN DOPPELTE_VERSNR THEN

	            v_meldung := 'Die Vers-Nr. '||pa_schadensnr||' besteht bereits ';

	            if length(v_doppelt_bearbnr)>9 then
	   	             v_meldung := v_meldung||'bei den Datens�tzen mit folgenden Schadens-Nummern:<br>'||v_doppelt_bearbnr;
	            else
		             v_meldung := v_meldung||'beim Datensatz mit folgender Schadens-Nummer:<br>'|| v_doppelt_bearbnr;
	            end if;

	            v_meldung := v_meldung||'<br>Der Datensatz konnte nicht aktualisiert werden!';

	            return v_meldung;
          */


           WHEN OTHERS THEN

                v_meldung := 'Fehler!<br>'||SQLERRM||'<br>Der Datensatz konnte nicht ge�ndert werden!';
		        return v_meldung;
	 END aend_zahlungen_vers_new;

	 function zahlungen_vers
	 (p_bearbeitungsnr IN zahlungen.bearbeitungsnr%TYPE,
	 p_bid IN zahlungen.bid%TYPE,
	 p_betrag IN zahlungen.betrag%TYPE,
	 p_verwendungszweck IN zahlungen.verwendungszweck%TYPE,
	 p_datum IN varchar2,
	 p_zusatzinfo IN zahlungen.zusatzinfo%TYPE,
   p_user IN zahlungen.z_user%TYPE DEFAULT '',
   p_userdat IN zahlungen.z_userdat%TYPE DEFAULT '',
   p_schadensnr IN vers_new.schadensnr%TYPE DEFAULT '',
	 p_art IN vers_new.art%TYPE DEFAULT '',
	 p_standid IN vers_new.standid%TYPE DEFAULT 0
   )
	 return varchar2
	 IS
	       v_datum date := to_date(p_datum,'DD.MM.YY');
           v_doppelt_bearbnr varchar2(256) :='';
  		   v_vs_id vers_new.vs_id%type := 0;
  		   v_meldung varchar2(1000);
  		   doppelte_versnr exception;

     BEGIN

  	       --Wenn eine Vers-Nr. �bergeben worden ist

  		   IF p_schadensnr is not null
  		   THEN

		        /*
                --ermittelt ob bereits Datens�tze mit der �bergebenen Vers-Nr. in der Datenbank vorhanden sind,
	  			--welche nicht mit der aktuellen Bearbeitungsnr zusammenh�ngen! In die Variable v_doppelt_bearbnr
				--werden die Bearbeitungsnummer der gefundenen Datens�tze geschrieben


				if p_schadensnr != '0' then

				     for vers_rec in(
                          SELECT a.bearbeitungsnr FROM
            	  		  zahlungen a,
            	  		  vers_new b
            	  		  WHERE
            	  		  a.bearbeitungsnr != p_bearbeitungsnr
    	  		  		  AND
				  		  a.zuvsid = b.vs_id(+)
    	  		  		  AND b.schadensnr = p_schadensnr)
				     loop
                  	      v_doppelt_bearbnr := to_char(v_doppelt_bearbnr||vers_rec.bearbeitungsnr||'\n');
                 	 end loop;
			    end if;

        		if v_doppelt_bearbnr is not null then								   -- es sind doppelte vorhanden
           		     v_doppelt_bearbnr := rtrim(v_doppelt_bearbnr,'\n');
		   	    raise doppelte_versnr;

		        else
                */
			         --Wenn keine doppelten Vers-Nr. vorhanden sind, wird der Datensatz in die Tabelle Vers_New eingef�gt

				     select vs_id_vers_new.nextval into v_vs_id from dual;

				     INSERT INTO vers_new(vs_id,bearbeitungsnr,schadensnr,art,forderung,standid)
          		     VALUES(v_vs_id,p_bearbeitungsnr,p_schadensnr,p_art,0,p_standid);

                /*
                end if;
                */

           END IF;

           INSERT INTO zahlungen(zkey,bearbeitungsnr,bid,betrag,verwendungszweck,datum,zuvsid,zusatzinfo,z_user,z_userdat)
           VALUES(zkey_zahlungen.nextval,p_bearbeitungsnr,p_bid,p_betrag,p_verwendungszweck,v_datum,v_vs_id,p_zusatzinfo,p_user,p_userdat);
           COMMIT;

           v_meldung := 'Datensatz neu angelegt!';

   		   return v_meldung;

     EXCEPTION
           /*
           WHEN DOPPELTE_VERSNR THEN

	            v_meldung := 'Die Vers-Nr. '||p_schadensnr||' besteht bereits ';

	            if length(v_doppelt_bearbnr)>9 then
	   	             v_meldung := v_meldung||'bei den Datens�tzen mit folgenden Schadens-Nummern:<br>'||v_doppelt_bearbnr;
	            else
		             v_meldung := v_meldung||'beim Datensatz mit folgender Schadens-Nummer:<br>'|| v_doppelt_bearbnr;
	            end if;

	            v_meldung := v_meldung||'<br>Der Datensatz konnte nicht angelegt werden!';

	            return v_meldung;
           */
           WHEN OTHERS THEN

		        v_meldung := 'Fehler!<br>'||SQLERRM||'<br>Der Datensatz konnte nicht angelegt werden!';
		        return v_meldung;

	 END zahlungen_vers;


end zahl_vers;