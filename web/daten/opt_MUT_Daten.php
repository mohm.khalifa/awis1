<?php
//**************************************************************
// Liefert die Benutzer f�r das Personaleinsatzpl�ne Formular
// je nach ausgew�hltem PersEinsatz Bereich
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['MBK_KEY']))
{
	die('##PARAM##');
}
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT MUT_KEY AS KEY, MUT_UNTERNEHMENSTYP AS ANZEIGE ';
$SQL .= ' FROM MITBEWERBUTYPENTYPEN';
$SQL .= ' WHERE MUT_MBK_KEY='.$DB->FeldInhaltFormat('N0',$_GET['MBK_KEY'],false);
$SQL .= ' AND (MUT_STATUS = \'A\' OR MUT_KEY = 0'.$DB->FeldInhaltFormat('N0',$_GET['WERT'],false).')';
$SQL .= ' ORDER BY MUT_SORTIERUNG,MUT_UNTERNEHMENSTYP';
$rsPEB = $DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsPEB->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsPEB->FeldInhalt('KEY').'">'.$rsPEB->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsPEB->FeldInhalt('KEY').'#+#'.$rsPEB->FeldInhalt('ANZEIGE').'';
	}

	$rsPEB->DSWeiter();
}
?>