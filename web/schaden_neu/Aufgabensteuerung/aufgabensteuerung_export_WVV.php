<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
    
	$AWISBenutzer = awisBenutzer::Init('');
    $Form = new awisFormular();
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
    $DBSCHAD->Oeffnen();
    
	
	$Recht40016 = $AWISBenutzer->HatDasRecht(40016);
	
	$User = '';
	
	
	
	
	if ($_POST['txtSBV_WV_ZUVERTRETEN'] != '::bitte w�hlen::')
	{
		$User = $_POST['txtSBV_WV_ZUVERTRETEN'];
	}
	else
	{

			$User = '0';
	
	}
	
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$ExportFormat = 1;
	$Anzahl = 0;
  
	$DateiName = 'Wiedervorlagenverteilung';
	  
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	 
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
	        break;
	    case 2:                 // Excel 2007
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
	        break;
	}
	 
	$XLSXObj = new PHPExcel();
	$XLSXObj->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$XLSXObj->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToPage(true);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	$XLSXObj->getProperties()->setCreator(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setTitle(utf8_encode('Wiedervorlageverteilung'));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode('Wiedervorlageverteilung'));
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode('WVVerteilung'));
	
	
	$SQLWV ='SELECT';
	$SQLWV .='   WV.WV_ID WV_ID,';
	$SQLWV .='   WV.WV_DATUM WV_DATUM,';
	$SQLWV .='   WV.WV_GRUND WV_GRUND,';
	$SQLWV .='   SB.ID ID,';
	$SQLWV .='   SB.SBNAME,';
	$SQLWV .='   SB.SBVORNAME,';
	$SQLWV .='   SCHAD.ART ART,';
	$SQLWV .='   SCHAD.BEARBNRNEU BEARBNRNEU,';
	$SQLWV .='   SCHAD.BEARBEITUNGSNR BEARBEITUNGSNR,';
	$SQLWV .='   SCHAD.BEARBEITUNGSNRFIL BEARBEITUNGSNRFIL,';
	$SQLWV .='   SCHAD.SCHADENSJAHR SCHADENSJAHR,';
	$SQLWV .='   SCHAD.FILNR FILNR,';
	$SQLWV .='   SCHAD.LFDNR LFDNR,';
	$SQLWV .='   SCHAD.AUFTRAGSDATUM AUFTRAGSDATUM,';
	$SQLWV .='   SCHAD.WANR WANR,';
	$SQLWV .='   SCHAD.BONNR BONNR,';
	$SQLWV .='   SCHAD.KUNDENNAME KUNDENNAME,';
	$SQLWV .='   SCHAD.VORNAME VORNAME,';
	$SQLWV .='   SCHAD.TYP TYP,';
	$SQLWV .='   SCHAD.AUFTRAGSART_ATU AUFTRAGSART_ATU,';
	$SQLWV .='   SCHAD.AUFTRAGSART_ATU_NEU AUFTRAGSART_ATU_NEU,';
	$SQLWV .='   SCHAD.EINGABEAM EINGABEAM,';
	$SQLWV .='   SCHAD.KENNUNG KENNUNG,';
	$SQLWV .='   SCHAD.BEZEICHNUNG BEZEICHNUNG,';
	$SQLWV .='   SCHAD.ANWALTSSACHE ANWALTSSACHE,';
	$SQLWV .='   SCHAD.STAND STAND,';
	$SQLWV .='   SCHAD.BID BID,';
	$SQLWV .='   SCHAD.VORGANGSNR VORGANGSNR,';
	$SQLWV .='   SCHAD.ANTRAGART ANTRAGART,';
	$SQLWV .='   SCHAD.EINGABEDURCH EINGABEDURCH,';
	$SQLWV .='   SCHAD.SCHADENSGRUND SCHADENSGRUND,';
	$SQLWV .='   SCHAD.GROSSKDNR GROSSKDNR,';
	$SQLWV .='   SCHAD.GEWICHTUNG GEWICHTUNG,';
	$SQLWV .='   SCHAD.ZUSATZINFO ZUSATZINFO,';
	$SQLWV .='   SCHAD.BEMINTERN BEMINTERN,';
	$SQLWV .='   AA.WERT';
	$SQLWV .=' FROM';
	$SQLWV .='   WIEDERVORLAGEN_NEW WV';
	$SQLWV .=' LEFT JOIN SACHBEARBEITER SB';
	$SQLWV .=' ON';
	$SQLWV .='   WV.WV_AN = SB.SBNAME';
	$SQLWV .=' INNER JOIN SCHAEDEN_NEW SCHAD';
	$SQLWV .=' ON';
	$SQLWV .='   SCHAD.BEARBEITUNGSNR = WV.BEARBEITUNGSNR';
	$SQLWV .=' LEFT JOIN ANTRAGART AA';
	$SQLWV .=' ON';
	$SQLWV .='   SCHAD.ANTRAGART = AA.ID';
	if($User != '')
	{
		$SQLWV .=' WHERE';
		$SQLWV .='   SB.ID = '.$DB->WertSetzen('SBV', 'N0', $User);
	}
	
	$SQLWV .='  ORDER BY EINGABEAM';
	
	$rsVorgaenge = $DBSCHAD->RecordSetOeffnen($SQLWV,$DB->Bindevariablen('SBV',true));
	
	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	
	
	
	//�berschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('SBNAME'). ' '.$rsVorgaenge->FeldInhalt('SBVORNAME'))));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	

	

	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y H:i:s',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	
	$ZeilenNr++;
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Bearbeitungs-Nr.');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Eing. am');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Antragsart');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Fil-Nr');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Kundenname');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WV-Datum');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;

	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WV-Grund');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	
	
	while(!$rsVorgaenge->EOF()) 
	{    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('BEARBEITUNGSNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	    
	    
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	   
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('WERT'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr++;
	     
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('N0',$rsVorgaenge->FeldInhalt('FILNR'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
	    
	    $SpaltenNr++;
	     
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr++;
	     
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('D',$rsVorgaenge->FeldInhalt('WV_DATUM'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	    
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('WV_GRUND'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    
	    $SpaltenNr = 0;
	    $ZeilenNr++;
	    
        $rsVorgaenge->DSWeiter();
	}
	
	
	for($S='A';$S<='G';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	 
	
	
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="WVVerteilung.xls"');
	        $DateiEndung = '.xls';
	        break;
	    case 2:                 // Excel 2007
	         
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="WVVerteilung.xlsx"');
	        $DateiEndung = '.xlsx';
	        break;
	}
	 
	

	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
	        break;
	    case 2:                 // Excel 2007
	        $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
	        break;
	}
	//var_dump($Lagerkennung);
	//$objWriter->save('/daten/daten/pccommon/Gebhardt/' . $DateiName . '.xls');
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
	$Anzahl++;
	
		
}
catch(exception $ex)
{
	$Text = $ex->getMessage();
	
	
	$Absender = 'awisstag@de.atu.eu';
	$Betreff = "OK Personaleinsaetze-Export-Excel " . date('ymd');
	$Text = $Anzahl . ' Daten exportiert  >>/var/log/awis/Personaleinsaetze.log';
	$Empfaenger = 'shuttle@de.atu.eu';
	$Mail->AnhaengeLoeschen();
	$Mail->LoescheAdressListe();
	$Mail->DebugLevel(0);
	$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
	$Mail->Absender($Absender);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
	$Mail->SetzeBezug('TES',0); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
	$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
	$Mail->MailSenden();
}




?>