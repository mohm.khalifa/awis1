<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'crm_funktionen.inc';

    global $AWISCursorPosition;
    global $CRM;

    try {
    $CRM = new crm_funktionen();

    echo "<link rel=stylesheet type=text/css href='" . $CRM->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $CRM->AWISSprachKonserven['TITEL']['tit_CRM'] . '</title>';
    ?>
</head>
<body>

<?php
include("awisHeader3.inc");    // Kopfzeile

$CRM->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(68000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$CRM->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($CRM->Form instanceof awisFormular) {
        $CRM->Form->DebugAusgabe(1, $ex->getSQL());
        $CRM->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $CRM->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($CRM->Form instanceof awisFormular) {
        $CRM->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>