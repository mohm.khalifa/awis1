<?php


class awisDistributionSalesForce
{
    /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * Tabellenkuerzel der Zuordnung
     *
     * @var string
     */
    private $_XTN_Kuerzel;

    /**
     * Key der Zuordnung
     *
     * @var integer
     */
    private $_XXX_KEY;

    function __construct($DB,$XTN_Kuerzel, $XXX_KEY)
    {
        $this->_DB = $DB;
        $this->_XTN_Kuerzel = $XTN_Kuerzel;
        $this->_XXX_KEY = $XXX_KEY;
    }

    public function getSalesForce($ValidDate,$FER_KEYs = array())
    {
        $SalesForces = array();


        $SQL  ='select a.*, B.FER_BEZEICHNUNG,c.KON_NAME1, c.KON_NAME2, c.KON_PER_NR, e.XBL_LOGIN, XBN_KEY,';
        $SQL .=' (select KKO_WERT AS EMAIL_ADDRESS from KONTAKTEKOMMUNIKATION where KKO_KOT_KEY = 7 and KKO_KON_KEY = c.KON_KEY and ROWNUM = 1) as EMAIL_ADDRESS ';
        $SQL .=' from FILIALEBENENROLLENZUORDNUNGEN a';
        $SQL .=' inner join FILIALEBENENROLLEN B';
        $SQL .=' on a.FRZ_FER_KEY = B.FER_KEY ';
        $SQL .=' inner join KONTAKTE c';
        $SQL .=' on c.KON_KEY = a.FRZ_KON_KEY and c.KON_STATUS <> \'I\' ';
        $SQL .=' inner join BENUTZER d ';
        $SQL .=' on d.XBN_KON_KEY = c.KON_KEY';
        $SQL .=' inner join BENUTZERLOGINS e';
        $SQL .=' on e.XBL_XBN_KEY = d.XBN_KEY';

        if($this->_XTN_Kuerzel == 'FRZ'){
            $SQL .= ' WHERE FRZ_KEY = ' .$this->_DB->WertSetzen('FRZ','Z',$this->_XXX_KEY);
        }else{
            $SQL .= ' WHERE FRZ_XXX_KEY = ' . $this->_DB->WertSetzen('FRZ','Z',$this->_XXX_KEY);
            $SQL .= ' AND FRZ_XTN_KUERZEL = ' . $this->_DB->WertSetzen('FRZ','T',$this->_XTN_Kuerzel);
        }

        if($ValidDate!=''){
            $SQL .=' and FRZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FRZ','D',$ValidDate);
            $SQL .=' and FRZ_GUELTIGBIS  >= ' . $this->_DB->WertSetzen('FRZ','D',$ValidDate);
        }else{
            $SQL .=' and FRZ_GUELTIGAB <= sysdate';
            $SQL .=' and FRZ_GUELTIGBIS  >= sysdate';
        }

        if(count($FER_KEYs)){
            $FER_KEYs = implode(',',$FER_KEYs);
            $SQL .= ' AND FER_KEY in ('.$FER_KEYs.')';
        }

        $rsFRZ = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FRZ'));

        file_put_contents('debug.log',$this->_DB->LetzterSQL(),FILE_APPEND);


        while (!$rsFRZ->EOF()) {
            $SalesForce = new stdClass();
            $SalesForce->key = $rsFRZ->FeldInhalt('XBN_KEY');
            $SalesForce->text = $rsFRZ->FeldInhalt('KON_NAME1') .', '. $rsFRZ->FeldInhalt('KON_NAME2') . '('.$rsFRZ->FeldInhalt('FER_BEZEICHNUNG').')';
            $SalesForce->RoleKey = $rsFRZ->FeldInhalt('FRZ_FER_KEY');
            $SalesForce->FRZ_KEY = $rsFRZ->FeldInhalt('FRZ_KEY');
            $SalesForce->Role = $rsFRZ->FeldInhalt('FER_BEZEICHNUNG');
            $NamePrefix = '';
            if($rsFRZ->FeldInhalt('FRZ_FER_KEY')==3 and substr($rsFRZ->FeldInhalt('XBL_LOGIN'),0,4)!= 'FIL-'){
                $NamePrefix = 'FIL-';
            }
            $SalesForce->FirstName =  $NamePrefix .$rsFRZ->FeldInhalt('KON_NAME1');
            $SalesForce->LastName = $rsFRZ->FeldInhalt('KON_NAME2');
            $SalesForce->EmployeeId =  $rsFRZ->FeldInhalt('KON_PER_NR');
            $SalesForce->LoginName = $rsFRZ->FeldInhalt('XBL_LOGIN');
            $SalesForce->ValidFrom = $rsFRZ->FeldInhalt('FRZ_GUELTIGAB');
            $SalesForce->ValidTo = $rsFRZ->FeldInhalt('FRZ_GUELTIGBIS');
            $SalesForce->Notice = $rsFRZ->FeldInhalt('FRZ_BEMERKUNG');
            $SalesForce->EmailAddress = $rsFRZ->FeldInhalt('EMAIL_ADDRESS');
            $SalesForces[] = $SalesForce;
            $rsFRZ->DSWeiter();
        }

        return $SalesForces;

    }

}