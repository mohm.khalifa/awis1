<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_QMG'));
	
	if($Recht34000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Form->Formular_Start();
	
	$FeldBreiten = array();
	$FeldBreiten['Lang'] = 120;
	$FeldBreiten['Aktiv'] = 120;
	$Param['BLOCK']=1;
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'qmg_gueltigab';
	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	

	$SQL  = 'select qmg_key,qmg_qmk_key,qmg_gueltigab,qmg_gueltigbis,qmg_user,qmg_userdat, ';
	$SQL .='row_number() OVER (';
	// sonst pauschal nach Pr�fdatum
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= ' Select * from QMKRITERIENGUELTIG ';
	$SQL .= ' where QMG_QMK_KEY =\''.$_GET['QMK_KEY'].'\'';
	if(isset($_GET['QMG_KEY']))
	{
		$SQL .= ' and qmg_key='.$_GET['QMG_KEY'];
	}
	$SQL .= ')';
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	
	
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	
	
	$rsQMK = $DB->RecordSetOeffnen($SQL);
	
	if(!isset($_GET['QMG_KEY']))
	{
		$Form->ZeileStart('width:100%;');
		$Icons = array();
		$Icons[] = array('new','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Unterseite='.$_GET['Unterseite'].'&QMK_KEY='.$_GET['QMK_KEY'].'&QMG_KEY=-1','g');
		$Form->Erstelle_ListeIcons($Icons,38,0);
		
		$Link= '';
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Unterseite='.$_GET['Unterseite'].'&QMK_KEY='.$_GET['QMK_KEY'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMG_GUELTIGAB'], $FeldBreiten['Lang'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Unterseite='.$_GET['Unterseite'].'&QMK_KEY='.$_GET['QMK_KEY'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMG_GUELTIGBIS'], $FeldBreiten['Aktiv'], '', $Link);
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsQMK->EOF())
		{
		
			$Form->ZeileStart('width:100%');
			$Icons = array();
			//$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FFA_KEY='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Icons[] = array('edit','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&QMK_KEY='.$_GET['QMK_KEY'].(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'').'&Unterseite=Gueltigkeiten&QMG_KEY='.$rsQMK->FeldInhalt('QMG_KEY'));
			$Icons[] = array('delete','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Unterseite=Gueltigkeiten&Del='.$rsQMK->FeldInhalt('QMG_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$Style = '';
			if(strtotime($rsQMK->FeldInhalt('QMG_GUELTIGAB')) > strtotime(date('d.m.Y'),time()))
			{
				$Style='font-style:italic;color:#0266C8';
			}
			if(strtotime($rsQMK->FeldInhalt('QMG_GUELTIGBIS')) < strtotime(date('d.m.Y',time())))
			{
				$Style='font-style:italic;color:#FF2020';
			}
			$TTT =  $rsQMK->FeldInhalt('QMG_GUELTIGAB');
			$Form->Erstelle_ListenFeld('QMG_GUELTIGAB',$Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGAB')), 0, $FeldBreiten['Lang'], false, ($DS%2),$Style,'', 'T', 'L', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMG_GUELTIGBIS');
			$Form->Erstelle_ListenFeld('QMG_GUELTIGBIS',$Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGBIS')), 0, $FeldBreiten['Aktiv'], false, ($DS%2),$Style,'', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsQMK->DSWeiter();
		}
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Unterseite='.$_GET['Unterseite'].'&QMK_KEY='.$_GET['QMK_KEY'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else 
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Kriterien&Unterseite=Gueltigkeiten&QMK_KEY=".$_GET['QMK_KEY']. " accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMG_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMG_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_GUELTIGAB'] . ':',140);
		if (($rsQMK->AnzahlDatensaetze() == 0))
		{
			$Form->Erstelle_TextFeld('QMG_GUELTIGAB',!isset($_POST['cmdSpeichern_x']) ? date('d.m.Y',time()) : $_POST['txtQMG_GUELTIGAB'], 10,170,true, '','', '','D', 'L');
		}
		else
		{
			$Form->Erstelle_TextFeld('QMG_GUELTIGAB',!isset($_POST['cmdSpeichern_x']) ? $Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGAB')) : $_POST['txtQMG_GUELTIGAB'], 10,170,true, '','', '','D', 'L');
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_GUELTIGBIS'] . ':',140);
		if (($rsQMK->AnzahlDatensaetze() == 0))
		{
			$Form->Erstelle_TextFeld('QMG_GUELTIGBIS',!isset($_POST['cmdSpeichern_x']) ? $Form->Format('D','31.12.2030') : $_POST['txtQMG_GUELTIGBIS'], 10,170,true, '','', '','D', 'L');
		}
		else
		{
			$Form->Erstelle_TextFeld('QMG_GUELTIGBIS',!isset($_POST['cmdSpeichern_x']) ? $Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGBIS')) : $_POST['txtQMG_GUELTIGBIS'], 10,170,true, '','', '','D', 'L');
		}
		$Form->ZeileEnde();
		
		if(isset($_GET['QMG_KEY']))
		{
			$Form->Erstelle_HiddenFeld('QMG_KEY',$_GET['QMG_KEY']);
		}
	}
	
	$AWISBenutzer->ParameterSchreiben('Formular_QMG',serialize($Param));
	$Form->Formular_Ende();
	
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>