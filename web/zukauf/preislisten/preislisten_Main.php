<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'preislisten_funktionen.inc';

    global $AWISCursorPosition;
    global $ZPL;

    try {
    $ZPL = new preislisten_funktionen();

    echo "<link rel=stylesheet type=text/css href='" . $ZPL->AWISBenutzer->CSSDatei() . "'>";
    echo '<script src="/js/jquery-ui.js" type="text/javascript"></script>';
    echo '<script src="/jquery_ie11.js" type="text/javascript"></script>';

    echo '<title>Preislisten</title>';
    ?>
    <script src="/jquery_ie11.js"></script>
    <script src="/jquery.scrollUp.min.js"></script>
    <script src="/popup.js"></script>
    <script src="/formularBereich.js"></script>
    <script src="/jquery.tooltipster.js"></script>
    <script src="/jquery-ui.js"></script>
    <link rel=stylesheet type=text/css href="/css/jquery-ui.css">
    <link rel=stylesheet type=text/css href="/css/tooltipster.css">
    <link rel=stylesheet type=text/css href="/css/tooltipster-punk.css">
    <link rel=stylesheet type=text/css href="/css/popup.css">
</head>
<body>
<?php
include("awisHeader.inc");    // Kopfzeile

$ZPL->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(10070);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$ZPL->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($ZPL->Form instanceof awisFormular) {
        $ZPL->Form->DebugAusgabe(1, $ex->getSQL());
        $ZPL->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $ZPL->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($ZPL->Form instanceof awisFormular) {
        $ZPL->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>