<?php
global $BMV;
try {
    $BMV->Form->SchreibeHTMLCode('<form name="frmSicherheitsanlagenSuche" action="./sicherheitsanlagen_Main.php?cmdAktion=Details" method="POST"  >');

    if (!isset($BMV->Param['SPEICHERN'])) {
        $BMV->Param['SPEICHERN'] = 'off';
    }

    $LabelBreite = 200;

    $BMV->Form->Formular_Start();

    $BMV->Form->ZeileStart();
    $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMK']['BMK_KATEGORIE'] . ':', $LabelBreite);
    $SQL = "select BMK_KEY, BMK_KATEGORIE from BMAKATEGORIE order by BMK_KATEGORIE";
    $BMV->Form->Erstelle_SelectFeld('*BMK_KATEGORIE', ($BMV->Param['SPEICHERN'] == 'on'?$BMV->Param['BMK_KATEGORIE']:''), 400, true, $SQL,
        $BMV->AWISSprachKonserven['Liste']['lst_ALLE_0']);
    $BMV->Form->ZeileEnde();

    $BMV->Form->ZeileStart();
    $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_FIL_ID'] . ':', $LabelBreite);
    // alle akt. gueltigen Filialen (!Fil-Ebenen!) anzeigen, und alle zu denen ein Vertrag gepflegt ist
    $SQL = 'select FIL_ID, FIL_ID || \' - \' || FIL_BEZ ';
    $SQL .= 'from V_FILIALEN_AKTUELL ';
    $SQL .= 'union ';
    $SQL .= 'select FIL_ID, FIL_ID || \' - \' || FIL_BEZ ';
    $SQL .= 'from BMAVERTRAEGE inner join filialen ';
    $SQL .= 'on bmv_fil_id = fil_id ';
    $SQL .= 'order by FIL_ID';
    $BMV->Form->Erstelle_SelectFeld('*BMV_FIL_ID', ($BMV->Param['SPEICHERN'] == 'on'?$BMV->Param['BMK_FIL_ID']:''), 400, true, $SQL,
        $BMV->AWISSprachKonserven['Liste']['lst_ALLE_0']);
    $BMV->Form->ZeileEnde();

    $BMV->Form->ZeileStart();
    $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_TNNL'] . ':', $LabelBreite);
    $SQL = "select upper(BMV_TNNL) as BMV_TNNL, upper(BMV_TNNL) from bmavertraege group by upper(BMV_TNNL)";
    $BMV->Form->Erstelle_SelectFeld('*BMV_TNNL', ($BMV->Param['SPEICHERN'] == 'on'?$BMV->Param['BMV_TNNL']:''), 400, true, $SQL,
        $BMV->AWISSprachKonserven['Liste']['lst_ALLE_0']);
    $BMV->Form->ZeileEnde();

    $BMV->Form->ZeileStart();
    $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_VERTRAGSNR'] . ':', $LabelBreite);
    $BMV->Form->Erstelle_TextFeld('*BMV_VERTRAGSNR', ($BMV->Param['SPEICHERN'] == 'on'?$BMV->Param['BMV_VERTRAGSNR']:''), 25, 200, true);
    $BMV->Form->ZeileEnde();

    // Auswahl kann gespeichert werden
    $BMV->Form->ZeileStart();
    $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', $LabelBreite);
    $BMV->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($BMV->Param['SPEICHERN'] == 'on'?'on':''), 20, true, 'on');
    $BMV->Form->ZeileEnde();

    $BMV->Form->Formular_Ende();

    $BMV->Form->SchaltflaechenStart();
    $BMV->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $BMV->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $BMV->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $BMV->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if (($BMV->Recht50000 & 8) == 8) {
        $BMV->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $BMV->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    $BMV->Form->SchaltflaechenEnde();

    $BMV->Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($BMV->Form instanceof awisFormular) {
        $BMV->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>