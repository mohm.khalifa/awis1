<html>
<head>

<title>Awis 1.0 - ATU webbasierendes Informationssystem - Benutzerverwaltung</title>
<meta http-equiv="expires" content="01 01 2000 00:00:00 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<body>
<?php

global $erg;

$con = awisLogon();

if(awisBenutzerRecht($con,5)==0)
{
	awisEreignis(3,1000,'Kennwort �ndern',$_SERVER['PHP_AUTH_USER'],'','','');
	die("Keine ausreichenden Rechte um Ihr Kennwort zu �ndern!");
}

//*****************************************************
// Kennwort speichern
//*****************************************************

if(isset($_POST["txtKennwort1"]) AND $_POST["txtKennwort1"]!="")
{
	if($_POST["txtKennwort1"] != $_POST["txtKennwort2"])
	{
		print "Die Kennw�rter stimmen nicht �berein!"	;
		print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/KennwortAendern.php'>";
	}
	else
	{
		// Kennwort �berpr�fen
		$Fehler = False;
		if(strlen($_POST["txtKennwort1"])<8)
		{
			print "<br>Das von Ihnen gew�hlte Kennwort ist k�rzer als 8 Zeichen!";
			$Fehler = True;
		}
		$Pruef = 0;
		$AnzDollar=0;
		$AnzUnd=0;
		$AnzDoppel=0;
		
		for($i=0;$i<strlen($_POST["txtKennwort1"]);$i++)
		{
			$Zeichen = ord($_POST["txtKennwort1"][$i]);
			
			if($Zeichen == 36)			// Dollarzeichen
			{
				$AnzDollar++;
			}
			if($Zeichen == 38)			// &-Zeichen
			{
				$AnzUnd++;
			}
			if($Zeichen == 58)			// :-Zeichen
			{
				$AnzDoppel++;
			}
			if($Zeichen >= 48 AND $Zeichen <= 57)			// Zahlen
			{
				$Pruef = $Pruef | 1;
			}
			elseif($Zeichen >= 65 AND $Zeichen <= 90)			// Gro�buchstaben
			{
				$Pruef = $Pruef | 2;
			}
			elseif($Zeichen >= 97 AND $Zeichen <= 122)			// Kleinbuchstaben
			{
				$Pruef = $Pruef | 4;
			}
			else
			{
				$Pruef = $Pruef | 8;
			}			
		}
		if($Pruef != 15)
		{
			print "<br>Das von Ihnen gew�hlte Kennwort entspricht nicht den ATU Richtlinien!<br>";
			if(($Pruef & 1)==0)
			{
				print "<br>&nbsp;- Es ist keine Zahl enthalten.";
			}
			if(($Pruef & 2)==0)
			{
				print "<br>&nbsp;- Es ist kein Gro�buchstabe enthalten.";
			}
			if(($Pruef & 4)==0)
			{
				print "<br>&nbsp;- Es ist kein Kleinbuchstabe enthalten.";
			}
			if(($Pruef & 8)==0)
			{
				print "<br>&nbsp;- Es ist kein Sonderzeichen enthalten.";
			}
			
			$Fehler = True;
		}

		If($AnzDollar>0)		
		{
			echo '<br>&nbsp;- Es ist ein ung�ltiges Zeichen enthalten ($).';
			$Fehler = True;
		}
		If($AnzUnd>0)		
		{
			echo '<br>&nbsp;- Es ist ein ung�ltiges Zeichen enthalten (&).';
			$Fehler = True;
		}
		If($AnzDoppel>0)		
		{
			echo '<br>&nbsp;- Es ist ein ung�ltiges Zeichen enthalten (:).';
			$Fehler = True;
		}
		
		if($Fehler==True)
		{
			print "<br><br>Bitte versuchen Sie es erneut.<br><br>";
			print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/KennwortAendern.php'>";
		}
		else	// Speichern
		{
			system("/daten/zugang/newuser.sh " . strtolower($_SERVER['PHP_AUTH_USER']) . " \"" . $_POST["txtKennwort1"] . "\"",$erg);

			sleep(2);
			if($erg != 0)
			{
				print "<br><br>Fehler beim Kennwort �ndern. Fehlercode: $erg<br><br>";
			}
			else
			{
				awis_BenutzerParameterSpeichern($con,"Passwortaendern",$_SERVER['PHP_AUTH_USER'],0);
				$dat = getdate();
				awis_BenutzerParameterSpeichern($con,"PasswortaendernDatum",$_SERVER['PHP_AUTH_USER'], mktime(0,0,0,$dat['mon'],$dat['mday']+90,$dat['year']));
				print "<br>Ihr Kennwort wurde ge�ndert.<br>";
			}		

			print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php'>";
		}
	}	
}
else
{
	print "<h2>Kennwort �ndern</h2>";

	print "Bitte die Sonderzeichen <b>&</b>,<b>$</b>,<b>:</b> und <b>( )</b> nicht im Kennwort verwenden!<br>Das Sonderzeichen nicht an erster Stelle eingeben!<br><br>";
	
	print "<form name=frmKennwort method=post action=/KennwortAendern.php><table>";

	print "\n<tr><td>Neues Kennwort:</td><td><input name=txtKennwort1 type=password></td></tr>";
	print "\n<tr><td>Best�tigen:</td><td><input name=txtKennwort2 type=password></td></tr>";

	print "\n<tr><td><input type=image alt=Speichern src=/bilder/diskette.png name=cmdSpeichern onclick='document.frmKennwort.submit();'>";
	
	print "\n</td></tr></table></form>";
	
	print "<span class=HinweisText>Hinweis: Nach der Kennwort�nderung werden Sie neu angemeldet. Bitte geben Sie in der Dialogbox Ihr NEUES Kennwort an!</span><br>";

	print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php'>";

	print "<br><br>";
	print "<span class=HinweisText>Bitte beachten Sie die neuen <a href=/kennwortrichtlinien.pdf style=\"text-decoration:underline;\">Kennwortrichtlinien</a> vom 26.08.2003.</span>";

	echo "<hr><a href=\"javascript:window.external.AddFavorite('http://atuweb.awis.atu.de', 'AWIS')\" style=\"text-decoration:underline;\">AWIS</a> zu den Favoriten hinzuf�gen.<br>";
	if (strtoupper(substr($_SERVER['PHP_AUTH_USER'],0,2)) != 'CT') {
		echo "<a href=./KennwortAendern.php onClick=\"javascript:this.style.behavior='url(#default#homepage)';this.setHomePage('http://atuweb.awis.atu.de');\" style=\"text-decoration:underline;\">AWIS</a> als Startseite festlegen.";
	}
}

awisLogoff($con);
if($_SERVER['PHP_AUTH_USER']=='entwick')
{
 include("debug_info.php");
}

?>
</body>
</html>
