<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('LIK','%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Wort','Gesamt');
	$TextKonserven[]=array('Wort','Soll');
	$TextKonserven[]=array('Wort','Ist');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $AWIS_KEY1;
	
	$Recht3600 = $AWISBenutzer->HatDasRecht(3600);
	if($Recht3600==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

	if (isset($_GET['LKK_KEY'])) 
	{
		$AWIS_KEY1=intval($_GET['LKK_KEY']);		
	}	
	
	$Spalte = 10;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Differenzliste');
	
	$Zeile=_Erzeuge_Seite_Lieferschein();
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT *';
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .=' INNER JOIN Lieferkontrollen LIK on LIK_LKK_KEY = LKK_KEY';
	$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = LIK_AST_ATUNR';
	$SQL .=' WHERE LKK_KEY=0'.$AWIS_KEY1;	
	$SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsLIK = $DB->RecordSetOeffnen($SQL);
	$rsLIKZeilen = $rsLIK->AnzahlDatensaetze();
	
	if($rsLIKZeilen==0)		// Keine Daten
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}		
		
	for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-20)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Lieferschein();
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);
		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(15,6,$rsLIK->FeldInhalt('LIK_AST_ATUNR'),1,0,'L',0);					
		
		$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->cell(90,6,$rsLIK->FeldInhalt('AST_BEZEICHNUNGWW'),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->cell(22,6,$Form->Format('D', $rsLIK->FeldInhalt('LIK_DATUMSCAN')),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+127,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_MENGESOLL'),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+142,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_MENGEIST'),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+157,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_SOLLBESTAND'),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+172,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_ISTBESTAND'),1,0,'L',0);			
		
		$Zeile+=6;
	
		$rsLIK->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Lieferschein()
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;
	global $Form;	
	global $AWIS_KEY1;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	if ($Seite == 1)
	{
		// �berschrift		
		
		$SQL = 'SELECT DISTINCT LKK_DATUMKOMM, LKK_FIL_ID, LKK_LIEFERSCHEINNR';
		$SQL .=' FROM Lieferkontrollenkopf LKK';
		$SQL .=' INNER JOIN Lieferkontrollen LIK on LIK_LKK_KEY = LKK_KEY';
		$SQL .=' WHERE LKK_KEY=0'.$AWIS_KEY1;		
		$SQL .= ' ORDER BY LKK_DATUMKOMM';
		
		$rsLIKKopf = $DB->RecordSetOeffnen($SQL);		
		
		$Ausdruck->_pdf->SetFont('Arial','B',14);			
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);			
		$Ausdruck->_pdf->Cell(180,6,'Differenzliste von Filiale: '.$rsLIKKopf->FeldInhalt('LKK_FIL_ID'),0,0,'L',0);
		$Zeile+=15;			
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		
		$rsFilLager = $DB->RecordSetOeffnen('SELECT UPPER(FIL_LAGERKZ) AS LAGER FROM FILIALEN WHERE FIL_ID=0'.$DB->FeldInhaltFormat('Z',$rsLIKKopf->FeldInhalt('LKK_FIL_ID'),false));
		$FilLager = $rsFilLager->FeldInhalt('LAGER');
				
		//$Lager = $rsLIKKopf->FeldInhalt('LKK_QUELLE');		
		$Datumkomm = $rsLIKKopf->FeldInhalt('LKK_DATUMKOMM');		
		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['LIK']['LIK_LIEFERSCHEINNR'].':',0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$rsLIKKopf->FeldInhalt('LKK_LIEFERSCHEINNR'),0,0,'L',0);
		$Zeile+=6;								

		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['LIK']['LIK_DATUMKOMM'].':',0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$Form->Format('D', $Datumkomm),0,0,'L',0);
		$Zeile+=6;								
					
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['LIK']['LIK_QUELLE'].':',0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$FilLager,0,0,'L',0);
		$Zeile+=6;										
	}
	else 
	{
		$Zeile+=15;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(15,12,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);
	$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte,$Zeile+12);
			
	$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
	$Ausdruck->_pdf->Cell(90,12,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],0,0,'L',1);
	$Ausdruck->_pdf->line($Spalte+15,$Zeile,$Spalte+15,$Zeile+12);
	
	$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
	$Ausdruck->_pdf->cell(22,12,$AWISSprachKonserven['LIK']['LIK_DATUMSCAN'],0,0,'L',1);
	$Ausdruck->_pdf->line($Spalte+105,$Zeile,$Spalte+105,$Zeile+12);
			
	$Ausdruck->_pdf->setXY($Spalte+127,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Menge'],0,0,'L',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+142,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Menge'],0,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+157,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Gesamt'],0,0,'L',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+172,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Gesamt'],0,0,'L',1);
			
	$Zeile=$Zeile+6;
	$Ausdruck->_pdf->setXY($Spalte+127,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Soll'],0,0,'L',1);		
	$Ausdruck->_pdf->line($Spalte+127,$Zeile-6,$Spalte+127,$Zeile+6);
	
	$Ausdruck->_pdf->setXY($Spalte+142,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['LIK']['Scan'],0,0,'L',1);		
	$Ausdruck->_pdf->line($Spalte+142,$Zeile-6,$Spalte+142,$Zeile+6);
	
	$Ausdruck->_pdf->setXY($Spalte+157,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Soll'],0,0,'L',1);		
	$Ausdruck->_pdf->line($Spalte+157,$Zeile-6,$Spalte+157,$Zeile+6);
	
	$Ausdruck->_pdf->setXY($Spalte+172,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Ist'],0,0,'L',1);				
	$Ausdruck->_pdf->line($Spalte+172,$Zeile-6,$Spalte+172,$Zeile+6);
	$Ausdruck->_pdf->line($Spalte+187,$Zeile-6,$Spalte+187,$Zeile+6);
	$Ausdruck->_pdf->line($Spalte,$Zeile-6,$Spalte+187,$Zeile-6);
			
	$Zeile+=6;	
	
	return $Zeile;
}
?>

