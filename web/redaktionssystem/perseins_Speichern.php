<?php
global $awisDBFehler;

//var_dump($_POST);

/********************************************
* Speichern von Datens�tzen
********************************************/

if(isset($_POST['cmdSpeichern_x']))		
{
	if(!isset($_POST['txtMEI_KEY']))		// Neuer Datensatz
	{
		$SQL = 'INSERT INTO MitarbeiterEinsaetze(';
		$SQL .= 'MEI_MIT_KEY, MEI_TAG, MEI_VONXTZ_ID, MEI_BISXTZ_ID, MEI_MEA_KEY, MEI_FIL_ID, MEI_BEMERKUNG, MEI_USER, MEI_USERDAT)';
		$SQL .= 'VALUES (';
		
		$SQL .= $_POST['txtMEI_MIT_KEY'];
		$SQL .= ", TO_DATE('" . $_POST['txtMEI_TAG'] . "')";
		$SQL .= ", " . $_POST['txtMEI_VONXTZ_ID'];
		$SQL .= ", " . $_POST['txtMEI_BISXTZ_ID'];
		$SQL .= ", " . $_POST['txtMEI_MEA_KEY'];
		$SQL .= ", " . ($_POST['txtMEI_FIL_ID']==''?'NULL':"'" . $_POST['txtMEI_FIL_ID'] . "'");
		$SQL .= ", '" . $_POST['txtMEI_BEMERKUNG'] . "'";
		$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "'";
		$SQL .= ',SYSDATE)';
	
	//	var_dump($SQL);
	
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("perseins_Speichern.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
	else		// Datensatz �ndern
	{
		$SQL = '';
		$chkFehler = '';
		$rschkMEI = awisOpenRecordset($con, 'SELECT * FROM MitarbeiterEinsaetze WHERE MEI_KEY=' . $_POST['txtMEI_KEY']);

		// Datens�tze pr�fen		
		if($_POST['txtMEI_MIT_KEY'] != $_POST['txtMEI_MIT_KEY_old'])
		{
			if($_POST['txtMEI_MIT_KEY_old'] != $rschkMEI['MEI_MIT_KEY'][0])
			{
				$chkFehler .= 'Mitarbeiter(' . $_POST['txtMEI_MIT_KEY_old'] . '/' . $rschkMEI['MEI_MIT_KEY'][0] . ')';
			}
			else
			{
				$SQL .= ', MEI_MIT_KEY=' . $_POST['txtMEI_MIT_KEY'];
			}
		}
		if($_POST['txtMEI_TAG'] != $_POST['txtMEI_TAG_old'])
		{
			if($_POST['txtMEI_TAG_old'] != str_replace('-','.',$rschkMEI['MEI_TAG'][0]))
			{
				$chkFehler .= 'Datum';
			}
			else
			{
				$SQL .= ", MEI_TAG=TO_DATE('" . awis_format($_POST['txtMEI_TAG'],'OracleDatum') . "','DD.MM.RRRR')";
			}
		}
		if($_POST['txtMEI_VONXTZ_ID'] != $_POST['txtMEI_VONXTZ_ID_old'])
		{
			if($_POST['txtMEI_VONXTZ_ID_old'] != $rschkMEI['MEI_VONXTZ_ID'][0])
			{
				$chkFehler .= 'Zeit von(' . $_POST['txtMEI_VONXTZ_ID_old'] . '/' . $rschkMEI['MEI_VONXTZ_ID'][0] . ')';
			}
			else
			{
				$SQL .= ', MEI_VONXTZ_ID=' . $_POST['txtMEI_VONXTZ_ID'];
			}
		}
		if($_POST['txtMEI_BISXTZ_ID'] != $_POST['txtMEI_BISXTZ_ID_old'])
		{
			if($_POST['txtMEI_BISXTZ_ID_old'] != $rschkMEI['MEI_BISXTZ_ID'][0])
			{
				$chkFehler .= 'Zeit bis';
			}
			else
			{
				$SQL .= ', MEI_BISXTZ_ID=' . $_POST['txtMEI_BISXTZ_ID'];
			}
		}
		if($_POST['txtMEI_MEA_KEY'] != $_POST['txtMEI_MEA_KEY_old'])
		{
			if($_POST['txtMEI_MEA_KEY_old'] != $rschkMEI['MEI_MEA_KEY'][0])
			{
				$chkFehler .= 'Aktion';
			}
			else
			{
				$SQL .= ', MEI_MEA_KEY=' . $_POST['txtMEI_MEA_KEY'];
			}
		}
		if($_POST['txtMEI_FIL_ID'] != $_POST['txtMEI_FIL_ID_old'])
		{
			if($_POST['txtMEI_FIL_ID_old'] != $rschkMEI['MEI_FIL_ID'][0])
			{
				$chkFehler .= 'Filiale';
			}
			else
			{
				$SQL .= ", MEI_FIL_ID='" . $_POST['txtMEI_FIL_ID'] . "'";
			}
		}
		if($_POST['txtMEI_BEMERKUNG'] != $_POST['txtMEI_BEMERKUNG_old'])
		{
			if($_POST['txtMEI_BEMERKUNG_old'] != $rschkMEI['MEI_BEMERKUNG'][0])
			{
				$chkFehler .= 'Bemerkung';
			}
			else
			{
				$SQL .= ", MEI_BEMERKUNG='" . $_POST['txtMEI_BEMERKUNG'] . "'";
			}
		}

		If($chkFehler!='')
		{
			echo '<span class=Hinweistext>PROBLEM : ';
			echo '<br>Folgende Felder wurden in der Zwischenzeit von einem anderen Anwender ver�ndert!';
			echo '<br><br>' . $chkFehler . '<br>';
			echo '<br>Ge�ndert von: ' . $rschkMEI['MEI_USER'][0];
			echo '<br>Ge�ndert am: ' . $rschkMEI['MEI_USERDAT'][0];			
		}
		elseif($SQL!='')
		{
			$SQL = 'UPDATE MitarbeiterEinsaetze SET ' . substr($SQL, 1) . ' WHERE MEI_KEY=' . $_POST['txtMEI_KEY'];
			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Speichern.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}

		}
	}
}	// Ende Speichern
?>