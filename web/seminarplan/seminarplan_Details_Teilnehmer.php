<?php

require_once "awisSeminarplanEmailHotelZeitAenderung.php";
require_once "awisSeminarplanEmailHotelAbbestellt.php";
require_once "awisSeminarplanEmailTeilnehmerVorschlag.php";


global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SEP','%');	
	$TextKonserven[]=array('SET','%');	
	$TextKonserven[]=array('SEH','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Uploaddatei');
	$TextKonserven[]=array('Wort','Typ');
	$TextKonserven[]=array('DOC','%');
    $TextKonserven[]=array('Wort','DateiOeffnen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$AWISSprache = $AWISBenutzer->BenutzerSprache();

	if($AWISSprache=='')
	{
		$AWISSprache='DE';
	}

	$Recht4200 = $AWISBenutzer->HatDasRecht(4200);
	if($Recht4200==0)
	{
		$Form->Fehler_KeineRechte();
	}

	
	//********************************************************
	// Daten suchen
	//********************************************************	
	$SQL = 'SELECT distinct SeminarTeilnehmer.*, SEP_SCHULUNGSART, SEP_VON, SEP_SEMINARSTATUS';
	$SQL .= ', row_number() over (order by 1) AS ZeilenNr';
	$SQL .= ' FROM SeminarPlan';	
	$SQL .= ' LEFT OUTER JOIN SeminarTeilnehmer on SET_SEP_KEY = SEP_KEY';
	$SQL .= ' AND ((SEP_STATUS = 3 and (trunc(SEP_BIS) >= trunc(sysdate) - 70))';
	$SQL .= ' OR SEP_STATUS = 1)';
	$SQL .= ' AND UPPER(SET_STATUS) != UPPER(\'Abgelehnt\')';
	$SQL .= ' WHERE SEP_KEY=0'.$AWIS_KEY1;
	$SQL .= ' AND SET_STATUSTEILNEHMER != 3';

	if(isset($_GET['SET_KEY']))
	{
		$SQL .= ' AND SET_KEY = '.floatval($_GET['SET_KEY']);		
	}

    if(isset($_GET['HOTEL_SET_KEY'])){
        $AWIS_KEY2 = $_GET['HOTEL_SET_KEY'];
    }
	
	if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
	{
		$SQL .= ' AND SET_KEY=0'.intval($AWIS_KEY2);
	}		
		
	if(!isset($_GET['SETSort']))
	{
		$SQL .= ' ORDER BY SET_NACHNAME, SET_VORNAME';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['SETSort']);
	}
	
	$Form->DebugAusgabe(1,$SQL);
	$rsSET = $DB->RecordSetOeffnen($SQL);
	if(!isset($_GET['SET_KEY']) AND !isset($_GET['ANABMELDUNG']) AND !isset($_GET['SET_ABSAGEBEST']) AND !isset($_GET['SET_MAIL_KEY']) and !isset($_POST['txtSET_MAIL_KEY']) and !isset($_GET['HOTEL_SET_KEY']) )//$rsSET->AnzahlDatensaetze()>1)						// Liste anzeigen
	{

		$AktDatum = date("d.m.Y");		

		$Aendern = true;
		if ((strtotime($rsSET->FeldInhalt('SEP_VON')) - strtotime($AktDatum)) / 86400 < 7 )
		{
			$Aendern=false;
		}
				
		$Form->ZeileStart();

        $SQL = "select SEP_SCHULUNGSART from SEMINARPLAN where SEP_KEY = 0".$AWIS_KEY1;
        $rsSEP = $DB->RecordSetOeffnen($SQL);

        if ((($Recht4200&128)>0 AND $Aendern==true) or (($Recht4200&8192)!=0 and $rsSEP->FeldInhalt("SEP_SCHULUNGSART") == 2))
        {
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_KLICKENZUMANMELDEN'], 500,'','./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_KEY=0');
        } //SET_KLICKENZUMANMELDEN

		$Form->ZeileEnde();

		$Form->ZeileStart();

		$Icons = array();
		if ((($Recht4200&128)>0 AND $Aendern==true) or (($Recht4200&8192)!=0 and $rsSEP->FeldInhalt("SEP_SCHULUNGSART") == 2))
		{
			$Icons[] = array('new','./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_KEY=0');
		}
		$Form->Erstelle_ListeIcons($Icons,38,-2,$AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);

		if(($Recht4200&4096)>0)
		{
			$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
			$Link .= '&SETSort=SET_PER_NR'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_PER_NR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_PER_NR'],150,'',$Link);
		}
		$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';		
		$Link .= '&SETSort=SET_NACHNAME'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_NACHNAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_NACHNAME'],150,'',$Link);
		$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		$Link .= '&SETSort=SET_VORNAME'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_VORNAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_VORNAME'],150,'',$Link);		
		$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		$Link .= '&SETSort=SET_ANMELDETAG'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_ANMELDETAG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_ANMELDETAG'],150,'',$Link);				
		$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		$Link .= '&SETSort=SET_STATUS'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_STATUS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_STATUS'],160,'',$Link);				
		
		if ($rsSET->FeldInhalt('SEP_SCHULUNGSART') == '1')
		{
			$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
			$Link .= '&SETSort=SET_FIL_NR'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_FIL_NR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_FILIALE'],200,'',$Link);						
		}

		if ($rsSET->FeldInhalt('SEP_SEMINARSTATUS') == 'Anmeldephase')
		{
    		$Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
    		$Link .= '&SETSort=SET_ANMELDUNG'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_ANMELDUNG'))?'~':'');
    		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_A'],20,'',$Link, $AWISSprachKonserven['SET']['SET_ANMELDUNG']);	
		
    		
		}

		if(($Recht4200&32768)==32768)
		{
		    $Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		    $Link .= '&SETSort=SET_STATUSEMAIL'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_STATUSEMAIL'))?'~':'');
		    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_M'],20,'',$Link, $AWISSprachKonserven['SET']['SET_STATUSEMAIL']);
		}
		
       
		if(($Recht4200&16384 )==16384)
		{
		    $Link = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		    $Link .= '&SETSort=SET_ABSAGE'.((isset($_GET['SETSort']) AND ($_GET['SETSort']=='SET_ABSAGE'))?'~':'');
		    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_G'],20,'',$Link, $AWISSprachKonserven['SET']['SET_ABSAGE']);
		}

        if(($Recht4200&262144)==262144) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_H'],20,'font-weight: bold');
        }
		
		$Form->ZeileEnde();

		$SETZeile=0;
		while(!$rsSET->EOF())
		{
			$Form->ZeileStart();
			
			$Icons = array();			

			if((($Recht4200&256)>0 AND $rsSET->FeldInhalt('SET_STATUS')=='Warteliste' AND $Aendern==true) or (($Recht4200&2048)!=0))	// L�schen
			{
				$Icons[] = array('delete','./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&Del='.$rsSET->FeldInhalt('SET_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,18,($SETZeile%2));
			$Icons = array();

			if(($Recht4200&512)>0 AND $rsSET->FeldInhalt('SET_STATUS')=='Warteliste')	// Buchen
			{				
				$Icons[] = array('ok','./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&ok='.$rsSET->FeldInhalt('SET_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,18,($SETZeile%2));
			$Icons = array();

			//$Form->Erstelle_ListeIcons($Icons,57,($SETZeile%2));

			if(($Recht4200&4096)>0)
			{
				$Form->Erstelle_ListenFeld('SET_PER_NR',$rsSET->FeldInhalt('SET_PER_NR'),0,150,false,($SETZeile%2),'','','T');
			}
			$Form->Erstelle_ListenFeld('SET_NACHNAME',$rsSET->FeldInhalt('SET_NACHNAME'),0,150,false,($SETZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('SET_VORNAME',$rsSET->FeldInhalt('SET_VORNAME'),0,150,false,($SETZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('SET_ANMELDETAG',$rsSET->FeldInhalt('SET_ANMELDETAG'),0,150,false,($SETZeile%2),'','','D');
			$Form->Erstelle_ListenFeld('SET_STATUS',$rsSET->FeldInhalt('SET_STATUS'),0,160,false,($SETZeile%2),'','','T');
			
			if ($rsSET->FeldInhalt('SEP_SCHULUNGSART') == '1')
			{
				$Form->Erstelle_ListenFeld('SET_FIL_NR',$rsSET->FeldInhalt('SET_FIL_NR').($rsSET->FeldInhalt('SET_FIL_NR')!=''?' - ':'').$rsSET->FeldInhalt('SET_FILIALE'),0,200,false,($SETZeile%2),'','','T');			
			}
			

			if ($rsSET->FeldInhalt('SEP_SEMINARSTATUS') == 'Anmeldephase' and $rsSET->FeldInhalt('SET_STATUS') == 'Gebucht')
			{
			    //Anmeldung
			    $Icons = array();
			    $Icons[] = array('zoom', './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ANAB_KEY='.$rsSET->FeldInhalt('SET_KEY').'&ANABMELDUNG=1'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:''));
			    $TTT = $AWISSprachKonserven['SET']['SET_ANABMELDUNG'];
			    $Form->Erstelle_ListeIcons($Icons, 20, ($SETZeile%2),$TTT);
			    
				if(($Recht4200&32768)==32768)
				{
					//Mail Einladung
					$Icons = array();
					$Icons[] = array((($rsSET->FeldInhalt('SET_STATUSEMAIL')==1 || $rsSET->FeldInhalt('SET_STATUSEMAIL')==2 )? 'flagge_gruen': 'flagge_rot'), './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_MAIL_KEY='.$rsSET->FeldInhalt('SET_KEY').'&SET_STATUSEMAIL='.$rsSET->FeldInhalt('SET_STATUSEMAIL').(isset($_GET['Block'])?'&Block='.$_GET['Block']:''));
					$TTT = ($rsSET->FeldInhalt('SET_STATUSEMAIL')== 0)?$AWISSprachKonserven['SET']['SET_NOCHNICHTVERSENDET']: $AWISSprachKonserven['SET']['SET_VERSANDZEITPUNKT'] . $rsSET->FeldInhalt('SET_EMAILVERSANDZEITPUNKT') ;
					$Form->Erstelle_ListeIcons($Icons, 20, ($SETZeile%2),$TTT);
				}
			}else{
                $Form->Erstelle_ListenFeld('','','2',20,false,($SETZeile%2));
                if(($Recht4200&32768)==32768) {
                    $Form->Erstelle_ListeIcons($Icons, 20, ($SETZeile%2));
                }
            }

			//Mail Absage GBL
			if(($Recht4200&16384 )==16384)
			{
			    $Icons = array();
			    if($rsSET->FeldInhalt('SET_TEILNAHME')== 0 and $rsSET->FeldInhalt('SET_ABSAGEBEST') == -1)
                {         
                    $Icons[] = array('flagge_rot', './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ABSAGEBEST_KEY='.$rsSET->FeldInhalt('SET_KEY').'&SET_ABSAGEBEST=1'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:''));                   
                }
                elseif($rsSET->FeldInhalt('SET_TEILNAHME')== 0 and $rsSET->FeldInhalt('SET_ABSAGEBEST') <> -1)
                {
                    $Icons[] = array('flagge_gruen', './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ABSAGEBEST_KEY='.$rsSET->FeldInhalt('SET_KEY').'&SET_ABSAGEBEST=1'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:''));
                }
                $TTT = $AWISSprachKonserven['SET']['SET_ABSAGE'] ;
                $Form->Erstelle_ListeIcons($Icons, 20, ($SETZeile%2),$TTT);
			}
			//Hotelbuchung
            if(($Recht4200&262144)==262144) {
                $SQL = 'SELECT * from SEMINARHOTELZUORD WHERE SHZ_SET_KEY = ' .$DB->WertSetzen('SEH','Z',$rsSET->FeldInhalt('SET_KEY'));
                $AnzHotel = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('SEH'));

                $URL = './seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&HOTEL_SET_KEY='.$rsSET->FeldInhalt('SET_KEY');
                $Icons = array();

                if($AnzHotel >= 1){
                    $Icons[] = array('flagge_gruen',$URL);
                }elseif($rsSET->FeldInhalt('SET_HOTEL') == '1' and $AnzHotel == 0){
                    $Icons[] = array('flagge_orange',$URL);
                }elseif($rsSET->FeldInhalt('SET_HOTEL') == '0'){
                    $Icons[] = array('flagge_weiss',$URL);
                }elseif(($Recht4200&524288)==524288){
                    $Icons[] = array('flagge_rot',$URL);
                }else{
                    $Icons = array();
                }
                $TTT = $AWISSprachKonserven['SET']['ttt_H_DATENSAETZE'];
                $Form->Erstelle_ListeIcons($Icons,'20',($SETZeile%2),$TTT);
            }
			
			$Form->ZeileEnde();

			$rsSET->DSWeiter();
			$SETZeile++;
			
		}

		$Form->DebugAusgabe(1,$_GET);
		
	}
	elseif (isset($_GET['ANABMELDUNG']))
	{
       $SQLTeilnahmeAlt = 'Select SET_TEILNAHME, SET_HOTEL, SET_HOTEL_VON, SET_HOTEL_BIS from Seminarteilnehmer where SET_KEY = ' .  $_GET['SET_ANAB_KEY'];
	   $rsTeilnahmeAlt = $DB->RecordSetOeffnen($SQLTeilnahmeAlt); 
	   // var_dump($_POST);
	   if (isset($_POST['cmdSpeichern_x']))
	   {
	       $Debug = '';
	       $Fehler = false;
	       $SQLUpdateTeilnahme  ='update Seminarteilnehmer';
	       $SQLUpdateTeilnahme .=' set SET_USER = \'' . $AWISBenutzer->BenutzerName(1) . '\'';
	       $SQLUpdateTeilnahme .=' ,SET_USERDAT = sysdate';
	       //Bei Teilnahme
	       //var_dump ($_POST);
	       if (isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '1') //ja
	       {
	           $Debug .=  "<br>";
	           $Debug .=  "ich nehme teil";
	           $Debug .=  "<br>";
	           $SQLUpdateTeilnahme .=' ,SET_TEILNAHME = 1';
	           
	           if(isset($_POST['txtSetHotel']) and $_POST['txtSetHotel'] == '1') //Hotel ja
	           {
	               $Debug .=  "<br>";
	               $Debug .=  "ich will Hotel";
	               $Debug .=  "<br>";
	               $SQLUpdateTeilnahme .=' ,SET_HOTEL = 1';
	               $SQLUpdateTeilnahme .=' ,SET_HOTEL_VON = \'' . $_POST['txtSET_ANREISE'] . '\'';
	               $SQLUpdateTeilnahme .=' ,SET_HOTEL_BIS =\'' . $_POST['txtSET_ABREISE'] . '\'';

	           }
	           elseif (isset($_POST['txtSetHotel']) and $_POST['txtSetHotel'] == '0') //nein
	           {
	               $Debug .=  "<br>";
	               $Debug .=  "ich brauch kein Hotel";
	               $Debug .=  "<br>";
	               
	               //Update auf 0
	               $SQLUpdateTeilnahme .=' ,SET_HOTEL = 0';

	               if($rsTeilnahmeAlt->FeldInhalt("SET_HOTEL")=='1'){

                       $Debug .=  "<br>";
                       $Debug .=  "ich wollte aber mal ein Hotel";
                       $Debug .=  "<br>";
                       $SQLUpdateTeilnahme .= " ,SET_HOTEL_VON = NULL";
                       $SQLUpdateTeilnahme .= " ,SET_HOTEL_BIS = NULL";
	                   //Schreibt unten Mail an Academy
                   }

	           }
	           else
	           {
				   //Fehler anzeigen
				   $Form->ZeileStart();
				   $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_HOTEL']);
				   $Form->ZeileEnde();
	               $Fehler = true;
	           }
	            
	       }
	       elseif (isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '0')//nein
	       {
	           $Debug .=  "<br>";
	           $Debug .=  "ich nehme ned teil";
	           $Debug .=  "<br>";
	           
	           if (isset($_POST['txtSET_ABSAGEGRUND']) and strlen($_POST['txtSET_ABSAGEGRUND']) <= 3)
	           {
	               $Debug .=  "<br>";
	               $Debug .=  "ich hab keinen absagegrund";
	               $Debug .=  "<br>";
	               
	               $Form->ZeileStart();
	               $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_ABSAGEGRUND']);
	               $Form->ZeileEnde();
	               $Fehler = true;
	           }
	           else
	           {
	               $Debug .=  "<br>";
	               $Debug .=  "ich habe einen Absagegrund";
	               $Debug .=  "<br>";
	               $SQLUpdateTeilnahme .= ' ,SET_ABSAGEGRUND = \'' . $_POST['txtSET_ABSAGEGRUND'] . '\'';
	           }
	           
	           $SQLUpdateTeilnahme .=' ,SET_TEILNAHME = 0';   
	           $SQLUpdateTeilnahme .=' , SET_ABSAGEBEST = -1';
	       }
	       elseif(isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '2'){

               $Debug .=  "<br>";
               $Debug .=  "ich tausche Teilnahme";
               $Debug .=  "<br>";

               if (isset($_POST['txtSET_TEILNEHMERTAUSCH']) and $_POST['txtSET_TEILNEHMERTAUSCH'] <> '')
               {
                   $Debug .=  "<br>";
                   $Debug .=  "ich habe einen Freitext eingegeben";
                   $Debug .=  "<br>";

                   $SQL = "UPDATE SEMINARTEILNEHMER ";
                   $SQL .= " SET SET_TEILNEHMERVORSCHLAG =".$DB->WertSetzen('SET','T',$_POST['txtSET_TEILNEHMERTAUSCH']);
                   $SQL .= ", SET_TEILNAHME =".$DB->WertSetzen('SET','Z',$_POST['txtSET_TEILNAHME']);
                   $SQL .= " WHERE SET_KEY = ".$DB->WertSetzen('SET','Z',$_GET['SET_ANAB_KEY']);
                   $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('SET'));

                   $TeilnehmerVorschlag = new awisSeminarplanEmailTeilnehmerVorschlag($_GET['SET_ANAB_KEY']);
                   $TeilnehmerVorschlag->Verarbeitung('');
               }
               else
               {
                   $Debug .=  "<br>";
                   $Debug .=  "ich habe keine Person ausgew�hlt";
                   $Debug .=  "<br>";

                   $Form->ZeileStart();
                   $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_TAUSCH_AUSWAHL']);
                   $Form->ZeileEnde();
                   $Fehler = true;
               }
           }
	       else 
	       {
           	   $Form->ZeileStart();
               $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_TEILNAHME']);
               $Form->ZeileEnde();
               $Fehler = true;
	       }
	       
	       $Form->DebugAusgabe(1, $Debug);
	       
	       if ($Fehler == false)
	       {
	           //Auf Datensatz einschr�nken
	           
	           
	           $SQLUpdateTeilnahme .=' where SET_KEY = ' .  $_GET['SET_ANAB_KEY'];
	           
	           $Form->DebugAusgabe(1,$SQLUpdateTeilnahme);

	           $DB->Ausfuehren($SQLUpdateTeilnahme);
	           
	           $Form->ZeileStart();
	           $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERFOLGREICH_GESPEICHERT']);
	           $Form->ZeileEnde();
	           
	           if (isset($_POST['txtSET_TEILNAHME']) and $_POST['txtSET_TEILNAHME'] == 0 )
	           {
	               $WertNeu = 0;
	           }
	           elseif(isset($_POST['txtSET_TEILNAHME']) and $_POST['txtSET_TEILNAHME'] == 1 )
	           {
	               $WertNeu = 1;
	           }
	           else 
	           {
	               $WertNeu = -1;
	           }
                
               if ($rsTeilnahmeAlt->FeldInhalt('SET_TEILNAHME') <> $WertNeu  and $WertNeu == 1 and $rsTeilnahmeAlt->FeldInhalt('SET_TEILNAHME') <> -1) //Die Teilnahme ist nun ja, und war vorher nicht ja.
               {
                   include('./seminarplan_Email_Academy_Storno.php');
               }

	           if($_POST['txtSET_TEILNAHME'] == '0') //Teilnahme nein, dann Email an GBL
	           {
	               include('./seminarplan_Email_GBL.php');
	           }

	           if($rsTeilnahmeAlt->FeldInhalt('SET_TEILNAHNE')!='-1')
               { //Wenn der User das erste mal speichert, is SET_TEILNAHME -1 gewesen. Das bedeutet, dass man auf keine �nderung reagieren muss, da der Ablauf greift.

                   if($rsTeilnahmeAlt->FeldInhalt('SET_HOTEL') == 1 and $_POST['txtSetHotel'] == 0) { //Teilnehmer wollte Hotel, jedoch jetzt nicht mehr
                       $HotelAbbestellt = new awisSeminarplanEmailHotelAbbestellt($_GET['SET_ANAB_KEY']);
                       $HotelAbbestellt->Verarbeitung('');
                   }
                   elseif(($_POST['oldSET_ANREISE']  <> $_POST['txtSET_ANREISE'] OR  $_POST['oldSET_ABREISE']  <> $_POST['txtSET_ABREISE']) and $rsTeilnahmeAlt->FeldInhalt('SET_HOTEL') == 1)
                   { //Teilnehmer hat An oder Abreise ge�ndert
                       $HotelZeitAenderung = new awisSeminarplanEmailHotelZeitAenderung($_GET['SET_ANAB_KEY']);
                       $HotelZeitAenderung->Verarbeitung('');
                   }
                   elseif ($_POST['txtSET_TEILNAHME'] == '1' AND $_POST['txtSetHotel'] == '1') //Teilnahme ja und Hotel ja
                   {
                       include('./seminarplan_Email_Academy.php');
                   }

               }
	           elseif ($_POST['txtSET_TEILNAHME'] == '1' AND $_POST['txtSetHotel'] == '1') //Teilnahme ja und Hotel ja 
	           {
	               include('./seminarplan_Email_Academy.php');
	           }
	           
            }    
	   }    
    	    $SQL ='SELECT';
    	    $SQL .='   set_key, set_per_nr,';
    	    $SQL .='   set_nachname,';
    	    $SQL .='   set_vorname,SET_HOTEL_VON, SET_HOTEL_BIS,';
    	    $SQL .='   set_fil_nr,';
    	    $SQL .='   SEP_KEY, SET_ABSAGEGRUND, ';
    	    $SQL .='   SEP_SEMINAR, SRG_REGION, SER_RAUM, SET_TEILNAHME, SET_HOTEL,';
    	    $SQL .='   SEP_BESCHREIBUNG, to_char(SEP_VON,\'DD.MM.YYYY\') as SEP_VON, to_char(SEP_BIS,\'DD.MM.YYYY\') as SEP_BIS';
    	    $SQL .='   ,SET_TEILNEHMERVORSCHLAG ';
    	    $SQL .=' FROM';
    	    $SQL .='   seminarteilnehmer se';
    	    $SQL .=' INNER JOIN seminarplan sep';
    	    $SQL .=' ON';
    	    $SQL .='   se.SET_SEP_KEY = SEP.SEP_KEY';
    	    
    	    $SQL .=' left JOIN seminarregionen srg';
    	    $SQL .=' ON';
    	    $SQL .='   srg.SRG_KEY = SEP.SEP_SRG_KEY';
    
    	    $SQL .=' INNER JOIN SEMINARRAEUME SER';
    	    $SQL .=' ON';
    	    $SQL .='   SER.SER_KEY = SEP.SEP_SER_KEY';
    	    
    	    
    	    $SQL .='   where sep_key = ' . $AWIS_KEY1 . ' and set_key = ' . $_GET['SET_ANAB_KEY'];

    	    $Form->DebugAusgabe(1, $SQL);
    	    $rsANAB = $DB->RecordSetOeffnen($SQL);
    	    $Aendern = true;
    	    

            $Form->Erstelle_HiddenFeld('SET_ANAB_KEY', $_GET['SET_ANAB_KEY']);
             
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_VORNAME'].':',120);
            $Form->Erstelle_TextFeld('SET_VORNAME',$rsANAB->FeldInhalt('SET_VORNAME'),50,170,false);
    	  
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_NACHNAME'].':',130);
    	    $Form->Erstelle_TextFeld('SET_NACHNAME',$rsANAB->FeldInhalt('SET_NACHNAME'),50,150,false);
    	    $Form->ZeileEnde();
    	    
    	   
    	    //Blendet Divs ein und aus, anhand der Auswhl in den Checkboxen.
    	    $script = "<script type='text/javascript'>
    	
    			
            function blendeEinHotel(obj) {	           
    	       var checkWert = obj.options[obj.selectedIndex].value;
    	      
    	       if(checkWert == 1)
    	       { 
                   //alert('0'); //ja
    	           document.getElementById('Hoteljanein').style.visibility = 'visible';	       
    	           document.getElementById('Absage').style.visibility = 'hidden';
    	           document.getElementById('Teilnehmertausch').style.visibility = 'hidden';
    	           document.frmSeminarplan.txtSetHotel.selectedIndex = 0;	        
	           }
    	       else if(checkWert == 0) //nein
    	       {        
    	            document.getElementById('Absage').style.visibility = 'visible';
    	            document.getElementById('Hoteljanein').style.visibility = 'hidden';
    	            document.getElementById('Hotelvonbis').style.visibility = 'hidden';
    	            document.getElementById('Teilnehmertausch').style.visibility = 'hidden';
    	             //alert('1');
    	            document.frmSeminarplan.txtSetHotel.selectedIndex = 0;
    	            //alert('2');
    	       } else if(checkWert == 2){
    	           document.getElementById('Hoteljanein').style.visibility = 'hidden';
    	           document.getElementById('Hotelvonbis').style.visibility = 'hidden';
    	           document.getElementById('Absage').style.visibility = 'hidden';
    	           document.getElementById('Teilnehmertausch').style.visibility = 'visible';
    	           document.frmSeminarplan.txtSetHotel.selectedIndex = 0;
    	       }
    	       else //bitte w�hlen
    	       {
    	             document.getElementById('Hoteljanein').style.visibility = 'hidden';
    	             document.getElementById('Hotelvonbis').style.visibility = 'hidden';
    	             document.getElementById('Teilnehmertausch').style.visibility = 'hidden';
    	             document.getElementById('Absage').style.visibility = 'hidden';
    	             //alert('1');
    	            document.frmSeminarplan.txtSetHotel.selectedIndex = 0;
    	            //alert('2');
    	       }
    	        
    	        	
             }
             function blendeEinHotelVonBis(obj) {	
    
    	       var checkWert = obj.options[obj.selectedIndex].value;
    	       //alert(checkWert);
    	        if(checkWert == 1) //ja
    	       { 
                   document.getElementById('Hotelvonbis').style.visibility = 'visible';
                   	          
    	       }
    	        else
    	        {
                     document.getElementById('Hotelvonbis').style.visibility = 'hidden';
    	        }
            }            
            </script>";
    	
    	    echo $script;

    	    //Muss was vorgeblendet werden in die Kombobox?
    	    if (isset($_POST['txtSET_TEILNAHME']))
    	    {    	       
    	        $Wert = $_POST['txtSET_TEILNAHME'];
    	    }
    	    else
    	    {
    	        $Wert = $rsANAB->FeldInhalt('SET_TEILNAHME');
    	    }
			$Form->DebugAusgabe(1, $Wert);
    	   

           
    	    $Form->ZeileStart();
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_TEILNAHME'].':',120);
    	    $Daten = explode('|',$AWISSprachKonserven['SET']['SET_DROP_TEILNAHME']);
    	    $Form->Erstelle_SelectFeld('SET_TEILNAHME',$Wert ,150,$Aendern,'','Bitte w�hlen','','','',$Daten,'onchange="blendeEinHotel(this)"; onload="blendeEinHotel(this)"','Bitte waehlen');
    	    
            if($Wert ==0 )
    	    {
    	        $Form->ZeileStart('visibility: visible','Absage"ID="Absage');
				//$Form->SchreibeHTMLCode('<div id="Absage" style="visibility: visible">');
    	    } else {
                $Form->ZeileStart('visibility: hidden','Absage"ID="Absage');
                //$Form->SchreibeHTMLCode('<div id="Absage" style:visibility: hidden">');
                $Form->DebugAusgabe(2, 'Absage hidden');
            }
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_ABSAGEGRUND'].':',120,false,'','"id="Grundlabel"');
            //$Form->SchreibeHTMLCode('<label for="vorname">Vorname:</label>');
    	    
    	    $Form->Erstelle_TextFeld('SET_ABSAGEGRUND', isset($_POST['txtSET_ABSAGEGRUND'])?$_POST['txtSET_ABSAGEGRUND']:$rsANAB->FeldInhalt('SET_ABSAGEGRUND'), 30, 250, true);
    	    $Form->SchreibeHTMLCode('</div>');
    	    $Form->ZeileEnde();

    	    if(isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '2' or ($rsANAB->FeldInhalt('SET_TEILNAHME') == '2')){
                $Form->ZeileStart('visibility: visible','Teilnehmertausch"ID="Teilnehmertausch');
            } else {
                $Form->ZeileStart('visibility: hidden','Teilnehmertausch"ID="Teilnehmertausch');
            }

            $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_TEILNEHMERTAUSCH'].":",300);
            $Form->Erstelle_TextFeld('SET_TEILNEHMERTAUSCH',((isset($_POST['txtSET_TEILNEHMERVORSCHLAG']))?$_POST['txtSET_TEILNEHMERVORSCHLAG']:(($rsANAB->FeldInhalt('SET_TEILNEHMERVORSCHLAG') <> '')?$rsANAB->FeldInhalt('SET_TEILNEHMERVORSCHLAG'):'')),50,300,$Aendern);
            $Form->ZeileEnde();

    	    //Wird nur angezeigt, wenn die Checkbox SET_TEILNAHME auf ja steht
    	    if(isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '1' or ($rsANAB->FeldInhalt('SET_TEILNAHME') == '1'))
    	    {
    	        $Form->ZeileStart('visibility: visible','Hotel"ID="Hoteljanein');
    	    }
    	    else 
    	    {
    	        $Form->ZeileStart('visibility: hidden','Hotel"ID="Hoteljanein');
    	    }


    	    //Muss was vorgeblendet werden in die Kombobox?
    	    if (isset($_POST['txtSetHotel']))
    	    {
    	        $Wert = $_POST['txtSetHotel'];
    	    }
    	    else
    	    {
    	        $Wert = $rsANAB->FeldInhalt('SET_HOTEL');
    	    }
    	    
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_HOTEL'],200);
    	    $Daten = explode('|',$AWISSprachKonserven['SET']['SET_SELECT_JANEIN']);
    	    $Form->Erstelle_SelectFeld('SetHotel',$Wert,300,$Aendern,'','Bitte w�hlen','','','',$Daten,'onchange="blendeEinHotelVonBis(this)"; onload="blendeEinHotelVonBis(this)"','Bitte waehlen');
    	    $Form->ZeileEnde();
    	    
    	    
    	    //Wird nur angezeigt, wenn die Checkbox SET_HOTEL auf ja steht
    	    if(((isset($_POST['txtSetHotel']) AND $_POST['txtSetHotel'] == '1' or $rsANAB->FeldInhalt('SET_HOTEL') == '1') and (isset($_POST['txtSET_TEILNAHME']) AND $_POST['txtSET_TEILNAHME'] == '1' or ($rsANAB->FeldInhalt('SET_TEILNAHME') == '1'))))
    	    {
    	        $Form->ZeileStart('visibility: visible','Hotel"ID="Hotelvonbis');
    	    }
    	    else
    	    {
    	        $Form->ZeileStart('visibility: hidden','Hotel"ID="Hotelvonbis');    	    
    	    }
    	    
    	    
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_ANREISE'].':',70);
    	    //Werte vorblenden
			if ($rsANAB->FeldInhalt('SET_HOTEL_VON') != '')
			{
				$Wert = $rsANAB->FeldInhalt('SET_HOTEL_VON');
			}
			elseif (isset($_POST['txtSET_ANREISE']))
			{
				$Wert = $_POST['txtSET_ANREISE'];
			}
			else
			{
				$Wert = $rsANAB->FeldInhalt('SEP_VON');
			}
			$Form->Erstelle_TextFeld('SET_ANREISE',$Wert,20,200,$Aendern,'','','','D');
    	    
    	    
    	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_ABREISE'].':',70);
    	    //Werte vorblenden
			if ($rsANAB->FeldInhalt('SET_HOTEL_BIS') != '')
			{
				$Wert = $rsANAB->FeldInhalt('SET_HOTEL_BIS');
			}
			elseif (isset($_POST['txtSET_ABREISE']))
			{
				$Wert = $_POST['txtSET_ABREISE'];
			}
			else
			{
				$Wert = $rsANAB->FeldInhalt('SEP_BIS');
			}
			
			$Form->Erstelle_TextFeld('SET_ABREISE',$Wert,20,200,$Aendern,'','','','D');
    	    $Form->ZeileEnde();
    	    

    		$Form->DebugAusgabe(2,$_GET);
	}
	elseif(isset($_GET['SET_ABSAGEBEST']))
	{
	    if(isset($_POST['cmdSpeichern_x']))
	    {
	        $Fehler = false;
	        $SQLUpdateAbsage  ='update Seminarteilnehmer';
	        $SQLUpdateAbsage .=' set SET_USER = \'' . $AWISBenutzer->BenutzerName(1) . '\'';
	        $SQLUpdateAbsage .=' ,SET_USERDAT = sysdate';
	        //Bei Teilnahme
	        if ($_POST['txtSET_ABSAGE'] == '1') //ja
	        {
	            $SQLUpdateAbsage .=' ,SET_ABSAGEBEST = 1';
	            include('./seminarplan_Email_Academy.php');
	            $Fehler = false;
	        }
	        elseif ($_POST['txtSET_ABSAGE'] == '0') //Auswahl nein
	        {
	            include('./seminarplan_Email_GBL_NICHTBESTAEDIGT.php');
	            $SQLUpdateAbsage .=' ,SET_ABSAGEBEST = 0';
	            $Fehler = false;
	        } 
	        else 
	        {
	            $Form->ZeileStart();
	            $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_ABSAGEBEST']);
	            $Form->ZeileEnde();
	            $Fehler = true;
	        }
	        
	        if($Fehler == false)
	        {
	            $SQLUpdateAbsage .=' where SET_KEY = ' .  $_GET['SET_ABSAGEBEST_KEY'];
	            $Form->DebugAusgabe(2,$SQLUpdateAbsage);
	            $DB->Ausfuehren($SQLUpdateAbsage);  
	            
	            $Form->ZeileStart();
	            $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERFOLGREICH_GESPEICHERT']);
	            $Form->ZeileEnde();
	            
	        }
	            
	    }
	    
	    $SQL ='SELECT';
	    $SQL .='   set_key, set_per_nr, set_absagebest,set_teilnahme,';
	    $SQL .='   set_nachname,';
	    $SQL .='   set_vorname,';
	    $SQL .='   set_fil_nr,';
	    $SQL .='   set_absagegrund,';
	    $SQL .='   SEP_KEY,';
	    $SQL .='   SEP_SEMINAR, SRG_REGION, SER_RAUM, ';
	    $SQL .='   SEP_BESCHREIBUNG, to_char(SEP_VON,\'DD.MM.YYYY\') as SEP_VON, to_char(SEP_BIS,\'DD.MM.YYYY\') as SEP_BIS';
	    $SQL .=' FROM';
	    $SQL .='   seminarteilnehmer se';
	    $SQL .=' INNER JOIN seminarplan sep';
	    $SQL .=' ON';
	    $SQL .='   se.SET_SEP_KEY = SEP.SEP_KEY';
	    	
	    $SQL .=' left JOIN seminarregionen srg';
	    $SQL .=' ON';
	    $SQL .='   srg.SRG_KEY = SEP.SEP_SRG_KEY';
	    
	    $SQL .=' INNER JOIN SEMINARRAEUME SER';
	    $SQL .=' ON';
	    $SQL .='   SER.SER_KEY = SEP.SEP_SER_KEY';
	    	
	    	
	    $SQL .='   where sep_key = ' . $AWIS_KEY1 . 'and set_key = ' . $_GET['SET_ABSAGEBEST_KEY'];
	    
	    $Form->DebugAusgabe(1, $SQL);
	    
	    $rsANAB = $DB->RecordSetOeffnen($SQL);

		if(isset($_GET['SET_ABSAGEBEST_KEY']) and $rsANAB->FeldInhalt('SET_TEILNAHME') != 0)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_TEILNAHME_GEWECHSELT']);
			$Form->ZeileEnde();
		}
		else {

			$Form->Erstelle_HiddenFeld('SET_ABSAGEBEST_KEY', $_GET['SET_ABSAGEBEST_KEY']);

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_VORNAME'] . ':', 90);
			$Form->Erstelle_TextFeld('SET_VORNAME', $rsANAB->FeldInhalt('SET_VORNAME'), 50, 150, false);

			$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_NACHNAME'] . ':', 100);
			$Form->Erstelle_TextFeld('SET_NACHNAME', $rsANAB->FeldInhalt('SET_NACHNAME'), 50, 150, false);
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_ABSAGEGRUND'] . ':', 100);
			$Form->Erstelle_TextFeld('SET_ABSAGEGRUND', $rsANAB->FeldInhalt('SET_ABSAGEGRUND'), 100, 300, false);
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_ABSAGEBEST'], 600);
			$Daten = explode('|', $AWISSprachKonserven['SET']['SET_SELECT_JANEIN']);
			$Form->ZeileEnde();

			if ($rsANAB->FeldInhalt('SET_ABSAGEBEST') == -1) {
				$Aendern = true;
			} else {
				$Aendern = false;
			}

			$Form->ZeileStart();
			$Form->Erstelle_SelectFeld('SET_ABSAGE', isset($_POST['txtSET_ABSAGE'])?$_POST['txtSET_ABSAGE']:$rsANAB->FeldInhalt('SET_ABSAGEBEST'), 300, $Aendern, '',
				'Bitte w�hlen', '', '', '', $Daten, 'onchange="blendeEinHotelVonBis(this)"; onload="blendeEinHotelVonBis(this)"', 'Bitte waehlen');
			$Form->ZeileEnde();
		}
	    
	}
	elseif(isset($_GET['SET_MAIL_KEY']) or isset($_POST['txtSET_MAIL_KEY']))
	{
        include('./seminarplan_Email_Manuell.php');         
	}elseif(isset($_GET['HOTEL_SET_KEY'])){ //Hotelbuchung hinterlegen oder anzeigen?
        include('./seminarplan_Details_Teilnehmer_Hotel.php');
    }
	else										// Ein einzelner oder neuer Teilnehmer
	{


		$Form->Erstelle_HiddenFeld('SET_KEY',$rsSET->FeldInhalt('SET_KEY'));
		$Form->Erstelle_HiddenFeld('SET_SEP_KEY',$AWIS_KEY1);
		
		$AWIS_KEY2 = $rsSET->FeldInhalt('SET_KEY');

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./seminarplan_Main.php?cmdAktion=Details&SETListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSET->FeldInhalt('SET_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSET->FeldInhalt('SET_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4200&128)!=0);
				
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_PER_NR'].':',150);
		$Form->Erstelle_TextFeld('SET_PER_NR',$rsSET->FeldInhalt('SET_PER_NR'),50,350,$EditRecht);
		$AWISCursorPosition='txtSET_PER_NR';
		$Form->ZeileEnde();				

	}
}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>