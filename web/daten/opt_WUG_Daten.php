<?php
//**************************************************************
// Liefert Warenuntergruppen einer ausgewählren Warengruppe
// 
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['WGR']))
{
	die('##PARAM##');
}

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abhängigkeit der WGR anzeigen
$SQL = "SELECT DISTINCT WUG_ID AS KEY, WUG_ID || ' - ' || WUG_BEZEICHNUNG AS ANZEIGE";
$SQL .= ' FROM WARENUNTERGRUPPEN';
$SQL .= ' WHERE WUG_WGR_ID = :var_T_WUG_WGR_ID';
$SQL .= ' ORDER BY WUG_ID';
$DB->SetzeBindevariable('WUG', 'var_T_WUG_WGR_ID', $_GET['WGR'], awisDatenbank::VAR_TYP_TEXT);
$rsWUG = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WUG'));

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
if($_GET['Zusatz'])
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">'.$_GET['Zusatz'].'</option>';
	}
	else
	{
		echo '#+##+#'.$_GET['Zusatz'].'';
	}
}
$DS=0;
while(!$rsWUG->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsWUG->FeldInhalt('KEY').'">'.$rsWUG->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsWUG->FeldInhalt('KEY').'#+#'.$rsWUG->FeldInhalt('ANZEIGE').'';
	}

	$rsWUG->DSWeiter();
	
	if(++$DS>200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="">...</option>';
		}
		else
		{
			echo '#+##+#...';
		}
		break;
	}
}
?>