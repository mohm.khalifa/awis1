<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090220
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht5000=$AWISBenutzer->HatDasRecht(5000);		// Rechte des Mitarbeiters

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./filialinfos_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	// Filialnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',190);
	$Form->Erstelle_TextFeld('*FIL_ID',($Param['SPEICHERN']=='on'?$Param['FIL_ID']:''),5,200,true);
	$AWISCursorPosition='sucFIL_ID';
	$Form->ZeileEnde();

	// Name der Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_BEZ'].':',190);
	$Form->Erstelle_TextFeld('*FIL_BEZ',($Param['SPEICHERN']=='on'?$Param['FIL_BEZ']:''),25,200,true);
	$Form->ZeileEnde();

	// Strasse
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_STRASSE'].':',190);
	$Form->Erstelle_TextFeld('*FIL_STRASSE',($Param['SPEICHERN']=='on'?$Param['FIL_STRASSE']:''),25,200,true);
	$Form->ZeileEnde();

	// Ort
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ORT'].':',190);
	$Form->Erstelle_TextFeld('*FIL_ORT',($Param['SPEICHERN']=='on'?$Param['FIL_ORT']:''),25,200,true);
	$Form->ZeileEnde();
	
	// Ortsteil
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ORTSTEIL'].':',190);
	$Form->Erstelle_TextFeld('*FIL_ORTSTEIL',($Param['SPEICHERN']=='on'?$Param['FIL_ORTSTEIL']:''),25,200,true);
	$Form->ZeileEnde();

	// Postleitzahl
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_PLZ'].':',190);
	$Form->Erstelle_TextFeld('*FIL_PLZ',($Param['SPEICHERN']=='on'?$Param['FIL_PLZ']:''),5,200,true);
	$Form->ZeileEnde();

	// Land
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_LAN_WWSKENN'].':',190);
	$SQL = "SELECT LAN_WWSKENN, LAN_LAND FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL ORDER BY LAN_LAND";
	$Form->Erstelle_SelectFeld('*FIL_LAND',($Param['SPEICHERN']=='on'?$Param['FIL_LAND']:''),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
	$Form->ZeileEnde();

	if(($Recht5000&16)!=0)
	{
		// Mitarbeiter
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_MITARBEITER'].':',190);
		$Form->Erstelle_TextFeld('*FIL_MITARBEITER',($Param['SPEICHERN']=='on'?$Param['FIL_MITARBEITER']:''),20,200,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_MITARBEITERVORNAME'].':',190);
		$Form->Erstelle_TextFeld('*FIL_MITARBEITERVORNAME',($Param['SPEICHERN']=='on'?$Param['FIL_MITARBEITERVORNAME']:''),20,200,true);
		$Form->ZeileEnde();

		if(($Recht5000&32)!=0)
		{
			// Mitarbeiter
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_MITARBEITERPERSNR'].':',190);
			$Form->Erstelle_TextFeld('*FIL_MITARBEITERPERSNR',($Param['SPEICHERN']=='on'?$Param['FIL_MITARBEITERPERSNR']:''),20,200,true);
			$Form->ZeileEnde();
		}
	}	// Recht 2^4


	if(($Recht5000&64)!=0)
	{
		// Betreuer nach Filialrollen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_BETREUER'].':',190);

		$SQL = 'select distinct kon_key, kon_name1 || \', \' || kon_name2 AS Anz';
		$SQL .= ' from AWIS.FILIALEBENENROLLENZUORDNUNGEN';
		$SQL .= ' inner join kontakte ON frz_kon_key = kon_key';
        $SQL .= ' where FRZ_GUELTIGAB <= SYSDATE AND FRZ_GUELTIGBIS >= SYSDATE';
        $SQL .= ' AND KON_STATUS = \'A\'';
        $SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('*FIL_BETREUER',($Param['SPEICHERN']=='on'?$Param['FIL_BETREUER']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();
	}

	if(($Recht5000&128)!=0)
	{
		// Gebiet
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',190);
		$SQL = "SELECT DISTINCT FEB_KEY, FEB_BEZEICHNUNG";
		$SQL .= ' FROM FILIALEBENEN';
        $SQL .= ' WHERE FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
        $SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();

	}

	$Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_NOTFILIALE'].':',190);
    $Liste = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
    $Form->Erstelle_SelectFeld('*FIL_NOTFILIALE',($Param['SPEICHERN']=='on'?$Param['FIL_NOTFILIALE']:''),400,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Liste);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Shop ge�ffnet',190);
    $Liste = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
    $Form->Erstelle_SelectFeld('*FIL_SHOP_GEOFFNET',($Param['SPEICHERN']=='on'?$Param['FIL_SHOP_GEOFFNET']:''),400,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Liste);
    $Form->ZeileEnde();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht5000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=filialinfos&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301833");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301834");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>