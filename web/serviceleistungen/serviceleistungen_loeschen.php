<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtSVA_KEY']))
		{
			$Tabelle = 'SVA';
			$Key=$_POST['txtSVA_KEY'];

			$SQL = 'SELECT SVA_KEY, SVA_BEZEICHNUNG, SVA_AST_ATUNR';
			$SQL .= ' FROM SVCARTIKEL ';
			$SQL .= ' WHERE SVA_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$HauptKey = $rsDaten->FeldInhalt('SVA_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('SVA','SVA_BEZEICHNUNG'),$rsDaten->FeldInhalt('SVA_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('SVA','SVA_AST_ATUNR'),$rsDaten->FeldInhalt('SVA_AST_ATUNR'));
		}
		elseif(isset($_POST['txtSVB_KEY']))		// Zugriffsgruupe AP
		{
			$Tabelle = 'SVB';
			$Key=$_POST['txtSVB_KEY'];

			$SQL = 'SELECT *';
			$SQL .= ' FROM SVCVEREINBARUNGEN ';
			$SQL .= ' WHERE SVB_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$HauptKey = $rsDaten->FeldInhalt('SVA_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('SVB','SVB_VERTRAGSNUMMER'),$rsDaten->FeldInhalt('SVB_VERTRAGSNUMMER'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case 'Benachrichtigungen':		// In den Teilnehmern
					$Tabelle = 'STB';
					$Key=$_GET['Del'];

					$SQL = 'SELECT STB_KEY, STB_DATUM, SVT_SVB_KEY';
					$SQL .= ' FROM SVCTEILNBENACHRICHTUNGEN ';
					$SQL .= ' INNER JOIN SVCTEILNEHMER ON STB_SVT_KEY = SVT_KEY';
					$SQL .= ' WHERE STB_KEY=0'.$Key . '';
					$SQL .= ' AND STB_SVT_KEY=0'.$_GET['SVT_KEY']. '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$HauptKey=$rsDaten->FeldInhalt('SVT_SVB_KEY');
					$SubKey=$rsDaten->FeldInhalt('STB_SVB_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('STB','STB_DATUM'),$Form->Format('D',$rsDaten->FeldInhalt('STB_DATUM')));
					break;
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Fahrzeuge':
				case 'Teilnehmer':
					$Tabelle = 'SVT';
					$Key=$_GET['Del'];

					$SQL = 'SELECT SVT_KEY, SVT_NAME, SVT_VORNAME, SVT_SVB_KEY';
					$SQL .= ' FROM SVCTEILNEHMER ';
					$SQL .= ' WHERE SVT_KEY=0'.$Key . '';
					$SQL .= ' AND SVT_SVB_KEY=0'.$_GET['SVB_KEY']. '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$HauptKey=$rsDaten->FeldInhalt('SVT_SVB_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('SVT','SVT_NAME'),$rsDaten->FeldInhalt('SVT_NAME'));
					$Felder[]=array($Form->LadeTextBaustein('SVT','SVT_VORNAME'),$rsDaten->FeldInhalt('SVT_VORNAME'));
					break;
				case 'Benachrichtigungen':
					$Tabelle = 'SAB';
					$Key=$_GET['Del'];

					$SQL = 'SELECT SAB_KEY,SAB_BEZEICHNUNG,SAB_SVA_KEY';
					$SQL .= ' FROM SVCARTIKELBENACHRICHTIGUNGEN ';
					$SQL .= ' WHERE SAB_KEY=0'.$Key . '';
					$SQL .= ' AND SAB_SVA_KEY=0'.$_GET['SVA_KEY']. '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$HauptKey=$rsDaten->FeldInhalt('SAB_SVA_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('SAB','SAB_BEZEICHNUNG'),$rsDaten->FeldInhalt('SAB_BEZEICHNUNG'));
					break;
				case 'Parameter':
					$Tabelle = 'SAA';
					$Key=$_GET['Del'];

					$SQL = 'SELECT SAA_KEY,SAA_BEZEICHNUNG,SAA_SVA_KEY';
					$SQL .= ' FROM SVCARTIKELANGABEN ';
					$SQL .= ' WHERE SAA_KEY=0'.$Key . '';
					$SQL .= ' AND SAA_SVA_KEY=0'.$_GET['SVA_KEY']. '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$HauptKey=$rsDaten->FeldInhalt('SAA_SVA_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('SAA','SAA_BEZEICHNUNG'),$rsDaten->FeldInhalt('SAA_BEZEICHNUNG'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'SVA':
				$SQL = 'DELETE FROM SVCARTIKEL WHERE SVA_KEY=0'.$_POST['txtKey'];
				$AWIS_KEY1=-1;
				break;
			case 'SVB':
				$SQL = 'DELETE FROM SVCVEREINBARUNGEN WHERE SVB_KEY=0'.$_POST['txtKey'];
				$AWIS_KEY1=-1;
				break;
			case 'SVT':
				$SQL = 'DELETE FROM SVCTEILNEHMER WHERE SVT_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtHauptKey'];
				break;
			case 'SAB':
				$SQL = 'DELETE FROM SVCARTIKELBENACHRICHTIGUNGEN WHERE SAB_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtHauptKey'];
				break;
			case 'SAA':
				$SQL = 'DELETE FROM SVCARTIKELANGABEN WHERE SAA_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtHauptKey'];
				break;
			case 'STB':
				$SQL = 'DELETE FROM SVCTEILNBENACHRICHTUNGEN WHERE STB_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2=$_POST['txtSubKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./serviceleistungen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
		if(isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>