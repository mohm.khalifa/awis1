<?
global $CursorFeld;
global $AWISBenutzer;
global $con;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZSZ','%');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Liste','lst_JaNein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht10008=awisBenutzerRecht($con,10008);

echo "<br>";
echo "<form name=frmSuche method=post action=./warengruppen_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',190);
awis_FORM_Erstelle_TextFeld('*ZSZ_SORTIMENT','',20,200,true);
$CursorFeld='sucZSZ_SORTIMENT';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG1'].':',190);
awis_FORM_Erstelle_TextFeld('*ZSZ_WG1','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG2'].':',190);
awis_FORM_Erstelle_TextFeld('*ZSZ_WG2','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG3'].':',190);
awis_FORM_Erstelle_TextFeld('*ZSZ_WG3','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_ZLI_KEY'].':',190);
$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*ZSZ_ZLI_KEY','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


awis_FORM_SchaltflaechenStart();
	// Zur�ck zum Men�
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht10008&4) == 4)		// Hinzuf�gen erlaubt?
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
awis_FORM_Schaltflaeche('image','cmdXReset','','/bilder/radierer.png',$AWISSprachKonserven['Wort']['lbl_reset'],'R');

awis_FORM_SchaltflaechenEnde();


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>