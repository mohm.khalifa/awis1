<?php

require_once('awisDatenbank.inc');
require_once('awisFirstglasFunktionen.php');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);


class awisFirstglasStatus {

/******************************************************************************
 *                                                                           				       *
 *                           Firstglas Status                                 			       *
 *                                                                            *			       *
 *                                                                            *      			       *
 *                                                                            *      			       *
 ******************************************************************************/

    private $_AWISBenutzer;
    private $_DB;
    private $_Form;
    private $_Werkzeug;
    private $_ZugangsDatei;
    private $_StornoDatei;
    private $_Funktionen;
    private $_FirstglasDatei;
    private $_flagMittwoch = false;

    function __construct() {
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
        $this->_Werkzeug = new awisWerkzeuge();
        $this->_Funktionen = new FirstglasFunktionen();
    }

    function setStatus() {
        //Vorg�nge die manuell freigegeben wurden - STATUS NR.10
        
        $this->_Funktionen->SchreibeStartZeit("setStatus");
        ////Setze DS die den Status 11,12,13,14 haben zur�ck auf null
	
        $this->_Funktionen->SchreibeModulStatus("setStatus","ZugangsUeberpruefung");
      	$this->ZugangsUeberpruefung();
        $this->_Funktionen->SchreibeModulStatus("setStatus","ZugangsUeberpruefung");
        
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start ARTNR_EZDVZ");
        $this->ARTNR_EZDVZ();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ARTNR_EZDVZ");
        //OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start stornierteDatensaetzeErmitteln");
        $this->stornierteDatensaetzeErmitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende stornierteDatensaetzeErmitteln");

        //OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start stronierteDatensaetzeMitKassendatenErmitteln");
        $this->stronierteDatensaetzeMitKassendatenErmitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende stronierteDatensaetzeMitKassendatenErmitteln");
        
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start KassendatenErmitteln");
        $this->KassendatenErmitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende KassendatenErmitteln");

        //OK
        //Status "11" -- OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start ArtNR_NKKSFAR");
        //$this->ArtNR_NKKSFAR();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ArtNR_NKKSFAR");

        //Status "12" -- OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start ArtNR_KFAR");
        $this->ArtNr_KFAR();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ArtNR_KFAR");
        //Status "13" -- OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start KFARMS");
        $this->ArtNr_KFARMS();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende KFARMS");

        //Status "14" -- OK
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start FAMS");
        $this->ArtNr_FAMS();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende FAMS");


        //Bereits UEBERTRAGE AUFTRAEGE MARKIEREN
        //-----------------------------------------------
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start bereitsUebertrageneAuftraege");
        $this->bereitsUebertrageneAuftraege();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende bereitsUebertrageneAuftraege");
        //Bereits Gesperrte AUFTRAEGE AUF Gesperrt setzen
        //-----------------------------------------------

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start bereitsGesperrteAuftraege");
        $this->bereitsGesperrteAuftraege();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende bereitsGesperrteAuftraege");

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start pruefeGesperrteAuftraegeErneut");
        $this->pruefeGesperrteAuftraegeErneut();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende pruefeGesperrteAuftraegeErneut");
	
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start Verarbeite");
        $this->Verarbeite();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende Verarbeite");

        $this->_Funktionen->SchreibeModulStatus("setStatus","RuecknahmenAnreicherung");
        $this->ImportRuecknahmen();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende RuecknahmenAnreicherung");	

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start OES_FILIALEN_ERMITTELN");
        $this->oes_Filialen_Ermitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende OES_FILIALEN_ERMITTELN");

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start ital_Filialen_Ermitteln");
        $this->ital_Filialen_Ermitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ital_Filialen_Ermitteln");

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start cze_Filialen_Ermitteln");
        $this->cze_Filialen_Ermitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende cze_Filialen_Ermitteln");

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start ned_Filialen_Ermitteln");
        $this->ned_Filialen_Ermitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ned_Filialen_Ermitteln");

        $this->_Funktionen->SchreibeModulStatus("setStatus","Start sui_Filialen_Ermitteln");
        $this->sui_Filialen_Ermitteln();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende sui_Filialen_Ermitteln");
	
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start Zugang");
        $this->Zugang();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende Zugang");
        
        $this->_Funktionen->SchreibeModulStatus("setStatus","Start Anreicherung");
        $this->verarbeiteStatusSieben();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende Anreicherung");
        
        $this->_Funktionen->SchreibeModulStatus("setStatus","START Export ZUGANG");
        $this->ExportZugang();
        $this->_Funktionen->SchreibeModulStatus("setStatus","ENDE Export ZUGANG");
        
	$this->_Funktionen->SchreibeModulStatus("setStatus","WE ExportDatei");
        $this->erstelleWoechentlicheFirstglasDatei();
        $this->_Funktionen->SchreibeModulStatus("setStatus","ENDE WE ExportDatei");

        $this->_Funktionen->SchreibeEndeZeit("setStatus","Start ExportFirstglas");
            //Status 2 wird gesetzt
        $this->pruefeVorgangsNrGlaskopf();
        $this->ExportFirstglas();
        $this->setzeKopfdatenImportUndExport();
        $this->_Funktionen->SchreibeEndeZeit("setStatus","Ende ExportFirstglas");

        $this->_Funktionen->SchreibeModulStatus("setStatus","ExportKassendatenStorno");
	$this->erstelleStornoDatei();
	$this->ExportKassendatenStorno();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende ExportKassendatenStorno");

        $this->_Funktionen->SchreibeModulStatus("setStatus","PruefeAbarbeitungsFrist");
        $this->pruefeAbarbeitungsFrist();
        $this->_Funktionen->SchreibeModulStatus("setStatus","PruefeAbarbeitungsFrist");
        
        $this->_Funktionen->SchreibeModulStatus("setStatus","Mails");
        $this->sendeDifferenzMails();
        $this->_Funktionen->SchreibeModulStatus("setStatus","DifferenzMails");

        $this->_Funktionen->SchreibeModulStatus("setStatus","entferneAbgeglicheneVorgaengeAusDifferenzListe");
      	$this->entferneAbgeglicheneVorgaengeAusDifferenzListe();
        $this->_Funktionen->SchreibeModulStatus("setStatus","Ende entferneAbgeglicheneVorgaengeAusDifferenzListe");
	
	$this->_Funktionen->SchreibeModulStatus("setStatus","OPAL - Anreicherung");
	
        $this->FGOpalAnreicherung();
        $this->FGOpalAnreicherungStatusSetzen();

        $this->FGOpalSpeichereDatensaetze();
        $this->OpalAnreicherungCOMServer();
	
	$this->_Funktionen->SchreibeModulStatus("setStatus","ENDE OPAL - Anreicherung");
	
	$this->_Funktionen->SchreibeModulStatus("setStatus","ENDE VERARBEITUNG");
	$this->_Funktionen->SchreibeEndeZeit("setStatus","Ende ExportFirstglas");
 }
 
	public function ImportRuecknahmen()
	{
		$SQL = 'Select DISTINCT FGK_VORGANGNR FROM FGKOPFDATEN WHERE FGK_FGN_KEY=6';
		
		$rsGesperrteVorgaenge = $this->_DB->RecordSetOeffnen($SQL);
	
		while(!$rsGesperrteVorgaenge->EOF())
		{
		   $SQL = "Select Max(Fka_Datum) As Maxdatum,Min(Fka_Datum) As Mindatum FROM FKASSENDATEN WHERE FKA_AEMNR=".$this->_DB->FeldInhaltFormat('T',$rsGesperrteVorgaenge->FeldInhalt('FGK_VORGANGNR'));		   
		   
		   $rsMAXMINDate = $this->_DB->RecordSetOeffnen($SQL);
		
		   $SQL = "Select FKA_KEY,  FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_AEM_VERS_KZ ";
		   $SQL .= " from FKASSENDATEN WHERE FKA_AEMNR=".$this->_DB->FeldInhaltFormat('T',$rsGesperrteVorgaenge->FeldInhalt('FGK_VORGANGNR'));	
		
		   echo "KASSENDATEN".$SQL."\n";
		
		   $rsKassendaten = $this->_DB->RecordSetOeffnen($SQL);
		
		   $rsKassendaten->DSErster();
		
		   echo "DA";
		   
		   $count = 0;
		   $countInsert = 0;
		   
		   while(!$rsKassendaten->EOF())
		   {
		     $SQL = "Select substr(Datum,1,50) as Datum,substr(Zeit,1,50) as Zeit,substr(Bsa,1,50) as Bsa,substr(Vorgangsnr,1,50) as Vorgangsnr,Filialnummer,substr(Kfz_Kennzeichen,1,50) as Kfz_Kennzeichen,Werkstatt_Ja_Nein,substr(Bgt1,1,50) As Bgt1,substr(Bgt2,1,50) as BGT2,Umsatz_Bon From Ods.Trans@Dwh26_Ods ";
		     $SQL .= " WHERE DATUM >=".$this->_DB->FeldInhaltFormat('DU',$rsMAXMINDate->FeldInhalt('MINDATUM'));  
		     $SQL .= " AND DATUM <=".$this->_DB->FeldInhaltFormat('DU',$rsMAXMINDate->FeldInhalt('MAXDATUM')); 
		     $SQL .= " AND FILIALNUMMER=".$this->_DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('FKA_FILID')); 
		     $SQL .= " AND WERKSTATT_JA_NEIN=".$this->_DB->FeldInhaltFormat('T','R');

		     echo "TRANS_SQL".$SQL."\n";	

		     $rsTrans = $this->_DB->RecordSetOeffnen($SQL);	
		    
		     echo "Anzahl:".$count; 
		    
		     while(!$rsTrans->EOF()) 
		     {
		        echo "TRANS\n"; 
		        
			$BGTFILNR =  substr($rsTrans->FeldInhalt('BGT1'),0,4);
			$BGTWANR = substr($rsTrans->FeldInhalt('BGT1'),5,6);
			
			echo "FILNR:".$BGTFILNR."\n";
			echo "WANR:".$BGTWANR."\n"; 	
			
			echo "KasseWANR".$rsKassendaten->FeldInhalt('FKA_WANR');
		     
		     
		        if($BGTWANR == $rsKassendaten->FeldInhalt('FKA_WANR'))
			{
			   echo "MATCHMATCHMATCHMATCHMATCH....\n";
			   
			   $SQL = "Select * from FKASSENDATEN WHERE FKA_DATUM=".$this->_DB->FeldInhaltFormat('DU',$rsTrans->FeldInhalt('DATUM'));
			   $SQL .= " AND FKA_UHRZEIT=".$this->_DB->FeldInhaltFormat('T',$rsTrans->FeldInhalt('ZEIT'));
			   $SQL .= " AND FKA_WANR=".$this->_DB->FeldInhaltFormat('T',$BGTWANR);
			   $SQL .= " AND FKA_MENGE=".$this->_DB->FeldInhaltFormat('NO','-1');
			   $SQL .= " AND FKA_BETRAG=".$this->_DB->FeldInhaltFormat('N2',$rsTrans->FeldInhalt('UMSATZ_BON'));
			   
			   $rsDuplikat = $this->_DB->RecordSetOeffnen($SQL);
			   
			   if($rsDuplikat->AnzahlDatensaetze() == 0)
			   {
				echo "INSERT\n";
				$countInsert++;
			   
				echo "\n";
				echo "Eintrag_Kassendaten\n";
			   
			      if($rsTrans->FeldInhalt('UMSATZ_BON') != 0)
			      {
			        $SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
			        $SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
			        $SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_USER,FKA_USERDAT";
			        $SQL .= ") VALUES (";
			        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','D',false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('FKA_FILID'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$rsTrans->FeldInhalt('DATUM'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsTrans->FeldInhalt('ZEIT'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsTrans->FeldInhalt('BSA'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$BGTWANR,false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('FKA_AEMNR'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','GLAS01',false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('N0','-1',false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsTrans->FeldInhalt('UMSATZ_BON'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsTrans->FeldInhalt('Kfz_Kennzeichen'),false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','',false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',0,false);
				$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',0,True);
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
			   
				if($this->_DB->Ausfuehren($SQL)===false) {
				}
			       }
			   
			        echo $SQL;
				echo "\n";
			   }
			   else
			   {
			       echo "DUPLIKAT\n";
			       //var_dump($rsDuplikat);
			   }
			   
			}
		        echo $count++;
		        
			$rsTrans->DSWeiter();
			
			if($rsTrans->EOF())
			{
			  echo "COUNTDS:".$count;
			}
		     }
		     
		     $rsKassendaten->DSWeiter();		
		}
		
		   
		   $rsGesperrteVorgaenge->DSWeiter();
		}
		
		echo "AnzahlInsert:".$countInsert;
		
		
		$this->pruefeGesperrteAuftraegeErneut();
		$this->Verarbeite();
	}
 
 
        public function entferneAbgeglicheneVorgaengeAusDifferenzListe()
        {

            $SQL = " Select * from FGKOPFDATEN INNER JOIN FGRUECKFUEHRUNG ON fgk_vorgangnr = ffr_vorgangnr ";
            $SQL .= " WHERE (FGK_FGN_KEY = 8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY = 10) AND ffr_status < 2";

            $rsEntferne = $this->_DB->RecordSetOeffnen($SQL);

             while(!$rsEntferne->EOF())
             {

                  $SQL = " UPDATE FGRUECKFUEHRUNG SET FFR_STATUS = 4 ";
                  $SQL .= "WHERE FFR_VORGANGNR=".$rsEntferne->FeldInhalt('FGK_VORGANGNR');

                  if($this->_DB->Ausfuehren($SQL)===false) {
                  }

               $rsEntferne->DSWeiter();
             }

        }


        public function ZugangsUeberpruefung()
	{
		//Neuer Status f�r Dateien die bereits an Zugang �bertragen wurden --> STATUS (16)
		//--------------------------------------------------------------------------------
		
		$SQL =  "Select * from FGINZUGANG INNER JOIN FGKOPFDATEN ON FGI_VORGANGNR = FGK_VORGANGNR";
		$SQL .= " WHERE FGK_FGN_KEY IS NULL";		
		
		$rsBereitsAnZugang = $this->_DB->RecordSetOeffnen($SQL);
		
		while (!$rsBereitsAnZugang->EOF())
		{
			
			$SQL = 'UPDATE FGKOPFDATEN SET ';
                        $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_KENNUNG"),false);
                        $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsBereitsAnZugang->FeldInhalt("FGK_FILID"),false);
                        $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_VORGANGNR"),false);
                        $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_KFZBEZ"),false);
                        $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                        $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_KBANR"),false);
                        $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                        $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                        $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                        $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsBereitsAnZugang->FeldInhalt("FGK_SB"),false);
                        $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsBereitsAnZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                        $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsBereitsAnZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                        $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsBereitsAnZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                        $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                        $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsBereitsAnZugang->FeldInhalt('FGK_IMP_KEY'),true);
                        $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsBereitsAnZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                        $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',16,true);
                        $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsBereitsAnZugang->FeldInhalt('FGK_FCN_KEY'),true);
                        $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                        $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsBereitsAnZugang->FeldInhalt('FGI_USERDAT'),true);
                        $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                        $SQL .= ',FGK_USERDAT=sysdate';
                        $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsBereitsAnZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    	if($this->_DB->Ausfuehren($SQL)===false) {
                        }
            
                        $rsBereitsAnZugang->DSWeiter();

                }
	}
 
 
 
    public function bereitsUebertrageneAuftraege() {
    //Vorgang wurde bereits freigebenen
    //---------------------

        $SQL = 'Select * from FGFREIGABERECHNUNGEN INNER JOIN FGKOPFDATEN ON FGR_VORGANGNR = FGK_VORGANGNR AND FGK_FGN_KEY IS NULL OR FGK_FGN_KEY = 0';
        $rsUebertragen = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsUebertragen->EOF()) {
            $SQL = 'UPDATE FGKOPFDATEN SET ';
            $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_KENNUNG"),false);
            $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsUebertragen->FeldInhalt("FGK_FILID"),false);
            $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_VORGANGNR"),false);
            $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_KFZBEZ"),false);
            $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_KFZKENNZ"),false);
            $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_KBANR"),false);
            $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_FAHRGESTELLNR"),false);
            $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_VERSICHERUNG"),false);
            $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_VERSSCHEINNR"),false);
            $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsUebertragen->FeldInhalt("FGK_SB"),false);
            $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsUebertragen->FeldInhalt("FGK_MONTAGEDATUM"),false);
            $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsUebertragen->FeldInhalt("FGK_ZEITSTEMPEL"),false);
            $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsUebertragen->FeldInhalt("FGK_STEUERSATZ"),false);
            $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt("FGK_KUNDENNAME"),false);
            $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsUebertragen->FeldInhalt('FGK_IMP_KEY'),true);
            $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsUebertragen->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
            $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',15,true);
            $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsUebertragen->FeldInhalt('FGK_FCN_KEY'),true);
            $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt('FGK_FREIGABEGRUND'),true);
            $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsUebertragen->FeldInhalt('FGK_DATUMZUGANG'),true);
            $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
            $SQL .= ',FGK_USERDAT=sysdate';
            $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsUebertragen->FeldInhalt('FGK_VORGANGNR'),false);

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            $rsUebertragen->DSWeiter();
        }
    }

    public function bereitsGesperrteAuftraege() {
        $SQL = 'Select * from FGKOPFDATEN aa WHERE FGK_FREIGABEGRUND IS NULL AND aa.FGK_VORGANGNR IN ';
        $SQL .= '(Select bb.FKH_VORGANGNR FROM FGKOPFDATEN_HIST bb WHERE bb.FKH_FGN_KEY = 6)';

        //echo $SQL;

        $rsGesperrte = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsGesperrte->EOF()) {
            $SQL = 'UPDATE FGKOPFDATEN SET ';
            $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_KENNUNG"),false);
            $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsGesperrte->FeldInhalt("FGK_FILID"),false);
            $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_VORGANGNR"),false);
            $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_KFZBEZ"),false);
            $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_KFZKENNZ"),false);
            $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_KBANR"),false);
            $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_FAHRGESTELLNR"),false);
            $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_VERSICHERUNG"),false);
            $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_VERSSCHEINNR"),false);
            $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsGesperrte->FeldInhalt("FGK_SB"),false);
            $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsGesperrte->FeldInhalt("FGK_MONTAGEDATUM"),false);
            $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsGesperrte->FeldInhalt("FGK_ZEITSTEMPEL"),false);
            $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsGesperrte->FeldInhalt("FGK_STEUERSATZ"),false);
            $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt("FGK_KUNDENNAME"),false);
            $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsGesperrte->FeldInhalt('FGK_IMP_KEY'),true);
            $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsGesperrte->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
            $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',6,true);
            $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsGesperrte->FeldInhalt('FGK_FCN_KEY'),true);
            $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt('FGK_FREIGABEGRUND'),true);
            $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsGesperrte->FeldInhalt('FGK_DATUMZUGANG'),true);
            $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
            $SQL .= ',FGK_USERDAT=sysdate';
            $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsGesperrte->FeldInhalt('FGK_VORGANGNR'),false);

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            $rsGesperrte->DSWeiter();
        }

    }


    public function pruefeGesperrteAuftraegeErneut() {
    //Selektiere alle Gesperrten Auftr�ge
    //Summiere Auftragsdaten und Summiere Kassendaten
    //Errechne Differenz
    //Falls Betrag nicht �bereinstimmt behalten die Daten den Status "6"
    //Wenn Korrekt wird ein neuer Status gesetzt

    //Checke bereits gesperrte Auftr�ge Erneut

        $SQL = 'SELECT * from FGKOPFDATEN WHERE FGK_FGN_KEY = 6';

        $rsVerarbeite = $this->_DB->RecordSetOeffnen($SQL);

        //Erste VorgangsNr
        $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');
        $Betrag = 0;

        while(!$rsVerarbeite->EOF()) {
            $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

            $SQLSUMME = 'Select sum((FGP_ANZAHL*FGP_OEMPREIS)*(1+(FGK_STEUERSATZ/100)))';
            $SQLSUMME .=' AS OEM,Sum((FGP_EKNETTO * FGP_ANZAHL)*(1+(FGK_STEUERSATZ/100))) AS EK';
            $SQLSUMME .=' from FGPOSITIONSDATEN INNER JOIN FGKOPFDATEN';
            $SQLSUMME .=' ON FGK_VORGANGNR = FGP_VORGANGNR WHERE FGK_VORGANGNR ='.$this->_DB->FeldInhaltFormat('T',$VorgangKopfNr,false);

            $rsSumme = $this->_DB->RecordSetOeffnen($SQLSUMME);

            $BetragVorgang = $rsSumme->FeldInhalt('OEM');
            $BetragVorgang = str_replace(",",".",$BetragVorgang);
            $BetragVorgang = round($BetragVorgang,2);

            $BetragVorgang = round($BetragVorgang,2);
            //Ermittle dazugeh�rigen Kassendatensatz

            //echo "<br>";
            //echo "VORGANGBETRAG".$BetragVorgang;
            //echo "<br>";

            $SQLKasse = 'Select FKA_BETRAG from FKASSENDATEN WHERE FKA_AEMNR='.$this->_DB->FeldInhaltFormat('T',$VorgangKopfNr,false);

            //echo $SQLKasse;
            $rsSummeKasse = $this->_DB->RecordSetOeffnen($SQLKasse);
            //R�cknahmen ber�cksichtigen

            if($rsSummeKasse->AnzahlDatensaetze() == 1) {

                $BetragKasse = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);
            }
            else {
                $BetragKasse = 0;

                while(!$rsSummeKasse->EOF()) {
                    $zwBetrag = '';
                    $zwBetrag = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                    $zwBetrag = str_replace(',','.',$zwBetrag);
                    //Check ob Negativbetrag

                    $checkMinusBetrag = 0;
                    $checkMinusBetrag = substr_count($zwBetrag, '-');

                    if($checkMinusBetrag == 0) {
                        $BetragKasse = $BetragKasse + $zwBetrag;
                    //echo "<br>";
                    //echo "Summieren".$BetragKasse;
                    //echo "<br>";
                    }
                    else {
                    //Entferne Minuszeichen
                        $zwBetrag = str_replace('-',"",$zwBetrag);
                        $BetragKasse = $BetragKasse - $zwBetrag;
                    //echo "<br>";
                    //echo "MINUS".$BetragKasse;
                    //echo "<br>";
                    }

                    $rsSummeKasse->DSWeiter();
                }

                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);
            //echo "<br>";
            //echo "KASSENDATEN ERGEBNIS:".$Ergebnis;
            //echo "<br>";
            }


            if($Ergebnis >=0 AND $Ergebnis <= 1) {
            //KANN AN ZUGANG UEBERTRAGEN WERDEN

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',3,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false);

		//echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }

            if($Ergebnis > 1 AND $Ergebnis <= 10) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',3,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false);

		//echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }



            $rsVerarbeite->DSWeiter();
        }
    }



    public function stornierteDatensaetzeErmitteln() {
        $SQL = "Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,";
        $SQL .=" FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,";
        $SQL .=" FGK_STEUERSATZ,FGK_KUNDENNAME,FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,";
        $SQL .=" FGK_FREIGABEGRUND,FGK_DATUMZUGANG,FGK_USER,FGK_USERDAT,FGS_KEY,FGS_KENNUNG,FGS_FILIALNR,FGS_VORGANGNR,";
        $SQL .=" FGS_KFZBEZ,FGS_KFZKENNZ,FGS_KBANR,FGS_FAHRGESTELLNR,FGS_VERSICHERUNG,FGS_VERSSCHEINNR,FGS_SB,";
        $SQL .=" FGS_MONTAGEDATUM,FGS_ZEITSTEMPEL,FGS_STEUERSATZ,FGS_AKTIV,FGS_FGK_KEY,FGS_IMP_KEY,FGS_USER,";
        $SQL .=" FGS_USERDAT from FGKOPFDATEN aa inner join FGSTORNODATEN bb on aa.FGK_VORGANGNR = bb.FGS_VORGANGNR";
        $SQL .=" WHERE FGS_AKTIV <> 0";

        $rsKopfStorno = $this->_DB->RecordSetOeffnen($SQL);

        //Stornodatensaetze setzen

        while (!$rsKopfStorno->EOF()) {    //&& $rsKopfStorno->FeldInhalt('FGS_AKTIV') != 1
            if($rsKopfStorno->FeldInhalt('FGK_FGN_KEY') == null || $rsKopfStorno->FeldInhalt('FGK_FGN_KEY') == null || $rsKopfStorno->FeldInhalt('FGK_FGN_KEY') == 0) {

            //Datensatz auf storniert setzen

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKopfStorno->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKopfStorno->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',1,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {

                }

                //Storno auf AKTIV setzen

                $SQL = 'UPDATE FGSTORNODATEN SET ';
                $SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_KENNUNG"),false);
                $SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt("FGS_FILIALNR"),false);
                $SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_VORGANGNR"),false);
                $SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_KFZBEZ"),false);
                $SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_KFZKENNZ"),false);
                $SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_KBANR"),false);
                $SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_FAHRGESTELLNR"),false);
                $SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_VERSICHERUNG"),false);
                $SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt("FGS_VERSSCHEINNR"),false);
                $SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKopfStorno->FeldInhalt("FGS_SB"),false);
                $SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt("FGS_MONTAGEDATUM"),false);
                $SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKopfStorno->FeldInhalt("FGS_ZEITSTEMPEL"),false);
                $SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKopfStorno->FeldInhalt("FGS_STEUERSATZ"),false);
                $SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat('NO',1,false);
                $SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt('FGK_KEY'),true);
                $SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfStorno->FeldInhalt('FGS_IMP_KEY'),true);
                $SQL .= ',FGS_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGS_USERDAT=sysdate';
                $SQL .= ' WHERE FGS_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKopfStorno->FeldInhalt('FGK_VORGANGNR'),false);


                if($this->_DB->Ausfuehren($SQL)===false) {

                }
            }
            $rsKopfStorno->DSWeiter();
        }

    }

    public function stronierteDatensaetzeMitKassendatenErmitteln() {
    //Stornierte Datensaetze mit Kassendaten ermitteln
    //AKTIV - Kennzeichen setzen

        $SQL='Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,';
        $SQL.='FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME,';
        $SQL.='FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG,FGK_USER,';
        $SQL.='FGK_USERDAT,FKA_KEY,FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,';
        $SQL.='FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS';
        $SQL.=' from AWIS.FGKOPFDATEN inner join AWIS.FKASSENDATEN on FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID';
        $SQL.=' WHERE FGK_FGN_KEY = 1';

    /*
    echo "<br>";
    echo $SQL;
    echo "<br>";
    */

        $rsKassendatenVorhandenUndStorniert =$this->_DB->RecordSetOeffnen($SQL);

        while (!$rsKassendatenVorhandenUndStorniert->EOF()) {    //&& $rsKassendatenVorhandenUndStorniert->FeldInhalt('FGS_AKTIV') != 1
        //Ueberpruefen ob Datensatz storniert ist
            if($rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_FGN_KEY') == 1 ) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',2,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {

                }

            //Stornierte Datensaetze mit Kassendaten (AKTIV - Kennzeichen setzen)
                        /*
                        $SQL = 'UPDATE FGSTORNODATEN SET ';
                        $SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_KENNUNG"),false);
	                $SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_FILIALNR"),false);
	                $SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_VORGANGNR"),false);
	                $SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_KFZBEZ"),false);
	                $SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_KFZKENNZ"),false);
	                $SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_KBANR"),false);
	                $SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_FAHRGESTELLNR"),false);
	                $SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_VERSICHERUNG"),false);
	                $SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_VERSSCHEINNR"),false);
	                $SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat('N',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_SB"),false);
	                $SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('D',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_MONTAGEDATUM"),false);
	                $SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_ZEITSTEMPEL"),false);
	                $SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt("FGS_STEUERSATZ"),false);
	                $SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat('NO',1,false);
	                $SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_KEY'),true);
                        $SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGS_IMP_KEY'),true);
                        $SQL .= ',FGS_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
	                $SQL .= ',FGS_USERDAT=sysdate';
	                $SQL .= ' WHERE FGS_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhandenUndStorniert->FeldInhalt('FGK_VORGANGNR'),false);

                        if($this->_DB->Ausfuehren($SQL)===false)
	                {
                        }
                        */

            }
            $rsKassendatenVorhandenUndStorniert->DSWeiter();
        }



    }


    public function KassendatenErmitteln() {
    //Ermittle Kassendaten die nicht storniert sind - setze Status "3"
    //----------------------------------------------------------------
        /*
	$SQL='Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,';
	$SQL.='FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME,';
	$SQL.='FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG,FGK_USER,';
	$SQL.='FGK_USERDAT,FKA_KEY,FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,';
	$SQL.='FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS';
	$SQL.=' from AWIS.FGKOPFDATEN inner join AWIS.FKASSENDATEN on FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID';
	$SQL.=' WHERE FGK_FGN_KEY IS NULL OR FGK_FGN_KEY <> 1 OR FGK_FGN_KEY <> 2';
        */

        $SQL='Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,';
        $SQL.='FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME,';
        $SQL.='FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG,FGK_USER,';
        $SQL.='FGK_USERDAT,FKA_KEY,FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,';
        $SQL.='FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS';
        $SQL.=' from AWIS.FGKOPFDATEN inner join AWIS.FKASSENDATEN on FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID';
        $SQL.=' WHERE FGK_FGN_KEY IS NULL OR FGK_FGN_KEY = 0';

    /*
    echo "<br>";
    echo "Kassendaten";
    echo $SQL;
    echo "<br>";
    */

        $rsKassendatenVorhanden = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsKassendatenVorhanden->EOF()) {    //&& $rsKassendatenVorhanden->FeldInhalt('FGS_AKTIV') != 1
            if($rsKassendatenVorhanden->FeldInhalt('FGK_FGN_KEY') != 1 OR $rsKassendatenVorhanden->FeldInhalt('FGK_FGN_KEY') != 2) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhanden->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKassendatenVorhanden->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhanden->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhanden->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKassendatenVorhanden->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhanden->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhanden->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',3,true); //Status 3 setzen wenn DS bearbeitet werden sollen
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKassendatenVorhanden->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsKassendatenVorhanden->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKassendatenVorhanden->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {

                }
            }
            $rsKassendatenVorhanden->DSWeiter();
        }
    }



    public function ArtNR_NKKSFAR() {
    //------------------------------------------------------------
    //Fehlende Artnr noch nicht kassiert und kein Storno vorhanden
    //-------------------------------------------------------------
        $SQL ='Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,';
        $SQL .=' FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME,';
        $SQL .=' FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG,';
        $SQL .=' FGK_USER,FGK_USERDAT,FGP_KEY,FGP_KENNUNG,FGP_VORGANGNR,FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,';
        $SQL .=' FGP_OEMPREIS,FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,FGP_USER,FGP_USERDAT,FGL_ARTNR' ;
        $SQL .=' from AWIS.FGKOPFDATEN INNER JOIN AWIS.FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR ';
        $SQL .=' INNER JOIN FGLASSTAMM ON FGP_ARTNR = FGL_ARTNR WHERE FGK_FGN_KEY IS NULL OR FGK_FGN_KEY = 0 AND FGL_ARTNR IS NULL ';
        $SQL .=' ORDER BY FGK_KEY';

        $rsNKKS = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsNKKS->EOF()) {
            if($rsNKKS->FeldInhalt('FGL_ARTNR') == null) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsNKKS->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsNKKS->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsNKKS->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsNKKS->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsNKKS->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNKKS->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsNKKS->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',11,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNKKS->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsNKKS->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsNKKS->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }
            $rsNKKS->DSWeiter();
        }
    }

    function ArtNr_KFAR() {
    //-------------------------------------------
    //Kassiert fehlende Artikelnummer Status "12"
    //-------------------------------------------

        $SQL =  'Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR, ';
        $SQL .= ' FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME, ';
        $SQL .= ' FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG, ';
        $SQL .= ' FGK_USER,FGK_USERDAT,FGP_KEY,FGP_KENNUNG,FGP_VORGANGNR,FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT, ';
        $SQL .= ' FGP_OEMPREIS,FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,FGP_USER,FGP_USERDAT,FGL_ARTNR ';
        $SQL .= ' from AWIS.FGKOPFDATEN INNER JOIN AWIS.FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR ';
        $SQL .= ' INNER JOIN FGLASSTAMM ON FGP_ARTNR = FGL_ARTNR WHERE FGK_FGN_KEY = 3 AND FGL_ARTNR IS NULL ';
        $SQL .= ' ORDER BY FGK_KEY';


        $rsKFAR = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsKFAR->EOF()) {
            if($rsKFAR->FeldInhalt('FGL_ARTNR') == null) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsKFAR->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKFAR->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKFAR->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKFAR->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKFAR->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKFAR->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsKFAR->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',12,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKFAR->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsKFAR->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKFAR->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {

                }
            }
            $rsKFAR->DSWeiter();
        }
    }

    //Kassierter Vorgang mit Fehlender ArtNr und Storniert
    //----------------------------------------------------

    function ArtNr_KFARMS() {
        $SQL =  'Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR, ';
        $SQL .= ' FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME, ';
        $SQL .= ' FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG, ';
        $SQL .= ' FGK_USER,FGK_USERDAT,FGP_KEY,FGP_KENNUNG,FGP_VORGANGNR,FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT, ';
        $SQL .= ' FGP_OEMPREIS,FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,FGP_USER,FGP_USERDAT,FGL_ARTNR ';
        $SQL .= ' from AWIS.FGKOPFDATEN INNER JOIN AWIS.FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR ';
        $SQL .= ' INNER JOIN FGLASSTAMM ON FGP_ARTNR = FGL_ARTNR WHERE FGK_FGN_KEY = 2 AND FGL_ARTNR IS NULL ';
        $SQL .= ' ORDER BY FGK_KEY';


        $rsKFARMS = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsKFARMS->EOF()) {
            if($rsKFARMS->FeldInhalt('FGL_ARTNR') == null) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsKFARMS->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsKFARMS->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsKFARMS->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsKFARMS->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsKFARMS->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKFARMS->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsKFARMS->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',13,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKFARMS->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsKFARMS->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKFARMS->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }
            $rsKFARMS->DSWeiter();
        }

    }

    function ArtNr_FAMS() {
        $SQL =  'Select FGK_KEY,FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR, ';
        $SQL .= 'FGK_VERSICHERUNG,FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,FGK_KUNDENNAME, ';
        $SQL .= 'FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_FREIGABEGRUND,FGK_DATUMZUGANG, ';
        $SQL .= 'FGK_USER,FGK_USERDAT,FGP_KEY,FGP_KENNUNG,FGP_VORGANGNR,FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT, ';
        $SQL .= 'FGP_OEMPREIS,FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,FGP_USER,FGP_USERDAT,FGL_ARTNR ';
        $SQL .= 'from AWIS.FGKOPFDATEN INNER JOIN AWIS.FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR ';
        $SQL .= 'INNER JOIN FGLASSTAMM ON FGP_ARTNR = FGL_ARTNR WHERE FGK_FGN_KEY = 1 AND FGL_ARTNR IS NULL ';
        $SQL .= 'ORDER BY FGK_KEY';

        $rsFAMS = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsFAMS->EOF()) {
            if($rsFAMS->FeldInhalt('FGL_ARTNR') == null) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsFAMS->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsFAMS->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsFAMS->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsFAMS->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsFAMS->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsFAMS->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsFAMS->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',14,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsFAMS->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsFAMS->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsFAMS->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }
            $rsFAMS->DSWeiter();
        }
    }

    function Verarbeite() {
    //$this->_Werkzeug->EMail($TO, $Betreff, $Text);

        $SQL = 'SELECT * from FGKOPFDATEN WHERE FGK_FGN_KEY = 3';

        $rsVerarbeite = $this->_DB->RecordSetOeffnen($SQL);

        

        //Erste VorgangsNr
        $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');
        $Betrag = 0;

        while(!$rsVerarbeite->EOF()) {
            $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

            $SQLSUMME = 'Select sum((FGP_ANZAHL*FGP_OEMPREIS)*(1+(FGK_STEUERSATZ/100)))';
            $SQLSUMME .=' AS OEM,Sum((FGP_EKNETTO * FGP_ANZAHL)*(1+(FGK_STEUERSATZ/100))) AS EK';
            $SQLSUMME .=' from FGPOSITIONSDATEN INNER JOIN FGKOPFDATEN';
            $SQLSUMME .=' ON FGK_VORGANGNR = FGP_VORGANGNR WHERE FGK_VORGANGNR ='.$this->_DB->FeldInhaltFormat('T',$VorgangKopfNr,false);

            $rsSumme = $this->_DB->RecordSetOeffnen($SQLSUMME);

            $BetragVorgang = $rsSumme->FeldInhalt('OEM');
            $BetragVorgang = str_replace(",",".",$BetragVorgang);
            $BetragVorgang = round($BetragVorgang,2);

            $BetragVorgang = round($BetragVorgang,2);
            //Ermittle dazugeh�rigen Kassendatensatz

            //echo "<br>";
            //echo "VORGANGBETRAG".$BetragVorgang;
            //echo "<br>";

            $SQLKasse = 'Select FKA_BETRAG from FKASSENDATEN WHERE FKA_AEMNR='.$this->_DB->FeldInhaltFormat('T',$VorgangKopfNr,false);

            //echo $SQLKasse;
            $rsSummeKasse = $this->_DB->RecordSetOeffnen($SQLKasse);
            //R�cknahmen ber�cksichtigen

            if($rsSummeKasse->AnzahlDatensaetze() == 1) {

                $BetragKasse = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);
            }
            else {
                $BetragKasse = 0;

                while(!$rsSummeKasse->EOF()) {
                    $zwBetrag = '';
                    $zwBetrag = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                    $zwBetrag = str_replace(',','.',$zwBetrag);
                    
                    $checkMinusBetrag = 0;
                    $checkMinusBetrag = substr_count($zwBetrag, '-');

                    if($checkMinusBetrag == 0) {
                        $BetragKasse = $BetragKasse + $zwBetrag;
                    
                    }
                    else {
                        $zwBetrag = str_replace('-',"",$zwBetrag);
                        $BetragKasse = $BetragKasse - $zwBetrag;
                    }

                    $rsSummeKasse->DSWeiter();
                }

                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);
            
            }


            if($Ergebnis >=0 AND $Ergebnis <= 1) {

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',4,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false);

                //echo "</br>1-10</br>";
                //echo $SQL;


                if($this->_DB->Ausfuehren($SQL)===false) {
			throw new awisException('FEHLER1','',$SQL,2);
                }



                if($Ergebnis == 0) {
                    $Differenz = $BetragVorgang - $BetragKasse;

                    //Schreibe Positionsdatensatz ins DWH
                    //--------------------------------------------------

                    $SQL = "Select * from FGPOSITIONSDATEN inner join FGKOPFDATEN ON FGP_VORGANGNR = FGK_VORGANGNR ";
                    $SQL .=" WHERE FGP_VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

                    $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

                    $SQL =  "Select VORGANGNR, ARTNR, ARTBEZ, ANZAHL,EINHEIT, ";
                    $SQL .= " OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG, AUSBUCHUNG ";
                    $SQL .= " from DWH.GLASPOS@DWH WHERE VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');
	            
		    $rsPruefeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

                    if($rsPruefeDuplikate->AnzahlDatensaetze() == 0) {
                        while(!$rsAnreicherung->EOF()) {

                            $Brutto = 0;
                            $OemPreis = str_replace(',', '.', $rsAnreicherung->FeldInhalt('FGP_OEMPREIS'));
                            $Brutto = ($OemPreis * ('1.'.$rsAnreicherung->FeldInhalt('FGK_STEUERSATZ')));

                            $Brutto = str_replace('.', ',', $Brutto);

                            $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
                            $SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
                            $SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
                            //$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
			    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
                            $SQL .= ')';

                            if($this->_DB->Ausfuehren($SQL)===false) {
				throw new awisException('FEHLER2','',$SQL,2);
                            }


                            $rsAnreicherung->DSWeiter();
                        }

                    }
                }
                elseif($Ergebnis <= 1) {
                    $countEintraege = 0;

                    $Differenz = $BetragVorgang - $BetragKasse;

                    $SQL = "Select * from FGPOSITIONSDATEN inner join FGKOPFDATEN ON FGP_VORGANGNR = FGK_VORGANGNR ";
                    $SQL .=" WHERE FGP_VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

                    $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

                    $SQL =  "Select VORGANGNR, ARTNR, ARTBEZ, ANZAHL,EINHEIT,";
                    $SQL .= " OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG, AUSBUCHUNG";
                    $SQL .= " from DWH.GLASPOS@DWH WHERE VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

                    $rsPruefeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

                    if($rsPruefeDuplikate->AnzahlDatensaetze() == 0) {
                        $countEintraege = 0;

                        while(!$rsAnreicherung->EOF()) {

                            $Brutto = 0;
                            $OemPreis = str_replace(',', '.', $rsAnreicherung->FeldInhalt('FGP_OEMPREIS'));
                            $Brutto = ($OemPreis * ('1.'.$rsAnreicherung->FeldInhalt('FGK_STEUERSATZ')));


                            $Brutto = str_replace('.', ',', $Brutto);

                            $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
                            $SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
                            $SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
                            //$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
			    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
                            $SQL .= ')';
			    
                            if($this->_DB->Ausfuehren($SQL)===false) {
				throw new awisException('FEHLER1','',$SQL,3);
                            }

                            if($countEintraege == 0) {

                                $SQL = "Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=".$rsAnreicherung->FeldInhalt('FGP_VORGANGNR');

                                $rsMwst = $this->_DB->RecordSetOeffnen($SQL);

                                $Mwst = 0;
                                $Mwst = '1'.$rsMwst->FeldInhalt('FGK_STEUERSATZ');

                                $Netto = 0.0;
                                $MINUSZEICHEN = '-';

                                if (stripos($Differenz, '-') == false)
                                {
                                    $Netto = $Differenz / $Mwst * 100;
                                }
                                else
                                {
                                    //Minuszeichen hinzufuegen
                                    $Differenz = str_replace('-', '', $Differenz);
                                    $Netto = $Differenz / $Mwst * 100;
                                    $Netto = $MINUSZEICHEN.$Netto;
                                }

                                $Netto = $Differenz / $Mwst * 100;

                                $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
				$SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
				$SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
				//$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
				$SQL .= ')';

                                if($this->_DB->Ausfuehren($SQL)===false) {
					throw new awisException('FEHLER1','',$SQL,4);
                                }

                                $countEintraege++;
                            }


                            $rsAnreicherung->DSWeiter();
                        }

                    //Schreibe Differenz
                    }
                }
            }

            if($Ergebnis > 1 AND $Ergebnis <= 10) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',5,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false);

                //echo "</br>10</br>";
                //echo $SQL;


                if($this->_DB->Ausfuehren($SQL)===false) {
		     throw new awisException('FEHLER1','',$SQL,5);
                }

                if ($Ergebnis <= 10) {
                    $countEintraege = 0;

                    $Differenz = $BetragVorgang - $BetragKasse;

                    $SQL = "Select * from FGPOSITIONSDATEN inner join FGKOPFDATEN ON FGP_VORGANGNR = FGK_VORGANGNR ";
                    $SQL .=" WHERE FGP_VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

                    $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

                    $SQL =  "Select VORGANGNR, ARTNR, ARTBEZ, ANZAHL,EINHEIT,";
                    $SQL .= " OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG, AUSBUCHUNG";
                    $SQL .= " from DWH.GLASPOS@DWH WHERE VORGANGNR=".$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

                    $rsPruefeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

                    if($rsPruefeDuplikate->AnzahlDatensaetze() == 0) {
                        $countEintraege = 0;

                        while(!$rsAnreicherung->EOF()) {

                            $Brutto = 0;
                            $OemPreis = str_replace(',', '.', $rsAnreicherung->FeldInhalt('FGP_OEMPREIS'));
                            $Brutto = ($OemPreis * ('1.'.$rsAnreicherung->FeldInhalt('FGK_STEUERSATZ')));


                            $Brutto = str_replace('.', ',', $Brutto);

                            $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
                            $SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
                            $SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
                            //$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
			    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
                            $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
                            $SQL .= ')';

                            if($this->_DB->Ausfuehren($SQL)===false) {
			       throw new awisException('FEHLER1','',$SQL,6);
                            }

                            if($countEintraege == 0) {
                                $SQL = "Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=".$rsAnreicherung->FeldInhalt('FGP_VORGANGNR');

                                $rsMwst = $this->_DB->RecordSetOeffnen($SQL);

                                $Mwst = 0;
                                $Mwst = '1'.$rsMwst->FeldInhalt('FGK_STEUERSATZ');

                                $Netto = 0.0;
                                $MINUSZEICHEN = '-';

                                if (stripos($Differenz, '-') == false)
                                {
                                    $Netto = $Differenz / $Mwst * 100;
                                }
                                else
                                {
                                    //Minuszeichen hinzufuegen
                                    $Differenz = str_replace('-', '', $Differenz);
                                    $Netto = $Differenz / $Mwst * 100;
                                    $Netto = $MINUSZEICHEN.$Netto;
                                }

                                $Netto = $Differenz / $Mwst * 100;

                                $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
				$SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
				$SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
				//$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
				$SQL .= ')';


                                if($this->_DB->Ausfuehren($SQL)===false) {
				  throw new awisException('FEHLER1','',$SQL,7);
                                }

                                $countEintraege++;
                            }

                            $rsAnreicherung->DSWeiter();
                        }

                    }
                }
            }

            if($Ergebnis > 10) {
            //Status auf gesperrt setzen
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsVerarbeite->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',6,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsVerarbeite->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsVerarbeite->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false);

                //echo "</br>>10</br>";
                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
		  throw new awisException('FEHLER1','',$SQL,8);
                }


                //Sende E-MAIL AN FILIALE - ANPASSEN
                //------------------------------------------------------------------

                $Text  = "Betragsdifferenz > 10 Euro </br>";
                $Text .= "VORGANGSNR =". $rsVerarbeite->FeldInhalt('FGK_VORGANGNR')."</br>";
                $Text .= "FILIALNR =". $rsVerarbeite->FeldInhalt('FGK_FILID')."</br>";
                $Text .= "BETRAG_KASSE =". $BetragKasse."</br>";
                $Text .= "BETRAG_FIRSTGLAS =". $BetragVorgang."</br>";
                //$this->_Werkzeug->EMail($TO, $Betreff, $Text)
                //$this->_Werkzeug->EMail('Christian.Beierl@de.atu.eu','Firstglas - Differenz', $Text);

                $Differenz = $BetragKasse - $BetragVorgang;
                //$Differenz = $BetragVorgang - $BetragKasse;

            /*
            $BetragVorgang = str_replace('.', ',', $BetragVorgang);
            $BetragKasse = str_replace('.',',',$BetragKasse);
            $Differenz = str_replace('.',',',$Differenz);
            */

                $SQL = 'Select * from FKASSENDATEN WHERE FKA_AEMNR = '.$rsVerarbeite->FeldInhalt('FGK_VORGANGNR').'AND FKA_FILID ='.$rsVerarbeite->FeldInhalt('FGK_FILID');
                $rsRueckFuehrung = $this->_DB->RecordSetOeffnen($SQL);

                $SQL  = 'INSERT INTO FGRUECKFUEHRUNG (FFR_FILID,FFR_WANR,FFR_VORGANGNR,FFR_KFZKENNZ,FFR_AEMBETRAG,FFR_KASSENBETRAG,FFR_DIFFERENZ,FFR_STATUS,FFR_DIFFERENZDATUM,FFR_MAILVERSENDET,FFR_USER,FFR_USERDAT) VALUES(';
                $SQL .= $rsVerarbeite->FeldInhalt('FGK_FILID').','.$rsRueckFuehrung->FeldInhalt('FKA_WANR').','.$this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_VORGANGNR'),false).','.$this->_DB->FeldInhaltFormat('T',$rsVerarbeite->FeldInhalt('FGK_KFZKENNZ'),false).','.$BetragVorgang.','.$BetragKasse.','.$Differenz.',0'.',sysdate'.','.$this->_DB->FeldInhaltFormat('NO',0,false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate)';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
			throw new awisException('FEHLER1','',$SQL,9);
                }

            }

            $rsVerarbeite->DSWeiter();
        }
    }

    //-------------------------------------------------------------
    //Vorg�nge die den Status 11,12,13,14 haben wieder zur�cksetzen
    //-------------------------------------------------------------

    function ARTNR_EZDVZ() {
        $SQL = "Select * from FGKOPFDATEN WHERE ";
        $SQL .= "FGK_FGN_KEY = 11 OR FGK_FGN_KEY = 12 OR FGK_FGN_KEY = 13 ";
        $SQL .= " OR FGK_FGN_KEY = 14";

        $rsSetzeZurueck = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsSetzeZurueck->EOF()) {
            if($rsSetzeZurueck->FeldInhalt('FGL_ARTNR') == null) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsSetzeZurueck->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsSetzeZurueck->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsSetzeZurueck->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsSetzeZurueck->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsSetzeZurueck->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSetzeZurueck->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsSetzeZurueck->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',null,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSetzeZurueck->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt('FGK_FREIGABEGRUND'),true);
                $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU',$rsSetzeZurueck->FeldInhalt('FGK_DATUMZUGANG'),true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsSetzeZurueck->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            $rsSetzeZurueck->DSWeiter();
        }

    }

     public function ital_Filialen_Ermitteln() {
    //Italienische Filialen ermitteln
    //--------------------------------------

        $SQL = 'Select * from FGKOPFDATEN INNER JOIN FILIALEN ON FGK_FILID = FIL_ID INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID ';
        $SQL .= 'AND FIL_LAN_WWSKENN ='.$this->_DB->FeldInhaltFormat('T', 'ITA',false).' WHERE FGK_FGN_KEY=4 OR FGK_FGN_KEY=5 OR FGK_FGN_KEY=7';

        $rsITA = $this->_DB->RecordSetOeffnen($SQL);

        //var_dump($rsITA);
        //die();
        //Setze f�r die Italienischen Filialen Status '100'

        while (!$rsITA->EOF()) {
            if($rsITA->FeldInhalt('FGK_FGN_KEY') == 4) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Italienische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

                //Schreibe Daten in die Tabelle FGFREIGABERECHNUNGEN
                //--------------------------------------------------

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsITA->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }




            }
            elseif($rsITA->FeldInhalt('FGK_FGN_KEY') == 5) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Italienische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsITA->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsITA->FeldInhalt('FGK_FGN_KEY') == 7) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsITA->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsITA->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Italienische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsITA->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsITA->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsITA->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }

            $rsITA->DSWeiter();
        }
    }


     function oes_Filialen_Ermitteln() {

        $SQL = 'Select * from FGKOPFDATEN INNER JOIN FILIALEN ON FGK_FILID = FIL_ID INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID ';
        $SQL .= 'AND FIL_LAN_WWSKENN ='.$this->_DB->FeldInhaltFormat('T', 'OES', false).' WHERE FGK_FGN_KEY=4 OR FGK_FGN_KEY=5 OR FGK_FGN_KEY=7';

        //var_dump($SQL);
        //echo $SQL;

        $rsOES = $this->_DB->RecordSetOeffnen($SQL);

        //Setze f�r die Italienischen Filialen Status '100'

        while (!$rsOES->EOF()) {
            if($rsOES->FeldInhalt('FGK_FGN_KEY') == 4) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','�sterreichische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

                //Schreibe Daten in die Tabelle FGFREIGABERECHNUNGEN
                //--------------------------------------------------

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsOES->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }




            }
            elseif($rsOES->FeldInhalt('FGK_FGN_KEY') == 5) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','�sterreichische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FGK_VORGANGNR'),false);
                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsOES->FeldInhalt('FGK_FGN_KEY') == 7) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsOES->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsOES->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','�sterreichische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FGK_VORGANGNR'),false);
                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsOES->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsOES->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }

            $rsOES->DSWeiter();
        }


    }

     function cze_Filialen_Ermitteln() {

    //Evtl. Duplikat Entfernungs - Routine

        $SQL = 'Select * from FGKOPFDATEN INNER JOIN FILIALEN ON FGK_FILID = FIL_ID INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID ';
        $SQL .= 'AND FIL_LAN_WWSKENN ='.$this->_DB->FeldInhaltFormat('T', 'CZE', false).' WHERE FGK_FGN_KEY=4 OR FGK_FGN_KEY=5 OR FGK_FGN_KEY=7';

        $rsCZE = $this->_DB->RecordSetOeffnen($SQL);


        //Setze f�r die Italienischen Filialen Status '100'

        while (!$rsCZE->EOF()) {
            if($rsCZE->FeldInhalt('FGK_FGN_KEY') == 4) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Tschechische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

                //Schreibe Daten in die Tabelle FGFREIGABERECHNUNGEN
                //--------------------------------------------------

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsCZE->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }




            }
            elseif($rsCZE->FeldInhalt('FGK_FGN_KEY') == 5) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Tschechische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsCZE->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsCZE->FeldInhalt('FGK_FGN_KEY') == 7) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsCZE->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsCZE->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Tschechische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsCZE->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsCZE->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsCZE->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }

            $rsCZE->DSWeiter();
        }

    }

     function ned_Filialen_Ermitteln() {

        $SQL = 'Select * from FGKOPFDATEN INNER JOIN FILIALEN ON FGK_FILID = FIL_ID INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID ';
        $SQL .= 'AND FIL_LAN_WWSKENN ='.$this->_DB->FeldInhaltFormat('T', 'NED', false).' WHERE FGK_FGN_KEY=4 OR FGK_FGN_KEY=5 OR FGK_FGN_KEY=7';

        $rsNED = $this->_DB->RecordSetOeffnen($SQL);

        //Setze f�r die Italienischen Filialen Status '100'

        while (!$rsNED->EOF()) {
            if($rsNED->FeldInhalt('FGK_FGN_KEY') == 4) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Niederl�ndische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

                //Schreibe Daten in die Tabelle FGFREIGABERECHNUNGEN
                //--------------------------------------------------

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsNED->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }




            }
            elseif($rsNED->FeldInhalt('FGK_FGN_KEY') == 5) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Niederl�ndische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsNED->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsNED->FeldInhalt('FGK_FGN_KEY') == 7) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsNED->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsNED->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Niederl�ndische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T', $rsNED->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsNED->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsNED->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }
            $rsNED->DSWeiter();
        }


    }

     function sui_Filialen_Ermitteln() {

        $SQL = 'Select * from FGKOPFDATEN INNER JOIN FILIALEN ON FGK_FILID = FIL_ID INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR AND FGK_FILID = FKA_FILID ';
        $SQL .= 'AND FIL_LAN_WWSKENN ='.$this->_DB->FeldInhaltFormat('T', 'SUI', false).' WHERE FGK_FGN_KEY=4 OR FGK_FGN_KEY=5 OR FGK_FGN_KEY=7';

        $rsSUI = $this->_DB->RecordSetOeffnen($SQL);

        //Setze f�r die Italienischen Filialen Status '100'

        while (!$rsSUI->EOF()) {
            if($rsSUI->FeldInhalt('FGK_FGN_KEY') == 4) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Schweizerische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

                //Schreibe Daten in die Tabelle FGFREIGABERECHNUNGEN
                //--------------------------------------------------

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsSUI->FeldInhalt('FGK_FGN_KEY') == 5) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Schweizerische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }
            elseif($rsSUI->FeldInhalt('FGK_FGN_KEY') == 7) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KENNUNG"),false);
                $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt("FGK_FILID"),false);
                $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VORGANGNR"),false);
                $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZBEZ"),false);
                $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KFZKENNZ"),false);
                $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KBANR"),false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSICHERUNG"),false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_VERSSCHEINNR"),false);
                $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_SB"),false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_MONTAGEDATUM"),false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsSUI->FeldInhalt("FGK_STEUERSATZ"),false);
                $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt("FGK_KUNDENNAME"),false);
                $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_IMP_KEY'),true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsSUI->FeldInhalt('FGK_FCN_KEY'),true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T','Schweizerische Filialen werden nicht an Zugang uebertragen',true);
                $SQL .= ',FGK_DATUMZUGANG=sysdate';
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR'),false);

                $SQL = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsSUI->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsSUI->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                //echo $SQL;

                if($this->_DB->Ausfuehren($SQL)===false) {
                }

            }

            $rsSUI->DSWeiter();
        }

    }

    function Zugang() {

        $SQL =  'Select * from FGKOPFDATEN WHERE FGK_FGN_KEY = 4  OR FGK_FGN_KEY = 5 OR FGK_FGN_KEY = 7';

	//echo $SQL;

        //�ffne RecordSet

        $rsZugang = '';
        $rsZugang = $this->_DB->RecordSetOeffnen($SQL);

        $Number = '';

        //$DateTime = date('Ymd H:i:s');
        //$DateTime = str_replace(" ", "_", $DateTime);
        //$DateTime = str_replace(":","_",$DateTime);

        $SQL = "Select * from FGLFDNREXPORT WHERE FGX_BEZEICHNUNG='LFDNR_ZUGANG'";

        $rsLFDNR = $this->_DB->RecordSetOeffnen($SQL);

        $Number = $rsLFDNR->FeldInhalt('FGX_WERT');

        $FileName = 'glas_auftrag_';

        $FileName = $FileName.$Number.".dat";

        $Number++;

        $SQL = "UPDATE FGLFDNREXPORT SET FGX_WERT =".$Number;
        $SQL .= " WHERE FGX_BEZEICHNUNG='LFDNR_ZUGANG'";

        $this->_ZugangsDatei = $FileName;

        if($this->_DB->Ausfuehren($SQL)===false) {
        }

  /*
  echo "<br>";
  echo "AKTUELLER DATEINAME".$FileName;
  echo "<br>";
  */
        $count = 0;
        while(!$rsZugang->EOF()) {
            	
            $this->_Funktionen->SchreibeModulStatus("setStatus","Zugang".$count);
            $SQLCHECK = "Select * from FGZUGANG WHERE FGZ_EXPFELD3 =".$this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

            $rsCheck = $this->_DB->RecordSetOeffnen($SQLCHECK);
            
            if($rsCheck->AnzahlDatensaetze() == 0) {
                $SQL =  'INSERT INTO FGZUGANG(FGZ_EXPFELD1, FGZ_EXPFELD2, FGZ_EXPFELD3, FGZ_EXPFELD4,';
                $SQL .= 'FGZ_EXPFELD5, FGZ_EXPFELD6, FGZ_EXPFELD7, FGZ_EXPFELD8, FGZ_EXPFELD9, FGZ_EXPFELD10';
                $SQL .= ',FGZ_EXPFELD11, FGZ_EXPFELD12, FGZ_EXPFELD13,FGZ_FIRSTGLASEXPORT,FGZ_USER,FGZ_USERDAT) VALUES (';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_KENNUNG'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FILID'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_KFZBEZ'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_KFZKENNZ'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_KBANR'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FAHRGESTELLNR'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VERSICHERUNG'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VERSSCHEINNR'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt('FGK_SB'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_MONTAGEDATUM'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt('FGK_STEUERSATZ'),false).', ';
                $SQL .= $this->_DB->FeldInhaltFormat('NO',0,false).', ';
		
		//$SQL .= $this->_DB->FeldInhaltFormat('T',$FileName,false).', ';
                $SQL .= '\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
		
		if($this->_DB->Ausfuehren($SQL)===false) {
                }

                $Montagedatum = $rsZugang->FeldInhalt('FGK_MONTAGEDATUM');

		echo "MONTAGEDATUM".$Montagedatum."\r \n";
		echo "TIMEWERT".strtotime($Montagedatum)."\r \n";
		echo "VERGLEICH=".strtotime("01.07.2008")."\r \n";

                if(strtotime($Montagedatum) < strtotime("01.07.2008")) {
                //Selektiere Positionsdaten und schreibe Positionssaetze in die Tabzugang
                //Alle
		
                    $SQL = 'Select * from FGKOPFDATEN inner join FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR WHERE FGK_VORGANGNR='.$this->_DB->FeldInhaltFormat('T', $rsZugang->FeldInhalt('FGK_VORGANGNR'), false);
		    $rsPos = $this->_DB->RecordSetOeffnen($SQL);

                    while(!$rsPos->EOF()) {
                    //AEMNR eintragen f�r sauberen Export

                        $SQL =  'INSERT INTO FGZUGANG(FGZ_EXPFELD1, FGZ_EXPFELD2, FGZ_EXPFELD3, FGZ_EXPFELD4,';
                        $SQL .= 'FGZ_EXPFELD5, FGZ_EXPFELD6, FGZ_EXPFELD7, FGZ_EXPFELD8, FGZ_EXPFELD9,';
                        $SQL .= 'FGZ_POSAEMNR,FGZ_FIRSTGLASEXPORT) VALUES (';
                        $SQL .= $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_KENNUNG'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_VORGANGNR'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('NO',$rsPos->FeldInhalt('FGP_ARTNR'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_ARTBEZ'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_ANZAHL'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_EINHEIT'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_OEMPREIS'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_EKNETTO'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('DU',$rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGK_VORGANGNR'),false).', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('NO',0,false).' ';
                        $SQL .= ')';

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }

                        $rsPos->DSWeiter();
                    }
                }
                else {
		
                    $SQL =  'INSERT INTO FGZUGANG(FGZ_EXPFELD1, FGZ_EXPFELD2, FGZ_EXPFELD3, FGZ_EXPFELD4,';
                    $SQL .= 'FGZ_EXPFELD5, FGZ_EXPFELD6, FGZ_EXPFELD7, FGZ_EXPFELD8, FGZ_EXPFELD9, FGZ_POSAEMNR,FGZ_FIRSTGLASEXPORT';
                    $SQL .= ' ) VALUES (';
                    $SQL .= $this->_DB->FeldInhaltFormat('T','P',false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false).', ';
		    $SQL .= $this->_DB->FeldInhaltFormat('NO',700000,false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T','Pauschal',false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2','1',false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T','STUECK',false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',210,false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',210,false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('NO',0,false).' ';
                    $SQL .= ')';

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                //echo $SQL;

                }

                if($rsZugang->FeldInhalt('FGK_FGN_KEY') == 4) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                    $SQLKASSENDATEN = 'Select * from FKASSENDATEN WHERE FKA_AEMNR='.$this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);
                    $rsKassendaten = $this->_DB->RecordSetOeffnen($SQLKASSENDATEN);

                    $SQLKASSENDATEN = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                    $SQLKASSENDATEN .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                    if($this->_DB->Ausfuehren($SQLKASSENDATEN)===false) {
                    }

                //Status 8 kleiner < 1
                }
                elseif($rsZugang->FeldInhalt('FGK_FGN_KEY') == 5) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                    $SQLKASSENDATEN = 'Select * from FKASSENDATEN WHERE FKA_AEMNR='.$this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);
                    $rsKassendaten = $this->_DB->RecordSetOeffnen($SQLKASSENDATEN);

                    $SQLKASSENDATEN = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                    $SQLKASSENDATEN .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),true).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                    if($this->_DB->Ausfuehren($SQLKASSENDATEN)===false) {
                    }

                //Status 9 unter < 10
                }
                elseif($rsZugang->FeldInhalt('FGK_FGN_KEY') == 7) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                    $SQLKASSENDATEN = 'Select * from FKASSENDATEN WHERE FKA_AEMNR='.$this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);
                    $rsKassendaten = $this->_DB->RecordSetOeffnen($SQLKASSENDATEN);

                    $SQLKASSENDATEN = 'INSERT INTO FGFREIGABERECHNUNGEN (FGR_VORGANGNR,FGR_KASSIERDATUM,FGR_USER,FGR_USERDAT,FGR_SELBSTZAHLER) VALUES (';
                    $SQLKASSENDATEN .= $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR')).','.$this->_DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('FKA_DATUM'),false).','.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName(),false).',sysdate,'.$this->_DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('FKA_AEM_VERS_KZ'),false).')';

                    if($this->_DB->Ausfuehren($SQLKASSENDATEN)===false) {
                    }
                }


                $count++;
                //$rsCheck->DSWeiter();
            }
            else {

                    if($rsZugang->FeldInhalt('FGK_FGN_KEY') == 4) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',8,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                //Status 8 kleiner < 1
                }
                elseif($rsZugang->FeldInhalt('FGK_FGN_KEY') == 5) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',9,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                    

                //Status 9 unter < 10
                }
                elseif($rsZugang->FeldInhalt('FGK_FGN_KEY') == 7) {

                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KENNUNG"),false);
                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt("FGK_FILID"),false);
                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VORGANGNR"),false);
                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZBEZ"),false);
                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KFZKENNZ"),false);
                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KBANR"),false);
                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_FAHRGESTELLNR"),false);
                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSICHERUNG"),false);
                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_VERSSCHEINNR"),false);
                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_SB"),false);
                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_MONTAGEDATUM"),false);
                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt("FGK_ZEITSTEMPEL"),false);
                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2',$rsZugang->FeldInhalt("FGK_STEUERSATZ"),false);
                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGK_KUNDENNAME"),false);
                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_IMP_KEY'),true);
                    $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU',$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                    $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',10,true);
                    $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGK_FCN_KEY'),true);
                    $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_FREIGABEGRUND'),true);
                    $SQL .= ',FGK_DATUMZUGANG=sysdate';
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

            }
           }

            $rsZugang->DSWeiter();
    }//�berspringe DS wenn dieser bereits an Zugang �bertragen wurde


    //Wenn DATUM MITTWOCH EXPORTIERE DATEI
    //Ist EXPORTIERT SETZE STATUS 10


    }

     function verarbeiteStatusSieben() {

        

        $VorgangKopfNr='';
        $Differenz = 0;

        $SQL = 'SELECT * from FGKOPFDATEN WHERE FGK_FGN_KEY = 10';

        $rsVerarbeite = $this->_DB->RecordSetOeffnen($SQL);

        $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');
        $Betrag = 0;

        while(!$rsVerarbeite->EOF()) {
            $VorgangKopfNr = $rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

         if($VorgangKopfNr != '')
         {
            $SQLSUMME = 'Select sum((FGP_ANZAHL*FGP_OEMPREIS)*(1+(FGK_STEUERSATZ/100)))';
            $SQLSUMME .=' AS OEM,Sum((FGP_EKNETTO * FGP_ANZAHL)*(1+(FGK_STEUERSATZ/100))) AS EK';
            $SQLSUMME .=' from FGPOSITIONSDATEN INNER JOIN FGKOPFDATEN';
            $SQLSUMME .=' ON FGK_VORGANGNR = FGP_VORGANGNR WHERE FGK_VORGANGNR ='.$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

            $rsSumme = $this->_DB->RecordSetOeffnen($SQLSUMME);

            $BetragVorgang = $rsSumme->FeldInhalt('OEM');
            $BetragVorgang = str_replace(",",".",$BetragVorgang);
            $BetragVorgang = round($BetragVorgang,2);

            $BetragVorgang = round($BetragVorgang,2);

            $SQLKasse = 'Select FKA_BETRAG from FKASSENDATEN WHERE FKA_AEMNR='.$rsVerarbeite->FeldInhalt('FGK_VORGANGNR');

            $rsSummeKasse = $this->_DB->RecordSetOeffnen($SQLKasse);

            if($rsSummeKasse->AnzahlDatensaetze() == 1) {

                $BetragKasse = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);

                $Differenz = $BetragKasse - $BetragVorgang;
            }
            else {
                $BetragKasse = 0;

                while(!$rsSummeKasse->EOF()) {
                    $zwBetrag = '';
                    $zwBetrag = $rsSummeKasse->FeldInhalt('FKA_BETRAG');
                    $zwBetrag = str_replace(',','.',$zwBetrag);
                    //Check ob Negativbetrag

                    $checkMinusBetrag = 0;
                    $checkMinusBetrag = substr_count($zwBetrag, '-');

                    if($checkMinusBetrag == 0) {
                        $BetragKasse = $BetragKasse + $zwBetrag;

                    }
                    else {
                    //Entferne Minuszeichen
                        $zwBetrag = str_replace('-',"",$zwBetrag);
                        $BetragKasse = $BetragKasse - $zwBetrag;

                    }

                    $rsSummeKasse->DSWeiter();
                }

                $BetragKasse = str_replace(",",".",$BetragKasse);
                $BetragKasse = round($BetragKasse,2);

                $Ergebnis = $BetragKasse - $BetragVorgang;
                $Ergebnis = str_replace('-',"",$Ergebnis);

                $Differenz = $BetragKasse - $BetragVorgang;
            }

            $countEintraege = 0;

            $SQL = "Select * from FGFREIGABERECHNUNGEN WHERE FGR_VORGANGNR=".$VorgangKopfNr;

            $rsFreigabe = $this->_DB->RecordSetOeffnen($SQL);

            $SQL = "Select * from FGPOSITIONSDATEN inner join FGKOPFDATEN ON FGP_VORGANGNR = FGK_VORGANGNR ";
            $SQL .=" WHERE FGP_VORGANGNR=".$VorgangKopfNr;

            $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

            $SQL =  "Select VORGANGNR, ARTNR, ARTBEZ, ANZAHL,EINHEIT,";
            $SQL .= " OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG, AUSBUCHUNG";
            $SQL .= " from DWH.GLASPOS@DWH WHERE VORGANGNR=".$VorgangKopfNr;


            $rsPruefeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

            if($rsPruefeDuplikate->AnzahlDatensaetze() == 0) {
                $countEintraege = 0;

                while(!$rsAnreicherung->EOF()) {

                    $Brutto = 0;
                    $OemPreis = str_replace(',', '.', $rsAnreicherung->FeldInhalt('FGP_OEMPREIS'));
                    $Brutto = ($OemPreis * ('1.'.$rsAnreicherung->FeldInhalt('FGK_STEUERSATZ')));


                    $Brutto = str_replace('.', ',', $Brutto);

                    $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
                    $SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
                    $SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
                    $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
                    //$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
		    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
                    $SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
                    $SQL .= ')';

                    
                    if($this->_DB->Ausfuehren($SQL)===false) {
                        throw new awisException('FEHLER2','',$SQL,4);
                    }

                   if($countEintraege == 0) {

                                $SQL = "Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=".$rsAnreicherung->FeldInhalt('FGP_VORGANGNR');

                                $rsMwst = $this->_DB->RecordSetOeffnen($SQL);

                                $Mwst = 0;
                                $Mwst = '1'.$rsMwst->FeldInhalt('FGK_STEUERSATZ');

                                $Netto = 0.0;
                                $MINUSZEICHEN = '-';

                                if (stripos($Differenz, '-') == false)
                                {
                                    $Netto = $Differenz / $Mwst * 100;
                                }
                                else
                                {
                                    //Minuszeichen hinzufuegen
                                    $Differenz = str_replace('-', '', $Differenz);
                                    $Netto = $Differenz / $Mwst * 100;
                                    $Netto = $MINUSZEICHEN.$Netto;
                                }

                                $Netto = $Differenz / $Mwst * 100;

                                $SQL =  'INSERT INTO DWH.GLASPOS@DWH(VORGANGNR, ARTNR, ARTBEZ, ANZAHL,';
				$SQL .= 'EINHEIT, OEMPREIS_NETTO, OEMPREIS_BRUTTO, EKNETTO, ZEITSTEMPEL, FREMDWAEHRUNG,';
				$SQL .= 'AUSBUCHUNG,DIFFORGPALLGREGEL,VERCLIENT,GLASMREGEL) VALUES (';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VORGANGNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTNR'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_ARTBEZ'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_ANZAHL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_EINHEIT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_OEMPREIS'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$Brutto,false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_EKNETTO'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('DU', $rsAnreicherung->FeldInhalt('FGP_ZEITSTEMPEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N0',0,false).', ';
				//$SQL .= $this->_DB->FeldInhaltFormat('N2',0,false).' ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPGLASMREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_DIFFORGPALLGREGEL'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('T', $rsAnreicherung->FeldInhalt('FGP_VERCLIENT'),false).', ';
				$SQL .= $this->_DB->FeldInhaltFormat('N2',$rsAnreicherung->FeldInhalt('FGP_GLASMREGEL'),false).' ';
				$SQL .= ')';

                                if($this->_DB->Ausfuehren($SQL)===false) {
					throw new awisException('FEHLER1','',$SQL,4);
                                }

                                $countEintraege++;
                            }

                    $rsAnreicherung->DSWeiter();
                }
            }
         }
         else
         {
             //echo "Keine VorgangsNr";
         }

            $rsVerarbeite->DSWeiter();
        }
        
        //Setze alle angereicherten Daten auf Status '17'

        $SQL = "Select * from FGKOPFDATEN WHERE FGK_FGN_KEY=10";

        $rsSetzeStatus = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsSetzeStatus->EOF())
        {
           $SQL  = "UPDATE FGKOPFDATEN SET FGK_FGN_KEY=17 ";
           $SQL .= " WHERE FGK_VORGANGNR=".$rsSetzeStatus->FeldInhalt('FGK_VORGANGNR');

           if($this->_DB->Ausfuehren($SQL)===false) {
           }

           $rsSetzeStatus->DSWeiter();
        }
    }

    public function erstelleWoechentlicheFirstglasDatei() {
        $DateTime = date('Ymd H:i:s');
        $aktuellerWochentag = date('w');

        //$DateTime = str_replace(" ", "_", $DateTime);
        //$DateTime = str_replace(":","_",$DateTime);
        $FileName = 'FirstglassExport_';

        $Number = '';

        $SQL = "Select * from FGLFDNREXPORT WHERE FGX_BEZEICHNUNG='LFDNR_EXPORT'";

        $rsLFDNR = $this->_DB->RecordSetOeffnen($SQL);

        $Number = $rsLFDNR->FeldInhalt('FGX_WERT');

        $FileName = $FileName.$Number.".dat";


        //if($aktuellerWochentag == 2) {
        //Dann Exportdatei f�r Firstglas
            $this->_FirstglasDatei = $FileName;
            $this->_flagMittwoch = true;
            $Number++;

            $SQL = "UPDATE FGLFDNREXPORT SET FGX_WERT =".$Number;
            $SQL .= " WHERE FGX_BEZEICHNUNG='LFDNR_EXPORT'";

            if($this->_DB->Ausfuehren($SQL)===false) {
            }


        //}
        //else {
        //$this->_flagMittwoch = false;
        //}
    }
    
    public function erstelleStornoDatei() {
        $DateTime = date('Ymd H:i:s');
        $aktuellerWochentag = date('w');

        $FileName = 'KassendatenStorniert_';

        $Number = '';

        $SQL = "Select * from FGLFDNREXPORT WHERE FGX_BEZEICHNUNG='LFDNR_STORNO'";

        $rsLFDNR = $this->_DB->RecordSetOeffnen($SQL);

        $Number = $rsLFDNR->FeldInhalt('FGX_WERT');

        $FileName = $FileName.$Number.".dat";

	$this->_StornoDatei = $FileName;
        $this->_flagMittwoch = true;
        $Number++;

            $SQL = "UPDATE FGLFDNREXPORT SET FGX_WERT =".$Number;
            $SQL .= " WHERE FGX_BEZEICHNUNG='LFDNR_STORNO'";

            if($this->_DB->Ausfuehren($SQL)===false) {
            }
   } 
    
    
    
    


    function pruefeVorgangsNrGlaskopf() {

        $SQL = "Select * from FGKOPFDATEN inner join FGINZUGANG ON FGK_VORGANGNR = FGI_VORGANGNR WHERE FGK_FCN_KEY IS NULL OR FGK_FCN_KEY = 0 OR FGK_FCN_KEY = 1";

        $rsKopf = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsKopf->EOF()) {

            $SQL = 'UPDATE FGKOPFDATEN SET ';
            $SQL .= ' FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO','2',true);
            $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
            $SQL .= ',FGK_USERDAT=sysdate';
            $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKopf->FeldInhalt('FGK_VORGANGNR'),false);

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            $rsKopf->DSWeiter();
        }
    }




    function setzeKopfdatenImportUndExport() {

        $SQL  = "Select * from FGKOPFDATEN INNER JOIN FGZUGANG ON FGK_VORGANGNR = FGZ_EXPFELD3 WHERE FGZ_EXPFELD3 <> 700000 ";
        $SQL .= " AND FGZ_FIRSTGLASEXPORT = 1 AND FGK_FCN_KEY IS NULL OR FGK_FCN_KEY = 0 OR FGK_FCN_KEY = 1 OR FGK_FCN_KEY = 2";

        $rsKopf = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsKopf->EOF()) {

            if($rsKopf->FeldInhalt('FGK_FCN_KEY') == null || $rsKopf->FeldInhalt('FGK_FCN_KEY') == 0) {
                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO','1',true);
                $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKopf->FeldInhalt('FGK_VORGANGNR'),false);

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
            }
            else if($rsKopf->FeldInhalt('FGK_FCN_KEY') == 2) {
                    $SQL = 'UPDATE FGKOPFDATEN SET ';
                    $SQL .= ' FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO','3',true);
                    $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',FGK_USERDAT=sysdate';
                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsKopf->FeldInhalt('FGK_VORGANGNR'),false);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }

                }


            $rsKopf->DSWeiter();
        }



    }

    function ExportFirstglas() {
        $fgf = new FirstglasFunktionen();
        $Dateiname = $this->_FirstglasDatei;

        $fgf->csv_FirtglasExport($Dateiname);
    }

    function ExportZugang() {
        $fgf = new FirstglasFunktionen();

        $Dateiname = $this->_ZugangsDatei;
        $fgf->csv_ExportZugang($Dateiname);
    }

    function ExportKassendatenStorno()
    {
        $fgf = new FirstglasFunktionen();
        $Dateiname = $this->_StornoDatei;
	
	$fgf->csv_ExportKassendatenStorniert($Dateiname);
    }



    function sendeDifferenzMails()
    {
        $Text = '';

        $SQL = " Select DISTINCT FFR_FILID,FFR_VORGANGNR from FGRUECKFUEHRUNG inner join FILIALEN ON FFR_FILID = FIL_ID AND FIL_LAN_WWSKENN = 'BRD' WHERE FFR_MAILVERSENDET = 0 ";

        $rsDifferenzMails = $this->_DB->RecordSetOeffnen($SQL);

        $FIL_ID = '';
        $Laenge = '';

        while(!$rsDifferenzMails->EOF())
        {
	
	    $Text = "";	
	
            //L�nge der FIL_ID
            $FIL_ID = '';
            $FIL_ID = $rsDifferenzMails->FeldInhalt('FFR_FILID');
            $Laenge = strlen($FIL_ID);

            if($Laenge == 1)
            {
               $FIL_ID = '000'.$FIL_ID;

            }
            elseif ($Laenge == 2)
            {
               $FIL_ID = '00'.$FIL_ID;
            }
            elseif($Laenge == 3)
            {
               $FIL_ID = '0'.$FIL_ID;
            }

			    $Text  = 'Sehr geehrte Damen und Herren,';
			    $Text .= "\r \n";
			    $Text .= "bei der Kontrolle der First Glass Rechnungen wurde festgestellt, dass bei einigen Vorg�ngen";
			    $Text .= " der von Ihnen �ber den Werkstattauftrag abgerechnete Betrag von der First Glass Auftragsbest�tigung";
			    $Text .= " abweicht.\r \n";
			    $Text .= "Verantwortlichkeiten und Zust�ndigkeiten\r \n";
			    $Text .= "\r \n";
			    $Text .= "Wof�r: \t \t \t R�ckfragen\r \n";
			    $Text .= "Abteilung: \t \t \t Debitorenmanagement\r \n";
			    $Text .= "Zust�ndigkeit: \t \t Glassch�den\r \n";
			    $Text .= "Ansprechpartner: \t \t Witt Yasmin\r \n";
			    $Text .= "Kontakt: \t \t \t Tel: -5037 Fax: -934 5037\r \n";
			    $Text .= "Link Startmen�: \t \t Artikelinfo_Erweiterte->Stammdaten->Glas_01_LF_Differenz bei Scheibtausch\r \n";
			    $Text .= "\r \n";
			    $Text .= "Durchf�hrung\r \n";
			    $Text .= "Sie finden die fehlerhaften Vorg�nge im AWIS unter dem Punkt: ";
			    $Text .= "Operative Filialaufgaben -> First Glass Differenzkl�rung.";
			    $Text .= "\r \n";
			    $Text .= "Eine ausf�hrliche Erkl�rung zur Bearbeitung der Differenzen finden Sie im ";
			    $Text .= "Leitfaden Differenzen Scheibentausch unter o.a. Link. \r \n";
			    $Text .= "\r \n";


		   //Produktiv --> durch FIL_ID ersetzen
                  //-----------------------------------
            
		 //$this->_Werkzeug->EMail($FIL_ID.'@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu');
                 //$this->_Werkzeug->EMail('Christian.Beierl@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu','AFG@de.atu.eu');

                 $this->_Werkzeug->EMail($FIL_ID.'@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu','AFG@de.atu.eu');

		 $SQL = "UPDATE FGRUECKFUEHRUNG SET FFR_MAILVERSENDET = 1 WHERE FFR_VORGANGNR=".$rsDifferenzMails->FeldInhalt('FFR_VORGANGNR');


            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            $rsDifferenzMails->DSWeiter();
        }

    }

    function pruefeAbarbeitungsFrist()
    {
        $Text = '';
        $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_STATUS = 0 AND FFR_MAILVERSENDET = 1';
        $rsZeitraum = $this->_DB->RecordSetOeffnen($SQL);

        $AktDatum = date('d.m.Y');

        $FIL_ID = '';
        $Laenge = '';

        while(!$rsZeitraum->EOF())
        {
            $FIL_ID = '';
            $FIL_ID = $rsZeitraum->FeldInhalt('FFR_FILID');
            $Laenge = strlen($FIL_ID);

            if($Laenge == 1)
            {
               $FIL_ID = '000'.$FIL_ID;

            }
            elseif ($Laenge == 2)
            {
               $FIL_ID = '00'.$FIL_ID;
            }
            elseif($Laenge == 3)
            {
               $FIL_ID = '0'.$FIL_ID;
            }


            $DatumVorgang = $rsZeitraum->FeldInhalt('FFR_DIFFERENZDATUM');
            $DatumVorgang = substr($DatumVorgang, 0, 10);

            $DifferenzTage = strtotime($AktDatum) - strtotime($DatumVorgang);
            $DifferenzTage = $DifferenzTage / 86400;

            if($DifferenzTage >= 7)
            {
		    $Text  = 'Sehr geehrte Damen und Herren,';
		    $Text .= "\r \n";
		    $Text .= "bei der Kontrolle der First Glass Rechnungen wurde festgestellt, dass bei einigen Vorg�ngen";
		    $Text .= " der von Ihnen �ber den Werkstattauftrag abgerechnete Betrag von der First Glass Auftragsbest�tigung";
		    $Text .= " abweicht.\r \n";
		    $Text .= "Verantwortlichkeiten und Zust�ndigkeiten\r \n";
		    $Text .= "\r \n";
		    $Text .= "Wof�r: \t \t \t R�ckfragen\r \n";
		    $Text .= "Abteilung: \t \t \t Debitorenmanagement\r \n";
		    $Text .= "Zust�ndigkeit: \t \t Glassch�den\r \n";
		    $Text .= "Ansprechpartner: \t \t Witt Yasmin\r \n";
		    $Text .= "Kontakt: \t \t \t Tel: -5037 Fax: -934 5037\r \n";
		    $Text .= "Link Startmen�: \t \t Artikelinfo_Erweiterte->Stammdaten->Glas_01_LF_Differenz bei Scheibtausch\r \n";
		    $Text .= "\r \n";
		    $Text .= "Durchf�hrung\r \n";
		    $Text .= "Sie finden die fehlerhaften Vorg�nge im AWIS unter dem Punkt: ";
		    $Text .= "Operative Filialaufgaben -> First Glass Differenzkl�rung.";
		    $Text .= "\r \n";
		    $Text .= "Eine ausf�hrliche Erkl�rung zur Bearbeitung der Differenzen finden Sie im ";
		    $Text .= "Leitfaden Differenzen Scheibentausch unter o.a. Link. \r \n";
		    $Text .= "\r \n"; 
	       
	        //$Text .= "Betroffene Filiale".$rsZeitraum->FeldInhalt('FFR_FILID')."<br>";
                //$Text .= "Vorgang wurde seit ".$DifferenzTage."Tage nicht bearbeitet </br>";
                //$Text .= "DIFFERENZEN SIND VORHANDEN BITTE ABARBEITEN!!!</br>";
                //$this->_Werkzeug->EMail('Christian.Beierl@de.atu.eu','Firstglas - Differenz', $Text);
	       
	      //$this->_Werkzeug->EMail($FIL_ID.'@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu');
	      //$this->_Werkzeug->EMail('Christian.Beierl@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu'); 

              $this->_Werkzeug->EMail($FIL_ID.'@de.atu.eu','Differenz - Firstglass', $Text,3,'AFG@de.atu.eu','AFG@de.atu.eu');
	       
            }

            $rsZeitraum->DSWeiter();
        }

    }


    private function FGOpalAnreicherung()
    {
        /*
	$SQL = "Select min(VJS_JOBID) from VERSJOBSTEUERUNG";

        $rsDatum = $this->_DB->RecordSetOeffnen($SQL);

        $Datum = $rsDatum->FeldInhalt('VJS_STARTZEIT');

        $Datum = substr($Datum, 0, 10);
        */

        //$SQL = "Delete from FGOPALANREICHERUNG";

        //if($this->_DB->Ausfuehren($SQL)===false) {
        //}

	$SQL = "Select DISTINCT FGK_VORGANGNR FROM ";
	$SQL .= "(Select DISTINCT FGK_VORGANGNR,FGK_FGN_KEY, FKA_DATUM,FKA_WANR, row_number() over (partition by FGK_VORGANGNR order by FKA_DATUM ASC) ";
	$SQL .= "AS RNUM FROM FGKOPFDATEN ";
	$SQL .= "INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR) ";
	$SQL .= "WHERE FKA_DATUM >='18.05.2010 00:00:00' AND (FGK_FGN_KEY=8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY=10 OR FGK_FGN_KEY=17) AND FKA_WANR IS NOT NULL AND RNUM = 1  ";
	$SQL .= "AND ((FGK_VORGANGNR NOT IN ";
	$SQL .= " (Select DISTINCT FGP_VORGANGNR ";
	$SQL .= " FROM FGPOSITIONSDATEN INNER JOIN EXPERIAN_ATU.EXP_GLAS_KOPF_TAB@COM_DE.ATU.DE ON FGP_VORGANGNR = FG_VORGANG_NR)) ";
	$SQL .= " AND (FGK_VORGANGNR NOT IN ( ";
	$SQL .= " Select DISTINCT FGP_VORGANGNR FROM ";
	$SQL .= " (Select fgp_vorgangnr, fgp_artnr, fgp_artbez, count(*) over (partition by fgp_vorgangnr, fgp_artnr) as anz ";
	$SQL .= "  FROM FGPOSITIONSDATEN) WHERE anz > 1)))";
	
	
	//$SQL = "Select DISTINCT FGK_VORGANGNR FROM ";
	//$SQL .= " (Select DISTINCT FGK_VORGANGNR,FGK_FGN_KEY, FKA_DATUM, row_number() over (partition by FGK_VORGANGNR order by FKA_DATUM ASC) AS RNUM FROM FGKOPFDATEN ";
	//$SQL .= " INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR) ";
	//$SQL .= " WHERE FKA_DATUM >=". $this->_DB->FeldInhaltFormat('DU','18.05.10 00:00:00')." AND (FGK_FGN_KEY=8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY=10) AND RNUM = 1";
	
	//$SQL = "Select DISTINCT FGK_VORGANGNR from FGKOPFDATEN INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR WHERE TRUNC(FKA_DATUM) >= '18.05.10' ";
        //$SQL .= " AND (FGK_FGN_KEY=8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY=10)";
	
	//$SQL .= " AND (FGK_FGN_KEY=8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY=10) AND FGK_USERDAT >".$this->_DB->FeldInhaltFormat('DU',$Datum);

        /*
        $SQL = "Select DISTINCT FGK_VORGANGNR from FGKOPFDATEN WHERE (FGK_FGN_KEY=8 OR FGK_FGN_KEY=9 OR FGK_FGN_KEY=10) AND FGK_USERDAT >".$this->_DB->FeldInhaltFormat('DU',$Datum);
        $SQL .= " AND FGK_VORGANGNR NOT IN (Select DISTINCT FGP_VORGANGNR FROM ( ";
        $SQL .= " Select fgp_vorgangnr, fgp_artnr, fgp_artbez, count(*) over (partition by fgp_vorgangnr, fgp_artnr) as anz ";
        $SQL .= " FROM FGPOSITIONSDATEN) WHERE anz > 1)";
        */

        $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsAnreicherung->EOF())
        {
          $SQL = "Select * from FGOPALANREICHERUNG WHERE FGO_VORGANGSNR=".$this->_DB->FeldInhaltFormat('T',$rsAnreicherung->FeldInhalt('FGK_VORGANGNR'));

          $rsDuplikate = $this->_DB->RecordSetOeffnen($SQL);

          if($rsDuplikate->AnzahlDatensaetze() == 0)
          {
            $SQL  = "INSERT INTO FGOPALANREICHERUNG(FGO_VORGANGSNR,FGO_FLAG) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsAnreicherung->FeldInhalt('FGK_VORGANGNR'),false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',0,false);
            $SQL .= ')';

            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false) {
            }
          }

          $rsAnreicherung->DSWeiter();
        }
    }


    private function FGOpalAnreicherungStatusSetzen()
    {
		
      $SQL = "UPDATE FGOPALANREICHERUNG SET FGO_FLAG = 0 WHERE FGO_FLAG = 1";

      if($this->_DB->Ausfuehren($SQL)===false) {
      }
	
      $SQL = "Delete from FGMATANREICHERUNG";

      if($this->_DB->Ausfuehren($SQL)===false) {
      }

      $SQL = "Select * from FGOPALANREICHERUNG WHERE FGO_FLAG = 0";

      $rsAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

      $SQL = "INSERT INTO FGMATANREICHERUNG ";
      $SQL .= " Select DISTINCT aa.AEM_NR,aa.LIEFER_NR from DWH.V_FIRSTGLASS_OPALANREICHERUNG@DWH aa ";
      $SQL .= "INNER JOIN ods.KBEW_BONS@DWH bb ON aa.datum = bb.datum AND aa.ZEIT = bb.ZEIT ";
      $SQL .= " AND aa.FIL_ID = bb.FIL_ID ";
      $SQL .= "WHERE (KUNDEN_NR IS NOT NULL OR (CARDPAN IS NOT NULL AND KUNDEN_NR IS NULL AND substr(CARDPAN,1,10) = '6014340340') ";
      $SQL .= "OR SHELLPAN IS NOT NULL)";
      
      if($this->_DB->Ausfuehren($SQL)===false) {
      }

      while(!$rsAnreicherung->EOF())
      {
        $SQL = "Select * from FGMATANREICHERUNG WHERE FMA_FGNUMMER=".$this->_DB->FeldInhaltFormat('T',$rsAnreicherung->FeldInhalt('FGO_VORGANGSNR'));

        $rsAnreicherungStatus = $this->_DB->RecordSetOeffnen($SQL);

        if ($rsAnreicherungStatus->AnzahlDatensaetze() == 0)
        {
          //Setze Status '1' --> Nicht Gefunden
          $SQL = "UPDATE FGOPALANREICHERUNG SET FGO_FLAG = 1 ";
          $SQL .= "WHERE FGO_VORGANGSNR=".$this->_DB->FeldInhaltFormat('T',$rsAnreicherung->FeldInhalt('FGO_VORGANGSNR'));

          if($this->_DB->Ausfuehren($SQL)===false) {
          }

        }
        else
        {

          $SQL = "UPDATE FGOPALANREICHERUNG SET FGO_FLAG = 2 ";
          $SQL .= "WHERE FGO_VORGANGSNR=".$this->_DB->FeldInhaltFormat('T',$rsAnreicherung->FeldInhalt('FGO_VORGANGSNR'));

          if($this->_DB->Ausfuehren($SQL)===false) {
          }
        }

        $rsAnreicherung->DSWeiter();
      }
}

private function FGOpalSpeichereDatensaetze()
{
  $SQL = "Select * from FGOPALANREICHERUNG WHERE FGO_FLAG = 2";

  $rsOpalAnreicherung = $this->_DB->RecordSetOeffnen($SQL);

  while(!$rsOpalAnreicherung->EOF())
  {
  
    //--->inner join FGMATANREICHERUNG (KOPFSATZ)
    $SQL = "Select * from FGKOPFDATEN INNER JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR ";
    $SQL .= " inner join FGMATANREICHERUNG ON FGK_VORGANGNR = fma_fgnummer ";
    $SQL .= " WHERE FKA_WANR IS NOT NULL AND FGK_VORGANGNR =".$this->_DB->FeldInhaltFormat('T',$rsOpalAnreicherung->FeldInhalt('FGO_VORGANGSNR'));

    $rsKopfDatensatz = $this->_DB->RecordSetOeffnen($SQL);

    $KopfBrutto = 0.0;
    $Mwst = '';
    $Mwst = "1".$rsKopfDatensatz->FeldInhalt('FGK_STEUERSATZ');

    
    $SQL = "Select * from FGPOSITIONSDATEN WHERE FGP_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$rsOpalAnreicherung->FeldInhalt('FGO_VORGANGSNR'));

    $rsPositionsDatensatz = $this->_DB->RecordSetOeffnen($SQL);

    while(!$rsPositionsDatensatz->EOF())
    {

      $KopfPreis = '';
      $KopfAnzahl = '';
      $KopfOEMPreis = '';

      $KopfAnzahl = $rsPositionsDatensatz->FeldInhalt('FGP_ANZAHL');
      $KopfOEMPreis = str_replace(',','.',$rsPositionsDatensatz->FeldInhalt('FGP_OEMPREIS'));
      $KopfPreis = $KopfAnzahl * $KopfOEMPreis;
      
      $KopfBrutto = $KopfBrutto + $KopfPreis;

      $Preis = '';
      $anzahl = '';

      $Oempreis = str_replace(',','.',$rsPositionsDatensatz->FeldInhalt('FGP_OEMPREIS'));
      $anzahl = $rsPositionsDatensatz->FeldInhalt('FGP_ANZAHL');

      
      $Preis = $anzahl * $Oempreis / 100 * $Mwst;

      $Preis = str_replace('.',',',$Preis);

      $SQL = "INSERT INTO FGOPALPOSITIONSSATZ(FOP_KENNUNG,FOP_FGVORGANGNR,FOP_ARTNR,FOP_ARTIKELBEZEICHNUNG, ";
      $SQL .= "FOP_ANZAHL,FOP_OEMPREIS) VALUES (";
      $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsPositionsDatensatz->FeldInhalt('FGP_KENNUNG'),false);
      $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPositionsDatensatz->FeldInhalt('FGP_VORGANGNR'),false);
      $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPositionsDatensatz->FeldInhalt('FGP_ARTNR'),false);
      $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPositionsDatensatz->FeldInhalt('FGP_ARTBEZ'),false);
      $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsPositionsDatensatz->FeldInhalt('FGP_ANZAHL'),false);
      $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$Preis,false);
      $SQL .= ')';

      if($this->_DB->Ausfuehren($SQL)===false) {
      }

     $rsPositionsDatensatz->DSWeiter();
    }

    $KopfBrutto = $KopfBrutto / 100 * $Mwst;

    $KopfBrutto = str_replace('.',',',$KopfBrutto);

    if($rsKopfDatensatz->FeldInhalt('FMA_LIEFERNR') == null OR $rsKopfDatensatz->FeldInhalt('FMA_LIEFERNR') == 0)
    {
        $SQL = "INSERT INTO FGOPALKOPFSATZ(FOK_KENNUNG,FOK_FILIALNUMMER,FOK_VORGANGSNR,FOK_BONBETRAG, ";
        $SQL .= "FOK_FGVORGANGSNR) VALUES (";
        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FGK_KENNUNG'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatensatz->FeldInhalt('FKA_FILID'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FKA_WANR'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$KopfBrutto,false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FGK_VORGANGNR'),false);
        $SQL .= ')';
    }
    else
    {
        $SQL = "INSERT INTO FGOPALKOPFSATZ(FOK_KENNUNG,FOK_FILIALNUMMER,FOK_VORGANGSNR,FOK_BONBETRAG, ";
        $SQL .= "FOK_FGVORGANGSNR) VALUES (";
        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FGK_KENNUNG'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatensatz->FeldInhalt('FKA_FILID'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FMA_LIEFERNR'),false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$KopfBrutto,false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatensatz->FeldInhalt('FGK_VORGANGNR'),false);
        $SQL .= ')';
    }

    if($this->_DB->Ausfuehren($SQL)===false) {
    }

   $SQL = "UPDATE FGOPALANREICHERUNG SET FGO_FLAG = 3 ";
   $SQL .= "WHERE FGO_VORGANGSNR=".$rsKopfDatensatz->FeldInhalt('FGK_VORGANGNR');

   if($this->_DB->Ausfuehren($SQL)===false) {
   }

   $rsOpalAnreicherung->DSWeiter();
  }
}

private function OpalAnreicherungCOMServer()
{

  $SQL = "DELETE FROM EXPERIAN_ATU.EXP_GLAS_KOPF_TMP@COM_DE.ATU.DE";

  if($this->_DB->Ausfuehren($SQL)===false) {
  }

  $SQL = "DELETE FROM EXPERIAN_ATU.EXP_GLAS_POS_TMP@COM_DE.ATU.DE";

  if($this->_DB->Ausfuehren($SQL)===false) {
  }

  $SQL = "Select * from FGOPALKOPFSATZ";

  $rsKopfsatz = $this->_DB->RecordSetOeffnen($SQL);

  $this->_DB->TransaktionBegin();

  while(!$rsKopfsatz->EOF())
  {
    $SQL = "Select count(*) AS Anzahl from FGOPALPOSITIONSSATZ WHERE FOP_FGVORGANGNR=".$this->_DB->FeldInhaltFormat('T',$rsKopfsatz->FeldInhalt('FOK_FGVORGANGSNR'));
    
    $anzahlPositionsdaten = $this->_DB->RecordSetOeffnen($SQL);

    $SQL = "INSERT INTO EXPERIAN_ATU.EXP_GLAS_KOPF_TMP@COM_DE.ATU.DE(KENNUNG,FIL_ID,VORGANG_NR,BONBETRAG, ";
    $SQL .= "FG_VORGANG_NR,ANZAHL_POS) VALUES (";
    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsKopfsatz->FeldInhalt('FOK_KENNUNG'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopfsatz->FeldInhalt('FOK_FILIALNUMMER'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfsatz->FeldInhalt('FOK_VORGANGSNR'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',str_replace(',','.',$rsKopfsatz->FeldInhalt('FOK_BONBETRAG')),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsKopfsatz->FeldInhalt('FOK_FGVORGANGSNR'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$anzahlPositionsdaten->FeldInhalt('ANZAHL'),false);
    $SQL .= ')';
    
    //LOG ERRORS INTO EXPERIAN_ATU.ERR\$_EXP_GLAS_KOPF_TMP@COM_TEST_DE.ATU.DE REJECT LIMIT UNLIMITED";

    if($this->_DB->Ausfuehren($SQL)===false) {
     $this->_DB->TransaktionRollback();
    }

    $rsKopfsatz->DSWeiter();
  }


  $SQL = "Select * from FGOPALPOSITIONSSATZ";

  $rsPostionssatz = $this->_DB->RecordSetOeffnen($SQL);

  while(!$rsPostionssatz->EOF())
  {

    $SQL = "INSERT INTO EXPERIAN_ATU.EXP_GLAS_POS_TMP@COM_DE.ATU.DE(KENNUNG,FG_VORGANG_NR,ARTNR,ARTIKELBEZEICHNUNG, ";
    $SQL .= "ANZAHL,OEM_PREIS) VALUES (";
    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsPostionssatz->FeldInhalt('FOP_KENNUNG'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPostionssatz->FeldInhalt('FOP_FGVORGANGNR'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPostionssatz->FeldInhalt('FOP_ARTNR'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPostionssatz->FeldInhalt('FOP_ARTIKELBEZEICHNUNG'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsPostionssatz->FeldInhalt('FOP_ANZAHL'),false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',str_replace(',','.',$rsPostionssatz->FeldInhalt('FOP_OEMPREIS')),false);
    $SQL .= ')';

    //LOG ERRORS INTO EXPERIAN_ATU.ERR\$_EXP_GLAS_POS_TMP@COM_TEST_DE.ATU.DE REJECT LIMIT UNLIMITED";



    if($this->_DB->Ausfuehren($SQL)===false) {
       $this->_DB->TransaktionRollback();
    }

    $rsPostionssatz->DSWeiter();
  }

  
  $this->_DB->TransaktionCommit();

  

  $SQL = "Delete from FGOPALKOPFSATZ";

  if($this->_DB->Ausfuehren($SQL)===false) {
  }


  $SQL = "Delete from FGOPALPOSITIONSSATZ";

  if($this->_DB->Ausfuehren($SQL)===false) {
  }
  
}

}
