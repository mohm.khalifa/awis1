<?php
global $CursorFeld;
global $con;
global $AWISBenutzer;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('LIK','txt_SucheFiliale');
$TextKonserven[]=array('LIK','txt_SucheDatum');
$TextKonserven[]=array('LIK','txt_SucheDatumVon');
$TextKonserven[]=array('LIK','txt_SucheDatumBis');
$TextKonserven[]=array('LIK','txt_SucheArtnr');
$TextKonserven[]=array('LIK','txt_SucheLSNR');
$TextKonserven[]=array('Liste','lst_Ausgabeformat');
$TextKonserven[]=array('Liste','lst_Lager');
$TextKonserven[]=array('Wort','Ausgabeformat');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','AlleAnzeigen');
$TextKonserven[]=array('Wort','NurDifferenzen');
$TextKonserven[]=array('Wort','Lager');
$TextKonserven[]=array('Wort','AuswahlSpeichern');


$TXT_Baustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$RechteStufe = awisBenutzerRecht($con, 507);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Lieferkontrolle',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

echo "<br>";

echo "<form name=frmSuche method=post action=./lieferkontrolle_Main.php?cmdAktion=Liste>";

$lieKonSuche = unserialize(awis_BenutzerParameter($con, 'Formular_AUS_LIK', $AWISBenutzer->BenutzerName()));

awis_FORM_FormularStart('');

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['txt_SucheFiliale'].':',180);
awis_FORM_Erstelle_TextFeld('*SuchFiliale',($lieKonSuche[0]=='on'?$lieKonSuche[1]:''),20,200,true);
$CursorFeld='sucSuchFiliale';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['txt_SucheDatumVon'].':',180);
awis_FORM_Erstelle_TextFeld('*SuchDatumVon',($lieKonSuche[0]=='on'?$lieKonSuche[2]:''),20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['txt_SucheDatumBis'].':',180);
awis_FORM_Erstelle_TextFeld('*SuchDatumBis',($lieKonSuche[0]=='on'?$lieKonSuche[3]:''),20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['txt_SucheArtnr'].':',180);
awis_FORM_Erstelle_TextFeld('*SuchArtnr',($lieKonSuche[0]=='on'?$lieKonSuche[4]:''),20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['txt_SucheLSNR'].':',180);
awis_FORM_Erstelle_TextFeld('*SuchLSNR',($lieKonSuche[0]=='on'?$lieKonSuche[5]:''),20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['NurDifferenzen'].':',180);
awis_FORM_Erstelle_Checkbox('NurDifferenzen',($lieKonSuche[0]=='on'?$lieKonSuche[6]:''),50,true,'on','');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['Lager'].':',180);
$Lager = explode("|",$TXT_Baustein['Liste']['lst_Lager']);
awis_FORM_Erstelle_SelectFeld('*SuchLager',($lieKonSuche[0]=='on'?$lieKonSuche[7]:''),150,true,$con,'','~'.$TXT_Baustein['Wort']['Auswahl_ALLE'],'','','',$Lager,'');
awis_FORM_ZeileEnde();	

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['Ausgabeformat'].':',180);
$Ausgabeformat = explode("|",$TXT_Baustein['Liste']['lst_Ausgabeformat']);
awis_FORM_Erstelle_SelectFeld('Format',($lieKonSuche[0]=='on'?$lieKonSuche[8]:''),150,true,$con,'','','','','',$Ausgabeformat,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['AuswahlSpeichern'].':',180);
awis_FORM_Erstelle_Checkbox('AuswahlSpeichern',$lieKonSuche[0],30,true,'on');
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();

echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

echo '</form>';

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}

?>

