<?php
require_once('awisArtikelstammImport.php');
require_once('awisDatenbank.inc');
require_once('awisJobVerwaltung.inc');

try 
{
	
	$Benutzer = '';
	$DebugLevel = 0;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	for($i=1;$i<$argc;$i++)
	{
		switch (strtolower($argv[$i]))
		{
			case '--benutzer':
				$Benutzer = $argv[++$i];
				break;
			case '--debuglevel':
				$DebugLevel = $argv[++$i];
				break;
			default:
				echo 'Parameter '.$argv[$i].' ist unbekannt.';
			case '--help':
				echo $argv[0].PHP_EOL;
				echo 'Optionen:'.PHP_EOL;
				echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
				echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
				echo '  --mount  <bool>                    Mount erfolgreich? 1=Ja 0=Nein'.PHP_EOL;
				echo PHP_EOL;
				die();
		}
	}
	
	
	
	$Artikel = new awisArtikelstammImport($Benutzer, $DebugLevel);
	$Artikel->ImportArtikel();
	#$Artikel->import_artikel_lucas_3();
	#$Artikel->ImportArtikelTest(0,'riedl_t');
		
}catch (awisException $ex)
{
    echo PHP_EOL.'awisArtikelstammImport: Fehler: '.$ex->getMessage().PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    awisWerkzeuge::EMail(array('shuttle@de.atu.eu'),'Artikelstammimport-Fehler','Fehler :' .$ex->getMessage(),3,'','awis@de.atu.eu');

    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;


}
catch (Exception $ex)
{	
	echo PHP_EOL.'awisArtikelstammImport: Fehler: '.$ex->getMessage().PHP_EOL;
	
	$AWISWerkzeug = new awisWerkzeuge();
	awisWerkzeuge::EMail(array('shuttle@de.atu.eu'),'Artikelstammimport-Fehler','Fehler :' .$ex->getMessage(),3,'','awis@de.atu.eu');
		
	echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
	echo 'CODE:  '.$ex->getCode().PHP_EOL;
	echo 'Zeile: '.$ex->getLine().PHP_EOL;
	echo 'Datei: '.$ex->getFile().PHP_EOL;


}



?>