<?php
global $REX;
global $AWIS_KEY1;
try {
    if(isset($_POST['txtREX_REKLAMATIONSDATUM'])){
        if ($AWIS_KEY1==-1) { //Neuer DS
            $SQL  ='INSERT';
            $SQL .=' INTO REIFENREKLAMATIONEN';
            $SQL .='   (';
            $SQL .='     REX_REKLAMATIONSDATUM,';
            $SQL .='     REX_WERK,';
            $SQL .='     REX_AST_ATUNR,';
            $SQL .='     REX_LAR_NR,';
            $SQL .='     REX_HERSTELLER,';
            $SQL .='     REX_P,';
            $SQL .='     REX_EM,';
            $SQL .='     REX_RA,';
            $SQL .='     REX_LAN_CODE,';
            $SQL .='     REX_PROFILTIEFE,';
            $SQL .='     REX_STATUS,';
            $SQL .='     REX_USER,';
            $SQL .='     REX_USERDAT';
            $SQL .='   )';
            $SQL .='   VALUES';
            $SQL .='   (';
            $SQL .= $REX->DB->WertSetzen('REX','D',$_POST['txtREX_REKLAMATIONSDATUM'],false);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_WERK'],false);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['sucREX_AST_ATUNR'],false);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_LAR_NR'],false);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_HERSTELLER'],true);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_P'],true);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_EM'],true);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_RA'],true);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$_POST['txtREX_LAN_CODE'],true);
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','N0',$_POST['txtREX_PROFILTIEFE'],true);
            $SQL .= ', \'E\'';
            $SQL .= ', ' . $REX->DB->WertSetzen('REX','T',$REX->AWISBenutzer->BenutzerName(),true);
            $SQL .= ', sysdate' ;
            $SQL .='   )';

            $REX->DB->Ausfuehren($SQL, '', true, $REX->DB->Bindevariablen('REX'));

            $SQL = 'SELECT seq_REX_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $REX->DB->RecordSetOeffnen($SQL);
            $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
            $Meldung = $REX->AWISSprachKonserven['REX']['REX_INSERT_OK'];
        } else { //gešnderter DS
            $SQL  ='UPDATE REIFENREKLAMATIONEN';
            $SQL .=' SET ';
            $SQL .='   REX_REKLAMATIONSDATUM = '. $REX->DB->WertSetzen('REX','D',$_POST['txtREX_REKLAMATIONSDATUM'],false);
            $SQL .=' , REX_WERK              = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_WERK'],false);
            $SQL .=' , REX_AST_ATUNR         = '. $REX->DB->WertSetzen('REX','T',$_POST['sucREX_AST_ATUNR'],false);
            $SQL .=' , REX_LAR_NR         = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_LAR_NR'],false);
            $SQL .=' , REX_HERSTELLER            = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_HERSTELLER'],false);
            $SQL .=' , REX_P                 = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_P'],false);
            $SQL .=' , REX_EM                = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_EM'],false);
            $SQL .=' , REX_RA                = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_RA'],false);
            $SQL .=' , REX_LAN_CODE          = '. $REX->DB->WertSetzen('REX','T',$_POST['txtREX_LAN_CODE'],false);
            $SQL .=' , REX_PROFILTIEFE       = '. $REX->DB->WertSetzen('REX','N0',$_POST['txtREX_PROFILTIEFE'],false);
            $SQL .=' , REX_USER              = '. $REX->DB->WertSetzen('REX','T',$REX->AWISBenutzer->BenutzerName(),false);
            $SQL .=' , REX_USERDAT           = sysdate';
            $SQL .=' WHERE REX_KEY             = '. $REX->DB->WertSetzen('REX','N0',$AWIS_KEY1,false);

            $REX->DB->Ausfuehren($SQL, '', true, $REX->DB->Bindevariablen('REX'));
            $Meldung = $REX->AWISSprachKonserven['REX']['REX_UPDATE_OK'];
        }
        if($Meldung){
            $REX->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
        }
    }


} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $REX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $REX->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $REX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>