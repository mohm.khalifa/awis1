
<script src="stellenanzeigen.js"></script>

<?php
//var_dump($_POST);
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular


$Form = new awisFormular();

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Form->Formular_Start();
$Form->SchreibeHTMLCode('<form name=frmStellenanzeigen method=post action=./stellenanzeigen_Main.php>');

if(($AWISBenutzer->HatDasRecht(41000)&1) != 1)
{
    $Form->DebugAusgabe(1,'Keine Rechte');
    $Form->Fehler_KeineRechte();
}


//Buttons oben
$Form->ZeileStart();
$Form->Erstelle_TextLabel('', 100); //Platzhalter
$Form->Schaltflaeche('script', 'cmdUeberschrift', 'onclick="TagsEinfuegen(\'~\', \'~\')"', '/bilder/cmd_stern.png','Überschrift','U','',27,27 );
$Form->Schaltflaeche('script', 'cmdListe', 'onclick="TagsEinfuegen(\'->\', \'<-\')"', '/bilder/cmd_frmweiter.png','Liste','L','',27,27 );
$Form->Erstelle_TextLabel('', 607); //Noch einer
$Form->Erstelle_Checkbox('Vorschau', 'on', 20, true, 'on', '', '', 'onClick="textArea2Auswahlbox();"');
$Form->Erstelle_TextLabel('Vorschau anzeigen', 150);
$Form->ZeileEnde();

//Textareas
$Form->ZeileStart('width:1800px; margin-bottom: 20px');
$Form->Erstelle_TextLabel('Text:', 100);
$Form->Erstelle_Textarea('!Inhalt', (isset($_POST['txtInhalt'])?$_POST['txtInhalt']:''), 380, 380, 30, true,'','','onKeyUp="textArea2Auswahlbox();"; onPropertyChange="textArea2Auswahlbox();"; onFocusOut="saveFocused(this);";');
$Form->Erstelle_TextLabel("", 47); //So viele Platzhalter :D
$Form->Erstelle_Textarea('!Inhalt2', (isset($_POST['txtInhalt'])?$_POST['txtInhalt']:''), 380, 380, 30, true,'','','onKeyUp="textArea2Auswahlbox();"; onPropertyChange="textArea2Auswahlbox();"; onFocusOut="saveFocused(this);";');
$Form->ZeileEnde();

//Ajax
$Form->ZeileStart();
$Form->Auswahlbox('ajaxBox1','HTMLCodeAusgabe','HTMLCodeAusgabe','HTMLCodeAusgabe','',100,100,'','T',true,'','','display:none', '', '', '', '', '4', 'display:none');
$Form->ZeileEnde();

//Ausgabeboxen
$Form->ZeileStart();
echo '<div id="AusgabeLive">';
$Form->AuswahlBoxHuelle('HTMLCodeAusgabe');
echo '</div>';
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->AuswahlBoxHuelle('HTMLCodeAusgabeUnten');
$Form->Auswahlbox('ajaxBox2','HTMLCodeAusgabeUnten','HTMLCodeAusgabeUnten','HTMLCodeAusgabeUnten','',100,100,'','T',true,'','','display:none', '', '', '', '', '4', 'display:none');
$Form->ZeileEnde();
$Form->Formular_Ende();


$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png','Zur&uuml;ck','Z');
$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form>');
?>