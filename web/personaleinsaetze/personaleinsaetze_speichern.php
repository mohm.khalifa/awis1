<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	//***********************************************************************************
	//** Infos speichern ung l�schen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPEZ_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')// AND $ATUNR!='')
	{
	//awisFormular::DebugAusgabe(1,$_POST,$Felder);
		
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$SQL = '';
			
			if($FeldTeile[3]>0) //Key > 0 --> aktualisieren
			{
				// Nur schreiben, wenn sich die Felder ge�ndert haben sollten!
				if($DB->FeldInhaltFormat('T',$_POST[$Feld],true)!=$DB->FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)],true))
				{
					$SQL .= 'UPDATE Personaleinsberichtszuord';
					if ($FeldTeile[1]=='BEMERKUNG')
					{
						$SQL .= ' SET PEZ_BEMERKUNG='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
					}
					else
					{
						$SQL .= ' SET PEZ_WERT='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
					}
					$SQL .= ', PEZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', PEZ_USERDAT=SYSDATE';
					$SQL .= ' WHERE PEZ_KEY=0'.$FeldTeile[3];				
					$Form->DebugAusgabe(1,$SQL);						
				}
			}
			else 
			{
				$AWIS_KEY1=$_POST['txtPEI_KEY'];
				
				$SQL = 'INSERT INTO personaleinsberichtszuord';
				$SQL .= '(PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_USER,PEZ_USERDAT)';
				$SQL .= ' select '.$AWIS_KEY1. ', PBF_ID , ' . $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
				$SQL .= ' ,\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ' ,SYSDATE';
				$SQL .= ' FROM Personaleinsberichtsfelder';
				$SQL .= ' WHERE PBF_PBE_KEY = '.$_POST['txtPEI_PBE_KEY'];
				$SQL .= ' AND PBF_SPALTEN > 0';
				$SQL .= ' AND PBF_ID = ' . $FeldTeile[2];
			}

			if($SQL!='')
			{
				$DB->Ausfuehren($SQL, '', true);
			}
		}
	}


	if(isset($_POST['txtPEI_KEY']))
	{
		//***********************************************
		// Personaleinsaetze
		//***********************************************
		$AWIS_KEY1=$_POST['txtPEI_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtPEI_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('PEI','PEI_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('PEI_DATUM','PEI_PEA_KEY');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PEI'][$Pflichtfeld].'<br>';
				}
			}

			if (($_POST['txtPEI_PEA_KEY'] == '8' OR $_POST['txtPEI_PBE_KEY'] != '') AND $_POST['txtPEI_FIL_ID']=='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PEI']['PEI_FIL_ID'].'<br>';
			}			
						
			if ($_POST['txtPEI_PEA_KEY'] != '8' AND $_POST['txtPEI_PBE_KEY'] != '')
			{
				$Fehler .= $TXT_Speichern['PEI']['err_CheckTyp'].'<br>';				
			}			
			
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->Hinweistext($Fehler);
				
				$Link='./personaleinsaetze_Main.php?cmdAktion=Details&PEI_KEY=0'.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();					
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtPEI_KEY'])<=0)
			{
				$Speichern = true;

				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO personaleinsaetze';
					$SQL .= '(PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PBE_KEY,PEI_PEZ_STATUS, PEI_PLANUNGSTYP';//,PEI_PEB_KEY';
					$SQL .= ',PEI_USER, PEI_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('D',$_POST['txtPEI_DATUM'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPEI_XBN_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',(isset($_POST['txtPEI_VONXTZ_ID'])?$_POST['txtPEI_VONXTZ_ID']:''),true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',(isset($_POST['txtPEI_BISXTZ_ID'])?$_POST['txtPEI_BISXTZ_ID']:''),true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPEI_PEA_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPEI_FIL_ID'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPEI_BEMERKUNG'],true);
					//$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPEI_PEB_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('NO',$_POST['txtPEI_PBE_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('NO','0',true);
					$SQL .= ',' . $DB->FeldInhaltFormat('NO',$_POST['txtPEI_PLANUNGSTYP'],true);					
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					if($DB->Ausfuehren($SQL)===false)
					{
						die();
					}
					$SQL = 'SELECT seq_PEI_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
										
					if(floatval($_POST['txtPEI_PBE_KEY'])>0)
					{
						$SQL3  = ' Select * ';
						$SQL3 .= ' FROM Personaleinsberichtsfelder';
						$SQL3 .= ' WHERE PBF_PBE_KEY = '.$_POST['txtPEI_PBE_KEY'];
						$SQL3 .= ' AND PBF_SPALTEN > 0';
						
						
						$rsPBE = $DB->RecordSetOeffnen($SQL3);
						
						$Wert = '';
						while(! $rsPBE->EOF())
						{
							if(($rsPBE->FeldInhalt('PBF_SPALTEN')&1)==1)
							{
								if($rsPBE->FeldInhalt('PBF_DATENQUELLE')!='')
								{
									switch(substr($rsPBE->FeldInhalt('PBF_DATENQUELLE'),0,3))
									{
										case 'FIL':
											$Felder = explode(':',$rsPBE->FeldInhalt('PBF_DATENQUELLE'));
											$SQL2 = $Felder[1];
											$SQL2 .= 'and FIL_ID = '.$_POST['txtPEI_FIL_ID'];
											$rsFIL = $DB->RecordSetOeffnen($SQL2);
											$Wert = $rsFIL->FeldInhalt('NAME');
											break;
										case 'GBL':
											$Felder = explode(':',$rsPBE->FeldInhalt('PBF_DATENQUELLE'));
											$SQL2 = $Felder[1];
											$SQL2 = str_replace('#FIL_ID#',$_POST['txtPEI_FIL_ID'],$SQL2);
											$rsFIL = $DB->RecordSetOeffnen($SQL2);
											$Wert = $rsFIL->FeldInhalt('NAME');
											break;
										default:
											$Wert = '';
											break;
									}
										
								}
							}
						
							
							$Fehler = '';
							$SQL = 'INSERT INTO personaleinsberichtszuord';
							$SQL .= '(PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_USER,PEZ_USERDAT)';
							$SQL .= ' select '.$AWIS_KEY1. ', PBF_ID, \''.$Wert.'\'';
							$SQL .= ' ,\'' . $AWISBenutzer->BenutzerName() . '\'';
							$SQL .= ' ,SYSDATE';
							$SQL .= ' FROM Personaleinsberichtsfelder';
							$SQL .= ' WHERE PBF_PBE_KEY = '.$_POST['txtPEI_PBE_KEY'];
							$SQL .= ' AND PBF_SPALTEN > 0';
						    $SQL .= ' AND PBF_ID ='.$rsPBE->FeldInhalt('PBF_ID');
							
							if($DB->Ausfuehren($SQL)===false)
							{
								die();
							}
							$rsPBE->DSWeiter();
						}
					}
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM personaleinsaetze WHERE PEI_key=' . $_POST['txtPEI_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$Fehler = '';
								
								if($FeldName == 'PEI_PEZ_STATUS' and $WertNeu=='1')
								{
									//Pr�fen, ob Bemerkungsfeld, wenn nicht ok
									$SQL = 'SELECT * FROM PERSONALEINSBERICHTSZUORD ';
									$SQL.= ' INNER JOIN PERSONALEINSBERICHTSFELDER ON PBF_ID = PEZ_PBF_ID AND PBF_SPALTEN=3';
									$SQL.= ' WHERE PEZ_WERT = \'0\' AND (PEZ_BEMERKUNG IS NULL OR PEZ_BEMERKUNG = \'\') ';
									$SQL.= ' AND PEZ_PEI_KEY = 0'.$AWIS_KEY1;
									
									$rsPruef = $DB->RecordSetOeffnen($SQL);
									if ($rsPruef->AnzahlDatensaetze() > 0)
									{
										$Fehler.= $TXT_Speichern['PEI']['err_Bemerkung'].'<br>';
									}
									
									$SQL = 'SELECT PBE_ANZAHLFELDER, PBE_KEY FROM PERSONALEINSAETZE ';
									$SQL.= ' LEFT JOIN PERSONALEINSBERICHTE ON PEI_PBE_KEY = PBE_KEY';
									$SQL.= ' WHERE PEI_KEY = 0'.$AWIS_KEY1;
									
									$rsAnzahlFelder = $DB->RecordSetOeffnen($SQL);
									
									if ($rsAnzahlFelder->FeldInhalt('PBE_KEY')!='2' and $rsAnzahlFelder->FeldInhalt('PBE_KEY')!='7' and $rsAnzahlFelder->FeldInhalt('PBE_KEY')!='10' and $rsAnzahlFelder->FeldInhalt('PBE_KEY')!='11')
									{									
										//Pr�fen, ob Bemerkungsfeld, wenn nicht ok
										$SQL = 'SELECT COUNT(*) AS ANZ FROM PERSONALEINSBERICHTSZUORD ';
										$SQL.= ' WHERE (PEZ_WERT = \'0\' OR PEZ_WERT = \'1\') ';
										$SQL.= ' AND PEZ_PEI_KEY = 0'.$AWIS_KEY1;
	
										$rsPruef = $DB->RecordSetOeffnen($SQL);
										
										$MindestFelder = 0;
										$MindestFelder = round($DB->FeldInhaltFormat('N0', $rsAnzahlFelder->FeldInhalt('PBE_ANZAHLFELDER')) * 0.75);																	
										
										if ($DB->FeldInhaltFormat('NO', $rsPruef->FeldInhalt('ANZ')) < $MindestFelder)
										{
											$Fehler.= 'Sie m�ssen mindestens '.$MindestFelder.' Felder ausf�llen';//$TXT_Speichern['PEI']['err_Bemerkung'];
										}
									}

									if ($rsAnzahlFelder->FeldInhalt('PBE_KEY')=='7')
									{
										//Pr�fen, ob Bemerkungsfeld, wenn nicht ok
										$SQL = 'SELECT * FROM PERSONALEINSBERICHTSZUORD ';
										$SQL.= ' WHERE PEZ_WERT is null ';
										$SQL.= ' AND PEZ_PBF_ID not in(407,408,409,471,445) ';
										$SQL.= ' AND PEZ_PEI_KEY = 0'.$AWIS_KEY1;
									
										$rsPruef = $DB->RecordSetOeffnen($SQL);
									
										if ($rsPruef->AnzahlDatensaetze() > 0)
										{
											$Fehler.= 'Alle Fragen ausser A2 und D12 m�ssen beantwortet werden!';//$TXT_Speichern['PEI']['err_Bemerkung'];
										}
									}
									
									if ($Fehler != '')
									{
										$Form->Hinweistext($Fehler);
										
										$Link='./personaleinsaetze_Main.php?cmdAktion=Details&PEI_KEY='.$AWIS_KEY1.'&PBE=1';
										$Form->SchaltflaechenStart();
										$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
										$Form->SchaltflaechenEnde();				
										die();												
									}									
									
								}								
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
								
								if($FeldName == 'PEI_PBE_KEY')
								{
									$SQL = 'Delete From personaleinsberichtszuord';
									$SQL .= ' WHERE PEZ_PEI_KEY='.$AWIS_KEY1;
									
									if($DB->Ausfuehren($SQL)===false)
									{
										die();
									}
									
									if(floatval($_POST['txtPEI_PBE_KEY'])>0)
									{
										$Fehler = '';
										$SQL = 'INSERT INTO personaleinsberichtszuord';
										$SQL .= '(PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_USER,PEZ_USERDAT)';
										$SQL .= ' select '.$AWIS_KEY1. ', PBF_ID ';
										$SQL .= ' ,\'' . $AWISBenutzer->BenutzerName() . '\'';
										$SQL .= ' ,SYSDATE';
										$SQL .= ' FROM Personaleinsberichtsfelder';
										$SQL .= ' WHERE PBF_PBE_KEY = '.$_POST['txtPEI_PBE_KEY'];
										$SQL .= ' AND PBF_SPALTEN > 0';
										
										//echo '<br>'.$SQL.'<br>';
										
				
										if($DB->Ausfuehren($SQL)===false)
										{
											die();
										}
									}
								}								
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('PEI_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE personaleinsaetze SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', PEI_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', PEI_userdat=sysdate';
					$SQL .= ' WHERE PEI_key=0' . $_POST['txtPEI_KEY'] . '';


					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
					}
				}

	$Form->DebugAusgabe(1,$SQL);
			}
		}
		$AWIS_KEY1=0;		// Zur�ck zur Liste
	}

	if(isset($_POST['txtPPR_KEY']))
	{
		//***********************************************
		// Personaleinsaetze
		//***********************************************
		$AWIS_KEY1=$_POST['txtPPR_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtPPR_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('PEI','PPR_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('PPR_TAG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PEI'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtPPR_KEY'])==0)
			{
				$Speichern = true;

				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO perseinspruefungen';
					$SQL .= '(PPR_TAG,PPR_PEB_KEY,PPR_STATUS,PPR_BEMERKUNG';
					$SQL .= ',PPR_USER, PPR_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('D',$_POST['txtPPR_TAG'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPPR_PEB_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPPR_STATUS'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPPR_BEMERKUNG'],true);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';


					if($DB->Ausfuehren($SQL)===false)
					{
						die();
					}
					$SQL = 'SELECT seq_PPR_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM perseinspruefungen WHERE PPR_key=' . $_POST['txtPPR_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('PPR_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE perseinspruefungen SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', PPR_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', PPR_userdat=sysdate';
					$SQL .= ' WHERE PPR_key=0' . $_POST['txtPPR_KEY'] . '';
					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
					}
				}

			}
		}
	
	}	

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>