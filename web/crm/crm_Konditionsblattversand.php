<?php

global $con;
global $AWISSprache;
global $CursorFeld;  // Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('CAD', '%');
$TextKonserven[] = array('CAQ', 'CAQ_VERTRAGSNUMMER');
$TextKonserven[] = array('CKV', '%');
$TextKonserven[] = array('MBW', 'txtAnredenListe');
$TextKonserven[] = array('Wort', 'AnzKondblattKBFIL');
$TextKonserven[] = array('Wort', 'AnzKondblattBAR');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3717 = awisBenutzerRecht($con, 3717, $AWISBenutzer->BenutzerName());
if ($Recht3717 == 0)
{
    awisEreignis(3, 1000, 'CRM', $AWISBenutzer->BenutzerName(), '', '', '');
    echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
    echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
    die();
}

if (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
    include('./crm_loeschen.php');
}
elseif (isset($_GET['CKVKEY']))
{
    $AWIS_KEY1 = intval($_GET['CKVKEY']);
}
elseif (isset($_POST['cmdSpeichern_x']))
{
    include('./crm_speichern.php');
}

$SQL = 'SELECT * FROM CRMKONDBLATTVERSAND';
if($AWIS_KEY1 > 0 or $AWIS_KEY1 === 0)
{
    $SQL .= ' WHERE CKV_KEY = ' . $AWIS_KEY1;
}

$rsCKV = awisOpenRecordset($con, $SQL);
$rsCKVZeilen = $awisRSZeilen;

if ($rsCKVZeilen == 0)
{
    $AWIS_KEY1 = 0;
}

echo '<form name=frmCRMAdressen action=./crm_Main.php?cmdAktion=Kondblattversand method=POST  enctype="multipart/form-data">';

awis_FORM_FormularStart();

if ($rsCKVZeilen > 1 or isset($_GET['CKVListe']))
{

    awis_FORM_ZeileStart();

    $Icons[] = array('new','./crm_Main.php?cmdAktion=Kondblattversand&CKVKEY=0');
    awis_FORM_Erstelle_ListeIcons($Icons,38,-1);

    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKV']['CKV_NAME'],450);
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKV']['CKV_QUELLE'],200);
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKV']['CKV_DATUM'],180);
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKV']['CKV_STATUS'],200);
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKV']['CKV_ANZOFFEN'],200);

    for($CKVZeile=0;$CKVZeile<$rsCKVZeilen;$CKVZeile++)
    {
        awis_FORM_ZeileStart();
        $Icons = array();
        $Icons[] = array('edit','./crm_Main.php?cmdAktion=Kondblattversand&CKVKEY='.$rsCKV['CKV_KEY'][$CKVZeile]);
        $Icons[] = array('delete','./crm_Main.php?cmdAktion=Kondblattversand&Seite=Kondblattversand&Del='.$rsCKV['CKV_KEY'][$CKVZeile]);

        awis_FORM_Erstelle_ListeIcons($Icons,38);

        awis_FORM_Erstelle_ListenFeld('CKV_NAME',$rsCKV['CKV_NAME'][$CKVZeile],20,450,false,($CKVZeile%2),'','','T');
        $Quellen = explode("|", $AWISSprachKonserven['CKV']['lst_CKV_QUELLE']);
        $Quellen = explode("~", $Quellen[$rsCKV['CKV_QUELLE'][$CKVZeile] -1]);
        awis_FORM_Erstelle_ListenFeld('CKV_QUELLE',$Quellen[1],20,200,false,($CKVZeile%2),'','','T');
        awis_FORM_Erstelle_ListenFeld('CKV_DATUM',$rsCKV['CKV_DATUM'][$CKVZeile],20,180,false,($CKVZeile%2),'','','DU');
        $Status = explode("|", $AWISSprachKonserven['CKV']['lst_CKV_STATUS']);
        $Status = explode("~", $Status[$rsCKV['CKV_STATUS'][$CKVZeile]]);
        awis_FORM_Erstelle_ListenFeld('CKV_STATUS',$Status[1],20,200,false,($CKVZeile%2),'','','T','C');
        awis_FORM_Erstelle_ListenFeld('CKV_ANZOFFEN',$rsCKV['CKV_ANZOFFEN'][$CKVZeile],20,200,false,($CKVZeile%2),'','','T');
        awis_FORM_ZeileEnde();
    }
}
else
{
    $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    echo '<input type=hidden name=txtCKV_KEY value=' . $AWIS_KEY1 . '>';

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Kondblattversand&CKVListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKV['CKV_KEY'][0])?$rsCKV['CKV_USER'][0]:'')));
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKV['CKV_KEY'][0])?$rsCKV['CKV_USERDAT'][0]:'')));
    awis_FORM_InfoZeile($Felder,'');

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKV']['CKV_NAME'] . ':', 150);
    awis_FORM_Erstelle_TextFeld('CKV_NAME', ($AWIS_KEY1 === 0 ? '' : $rsCKV['CKV_NAME'][0]), 50, 350,true);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKV']['CKV_QUELLE'] . ':', 150);
    $Quellen = explode("|", $AWISSprachKonserven['CKV']['lst_CKV_QUELLE']);
    awis_FORM_Erstelle_SelectFeld('CKV_QUELLE', ($AWIS_KEY1 === 0 ? '' : $rsCKV['CKV_QUELLE'][0]), 200, true, $con, '', $OptionBitteWaehlen, '', '', '', $Quellen, '');
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKV']['CKV_DATUM'] . ':', 150);
    awis_FORM_Erstelle_TextFeld('CKV_DATUM', ($AWIS_KEY1 === 0 ? '' : $rsCKV['CKV_DATUM'][0]), 50, 350,true);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKV']['CKV_STATUS'] . ':', 150);
    $Quellen = explode("|", $AWISSprachKonserven['CKV']['lst_CKV_STATUS']);
    $Quellen = explode("~", $Quellen[($AWIS_KEY1 === 0 ? 0 : $rsCKV['CKV_STATUS'][0])]);
    awis_FORM_Erstelle_TextLabel($Quellen[1],150);
    awis_FORM_ZeileEnde();

}

awis_FORM_FormularEnde();

//***************************************
// Schaltflaechen fuer dieses Register
//***************************************
awis_FORM_SchaltflaechenStart();

if ($Recht3717 !== 0)
{
    awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}
awis_FORM_SchaltflaechenEnde();

echo '</form>';