<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 01.12.2016
 * Time: 19:17
 */

require_once 'awisRest.inc';

$awisRest = new awisRest();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$SQL = ' Select to_char(mob_datum,\'DD\') as MOB_DATUM, MOB_ANZ from (';
$SQL .= ' select trunc(mob_datum) as mob_datum, count(*) as MOB_ANZ from MOBILITAETSGARANTIEN ';
$SQL .= ' WHERE mob_datum between add_months(trunc(sysdate,\'mm\'),-1) and last_day(add_months(trunc(sysdate,\'mm\'),-1)) ';
$SQL .= ' group by trunc(mob_datum) order by trunc(mob_datum)';
$SQL .= ' ) ';


$rs = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('MOB'));

$awisRest->sendeResponse($rs,'application/json');
