<?php
global $AWIS_KEY1;

/**
 * Abbrechen? Raus!
 */
if (isset($_POST['cmdLoeschenAbbrechen'])) {
    return;
}

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');

try {
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Tabelle = '';

    if (isset($_GET['Del']) AND !isset($_POST['cmdLoeschenOK'])) {
        if (isset($_GET['WGI_KEY_LIE_NR'])) {
            $SQL = 'SELECT *';
            $SQL .= ' FROM v_warengruppen_lie_nr ';
            $SQL .= ' WHERE wgi_key_lie_nr=' . $DB->WertSetzen('WGI', 'N0', $_GET['WGI_KEY_LIE_NR'], false);

            $rsDaten = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('WGI', true));

            $Tabelle = 'WGI';
            $Key = array();
            $Key[] = $rsDaten->FeldInhalt('WGI_KEY_LIE_NR');
            $Key[] = $rsDaten->FeldInhalt('WGI_KEY_WERTIGKEIT');

            $Felder = array();
            $Felder[] = array($Form->LadeTextBaustein('WGI', 'WGI_LIE_NAME'), $rsDaten->FeldInhalt('WGI_LIE_NAME'));
            $Felder[] = array($Form->LadeTextBaustein('WGI', 'WGI_LIE_NR'), $rsDaten->FeldInhalt('WGI_LIE_NR'));
            $Felder[] = array('Trennzeile', 'T');
            $Felder[] = array($Form->LadeTextBaustein('WGR', 'WGR_ID'), $rsDaten->FeldInhalt('WGI_WGR_ID'));
            if ($rsDaten->FeldInhalt('WGI_WUG_ID') != '') {
                $Felder[] = array($Form->LadeTextBaustein('WUG', 'WUG_ID'), $rsDaten->FeldInhalt('WGI_WUG_ID'));
            }
        }
    } elseif (isset($_POST['cmdLoeschenOK']))    // Loeschen durchführen
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) {
            case 'WGI':
                $SQL = 'DELETE FROM Warengruppeninfos WHERE WGI_KEY ';
                if (is_array($_POST['txtKey']) and count($_POST['txtKey']) > 1) {
                    $SQL .= ' in (' . implode(',', $_POST['txtKey']);
                    $SQL = substr($SQL, 0, strlen($SQL) - 1);
                    $SQL .= ')';
                } else {
                    $SQL .= '=' . $DB->WertSetzen('WGI', 'N0', $_POST['txtKey']);
                }
            default:
                break;
        }
        if ($SQL != '') {
            $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('WGI', true));
        }
    }

    if ($Tabelle != '') {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

        $Form->SchreibeHTMLCode('<form name=frmLoeschen method=post>');
        $Form->Formular_Start();
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
        $Form->ZeileEnde();

        foreach ($Felder AS $Feld) {
            $Form->ZeileStart();
            if ($Feld[0] == 'Trennzeile') {
                $Form->Trennzeile($Feld[1]);
            } else {
                $Form->Erstelle_TextLabel($Feld[0] . ':', 150);
                $Form->Erstelle_TextFeld('Feld', $Feld[1], 100, 500, false);
            }
            $Form->ZeileEnde();
        }

        $Form->Erstelle_HiddenFeld('Tabelle', $Tabelle);

        foreach ($Key as $K) {
            $Form->Erstelle_HiddenFeld('Key[]', $K);
        }

        $Form->Formular_Ende();
        $Form->SchaltflaechenStart();

        $Form->ZeileStart();
        $Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '', 'width: 50px', 50, 27, '', false, 'width: 50px !important');
        $Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '', 'width: 50px', 50, 27, '', false, 'width: 50px !important');
        $Form->ZeileEnde();
        $Form->SchaltflaechenEnde();
        $Form->SchreibeHTMLCode('</form>');

        die();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>