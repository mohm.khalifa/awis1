<?php

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('FFR', 'FFR_*');
$TextKonserven[] = array('FIL', '%');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('FKH', '*');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_Hilfe');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Liste', 'lst_JaNein');

try
{
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();

    $Form->DebugAusgabe(1, $_REQUEST);

    //Wenn Begruendung (FILIALE) vorhanden

    if (isset($_REQUEST["txtFFR_KEY"]) AND (isset($_REQUEST["txtFFR_BEGRUENDUNG"]) == isset($_REQUEST["oldFFR_BEGRUENDUNG"])))
    {

        $SQL = "Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=" . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);

        $rsUpdate = $DB->RecordSetOeffnen($SQL);

        $SQL = 'UPDATE FGRUECKFUEHRUNG SET ';
        $SQL .= ' FFR_FILID=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FILID"), false);
        $SQL .= ', FFR_WANR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_WANR"), false);
        $SQL .= ', FFR_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_VORGANGNR"), false);
        $SQL .= ', FFR_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_KFZKENNZ"), false);
        $SQL .= ', FFR_AEMBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_AEMBETRAG"), false);
        $SQL .= ', FFR_KASSENBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_KASSENBETRAG"), false);
        $SQL .= ', FFR_DIFFERENZ=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_DIFFERENZ"), false);
        $SQL .= ', FFR_FRG_KEY=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FRG_KEY"), false);
        $SQL .= ', FFR_BEGRUENDUNG=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_BEGRUENDUNG"), false);
        $SQL .= ', FFR_STATUS=' . $DB->FeldInhaltFormat('N0', 2, false);
        $SQL .= ', FFR_DIFFERENZDATUM=' . $DB->FeldInhaltFormat('DU', $rsUpdate->FeldInhalt("FFR_DIFFERENZDATUM"), false);
        $SQL .= ', FFR_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ', FFR_USERDAT=sysdate';
        $SQL .= ' WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt('FFR_KEY'), false);


        if ($DB->Ausfuehren($SQL) === false)
        {
            //throw new awisException('',,$SQL,2);
        }
    }

    if ($_REQUEST["oldFFR_BEGRUENDUNG"] == "" AND $_REQUEST["txtFFR_BEGRUENDUNG"] != "")
    {

        if (isset($_REQUEST["txtFFR_KEY"]))
        {

            $SQL = "Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=" . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);

            $rsUpdate = $DB->RecordSetOeffnen($SQL);

            $SQL = 'UPDATE FGRUECKFUEHRUNG SET ';
            $SQL .= ' FFR_FILID=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FILID"), false);
            $SQL .= ', FFR_WANR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_WANR"), false);
            $SQL .= ', FFR_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_VORGANGNR"), false);
            $SQL .= ', FFR_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_KFZKENNZ"), false);
            $SQL .= ', FFR_AEMBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_AEMBETRAG"), false);
            $SQL .= ', FFR_KASSENBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_KASSENBETRAG"), false);
            $SQL .= ', FFR_DIFFERENZ=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_DIFFERENZ"), false);
            $SQL .= ', FFR_FRG_KEY=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FRG_KEY"), false);
            $SQL .= ', FFR_BEGRUENDUNG=' . $DB->FeldInhaltFormat('T', $_REQUEST["txtFFR_BEGRUENDUNG"], false);
            $SQL .= ', FFR_STATUS=' . $DB->FeldInhaltFormat('N0', 3, false);
            $SQL .= ', FFR_DIFFERENZDATUM=' . $DB->FeldInhaltFormat('DU', $rsUpdate->FeldInhalt("FFR_DIFFERENZDATUM"), false);
            $SQL .= ', FFR_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ', FFR_USERDAT=sysdate';
            $SQL .= ' WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt('FFR_KEY'), false);

            $DB->Ausfuehren($SQL);
        }
    }
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>
