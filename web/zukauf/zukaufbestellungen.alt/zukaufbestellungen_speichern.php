<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('ZUB','ZUB_%');

//***********************************************************
// Zukaufbestellungen speichern
//***********************************************************

if(isset($_POST['txtZUB_KEY']))
{
	$AWIS_KEY1 = $_POST['txtZUB_KEY'];
	
	$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	
	if(intval($_POST['txtZUB_KEY'])===0)		// Neuer Artikel
	{
	awis_Debug(1, $_POST);
			// daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$Pflichtfelder = array();
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZUB'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$SQL = 'INSERT INTO ZukaufBestellungen';
		$SQL .= '(ZUB_ZLI_KEY,ZUB_BESTELLDATUM,ZUB_FIL_ID,ZUB_WANR,';
		$SQL .= 'ZUB_ARTIKELNUMMER,ZUB_ARTIKELBEZEICHNUNG,ZUB_HERSTELLER,ZUB_VERSANDARTBEZ,';
		$SQL .= 'ZUB_SORTIMENT,ZUB_BESTELLMENGE,ZUB_MENGENEINHEITEN,ZUB_TAUSCHTEIL,ZUB_PREISBRUTTO,';
		$SQL .= 'ZUB_PREISANGELIEFERT,ZUB_ALTTEILWERT,ZUB_EINHEITENPROPREIS,ZUB_WAEHRUNG,';
		$SQL .= 'ZUB_ABSCHLUSSART,ZUB_ABSCHLUSSGRUND,';
		$SQL .= 'ZUB_USER,ZUB_USERDAT';
		$SQL .= ')VALUES(';
		$SQL .= ' ' . awis_FeldInhaltFormat('Z',$_POST['txtZUB_ZLI_KEY'],false);
		$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtZUB_BESTELLDATUM'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZUB_FIL_ID'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_WANRKORR'],true);		// in das richtige Feld speichern!!
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_ARTIKELNUMMER'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_ARTIKELBEZEICHNUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_HERSTELLER'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_VERSANDARTBEZ'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_SORTIMENT'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N3',$_POST['txtZUB_BESTELLMENGE'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_MENGENEINHEITEN'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZUB_TAUSCHTEIL'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N3',$_POST['txtZUB_PREISBRUTTO'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N3',$_POST['txtZUB_PREISANGELIEFERT'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N3',$_POST['txtZUB_ALTTEILWERT'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZUB_EINHEITENPROPREIS'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_WAEHRUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZUB_ABSCHLUSSART'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZUB_ABSCHLUSSGRUND'],true);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
	awis_Debug(1,$SQL);	
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('zukaufartikel_speichern.php',1,$awisDBError['message'],'200709101821');
			die();
		}
		$SQL = 'SELECT seq_ZUB_KEY.CurrVal AS KEY FROM DUAL';
		$rsKey = awisOpenRecordset($con,$SQL);
		$AWIS_KEY1=$rsKey['KEY'][0];
		awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() ,$rsKey['KEY'][0]);
	}
	else 					// ge�nderter ZUB
	{
		$Felder = explode(';',awis_NameInArray($_POST, 'txtZUB',1,1));
		$FehlerListe = array();
		$UpdateFelder = '';
	
		awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() ,$_POST['txtZUB_KEY']);
		$rsZUB = awisOpenRecordset($con,'SELECT * FROM ZukaufBestellungen WHERE ZUB_key=' . $_POST['txtZUB_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
				// Alten und neuen Wert umformatieren!!
				switch ($FeldName)
				{
					default:
						$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				}
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsZUB[$FeldName][0],true);
		//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';
	
						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}
	
		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsZUB['ZUB_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE ZukaufBestellungen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', ZUB_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', ZUB_userdat=sysdate';
			$SQL .= ' WHERE ZUB_key=0' . $_POST['txtZUB_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('ZukaufBestellungen',1,'Fehler beim Speichern',$SQL);
			}
		}
awis_Debug(1,$SQL);	
		$AWIS_KEY1=$_POST['txtZUB_KEY'];
	}
	
	// Aktuellen ZUB speichern
	awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() ,$AWIS_KEY1);
}

//*******************************************************
// Sonderanfragen
//*******************************************************
if(isset($_POST['txtZSA_WANR']))
{
		$SQL = 'INSERT INTO ZukaufSonderanfragen';
		$SQL .= '(ZSA_WANR,ZSA_FIRMA, ZSA_USER,ZSA_USERDAT';
		$SQL .= ')VALUES(';
		$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtZSA_WANR'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZSA_FIRMA'],true);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('zukaufartikel_speichern.php',1,$awisDBError['message'],'200709101821');
			die();
		}
}
?>