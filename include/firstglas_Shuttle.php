<?php

require_once('awisFirstglasFunktionen.php');
require_once('awisVersicherungenFunktionen.php');
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
include_once('awisFirstglasImport_neu.php');
include_once('awisFirstglasStatus.php');

ini_set('max_execution_time', 0);

class firstglasShuttle
{

    private $_Funktionen;
    private $_DB;
    private $_AWISBenutzer;
    private $_FORM;
    private $_FirstglasImport;
    private $_FirstglasStatus;
    private $_DWHGeladen = true;
    //private $_DatumLetzteJobAusfuehrung = '';
    //private $_AktuellesDatum = '';
    private $_JobID;
    private $_pruefeMappings = array();
    private $_AktDatum = '';

    function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_FORM = new awisFormular();
        $this->_FirstglasImport = FirstGlasImport_neu::getInstance();
        $this->_FirstglasStatus = new awisFirstglasStatus();
        $this->_Funktionen = new VersicherungenFunktionen();
    }

    public function Shuttle()
    {
        $SQL = 'Select VJI_KEY,VJI_JOBID,VJI_DATUMJOBID from VERSJOBID ORDER BY VJI_KEY';
        $rsJOB = $this->_DB->RecordSetOeffnen($SQL);

        $this->_JobID = $rsJOB->FeldInhalt('VJI_JOBID');

        $this->SichereDWHTabelle();

        // pruefen ob Modul im aktuellen Verarbeitungsturnus schon lief und
        // wenn nicht dann Modul aufrufen
        if($this->_Funktionen->PruefeAnstehend('runImport', 'Firstglas'))
        {
            $this->_FirstglasImport->starteImport($this->_JobID);
        }
        if($this->_Funktionen->PruefeAnstehend('setStatus', 'Firstglas'))
        {
            $this->_FirstglasStatus->setStatus($this->_JobID);
        }
        $this->SichereDWHTabelle();
    }

    private function SichereDWHTabelle()
    {
        try
        {

            $SQL = 'ALTER TABLE SICH_WA_GLAS RENAME TO SICH_WA_GLAS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'ALTER TABLE SICH_GLASPOS RENAME TO SICH_GLASPOS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'ALTER TABLE SICH_GLASREP RENAME TO SICH_GLASREP_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_WA_GLAS AS (SELECT * FROM DWH.WA_GLAS@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_GLASPOS AS (SELECT * FROM DWH.GLASPOS@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_GLASREP AS (SELECT * FROM DWH.GLASREP@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_WA_GLAS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_GLASPOS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_GLASREP_TMP';
            $this->_DB->Ausfuehren($SQL);
        }
        catch(Exception $e)
        {
            echo($e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo($awis->getMessage() . $awis->SQL());
            die();
        }
    }
}

//Ende Klasse
?>