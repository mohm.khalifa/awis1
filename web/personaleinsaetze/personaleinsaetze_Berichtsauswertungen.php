<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht4507;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
    $TextKonserven = array();
    $TextKonserven[]=array('PEI','*');        
    $TextKonserven[]=array('PBM','PBM_XBN_KEY');
    $TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('Wort','Abschlussdatum');
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Auswertung');
	$TextKonserven[]=array('Liste','lst_PEI_AUSWERTUNGEN');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','FilialenAnzeigen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswertung');

    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4507 = $AWISBenutzer->HatDasRecht(4507);

	if($Recht4507==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	//$Form->DebugAusgabe(1,$_POST);
	if(!isset($_POST['cmdSuche_x']) and !isset($_POST['cmdBericht_x']))
	{	
		
		$Form->SchreibeHTMLCode('<form name="frmPersonaleinsatzAuswertungSuche" action="./personaleinsaetze_Main.php?cmdAktion=Berichtsauswertungen" method="POST"  enctype="multipart/form-data">');
		
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Berichtsauswertung'));
				
		$Form->Formular_Start();	
		
		// Datumsbereich festlegen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*PEI_DATUM_VOM',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('01.01.Y'))),10);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',200);
		$Form->Erstelle_TextFeld('*PEI_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('31.12.Y'))),10);		
		$Form->ZeileEnde();
		
		//Filiale
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_FIL_ID'].':',200);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$Form->Erstelle_SelectFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		}
		else
		{
			$Form->Erstelle_TextFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
		}
		$Form->ZeileEnde();				
		
		// Gebiet
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);

		//Alle Gebiete anzeigen	
		if (($Recht4507&2)==2)
		{
			$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
			$SQL .= ' FROM Filialebenen';
			$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
			$SQL .= ' ORDER BY 2';		
		}
		else 
		{
			$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
			$SQL .= ' FROM Filialebenen';
			$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
			$SQL .= ' AND EXISTS';
			$SQL .= '  (select DISTINCT XX1_FEB_KEY';
			$SQL .= '  FROM V_FILIALEBENENROLLEN_AKTUELL';
			$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
			$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)';
			$SQL .= '  WHERE  XX1_FEB_KEY = FEB_KEY';		
			$SQL .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();			
			$SQL .= ')';
			$SQL .= ' ORDER BY 2';	
			
			//echo $SQL;
			//die();	
		}		
		
		
		$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();
							
		//Bericht
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_BERICHTTYP'].':',200);
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		$SQL = 'SELECT PBE_KEY, PBE_BEZEICHNUNG as BEZ';
		$SQL .= ' FROM Personaleinsberichte ';
		$SQL .= ' ORDER BY PBE_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('*PBE_KEY',($Param['SPEICHERN']=='on'?$Param['PBE_KEY']:''),200,true,$SQL,'','','','','','');		
		$Form->ZeileEnde();							

		if (($Recht4507&4)==4)
		{
			// Bis auf FilialebeneAuswahl kann gespeichert werden
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FilialenAnzeigen'].':',200);
			$Form->Erstelle_Checkbox('*FilialenAnzeigen',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
			$Form->ZeileEnde();
		}
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
		$Form->Schaltflaeche('image', 'cmdBericht', '', '/bilder/cmd_statistik.png', $AWISSprachKonserven['Wort']['Auswertung'], 'W');		
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');		
	}
	elseif (isset($_POST['cmdBericht_x']))
	{
		$Form->DebugAusgabe(1, $_POST);
		$Ergs = array();
		$Ergs = _ErgsInit($Ergs);
				
		$Param = array();
	
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['FILIALANZAHL']=0;

		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_POST['sucPEI_DATUM_VOM'],true);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_POST['sucPEI_DATUM_BIS'],true);
		$Param['PEI_FIL_ID']=$Form->Format('N0',$_POST['sucPEI_FIL_ID'],true);
		$Param['FIL_GEBIET']=$Form->Format('N0',$_POST['sucFIL_GEBIET'],true);
		$Param['PBE_KEY']=$Form->Format('N0',$_POST['sucPBE_KEY'],true);
		
		$Param['FILIALENANZEIGEN']=isset($_POST['sucFilialenAnzeigen'])?'on':'';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_PEI_Berichtsauswertung',serialize($Param));
		
		$SQL  = 'SELECT DISTINCT FEB_KEY, FEB_BEZEICHNUNG, PFAD, PFAD_KEY, FEB_EBENE';
		$SQL .= ' FROM'; 
		$SQL .= '	(';
		$SQL .= '	SELECT'; 
    	$SQL .= '		sys_connect_by_path(feb_bezeichnung,\'/\')  AS pfad,';
    	$SQL .= '		sys_connect_by_path(feb_key,\'/\')          AS pfad_key,';
    	$SQL .= '		level stufe,';
    	$SQL .= '		feb_ebene,';
    	$SQL .= '		feb_key,';
    	$SQL .= '		feb_bezeichnung,';
    	$SQL .= '		feb_feb_key,';
    	$SQL .= '		connect_by_root feb_key feb_key_root';
    	$SQL .= '	FROM filialebenen';
    	$SQL .= '	WHERE trunc(FEB_GUELTIGAB) <= SYSDATE AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
    	$SQL .= '	CONNECT BY  prior feb_key = feb_feb_key';
    	$SQL .= '	)';
    	if (($Recht4507&2)==2)	//alle Gebiete anzeigen
    	{
			$SQL .= ' INNER JOIN v_filialebenenrollen_aktuell ON XX1_FEB_KEY = FEB_KEY';//TEST AND XX1_KON_KEY = 6400'; 
    	}
		else					//nur der aufrufenden Person zugeordnete Gebiete anzeigen
		{
    		$SQL .= ' INNER JOIN v_filialebenenrollen_aktuell ON XX1_FEB_KEY = FEB_KEY AND XX1_KON_KEY ='.$AWISBenutzer->BenutzerKontaktKEY(); 
			$SQL .= ' INNER JOIN KONTAKTE ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
		}
		$SQL .= ' INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)';
		$SQL .= ' INNER JOIN FilialebenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) >= trunc(SYSDATE)';
		$SQL .= ' INNER JOIN FilialebenenRollen ON XX1_FER_KEY = FER_KEY AND trunc(FER_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FER_GUELTIGBIS) >= trunc(SYSDATE)';
		if ($Param['FIL_GEBIET']=='0')
		{
			$SQL .= ' WHERE feb_key_root = ' .$AWISBenutzer->ParameterLesen('BerichtsauswertungRootEbene');
		}
		else
		{
			$SQL .= ' WHERE feb_key_root = ' .$Param['FIL_GEBIET'];
			//$SQL .= ' WHERE feb_key_root = ' .$AWISBenutzer->ParameterLesen('BerichtsauswertungRootEbene');
		}
		$SQL .= ' ORDER BY PFAD_KEY'; //Order nicht ändern, da wichtig für Folgebearbeitung

		//$Form->DebugAusgabe(1,$SQL);
		$rsGebiete = $DB->RecordSetOeffnen($SQL);
		
		//Datei und Bildschirmausgabe
		$DateiName = awis_UserExportDateiName('.csv');
		$fp = fopen($DateiName,'w+');

		$Zeile = 'Berichtsauswertungen ' .date("d.m.Y") ."\n";
		fputs($fp,$Zeile); 
		
		//Überschriften für Ausgabe am Bildschirm und als CSV
		$SQL = 'SELECT PBE_BEZEICHNUNG ';
		$SQL .= 'FROM Personaleinsberichte ';
		$SQL .= 'WHERE PBE_KEY=' .$Param['PBE_KEY'];
		$rsBericht = $DB->RecordSetOeffnen($SQL);
		
		if (!$rsBericht->EOF())
		{
			$Zeile = $rsBericht->Feldinhalt('PBE_BEZEICHNUNG') .' vom: '; 
		}
		else
		{
			$Zeile = 'Unbekannt vom: '; 
		}

		$Zeile.= $Param['PEI_DATUM_VOM'] .' bis: ';
		$Zeile.= $Param['PEI_DATUM_BIS']; 
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Zeile,600,'font-weight:bold');
		$Form->ZeileEnde();
		
		$Zeile.= "\n";
		fputs($fp,$Zeile); 
		$Zeile = "GEBIET;FILIALEN;GRÜN;GELB;ROT;GESAMT\n";
		fputs($fp,$Zeile);
		
		$Form->Trennzeile('O');
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Gebiet',130);
		$Form->Erstelle_Liste_Ueberschrift('Filialen',150);
		$Form->Erstelle_Liste_Ueberschrift('Grün',130);
		$Form->Erstelle_Liste_Ueberschrift('Gelb',150);
		$Form->Erstelle_Liste_Ueberschrift('Rot',120);
		$Form->Erstelle_Liste_Ueberschrift('Gesamt',80);
		$Form->ZeileEnde();
		$EbenenMerkerListe = array(); //Merkerliste für bereits ausgegebene Gebiete
		$EbenenStufeMerker = 0;	//Stufe der letzen ausgegebenen Ebene	
		while (!$rsGebiete->EOF())
		{
			$PfadListe = array();
			$PfadListe = explode('/', $rsGebiete->FeldInhalt('PFAD')); //Bsp.: "/D/Bayern/BY1"
			$PfadKeyListe = array();
			$PfadKeyListe = explode('/', $rsGebiete->FeldInhalt('PFAD_KEY')); //Bsp: "/369/371/377"
			$i = 0;
			
			foreach($PfadKeyListe as $Gebiet)
			{
				if (!isset($EbenenMerkerListe[$i]))
				{
					$EbenenMerkerListe[$i] = '---';
				}
				if ($Gebiet <> '')
				{
					if ($EbenenMerkerListe[$i] <> $Gebiet) //Neue Ebene, Werte bestimmen
					{
						if ($i < $EbenenStufeMerker) //Bei Ebenenrücksprung Trennzeile
						{
							$Form->Trennzeile('O');
						}
						$EbenenStufeMerker = $i;
						
						$EbenenMerkerListe[$i] = $Gebiet ; //Gebiet als bereits ausgegeben merken
						$PfadAktuell = '/'.$Gebiet;
						
						$SQL = _SQLErstellenGebiete($Param, $Gebiet);
						//$Form->DebugAusgabe(1,$SQL);
						$rsResult = $DB->RecordSetOeffnen($SQL);
						$SummeBerichte=0;
						$Ergs = _ErgsInit($Ergs);
						while (!$rsResult->EOF())
						{
							$Ergs[$rsResult->Feldinhalt('IDX')] = $rsResult->Feldinhalt('ANZAHL');
							$SummeBerichte+=$rsResult->FeldInhalt('ANZAHL');
							$rsResult->DSWeiter();
						}
						$Form->ZeileStart();
						if (isset($PfadKeyListe[$i+1])) //gibts noch eine Nachfolgeebene? Wenn ja dann bold
						{
							$Form->Erstelle_TextLabel($PfadListe[$i],150,'font-weight:bold');
							$Form->Erstelle_TextLabel($Param['FILIALANZAHL'],150,'font-weight:bold');
							$Form->Erstelle_TextLabel($Ergs['1'],140,'font-weight:bold');
							$Form->Erstelle_TextLabel($Ergs['2'],140,'font-weight:bold');
							$Form->Erstelle_TextLabel($Ergs['3'],140,'font-weight:bold');
							$Form->Erstelle_TextLabel($SummeBerichte,140,'font-weight:bold');
						}
						else
						{
							$Form->Erstelle_TextLabel($PfadListe[$i],150);
							$Form->Erstelle_TextLabel($Param['FILIALANZAHL'],150);
							$Form->Erstelle_TextLabel($Ergs['1'],140);
							$Form->Erstelle_TextLabel($Ergs['2'],140);
							$Form->Erstelle_TextLabel($Ergs['3'],140);
							$Form->Erstelle_TextLabel($SummeBerichte,140);
						}
						$Form->ZeileEnde();
							
						$Zeile = $PfadListe[$i] .';';
						$Zeile.= $Param['FILIALANZAHL'] .';';
						$Zeile.= $Ergs['1'] .';';
						$Zeile.= $Ergs['2'] .';';
						$Zeile.= $Ergs['3'] .';';
						$Zeile.= $SummeBerichte ."\n";
						fputs($fp,$Zeile);
						
						//evtl zugeordnete Filialen für diesen Pfad anzeigen
						
						if ($Param['FILIALENANZEIGEN']=='on')
						{
							$SQL  = 'SELECT DISTINCT xx1_fil_id, fil_bez';
							$SQL .= ' FROM V_FILIALEBENENROLLEN v';
							$SQL .= ' INNER JOIN filialebenen feb';
							$SQL .= ' ON feb.feb_key = v.xx1_feb_key';
							$SQL .= ' INNER JOIN filialebenenzuordnungen fez';
							$SQL .= ' ON fez.fez_key = v.xx1_fez_key';
							$SQL .= ' INNER JOIN filialen fil';
							$SQL .= ' ON fil.fil_id = v.xx1_fil_id';
							$SQL .= ' WHERE XX1_PFADKEY=' .$DB->FeldInhaltFormat('T',$PfadAktuell);
							$SQL .= ' AND TRUNC(FEB_GUELTIGAB)   <= TRUNC(SYSDATE)';
							$SQL .= ' AND TRUNC(FEB_GUELTIGBIS)  >= TRUNC(SYSDATE)';
							$SQL .= ' AND TRUNC(FEZ_GUELTIGAB)   <= TRUNC(SYSDATE)';
							$SQL .= ' AND TRUNC(FEZ_GUELTIGBIS)  >= TRUNC(SYSDATE)';
							$SQL .= ' ORDER BY xx1_fil_id';

							//$Form->DebugAusgabe(1,$SQL, $PfadAktuell);
							$rsFil = $DB->RecordSetOeffnen($SQL);
							
							if (!$rsFil->EOF())
							{
								while (!$rsFil->EOF())
								{
									$SQL = _SQLErstellenFiliale($Param, $rsFil->FeldInhalt('XX1_FIL_ID'));
									//$Form->DebugAusgabe(1,$SQL);
									$rsResult = $DB->RecordSetOeffnen($SQL);
									$SummeBerichte=0;
									$Ergs = _ErgsInit($Ergs);
									while (!$rsResult->EOF())
									{
										$Ergs[$rsResult->Feldinhalt('IDX')] = $rsResult->Feldinhalt('ANZAHL');
										$SummeBerichte+=$rsResult->FeldInhalt('ANZAHL');
										$rsResult->DSWeiter();
									}
									$Form->ZeileStart();
									$Form->Erstelle_TextLabel($rsFil->FeldInhalt('XX1_FIL_ID'),150);
									$Form->Erstelle_TextLabel($Param['FILIALANZAHL'],150);
									$Form->Erstelle_TextLabel($Ergs['1'],140);
									$Form->Erstelle_TextLabel($Ergs['2'],140);
									$Form->Erstelle_TextLabel($Ergs['3'],140);
									$Form->Erstelle_TextLabel($SummeBerichte,140);
									$Form->ZeileEnde();
		
									$Zeile = str_pad($rsFil->FeldInhalt('XX1_FIL_ID'), 3,'0',STR_PAD_LEFT) .' - '.$rsFil->FeldInhalt('FIL_BEZ') .';';
									$Zeile.= $Param['FILIALANZAHL'] .';';
									$Zeile.= $Ergs['1'] .';';
									$Zeile.= $Ergs['2'] .';';
									$Zeile.= $Ergs['3'] .';';
									$Zeile.= $SummeBerichte ."\n";
									fputs($fp,$Zeile);
									$rsFil->DSWeiter();
								}
								$Form->Trennzeile('O');
							}
							
						}
						
					}
				}
				
				$i++;
			}

			$rsGebiete->DSWeiter();	
		}//while

		fclose($fp);
		$DateiName = pathinfo($DateiName);
		
		$Form->Trennzeile('O');
		$Form->ZeileStart();
		$Form->SchreibeHTMLCode('<a href=/export/' . $DateiName["basename"] . '>Berichtsdatei Öffnen</a>');
		$Form->ZeileEnde();
			
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/personaleinsaetze/personaleinsaetze_Main.php?cmdAktion=Berichtsauswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->SchaltflaechenEnde();		
	}
	else
	{	
		$Param = array();
	
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_POST['sucPEI_DATUM_VOM'],true);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_POST['sucPEI_DATUM_BIS'],true);
		$Param['PEI_FIL_ID']=$Form->Format('N0',$_POST['sucPEI_FIL_ID'],true);
		$Param['FIL_GEBIET']=$Form->Format('N0',$_POST['sucFIL_GEBIET'],true);
		$Param['PBE_KEY']=$Form->Format('N0',$_POST['sucPBE_KEY'],true);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_PEI_Berichtsauswertung',serialize($Param));
		
		$Bedingung = _BedingungErstellen($Param);
		
		$SQL = 'select ergebnis, count(*) as Anzahl ';
		$SQL.= ' from(';
		$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok, quote,';//xx1_ebene, Region,';
		$SQL.= ' case when quote >= 90 then \'01 - GRUEN\' when quote < 90 and quote >= 80 then \'02 - GELB\' else \'03 - ROT\' end as Ergebnis';
		$SQL.= ' from (';
		$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok,';
		$SQL.= ' round(100 * anzfelderok / decode(anzfelder,0,1,anzfelder),0) as quote';
		$SQL.= ' from (';
		$SQL.= ' select distinct pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where (pez_wert = \'1\' or pez_wert = \'0\') and pez_pei_key = pei_key) as anzfelder';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'1\' and pez_pei_key = pei_key) as anzfelderok';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'0\' and pez_pei_key = pei_key) as anzfeldernichtok';
		$SQL.= ' from personaleinsaetze p';
		$SQL.= ' inner join personaleinsberichte on pbe_key = pei_pbe_key';
		$SQL.= ' inner join filialen on pei_fil_id = fil_id ';
		$SQL.= ' where pei_pez_status = 1';						
		
		if($Bedingung!='')
		{
			$SQL .= ' AND ' . substr($Bedingung,4);
		}		
		
		$SQL.=')))';				
		$SQL.= ' group by ergebnis';
		$SQL.= ' order by ergebnis';
		$Form->DebugAusgabe(1,$SQL);
		$rsPEI = $DB->RecordSetOeffnen($SQL);

		$SummeBerichte=0;
		
		//Überschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Status',200);
		$Form->Erstelle_Liste_Ueberschrift('Anzahl',200);
		$Form->ZeileEnde();
		
		while (!$rsPEI->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('Erg', substr($rsPEI->FeldInhalt('ERGEBNIS'),5),10,200);
			$Form->Erstelle_ListenFeld('Anz', $rsPEI->FeldInhalt('ANZAHL'),10,200);
			$Form->ZeileEnde();
			
			$SummeBerichte+=$rsPEI->FeldInhalt('ANZAHL');
			
			$rsPEI->DSWeiter();			
		}
				
		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('Sum', 'Summe',10,200,false,0,'font-weight:bold');
		$Form->Erstelle_ListenFeld('Anz', $Form->Format('N0',$SummeBerichte),10,200,false,0,'font-weight:bold');
		$Form->ZeileEnde();				
		
		$Form->Trennzeile('O');
		$Form->Trennzeile('O');		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Fehlerquote Einzelkategorien',500,'font-weight:bold');
		$Form->ZeileEnde();
		
		//Überschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
		$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
		$Form->Erstelle_Liste_Ueberschrift('OK',100);
		$Form->Erstelle_Liste_Ueberschrift('Gesamt',100);
		$Form->ZeileEnde();

		
		//Bericht TYP A 
		if (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '1')
		{		
			$SummeNichtOk=0;

			$Summe['A'] = Auswertung('A', 'Außenbereich', '2,3', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Verkauf', '5,6,7,8,9,10', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Werkstatt', '12', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			$Summe['D'] = Auswertung('D', 'Lager', '14,15,16', $Bedingung);
			$SummeNichtOk += $Summe['D'];
			$Summe['E'] = Auswertung('E', 'Filialorganisation', '18', $Bedingung);
			$SummeNichtOk += $Summe['E'];
			$Summe['F'] = Auswertung('F', 'Sonderthemen', '400', $Bedingung);
			$SummeNichtOk += $Summe['F'];

			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Außenbereich',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Verkauf',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Werkstatt',$Summe['C'],$SummeNichtOk);
			Auswertung_2('D', 'Lager',$Summe['D'],$SummeNichtOk);
			Auswertung_2('E', 'Filialorganisation',$Summe['E'],$SummeNichtOk);
			Auswertung_2('F', 'Sonderthemen',$Summe['F'],$SummeNichtOk);
		}
		//QS
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '2')
		{		
			$SummeNichtOk=0;
			
			//Berichtsauswertung je nach Kategorie
			$Summe['A'] = Auswertung('A', 'Allgemein', '21,22,23,24', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Inspektionen', '26,27', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Positonen', '29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,59', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			
			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Allgemein',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Inspektionen',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Positonen',$Summe['C'],$SummeNichtOk);
		}
		//TB
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '3')
		{	
			$SummeNichtOk=0;
				
			//Berichtsauswertung je nach Kategorie
			$Summe['A'] = Auswertung('A', 'Außenbereich', '101,102,103,104', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Verkaufsraum', '106,107', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Lager', '109,110,111', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			$Summe['D'] = Auswertung('D', 'Werkstatt', '113,115,116,117,118', $Bedingung);
			$SummeNichtOk += $Summe['D'];
			$Summe['E'] = Auswertung('E', 'Arbeitssicherheit', '120,121', $Bedingung);
			$SummeNichtOk += $Summe['E'];
			$Summe['F'] = Auswertung('F', 'Organisation', '123,124,125', $Bedingung);
			$SummeNichtOk += $Summe['F'];
			$Summe['G'] = Auswertung('G', 'Qualitäts und Dienstleistungsmanagement', '127,128,129,130,131,132', $Bedingung);
			$SummeNichtOk += $Summe['G'];
			$Summe['H'] = Auswertung('H', 'Personal', '134,135,136', $Bedingung);
			$SummeNichtOk += $Summe['H'];
			$Summe['I'] = Auswertung('I', 'Personalgespräche', '138', $Bedingung);
			$SummeNichtOk += $Summe['I'];
			$Summe['J'] = Auswertung('J', 'Zusammenfassung', '140', $Bedingung);
			$SummeNichtOk += $Summe['J'];
			
			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Außenbereich',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Verkaufsraum',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Lager',$Summe['C'],$SummeNichtOk);
			Auswertung_2('D', 'Werkstatt',$Summe['D'],$SummeNichtOk);
			Auswertung_2('E', 'Arbeitssicherheit',$Summe['E'],$SummeNichtOk);
			Auswertung_2('F', 'Organisation',$Summe['F'],$SummeNichtOk);
			Auswertung_2('G', 'Qualitäts und Dienstleistungsmanagement',$Summe['G'],$SummeNichtOk);
			Auswertung_2('H', 'Personal',$Summe['H'],$SummeNichtOk);
			Auswertung_2('I', 'Personalgespräche',$Summe['I'],$SummeNichtOk);
			Auswertung_2('J', 'Zusammenfassung',$Summe['J'],$SummeNichtOk);
		}
		//GB
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '4')
		{		
			$SummeNichtOk=0;
			
			//Berichtsauswertung je nach Kategorie
			$Summe['A'] = Auswertung('A', 'Außenbereich', '151,152,153,154,155', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Verkaufsraum', '157,158,159,160,161,162', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Lager', '164,165,166,167', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			$Summe['D'] = Auswertung('D', 'Werkstatt', '169,170,171', $Bedingung);
			$SummeNichtOk += $Summe['D'];
			$Summe['E'] = Auswertung('E', 'Organisation', '173,174', $Bedingung);
			$SummeNichtOk += $Summe['E'];
			$Summe['F'] = Auswertung('F', 'Qualitäts und Dienstleistungsmanagement', '176,177,178', $Bedingung);
			$SummeNichtOk += $Summe['F'];
			$Summe['G'] = Auswertung('G', 'Personal', '180,181,182,183', $Bedingung);
			$SummeNichtOk += $Summe['G'];
			$Summe['H'] = Auswertung('H', 'Zusammenfassung', '185', $Bedingung);
			$SummeNichtOk += $Summe['H'];
			
			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Außenbereich',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Verkaufsraum',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Lager',$Summe['C'],$SummeNichtOk);
			Auswertung_2('D', 'Werkstatt',$Summe['D'],$SummeNichtOk);
			Auswertung_2('E', 'Organisation',$Summe['E'],$SummeNichtOk);
			Auswertung_2('F', 'Qualitäts und Dienstleistungsmanagement',$Summe['F'],$SummeNichtOk);
			Auswertung_2('G', 'Personal',$Summe['G'],$SummeNichtOk);
			Auswertung_2('H', 'Zusammenfassung',$Summe['H'],$SummeNichtOk);
		}
		//TC
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '5')
		{		
			$SummeNichtOk=0;
			
			//Berichtsauswertung je nach Kategorie
			$Summe['A'] = Auswertung('A', 'Außenbereich', '201,202,203,204', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Verkaufsraum', '206,207,208', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Lager', '210,211,212,213', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			$Summe['D'] = Auswertung('D', 'Werkstatt', '215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233', $Bedingung);
			$SummeNichtOk += $Summe['D'];
			$Summe['E'] = Auswertung('E', 'Arbeitssicherheit', '235,236,237,238', $Bedingung);
			$SummeNichtOk += $Summe['E'];
			$Summe['F'] = Auswertung('F', 'Werkstattorganisation', '240,241,242,243', $Bedingung);
			$SummeNichtOk += $Summe['F'];
			$Summe['G'] = Auswertung('G', 'Organisation', '245,246', $Bedingung);
			$SummeNichtOk += $Summe['G'];
			$Summe['H'] = Auswertung('H', 'Qualitäts und Dienstleistungsmanagement', '248,249,250,251,252,253,254,255,256,257,258,259,260', $Bedingung);
			$SummeNichtOk += $Summe['H'];
			$Summe['I'] = Auswertung('I', 'Personal', '262,263,264,265,266,267', $Bedingung);
			$SummeNichtOk += $Summe['I'];
			$Summe['J'] = Auswertung('J', 'Personalgespräche', '269,270,271,272,273', $Bedingung);
			$SummeNichtOk += $Summe['J'];
			$Summe['K'] = Auswertung('K', 'Zusammenfasung', '275', $Bedingung);
			$SummeNichtOk += $Summe['K'];
			
			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Außenbereich',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Verkaufsraum',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Lager',$Summe['C'],$SummeNichtOk);
			Auswertung_2('D', 'Werkstatt',$Summe['D'],$SummeNichtOk);
			Auswertung_2('E', 'Arbeitssicherheit',$Summe['E'],$SummeNichtOk);
			Auswertung_2('F', 'Werkstattorganisation',$Summe['F'],$SummeNichtOk);
			Auswertung_2('G', 'Organisation',$Summe['G'],$SummeNichtOk);
			Auswertung_2('H', 'Qualitäts und Dienstleistungsmanagement',$Summe['H'],$SummeNichtOk);
			Auswertung_2('I', 'Personal',$Summe['I'],$SummeNichtOk);
			Auswertung_2('J', 'Personalgespräche',$Summe['J'],$SummeNichtOk);
			Auswertung_2('K', 'Zusammenfassung',$Summe['K'],$SummeNichtOk);			
		}
		//GC
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '6')
		{
			$SummeNichtOk=0;
			
			//Berichtsauswertung je nach Kategorie
			$Summe['A'] = Auswertung('A', 'Außenbereich', '301,302,303,304', $Bedingung);
			$SummeNichtOk += $Summe['A'];
			$Summe['B'] = Auswertung('B', 'Verkaufsraum', '306,307,308,309,310,311,312,313,315,315', $Bedingung);
			$SummeNichtOk += $Summe['B'];
			$Summe['C'] = Auswertung('C', 'Lager', '317,318,319,320,321', $Bedingung);
			$SummeNichtOk += $Summe['C'];
			$Summe['D'] = Auswertung('D', 'Werkstatt', '323,324,325', $Bedingung);
			$SummeNichtOk += $Summe['D'];
			$Summe['E'] = Auswertung('E', 'Arbeitssicherheit', '328,329,330', $Bedingung);
			$SummeNichtOk += $Summe['E'];
			$Summe['F'] = Auswertung('F', 'Organisation', '332,333,334,335,336,337,339,340,341', $Bedingung);
			$SummeNichtOk += $Summe['F'];
			$Summe['G'] = Auswertung('G', 'Qualitäts und Dienstleistungsmanagement', '344,345,346,347,348,349,350,351,352,353,354,355,356', $Bedingung);
			$SummeNichtOk += $Summe['G'];
			$Summe['H'] = Auswertung('H', 'Personal', '358,359,360,361,362,363,364', $Bedingung);
			$SummeNichtOk += $Summe['H'];
			$Summe['I'] = Auswertung('I', 'Personalgespräche', '366,367,368,369,370,371', $Bedingung);
			$SummeNichtOk += $Summe['I'];
			$Summe['J'] = Auswertung('J', 'Zusammenfassung', '373', $Bedingung);
			$SummeNichtOk += $Summe['J'];
			
			$Form->Trennzeile('O');
			$Form->Trennzeile('O');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
			$Form->ZeileEnde();
			
			//Überschrift
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
			$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
			$Form->ZeileEnde();

			Auswertung_2('A', 'Außenbereich',$Summe['A'],$SummeNichtOk);
			Auswertung_2('B', 'Verkaufsraum',$Summe['B'],$SummeNichtOk);
			Auswertung_2('C', 'Lager',$Summe['C'],$SummeNichtOk);
			Auswertung_2('D', 'Werkstatt',$Summe['D'],$SummeNichtOk);
			Auswertung_2('E', 'Arbeitssicherheit',$Summe['E'],$SummeNichtOk);
			Auswertung_2('F', 'Organisation',$Summe['F'],$SummeNichtOk);
			Auswertung_2('G', 'Qualitäts und Dienstleistungsmanagement',$Summe['G'],$SummeNichtOk);
			Auswertung_2('H', 'Personal',$Summe['H'],$SummeNichtOk);
			Auswertung_2('I', 'Personalgespräche',$Summe['I'],$SummeNichtOk);
			Auswertung_2('J', 'Zusammenfassung',$Summe['J'],$SummeNichtOk);			
		}
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '7')
		{
		    $SummeNichtOk=0;
		    	
		    //Berichtsauswertung je nach Kategorie
		    $Summe['A'] = Auswertung('A', 'Filiale: Außenbereich, optisches Erscheinungsbild,Ersteindruck', '411,471,472,412,413,414', $Bedingung);
		    $SummeNichtOk += $Summe['A'];
		    $Summe['B'] = Auswertung('B', 'Verkaufsraum', '416,425,426,427,428,417,418,419,420,421,422,423,424', $Bedingung);
		    $SummeNichtOk += $Summe['B'];
		    $Summe['C'] = Auswertung('C', 'Lager', '430,431,432', $Bedingung);
		    $SummeNichtOk += $Summe['C'];
		    $Summe['D'] = Auswertung('D', 'Werkstatt', '434,443,444,445,446,447,448,449,450,451,452,435,436,437,438,439,440,441,442', $Bedingung);
		    $SummeNichtOk += $Summe['D'];
		    $Summe['E'] = Auswertung('E', 'Filialorganisation', '454,455,456,457', $Bedingung);
		    $SummeNichtOk += $Summe['E'];
		    $Summe['F'] = Auswertung('F', 'Arbeitssicherheit', '459,460,461,462', $Bedingung);
		    $SummeNichtOk += $Summe['F'];
		    $Summe['G'] = Auswertung('G', 'Sauberkeit', '464,465', $Bedingung);
		    $SummeNichtOk += $Summe['G'];
		    $Summe['H'] = Auswertung('H', 'Information / Kommunikation', '467,468,469,470', $Bedingung);
		    $SummeNichtOk += $Summe['H'];
		 	
		    $Form->Trennzeile('O');
		    $Form->Trennzeile('O');
		    	
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
		    $Form->ZeileEnde();
		    	
		    //Überschrift
		    $Form->ZeileStart();
		    $Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
		    $Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
		    $Form->ZeileEnde();
		
		    Auswertung_2('A', 'Filiale: Außenbereich, optisches Erscheinungsbild,Ersteindruck',$Summe['A'],$SummeNichtOk);
		    Auswertung_2('B', 'Verkaufsraum',$Summe['B'],$SummeNichtOk);
		    Auswertung_2('C', 'Lager',$Summe['C'],$SummeNichtOk);
		    Auswertung_2('D', 'Werkstatt',$Summe['D'],$SummeNichtOk);
		    Auswertung_2('E', 'Filialorganisation',$Summe['E'],$SummeNichtOk);
		    Auswertung_2('F', 'Arbeitssicherheit',$Summe['F'],$SummeNichtOk);
		    Auswertung_2('G', 'Sauberkeit',$Summe['G'],$SummeNichtOk);
		    Auswertung_2('H', 'Information / Kommunikation',$Summe['H'],$SummeNichtOk);

		}
		elseif (isset($Param['PBE_KEY']) and $Param['PBE_KEY'] == '8')
		{
		    $SummeNichtOk=0;
		     
		    //Berichtsauswertung je nach Kategorie
		    $Summe['A'] = Auswertung('A', 'Filialausstattung Werbematerialien', '478,479,480', $Bedingung);
		    $SummeNichtOk += $Summe['A'];
		    $Summe['B'] = Auswertung('B', 'Personal', '482,483,484,485,486,489,490,491', $Bedingung);
		    $SummeNichtOk += $Summe['B'];
		    $Summe['C'] = Auswertung('C', 'Schadensidentifikation', '493,494,495,496,497,498', $Bedingung);
		    $SummeNichtOk += $Summe['C'];
		    $Summe['D'] = Auswertung('D', 'Verkaufssituation', '500,501,502,503,504,505', $Bedingung);
		    $SummeNichtOk += $Summe['D'];
		    $Summe['E'] = Auswertung('E', 'Warenwirtschaft', '507,508,509,510,511,512,513,514,515', $Bedingung);
		    $SummeNichtOk += $Summe['E'];

		
		    $Form->Trennzeile('O');
		    $Form->Trennzeile('O');
		     
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
		    $Form->ZeileEnde();
		     
		    //Überschrift
		    $Form->ZeileStart();
		    $Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
		    $Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
		    $Form->ZeileEnde();
		
		    Auswertung_2('A', 'Filialausstattung Werbematerialien:',$Summe['A'],$SummeNichtOk);
		    Auswertung_2('B', 'Personal',$Summe['B'],$SummeNichtOk);
		    Auswertung_2('C', 'Schadensidentifikation',$Summe['C'],$SummeNichtOk);
		    Auswertung_2('D', 'Verkaufssituation',$Summe['D'],$SummeNichtOk);
		    Auswertung_2('E', 'Warenwirtschaft',$Summe['E'],$SummeNichtOk);

		}
				
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/personaleinsaetze/personaleinsaetze_Main.php?cmdAktion=Berichtsauswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->SchaltflaechenEnde();		
	}
				
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
			
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function Auswertung($Kategorie, $Bezeichnung, $IDs, $Bedingung)
{
	global $Form;
	global $DB;		
	
	//nicht o.K. Kategorie A
	$SQL= 'SELECT SUM(anzfelder) as gesamt, sum(anzfelderok) as ok, sum(anzfeldernichtok) as nichtok';
	$SQL.= ' from(';
	$SQL.= ' select distinct pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status';//, xx1_ebene, f2.feb_bezeichnung as Region';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and (pez_wert = \'1\' or pez_wert = \'0\') and pez_pei_key = pei_key) as anzfelder';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and pez_wert = \'1\' and pez_pei_key = pei_key) as anzfelderok';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and pez_wert = \'0\' and pez_pei_key = pei_key) as anzfeldernichtok';
	$SQL.= ' from personaleinsaetze p';
	$SQL.= ' inner join personaleinsberichte on pbe_key = pei_pbe_key';
	$SQL.= ' inner join filialen on fil_id = pei_fil_id';
	$SQL.= ' where pei_pez_status = 1';
	
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}			
	
	$SQL.= ' )';			
	//$Form->DebugAusgabe(1,$SQL);
	$rsPEI = $DB->RecordSetOeffnen($SQL);
		
	$Form->ZeileStart();
	$Form->Erstelle_ListenFeld($Kategorie,$Kategorie.' - '.$Bezeichnung,10,350);
	
	
	if ($rsPEI->FeldInhalt('GESAMT') == '0' or $rsPEI->FeldInhalt('GESAMT') == '')
	{
		$Gesamt = 1;
	}
	else 
	{
		$Gesamt = $rsPEI->FeldInhalt('GESAMT');
	}
	
	$Prozent = $Form->Format('N2', $rsPEI->FeldInhalt('NICHTOK') * 100 / $Gesamt);		
	$Form->Erstelle_ListenFeld('nichtok',$rsPEI->FeldInhalt('NICHTOK'),10,80);
	$Form->Erstelle_ListenFeld('nichtokprozent',$Prozent.' %',10,100);
	
	$Form->Erstelle_ListenFeld('ok',$rsPEI->FeldInhalt('OK'),10,100);
	$Form->Erstelle_ListenFeld('gesamt',$rsPEI->FeldInhalt('GESAMT'),10,100);		
	$Form->ZeileEnde();
	
	return $rsPEI->FeldInhalt('NICHTOK');		
}


function Auswertung_2($Kategorie, $Bezeichnung, $Summe, $SummeNichtOk)
{
	global $Form;
	global $DB;
	
	if ($SummeNichtOk==0)
	{
		$SummeNichtOk=1;
	}
	
	$Form->ZeileStart();
	$Prozent = $Form->Format('N2', $Summe * 100 / $SummeNichtOk);					
	$Form->Erstelle_ListenFeld($Kategorie,$Kategorie.' - '.$Bezeichnung,10,350);
	$Form->Erstelle_ListenFeld('nichtok',$Summe,10,80);			
	$Form->Erstelle_ListenFeld('nichtokprozent',$Prozent.' %',10,100);
	$Form->ZeileEnde();
}

function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht4507;

	$Bedingung = '';
	if (isset($Param['PBE_KEY']) and $Param['PBE_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PBE_KEY = '.$Param['PBE_KEY'];
	}
	
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$Bedingung .= ' AND FIL_ID =' .$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID']);
		
		if (($Recht4507&2)!=2)
		{		
			$Bedingung .= ' AND FIL_ID IN (SELECT FEZ_FIL_ID FROM FILIALEBENENZUORDNUNGEN WHERE FEZ_FEB_KEY IN (';			
			$Bedingung .= 'SELECT FEB_KEY';//, FEB_BEZEICHNUNG';
			$Bedingung .= ' FROM Filialebenen';
			$Bedingung .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
			$Bedingung .= ' AND EXISTS';
			$Bedingung .= '  (select DISTINCT XX1_FEB_KEY';
			$Bedingung .= '  FROM V_FILIALEBENENROLLEN';
			$Bedingung .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
			$Bedingung .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)';
			$Bedingung .= '  WHERE  XX1_FEB_KEY = FEB_KEY';		
			$Bedingung .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();			
			$Bedingung .= '))';
			$Bedingung .= ' AND trunc(FEZ_GUELTIGAB) <= trunc(sysdate) and trunc(fEz_gueltigbis) >= trunc(sysdate))';
		}		
	}

	if($Param['FIL_GEBIET']!='0')
	{
		$Bedingung .= "AND FIL_ID IN (0";
		$FilObj = new awisFilialen();
		$FilListe = $FilObj->FilialenEinerEbene($DB->FeldInhaltFormat('N0',$Param['FIL_GEBIET'],false));
		foreach($FilListe AS $FilialZeile)
		{
			$Bedingung .= ','.$FilialZeile['FEZ_FIL_ID'];
		}
		$Bedingung .= ')';
	}

	$Param['WHERE']=$Bedingung;
	return $Bedingung;
}

function _ErgsInit($Ergs)
{
	$Ergs['1'] = '0';
	$Ergs['2'] = '0';
	$Ergs['3'] = '0';
	return $Ergs;
}

function _SQLErstellenGebiete(&$Param, $Gebiet=0)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht4507;

	$Bedingung = '';
	if (isset($Param['PBE_KEY']) and $Param['PBE_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PBE_KEY = '.$Param['PBE_KEY'];
	}
	
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	$i=0;
	if($Gebiet!=0)
	{
		$Bedingung .= "AND FIL_ID IN (0";
		$FilObj = new awisFilialen();
		$FilListe = $FilObj->FilialenEinerEbene($Gebiet);
		//var_dump($FilListe);
		foreach($FilListe AS $FilialZeile)
		{
			$Bedingung .= ','.$FilialZeile['FEZ_FIL_ID'];
			$i++;
		}
		$Bedingung .= ')';
	}
	$Param['FILIALANZAHL'] = $i;
	
	$Param['WHERE']=$Bedingung;

	$SQL = 'select idx, count(*) as Anzahl ';
	$SQL.= ' from(';
	$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok, quote,';//xx1_ebene, Region,';
	$SQL.= ' case when quote >= 90 then \'1\' when quote < 90 and quote >= 80 then \'2\' else \'3\' end as idx';
	$SQL.= ' from (';
	$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok,';
	$SQL.= ' round(100 * anzfelderok / decode(anzfelder,0,1,anzfelder),0) as quote';
	$SQL.= ' from (';
	$SQL.= ' select distinct pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where (pez_wert = \'1\' or pez_wert = \'0\') and pez_pei_key = pei_key) as anzfelder';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'1\' and pez_pei_key = pei_key) as anzfelderok';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'0\' and pez_pei_key = pei_key) as anzfeldernichtok';
	$SQL.= ' from personaleinsaetze p';
	$SQL.= ' inner join personaleinsberichte on pbe_key = pei_pbe_key';
	$SQL.= ' inner join filialen on pei_fil_id = fil_id ';
	$SQL.= ' where pei_pez_status = 1';						
		
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}		
		
	$SQL.=')))';				
	$SQL.= ' group by idx';
	$SQL.= ' order by idx';

	//echo ($SQL);
	//die();
	return $SQL;
}

function _SQLErstellenFiliale(&$Param, $Filiale=0)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht4507;

	$Bedingung = '';
	if (isset($Param['PBE_KEY']) and $Param['PBE_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PBE_KEY = '.$Param['PBE_KEY'];
	}
	
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	if($Filiale!=0)
	{
		$Bedingung .= 'AND FIL_ID=' .$Filiale;
	}
	$Param['FILIALANZAHL'] = 1;
	
	$Param['WHERE']=$Bedingung;

	$SQL = 'select idx, count(*) as Anzahl ';
	$SQL.= ' from(';
	$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok, quote,';//xx1_ebene, Region,';
	$SQL.= ' case when quote >= 90 then \'1\' when quote < 90 and quote >= 80 then \'2\' else \'3\' end as idx';
	$SQL.= ' from (';
	$SQL.= ' select pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status, anzfelder, anzfelderok, anzfeldernichtok,';
	$SQL.= ' round(100 * anzfelderok / decode(anzfelder,0,1,anzfelder),0) as quote';
	$SQL.= ' from (';
	$SQL.= ' select distinct pei_key, pei_datum, pei_fil_id, pei_pbe_key, pei_pez_status';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where (pez_wert = \'1\' or pez_wert = \'0\') and pez_pei_key = pei_key) as anzfelder';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'1\' and pez_pei_key = pei_key) as anzfelderok';
	$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'0\' and pez_pei_key = pei_key) as anzfeldernichtok';
	$SQL.= ' from personaleinsaetze p';
	$SQL.= ' inner join personaleinsberichte on pbe_key = pei_pbe_key';
	$SQL.= ' inner join filialen on pei_fil_id = fil_id ';
	$SQL.= ' where pei_pez_status = 1';						
		
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}		
		
	$SQL.=')))';				
	$SQL.= ' group by idx';
	$SQL.= ' order by idx';

	//echo ($SQL);
	//die();
	return $SQL;
}

?>

