<?php
require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisProtokoll.inc';

/**
 * Created by PhpStorm.
 * User: patrickgebhardt
 * Date: 19.07.17
 * Time: 10:18
 */
class awisZukaufSonderanfragen
{

    /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * @var awisFormular
     */
    private $_Form;

    /**
     * @var awisBenutzer
     */
    private $_Benutzer;

    /**
     * @var awisProtokoll
     */
    private $_Protokoll;

    /**
     * @var int
     */
    private $_DebugLevel;

    /**
     * @var resource
     */
    private $_Prozess;

    /**
     * awisZukaufSonderanfragen constructor.
     * @param $Benutzer
     * @param int $DebugLebel
     */
    function __construct($Benutzer, $DebugLevel = 999)
    {
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
        $this->_DebugLevel = $DebugLevel;
        $this->_Protokoll = new awisProtokoll($this->_DB,$this->_Benutzer);
        $this->_Protokoll->Init($DebugLevel,'Sonderanfragen','');
    }


    public function VerarbeiteSonderanfragen()
    {
        $this->_Protokoll->KonsolenAusgabe('Verarbeite Sonderanfragen...',10,awisProtokoll::AUSGABETYP_INFO);

        $SQL = 'select';
        $SQL .= '     ZUKAUFSONDERANFRAGEN.*,';
        $SQL .= '     LIN_WERT as LIN_501_PHP_SCRIPT';
        $SQL .= ' from';
        $SQL .= '     ZUKAUFSONDERANFRAGEN';
        $SQL .= '     inner join LIEFERANTEN on ZSA_FIRMA = LIE_NR';
        $SQL .= '     inner join LIEFERANTENINFOS on';
        $SQL .= '         LIN_LIE_NR = LIE_NR';
        $SQL .= '     and';
        $SQL .= '         LIN_ITY_KEY = 501';
        $SQL .= ' order by';
        $SQL .= '     ZSA_USERDAT asc';

        $rsZSA = $this->_DB->RecordSetOeffnen($SQL);

        if ($rsZSA->AnzahlDatensaetze() > 0) {

            $this->_Protokoll->KonsolenAusgabe('Starte Anfrage von '. $rsZSA->FeldInhalt('ZSA_USER') . ' bei ' . $rsZSA->FeldInhalt('ZSA_FIRMA'),10,awisProtokoll::AUSGABETYP_INFO);

            //Befehl zusammenbauen
            $Befehl = '/usr/local/php/bin/php -c /etc/php.ini -f /daten/jobs/bin/' . $rsZSA->FeldInhalt('LIN_501_PHP_SCRIPT');
            $Befehl .= ' -- --benutzer awis_jobs --abfrage Bestellung ';

            if ($rsZSA->FeldInhalt('ZSA_DATUM_VON') != '') { //Anfrage mit Datum
                $Befehl .= ' --starttag ' . $rsZSA->FeldInhalt('ZSA_DATUM_VON','D');
                $Befehl .= ' --endtag ' . $rsZSA->FeldInhalt('ZSA_DATUM_BIS','D');
            }

            if($rsZSA->FeldInhalt('ZSA_WANR')!=''){ //Anfrage nur f�r eine WA-Nummer
                $Befehl .= ' --wanummer ' . $rsZSA->FeldInhalt('ZSA_WANR');
            }

            //Jeder Lieferant sollte eine eigene Log haben..
            $Logs = '/var/log/awis/zukauf_sonderanfrage_' . $rsZSA->FeldInhalt('ZSA_FIRMA') . '.log';

            $P = array(
                0 => array('pipe', 'r'),
                1 => array('file', $Logs, 'a'),
                2 => array('file', $Logs, 'a')
            );

            $this->_Protokoll->KonsolenAusgabe('Befehl: ' . $Befehl,50,awisProtokoll::AUSGABETYP_INFO);
            $this->_Protokoll->KonsolenAusgabe('Log: ' . $Logs,50,awisProtokoll::AUSGABETYP_INFO);

            $this->_Prozess = proc_open($Befehl, $P, $pip, '/daten/jobs', null);
            if (is_resource($this->_Prozess)) {
                $this->_Protokoll->KonsolenAusgabe('Anfrage hat folgende PID bekommen: ' .proc_get_status($this->_Prozess)['pid'] ,50,awisProtokoll::AUSGABETYP_INFO);
                while (proc_get_status($this->_Prozess) !== false and proc_get_status($this->_Prozess)['running'] === true) {
                    $this->_Protokoll->KonsolenAusgabe('Prozess l�uft noch. Warte 10 Sekunden... ',50,awisProtokoll::AUSGABETYP_INFO);
                    sleep(10);
                }

                //Kein schlimmer Fehler aufgetreten? => L�schen
                if(!$this->_Protokoll->IstAufgetreten(awisProtokoll::AUSGABETYP_FATALERFEHLER)){
                    $SQL = 'delete from ZUKAUFSONDERANFRAGEN where ZSA_KEY = ' . $this->_DB->WertSetzen('ZSA','Z',$rsZSA->FeldInhalt('ZSA_KEY'));
                    $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('ZSA'));
                }

            }else{
                $this->_Protokoll->KonsolenAusgabe('Habe keine Resource bekommen... Breche ab!',1,awisProtokoll::AUSGABETYP_FATALERFEHLER);
            }

        } else {
            $this->_Protokoll->KonsolenAusgabe('Keine Sonderanfrage zu bearbeiten!',1,awisProtokoll::AUSGABETYP_INFO);
        }

    }
}