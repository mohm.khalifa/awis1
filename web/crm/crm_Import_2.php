<?php
/**
 * Importmodul f�r die Hoppenstedt Dateien
 * Diese liegen als CSV (TAB getrennt) vor
 * Es gibt zwei unterschiedliche Dateien (CAD = Adressen und CAP = Ansprechpartner)
 * 
 */
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
ini_set('max_execution_time', 900);
require_once('awisDatenbank.inc');
try
{
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('CAD','%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_FalschesFormat');
	$TextKonserven[]=array('Wort','lbl_zurueck');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonservenImport = $Form->LadeTexte($TextKonserven);
	$Recht3701 = $AWISBenutzer->HatDasRecht(3701);
	if($Recht3701==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	// Hochgeladene Datei direkt verarbeiten
	$Datei = $_FILES['datAdressen']['tmp_name'];
	$fp = @fopen($Datei, 'r');
	if($fp===false)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_keineUploadDatei']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
			
	
	$Zeile = fgets($fp);
	
	if(substr($Zeile,0,4)=='AWIS')
	{
		$Zeile = fgets($fp);			// N�chste Zeile lesen	
	}
	
	$FeldNamen = array_flip(explode(';',str_replace('"', '', $Zeile)));
	if(!isset($FeldNamen['FZDNR']))
	{
		$FeldNamen = array_flip(explode("\t",str_replace('"', '', $Zeile)));	
	}

	if(array_key_exists('FZDNR', $FeldNamen)===true)
	{
		if(array_key_exists('FNAME', $FeldNamen))			// Adressenimport
		{
			$ImportTyp = 'CAD';			
		}
		elseif(array_key_exists('P_VNAME', $FeldNamen))		// Ansprechpartner
		{
			$ImportTyp = 'CAP';
		}
		else 
		{
	    	echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']." (1)</span>";
			echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
			die();
		}
	}
	else
	{
	    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']." (2)</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$Werkzeug = new awisWerkzeuge();
	$Pfad = $AWISBenutzer->ParameterLesen('CRMAdressenImportPfad',true);
	$Erg = move_uploaded_file($_FILES['datAdressen']['tmp_name'], $Pfad.'/'.$Werkzeug->awisLevel().'/'.$_FILES['datAdressen']['name'].'~'.$AWISBenutzer->BenutzerName(1).'~'.($_POST['txtAktion']==1?'P':'I').($_POST['txtCAD_KON_KEY']>0?'~'.$_POST['txtCAD_KON_KEY']:''));
	$Form->ZeileStart();
	if($Erg===false)
	{
		$Form->Erstelle_TextLabel('Fehler beim Zugriff auf die hochgeladene Datei. Bitte Entwickler benachrichtigen..', 0);
	}
	else 
	{
		
		if($_POST['txtAktion']==1)
		{
			$Form->Erstelle_TextLabel('Die Datei wird gepr&uuml;ft. Das Ergebnis wird Ihnen per E-Mail zugesendet.', 0);
		}
		else 
		{
			$Form->Erstelle_TextLabel('Die Datei wird importiert. Das Protokoll des Imports wird Ihnen per E-Mail zugesendet.', 0);
		}
		
	}	
	$Form->ZeileEnde();
	$Form->Formular_Ende();
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
//unlink($_FILES['datAdressen']['tmp_name']);
?>