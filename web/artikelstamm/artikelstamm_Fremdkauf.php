<?php
/**
 * Fremdkauf
 *
 * Wird nicht in den Filialen angezeigt => keine Bildschirmbreitenanpassung
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200901
 *
 */
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

require_once('awisATUArtikel.inc');

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('FRK', '%');
$TextKonserven[] = array('IMQ', 'FRK_MENGE');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'txt_Fremdkauf_%');

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht400 = $AWISBenutzer->HatDasRecht(400);            // Recht f�r die Lieferantenartikel
    if ($Recht400 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    
    $Sort = '';
    if (isset($_GET['FRKSort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['FRKSort']);
        $Sort = $_GET['FRKSort'];
    } elseif (isset($_POST['txtFRKSort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_POST['txtFRKSort']);
        $Sort = $_POST['txtFRKSort'];
    } else {
        $ORDERBY = ' ORDER BY FRK_DATUM DESC, FRK_LFDNR, FRK_FIL_ID';
    }

    $DB->SetzeBindevariable('FRK', 'var_NO_ast_key', '0' . $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);

    $SQL = "select DISTINCT Daten.*, OEN_Key ";
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= " FROM (SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE";
    $SQL .= " FRK_AST_ATUNR = (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE ast_key = :var_NO_ast_key)";
    $SQL .= " OR (FRK_AST_ATUNR IS NULL AND FRK_OEN_NUMMER = (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE ast_key = :var_NO_ast_key))";
    $SQL .= " UNION SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE FRK_OEN_SUCHNUMMER IN ";
    $SQL .= " (SELECT TEI_SUCH2 FROM AWIS.TeileInfos WHERE TEI_ITY_ID2='OEN' AND TEI_KEY1 = :var_NO_ast_key)) DATEN";
    $SQL .= " 	  	LEFT OUTER JOIN OENUmmern ON OENUMMERN.OEN_NUMMER = Daten.FRK_OEN_Nummer";

    $UserFilialen = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
    if (($Recht400 & 1) == 1 AND $UserFilialen != '') {
        if (count(explode(',', $UserFilialen)) == 1) {
            $DB->SetzeBindevariable('FRK', 'var_NO_fil_id', $UserFilialen, awisDatenbank::VAR_TYP_GANZEZAHL);
            $SQL .= " WHERE FRK_QUELLE='F' AND FRK_FIL_ID = :var_NO_fil_id";
        } else {
            $SQL .= " WHERE FRK_QUELLE='F' AND FRK_FIL_ID IN (" . $UserFilialen . ")";
        }
    }
    $Form->DebugAusgabe(1, $SQL);
    $Artikel = new awisATUArtikel();
    $Artikel->LadeArtikel($AWIS_KEY1, true);
    $FremdKauf = $Artikel->Fremdkaeufe();
    $Form->SchreibeHTMLCode($AWISSprachKonserven['Wort']['txt_Fremdkauf_24'] . ': ' . $FremdKauf[24] . '. ' . $AWISSprachKonserven['Wort']['txt_Fremdkauf_12'] . ': ' . $FremdKauf[12] . '.');

    // Zum Bl�ttern in den Daten
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }

    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('FRK', false));

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

    $rsFRK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FRK', false));

    $Form->Formular_Start();
    $Form->ZeileStart();

    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_FIL_ID' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_FIL_ID'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_FIL_ID'], 55, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_DATUM' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_DATUM'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_DATUM'], 100, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_LFDNR' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_LFDNR'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_LFDNR'], 65, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_KBA' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_KBA'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_KBA'], 100, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_MENGE' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_MENGE'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_MENGE'], 60, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_GRUND' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_GRUND'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_GRUND'], 250, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_OEN_NUMMER' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_OEN_NUMMER'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_OEN_NUMMER'], 110, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_ARTBEZ' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_ARTBEZ'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_ARTBEZ'], 250, '', $Link);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_PREIS' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_PREIS'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_PREIS'], 80, '', $Link, '', 'R');
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf';
    $Link .= '&FRKSort=FRK_RABATT' . ((isset($_GET['FRKSort']) AND ($_GET['FRKSort'] == 'FRK_RABATT'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRK']['FRK_RABATT'], 80, '', $Link, '', 'R');

    $Form->ZeileEnde();

    $DS = 0;
    while (!$rsFRK->EOF()) {
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('*FRK_FIL_ID', $rsFRK->FeldInhalt('FRK_FIL_ID'), 10, 55, false, ($DS % 2), '', '', 'T', 'C');
        $Form->Erstelle_ListenFeld('*FRK_DATUM', $rsFRK->FeldInhalt('FRK_DATUM'), 10, 100, false, ($DS % 2), '', '', 'D', 'L', $rsFRK->FeldInhalt('FRK_FIRMA'));
        $Form->Erstelle_ListenFeld('*FRK_LFDNR', $rsFRK->FeldInhalt('FRK_LFDNR'), 10, 65, false, ($DS % 2), '', '', 'N0', 'L');
        $Form->Erstelle_ListenFeld('*FRK_KBA', $rsFRK->FeldInhalt('FRK_KBA'), 10, 100, false, ($DS % 2), '', '', 'T', 'L');
        $Form->Erstelle_ListenFeld('*FRK_MENGE', $rsFRK->FeldInhalt('FRK_MENGE'), 10, 60, false, ($DS % 2), '', '', 'N0', 'L');
        $Text = (strlen($rsFRK->FeldInhalt('FRK_GRUND')) > 26?substr($rsFRK->FeldInhalt('FRK_GRUND'), 0, 24) . '...':$rsFRK->FeldInhalt('FRK_GRUND'));
        $Form->Erstelle_ListenFeld('*FRK_GRUND', $Text, 10, 250, false, ($DS % 2), '', '', 'T', 'L', $rsFRK->FeldInhalt('FRK_GRUND'));
        $Form->Erstelle_ListenFeld('*FRK_OEN_NUMMER', $rsFRK->FeldInhalt('FRK_OEN_NUMMER'), 10, 110, false, ($DS % 2), '', '', 'T', 'L', $rsFRK->FeldInhalt('FRK_OEN_NUMMER'));
        $Text = (strlen($rsFRK->FeldInhalt('FRK_ARTBEZ')) > 25?substr($rsFRK->FeldInhalt('FRK_ARTBEZ'), 0, 23) . '...':$rsFRK->FeldInhalt('FRK_ARTBEZ'));
        $Form->Erstelle_ListenFeld('*FRK_ARTBEZ', $Text, 10, 250, false, ($DS % 2), '', '', 'T', 'L', $rsFRK->FeldInhalt('FRK_ARTBEZ'));
        $Form->Erstelle_ListenFeld('*FRK_PREIS', $rsFRK->FeldInhalt('FRK_PREIS'), 10, 80, false, ($DS % 2), '', '', 'N2', 'R');
        $Form->Erstelle_ListenFeld('*FRK_RABATT', $Form->Format('Nx', str_replace(',','.',$rsFRK->FeldInhalt('FRK_RABATT'))) . '%', 10, 80, false, ($DS % 2), '', '', 'T', 'R');

        $Form->ZeileEnde();
        $DS++;

        $rsFRK->DSWeiter();
    }

    $BlaetternSort = '';
    $BlaetternSort = '&FRKSort='. $Sort;

    $Form->Erstelle_HiddenFeld('FRKSort', $Sort);
    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Fremdkauf&AST_KEY=' . $AWIS_KEY1. $BlaetternSort;
    $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201320");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201321");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>