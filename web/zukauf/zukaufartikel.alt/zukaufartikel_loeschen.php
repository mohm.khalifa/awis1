<?php
global $Param;
global $awisRSInfo;
global $awisDBError;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWISBenutzer;
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

//awis_Debug(1,$_POST,$_GET);
$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamter Mitbewerber
{
	$Tabelle = 'ZLA';
	$Key=$_POST['txtZLA_KEY'];

	$Felder=array();
	$Felder[]=array(awis_TextKonserve($con,'ZLA_ARTIKELNUMMER','ZLA',$AWISSprache),$_POST['txtZLA_ARTIKELNUMMER']);
	$Felder[]=array(awis_TextKonserve($con,'ZLA_BEZEICHNUNG','ZLA',$AWISSprache),$_POST['txtZLA_BEZEICHNUNG']);
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	if(isset($_GET['Unterseite']))
	{
		switch($_GET['Unterseite'])
		{
			case 'XXX':
				$Tabelle = 'XXX';
				$Key=$_GET['Del'];
		
				$SQL = 'SELECT XXX ';
				$SQL .= ' FROM XXX';
				$SQL .= ' WHERE XXX_KEY=0'.$Key;
				$rsDaten = awisOpenRecordset($con, $SQL);
				$Felder=array();
	
				$Felder[]=array(awis_TextKonserve($con,'XXX','XXX',$AWISSprache),$rsDaten['XXX'][0]);
				break;
		}
	}
	else 
	{
		switch($_GET['Seite'])
		{
			case 'XXX':
				$Tabelle = 'XXX';
				$Key=$_GET['Del'];
	
				$rsDaten = awisOpenRecordset($con, 'SELECT XXX'.$Key);
				$Felder=array();
	
				$Felder[]=array(awis_TextKonserve($con,'XXX','XXX',$AWISSprache),$rsDaten['XXX'][0]);
				break;
		}
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{

//awis_Debug(1,$_POST);
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'ZLA':
			$SQL = 'DELETE FROM ZukaufLieferantenArtikel WHERE ZLA_key=0'.$_POST['txtKey'];
			awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName(),'');
			$AWIS_KEY1=0;
			break;
		default:
			break;
	}

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)===false)
		{
			awisErrorMailLink('zukaufartikel_loeschen_1',1,$awisDBError['messages'],'200709101914');
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./zukaufartikel_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}
?>