<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

/*$strSql="select ban_kuerzel from bank order by ban_kuerzel";

$arrKuerzel = abfrage("ora",$strSql);
$intZeilen = $arrKuerzel["zeilen"];
$arrKuerzel = $arrKuerzel["trefferarray"];

$strBankOption='';

for($i=0;$i<$intZeilen;$i++)
{
	$strBankOption.="<option>".$arrKuerzel["BAN_KUERZEL"][$i]."</option>";
}*/

$strSql="select count(*) anzahl from kontoauszuege where IKA_TRANSFER_IN_KONTOAUSZUEGE=0 and IKA_LOESCH=0";

$arrOffen = abfrage("ora",$strSql);
$intAnzOffen = $arrOffen["trefferarray"]["ANZAHL"][0];

?>
<html>
<head>
	<title>Offene Vorg&auml;nge</title>
	<?php include("card/css/css_card_master.php"); ?>
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	<script language="JavaScript">
	
	function init()		//Aufruf �ber <body onload="init();">
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
		document.ondblclick = zoom;
	}
	
	function kontoauszug()
	{
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit("�bersicht Kontoausz�ge anzeigen");	//Zeitmessung
		<?php } ?>
			
		var parameter = "intDialog=1";
	
		<?php echo dialog("kazuebersicht_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			var intKazId=rueckgabe;
			
			parameter += "&intKazId="+intKazId+"&intKeinNeuButton=1";
			
			<?php echo dialog("kazeinzelsatz_frameset.php"); ?>
			
			parent.card_navigation.aktualisieren();		//Fenster neu einlesen
		}
	}
	
	function geklickt(strGruppe)
	{
		var obj = window.event.srcElement;
		
		var objId= obj.id;
		
		var arrObj=document.getElementsByName(strGruppe);
		
		if(obj.checked)
		{
			for(i=0;i<arrObj.length;i++)
			{
				if(arrObj[i].id!=objId)
					arrObj[i].checked=false;
			}
		}
		else
		{
			obj.checked=true;
			return;
		}
		
		/*if(strGruppe=='auswahl')
		{
			switch(objId) {
				case "ausw1":
					einblenden("bearb");
				break;
				case "ausw2":
					einblenden("import");
				break;
				case "ausw3":
					einblenden("manuell");
				break;
				case "ausw4":
					einblenden("suchen");
				break;
			}
		}*/
	}
	
	//Blendet Elemente gem. Auswahl ein bzw. aus
	
	/*function einblenden(strVariante)
	{
		var obj = document.all;
	
		for(i=0;i<obj.length;i++)
		{
			
			if(obj[i].className==strVariante+'_verst')
			{
				if( !((obj[i].id=="nnzeile"||obj[i].id=="nnzeileleer") && (document.getElementById("nntext")==null || document.getElementById("nntext").firstChild.nodeValue==0)) )
				{
					if(obj[i].id=="nnzeile"||obj[i].id=="nnzeileleer")
						obj[i].style.display="inline";
					
					obj[i].className=strVariante+'_sicht';
				}
				else
				{
					obj[i].style.display="none";
				}
			}
			else
			{
				if(obj[i].id=="nnzeile"||obj[i].id=="nnzeileleer")
				{
					obj[i].style.display="none";
				}
					
				if(obj[i].className.search(/_sicht$/)!=-1)
				{
					strKlasse=obj[i].className.replace(/_sicht$/,'')
					obj[i].className=strKlasse+'_verst';
				}
			}
		}
	}*/
	
	function senden()
	{
		strIdChecked=checkboxtest("auswahl");
		
		switch(strIdChecked) {
			case "ausw1":
				send_bearbeiten();
			break;
			case "ausw2":
				//send_importieren();
				send_importieren("impo2");
			break;
			case "ausw3":
				//send_manuell_eingeben();
				send_importieren("impo3");
			break;
			/*case "ausw4":
				send_suchen();
			break;*/
		}
	}
	
	function send_bearbeiten()
	{
		kontoauszug();
	}
	
	//function send_importieren()
	function send_importieren(strIdChecked)
	{
		//Datumspr�fung
		/*var datobj = document.getElementById('kazdatum');
		
		if(datobj.value!=''&&!datumspruefung(datobj))
		{
			return;
		}*/
		
		//Werte ermitteln
		//Auswahl Checkbox
		//strIdChecked=checkboxtest("import");
		//Bank
		/*var objBank=document.getElementById('bank');
		var strKazBank=objBank.options[objBank.selectedIndex].text;*/
		//Datum
		/*var strKazDatum=document.getElementById('kazDatum').value;*/
		
		var strUrl='?strIdChecked='+strIdChecked;
		
		/*if(strKazBank!='')
			strUrl+='&strKazBank='+strKazBank;
			
		if(strKazDatum!='')
			strUrl+='&strKazDatum='+strKazDatum;*/
				
		<?php
		if($intZeitmessung)
		{
		?>
			top.card_ladezeit.sendezeit("Kontoausz�ge importieren");	//Zeitmessung
		<?php } ?>
		parent.card_import.location.href='card_import_kontoauszuege.php'+strUrl;
	}
	
	/*function send_manuell_eingeben()
	{
		alert("Diese Funktion steht noch nicht zur Verf�gung");
	}*/
	
	/*function send_suchen()
	{
		alert("Diese Funktion steht noch nicht zur Verf�gung");
	}*/
	
	/*function datumheute()
	{
		var obj = window.event.srcElement;
		obj.value = '<?php echo strftime("%d.%m.%y");?>';
		window.event.cancelBubble = true;
	}*/	
	
	//Ermittelt die Id der ausgew�hlten Checkbox einer Checkboxgruppe (name-Attribut) bei �bergabe des name-Attributes
	
	function checkboxtest(strGruppe)
	{
		var arrObj=document.getElementsByName(strGruppe);
	
		var strIdChecked='';
	
		for(i=0;i<arrObj.length&&strIdChecked=='';i++)
		{
			if(arrObj[i].checked)
				strIdChecked=arrObj[i].id;
		}
		return strIdChecked;
	}
	
	</script>
</head>
<body onload="init();">
	
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="100%">
<tr><td align="center">

	<table border="0" cellpadding="0" cellspacing="0" align="center" id="tabelleaussen" style="table-layout:fixed;">
		<colgroup>
        <col id="spalte1">
		<col id="spalte2">
		<col id="spalte3">
     	</colgroup>
		<tr><td class="fett" align="center" colspan="3">Kontoausz&uuml;ge</td></tr>
		<tr><td colspan="3" class="unten">&nbsp;</td></tr>
		<tr><td align="center"><input type="checkbox" name="auswahl" id="ausw1" onclick="geklickt('auswahl');" checked></td><td colspan="2">bearbeiten (&uuml;bernommen)</td></tr>
		<!--<tr><td align="center"><input type="checkbox" name="auswahl" id="ausw2" onclick="geklickt('auswahl');"></td><td colspan="2">importieren</td></tr>
		<tr><td align="center"><input type="checkbox" name="auswahl" id="ausw3" onclick="geklickt('auswahl');"></td><td colspan="2">manuell eingeben</td></tr>
		<tr><td align="center"><input type="checkbox" name="auswahl" id="ausw4" onclick="geklickt('auswahl');"></td><td colspan="2">suchen</td></tr>
		<tr><td colspan="3" class="oben">&nbsp;</td></tr>
		<tr class="import_verst"><td colspan="3" class="titel">importieren</td></tr>
		<tr class="import_verst"><td colspan="3" class="unten">&nbsp;</td></tr>
		<tr class="import_verst">
		<td align="center"><input type="checkbox" name="import" id="impo1" onclick="geklickt('import');" checked></td>
		<td colspan="2">neuer Tagesimport</td>
		</tr>-->
		<tr class="import_verst"  id="nnzeile">
		<!--<td align="center" rowspan="2"><input type="checkbox" name="import" id="impo2" onclick="geklickt('import');"></td>-->
		<td align="center" rowspan="2"><input type="checkbox" name="auswahl" id="ausw2" onclick="geklickt('auswahl');"></td>
		<td colspan="2" rowspan="2">noch nicht &uuml;bernommen (<span id="nntext"><?php echo $intAnzOffen;?></span>)</td>
		</tr>
		<tr class="import_verst" id="nnzeileleer">&nbsp;</tr>
		<tr class="import_verst">
		<!--<td align="center"><input type="checkbox" name="import" id="impo3" onclick="geklickt('import');"></td>-->
		<td align="center"><input type="checkbox" name="auswahl" id="ausw3" onclick="geklickt('auswahl');"></td>
		<td colspan="2">nicht &uuml;bernommen</td>
		</tr>
		<!--<tr class="import_verst">
		<td>&nbsp;</td>
		<td>Bank</td>
		<td class="import_verst">
		<select id="bank" class="datum">
		<option></option>
		<?php echo $strBankOption; ?>
		</select>
		</td>
		</tr>
		<tr class="import_verst">
		<td>&nbsp;</td>
		<td>Datum</td>
		<td><input type="text" class="datum" maxlength="8" id="kazdatum" ondblclick="datumheute();"></td>
		</tr>-->
		<tr class="import_verst">
		<td colspan="3" class="oben">&nbsp;</td>
		</tr>
		<tr><td align="center" colspan="3"><button type="button" onclick="senden();">senden</button></td></tr>
	</table>
</td></tr>
</table>
</body>
</html>
