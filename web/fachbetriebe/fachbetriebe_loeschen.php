<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try 
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	$Tabelle= '';
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtFAB_KEY']))
		{
			$Tabelle = 'FAB';
			$Key=$_POST['txtFAB_KEY'];
	
			$SQL = 'SELECT *';
			$SQL .= ' FROM fachbetriebe ';
			$SQL .= ' WHERE FAB_KEY=0'.$Key;
			
			$rsDaten = $DB->RecordsetOeffnen($SQL);
			$HauptKey = $rsDaten->FeldInhalt('FAB_KEY');
				
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_KEY'),$rsDaten->FeldInhalt('FAB_KEY'));
			$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_NAME1'),$rsDaten->FeldInhalt('FAB_NAME1'));
			$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_LAN_CODE'),$rsDaten->FeldInhalt('FAB_LAN_CODE'));
			$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_PLZ'),$rsDaten->FeldInhalt('FAB_PLZ'));
			$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_ORT'),$rsDaten->FeldInhalt('FAB_ORT'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Reifenstamm':
				$Tabelle = 'FAB';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT *';
				$SQL .= ' FROM fachbetriebe ';
				$SQL .= ' WHERE FAB_KEY=0'.$Key;
								
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('FAB_KEY');
					
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_KEY'),$rsDaten->FeldInhalt('FAB_KEY'));
				$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_NAME1'),$rsDaten->FeldInhalt('FAB_NAME1'));
				$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_LAN_CODE'),$rsDaten->FeldInhalt('FAB_LAN_CODE'));
				$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_PLZ'),$rsDaten->FeldInhalt('FAB_PLZ'));
				$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_ORT'),$rsDaten->FeldInhalt('FAB_ORT'));
				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'FAB':
				$SQL = 'DELETE FROM fachbetriebe WHERE FAB_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1='';
				break;		
			default:
				break;
		}
	}
	
	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./fachbetriebe_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
		$Form->Formular_Start();
		
		$Form->ZeileStart();		
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
		$Form->ZeileEnde();
	
		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',200);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}
	
		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
/*		
		if (isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
*/		
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();
	
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
	
		$Form->SchreibeHTMLCode('</form>');
	
		$Form->Formular_Ende();
	
		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>