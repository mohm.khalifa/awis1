<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');	
	$TextKonserven[]=array('FAB','%');
	$TextKonserven[]=array('SEE','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Link_Route');
	$TextKonserven[]=array('Wort','Link_Maps');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht4204= $AWISBenutzer->HatDasRecht(4204);		// Feedback Seminarteilnehmer
	//var_dump($AWIS_KEY1);
	if(($Recht4204&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(isset($_GET['SEE_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['SEE_KEY']);
	}
	
	//*********************************************************
	//* Sortierung
	//*********************************************************
	
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY SEE_BEZEICHNUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}
	
	$SQL = 'select *  ';
	$SQL.= 'FROM SEMINARPLAN';
	$SQL.= ' INNER JOIN SEMINARFEEDBACK';
	$SQL.= ' ON SEP_KEY = SEE_SEP_KEY';
	$SQL.= ' WHERE SEP_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);	
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND SEE_KEY = '.$AWIS_KEY2;
	}
	
	$SQL .= $ORDERBY;

	$rsSEE = $DB->RecordSetOeffnen($SQL);
    
	if (isset($_GET['ListeSEE']) or !isset($_GET['SEE_KEY']))
	{
		$DetailAnsicht = false;

		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['SEE_BEZEICHNUNG']=390;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['SEE_BEZEICHNUNG']=390;
		}
		else
		{
			$Breiten['SEE_BEZEICHNUNG']=390;
		}


		$Form->ZeileStart();
		
		$SQL = 'SELECT distinct SET_KEY';
		$SQL .= ' FROM SeminarTeilnehmer';
		$SQL .= ' WHERE UPPER(SET_STATUS) != UPPER(\'Abgelehnt\')';
		$SQL .= ' AND SET_SEP_KEY=0'.$AWIS_KEY1;
		
		$rsSET = $DB->RecordSetOeffnen($SQL);
		
		$SQL = 'select *  ';
		$SQL.= 'FROM SEMINARPLAN';
		$SQL.= ' WHERE SEP_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);
		$rsDAT = $DB->RecordSetOeffnen($SQL);
		
		$FBBeginn = strtotime($Form->Format('DU',$rsDAT->FeldInhalt('SEP_BIS'))) + (86400*28);
		$FBEnde = $FBBeginn + (86400*28);
		$Now = strtotime(date("d.m.Y",time()));
		if((($Recht4204&2)==2 and ($Now >= $FBBeginn and $Now <= $FBEnde) and ($rsSET->AnzahlDatensaetze() > $rsSEE->AnzahlDatensaetze())) or ($Recht4204&32)==32)
		{
			$Icons = array();
			$Icons[] = array('new','./seminarplan_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&SEE_KEY=-1','g');
			$Form->Erstelle_ListeIcons($Icons,38,0);
		}
		else 
		{
			$Form->Erstelle_Liste_Ueberschrift('',38,'','');
		}
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=SEE_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SEE_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEE']['SEE_BEZEICHNUNG'],$Breiten['SEE_BEZEICHNUNG'],'',$Link);
		$Form->ZeileEnde();
		
		if ($rsSEE->AnzahlDatensaetze() < 1)
		{
		    $Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);	
		    $Form->ZeileEnde();
		}
		else 
		{
			$DS = 0;
			while(!$rsSEE->EOF())
			{
				$Form->ZeileStart();
					
				if((($Recht4204&4)==4) or (($Recht4204&8)==8))
				{
					$Icons = array();
					if((($Recht4204&4)==4 and ($Now >= $FBBeginn and $Now <= $FBEnde)) or ($Recht4204&32)==32)
					{
						//$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FFA_KEY='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
						$Icons[] = array('edit','./seminarplan_Main.php?cmdAktion=Details&SEE_KEY='.$rsSEE->FeldInhalt('SEE_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
					}
					if((($Recht4204&8)==8 and ($Now >= $FBBeginn and $Now <= $FBEnde)) or ($Recht4204&32)==32)
					{
						$Icons[] = array('delete','./seminarplan_Main.php?cmdAktion=Details&Del='.$rsSEE->FeldInhalt('SEE_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
					}
					$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
				}
				$Form->Erstelle_ListenFeld('SEE_BEZEICHNUNG',$rsSEE->FeldInhalt('SEE_BEZEICHNUNG'),0,$Breiten['SEE_BEZEICHNUNG'],false,($DS%2),'','','T');
				$rsSEE->DSWeiter();
				$DS++;
			}	
		}
	}
	else
	{
		$AWIS_KEY2 = $rsSEE->FeldInhalt('SEE_KEY');
		
		$SQL = 'select *  ';
		$SQL.= 'FROM SEMINARFEEDBACK';
		//$SQL .= ' INNER JOIN SEMINARKATALOG on SEK_KEY = 1';
		$SQL.= ' WHERE SEE_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY2);
		
		//SQL nur solange keine variablen Kataloge auf dem Seminar hinterlegt sind
		//sonst oberer inner join.
		$SQL2 = 'select *  ';
		$SQL2.= 'FROM SEMINARKATALOG';
		$SQL2 .= ' INNER JOIN SEMINARFRAGEN on SEK_KEY = SEF_SEK_ID';
		$SQL2 .= ' LEFT JOIN SEMINARZUORD  on SEZ_SEF_ID = SEF_KEY and SEZ_SEE_KEY ='.$DB->FeldInhaltFormat('N0',$AWIS_KEY2);
		$SQL2.= ' WHERE SEK_KEY = 1';
		$SQL2 .= ' AND SEF_STATUS=\'A\'';
		$SQL2 .= ' ORDER BY SEF_SORTIERUNG';
		
		$rsDaten = $DB->RecordSetOeffnen($SQL);
		$rsKat  = $DB->RecordSetOeffnen($SQL2);

		$TextKonserven[]=array($rsKat->FeldInhalt('SEK_TEXTKONSERVE'),'%');
		$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
		
		echo '<input type=hidden name=txtSEE_KEY value='.$AWIS_KEY2. '>';
		echo '<input type=hidden name=txtSEE_SEP_KEY value='.$AWIS_KEY1. '>';

		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

	
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./seminarplan_Main.php?cmdAktion=Details&ListeSEE".(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSEE->FeldInhalt('SEE_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSEE->FeldInhalt('SEE_USERDAT'));
		$Form->InfoZeile($Felder,'');
		

		$EditModus = ($Recht4204&6);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEE']['SEE_BEZEICHNUNG'].':',200,'','');
		$Form->Erstelle_TextFeld('SEE_BEZEICHNUNG',$rsDaten->FeldInhalt('SEE_BEZEICHNUNG'),25,250,true,'','','','T','','','',25);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEE']['SEE_KATALOG'].':',200,'','');
		$Form->Erstelle_TextFeld('SEE_KATALOG',$rsKat->FeldInhalt('SEK_BEZEICHNUNG'),25,250,false,'','','','T');
		$Form->ZeileEnde();
        $Form->Trennzeile('L');

		while(!$rsKat->EOF())
		{
			//Überschrift
			if($rsKat->FeldInhalt('SEF_SEF_ID')=='0')
			{
				if($rsKat->FeldInhalt('SEF_DATENQUELLE')=='TXT')
				{
					$Form->ZeileStart();
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven[$rsKat->FeldInhalt('SEK_TEXTKONSERVE')][$rsKat->FeldInhalt('SEF_BEZEICHNUNG')].':',$rsKat->FeldInhalt('SEF_ZEICHEN'));
					$Form->ZeileEnde();
				}
				else 
				{
					$Form->ZeileStart();
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven[$rsKat->FeldInhalt('SEK_TEXTKONSERVE')][$rsKat->FeldInhalt('SEF_BEZEICHNUNG')].':',800, 'font-weight:bolder','','');
					$Form->ZeileEnde();
				}
				
			}
			else
			{
				$Form->ZeileStart();
				$Frage = $AWISSprachKonserven[$rsKat->FeldInhalt('SEK_TEXTKONSERVE')][$rsKat->FeldInhalt('SEF_BEZEICHNUNG')];
				$Form->Erstelle_TextLabel($Frage.':',400,'margin-top:5px');
				switch(substr($rsKat->FeldInhalt('SEF_DATENQUELLE'),0,3))
				{
					case 'TXT':
						$Felder = explode(':',$rsKat->FeldInhalt('SEF_DATENQUELLE'));
						//$Form->DebugAusgabe(1,$Felder[1],$Felder[2]);
						$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])), $AWISBenutzer->BenutzerSprache());
						$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
						$Form->Erstelle_SelectFeld('SEF_WERT_'.$rsKat->FeldInhalt('SEF_KEY').'_'.$rsKat->FeldInhalt('SEZ_KEY'),$rsKat->FeldInhalt('SEZ_WERT'),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
						break;
					default:
						$Form->Erstelle_Textarea('SEF_WERT_'.$rsKat->FeldInhalt('SEF_KEY').'_'.$rsKat->FeldInhalt('SEZ_KEY'),$rsKat->FeldInhalt('SEZ_WERT'),$rsKat->FeldInhalt('SEF_BREITE'),$rsKat->FeldInhalt('SEF_ZEICHEN'),3,true,'font: bold');
						break;
				}
                $Form->ZeileEnde();
				$Form->Trennzeile('O');
			}
			$rsKat->DSWeiter();
		}			

	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002241128");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002241128");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>