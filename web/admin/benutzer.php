
<html>
<head>

<title>Awis 1.0 - ATU webbasierendes Informationssystem - Absatz</title>
<meta http-equiv="expires" content="01 01 2000 00:00:00 GMT">

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) . '>';
?>
</head>
<body>
<?
global $HTTP_POST_VARS;
include ("ATU_Header.php");	// Kopfzeile


$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 1);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Benutzerverwaltung',$_SERVER['PHP_AUTH_USER'],'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

	// Defaults
$XBN_Key = $_POST['txtXBN_Key'] . $_GET['txtXBN_Key'];
$Aktion =  $_POST['cmdAktion'] . $_GET['cmdAktion'];

    //************************************
    // LoginName l�schen
    //************************************
if(awis_NameInArray($_POST,"cmdDelXBL_"))
{
    $nr = intval(substr(awis_NameInArray($_POST,"cmdDelXBL_"),10));

    $SQL = "DELETE FROM AWIS.BenutzerLogins WHERE XBL_KEY=0" .  $nr;
	$Erg = awisExecute($con, $SQL );
	awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

    $Feld = "txtXBL_KEY_" . $nr;
	

        // Typ der Anmeldung pr�fen und evtl. anpassen, falls WIN Login ben�tigt wird
            // User vom System l�schen
    $erg = system("/daten/zugang/deluser.sh " .  strtolower($_POST[$Feld]));
	echo '<span class=HinweisText>Erg: ' . $erg . '</span>';

    $Aktion="Weiter";
}

    //************************************
    // LoginName Speichern
    //************************************
if(awis_NameInArray($_POST,"cmdSAVXBL_"))
{
    $nr = intval(substr(awis_NameInArray($_POST,"cmdSAVXBL_"),10));

    if($_POST['txtKennwort1_0']!="")
    {
        $KFeld1=$_POST['txtKennwort1_0'];
        $KFeld2=$_POST['txtKennwort2_0'];
        $nr=0;
    }
    else
    {
        $KFeld1=$_POST['txtKennwort1_'] . $nr;
        $KFeld2=$_POST['txtKennwort2_'] . $nr;
    }

    if($KFeld1 != "")
    {
        if($KFeld1==$KFeld2)
        {
            $Feld="txtXBL_KEY_".$nr;
            if($nr==0)      // Neuer Login
            {
                $SQL = "INSERT INTO AWIS.BenutzerLogins(XBL_XBN_Key, XBL_LOGIN) VALUES(" . $XBN_Key . ",'" . strtoupper($_POST[$Feld]) ."')";
				$Erg = awisExecute($con, $SQL );
				awisLogoff($con);
				if($Erg==FALSE)
				{
					awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
					die();
				}
            }
            else            // Alten Login �ndern
            {
                $SQL = "UPDATE AWIS.BenutzerLogins SET XBL_LOGIN='" . strtoupper($_POST[$Feld]) ."' WHERE XBL_KEY=" . $nr;
				$Erg = awisExecute($con, $SQL );
				awisLogoff($con);
				if($Erg==FALSE)
				{
					awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
					die();
				}
            }

                            // Anwender im Linux anlegen/�ndern
            system("/daten/zugang/newuser.sh " . strtolower($_POST[$Feld]) . " " . $KFeld1,$erg);
            print "<table ><tr><td><span class=HinweisText>Meldung: (" . "/daten/zugang/newuser.sh " . $_POST[$Feld] . " " . $KFeld1 ."): " . $erg . "</span></td></tr></table><br>";

        }
        else
        {
            print "<table><tr><td><span class=HinweisText>Kennwort wurde nicht korrekt best�tigt. Bitte wiederholen Sie Ihre Angaben.</span></td></tr></table><br>";
        }
    }
    else
    {
        print "<table><tr><td><span class=HinweisText>Kennwort wurde nicht angeben. Benutzer wurde nicht im LINUX angelegt</span></td></tr></table><br>";
    }

    $Aktion="Weiter";
}

    //************************************
    // Benutzerangaben speichern
    //************************************

if($_POST['cmdSave_x'])
{
    if($_POST['txtXBN_Key']=='')
    {
        $SQL = "INSERT INTO AWIS.Benutzer(XBN_NAME, XBN_VOLLERNAME, XBN_IPADRESSE, XBN_KON_KEY, XBN_USER, XBN_USERDAT) VALUES('";
		$SQL .= $_POST['txtXBN_NAME'] . "', '" . $_POST['txtXBN_VOLLERNAME'] . "', '" . $_POST['txtXBN_IPADRESSE'] . "', 0, '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE)";
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
			die();
		}

        $SQL = "SELECT XBN_Key FROM AWIS.Benutzer WHERE XBN_Name='" . $_POST['txtXBN_NAME'] . "'";

        $rsBenutzer = awisOpenRecordset($con, $SQL);
        $XBN_Key=$rsBenutzer["XBN_KEY"][0];
		unset($rsBenutzer);
		$Aktion='Weiter';
		
		awis_BenutzerParameterSpeichern($con,"Passwortaendern",$_POST['txtXBN_NAME'],-1);
    }
    else
    {
        $SQL = "UPDATE AWIS.Benutzer SET XBN_Name='" . $_POST['txtXBN_NAME'] . "', XBN_VOLLERNAME='" . $_POST['txtXBN_VOLLERNAME'] . "', XBN_IPADRESSE='" . $_POST['txtXBN_IPADRESSE'] . "', XBN_KON_KEY=0" . $_POST['txtXBN_KON_KEY'] ;
        $SQL .= " WHERE XBN_Key=0" . $XBN_Key;
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
			die();
		}
		$Aktion = 'Weiter';
    }

	if($txtKennwortAendern==1)
	{
		$SQL = awis_BenutzerParameterSpeichern($con,"Passwortaendern",$txtXBN_Key,-1);
	}
	else
	{
		$SQL = awis_BenutzerParameterSpeichern($con,"Passwortaendern",$txtXBN_Key,0);
	}
    $cmdAktion="Weiter";
}

    //***********************************
    // Gruppenangaben hinzuf�gen
    //***********************************

//OK
if(awis_NameInArray($_POST,'cmdSAVGRP')!='')
{
	$GrpNr = $_POST['txtXBG_XBB_Key_0'];
	
    $SQL = "INSERT INTO AWIS.BENUTZERGRUPPEN(XBG_XBN_Key,XBG_XBB_Key) VALUES(" . $_POST['txtXBN_Key'] . ", " . $GrpNr .")";

	$Erg = awisExecute($con, $SQL );
	awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

    $Aktion = "Weiter";
}

    //************************************
    // Benutzer l�schen
    //************************************
if($_POST['cmdLoeschen_x']!='')
{
    $SQL = "SELECT XBL_LOGIN From AWIS.BenutzerLogins WHERE XBL_XBN_Key=0" . $XBN_Key;
    $rsLogins = awisOpenRecordset($con, $SQL);
    $rsLoginsZeilen = $awisRSZeilen;

    for($i=0;$i<$rsLoginsZeilen; $i++)
    {
        $erg = system("/daten/zugang/deluser.sh " .  strtolower($rsLogins["XBL_LOGIN"][$i]));
        print "<table bgcolor=#FFFF00><tr><td><font color=#FF0000>Meldung: (" . "/daten/zugang/newuser.sh ) " . $erg . "</font></td></tr></table><br>";
    }

    $SQL = "DELETE FROM AWIS.Benutzer WHERE XBN_Key=0" . $XBN_Key;

	$Erg = awisExecute($con, $SQL );
	if($Erg==FALSE)
	{
		awisLogoff($con);
		awisErrorMailLink("benutzer1.php", 2, $awisDBFehler['message']);
		die();
	}

   $Aktion = "";    // Zur Auswahl der Benutzer
   $XBN_Key=0;
}


    //************************************
    // Gruppenangaben l�schen
    //************************************

//OK
if(awis_NameInArray($_POST,"cmdDELGRP")!='')
{
    $nr = intval(substr(awis_NameInArray($_POST,"cmdDELGRP"),10));
    $SQL = "DELETE FROM AWIS.BENUTZERGRUPPEN WHERE XBG_XBN_Key=0" . $_POST['txtXBN_Key'] . " AND XBG_XBB_KEY=0" . $nr ."";

	$Erg = awisExecute($con, $SQL );
	awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

    $Aktion = "Weiter";
}

if($HTTP_GET_VARS['XBNKEY']!='')
{
	$cmdAktion='Weiter';
	$txtXBN_Key=$HTTP_GET_VARS['XBNKEY'];
}

/*******************************************************
* Recht hinzuf�gen
*******************************************************/

if($_POST['cmdNEURecht_x'])
//if("0" . $HTTP_GET_VARS['txtXRC_Key_0'] <> "0")
{

    $SQL = "INSERT INTO BENUTZERACLS(XBA_XRC_ID, XBA_TYP, XBA_XXX_KEY, XBA_Stufe) VALUES(" . $_POST['txtXRC_Key_0'] . ", 'B', " . $XBN_Key . ", " . $_POST['txtStufe'] . " )";

	$Erg = awisExecute($con, $SQL );
	awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

    $Aktion = "Weiter";
}

/*******************************************************
* Recht l�schen
*******************************************************/

if(awis_NameInArray($_POST,"cmdDELXRC"))
{
    $nr = intval(substr(awis_NameInArray($_POST,"cmdDELXRC"),10));

    $SQL = "DELETE FROM BENUTZERACLS WHERE XBA_XXX_KEY=0" . $XBN_Key . " AND XBA_XRC_ID=0" . $nr ."";

	$Erg = awisExecute($con, $SQL );
	awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

    $Aktion = "Weiter";

}

    //************************************
    // Erster Aufruf
    //************************************
	
if($Aktion=='')
{
    print "<form name=frmBenutzer method=post action=./benutzer.php>";

    print "<table boder=1 cellspacing=5 bgcolor=#C0C0C0 style=border-color=#000000><tr><td><table border=0 bgcolor=#C0C0C0><tr><td>Benutzer:</td>";
    print "<td><select name=txtXBN_Key size=1>";

	if(($RechteStufe&16)==16)		// Neuen Benutzer anlegen
	{
    	print "<option value=0 selected>Neuer Benutzer...</option>";
	}

    $rsBenutzer = awisOpenRecordset($con, "SELECT XBN_Key, XBN_Name FROM AWIS.Benutzer ORDER BY XBN_NAME");
    $rsBenutzerZeilen = $awisRSZeilen;

    for($i = 0; $i<$rsBenutzerZeilen; $i++)
    {
        print "<option value=" . $rsBenutzer["XBN_KEY"][$i] . ">" . $rsBenutzer["XBN_NAME"][$i] . "</option>";
    }

    print "</select></td></tr>";

    print "<tr><td colspan=2><hr></td></tr>";
    print "<tr><td colspan=2><input type=submit name=cmdAktion Value=Weiter src=/bilder/weiter.png></form></td></tr>";
    print "</table></td></tr></table>";

    print "<br><img src=/bilder/zurueck.png name=cmdZurueck value=Zur�ck onclick=self.location.href='./benutzerverwaltung.php'>";

    print "</form>";

}
    //***************************************
    // Benutzer �ndern oder neu anlegen
    //***************************************
elseif($Aktion=='Weiter')
{

    if($XBN_Key==0)           // Neuer Benutzer
    {
		print "<input type=hidden name=cmdAktion value=Weiter>";
        print "<h2>Benutzer anlegen</h2>";
        print "<form name=frmBenutzer method=post action=./benutzer.php>";
        print "<table border=0 id=Maske>";

        print "<tr><td>Benutzer-ID:</td><td><input style=background-color:#DCDCDC; size=8 type=text readonly name=txtXBN_Key value=" . $rsBenutzer[XBN_Key][0] . "></td><td></td></tr>";

        print "<tr><td>Benutzername:</td><td><input type=text name=txtXBN_NAME value=" . chr(34) . $rsBenutzer[XBN_NAME][0] . chr(34) ."></td><td></td></tr>";
        print "<tr><td>Voller Name:</td><td><input type=text name=txtXBN_VOLLERNAME value=" . chr(34) . $rsBenutzer[XBN_VOLLERNAME][0] . chr(34) . "></td><td></td></tr>";
        print "<tr><td>IP Adresse:</td><td><input type=text name=txtXBN_IPADRESSE value=" . chr(34) . $rsBenutzer[XBN_IPADRESSE][0] . chr(34) ."></td><td></td></tr>";
        //print "<tr><td colspan=4><hr></td></tr>";

            // Schaltfl�chen f�r die Hauptmaske

        print "</td><td colspan=4><input accesskey=s type=image title='Benutzer speichern (Alt+S)' src=/bilder/diskette.png name=cmdSave>";
        print "</table></form>";
    }
    else                        // Benutzer �ndern
    {
        $rsBenutzer = awisOpenRecordset($con, "SELECT * FROM AWIS.Benutzer, AWIS.BenutzerLogins WHERE XBN_Key=XBL_XBN_Key(+) AND XBN_Key=" . $XBN_Key . " ORDER BY XBN_NAME");
        $rsBenutzerZeilen = $awisRSZeilen;
        if($rsBenutzerZeilen>0)
        {
            print "<h2>Benutzer �ndern</h2>";
            print "<form name=frmBenutzer method=post action=./benutzer.php>";
            print "<table border=0 id=Maske>";

            print "<tr><td>Benutzer-ID:</td><td><input style=background-color:#DCDCDC; size=8 type=text readonly name=txtXBN_Key value=" . $rsBenutzer[XBN_KEY][0] . "></td><td></td></tr>";


			if(($RechteStufe&4)==4)		// Neuen Benutzer anlegen
			{
				$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KAB_ABTEILUNG AS KONTAKT FROM AWIS.KONTAKTE INNER JOIN  KONTAKTEABTEILUNGEN ON KON_KAB_KEY = KAB_KEY WHERE KON_KKA_KEY=1 ORDER BY KON_NAME1, KON_NAME2");
				//$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KON_ZUSTAENDIGKEIT AS KONTAKT FROM AWIS.KONTAKTE WHERE KON_KKA_KEY=1 ORDER BY KON_NAME1, KON_NAME2");
				$rsKontaktZeilen = $awisRSZeilen;
	            print "<tr><td>Benutzername:</td><td><input type=text name=txtXBN_NAME value=" . chr(34) . $rsBenutzer['XBN_NAME'][0] . chr(34) ."></td><td></td></tr>";
	            print "<tr><td>Voller Name:</td><td><input type=text name=txtXBN_VOLLERNAME value=" . chr(34) . $rsBenutzer['XBN_VOLLERNAME'][0] . chr(34) . "></td><td></td></tr>";
	            print "<tr><td>IP Adresse:</td><td><input type=text name=txtXBN_IPADRESSE value=" . chr(34) . $rsBenutzer['XBN_IPADRESSE'][0] . chr(34) ."></td><td></td></tr>";
	            print "<tr><td>Kennwort�nderung:</td><td><input type=checkbox name=txtKennwortAendern value=1 " . (awis_BenutzerParameter($con,"Passwortaendern",$rsBenutzer['XBN_KEY'][0])==-1?" checked ":"") . "></td><td></td></tr>";

			    print "<tr><td>Kontakt:</td><td><select size=1 name=txtXBN_KON_KEY><option value=0>Kontakt ausw�hlen...</option>";
	            for($i=0; $i<$rsKontaktZeilen;$i++)
	            {
	                print "<option ";
					if($rsKontakt["KON_KEY"][$i] == $rsBenutzer["XBN_KON_KEY"][0])
					{
						print " selected ";
					}
					print "value=" . $rsKontakt["KON_KEY"][$i] . ">". $rsKontakt["KONTAKT"][$i] . "</option>";
	            }
	            print "</select></td></tr>";
			}
			else
			{
				$rsKontakt = awisOpenRecordset($con,"SELECT KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KON_ZUSTAENDIGKEIT AS KONTAKT FROM AWIS.KONTAKTE WHERE KON_KKA_KEY=1 AND KON_KEY=" . $rsBenutzer['XBN_KON_KEY'][0] . "");
		        print "<tr><td>Benutzername:</td><td>" . $rsBenutzer['XBN_NAME'][0] . "</td><td></td></tr>";
		        print "<tr><td>Voller Name:</td><td>" . $rsBenutzer['XBN_VOLLERNAME'][0] . "</td><td></td></tr>";
		        print "<tr><td>IP Adresse:</td><td>" . $rsBenutzer['XBN_IPADRESSE'][0] . "</td><td></td></tr>";
			    print "<tr><td>Kontakt:</td><td>" . $rsKontakt['KONTAKT'][0] . "</td></tr>";
			}

			unset($rsKontakt);
			
            //print "<tr><td colspan=4><hr></td></tr>";

                // Schaltfl�chen f�r die Hauptmaske

			if(($RechteStufe&4)==4 OR ($RechteStufe&16)==16)		// Neuen Benutzer anlegen
			{
	            print "</td><td colspan=4><input type=image alt=Benutzer_speichern src=/bilder/diskette.png name=cmdSave>";
			}    
			if(($RechteStufe&8)==8)		// Neuen Benutzer anlegen
			{
		        print "<input type=image alt=Benutzer_l�schen src=/bilder/muelleimer.png name=cmdLoeschen >";
    		}
	        print "</td></tr></table>";


                //*****************************
                // Anmeldenamen
                //*****************************
            print "<table border=0><tr><td colspan=9><hr></td></tr>";


			if(($RechteStufe&4)==4)		// Benutzer �ndern
			{
	            print "<tr><td colspan=2 id=FeldBez></b>Anmeldenamen</td><td colspan=2 id=FeldBez>Kennwort</td><td id=FeldBez colspan=3>Best�tigung</td></tr>";
	            print "<td colspan=2><input size=40 name=txtXBL_KEY_0></td>";
	            print "<td>Kennwort:</td><td><input type=password name=txtKennwort1_0></td><td></td>";
	            print "<td>Best�tigung:</td><td><input type=password name=txtKennwort2_0></td><td></td>";
	            print "<td><input type=image alt=Login_hinzuf�gen src=/bilder/diskette.png name=cmdSAVXBL_0 onclick=document.frmBenutzer.submit();></td>";
	            print "</tr>";
			}

			if(($RechteStufe&2)==2)		// Neuen Benutzer anlegen
			{
				if(($RechteStufe&4)!=4)		// �berschrift nicht doppelt
				{
		            print "<tr><td colspan=2 id=FeldBez></b>Anmeldenamen</td><td colspan=2 id=FeldBez>Kennwort</td><td id=FeldBez colspan=3>Best�tigung</td></tr>";
				}
	            for($i=0;$i<$rsBenutzerZeilen && $rsBenutzer["XBL_LOGIN"][$i]!="";$i++)
	            {
	                print "<tr><td colspan=2><input size=40 name=txtXBL_KEY_" . $rsBenutzer["XBL_KEY"][$i] . " value=" .chr(34) . $rsBenutzer["XBL_LOGIN"][$i] . chr(34) . ">";
	
	                print "<td>Neues Kennwort:</td><td><input type=password name=txtKennwort1_" . $rsBenutzer["XBL_KEY"][$i] . "></td><td></td>";
	                print "<td>Best�tigung:</td><td><input type=password name=txtKennwort2_" . $rsBenutzer["XBL_KEY"][$i] . "></td><td></td>";
	
	                print "</td><td><input type=image alt=Login_l�schen src=/bilder/muelleimer.png name=cmdDelXBL_" . $rsBenutzer["XBL_KEY"][$i] . " onclick=document.frmBenutzer.submit();>";
	                print "<input type=image alt=Login_speichern src=/bilder/diskette.png name=cmdSAVXBL_" . $rsBenutzer["XBL_KEY"][$i] . " onclick=document.frmBenutzer.submit();>";
	
	                print "</td></tr>";
	            }
			}
			else
			{
	            print "<tr><td colspan=2 id=FeldBez></b>Anmeldenamen</td></tr>";
	            for($i=0;$i<$rsBenutzerZeilen && $rsBenutzer["XBL_LOGIN"][$i]!="";$i++)
	            {
	                print "<tr><td colspan=2>" . $rsBenutzer["XBL_LOGIN"][$i] . "";
	                print "</td></tr>";
	            }
			}
            
			print "</table>";

                //*****************************
                // Benutzergruppen
                //*****************************

            $rsBenutzerGruppen = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez WHERE XBB_KEY Not In (SELECT XBG_XBB_KEY FROM AWIS.BenutzerGruppen WHERE XBG_XBN_Key=0" . $txtXBN_Key . ") ORDER BY XBB_BEZ");
            $rsBenutzerGruppenZeilen = $awisRSZeilen;

            print "<table border=0><tr><td colspan=5><hr></td></tr>";
            print "<tr><td colspan=2 id=FeldBez></b>Gruppenmitgliedschaften</td></tr>";

			if(($RechteStufe&32)==32)		// In Gruppe hinzuf�gen
			{
	            print "<tr><td><select size=1 name=txtXBG_XBB_Key_0>";
	            print "<option selected value=0>Gruppe hinzuf�gen...</option>";
	            for($BGZeilen=0; $BGZeilen<$rsBenutzerGruppenZeilen;$BGZeilen++)
	            {
	                print "<option value=" . $rsBenutzerGruppen['XBB_KEY'][$BGZeilen] . ">". $rsBenutzerGruppen['XBB_BEZ'][$BGZeilen] . "</option>";
	            }
	
	            print "</select></td><td><input type=image alt=Gruppe_hinzuf�gen src=/bilder/diskette.png name=cmdSAVGRP_" . $rsBenutzer['XBN_KEY'][0] . " ></td></tr>";
			}
            $rsGMitglieder = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez WHERE XBB_KEY In (SELECT XBG_XBB_KEY FROM AWIS.BenutzerGruppen WHERE XBG_XBN_Key=0" . $txtXBN_Key . ")");
            $rsGMitgliederZeilen = $awisRSZeilen;

            for($i=0;$i<$rsGMitgliederZeilen;$i++)
            {
       	        print "<tr><td style=\"border-bottom-width:2px;border-bottom-style:solid;\">";
				echo '<a href=./gruppen.php?txtXBB_Key=' . $rsGMitglieder['XBB_KEY'][$i] . '&cmdAktion=Weiter>' . $rsGMitglieder['XBB_BEZ'][$i] .  '</a></td>';

				if(($RechteStufe&64)==64)		// Aus Gruppe l�schen
				{
	                print "</td><td><input type=image alt=Gruppe_l�schen src=/bilder/muelleimer.png name=cmdDELGRP_" . $rsGMitglieder['XBB_KEY'][$i] . " onclick=document.frmBenutzer.submit();><td></tr>";
				}
            }
            print "</table>";


			/*****************************************************
			* Benutzerrechte
			*****************************************************/

            print "<form name=frmRechte method=post action=./benutzer.php>";

            print "<table border=0><tr><td colspan=5><hr></td></tr>";
            print "<tr><td colspan=2 height=35 id=FeldBez></b>Benutzerrechte</td><td><img alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rechte','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');></td></tr>";

			if(($RechteStufe&1024) == 1024)
			{
	            $rsRechte = awisOpenRecordset($con, "select * from AWIS.Rechte where XRC_ID not in (select XBA_XRC_ID from AWIS.BenutzerACLS WHERE XBA_XXX_Key=0" . $txtXBN_Key . ") ORDER BY XRC_RECHT");
	            $rsRechteZeilen = $awisRSZeilen;
	
	            print "<tr><td width=300><select size=1 name=txtXRC_Key_0>";
	            print "<option selected value=0>Recht hinzuf�gen...</option>";
	            for($i=0; $i<$rsRechteZeilen;$i++)
	            {
	                print "<option value=" . $rsRechte[XRC_ID][$i] . ">". $rsRechte[XRC_RECHT][$i] . "</option>";
	            }
	
	            print "</select>";
	
	            print "</td><td><input name=txtStufe value=1 size=4></td><td><input type=image alt=Recht_hinzuf�gen src=/bilder/diskette.png name=cmdNEURecht onclick=document.frmRechte.submit();></td></tr>";
			}

            $rsRechte = awisOpenRecordset($con, "select DISTINCT * from AWIS.Rechte, AWIS.BenutzerACLS where XBA_XRC_ID = XRC_ID AND XBA_XXX_Key=0" . $txtXBN_Key . " ORDER BY XRC_RECHT");
            $rsRechteZeilen = $awisRSZeilen;

            for($i=0; $i<$rsRechteZeilen;$i++)
            {
                print "<tr><td style=\"border-bottom-width:2px;border-bottom-style:solid;\"><input type=hidden name=txtRCID_" . $rsRechte[XRC_ID][$i] . ">" . $rsRechte["XRC_RECHT"][$i] . " (" . $rsRechte[XRC_ID][$i] . ")" . "</td>";
				print "<td style=\"border-left-width:2px;border-left-style:solid;border-bottom-width:2px;border-bottom-style:solid;\">" . $rsRechte[XBA_STUFE][$i] .  "</td>";
				if(($RechteStufe&1024) == 1024)
				{
					print "<td><input type=image alt=Recht_entziehen src=/bilder/muelleimer.png name=cmdDELXRC_" . $rsRechte[XRC_ID][$i] . " onclick=document.frmRechte.submit();><td></tr>";
	            }
			}
            print "</table></form>";
        }
    }
    print "<hr><input type=image accesskey=z Title=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./benutzer.php'>";
}

awislogoff($con);
?>
</body>
</html>
