<?php
require_once('/daten/include/awisBenutzer.inc');
require_once('/daten/include/awisCRM.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $Testsystem = false;
    $Pfad = '';
    $Modus = 'P';
    $EMail = '';
    $Auto = 0;          // Automatische Verarbeitung
    $Duplikate = 0;     // Sucht Duplikate

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Level = '';

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--auto':
                            $Auto=1;
                            break;
                    case '--duplikate':
                            $Duplikate = 1;
                            break;
                    case '--pfad':
                            $Pfad = $argv[++$i];
                            break;
                    case '--modus':
                            $Modus = $argv[++$i];
                            break;
                    case '--email':
                            $EMail = $argv[++$i];
                            break;
                                case '--datei':
                            $Datei = $argv[++$i];
                                        if(!is_file($Datei))
                            {
                                echo $Datei.' kann nicht gefunden werden.'.PHP_EOL;
                                die();
                            }
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                                    case '--level':
                                            $Level = strtoupper($argv[++$i]);
                                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                                case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --datei <Pfad>                     Zu pruefende/importierende Datei.'.PHP_EOL;
                            echo '  --auto                             Alle offenen Dateien verarbeiten.'.PHP_EOL;
                            echo '  --modus <Modus>                    Modus (I=Import, P=Pruefung.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                                                    echo '  --level <Name>                     Ueberschreibt den AWIS Produktionslevel (ENTW,STAG,PROD)'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }


    if($JobId!=0)
    {
        $Job = new awisJobProtokoll($JobId, 'Starte WM - Import.');
    }

    $CRMObj = new awisCRM($Benutzer);
    $CRMObj->DebugLevel($DebugLevel);

    if($Auto==1)
    {
        $AWISBenutzer = awisBenutzer::init($Benutzer);
        $Pfad = $AWISBenutzer->ParameterLesen('CRMAdressenImportPfad');
        $Werkzeug = new awisWerkzeuge();
        if($Level=='')
        {
                $Level = $Werkzeug->awisLevel();
        }
        $Pfad.='/'.$Level;

        $Dateien = new DirectoryIterator($Pfad);
        foreach($Dateien as $Datei)
        {
                if($DebugLevel>40)
                {
                        echo 'Lese Datei '.$Datei->getFilename().PHP_EOL;
                }
                $DateiName = explode('~',$Datei->getFilename());
                if(isset($DateiName[1]))
                {
                                $SQL = 'SELECT KKO_WERT ';
                                $SQL .= ' FROM KONTAKTEKOMMUNIKATION ';
                                $SQL .= ' INNER JOIN Benutzer ON XBN_KON_KEY = KKO_KON_KEY';
                                $SQL .= ' INNER JOIN BenutzerLogins ON XBL_XBN_KEY = XBN_KEY';
                                $SQL .= ' WHERE KKO_KOT_KEY = 7 ';
                                $SQL .= ' AND XBL_LOGIN = :var_T_xbl_login';
                            $BindeVariablen=array();
                                $BindeVariablen['var_T_xbl_login']=$DateiName[1];

                                $rsEMail = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
                                if($rsEMail->EOF())
                                {
                                        $EMail = 'shuttle@de.atu.eu';
                                }
                                else
                                {
                                        $EMail = $rsEMail->FeldInhalt('KKO_WERT');
                                }

                            switch($DateiName[2])
                            {
                               case 'P':
                                        if(substr($DateiName[0],0,1)!='_')
                                        {
                                                $CRMObj->PruefeDatei_HOP($Pfad.'/'.$Datei->getFileName(),$EMail);
                                                unlink($Pfad.'/'.$Datei->getFileName());
                                        }
                                        break;
                               case 'I':
                                        $CRMObj->ImportiereDatei_HOP($Pfad.'/'.$Datei->getFileName(),(isset($DateiName[3])?$DateiName[3]:0),$EMail);
                                        unlink($Pfad.'/'.$Datei->getFileName());
                                        break;
                            }
                }
        }
    }
    elseif($Duplikate==1)
    {
        $CRMObj->SucheDuplikate();
    }
        else
        {
            switch($Modus)
            {
               case 'P':
                   $CRMObj->PruefeDatei_HOP($Datei, $EMail);
                   break;
            }
    }

    if($JobId!=0)
    {
        $Job->SchreibeLog(1,0,'Ende des Jobs.');
    }
}
catch (Exception $ex)
{
    echo $ex->getMessage().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
?>
                                                      
