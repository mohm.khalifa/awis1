<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $Pflicht_Felder;
global $AWISBenutzer;


print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php

$DBServer = awisDBServer();
$conSchad = awisAdminLogon($DBServer, "schaddev09", "schad");

$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
$Rechtestufe = awisBenutzerRecht($con, 3300);
		//  1=Suchen
		//	2=�ndern
		//	4=Hinzuf�gen
		//	8=L�schen
		// 16=PDF erzeugen
		// 32=Beschwerdebearbeitung Filiale
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schadens-DB',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$PDF_Link='';
$txt_Bearb_ID=(isset($_REQUEST['txt_Bearb_ID_Save'])?$_REQUEST['txt_Bearb_ID_Save']:'');
$ART=(isset($_REQUEST['ART'])?$_REQUEST['ART']:'');

//awis_Debug(1,$_REQUEST);

$Save_Felder=true;
//$Pflicht_Felder=''; 

if (isset($_POST['txtSQL_Anweisung']) AND $_POST['txtSQL_Anweisung']!='INSERT' AND isset($_POST['txtPruefung']) AND $_POST['txtPruefung']!='N' and ($Rechtestufe&32)==32) 
{
	echo "<br>";	
	if (isset($_POST['cmdSpeichern_x']) and Pruefe_Felder()==false)
	{		
		$Save_Felder=false;	
	}	
}

//awis_Debug(1,$Pflicht_Felder);

if(isset($_REQUEST['cmdTrotzdemSpeichern'])) 
{
	$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Speichern", $AWISBenutzer->BenutzerName()));
	
	$SQL='';
		
	if ($ART=='S')
	{					           			
		//$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE FILNR=".$Params[0]." AND SCHADENSJAHR='".date("Y")."'";
		$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW WHERE FILNR=".$Params[0]." AND SCHADENSJAHR='".date("Y")."'";
			
		$rsResult = awisOpenRecordset($conSchad,$SQL_LFDNR);
		$rsResultZeilen = $awisRSZeilen;
		
		if($rsResultZeilen==0)		// Keine Daten
		{
			die("<center><span class=HinweisText>Datensatz konnte nicht gespeichert werden!</span></center>");
		}
		else
		{
				$LFDNR=str_pad($rsResult['LFNR'][0], 4, "0", STR_PAD_LEFT);
		}
			
		$BEARBNR_NEU=date("y").str_pad($Params[0], 3, "0", STR_PAD_LEFT).$LFDNR;
		$SCHADJAHR=date("Y");
		$STAND=22; //Bearbeitungsstand auf Offen immer vorbelegen
			
		awis_Debug(1,$LFDNR,$BEARBNR_NEU,$SCHADJAHR);
			
		//$SQL='INSERT INTO SCHAEDEN_NEW@SCHAD.ATU.DE (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,EINGABEAM,EINGABEDURCH)';
		//$SQL='INSERT INTO SCHAEDEN_NEW (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,GROSSKDNR,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,ANTRAGART,EINGABEAM,EINGABEDURCH)';
		//$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $Params[0] . ',' . $Params[0] . ','.$Params[16].',\''.$Params[1].'\',\''.$Params[2].'\',\''.$Params[3].'\',\''.$Params[4].'\',\''.$Params[5].'\',\''.$Params[6].'\',\''.$Params[7].'\',\''.$Params[8].'\',\''.$Params[9].'\',\''.$Params[10].'\',\''.$Params[17].'\',\''.$Params[11].'\',\''.$Params[12].'\','.$Params[13].','.($Params[14]!=''?$Params[14]:'NULL').','.$SCHADJAHR.','.$Params[15].',\''."FEHLT".'\',' . $STAND . ',3,SYSDATE,\''.$AWISBenutzer->BenutzerName().'\')';
		//groskdnr
		$SQL='INSERT INTO SCHAEDEN_NEW (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,ANTRAGART,EINGABEAM,EINGABEDURCH)';
		$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $Params[0] . ',' . $Params[0] . ','.$Params[16].',\''.$Params[1].'\',\''.$Params[2].'\',\''.$Params[3].'\',\''.$Params[4].'\',\''.$Params[5].'\',\''.$Params[6].'\',\''.$Params[7].'\',\''.$Params[8].'\',\''.$Params[9].'\',\''.$Params[10].'\',\''.$Params[11].'\',\''.$Params[12].'\','.$Params[13].','.($Params[14]!=''?$Params[14]:'NULL').','.$SCHADJAHR.','.$Params[15].',\''."FEHLT".'\',' . $STAND . ',3,SYSDATE,\''.$AWISBenutzer->BenutzerName().'\')';
	}
			
	awis_Debug(1,$SQL);
	
	if(awisExecute($conSchad, $SQL)===FALSE)
	{
		awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
		echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
		awisLogoff($conSchad);
		die();
	}
	
	awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $AWISBenutzer->BenutzerName(),"");

	if ($Params[15]==303){
		//$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL='INSERT INTO WIEDERVORLAGEN_NEW (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL.=' VALUES(TRUNC(SYSDATE),\''.$BEARBNR_NEU.'\',\''."BackOffice".'\')';	
	}
	else {
		//$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL='INSERT INTO WIEDERVORLAGEN_NEW (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL.=' VALUES(TRUNC(SYSDATE)+1,\''.$BEARBNR_NEU.'\',\''."Filialen".'\')';	
	}
	
	if(awisExecute($conSchad, $SQL)===FALSE)
	{
		awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
		echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Wiedervorlage konnte nicht erstellt werden. Zur�ck zur Eingabe</a><p>";
		awisLogoff($conSchad);
		die();
	}
	//$SQL="SELECT BEARBNRNEU FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$BEARBNR_NEU."'";
	$SQL="SELECT BEARBNRNEU FROM SCHAEDEN_NEW WHERE BEARBEITUNGSNR='".$BEARBNR_NEU."'";
	$rsResult = awisOpenRecordset($conSchad,$SQL_LFDNR);
	$rsResultZeilen = $awisRSZeilen;
	
	if($rsResultZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Fehler!</span></center>");
	}
	else
	{
		$BEARBNRNEU=$rsResult['BEARBNRNEU'][0];
	}
	
	$PDF_Link='./schaden_pdf.php?BEARB_NR='.$BEARBNR_NEU;
	$Hinzufuegen=TRUE;
}

if(isset($_POST['cmdSpeichern_x']) && $Save_Felder==true)
{	
	if ($_POST['txtSQL_Anweisung']=='INSERT')
	{
		$Params[0]=$_POST['txtFIL_ID'];
		$Params[1]=$_POST['txtKundenname'];
		$Params[2]=$_POST['txtKundenvorname'];
		$Params[3]=$_POST['txtStrasse'];
		$Params[4]=$_POST['txtPLZ'];
		$Params[5]=$_POST['txtOrt'];
		$Params[6]=$_POST['txtTelefon'];
		$Params[7]=$_POST['txtTelefon2'];
		$Params[8]=$_POST['txtFax'];
		$Params[9]=$_POST['txtEMail'];
		$Params[10]=$_POST['txtPAN'];
		$Params[11]=$_POST['txtKFZ_Kennz'];
		$Params[12]=$_POST['txtRekl_Text'];
		$Params[13]=$_POST['txtEingang'];
		$Params[14]=$_POST['txtWA_NR'];
		$Params[15]=$_POST['txtSachbearbeiter'];
		$Params[16]=$_POST['txtAnrede'];
		//groskdnr
		//$Params[17]=$_POST['txtGrosskdnr'];
		
		$Params[14]=str_replace(" ","",$Params[14]);
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $AWISBenutzer->BenutzerName(), implode(';',$Params));
		
		awis_Debug(1,$Params);
		
		$SQL='';
		$Hinweis='';
		
		if ($ART=='S')
		{

			if (strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");
			}
			
			//groskdnr
			/*
			if ($Params[17] != '')
			{
				$SQL = ' SELECT * FROM CRMAKQUISESTAND';
				$SQL .= ' WHERE CAQ_VERTRAGSNUMMER ' . awisLIKEoderIST($Params[17]);
				
				$rsGrosskunde = awisOpenRecordset($con,$SQL);
				$rsGrosskundeZeilen = $awisRSZeilen;
				
				if ($rsGrosskundeZeilen == 0)
				{
					$Hinweis.= "Zu dieser Gro�kundennummer wurde keine Eintrag gefunden. Troztdem anlegen?<br>";										
				}		
			}
			*/
					
			$SQL="select 'S_'||BEARBEITUNGSNR BEARBEITUNGSNR, KUNDENNAME, FILNR";
            //$SQL.=" from SCHAEDEN_NEW@SCHAD.ATU.DE where filnr=".$_POST['txtFIL_ID']." and";            
            $SQL.=" from SCHAEDEN_NEW where filnr=".$_POST['txtFIL_ID']." and";            
            $SQL.=" schadensjahr = TO_CHAR(sysdate, 'YYYY') and";
            $SQL.=" upper(trim(' ' from kundenname)) like '".strtoupper(trim($Params[1]))."' union";
            $SQL.=" select 'B_'||BEARBEITUNGSNR BEARBEITUNGSNR, KUNDENNAME, FILNR";
            //$SQL.=" from BESCHWERDE@SCHAD.ATU.DE where filnr=".$_POST['txtFIL_ID']." and";
            $SQL.=" from BESCHWERDE where filnr=".$_POST['txtFIL_ID']." and";
            $SQL.=" beschwerdejahr = TO_CHAR(sysdate, 'YYYY') and";
            $SQL.=" upper(trim(' ' from kundenname)) like '".strtoupper(trim($Params[1]))."'";
            
            awis_Debug(1,$SQL);

			$rsErgebnis = awisOpenRecordset($conSchad,$SQL);
			$rsErgebnisZeilen = $awisRSZeilen;
			
			if ($rsErgebnisZeilen > 0)
			{
				$Hinweis.=  "F�r die Filiale Nr. ".$_POST['txtFIL_ID'] ."<br>";
				
				if ($rsErgebnisZeilen==1)
				{
					$Hinweis.= "existiert bereits ein Eintrag unter folgender Bearbeitungsnummer: ".$rsErgebnis['BEARBEITUNGSNR'][0];		
				}
				
				else 
				{
					$Hinweis.= "existieren bereits Eintr�ge unter folgender Bearbeitungsnummer: <br>";
					for($i=0;$i<$rsErgebnisZeilen;$i++)
					{
						$Hinweis.= $rsErgebnis['BEARBEITUNGSNR'][$i] ."<br>";
					}	
				}
			}
			
			if ($Hinweis != '')
			{
				echo $Hinweis;						
				echo "<br><img title=zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./schaden_Main.php?cmdAktion=Suche'>";
				echo "&nbsp;<input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdTrotzdemSpeichern onclick=location.href='./schaden_Main.php?cmdAktion=Liste&cmdTrotzdemSpeichern=J&ART=S'>";
				//echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
				die();
			}
					
			//$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE FILNR=".$_POST['txtFIL_ID']." AND SCHADENSJAHR='".date("Y")."'";
			$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW WHERE FILNR=".$_POST['txtFIL_ID']." AND SCHADENSJAHR='".date("Y")."'";
			
			$rsResult = awisOpenRecordset($conSchad,$SQL_LFDNR);
			$rsResultZeilen = $awisRSZeilen;
		
			if($rsResultZeilen==0)		// Keine Daten
			{
				die("<center><span class=HinweisText>Datensatz konnte nicht gespeichert werden!</span></center>");
			}
			else
			{
					$LFDNR=str_pad($rsResult['LFNR'][0], 4, "0", STR_PAD_LEFT);
			}
			
			$BEARBNR_NEU=date("y").str_pad($_POST['txtFIL_ID'], 3, "0", STR_PAD_LEFT).$LFDNR;
			$SCHADJAHR=date("Y");
			$STAND=22; //Bearbeitungsstand auf OFFEN immer vorbelegen
			
			awis_Debug(1,$LFDNR,$BEARBNR_NEU,$SCHADJAHR);
				
			//$SQL='INSERT INTO SCHAEDEN_NEW@SCHAD.ATU.DE (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,EINGABEAM,EINGABEDURCH)';
			//$SQL='INSERT INTO SCHAEDEN_NEW (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,GROSSKDNR,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,ANTRAGART,EINGABEAM,EINGABEDURCH)';
			//$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $_POST['txtFIL_ID'] . ',' . $_POST['txtFIL_ID'] . ','.$_POST['txtAnrede'].',\''.$_POST['txtKundenname'].'\',\''.$_POST['txtKundenvorname'].'\',\''.$_POST['txtStrasse'].'\',\''.$_POST['txtPLZ'].'\',\''.$_POST['txtOrt'].'\',\''.$_POST['txtTelefon'].'\',\''.$_POST['txtTelefon2'].'\',\''.$_POST['txtFax'].'\',\''.$_POST['txtEMail'].'\',\''.$_POST['txtPAN'].'\',\''.$_POST['txtGrosskdnr'].'\',\''.$_POST['txtKFZ_Kennz'].'\',\''.$_POST['txtRekl_Text'].'\','.$_POST['txtEingang'].','.($_POST['txtWA_NR']!=''?$_POST['txtWA_NR']:'NULL').','.$SCHADJAHR.','.$_POST['txtSachbearbeiter'].',\''."FEHLT".'\',' . $STAND . ',3,SYSDATE,\''.$AWISBenutzer->BenutzerName().'\')';
			
			//groskdnr
			$SQL='INSERT INTO SCHAEDEN_NEW (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,ANTRAGART,EINGABEAM,EINGABEDURCH)';
			$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $_POST['txtFIL_ID'] . ',' . $_POST['txtFIL_ID'] . ','.$_POST['txtAnrede'].',\''.$_POST['txtKundenname'].'\',\''.$_POST['txtKundenvorname'].'\',\''.$_POST['txtStrasse'].'\',\''.$_POST['txtPLZ'].'\',\''.$_POST['txtOrt'].'\',\''.$_POST['txtTelefon'].'\',\''.$_POST['txtTelefon2'].'\',\''.$_POST['txtFax'].'\',\''.$_POST['txtEMail'].'\',\''.$_POST['txtPAN'].'\',\''.$_POST['txtKFZ_Kennz'].'\',\''.$_POST['txtRekl_Text'].'\','.$_POST['txtEingang'].','.($_POST['txtWA_NR']!=''?$_POST['txtWA_NR']:'NULL').','.$SCHADJAHR.','.$_POST['txtSachbearbeiter'].',\''."FEHLT".'\',' . $STAND . ',3,SYSDATE,\''.$AWISBenutzer->BenutzerName().'\')';
		}
		
		/*if ($ART=='B')
		{
			
		}*/

		awis_Debug(1,$SQL);
		
		if(awisExecute($conSchad, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
			awisLogoff($conSchad);
			die();
		}
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $AWISBenutzer->BenutzerName(),"");

		if ($_POST['txtSachbearbeiter']==303){
			//$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE),\''.$BEARBNR_NEU.'\',\''."BackOffice".'\')';	
		}
		else {
			//$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE)+1,\''.$BEARBNR_NEU.'\',\''."Filialen".'\')';	
		}
		
		if(awisExecute($conSchad, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Wiedervorlage konnte nicht erstellt werden. Zur�ck zur Eingabe</a><p>";
			awisLogoff($conSchad);
			die();
		}
		
		$PDF_Link='./schaden_pdf.php?BEARB_NR='.$BEARBNR_NEU;
		$Hinzufuegen=TRUE;

	}
	elseif ($_POST['txtSQL_Anweisung']=='UPDATE')
	{
		
		//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
		//--->
	
		$SQL='';
		
		if ($ART=='S')
		{
			if (isset($_POST['txtRekl_Text']) AND strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&BEARB_ID=".$txt_Bearb_ID."&ART=S'>Zur�ck zur Eingabe</a><p>";	
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");				
			}
			
			$SQL = "SELECT BEARBNRNEU, ART, to_clob(FEHLERBESCHREIBUNG) as FEHLERBESCHREIBUNG, FILNR, VERURS_FILIALE, BEARBEITUNGSNR, BEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ";
			//groskdnr
			//$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, GROSSKDNR, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM, ";
			$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM, ";
			$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL, EINGABEDURCH_FILPERSNR ";
			//$SQL .= "FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
			$SQL .= "FROM SCHAEDEN_NEW WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
			
			//$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}else{
			die("<span class=HinweisText>Datensatz wurde nicht gefunden!</span>");
			//$SQL="SELECT * FROM BESCHWERDE@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}
		
		$rsSchad_Save = awisOpenRecordset($conSchad,$SQL);
		$rsSchad_SaveZeilen = $awisRSZeilen;
		
		$SQL='';
		$txtHinweis='';
		
		if($rsSchad_SaveZeilen==0)		// Keine Daten
		{
				awislogoff($conSchad);
				die("<span class=HinweisText>Datensatz wurde nicht gefunden!</span>");
		}
		
		if($_POST['txtEingang'] != $_POST['txtEingang_old'])
		{
			if($rsSchad_Save['EINGANGPER'][0] != $_POST['txtEingang_old'])
			{
				$txtHinweis.=',EINGANGPER von '.$_POST['txtEingang_old']. ' auf '.$rsSchad_Save['EINGANGPER'][0];
			}
			else
			{
				$SQL.=',EINGANGPER='.$_POST['txtEingang'];
			}
		}
		
		if($_POST['txtAnrede'] != $_POST['txtAnrede_old'])
		{
			if($rsSchad_Save['GESCHLECHT'][0] != $_POST['txtAnrede_old'])
			{
				$txtHinweis.=',GESCHLECHT von '.$_POST['txtAnrede_old']. ' auf '.$rsSchad_Save['GESCHLECHT'][0];
			}
			else
			{
				$SQL.=',GESCHLECHT='.$_POST['txtAnrede'];
			}
		}
		
		if($_POST['txtKundenname'] != $_POST['txtKundenname_old'])
		{
			if($rsSchad_Save['KUNDENNAME'][0] != $_POST['txtKundenname_old'])
			{
				$txtHinweis.=',KUNDENNAME von '.$_POST['txtKundenname_old']. ' auf '.$rsSchad_Save['KUNDENNAME'][0];
			}
			else
			{
				$SQL.=',KUNDENNAME=\''.$_POST['txtKundenname'].'\'';
			}
		}
		
		if($_POST['txtKundenvorname'] != $_POST['txtKundenvorname_old'])
		{
			if($rsSchad_Save['VORNAME'][0] != $_POST['txtKundenvorname_old'])
			{
				$txtHinweis.=',VORNAME von '.$_POST['txtKundenvorname_old']. ' auf '.$rsSchad_Save['VORNAME'][0];
			}
			else
			{
				$SQL.=',VORNAME=\''.$_POST['txtKundenvorname'].'\'';
			}
		}
			
		if($_POST['txtStrasse'] != $_POST['txtStrasse_old'])
		{
			if($rsSchad_Save['STRASSE'][0] != $_POST['txtStrasse_old'])
			{
				$txtHinweis.=',STRASSE von '.$_POST['txtStrasse_old']. ' auf '.$rsSchad_Save['STRASSE'][0];
			}
			else
			{
				$SQL.=',STRASSE=\''.$_POST['txtStrasse'].'\'';
			}
		}
		
		if($_POST['txtPLZ'] != $_POST['txtPLZ_old'])
		{
			if($rsSchad_Save['PLZ'][0] != $_POST['txtPLZ_old'])
			{
				$txtHinweis.=',PLZ von '.$_POST['txtPLZ_old']. ' auf '.$rsSchad_Save['PLZ'][0];
			}
			else
			{
				$SQL.=',PLZ=\''.$_POST['txtPLZ'].'\'';
			}
		}
		
		if($_POST['txtOrt'] != $_POST['txtOrt_old'])
		{
			if($rsSchad_Save['ORT'][0] != $_POST['txtOrt_old'])
			{
				$txtHinweis.=',ORT von '.$_POST['txtOrt_old']. ' auf '.$rsSchad_Save['ORT'][0];
			}
			else
			{
				$SQL.=',ORT=\''.$_POST['txtOrt'].'\'';
			}
		}
		
		if($_POST['txtTelefon'] != $_POST['txtTelefon_old'])
		{
			if($rsSchad_Save['TELEFON'][0] != $_POST['txtTelefon_old'])
			{
				$txtHinweis.=',TELEFON von '.$_POST['txtTelefon_old']. ' auf '.$rsSchad_Save['TELEFON'][0];
			}
			else
			{
				$SQL.=',TELEFON=\''.$_POST['txtTelefon'].'\'';
			}
		}
	
		if($_POST['txtTelefon2'] != $_POST['txtTelefon2_old'])
		{
			if($rsSchad_Save['TELEFON2'][0] != $_POST['txtTelefon2_old'])
			{
				$txtHinweis.=',TELEFON2 von '.$_POST['txtTelefon2_old']. ' auf '.$rsSchad_Save['TELEFON2'][0];
			}
			else
			{
				$SQL.=',TELEFON2=\''.$_POST['txtTelefon2'].'\'';
			}
		}
		
		if($_POST['txtEMail'] != $_POST['txtEMail_old'])
		{
			if($rsSchad_Save['EMAIL'][0] != $_POST['txtEMail_old'])
			{
				$txtHinweis.=',EMAIL von '.$_POST['txtEMail_old']. ' auf '.$rsSchad_Save['EMAIL'][0];
			}
			else
			{
				$SQL.=',EMAIL=\''.$_POST['txtEMail'].'\'';
			}
		}
		
		if($_POST['txtPAN'] != $_POST['txtPAN_old'])
		{
			if($rsSchad_Save['PAN'][0] != $_POST['txtPAN_old'])
			{
				$txtHinweis.=',PAN von '.$_POST['txtPAN_old']. ' auf '.$rsSchad_Save['PAN'][0];
			}
			else
			{
				$SQL.=',PAN=\''.$_POST['txtPAN'].'\'';
			}
		}
		//groskdnr
		/*
		if($_POST['txtGrosskdnr'] != $_POST['txtGrosskdnr_old'])
		{
			if($rsSchad_Save['GROSSKDNR'][0] != $_POST['txtGrosskdnr_old'])
			{
				$txtHinweis.=',Gro�kundennr von '.$_POST['txtGrosskdnr_old']. ' auf '.$rsSchad_Save['GROSSKDNR'][0];
			}
			else
			{
				$SQL.=',GROSSKDNR=\''.$_POST['txtGrosskdnr'].'\'';
			}
		}
		*/
		if ($ART=='S') // �nderungen welche die Tabelle der Sch�den betrifft
		{
			if($_POST['txtFax'] != $_POST['txtFax_old'])
			{
				if($rsSchad_Save['FAX'][0] != $_POST['txtFax_old'])
				{
					$txtHinweis.=',FAX von '.$_POST['txtFax_old']. ' auf '.$rsSchad_Save['FAX'][0];
				}
				else
				{
					$SQL.=',FAX=\''.$_POST['txtFax'].'\'';
				}
			}	
			
			if($_POST['txtKFZ_Kennz'] != $_POST['txtKFZ_Kennz_old'])
			{
				if($rsSchad_Save['KFZ_KENNZ'][0] != $_POST['txtKFZ_Kennz_old'])
				{
					$txtHinweis.=',KFZ_KENNZ von '.$_POST['txtKFZ_Kennz_old']. ' auf '.$rsSchad_Save['KFZ_KENNZ'][0];
				}
				else
				{
					$SQL.=',KFZ_KENNZ=\''.$_POST['txtKFZ_Kennz'].'\'';
				}
			}
			
			if($_POST['txtWANR'] != $_POST['txtWANR_old'])
			{
				if($rsSchad_Save['WANR'][0] != $_POST['txtWANR_old'])
				{
					$txtHinweis.=',WANR von '.$_POST['txtWANR_old']. ' auf '.$rsSchad_Save['WANR'][0];
				}
				else
				{
					$SQL.=',WANR=\''.($_POST['txtWANR']!=''?$_POST['txtWANR']:NULL).'\'';
				}
			}
			
			if(isset($_POST['txtRekl_Text']) and isset($_POST['txtRekl_Text_old']) and $_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['FEHLERBESCHREIBUNG'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',FEHLERBESCHREIBUNG von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['FEHLERBESCHREIBUNG'][0];
				}
				else
				{
					$SQL.=',FEHLERBESCHREIBUNG=\''.$_POST['txtRekl_Text'].'\'';
				}
			}
			
			if((isset($_POST['txtSachbearbeiter']) and isset($_POST['txtSachbearbeiter_old']) and $_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old']) AND (!isset($_POST['txtSachbearbeiter_sicherung'])))
			{
				if($rsSchad_Save['BEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',BEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['BEARBEITER'][0];
				}
				else
				{
					$SQL.=',BEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
			
			if(($Rechtestufe&32)==32)
			{
				if($_POST['txtKennung'] != $_POST['txtKennung_old'])
				{
					if($rsSchad_Save['KENNUNG'][0] != $_POST['txtKennung_old'])
					{
						$txtHinweis.=',Kennung von '.$_POST['txtKennung_old']. ' auf '.$rsSchad_Save['KENNUNG'][0];
					}
					else
					{
						$SQL.=',KENNUNG=\''.$_POST['txtKennung'].'\'';
					}
				}
							
				if($_POST['txtBearbStand'] != $_POST['txtBearbStand_old'])
				{
					if($rsSchad_Save['STAND'][0] != $_POST['txtBearbStand_old'])
					{
						$txtHinweis.=',Stand von '.$_POST['txtBearbStand_old']. ' auf '.$rsSchad_Save['STAND'][0];
					}
					else
					{
						$SQL.=',STAND='.$_POST['txtBearbStand'];
					}
				}
				
				if($_POST['txtUrsprAuftrag'] != $_POST['txtUrsprAuftrag_old'])
				{
					if($rsSchad_Save['AUFTRAGSART_ATU_NEU'][0] != $_POST['txtUrsprAuftrag_old'])
					{
						$txtHinweis.=',Auftragsart von '.$_POST['txtUrsprAuftrag_old']. ' auf '.$rsSchad_Save['AUFTRAGSART_ATU_NEU'][0];
					}
					else
					{
						$SQL.=',AUFTRAGSART_ATU_NEU='.$_POST['txtUrsprAuftrag'];
					}
				}
				
				if($_POST['txtWasBeschaedigt'] != $_POST['txtWasBeschaedigt_old'])
				{
					if($rsSchad_Save['BID'][0] != $_POST['txtWasBeschaedigt_old'])
					{
						$txtHinweis.=',Was besch�digt von '.$_POST['txtWasBeschaedigt_old']. ' auf '.$rsSchad_Save['BID'][0];
					}
					else
					{
						$SQL.=',BID='.$_POST['txtWasBeschaedigt'];
					}
				}
				
				if($_POST['txtWoranGearbeitet'] != $_POST['txtWoranGearbeitet_old'])
				{
					if($rsSchad_Save['SCHADENSGRUND'][0] != $_POST['txtWoranGearbeitet_old'])
					{
						$txtHinweis.=',Woran gearbeitet von '.$_POST['txtWoranGearbeitet_old']. ' auf '.$rsSchad_Save['SCHADENSGRUND'][0];
					}
					else
					{
						if ($_POST['txtWoranGearbeitet'] != '0')
						{
							$SQL.=',SCHADENSGRUND='.$_POST['txtWoranGearbeitet'];	
							if ($_POST['txtWoranGearbeitet']!='38' and $_POST['txtWoranGearbeitet']!='51' and $_POST['txtWoranGearbeitet']!='53' and $_POST['txtWoranGearbeitet']!='59' and $_POST['txtWoranGearbeitet']!='12')
							{
								$SQL.=",AUSFALLURSACHE=null";	
							}
						}
						else 
						{
							$SQL.=',SCHADENSGRUND=null';
							if ($_POST['txtWoranGearbeitet']!='38' and $_POST['txtWoranGearbeitet']!='51' and $_POST['txtWoranGearbeitet']!='53' and $_POST['txtWoranGearbeitet']!='59' and $_POST['txtWoranGearbeitet']!='12')
							{
								$SQL.=",AUSFALLURSACHE=null";	
							}
						}					
					}
				}
				
				if($_POST['txtKontaktKunde'] != $_POST['txtKontaktKunde_old'])
				{
					if($rsSchad_Save['KONTAKT_KUNDE'][0] != $_POST['txtKontaktKunde_old'])
					{
						$txtHinweis.=',Kontakt mit Kunde von '.$_POST['txtKontaktKunde_old']. ' auf '.$rsSchad_Save['KONTAKT_KUNDE'][0];
					}
					else
					{
						$SQL.=',KONTAKT_KUNDE='.$_POST['txtKontaktKunde'];
					}
				}
				
				if(($_POST['txtTerminKunde1'] != $_POST['txtTerminKunde1_old']) and $_POST['txtTerminKunde1']!='TT.MM.JJJJ')
				{
					if($rsSchad_Save['TERMIN_KUNDE_1'][0] != $_POST['txtTerminKunde1_old'])
					{
						$txtHinweis.=',Termin 1 mit Kunde von '.$_POST['txtTerminKunde1_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_1'][0];
					}
					else
					{
						$SQL.=',TERMIN_KUNDE_1=\''.$_POST['txtTerminKunde1'].'\'';					
					}
				}
				
				if(($_POST['txtTerminKundeWer1'] != $_POST['txtTerminKundeWer1_old']) and $_POST['txtTerminKundeWer1']!='Name eingeben')
				{
					if($rsSchad_Save['TERMIN_KUNDE_WER_1'][0] != $_POST['txtTerminKundeWer1_old'])
					{
						$txtHinweis.=',Termin (Wer) 1 mit Kunde von '.$_POST['txtTerminKundeWer1_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_WER_1'][0];
					}
					else
					{
						$SQL.=',TERMIN_KUNDE_WER_1=\''.$_POST['txtTerminKundeWer1'].'\'';
					}
				}
				
				if(($_POST['txtTerminKunde2'] != $_POST['txtTerminKunde2_old']) and $_POST['txtTerminKunde2']!='TT.MM.JJJJ')
				{
					if($rsSchad_Save['TERMIN_KUNDE_2'][0] != $_POST['txtTerminKunde2_old'])
					{
						$txtHinweis.=',Termin 2 mit Kunde von '.$_POST['txtTerminKunde2_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_2'][0];
					}
					else
					{
						$SQL.=',TERMIN_KUNDE_2=\''.$_POST['txtTerminKunde2'].'\'';					
					}
				}
							
				if(($_POST['txtTerminKundeWer2'] != $_POST['txtTerminKundeWer2_old']) and $_POST['txtTerminKundeWer2']!='Name eingeben')
				{
					if($rsSchad_Save['TERMIN_KUNDE_WER_2'][0] != $_POST['txtTerminKundeWer2_old'])
					{
						$txtHinweis.=',Termin (Wer) 2 mit Kunde von '.$_POST['txtTerminKundeWer2_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_WER_2'][0];
					}
					else
					{
						$SQL.=',TERMIN_KUNDE_WER_2=\''.$_POST['txtTerminKundeWer2'].'\'';
					}
				}
				
				if($_POST['txtInfoGBL'] != $_POST['txtInfoGBL_old'])
				{
					if($rsSchad_Save['INFO_AN_GBL'][0] != $_POST['txtInfoGBL_old'])
					{
						$txtHinweis.=',Info an GBL von '.$_POST['txtInfoGBL_old']. ' auf '.$rsSchad_Save['INFO_AN_GBL'][0];
					}
					else
					{
						$SQL.=',INFO_AN_GBL='.$_POST['txtInfoGBL'];
					}
				}
				
				if(($_POST['txtEingabedurch'] != $_POST['txtEingabedurch_old']) and $_POST['txtEingabedurch'] != 'Personalnummer eingeben')
				{
					if($rsSchad_Save['EINGABEDURCH_FIL'][0] != $_POST['txtEingabedurch_old'])
					{
						$txtHinweis.=',Eingabedurch von '.$_POST['f']. ' auf '.$rsSchad_Save['EINGABEDURCH_FIL'][0];
					}
					else
					{
						//$SQL.=',EINGABEDURCH_FIL=\''.$_POST['txtEingabedurch'].'\'';
						//$SQL.=',EINGABEDURCH_FIL=\''.$_POST['txtEingabedurchName'].' '.$_POST['txtEingabedurchVorname'].'\'';
						$SQL.=',EINGABEDURCH_FIL=\''.$Pflicht_Felder[37].' '.$Pflicht_Felder[38].'\'';
						$SQL.=',EINGABEDURCH_FILPERSNR=\''.$_POST['txtEingabedurch'].'\'';												
					}
				}
				
				//urldecode()()
				//htmlspecialchars
				if($_POST['txtErg_Text'] != '')
				{	
					$ERGTEXT = $_POST['txtErg_Text'];
					$ERGTEXT = str_replace(chr(13)," ",$ERGTEXT);
					$ERGTEXT = str_replace(chr(10)," ",$ERGTEXT);
					$ERGTEXT = str_replace("'",'"',$ERGTEXT);
					
					//$SQL_BSTANDNEW= 'insert into BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE (BEARBNRNEU,DATUM,BEMERKUNGEN,EINGABEDURCH_FILPERSNR,EINTRAG,ID,BETRAG,ZAHLUNGSART)';
					$SQL_BSTANDNEW= 'insert into BEARBEITUNGSSTAND_NEW (BEARBNRNEU,DATUM,BEMERKUNGEN,EINGABEDURCH_FILPERSNR,EINTRAG,ID,BETRAG,ZAHLUNGSART)';
					$SQL_BSTANDNEW.=' values (\''.$rsSchad_Save['BEARBNRNEU'][0].'\',SYSDATE';				
					//$SQL_BSTANDNEW.=',\''.str_replace("'",'"',$_POST['txtErg_Text']).'\'';												
					$SQL_BSTANDNEW.=',\''.$ERGTEXT.'\'';												
					$SQL_BSTANDNEW.=',\''.$_POST['txtEingabedurch'].'\'';					
					$SQL_BSTANDNEW.=',\''.$Pflicht_Felder[37].' '.$Pflicht_Felder[38].'\'';					
					//$SQL_BSTANDNEW.=',SEQ_BEARBST_NEW_ID.nextval@SCHAD.ATU.DE';
					$SQL_BSTANDNEW.=',SEQ_BEARBST_NEW_ID.nextval';
					
					if (isset($_POST['txtZahlungWert']) and $_POST['txtZahlungWert']!='')
					{
						$ZAHLUNGWERT=$_POST['txtZahlungWert'];
					}
					else 
					{
						$ZAHLUNGWERT='';
					}
					
					if (isset($_POST['txtZahlungArt']) and $_POST['txtZahlungArt']!='')
					{
						$ZAHLUNGART=$_POST['txtZahlungArt'];
					}
					else 
					{
						$ZAHLUNGART='';
					}
					
					$SQL_BSTANDNEW.=',\''.$ZAHLUNGWERT.'\'';
					$SQL_BSTANDNEW.=',\''.$ZAHLUNGART.'\')';
									
					//if ($_POST['txtZahlungWert'] != '' and $_POST['txtZahlungArt']!= '')
					//{
						//$SQL_BESTANDNEW=
						//$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
						//if ($Fil_User == 'fil-')
						//{
						//	$EINGABEDURCH_FILIALE=1;
						//}
						//else 
						//{
						//	$EINGABEDURCH_FILIALE=0;
						//}
						//$SQL_ZAHLUNG= 'insert into ZAHLUNGEN@SCHAD.ATU.DE (zkey,bearbeitungsnr,bid,betrag,verwendungszweck,datum,eingabedurch_filiale)';
						//$SQL_ZAHLUNG.= ' values (zkey_zahlungen.nextval@schad.atu.de,\''.$txt_Bearb_ID.'\',3,\''.$_POST['txtZahlungWert'].'\',\''.$_POST['txtZahlungArt'].'\',SYSDATE, '.$EINGABEDURCH_FILIALE.')';						
					//}
				}				
	
				if((isset($_POST['txtAusfallursache']) AND $_POST['txtAusfallursache'] != $_POST['txtAusfallursache_old']) AND ($_POST['txtWoranGearbeitet']=='38' or $_POST['txtWoranGearbeitet']=='51' or $_POST['txtWoranGearbeitet']=='53' or $_POST['txtWoranGearbeitet']=='59' or $_POST['txtWoranGearbeitet']=='12'))
				{
					if($rsSchad_Save['AUSFALLURSACHE'][0] != $_POST['txtAusfallursache_old'])
					{
						$txtHinweis.=',Ausfallursache von '.$_POST['txtAusfallursache_old']. ' auf '.$rsSchad_Save['AUSFALLURSACHE'][0];
					}
					else 
					{				
						$SQL.=',AUSFALLURSACHE='.$_POST['txtAusfallursache'];
					}
				}
			}
			
	}
		
		if ($ART=='B') // �nderungen welche die Tabelle der Bechwerden betrifft
		{
			if($_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old'])
			{
				if($rsSchad_Save['SACHBEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',SACHBEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['SACHBEARBEITER'][0];
				}
				else
				{
					$SQL.=',SACHBEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
			
			if($_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['BESCHWERDEGRUND'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',BESCHWERDEGRUND von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['BESCHWERDEGRUND'][0];
				}
				else
				{
					$SQL.=',BESCHWERDEGRUND='.$_POST['txtRekl_Text'];	
				}
			}
		}
			
		//<---
		
		if(isset($SQL_BSTANDNEW) and $SQL_BSTANDNEW!='')
		{
			awis_Debug(1,$SQL_BSTANDNEW);
				
			if(awisExecute($conSchad, $SQL_BSTANDNEW)===FALSE)
			{
				awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
				awisLogoff($conSchad);
				die();
			}		
		}
		
		/*if($SQL_ZAHLUNG!='')
		{
			awis_Debug(1,$SQL_ZAHLUNG);
				
			if(awisExecute($con, $SQL_ZAHLUNG)===FALSE)
			{
				awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}			
		}
		*/
		// Update- Befehl		
		
		if($txtHinweis=='' && $SQL!='')
		{
				
				//$SQL.=',EINGABEAM=SYSDATE';
		
				if ($ART=='S')
				{
					$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
					if ($Fil_User == 'fil-')
					{						
						$SQL.=',EINGABEDURCH_FILIALE=1';
						$SQL.=',EINGABEDURCH_FIL_DATUM=SYSDATE';
						$SQL.=',VORGANG_FIL_GEDRUCKT=0';
					}
					//else 
					//{
					//	$EINGABEDURCH_FILIALE=0;
					//}
					
					$SQL.=',EINGABEDURCH=\''.$AWISBenutzer->BenutzerName().'\'';
					
					//$SQL="UPDATE SCHAEDEN_NEW@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
					$SQL="UPDATE SCHAEDEN_NEW SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				else 
				{
					//$SQL="UPDATE BESCHWERDE@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
					$SQL="UPDATE BESCHWERDE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				
				awis_Debug(1,$_REQUEST,$SQL);
				
				if(awisExecute($conSchad, $SQL)===FALSE)
				{
					awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
					awisLogoff($conSchad);
					die();
				}
		}
		elseif($txtHinweis!='')
		{
				echo $txtHinweis;
				awislogoff($conSchad);
				
				die("<br><span class=HinweisText>Datensatz wurde von einen anderen Benutzer ge�ndert !</span>");
		}
	}
}
if ((isset($_POST['cmdHinzufuegen_x']) && ($Rechtestufe&4)==4) || (((isset($Hinzufuegen) && $Hinzufuegen==TRUE) || (isset($_REQUEST['cmdHinzufuegen']) && $_REQUEST['cmdHinzufuegen']==TRUE)) && ($Rechtestufe&4)==4))
{
	if($PDF_Link!='' && ($Rechtestufe&16)==16)
	{
		echo "<br>";
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>&nbsp;PDF-Datei erzeugen";
		echo "<p>";		
	}
	
	$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Speichern", $AWISBenutzer->BenutzerName()));
	
	if(isset($_REQUEST['txtAuftragsart_NEU']) AND $_REQUEST['txtAuftragsart_NEU']!='')
	{
		$ART=$_REQUEST['txtAuftragsart_NEU'];
	}
	else {$ART='S';}
	
	//awis_Debug(1,$Params);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='INSERT'>";
	echo "<input type=hidden name=txtPruefung value='N'>";
	
	
	echo "<table border=0>";
	echo "<tr>";
	echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td>";
		echo "<table border=0>";
		echo '<tr>';
		echo '<td  id=FeldBez>Eingang per</td>';
		echo "<td><select name=txtEingang>";
			echo "<option value='NULL'>Bitte w�hlen...</option>";
			//$rsEingang = awisOpenRecordset($con, 'SELECT id, initcap(mittel) as Eingang FROM EINGANGPER@SCHAD.ATU.DE ORDER BY 1');
			$rsEingang = awisOpenRecordset($conSchad, 'SELECT id, initcap(mittel) as Eingang FROM EINGANGPER ORDER BY 1');
			$rsEingangZeilen = $awisRSZeilen;
			
			if(empty($Params[13]))
			{
				for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
				{
					echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
				}
			}
			else 
			{
				for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
				{
					if ($Params[13]==$rsEingang['ID'][$EingangZeile])
					{
						echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
					}
					else
					{
						echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsEingang);
		echo '</tr>';
		echo '<tr><td><br></td></tr>';
		
				
		if ($ART=='S')
		{
			echo '<tr>';
			echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
			echo "<td><input type=text name=txtKFZ_Kennz value='".(empty($Params[11])?"":$Params[11]) ."' size=30></td>";
			echo '</tr>';
			echo '<tr>';
			echo '<td  id=FeldBez>Filiale</td>';
			echo "<td><select name=txtFIL_ID>";
				echo "<option value='999'>Bitte w�hlen...</option>";
				$rsFIL = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ, NVL2(FIL_ORTSTEIL,FIL_ORT ||'-'|| FIL_ORTSTEIL,FIL_ORT) AS ORT, FIL_ORT, FIL_ORTSTEIL, FIL_STRASSE FROM V_FIL_VKL_GBL ORDER BY FIL_ORT");
				$rsFILZeilen = $awisRSZeilen;
				
				if(empty($Params[0]))
				{
					for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
					{
							echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . '</option>';
					}
				}
				else 
				{
					for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
					{
						if($Params[0]==$rsFIL['FIL_ID'][$FILZeile])
						{
							echo "<option value=" . $rsFIL['FIL_ID'][$FILZeile] . " selected='selected'>" . $rsFIL['ORT'][$FILZeile] . " | " . $rsFIL['FIL_STRASSE'][$FILZeile] . " | " . $rsFIL['FIL_ID'][$FILZeile] . "</option>";	
						}
						else 
						{
							echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . '</option>';
						}
					}
				}
				echo '</select></td>';
				unset($rsFIL);
			echo '</tr>';
			echo '<tr>';
			echo '<td id=FeldBez>WA-Nummer</td>';
			echo "<td><input type=text name=txtWA_NR value='".(empty($Params[14])?"":$Params[14]) ."' size=10></td>";
			echo '</tr>';
			
		}
		else
		{
			echo '<tr>';
			echo '<td  id=FeldBez>Beschwerdegrund</td>';
			echo "<td><select name=txtRekl_Text>";
				echo '<option value=-1>Bitte w�hlen...</option>';
				//$rsRekl_Text = awisOpenRecordset($con, 'SELECT ID, BESCHWERDEGRUND FROM BESCHWERDEGRUENDE@SCHAD.ATU.DE ORDER BY 1');
				$rsRekl_Text = awisOpenRecordset($conSchad, 'SELECT ID, BESCHWERDEGRUND FROM BESCHWERDEGRUENDE ORDER BY 1');
				$rsRekl_TextZeilen = $awisRSZeilen;
				for($Rekl_TextZeile=0;$Rekl_TextZeile<$rsRekl_TextZeilen;$Rekl_TextZeile++)
				{
						echo '<option value=' . $rsRekl_Text['ID'][$Rekl_TextZeile] . '>' . $rsRekl_Text['BESCHWERDEGRUND'][$Rekl_TextZeile] . '</option>';
				}
				echo '</select></td>';
				unset($rsRekl_Text);
			echo '</tr>';
			
		}
		
		echo '<tr><td><br></td></tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Sachbearbeiter</td>';
		echo "<td><select name=txtSachbearbeiter>";
			echo '<option value=0>Bitte w�hlen...</option>';
			//$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME,SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304) ORDER BY 1');
			$rsSB = awisOpenRecordset($conSchad, 'SELECT ID, SBNAME,SBVORNAME FROM SACHBEARBEITER WHERE ID IN (303,304) ORDER BY 1');
			$rsSBZeilen = $awisRSZeilen;
			
			if(empty($Params[15]))
			{
				for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
				{
						echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
				}
			}
			else 
			{
				for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
				{
					if($Params[15]==$rsSB['ID'][$SBZeile])
					{
						echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile] . "" . $rsSB['SBVORNAME'][$SBZeile] . "</option>";	
					}
					else 
					{
						echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsSB);
		echo '</tr>';
		
		echo '</table>';
	
	
	
	
	echo "</td>";
	
	echo "<td>";
		echo "<table border=0";
		
		echo '<tr>';
		echo '<td  id=FeldBez>Anrede</td>';
		echo "<td><select name=txtAnrede>";
			//$rsAnrede = awisOpenRecordset($con, 'SELECT ID, NAME FROM ANREDE@SCHAD.ATU.DE ORDER BY 1');
			$rsAnrede = awisOpenRecordset($conSchad, 'SELECT ID, NAME FROM ANREDE ORDER BY 1');
			$rsAnredeZeilen = $awisRSZeilen;
			
			if(empty($Params[16]))
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
					echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
				}
			}
			else 
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
					if ($Params[16]==$rsAnrede['ID'][$AnredeZeile])
					{
						echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
					}
					else
					{
						echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsAnrede);
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Kundenname / -vorname</td>';
		echo "<td><input type=text name=txtKundenname value='".(empty($Params[1])?"":$Params[1]) ."' size=15>&nbsp;<input type=text name=txtKundenvorname value='".(empty($Params[2])?"":$Params[2]) ."' size=15></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Strasse</td>';
		echo "<td><input type=text name=txtStrasse value='".(empty($Params[3])?"":$Params[3]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>PLZ / Ort</td>';
		echo "<td><input type=text name=txtPLZ value='".(empty($Params[4])?"":$Params[4]) ."' size=8>&nbsp;<input type=text name=txtOrt value='".(empty($Params[5])?"":$Params[5]) ."' size=20></td>";
		echo '</tr>';
		echo '<tr>';
		echo "<td id=FeldBez>Telefon / Telefon 2</td>";
		echo "<td><input type=text name=txtTelefon value='".(empty($Params[6])?"":$Params[6]) ."' size=15>&nbsp;<input type=text name=txtTelefon2 value='".(empty($Params[7])?"":$Params[7]) ."' size=15></td>";
		echo '</tr>';
		if ($ART=='S')
		{
			echo '<tr>';
			echo '<td id=FeldBez>Fax</td>';
			echo "<td><input type=text name=txtFax value='".(empty($Params[8])?"":$Params[8]) ."' size=30></td>";
			echo '</tr>';
		}
		echo '<tr>';
		echo '<td id=FeldBez>E-Mail</td>';
		echo "<td><input type=text name=txtEMail value='".(empty($Params[9])?"":$Params[9]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>ATU-Card-Nummer</td>'; 
		echo "<td><input type=text name=txtPAN value='".(empty($Params[10])?"":$Params[10]) ."' size=30></td>";
		echo '</tr>';
		//groskdnr
		/*
		echo '<tr>';
		echo '<td id=FeldBez>Gro�-Kunden-Nummer</td>'; 
		echo "<td><input type=text name=txtGrosskdnr value='".(empty($Params[17])?"":$Params[17]) ."' size=30></td>";
		echo '</tr>';
		*/
		//echo '<tr>';
		//echo '<td id=FeldBez>Gro�-Kunden-Name</td>'; 
		//echo "<td><input type=text name=txtGrosskdname value='".(empty($Params[18])?"":$Params[18]) ."' size=30></td>";
		//echo '</tr>';				
		echo "</table>";
	
	echo "</td>";
	echo "</tr>";	
	echo "</table>";
	
	if ($ART=='S'){
		echo "<table border=0>";
		echo '<tr>';
		echo '<td id=FeldBez>Beschwerdebeschreibung (max. 2500 Zeichen)</td>';
		echo '</tr>';
		echo '<tr>';
		echo "<td><textarea name=txtRekl_Text rows=7 cols=150>".(empty($Params[12])?"":$Params[12]) ."</textarea></td>";
		echo '</tr>';
		echo "</table>";
	}
	
	echo "</form>";
	
}
elseif ((((isset($_REQUEST['BEARB_ID']) && !isset($_POST['txtFilialNrOffene'])) || (isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='' && !isset($_POST['txtFilialNrOffene']))) && ! isset($_POST['cmdAnzeigen_x'])) ||
	   ((isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='' && isset($_POST['txtFilialNrOffene']) && $_POST['txtFilialNrOffene']==''))) //Anzeigen der Details f�r Bearbeitungsnummer 
//elseif ((isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='' && isset($_POST['txtFilialNrOffene']) && $_POST['txtFilialNrOffene']=='')) //Anzeigen der Details f�r Bearbeitungsnummer 
{
	if (isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='')
	{
		$BEARB_ID=$_POST['BEARB_ID'];
	}
	else 
	{
		$BEARB_ID=$_REQUEST['BEARB_ID'];
	}
	
	if (isset($_POST['ART']) && $_POST['ART']!='')
	{
		$ART=$_POST['ART'];
	}
	else 
	{
		$ART=$_REQUEST['ART'];
	}
	
	awis_Debug(1,$_REQUEST);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";

	echo "<input type=hidden name=txt_Bearb_ID_Save  value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=BEARB_ID value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='UPDATE'>";
	echo "<input type=hidden name=txtPruefung value='J'>";
	
	if ($ART=='S')
	{
		$SQL = "SELECT BEARBNRNEU, ART, to_clob(FEHLERBESCHREIBUNG) AS REKL_TEXT, FILNR, VERURS_FILIALE, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ";
		//groskdnr
		//$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, GROSSKDNR, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM, ";
		$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL, EINGABEDURCH_FILPERSNR ";
		//$SQL .= "FROM SCHAEDEN_NEW@SCHAD.ATU.DE ";
		$SQL .= "FROM SCHAEDEN_NEW ";
	}
	else 
	{
		$SQL = "SELECT ART, BESCHWERDEGRUND AS REKL_TEXT, FILNR, BEARBEITUNGSNR, SACHBEARBEITER, KUNDENNAME, VORNAME, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, EMAIL, PAN, EINGANGPER ";
		//$SQL .= "FROM BESCHWERDE@SCHAD.ATU.DE ";
		$SQL .= "FROM BESCHWERDE ";
	}
	
	$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
	$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);
	
	$SQL.="WHERE BEARBEITUNGSNR='". $BEARB_ID ."'";
	
	if (($UserFilialen!='' AND $UserFilialen!='0') or ($Fil_User=='fil-'))
	{
		if ($UserFilialen!='' AND $UserFilialen!='0')
		{
			$SQL.= " AND FILNR='".intval($UserFilialen)."'";
		}
		else 
		{
			$SQL.= " AND FILNR='".intval($Fil_Nr)."'";
		}

		//$SQL.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";
		$SQL.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";
		
	}
	
	awis_Debug(1,$SQL);
	
	$rsSchad = awisOpenRecordset($conSchad,$SQL,true,true);
	$rsSchadZeilen = $awisRSZeilen;		
	
	if ($rsSchadZeilen==0)
	{
		echo "<br>";
		echo "<center><span class=HinweisText>F&uuml;r die angegebenen Bearbeitungsnummer wurde kein Ergebniss gefunden!</span></center>";
	}
	else
	{
		for($i=0;$i<$rsSchadZeilen;$i++)
		{
			//if ($rsSchad['AKTEGESCHLOSSENAM'][$i]=='' or !($UserFilialen!='' AND $UserFilialen!='0'))
			if ($rsSchad['AKTEGESCHLOSSENAM'][$i]=='' or ($Rechtestufe&128)==128)
			{
				$aendern=true;
			}
			else 
			{
				$aendern=false;
			}
			
			if ($Fil_User != 'fil-')
			{
				echo "<table width=100% border=0><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
				echo "</tr>";
				echo "</table>";
			}
			
			echo "<table width=80% border=0>";
			echo '<tr>';
			echo '<td>';
				echo "<table border=0>";
				//echo '<tr>';
				//echo '<td  id=FeldBez>Auftragsart</td>';
				//echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . ($rsSchad['ART'][$i]=='S'?'Schaden':'Beschwerde') . "</td>";
				//echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Bearbeitungs-Nr</td>';
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['BEARBEITUNGSNR'][$i] . "</td>";
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Eingang per</td>';
                if ($aendern==true)
                {
				    echo "<td id=TabellenZeileGrau><select name=txtEingang>";
                }
                else
                {
                    echo "<td id=TabellenZeileGrau><select name=txtEingang disabled>";
                }
					//$rsEingang = awisOpenRecordset($con, 'SELECT ID, INITCAP(MITTEL) AS EINGANG FROM EINGANGPER@SCHAD.ATU.DE');
					$rsEingang = awisOpenRecordset($conSchad, 'SELECT ID, INITCAP(MITTEL) AS EINGANG FROM EINGANGPER');
					$rsEingangZeilen = $awisRSZeilen;
					
					echo "<option value='NULL' selected='selected'>:: Kein Angabe ::</option>";
					
                    if ($Save_Felder==false)
                    {
                        for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
					    {
                            if(isset($Pflicht_Felder[20]) AND $Pflicht_Felder[20]==$rsEingang['ID'][$EingangZeile])
                            {
                                echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
                            }                            
                            else 
                            {
                                echo "<option value=" . $rsEingang['ID'][$EingangZeile] . ">" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
						    }
					    }
                    }
                    else
                    {                                        
                        for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
                        {
                            if($rsEingang['ID'][$EingangZeile]==$rsSchad['EINGANGPER'][$i])
                            {
                                echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
                            }
                            else 
                            {
                                echo "<option value=" . $rsEingang['ID'][$EingangZeile] . ">" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
                            }
                        }
                    }
					
				echo "</select><input type=hidden name=txtEingang_old value=".$rsSchad['EINGANGPER'][$i] .">";
				echo '<tr><td><br></td></tr>';
				echo '<tr>';
				echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . (isset($Pflicht_Felder[21])?$Pflicht_Felder[21]:'') . "'><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . $rsSchad['KFZ_KENNZ'][$i] . "'><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . $rsSchad['KFZ_KENNZ'][$i] . "' disabled><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
                    }
				}
				echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Filial-Nr</td>';
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['FILNR'][$i] . "</td>";
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>WA-Nummer</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . (isset($Pflicht_Felder[22])?$Pflicht_Felder[22]:'') . "'><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";					
				}
				else 
				{
                    if ($aendern==true)
                    {                    
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . $rsSchad['WANR'][$i] . "'><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . $rsSchad['WANR'][$i] . "' disabled><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				echo '<tr><td><br></td></tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Sachbearbeiter</td>';
                if (($Rechtestufe&32)==32)
                {
					echo "<input type=hidden name=txtSachbearbeiter_sicherung value=".$rsSchad['SACHBEARBEITER'][$i] .">";
                	echo "<td id=TabellenZeileGrau><select name=txtSachbearbeiter disabled>";
                }
                else
                {
                    echo "<td id=TabellenZeileGrau><select name=txtSachbearbeiter>";
                }
					//$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME, SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304)');
					$rsSB = awisOpenRecordset($conSchad, 'SELECT ID, SBNAME, SBVORNAME FROM SACHBEARBEITER WHERE ID IN (303,304)');
					$rsSBZeilen = $awisRSZeilen;
					
					echo "<option value='NULL'>:: Kein Angabe ::</option>";
					
                    if ($Save_Felder==false)
                    {
                        for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
                        {
                            if(isset($Pflicht_Felder[23]) AND $rsSB['ID'][$SBZeile]==$Pflicht_Felder[23])
                            {
                                echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
                            }						                            
                            else 
                            {
                                echo "<option value=" . $rsSB['ID'][$SBZeile] . ">" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
                            }
                        }
                    }
                    else
                    {                    
                        for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
                        {
                            if($rsSB['ID'][$SBZeile]==$rsSchad['SACHBEARBEITER'][$i])
                            {
                                echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
                            }						
                            else 
                            {
                                echo "<option value=" . $rsSB['ID'][$SBZeile] . ">" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
                            }
                        }
                    }
					
				echo "</select><input type=hidden name=txtSachbearbeiter_old value=".$rsSchad['SACHBEARBEITER'][$i] ."></td>";
				echo '</tr>';
				echo "</table>";
			echo '</td>';
			echo '<td>';
				echo "<table border=0>";
				
				echo '<tr>';
				echo '<td  id=FeldBez>Anrede</td>';
                
                if ($aendern==true)
                {                
				    echo "<td><select name=txtAnrede>";
                }
                else
                {
                    echo "<td><select name=txtAnrede disabled>";
                }
					//$rsAnrede = awisOpenRecordset($con, 'SELECT ID, NAME FROM ANREDE@SCHAD.ATU.DE ORDER BY 1');
					$rsAnrede = awisOpenRecordset($conSchad, 'SELECT ID, NAME FROM ANREDE ORDER BY 1');
					$rsAnredeZeilen = $awisRSZeilen;
                    
                    if ($Save_Felder==false)
                    {
                        for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
						{
							if (isset($Pflicht_Felder[24]) AND $Pflicht_Felder[24]==$rsAnrede['ID'][$AnredeZeile])
							{
								echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
							}
							else
							{
								echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
							}
						}
                    }                    
                    else
                    {					
						for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
						{
							if ($rsSchad['GESCHLECHT'][$i]==$rsAnrede['ID'][$AnredeZeile])
							{
								echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
							}					
							else
							{
								echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
							}
						}
                    }
					echo "</select><input type=hidden name=txtAnrede_old value=".$rsSchad['GESCHLECHT'][$i] ."></td>";
					unset($rsAnrede);
				echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Kundenname / -vorname</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=15 value='" . (isset($Pflicht_Felder[25])?$Pflicht_Felder[25]:'') . "'><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=15 value='" . $rsSchad['KUNDENNAME'][$i] . "'><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=15 value='" . $rsSchad['KUNDENNAME'][$i] . "' disabled><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";					
                    }
				}				
				echo "&nbsp;";
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtKundenvorname size=15 value='" . (isset($Pflicht_Felder[26])?$Pflicht_Felder[26]:'') . "'><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<input type=text name=txtKundenvorname size=15 value='" . $rsSchad['VORNAME'][$i] . "'><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<input type=text name=txtKundenvorname size=15 value='" . $rsSchad['VORNAME'][$i] . "' disabled><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Strasse</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . (isset($Pflicht_Felder[27])?$Pflicht_Felder[27]:'') . "'><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . $rsSchad['STRASSE'][$i] . "'><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . $rsSchad['STRASSE'][$i] . "' disabled><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
                    }
				}   
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>PLZ / Ort</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=8 value='" . (isset($Pflicht_Felder[28])?$Pflicht_Felder[28]:'') . "'><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";					
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=8 value='" . $rsSchad['PLZ'][$i] . "'><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=8 value='" . $rsSchad['PLZ'][$i] . "' disabled><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";					
                    }
				}				
				echo '&nbsp;';
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtOrt size=20 value='" . (isset($Pflicht_Felder[29])?$Pflicht_Felder[29]:'') . "'><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {                    
					    echo "<input type=text name=txtOrt size=20 value='" . $rsSchad['ORT'][$i] . "'><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
                    }
                    else
                    {
                        echo "<input type=text name=txtOrt size=20 value='" . $rsSchad['ORT'][$i] . "' disabled><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
                    }
				}
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Telefon / Telefon 2</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=15 value='" . (isset($Pflicht_Felder[30])?$Pflicht_Felder[30]:'') . "'><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=15 value='" . $rsSchad['TELEFON'][$i] . "'><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=15 value='" . $rsSchad['TELEFON'][$i] . "' disabled><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>";					
                    }
				}				
				echo "&nbsp;";
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtTelefon2 size=15 value='" . (isset($Pflicht_Felder[31])?$Pflicht_Felder[31]:'') . "'><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<input type=text name=txtTelefon2 size=15 value='" . $rsSchad['TELEFON2'][$i] . "'><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";	
                    }
                    else
                    {
                        echo "<input type=text name=txtTelefon2 size=15 value='" . $rsSchad['TELEFON2'][$i] . "' disabled><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";	
                    }
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Fax</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . (isset($Pflicht_Felder[32])?$Pflicht_Felder[32]:'') . "'><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . $rsSchad['FAX'][$i] . "'><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . $rsSchad['FAX'][$i] . "' disabled><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>E-Mail</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . (isset($Pflicht_Felder[33])?$Pflicht_Felder[33]:'') . "'><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . $rsSchad['EMAIL'][$i] . "'><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . $rsSchad['EMAIL'][$i] . "' disabled><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>ATU-Card-Nummer</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . (isset($Pflicht_Felder[34])?$Pflicht_Felder[34]:'') . "'><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {                    
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . $rsSchad['PAN'][$i] . "'><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . $rsSchad['PAN'][$i] . "' disabled><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				//groskdnr
				/*
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Gro�-Kunden-Nummer</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtGrosskdnr size=30 value='" . (isset($Pflicht_Felder[41])?$Pflicht_Felder[41]:'') . "'><input type=hidden name=txtGrosskdnr_old  value='" . $rsSchad['GROSSKDNR'][$i] . "'></td>";
				}
				else 
				{
                    if ($aendern==true)
                    {                    
					    echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtGrosskdnr size=30 value='" . $rsSchad['GROSSKDNR'][$i] . "'><input type=hidden name=txtGrosskdnr_old  value='" . $rsSchad['GROSSKDNR'][$i] . "'></td>";					
                    }
                    else
                    {
                        echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtGrosskdnr size=30 value='" . $rsSchad['GROSSKDNR'][$i] . "' disabled><input type=hidden name=txtGrosskdnr_old  value='" . $rsSchad['GROSSKDNR'][$i] . "'></td>";					
                    }
				}				
				echo '</tr>';
				
				if (isset($rsSchad['GROSSKDNR'][$i]) and $rsSchad['GROSSKDNR'][$i] != '')
				{
					$SQL = ' SELECT * FROM CRMAKQUISESTAND INNER JOIN CRMADRESSEN ON CAD_KEY = CAQ_CAD_KEY';
					$SQL .= ' WHERE CAQ_VERTRAGSNUMMER ' . awisLIKEoderIST($rsSchad['GROSSKDNR'][$i]);
					
					$rsGKD = awisOpenRecordset($con, $SQL);
					$rsGKDZeilen = $awisRSZeilen;

					if ($rsGKDZeilen > 0)
					{					
						echo '<tr>';
						echo '<td id=FeldBez>Gro�-Kunden-Name</td>';
						echo '<td>'.$rsGKD['CAD_NAME1'][0].'</td>';
						echo '</tr>';
					}												
				}								
				*/
				echo "</table>";
				
			echo '</td>';
			echo '</tr>';
			echo "</table>";
			
			echo "<table border=0>";
			echo '<tr>';
			echo '<td id=FeldBez>Beschwerdebeschreibung (max. 2500 Zeichen)</td>';
			echo '</tr>';
			echo '<tr>';
			if (($Rechtestufe&32)==32)
			{
				echo '<td>'. $rsSchad['REKL_TEXT'][$i].'</td>';
			}
			else 
			{
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><textarea name=txtRekl_Text  rows=7 cols=150>" . $rsSchad['REKL_TEXT'][$i] . "</textarea><input type=hidden name=txtRekl_Text_old  value='" . $rsSchad['REKL_TEXT'][$i] . "'></td>";			
			}
			echo '</tr>';
			echo "</table>";
		
		if (($Rechtestufe&32)==32)
		{		
			echo "<br><hr noshade><b>Beschwerdebearbeitung / R�ckmeldung Filiale</b>";
			
			if (isset($rsSchad['AKTEGESCHLOSSENAM'][$i]) AND $rsSchad['AKTEGESCHLOSSENAM'][$i]!='')
			{
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=HinweisText>Vorgang geschlossen!</span>";			
			}
			echo "<br><br>";
			
			echo "<table border=0>";
			echo "<tr>";
//			echo "<td>";
//				echo "<table border=0>";			
//				echo "<tr>";
				if ($Save_Felder==False and !isset($Pflicht_Felder[3]))
				{
					echo "<td id=FeldBezPflicht>Kontakt mit Kunde*</td>";
				}
				
				else 
				{
					echo "<td id=FeldBez>Kontakt mit Kunde*</td>";
				}
                if ($aendern==true)
                {
                	echo "<td><select name=txtKontaktKunde>";
                }
                else
                {
                	echo "<td><select name=txtKontaktKunde disabled>";
                }
				//$rsKontaktKunde = awisOpenRecordset($con, 'SELECT * FROM KONTAKT_KUNDE@SCHAD.ATU.DE ORDER BY WERT');
				$rsKontaktKunde = awisOpenRecordset($conSchad, 'SELECT * FROM KONTAKT_KUNDE ORDER BY WERT');
				$rsKontaktKundeZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($KontaktKundeZeile=0;$KontaktKundeZeile<$rsKontaktKundeZeilen;$KontaktKundeZeile++)
					{
						if(isset($Pflicht_Felder[3]) AND $Pflicht_Felder[3]==$rsKontaktKunde['ID'][$KontaktKundeZeile])
						{
							echo "<option value=" . $rsKontaktKunde['ID'][$KontaktKundeZeile] . " selected='selected'>" . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . "</option>";							
						}						
						else
						{
							echo '<option value=' . $rsKontaktKunde['ID'][$KontaktKundeZeile] . '>' . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($KontaktKundeZeile=0;$KontaktKundeZeile<$rsKontaktKundeZeilen;$KontaktKundeZeile++)
					{
						if ($rsSchad['KONTAKT_KUNDE'][0]==$rsKontaktKunde['ID'][$KontaktKundeZeile])
						{
							echo "<option value=" . $rsKontaktKunde['ID'][$KontaktKundeZeile] . " selected='selected'>" . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . "</option>";
						}					
						else 
						{
							echo '<option value=' . $rsKontaktKunde['ID'][$KontaktKundeZeile] . '>' . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . '</option>';
						}
					}										
				}
								
				echo "</select><input type=hidden name=txtKontaktKunde_old value=".$rsSchad['KONTAKT_KUNDE'][$i] ."></td>";
				unset($rsKontaktKunde);	
				echo "<td id=FeldBez>Bearbeitende Filiale</td>";
				echo "<td>".$rsSchad['FILNR'][$i]."</td>";
				echo "</tr>";								
				
				echo "<tr>";
				echo '<td id=FeldBez>Termin mit Kunden am</td>';
				if($Save_Felder==False)
				{
					if ($Pflicht_Felder[13]!='TT.MM.JJJJ')// and $Pflicht_Felder[13]!='')
					{
						echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde1 size=10 value='" . $Pflicht_Felder[13] . "'><input type=hidden name=txtTerminKunde1_old  value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
					}
					else 
					{
						echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde1 size=10 value='TT.MM.JJJJ' onfocus=this.value=''><input type=hidden name=txtTerminKunde1_old  value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
					}
				}
				else 
				{
                    if ($aendern==true)
                    {
                    	if ($rsSchad['TERMIN_KUNDE_1'][$i]!='')
                    	{                    		
					    	echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde1 size=10 value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'><input type=hidden name=txtTerminKunde1_old value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
                    	}
                    	else 
                    	{
                    		echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde1 size=10 value='TT.MM.JJJJ' onfocus=this.value=''><input type=hidden name=txtTerminKunde1_old value='" . $rsSchad['TERMIN_KUNDE_1'][$i]."'>";
                    	}
					    	
                    }
                    else
                    {
                        echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde1 size=10 value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "' disabled><input type=hidden name=txtTerminKunde1_old  value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
                    }
				}			
				
				if($Save_Felder==False)
				{
					if ($Pflicht_Felder[14]!='Name eingeben')// and $Pflicht_Felder[14]!='')
					{
						echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer1 size=20 value='" . $Pflicht_Felder[14] . "'><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
					}
					else 
					{
						echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer1 size=20 value='Name eingeben' onfocus=this.value=''><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
					}
				}
				else 
				{
                    if ($aendern==true)
                    {
                    	if ($rsSchad['TERMIN_KUNDE_WER_1'][$i]!='')
                    	{
					    	echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer1 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
                    	}
                    	else 
                    	{
                    		echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer1 size=20 value='Name eingeben' onfocus=this.value=''><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
                    	}
                    }
                    else
                    {
                        echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer1 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "' disabled><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
                    }
				}
				echo "</td>";
				echo "<td id=FeldBez>Verursachende Filiale</td>";
				echo "<td>".$rsSchad['VERURS_FILIALE'][$i]."</td>";
				echo "</tr>";
				
				echo "<tr>";
				echo '<td id=FeldBez>Termin mit Kunden am</td>';
				if($Save_Felder==False)
				{
					if ($Pflicht_Felder[15]!='TT.MM.JJJJ') //and $Pflicht_Felder[15]!='')
					{
						echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde2 size=10 value='" . $Pflicht_Felder[15] . "'><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
					}
					else 
					{
						echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde2 size=10 value='TT.MM.JJJJ' onfocus=this.value=''><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
					}
				}
				else 
				{
                    if ($aendern==true)
                    {
                    	if ($rsSchad['TERMIN_KUNDE_2'][$i]!='')
                    	{
					    	echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde2 size=10 value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
                    	}
                    	else 
                    	{
                    		echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde2 size=10 value='TT.MM.JJJJ' onfocus=this.value=''><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
                    	}
                    }
                    else
                    {
                        echo "<td><input type=text title='Bitte Datum (TT.MM.JJJJ) eingeben' name=txtTerminKunde2 size=10 value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "' disabled><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
                    }
				}			
				
				if($Save_Felder==False)
				{
					if ($Pflicht_Felder[16]!='Name eingeben')// and $Pflicht_Felder[16]!='')
					{
						echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer2 size=20 value='" . $Pflicht_Felder[16] . "'><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
					}
					else 
					{
						echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer2 size=20 value='Name eingeben' onfocus=this.value=''><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
					}
				}
				else 
				{
                    if ($aendern==true)
                    {
                    	if ($rsSchad['TERMIN_KUNDE_WER_2'][$i]!='')
                    	{
					    	echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer2 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
                    	}
                    	else 
                    	{
                    		echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer2 size=20 value='Name eingeben' onfocus=this.value=''><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
                    	}
                    }
                    else
                    {
                        echo "Wer:<input type=text title='Bitte Namen eingeben' name=txtTerminKundeWer2 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "' disabled><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
                    }
				}
				echo "</td></tr>";			
				
				echo "<tr>";
				if (($Save_Felder==False and !isset($Pflicht_Felder[4])) or ($Save_Felder==False and $Pflicht_Felder[4]!='1' and intval($Pflicht_Felder[12])>=250))
				{
					echo "<td id=FeldBezPflicht>Info an GBL*</td>";			
				}
				else 
				{
					echo "<td id=FeldBez>Info an GBL*</td>";			
				}
                if ($aendern==true)
                {                
				    echo "<td><select name=txtInfoGBL>";			
                }
                else
                {
                    echo "<td><select name=txtInfoGBL disabled>";			
                }
				//$rsInfoGBL = awisOpenRecordset($con, 'SELECT * FROM INFO_AN_GBL@SCHAD.ATU.DE ORDER BY WERT');
				$rsInfoGBL = awisOpenRecordset($conSchad, 'SELECT * FROM INFO_AN_GBL ORDER BY WERT');
				$rsInfoGBLZeilen = $awisRSZeilen;
							
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
								
				if($Save_Felder==False)
				{
					for($InfoGBLZeile=0;$InfoGBLZeile<$rsInfoGBLZeilen;$InfoGBLZeile++)
					{
						if(isset($Pflicht_Felder[4]) AND $Pflicht_Felder[4]==$rsInfoGBL['ID'][$InfoGBLZeile])
						{
							echo "<option value=" . $rsInfoGBL['ID'][$InfoGBLZeile] . " selected='selected'>" . $rsInfoGBL['WERT'][$InfoGBLZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsInfoGBL['ID'][$InfoGBLZeile] . '>' . $rsInfoGBL['WERT'][$InfoGBLZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($InfoGBLZeile=0;$InfoGBLZeile<$rsInfoGBLZeilen;$InfoGBLZeile++)
					{
						if ($rsSchad['INFO_AN_GBL'][0]==$rsInfoGBL['ID'][$InfoGBLZeile])
						{
							echo "<option value=" . $rsInfoGBL['ID'][$InfoGBLZeile] . " selected='selected'>" . $rsInfoGBL['WERT'][$InfoGBLZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsInfoGBL['ID'][$InfoGBLZeile] . '>' . $rsInfoGBL['WERT'][$InfoGBLZeile] . '</option>';
						}
					}										
				}
				
				echo "</select></td><input type=hidden name=txtInfoGBL_old value=".$rsSchad['INFO_AN_GBL'][$i] ."></td>";
				unset($rsInfoGBL);
				echo "</select></td>";
				echo "</tr>";										
				
				echo "<tr>";
				if ($Save_Felder==False and $Pflicht_Felder[12]!='' and $Pflicht_Felder[11]=='')
				{
					echo "<td id=FeldBezPflicht>Ggf. Zahlung</td>";
				}
				else 
				{
					echo "<td id=FeldBez>Ggf. Zahlung</td>";
				}
				//$SQLZL="SELECT * FROM ZAHLUNGEN@SCHAD.ATU.DE WHERE EINGABEDURCH_FILIALE=1 and BEARBEITUNGSNR= '". $rsSchad['BEARBEITUNGSNR'][$i] ."' order by ZKEY DESC";
				//$rsZahlung = awisOpenRecordset($con, $SQLZL);
				//$rsZahlungZeilen = $awisRSZeilen;
				//if ($rsZahlungZeilen > 0)						
				//{
				if ($Save_Felder==False)
				{
					echo "<td>Wert in &euro;: <input type=text title='Bitte Wert in &euro; eingeben' name=txtZahlungWert value='" .$Pflicht_Felder[12]. "' size=10>";			
				}
				else 
				{
					if ($aendern==true)
					{
						echo "<td>Wert in &euro;: <input type=text title='Bitte Wert in &euro; eingeben' name=txtZahlungWert size=10>";			
					}
					else 
					{
						echo "<td>Wert in &euro;: <input type=text title='Bitte Wert in &euro; eingeben' name=txtZahlungWert disabled>";			
					}
				}
/*				}
				else 
				{
					if($Save_Felder==False)
					{
						echo "<td>Wert in &euro;: <input type=text name=txtZahlungWert value='" .$Pflicht_Felder[12]."' size=10>";			
					}
					else 
					{
						if ($aendern==true)
						{
							echo "<td>Wert in &euro;: <input type=text name=txtZahlungWert size=10>";			
						}
						else 
						{
							echo "<td>Wert in &euro;: <input type=text name=txtZahlungWert size=10 disabled>";			
						}
					}
				}
*/				
                if ($aendern==true)
                {
				    echo "<select name=txtZahlungArt>";
                }
                else
                {
				    echo "<select name=txtZahlungArt disabled>";
                }
				//$rsZahlungDurch = awisOpenRecordset($con, 'SELECT * FROM ZAHLUNG_DURCH@SCHAD.ATU.DE ORDER BY WERT');
				$rsZahlungDurch = awisOpenRecordset($conSchad, 'SELECT * FROM ZAHLUNG_DURCH ORDER BY WERT');
				$rsZahlungDurchZeilen = $awisRSZeilen;
				
				echo "<option value='' selected='selected'>:: Bitte w�hlen ::</option>";
                
                if ($Save_Felder==false)
                {
                    for($ZahlungDurchZeile=0;$ZahlungDurchZeile<$rsZahlungDurchZeilen;$ZahlungDurchZeile++)
                    {
                        if(isset($Pflicht_Felder[11]) AND $Pflicht_Felder[11]==$rsZahlungDurch['WERT'][$ZahlungDurchZeile])
                        {
                            echo "<option value=" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . " selected='selected'>" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . "</option>";						
                        }
                        else
                        {
                            echo '<option value=' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '>' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '</option>';
                        }
                    }
                }
                else
                {                    
                    for($ZahlungDurchZeile=0;$ZahlungDurchZeile<$rsZahlungDurchZeilen;$ZahlungDurchZeile++)
                    {
//                        if ($rsZahlungZeilen > 0 AND $rsZahlung['VERWENDUNGSZWECK'][0]==$rsZahlungDurch['WERT'][$ZahlungDurchZeile])
//                        {
//                            echo "<option value=" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . " selected='selected'>" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . "</option>";
//                        }
//                        else 
//                       {
                            echo '<option value=' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '>' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '</option>';
//                        }
                    }
                }

			    //echo "</select><input type=hidden name=txtKennung_old value=".$rsSchad['KENNUNG'][$i] ."></td>";			
				echo "</select></td>";
			    unset($rsZahlungDurch);					
                unset($rsZahlung);
                echo "</tr></td><tr><td colspan=2><font size=1>Ab einer Schadenssumme von 250 &euro; ist Ihr GBL generell zu informieren</font></td>";
				echo "</tr>";
//				echo "</table>";
				
//			echo "</td>";
//			echo "<td>";
//				echo "<table border=0>";						
				echo "<tr>";		
				if ($Save_Felder==False and !isset($Pflicht_Felder[1]))
				{
					echo "<td id=FeldBezPflicht>Kennung*</td>";
				}
				else 
				{
					echo "<td id=FeldBez>Kennung*</td>";
				}
                if ($aendern==true)
                {
                	echo "<td><select name=txtKennung colour=red>";
                }
                else
                {
                    echo "<td><select name=txtKennung disabled>";
                }
				//$rsKennung = awisOpenRecordset($con, 'SELECT * FROM KENNUNGEN@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY KENNUNG');
				$rsKennung = awisOpenRecordset($conSchad, 'SELECT * FROM KENNUNGEN WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY KENNUNG');
				$rsKennungZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($KennungZeile=0;$KennungZeile<$rsKennungZeilen;$KennungZeile++)
					{
						if(isset($Pflicht_Felder[1]) AND $Pflicht_Felder[1]==$rsKennung['KENNUNG'][$KennungZeile])
						{
							echo "<option value=" . $rsKennung['KENNUNG'][$KennungZeile] . " selected='selected'>" . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . "</option>";													
						}						
						else
						{
							echo '<option value=' . $rsKennung['KENNUNG'][$KennungZeile] . '>' . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($KennungZeile=0;$KennungZeile<$rsKennungZeilen;$KennungZeile++)
					{
						if ($rsSchad['KENNUNG'][0]==$rsKennung['KENNUNG'][$KennungZeile])	
						{
							echo "<option value=" . $rsKennung['KENNUNG'][$KennungZeile] . " selected='selected'>" . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . "</option>";
						}	
						else 
						{
							echo '<option value=' . $rsKennung['KENNUNG'][$KennungZeile] . '>' . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . '</option>';
						}			
					}										
				}
				
				echo "</select><input type=hidden name=txtKennung_old value=".$rsSchad['KENNUNG'][$i] ."></td>";			
				unset($rsKennung);	
				echo "</tr>";
	
				echo "<tr>";
				echo "<td id=FeldBez>Ursrp. Auftrag</td>";
                if ($aendern==true)
                {
				    echo "<td><select name=txtUrsprAuftrag>";
                }
                else
                {
                    echo "<td><select name=txtUrsprAuftrag disabled>";
                }
				//$rsAuftragsarten = awisOpenRecordset($con, 'SELECT * FROM AUFTRAGSARTEN@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY AUFTRAGSART_ATU');
				$rsAuftragsarten = awisOpenRecordset($conSchad, 'SELECT * FROM AUFTRAGSARTEN WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY AUFTRAGSART_ATU');
				$rsAuftragsartenZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
					
				if($Save_Felder==False)
				{
					for($AuftragsartenZeile=0;$AuftragsartenZeile<$rsAuftragsartenZeilen;$AuftragsartenZeile++)
					{
						if(isset($Pflicht_Felder[17]) AND $Pflicht_Felder[17]==$rsAuftragsarten['ART_ID'][$AuftragsartenZeile])
						{
							echo "<option value=" . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . " selected='selected'>" . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . "</option>";							
						}						
						else
						{
							echo '<option value=' . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . '>' . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($AuftragsartenZeile=0;$AuftragsartenZeile<$rsAuftragsartenZeilen;$AuftragsartenZeile++)
					{
						if ($rsSchad['AUFTRAGSART_ATU_NEU'][0]==$rsAuftragsarten['ART_ID'][$AuftragsartenZeile])
						{
							echo "<option value=" . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . " selected='selected'>" . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . "</option>";							
						}	
						else 
						{
							echo '<option value=' . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . '>' . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . '</option>';
						}			
					}										
				}
					
				echo "</select><input type=hidden name=txtUrsprAuftrag_old value=".$rsSchad['AUFTRAGSART_ATU_NEU'][$i] ."></td>";
				unset($rsAuftragsarten);
				echo "</tr>";
				
				echo "<tr>";
				echo "<td id=FeldBez>Woran gearbeitet</td>";
                if ($aendern==true)
                {
                    echo "<td><select name=txtWoranGearbeitet>";    
                }
                else
                {
                    echo "<td><select name=txtWoranGearbeitet disabled>";    
                }
				
				//$rsSchadensgrund = awisOpenRecordset($con, 'SELECT * FROM SCHADENSGRUND@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY GRUND');
				$rsSchadensgrund = awisOpenRecordset($conSchad, 'SELECT * FROM SCHADENSGRUND WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY GRUND');
				$rsSchadensgrundZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
					
				if($Save_Felder==False)
				{
					for($SchadensgrundZeile=0;$SchadensgrundZeile<$rsSchadensgrundZeilen;$SchadensgrundZeile++)
					{
						if(isset($Pflicht_Felder[18]) AND $Pflicht_Felder[18]==$rsSchadensgrund['ID'][$SchadensgrundZeile])
						{
							echo "<option value=" . $rsSchadensgrund['ID'][$SchadensgrundZeile] . " selected='selected'>" . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsSchadensgrund['ID'][$SchadensgrundZeile] . '>' . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($SchadensgrundZeile=0;$SchadensgrundZeile<$rsSchadensgrundZeilen;$SchadensgrundZeile++)
					{
						if ($rsSchad['SCHADENSGRUND'][0]==$rsSchadensgrund['ID'][$SchadensgrundZeile])
						{
							echo "<option value=" . $rsSchadensgrund['ID'][$SchadensgrundZeile] . " selected='selected'>" . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . "</option>";
						}
						else
						{
							echo '<option value=' . $rsSchadensgrund['ID'][$SchadensgrundZeile] . '>' . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . '</option>';
						}					}										
				}	

				echo "</select><input type=hidden name=txtWoranGearbeitet_old value=".$rsSchad['SCHADENSGRUND'][$i] ."></td>";
				unset($rsSchadensgrund);
				echo "</tr>";
				
				echo "<tr>";
				echo "<td id=FeldBez>Was besch�digt</td>";
                if ($aendern==true)
                {                
				    echo "<td><select name=txtWasBeschaedigt>";
                }
                else
                {
                    echo "<td><select name=txtWasBeschaedigt disabled>";
                }
				//$rsBeschaedigt = awisOpenRecordset($con, 'SELECT * FROM BESCHAEDIGT@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY SCHLAGWORT');
				$rsBeschaedigt = awisOpenRecordset($conSchad, 'SELECT * FROM BESCHAEDIGT WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY SCHLAGWORT');
				$rsBeschaedigtZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($BeschaedigtZeile=0;$BeschaedigtZeile<$rsBeschaedigtZeilen;$BeschaedigtZeile++)
					{
						if(isset($Pflicht_Felder[19]) AND $Pflicht_Felder[19]==$rsBeschaedigt['BID'][$BeschaedigtZeile])
						{
							echo "<option value=" . $rsBeschaedigt['BID'][$BeschaedigtZeile] . " selected='selected'>" . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . "</option>";
						}						
						else
						{						
							echo '<option value=' . $rsBeschaedigt['BID'][$BeschaedigtZeile] . '>' . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($BeschaedigtZeile=0;$BeschaedigtZeile<$rsBeschaedigtZeilen;$BeschaedigtZeile++)
					{
						if ($rsSchad['BID'][0]==$rsBeschaedigt['BID'][$BeschaedigtZeile])
						{
							echo "<option value=" . $rsBeschaedigt['BID'][$BeschaedigtZeile] . " selected='selected'>" . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . "</option>";
						}
						else
						{
							echo '<option value=' . $rsBeschaedigt['BID'][$BeschaedigtZeile] . '>' . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . '</option>';
						}
					}
				}	
						
				echo "</select><input type=hidden name=txtWasBeschaedigt_old value=".$rsSchad['BID'][$i] ."></td>";
				unset($rsBeschaedigt);
				echo "</tr>";
				
				//Wird nur in bestimmten F�llen angezeigt
				if ((isset($Pflicht_Felder[6]) or isset($Pflicht_Felder[7]) or isset($Pflicht_Felder[8]) or 
					isset($Pflicht_Felder[9]) or isset($Pflicht_Felder[10])) or (isset($Pflicht_Felder[35]) and 
					($Pflicht_Felder[36]=='38' or $Pflicht_Felder[36]=='51' or $Pflicht_Felder[36]=='53' 
					or $Pflicht_Felder[36]=='59' or $Pflicht_Felder[36]=='12')) 
					or ($rsSchad['AUSFALLURSACHE'][$i]!='' AND $Save_Felder!=False))
				{
					echo "<tr>";
					if ($Save_Felder==False and $Pflicht_Felder[35]=='')
					{
						echo "<td id=FeldBezPflicht>Ausfallursache</td>";
						echo "<input type=hidden name=txtAusfallursachePflicht value=1>";
					}
					else 
					{
						echo "<td id=FeldBez>Ausfallursache</td>";
						echo "<input type=hidden name=txtAusfallursachePflicht value=1>";
					}
					
                    if ($aendern==true)
                    {               
					    echo "<td><select name=txtAusfallursache>";
                    }
                    else
                    {
                        echo "<td><select name=txtAusfallursache disabled>";
                    }
                    
						if (($Pflicht_Felder[6]=='38' AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='') or ($Pflicht_Felder[36]=='38') or ($rsSchad['SCHADENSGRUND'][$i]=='38'))
						{
							//$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE@SCHAD.ATU.DE WHERE BREMSE=1 ORDER BY WERT";
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE WHERE BREMSE=1 ORDER BY WERT";
						}
						elseif (($Pflicht_Felder[7]=='51' AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='') or ($Pflicht_Felder[36]=='51') or ($rsSchad['SCHADENSGRUND'][$i]=='51'))
						{
							//$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE@SCHAD.ATU.DE WHERE MOTOR=1 ORDER BY WERT";
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE WHERE MOTOR=1 ORDER BY WERT";
						}
						elseif (($Pflicht_Felder[8]=='53' AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='') or ($Pflicht_Felder[36]=='53') or ($rsSchad['SCHADENSGRUND'][$i]=='53'))
						{
							//$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE@SCHAD.ATU.DE WHERE REIFEN=1 ORDER BY WERT";
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE WHERE REIFEN=1 ORDER BY WERT";
						}
						elseif (($Pflicht_Felder[9]=='59' AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[10]=='') or ($Pflicht_Felder[36]=='59') or ($rsSchad['SCHADENSGRUND'][$i]=='59'))
						{
							//$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE@SCHAD.ATU.DE WHERE ZAHNRIEMEN=1 ORDER BY WERT";
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE WHERE ZAHNRIEMEN=1 ORDER BY WERT";
						}
						else
						{
							//$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE@SCHAD.ATU.DE WHERE OELWECHSEL=1 ORDER BY WERT";
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE WHERE OELWECHSEL=1 ORDER BY WERT";
						}
						//awis_Debug(1,$SQLAusfallursache);
						$rsAusfallursache = awisOpenRecordset($conSchad, $SQLAusfallursache);
						$rsAusfallursacheZeilen = $awisRSZeilen;
						
						echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";												
						
						if ($Save_Felder==False)
						{						
							for($AusfallursacheZeile=0;$AusfallursacheZeile<$rsAusfallursacheZeilen;$AusfallursacheZeile++)
							{
								if (isset($Pflicht_Felder[35]) AND $Pflicht_Felder[35] == $rsAusfallursache['CODE'][$AusfallursacheZeile])
								{
									echo "<option value=" . $rsAusfallursache['CODE'][$AusfallursacheZeile] . " selected='selected'>" . $rsAusfallursache['WERT'][$AusfallursacheZeile] . "</option>";
								}
								else 
								{
									echo '<option value=' . $rsAusfallursache['CODE'][$AusfallursacheZeile] . '>' . $rsAusfallursache['WERT'][$AusfallursacheZeile] . '</option>';						
								}
							}
						}
						else 
						{
							for($AusfallursacheZeile=0;$AusfallursacheZeile<$rsAusfallursacheZeilen;$AusfallursacheZeile++)
							{
								//awis_Debug(1,$rsSchad['AUSFALLURSACHE'][$i]);
								//awis_Debug(1,$rsAusfallursache['CODE'][$rsAusfallursacheZeile]);
								if ($rsSchad['AUSFALLURSACHE'][0] == $rsAusfallursache['CODE'][$AusfallursacheZeile])
								{
									echo "<option value=" . $rsAusfallursache['CODE'][$AusfallursacheZeile] . " selected='selected'>" . $rsAusfallursache['WERT'][$AusfallursacheZeile] . "</option>";
								}
								else 
								{
									echo '<option value=' . $rsAusfallursache['CODE'][$AusfallursacheZeile] . '>' . $rsAusfallursache['WERT'][$AusfallursacheZeile] . '</option>';						
								}
							}
						}

						echo "</select><input type=hidden name=txtAusfallursache_old value=".$rsSchad['AUSFALLURSACHE'][$i] ."></td>";
						unset($rsAusfallursache);
					echo "</tr>";	
				}
				
				
					
				
//				echo "</table>";
				
//			echo "</td>";
			echo "</table>";
			
			//$conSchad=awisLogonSchad();
			
			echo "<br><b>Diagnose / Ergebnis und besprochene Ma�nahme (max. 4000 Zeichen)*</b>";
			echo "<br><b>Wie ist der aktuelle Bearbeitungsstand?</b>";
			echo "<br><b>Was wurde mit dem Kunden vereinbart?</b>";
			echo "<br><b>Welche Ma�nahmen wurden gegen�ber Kunde getroffen?</b>";
						
			//$SQL = "SELECT B.BEARBNRNEU, substr(B.BEMERKUNGEN,1,2000) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
			//$SQL = "SELECT B.BEARBNRNEU, B.BEMERKUNGEN as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
			//$SQL = "SELECT B.BEARBNRNEU, substr(to_char(B.BEMERKUNGEN),1,500) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
			$SQL = "SELECT B.BEARBNRNEU, to_clob(B.BEMERKUNGEN) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW B";
			//$SQL = "SELECT B.BEARBNRNEU, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
			//$SQL = "SELECT B.BEARBNRNEU, B.DATUM, B.BEMERKUNGEN FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
			$SQL .= " WHERE BEARBNRNEU=".intval($rsSchad['BEARBNRNEU'][0]);
			$SQL .= " ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
			
			//$SQL .= "WHERE BEARBNRNEU=".$rsSchad['BEARBNRNEU'][0] . " ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
			//$SQL .= "WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='". $BEARB_ID ."') ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
		
			//var_dump($SQL);
			awis_Debug(1, $SQL);
			
			//$DBServer = awisDBServer();
			//$con = awisAdminLogon($DBServer, 'schaddev09', 'schad');
			//die();
			//$rsSchadInfo = awisOpenRecordset($conSchad,$SQL,true,true);
			$rsSchadInfo = awisOpenRecordset($conSchad,$SQL,true,true);
			$rsSchadInfoZeilen = $awisRSZeilen;						
			//die();
			
			//if ($Save_Felder==False and $Pflicht_Felder[0]=='')
			//{
			//	echo "<span class=HinweisText>Diagnose / Ergebnis und besprochene Ma�nahme (max. 500 Zeichen)*</span>";
			//}
			//else
			//{
				//echo "Diagnose / Ergebnis und besprochene Ma�nahme (max. 4000 Zeichen)*";				
			//}								
			
			if ($rsSchadInfoZeilen==0)
			{
				echo "<table border=0>";
				echo '<tr>';				
				if($Save_Felder==False and isset($Pflicht_Felder[0]) and $Pflicht_Felder[0]!='')
				{
					echo "<td><textarea name=txtErg_Text rows=4 cols=150>".$Pflicht_Felder[0]."</textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				else 
				{
					if ($aendern==true)
					{
						echo "<td><textarea name=txtErg_Text  rows=4 cols=150></textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
					}
				}
				
				echo '</tr>';
				echo "</table>";
			}
			else
			{
				echo "<table border=0>";
				
				echo '<colgroup>';
			    echo '<col width="100">';
			    echo '<col width="80">';
			    echo '<col width="400">';
			    echo '<col width="100">';
			    echo '<col width="100">';			    
			  	echo '</colgroup>';
			  	
				echo '<tr>';
				echo "<td id=FeldBez width=100>Datum</td>";
				echo "<td id=FeldBez width=80>Eintrag</td>";			
				echo "<td id=FeldBez width=400>Diagnose / Ergebnis</td>";
				echo "<td id=FeldBez width=100>Betrag</td>";			
				echo "<td id=FeldBez width=100>Zahlungsart</td>";							
				echo '</tr>';
				
				for($ii=0;$ii<$rsSchadInfoZeilen;$ii++)
				{
					echo '<tr>';
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">". $rsSchadInfo['DATUM'][$ii] ."</td>";
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchadInfo['EINTRAG'][$ii] . "</td>";
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . str_replace('\"','"',$rsSchadInfo['BEMERKUNG'][$ii]) . "</td>";
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" .awis_format($rsSchadInfo["BETRAG"][$ii],"Currency")."</td>";			
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchadInfo['ZAHLUNGSART'][$ii] . "</td>";					
					echo '</tr>';	
				}
				echo "</table>";
				
				echo "<table border=0>";
				echo "<tr>";
				if ($aendern==true)
				{
					if ($Save_Felder==False and !isset($Pflicht_Felder[0]))
					{
						echo "<td id=FeldBezPflicht>Neuer Eintrag*</td>";
					}
					else 
					{
						echo "<td id=FeldBez>Neuer Eintrag*</td>";
					}
				}
				echo "</tr>";
				echo "<tr>";				
				if($Save_Felder==False and isset($Pflicht_Felder[0]))
				{
					echo "<td><textarea name=txtErg_Text rows=4 cols=120>".$Pflicht_Felder[0]."</textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				else 
				{
					if ($aendern==true)
					{
						echo "<td><textarea name=txtErg_Text  rows=4 cols=120></textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
					}
				}
				
				
				echo "</tr>";
				
				echo "</table>";
				
			}
	
			echo "<table>";
			echo "<tr>";			
			if (($Save_Felder==False and !isset($Pflicht_Felder[5])) or (isset($Pflicht_Felder[5]) and !isset($Pflicht_Felder[37]) and !isset($Pflicht_Felder[38])))
			{
				echo '<td id=FeldBezPflicht>Eingabe / Bearbeitet durch*</td>';
			}
			else 
			{
				echo '<td id=FeldBez>Eingabe / Bearbeitet durch*</td>';
			}
			if($Save_Felder==False)// and isset ($Pflicht_Felder[5]))
			{
				if (isset($Pflicht_Felder[5])) //and $Pflicht_Felder[5]!='Name eingeben')
				//if ($Pflicht_Felder[5]!='Name eingeben')// and $Pflicht_Felder[5]!='')
				{
					echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='".$Pflicht_Felder[5]."'>";
					
					if (isset($Pflicht_Felder[37]) || isset($Pflicht_Felder[38])) 
					{
						echo "<td><input type=text name=txtEingabedurchName size=50 value='".$Pflicht_Felder[37].' '.$Pflicht_Felder[38]. "' readonly>";						
					}
					
					//if (isset($Pflicht_Felder[38]))
					//{
					//	echo "<td><input type=text name=txtEingabedurchVorname size=20 value='".$Pflicht_Felder[38]."' readonly>";						
					//}
					
				}
				else 
				{									
					echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='Personalnummer eingeben' onfocus=this.value=''>";					
				}
			}
			else 
			{
				if ($aendern==true)
				{
					echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='Personalnummer eingeben' onfocus=this.value=''>";						
				}
				else 
				{
					echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 disabled>";
				}
			}
				//if ($aendern==true)
				//{
					//if ($rsSchad['EINGABEDURCH_FILPERSNR'][$i]!='')
					//{
					//	echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='".$rsSchad['EINGABEDURCH_FILPERSNR'][$i]."'>";
					//}
					//else 
					//{
					//	echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='Personalnummer eingeben' onfocus=this.value=''>";						
					//}
					
					//if ($rsSchad['EINGABEDURCH_FIL'][$i]!='')
					//{
					//	echo "<td><input type=text name=txtEingabedurchName size=50 value='".$rsSchad['EINGABEDURCH_FIL'][$i]."' readonly>";
					//}
					//else 
					//{
					//	echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=25 value='Personalnummer eingeben' onfocus=this.value=''>";
					//}
					
					//echo "&nbsp;<input type=text title='Bitte Namen eingeben' name=txtEingabedurch size=20 value='".$rsSchad['EINGABEDURCH_FIL'][$i]."'>";
					//echo "&nbsp;<input type=text title='Bitte Namen eingeben' name=txtEingabedurch size=20 value='".$rsSchad['EINGABEDURCH_FIL'][$i]."'>";
				//}
				//else 
				//{
				//	echo "<td><input type=text title='Bitte Personalnummer eingeben' name=txtEingabedurch size=20 value='".$rsSchad['EINGABEDURCH_FILPERSNR'][$i]."' disabled>";
				//	echo "<td><input type=text name=txtEingabedurchName size=20 value='".$rsSchad['EINGABEDURCH_FIL'][$i]."' disabled>";
				//}
			//}
			echo "<input type=hidden name=txtEingabedurch_old value='".$rsSchad['EINGABEDURCH_FIL'][$i]."'>";
			echo "</td></tr>";
			
			echo "<tr>";
			if ($Save_Felder==False and !isset($Pflicht_Felder[2]))
			{
				echo "<td id=FeldBezPflicht>Bearbeitungsstand*</td>";
			}
			else 
			{
				echo "<td id=FeldBez>Bearbeitungsstand*</td>";
			}
            if ($aendern==true)
            {
			    echo "<td><select name=txtBearbStand>";
            }
            else
            {
                echo "<td><select name=txtBearbStand disabled>";
            }
						
				//$rsStand = awisOpenRecordset($con, 'SELECT * FROM STAND@SCHAD.ATU.DE WHERE STANDID IN (14,15,19,22) ORDER BY BESCHREIBUNG');
				$rsStand = awisOpenRecordset($conSchad, 'SELECT * FROM STAND WHERE STANDID IN (14,15,19,22) ORDER BY BESCHREIBUNG');
				$rsStandZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";

				if($Save_Felder==False)
				{
					for($StandZeile=0;$StandZeile<$rsStandZeilen;$StandZeile++)
					{
						if(isset($Pflicht_Felder[2]) AND $Pflicht_Felder[2]==$rsStand['STANDID'][$StandZeile])
						{
							echo "<option value=" . $rsStand['STANDID'][$StandZeile] . " selected='selected'>" . $rsStand['BESCHREIBUNG'][$StandZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsStand['STANDID'][$StandZeile] . '>' . $rsStand['BESCHREIBUNG'][$StandZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($StandZeile=0;$StandZeile<$rsStandZeilen;$StandZeile++)
					{
						if ($rsSchad['STAND'][0]==$rsStand['STANDID'][$StandZeile])
						{
							echo "<option value=" . $rsStand['STANDID'][$StandZeile] . " selected='selected'>" . $rsStand['BESCHREIBUNG'][$StandZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsStand['STANDID'][$StandZeile] . '>' . $rsStand['BESCHREIBUNG'][$StandZeile] . '</option>';
						}
					}				
					
				}
				echo "</select></td><input type=hidden name=txtBearbStand_old value=".$rsSchad['STAND'][$i] .">&nbsp;&nbsp;</td>";
				unset($rsStand);				
			
			echo "</tr><tr><td><FONT SIZE=1>(*) = Pflichtfelder</FONT></tr></td>";
			echo "</table>";
			
		}
		
		if ($aendern==true)
		{			
			echo "<br><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";			
		}		
		}
	}
			
	echo "</form>";
	
	if (($Rechtestufe&64)==64 AND $rsSchadZeilen > 0)
	{
		echo "<form name=frmSchadenListe method=post action=./schaden_pdf_neu.php>";
		echo "<input type=image accesskey=P title='PDF erstellen (Alt+P)' src=/bilder/pdf_gross.png name=cmdProtokoll>";			
		echo "<input type=hidden name=BEARB_NR value='".$BEARB_ID."'>";
		echo "</form>";			
	}
	
	if ($ART=='S' && ($Rechtestufe&16)==16)
	{
		echo "<form name=frmSchadenListe method=post action=./schaden_pdf.php>";
		echo "<input type=image align=right accesskey=P title='PDF erstellen (Alt+P)' src=/bilder/pdf_gross.png name=cmdProtokoll>";
		echo "<input type=hidden name=BEARB_NR value='".$BEARB_ID."'>";
		echo "</form>";
	}
}
elseif (isset($_REQUEST['txtFilialNrOffene']) || (isset($_POST['txtFilialNrOffene']) && $_POST['txtFilialNrOffene']!='')) 
//elseif (isset($_POST['txtFilialNrOffene']) && $_POST['txtFilialNrOffene']!='') 
{
	if ($_POST['txtFilialNrOffene']=='')
	{
		//echo 'Es wurde keine Filialnummer angegeben.';
		echo "<br>";
		die("<center><span class=HinweisText>Es wurde kein Suchkriterium angegeben!</span></center>");		
	}
	
	if(isset($_POST['cmdSuche_x']))			
	{		
		$Params[0] = $_POST['BEARB_ID'];
		$Params[1] = $_POST['txtFilialNrOffene'];						
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Suche-Filiale", $AWISBenutzer->BenutzerName(), implode(';',$Params));
	}
	else
	{
		$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Suche-Filiale", $AWISBenutzer->BenutzerName()));
	}
	
	awis_ZeitMessung(0);
	
	$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
			
	$SQL = "SELECT BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT";			
	//$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE";			
	$SQL .= " FROM SCHAEDEN_NEW";			
	$SQL .= " WHERE (AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";						
	
	$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
	$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);				
		
	
	/*SQL-Zusatz Bearbeitungsnummer*/
	if($Params[0]!='')
	{
			$SQL.=" AND BEARBEITUNGSNR " . awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0);
	}
	
	/*SQL-Zusatz Filial-Nummer*/
	if($Params[1]!='')
	{
		//Wenn es sich um einen Filial-User handelt
		if (($UserFilialen!='' AND $UserFilialen!='0') or ($Fil_User=='fil-'))
		{			
			if ($UserFilialen!='' AND $UserFilialen!='0')
			{
				if(intval($Params[1])!=intval($UserFilialen))
				{
					$SQL.= " AND FILNR='".intval($UserFilialen)."'";
					//echo "Andere Filiale 1";
				}				
			}
			else 
			{
				if(intval($Params[1])!=intval($Fil_Nr))
				{
					//echo "Andere Filiale 2";
					$SQL.= " AND FILNR='".intval($Fil_Nr)."'";
				}				
			}
	
			//$SQL.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";				
			$SQL.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";				
		}	
	
		$SQL.=" AND FILNR " . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
	}		
	
	$SQL .= "AND ROWNUM<=" . $AwisBenuPara;
	
	$SQL .= " ORDER BY FILNR ASC, BEARBEITUNGSNR DESC";

	awis_Debug(1,$SQL);

	$rsResult = awisOpenRecordset($conSchad,$SQL);
	$rsResultZeilen = $awisRSZeilen;

	if($rsResultZeilen==0)		// Keine Daten
	{
		//echo 'Es wurde kein Eintrag gefunden.';
		echo "<br>";
		die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
	}
	else
	{
		// �berschrift aufbauen
		echo "<table  width=100% id=DatenTabelle border=1><tr>";											
		echo "<td id=FeldBez>Bearbeitungs-Nr</td>";
		echo "<td id=FeldBez>Filial-Nr</td>";
		echo "<td id=FeldBez>Kunde</td>";
		echo "<td id=FeldBez>Strasse</td>";
		echo "<td id=FeldBez>PLZ</td>";
		echo "<td id=FeldBez>Ort</td>";				
		echo "</tr>";
	
		for($i=0;$i<$rsResultZeilen;$i++)
		{
			echo '<tr>';					
			echo '<td id=TabellenZeileGrau><a href=./schaden_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsResult['BEARBEITUNGSNR'][$i] . '&ART=S>'. $rsResult['BEARBEITUNGSNR'][$i] . '</a></td>';
			//echo '<td id=TabellenZeileGrau><a href=./schaden_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsResult['BEARBEITUNGSNR'][$i] . '&ART=' . $rsResult['ART'][$i] . '>' . $rsResult['BEARBEITUNGSNR'][$i] . '</a></td>';
			echo '<td id=TabellenZeileGrau><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsResult['FILNR'][$i] . '>' . $rsResult['FILNR'][$i] .  '</a></td>';
			echo '<td id=TabellenZeileGrau>' . $rsResult['NAME'][$i] . '</td>';
			echo '<td id=TabellenZeileGrau>' . $rsResult['STRASSE'][$i] . '</td>';
			echo '<td id=TabellenZeileGrau>' . $rsResult['PLZ'][$i] . '</td>';
			echo '<td id=TabellenZeileGrau>' . $rsResult['ORT'][$i] . '</td>';
			echo '</tr>';
		}
		
		print "</table>";
	
		if($i<$AwisBenuPara)
		{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden!';
		}
		else
		{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
		}
	}
}
	
else  //Zweig f�r Listen-Aufbau nach Suche
{
		if(isset($_POST['cmdSuche_x']))			
		{
			$Params[0] = (isset($_POST['txtAuftragsart'])?$_POST['txtAuftragsart']:'');
			$Params[1] = (isset($_POST['txtBearbeitungsnummer'])?$_POST['txtBearbeitungsnummer']:'');
			$Params[2] = (isset($_POST['txtFilialNr'])?$_POST['txtFilialNr']:'');
			$Params[3] = (isset($_POST['txtKundenname'])?$_POST['txtKundenname']:'');
			$Params[4] = (isset($_POST['txtStrasse'])?$_POST['txtStrasse']:'');
			$Params[5] = (isset($_POST['txtPLZ'])?$_POST['txtPLZ']:'');
			$Params[6] = (isset($_POST['txtOrt'])?$_POST['txtOrt']:'');
			$Params[7] = (isset($_POST['txtTelefon'])?$_POST['txtTelefon']:'');
			$Params[8] = (isset($_POST['txtEMail'])?$_POST['txtEMail']:'');
			$Params[9] = (isset($_POST['txtPAN'])?$_POST['txtPAN']:'');
			$Params[10] = (isset($_POST['txtSachbearbeiter'])?$_POST['txtSachbearbeiter']:'');
			$Params[11] = (isset($_POST['txtJahr'])?$_POST['txtJahr']:'');
			$Params[12] = (isset($_POST['txtStatus'])?$_POST['txtStatus']:'');
			
			
			awis_BenutzerParameterSpeichern($con, "Schaden-Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
		}
		else
		{
				$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Suche", $AWISBenutzer->BenutzerName()));
		}
		
		awis_ZeitMessung(0);
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

		if ($Params[0]=='S')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER,";
			$SQL .= " DECODE(ANTRAGART, 1, 'Kulanzantrag', 2, 'Verbindliche Vereinbarung', 3, 'Kundenbeschwerde', 4, 'Rechtsanwalt/Gerichtssache', 5, 'Versicherung', 6, 'Kundenware plus Folgekosten', 7, 'Beh�rdliche Beanstandung', 8, 'Lagerware plus Folgekosten', 9, 'Offen', 10, 'CSI Handlungsbedarfsbericht', '') as ANTRAGSART";
			//$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
			$SQL .= " FROM SCHAEDEN_NEW LEFT JOIN SACHBEARBEITER ON (BEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND (TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='(TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Jahr*/
			if($Zusatz)
			{
					if($Params[11]!='')
					{
							$Bedingung.=' AND SCHADENSJAHR ' . awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[11]!='')
					{
							$Bedingung='SCHADENSJAHR ' . awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Status*/
			if($Zusatz)
			{
					if($Params[12]=='O')
					{
							$Bedingung .= " AND (AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";													
							$Zusatz=TRUE;
					}
					elseif($Params[12]=='G')
					{
							$Bedingung .= " AND AKTEGESCHLOSSENAM is not null";													
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[12]=='O')
					{
							$Bedingung .= "(AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";																				
							$Zusatz=TRUE;
					}
					elseif($Params[12]=='G')
					{
							$Bedingung .= "AKTEGESCHLOSSENAM is not null";													
							$Zusatz=TRUE;
					}
			}
			
		}
		elseif ($Params[0]=='B')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " TELEFON AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
			//$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
			$SQL .= " FROM BESCHWERDE LEFT JOIN SACHBEARBEITER ON (SACHBEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		else
		{
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		
		
		if($Bedingung!='')
		{
			if ($Params[0]!='A')
			{
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
			else 
			{
				$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				//$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
				$SQL .= " FROM SCHAEDEN_NEW LEFT JOIN SACHBEARBEITER ON (BEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
				$SQL .= ' UNION';
				$SQL .= " SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " TELEFON AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				//$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
				$SQL .= " FROM BESCHWERDE LEFT JOIN SACHBEARBEITER ON (SACHBEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
		}
		else
		{
				//$SQL .= ' WHERE ROWNUM<=' . $AwisBenuPara;
				echo "<br>";
				die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		
		$SQL .= ' ORDER BY FILNR ASC, BEARBEITUNGSNR DESC';
		
		awis_Debug(1,$SQL);
		
		$rsResult = awisOpenRecordset($conSchad,$SQL);
		$rsResultZeilen = $awisRSZeilen;
		
		if($rsResultZeilen==0)		// Keine Daten
		{
			//echo 'Es wurde kein Eintrag gefunden.';
			echo "<br>";
			die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
		
			if(($Rechtestufe&16)==16)
			{
				echo "<td id=FeldBez></td>";
			}
			
			//echo "<td id=FeldBez>Auftragsart</td>";
			echo "<td id=FeldBez>Bearbeitungs-Nr</td>";
			echo "<td id=FeldBez>Antrag-Art</td>";
			echo "<td id=FeldBez>Filial-Nr</td>";
			echo "<td id=FeldBez>Kunde</td>";
			echo "<td id=FeldBez>Strasse</td>";
			echo "<td id=FeldBez>PLZ</td>";
			echo "<td id=FeldBez>Ort</td>";
			echo "<td id=FeldBez>Telefon</td>";
			echo "<td id=FeldBez>E-Mail</td>";
			echo "<td id=FeldBez>ATU-Card-Nummer</td>";
			//echo "<td id=FeldBez>Sachbearbeiter</td>";
			echo "</tr>";
			
			for($i=0;$i<$rsResultZeilen;$i++)
			{
				echo '<tr>';
				if(($Rechtestufe&16)==16 && $rsResult['ART'][$i]=='S')
				{
					//$PDF_Link='./filialinfo_Infos_lieferkontrolle_pdf.php?LIK_LIEFERSCHEINNR='.$rsLIK['LIK_LIEFERSCHEINNR'][$LIKZeile].'&LIK_FIL_ID='.$FIL_ID; 
					//echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
					
					$PDF_Link='./schaden_pdf.php?BEARB_NR='.$rsResult['BEARBEITUNGSNR'][$i]; 
					//$PDF_Link='./schaden_pdf.php?BEARB_NR=150162'; 
					echo "<td id=TabellenZeileGrau><a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>";
				}
				//else 
				//{
				//	echo "<td>&nbsp;</td>";
				//}
				//echo '<td id=TabellenZeileGrau>' . ($rsResult['ART'][$i]=='S'?'Schaden':'Beschwerde') . '</td>';
				echo '<td id=TabellenZeileGrau><a href=./schaden_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsResult['BEARBEITUNGSNR'][$i] . '&ART=' . $rsResult['ART'][$i] . '>' . $rsResult['BEARBEITUNGSNR'][$i] . '</a></td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['ANTRAGSART'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsResult['FILNR'][$i] . '>' . $rsResult['FILNR'][$i] .  '</a></td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['NAME'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['STRASSE'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PLZ'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['ORT'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['TELNR'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['EMAIL'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PAN'][$i] . '</td>';
				//echo '<td id=TabellenZeileGrau>' . $rsResult['SACHBEARBEITER'][$i] . '</td>';
				echo '</tr>';
			}
				
			print "</table>";
			
			if($i<$AwisBenuPara)
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden!';
			}
			else
			{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
}

function Pruefe_Felder()
{
	global $con;
	global $awisRSZeilen;
	global $awisDBFehler;
	global $Pflicht_Felder;
	//$Pflicht_Felder=array();
	
	$Fehler_Anzahl=0;

	if (! isset($_REQUEST['txtErg_Text']) || ! $_REQUEST['txtErg_Text']!='')
	{
		echo "<center><span class=HinweisText>Es wurde kein Diagnose/Ergebnis-Text angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	elseif (strlen($_REQUEST['txtErg_Text'])>4000)
	{				
		echo("<center><span class=HinweisText>Der Text des Diagnose/Ergebnis-Textes ist l�nger als 4000 Zeichen.</span></center><br>");
		$Fehler_Anzahl++;
		//$Pflicht_Felder[0]=$_REQUEST['txtErg_Text'];
		$Pflicht_Felder[0]=str_replace("\\",'',$_POST['txtErg_Text']);		
	}
	else{
		//$Pflicht_Felder[0]=$_REQUEST['txtErg_Text'];
		$Pflicht_Felder[0]=str_replace("\\",'',$_POST['txtErg_Text']);		
	}
	
	if (! isset($_REQUEST['txtKennung']) || ! $_REQUEST['txtKennung']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde keine Kennung angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[1]=$_REQUEST['txtKennung'];
	}
	
	if (! isset($_REQUEST['txtBearbStand']) || ! $_REQUEST['txtBearbStand']!='0') 
	{
		echo "<center><span class=HinweisText>Es wurde kein Bearbeitungsstand angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[2]=$_REQUEST['txtBearbStand'];
	}
	
	if (! isset($_REQUEST['txtKontaktKunde']) || ! $_REQUEST['txtKontaktKunde']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Kontakt mit Kunde angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[3]=$_REQUEST['txtKontaktKunde'];
	}
	
	if (! isset($_REQUEST['txtInfoGBL']) || ! $_REQUEST['txtInfoGBL']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Info an GBL angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	elseif(isset($_REQUEST['txtInfoGBL']) AND $_REQUEST['txtInfoGBL']!='1' AND intval($_REQUEST['txtZahlungWert'])>=250)
	{
		echo "<center><span class=HinweisText>Ab einer Schadenssumme von 250 &euro; muss der GBL informiert werden!</span></center><br>";
		$Pflicht_Felder[4]=$_REQUEST['txtInfoGBL'];
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[4]=$_REQUEST['txtInfoGBL'];
	}
	
	if (! isset($_REQUEST['txtEingabedurch']) || ! $_REQUEST['txtEingabedurch']!='' || $_REQUEST['txtEingabedurch']=='Personalnummer eingeben')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Eingabe / Bearbeitet durch angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else
	{
		
		$SQL = 'SELECT PER_NACHNAME, PER_VORNAME, PER_FIL_ID, PER_NR FROM PERSONAL WHERE (PER_AUSTRITT > SYSDATE OR PER_AUSTRITT IS NULL) AND PER_NR = \''.$_REQUEST['txtEingabedurch'].'\'';
		$rsPER = awisOpenRecordset($con, $SQL);
		$rsPERZeilen = $awisRSZeilen;
		
		if ($rsPERZeilen == 0)
		{
			$SQL = 'SELECT SAW_PER_NR, SAW_NAME, SAW_VORNAME, SAW_FIL_ID FROM SONDERAUSWEISE WHERE (SAW_AUSTRITT > SYSDATE OR SAW_AUSTRITT IS NULL) AND SAW_PER_NR < 900000 AND SAW_SAG_KEY = 1 AND SAW_PER_NR = \''.$_REQUEST['txtEingabedurch'].'\'';
			$rsSAW = awisOpenRecordset($con, $SQL);
			$rsSAWZeilen = $awisRSZeilen;
			
			if ($rsSAWZeilen == 0)
			{
				echo "<center><span class=HinweisText>Unter der angegebenen Personalnummer wurde kein Mitarbeiter gefunden!</span></center><br>";
				//echo "<span class=HinweisText>Mit den angegebenen Parametern konnte kein Personal gefunden werden</span>";
				$Fehler_Anzahl++;
				//echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./gewinnspiel_Main.php';>";
			}
			
			else
			{
				$Pflicht_Felder[37]=$rsSAW['SAW_NAME'][0];
				$Pflicht_Felder[38]=$rsSAW['SAW_VORNAME'][0];
				$Pflicht_Felder[39]=$rsSAW['SAW_FIL_ID'][0];
				$Pflicht_Felder[40]=$rsSAW['SAW_PER_NR'][0];
				//$NACHNAME = $rsSAW['SAW_NAME'][0];
				//$VORNAME = $rsSAW['SAW_VORNAME'][0];
				//$FIL_ID = $rsSAW['SAW_FIL_ID'][0];
				//$PER_NR = $rsSAW['SAW_PER_NR'][0];
			}
		}
		
		else 
		{
			$Pflicht_Felder[37]=$rsPER['PER_NACHNAME'][0];
			$Pflicht_Felder[38]=$rsPER['PER_VORNAME'][0];
			$Pflicht_Felder[39]=$rsPER['PER_FIL_ID'][0];
			$Pflicht_Felder[40]=$rsPER['PER_NR'][0];				
			//$NACHNAME = $rsPER['PER_NACHNAME'][0];
			//$VORNAME = $rsPER['PER_VORNAME'][0];
			//$FIL_ID = $rsPER['PER_FIL_ID'][0];
			//$PER_NR = $rsPER['PER_NR'][0];
		}	
		
		$Pflicht_Felder[5]=$_REQUEST['txtEingabedurch'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '38') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!='0') AND (! isset($_REQUEST['txtAusfallursachePflicht']) or $_REQUEST['txtAusfallursachePflicht']!='1'))
	{
		echo "<center><span class=HinweisText>Wenn m�glich, bitte noch eine Ausfallursache (Bremse) angegeben!<br>Anschlie�end nochmal abspeichern, auch wenn keine Ausfallursache ausgew�hlt wurde!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[6]=$_REQUEST['txtWoranGearbeitet'];
	}
	else 
	{
		if(isset($_REQUEST['txtAusfallursache']))
		{
			$Pflicht_Felder[35]=$_REQUEST['txtAusfallursache'];
		}
		$Pflicht_Felder[36]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '51') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!='0') AND (! isset($_REQUEST['txtAusfallursachePflicht']) or $_REQUEST['txtAusfallursachePflicht']!='1'))
	{
		echo "<center><span class=HinweisText>Wenn m�glich, bitte noch eine Ausfallursache (Motor) angegeben!<br>Anschlie�end nochmal abspeichern, auch wenn keine Ausfallursache ausgew�hlt wurde!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[7]=$_REQUEST['txtWoranGearbeitet'];
	}
	else 
	{
		if(isset($_REQUEST['txtAusfallursache']))
		{
			$Pflicht_Felder[35]=$_REQUEST['txtAusfallursache'];
		}
		$Pflicht_Felder[36]=$_REQUEST['txtWoranGearbeitet'];
	}

	if (($_REQUEST['txtWoranGearbeitet'] == '53') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!='0') AND (! isset($_REQUEST['txtAusfallursachePflicht']) or $_REQUEST['txtAusfallursachePflicht']!='1'))
	{
		echo "<center><span class=HinweisText>Wenn m�glich, bitte noch eine Ausfallursache (R�der/Reifen) angegeben!<br>Anschlie�end nochmal abspeichern, auch wenn keine Ausfallursache ausgew�hlt wurde!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[8]=$_REQUEST['txtWoranGearbeitet'];
	}
	else 
	{
		if(isset($_REQUEST['txtAusfallursache']))
		{
			$Pflicht_Felder[35]=$_REQUEST['txtAusfallursache'];
		}
		$Pflicht_Felder[36]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '59') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!='0') AND (! isset($_REQUEST['txtAusfallursachePflicht']) or $_REQUEST['txtAusfallursachePflicht']!='1'))
	{
		echo "<center><span class=HinweisText>Wenn m�glich, bitte noch eine Ausfallursache (Zahnriemen) angegeben!<br>Anschlie�end nochmal abspeichern, auch wenn keine Ausfallursache ausgew�hlt wurde!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[9]=$_REQUEST['txtWoranGearbeitet'];
	}
	else 
	{
		if(isset($_REQUEST['txtAusfallursache']))
		{
			$Pflicht_Felder[35]=$_REQUEST['txtAusfallursache'];
		}
		$Pflicht_Felder[36]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '12') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!='0') AND (! isset($_REQUEST['txtAusfallursachePflicht']) or $_REQUEST['txtAusfallursachePflicht']!='1'))
	{
		echo "<center><span class=HinweisText>Wenn m�glich, bitte noch eine Ausfallursache (�lwechsel) angegeben!<br>Anschlie�end nochmal abspeichern, auch wenn keine Ausfallursache ausgew�hlt wurde!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[10]=$_REQUEST['txtWoranGearbeitet'];
	}
	else 
	{
		if(isset($_REQUEST['txtAusfallursache']))
		{
			$Pflicht_Felder[35]=$_REQUEST['txtAusfallursache'];
		}
		$Pflicht_Felder[36]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if ((! isset($_REQUEST['txtZahlungArt']) || ! $_REQUEST['txtZahlungArt']!='') and $_REQUEST['txtZahlungWert']!='')
	{
		echo "<center><span class=HinweisText>Es wurde keine Zahlungsart angegeben!</span></center><br>";
		$Fehler_Anzahl++;						
		$Pflicht_Felder[12]=$_REQUEST['txtZahlungWert'];		
	}
	else{
		$Pflicht_Felder[11]=$_REQUEST['txtZahlungArt'];
		$Pflicht_Felder[12]=$_REQUEST['txtZahlungWert'];		
	}		 
	
	if (isset($_REQUEST['txtTerminKunde1']) and $_REQUEST['txtTerminKunde1']!='' and $_REQUEST['txtTerminKunde1']!='TT.MM.JJJJ')
	{
		$datumpruefung = checkData($_REQUEST['txtTerminKunde1']);
		if ($datumpruefung == false)
		{
			echo "<center><span class=HinweisText>Es wurde kein korrektes Datum in der Form TT.MM.JJJJ angegeben!</span></center><br>";			
			$Fehler_Anzahl++;						
		}
	}
	
	if (isset($_REQUEST['txtTerminKunde2']) and $_REQUEST['txtTerminKunde2']!='' and $_REQUEST['txtTerminKunde2']!='TT.MM.JJJJ')
	{
		$datumpruefung = checkData($_REQUEST['txtTerminKunde2']);
		if ($datumpruefung == false)
		{
			echo "<center><span class=HinweisText>Es wurde kein korrektes Datum in der Form TT.MM.JJJJ angegeben!</span></center><br>";			
			$Fehler_Anzahl++;						
		}
	}
	
	$Pflicht_Felder[13]=$_REQUEST['txtTerminKunde1'];
	$Pflicht_Felder[14]=$_REQUEST['txtTerminKundeWer1'];
	$Pflicht_Felder[15]=$_REQUEST['txtTerminKunde2'];
	$Pflicht_Felder[16]=$_REQUEST['txtTerminKundeWer2'];
	$Pflicht_Felder[17]=$_REQUEST['txtUrsprAuftrag'];
	$Pflicht_Felder[18]=$_REQUEST['txtWoranGearbeitet'];
	$Pflicht_Felder[19]=$_REQUEST['txtWasBeschaedigt'];
	
	$Pflicht_Felder[20]=$_REQUEST['txtEingang'];
	$Pflicht_Felder[21]=$_REQUEST['txtKFZ_Kennz'];
	$Pflicht_Felder[22]=$_REQUEST['txtWANR'];
	
	if (isset($_REQUEST['txtSachbearbeiter']))
	{
		$Pflicht_Felder[23]=$_REQUEST['txtSachbearbeiter'];
	}
	else 
	{
		$Pflicht_Felder[23]='';
	}
	$Pflicht_Felder[24]=$_REQUEST['txtAnrede'];
	$Pflicht_Felder[25]=$_REQUEST['txtKundenname'];
	$Pflicht_Felder[26]=$_REQUEST['txtKundenvorname'];
	$Pflicht_Felder[27]=$_REQUEST['txtStrasse'];
	$Pflicht_Felder[28]=$_REQUEST['txtPLZ'];
	$Pflicht_Felder[29]=$_REQUEST['txtOrt'];
	$Pflicht_Felder[30]=$_REQUEST['txtTelefon'];
	$Pflicht_Felder[31]=$_REQUEST['txtTelefon2'];
	$Pflicht_Felder[32]=$_REQUEST['txtFax'];
	$Pflicht_Felder[33]=$_REQUEST['txtEMail'];
	$Pflicht_Felder[34]=$_REQUEST['txtPAN'];
	//groskdnr
	//$Pflicht_Felder[41]=$_REQUEST['txtGrosskdnr'];
		
		
		
	if ($Fehler_Anzahl==0)
	{
		$retval=true;
	}else{
		$retval=false;
	}
	
	return $retval;
}

function checkData($date)
{
    if (!isset($date) || $date=="")
    {
        return false;
    }
   
    list($dd,$mm,$yy)=explode(".",$date);
    if ($dd!="" && $mm!="" && $yy!="")
    {
    if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd))
        {
            return checkdate($mm,$dd,$yy);

        }
    }   
    return false;

}


?>
</body>
</html>

