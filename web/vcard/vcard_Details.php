<?php
require_once 'awisVCard.php';
require_once 'awisMailer.inc';
global $AWIS_KEY1;
global $AWIS_KEY2;
global $KON;


if(isset($_POST['cmdSenden_x'])){
    $Mailer = new awisMailer($KON->DB, $KON->AWISBenutzer);
    $Empfaenger = $_POST['txtEmail'];

    if($Mailer->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_LOGIK)===true){

        $SQL = 'select * from activedirectoryexport';

        $rsKon = $KON->DB->RecordSetOeffnen($SQL);
        $VCard = new awisVCard();


        $tmpFileName = '/daten/web/dokumente/'.$KON->AWISBenutzer->BenutzerID(). '.vcf';
        if(file_exists($tmpFileName)){
            unlink($tmpFileName);
        }

        $tmpFile = fopen($tmpFileName,'w');

        while (!$rsKon->EOF()){


            $Str = $VCard->getVCard($rsKon->FeldInhalt('ADE_GIVENNAME'),$rsKon->FeldInhalt('ADE_SURNAME'),'',$rsKon->FeldInhalt('ADE_COMPANY'),$rsKon->FeldInhalt('ADE_DEPARTMENT'),'',$rsKon->FeldInhalt('ADE_TELEPHONENUMBER'),$rsKon->FeldInhalt('ADE_MOBILE'),$rsKon->FeldInhalt('ADE_MAIL'));

            fwrite($tmpFile,$Str);


            $rsKon->DSWeiter();
        }

        fclose($tmpFile);

        $Text = 'Hallo,'.PHP_EOL.' anbei erh�ltst du die aktuellen Kontakte als VCard zum Import. ' . PHP_EOL . 'Dein AWIS-Team';

        $Mailer->Absender('awis@de.atu.eu');
        $Mailer->Text($Text);
        $Mailer->Betreff('ATU Kontakteexport');
        $Mailer->Anhaenge($tmpFileName,'Kontakte.vcf');

        $Mailer->MailInWarteschlange();

        $KON->Form->Hinweistext('Email erfolgreich in die Warteschlange geschrieben.',awisFormular::HINWEISTEXT_OK);
    }else{
        $KON->Form->ZeileStart();
        $KON->Form->Hinweistext('Bitte geben Sie eine Korrekte Emailadresse an.',awisFormular::HINWEISTEXT_FEHLER);
        $KON->Form->ZeileEnde();
    }





}

//********************************************************
// Anzeige Start
//********************************************************
$KON->Form->Formular_Start();
echo '<form name=frmVCard action=./vcard_Main.php?cmdAktion=Details method=POST>';

if(1==0){
    $KON->Form->ZeileStart();
    $KON->Form->Erstelle_TextLabel('Abteilungen: ', 200);
    $SQL = 'select kab_key, kab_abteilung from kontakteabteilungen order by 2 asc';
    $KON->Form->Erstelle_MehrfachSelectFeld('Abteilungen',isset($_POST['txtAbteilungen'])?$_POST['txtAbteilungen']:[],300,true, $SQL, $KON->OptionBitteWaehlen,'','','',null,'','',[],'','AWIS','','',20);
    $KON->Form->ZeileEnde();

    $KON->Form->ZeileStart();
    $KON->Form->Erstelle_TextLabel('Letzte �nderung neuer als: ', 200);
    $KON->Form->Erstelle_TextFeld('Datum',isset($_POST['txtDatum'])?$_POST['txtDatum']:'',30,300,true,'','','','D');
    $KON->Form->ZeileEnde();
}


$KON->Form->ZeileStart();
$KON->Form->Erstelle_TextLabel('VCard senden an: ', 200);
$KON->Form->Erstelle_TextFeld('!Email',isset($_POST['txtEmail'])?$_POST['txtEmail']:'',30,300,true);
$KON->Form->ZeileEnde();


//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$KON->Form->Formular_Ende();

$KON->Form->SchaltflaechenStart();

$KON->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $KON->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

$KON->Form->Schaltflaeche('image', 'cmdSenden', '', '/bilder/cmd_weiter.png', $KON->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');


$KON->Form->SchaltflaechenEnde();

$KON->Form->SchreibeHTMLCode('</form>');

?>