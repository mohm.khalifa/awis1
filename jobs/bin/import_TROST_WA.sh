<?php
require_once('/daten/include/awisZukauf.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
	$Job = new awisJobProtokoll(3, 'Starte TROST - Import.');

	$ZUK = new awisZukauf_TROST(10,false);

        $Job->SchreibeLog(3,1,'Suche offene Bestellungen...');

	if($ZUK->LeseBestellungen($argv[1],'','','')>0)
	{
        	$Job->SchreibeLog(3,1,'Speichere Bestellungen...');
		$ZUK->SpeichereBestellungen();
	}

        $Job->SchreibeLog(1,0,'Ende des Jobs.');
}
catch (Exception $ex)
{
        echo $ex->getMessage().PHP_EOL;
	if(is_object($Job))
	{
	        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
	}
}

?>
