<?php
GLOBAL $JSK;

try {
    $JSK->Form->SchreibeHTMLCode("<form name=frmDetails method=post action=jskonverter_Main.php>");

    $JSK->Form->Formular_Start();

    $Sprachen = explode('|', $JSK->AWISSprachKonserven['JSK']['JSK_PHPJS']);

    if (isset($_POST['txtSprachwahl']) && $_POST['txtSprachwahl'] == "2") {             //Wenn auf PHP gestellt wurde soll danach auch wieder PHP da stehen
        $Sprachen = array_reverse($Sprachen);
    }

    $JSK->Form->ZeileStart();
    $JSK->Form->Erstelle_SelectFeld("Sprachwahl", 0, 70, true, "", "", "", "", "", $Sprachen);
    $JSK->Form->Erstelle_Textarea("PHPJS", (isset($_POST['txtPHPJS']) ? $_POST['txtPHPJS'] : ""), 150, 150, 16, true);
    $JSK->Form->ZeileEnde();
    $JSK->Form->ZeileStart();
    $JSK->Form->SchreibeHTMLCode("<br> ");
    $JSK->Form->ZeileEnde();

    if (isset($_POST["txtSprachwahl"]) && isset($_POST["txtPHPJS"]) && trim($_POST['txtPHPJS']) <> '') {
        $JSK->Form->ZeileStart();
        $JSK->Form->Erstelle_TextLabel("&nbsp;Ausgabe:", 70);
        if (isset($_POST["txtSprachwahl"]) && $_POST["txtSprachwahl"] == "1") {
            $JSK->Form->Erstelle_Textarea("Umgewandelt", $JSK->JSzuPHP($_POST["txtPHPJS"]), 150, 150, 16, true);
        } elseif (isset($_POST["txtSprachwahl"]) && $_POST["txtSprachwahl"] == "2") {
            $JSK->Form->Erstelle_Textarea("Umgewandelt", $JSK->PHPzuJS($_POST["txtPHPJS"]), 150, 150, 16, true);
        }
        $JSK->Form->ZeileEnde();
        $JSK->Form->ZeileStart();
        $JSK->Form->SchreibeHTMLCode('<br>');
        $JSK->Form->ZeileEnde();
    }

    $JSK->Form->Formular_Ende();

    $JSK->Form->SchaltflaechenStart();
    $JSK->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $JSK->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $JSK->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $JSK->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $JSK->Form->SchaltflaechenEnde();

    $JSK->Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($JSK->Form instanceof awisFormular) {
        $JSK->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201701301500");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>