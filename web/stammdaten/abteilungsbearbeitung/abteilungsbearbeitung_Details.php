<?php

global $Funktionen;

$Funktionen->RechteMeldung(0);

$Ansicht = 0;

$Funktionen->AWIS_KEY1 = 0;

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./abteilungsbearbeitung_Main.php?cmdAktion=Details>");
$Funktionen->Form->Formular_Start();

if(isset($_POST['txtAWIS_KEY1'])){
    $Funktionen->AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
} elseif(isset($_GET['KAB_KEY'])){
    $Funktionen->AWIS_KEY1 = $_GET['KAB_KEY'];
} else {
    $Funktionen->AWIS_KEY1 = "0";
}

if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Bearb']) or isset($_GET['Bearb2'])){ //L�schen der Abteilung / Personen-DS
    include('./abteilungsbearbeitung_loeschen.php');
} elseif(isset($_POST['cmdSpeichern_x'])) { //Beim �ndern von Daten
    include('./abteilungsbearbeitung_speichern.php');
}

$SelectFelder = "KAB_KEY, KAB_ABTEILUNG, KAB_BEMERKUNG ";
$Group = " group by ".$SelectFelder;
if (isset($_POST['cmdDSNeu_x'])) { //Erstellung neues DS -> kein RS ben�tigt
    $Funktionen->AWIS_KEY1 = "-1";
} elseif (isset($_GET['KAB_KEY']) or isset($_POST['txtAWIS_KEY1']) or $Funktionen->AWIS_KEY1 <> 0) { //Wenn bereits in Einzel-DS-Ansicht/Nach Speichern
    if(isset($_GET['KAB_KEY'])) {
        $Funktionen->AWIS_KEY1 = $_GET['KAB_KEY'];
    } elseif(isset($_POST['txtAWIS_KEY1'])) {
            $SQL = "Select * from kontakteabteilungen where kab_key = ". $Funktionen->DB->WertSetzen("KAB","Z",$_POST['txtAWIS_KEY1']);
            $rsCheck = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen("KAB"));
            if($rsCheck->AnzahlDatensaetze()){
                $Funktionen->AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
            }
    }

    $JOIN = " left join kontakteabteilungenzuordnungen on KZA_kab_key = KAB_KEY";
    $Order = ' ORDER BY KAB_ABTEILUNG ASC';
    $Bedingung = $Funktionen->BedingungErstellen();
    $SQL = "Select " . $SelectFelder . " from kontakteabteilungen " . $JOIN ." ". $Bedingung ." ". $Group. " ". $Order;
    $rsKAB = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('KAB'));
} else { //aus der Suche/direkt auf Detail-Seite

    if (isset($_POST['cmdSuche_x'])) {
        $Funktionen->Param['KAB_ABTEILUNG']=$_POST['sucKAB_ABTEILUNG'];
        $Funktionen->Param['KON_NAME']=$_POST['sucKON_NAME'];
        $Funktionen->Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
    }
    if (!isset($_GET['SortList'])) {
        $Order = ' ORDER BY KAB_ABTEILUNG ASC';
    } else {
        $Order = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SortList']);
    }

    $JOIN = " left join ( select * from kontakteabteilungenzuordnungen left join kontakte on KZA_KON_KEY = KON_KEY ) on KZA_KAB_KEY = KAB_KEY";
    $Bedingung = $Funktionen->BedingungErstellen();
    $SQL = "Select " . $SelectFelder . " from kontakteabteilungen " . $JOIN ." ". $Bedingung ." ". $Group ." ". $Order;
    $rsKAB = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('KAB'));
}

if($Funktionen->AWIS_KEY1 == "-1" or $rsKAB->AnzahlDatensaetze() == 1) { //Einzel-DS-Ansicht/Neuer DS/Sortierung der Liste bei Einzel-DS-Ansicht
    $NeuerDatensatz = true;
    if((isset($rsKAB) and $rsKAB->AnzahlDatensaetze() == 1)){
        if($Funktionen->AWIS_KEY1 == 0) {
            $rsKAB->DSErster();
            $Funktionen->AWIS_KEY1 = $rsKAB->FeldInhalt('KAB_KEY');
        }

        $SQL = "select * from kontakteabteilungen where kab_key = ".$Funktionen->DB->WertSetzen('KAB','Z',$Funktionen->AWIS_KEY1);
        $rsKAB = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('KAB'));
        $NeuerDatensatz = false;
        $Funktionen->Form->Erstelle_HiddenFeld('AWIS_KEY1',$Funktionen->AWIS_KEY1);
    } else {
        $Funktionen->Form->Erstelle_HiddenFeld('AWIS_KEY1',"-1");
    }

    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./abteilungsbearbeitung_Main.php?cmdAktion=Details accesskey=T title='" . $Funktionen->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    if(isset($rsKAB)){
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKAB->FeldInhalt('KAB_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKAB->FeldInhalt('KAB_USERDAT'));
    }
    $Funktionen->Form->InfoZeile($Felder);

    if($NeuerDatensatz and ($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4) { //Neuer Datensatz
        $Ansicht = -1;

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("!KAB_ABTEILUNG", (isset($rsKAB) and $rsKAB->FeldInhalt('KAB_ABTEILUNG') <> "")?($rsKAB->FeldInhalt('KAB_ABTEILUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4)?true:false);
        $Funktionen->Form->ZeileEnde();
        $Funktionen->AWISCursorPosition = "txtKAB_ABTEILUNG";

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_BEMERKUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("KAB_BEMERKUNG", (isset($rsKAB) and $rsKAB->FeldInhalt('KAB_BEMERKUNG') <> "")?($rsKAB->FeldInhalt('KAB_BEMERKUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4)?true:false);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Trennzeile('O');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $SQL = "Select KON_KEY, (KON_NAME1 || ', ' || KON_NAME2 ) as NAME from kontakte where KON_STATUS = 'A' and KON_NAME1 IS NOT NULL and KON_NAME2 IS NOT NULL order by NAME asc";
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNGSBETREUERIT'].":",200);
        $Funktionen->Form->Erstelle_MehrfachSelectFeld("KAB_ABTEILUNGSBETREUERIT", array(), 500,true, $SQL,'','','','','','','',array(),'','AWIS','','',0);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $SQL = "Select KON_KEY, (KON_NAME1 || ', ' || KON_NAME2 ) as NAME from kontakte where KON_STATUS = 'A' and KON_NAME1 IS NOT NULL and KON_NAME2 IS NOT NULL order by NAME asc";
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNGSMITGLIED'].":",200);
        $Funktionen->Form->Erstelle_MehrfachSelectFeld("KAB_ABTEILUNGSMITGLIED", array(), 500,true, $SQL,'','','','','','','',array(),'','AWIS','','',0);
        $Funktionen->Form->ZeileEnde();
    } else { //Alter Datensatz -> Ansehen/�ndern
        $Ansicht = 1;

        //Erstelle die PopUps zum L�schen von Personen

        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', '', 'href', './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb=0'), array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Funktionen->Form->PopupDialog($Funktionen->AWISSprachKonserven['Wort']['Achtung'] . "!", $Funktionen->AWISSprachKonserven['KAB']['KAB_POPUP_TEXT'] , $Schaltflaechen, awisFormular::POPUP_WARNUNG, '0');

            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', '', 'href', './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb2=0'), array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Funktionen->Form->PopupDialog($Funktionen->AWISSprachKonserven['Wort']['Achtung'] . "!", $Funktionen->AWISSprachKonserven['KAB']['KAB_POPUP_TEXT'] , $Schaltflaechen, awisFormular::POPUP_WARNUNG, '2_0');
        }

        //Erstelle das PopUp zum L�schen der Abteilung

        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 8) == 8) {

            $SQL = "Select count(*) as ANZAHL from kontakteabteilungen right join (select * from kontakteinfos left join kontakte on KIN_KON_KEY = KON_KEY where KON_STATUS = 'A' and (KON_NAME1 IS NOT NULL or KON_NAME2 IS NOT NULL)) on KIN_WERT = KAB_KEY where kab_key = ".$Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1);
            $rsANZBETR = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen("KAB"));

            $SQL = "Select count(*) as ANZAHL from kontakteabteilungen right join (select * from kontakteabteilungenzuordnungen left join kontakte on KZA_KON_KEY = KON_KEY where KON_STATUS = 'A' and (KON_NAME1 IS NOT NULL or KON_NAME2 IS NOT NULL)) on KZA_KAB_KEY = KAB_KEY where kab_key = ".$Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1);
            $rsANZMIT = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen("KAB"));

            $Inhalt = str_replace("#ANZ_MIT#", $rsANZMIT->FeldInhalt("ANZAHL"), str_replace("#ANZ_BETR#", $rsANZBETR->FeldInhalt("ANZAHL"), $Funktionen->AWISSprachKonserven['KAB']['KAB_POPUP_LOESCHEN']));
            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', 'cmdLoeschen', 'post', 'post'),array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Funktionen->Form->PopupDialog($Funktionen->AWISSprachKonserven['Wort']['Achtung'] . "!", $Inhalt, $Schaltflaechen, awisFormular::POPUP_WARNUNG, 'LOESCHEN');

        }
        //Betreuung der Abteilung

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("!KAB_ABTEILUNG", (isset($rsKAB) and $rsKAB->FeldInhalt('KAB_ABTEILUNG') <> "")?($rsKAB->FeldInhalt('KAB_ABTEILUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2)?true:false);
        $Funktionen->Form->ZeileEnde();
        $Funktionen->AWISCursorPosition = "txtKAB_ABTEILUNG";

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_BEMERKUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("KAB_BEMERKUNG", (isset($rsKAB) and $rsKAB->FeldInhalt('KAB_BEMERKUNG') <> "")?($rsKAB->FeldInhalt('KAB_BEMERKUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2)?true:false);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Trennzeile('O');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNGSBETREUERIT'].":",400);
        $Funktionen->Form->ZeileEnde();

        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
            $Funktionen->Form->ZeileStart('', 'KAB_ABTEILUNGSBETREUERIT_DIV'); //Personen hinzuf�gen -> automatisch ausgeblendet -> Einblendung durch hinzuf�gen-Button
            $SQL = "SELECT kon_key, ( kon_name1 ||  ',' ||  kon_name2 ) AS name FROM kontakte WHERE kon_key NOT IN (SELECT kon_key FROM kontakte INNER JOIN kontakteinfos ON kin_kon_key = kon_key WHERE kin_wert = " . $Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1) . ") AND kon_key NOT IN (10319,10576,10409) AND kon_status = 'A' ORDER BY name ASC";
            $Funktionen->Form->Erstelle_MehrfachSelectFeld("KAB_ABTEILUNGSBETREUERIT", array(), 500, true, $SQL, '', '', '', '', '', '', '', $Funktionen->DB->Bindevariablen("KAB"), '', 'AWIS', '', '', 0);
            $Funktionen->Form->ZeileEnde();
        }

        $Funktionen->Form->ZeileStart(); //Kopfzeile der 1. Tabelle
        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
            $Funktionen->Form->Erstelle_ListenBild('ohne', 'KAB_ABTEILUNGSBETREUERIT_ADD', '', '/bilder/icon_new.png', 'KAB_ABTEILUNGSBETREUERIT_ADD', -2, '', 16, 16, 19);
            $Funktionen->Form->Erstelle_ListenBild('script', 'Del_PopUp', $Funktionen->Form->PopupOeffnen("0"), '/bilder/icon_delete.png', '', -2, '', 16, 16, 19);
        }
        $Link = './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY='.$Funktionen->AWIS_KEY1.'&Sort=KON_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KON_NAME1'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['KON']['KON_NAME1_lang'],250,'',$Link);
        $Link = './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY='.$Funktionen->AWIS_KEY1.'&Sort=KON_NAME2'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KON_NAME2'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['KON']['KON_NAME2_lang'],250,'',$Link);
        $Funktionen->Form->ZeileEnde();

        $SQL = "Select KON_NAME1, KON_NAME2, KON_KEY from kontakteabteilungen right join (select * from kontakteinfos left join kontakte on KIN_KON_KEY = KON_KEY where KON_STATUS = 'A' and (KON_NAME1 IS NOT NULL or KON_NAME2 IS NOT NULL)) on KIN_WERT = KAB_KEY where kab_key = ".$Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1);
        if (!isset($_GET['Sort'])) {
            $Order = ' ORDER BY KON_NAME1, KON_NAME2 ASC';
        } else {
            $Order = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
        }
        $SQL.= $Order;
        $rsKON = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen('KAB'));

        $DS = 0;    // f�r Hintergrundfarbumschaltung

        while(!$rsKON->EOF()) //Alle Datens�tze einzeln ausgeben
        {
            $Funktionen->Form->ZeileStart();
            if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
                $Funktionen->Form->Erstelle_ListenFeld('Platzhalter', '', 0, 19, false, ($DS % 2));
                $Funktionen->Form->Erstelle_ListenLoeschPopUp('./abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb=' . $rsKON->FeldInhalt('KON_KEY'), $rsKON->FeldInhalt('KON_KEY'), 19, '', '', ($DS % 2));
            }
            $Funktionen->Form->Erstelle_ListenFeld('KON_NAME1',$rsKON->FeldInhalt('KON_NAME1'),200,250,false,($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('KON_NAME2',$rsKON->FeldInhalt('KON_NAME2'),200,250,false,($DS % 2));
            $Funktionen->Form->ZeileEnde();

            $DS++;
            $rsKON->DSWeiter();
        }

        //Mitglieder der Abteilung:

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNGSMITGLIED'].":",400);
        $Funktionen->Form->ZeileEnde();

        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
            $Funktionen->Form->ZeileStart('', 'KAB_ABTEILUNGSMITGLIED_DIV'); //Personen hinzuf�gen -> automatisch ausgeblendet -> Einblendung durch hinzuf�gen-Button
            $SQL = "SELECT kon_key, ( kon_name1 ||  ',' ||  kon_name2 ) AS name FROM kontakte WHERE kon_key NOT IN(SELECT kon_key FROM kontakte INNER JOIN kontakteabteilungenzuordnungen ON kza_kon_key = kon_key WHERE kza_kab_key = " . $Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1) . ") AND kon_key NOT IN (10319,10576,10409) AND kon_status = 'A' ORDER BY name ASC";
            $Funktionen->Form->Erstelle_MehrfachSelectFeld("KAB_ABTEILUNGSMITGLIED", array(), 500, true, $SQL, '', '', '', '', '', '', '', $Funktionen->DB->Bindevariablen('KAB'), '', 'AWIS', '', '', 0);
            $Funktionen->Form->ZeileEnde();
        }

        $Funktionen->Form->ZeileStart(); //Kopfzeile der 2. Tabelle
        if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2) {
            $Funktionen->Form->Erstelle_ListenBild('ohne', 'KAB_ABTEILUNGSMITGLIED_ADD', '', '/bilder/icon_new.png', 'KAB_ABTEILUNGSMITGLIED_ADD', -2, '', 16, 16, 19);
            $Funktionen->Form->Erstelle_ListenBild('script', 'Del_PopUp', $Funktionen->Form->PopupOeffnen("2_0"), '/bilder/icon_delete.png', '', -2, '', 16, 16, 19);
        }
        $Link = './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY='.$Funktionen->AWIS_KEY1.'&Sort2=KON_NAME1'.((isset($_GET['Sort2']) AND ($_GET['Sort2']=='KON_NAME1'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['KON']['KON_NAME1_lang'],250,'',$Link);
        $Link = './abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY='.$Funktionen->AWIS_KEY1.'&Sort2=KON_NAME2'.((isset($_GET['Sort2']) AND ($_GET['Sort2']=='KON_NAME2'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['KON']['KON_NAME2_lang'],250,'',$Link);
        $Funktionen->Form->ZeileEnde();

        $SQL = "Select KON_NAME1, KON_NAME2, KON_KEY from kontakteabteilungen right join (select * from kontakteabteilungenzuordnungen left join kontakte on KZA_KON_KEY = KON_KEY where KON_STATUS = 'A' and (KON_NAME1 IS NOT NULL or KON_NAME2 IS NOT NULL)) on KZA_KAB_KEY = KAB_KEY where kab_key = ".$Funktionen->DB->WertSetzen('KAB',"Z",$Funktionen->AWIS_KEY1);

        if (!isset($_GET['Sort2'])) {
            $Order = ' ORDER BY KON_NAME1, KON_NAME2 ASC';
        } else {
            $Order = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort2']);
        }
        $SQL.= $Order;
        $rsKON = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('KAB'));

        $DS = 0;    // f�r Hintergrundfarbumschaltung

        while(!$rsKON->EOF()) //Alle Datens�tze einzeln ausgeben
        {
            $Funktionen->Form->ZeileStart();
            if((($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2)) {
                $Funktionen->Form->Erstelle_ListenFeld('Platzhalter', '', 0, 19, false, ($DS % 2));
                $Funktionen->Form->Erstelle_ListenLoeschPopUp('./abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb2=' . $rsKON->FeldInhalt('KON_KEY'), "2_" . $rsKON->FeldInhalt('KON_KEY'), 19, '', '', ($DS % 2));
            }
            $Funktionen->Form->Erstelle_ListenFeld('KON_NAME1',$rsKON->FeldInhalt('KON_NAME1'),200,250,false,($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('KON_NAME2',$rsKON->FeldInhalt('KON_NAME2'),200,250,false,($DS % 2));
            $Funktionen->Form->ZeileEnde();

            $DS++;
            $rsKON->DSWeiter();
        }
        $Funktionen->LadePersonenHinzufuegen();
    }

} elseif ($rsKAB->AnzahlDatensaetze() == 0) { //Ausgabe wenn kein Datensatz zur Suche gefunden wurde
    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['KAB']['KAB_KEINDS'],awisFormular::HINWEISTEXT_HINWEIS);
    $Funktionen->Form->ZeileEnde();
} else { //Listenansicht
    $Feldbreiten = array();
    $Feldbreiten['KAB_ABTEILUNG']=500;

    $Funktionen->Form->ZeileStart();
    $Link = './abteilungsbearbeitung_Main.php?cmdAktion=Details&SortList=KAB_ABTEILUNG' . ((isset($_GET['SortList']) AND ($_GET['SortList'] == 'KAB_ABTEILUNG')) ? '~' : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNG'], $Feldbreiten['KAB_ABTEILUNG'], '', $Link);
    $Funktionen->Form->ZeileEnde();

    $DS = 0;    // f�r Hintergrundfarbumschaltung

    while (!$rsKAB->EOF()) {
        if($rsKAB->FeldInhalt('KAB_KEY') <> "") {
            $Link = "./abteilungsbearbeitung_Main.php?cmdAktion=Details&KAB_KEY=" . $rsKAB->FeldInhalt('KAB_KEY');
            $Funktionen->Form->ZeileStart();
            $Funktionen->Form->Erstelle_ListenFeld('KAB_ABTEILUNG', $rsKAB->FeldInhalt('KAB_ABTEILUNG'), 0,  $Feldbreiten['KAB_ABTEILUNG'], false, ($DS % 2), '', $Link);
            $Funktionen->Form->ZeileEnde();
            $DS++;
        }
        $rsKAB->DSWeiter();
    }
}

$Funktionen->Form->Formular_Ende();

$Funktionen->Form->SchaltflaechenStart();
$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
if((($Funktionen->AWISBenutzer->HatDasRecht(650)& 2) == 2 and $Ansicht == 1) or (($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4 and $Ansicht == -1)) {
    $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}
if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4 and $Ansicht <> "-1") {
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 8) == 8 and $Ansicht == 1) {
    $Funktionen->Form->Schaltflaeche('script', 'cmdLoeschen', $Funktionen->Form->PopupOeffnen("LOESCHEN"), '/bilder/cmd_loeschen.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
}
$Funktionen->Form->SchaltflaechenEnde();
$Funktionen->Form->SchreibeHTMLCode("</form>");