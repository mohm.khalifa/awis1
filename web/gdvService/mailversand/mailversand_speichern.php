<?php
/**
 * Speichern Stammdatenpflege GDV-Mailversand
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607081616
 * @todo
 */

require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $Fehler;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if (isset($FelderNew) and $FelderNew != '') //Neuanlage
	{
		if(($_POST['txtVMV_FELD'] != '' and $_POST['txtVMV_WERT'] == '') or ($_POST['txtVMV_FELD'] == '' and $_POST['txtVMV_WERT'] != ''))
		{
			$Form->Hinweistext('Feld und Wert k&oumlnnen nur in Kombination bestehen.');
			$Fehler = true;
		}
		else {

			$SQL =' insert into versmailversand ';
			$SQL .=' (VMV_VERSNR,VMV_BEDINGUNG,VMV_MAILADRESSE,VMV_USER,VMV_USERDAT)';
			$SQL .=' values';
			$SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtAWIS_KEY1']);
			$SQL .=', '.$DB->FeldInhaltFormat('T',$_POST['txtVMV_FELD'] != ''?$_POST['txtVMV_FELD'].':'.$_POST['txtVMV_WERT']:null);
			$SQL .=', '.$DB->FeldInhaltFormat('T',$_POST['txtVMV_MAILADRESSE']);
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate ';
			$SQL .= ' )';


			$DB->Ausfuehren($SQL,'',true);
			$Fehler = false;
		}
	}
}
catch (awisException $ex)
{
    
    if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.UID_AUFTRAGSART_ATU) verletzt') // F5 Problem als Hinweistext ausgeben.
    {
        $Fehler = 1;
        $Hinweis = 'Die Auftragsart befindet sich schon in der Datenbank.';
       
        $Form->Hinweistext($Hinweis);
    }
    else
    {
        $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
        $Form->DebugAusgabe(1,$ex->getSQL());
    }
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
