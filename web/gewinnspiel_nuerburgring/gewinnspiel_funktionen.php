<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';

class gewinnspiel_funktionen
{

    public $AWIS_KEY1;
    private $AWISSprachKonserven;
    private $_DB;
    private $_Form;

    public function __construct()
    {

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('GWN', '%');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');

        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $this->_Form = new awisFormular();

        $this->AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    public function GewinnspielAJAX($PERSNR)
    {
        $SQL = 'SELECT PERSNR, BEREICH, ABTEILUNG FROM PERSONAL_KOMPLETT WHERE PERSNR = ' . $this->_DB->WertSetzen('GWN', 'Z', $PERSNR);
        $rsPers = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('GWN', true));

        $LabelBreite = 220;
        $FeldBreite = 200;

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['GWN']['GWN_HERKUNFT'], $LabelBreite);
        $this->_Form->Erstelle_TextFeld('!GWN_HERKUNFT', (isset($_GET['txtGWN_HERKUNFT_HIDDEN']) and $_GET['txtGWN_HERKUNFT_HIDDEN'] != '')?$_GET['txtGWN_HERKUNFT_HIDDEN']:(($rsPers->FeldInhalt('BEREICH') == 'W' OR $rsPers->FeldInhalt('BEREICH') == 'V')?$rsPers->FeldInhalt('ABTEILUNG'):''), 200, $FeldBreite, true);
        $this->_Form->ZeileEnde();
    }
}

?>