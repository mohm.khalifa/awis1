<?php
global $AWISCursorPosition;
global $AWISBenutzer;


try
{
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('LVB','%');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht4100=$AWISBenutzer->HatDasRecht(4100);
	
	echo "<br>";
	
	echo "<form name=frmSuche method=post action=./behaelterabfrage_Main.php?cmdAktion=Details>";
	
	
	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LVB'));
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	
	$Form->Formular_Start();
	
	//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
	$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LVB']['LVB_AUFTR_ID'].':',200);
	if($UserFilialen!='')
	{	
		$Form->Erstelle_TextFeld('*LVB_AUFTR_ID',$UserFilialen,20,200,false,'','','','T');
		echo '<input type=hidden name="sucLVB_AUFTR_ID" value="'.$UserFilialen.'">';
	}
	else 
	{
		$Form->Erstelle_TextFeld('*LVB_AUFTR_ID',($Param['SPEICHERN']=='on'?$Param['LVB_AUFTR_ID']:''),20,200,true);	
	}
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LVB']['LVB_ART_ID'].':',200);
	$Form->Erstelle_TextFeld('*LVB_ART_ID',($Param['SPEICHERN']=='on'?$Param['LVB_ART_ID']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LVB']['LVB_ID_VS'].':',200);
	$Form->Erstelle_TextFeld('*LVB_ID_VS',($Param['SPEICHERN']=='on'?$Param['LVB_ID_VS']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=behaelterabfrage&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	
	$Form->SchaltflaechenEnde();
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200811241411");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200811241412");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}	
?>
