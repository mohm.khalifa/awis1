<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20081113
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PZU','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht2100=$AWISBenutzer->HatDasRecht(2100);		// Rechte des Mitarbeiters
$Form->DebugAusgabe(1,$Recht2100);	
	if($Recht2100==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./portalzugaenge_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/
	$Form->Formular_Start();


	// Artikelnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_BENUTZERNAME'].':',160);
	$Form->Erstelle_TextFeld('*PZU_BENUTZERNAME','',30,150,true,'','','','T','L');
	$AWISCursorPosition=($AWISCursorPosition==''?'sucPZU_BENUTZERNAME':$AWISCursorPosition);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_KENNWORT'].':',160);
	$Form->Erstelle_TextFeld('*PZU_KENNWORT','',30,150,true,'','','','P','L');
	$Form->ZeileEnde();
	
	if(($Recht2100&48)!=0)		// User nur sich selber
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_PERSNR'].':',160);
		$Form->Erstelle_TextFeld('*PZU_PERSNR','',10,150,true,'','','','T','L');
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if((($Recht2100&4)!=0))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200921041807");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200921041808");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>