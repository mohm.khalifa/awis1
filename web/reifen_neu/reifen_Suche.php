<?php
/**
 * Suchmaske f�r die Auswahl Reifenpflege
 *
 * @author Christian Beierl
 * @copyright ATU
 * @version 20101412
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('RRT','%');
	$TextKonserven[]=array('SPI','%');
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('RET','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','Sortierung');
	$TextKonserven[]=array('Wort','Preisefuer');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','MaxVK');
	$TextKonserven[]=array('Wort','Filiale');
	$TextKonserven[]=array('Wort','Bezeichnung');
	$TextKonserven[]=array('Liste','lst_AlleOffenErledigt');
	$TextKonserven[]=array('Liste','lst_Laender');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15100 = $AWISBenutzer->HatDasRecht(15100);		
	$Recht250 = $AWISBenutzer->HatDasRecht(250);
	$Recht251 = $AWISBenutzer->HatDasRecht(251);
	$Recht252 = $AWISBenutzer->HatDasRecht(252);

	$Form->SchreibeHTMLCode('<form name=frmSuche method=post action="./reifen_Main.php?cmdAktion=Reifen">');

	$Param = unserialize($AWISBenutzer->ParameterLesen('RRESuche'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();

	//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
	$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if($UserFilialen!='')
	{		
		echo '<input type=hidden name="sucFIL_ID" value="'.$UserFilialen.'">';
	}

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['BREITE'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['QUERSCHNITT'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['INNENDURCHMESSER'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRT']['LLKW'].':',65);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRT']['LOADINDEX'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRT']['LOADINDEX2'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SPI']['SPEEDINDEX'].':',115);
	//$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['MaxVK'].':',100);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextFeld('*BREITE',($Param['SPEICHERN']=='on'?$Param['BREITE']:''),6,75,true);
	$AWISCursorPosition='sucBREITE';
	$Form->Erstelle_TextFeld('*QUERSCHNITT',($Param['SPEICHERN']=='on'?$Param['QUERSCHNITT']:''),10,100,true);
	$Form->Erstelle_TextFeld('*INNENDURCHMESSER',($Param['SPEICHERN']=='on'?$Param['INNENDURCHMESSER']:''),10,100,true);
	$Form->Erstelle_TextFeld('*LLKW',($Param['SPEICHERN']=='on'?$Param['LLKW']:''),4,65,true);
	$Form->Erstelle_TextFeld('*LOADINDEX',($Param['SPEICHERN']=='on'?$Param['LOADINDEX']:''),10,100,true);
	$Form->Erstelle_TextFeld('*LOADINDEX2',($Param['SPEICHERN']=='on'?$Param['LOADINDEX2']:''),10,100,true);
	$SQL = 'SELECT SPI_ID, SPI_SYMBOL FROM SPEEDINDEX ORDER BY SPI_ID';
	$Form->Erstelle_SelectFeld('*SPI_ID',($Param['SPEICHERN']=='on'?$Param['SPI_ID']:'0'),115,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	//$Form->Erstelle_TextFeld('*MaxVK','',10,50,true);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['ATUNR'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Bezeichnung'].':',200);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['HERSTELLER'].':',150);	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RET']['REIFENTYP'].':',100);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextFeld('*ATUNR',($Param['SPEICHERN']=='on'?$Param['ATUNR']:''),6,75,true);
	$Form->Erstelle_TextFeld('*Bezeichnung',($Param['SPEICHERN']=='on'?$Param['BEZEICHNUNG']:''),22,200,true);
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIREIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*REH_KEY',($Param['SPEICHERN']=='on'?$Param['REHKEY']:'0'),150,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$SQL = 'SELECT RET_ID, RET_TYP FROM REIFENTYP ORDER BY RET_ID';
	$Form->Erstelle_SelectFeld('*RET_ID',($Param['SPEICHERN']=='on'?$Param['RETID']:'0'),100,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');		
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Sortierung'].':',150);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Preisefuer'].':',150);
	if($UserFilialen=='')
	{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filiale'].':',150);
	}
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$KateogrieAlt = explode("|",$AWISSprachKonserven['RRP']['lst_RRP_Sortierung']);
	$Form->Erstelle_SelectFeld('*Sortierung',($Param['SPEICHERN']=='on'?$Param['SORT']:''),150,true,'','','','','',$KateogrieAlt);		
	//$StatusText = explode("|",$AWISSprachKonserven['Liste']['lst_Laender']);
	$SQL = 'SELECT LAN_CODE, LAN_LAND FROM LAENDER';	
	
	$LAND='';

	if(strtoupper(substr($AWISBenutzer->BenutzerName(),0,4))=='FIL-')
	{
		$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);
		$SQLLAND='SELECT FIL_LAN_WWSKENN FROM FILIALEN WHERE FIL_ID = '.intval($Fil_Nr);
		$rsLAND = $DB->RecordSetOeffnen($SQLLAND);
		
		if($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='BRD')
		{
			$LAND='BRD';
		}
		elseif($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='OES')
		{
			$LAND='OES';		
		}
		elseif($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='CZE')
		{
			$LAND='CZE';
		}
		elseif($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='ITA')
		{
			$LAND='ITA';
		}
		elseif($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='NED')
		{
			$LAND='NED';
		}
		elseif($rsLAND->FeldInhalt('FIL_LAN_WWSKENN')=='SUI')
		{
			$LAND='SUI';
		}
		else
		{
			$LAND='BRD';
		}	
	}
	
	$Bedingung = '';
	if((($Recht250&128)==128 AND strtoupper(substr($AWISBenutzer->BenutzerName(),0,4))!='FIL-') or (($Recht250&128)==128 AND $LAND=='BRD'))
	{
		$Bedingung.= ' OR LAN_CODE = \'DE\'';
	}
	if(($Recht250&2048)==2048 or (($Recht250&128)==128 AND $LAND=='ITA'))
	{
		$Bedingung.= ' OR LAN_CODE = \'IT\'';
	}
	if(($Recht250&4096)==4096 or (($Recht250&128)==128 AND $LAND=='NED'))
	{
		$Bedingung.= ' OR LAN_CODE = \'NL\'';
	}
	if(($Recht250&256)==256 or (($Recht250&128)==128 AND $LAND=='OES'))
	{
		$Bedingung.= ' OR LAN_CODE = \'AT\'';
	}
	if(($Recht250&8192)==8192 or (($Recht250&128)==128 AND $LAND=='SUI'))
	{
		$Bedingung.= ' OR LAN_CODE = \'CH\'';
	}
	if(($Recht250&512)==512 or (($Recht250&128)==128 AND $LAND=='CZE'))
	{
		$Bedingung.= ' OR LAN_CODE = \'CZ\'';		
	}
	
	if ($Bedingung != '')
	{
		$SQL.= ' WHERE '.substr($Bedingung,3);
	}

	$Form->Erstelle_SelectFeld('*Laender',($Param['SPEICHERN']=='on'?$Param['LAND']:''),150,true,$SQL,'','','','','');		
	//$Form->Erstelle_SelectFeld('*Laender','',150,true,$SQL,'','','','',$StatusText);		
	if($UserFilialen=='')
	{
		$Form->Erstelle_TextFeld('*FIL_ID',($Param['SPEICHERN']=='on'?$Param['FIL_ID']:''),10,200,true,'','','','T','','Filialpreis f�r Filiale suchen','',4);		
	}	
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();	
	if ($Recht252 > 0)
	{
		$Form->Erstelle_Checkbox('CCSort','on',50,true,'on');
	}
	else 
	{
		$Form->Erstelle_Checkbox('CCSort',($Param['SPEICHERN']=='on'?$Param['CCSORT']:''),50,true,'on');
	}
	$Form->Erstelle_TextLabel('CC-Sortierung',350);
	$Form->ZeileEnde();	
	
	$Form->ZeileStart();	
	$Form->Erstelle_Checkbox('HSpeed',($Param['SPEICHERN']=='on'?$Param['HSPEED']:''),50,true,'on');
	$Form->Erstelle_TextLabel('Reifen mit h�herem Speedindex',350);
	$Form->ZeileEnde();	
	
	$Form->ZeileStart();	
	$Form->Erstelle_Checkbox('HLoad',($Param['SPEICHERN']=='on'?$Param['HLOAD']:''),50,true,'on');
	$Form->Erstelle_TextLabel('Reifen mit h�herem Loadindex',350);
	$Form->ZeileEnde();	
	
	if (($Recht251&1) == 1)
	{	
		$Form->ZeileStart();	
		$Form->Erstelle_Checkbox('KBListe',($Param['SPEICHERN']=='on'?$Param['KBLISTE']:''),50,true,'on');
		$Form->Erstelle_TextLabel('KB-Liste',350);
		$Form->ZeileEnde();	
		
		if (($Recht251&4) == 4)
		{
			$Form->ZeileStart();	
			$Form->Erstelle_Checkbox('KBPreise',($Param['SPEICHERN']=='on'?$Param['KBPREIS']:''),50,true,'on');
			$Form->Erstelle_TextLabel('KB-Preise',350);
			$Form->ZeileEnde();	
		}
	}
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
		
	$Form->Formular_Ende();
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht15100&4) == 4)		
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor($AWISCursorPosition);
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301833");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301834");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>