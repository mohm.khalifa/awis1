<?php
//*************************************************************
// Reifen - Konditionen
//*************************************************************
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;
global $CursorFeld;

$TextKonserven = array();
$TextKonserven[]=array('CKM','%');
$TextKonserven[]=array('CKD','%');
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineUploadDatei');
$TextKonserven[]=array('Wort','Uploaddatei');
$TextKonserven[]=array('Wort','lbl_zurueck');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3707 = awisBenutzerRecht($con,3707,$AWISBenutzer->BenutzerName());
if($Recht3707==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
    echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
    die();
}

/**********************************************
 * * Eingabemaske
 ***********************************************/
$Form->Formular_Start();

$Form->Erstelle_HiddenFeld('DATEINAME',strftime('%Y%m%d%H%M%S') . '_export_crm_reifenkond.csv');
$Form->Erstelle_HiddenFeld('PFAD','/dokumente/temp/');

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_ImportAktion'].':',150);
$ImportAktionen = explode("|",$AWISSprachKonserven['CKM']['lst_CKM_IMPKENN']);
$Form->Erstelle_SelectFeld('Aktion',1,100,true,'','','3','','',$ImportAktionen,'');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
$Form->Erstelle_DateiUpload('datImport',200,20,7*1024*1024);
$Form->ZeileEnde();

if (isset($_POST['txtAktion']))
{

    if ($_POST['txtAktion'] == 1)
    {
        echo '<a href ="' . $_POST['txtPFAD'] . $_POST['txtDATEINAME'] . '" download>Download Exportdatei</a>';
    }
    else
    {
        $Datei = $_FILES['datImport']['tmp_name'];
        $fp = @fopen($Datei, 'r');
        if($fp===false)
        {
            $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineUploadDatei']);
            echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
            die();
        }

        // Ueberschrift
        $Felder = explode(';', fgets($fp));

        while(!feof($fp))
        {
            $Felder = explode(';',fgets($fp));

            if(!is_numeric($Felder[0]))
            {
                // insert
                $SQL = "SELECT CKM_KEY FROM CRMKONDITIONSMODELLE WHERE TRIM(CKM_BEZEICHNUNG) = ".$DB->WertSetzen('CKM','T',trim($Felder[1]));
                $rsCKDKey = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('CKM'));

                if($rsCKDKey->AnzahlDatensaetze() == 1)
                {
                    $CKD_CKM_KEY = $rsCKDKey->FeldInhalt('CKM_KEY');
                }

                $SQL = "INSERT INTO CRMKONDITIONSMODELLDETAILS (CKD_CKM_KEY,CKD_BEZEICHNUNG,CKD_BEMERKUNG,CKD_STATUS,CKD_BEREICH,CKD_TEXT,CKD_USER,CKD_USERDAT,CKD_TEXT2) ";
                $SQL .= "VALUES (";
                $SQL .= $DB->WertSetzen('CKD','N0',$CKD_CKM_KEY);
                $SQL .= "," . $DB->WertSetzen('CKD','T',$Felder[2]);
                $SQL .= "," . $DB->WertSetzen('CKD','T',$Felder[3]);
                $SQL .= "," . $DB->WertSetzen('CKD','T','A');
                $SQL .= "," . $DB->WertSetzen('CKD','T','Reifen');
                $SQL .= "," . $DB->WertSetzen('CKD','T',str_replace('%',' %',$Felder[4]));
                $SQL .= "," . $DB->WertSetzen('CKD','T','AWIS');
                $SQL .= ",SYSDATE";
                $SQL .= "," . $DB->WertSetzen('CKD','T',str_replace('%',' %', trim($Felder[5])));
                $SQL .= ")";

                $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('CKD'));

            }
            else
            {
                // update
                $SQL = "UPDATE CRMKONDITIONSMODELLDETAILS ";
                $SQL .= "SET ";
                $SQL .= "CKD_BEZEICHNUNG = " . $DB->WertSetzen('CKD',$Felder[2]);
                $SQL .= ",CKD_BEMERKUNG = " . $DB->WertSetzen('CKD',$Felder[3]);
                $SQL .= ",CKD_TEXT = " . $DB->WertSetzen('CKD',str_replace('%',' %',$Felder[4]));
                $SQL .= ",CKD_TEXT2 = " . $DB->WertSetzen('CKD',str_replace('%',' %',trim($Felder[5])));
                $SQL .= ",CKD_USERDAT = SYSDATE ";
                $SQL .= "WHERE CKD_KEY = " . $DB->WertSetzen('CKD',$Felder[0]);

                $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('CKD'));
            }
        }
    }
}

$Form->Formular_Ende($CursorFeld);

?>