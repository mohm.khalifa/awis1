<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FGP', 'FGP*');
    $TextKonserven[] = array('FPH', 'FPH_*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'txt_FGBruttopreis');

    $Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if ($Recht9100 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->Formular_Start();

    if ($AWIS_KEY1 != 0)
    {

        if ($AWIS_HISTKEY == 0 && $AWIS_KEY1 != 0)
        {
            $AWIS_KEY2 = $AWIS_KEY1;

            $SQL = 'Select FGPOSITIONSDATEN.*,FGKOPFDATEN.* from FGPOSITIONSDATEN inner join FGKOPFDATEN ON FGP_VORGANGNR = FGK_VORGANGNR WHERE FGP_FGK_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY1, "false");
            //echo $SQL;
            $rsFGP = $DB->RecordSetOeffnen($SQL);
        }
        else
        {
            $SQL = 'Select FGPOSITIONSDATEN_HIST.* from FGPOSITIONSDATEN_HIST WHERE FPH_FKH_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_HISTKEY, "false");
            //$Form->DebugAusgabe(1,$AWIS_HISTKEY);
            //echo $SQL;
            $rsPOSHIST = $DB->RecordSetOeffnen($SQL);

            $SQLFKHSteuersatz = 'Select * from FGKOPFDATEN_HIST WHERE FKH_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_HISTKEY, "false");

            $rsPOSHISTSteuer = $DB->RecordSetOeffnen($SQLFKHSteuersatz);
            //$Form->DebugAusgabe(1,$rsPOSHISTSteuer);
        }
        //$Form->DebugAusgabe(1,$rsFGP);
    }
    else
    {
        if ($AWIS_HISTKEY == 0 && $AWIS_KEY1 != 0)
        {
            $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
            $AWIS_KEY2 = $Param['FGK_KEY'];
            //echo $AWIS_KEY2;

            $SQL = 'Select FGPOSITIONSDATEN.* from FGPOSITIONSDATEN WHERE FGP_FGK_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY2, "false");
            //echo $SQL;
            $rsFGP = $DB->RecordSetOeffnen($SQL);
        }
        else
        {
            $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
            $AWIS_HISTKEY = $Param['FKH_KEY'];

            $SQL = 'Select FGPOSITIONSDATEN_HIST.* from FGPOSITIONSDATEN_HIST WHERE FPH_FKH_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_HISTKEY, "false");

            $rsPOSHIST = $DB->RecordSetOeffnen($SQL);
        }
        //$Form->DebugAusgabe(1,$rsFGP);
    }
    $Steuer = 0.0;

    $Gesamtpreis = 0.0;
    $ErgebnisBrutto = 0.0;
    $zwSumme = 0.0;
    $Menge = 0.0;
    $Preis = 0.0;

    if ($AWIS_HISTKEY == 0 && $AWIS_KEY1 != 0)
    {
        //Zeige Positionsdaten an

        $EditRecht = (($Recht9100 & 2) != 0);
        //$Form->Formular_Start();
        $Steuer = '1.' . $rsFGP->FeldInhalt('FGK_STEUERSATZ');

        $Form->ZeileStart();
        //?ID='.base64_encode($rsZLB->FeldInhalt('ZLB_LIE_NR').'~'.$rsZLB->FeldInhalt('ZLB_WANR'));
        //$Icons[] = array('pdf','./firstglas_Drucken.php?FGK_KEY='.$AWIS_KEY1.'&PDF_LISTE=1');
        $Form->Erstelle_Liste_Ueberschrift('', 20, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGP']['FGP_ARTNR'], 120, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGP']['FGP_ARTBEZ'], 500, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGP']['FGP_ANZAHL'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGP']['FGP_EINHEIT'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGP']['FGP_OEMPREIS'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['txt_FGBruttopreis'], 100, '');
        $Icons[0] = array('pdf', './firstglas_Drucken.php?FGK_KEY=' . base64_encode($AWIS_KEY1));
        $Form->Erstelle_ListeIcons($Icons, 20, -1);
        $Icons[0] = array('pdf', './firstglas_Drucken_Brutto.php?FGK_KEY=' . base64_encode($AWIS_KEY1));
        $Form->Erstelle_ListeIcons($Icons, 40, -1);
        $Form->ZeileEnde();

        $FGKZeile = 0;
        while (!$rsFGP->EOF())
        {
            $CardBrutto = 0.0;
            $bruttoCard = 0.0;
            $zwSumme = 0.0;
            $Menge = 0.0;
            $Preis = 0.0;
            $brutto = 0.0;
            $gesamtBrutto = 0.0;
            //Datenbank - ID
            $Form->Erstelle_HiddenFeld('FGP_KEY', $rsFGP->FeldInhalt('FGP_KEY'));
            //Datensatz - Kennung
            $Form->Erstelle_HiddenFeld('FGP_KENNUNG', $rsFGP->FeldInhalt('FGP_KENNUNG'));
            //Vorgangsnummer
            $Form->Erstelle_HiddenFeld('FGP_VORGANGNR', $rsFGP->FeldInhalt('FGP_VORGANGNR'));

            $Form->ZeileStart();
            $Link = './autoglas_Main.php?cmdAktion=Details&FGP_KEY=' . $rsFGP->FeldInhalt('FGP_KEY') . '';
            $Form->Erstelle_ListenFeld('', '', 0, 20, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FGP_ARTNR', $rsFGP->FeldInhalt('FGP_ARTNR'), 0, 120, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FGP_ARTBEZ', $rsFGP->FeldInhalt('FGP_ARTBEZ'), 0, 500, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FGP_ANZAHL', $rsFGP->FeldInhalt('FGP_ANZAHL'), 0, 100, false, ($FGKZeile % 2), '', '');
            $Menge = $rsFGP->FeldInhalt('FGP_ANZAHL');

            $Form->Erstelle_ListenFeld('FGP_EINHEIT', $rsFGP->FeldInhalt('FGP_EINHEIT'), 0, 100, false, ($FGKZeile % 2), '', '');
            $checkBetrag = 0.0;
            $checkBetrag = str_replace(',', '.', $rsFGP->FeldInhalt('FGP_OEMPREIS'));
            //$checkBetrag = str_replace($rsFGP->FeldInhalt('FGP_OEMPREIS'),',', '.');
            $Form->Erstelle_ListenFeld('FGP_OEMPREIS', $checkBetrag, 0, 100, false, ($FGKZeile % 2), '', '', 'N2');

            $Preis = $checkBetrag;

            $Menge = str_replace(',', '.', $Menge);

            $zwSumme = $Menge * $Preis;
            //$zwSumme = $Preis;
            $CardBrutto = $Preis;
            $Gesamtpreis = $Gesamtpreis + $zwSumme;

            $bruttoCard = $CardBrutto * $Steuer;

            $brutto = $zwSumme * $Steuer;
            $ErgebnisBrutto = $ErgebnisBrutto + $brutto;

            $Form->Erstelle_ListenFeld('txt_FGBruttopreis', $bruttoCard, 0, 100, false, ($FGKZeile % 2), '', '', 'N2');
            //$Form->Erstelle_ListenFeld($Name, $Wert, $Zeichen, $Breite, $AendernRecht, $Hintergrund, $Style, $Link, $Format, $Ausrichtung, $ToolTipText);
            //echo round($Gesamtpreis, 2);
            $Form->ZeileEnde();

            $FGKZeile++;

            $rsFGP->DSWeiter();
        }
        //$Steuer = '1'.$rsFGP->FeldInhalt('FGK_STEUER');
        $Steuer = str_replace('.', '', $Steuer);
        $Gesamtpreis = (($Gesamtpreis / 100) * $Steuer);
        $Gesamtpreis = round($Gesamtpreis, 2);

        $ErgebnisBrutto = round($ErgebnisBrutto, 2);

        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('', '', 0, 20, false, ($FGKZeile % 2), '', '');
        $Form->Erstelle_ListenFeld('FGP_GEPREIS', 'Gesamtpreis:', 0, 834, false, ($FGKZeile % 2), '', '');
        //$Form->Erstelle_ListenFeld('FGP_GESAMTPREIS',$Gesamtpreis,0,98,false,($FGKZeile%2),'font-weight:bold','','N2');
        $Form->Erstelle_ListenFeld('', '', 0, 98, false, ($FGKZeile % 2), '', '', '');
        $Form->Erstelle_ListenFeld('FGP_Brutto', $ErgebnisBrutto, 0, 98, false, ($FGKZeile % 2), 'font-weight:bold', '', 'N2');
        $Form->ZeileEnde();
    }
    else
    {
        if ($AWIS_KEY1 != 0)
        {
            if ($rsPOSHIST->AnzahlDatensaetze() > 0)
            {
                $EditRecht = (($Recht9100 & 2) != 0);

                //$Form->Formular_Start();

                $Form->ZeileStart();
                $Icons[] = array('pdf', './firstglashist_Drucken.php?FKH_KEY=' . base64_encode($AWIS_HISTKEY));
                $Form->Erstelle_ListeIcons($Icons, 20, -1);
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FPH']['FPH_ARTNR'], 120, '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FPH']['FPH_ARTBEZ'], 500, '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FPH']['FPH_ANZAHL'], 100, '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FPH']['FPH_EINHEIT'], 100, '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FPH']['FPH_OEMPREIS'], 100, '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['txt_FGBruttopreis'], 100, '');
                $Form->ZeileEnde();

                $Steuer = '1.' . $rsPOSHISTSteuer->FeldInhalt('FKH_STEUERSATZ');
                $Gesamtpreis = 0;
                $ErgebnisBrutto = 0.0;
                $zwSumme = 0;
                $Menge = 0;
                $Preis = 0;
                $CardBrutto = 0.0;
                $FPHZeile = 0;
                while (!$rsPOSHIST->EOF())
                {

                    $CardBrutto = 0.0;
                    $bruttoCard = 0.0;
                    $Menge = 0;
                    $Preis = 0;
                    $zwSumme = 0;
                    //Datenbank - ID
                    $Form->Erstelle_HiddenFeld('FPH_KEY', $rsPOSHIST->FeldInhalt('FPH_KEY'));
                    //Datensatz - Kennung
                    $Form->Erstelle_HiddenFeld('FPH_KENNUNG', $rsPOSHIST->FeldInhalt('FPH_KENNUNG'));
                    //Vorgangsnummer
                    $Form->Erstelle_HiddenFeld('FPH_VORGANGNR', $rsPOSHIST->FeldInhalt('FPH_VORGANGNR'));

                    $Form->ZeileStart();
                    $Link = './autoglas_Main.php?cmdAktion=Details&FPH_KEY=' . $rsPOSHIST->FeldInhalt('FPH_KEY') . '';
                    $Form->Erstelle_ListenFeld('', '', 0, 20, false, ($FPHZeile % 2), '', '');
                    $Form->Erstelle_ListenFeld('FPH_ARTNR', $rsPOSHIST->FeldInhalt('FPH_ARTNR'), 0, 120, false, ($FPHZeile % 2), '', '');
                    $Form->Erstelle_ListenFeld('FPH_ARTBEZ', $rsPOSHIST->FeldInhalt('FPH_ARTBEZ'), 0, 500, false, ($FPHZeile % 2), '', '');
                    $Form->Erstelle_ListenFeld('FPH_ANZAHL', $rsPOSHIST->FeldInhalt('FPH_ANZAHL'), 0, 100, false, ($FPHZeile % 2), '', '');

                    $Menge = $rsPOSHIST->FeldInhalt('FPH_ANZAHL');

                    $Form->Erstelle_ListenFeld('FPH_EINHEIT', $rsPOSHIST->FeldInhalt('FPH_EINHEIT'), 0, 100, false, ($FPHZeile % 2), '', '');

                    $Preis = str_replace(',', '.', $rsPOSHIST->FeldInhalt('FPH_OEMPREIS'));

                    $Form->Erstelle_ListenFeld('FPH_OEMPREIS', $Preis, 0, 100, false, ($FPHZeile % 2), '', '', 'N2');

                    $Menge = str_replace(',', '.', $Menge);
                    $zwSumme = $Menge * $Preis;
                    $CardBrutto = $Preis;
                    $Gesamtpreis = $Gesamtpreis + $zwSumme;

                    $bruttoCard = $CardBrutto * $Steuer;

                    $brutto = $zwSumme * $Steuer;
                    $ErgebnisBrutto = $ErgebnisBrutto + $brutto;

                    $Form->Erstelle_ListenFeld('txt_FGBruttopreis', $bruttoCard, 0, 100, false, ($FPHZeile % 2), '', '', 'N2');
                    $Form->ZeileEnde();

                    $FPHZeile++;
                    $rsPOSHIST->DSWeiter();
                }
                $Steuer = str_replace('.', '', $Steuer);
                $Gesamtpreis = (($Gesamtpreis / 100) * $Steuer);
                $Gesamtpreis = round($Gesamtpreis, 2);

                $ErgebnisBrutto = round($ErgebnisBrutto, 2);

                $Form->ZeileStart();
                $Form->Erstelle_ListenFeld('', '', 0, 20, false, ($FPHZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FGP_GEPREIS', 'Gesamtpreis:', 0, 834, false, ($FPHZeile % 2), '', '', 'N2');
                //$Form->Erstelle_ListenFeld('FGP_GESAMTPREIS',$Gesamtpreis,0,98,false,($FPHZeile%2),'','','N2');
                $Form->Erstelle_ListenFeld('', '', 0, 98, false, ($FPHZeile % 2), '', '', '');
                $Form->Erstelle_ListenFeld('FGP_Brutto', $ErgebnisBrutto, 0, 98, false, ($FPHZeile % 2), '', '', 'N2');

                $Form->ZeileEnde();
                $Form->Formular_Ende();
            }
        }
    }
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
