INSERT INTO azubiprojektebereiche (
    azb_stufe,
    azb_bereichname,
    azb_infotext,
    azb_user,
    azb_status
) VALUES (
    3,
    'Einbau des Motor / Antriebsstrang (25.06.2018 - 29.06.2018)',
    'Hier erwartet Sie die Vorbereitung der Karosserie f�r den Zusammenbau, der Einbau des Motors und Teile des Antriebsstrangs. Zudem erfolgt die Verlegung und Vorbereitungen der Verkabelung im Motor- und Innenraum.<br><br>

Sie befinden sich in der Ausbildung zum Kfz-Mechatroniker im 3. oder 4. Lehrjahr und m�chten ein Teil dieses Projekts werden, dann nutzen Sie Ihre Chance. Die Teilnehmer werden ca. 2 Wochen vor Beginn ausgelost und entsprechend per E-Mail informiert.
',
    'streit_d',
    'A',
);

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('AZT_BILDER', 'AZT', 'Bilder f�r die Seite', '<style>
figure.pic_corvette {display: table-cell;  padding: 0.5em;}
img.pic_corvette {width: auto; height: 200px;}
</style>

<div style="margin-top: 1em;">
<figure class="pic_corvette"><img class="pic_corvette" src=''/bilder/Azubiprojekte_C3.jpg'' alt=''Azubiprojekte_Bilder_Corvette_C3'' align=''center''><figcaption>Originalzustand</figcaption></figure>
<figure class="pic_corvette"><img class="pic_corvette" src=''/bilder/Azubiprojekt_C3_Azubis.png'' alt=''Azubiprojekte_Bilder_Corvette_C3''></figure>
<figure class="pic_corvette"><img class="pic_corvette" src=''/bilder/Azubiprojekt_C3_Zwischenstand.jpg'' alt=''Azubiprojekte_Bilder_Corvette_C3''><figcaption>der aktuelle Stand der Corvette</figcaption></figure>
</div>')

COMMIT;
