<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();
	
	
	/**
	 * Script zieht sich die Werte aus den Eingabefeldern um sie sich beim L�schen einer Massnahme
	 * beim Neuladen der Seite merken zu k�nnen.
	 **/
	$script = "<script type='text/javascript'>
	
			function urlanpassen (linkObjekt) { 
			// Alle Werte beim l�schen der Massnahme wieder zur�ck in die Maske geben.
				urlNeu = linkObjekt.getAttribute('href');
				urlZusatz = '&txtFESTSTELLUNG='+ document.getElementById('txtFESTSTELLUNG').value;
				urlZusatz = urlZusatz + '&txtQMM_MASSNAHME='+ document.getElementById('txtQMM_MASSNAHME').value;
				urlZusatz = urlZusatz + '&txtQMZ_BESCHREIBUNG='+ document.getElementById('txtQMZ_BESCHREIBUNG').value;
				urlZusatz = urlZusatz + '&txtQMM_FRIST='+ document.getElementById('txtQMM_FRIST').value;
				urlZusatz = urlZusatz + '&txtQMM_Persnr='+ document.getElementById('txtQMM_Persnr').value;
				urlZusatz = urlZusatz + '&txtQMM_STATUS='+ document.getElementById('txtQMM_STATUS').value;
				urlNeu = urlNeu+urlZusatz;
				
				linkObjekt.setAttribute('href', urlNeu);
		  }
			
			
			
		  function toggleDiv(KEY) {
			
			var position = document.getElementById('Zeile_'+KEY).offsetTop; 
			if(window.ActiveXObject){ 
			 	position = position + 110; //F�r IE 110 mehr!?
			}
			document.getElementById('Hist').style.top = position ;	// Position Div an Datenzeile festmachen.
			document.getElementById('Hist').style.left =  200;	
			
			if(document.getElementById('Hist').style.display == 'none')
			{
				document.getElementById('Hist').style.display = 'block'; // History einblenden
			}			
		}
			
		  function closeHist() {	
			if(document.getElementById('Hist').style.display = 'block')
			{
				document.getElementById('Hist').style.display = 'none'; // History schlie�en
			}	
		  }
			
		    function ladeDiv(KEY) {
				var test = null;
				if(window.XMLHttpRequest){ 
					test = new XMLHttpRequest();
				}
				else if(window.ActiveXObject){ 
					try{
						test = new ActiveXObject('Msxml2.XMLHTTP.6.0');
					} catch(e){
						try{
							test = new ActiveXObject('Msxml2.XMLHTTP.3.0');
						}
						catch(e){}
					}
				}
				if(test==null)
				{
					alert('Ihr Browser unterst�tzt kein Ajax!');
				}
				
				test.open('GET','../daten/qualitaetsmaengel_Hist.php?KEY='+KEY);
				test.send(null);

				test.onreadystatechange = function() {
            	if(test.readyState == 4 && test.status == 200) {
				document.getElementById('Hist').innerHTML = test.responseText; // Wert von Seite per Ajax ins Div laden
            	}
				
			
				toggleDiv(KEY);
        }
			
              
		  }
	</script>";
	
	echo $script;
	

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_QMM'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;	
	
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	

	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'qmp_fil_id';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
	 $Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht34000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='' and ($Recht34000&2)==2)
		{
			$Param['QMM_FIL_ID']=$Form->Format('T',$_POST['txtQMM_FIL_ID'],true);
		}
		else 
		{
			if(isset($_POST['sucQMM_FIL_ID']))
			{
				$Param['QMM_FIL_ID']=$Form->Format('N0',$_POST['sucQMM_FIL_ID'],true);
				if($Param['QMM_FIL_ID'] == 0)
				{
					$Param['QMM_FIL_ID'] = '';
				}
			}
			else 
			{
				$Param['QMM_FIL_ID']=0;
			}
			if(($Recht34000&4)==4 or ($Recht34000&8)==8)
			{
				$Param['FIL_GEBIET'] = $Form->Format('N0',$_POST['sucFIL_GEBIET'],true);
			}
		}	
		$Param['QMM_QMK_KEY'] = $Form->Format('T',$_POST['sucQMK_KRITERIUM'],true);
		$Param['QMM_STATUS'] = 	$Form->Format('N0',$_POST['sucQMM_STATUS'],true);
		$Param['ALLE']=isset($_POST['sucAlleAnzeigen'])?'on':'';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		if(($_POST['txtQMS_FOLGESTATUS'] != '' and $_POST['txtQMS_FOLGESTATUS'] != 0))
		{
			if(!$_POST['txtFESTSTELLUNG'] and $_POST['txtQMM_STATUS'] == 1)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_FESTSTELLUNG']);
				$Form->ZeileEnde();
				$Fehler++;
			}
			
			if(!$_POST['txtQMM_Persnr'] and $_POST['txtQMM_STATUS'] == 1)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_PERSNR']);
				$Form->ZeileEnde();
				$Fehler++;
			}
			elseif($_POST['txtQMM_Persnr'] and $_POST['txtQMM_STATUS'] == 1)
			{
				if(ctype_digit($_POST['txtQMM_Persnr']))
				{
					$SQL = 'SELECT Name || \',\' || Vorname as Name ';
					$SQL .= ' from personal_komplett where persnr ='.$_POST['txtQMM_Persnr'];
					$SQL .= ' and (trunc(datum_eintritt) <= trunc(sysdate)) and (datum_austritt is null or trunc(datum_austritt) >= trunc(sysdate))';
					$rsCHECK = $DB->RecordSetOeffnen($SQL);
					
					if (($rsCHECK->AnzahlDatensaetze() == 0))
					{
						$Form->ZeileStart();
						$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_PERSNA']);
						$Form->ZeileEnde();
						$Fehler++;
					}
				}
				else 
				{
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_PERSZAHL']);
					$Form->ZeileEnde();
					$Fehler++;
				}
			}
			if($_POST['txtQMM_Persnr'] and $_POST['txtQMM_STATUS'] == 0)
			{
				if(ctype_digit($_POST['txtQMM_Persnr']))
				{
					$SQL = 'SELECT Name || \',\' || Vorname as Name ';
					$SQL .= ' from personal_komplett where persnr ='.$_POST['txtQMM_Persnr'];
					$SQL .= ' and (trunc(datum_eintritt) <= trunc(sysdate)) and (datum_austritt is null or trunc(datum_austritt) >= trunc(sysdate))';
					$rsCHECK = $DB->RecordSetOeffnen($SQL);
					
					if (($rsCHECK->AnzahlDatensaetze() == 0))
					{
						$Form->ZeileStart();
						$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_PERSNA']);
						$Form->ZeileEnde();
						$Fehler++;
					}
				}
				else
				{
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_PERSZAHL']);
					$Form->ZeileEnde();
					$Fehler++;
				}
			}
			
			if($_POST['txtQMM_STATUS'] == 1)
			{
				$SQL ='select qmz.* from qmplaene qmp';
				$SQL .=' inner join qmmassnahmenzuord qmz on qmp_key = qmz_qmp_key';
				$SQL .=' where qmp_key = '.$_POST['txtQMP_KEY'];
				$rsMassPruef = $DB->RecordSetOeffnen($SQL);
				$Frist = 0;
				if($rsMassPruef->AnzahlDatensaetze() == 0)
				{
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_NOMASS']);
					$Form->ZeileEnde();
					$Fehler++;
				}
				
				while(! $rsMassPruef->EOF())
				{
					if ((strtotime($rsMassPruef->FeldInhalt('QMZ_FRIST')) < strtotime(date('d.m.Y',time()))))
					{
						$Frist++;
					}
					$rsMassPruef->DSWeiter();
				}
					
				if($Frist > 0)
				{
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_FRISTZUORD']);
					$Form->ZeileEnde();
					$Fehler++;
				}
					
			}
			if($Fehler > 0)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_NSPEICHERN']);
				$Form->ZeileEnde();
			}
			else 
			{	
					include('./qualitaetsmaengel_speichern.php');
					$Save = 1;
			}
		}
		if($_POST['txtQMS_FOLGESTATUS'] == 0)
		{
			include('./qualitaetsmaengel_speichern.php');
			$Save = 1;
		}
	}
	elseif($FelderDel != '' or isset($_POST['cmdLoeschenOK']))
	{
		include('./qualitaetsmaengel_loeschen.php');
	}
	
	if(isset($_GET['QMP_KEY']))
	{
		$AWIS_KEY1 = $_GET['QMP_KEY'];
	}
	if((isset($_POST['txtQMP_KEY']) and $Fehler > 0) or (isset($_POST['cmdHinzufuegen_x'])))
	{
		$AWIS_KEY1 = $_POST['txtQMP_KEY'];
	}
	if(isset($_POST['cmdHinzufuegen_x']))
	{
		if($_POST['txtQMZ_BESCHREIBUNG'] == '')
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_BEMERKUNG']);
			$Form->ZeileEnde();
			$Fehler++;
		}
		if($_POST['txtQMM_FRIST'] != '' and ((strtotime($_POST['txtQMM_FRIST']) < strtotime(date('d.m.Y',time())))))
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_FRIST']);
			$Form->ZeileEnde();
			$Fehler++;
		}
		if($_POST['txtQMM_FRIST'] == '')
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_FRISTEMP']);
			$Form->ZeileEnde();
			$Fehler++;
		}
		if((strtotime($_POST['txtQMM_FRIST']) > strtotime(date('d.m.Y',time()+(84*86400)))))
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_LANGFRIST']);
			$Form->ZeileEnde();
			$Fehler++;
		}
		if($Fehler == 0)
		{
			include('./qualitaetsmaengel_speichern.php');
			$Save = 1;
		}
	}

	$Bedingung = '';
	/*
	$SQL = 'select qms_folgestatus,qmp_key,qmp_qmk_key,qmp_fil_id,qmk_lang,qmp_erhebdat,qmp_ergebnis,frist,qms_bezeichnung,case when anz is null then 0 else anz end as Anz,';
	$SQL .='row_number() OVER (';
	// sonst pauschal nach Pr�fdatum
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select qmp.*,qmk.*,qms.*, ';
	$SQL .=' case when qms_folgestatus = 0 and qms_massnahme = 0';
	$SQL .='   then (select frist from(';
	$SQL .='           SELECT qmz_qmp_key,MIN(qmz_frist) AS frist';
	$SQL .='           FROM qmmassnahmenzuord qmz';
	$SQL .='           INNER JOIN qmmassnahmen qmm';
	$SQL .='           ON qmz.qmz_qmm_key = qmm.qmm_key';
	$SQL .='           INNER JOIN qmstatus qms';
	$SQL .='           ON qmz.qmz_status   = qms.qms_key';
	$SQL .='           AND qms_folgestatus is not null';
	$SQL .='           AND qms_massnahme   = 1';
	$SQL .='           GROUP BY qmz_qmp_key)';
	$SQL .='           WHERE qmz_qmp_key   = qmp.qmp_key) ';
	$SQL .='   else qmp_eskalationsdat+qms_frist ';
	$SQL .='   end as frist,';	
	$SQL .=' (select Anz from(';
	$SQL .='            SELECT qmz_qmp_key,count(qmz_key) AS Anz';
	$SQL .='            FROM qmmassnahmenzuord qmz';
	$SQL .='            INNER JOIN qmmassnahmen qmm';
	$SQL .='            ON qmz.qmz_qmm_key = qmm.qmm_key';
	$SQL .='            INNER JOIN qmstatus qms';
	$SQL .='            ON qmz.qmz_status   = qms.qms_key';
	$SQL .='            AND qms_massnahme   = 1';
	$SQL .='            GROUP BY qmz_qmp_key)';
	$SQL .='            WHERE qmz_qmp_key   = qmp.qmp_key) as Anz';
	$SQL .= ' from qmplaene qmp ';
	$SQL .= ' inner join qmkriterien qmk on qmp.qmp_qmk_key = qmk.qmk_key and qmk_aktiv = 1';
	$SQL .= ' inner join qmstatus qms on qmp.qmp_status = qms.qms_key)';
	*/
	
	/** Test-SQL Eskalationen **/
	
	$SQL = 'select qms_folgestatus,qmp_key,qmp_qmk_key,qmp_fil_id,qmk_lang,qmp_erhebdat,qmp_ergebnis,frist,qms_bezeichnung,case when anz is null then 0 else anz end as Anz,';
	$SQL .='row_number() OVER (';
	// sonst pauschal nach Pr�fdatum
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select qmp.*,qmk.*,qms.*, ';
	$SQL .=' case when qms_folgestatus = 0 and qms_massnahme = 0';
	$SQL .='   then (SELECT min(frist) as frist from(';
	$SQL .='           SELECT qmz_qmp_key,case when qms_frist != 0 then qmz_frist+qms_frist else qmz_frist end AS frist';
	$SQL .='           FROM qmmassnahmenzuord qmz';
	$SQL .='           INNER JOIN qmmassnahmen qmm';
	$SQL .='           ON qmz.qmz_qmm_key = qmm.qmm_key';
	$SQL .='           INNER JOIN qmstatus qms';
	$SQL .='           ON qmz.qmz_status   = qms.qms_key';
	$SQL .='           AND qms_folgestatus is not null';
	$SQL .='           AND qms_massnahme   = 1';
	$SQL .='           )';
	$SQL .='           WHERE qmz_qmp_key   = qmp.qmp_key) ';
	$SQL .='   else qmp_eskalationsdat+qms_frist ';
	$SQL .='   end as frist,';
	$SQL .=' (select Anz from(';
	$SQL .='            SELECT qmz_qmp_key,count(qmz_key) AS Anz';
	$SQL .='            FROM qmmassnahmenzuord qmz';
	$SQL .='            INNER JOIN qmmassnahmen qmm';
	$SQL .='            ON qmz.qmz_qmm_key = qmm.qmm_key';
	$SQL .='            INNER JOIN qmstatus qms';
	$SQL .='            ON qmz.qmz_status   = qms.qms_key';
	$SQL .='            AND qms_massnahme   = 1';
	$SQL .='            GROUP BY qmz_qmp_key)';
	$SQL .='            WHERE qmz_qmp_key   = qmp.qmp_key) as Anz';
	$SQL .= ' from qmplaene qmp ';
	$SQL .= ' inner join qmkriterien qmk on qmp.qmp_qmk_key = qmk.qmk_key and qmk_aktiv = 1';
	$SQL .= ' inner join qmstatus qms on qmp.qmp_status = qms.qms_key)';
	
	
	$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	if($AWIS_KEY1 != '')
	{
		$SQL .= ' AND qmp_key='.$AWIS_KEY1;
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsHRK = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['FilID'] = 50;
	$FeldBreiten['Kriterium'] = 250;
	$FeldBreiten['Erhebdat'] = 120;
	$FeldBreiten['Ergebnis'] = 100;
	$FeldBreiten['NEskalationsdat'] = 110;
	$FeldBreiten['Status'] = 260;
	$FeldBreiten['Anz'] = 150;
	$FeldBreiten['Hist'] = 40;
	$FeldBreiten['Aktion'] = 100;
	$FeldBreiten['Alt'] = 170;
	$FeldBreiten['Neu'] = 170;
	$FeldBreiten['User'] = 150;
	$FeldBreiten['Userdat'] = 150;
	
	$Form->SchreibeHTMLCode('<form name="frmQMMDetails" action="./qualitaetsmaengel_Main.php?cmdAktion=Details" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if($AWISBenutzer->BenutzerKontaktKEY() != 0 or ($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		if (($rsHRK->AnzahlDatensaetze() > 1))
		{
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
			//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
			// �berschrift der Listenansicht mit Sortierungslink: Filiale
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_FIL_ID'], $FeldBreiten['FilID'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_KRITERIUM'], $FeldBreiten['Kriterium'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_ERHEBDAT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_ERHEBDAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_ERHEBDAT'], $FeldBreiten['Erhebdat'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_ERGEBNIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_ERGEBNIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_ERGEBNIS'], $FeldBreiten['Ergebnis'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=FRIST'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FRIST'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_NESKALATION'], $FeldBreiten['NEskalationsdat'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMS_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMS_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_Status'], $FeldBreiten['Status'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_ANZ'], $FeldBreiten['Anz'], '', $Link);
			if(($Recht34000&8)==8)
			{
				// �berschrift der Listenansicht: History
				//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_HIST'], $FeldBreiten['Hist'], '', '');
			}
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
			while(! $rsHRK->EOF())
			{
				$Form->ZeileStart('','Zeile"ID="Zeile_'.$rsHRK->FeldInhalt('QMP_KEY'));
				$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&QMP_KEY=0'.$rsHRK->FeldInhalt('QMP_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$TTT = $rsHRK->FeldInhalt('QMP_FIL_ID');
				$Form->Erstelle_ListenFeld('QMP_FIL_ID', $rsHRK->FeldInhalt('QMP_FIL_ID'), 0, $FeldBreiten['FilID'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT =  $rsHRK->FeldInhalt('QMK_LANG');
				$Form->Erstelle_ListenFeld('QMK_LANG',$rsHRK->FeldInhalt('QMK_LANG'), 0, $FeldBreiten['Kriterium'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
				$TTT = $rsHRK->FeldInhalt('QMP_ERHEBDAT');
				$Form->Erstelle_ListenFeld('QMM_ERHEBDAT', $Form->Format('D',$rsHRK->FeldInhalt('QMP_ERHEBDAT')), 0, $FeldBreiten['Erhebdat'], false, ($DS%2),'','', 'T', 'L', $TTT);
				$TTT = $Form->Format('N2', $rsHRK->FeldInhalt('QMP_ERGEBNIS'));
				$Form->Erstelle_ListenFeld('QMM_ERGEBNIS', ($rsHRK->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ' ?$Form->Format('N2',$rsHRK->FeldInhalt('QMP_ERGEBNIS')).' %':$Form->Format('N8',$rsHRK->FeldInhalt('QMP_ERGEBNIS'))), 0, $FeldBreiten['Ergebnis'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsHRK->FeldInhalt('FRIST');
				$Form->Erstelle_ListenFeld('QMM_NESKALATIONSDAT',$Form->Format('D',$rsHRK->FeldInhalt('FRIST')), 0, $FeldBreiten['NEskalationsdat'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsHRK->FeldInhalt('QMS_BEZEICHNUNG');
				$Form->Erstelle_ListenFeld('QMS_BEZEICHNUNG',$rsHRK->FeldInhalt('QMS_BEZEICHNUNG'), 0, $FeldBreiten['Status'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsHRK->FeldInhalt('ANZ');
				$Form->Erstelle_ListenFeld('QMZ_ANZ',$rsHRK->FeldInhalt('ANZ'), 0, $FeldBreiten['Anz'], false, ($DS%2), '','', 'T', 'C', $TTT);
				/** Neue History erst mit VERSION 2
				 * 
				 */
				
				if(($Recht34000&8)==8)
				{
					$SQL  = ' Select * from QMHISTORY '; 
					$SQL .= ' where QMH_QMP_KEY='.$rsHRK->FeldInhalt('QMP_KEY');
					$SQL .= ' order by QMH_USERDAT';
					
					$rsQMH = $DB->RecordSetOeffnen($SQL);
					$Bild = '';
					
					if($rsQMH->AnzahlDatensaetze() > 0)
					{
						$Bild = '/bilder/attach.png';
						$Form->Erstelle_ListenBild('script','QMZ_HIST','onclick="ladeDiv('.$rsHRK->FeldInhalt('QMP_KEY').');"',$Bild,'',($DS%2),'',16,18,$FeldBreiten['Hist'],'C');
					}
					else 
					{
						$Form->Erstelle_ListenFeld('QMZ_HIST','', 0, $FeldBreiten['Hist'], false, ($DS%2), '','', 'T', 'C','');
					}				
				}
				$Form->ZeileEnde();
				$DS++;
				$rsHRK->DSWeiter();
			}
			
			if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
			{
				$Rand = 20;
			}
			else
			{
				$Rand = 10;
			}
			$Breite = $FeldBreiten['Aktion'] + $FeldBreiten['Alt']	+$FeldBreiten['Neu'] +$FeldBreiten['User'] +$FeldBreiten['Userdat']+$Rand+20;
			//DIV NOCH INS FRAMEWORK PACKEN!!!
			echo '<div id="Hist" style="display:none; position:absolute; border:5px outset #81bef7;border-radius:5px; z-index:1; background-color:#81bef7; top:100px; left:100px; width:'.$Breite.'px;">';
			echo '</div>';
			$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		}
		elseif(($rsHRK->AnzahlDatensaetze() == 1))
		{
			
			
			$SQL = 'SELECT *';
			$SQL .= 'FROM QMPLAENE';
			$SQL .= ' INNER JOIN QMSTATUS ON QMP_STATUS = QMS_KEY';
			$SQL .= ' WHERE QMP_KEY='.$rsHRK->FeldInhalt('QMP_KEY');
			
			$rsDetails = $DB->RecordSetOeffnen($SQL);
			
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./qualitaetsmaengel_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsDetails->FeldInhalt('QMP_USER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsDetails->FeldInhalt('QMP_USERDAT'));
			$Form->InfoZeile($Felder,'');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['UEBERSCHRIFT_QMM'] . ':', 800, 'font-weight:bolder');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			
			$Check = 0;
			
			$SQL = 'SELECT distinct qmk_lang,qmk_key ';
			$SQL .= 'FROM QMKRITERIEN';
			$SQL .= ' INNER JOIN QMKRITERIENGUELTIG ON QMK_KEY = QMG_QMK_KEY';
			$SQL .= ' WHERE QMK_AKTIV = 1 ';
			$SQL .= ' AND QMG_GUELTIGAB <=\''.$Form->Format('D',$rsDetails->FeldInhalt('QMP_ERHEBDAT')).'\'';
			$SQL .= ' AND QMG_GUELTIGBIS >=\''.$Form->Format('D',$rsDetails->FeldInhalt('QMP_ERHEBDAT')).'\'';
			$SQL .= ' order by qmk_lang ';
					
			$rsKrit = $DB->RecordSetOeffnen($SQL);
			
			$Zeile = 0;
			while(! $rsKrit->EOF())
			{
				if($rsDetails->FeldInhalt('QMP_QMK_KEY') == $rsKrit->FeldInhalt('QMK_KEY'))
				{
					$Check = 1;
				}
				else 
				{
					$Check = 0;
				}
				
				if($Zeile == 0)
				{
					$Form->ZeileStart();
				}	
				$Form->Erstelle_Checkbox('CHK_'.$rsKrit->FeldInhalt('QMK_LANG'),$Check,20,false,1);
				$Form->Erstelle_TextLabel($rsKrit->FeldInhalt('QMK_LANG'),240);
				if($Zeile == 1)
				{
					$Form->ZeileEnde();
					$Zeile = 0;
				}
				else
				{
					$Zeile++;
				}
				$rsKrit->DSWeiter();
			}
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['ERHEBDAT'] . ':',140);
			$Form->Erstelle_TextFeld('QMM_ERHEBDAT',$Form->Format('D',$rsDetails->FeldInhalt('QMP_ERHEBDAT')), 10, 120,false, '','', '','', 'L');
			
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_ERGEBNIS'] . ':',85);
			$Form->Erstelle_TextFeld('QMM_ERGEBNIS',($rsDetails->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ'?$Form->Format('N2',$rsDetails->FeldInhalt('QMP_ERGEBNIS')).' %':$Form->Format('N8',$rsDetails->FeldInhalt('QMP_ERGEBNIS'))), 10, 120,false, '','', '','', 'L');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_PLANSTATUS'] . ':',85);
			$Form->Erstelle_TextFeld('QMM_PLANSTATUS',$rsDetails->FeldInhalt('QMS_BEZEICHNUNG'), 50, 300,false, '','', '','', 'L');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Trennzeile('L');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			$SQL = 'Select * from( ';
			$SQL .='(SELECT distinct FIF_FIL_ID,FIL_BEZ,PER_NACHNAME || \', \' || PER_VORNAME as Name,FIF_FIT_ID';
			$SQL .='       FROM PERSONAL';
			$SQL .='       INNER JOIN FILIALINFOS ON FIF_WERT = PER_NR ';
			$SQL .='       INNER JOIN filialinfostypen2 ON FIT_ID = 74';
			$SQL .='       INNER JOIN FILIALEN ON FIL_ID = FIF_FIL_ID ';
			$SQL .=' WHERE FIF_FIT_ID = 74 AND FIF_IMQ_ID = 8 AND FIL_LAN_WWSKENN = \'BRD\'';
			$SQL .=' )';
			$SQL .=' UNION';
			$SQL .=' (';
			$SQL .=' SELECT distinct FIF_FIL_ID,FIL_BEZ,PER_NACHNAME || \', \' || PER_VORNAME as Name,FIF_FIT_ID';
			$SQL .='       FROM PERSONAL';
			$SQL .='       INNER JOIN FILIALINFOS ON FIF_WERT = PER_NR ';
			$SQL .='       INNER JOIN filialinfostypen2 ON FIT_ID = 76';
			$SQL .='       INNER JOIN FILIALEN ON FIL_ID = FIF_FIL_ID ';
			$SQL .=' WHERE FIF_FIT_ID = 76 AND FIF_IMQ_ID = 8 AND FIL_LAN_WWSKENN = \'BRD\'';
			$SQL .=' ))';
			$SQL .=' WHERE FIF_FIL_ID='.$rsDetails->FeldInhalt('QMP_FIL_ID');
			
			$rsFil = $DB->RecordSetOeffnen($SQL);
			
			$FIL_NAME = '';
			$GL = '';
			$WL = '';
			while(! $rsFil->EOF())
			{
				if($rsFil->FeldInhalt('FIF_FIT_ID') == 74)
				{
					$GL = $rsFil->FeldInhalt('NAME');
				}
				elseif($rsFil->FeldInhalt('FIF_FIT_ID') == 76)
				{
					$WL = $rsFil->FeldInhalt('NAME');
				}
				$rsFil->DSWeiter();
			}
			
			$SQL2 = 'Select * from Filialen where FIL_ID='.$rsDetails->FeldInhalt('QMP_FIL_ID');
			$rsFil2 = $DB->RecordSetOeffnen($SQL2);
			$FIL_NAME = $rsFil2->FeldInhalt('FIL_BEZ');
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FIL_NR'] . ':',140);
			$Form->Erstelle_TextFeld('QMM_FIL_NR',$rsDetails->FeldInhalt('QMP_FIL_ID'), 10,170,false, '','', '','', 'L');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FIL_ID'] . ':',85);
			$Form->Erstelle_TextFeld('QMM_FILIALE',$FIL_NAME, 10,200,false, '','', '','', 'L');
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_GL'] . ':',140);
			$Form->Erstelle_TextFeld('QMM_GL',$GL, 10, 170,false, '','', '','', 'L');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_WL'] . ':',85);
			$Form->Erstelle_TextFeld('QMM_WL',$WL, 10, 170,false, '','', '','', 'L');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Trennzeile('L');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			
			if(($Recht34000&2)==2 or ($Recht34000&4)==4 or ($Recht34000&8)==8)
			{
				$PersNr =isset($_POST['txtQMM_Persnr'])?$_POST['txtQMM_Persnr']:$rsDetails->FeldInhalt('QMP_PERSNR');
				
				if(!ctype_digit($PersNr))
				{
					$PersNr = '';
				}
				
				
				$SQL = 'SELECT Name || \',\' || Vorname as Name ';
				$SQL .= ' from personal_komplett where persnr =\''.$PersNr.'\'';
				$SQL .= ' and (trunc(datum_eintritt) <= trunc(sysdate)) and (datum_austritt is null or trunc(datum_austritt) >= trunc(sysdate))';
				
				$rsPers = $DB->RecordSetOeffnen($SQL);
				
				
				if(($rsDetails->FeldInhalt('QMS_FOLGESTATUS') != '' and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') != 0) and (($Recht34000&2)==2 or ($Recht34000&8)==8))
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FESTSTELLUNG'].':',400,'margin-top:5px');
					$Form->ZeileEnde();
					$Form->ZeileStart();

					$Form->Erstelle_Textarea('FESTSTELLUNG',isset($_REQUEST['txtFESTSTELLUNG'])?substr($_REQUEST['txtFESTSTELLUNG'],0,4000):substr($rsDetails->FeldInhalt('QMP_FESTSTELLUNG'),0,4000),500,80,3,true,'font: bold');
					$Form->ZeileEnde();
				
					$Form->ZeileStart();
					$Form->Trennzeile('L');
					$Form->ZeileEnde();
					
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_MASSNAHMEN'].':',400,'margin-top:5px');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$FeldBreiten = array();
					$FeldBreiten['Blank'] = 18;
					$FeldBreiten['Massnahme'] = 300;
					$FeldBreiten['Beschreibung'] = 280;
					$FeldBreiten['Frist'] = 120;
			
					$SQL ='select * from qmmassnahmenzuord ';
					$SQL .=' inner join qmmassnahmen on qmz_qmm_key = qmm_key';
					$SQL .=' where qmz_qmp_key ='.$rsHRK->FeldInhalt('QMP_KEY');
					$SQL .=' order by qmz_key';
					
					$rsMass = $DB->RecordSetOeffnen($SQL);
					
					$Form->ZeileStart();
					// �berschrift der Listenansicht mit Sortierungslink: Filiale
					$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
					//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_MASSNAHME'], $FeldBreiten['Massnahme'], '');
					// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
					//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_BESCHREIBUNG'], $FeldBreiten['Beschreibung'], '');
					// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
					//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_ERHEBDAT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_ERHEBDAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_FRIST'], $FeldBreiten['Frist'], '');
					$Form->ZeileEnde();
					$DS = 0;	// f�r Hintergrundfarbumschaltung
					
					while(! $rsMass->EOF())
					{
						$Form->ZeileStart();
						$Icons = array();
						//$Icons[] = array('delete','"./qualitaetsmaengel_Main.php?cmdAktion=Details&Del='.$rsMass->FeldInhalt('QMZ_KEY').'&QMP_KEY='.$rsHRK->FeldInhalt('QMP_KEY').'" onmouseover="urlanpassen(this);"');
						
						$Icons[] = array('delete','POST~'.$rsMass->FeldInhalt('QMZ_KEY'));
						$Form->Erstelle_ListeIcons($Icons,18,($DS%2));
						$TTT = $rsMass->FeldInhalt('QMM_BEZEICHNUNG');
						$Form->Erstelle_ListenFeld('QMM_BEZEICHNUNG', $rsMass->FeldInhalt('QMM_BEZEICHNUNG'), 0, $FeldBreiten['Massnahme'], false, ($DS%2), '','', 'T', 'L', $TTT);
						$TTT =  $rsMass->FeldInhalt('QMZ_BEMERKUNG');
						$Form->Erstelle_ListenFeld('QMZ_BEMERKUNG',strlen($rsMass->FeldInhalt('QMZ_BEMERKUNG')) >= 30? substr($rsMass->FeldInhalt('QMZ_BEMERKUNG'),0,30).'...':$rsMass->FeldInhalt('QMZ_BEMERKUNG'), 0, $FeldBreiten['Beschreibung'], false, ($DS%2),'','', 'T', 'L', $TTT);
						$TTT = $rsMass->FeldInhalt('QMZ_FRIST');
						$Form->Erstelle_ListenFeld('QMZ_FRIST', $Form->Format('D',$rsMass->FeldInhalt('QMZ_FRIST')), 0, $FeldBreiten['Frist'], false, ($DS%2),'','', 'T', 'L', $TTT);
						$Form->ZeileEnde();
						$DS++;
						$rsMass->DSWeiter();
					}
				
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
						
					$SQL ='select distinct qmm_key,qmm_bezeichnung from qmmassnahmen ';
					$SQL .=' left join qmmassnahmenzuord on qmz_qmp_key='.$rsHRK->FeldInhalt('QMP_KEY');
					$SQL .=' and qmz_qmm_key = qmm_key ';
					$SQL .=' where qmm_aktiv = 1';
					$SQL .=' and (qmz_key is null or qmz_qmm_key = 5)';
					$SQL .=' order by qmm_key';
						
					$rsList = $DB->RecordSetOeffnen($SQL);
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_MASSNAHME'] . ':',105);
					$Form->Erstelle_SelectFeld('QMM_MASSNAHME',$Save != 1 ?isset($_REQUEST['txtQMM_MASSNAHME'])?$_REQUEST['txtQMM_MASSNAHME']:'1':'1','',true,$SQL,'','','','');
					$Form->Schaltflaeche('image', 'cmdHinzufuegen','', '/bilder/icon_add.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'W','',18,18);
					$Form->ZeileEnde();
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_BESCHREIBUNG'] . ':',105,'','',$AWISSprachKonserven['QMM']['TTT_BEMERKUNG']);
					$Form->Erstelle_TextFeld('QMZ_BESCHREIBUNG',$Save != 1 ?isset($_REQUEST['txtQMZ_BESCHREIBUNG'])?$_REQUEST['txtQMZ_BESCHREIBUNG']:'':'', 30, 250,true, '','', '','', 'L');
					$Form->ZeileEnde();
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FRIST'] . ':',105);
					$Form->Erstelle_TextFeld('QMM_FRIST',$Save != 1 ?isset($_REQUEST['txtQMM_FRIST'])?$_REQUEST['txtQMM_FRIST']:'':'', 10, 170,true, '','', '','D', 'L','','',10);
					$Form->ZeileEnde();
					
					
					
					$Form->ZeileStart();
					$Form->Trennzeile('L');
					$Form->ZeileEnde();
					
					
					/*
					$Form->ZeileStart('position:relative; left:310px;');
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FRIST'],400,'margin-top:5px');
					$Form->ZeileEnde();
					
					
					$SQL = 'SELECT case when qmz_key is not null then 1 else 0 end as checked,qmz_frist,qmm_bezeichnung,qmm_key';
					$SQL .= ' FROM QMMASSNAHMEN ';
					$SQL .= ' LEFT JOIN QMMASSNAHMENZUORD on qmm_key = qmz_qmm_key and qmz_qmp_key='.$rsQMP->FeldInhalt('QMP_KEY');
					$SQL .= ' WHERE QMM_AKTIV = 1';
					$SQL .= ' AND QMM_STATUS = 0';
					$SQL .= ' ORDER BY QMM_KEY';
					
					$rsMass = $DB->RecordSetOeffnen($SQL);
						
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();	
					
					while(! $rsMass->EOF())
					{
						if(isset($_GET['QMP_KEY']))
						{
							$check = $rsMass->FeldInhalt('CHECKED');
							$FristTXT = $rsMass->FeldInhalt('QMZ_FRIST');
						}
						else 
						{
							$check = isset($_POST['txtCHK_Massnahme_'.$rsMass->FeldInhalt('QMM_KEY')]) or ($_POST['oldCHK_Massnahme_'.$rsMass->FeldInhalt('QMM_KEY')] == 1 and !isset($_POST['txtCHK_Massnahme_'.$rsMass->FeldInhalt('QMM_KEY')]))?0:1;
							$FristTXT = isset($_POST['txtCHK_Massnahme_'.$rsMass->FeldInhalt('QMM_KEY')])?$_POST['txtQMM_FRIST_'.$rsMass->FeldInhalt('QMM_KEY')]:'';
						}
						$Form->ZeileStart();
						$Form->Erstelle_Checkbox('CHK_Massnahme_'.$rsMass->FeldInhalt('QMM_KEY'),$check,40,true,1);
						$Form->Erstelle_TextLabel($rsMass->FeldInhalt('QMM_BEZEICHNUNG'),240);
						$Form->Erstelle_TextFeld('QMM_FRIST_'.$rsMass->FeldInhalt('QMM_KEY'),$FristTXT, 10, 170,true, '','', '','D', 'L','','',10);
						$Form->ZeileEnde();
						$rsMass->DSWeiter();
					}
					
					*/
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_PERSNR'] . ':',120);
					$Form->Erstelle_TextFeld('QMM_Persnr',isset($_REQUEST['txtQMM_Persnr'])?$_REQUEST['txtQMM_Persnr']:$rsDetails->FeldInhalt('QMP_PERSNR'), 10, 100,true, '','', '','', 'L','','',7);
					$Form->Erstelle_TextLabel($rsPers->FeldInhalt('NAME'),140);
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_Status'] . ':',120);
					$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_OffenMass']);
					$Form->Erstelle_SelectFeld('QMM_STATUS',isset($_REQUEST['txtQMM_STATUS'])?$_REQUEST['txtQMM_STATUS']:0,150,true,'','','','','',$Daten);
					$Form->ZeileEnde();
						
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();	
				}
				else
				{
					$SQL = 'SELECT case when qmz_key is not null then 1 else 0 end as checked,qmz_frist,qmm_bezeichnung,qmz_bemerkung,qms_bezeichnung,qmz_key,qmz_erfuellt,qms_folgestatus,qmz_qmm_key,qmz_user,qmz_userdat';
					$SQL .= ' FROM QMMASSNAHMENZUORD ';
					$SQL .= ' INNER JOIN QMMASSNAHMEN on qmz_qmm_key = qmm_key ';
					$SQL .= ' INNER JOIN QMSTATUS on qmz_status = qms_key ';
					$SQL .= '  where qmz_qmp_key='.$rsHRK->FeldInhalt('QMP_KEY');
					$SQL .= ' ORDER BY QMZ_KEY';
						
					$rsMass = $DB->RecordSetOeffnen($SQL);
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FESTSTELLUNG'].':',400,'margin-top:5px');
					$Form->ZeileEnde();
					$Form->ZeileStart();
					if((($Recht34000&8)==8) and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') != '')
					{
						$Form->Erstelle_Textarea('FESTSTELLUNG',$rsDetails->FeldInhalt('QMP_FESTSTELLUNG'),500,80,3,true,'font: bold');
					}
					else 
					{
						$Form->Erstelle_Textarea('FESTSTELLUNG',$rsDetails->FeldInhalt('QMP_FESTSTELLUNG'),500,80,3,false,'font: bold');
					}
					$Form->ZeileEnde();
						
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
						
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_MASSNAHMEN'].':',400,'margin-top:5px');
					$Form->ZeileEnde();
						
					$Form->ZeileStart('position:relative; left:245px;');
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_BESCHREIBUNG'],248,'margin-top:5px');
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FRIST'],92,'margin-top:5px');
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_Status'],220,'margin-top:5px');
					if((($Recht34000&4)==4 or ($Recht34000&8)==8) and ($rsDetails->FeldInhalt('QMS_FOLGESTATUS') == '0' or $rsDetails->FeldInhalt('QMS_FOLGESTATUS') == ''))
					{
						$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_ERFUELLT'],400,'margin-top:5px');
					}
					$Form->ZeileEnde();
					
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					while(! $rsMass->EOF())
					{
						$Form->ZeileStart();
						$Form->Erstelle_Checkbox('CHK_'.$rsMass->FeldInhalt('QMM_BEZEICHNUNG').'_'.$rsMass->FeldInhalt('QMM_KEY'),$rsMass->FeldInhalt('CHECKED'),40,false,1,'',$rsMass->FeldInhalt('QMZ_USER').' '.$rsMass->FeldInhalt('QMZ_USERDAT'));
						if((($Recht34000&8)==8) and ($rsMass->FeldInhalt('QMS_FOLGESTATUS') != '' and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') != ''))
						{
							$SQL2 ='select distinct qmm_key,qmm_bezeichnung,null,qmm_bezeichnung from qmmassnahmen ';
							$SQL2 .=' left join qmmassnahmenzuord on qmz_qmp_key='.$rsHRK->FeldInhalt('QMP_KEY');
							$SQL2 .=' and qmz_qmm_key = qmm_key ';
							$SQL2 .=' where qmm_aktiv = 1';
							//$SQL2 .=' and (qmz_key is null or qmz_qmm_key = 5)';
							$SQL2 .=' order by qmm_key';
							$Form->Erstelle_SelectFeld('QMZ_MASSNAHME_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_QMM_KEY'),'205:190',true,$SQL2);
						}
						else 
						{
							$Form->Erstelle_TextLabel(strlen($rsMass->FeldInhalt('QMM_BEZEICHNUNG')) >= 20? substr($rsMass->FeldInhalt('QMM_BEZEICHNUNG'),0,20).'...':$rsMass->FeldInhalt('QMM_BEZEICHNUNG'),205,'','',strlen($rsMass->FeldInhalt('QMM_BEZEICHNUNG')) >= 25?$rsMass->FeldInhalt('QMM_BEZEICHNUNG'):'');
						}
						if((($Recht34000&8)==8) and ($rsMass->FeldInhalt('QMS_FOLGESTATUS') != '' and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') != ''))
						{
							$Form->Erstelle_TextFeld('QMZ_BEMERKUNG_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_BEMERKUNG'),33,245,true, '','', '','T', 'L');
						}
						else 
						{
							$Form->Erstelle_TextLabel(strlen($rsMass->FeldInhalt('QMZ_BEMERKUNG')) >= 20? substr($rsMass->FeldInhalt('QMZ_BEMERKUNG'),0,20).'...':$rsMass->FeldInhalt('QMZ_BEMERKUNG'),245,'font-family:Courier New;','',strlen($rsMass->FeldInhalt('QMZ_BEMERKUNG')) >= 20?$rsMass->FeldInhalt('QMZ_BEMERKUNG'):'');
						}
						
						$Form->Erstelle_TextFeld('QMM_FRIST_'.$rsMass->FeldInhalt('QMM_KEY'),$rsMass->FeldInhalt('QMZ_FRIST'), 10, 95,false, '','', '','D', 'L');
						$Form->Erstelle_TextLabel($rsMass->FeldInhalt('QMS_BEZEICHNUNG'),220,'','','');
						//if(($Recht34000&4)==4 and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') == '0' and $rsMass->FeldInhalt('QMS_FOLGESTATUS') != '')
						//Editierung f�r GBL gleich Admin
						if((($Recht34000&8)==8 or ($Recht34000&4)==4) and $rsDetails->FeldInhalt('QMS_FOLGESTATUS') == '0' and $rsMass->FeldInhalt('QMS_FOLGESTATUS') != '')
						{
							$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_MassErfuellt']);
							$Form->Erstelle_SelectFeld('CMB_Erfuellt_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_ERFUELLT'),300,true,'','','','','',$Daten);
							
							//$Form->Erstelle_Checkbox('CHK_Erfuellt_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_ERFUELLT'),40,true,1);
						}
						else if((($Recht34000&8)==8 or ($Recht34000&4)==4 and $rsMass->FeldInhalt('QMS_FOLGESTATUS') == '')) 
						{
							$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_MassErfuellt']);
							$Form->Erstelle_SelectFeld('CMB_Erfuellt_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_ERFUELLT'),300,false,'','','','','',$Daten);
							//$Form->Erstelle_Checkbox('CHK_Erfuellt_'.$rsMass->FeldInhalt('QMZ_KEY'),$rsMass->FeldInhalt('QMZ_ERFUELLT'),40,false,1);
						}
						$Form->ZeileEnde();
						$rsMass->DSWeiter();
					}
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_PERSNR'] . ':',120);
					$Form->Erstelle_TextFeld('QMM_Persnr',$rsDetails->FeldInhalt('QMP_PERSNR'), 10, 120,false, '','', '','', 'L');
					$Form->Erstelle_TextLabel($rsPers->FeldInhalt('NAME'),140);
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}
				$Form->Erstelle_HiddenFeld('QMP_KEY',$rsHRK->FeldInhalt('QMP_KEY'));
				$Form->Erstelle_HiddenFeld('QMS_FOLGESTATUS',$rsDetails->FeldInhalt('QMS_FOLGESTATUS'));
			}	
		}
		else
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
	
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KONTAKT']);
		$Form->ZeileEnde();
	}
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if ((($Recht34000&2)==2 or ($Recht34000&8)==8) and ($rsHRK->AnzahlDatensaetze() == 1) and (($rsHRK->FeldInhalt('QMS_FOLGESTATUS') != '' and $rsHRK->FeldInhalt('QMS_FOLGESTATUS') != 0)))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	elseif((($Recht34000&4)==4 or ($Recht34000&8)==8) and ($rsHRK->AnzahlDatensaetze() == 1) and (($rsHRK->FeldInhalt('QMS_FOLGESTATUS') == '0') and $rsHRK->FeldInhalt('QMS_FOLGESTATUS') == '0'))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	if (($rsHRK->AnzahlDatensaetze() == 1) and (($rsHRK->FeldInhalt('QMS_FOLGESTATUS') == '0') || $rsHRK->FeldInhalt('QMS_FOLGESTATUS') == null))
	{//PDF Erzeugen
		$LinkPDF = '/berichte/drucken.php?XRE=46&ID='.base64_encode('&QMP_KEY='.$rsHRK->FeldInhalt('QMP_KEY').
				'&QMP_FIL_ID=' . $Param['QMM_FIL_ID'] .
				'&QMP_QMK_KEY=' . $Param['QMM_QMK_KEY'] .
				'%QMP_STATUS=' . $Param['QMM_STATUS']);
		$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_QMM',serialize($Param));
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	if(($Recht34000&4)==4)
	{
		/*
		$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
		$Bedingung .=' group by xx1_fil_id) ';
		*/

		//Neuer SQL f�r RGZ 14.01.2015 ST
		$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
		$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (23,25,57))';
		$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
		$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
		$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57))';
		$Bedingung .=' group by xx1_fil_id) ';
		
	}


	if(isset($Param['QMM_FIL_ID']) AND $Param['QMM_FIL_ID']!='' AND $Param['QMM_FIL_ID']!=0)
	{
		$Bedingung .= 'AND QMP_FIL_ID ' . $DB->LikeOderIst($Param['QMM_FIL_ID']) . ' ';
	}
	
	if(isset($Param['QMM_QMK_KEY']) AND $Param['QMM_QMK_KEY']!='' AND $Param['QMM_QMK_KEY'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
	{
		$Bedingung .= 'AND QMP_QMK_KEY ' . $DB->LikeOderIst($Param['QMM_QMK_KEY']) . ' ';
	}
	if(isset($Param['QMM_STATUS']) AND $Param['QMM_STATUS']!='' AND $Param['QMM_STATUS']!='0')
	{
		$Bedingung .= 'AND QMP_STATUS ' . $DB->LikeOderIst($Param['QMM_STATUS']) . ' ';
	}
	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET'] !='' AND $Param['FIL_GEBIET'] !='0')
	{
		if(($Recht34000&4)==4)
		{
			/*
			$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
			$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
			$Bedingung .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=' group by xx1_fil_id) ';
			*/
			
			//Neuer SQL f�r RGZ 14.01.2015 ST
			$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
			$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
			$Bedingung .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
			$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().' and xx1_fer_key in (23,25,57)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
			$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
			$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' group by xx1_fil_id) ';
			
		}
		elseif(($Recht34000&8)==8) 
		{
			//Fehler behoben G�ltigkeiten hinzugef�gt 14.01.2015 ST
			$Bedingung .= 'AND QMP_FIL_ID in( select fez_fil_id from filialebenenzuordnungen ';
			$Bedingung .=' where fez_feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=' and trunc(fez_gueltigab) <= trunc(sysdate)';
			$Bedingung .=' and trunc(fez_gueltigbis) >= trunc(sysdate))';
		}
	}

		
	
	return $Bedingung;
}

?>