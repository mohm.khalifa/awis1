<?php
global $BSW;
global $AWIS_KEY1;
try {

    if ($AWIS_KEY1 == -1) { //Neuer DS
        $BSW->DB->TransaktionBegin();

        $SQL = 'INSERT';
        $SQL .= ' INTO BSW';
        $SQL .= '   (';
        $SQL .= '     BSW_FIL_ID,';
        $SQL .= '     BSW_DATUM,';
        $SQL .= '     BSW_ZEIT,';
        $SQL .= '     BSW_BSA,';
        $SQL .= '     BSW_BONNR,';
        $SQL .= '     BSW_KARTENNUMMER,';
        $SQL .= '     BSW_UMSATZ,';
        $SQL .= '     BSW_MWST,';
        $SQL .= '     BSW_IMQ_ID,';
        $SQL .= '     BSW_USER,';
        $SQL .= '     BSW_USERDAT,';
        $SQL .= '     BSW_FIL_ANSPRECHPARTNER,';
        $SQL .= '     BSW_INFO_DURCH,';
        $SQL .= '     BSW_SONSTIGES';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['sucBSW_FIL_ID'], false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'D', $_POST['txtBSW_DATUM'], false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', '0000', false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', '01', false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_BONNR'], false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['sucBSW_KARTENNUMMER'], false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'N2', $_POST['txtBSW_UMSATZ'], false);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'N2', $_POST['txtBSW_MWST'], false);
        $SQL .= ',  4'; //manuell
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $BSW->AWISBenutzer->BenutzerName());
        $SQL .= ',  sysdate';
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_FIL_ANSPRECHPARTNER']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_INFO_DURCH']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_SONSTIGES']);
        $SQL .= '   )';

        $BSW->DB->Ausfuehren($SQL, '', true, $BSW->DB->Bindevariablen('BSW'));

        $SQL = 'SELECT seq_BSW_KEY.CurrVal AS KEY FROM DUAL';
        $rsKey = $BSW->DB->RecordSetOeffnen($SQL);
        $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');

        $SQL = 'INSERT';
        $SQL .= ' INTO BSWKUNDEN';
        $SQL .= '   (';
        $SQL .= '     BSK_TITEL,';
        $SQL .= '     BSK_NACHNAME,';
        $SQL .= '     BSK_VORNAME,';
        $SQL .= '     BSK_STRASSE,';
        $SQL .= '     BSK_POSTLEITZAHL,';
        $SQL .= '     BSK_ORT,';
        $SQL .= '     BSK_TELEFONNUMMER,';
        $SQL .= '     BSK_FAXNUMMER,';
        $SQL .= '     BSK_EMAIL,';
        $SQL .= '     BSK_GEBURTSDATUM,';
        $SQL .= '     BSK_KARTENNUMMER,';
        $SQL .= '     BSK_USER,';
        $SQL .= '     BSK_USERDAT';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_TITEL']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_NACHNAME']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_VORNAME']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_STRASSE']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_POSTLEITZAHL']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_ORT']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_TELEFONNUMMER']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_FAXNUMMER']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['txtBSK_EMAIL']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'D', $_POST['txtBSK_GEBURTSDATUM']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $_POST['sucBSW_KARTENNUMMER']);
        $SQL .= ',  ' . $BSW->DB->WertSetzen('BSK', 'T', $BSW->AWISBenutzer->BenutzerName());
        $SQL .= ' ,sysdate';
        $SQL .= '   )';
        try {
            $BSW->DB->Ausfuehren($SQL, '', true, $BSW->DB->Bindevariablen('BSK'));
        } catch (awisException $e) {
            //Wenn es den Kunden schon gibt, updaten, wenn nicht, Exception weiterreichen.
            if (strpos($e->getMessage(), 'IDX_BSK_KARTENNUMMER') !== false){
                $BSW->UpdateBSWKunden($_POST['sucBSW_KARTENNUMMER'],$_POST['txtBSK_TITEL'], $_POST['txtBSK_NACHNAME'],$_POST['txtBSK_VORNAME'],$_POST['txtBSK_STRASSE'],
                    $_POST['txtBSK_POSTLEITZAHL'],$_POST['txtBSK_ORT'], $_POST['txtBSK_TELEFONNUMMER'], $_POST['txtBSK_FAXNUMMER'],  $_POST['txtBSK_EMAIL'], $_POST['txtBSK_GEBURTSDATUM']);
            }else{
                throw new awisException($e->getMessage(),$e->getCode(),$e->getSQL(),$e->getKategorie());
            }

        }
        $BSW->DB->TransaktionCommit();

        $Meldung = $BSW->AWISSprachKonserven['BSW']['BSW_INSERT_OK'];
    } else { //gešnderter DS
        $SQL  ='UPDATE BSW';
        $SQL .=' SET ';
        $SQL .='  BSW_FIL_ID              = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['sucBSW_FIL_ID'], false);
        $SQL .=' , BSW_DATUM               = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_DATUM'], false);
        $SQL .=' , BSW_ZEIT                = '. $BSW->DB->WertSetzen('BSW', 'T','0000', false);
        $SQL .=' , BSW_BSA                 = '. $BSW->DB->WertSetzen('BSW', 'T',  '01', false);
        $SQL .=' , BSW_BONNR               = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_BONNR'], false);
        $SQL .=' , BSW_KARTENNUMMER        = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['sucBSW_KARTENNUMMER'], false);
        $SQL .=' , BSW_UMSATZ              = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_UMSATZ'], false);
        $SQL .=' , BSW_MWST                = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_MWST'], false);
        $SQL .=' , BSW_USER                = '. $BSW->DB->WertSetzen('BSW', 'T', $BSW->AWISBenutzer->BenutzerName(), false);
        $SQL .=' , BSW_USERDAT             = sysdate';
        $SQL .=' , BSW_FIL_ANSPRECHPARTNER = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_FIL_ANSPRECHPARTNER'], false);
        $SQL .=' , BSW_INFO_DURCH          = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_INFO_DURCH'], false);
        $SQL .=' , BSW_SONSTIGES           = '. $BSW->DB->WertSetzen('BSW', 'T', $_POST['txtBSW_SONSTIGES'], false);
        $SQL .=' WHERE BSW_KEY               = '. $BSW->DB->WertSetzen('BSW', 'T', $AWIS_KEY1, false);

        $BSW->DB->Ausfuehren($SQL, '', true, $BSW->DB->Bindevariablen('BSW'));

        $BSW->UpdateBSWKunden($_POST['sucBSW_KARTENNUMMER'],$_POST['txtBSK_TITEL'], $_POST['txtBSK_NACHNAME'],$_POST['txtBSK_VORNAME'],$_POST['txtBSK_STRASSE'],
            $_POST['txtBSK_POSTLEITZAHL'],$_POST['txtBSK_ORT'], $_POST['txtBSK_TELEFONNUMMER'], $_POST['txtBSK_FAXNUMMER'],  $_POST['txtBSK_EMAIL'], $_POST['txtBSK_GEBURTSDATUM']);

        $Meldung = $BSW->AWISSprachKonserven['BSW']['BSW_UPDATE_OK'];
    }
    if ($Meldung) {
        $BSW->Form->Hinweistext($Meldung);
    }
} catch (awisException $ex) {
    if (strpos($ex->getMessage(), 'UID_BSW_DATUM_BONNR_KART') !== false) {
        $BSW->Form->ZeileStart();
        $BSW->Form->Hinweistext($BSW->AWISSprachKonserven['BSW']['BSW_ERR_DS_BEREITS_VORHANDEN']);
        $BSW->Form->ZeileEnde();
    } else {
        $Information = '<br>Zeile: ' . $ex->getLine();
        $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
        $Information .= '<br>SQL: ' . $ex->getSQL() . '<br>';
        ob_start();
        var_dump($_REQUEST);
        $Information .= ob_get_clean();
        $BSW->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
        $BSW->Form->DebugAusgabe(1, $ex->getSQL());
    }
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $BSW->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>