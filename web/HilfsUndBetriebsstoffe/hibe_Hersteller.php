<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	HilfsUndBetriebsstoffGruppen
* 
* 	Pflege der Hersteller
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt 2004
* 
****************************************************************************************************/

$Rechtestufe = awisBenutzerRecht($con, 1103);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		//  8=L�schen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Hersteller',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($_POST);
echo '<form name=frmHBG method=post action=./hibe_Main.php?cmdAktion=Hersteller>';

/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM HERSTELLER WHERE HER_ID=0" . $_POST["txtHER_ID_old"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
//			var_dump($SQL);
			
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Die Gruppe wurde erfolgreich gel�scht<br><br><a href=./hibe_Main.php?cmdAktion=Hersteller&Liste=True>Weiter</a>');
	}
	elseif(empty($_POST["cmdLoeschAbbruch"]))
	{
		print "<form name=frmHBG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHER_ID_old value=" . $_POST["txtHER_ID_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Hilfs- und Betriebsstoffgruppe l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}


if(isset($_POST['cmdSpeichern_x']))
{
	$SQL = '';
	
	if($_POST['txtHER_ID_old']=='')
	{
		$SQL = "INSERT INTO Hersteller";
		$SQL .= "(HER_ID, HER_BEZEICHNUNG, HER_KONZERN, HER_TYP, HER_IMQ_ID, HER_USER, HER_USERDAT)";
		$SQL .= "VALUES('" . $_POST['txtHER_ID'] . "'";
		$SQL .= ",'" . $_POST['txtHER_BEZEICHNUNG'] . "'";
		$SQL .= ",'" . $_POST['txtHER_KONZERN'] . "'";
		$SQL .= ",4";
		$SQL .= ",4";
		$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ",SYSDATE)";
	}
	else
	{
		$Aenderungen = '';
		If($_POST['txtHER_BEZEICHNUNG'] != $_POST['txtHER_BEZEICHNUNG_old'])
		{
			$Aenderungen = ", HER_BEZEICHNUNG = '" . $_POST['txtHER_BEZEICHNUNG'] . "'";
		}
		If($_POST['txtHER_KONZERN'].'-' != $_POST['txtHER_KONZERN_old'].'-')
		{
			$Aenderungen .= ", HER_KONZERN = '" . $_POST['txtHER_KONZERN'] . "'";
		}
	
		If($Aenderungen!='')
		{
			$SQL = "UPDATE HERSTELLER SET " . substr($Aenderungen,2);
			$SQL .= ", HER_USER='" . $AWISBenutzer->BenutzerName() . "', HER_USERDAT=SYSDATE";
			$SQL .= " WHERE HER_ID=0" . $_POST['txtHER_ID_old'];
		}
	}	// Neu oder �ndern
//var_dump($SQL);
	if($SQL != '')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}	
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************
		
if((!isset($_GET['HER_ID']) and !isset($_POST['txtHER_ID'])) or isset($_POST['cmdTrefferListe_x']))
{
	$SQL = "SELECT * FROM Hersteller WHERE BITAND(HER_TYP,4)=4";
}
else
{
	$SQL = "SELECT * FROM Hersteller WHERE HER_ID=0". (isset($_GET['HER_ID'])?$_GET['HER_ID']:'') . (isset($_POST['txtHER_ID'])?$_POST['txtHER_ID']:'') ."";
}

if(isset($_GET['Sort']))
{
	$SQL .= " ORDER BY " . $_GET['Sort'];
}
else
{
	$SQL .= " ORDER BY HER_BEZEICHNUNG";
}

//$rsHER ='';

if(!isset($_GET['Hinzufuegen']))
{
	$rsHER = awisOpenRecordset($con, $SQL);
	$rsHERZeilen = $awisRSZeilen;
}
else
{
	$rsHERZeilen=-1;
}


	//**************************************************
	// Daten ausgeben
	//**************************************************
	
if($rsHERZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td width=100 id=FeldBez><a href=./hibe_Main.php?cmdAktion=Hersteller&Sort=HER_ID>Nummer</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Hersteller&Sort=HER_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Hersteller&Sort=HER_KONZERN>Konzern</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHERZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./hibe_Main.php?cmdAktion=Hersteller&HER_ID=" . $rsHER['HER_ID'][$Zeile] . ">" . $rsHER['HER_ID'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHER['HER_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHER['HER_KONZERN'][$Zeile] . "</td>";
		
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{

	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Hersteller f�r Hilfs- und Betriebsstoffgruppen</h3></td>';
	echo '<td><font size=1>' . (isset($rsHER['HER_USER'][0])?$rsHER['HER_USER'][0]:'') . '<br>' . (isset($rsHER['HER_USERDAT'][0])?$rsHER['HER_USERDAT'][0]:'') . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./hibe_Main.php?cmdAktion=Hersteller&Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=*></colgroup>';

		// Bezeichnung
	echo '<tr>';
	echo '<td id=FeldBez>ID</td>';
	if(($Rechtestufe&2)==2 AND (!isset($rsHER['HER_ID'][0]) or $rsHER['HER_ID'][0]==""))
	{
		echo "<td><input name=txtHER_ID size=10 value='" . (isset($rsHER['HER_ID'][0])?$rsHER['HER_ID'][0]:'') . "'>";
	}
	else
	{
		echo '<td>' . (isset($rsHER['HER_ID'][0])?$rsHER['HER_ID'][0]:'') . '';
	}	
	echo "<input type=hidden name=txtHER_ID_old value='" . (isset($rsHER['HER_ID'][0])?$rsHER['HER_ID'][0]:'') . "'></td>";
	echo '</tr>';
	

			// Zeile 
	echo '<tr>';
	echo '<td id=FeldBez>Bezeichnung</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHER_BEZEICHNUNG size=40 value='" . (isset($rsHER['HER_BEZEICHNUNG'][0])?$rsHER['HER_BEZEICHNUNG'][0]:'') . "'>";
		echo "<input type=hidden name=txtHER_BEZEICHNUNG_old value='" . (isset($rsHER['HER_BEZEICHNUNG'][0])?$rsHER['HER_BEZEICHNUNG'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHER['HER_BEZEICHNUNG'][0])?$rsHER['HER_BEZEICHNUNG'][0]:'') . '</td>';
	}
	echo '</tr>';

			// Zeile 
	echo '<tr>';
	echo '<td id=FeldBez>Konzern</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHER_KONZERN size=40 value='" . (isset($rsHER['HER_KONZERN'][0])?$rsHER['HER_KONZERN'][0]:'') . "'>";
		echo "<input type=hidden name=txtHER_KONZERN_old value='" . (isset($rsHER['HER_KONZERN'][0])?$rsHER['HER_KONZERN'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHER['HER_KONZERN'][0])?$rsHER['HER_KONZERN'][0]:'') . '</td>';
	}
	echo '</tr>';

	echo '</table>';

	if($Rechtestufe>1)
	{
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}
//		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}

	echo '</form>';
				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHER_ID\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
* 
* */

if(isset($_POST['txtZurueck']) && $_POST['txtZurueck']!='')
{
	$ZurueckLink = $_POST['txtZurueck'];
}
else
{
	if(isset($_GET['Zurueck']) && $_GET['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif(!isset($_GET['Zurueck']))
	{
		$ZurueckLink = '/index.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $_GET['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

if(!isset($_GET['Hinzufuegen']))
{
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./hibe_Main.php?cmdAktion=Hersteller&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}

echo '</form>';

?>