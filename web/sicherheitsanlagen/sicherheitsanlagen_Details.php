<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $BMV;

try {
    //*************************************************
    // Setzen von AWIS_KEY1
    //*************************************************
    if(isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
    }elseif(isset($_GET['BMV_KEY'])) {
        $AWIS_KEY1 = $_GET['BMV_KEY'];
    }
    elseif(isset($_POST['txtBMV_KEY'])) {
        $AWIS_KEY1 = $_POST['txtBMV_KEY'];
    }

    if(isset($_POST['cmdDSWeiter_x'])){
        $BMV->Param['BMV_FIL_ID'] = $BMV->DSButton_AWISKEY($AWIS_KEY1,'>');
        $AWIS_KEY1 = '';
    }elseif(isset($_POST['cmdDSZurueck_x'])){
        $BMV->Param['BMV_FIL_ID'] = $BMV->DSButton_AWISKEY($AWIS_KEY1,'<');
        $AWIS_KEY1 = '';
    }elseif(isset($_POST['txtSS_Fil_ID']) and $_POST['txtSS_Fil_ID'] != ''){
        $BMV->Param['BMV_FIL_ID'] = $_POST['txtSS_Fil_ID'];
        $AWIS_KEY1 = '';
    }


    $BMV->Param['KEY']  = $AWIS_KEY1;

    //*************************************************
    // Setzen von AWIS_KEY2
    //*************************************************
    if (isset($_POST['txtBMR_KEY'])) {
        $AWIS_KEY2 = $_POST['txtBMR_KEY'];
    } elseif(isset($_GET['BMR_KEY'])) {
        $AWIS_KEY2 = $_GET['BMR_KEY'];
    }else{
        $AWIS_KEY2 = '';
    }

    //********************************************************
    // Parameter setzen und Seiten inkludieren
    //********************************************************
    if(isset($_POST['cmdSuche_x'])) {
        $BMV->Param['BMK_KATEGORIE']= $BMV->Form->Format('T',$_POST['sucBMK_KATEGORIE'],true);
        $BMV->Param['BMV_FIL_ID'] = 	$BMV->Form->Format('T',$_POST['sucBMV_FIL_ID'],true);
        $BMV->Param['BMV_VERTRAGSNR']= $BMV->Form->Format('T',$_POST['sucBMV_VERTRAGSNR'],true);
        $BMV->Param['BMV_TNNL']= $BMV->Form->Format('T',$_POST['sucBMV_TNNL'],true);
        $BMV->Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
        $BMV->Param['Block']=1;
        $BMV->Param['ORDER']='BMV_VERTRAGSNR';
    }elseif (isset($_POST['cmdSpeichern_x'])){
        include('sicherheitsanlagen_speichern.php');
    }elseif(isset($_GET['DEL']) or isset($_POST['cmdLoeschen_x']) or isset($_POST['cmdLoeschenOK'])) {
        include('sicherheitsanlagen_loeschen.php');
    }else{ //User hat den Reiter gewechselt.
        $AWIS_KEY1 = $BMV->Param['KEY'];
    }


    //*********************************************************
    //* SQL Vorbereiten: Sortierung
    //*********************************************************
    if(!isset($_GET['Sort'])) {
        $BMV->Param['ORDER'] = ' BMV_VERTRAGSNR';
    }
    else {
        $BMV->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //********************************************************
    // SQL Vorbereiten: Bedingung
    //********************************************************
    $Bedingung = $BMV->BedingungErstellen();       // mit dem Rest

    //********************************************************
    // SQL der Detailsseite
    //********************************************************

    $SQL  ='select ';
    $SQL .=' BMV_KEY';
    $SQL .=' ,BMV_FIL_ID';
    $SQL .=' ,BMV_VERTRAGSNR';
    $SQL .=' ,BMV_MIETE';
    $SQL .=' ,BMV_MIETFREI';
    $SQL .=' ,BMV_TNNL';
    $SQL .=' ,BMV_KENNUNG';
    $SQL .=' ,BMV_LAUFZEITENDE';
    $SQL .=' ,BMK_KATEGORIE';
    $SQL .= ' ,BMK_BEREICH';
    $SQL .=' ,BMV_ZENTRALBEREICH';
    $SQL .= ' ,BMV_BMK_KEY';
    $SQL .=' ,BMV_USER';
    $SQL .=' ,BMV_USERDAT';
    $SQL .=' ,row_number() OVER (';
    $SQL .='  order by ' . $BMV->Param['ORDER'] . ') as ZEILENNR';
    $SQL .=' from BMAKATEGORIE inner join BMAVERTRAEGE';
    $SQL .=' on BMK_KEY = BMV_BMK_KEY';
    if($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    //********************************************************
    // SQL Nachbereiten: Blätternfunktion
    //********************************************************
    if ($AWIS_KEY1 == '') { //Liste?
        if (isset($_REQUEST['Block'])) { //Wurde geblättert?
            $Block = $BMV->Form->Format('N0', $_REQUEST['Block'], false);
            $BMV->Param['Block'] = $Block;
        } elseif (isset($BMV->Param['Block'])) { //Zurück zur Liste, Tab gewechselt..
            $Block = $BMV->Param['Block'];
        }

        $ZeilenProSeite = $BMV->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $BMV->DB->ErmittleZeilenAnzahl($SQL, $BMV->DB->Bindevariablen('BMV', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $BMV->DB->WertSetzen('BMV', 'N0', $StartZeile);
        $SQL .= ' AND  ZeilenNr<' . $BMV->DB->WertSetzen('BMV', 'N0', ($StartZeile + $ZeilenProSeite));
    }


    //********************************************************
    // Fertigen SQL ausführen
    //********************************************************
    $rsBMV = $BMV->DB->RecordSetOeffnen($SQL,$BMV->DB->Bindevariablen('BMV',true));
    $BMV->Form->DebugAusgabe(1, $BMV->DB->LetzterSQL());

    //********************************************************
    // Anzeige Start
    //********************************************************

    $BMV->Form->SchreibeHTMLCode('<form name=frmDetails action=./sicherheitsanlagen_Main.php?cmdAktion='.$_GET['cmdAktion'].
        (isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'')
        .(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

    $BMV->Form->Formular_Start();


    if($rsBMV->EOF() AND $AWIS_KEY1!=-1) {
        $BMV->Form->Hinweistext($BMV->AWISSprachKonserven['Fehler']['err_keineDaten']);
    }elseif ($rsBMV->AnzahlDatensaetze() > 1) { // Liste anzeigen

        // Spaltenbreiten fuer Listenansicht
        $FeldBreiten = array();
        $FeldBreiten['BMK_KATEGORIE'] = 250;
        $FeldBreiten['BMV_VERTRAGSNR'] = 150;
        $FeldBreiten['BMV_FIL_ID'] = 100;
        $FeldBreiten['BMV_ZENTRALBEREICH'] = 150;
        $FeldBreiten['BMV_TNNL'] = 150;

        $BMV->Form->ZeileStart();
        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&Sort=BMV_VERTRAGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BMV_VERTRAGSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMV']['BMV_VERTRAGSNR'], $FeldBreiten['BMV_VERTRAGSNR'], '', $Link);

        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&Sort=BMK_KATEGORIE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BMK_KATEGORIE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMK']['BMK_KATEGORIE'], $FeldBreiten['BMK_KATEGORIE'], '', $Link);

        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&Sort=BMV_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BMV_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMV']['BMV_FIL_ID'], $FeldBreiten['BMV_FIL_ID'], '', $Link);

        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&Sort=BMV_ZENTRALBEREICH'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BMV_ZENTRALBEREICH'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMV']['BMV_ZENTRALBEREICH'], $FeldBreiten['BMV_ZENTRALBEREICH'], '', $Link);

        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&Sort=BMV_TNNL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BMV_TNNL'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMV']['BMV_TNNL'], $FeldBreiten['BMV_TNNL'], '', $Link);

        $BMV->Form->ZeileEnde();
        $DS = 0;	// fuer Hintergrundfarbumschaltung

        while(! $rsBMV->EOF()) {
            $BMV->Form->ZeileStart();

            $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details&BMV_KEY=' . $rsBMV->FeldInhalt('BMV_KEY') .  (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $TTT =  $rsBMV->FeldInhalt('BMV_VERTRAGSNR');
            $BMV->Form->Erstelle_ListenFeld('BMV_VERTRAGSNR',$rsBMV->FeldInhalt('BMV_VERTRAGSNR'), 0, $FeldBreiten['BMV_VERTRAGSNR'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);

            $TTT = $rsBMV->FeldInhalt('BMK_KATEGORIE');
            $BMV->Form->Erstelle_ListenFeld('BMK_KATEGORIE', $rsBMV->FeldInhalt('BMK_KATEGORIE'), 0, $FeldBreiten['BMK_KATEGORIE'], false, ($DS%2), '','', 'T', 'L', $TTT);

            $TTT =  $rsBMV->FeldInhalt('BMV_FIL_ID');
            $BMV->Form->Erstelle_ListenFeld('BMV_FIL_ID',($rsBMV->FeldInhalt('BMV_FIL_ID')!=0?$rsBMV->FeldInhalt('BMV_FIL_ID'):''), 0, $FeldBreiten['BMV_FIL_ID'], false, ($DS%2), '','', 'T', 'L', $TTT);

            $TTT =  $rsBMV->FeldInhalt('BMV_ZENTRALBEREICH');
            $BMV->Form->Erstelle_ListenFeld('BMV_ZENTRALBEREICH',$rsBMV->FeldInhalt('BMV_ZENTRALBEREICH'), 0, $FeldBreiten['BMV_ZENTRALBEREICH'], false, ($DS%2), '','', 'T', 'L', $TTT);

            $TTT =  $rsBMV->FeldInhalt('BMV_TNNL');
            $BMV->Form->Erstelle_ListenFeld('BMV_TNNL',$rsBMV->FeldInhalt('BMV_TNNL'), 0, $FeldBreiten['BMV_TNNL'], false, ($DS%2), '','', 'T', 'L', $TTT);

            $BMV->Form->ZeileEnde();
            $DS++;
            $rsBMV->DSWeiter();
        }
        $Link = './sicherheitsanlagen_Main.php?cmdAktion=Details';
        $BMV->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

    }else{ //Detailansicht

        $AWIS_KEY1 = ($rsBMV->AnzahlDatensaetze()==1?$rsBMV->FeldInhalt('BMV_KEY'):-1);
        $BMV->Form->Erstelle_HiddenFeld('BMV_KEY',$AWIS_KEY1);

        $LabelBreite = 200;

        if(($BMV->Recht50000&2)==2) {
            $Aendernrecht = true;
        }else {
            $Aendernrecht = false;
        }

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./sicherheitsanlagen_Main.php?cmdAktion=Details accesskey=T title='".$BMV->AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsBMV->FeldInhalt('BMV_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsBMV->FeldInhalt('BMV_USERDAT'));
        $BMV->Form->InfoZeile($Felder,'');

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMK']['BMK_KATEGORIE'] . ':', $LabelBreite);
        $SQL = 'select BMK_KEY, BMK_KATEGORIE from BMAKATEGORIE '. ($AWIS_KEY1>0?'where bmk_bereich = \''.$rsBMV->FeldInhalt('BMK_BEREICH').'\'':'') . ' order by BMK_KATEGORIE';
        $BMV->Form->Erstelle_SelectFeld('BMV_BMK_KEY',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_BMK_KEY'):''),400,$Aendernrecht,$SQL);
        $BMV->Form->ZeileEnde();

        if($AWIS_KEY1 == -1) {

            $Block = 'BMV_FIL_ID';
            $BMV->Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
            $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV'][$Block] . ':',$LabelBreite);
            $BMV->Form->AuswahlBox('!'.$Block,'box1','','BMV_Daten','', 65, 170, ($AWIS_KEY1!=-1?$rsBMV->FeldInhalt($Block):''),'T',$Aendernrecht,'','','',0,'','','',0,'','autocomplete=off');
            $BMV->Form->ZeileEnde();

            $BMV->Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
            $BMV->Form->AuswahlBoxHuelle('box1');
            $BMV->Form->ZeileEnde();

            $Block = 'BMV_ZENTRALBEREICH';
            $BMV->Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
            $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV'][$Block] . ':',$LabelBreite);
            $BMV->Form->Erstelle_TextFeld('!'.$Block,($AWIS_KEY1!=-1?$rsBMV->FeldInhalt($Block):''),65,170,$Aendernrecht);
            $BMV->Form->ZeileEnde();

            echo '<script type="text/javascript">'.PHP_EOL;

            echo '$( document ).ready(function() {'.PHP_EOL;
            echo '  blenden();'.PHP_EOL;
            echo ' toggleRequire(); '.PHP_EOL;
            echo '});'.PHP_EOL;

            echo '$("#txtBMV_BMK_KEY").change(function(){'.PHP_EOL;
            echo '  blenden();'.PHP_EOL;
            echo ' toggleRequire(); '.PHP_EOL;
            echo '})'.PHP_EOL;

            echo 'function blenden() {'.PHP_EOL;
            echo 'if($("#txtBMV_BMK_KEY").val()==4)'.PHP_EOL;
            echo '{'.PHP_EOL;
            echo '  $(".BlockBMV_ZENTRALBEREICH").css({"visibility": "visible"});'.PHP_EOL;
            echo '  $(".BlockBMV_ZENTRALBEREICH").show();'.PHP_EOL;
            echo '  $(".BlockBMV_FIL_ID").css({"visibility": "hidden"});'.PHP_EOL;
            echo '  $(".BlockBMV_FIL_ID").hide();'.PHP_EOL;
            echo '}else'.PHP_EOL;
            echo '{'.PHP_EOL;
            echo '  $(".BlockBMV_ZENTRALBEREICH").css({"visibility": "hidden"});'.PHP_EOL;
            echo '  $(".BlockBMV_ZENTRALBEREICH").hide();'.PHP_EOL;
            echo '  $(".BlockBMV_FIL_ID").css({"visibility": "visible"});'.PHP_EOL;
            echo '  $(".BlockBMV_FIL_ID").show();'.PHP_EOL;
            echo '}';
            echo '};'.PHP_EOL;

            echo 'function toggleRequire(){ ' . PHP_EOL;
            echo '  if($("#txtBMV_BMK_KEY").val()==4){'.PHP_EOL;
            echo '      $("#sucBMV_FIL_ID").prop("required",false);' . PHP_EOL;
            echo '      $("#txtBMV_ZENTRALBEREICH").prop("required",true);' . PHP_EOL;
            echo '   }else{ ';
            echo '      $("#txtBMV_ZENTRALBEREICH").prop("required",false);' . PHP_EOL;
            echo '      $("#sucBMV_FIL_ID").prop("required",true);' . PHP_EOL;
            echo '   }';
            echo ' } ';
            echo '</script>'.PHP_EOL;
        }

        echo '<script type="text/javascript">'.PHP_EOL;
        echo '$(document).ready(function(){';
        echo '  key_BMV_FIL_ID(this);';
        echo '});';
        echo '</script>'.PHP_EOL;

        if($rsBMV->FeldInhalt('BMK_BEREICH') == 'FIL' and $AWIS_KEY1 > 0) {
            $BMV->Form->ZeileStart();
            $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_FIL_ID'] . ':',$LabelBreite);
            $BMV->Form->AuswahlBox('!'.'BMV_FIL_ID','box1','','BMV_Daten','', 65, 170, ($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_FIL_ID'):''),'T',$Aendernrecht,'','','',0,'','','',0,'','autocomplete=off');
            $BMV->Form->ZeileEnde();

            $BMV->Form->ZeileStart();
            $BMV->Form->AuswahlBoxHuelle('box1');
            $BMV->Form->ZeileEnde();
        }

        if($rsBMV->FeldInhalt('BMK_BEREICH') != 'FIL' and $AWIS_KEY1 > 0) {
            $BMV->Form->ZeileStart();
            $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_ZENTRALBEREICH'] . ':',$LabelBreite);
            $BMV->Form->Erstelle_TextFeld('BMV_ZENTRALBEREICH',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_ZENTRALBEREICH'):''),65,170,$Aendernrecht);
            $BMV->Form->ZeileEnde();
        }

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_KENNUNG'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('BMV_KENNUNG',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_KENNUNG'):''),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_LAUFZEITENDE'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('BMV_LAUFZEITENDE',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_LAUFZEITENDE'):''),10,150,$Aendernrecht,'','','','D');
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_MIETE'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('BMV_MIETE',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_MIETE','N2'):''),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_MIETFREI'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('BMV_MIETFREI',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_MIETFREI'):''),10,170,$Aendernrecht,'','','','D');
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_TNNL'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('BMV_TNNL',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_TNNL'):''),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMV']['BMV_VERTRAGSNR'] . ':',$LabelBreite);
        $BMV->Form->Erstelle_TextFeld('!BMV_VERTRAGSNR',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMV_VERTRAGSNR'):''),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->Erstelle_HiddenFeld('BMV_KEY',$AWIS_KEY1);
        $BMV->Form->Erstelle_HiddenFeld('BMK_BEREICH',($AWIS_KEY1!=-1?$rsBMV->FeldInhalt('BMK_BEREICH'):''));

    }

    $BMV->Form->Formular_Ende();

    if($AWIS_KEY1 > 0) {
        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));

        $RegDet = new awisRegister(50010);
        $RegDet->ZeichneRegister($RegisterSeite);
    }

    $BMV->Form->SchaltflaechenStart();
    $BMV->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $BMV->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if(($BMV->Recht50000&8)== 8 and $AWIS_KEY1 == ''){ //Neu nur bei Liste
        $BMV->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $BMV->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    if ($AWIS_KEY1 != '' AND ($BMV->Recht50000&2) == 2){ //Speichern immer wenn keine Liste
        $BMV->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $BMV->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (($BMV->Recht50000 & 4) == 4 AND $AWIS_KEY1 > 0){ //Löschen immer, wenn Detaildatensatz
        $BMV->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $BMV->AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
    }

    if ($AWIS_KEY1 != ''){
        $BMV->Form->Erstelle_TextLabel('',40);
        $BMV->Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $BMV->AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'S');
        $BMV->Form->Erstelle_TextFeld('SS_Fil_ID','',3,35,true,'margin-top: 7px;');
        $BMV->Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $BMV->AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'S');
    }



    $BMV->Form->SchaltflaechenEnde();
    $BMV->Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex) {
    if($BMV->Form instanceof awisFormular) {
        $BMV->Form->DebugAusgabe(1, $ex->getSQL());
        $BMV->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex) {
    if($BMV->Form instanceof awisFormular) {
        $BMV->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $BMV->Param
 * @return string
 */
function _BedingungErstellen() {



}