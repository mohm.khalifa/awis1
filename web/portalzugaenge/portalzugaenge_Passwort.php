<?php
/**
 * Passwort zur�cksetzen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091008
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PZU','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','HinweisZuruecksetzen');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht2100=$AWISBenutzer->HatDasRecht(2100);		// Rechte des Mitarbeiters
	if($Recht2100==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./portalzugaenge_Main.php?cmdAktion=Passwort>");
	$Form->Formular_Start();

	$NeueMaskeZeigen = true;

	if(isset($_POST['sucPZU_BENUTZERNAME']))
	{
		$SQL = 'SELECT portalzugaenge.*';
		$SQL .= ' FROM portalzugaenge';
		$SQL .= ' WHERE UPPER(PZU_BENUTZERNAME) = ' .  $DB->FeldInhaltFormat('TU',$_POST['sucPZU_BENUTZERNAME']);
		$SQL .= ' AND PZU_PERSNR = ' . $DB->FeldInhaltFormat('N0',$_POST['sucPZU_PERSNR']);
		$SQL .= ' AND PZU_GEBDATUM = ' . $DB->FeldInhaltFormat('D',$_POST['sucPZU_GEBDATUM']);

		$Form->DebugAusgabe(1,$SQL);

		$rsPZU = $DB->RecordsetOeffnen($SQL);
		if($rsPZU->EOF())
		{
			$NeueMaskeZeigen=true;
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['PZU']['FehlerNichtGefunden']);
			$Form->ZeileEnde();
		}
		else
		{
//TODO: Kennwort erstellen und Mailen


			$NeueMaskeZeigen=false;
		}
	}


	if($NeueMaskeZeigen)
	{
		/**********************************************
		* Eingabemaske
		***********************************************/

		$Form->ZeileStart();
		$Form->Erstelle_Textarea('Text',$AWISSprachKonserven['PZU']['HinweisZuruecksetzen'],400,0,0,false);
		$Form->ZeileEnde();

		// Artikelnummer
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_BENUTZERNAME'].':',160);
		$Form->Erstelle_TextFeld('*PZU_BENUTZERNAME','',30,150,true,'','','','T','L');
		$AWISCursorPosition=($AWISCursorPosition==''?'sucPZU_BENUTZERNAME':$AWISCursorPosition);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_PERSNR'].':',160);
		$Form->Erstelle_TextFeld('*PZU_PERSNR','',10,150,true,'','','','T','L');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_GEBDATUM'].':',160);
		$Form->Erstelle_TextFeld('*PZU_GEBDATUM','',10,200,true,'','','','D');
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200921041807");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200921041808");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>