<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 11.04.2017
 * Time: 14:47
 */

require_once '/daten/soap/goehrum/PartnerServiceV3/goehrum_autoload.php';

$Service = new PartnerserviceV3(array(),'http://webdienste01.goehrum.de/PartnerServiceV3.asmx?wsdl');

$PartnerBestellungV3 = new PartnerBestellungV3(40665,'12345',new dateTime(),Versandart::Standard);
$PartnerBestellungV3->setBestellReferenz('6789');
$PartnerBestellungV3->setKundennummer(40665);
$ArrayOfBestellpositionen = new ArrayOfBestellPosition();
$Artikel = array();

$Artikel[] = new BestellPosition(1,'3','13047039882',1);

$ArrayOfBestellpositionen->setBestellPosition($Artikel);

$PartnerBestellungV3->setPositionen($ArrayOfBestellpositionen);
$Bestellung = new Bestellung($PartnerBestellungV3);


$BestellungResponse = $Service->Bestellung($Bestellung);

$BestellungResponse = $BestellungResponse->getBestellungResult();

var_dump($BestellungResponse);
