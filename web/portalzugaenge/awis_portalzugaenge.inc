<?php
/**
 * awis_portalzugaenge.inc
 * 
 * Diese Klasse steuert die Portalzugänge im AWIS und im Portal
 * Diese Datei muss im Produktivsystem und im PHP Shuttle identisch sein
 * 
 * @author Sacha Kerres
 * @copyright ATU - Auto-Teile-Unger
 * @version 20091216
 * 
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_Portalzugaenge
{
	/**
	 * Datenbankverbindung
	 * 
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel für Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;

	/**
	 * Exportdateiname
	 * @var string
	 */
	private $_ExportDateiname = '/daten/daten/portal/portalzugang.dat';
	
	/**
	 * Importdateiname aus dem Portal
	 * @var string
	 */
	private $_ImportDateiname = '/daten/daten/portal/portalzugang.akt';
	
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
		
		
		
	}
	

	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);	
		}
		
		return $this->_DebugLevel;
	}

	/**
	 * Exportiert alle Benutzer in eine Datei
	 */
	public function PersonalExport()
	{
		$Werkzeug = new awisWerkzeuge();
		
		$SQL = 'SELECT *';
		$SQL .= ' FROM Portalzugaenge';
		$SQL .= ' WHERE (PZU_TYP = 10 OR PZU_BENUTZERNAME=\'paula_schmidt\')';				// Normale Anwender exportieren
		$SQL .= ' AND (COALESCE(PZU_EXPORTZEIT,to_date(\'01.01.2000\',\'DD.MM.YYYY\')) < PZU_USERDAT)';
		if($this->_DebugLevel> 4)
		{
			echo 'Exportiere Portalzugaenge...'.PHP_EOL;
		}
		$fp = fopen($this->_ExportDateiname,'a+');		// Daten werden angehängt, falls Übertragung nicht geht
		
		$rsPZU = $this->_DB->RecordSetOeffnen($SQL);
		
		$DS = 0;
		$Fehler = 0;
		
		if(!$rsPZU->EOF())
		{
			fputs($fp,"#\n# Exportdatei ".date('d.m.Y')."\n#\n");
		}
		
		while(!$rsPZU->EOF())
		{
			fputs($fp,$rsPZU->FeldInhalt('PZU_BENUTZERNAME').';');
			fputs($fp,$rsPZU->FeldInhalt('PZU_KENNWORT').';');
			fputs($fp,$rsPZU->FeldInhalt('PZU_KENNWORTABLAUF').';');
			fputs($fp,$rsPZU->FeldInhalt('PZU_EMAIL').';');
			fputs($fp,$rsPZU->FeldInhalt('PZU_SPERRE').';');
			fputs($fp,"\n");	

			$SQL = 'UPDATE Portalzugaenge ';
			$SQL .= ' SET PZU_EXPORTZEIT = SYSDATE ';					// Aktuelle Zeit setzen
			$SQL .= ' , PZU_KENNWORTABLAUF = null ';					// Änderungsaufforderung zurücksetzen nach dem Export
			$SQL .= ' WHERE PZU_KEY = '.$rsPZU->FeldInhalt('PZU_KEY');
			// BEA: Kein USERDAT setzen, da dies die Kennung für den Export ist!
			
			if($this->_DB->Ausfuehren($SQL)===false)
			{
				echo '   => Fehler beim Aktualisieren des Zugangs fuer '.$rsPZU->FeldInhalt('PZU_BENUTZERNAME').'.'.PHP_EOL;
				$Werkzeug->EMail('shuttle@de.atu.eu','Personalabgleich','Fehler beim Export: '.urlencode($SQL),1,'shuttle@de.atu.eu','shuttle@de.atu.eu');
				$Fehler++;
			}
			
			$rsPZU->DSWeiter();
			$DS++;
		}
		fclose($fp);
		
		if($this->_DebugLevel> 4)
		{
			echo ' Es wurden '.$DS.' Zugaenge exportiert. '.$Fehler.' Fehler.'.PHP_EOL;
		}
		
		return $DS;
	}


	/**
	 * Gleicht den Personalstamm mit den Portalusern ab 
	 */
	public function PersonalAbgleich()
	{
		$Werkzeug = new awisWerkzeuge();
		
		$Protokoll['NEU']=0;
		$Protokoll['SPERRE']=0;
		$Protokoll['UNVERAENDERT']=0;
		$Protokoll['FEHLER']=0;
		
		$SQL = 'SELECT * ';
		$SQL .= ' FROM (';
		$SQL .= " SELECT lower(SUBSTR(SUCHWORT(VORNAME),0,3) || SUBSTR(SUCHWORT(NAME),0,4) || TO_CHAR(GEB_DATUM,'DD') || SUBSTR(PERSNR,3,2)) as PER_BENUTZERNAME";
		$SQL .= ", PERSNR as PER_KENNWORT";
		$SQL .= ", ROW_NUMBER() OVER(PARTITION BY lower(SUBSTR(SUCHWORT(VORNAME),0,3) || SUBSTR(SUCHWORT(NAME),0,4) || TO_CHAR(GEB_DATUM,'DD.MM.RRRR')|| SUBSTR(PERSNR,3,2)) ORDER BY COALESCE(DATUM_AUSTRITT,TO_DATE('01.01.1970','DD.MM.RRRR')), DATUM_EINTRITT DESC, PERSNR DESC) AS Zeile";
		$SQL .= ", persnr as PER_PERSNR";
		$SQL .= ", geb_datum as PER_GEBDATUM";
		$SQL .= ", taetigkeit as PER_TAETIGKEIT";
		$SQL .= ", vorname || ' ' || name as PER_USER";
		$SQL .= ", sysdate as PER_USERDAT";
		$SQL .= ", Datum_Austritt as PER_SPERRE";
		$SQL .= " FROM (";
		$SQL .= "       SELECT VORNAME, NAME, strasse, datum_eintritt, persnr, geb_datum, taetigkeit, datum_austritt";
		$SQL .= "       from personal_komplett";
		$SQL .= "       WHERE GEB_DATUM IS NOT NULL";
		$SQL .= ") DATEN1";
		$SQL .= " WHERE persnr < 500000";
		$SQL .= " AND GEB_DATUM IS NOT NULL";		
		$SQL .= ') PersDaten ';
		$SQL .= ' LEFT OUTER JOIN Portalzugaenge ON per_persnr = pzu_persnr';
		$SQL .= ' WHERE zeile = 1';			// Doppelte PersEintraege rausfiltern
		$SQL .= " order by PER_PERSNR";
		
		if($this->_DebugLevel>5)
		{
			echo 'Starte Personalabgleich...'.PHP_EOL;
		}
		$rsPER = $this->_DB->RecordSetOeffnen($SQL);

		if($this->_DebugLevel>5)
		{
			echo '  Habe '.$rsPER->AnzahlDatensaetze().' Daten gefunden.'.PHP_EOL;
		}

		$DS = 0;
		while(!$rsPER->EOF())
		{
			if($this->_DebugLevel>8)
			{
				echo '    Lese '.$rsPER->FeldInhalt('PER_BENUTZERNAME').PHP_EOL;
			}
			
			// Status beachten
			if($rsPER->FeldInhalt('PZU_KEY')=='' AND $rsPER->FeldInhalt('PER_SPERRE')=='')
			{
				// Neuer Benutzer => anlegen
				if($this->_DebugLevel>8)
				{
					echo '      -> Neuer Benutzer. Wird angelegt.'.PHP_EOL;
				}
				
				$SQL = 'INSERT INTO Portalzugaenge';
				$SQL .= '(PZU_PERSNR,PZU_GEBDATUM,PZU_BENUTZERNAME,PZU_KENNWORT,PZU_EMAIL,PZU_SPERRE,PZU_TYP,PZU_TAETIGKEIT,PZU_KENNWORTABLAUF,PZU_VOLLERNAME,PZU_USER,PZU_USERDAT)';
				$SQL .= ' VALUES(';
				$SQL .= ' '.$this->_DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('PER_PERSNR'));
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('D',$rsPER->FeldInhalt('PER_GEBDATUM'));
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('PER_BENUTZERNAME'));
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->KennwortVerschluesseln($rsPER->FeldInhalt('PER_KENNWORT')));
				$SQL .= ', null';	// E-Mail
				$SQL .= ', 0';   	// Sperre
				$SQL .= ', 10';   	// Normaler Benutzer
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('PER_TAETIGKEIT'));
				$SQL .= ', SYSDATE';
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('PER_USER')).'';
				$SQL .= ', \''.$this->_AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', SYSDATE';
				$SQL .= ')';
								
				if($this->_DB->Ausfuehren($SQL,'',false)===false)
				{
					echo '    ==> FEHLER beim Speichern: ('.$SQL.')'.PHP_EOL;
					$Werkzeug->EMail('shuttle@de.atu.eu','Personalabgleich','Fehler beim Abgleich: '.urlencode($SQL),1,'shuttle@de.atu.eu','shuttle@de.atu.eu');
					$Protokoll['FEHLER']++;
				}
				else
				{
					$Protokoll['NEU']++;	
				}
			}
			elseif($rsPER->FeldInhalt('PZU_KEY')!='' AND $rsPER->FeldInhalt('PER_SPERRE')!='' AND $rsPER->FeldInhalt('PZU_SPERRE')==0)
			{
				// Benutzer ist ausgetreten => Status ändern
				if($this->_DebugLevel>8)
				{
					echo '      -> Benutzer ist ausgetreten. Wird gesperrt.'.PHP_EOL;
				}
				
				$SQL = 'UPDATE Portalzugaenge';
				$SQL .= ' SET PZU_SPERRE = 1';
				$SQL .= ' , PZU_USERDAT = SYSDATE';			// Userdat aktualisieren, damit ein Export stattfindet
				$SQL .= ' WHERE PZU_PERSNR = '.$this->_DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('PER_PERSNR'));
								
				if($this->_DB->Ausfuehren($SQL,'',false)===false)
				{
					echo '    ==> FEHLER beim Speichern: ('.$SQL.')'.PHP_EOL;
					$Werkzeug->EMail('shuttle@de.atu.eu','Personalabgleich','Fehler beim Abgleich: '.urlencode($SQL),1,'shuttle@de.atu.eu','shuttle@de.atu.eu');
					$Protokoll['FEHLER']++;
				}
				else
				{
					$Protokoll['SPERRE']++;
					$Werkzeug->EMail('shuttle@de.atu.eu','Personalabgleich','Benutzer '.$rsPER->FeldInhalt('PZU_VOLLERNAME').' '.$rsPER->FeldInhalt('PER_PERSNR').' wurde gesperrt.',1,'shuttle@de.atu.eu','shuttle@de.atu.eu');
				}
			}
			elseif($rsPER->FeldInhalt('PER_SPERRE')!='')			// Waren vorher schon gesperrt
			{
				echo '   => war schon inaktiv. Wird ignoriert.'.PHP_EOL;
			}
			else
			{
				$Protokoll['UNVERAENDERT']++;
			}
			
			$rsPER->DSWeiter();
			$DS++;
		}
		
		if($this->_DebugLevel>5)
		{
			echo '  Personalabgleich beendet.'.PHP_EOL;
		}
		$Werkzeug->EMail('shuttle@de.atu.eu','Personalabgleich','Abgleich beendet. '.$Protokoll['NEU'].' neue Benutzer, '.$Protokoll['SPERRE'].' Benutzer gesperrt. '.$Protokoll['UNVERAENDERT'].' Benutzer unverändert. Es sind '.$Protokoll['FEHLER'].' aufgetreten.','shuttle@de.atu.eu');
	}	
	
	/**
	 * Liest den aktuellen Stand der Benutzerdaten ins AWIS ein
	 * 
	 */
	public function BenutzerDatenEinlesen()
	{
		if(!is_file($this->_ImportDateiname))
		{
			echo 'Keine Datei zum importieren vorhanden...'.PHP_EOL;
			return true;			// Keine Datei da
		}

		$Form = new awisFormular();
		
		if($this->_DebugLevel>5)
		{
			echo '  Benutzerdatenimport startet...'.PHP_EOL;
		}

		$Unveraendert = 0;
		$Geaendert = 0;
		
		$fp = fopen($this->_ImportDateiname,'r');
		$Trennzeichen = "\t";
		while(!feof($fp))
		{
			$Zeile = fgets($fp);

			if(substr(trim($Zeile),0,1)!='#')			// Kommentare überlesen
			{
				$Felder = explode($Trennzeichen,$Zeile);

				if($Felder[0]!='')
				{
					$SQL = 'SELECT * FROM Portalzugaenge';
					$SQL .= ' WHERE PZU_BENUTZERNAME = \''. $Felder[0] . '\'';
	
					$rsPZU = $this->_DB->RecordSetOeffnen($SQL);
					if($rsPZU->EOF())
					{
						awisWerkzeuge::EMail('shuttle@de.atu.eu','Personalabgleich','Der Benutzername '.$Felder[0].' kann in den Portalzugaengen nicht gefunden werden.','shuttle@de.atu.eu');
					}
					else
					{
						$UpdateFelder = '';
						
						if(substr($Felder[5],0,4)!='0000' AND $Felder[5]!='')
						{
							if($Form->Format('DU',$rsPZU->FeldInhalt('PZU_LETZTERZUGRIFF'))!=$Form->Format('DU',$Felder[5]))
							{
								$UpdateFelder .= ', PZU_LETZTERZUGRIFF = '.$this->_DB->FeldInhaltFormat('DU',$Felder[5],true);
							}
						}
							
						if($rsPZU->FeldInhalt('PZU_EMAIL')=='' AND $Felder[3]!='')
						{
							$UpdateFelder .= ', PZU_EMAIL = '.$this->_DB->FeldInhaltFormat('T',$Felder[3],true);	
						}	
						if($rsPZU->FeldInhalt('PZU_VOLLERNAME')=='' AND $Felder[1]!='')
						{
							$UpdateFelder .= ', PZU_VOLLERNAME = '.$this->_DB->FeldInhaltFormat('T',$Felder[1],true);	
						}
						if($rsPZU->FeldInhalt('PZU_KENNWORT')!=$Felder[2])
						{
							$UpdateFelder .= ', PZU_KENNWORT = '.$this->_DB->FeldInhaltFormat('T',$Felder[2],true);	
						}

						if($UpdateFelder!='')
						{
							$SQL = 'UPDATE PortalZugaenge ';
							$SQL .= ' SET '.substr($UpdateFelder,1);
							$SQL .= ' WHERE PZU_BENUTZERNAME = '.$this->_DB->FeldInhaltFormat('T',$Felder[0]);

							if($this->_DB->Ausfuehren($SQL)===false)
							{
								awisWerkzeuge::EMail('shuttle@de.atu.eu','Personalabgleich','Der Benutzername '.$Felder[0].' konnte nicht geaendert.','shuttle@de.atu.eu');
							}
							$Geaendert++;
						}	
						else
						{	
							$Unveraendert++;
						}
						
					}
				}
			}
		}
		
		if($this->_DebugLevel>5)
		{
			echo '  Benutzerdatenimport beendet. Es wurden '.$Geaendert.' von '.($Unveraendert+$Geaendert).' Benutzer geaendert'.PHP_EOL;
		}

		// Datei löschen, damit eine neue kommt
		unlink($this->_ImportDateiname);
	}
	
	/**
	 * Kennwort nach Joomla-Syntax verschlüsseln
	 * 
	 * @param string $KennwortText
	 */
	public function KennwortVerschluesseln($KennwortText)
	{
		$saltText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len = strlen($saltText);
		$length = 8;
	
		$stat = @stat(__FILE__);
		if(empty($stat) || !is_array($stat)) $stat = array(php_uname());
	
		mt_srand(crc32(microtime() . implode('|', $stat)));
	
		$Salt = '';
		for ($i = 0; $i < $length; $i ++) 
		{
			$Salt .= $saltText[mt_rand(0, $len -1)];
		}

		$Verschluesselt = md5($KennwortText.$Salt).':'.$Salt;
		
		return $Verschluesselt;
	}

	/**
	 * Pruefung des Kennworts
	 * 
	 * @param $Kennwort
	 * @param $Pruefung
	 */
	public function KennwortPruefen($Kennwort, $Pruefung)
	{
		if($Kennwort=='' OR $Pruefung=='')
		{
			return false;
		}
		$KennwortTeile = explode(':',$Kennwort);
		$salt = $KennwortTeile[1];
		$Verschluesselt = md5($Pruefung.$salt).':'.$salt;
		return ($Kennwort == $Verschluesselt);
	}

    /**
     * Exportieren der Personallimits in eine Textdatei zum Export ins Portal
     */
    public function ExportierePersonalLimits()
    {
        $SQL = 'SELECT PERSONALLIMITBUCHUNGEN.*, PZU_BENUTZERNAME ';
        $SQL .= ', CASE WHEN FIL_ID IS NOT NULL THEN FIL_ID || \' \' || FIL_BEZ ';
        $SQL .= '       WHEN PLB_BUCHUNGSORT = 0 THEN \'Zentrale\' ';
        $SQL .= '       ELSE \'::unbekannt::\' END AS Buchungsort';
        $SQL .= ' FROM AWIS.PERSONALLIMITBUCHUNGEN';
        $SQL .= ' INNER JOIN PERSONAL_KOMPLETT ON CARDNUMBER = PLB_KARTENNUMMER';
        $SQL .= ' INNER JOIN PORTALZUGAENGE ON PZU_PERSNR = PERSNR';
        $SQL .= ' LEFT OUTER JOIN FILIALEN  ON PLB_BUCHUNGSORT = FIL_ID';
        $SQL .= ' WHERE PLB_EXPORTIERT = 0';

        $rsPLB = $this->_DB->RecordSetOeffnen($SQL);
        $Form = new awisFormular();

        $Dateiname = '/daten/daten/portal/personallimits.dat';
        $fp = fopen($Dateiname,'w+');
        while(!$rsPLB->EOF())
        {
            $Zeile = $rsPLB->FeldInhalt('PZU_BENUTZERNAME');
            $Zeile .= "\t" . $rsPLB->FeldInhalt('PLB_TRANSAKTIONSTYP');
            $Zeile .= "\t" . $rsPLB->FeldInhalt('PLB_BETRAG');
            $Zeile .= "\t" . date('Y-m-d',$Form->PruefeDatum($rsPLB->FeldInhalt('PLB_BUCHUNGSTAG'),false,false,true));
            $Zeile .= "\t" . date('Y-m-d',$Form->PruefeDatum($rsPLB->FeldInhalt('PLB_TRANSKTIONSDATUM'),false,false,true));
            $Zeile .= "\t" . $rsPLB->FeldInhalt('BUCHUNGSORT');
            $Zeile .= "\t" . $rsPLB->FeldInhalt('PLB_ID');
            $Zeile .= PHP_EOL;

            fputs($fp,$Zeile);

            $SQL = 'UPDATE PERSONALLIMITBUCHUNGEN SET PLB_EXPORTIERT = 1 WHERE PLB_ID = '.$this->_DB->FeldInhaltFormat('T',$rsPLB->FeldInhalt('PLB_ID'));
            $this->_DB->Ausfuehren($SQL);

            $rsPLB->DSWeiter();
        }
        fclose($fp);
    }

}
?>