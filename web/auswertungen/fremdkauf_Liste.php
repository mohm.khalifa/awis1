<?php
// Variablen
global $awisDBFehler;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 501);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='') 		// Neue suche
{
	$Param = $_POST['txtOEN_NUMMER'];		// 0
	$Param .= ';' . $_POST['txtKBA_NR'];	// 1
	$Param .= ';' . $_POST['txtFIL_ID'];	// 2
	$Param .= ';' . $_POST['txtLFDNR'];	// 3
	$Param .= ';' . $_POST['txtFormat'];	// 4
	$Param .= ';' . $_POST['txtDatumVom'];	// 5
	$Param .= ';' . $_POST['txtDatumBis'];	// 6
	$Param .= ';' . $_POST['txtFIL_ID_Bis'];	// 7

	awis_BenutzerParameterSpeichern($con,'AktuelleFremdkaufSuche' , $AWISBenutzer->BenutzerName(), $Param);
	$Param = explode(';',$Param);
}
elseif(isset($_GET['OEN_NUMMER']))	// OEN_Nummer wurde �bergeben
{
	$Param = array($_GET['OEN_NUMMER'],'','','','');
}
elseif(isset($_GET['KBA_NR']))	// KBA_NR wurde �bergeben
{
	$Param = array('',$_GET['KBA_NR'],'','','');
}
elseif(isset($_GET['LFDNR']))	//
{
	$Param = array('','',$_GET['FIL_ID'],$_GET['LFDNR'],'');
}
elseif(isset($_GET['FIL_ID']))	//
{
	$Param = array('','',$_GET['FIL_ID'],'','');
}
else	// Letzte gespeicherte Suche
{
	$Param = explode(';',awis_BenutzerParameter($con,'AktuelleFremdkaufSuche' , $AWISBenutzer->BenutzerName()));
}

$Bedingung = '';
$SQL = 'SELECT * FROM Fremdkaeufe';
If($Param[0]!='')		// OEN-Nummer
{
	$Bedingung .= 'AND FRK_OEN_SUCHNUMMER ' . awisLIKEoderIST($Param[0],true,false,false,2);
}

If($Param[1]!='')		// KBA-Nummer
{
	//$Bedingung .= "AND FRK_KBA = '" . $Param[1] . "'";
	$Bedingung .= "AND FRK_KBA " . awisLIKEoderIST($Param[1],true,false,false,0)." ";
}

If($Param[2]!='' OR (isset($Param[7]) AND $Param[7]!=''))		// Filialen
{
	if($Param[2]!='' AND (!isset($Param[7]) or $Param[7]==''))
	{
		$Bedingung .= "AND FRK_FIL_ID = '" . $Param[2] . "'";
	}
	else
	{
		if($Param[2]!='')
		{
			$Bedingung .= " AND FRK_FIL_ID >= '" . $Param[2] . "'";
		}
		if($Param[7]!='')
		{
			$Bedingung .= " AND FRK_FIL_ID <= '" . $Param[7] . "'";
		}
	}
}




If($Param[3]!='')		// Lfd-Nr
{
	$Bedingung .= "AND FRK_LFDNR = '" . $Param[3] . "'";
}

If(isset($Param[5]) AND $Param[5]!='')		// DatumVom
{
	If($Param[6]!='')		// DatumBis
	{
		$Bedingung .= " AND FRK_DATUM >= '" . awis_format($Param[5], 'OracleDatum') . "'";
		$Bedingung .= " AND FRK_DATUM <= '" . awis_format($Param[6], 'OracleDatum') . "'";
	}
	else
	{
		$Bedingung .= "AND (TO_DATE(FRK_DATUM,'dd.mm.yyyy') = '" . awis_format($Param[5], 'OracleDatum') . "')";
	}
}

if($Bedingung=='')		// Keine Kriterien -> w�rde alle Daten liefern-> ABBRUCH
{
	echo '<span class=HinweisText>Sie haben keine Kriterien angegeben.</span>';
	awisLogoff($con);
	die();
}

$SQL .= ' WHERE ' . substr($Bedingung,4);
if(isset($_GET['Sort']) AND $_GET['Sort']!='')
{
	$SQL .= ' ORDER BY ' . $_GET['Sort'];
}
else
{
	$SQL .= ' ORDER BY FRK_DATUM, FRK_FIL_ID, FRK_LFDNR DESC';
}

awis_Debug(1,$SQL);

$rsOEP = awisOpenRecordset($con, $SQL);

if($rsOEP==FALSE)
{
	awisErrorMailLink("Fremdkauf_Liste.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsOEPZeilen=$awisRSZeilen;

$AnzFK=0;
$Doppelte = array();

If($Param[0]!='')		// OEN-Nummer
{
	$AnzFKGesamt=0;
	$AnzFK=0;
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$SuchKey = $rsOEP['FRK_FIL_ID'][$OEPZeile] . '-' . $rsOEP['FRK_LFDNR'][$OEPZeile];
		if(array_search($SuchKey, $Doppelte)===FALSE)
		{
				// Gr��er als 1 Jahr
			$Dat1 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+1);
			$Dat2 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+2);
			$Dat3 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+3);
			if($Dat1 >= time())
			{
				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
				// Gr��er als 2 Jahre
			if($Dat2 >= time())
			{
//				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
				$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
//			$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			array_push($Doppelte, $SuchKey);
		}
	}
	echo 'Fremdk�ufe in den letzten 24 Monaten: ' . ($AnzFKGesamt) . '. Davon in den letzten 12 Monaten: ' . $AnzFK;
}
else	// Nur Zeilenanzahl, wenn keine OENummer gesucht wurde
{
	echo 'Es wurden ' . $rsOEPZeilen . ' Zeilen gefunden';
}

//****************************************************
//
// Ausgabe der Daten
//
// 	-> txtFormat: liste -> HTML
//                csv   -> CSV-Datei
//****************************************************


if((isset($_POST['txtFormat']) AND $_POST['txtFormat']=='liste') OR
  (!isset($_POST['txtFormat']) or$_POST['txtFormat']==''))		// Liste auf dem Bildschirm
{
	print "<table id=DatenTabelle width=100% border=1>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_FIL_ID" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_FIL_ID'?'%20DESC':'') . ">Filiale</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_DATUM" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_DATUM'?'%20DESC':'') . ">Datum</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_LFDNR" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_LFDNR'?'%20DESC':'') . ">Lfd.Nr</a></td>";
	print "<td id=FeldBez>WstNr</td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_KBA" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_KBA'?'%20DESC':'') . ">KBA-Nr</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_MENGE" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_MENGE'?'%20DESC':'') . ">Menge</a></td>";
	print "<td id=FeldBez>Grund</td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_OEN_NUMMER" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_OEN_NUMMER'?'%20DESC':'') . ">OE-Nr</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_ARTBEZ" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_ARTBEZ'?'%20DESC':'') . ">Artikel</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=Liste&Sort=FRK_PREIS" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_PREIS'?'%20DESC':'') . ">Preis</a></td>";
	print "<td id=FeldBez>Rabatt</td>";
	print "</tr>";

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		echo '<tr>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&FIL_ID=' .$rsOEP['FRK_FIL_ID'][$OEPZeile].'>'	. $rsOEP['FRK_FIL_ID'][$OEPZeile] . '</a></td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_DATUM'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&LFDNR=' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '&FIL_ID=' . $rsOEP['FRK_FIL_ID'][$OEPZeile] . '>' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '</a></td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_WSTNR'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&KBA_NR=' . $rsOEP['FRK_KBA'][$OEPZeile] . '>' . $rsOEP['FRK_KBA'][$OEPZeile] . '</a></td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_MENGE'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_GRUND'][$OEPZeile] . '</td>';
		// OE-Nummer mit Umschl�sselung auf ATUNR
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_OEN_NUMMER'][$OEPZeile];

//		$rsATU = awisOpenRecordset($con, 'SELECT TEI_KEY1, TEI_WERT1 FROM TEILEINFOS WHERE TEI_SUCH2 ' . awisLIKEoderIST($rsOEP['FRK_OEN_NUMMER'][$OEPZeile],false,false,false,2) . " AND TEI_ITY_ID1='AST' AND TEI_ITY_ID2='OEN'");
		$BindeVariablen=array()	;
		$BindeVariablen['var_T_frk_oen_nummer']=$rsOEP['FRK_OEN_NUMMER'][$OEPZeile];
		$rsATU = awisOpenRecordset($con, "SELECT TEI_KEY1, TEI_WERT1 FROM TEILEINFOS WHERE TEI_SUCH2 = :var_T_frk_oen_nummer AND TEI_ITY_ID1='AST' AND TEI_ITY_ID2='OEN'", true, false, $BindeVariablen);
		if($awisRSZeilen>0)
		{
			echo '<br>';
			$Text = '';
			for($ATUZeile=0;$ATUZeile<$awisRSZeilen;$ATUZeile++)
			{
				$Text .= ', <a href=/ATUArtikel/artikel_Main.php?Key=' . $rsATU['TEI_KEY1'][$ATUZeile] . '&cmdAktion=ArtikelInfos>' . $rsATU['TEI_WERT1'][$ATUZeile] . '</a>';
			}
			echo substr($Text,2);
		}
		unset($rsATU);
		echo '</td>';

		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_ARTBEZ'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=right>'	. awis_format($rsOEP['FRK_PREIS'][$OEPZeile],'Currency') . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=right>'	. $rsOEP['FRK_RABATT'][$OEPZeile] . '%</td>';
		echo '</tr>';
	}

	print "</table>";
}
elseif($_POST['txtFormat']=='csv')		// Liste als CSV
{
		// Dateinamen generieren
	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	fputs($fd, "Filiale;Datum;LfdNr;WstNr;KBA-Nr;Menge;Grund;OE-Nr;Artikel;Preis;Rabatt\n");

	echo '<BR>';
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$Zeile 	= $rsOEP['FRK_FIL_ID'][$OEPZeile];
		$Zeile .= ';' . $rsOEP['FRK_DATUM'][$OEPZeile];
		$Zeile .= ';' . $rsOEP['FRK_LFDNR'][$OEPZeile];
		$Zeile .= ';' . $rsOEP['FRK_WSTNR'][$OEPZeile];
		$Zeile .= ';' . $rsOEP['FRK_KBA'][$OEPZeile];
		$Zeile .= ';' . $rsOEP['FRK_MENGE'][$OEPZeile];
		$Zeile .= ';' . str_replace(';',',',$rsOEP['FRK_GRUND'][$OEPZeile]);
		$Zeile .= ';"' . $rsOEP['FRK_OEN_NUMMER'][$OEPZeile] .'"';
		$Zeile .= ';' . str_replace(';',',',$rsOEP['FRK_ARTBEZ'][$OEPZeile]);
		$Zeile .= ';' . awis_format($rsOEP['FRK_PREIS'][$OEPZeile],'Standardzahl');
		$Zeile .= ';' . $rsOEP['FRK_RABATT'][$OEPZeile];
		$Zeile .= "\n";
			
		fputs($fd,$Zeile);
	}
		// Fertig, Datei schlie�en
	fclose($fd);
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';

}
unset($rsOEP);

?>

