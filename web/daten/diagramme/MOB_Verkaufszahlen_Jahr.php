<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 01.12.2016
 * Time: 19:17
 */

require_once 'awisRest.inc';

$awisRest = new awisRest();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$SQL = ' select to_char(mob_datum,\'MM\') as mob_datum, count(*) as MOB_ANZ from MOBILITAETSGARANTIEN ';
$SQL .= ' Where mob_datum >= ' .$DB->WertSetzen('MOB','D',('01.01.'.date('Y')));
$SQL .= ' and mob_datum <=  '.$DB->WertSetzen('MOB','D',('31.12.'.date('Y')));
$SQL .= ' group by  to_char(mob_datum,\'MM\') order by trunc(mob_datum)';


$rs = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('MOB'));

$awisRest->sendeResponse($rs,'application/json');
