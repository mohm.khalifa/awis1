<?php
require_once("db.inc.php");		// DB-Befehle
$APPSRVZeit = file_get_contents('/daten/zugang/awis_timestamp_appsrv.txt');
$WEBSRVZeit = file_get_contents('/daten/web/admin/awis_timestamp_websrv.txt');
$DBServer = awisDBServer();

$con = @OCILogon("awis","IlOuLua",$DBServer);

if($con==FALSE)
{
	echo "AWIS Fehler<br>Keine Datenbankverbindung zu " . $DBServer;
}
else
{
	if(substr($APPSRVZeit,-1)=="\n")
	{
		$APPSRVZeit = substr($APPSRVZeit,0,strlen($APPSRVZeit)-1);
	}
        if(substr($WEBSRVZeit,-1)=="\n")
        {
                $WEBSRVZeit = substr($WEBSRVZeit,0,strlen($WEBSRVZeit)-1);
        }

	$DatumWEBSRV = mktime(substr($WEBSRVZeit,8,2),substr($WEBSRVZeit,10,2),0,substr($WEBSRVZeit,4,2),substr($WEBSRVZeit,6,2),substr($WEBSRVZeit,0,4));
	$DatumAPPSRV = mktime(substr($APPSRVZeit,8,2),substr($APPSRVZeit,10,2),0,substr($APPSRVZeit,4,2),substr($APPSRVZeit,6,2),substr($APPSRVZeit,0,4));
	
	$DatumDiff = abs($DatumAPPSRV - $DatumWEBSRV);
	if($DatumDiff<=60 AND $DatumAPPSRV!=-1 AND $DatumWEBSRV!=-1)
	{
		echo 'LoadBalancer';
		echo '<br>Alle Tests erfolgreich.';
	}
	else
	{
		echo "Zeitdifferenz Zentral-Lokal: " . $APPSRVZeit . " - " . $WEBSRVZeit;
		echo '<br>Zeitvergleich fehlgeschlagen. NFS Problem?';
	}
}
awisLogoff($con);
?>

