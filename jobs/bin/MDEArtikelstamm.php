<?php
require_once('awisMDEArtikelstammExport.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $Filiale = '';
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    
    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--filiale':
                    		$Filiale = $argv[++$i];
                    		break;
                   
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --filialen <Nr>                    zu exportierende Filialnummer';
                            echo PHP_EOL;
                            die();
            }
    }


    //$Job = new awisJobProtokoll($JobId, 'Starte Einkaufsgesellschaften Export.');
    $MDEExport = new awisMDEArtikelstammExport($Benutzer, $DebugLevel);
    

    if ($Filiale!='')
    {
    	
    	$MDEExport->setFilialen($Filiale);
    	//$MDEExport->setFilialen(array($Filiale));
    }
 
    $MDEExport->erstelleStammdaten();


}
catch (Exception $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;
  
}
?>
