<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	if($BildschirmBreite<1024)
	{
	}
	elseif($BildschirmBreite<1280)
	{
	}
	else
	{
	}
	
	$TextKonserven = array();
	$TextKonserven[]=array('WEB','*');
	$TextKonserven[]=array('WBE','*');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','FilialenDeutschAlle');
	
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht14000 = $AWISBenutzer->HatDasRecht(14000);
	
	if($Recht14000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$flagNewDS = false;
	
	if(isset($_GET["Edit"]))
	{
	  $flagNewDS = true;
	}
	
   if($flagNewDS == false)   	
   {	

    $Param = $AWISBenutzer->ParameterLesen('AktuellerWEB');
    
    $SQL = 'Select Wbe_Key,Wbe_Filialen,Wbe_Web_Key,Wbe_Datvon,Wbe_Datbis';
    $SQL .= ' FROM WERBEMITTELZUORDNUNG WHERE WBE_WEB_KEY ='.$Param;
    $SQL .= 'order by to_number(WBE_FILIALEN) ASC ';    
    
    $rsVSA = $DB->RecordSetOeffnen($SQL);
    
    $Form->SchreibeHTMLCode('<form name=frmVerteilqualitaetZuordnung action=./verteilqualitaet_Details.php?cmdAktion=Details&Seite=Zuordnung'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

    $Form->Formular_Start();

    if ($AWIS_KEY1 != 0 && !isset($_GET['WBE_WEB_KEY']))
     {	
	$EditRecht = $Recht14000&2;
	$Icons = '';
	      $Form->ZeileStart();
                if((intval($Recht14000)&8)!=0)
                {
			if($rsVSA->AnzahlDatensaetze() != 0)
			{
			   $Icons[] = array('new','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&WBE_WEB_KEY='.$rsVSA->FeldInhalt('WBE_WEB_KEY'));
			}
			elseif(isset($_GET['WEB_KEY']))
			{
			  $Icons[] = array('new','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&WBE_WEB_KEY='.$_GET['WEB_KEY']);
			}
			elseif(isset($_POST['txtWEB_KEY']))
			{
			  $Icons[] = array('new','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&WBE_WEB_KEY='.$_POST['txtWEB_KEY']);
			}
			//var_dump($Icons);	
			$Form->Erstelle_ListeIcons($Icons,38,-1);
                }
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_KEY'],60,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_FILIALEN'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATVON'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATBIS'],200,'');
                $Form->ZeileEnde();

            $FGKZeile=0;
            $DS = 0;

            while(!$rsVSA->EOF())
            {
                    $Form->ZeileStart();
                    if(intval($Recht14000&4)>0)	// Ändernrecht
                    {
			$Icons[0] = array('edit','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&Edit='.$rsVSA->FeldInhalt('WBE_KEY').'&WEB_KEY='.$Param);
			$Icons[1] = array('delete','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&Del='.$rsVSA->FeldInhalt('WBE_KEY').'&txtWEB_KEY='.$Param);
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
                    }
		    
                    $Link = './verteilqualitaet_Main.php?cmdAktion=Details&WEB_KEY='.$Param.'';
                    $Form->Erstelle_ListenFeld('WBE_KEY',$rsVSA->FeldInhalt('WBE_KEY'),0,60,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_FILIALEN',$rsVSA->FeldInhalt('WBE_FILIALEN'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_DATVON',$rsVSA->FeldInhalt('WBE_DATVON'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_DATBIS',$rsVSA->FeldInhalt('WBE_DATBIS'),0,200,false,($FGKZeile%2),'','');
// ALT                    $Export[0] = array('report','./verteilqualitaet_Main.php?cmdAktion=Details&Seite=Zuordnung&ExportWBE_KEY='.$rsWEB->FeldInhalt('WBE_KEY').'&ExportWEB_KEY='.$Param);
// SK, 08,05.2012, richtig gestellt
                    $Export[0] = array('report','./verteilqualitaet_Export.php?ExportWBE_KEY='.$rsVSA->FeldInhalt('WBE_KEY').'&ExportWEB_KEY='.$Param);
		    $Form->Erstelle_ListeIcons($Export,20,($FGKZeile%2));
                 
		    $Form->ZeileEnde();

                    $FGKZeile++;
                    $rsVSA->DSWeiter();
		}
}elseif(isset($_GET['WBE_WEB_KEY']))
{
		$Felder = array();
		
		$VSAKEY = $_GET['WBE_WEB_KEY'];
		
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./verteilqualitaet_Main.php?cmdAktion=Details&WEB_KEY=".$VSAKEY."&accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();

                $Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['WBE_FILIALEN'].':',150,'','','Filialenangabe mit Komma trennen --> Example (90,200,30)');
                $Form->Erstelle_TextFeld('WBEFILIALEN','',20,200,True);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATVON'].':',150,'','','Datum im Format 25.10.2010 eingeben');
                $Form->Erstelle_TextFeld('WBE_DATVON','',10,150,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATBIS'].':',150,'','','Datum im Format 25.10.2010 eingeben');
                $Form->Erstelle_TextFeld('WBE_DATBIS','',10,150,true);
		//10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('d.m.Y'))),10
		$Form->ZeileEnde();
}
else
{
	//Neuanlage
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['WBE_FILIALEN'].':',150,'','','Filialenangabe mit Komma trennen --> Example (90,200,30)');
        $Form->Erstelle_TextFeld('WBEFILIALEN','',20,200,True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATVON'].':',150,'','','Datum im Format 25.10.2010 eingeben');
        $Form->Erstelle_TextFeld('WBE_DATVON','',20,200,True,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATBIS'].':',150,'','','Datum im Format 25.10.2010 eingeben');
        $Form->Erstelle_TextFeld('WBE_DATBIS','',20,200,True,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
	$Form->Trennzeile('L');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FilialenDeutschAlle'].':',300);
	$Form->Erstelle_Checkbox('*FilialenDeutschAlle',($Param[0]=='on'?'on':''),30,true,'on','','');
		
        $Form->ZeileEnde();
}

}
else
{
    $SQL = 'Select Wbe_Key,Wbe_Filialen,Wbe_Web_Key,Wbe_Datvon,Wbe_Datbis';
    $SQL .= ' FROM WERBEMITTELZUORDNUNG WHERE WBE_KEY ='.$_GET['Edit'];  
   
    $rsVSA = $DB->RecordSetOeffnen($SQL);
    
    $Felder = array();
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./verteilqualitaet_Main.php?cmdAktion=Details&WEB_KEY=".$_GET['WEB_KEY']."&accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    $Form->InfoZeile($Felder,'');
	
    $Form->SchreibeHTMLCode('<form name=frmVerteilqualitaetZuordnung action=./verteilqualitaet_Details.php?cmdAktion=Details&Seite=Zuordnung'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

    $Form->Formular_Start();
	
    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_KEY'],60,'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_FILIALEN'],200,'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATVON'],200,'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATBIS'],200,'');
    $Form->ZeileEnde();

    $Form->Erstelle_ListenFeld('WBE_KEY',$rsVSA->FeldInhalt('WBE_KEY'),0,60,false,'','','');
    $Form->Erstelle_ListenFeld('WBE_FILIALEN',$rsVSA->FeldInhalt('WBE_FILIALEN'),0,200,false,'','','');
    $Form->Erstelle_ListenFeld('WBE_DATVON',$rsVSA->FeldInhalt('WBE_DATVON'),0,200,true,'','','');
    $Form->Erstelle_ListenFeld('WBE_DATBIS',$rsVSA->FeldInhalt('WBE_DATBIS'),0,200,true,'','','');
                 	
    $Form->Erstelle_HiddenFeld('Edit',true);
    $Form->Erstelle_HiddenFeld('WBE_KEY',$rsVSA->FeldInhalt('WBE_KEY'));
    $Form->Erstelle_HiddenFeld('WEB_KEY',$rsVSA->FeldInhalt('WBE_WEB_KEY'));

}

$Form->Formular_Ende();

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200905131842");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>