<?php
global $con;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
$BSBreite = awis_BenutzerParameter($con, "BildschirmBreite" , $_SERVER['PHP_AUTH_USER']);

$TextKonserven = array();
$TextKonserven[]=array('MAE','MAE_%');
$TextKonserven[]=array('MBW','MBW_%');
$TextKonserven[]=array('MAE','lstMAE_STATUS');
$TextKonserven[]=array('Adressen','txtAnredenListe');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3011 = awisBenutzerRecht($con,3011,$_SERVER['PHP_AUTH_USER']);
if($Recht3011==0)
{
    awisEreignis(3,1000,'Mitbewerberaenderungen',$_SERVER['PHP_AUTH_USER'],'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$MaxDSAnzahl=500;


$SQL = 'SELECT *';
$SQL .= ' FROM mitbewerbaenderungen ';
$SQL .= ' WHERE mae_mbw_key=0'.$Param;
if(isset($_GET['Edit']))
{
	$SQL .= ' AND mae_key=0'.$_GET['Edit'];
}
if(!isset($_GET['ALLE']))
{
	$SQL .= ' AND MAE_STATUS=\'O\'';
}
$SQL .= ' ORDER BY MAE_ANTRAGDATUM';

//awis_Debug(1,$SQL);
$rsMAE = awisOpenRecordset($con, $SQL);
$rsMAEZeilen = $awisRSZeilen;

//**************************************
// Liste anzeigen
//**************************************
if($rsMAEZeilen > 1 OR !isset($_GET['Edit']))
{
	awis_FORM_FormularStart();	
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('&nbsp;',52);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_ANTRAGSTELLER'],130);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_ANTRAGDATUM'],130);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_STATUS'],80);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_FELDNAME'],180);
	if($Recht3011&40)
	{
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_ALTERWERT'],300);
	}
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MAE']['MAE_NEUERWERT'],300);
	awis_FORM_ZeileEnde();
	
	if(!$rsMAEZeilen>$MaxDSAnzahl)
	{
		$rsMAEZeilen=$MaxDSAnzahl;
	}
	for($rsMAEZeile=0;$rsMAEZeile<$rsMAEZeilen;$rsMAEZeile++)
	{
		awis_FORM_ZeileStart();
		
		$Icons = array();
		if(($Recht3011&4)==4)	// �ndernrecht
		{
			$Icons[0] = array('edit','./mitbewerber_Main.php?cmdAktion=Details&Seite=Aenderungen&Edit='.$rsMAE['MAE_KEY'][$rsMAEZeile]);
		}
		
		if(($Recht3011&8)==8)	// L�schen
		{
			$Icons[1] = array('delete','./mitbewerber_Main.php?cmdAktion=Details&Seite=Aenderungen&Del='.$rsMAE['MAE_KEY'][$rsMAEZeile]);
		}
		if(($Recht3011&32)==32)	// Mitbewwerber �ndern
		{
			$Icons[2] = array('ok','./mitbewerber_Main.php?cmdAktion=Details&Seite=Aenderungen&Set='.$rsMAE['MAE_KEY'][$rsMAEZeile]);
		}

		awis_FORM_Erstelle_ListeIcons($Icons,52,($rsMAEZeile%2));
		awis_FORM_Erstelle_ListenFeld('MAE_ANTRAGSTELLER',$rsMAE['MAE_ANTRAGSTELLER'][$rsMAEZeile],0,130,false,($rsMAEZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MAE_ANTRAGDATUM',$rsMAE['MAE_ANTRAGDATUM'][$rsMAEZeile],0,130,false,($rsMAEZeile%2),'','','D');
		awis_FORM_Erstelle_ListenFeld('MAE_STATUS',$rsMAE['MAE_STATUS'][$rsMAEZeile],0,80,false,($rsMAEZeile%2),'','','');
		awis_FORM_Erstelle_ListenFeld('MAE_FELDNAME',$TXT_TextBaustein['MBW'][$rsMAE['MAE_FELDNAME'][$rsMAEZeile]],0,180,false,($rsMAEZeile%2),'','');
		if($rsMAE['MAE_FELDNAME'][$rsMAEZeile]=='MBW_ANREDE')
		{
			$StatusText = explode(";",$TXT_TextBaustein['Adressen']['txtAnredenListe']);
			$Anzeige = '';
			if($Recht3011&40)
			{
				foreach ($StatusText AS $Anreden)
				{
					$Anreden = explode('-',$Anreden);
					if($Anreden[0]==$rsMAE['MAE_ALTERWERT'][$rsMAEZeile])
					{
						$Anzeige = $Anreden[1];
					}
				}
				awis_FORM_Erstelle_ListenFeld('MAE_ALTERWERT',$Anzeige,0,300,false,($rsMAEZeile%2),'','');
			}
			
			foreach ($StatusText AS $Anreden)
			{
				$Anreden = explode('-',$Anreden);
				if($Anreden[0]==$rsMAE['MAE_NEUERWERT'][$rsMAEZeile])
				{
					$Anzeige = $Anreden[1];
				}
			}
			awis_FORM_Erstelle_ListenFeld('MAE_NEUERWERT',$Anzeige,0,300,false,($rsMAEZeile%2),'','');
		}
		else
		{
			if($Recht3011&40)
			{
				awis_FORM_Erstelle_ListenFeld('MAE_ALTERWERT',$rsMAE['MAE_ALTERWERT'][$rsMAEZeile],0,300,false,($rsMAEZeile%2),'','');
			}
			awis_FORM_Erstelle_ListenFeld('MAE_NEUERWERT',$rsMAE['MAE_NEUERWERT'][$rsMAEZeile],0,300,false,($rsMAEZeile%2),'','');
		}
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 			// einen Datensatz anzeigen
{
	awis_FORM_FormularStart();

	echo '<input type=hidden name=txtMAE_KEY value='.$rsMAE['MAE_KEY'][0].'>';
	echo '<input type=hidden name=txtMAE_MBW_KEY value='.$rsMAE['MAE_MBW_KEY'][0].'>';
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MAE']['MAE_ANTRAGSTELLER'],150,'');
	awis_FORM_Erstelle_TextFeld('MAE_ANTRAGSTELLER',$rsMAE['MAE_ANTRAGSTELLER'][0],20,0,($Recht3011&6),'','','');
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MAE']['MAE_ANTRAGDATUM'],150,'');
	awis_FORM_Erstelle_TextFeld('MAE_ANTRAGDATUM',$rsMAE['MAE_ANTRAGDATUM'][0],20,0,($Recht3011&6),'','','','D');
	$CursorFeld='txtMAE_ANTRAGSTELLER';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW'][$rsMAE['MAE_FELDNAME'][0]],150,'');
	awis_FORM_Erstelle_TextFeld('MAE_ALTERWERT',$rsMAE['MAE_ALTERWERT'][0],40,0,($Recht3011&6),'','','','T');	
	awis_FORM_Erstelle_TextLabel('==>',40,'');
	awis_FORM_Erstelle_TextFeld('MAE_NEUERWERT',$rsMAE['MAE_NEUERWERT'][0],40,0,($Recht3011&6),'','','','T');	
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MAE']['MAE_STATUS'],150,'');
	$MAE_STATUS = explode(";",$TXT_TextBaustein['MAE']['lstMAE_STATUS']);
	awis_FORM_Erstelle_SelectFeld('MAE_STATUS',$rsMAE['MAE_STATUS'][0],200,($Recht3011&6),$con,'','','','','',$MAE_STATUS);
	awis_FORM_ZeileEnde();
	
	awis_FORM_FormularEnde();
}




?>