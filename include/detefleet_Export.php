<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once 'awisMailer.inc';

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");


class detefleet_Export
{

    private $_Funktionen;
    private $_DB;
    protected $_AWISBenutzer;
    private $exportPfad = '/daten/daten/DETEFLEET/Export/';
    private $kopiePfad = '/daten/daten/pcedv11/daten/experian/preislisten/';
    private $exportDateiName= 'experian_fpl_23_Festpreisliste-DeTeFleet';
    protected $preisListe = '';
    private $Werkzeug;
    private $Mail;

    /**
     * detefleet_Export constructor.
     * @throws awisException
     */
    public function __construct()
    {
      $this->_AWISBenutzer = awisBenutzer::Init();
      $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
      $this->_DB->Oeffnen();
      $this->_Werkzeug = new awisWerkzeuge();
      $this->_Mail = new awisMailer($this->_DB, $this->_AWISBenutzer);
    }

    /**
     * Setter fuer die Preislisten-ID
     * @param $PreisListe
     */
    public function setPreisListe($PreisListe)
    {
        $this->preisListe = $PreisListe;
    }

    /**
     * Prueft den Export aus Duplikate
     * @return bool
     */
    public function checkDuplikate()
    {
        $SQL = 'select gka_ast_atunr, count(*) ';
        $SQL.= ' from gkkunde';
        $SQL.= ' inner join gkpreisliste on gkp_gkd_key = gkd_key';
        $SQL.= ' inner join gkpreislistezuord on gkz_gkp_key = gkp_key';
        $SQL.= ' inner join gkatunrzuord on gka_gkz_key = gkz_key';
        $SQL.= ' where gkp_key='.$this->_DB->FeldInhaltFormat('NO',$this->preisListe).' and gkz_auto=\'j\'';
        $SQL.= ' group by gka_ast_atunr';
        $SQL.= ' having count(*) > 1';
        $SQL.= ' order by 1';

        $rsDuplikate = $this->_DB->RecordSetOeffnen($SQL);

        if ($rsDuplikate->AnzahlDatensaetze() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Exportiert die Preislistendaten
     * @throws Exception
     */
    public function export()
    {
        $SQL = "Select cc.GKA_AST_ATUNR, nvl(cc.gka_preis, bb.GKZ_PREIS) as Preis, to_char(aa.GKP_GUELTIGAB,'YYYYMMDD') as GKP_GUELTIGAB, to_char(aa.GKP_GUELTIGBIS,'YYYYMMDD') as GKP_GUELTIGBIS From Gkpreisliste Aa";
        $SQL .= " Inner Join Gkpreislistezuord bb On Aa.Gkp_Key = Bb.Gkz_Gkp_Key";
        $SQL .= " Inner Join Gkatunrzuord Cc On Bb.Gkz_Key = Cc.Gka_Gkz_Key";
        $SQL .= " WHERE aa.GKP_KEY =".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
        $SQL .= " order by gka_ast_atunr";

        $rsPreisListeExport = $this->_DB->RecordSetOeffnen($SQL);

        $this->exportDateiName = $this->exportDateiName.'_'.''.$rsPreisListeExport->FeldInhalt('GKP_GUELTIGAB').'.csv';

        if(($fd = fopen($this->exportPfad.$this->exportDateiName,'w')) === false)
        {
              echo $this->error_msg = "Fehler beim Erstellen der Datei";
        }
        else
        {
            while(!$rsPreisListeExport->EOF())
            {
              $List = array();

              $List[0] = $rsPreisListeExport->FeldInhalt('GKA_AST_ATUNR');
              $List[1] = $rsPreisListeExport->FeldInhalt('PREIS');
              $List[2] = substr($rsPreisListeExport->FeldInhalt('GKP_GUELTIGBIS'),0,10);

              $Line = array($List[0].';'.$List[1].';'.$List[2].chr(13).chr(10));

              foreach($Line as $Test)
              {
                  fwrite($fd,$Test);
              }

              $rsPreisListeExport->DSWeiter();
            }
        }

        //Daten aus alter Preisliste wieder mit anfuegen
        $SQL = " select GKR_AST_ATUNR, gkr_preis, to_char(gkr_gultigbis,'YYYYMMDD') as gkr_gueltigbis";
        $SQL.= " from gkatunranreicherung";
        $SQL.= " WHERE GKR_GKP_KEY =".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
        $SQL.= " order by gkr_ast_atunr";

        $rsPreisListeExport = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsPreisListeExport->EOF())
            {
              $List = array();

              $List[0] = $rsPreisListeExport->FeldInhalt('GKR_AST_ATUNR');
              $List[1] = $rsPreisListeExport->FeldInhalt('GKR_PREIS');
              $List[2] = substr($rsPreisListeExport->FeldInhalt('GKR_GUELTIGBIS'),0,10);

              $Line = array($List[0].';'.$List[1].';'.$List[2].chr(13).chr(10));

              foreach($Line as $Test)
              {
                fwrite($fd,$Test);
              }

              $rsPreisListeExport->DSWeiter();
            }

        fclose($fd);

        //Kopieren nach PCEDV11
        copy ($this->exportPfad.$this->exportDateiName, $this->kopiePfad.$this->exportDateiName);

        $this->_Mail->Absender('awis@de.atu.eu');
        $this->_Mail->Betreff('PREISLISTE DETEFLEET - DATUM '.date('Ymd'));
        $this->_Mail->Prioritaet(awisMailer::PRIO_NORMAL );
        $this->_Mail->Text('Preisliste wurde auf '.$this->kopiePfad.$this->exportDateiName.' erstellt');
        $this->_Mail->AdressListe(awisMailer::TYP_TO, 'shuttle@de.atu.eu', awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_WARNUNG);

        $this->_Mail->Anhaenge($this->exportPfad.$this->exportDateiName, $this->exportDateiName);

        $this->_Mail->MailSenden();
    }
}

?>