<?php
/**
 * L�schen von Informationen auf der Filialinfo
 *
 * @author Sacha Kerres
 * @version 20090813
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','VerbindungMFILoesen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Frage = $TXT_AdrLoeschen['Wort']['WirklichLoeschen'];


	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		$Tabelle = 'MBW';
		$Key=$_POST['txtMBW_KEY'];
		$MBWKey=$_POST['txtMBW_KEY'];

		$AWIS_KEY1 = $Key;

		$Felder=array();

		$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_NAME1'),$_POST['txtMBW_NAME1']);
		$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_STRASSE'),$_POST['txtMBW_STRASSE']);
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case 'Filialzuordnungen':
					$Tabelle = 'MFI';
					$Key=$_GET['Del'];

					$SQL = 'SELECT MFI_KEY, MFI_MBW_KEY, MBW_NAME1, FIL_BEZ ';
					$SQL .= ' FROM MitbewerberFilialen';
					$SQL .= ' INNER JOIN Mitbewerber ON MFI_MBW_KEY = MBW_KEY';
					$SQL .= ' INNER JOIN Filialen ON MFI_FIL_ID = FIL_ID';
					$SQL .= ' WHERE MFI_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$MBWKey = $rsDaten->FeldInhalt('MFI_MBW_KEY');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_NAME1'),$rsDaten->FeldInhalt('MBW_NAME1'));
					$Felder[]=array($Form->LadeTextBaustein('FIL','FIL_BEZ'),$rsDaten->FeldInhalt('FIL_BEZ'));

					$Frage = $TXT_AdrLoeschen['Wort']['VerbindungMFILoesen'];
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'MBW':
				$SQL = 'DELETE FROM Mitbewerber WHERE mbw_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'MFI':
				$SQL = 'DELETE FROM MitbewerberFilialen WHERE mfi_key=0'.$_POST['txtKey'].' AND MFI_MBW_KEY = '.$_POST['txtMBWKey'];
				$AWIS_KEY1=$_POST['txtMBWKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('mitbewerb_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./mitbewerb_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($Frage);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('MBWKey',$MBWKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>