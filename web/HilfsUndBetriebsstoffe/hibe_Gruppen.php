<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	HilfsUndBetriebsstoffGruppen
* 
* 	Pflege der HilfsUndBetriebsstoffGruppen
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt 2004
* 
****************************************************************************************************/

$Rechtestufe = awisBenutzerRecht($con, 1101);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		//  8=L�schen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffGruppen',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($_POST);
echo '<form name=frmHBG method=post action=./hibe_Main.php?cmdAktion=Gruppen>';

/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM HILFSUNDBETRIEBSSTOFFGRUPPEN WHERE HBG_KEY=0" . $_POST["txtHGB_KEY_old"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			var_dump($SQL);
			
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Die Gruppe wurde erfolgreich gel�scht<br><br><a href=./hibe_Main.php?cmdAktion=Gruppen&Liste=True>Weiter</a>');
	}
	elseif(empty($_POST["cmdLoeschAbbruch"]))
	{
		print "<form name=frmHBG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHGB_KEY_old value=" . $_POST["txtHBG_KEY_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Hilfs- und Betriebsstoffgruppe l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}


if(isset($_POST['cmdSpeichern_x']))
{
	$SQL = '';
	
	if($_POST['txtHBG_KEY_old']=='')
	{
		$SQL = "INSERT INTO HilfsUndBetriebsstoffGruppen";
		$SQL .= "(HBG_BEZEICHNUNG, HBG_BEMERKUNG, HBG_USER, HBG_USERDAT)";
		$SQL .= "VALUES('" . $_POST['txtHBG_BEZEICHNUNG'] . "'";
		$SQL .= ",'" . $_POST['txtHBG_BEMERKUNG'] . "'";
		$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ",SYSDATE)";
	}
	else
	{
		$Aenderungen = '';
		If($_POST['txtHBG_BEZEICHNUNG'] != $_POST['txtHBG_BEZEICHNUNG_old'])
		{
			$Aenderungen = ", HBG_BEZEICHNUNG = '" . $_POST['txtHBG_BEZEICHNUNG'] . "'";
		}
		If($_POST['txtHBG_BEMERKUNG'].'-' != $_POST['txtHBG_BEMERKUNG_old'].'-')
		{
			$Aenderungen .= ", HBG_BEMERKUNG = '" . $_POST['txtHBG_BEMERKUNG'] . "'";
		}
	
		If($Aenderungen!='')
		{
			$SQL = "UPDATE HilfsUndBetriebsstoffGruppen SET " . substr($Aenderungen,2);
			$SQL .= ", HBG_USER='" . $AWISBenutzer->BenutzerName() . "', HBG_USERDAT=SYSDATE";
			$SQL .= " WHERE HBG_KEY=0" . $_POST['txtHBG_KEY_old'];
		}
	}	// Neu oder �ndern

	if($SQL != '')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}	
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************
		
if((!isset($_GET['HBG_KEY']) and !isset($_POST['txtHBG_KEY'])) or isset($_POST['cmdTrefferListe_x']))
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffGruppen";
}
else
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffGruppen WHERE HBG_KEY=0". (isset($_GET['HBG_KEY'])?$_GET['HBG_KEY']:'') . (isset($_POST['txtHBG_KEY'])?$_POST['txtHBG_KEY']:'') ."";
}

if(isset($_GET['Sort']))
{
	$SQL .= " ORDER BY " . $_GET['Sort'];
}
else
{
	$SQL .= " ORDER BY HBG_BEZEICHNUNG";
}

//$rsHBG ='';

if(!isset($_GET['Hinzufuegen']))
{
	$rsHBG = awisOpenRecordset($con, $SQL);
	$rsHBGZeilen = $awisRSZeilen;
}
else
{
	$rsHBGZeilen=-1;
}


	//**************************************************
	// Daten ausgeben
	//**************************************************
	
if($rsHBGZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td width=100 id=FeldBez><a href=./hibe_Main.php?cmdAktion=Gruppen&Sort=HBG_KEY>Nummer</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Gruppen&Sort=HBG_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Gruppen&Sort=HBG_BEMERKUNG>Bemerkung</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHBGZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./hibe_Main.php?cmdAktion=Gruppen&HBG_KEY=" . $rsHBG['HBG_KEY'][$Zeile] . ">" . $rsHBG['HBG_KEY'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBG_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBG_BEMERKUNG'][$Zeile] . "</td>";
		
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{

	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Hilfs- und Betriebsstoffgruppen</h3></td>';
	echo '<td><font size=1>' . (isset($rsHBG['HBG_USER'][0])?$rsHBG['HBG_USER'][0]:'') . '<br>' . (isset($rsHBG['HBG_USERDAT'][0])?$rsHBG['HBG_USERDAT'][0]:'') . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./hibe_Main.php?cmdAktion=Gruppen&Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=*></colgroup>';

		// Bezeichnung
	echo '<tr>';
	echo '<td id=FeldBez>ID</td>';
	echo '<td>' . (isset($rsHBG['HBG_KEY'][0])?$rsHBG['HBG_KEY'][0]:'') . '';
	echo '<input type=hidden name=txtHBG_KEY_old value=' . (isset($rsHBG['HBG_KEY'][0])?$rsHBG['HBG_KEY'][0]:'') . '></td>';
	echo '</tr>';
	

			// Zeile 
	echo '<tr>';
	echo '<td id=FeldBez>Bezeichnung</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBG_BEZEICHNUNG size=40 value='" . (isset($rsHBG['HBG_BEZEICHNUNG'][0])?$rsHBG['HBG_BEZEICHNUNG'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBG_BEZEICHNUNG_old value='" . (isset($rsHBG['HBG_BEZEICHNUNG'][0])?$rsHBG['HBG_BEZEICHNUNG'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBG['HBG_BEZEICHNUNG'][0])?$rsHBG['HBG_BEZEICHNUNG'][0]:'') . '</td>';
	}
	echo '</tr>';

			// Zeile 3
	echo '<tr>';
	echo '<td id=FeldBez>Bemerkung</td>';
	echo '<td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<input type=hidden name=txtHBG_BEMERKUNG_old value='" . (isset($rsHBG['HBG_BEMERKUNG'][0])?$rsHBG['HBG_BEMERKUNG'][0]:'') . "'>";

		echo '<textarea name=txtHBG_BEMERKUNG cols=60 rows=5>';
		echo (isset($rsHBG['HBG_BEMERKUNG'][0])?$rsHBG['HBG_BEMERKUNG'][0]:'') . "</textarea>";
	}
	else
	{
		echo (isset($rsHBG['HBG_BEMERKUNG'][0])?$rsHBG['HBG_BEMERKUNG'][0]:'');
	}
	echo '</td>';
	echo '</tr>';

	echo '</table>';

	if($Rechtestufe>1)
	{
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}
//		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}

	echo '</form>';
				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHBG_BEZEICHNUNG\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
* 
* */

if(isset($_POST['txtZurueck']) && $_POST['txtZurueck']!='')
{
	$ZurueckLink = $_POST['txtZurueck'];
}
else
{
	if(isset($_GET['Zurueck']) && $_GET['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif(!isset($_GET['Zurueck']))
	{
		$ZurueckLink = '/index.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $_GET['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

if(!isset($_GET['Hinzufuegen']))
{
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./hibe_Main.php?cmdAktion=Gruppen&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}

echo '</form>';

?>