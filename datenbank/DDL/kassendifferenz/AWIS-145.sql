--------------------------------------------------------
--  Textkonserven und ToolTipText hinzuf�gen/ �ndern
--------------------------------------------------------

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('LST_KDI_VORZEICHEN','KDI', 'Vorzeichen fuer Selectfeld', '-~Minusdifferenz|+~Plusdifferenz');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('ttt_KDI_FORMAT', 'KDI', 'Format des Betrages', 'Numerisch, mit Komma getrennt und ohne Vorzeichen. Beispiel: 12,99');
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Bitte geben Sie einen g�ltigen Differenzbetrag ein und w�hlen Sie, ob es sich um eine Plus- oder eine Minusdifferenz. (OHNE Euro Zeichen und OHNE Vorzeichen vor dem Betrag).' WHERE XTX_KENNUNG = 'KDI_ERR_BETRAG' AND XTX_BEREICH = 'KDI';