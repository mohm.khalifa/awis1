<?php
/**
 * awis_sonderausweiseJob.inc
 * 
 * Exportiert Personaldatens�tze aus den Sonderausweisen an die WAWI
 * 
 * @author Patrick Gebhardt 
 * @copyright ATU - Auto-Teile-Unger
 * @version 20100419
 * 
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_sonderausweiseJob
{
	/**
	 * Datenbankverbindung
	 * 
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 100=Alles)
	 */
	private $_DebugLevel = 0;
		
	/**
	 * Exportverzeichnis
	 * @var string
	 */	
	private $_ExportDateiPfad = '/daten/daten/sonderausweise/EDV/Personal/';
		
	/**
	 * Exportdatename
	 * @var string
	 */
	private $_ExportDateiName = 'Personal.txt';
	
	/**
	 * Emailbenachrichtigung
	 * @var string
	 */
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');
	
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);		
	}
	
	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);	
		}
		
		return $this->_DebugLevel;
	}
	
	
    public function ExportSonderausweise()
    {
        try 
        {
            $Werkzeug = new awisWerkzeuge();
            
            if($this->_DebugLevel>50)
            {
            	$Werkzeug->EMail($this->_EMAIL_Infos,'OK Sonderausweisexport beginnt.','',1,'','awis@de.atu.eu');
            }
                                  
            if($this->_DebugLevel>5)
            {
                echo date('d.m.Y') . 'Sonderausweisexport BEGINNT'.PHP_EOL;
            }
            
            $Protokoll['ExportierteDS']=0;
            
              
            $Form = new awisFormular();

      
            $SQL="SELECT SAW_PER_NR, SAW_PER_NR_ALT, SAW_NAME, SAW_VORNAME,
			SAW_GEBURTSDATUM, substr(SAW_STRASSE,1,25) AS STRASSE, LAN_AUTOKZ,
			SAW_PLZ, SAW_ORT, SAW_EINTRITT, SAW_AUSTRITT,
			SAW_TAETIGKEIT, SAW_BEREICH, SAW_FIL_ID, RPAD(SAW_KOSTENSTELLE,6,0) AS KOSTENSTELLE,
			ltrim(nvl2(SAW_ZEIT_TAG,to_char(SAW_ZEIT_TAG,'990.99'),'0.00')) AS ZEIT_TAG, SAW_ZEITKONTO
			FROM SONDERAUSWEISE, LAENDER
			WHERE SAW_SAG_KEY not in (2) and SAW_PER_NR IS NOT NULL AND SAW_LAN_CODE=LAN_CODE(+) AND SAW_VORNAME IS NOT NULL AND SAW_NAME IS NOT NULL AND SAW_EINTRITT IS NOT NULL ORDER BY SAW_PER_NR";
			            
						
            if($this->_DebugLevel>50)
            {
                echo date('d.m.Y H:i:s') . ': F�hre SQL aus: '. $SQL .PHP_EOL;
            }

            $rsSonderausweis =$this->_DB->RecordSetOeffnen($SQL);
            
            if($this->_DebugLevel>50)
            {
               	echo date('d.m.Y H:i:s') . ': SQL Abfrage ausgef�hrt. ' .PHP_EOL;
                echo date('d.m.Y H:i:s') . ': �ffne Datei zum schreiben: ' .$this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp' .PHP_EOL;
            }
             
            //Temp Datei erstellen. Damit die WAWI nicht drauf zugreift...
            $Datei = fopen($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp',"a");
            
            while(!$rsSonderausweis->EOF())
            {
                
            	$Zeile = ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_PER_NR') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_PER_NR_ALT') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_NAME') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_VORNAME') . ';';
            	$Zeile .= $Form->Format('D',$rsSonderausweis->FeldInhalt('SAW_GEBURTSDATUM')) . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('STRASSE') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('LAN_AUTOKZ') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_PLZ') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_ORT') . ';';
            	$Zeile .= $Form->Format('D',$rsSonderausweis->FeldInhalt('SAW_EINTRITT')) . ';';
            	$Zeile .= $Form->Format('D',$rsSonderausweis->FeldInhalt('SAW_AUSTRITT')) . ';';
            	$Zeile .= ($rsSonderausweis->FeldInhalt('SAW_TAETIGKEIT')!=''?$rsSonderausweis->FeldInhalt('SAW_TAETIGKEIT'):'Sonstiges') . ';';
            	
            	switch ($rsSonderausweis->FeldInhalt('SAW_BEREICH'))
            	{
            		case 'Sonstiges':
            			$Zeile .= 'S' . ';';
            			break;
            		case 'Verkauf':
            			$Zeile .= 'V' . ';';
            			break;
            		case 'Werkstatt':
            			$Zeile .= 'W' . ';';
            			break;
            		default: 
            			$Zeile .= '' . ';';
            	}
            	
            	
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_FIL_ID') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('KOSTENSTELLE') . ';';
            	$Zeile .= $rsSonderausweis->FeldInhalt('ZEIT_TAG') . ';';
            	$Zeile .= '0.00;0.00;';
            	$Zeile .= $rsSonderausweis->FeldInhalt('SAW_ZEITKONTO') ."\r\n";
          
            	
            	if($this->_DebugLevel>90)
                {
                    echo date('d.m.Y') . 'Exportiere Zeile:  ' . $Zeile .PHP_EOL;
                }
                
           
                fwrite($Datei, $Zeile);
                $Zeile = '';
                ++$Protokoll['ExportierteDS'];
                
               
                $rsSonderausweis->DSWeiter();
            }
            fclose($Datei);
            //Wenn es f�r einen DS keine ZamiNR gibt, darf nichts exportiert werden.
           
          
            
            //Auf .txt umbennen, damit WAWI holen kann
            rename($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp', $this->_ExportDateiPfad.$this->_ExportDateiName);
            $Text= 'Exportierte Datens�tze '.$Protokoll['ExportierteDS'].chr(10);
            
            if($this->_DebugLevel>50)
            {
            	 $Werkzeug->EMail($this->_EMAIL_Infos,'OK Sonderausweis_Export',$Text,1,'','awis@de.atu.eu');
            }
             
            if($this->_DebugLevel>10)
            {
            	echo date('d.m.Y H:i:s') . ': Job erfolgreich abgeschlossen... Datei nach .txt umbennen.   ' .PHP_EOL;
            } 
              
            
        } 
        catch (awisException $ex)
        {
            echo $this->_DB->LetzterSQL().PHP_EOL;      
            echo 'AWIS-Fehler:'.$ex->getMessage().PHP_EOL;            
        }
        catch (Exception $ex)
        {
            echo 'allg. Fehler:'.$ex->getMessage().PHP_EOL;            
        }
        
    }
}



?>
