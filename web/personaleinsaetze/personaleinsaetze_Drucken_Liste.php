<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();


	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
	$Form = new awisFormular();

	$RechteStufe = $AWISBenutzer->HatDasRecht(4500);
	if($RechteStufe==0)
	{
		$Form->DebugAusgabe(1,$AWISBenutzer->BenutzerName());
		$Form->Fehler_KeineRechte();
	}

	$TextKonserven=array();
	$TextKonserven[]=array('PEI','PEI%');
	$TextKonserven[]=array('PEI','tit_AusdruckUebersicht');
	$TextKonserven[]=array('Wort','wrd_Stand');
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,$AWISSprachKonserven['PEI']['tit_AusdruckUebersicht']);
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));

	//**************************************
	// Rechte des Benutzers ermitteln
	//**************************************
	$SQL = 'SELECT DISTINCT PBR_PBZ_KEY ';
	$SQL .= ' FROM PerseinsbereicheRechteVergabe';
	$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
	$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	//Perseins�tze Rechte pr�fen
	$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	if(isset($Param['PEB_KEY']) AND $Param['PEB_KEY']!=0)
	{
		$SQL .= ' AND PBM_PEB_KEY = 0'.$Param['PEB_KEY'];
	}
	$SQL .= ' AND (XBN_STATUS = \'A\' OR XBN_KEY = 0'.$AWISBenutzer->BenutzerID().')';
	$rsPBR = $DB->RecordSetOeffnen($SQL);
	$RechtPEI = 0;
	while(!$rsPBR->EOF())
	{
		$RechtPEI = $RechtPEI  | pow(2,($rsPBR->FeldInhalt('PBR_PBZ_KEY')-1));

		$rsPBR->DSWeiter();
	}
	// Daten ermitteln
	$SQL = 'SELECT DISTINCT kon_name1 || coalesce(\', \'||kon_name2,\'\') AS Mitarbeiter, Personaleinsaetze.*';
	$SQL .= ', XTZVON.XTZ_TAGESZEIT AS ZEITVON,  XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, PEA_BEZEICHNUNG, PEA_KEY, PEG_RGBCODE';
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	$SQL .= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
	$SQL .= ' INNER JOIN PersEinsArtenGruppen ON PEA_PEG_KEY = PEG_KEY';
// SK - 04.06.2009
	if(($RechtPEI&64)==0)	// Nur Bereichsverwalter sehen alle
	{
		// Perseins�tze Rechte pr�fen
		$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBM_XBN_KEY = XBN_KEY AND PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	}
	else
	{
		$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBM_XBN_KEY = XBN_KEY';// AND PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';		
	}

	$SQL.= ' WHERE (PEI_PLANUNGSTYP is null or PEI_PLANUNGSTYP = 2)';
	
	$Bedingung=_BedingungErstellen($Param);

	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}

//	$SQL .= ' ORDER BY 1, PEI_DATUM, PEI_VonXTZ_ID';
	$SQL .= ' ORDER BY PEI_DATUM, 1, PEI_VonXTZ_ID';
//var_dump($SQL);
//die();
	$rsPEI = $DB->RecordSetOeffnen($SQL);

		// Tageszeiten
	$rsXTZ = $DB->RecordSetOeffnen('SELECT * FROM TagesZeiten ORDER BY XTZ_ID');
	$TagesZeiten=array();
	$i=0;
	while(!$rsXTZ->EOF())
	{
		$TagesZeiten[$rsXTZ->FeldInhalt('XTZ_ID')]=$rsXTZ->FeldInhalt('XTZ_TAGESZEIT');
		$rsXTZ->DSWeiter();
	}
	unset($rsXTZ);

	/*************************************
	* PDF aufbauen
	*************************************/
//	$Ausdruck->NeueSeite(0,1);

	$RasterZeile=9999;
	$LetzterMitarbeiter = '';
	$LetztesDatum = '';
	$LetztesREZ='';
	$NeueSeite = true;

	if($rsPEI->EOF())
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);
	}

	while(!$rsPEI->EOF())
	{
		if($LetzterMitarbeiter!=$rsPEI->FeldInhalt('PEI_XBN_KEY'))
		{
					// Neuer Tag?
			$Ausgabe = '';
			//if($LetztesDatum!=$rsPEI->FeldInhalt('PEI_DATUM') OR $LetztesREZ!=$rsPEI->FeldInhalt('MIT_REZ_ID'))
			//if($LetztesDatum!=$rsPEI->FeldInhalt('PEI_DATUM') OR $LetzterMitarbeiter!=$rsPEI->FeldInhalt('PEI_XBN_KEY'))
			if($LetztesDatum!=$rsPEI->FeldInhalt('PEI_DATUM'))
			{
				//$LetztesDatum=$rsPEI->FeldInhalt('PEI_DATUM');
				//$LetztesREZ=$rsPEI->FeldInhalt('MIT_REZ_ID');
				$LetzterMitarbeiter=$rsPEI->FeldInhalt('PEI_XBN_KEY');
				$Ausgabe = $rsPEI->FeldInhalt('MITARBEITER');
				$RasterZeile = 9999;
			}

				// Neue Seite beginnen
			if($RasterZeile > 190)
			{
				$Zeile = $Ausdruck->NeueSeite(0,1);
				$Ausdruck->_pdf->SetFont('Arial','',6);
				$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

				$Ausdruck->_pdf->setXY(10,12);
				$Ausdruck->_pdf->SetFont('Arial','',6);
				$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);

				$RasterZeile = 10;
				$Ausdruck->_pdf->SetFont('Arial','',20);
				$Ausdruck->_pdf->text(10,$RasterZeile,$AWISSprachKonserven['PEI']['tit_AusdruckUebersicht']);
				$RasterZeile = 40;
				$Ausdruck->_pdf->SetFont('Arial','',10);

				$Ausdruck->_pdf->SetFillColor(240,240,240);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->setXY(10,$RasterZeile-10);

				$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['PEI']['PEI_XBN_KEY'],1,0,'C',1);

				for($Std=90;$Std<=240;$Std+=10)
				{
					$Ausdruck->_pdf->setXY(($Std*1.2)-30,$RasterZeile-10);
					$Ausdruck->_pdf->SetDrawColor(0,0,0);
					$Ausdruck->_pdf->cell(12,6,$TagesZeiten[$Std],1,0,'C',1);

					$Ausdruck->_pdf->SetDrawColor(230,230,230);
					$Ausdruck->_pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);
				}
				$Ausdruck->_pdf->SetDrawColor(230,230,230);
				//$Ausdruck->_pdf->line(($Std*1.2)-930,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);

				$Ausgabe = $rsPEI->FeldInhalt('MITARBEITER');
			}

			if($Ausgabe != '')		// Tages/REZ �berschrift
			{
				$Ausdruck->_pdf->setXY(10,$RasterZeile-3);
				$Ausdruck->_pdf->SetFont('Arial','',12);
				$Ausdruck->_pdf->cell(12,6,$Ausgabe,0,0,'L',0);
				$RasterZeile+=6;
			}
			else
			{
					// Neuer Mitarbeiter
				$SQL = "SELECT KKO_KURZWAHL FROM KontakteKommunikation WHERE KKO_KOT_Key =6 AND KKO_KON_Key=0" . $rsPEI->FeldInhalt('MIT_KON_KEY') . "";
				$rsKKO = $DB->RecordSetOeffnen($SQL);

				$Ausdruck->_pdf->SetDrawColor(230,230,230);
				$Ausdruck->_pdf->SetFont('Arial','',12);
				$Ausdruck->_pdf->text(12,$RasterZeile,$rsPEI->FeldInhalt('MITARBEITER'));
				$RasterZeile += 6;
				unset($rsKKO);
			}
			$LetzterMitarbeiter=$rsPEI->FeldInhalt('PEI_XBN_KEY');
		}	// Neuer Mitarbeiter
		//else

		if($LetztesDatum!=$rsPEI->FeldInhalt('PEI_DATUM'))		// gleiche Daten an einem anderen TAG
		{
			$RasterZeile+=6;

			if($RasterZeile > 190)
			{
				$Ausdruck->NeueSeite(0,1);
				$RasterZeile = 10;
				$Ausdruck->_pdf->SetFont('Arial','',6);
				$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
				$Ausdruck->_pdf->SetFont('Arial','',20);
				$Ausdruck->_pdf->text(10,$RasterZeile,$AWISSprachKonserven['PEI']['tit_AusdruckUebersicht']);

				$Ausdruck->_pdf->setXY(10,12);
				$Ausdruck->_pdf->SetFont('Arial','',6);
				$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);

				$RasterZeile = 40;
				$Ausdruck->_pdf->SetFont('Arial','',10);
				$Ausdruck->_pdf->SetFillColor(240,240,240);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->setXY(10,$RasterZeile-10);

				$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['PEI']['PEI_XBN_KEY'],1,0,'C',1);
				for($Std=90;$Std<=240;$Std+=10)
				{
					$Ausdruck->_pdf->setXY(($Std*1.2)-30,$RasterZeile-10);
					$Ausdruck->_pdf->SetDrawColor(0,0,0);
					$Ausdruck->_pdf->cell(12,6,$TagesZeiten[$Std],1,0,'C',1);

					$Ausdruck->_pdf->SetDrawColor(230,230,230);
					$Ausdruck->_pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);
				}
				$Ausdruck->_pdf->SetDrawColor(230,230,230);
				$Ausdruck->_pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);

				// Mitarbeiter wieder am Anfang
				$Ausdruck->_pdf->setXY(10,$RasterZeile-3);
				$Ausdruck->_pdf->SetFont('Arial','',12);
				$Ausdruck->_pdf->cell(260,6,$rsPEI->FeldInhalt('MITARBEITER'),0,0,'L',1);
				$RasterZeile = 53;
			}

			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->setXY(64,$RasterZeile-10);
			$Ausdruck->_pdf->cell(10,6,$Form->Format('D',$rsPEI->FeldInhalt('PEI_DATUM')),0,0,'R',0);
			$LetztesDatum=$rsPEI->FeldInhalt('PEI_DATUM');
		}

				// Eintr�ge schreiben
		$Ausdruck->_pdf->SetFont('Arial','',6);
		$Ausdruck->_pdf->SetDrawColor(0,0,0);

		$PosVon = ($rsPEI->FeldInhalt('PEI_VONXTZ_ID')*1.2)-30;
		$PosBis = ($rsPEI->FeldInhalt('PEI_BISXTZ_ID')*1.2)-30;

		$Ausdruck->_pdf->setXY($PosVon,$RasterZeile-10);

		$Text = $rsPEI->FeldInhalt('PEA_BEZEICHNUNG');
		$Farben = explode(',',$rsPEI->FeldInhalt('PEG_RGBCODE'));
		if(is_array($Farben))		// Farben definiert?
		{
			if($rsPEI->FeldInhalt('PEA_KEY')==8)		// Filiale
			{
				$Text = '' . $rsPEI->FeldInhalt('PEI_FIL_ID');
			}
			elseif($rsPEI->FeldInhalt('PEA_KEY')==9)		// Auto
			{
				$Text = '';
			}
			$Ausdruck->_pdf->SetFillColor($Farben[0],$Farben[1],$Farben[2]);
		}
		else
		{
			$Ausdruck->_pdf->SetFillColor(255,255,255);
		}
		$Text .= ' ' . $rsPEI->FeldInhalt('PEI_BEMERKUNG');
		$Ausdruck->_pdf->cell(($PosBis -$PosVon),6,$Text,1,0,'C',1);

		$rsPEI->DSWeiter();
	}		// Ende for

	/**************************************
	* Datei speichern
	**************************************/

	$Ausdruck->Anzeigen();
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',1,200810231232);
}
catch (Exception $ex)
{

}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PEI_KEY = '.floatval($AWIS_KEY1);
	}

	if(($AWISBenutzer->HatDasRecht(4500)&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PerseinsBereicheMitglieder WHERE ';
		$Bedingung .= ' PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}

	// Einschr�nken nach Bereich
	if(isset($Param['PEB_KEY']) AND $Param['PEB_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PEB_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEB_KEY'],false);
	}
	/*
	// Einschr�nken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
//		$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY']);
		if($Param['PBM_XBN_KEY']==0)
		{
			$Bedingung .= " AND EXISTS(
						    select * from(
						      SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT, PBM_GUELTIGAB, PBM_GUELTIGBIS
						      FROM Perseinsbereichemitglieder
						      INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY
						      INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY
						      WHERE PBM_XBN_KEY = 0".$AWISBenutzer->BenutzerID() . "";
			$Bedingung .= '     GROUP BY PEB_KEY, PBM_GUELTIGAB, PBM_GUELTIGBIS';
			$Bedingung .= ' ) MeineBereiche WHERE PEB_KEY = PBM_PEB_KEY';
			$Bedingung .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
			$Bedingung .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';

			$Bedingung .= ' )';
		}
		else
		{
			$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY']);
		}
	}
	*/
	// Einschr�nken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
//		$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY']);
		if($Param['PBM_XBN_KEY']==0)
		{
			$Bedingung .= " AND EXISTS (SELECT *";
			$Bedingung .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$Bedingung .= "       FROM Perseinsbereichemitglieder";
			$Bedingung .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$Bedingung .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$Bedingung .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			//Perseins�tze Rechte pr�fen
			$Bedingung .= ' AND PBM_GUELTIGAB <= SYSDATE';
			$Bedingung .= ' AND PBM_GUELTIGBIS >= SYSDATE';
			$Bedingung .= " GROUP BY PEB_KEY) Bereiche";
			$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key"; //TR 04.02.11
			//$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$Bedingung .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			//$Bedingung .= ' WHERE pbm_xbn_key = '.$AWISBenutzer->BenutzerID() . '';
			$Bedingung .= ' )';
		}
		else
		{
			$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}


	// Einschr�nken nach Datum
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	if(isset($Param['PEI_PEA_KEY']) AND $Param['PEI_PEA_KEY']!='0')
	{
		$Bedingung .= ' AND PEI_PEA_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEI_PEA_KEY'],false);
	}
	
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$Bedingung .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}

	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$Bedingung .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$Bedingung .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$Bedingung .= ' AND xx1_kon_key = KON_KEY)';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>