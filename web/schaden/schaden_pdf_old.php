<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<body>
<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;

//require_once("register.inc.php");
//require_once("db.inc.php");
//require_once("sicherheit.inc.php");
require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3300);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Schaden-PDF',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_REQUEST);

if ($_REQUEST['BEARB_NR']!='' && ($RechteStufe&16)==16)
{
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE, SACHBEARBEITER@SCHAD.ATU.DE";
	$SQL.=" WHERE BEARBEITER=ID(+) AND BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";
	
	awis_Debug(1,$SQL);
	
	$rsResult = awisOpenRecordset($con,$SQL);
	$rsResultZeilen = $awisRSZeilen;
	
	if($rsResultZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten f�r Bearbeitungs-Nr.: ".$_REQUEST['BEARB_NR']." gefunden!</span></center>");
	}
	else
	{
		$BEARBEITER=$rsResult['SBNAME'][0];
		$BEARBEITUNGSNR=$rsResult['BEARBEITUNGSNR'][0];
		$EINGABEAM=$rsResult['EINGABEAM'][0];
		$EINGANGPER=$rsResult['EINGANGPER'][0];
		$FEHLERBESCHREIBUNG=$rsResult['FEHLERBESCHREIBUNG'][0];
		$FILNR=$rsResult['FILNR'][0];
		$GESCHLECHT=$rsResult['GESCHLECHT'][0];
		$KFZ_KENNZ=$rsResult['KFZ_KENNZ'][0];
		$KUNDENNAME=$rsResult['KUNDENNAME'][0];
		$PLZ=$rsResult['PLZ'][0];
		$ORT=$rsResult['ORT'][0];
		$STRASSE=$rsResult['STRASSE'][0];
		$TELEFON=$rsResult['TELEFON'][0];
		$TELEFON2=$rsResult['TELEFON2'][0];
		$VORNAME=$rsResult['VORNAME'][0];
	}
	
	/***************************************
	* Neue PDF Datei erstellen
	***************************************/
	
	define('FPDF_FONTPATH','font/');
	$pdf = new fpdi('p','mm','a4');
	$pdf->open();
	$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
	$ATULogo = $pdf->ImportPage(1);				
	$pdf->setSourceFile("../bilder/schad_rueck.pdf");
	$Template = $pdf->ImportPage(1);				

	$Protokolltext1='Sehr geehrte Gesch�ftsleiterin / sehr geehrter Gesch�ftlsleiter,';
	$Protokolltext2='von unserem gemeinsamen Kunden erhielten wir die nachfolgende Beanstandung bzw. Anfrage.';
	$Protokolltext3='Bitte setzten Sie sich sofort unter den genannten Kontaktdaten mit den Kunden in Verbindung!';
	$Protokolltext4='Wir erwarten bis sp�testens zu Ihrem Gesch�ftsschluss eine R�ckmeldung,';
	$Protokolltext5='per Fax, mit der von Ihnen mit den Kunden besprochenen Man�nahme.';
	$Protokolltext6='Bei Fragen zur Bearbeitung bzw. bei Unklarheiten wenden Sie sich bitte in erster Linie an Ihren Gebietsleiter.';
	$Protokolltext7='Bitte nutzen Sie zur R�ckmeldung dieses Formular: FAX-Nr.: +49 (0)961 - 3 06 59 29';
	
	$Zeile=0;
	$Seitenrand=15;
	
	NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true);
	
	$LinkerRand=$Seitenrand;
		
	$Y_Wert=20;
	
	$pdf->SetFont('Arial','BU',12);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext1,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->SetFont('Arial','',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext2,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->SetFont('Arial','BU',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext3,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext4,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext5,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->SetFont('Arial','',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext6,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+10;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,$Protokolltext7,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+10;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(100,6,"Kundendaten",1,0,'L',1);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(80,6,"Allgemeine Infos",1,0,'L',1);
	
	$Y_Wert=$Y_Wert+9;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Name / Vorname : ".$KUNDENNAME." ".$VORNAME,0,0,'L',0);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(35,6,"Filiale : ".$FILNR,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Stra�e : ".$STRASSE,0,0,'L',0);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(35,6,"Bearbeitungs-Nr.: ".$BEARBEITUNGSNR,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"PLZ / Ort : ".$ORT." ".$PLZ,0,0,'L',0);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(35,6,"Sachbearbeiter: ".$BEARBEITER,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+6;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Telefon : ".$TELEFON.($TELEFON2!=''?" | ".$TELEFON2:""),0,0,'L',0);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(35,6,"Erfasst am : ".$EINGABEAM,0,0,'L',0);
	
	$Y_Wert=$Y_Wert+9;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,"Kommentar / Reklamation",1,0,'L',1);
	
	$Y_Wert=$Y_Wert+9;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->MultiCell(180,6,$FEHLERBESCHREIBUNG,0,'N',0);
	
	$bla=$pdf->getStringWidth($FEHLERBESCHREIBUNG);
	$Hoehe=round($bla/38+0.5,0);
	
	$Y_Wert=$Y_Wert+$Hoehe+12;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(180,6,"Ergebnis / besprochene Ma�nahme:",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+12;
	$pdf->SetFont('Arial','',10);
	
	for($i=0;$i<7;$i++)
	{
		$pdf->Line($LinkerRand,$Y_Wert,$LinkerRand+180,$Y_Wert);
		$Y_Wert=$Y_Wert+7;
	}
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Gespr�ch mit Kunden:",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+50,$Y_Wert);
	$pdf->cell(35,6,"telefonisch",0,0,'L',0);
	
	$pdf->Rect($LinkerRand+70,$Y_Wert,5,5);
	
	$pdf->setXY($LinkerRand+80,$Y_Wert);
	$pdf->cell(35,6,"vor Ort",0,0,'L',0);
	
	$pdf->Rect($LinkerRand+95,$Y_Wert,5,5);
	
	$Y_Wert=$Y_Wert+9;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"ggf. Termin mit Kunden:",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+50,$Y_Wert);
	$pdf->cell(35,6,"am: _________________",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+100,$Y_Wert);
	$pdf->cell(35,6,"Ort: ____________________",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+9;
	$pdf->Line($LinkerRand,$Y_Wert,$LinkerRand+180,$Y_Wert);
	$Y_Wert=$Y_Wert+3;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Beschwerde berechtigt:",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+55,$Y_Wert);
	$pdf->cell(35,6,"ja",0,0,'L',0);
	$pdf->Rect($LinkerRand+60,$Y_Wert,5,5);
	
	$pdf->setXY($LinkerRand+75,$Y_Wert);
	$pdf->cell(35,6,"nein",0,0,'L',0);
	$pdf->Rect($LinkerRand+85,$Y_Wert,5,5);
	
	$pdf->setXY($LinkerRand+105,$Y_Wert);
	$pdf->cell(35,6,"Kulanz",0,0,'L',0);
	$pdf->Rect($LinkerRand+120,$Y_Wert,5,5);
	
	$Y_Wert=$Y_Wert+9;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(60,6,"Zahlung: Gutschein / Bar / ______________________",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+90,$Y_Wert);
	$pdf->cell(30,6,"Betrag: _______________",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+9;
	$pdf->Line($LinkerRand,$Y_Wert,$LinkerRand+180,$Y_Wert);
	$Y_Wert=$Y_Wert+3;
	
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(35,6,"Sache ist erledigt:",0,0,'L',0);
	$pdf->setXY($LinkerRand+85,$Y_Wert);
	$pdf->cell(35,6,"Info an GBL:",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+35,$Y_Wert);
	$pdf->cell(35,6,"ja",0,0,'L',0);
	$pdf->setXY($LinkerRand+115,$Y_Wert);
	$pdf->cell(35,6,"ja",0,0,'L',0);
	
	$pdf->Rect($LinkerRand+40,$Y_Wert,5,5);
	$pdf->Rect($LinkerRand+120,$Y_Wert,5,5);
	
	$pdf->setXY($LinkerRand+55,$Y_Wert);
	$pdf->cell(35,6,"nein",0,0,'L',0);
	$pdf->setXY($LinkerRand+135,$Y_Wert);
	$pdf->cell(35,6,"nein",0,0,'L',0);
	
	$pdf->Rect($LinkerRand+65,$Y_Wert,5,5);
	$pdf->Rect($LinkerRand+145,$Y_Wert,5,5);
	
	$Y_Wert=$Y_Wert+12;
	
	$pdf->setXY($LinkerRand+10,$Y_Wert);
	$pdf->cell(35,6,"_________________",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+120,$Y_Wert);
	$pdf->cell(35,6,"_________________",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+5;
	
	$pdf->setXY($LinkerRand+10,$Y_Wert);
	$pdf->cell(35,6,"Datum",0,0,'C',0);
	
	$pdf->setXY($LinkerRand+120,$Y_Wert);
	$pdf->cell(35,6,"Name (lesbar)",0,0,'C',0);
	
	
	/*** Protokoll - Zusatz Beginn ***/
	/*$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$Seitenrand+170,4,20);
	$pdf->useTemplate($Template);
	$pdf->SetFont('Arial','',10);
	$pdf->setXY(25,10);
	$pdf->cell(50,8,'Bearbeitungsnummer: 070909999',0,0,'L',0);
	$pdf->setXY(90,10);
	$pdf->cell(50,8,'Kundenname: Max Mustermann',0,0,'L',0);*/
	/*** Protokoll - Zusatz Ende ***/
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
	echo "<hr><a href=./schaden_Main.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";

}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Christian Argauer
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool �berschrift
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+170,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,280);					// Cursor setzen
	$pdf->Cell(180,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);
	
	
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',18);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"ATU Protokoll - Qualit�tsmanagment",0,0,'C',0);
	
	$pdf->setXY($LinkerRand,283);				// Cursor setzen
	$pdf->SetFont('Arial','',6);				// Schrift setzen
	$pdf->cell(180,6,"Stand: " . date('d.m.Y'),0,0,'C',0);

	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	
	$Zeile = 22;
}	
	
	
?>
</body>
</html>
