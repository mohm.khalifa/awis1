<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;
global $con;
global $awisRSZeilen;


$HBSKey = awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName());
$SQL = 'SELECT * FROM HILFSUNDBETRIEBSSTOFFEINSAETZE';
$SQL .= ' INNER JOIN Filialen ON HBE_FIL_ID = FIL_ID';
$SQL .= ' WHERE HBE_HBS_KEY=0' . $HBSKey;
$SQL .= ' ORDER BY HBE_DATUMBIS DESC';

$rsHBE = awisOpenRecordset($con, $SQL);
$rsHBEZeilen = $awisRSZeilen;

//var_dump($rsHBE);

echo '<table border=1 width=100%>';

echo '<tr>';
echo '<td id=FeldBez width=120>Datum vom</td>';
echo '<td id=FeldBez width=120>Datum bis</td>';
echo '<td id=FeldBez width=350>Filiale</td>';
echo '<td id=FeldBez>Bemerkung</td>';
echo '</tr>';

for($HBEZeile=0;$HBEZeile<$rsHBEZeilen;$HBEZeile++)
{
	echo '<tr>';
	
	echo '<td>';
	echo '<a href=./hibe_Main.php?cmdAktion=Einsatz&HBE_KEY=' . $rsHBE['HBE_KEY'][$HBEZeile] . '>';
	echo $rsHBE['HBE_DATUMVOM'][$HBEZeile];
	echo '</a> ';
	echo '</td>';

	echo '<td>';
	echo $rsHBE['HBE_DATUMBIS'][$HBEZeile];
	echo '</td>';

	echo '<td>';
	echo '<a href=/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsHBE['FIL_ID'][$HBEZeile] . '>';
	echo $rsHBE['HBE_FIL_ID'][$HBEZeile];
	echo '</a> (';
	echo $rsHBE['FIL_BEZ'][$HBEZeile];
	echo ')</td>';

	echo '<td>';
	echo $rsHBE['HBE_BEMERKUNG'][$HBEZeile];
	echo '</td>';

	echo '<tr>';
}

echo '</table>';








?>