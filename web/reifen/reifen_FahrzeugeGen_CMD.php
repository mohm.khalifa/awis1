<?php

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

$Bedingung = '';
$SQL = '';
global $awisRSZeilen;
global $HTTP_POST_VARS;
// DB Init

// HIER FORSCHEN
$con = OCILogon('awis','Iloulua','AWIS25');

echo '</tr></td></table>';

//var_dump($HTTP_POST_VARS);

$SQL = "SELECT * FROM V_KFZ_DATENBLATT, ReifenFahrzeuge WHERE HER=REF_KBH_ID(+) AND TYP=REF_KBT_ID(+) AND AUSF=REF_KBA_ID(+) 
AND REF_KBH_ID IS NULL
AND ROWNUM < 200";

$rsReifen = awisOpenRecordset($con, $SQL);
$rsReifenZeilen = $awisRSZeilen;

Print "Zeilen: " . $awisRSZeilen . "<br>";

for($i=0;$i<$rsReifenZeilen;$i++)
{

//	echo '.';
	flush();

	$Felder = array('REIFV', 'REIFH', 'REIHZ', 'REIVZ');
	
	for($j=0;$j<4;$j++)	
	{
		$Ziffer = 20+$j;
		$Zeile = $rsReifen[$Felder[$j]][$i];

		SucheReifenDaten($Zeile,$rsReifen['HER'][$i],$rsReifen['TYP'][$i],$rsReifen['AUSF'][$i],$Ziffer,$con,$rsReifen['HERTX'][$i],$rsReifen['TYPTX'][$i],$rsReifen['FZAT'][$i],$rsReifen['REIFV'][$i],$rsReifen['REIFH'][$i],$rsReifen['REIVZ'][$i],$rsReifen['REIHZ'][$i],$rsReifen['ZUSATZTEXT'][$i]);
	}	

	$Zeile = $rsReifen['ZUSATZTEXT'][$i];
	
	$pos = strpos($Zeile, '/');
	while($pos > 0)
	{
		$ReifenText = substr($Zeile,$pos-3,14);
		if(substr($ReifenText,6,1)=='R' OR substr($ReifenText,6,1)=='D')
		{
			if(strpos($ReifenText,'('))			// Format ...R15C(106/108)Q
			{
				$ReifenText = substr($Zeile,$pos-3,20);				
			}
//			echo '<br>------POS: ' . $pos;
//			echo $ReifenText;
			$ReifenText = str_replace(chr(10),'',$ReifenText);
			$ReifenText = str_replace(chr(13),'',$ReifenText);
			$Ziffer = 33;
						
			SucheReifenDaten(trim($ReifenText), $rsReifen['HER'][$i],$rsReifen['TYP'][$i],$rsReifen['AUSF'][$i],$Ziffer,$con,$rsReifen['HERTX'][$i],$rsReifen['TYPTX'][$i],$rsReifen['FZAT'][$i],$rsReifen['REIFV'][$i],$rsReifen['REIFH'][$i],$rsReifen['REIVZ'][$i],$rsReifen['REIHZ'][$i],$rsReifen['ZUSATZTEXT'][$i]);
		}
		$Zeile = substr($Zeile,$pos+1);
		$pos = strpos($Zeile, '/');
	}

	echo '<hr>'.$rsReifen['ZUSATZTEXT'][$i].'<hr>';


}

/********************************************************************************
* 
* 
* 
********************************************************************************/
function SucheReifenDaten($Zeile, $Herst, $Typ, $Ausf, $Ziffer, $con, $txtHerst, $txtTyp, $txtAusf, $Ziff20, $Ziff21, $Ziff22, $Ziff23, $Ziff33)
{
	$CKennz='';
	$TragLast2='';
	
	echo '<br>';
	
	echo $j . '. # ' . $Zeile;
	
	$Breite=0;
	if(substr($Zeile,3,1)=='/')				// z.B. 275/70R15
	{
		$Breite = substr($Zeile,0,3);
		$Quer = substr($Zeile,4,2 );
		if(substr($Zeile,6,1)=='R' OR substr($Zeile,6,1)=='D')
		{
			$RZeichen = substr($Zeile,6,1);
			$Felge = substr($Zeile,7,2);

			$CKennz = substr($Zeile, 9,1);			// C

			if(strlen($Zeile)>13)			// Dreistellige Traglast
			{
				$TragLast = substr($Zeile,10,3);
				$SpeedIndex = substr($Zeile,13,1);
				if(!is_int($TragLast))
				{
					$TragLast = substr($Zeile,10,2);
					$SpeedIndex = substr($Zeile,12,1);
				}
			}
			else
			{
				$TragLast = substr($Zeile,10,2);
				$SpeedIndex = substr($Zeile,12,1);
			}

			if(substr($TragLast,0,1)=='(')
			{
				$TragLast = intval(substr($Zeile,11,3));
				$pos = strpos(substr($Zeile,11),",");
				$TragLast2 = intval(substr($Zeile,$pos+15,3));
				$pos = strpos($Zeile,")");
				$SpeedIndex = substr($Zeile,$pos+1,1);
			}
			
		}
		elseif(substr($Zeile,7,1)=='R' OR substr($Zeile,7,1)=='D')
		{
			$SpeedIndex = substr($Zeile,6,1);
			$RZeichen = substr($Zeile,7,1);
			$Felge = substr($Zeile,8,2);
			$TragLast = substr($Zeile,10,3);			
		}
	}
	elseif(substr($Zeile,1,1)=='R')
	{
		if(substr($Zeile,0,1)=='D')
		{
			$Breite = '175';			
		}
		elseif(substr($Zeile,0,1)=='T')
		{
			$Breite = '185';			
		}
		else
		{
			$Breite = '';
		}

		$Quer = substr($Zeile,2,2);
		$SpeedIndex = substr($Zeile,4,1);
		$RZeichen = substr($Zeile,5,1);
		$Felge = substr($Zeile,6,2);
		$TragLast = '';
	}
	elseif(substr($Zeile,2,1)=='X')		// US Fahrzeuge
	{
		$Breite = substr($Zeile,0,2);
		$Quer = substr($Zeile,3,5);
		$RZeichen = substr($Zeile,8,1);
		$Felge = substr($Zeile,9,2);

		$pos = strpos($Zeile,'LT');
		if($pos > 0)
		{
//			echo '::' . trim(substr($Zeile,$pos)) . '::';
			$TragLast = substr(trim(substr($Zeile,$pos+2)),0,3);
			$SpeedIndex = substr(trim(substr($Zeile,$pos+2)),3,1);
		}
		else
		{
			$pos = 11;
//				echo '::::' . trim(substr($Zeile,$pos)) . ':::';
			$TragLast = substr(trim(substr($Zeile,$pos)),$pos+2,3);
			$SpeedIndex = substr(trim(substr($Zeile,$pos)),$pos+4,1);
			if(!is_int($TragLast))
			{
				$TragLast = substr(trim(substr($Zeile,$pos)),$pos+2,2);
				$SpeedIndex = substr(trim(substr($Zeile,$pos)),$pos+3,1);
			}
		}

	}
	else
	{
		$Breite = substr($Zeile,0,3);
		$SpeedIndex = substr($Zeile,3,1);
		$RZeichen = substr($Zeile,4,1);
		$Felge = substr($Zeile,5,2);
		$TragLast = '';
		$Quer = '';
	}

	If(strstr($Zeile, 'OD.'))		// Alternativer Reifen
	{
		
	}
	
	echo '<br>   - Breite ' . ($Breite==''?0:$Breite) . ', Querschnitt ' . $Quer .', Bauart '. $RZeichen . ', Felge ' . $Felge . ', Traglast ' . $TragLast . ', Traglast2 ' . $TragLast2 . ', Speedindex ' . $SpeedIndex . ', CKennz ' . $CKennz;
	echo '<br>   - HERST ' . $rsReifen['HER'][$i] . ', TYP ' . $rsReifen['TYP'][$i] . ', AUSF ' . $rsReifen['AUSF'][$i];

	$SQL = 'INSERT INTO REIFENFAHRZEUGE (REF_KBH_ID, REF_KBT_ID, REF_KBA_ID, 
   REF_ZIFFER, REF_BREITE, REF_QUERSCHNITT, 
   REF_BAUART, REF_INNENDURCHMESSER, REF_LLKW, 
   REF_LOADINDEX, REF_LOADINDEX2, REF_SPI_SYMBOL, 
   REF_HERSTELLER, REF_TYP, REF_FAHRZEUGART, 
   REF_ZIFF20, REF_ZIFF21, REF_ZIFF22, 
   REF_ZIFF23, REF_ZIFF33)';

	$SQL .= " VALUES('" . $Herst . "', '" . $Typ . "', '" . $Ausf  . "', '" . $Ziffer;
	$SQL .= "', " . $Breite . ", '" . $Quer . "', '" . $RZeichen . "', '" . $Felge . "', '" . $CKennz . "', '" . $TragLast . "', '" . $TragLast2 . "', '" . $SpeedIndex;
	$SQL .= "', '" . $txtHerst . "', '" . $txtTyp . "', '" . $txtAusf;
	$SQL .= "', '" . $Ziff20 . "', '" . $Ziff21 ."', '" . $Ziff22 ."', '" . $Ziff23 ."', '" . $Ziff33 ."')";

	echo '<br>____' . $SQL;
	
	$Erg = awisExecute($con, $SQL);
	flush();
}



awisLogoff($con);

?>
