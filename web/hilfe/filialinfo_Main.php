<html>
<head>
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis 1.0 - ATU webbasierendes Informationssystem</title>
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<?php
require_once("register.inc.php");
require_once("db.inc.php"); // DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) . ">";
?>
</head>

<body>
<?php
global $PHP_AUTH_USER; // USER Kennung
global $QUERY_STRING;
global $HTTP_POST_VARS;
global $HTTP_GET_VARS;


include ("ATU_Header.php"); // Kopfzeile

$con = awislogon();

if(awisBenutzerRecht($con,100)==0)
{
     awisEreignis(3, 1000, 'Filialen', "$PHP_AUTH_USER", '', '', '');
     die("Keine ausreichenden Rechte!");
}

if($con == FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

/**
 * Daten speichern
 */

if($HTTP_POST_VARS["cmdSpeichern_x"]) 
{ // Nur wenn Speichernknopf aktiv ist

	if((awisBenutzerRecht($con, 110) & 2) == 2) 
	{ // Speichern Filialpreise
    
//		global $txtStundenSatz; // Neuer Wert
           
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,200,0" . abs(str_replace(",", ".", $HTTP_POST_VARS["txtStundenSatz"])) . ",'$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
    }
	
	$DatenBearbeitung = awisBenutzerRecht($con, 118);
	if(($DatenBearbeitung & 1)==1)		// T�V-Termine
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,41,'" . $HTTP_POST_VARS["txtTUEVMontag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,42,'" . $HTTP_POST_VARS["txtTUEVDienstag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,43,'" . $HTTP_POST_VARS["txtTUEVMittwoch"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,44,'" . $HTTP_POST_VARS["txtTUEVDonnerstag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,45,'" . $HTTP_POST_VARS["txtTUEVFreitag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,46,'" . $HTTP_POST_VARS["txtTUEVSamstag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}

	if(($DatenBearbeitung & 2)==2)		// Wegbeschreibung
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,47,'" . $HTTP_POST_VARS["txtWegBeschreibung"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}	

	if(($DatenBearbeitung & 4)==4)		// Anlieferungstag
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,49,'" . $HTTP_POST_VARS["txtAnlieferungstag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}	

	if(($DatenBearbeitung & 8)==8)		// Kommissioniertag
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,48,'" . $HTTP_POST_VARS["txtKommissioniertag"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}	

	if(($DatenBearbeitung & 16)==16)		// Gebietsleiter
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,72,'" . $HTTP_POST_VARS["txtGebLeiter"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}	
	if(($DatenBearbeitung & 32)==32)		// Verkaufsleiter
	{
        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,71,'" . $HTTP_POST_VARS["txtVerkLeiter"] . "','$PHP_AUTH_USER'); END;";
        awisExecute($con, $SQL);
	}	

	
	
		//*************************************
		// TUEV-Bemerkungen
		//*************************************		
	$FeldName = awis_NameInArray($HTTP_POST_VARS, "txtPRB_BEMERKUNG_");
	if($FeldName != '')	
	{
		for($i=0;$i<99;$i++)
		{
			if(!isset($HTTP_POST_VARS["txtPRB_BEMERKUNG_$i"]))		
			{
				break;
			}
			$FTUParm = explode("~", $HTTP_POST_VARS["txtPRA_ID_$i"]);

			$SQL = "SELECT * FROM PruefBemerkungen ";
			$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
			$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
			$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];
			
			$rsTest = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen > 0)
			{
				If($HTTP_POST_VARS["txtPRB_BEMERKUNG_$i"]=='')
				{
					$SQL = 'DELETE FROM PruefBemerkungen ';
					$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
					$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
					$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];
				}
				else
				{
					$SQL = "UPDATE PruefBemerkungen SET PRB_BEMERKUNG='" . $HTTP_POST_VARS["txtPRB_BEMERKUNG_$i"] . "'";
					$SQL .= ", PRB_USER = '" . $PHP_AUTH_USER . "'";
					$SQL .= ", PRB_USERDAT = SYSDATE";
					$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
					$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
					$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];
				}
			}
			elseIf($HTTP_POST_VARS["txtPRB_BEMERKUNG_$i"]!='')			// Neu hinzuf�gen
			{
				$SQL = "INSERT INTO PruefBemerkungen (PRB_FIL_ID, PRB_PRG_ID, PRB_PRA_ID, PRB_BEMERKUNG, PRB_USER, PRB_USERDAT)";
   				$SQL .= " VALUES(";
				$SQL .= " " . $FTUParm[0] . "";
				$SQL .= " ," . $FTUParm[1] . "";
				$SQL .= " ," . $FTUParm[2] . "";
				$SQL .= ", '" . $HTTP_POST_VARS["txtPRB_BEMERKUNG_$i"] . "'";
				$SQL .= ", '" . $PHP_AUTH_USER . "'";
				$SQL .= ", SYSDATE)";
			}
			awisExecute($con, $SQL);
		}
	}
	
	


} // Ende Speichern
    

	// Auswahl l�schen und leere Maske anzeigen
if(isset($HTTP_GET_VARS["Reset"]))
{
	awis_BenutzerParameterSpeichern($con, "FilialSuche", $_SERVER['PHP_AUTH_USER'], '');
    $cmdAktion = '';
}

    /**
     * Daten anzeigen
     */

	print "<form name=frmFilialInfo method=post action=./filialinfo_Main.php>";	// . $QUERY_STRING . ">";

    $SpeichernButton = False;
    
    awis_RegisterErstellen(1, $con);
    
    print "<br><hr><img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
    
    if($SpeichernButton)
    {
         print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./filialinfo_Main.php?" . $QUERY_STRING . "&Speichern=True'>";
    }
    
    print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=filialen&HilfeBereich=" . $cmdAktion . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";
    
    print "</form>";
    
    // include "debug_info.php";
    awislogoff($con);
    
    ?>
</body>
</html>

