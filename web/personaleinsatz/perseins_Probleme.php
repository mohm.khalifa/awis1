<?php

// Variablen
global $_POST;
global $_GET;
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe1300 = awisBenutzerRecht($con, 1300);
$RechteStufe = awisBenutzerRecht($con, 1303);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze Probleme',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(($RechteStufe&2)==0)		// Nicht alle
{
	$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschr�nkungen nach RZ
	$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $AWISBenutzer->BenutzerName()));
	
	$RZUserListe = '';
	for($i=0;$i<16;$i++)
	{
		if(($RZListeStufe&pow(2,$i))==pow(2,$i))
		{
			$RZUserListe .= ';' . $RZListe[$i+1];
		}
	}
	$RZUserListe .= ';';
}
else
{
	$RZUserListe = '';
}
$MBEBereiche = awisBenutzerRecht($con, 1306);
if($MBEBereiche==0)		// Wg. Umstellung!! -> Rausnehmen, wenn Rechte vergeben wurden
{
	$MBEBereiche=1;
}
$MBEBereicheAnz = substr_count(sprintf('%b',$MBEBereiche),'1');		// Anzahl Bereiche

awis_Debug(1,$_REQUEST);

	// Tageszeiten
$rsXTZ = awisOpenRecordset($con, 'SELECT * FROM TagesZeiten');
$TagesZeiten=array();
for($i=0;$i<$awisRSZeilen;$i++)
{
	$TagesZeiten[$rsXTZ['XTZ_ID'][$i]]=$rsXTZ['XTZ_TAGESZEIT'][$i];
}
unset($rsXTZ);

// var_dump($_POST['txtMEP_TAG']);
// Aktuellen Tag ermitteln
if(isset($_POST['txtMEP_TAG']))
{
	$AnzeigeTag = awis_format($_POST['txtMEP_TAG'],'Datum');
}
else
{
	$AnzeigeTag = awis_BenutzerParameter($con, 'Mitarbeiter_Probleme_Datum', $AWISBenutzer->BenutzerName());
	if($AnzeigeTag==';')
	{
		$AnzeigeTag = date('d.m.Y');
	}
}
	// Letzten Tag speichern
awis_BenutzerParameterSpeichern($con, 'Mitarbeiter_Probleme_Datum', $AWISBenutzer->BenutzerName(),$AnzeigeTag);

/****************************************
* Daten aktualisieren (Tagesstatus)
****************************************/
	// Tagesdaten lesen
if(isset($_POST['txtMEP_BEREICH']))
{
	$MBEBereich = $_POST['txtMEP_BEREICH'];
}else{	
	$MBEBereich = '';
}

if($MBEBereich=='')
{
	//$MBEBereich = (($MBEBereiche&1)==1?'Vertrieb':'ATU Academy');
	// erstellt am 21.02.06 CA
	// ge�ndert am 06.06.06 CA
	//$MBEBereich = ((($MBEBereiche&1)==1)?'Vertrieb':(($MBEBereiche&2)==2)?'ATU Academy':'Filial-QS');
	//$MBEBereich = ((($MBEBereiche & 1)==1)?'Vertrieb':(($MBEBereiche & 2)==2)?'ATU Academy':((($MBEBereiche & 4)==4)?'Filial-QS':'Gro�kunden'));
	$found=false;
	if(($MBEBereiche & 1)==1)			// Vertrieb
	{
		$MBEBereich = 'Vertrieb';
		$found=true;
	}
	if(($MBEBereiche & 2)==2 && $found==false)			// ATU Academy
	{
		$MBEBereich = 'ATU Academy';
		$found=true;
	}
	if(($MBEBereiche & 4)==4 && $found==false)			// Filial-QS erstellt am 21.02.06 CA
	{
		$MBEBereich = 'Filial-QS';
		$found=true;
	}
	if(($MBEBereiche & 8)==8 && $found==false)			// Gro�kunden erstellt am 06.06.06 CA
	{
		$MBEBereich = 'Gro�kunden';
		$found=true;
	}
	if(($MBEBereiche & 16)==16 && $found==false)			// Category Management erstellt am 19.04.07 CA
	{
		$MBEBereich = '"Category Management"';
		$found=true;
	}
	if(($MBEBereiche & 32)==32 && $found==false)			// Smart-Repair erstellt am 19.11.07 CA
	{
		$MBEBereich = '"Smart-Repair"';
		$found=true;
	}
	if(($MBEBereiche & 64)==64 && $found==false)			// Service-Europa-L erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-L"';
		$found=true;
	}
	if(($MBEBereiche & 128)==128 && $found==false)			// Service-Europa-TSB erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-TSB"';
		$found=true;
	}
	if(($MBEBereiche & 256)==256 && $found==false)			// Service-Europa-WT erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-WT"';
		$found=true;
	}
	if(($MBEBereiche & 512)==512 && $found==false)			// Service-Europa-HUB erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-HUB"';
		$found=true;
	}
	if(($MBEBereiche & 1024)==1024 && $found==false)		// Service-Europa-TWB erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-TWB"';
		$found=true;
	}
	if(($MBEBereiche & 2048)==2048 && $found==false)		// Service-Europa-DLM erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-DLM"';
		$found=true;
	}
	if(($MBEBereiche & 4096)==4096 && $found==false)		// Service-Europa-PM erstellt am 06.02.08 TR
	{
		$MBEBereich = '"Service-Europa-PM"';
		$found=true;
	}			
}

if(isset($_POST['cmdSpeichern_x']))
{
		// Pr�fen, ob die Daten ver�ndert wurden
	$rsMEP = awisOpenRecordset($con, "SELECT * FROM MitarbeiterEinsaetzePruefungen WHERE MEP_TAG='".$AnzeigeTag."' AND MEP_BEREICH='" . $MBEBereich . "'");
	if($rsMEP['MEP_TAG']!='')		// Nur wenn Daten gespeichert waren
	{
		$Aenderung = '';
		if($rsMEP['MEP_STATUS'][0]!=$_POST['txtMEP_STATUS_old'])
		{
			$Aenderung .= '<br><b>Status</b> von [' . $_POST['txtMEP_STATUS_old'] . '] auf [' . $rsMEP['MEP_STATUS'][0] . ']';
		}
		if($rsMEP['MEP_BEMERKUNG'][0]!=$_POST['txtMEP_BEMERKUNG_old'])
		{
			$Aenderung .= ('<br><b>Bemerkung</b> von [' . $_POST['txtMEP_BEMERKUNG_old'] . '] auf [' . $rsMEP['MEP_BEMERKUNG'][0] . ']');
		}
		if($rsMEP['MEP_BEREICH'][0]!=$_POST['txtMEP_BEREICH_old'])
		{
			$Aenderung .= '<br><b>Bereich</b> von [' . $_POST['txtMEP_BEREICH_old'] . '] auf [' . $rsMEP['MEP_BEREICH'][0] . ']';
		}
	
		if($Aenderung!='')
		{
			echo '<span class=Hinweistext>Der Datensatz wurde in der Zwischenzeit vom Benutzer <b>' . $rsMEP['MEP_USER'][0];
			echo '</b> ver�ndert und gespeichert.<br><br><b>Folgende Felder wurden ver�ndert:</b><br><br>';
			echo $Aenderung;
			echo '<br><br><b>Die Daten werden nicht gespeichert! Bitte wiederholen Sie die Eingabe.</b></span><hr>';		
		}
	
		$SQL = '';
		
		if($_POST['txtMEP_STATUS'] != $_POST['txtMEP_STATUS_old'])
		{
			$SQL .= ", MEP_STATUS='" . $_POST['txtMEP_STATUS'] . "'";
		}
		if($_POST['txtMEP_BEMERKUNG'] != $_POST['txtMEP_BEMERKUNG_old'])
		{
			$SQL .= ", MEP_BEMERKUNG='" . $_POST['txtMEP_BEMERKUNG'] . "'";
		}
		if($_POST['txtMEP_BEREICH'] != $_POST['txtMEP_BEREICH_old'])
		{
			$SQL .= ", MEP_BEREICH='" . $_POST['txtMEP_BEREICH'] . "'";
		}
		if($SQL != '' AND $Aenderung=='')
		{
			$SQL = 'UPDATE MitarbeiterEinsaetzePruefungen SET ' . substr($SQL,2);
			$SQL .= " ,MEP_USER='" . $AWISBenutzer->BenutzerName() . "'";
			$SQL .= " ,MEP_USERDAT=SYSDATE";
			$SQL .= " WHERE MEP_TAG='" . $_POST['txtMEP_TAG'] . "'";
			$SQL .= " AND MEP_BEREICH='" . $_POST['txtMEP_BEREICH'] . "'";
			
			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Probleme.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
		}
	}
}

$rsMEP = awisOpenRecordset($con, "SELECT * FROM MitarbeiterEinsaetzePruefungen WHERE MEP_TAG='".$AnzeigeTag."' AND MEP_BEREICH='" . $MBEBereich . "'");
if($awisRSZeilen==0)
{
	awisExecute($con, "INSERT INTO MitarbeiterEinsaetzePruefungen(MEP_TAG,MEP_STATUS,MEP_BEREICH) VALUES('" . $AnzeigeTag . "','O','" . $MBEBereich . "')");
awis_Debug(1,"INSERT INTO MitarbeiterEinsaetzePruefungen(MEP_TAG,MEP_STATUS,MEP_BEREICH) VALUES('" . $AnzeigeTag . "','O','" . $MBEBereich . "')");	
	$rsMEP = awisOpenRecordset($con, "SELECT * FROM MitarbeiterEinsaetzePruefungen WHERE MEP_TAG='".$AnzeigeTag."' AND MEP_BEREICH='" . $MBEBereich . "'");
}

/**********************************************
* Seite aufbauen
**********************************************/

echo '<form name=frmProbleme action=./perseins_Main.php?cmdAktion=Probleme method=post>';
echo '<table border=0>';
echo '<tr><td class=FeldBez>Datum:</td><td><input type=text size=10 name=txtMEP_TAG value=';
$EingabeFeld='txtMEP_TAG';
echo $AnzeigeTag;
echo '></td></tr>';

if(($RechteStufe&4)==4 AND ($RechteStufe&8)==0)	// Nur anzeigen
{
	echo '<tr><td class=FeldBez>Status:</td><td>';
	if($rsMEP['MEP_STATUS'][0]=='A')
	{
		echo 'Abgeschlossen.';
	}
	else
	{
		echo 'Offen.';
	}
	echo '</td></tr>';
}
elseif(($RechteStufe&8)==8)						// Bearbeiten
{
	echo '<tr><td class=FeldBez>Status:</td>';
	echo '<td><input type=hidden name=txtMEP_TAG_old value=' .  $AnzeigeTag . '>';
	echo '<input type=hidden name=txtMEP_STATUS_old value=' . $rsMEP['MEP_STATUS'][0] . '>';
	echo "<input type=hidden name=txtMEP_BEMERKUNG_old value='" . $rsMEP['MEP_BEMERKUNG'][0] . "'>";	
	echo '<select name=txtMEP_STATUS>';
	echo '<option ' . ($rsMEP['MEP_STATUS'][0]=='O'?'selected':'') . ' value=O>Offen</option>';
	echo '<option ' . ($rsMEP['MEP_STATUS'][0]=='A'?'selected':'') . ' value=A>Abgeschlossen</option>';
	echo '</select></td></tr>';

	echo '<tr><td class=FeldBez>Bemerkung:</td>';
	echo "<td><input type=text size=100 name=txtMEP_BEMERKUNG value='" .  $rsMEP['MEP_BEMERKUNG'][0] . "'>";
	echo '</td></tr>';	

			// Bereiche
	echo "<input type=hidden name=txtMEP_BEREICH_old value='" . $rsMEP['MEP_BEREICH'][0] . "'>";
	if($MBEBereicheAnz>1)
	{			
		echo "<tr><td id=FeldBez>";
		echo 'Bereich:</td><td><select name=txtMEP_BEREICH>';
		if(($MBEBereiche & 1)==1)			// Vertrieb
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='Vertrieb'?' selected':'') . '>Vertrieb</option>';
		}
		if(($MBEBereiche & 2)==2)			// ATU Academy
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='ATU Academy'?' selected':'') . '>ATU Academy</option>';
		}
		if(($MBEBereiche & 4)==4)			// Filial-QS erstellt am 21.02.06 CA
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='Filial-QS'?' selected':'') . '>Filial-QS</option>';
		}
		if(($MBEBereiche & 8)==8)			// Gro�kunden erstellt am 06.06.06 CA
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='Gro�kunden'?' selected':'') . '>Gro�kunden</option>';
		}
		if(($MBEBereiche & 16)==16)			// Category Management erstellt am 19.04.07 CA
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Category Management"'?' selected':'') . '>Category Management</option>';
		}
		if(($MBEBereiche & 32)==32)			// Smart-Repair erstellt am 19.11.07 CA
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Smart-Repair"'?' selected':'') . '>Smart-Repair</option>';
		}
		if(($MBEBereiche & 64)==64)			// Service-Europa-L erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-L"'?' selected':'') . '>Service-Europa-L</option>';
		}
		if(($MBEBereiche & 128)==128)			// Service-Europa-TSB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-TSB"'?' selected':'') . '>Service-Europa-TSB</option>';
		}
		if(($MBEBereiche & 256)==256)			// Service-Europa-WT erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-WT"'?' selected':'') . '>Service-Europa-WT</option>';
		}
		if(($MBEBereiche & 512)==512)			// Service-Europa-HUB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-HUB"'?' selected':'') . '>Service-Europa-HUB</option>';
		}
		if(($MBEBereiche & 1024)==1024)			// Service-Europa-TWB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-TWB"'?' selected':'') . '>Service-Europa-TWB</option>';
		}
		if(($MBEBereiche & 2048)==2048)			// Service-Europa-DLM erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-DLM"'?' selected':'') . '>Service-Europa-DLM</option>';
		}
		if(($MBEBereiche & 4096)==4096)			// Service Europa erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMEP['MEP_BEREICH'][0]=='"Service-Europa-PM"'?' selected':'') . '>Service-Europa-PM</option>';
		}												
		echo "</select></td></tr>";
	}
	else 
	{
		if(($MBEBereiche & 1)==1)			// Vertrieb
		{
			$Bereich='Vertrieb';
		}
		if(($MBEBereiche & 2)==2)			// ATU Academy
		{
			$Bereich='ATU Academy';
		}
		if(($MBEBereiche & 4)==4)			// Filial-QS - erstellt am 21.02.06 CA
		{
			$Bereich='Filial-QS';
		}
		if(($MBEBereiche & 8)==8)			// Gro�kunden - erstellt am 06.06.06 CA
		{
			$Bereich='Gro�kunden';
		}
		if(($MBEBereiche & 16)==16)			// Category Management - erstellt am 19.04.07 CA
		{
			$Bereich='"Category Management"';
		}
		if(($MBEBereiche & 32)==32)			// Smart-Repair - erstellt am 19.11.07 CA
		{
			$Bereich='"Smart-Repair"';
		}
		if(($MBEBereiche & 64)==64)			// Service-Europa-L - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-L"';
		}
		if(($MBEBereiche & 128)==128)			// Service-Europa-TSB - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-TSB"';
		}
		if(($MBEBereiche & 256)==256)			// Service-Europa-WT - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-WT"';
		}
		if(($MBEBereiche & 512)==512)			// Service-Europa-HUB - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-HUB"';
		}
		if(($MBEBereiche & 1024)==1024)			// Service-Europa-TWB - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-TWB"';
		}
		if(($MBEBereiche & 2048)==2048)			// Service-Europa-DLM - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-DLM"';
		}
		if(($MBEBereiche & 4096)==4096)			// Service-Europa-PM - erstellt am 06.02.08 TR
		{
			$Bereich='"Service-Europa-PM"';
		}												
	
		//echo '<input type=Hidden name=txtMEP_BEREICH value=' . ((($MBEBereiche & 1)==1)?'Vertrieb':(($MBEBereiche & 2)==2)?'ATU Academy':((($MBEBereiche & 4)==4)?'Filial-QS':'Gro�kunden')) . '>';
		echo '<input type=Hidden name=txtMEP_BEREICH value=' . $Bereich . '>';
		awis_Debug(1,"bla: ".$Bereich);
	}	
	
	echo '<tr><td>';
	echo " <input type=image accesskey=S title='Speichern (Alt+S)' alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '</td></tr>';	
}

echo '</table>';
echo '<hr>';


/*********************************************************************
* Vollst�ndigkeitspr�fung
*********************************************************************/
if(isset($_POST['txtMEP_TAG']))
{
	$AnzProbleme = 0;
	echo '<table border=0>';
	echo '<tr><td colspan=2><h4>Noch zu kl�rende F�lle f�r folgende aktive Mitarbeiter</h4></td></tr>';

	if($_POST['txtMEP_BEREICH']!='')
	{
		$SQL = "SELECT MIT_KEY,MIT_BEZEICHNUNG,MIT_REZ_ID,REZ_BEZEICHNUNG ";
		$SQL .= " FROM Mitarbeiter ";
		$SQL .= " LEFT OUTER JOIN REGIONALZENTREN ON MIT_REZ_ID=REZ_ID";
		$SQL .= " WHERE MIT_STATUS='A' AND MIT_AKTIVITAET=1";
		$SQL .= " AND MIT_AKTIVITAETAB <= TO_DATE('" . $_POST['txtMEP_TAG'] . "', 'DD.MM.RRRR')";
		$SQL .= " AND MIT_BEREICH='" . $_POST['txtMEP_BEREICH'] . "'";
		$SQL .= "  ORDER BY MIT_BEZEICHNUNG";
	}
	else
	{
		$SQL = "SELECT MIT_KEY,MIT_BEZEICHNUNG,MIT_REZ_ID,REZ_BEZEICHNUNG ";
		$SQL .= " FROM Mitarbeiter ";
		$SQL .= " LEFT OUTER JOIN REGIONALZENTREN ON MIT_REZ_ID=REZ_ID";
		$SQL .= " WHERE MIT_STATUS='A' AND MIT_AKTIVITAET=1";
		$SQL .= " AND MIT_AKTIVITAETAB <= TO_DATE('" . $_POST['txtMEP_TAG'] . "', 'DD.MM.RRRR')";
		$SQL .= " ORDER BY MIT_BEZEICHNUNG";
	}
	
	$rsMIT = awisOpenRecordset($con, $SQL);
	$rsMITZeilen = $awisRSZeilen;

	awis_Debug(1,$SQL);
	
	if($rsMITZeilen==0)
	{
		echo '<span class=HinweisText>Es wurden keine Daten zu diesem Datum gefunden!</span>';
	}
	
	for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
	{
		if($RZUserListe!='')	// Einschr�nkungen nach RZ
		{
			if(strstr($RZUserListe,';'.$rsMIT['MIT_REZ_ID'][$MITZeile].';')==FALSE)
			{
				continue;
			}
		}

		$SQL = "SELECT * FROM MitarbeiterEinsaetze ";
		$SQL .= " WHERE MEI_MIT_KEY=" . $rsMIT['MIT_KEY'][$MITZeile];
		$SQL .= " AND MEI_TAG='" . awis_format($_POST['txtMEP_TAG'], 'OracleDatum') . "'";
		
		$SQL .= " ORDER BY MEI_VONXTZ_ID";

		$rsMEI = awisOpenRecordset($con, $SQL);
		$rsMEIZeilen = $awisRSZeilen;
		$ProblemMeldung='';
		
		if($rsMEIZeilen>0)		// Es sind Daten da
		{
			$StartID = 0;	// Kleinste Zeit (gesamt)
			$EndeID = 0;	// Gr��te Zeit (gesamt)
			$LetztesEnde = 0;
			$ProblemMeldung = '';		// Beschreibung f�r die L�cke
			for($MEIZeile=0;$MEIZeile<$rsMEIZeilen;$MEIZeile++)
			{
				if($StartID==0)			// Kleinste Zeit
				{
					$StartID = $rsMEI['MEI_VONXTZ_ID'][$MEIZeile];
				}

				if($EndeID<$rsMEI['MEI_BISXTZ_ID'][$MEIZeile])			// Gr��te Zeit
				{
					$EndeID = $rsMEI['MEI_BISXTZ_ID'][$MEIZeile];
				}

					//*************************
					// �berschneidungen testen
					//*************************
				if($LetztesEnde > $rsMEI['MEI_VONXTZ_ID'][$MEIZeile])
				{
					$ProblemMeldung .= '<br>�berschneidung. Ende: '	. $TagesZeiten[$LetztesEnde] . '. n�chster Beginn: ' . $TagesZeiten[$rsMEI['MEI_VONXTZ_ID'][$MEIZeile]] . '.';
					$AnzProbleme++;
				}			
				
					//*************************
					// L�cken testen				
					//*************************
				if($LetztesEnde!=0)
				{
					if($rsMEI['MEI_VONXTZ_ID'][$MEIZeile] != $LetztesEnde)
					{
						$ProblemMeldung .= '<br>Keine Angabe zwischen '	. $TagesZeiten[$LetztesEnde] . ' und ' . $TagesZeiten[$rsMEI['MEI_VONXTZ_ID'][$MEIZeile]] . '.';
						$AnzProbleme++;
					}
				}			
				$LetztesEnde = $rsMEI['MEI_BISXTZ_ID'][$MEIZeile];

						// Filiale ausf�llen
				if($rsMEI['MEI_MEA_KEY'][$MEIZeile]==8 AND $rsMEI['MEI_FIL_ID'][$MEIZeile]=='')		// Filiale
				{
					$ProblemMeldung .= '<br>Keine Filialnummer eingetragen (ab ' . $TagesZeiten[$rsMEI['MEI_VONXTZ_ID'][$MEIZeile]] . ')';
					$AnzProbleme++;
				}
			}
			
			$Zeit = (($EndeID - $StartID)/5) * 30;
			if(($Zeit/60)<8)
			{
				$ProblemMeldung .= '<br>Arbeitszeit betr�gt nur ' . ($Zeit/60) . ' Stunden';
				$AnzProbleme++;
			}

		}
		else		// Gar keine Daten gefunden
		{
			$ProblemMeldung .= '<br><font color=#FF0000>Keine Daten erfasst</font>';
			$AnzProbleme++;
		}

		if($ProblemMeldung!='')
		{
			echo '<tr><td class=FeldBez colspan=2>';
			if(($RechteStufe1300&4)==4)
			{
				echo '<a href=./perseins_Main.php?cmdAktion=Einsaetze&Tag=' . awis_format($_POST['txtMEP_TAG'], 'OracleDatum') . '&MIT_KEY=' . $rsMIT['MIT_KEY'][$MITZeile] . '>';
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile] . ' (' . $rsMIT['REZ_BEZEICHNUNG'][$MITZeile] . ')';
				echo '</a>';
				echo '</td></tr>';
			}
			else
			{
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile] . ' (' . $rsMIT['REZ_BEZEICHNUNG'][$MITZeile] . ')</td></tr>';
			}
			
			echo '<tr><td>';
			echo substr($ProblemMeldung,4);
			echo '</td></tr>';
			$AnzProbleme++;			
		}
	}	// Ende alle Mitarbeiter
	
	if($AnzProbleme==0)	
	{
		echo '<tr><td colspan=2>';
		echo 'Alle Daten sind vollst�ndig erfasst.';
		echo '</td></tr>';
	}
	echo '</table>';
}

/*********************************************************************
* Ende Daten anzeigen
*********************************************************************/

echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

echo '</form>';

/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
