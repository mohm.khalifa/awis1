<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
require_once("operating_COM_config.php");

global $COMWebSrv;
global $COMAppSrv;

global $comcon;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Dbmdaten','OCD_%');
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Dbmdaten');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_COM_DBM_Import_Stop');
$TextKonserven[]=array('Wort','lbl_COM_DBM_Errlog');
$TextKonserven[]=array('Wort','Mail_SB');
$TextKonserven[]=array('Wort','Mail_WORK');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Dbmdaten');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Dbmdaten'].'</title>';
?>
</head>
<body>
<?php

$Recht3113 = awisBenutzerRecht($con,3113);
if($Recht3113==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$ROW_ID='';

if (isset($_REQUEST['RID']))
{
	$ROW_ID=$_REQUEST['RID'];
	//awis_Debug(1,$ROW_ID);
}

$MNU_ID='';

if (isset($_REQUEST['DBM_MNU']))
{
	$MNU_ID=$_REQUEST['DBM_MNU'];
}

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Dbmdaten action=./operating_COM_Main.php?cmdAktion=DBM-Daten method=POST>';

echo "<p><center><h1><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Dbmdaten']."</b></h1></center><p>";

awis_FORM_Trennzeile('D');

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_Progress_Uebersicht'],'','width: 100%; margin: 5px 0px 5px 0px; text-align:center; background-color: silver; font-size: large; font-weight: bold;');
awis_FORM_ZeileEnde();

$SQL = "SELECT * FROM EXPGW_EXPORT_PROGRESS_DBM ORDER BY JOBTYPE_ID, TRANSAKTIONSID";

$rsPIU=awisOpenRecordset($comcon,$SQL);
$rsPIUZeilen = $awisRSZeilen;

if($rsPIUZeilen>0)
{
	$styleAll='font-size: 90%;';
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBNAME'],120,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],130,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS'],50,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS_BEZEICHNUNG'],150,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ERWARTET'],80,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FREIGABE_PROZ'],100,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITET'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITUNG_PROZ'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FEHLER'],80,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FEHLER_PROZ'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_SYSTEM'],90,$styleAll);
	awis_FORM_ZeileEnde();	
	
	for($PIUZeile=0;$PIUZeile<$rsPIUZeilen;$PIUZeile++)
	{
		
		if ($rsPIU['JOBSTATUS'][$PIUZeile]=='E'){
			$styleDetail='background-color: FF0000; font-weight: bold;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='L'){
			$styleDetail='background-color: FFFF00;';	
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='T'){
			$styleDetail='background-color: 00FF00;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='M'){
			$styleDetail='background-color: FF6921;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='X'){
			$styleDetail='background-color: D1EFEE;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='V'){
			$styleDetail='background-color: FF3F47;';
		}
		else{
			$styleDetail='';
		}		
		
		$style=$styleAll.$styleDetail;
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['JOBTYPE_ID'][$PIUZeile],0,50,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['DESCRIPTION'][$PIUZeile],0,120,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['TRANSAKTIONSID'][$PIUZeile],0,130,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['JOBSTATUS'][$PIUZeile],0,50,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['STATUS_BEZEICHNUNG'][$PIUZeile],0,150,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['CREATED'][$PIUZeile],0,80,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['FREIGABE_PROZ'][$PIUZeile].' ('.$rsPIU['FREIGABE_CNT_FIL'][$PIUZeile].')',0,100,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['LOADED'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['LOAD_PC'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['ERROR'][$PIUZeile],0,80,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['ERROR_PC'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['DBM_SYSTEM'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','T');
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();	
}

awis_FORM_FormularEnde();

awis_FORM_Trennzeile('D');


$MNU_LINK="<p><li><a href=./operating_COM_Main.php?cmdAktion=DBM-Daten&DBM_MNU=1>Ausstehende A4 / DBM-Dateiversionen</a></li>";

echo $MNU_LINK;

if ($MNU_ID=='1')
{
$SQL="SELECT DISTINCT ROWIDTOCHAR(ROWID) AS RID, DIV_FIL_ID AS FILIALE, ";
$SQL.="LPAD(DIV_VERSION_FEHLT,5,0) AS IMPORT_VERSION_FEHLT, MIN(DIV_USERDAT) AS IMPORT_AM, ";
$SQL.="CASE WHEN DIV_IMD_NAME LIKE 'DW-%' THEN (CASE WHEN DIV_FIL_ID=598 THEN 'Onlineshop (S4T)' WHEN DIV_FIL_ID=597 THEN 'FirstGlass' ELSE 'FILSYS' END) ELSE 'AXAPTA' END AS DBM_SYSTEM ";
$SQL.="FROM EXPERIAN_ATU.DBM_IMPORT_VERSION WHERE DIV_STATUS<>0 ";
$SQL.="GROUP BY ROWIDTOCHAR(ROWID), DIV_FIL_ID, DIV_VERSION_FEHLT, DIV_IMD_NAME ORDER BY 1, 2";

$rsDNV=awisOpenRecordset($comcon,$SQL);
$rsDNVZeilen=$awisRSZeilen;

if ($rsDNVZeilen>0){

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_SYSTEM'],150);
awis_FORM_ZeileEnde();

	for($DNVZeile=0;$DNVZeile<$rsDNVZeilen;$DNVZeile++)
	{
		$Link="./operating_COM_Main.php?cmdAktion=DBM-Daten&&DBM_MNU=1&RID=".urlencode($rsDNV['RID'][$DNVZeile]);
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDNV['FILIALE'][$DNVZeile],0,100,false,($DNVZeile%2),'',$Link,'Z');
		awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION_FEHLT',$rsDNV['IMPORT_VERSION_FEHLT'][$DNVZeile],0,200,false,($DNVZeile%2),'',$Link,'T');	
		awis_FORM_Erstelle_ListenFeld('IMPORT_AM',$rsDNV['IMPORT_AM'][$DNVZeile],0,200,false,($DNVZeile%2),'',$Link,'DU');
		awis_FORM_Erstelle_ListenFeld('DBM_SYSTEM',$rsDNV['DBM_SYSTEM'][$DNVZeile],0,150,false,($DNVZeile%2),'','','T');
		awis_FORM_ZeileEnde();
		
		if ($ROW_ID==$rsDNV['RID'][$DNVZeile])
		{	
			awis_FORM_Trennzeile();
				
			
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],50,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Datei'],250,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import'],100,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt'],100,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Transaktionsid'],150,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Versuch'],100,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Durch'],100,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],150,'font-size: 75%;');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_SYSTEM'],150,'font-size: 75%;');
			awis_FORM_ZeileEnde();

			$BindeVariablen=array();
			$BindeVariablen['var_T_rowid']=$ROW_ID;
			
			$SQL="SELECT DIV_FIL_ID AS FILIALE, DIV_IMD_NAME AS IMPORTDATEI, LPAD(DIV_VERSION,5,0) AS IMPORT_VERSION, ";
			$SQL.="LPAD(DIV_VERSION_FEHLT,5,0) AS IMPORT_VERSION_FEHLT, DIV_TRANSID_IMPORT AS TRANSID_IMPORT, DIV_IMPORT_VERSUCH AS IMPORT_VERSUCH, ";
			$SQL.="DIV_USER AS IMPORT_USER, DIV_USERDAT AS IMPORT_AM, ";
			$SQL.="CASE WHEN DIV_IMD_NAME LIKE 'DW-%' THEN (CASE WHEN DIV_FIL_ID=598 THEN 'Onlineshop (S4T)' WHEN DIV_FIL_ID=597 THEN 'FirstGlass' ELSE 'FILSYS' END) ELSE 'AXAPTA' END AS DBM_SYSTEM ";
			$SQL.="FROM EXPERIAN_ATU.DBM_IMPORT_VERSION WHERE ROWID=CHARTOROWID(:var_T_rowid)";
			
			$rsDNVDetail=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
			$rsDNVDetailZeilen=$awisRSZeilen;
		
			for($DNVDetailZeile=0;$DNVDetailZeile<$rsDNVDetailZeilen;$DNVDetailZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDNVDetail['FILIALE'][$DNVDetailZeile],0,50,false,($DNVDetailZeile%2),'font-size: 75%;','','Z','L');
				awis_FORM_Erstelle_ListenFeld('IMPORTDATEI',$rsDNVDetail['IMPORTDATEI'][$DNVDetailZeile],0,250,false,($DNVDetailZeile%2),'font-size: 75%;','','T','L');	
				awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION',$rsDNVDetail['IMPORT_VERSION'][$DNVDetailZeile],0,100,false,($DNVDetailZeile%2),'font-size: 75%;','','T','L');	
				awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION_FEHLT',$rsDNVDetail['IMPORT_VERSION_FEHLT'][$DNVDetailZeile],0,100,false,($DNVDetailZeile%2),'font-size: 75%;','','','T','L');
				awis_FORM_Erstelle_ListenFeld('TRANSID_IMPORT',$rsDNVDetail['TRANSID_IMPORT'][$DNVDetailZeile],0,150,false,($DNVDetailZeile%2),'font-size: 75%;','','','T','L');		
				awis_FORM_Erstelle_ListenFeld('IMPORT_VERSUCH',$rsDNVDetail['IMPORT_VERSUCH'][$DNVDetailZeile],0,100,false,($DNVDetailZeile%2),'font-size: 75%;','','Z','L');	
				awis_FORM_Erstelle_ListenFeld('IMPORT_USER',$rsDNVDetail['IMPORT_USER'][$DNVDetailZeile],0,100,false,($DNVDetailZeile%2),'font-size: 75%;','','','T','L');		
				awis_FORM_Erstelle_ListenFeld('IMPORT_AM',$rsDNVDetail['IMPORT_AM'][$DNVDetailZeile],0,150,false,($DNVDetailZeile%2),'font-size: 75%;','','','DU','L');		
				awis_FORM_Erstelle_ListenFeld('DBM_SYSTEM',$rsDNVDetail['DBM_SYSTEM'][$DNVDetailZeile],0,150,false,($DNVDetailZeile%2),'font-size: 75%;','','T');
				awis_FORM_ZeileEnde();
			}
		//	awis_FORM_Trennzeile();			
		}
		
	}

awis_FORM_FormularEnde();
}
else{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_ZeileEnde();
	}
}


$MNU_LINK="<p><li><a href=./operating_COM_Main.php?cmdAktion=DBM-Daten&DBM_MNU=2>Fehlende A4 / DBM-Datei-Importe</a></li>";

echo $MNU_LINK;

if($MNU_ID=='2' || isset($_GET['DFD_EXEC']))
{
	if (isset($_GET['DFD_EXEC']) && $_GET['DFD_EXEC']!='')
	{
		$rsMail="";
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_exec_hash']=$_GET['DFD_EXEC'];
		
		$SQL="SELECT FILIALE, TO_CHAR(STARTSTAMP,'DD.MM.YY HH24:MI:SS') as STARTSTAMP, TO_CHAR(STOPSTAMP,'DD.MM.YY HH24:MI:SS') AS STOPSTAMP, DIFF_TAGE, DBM_SYSTEM, DBM_LAST_VERSION FROM DBM_KOPF_FEHLENDE WHERE EXEC_HASH=:var_T_exec_hash";
		
		$rsMail=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsMailZeilen = $awisRSZeilen;
		
		//awis_Debug(1,$SQL);
		
		$MAIL_SUBJECT="WARNING Fehlende DBM-Verarbeitung (".$rsMail['DBM_SYSTEM'][0].")";
		$MAIL_TEXT="Hallo,\r\n";
		$MAIL_TEXT.="folgende DBM-Verarbeitung (System: ".$rsMail['DBM_SYSTEM'][0].") wurde noch nicht geliefert:\r\n";
		$MAIL_TEXT.="Filiale: ".$rsMail['FILIALE'][0]."\r\n"; 
		$MAIL_TEXT.="Kein Import seit: ".$rsMail['STARTSTAMP'][0]."\r\n";  
		//$MAIL_TEXT.="Start: ".$rsMail['STARTSTAMP'][0]."\r\n";  
		//$MAIL_TEXT.="Ende: ".$rsMail['STOPSTAMP'][0]."\r\n";
		$MAIL_TEXT.="Letzte importierte DBM-Version: ".$rsMail['DBM_LAST_VERSION'][0]."\r\n";
		$MAIL_TEXT.="Gruss\r\n";
		$MAIL_TEXT.="COM-Operating";
		
		if($rsMail['DBM_SYSTEM'][0]=='FILSYS')
		{
			$MAIL_VT="filsys_ekat@de.atu.eu";
			$MAIL_VT_CC="atu-edv-k11-pc-db@de.atu.eu";
		}
		elseif ($rsMail['DBM_SYSTEM'][0]=='AXAPTA')
		{
			$MAIL_VT="desktop-sst-operating@de.atu.eu";
			$MAIL_VT_CC="atu-edv-k11-pc-db@de.atu.eu";
		}
		else
		{
            $MAIL_VT="atu-edv-k11-pc-db@de.atu.eu";
            $MAIL_VT_CC="";
		}
		
		$SQL="BEGIN COM_MAIL.PROC_SEND_MAIL('".$MAIL_TEXT."','".$MAIL_VT."','".$MAIL_SUBJECT."','".$MAIL_VT_CC."'); END;";
		awisExecute($comcon,$SQL);
		
		$SQL="INSERT INTO DBM_EMAIL_FEHLEND (EXEC_HASH) VALUES ('".$_REQUEST['DFD_EXEC']."')";
		awisExecute($comcon,$SQL);
	}
	
	$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
	awisExecute($comcon,$SQL);
	
	$SQL="SELECT AA.EXEC_HASH, AA.FILIALE, AA.STARTSTAMP, AA.STOPSTAMP, AA.DIFF_TAGE, NVL(BB.EXEC_HASH,0) AS EMAIL, DBM_SYSTEM, DBM_LAST_VERSION FROM DBM_KOPF_FEHLENDE AA, DBM_EMAIL_FEHLEND BB WHERE AA.EXEC_HASH=BB.EXEC_HASH(+)";
	
	$rsDFD=awisOpenRecordset($comcon,$SQL);
	$rsDFDZeilen = $awisRSZeilen;
	
	if($rsDFDZeilen>0)
	{
		awis_FORM_FormularStart();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Startzeit'],200);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Endezeit'],200);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Diff_Tage'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_SYSTEM'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_Last_Version'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift('',34);
		awis_FORM_ZeileEnde();
		
		$SQL="WITH ERRLOG AS (SELECT * FROM (SELECT ERR_KEY, NVL(DELIMITED.GET_NUMBER_WORD (LOG, 5, '-'), ";
		$SQL.="DELIMITED.GET_NUMBER_WORD(SUBSTR(ERR_MSG,REGEXP_INSTR(ERR_MSG,'[D][W].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'c'),LENGTH (ERR_MSG)),5,'-')) AS FILNR, ";
		$SQL.="ERR_DATUM FROM ERR_LOG WHERE NOT EXISTS (SELECT * FROM ERR_LOG_STATUS ";
		$SQL.="WHERE ERR_STAT_BEZEICHNUNG = 'Geschlossen' AND ERR_STATUS = ERR_STAT_KEY) ";
		$SQL.="AND (   LOG LIKE '%DW%.CSV.log' OR LOG LIKE '%DW%.csv.log' OR REGEXP_INSTR ";
		$SQL.="(ERR_MSG, '[D][W].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'c') > 0)) ";
		$SQL.="WHERE FILNR IS NOT NULL) ";
		$SQL.="SELECT ERR_KEY, EXEC_HASH ";
		$SQL.="FROM DBM_KOPF_FEHLENDE AA, ERRLOG BB ";
		$SQL.="WHERE AA.FILIALE = BB.FILNR ";
		$SQL.="AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP ";
		$SQL.="ORDER BY ERR_KEY DESC";

		$rsDFDErr=awisOpenRecordset($comcon,$SQL);
		$rsDFDErrZeilen = $awisRSZeilen;
	
		for($DFDZeile=0;$DFDZeile<$rsDFDZeilen;$DFDZeile++)
		{
			awis_FORM_ZeileStart();
			$FIL_LINK='"https://'.$_SERVER["SERVER_NAME"].'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsDFD['FILIALE'][$DFDZeile].'" target="_blank"';
			awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDFD['FILIALE'][$DFDZeile],0,100,false,($DFDZeile%2),'',$FIL_LINK,'Z');
			awis_FORM_Erstelle_ListenFeld('STARTSTAMP',$rsDFD['STARTSTAMP'][$DFDZeile],0,200,false,($DFDZeile%2),'','','T');
			awis_FORM_Erstelle_ListenFeld('STOPSTAMP',$rsDFD['STOPSTAMP'][$DFDZeile],0,200,false,($DFDZeile%2),'','','T');
			awis_FORM_Erstelle_ListenFeld('DIFF_TAGE',$rsDFD['DIFF_TAGE'][$DFDZeile],0,150,false,($DFDZeile%2),'color: red;','','T');
			awis_FORM_Erstelle_ListenFeld('DBM_SYSTEM',$rsDFD['DBM_SYSTEM'][$DFDZeile],0,150,false,($DFDZeile%2),'','','T');
			awis_FORM_Erstelle_ListenFeld('DBM_LAST_VERSION',$rsDFD['DBM_LAST_VERSION'][$DFDZeile],0,150,false,($DFDZeile%2),'','','T');
			
			$BindeVariablen=array();
			$BindeVariablen['var_N0_fil_id']='0'.$rsDFD['FILIALE'][$DFDZeile];
			
			$SQL="SELECT DISTINCT DIS_FIL_ID FROM EXPERIAN_ATU.DBM_IMPORT_STOP WHERE DIS_FIL_ID=:var_N0_fil_id";
			
			$rsDIS=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
			$rsDISZeilen=$awisRSZeilen;

			if ($rsDISZeilen>0)
			{
				echo "<img border=0 src=/bilder/icon_exclamation.png title='" . $AWISSprachKonserven['Wort']['lbl_COM_DBM_Import_Stop'] . "'>";
			}
			
			$BindeVariablen=array();
			$BindeVariablen['var_N0_fil_id']='0'.$rsDFD['FILIALE'][$DFDZeile];
/*
			$SQL="SELECT ERR_KEY, EXEC_HASH FROM DBM_KOPF_FEHLENDE AA, ";
			$SQL.="(SELECT ERR_KEY, NVL(DELIMITED.GET_NUMBER_WORD(LOG,5,'-'),DELIMITED.GET_NUMBER_WORD(SUBSTR(ERR_MSG, ";
			$SQL.="REGEXP_INSTR(ERR_MSG,'[d][w].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'i'),LENGTH(ERR_MSG)),5,'-')) AS FILNR, ERR_DATUM FROM ERR_LOG ";
			$SQL.="WHERE ERR_STATUS<>(SELECT ERR_STAT_KEY FROM ERR_LOG_STATUS WHERE ERR_STAT_BEZEICHNUNG='Geschlossen') ";
			$SQL.="AND (LOG LIKE '%DW%.CSV.log' OR LOG LIKE '%DW%.csv.log' OR REGEXP_INSTR(ERR_MSG,'[d][w].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'i')>0)) BB ";
			$SQL.="WHERE AA.FILIALE=FILNR AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP ";
			$SQL.="AND AA.FILIALE=:var_N0_fil_id ";
			$SQL.="ORDER BY ERR_KEY DESC";
*/			
/*
			$SQL="WITH ERRLOG AS (SELECT * FROM (SELECT ERR_KEY, NVL(DELIMITED.GET_NUMBER_WORD (LOG, 5, '-'), ";
			$SQL.="DELIMITED.GET_NUMBER_WORD(SUBSTR(ERR_MSG,REGEXP_INSTR(ERR_MSG,'[D][W].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'c'),LENGTH (ERR_MSG)),5,'-')) AS FILNR, ";
			$SQL.="ERR_DATUM FROM ERR_LOG WHERE NOT EXISTS (SELECT * FROM ERR_LOG_STATUS ";
			$SQL.="WHERE ERR_STAT_BEZEICHNUNG = 'Geschlossen' AND ERR_STATUS = ERR_STAT_KEY) ";
			$SQL.="AND (   LOG LIKE '%DW%.CSV.log' OR LOG LIKE '%DW%.csv.log' OR REGEXP_INSTR ";
			$SQL.="(ERR_MSG, '[D][W].+-[0-9]{5}-[0-9]{8}-[0-9]{2}-[0-9]{5}-[0-9]{3}.[c][s][v]',1,1,0,'c') > 0)) ";
			$SQL.="WHERE FILNR IS NOT NULL) ";
			$SQL.="SELECT ERR_KEY, EXEC_HASH ";
			$SQL.="FROM DBM_KOPF_FEHLENDE AA, ERRLOG BB ";
			$SQL.="WHERE AA.FILIALE = BB.FILNR ";
			$SQL.="AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP ";
			$SQL.="AND AA.FILIALE = :var_N0_fil_id ";
			$SQL.="ORDER BY ERR_KEY DESC";
			
			$rsDFDErr=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
			$rsDFDErrZeilen = $awisRSZeilen;
*/
			//if($rsDFDErrZeilen>0 && $rsDFDErr['EXEC_HASH'][0]==$rsDFD['EXEC_HASH'][$DFDZeile])
			if($rsDFDErrZeilen>0 && in_array($rsDFD['EXEC_HASH'][$DFDZeile],$rsDFDErr['EXEC_HASH']))
			{
				echo '<a href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=' . $rsDFDErr['ERR_KEY'][0] . ' target="_blank"><img border=0 src=/bilder/icon_warning.png title="' . $AWISSprachKonserven['Wort']['lbl_COM_DBM_Errlog'] . '"></a>';
			}
			
			if(($Recht3113&2)==2)
			{
				if ($rsDFD['EMAIL'][$DFDZeile]!='0'){
					echo "&nbsp;<img src=/bilder/icon_lkw.png title='".$AWISSprachKonserven['Wort']['Mail_WORK']."'>";
				}
				else{
					echo "&nbsp;<a href=./operating_COM_Main.php?cmdAktion=DBM-Daten&DFD_EXEC=" . $rsDFD['EXEC_HASH'][$DFDZeile] . "><img border=0 src=/bilder/icon_mail.png name=cmdAnzeige_Fehlende title='" . $AWISSprachKonserven['Wort']['Mail_SB'] . "'></a>";
				}
				echo "<input type=hidden name='txtEMAIL_ID' value='".$rsDFD['EXEC_HASH'][$DFDZeile]."'>";
				echo "<input type=hidden name='txtAnzeige_Fehlende' value='TRUE'>";
			}
			else
			{
				if ($rsDFD['EMAIL'][$DFDZeile]!=0){
					echo "&nbsp;<img src=/bilder/baustelle.png title='".$AWISSprachKonserven['Wort']['Mail_SB']."'>";
				}
			}
			awis_FORM_ZeileEnde();
		}
	}
	else{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
}

$MNU_LINK="<p><li><a href=./operating_COM_Main.php?cmdAktion=DBM-Daten&DBM_MNU=3>Angehaltene A4 / DBM-Datei-Importe</a></li>";

echo $MNU_LINK;

if ($MNU_ID=='3')
{
$SQL="SELECT DIS_FIL_ID, LPAD(DIS_VERSION_MISSING,5,0) AS DIS_VERSION_MISSING, DIS_USERDAT ";
$SQL.="FROM EXPERIAN_ATU.DBM_IMPORT_STOP ORDER BY 1, 2";

$rsDIS=awisOpenRecordset($comcon,$SQL);
$rsDISZeilen=$awisRSZeilen;

	if ($rsDISZeilen>0)
	{
		awis_FORM_FormularStart();
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt'],200);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],200);
		awis_FORM_ZeileEnde();
	
		for($DISZeile=0;$DISZeile<$rsDISZeilen;$DISZeile++)
		{
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDIS['DIS_FIL_ID'][$DISZeile],0,100,false,($DISZeile%2),'','','Z');
			awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION_FEHLT',$rsDIS['DIS_VERSION_MISSING'][$DISZeile],0,200,false,($DISZeile%2),'','','T');	
			awis_FORM_Erstelle_ListenFeld('IMPORT_AM',$rsDIS['DIS_USERDAT'][$DISZeile],0,200,false,($DISZeile%2),'','','DU');	
			awis_FORM_ZeileEnde();
		}
	
		awis_FORM_FormularEnde();
	}
	else{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_ZeileEnde();
	}
}

$MNU_LINK="<p><li><a href=./operating_COM_Main.php?cmdAktion=DBM-Daten&DBM_MNU=4>A4 / DBM-Datei-Importe ohne Freigabe</a></li>";

echo $MNU_LINK;

if ($MNU_ID=='4')
{
$SQL="SELECT FIL_ID, IMPORT_AM, TRANSAKTIONSID, DBM_SYSTEM ";
$SQL.="FROM DBM_IMPORT_OHNE_FREIGABE order by 1,3";

$rsDIF=awisOpenRecordset($comcon,$SQL);
$rsDIFZeilen=$awisRSZeilen;

	if ($rsDIFZeilen>0)
	{
		awis_FORM_FormularStart();
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],200);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Transaktionsid'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_DBM_SYSTEM'],150);
		awis_FORM_ZeileEnde();
	
		for($DIFZeile=0;$DIFZeile<$rsDIFZeilen;$DIFZeile++)
		{
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDIF['FIL_ID'][$DIFZeile],0,100,false,($DIFZeile%2),'','','Z');
			awis_FORM_Erstelle_ListenFeld('IMPORT_AM',$rsDIF['IMPORT_AM'][$DIFZeile],0,200,false,($DIFZeile%2),'','','DU');	
			awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDIF['TRANSAKTIONSID'][$DIFZeile],0,150,false,($DIFZeile%2),'','','Z');
			awis_FORM_Erstelle_ListenFeld('DBM_SYSTEM',$rsDIF['DBM_SYSTEM'][$DIFZeile],0,150,false,($DIFZeile%2),'','','T');
			awis_FORM_ZeileEnde();
		}
	
		awis_FORM_FormularEnde();
	}
	else{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_ZeileEnde();
	}
}

awis_FORM_Trennzeile('D');

awis_FORM_FormularStart();
	awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('Offene A4/DBM-Dateien auf COM-APP-Server ('.$COMAppSrv.')','','width: 100%; background-color: transparent;','"'.$COMWebSrv.'/schnelldiagnose/showfile_open_impdir.php?TYP=DBM" target="_blank"');
	awis_FORM_ZeileEnde();
awis_FORM_FormularEnde();

if(isset($_POST['cmdLoeschen_x']))
{
	include('./operating_COM_Errlog_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./operating_COM_Errlog_speichern.php');
}

if(isset($_POST['cmdLuecken_x']))
{	
	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Versuch'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt_Seit'],200);
	awis_FORM_ZeileEnde();
	
	$SQL="SELECT * FROM EXPERIAN_ATU.V_DBM_VERSION_FEHLEND ORDER BY 1, 2";
	
	$rsDVF=awisOpenRecordset($comcon,$SQL);
	$rsDVFZeilen = $awisRSZeilen;
	
	for($DVFZeile=0;$DVFZeile<$rsDVFZeilen;$DVFZeile++)
	{
		awis_FORM_ZeileStart();
		$Link='';
		if(($Recht3113&4)==4)
		{
			$Link = "./operating_COM_Main.php?cmdAktion=DBM-Daten&EXEC=".$rsDVF['PKL_ROWID'][$DVFZeile]."";
		}
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDVF['FILIALE'][$DVFZeile],0,100,false,($DVFZeile%2),'height: 27px; ',$Link,'Z');
		awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION',$rsDVF['IMPORT_VERSION'][$DVFZeile],0,200,false,($DVFZeile%2),'height: 27px; ',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION_FEHLT',$rsDVF['IMPORT_VERSION_FEHLT'][$DVFZeile],0,200,false,($DVFZeile%2),'height: 27px; ',$Link,'T');
	
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}


echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

