<?php
/**
 * "�bergangsmaske" f�r die Mitarbeiterzuordnungen
 *
 */
require_once('awisFilialen.inc');

global $AWISCursorPosition;
global $SRFunktionen;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KON','%');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');
	$TextKonserven[]=array('Filialinfo','fit_lbl_852');
	$TextKonserven[]=array('Filialinfo','fit_lbl_853');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht11009=$AWISBenutzer->HatDasRecht(11009);		// Rechte des Mitarbeiters
	if($Recht11009==0)
	{
		$Form->Fehler_KeineRechte();
	}
	if(isset($_POST['cmdSpeichern_x'])){
     	include './srpep_speichern.php';
	}


    $SQL  ='select ';
    $SQL .='     FIL_ID,';
    $SQL .='     FILIALEBENENROLLENZUORDNUNGEN.frz_kon_key,';
    $SQL .='     KONTAKTE.KON_PER_NR,';
    $SQL .='     KON_NAME1 || \' \' ||  KON_NAME2 as KON_NAME ';
    $SQL .=' from';
    $SQL .='     V_FILIALEN_AKTUELL';
    $SQL .='     left join FILIALEBENENROLLENZUORDNUNGEN on FIL_ID = FRZ_XXX_KEY';
    $SQL .='                                                 and FRZ_XTN_KUERZEL = \'FIL\'';
    $SQL .='                                                 and FRZ_FER_KEY = 39';
    $SQL .=' and FRZ_GUELTIGAB <= sysdate and FRZ_GUELTIGBIS >= sysdate ';
    $SQL .='     left join KONTAKTE on FRZ_KON_KEY = KON_KEY and KON_STATUS = \'A\'';
    $SQL .=' order by FIL_ID ';
    $Form->DebugAusgabe(1,$SQL);

    $rsFRZ = $DB->RecordSetOeffnen($SQL);


	$Form->SchreibeHTMLCode("<form name=frmSuche method=POST action=./srpep_Main.php?cmdAktion=Mitarbeiter>");

	$Form->Formular_Start();

	$Breiten['FIL_ID'] = 100;
    $Breiten['KON_PER_NR'] = 150;
    $Breiten['KON_NAME'] = 250;

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ID'],$Breiten['FIL_ID']);
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_PER_NR'],$Breiten['KON_PER_NR']);
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],$Breiten['KON_NAME']);
    $Form->ZeileEnde();

    while (!$rsFRZ->EOF()){
        $HG = $rsFRZ->DSNummer()%2;

        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('FIL_ID',$rsFRZ->FeldInhalt('FIL_ID'),$Breiten['FIL_ID']/10, $Breiten['FIL_ID'],false,$HG);
        $Form->Erstelle_ListenFeld('#KON_PER_NR__'.$rsFRZ->FeldInhalt('FIL_ID'),$rsFRZ->FeldInhalt('KON_PER_NR'),$Breiten['KON_PER_NR']/10, $Breiten['KON_PER_NR'],$Recht11009&2,$HG,'','','T','L','','height: 10px');
        $Form->Erstelle_ListenFeld('KON_NAME',$rsFRZ->FeldInhalt('KON_NAME'),$Breiten['KON_NAME']/10, $Breiten['KON_NAME'],false,$HG);
        $Form->ZeileEnde();

        $rsFRZ->DSWeiter();
    }

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht11009&2)==2))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}


	$Form->SchaltflaechenEnde();

	echo '</form>';
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200912171119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200912171120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}