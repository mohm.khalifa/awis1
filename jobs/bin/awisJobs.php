<?php

try {
    require_once('awisJobs.inc');

    $X = new awisJobs('awis_jobs');

    $X->DebugLevel(99);
    $X->StarteJobs();

    // Fehlerdatei leeren
    file_put_contents('/var/log/awis/awisJobsErr.log','');
}
catch(awisException $ex)
{
    echo $ex->getSQL().PHP_EOL;
    echo $ex->getMessage().PHP_EOL;


    // Eine Fehlerdatei schreiben, wenn noch keine da ist
    if(filesize('/var/log/awis/awisJobsErr.log')<10)
    {
        $fp=fopen('/var/log/awis/awisJobsErr.log','w+');

        fputs($fp,'Fehlerzeitpunkt: '.date('%c').PHP_EOL);
        fputs($fp,$ex->getSQL().PHP_EOL);
        fputs($fp,$ex->getMessage().PHP_EOL);

        fclose($fp);

        system("echo 'Bitte pruefen' | mail -s AWIS-Jobs -a /var/log/awis/awisJobsErr.log shuttle@de.atu.eu");
    }
}
?>
