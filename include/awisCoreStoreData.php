<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisRest.inc';
require_once 'awisBenutzer.inc';
require_once 'awisCoreStoreModel.php';
require_once 'awisSpecialOpeningModel.php';

class awisCoreStoreData {

    const CSD_URI_FILUPDATE = '/core-store-data';
    const CSD_URI_FEIERTAGE = '/special-opening-hours';
    const TELFON_NATIONAL = 'N';
    const TELFON_INTERNATIONAL = 'I';

    /**
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisFormular
     */
    protected $_Form;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var awisRest
     */
    protected $_AWISRest;

    /**
     * @var string URL
     */
    protected $CSDUrl;

    /**
     * @var int DebugLevel
     */
    protected $_DebugLevel;

    /**
     * @var string DebugAusgabe Text
     */
    protected $_DebugAusgabe;

    /**
     * awisCoreStoreData constructor.
     * @param string $Benutzer
     * @param int $DebugLevel
     * @throws awisException
     */
    function __construct($Benutzer = 'awis_jobs', $DebugLevel = 999)
    {
        $this->_DebugLevel = $DebugLevel;
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_Form = new awisFormular();
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_AWISRest = new awisRest('awisCoreStoreData');
        $this->_AWISRest->setzeDebugLevel($DebugLevel);
        $this->CSDUrl = $this->_Benutzer->ParameterLesen('CSD_URL');
    }

    /**
     * @throws Exception Hauptfunktion fuer normalen CoreStoreDataJob
     */
    public function CSDFindeDelta() {
        $SQL  ='select ';
        $SQL .=' VFAD.FIL_ID,';
        $SQL .=' VFAD.FIL_BEZ,';
        $SQL .=' VFAD.FIL_STRASSE,';
        $SQL .=' VFAD.FIL_PLZ,';
        $SQL .=' VFAD.FIL_ORT,';
        $SQL .=' VFAK.LAN_CODE,';
        $SQL .=' VFAD.KUNDENTELEFON,';
        $SQL .=' FOZ.FOZ_MOFRVON,';
        $SQL .=' FOZ.FOZ_MOFRBIS,';
        $SQL .=' FOZ.FOZ_SAVON,';
        $SQL .=' FOZ.FOZ_SABIS,';
        $SQL .=' FITUEV1.FIF_WERT as FUT_MONTAG,';
        $SQL .=' FITUEV2.FIF_WERT as FUT_DIENSTAG,';
        $SQL .=' FITUEV3.FIF_WERT as FUT_MITTWOCH,';
        $SQL .=' FITUEV4.FIF_WERT as FUT_DONNERSTAG,';
        $SQL .=' FITUEV5.FIF_WERT as FUT_FREITAG,';
        $SQL .=' FITUEV6.FIF_WERT as FUT_SAMSTAG,';
        $SQL .=' FI1.FIF_WERT as FIF_GEOBREITE,';
        $SQL .=' FI2.FIF_WERT as FIF_GEOHOEHE,';
        $SQL .=' FI3.FIF_WERT as FIF_GOOGLEID,';
        $SQL .=' FI4.FIF_WERT as FIF_ONLINEINFO,';
        $SQL .=' FI5.FIF_WERT as FIF_ONLINEFINDBAR,';
        $SQL .=' FI6.FIF_WERT as FIF_TERMINVEREINBARUNGMOEGLICH,';
        $SQL .=' FI7.FIF_WERT as FIF_LETZTERHASH,';
        $SQL .=' FI8.FIF_WERT as FIF_GOOGLENAME,';
        $SQL .=' FI9.FIF_WERT as FIF_TELEFON,';
        $SQL .=' FI10.FIF_WERT as FIF_ABOUTUSTEXT,';
        $SQL .=' FI11.FIF_WERT as FIF_EMAIL';
        $SQL .=' from V_FILIALEN_ADRESSEN VFAD';
        $SQL .=' right join V_FILIALEN_AKTUELL VFAK';
        $SQL .=' on VFAD.FIL_ID = VFAK.FIL_ID';
        $SQL .=' left join (';
        $SQL .=' select distinct FIF_FIL_ID, FOZ_MOFRVON, FOZ_MOFRBIS, FOZ_SAVON, FOZ_SABIS from FILIALINFOS';
        $SQL .=' left join FILIALENOEFFNUNGSZEITENMODELLE';
        $SQL .=' on FIF_WERT = FOZ_KEY';
        $SQL .=' where FIF_FIT_ID = 125 ';
        $SQL .=' and FOZ_MOFRVON is not null ';
        $SQL .=' and FOZ_MOFRBIS is not null ';
        $SQL .=' and FOZ_SAVON is not null ';
        $SQL .=' and FOZ_SABIS is not null ';
        $SQL .=' ) FOZ on VFAD.FIL_ID = FOZ.FIF_FIL_ID ';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 41) FITUEV1 on VFAD.FIL_ID = FITUEV1.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 42) FITUEV2 on VFAD.FIL_ID = FITUEV2.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 43) FITUEV3 on VFAD.FIL_ID = FITUEV3.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 44) FITUEV4 on VFAD.FIL_ID = FITUEV4.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 45) FITUEV5 on VFAD.FIL_ID = FITUEV5.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 46) FITUEV6 on VFAD.FIL_ID = FITUEV6.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 130) FI1 on VFAD.FIL_ID = FI1.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 131) FI2 on VFAD.FIL_ID = FI2.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 925) FI3 on VFAD.FIL_ID = FI3.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 926) FI4 on VFAD.FIL_ID = FI4.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 927) FI5 on VFAD.FIL_ID = FI5.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 928) FI6 on VFAD.FIL_ID = FI6.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 929) FI7 on VFAD.FIL_ID = FI7.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 930) FI8 on VFAD.FIL_ID = FI8.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 1 and FIF_IMQ_ID = 2 group by FIF_FIL_ID, FIF_WERT) FI9 on VFAD.FIL_ID = FI9.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 932) FI10 on VFAD.FIL_ID = FI10.FIF_FIL_ID';
        $SQL .=' left join (select FIF_WERT, FIF_FIL_ID from FILIALINFOS where FIF_FIT_ID = 933) FI11 on VFAD.FIL_ID = FI11.FIF_FIL_ID';
        $SQL .=' ORDER BY VFAD.FIL_ID';

        $rsFIF = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsFIF->EOF()){

            $Hash = $this->GeneriereHash($rsFIF);

            if($Hash != $rsFIF->FeldInhalt('FIF_LETZTERHASH')){ //Pruefe ob unterschiedliche Daten

                $CSDModel = $this->Recordset2CorestoreModel($rsFIF);

                $Antwort = $this->CSDRequest($CSDModel); //Schicke Request

                if($Antwort==200){
                    $this->UpdateHash($rsFIF->FeldInhalt('FIL_ID'),$Hash); //Wenn OK dann neuen Hash hinterlegen
                } else {
                    $this->_Log("Habe Fehler beim Request. Error Code:".$Antwort,10);
                    throw new Exception("Habe Fehler beim Request. Error Code:".$Antwort);
                }

            }
            $rsFIF->DSWeiter();
        }
    }

    /**
     * Erstellung des CoreStoreModels mit dem Recordset
     *
     * @param awisRecordset $rs
     * @return awisCoreStoreModel
     */
    protected function Recordset2CorestoreModel(awisRecordset $rs){
        $CoreStoreModel = new awisCoreStoreModel();
        $CoreStoreModel->id = $rs->FeldInhalt('FIL_ID');
        $CoreStoreModel->name = $rs->FeldInhalt('FIF_GOOGLENAME');
        $CoreStoreModel->shortName = preg_replace("/ATU /",'',preg_replace("/A\\.T\\.U /",'',$rs->FeldInhalt('FIF_GOOGLENAME')));
        $CoreStoreModel->street = $rs->FeldInhalt('FIL_STRASSE');
        $CoreStoreModel->zipCode = preg_replace('/^[A-Za-z]/','',$rs->FeldInhalt('FIL_PLZ'));
        $CoreStoreModel->city =  $rs->FeldInhalt('FIL_ORT');

        $Country = new Country();
        $Country->isoCode = $rs->FeldInhalt('LAN_CODE');
        $Country->isoCurrencyCode = 'EUR';
        $CoreStoreModel->country = $Country;

        $Phone = new Phone();
        $Phone->international = $this->HoleTelefonNummer($rs, self::TELFON_INTERNATIONAL);
        $Phone->national = $this->HoleTelefonNummer($rs, self::TELFON_NATIONAL);
        $CoreStoreModel->phone = $Phone;

        $OpeningHours = new OpeningHours();
        $OpeningHours->monday = $this->OeffnungszeitenTag(new Monday(),$rs->FeldInhalt('FOZ_MOFRVON'),$rs->FeldInhalt('FOZ_MOFRBIS'));
        $OpeningHours->tuesday = $this->OeffnungszeitenTag(new Tuesday(),$rs->FeldInhalt('FOZ_MOFRVON'),$rs->FeldInhalt('FOZ_MOFRBIS'));
        $OpeningHours->wednesday = $this->OeffnungszeitenTag(new Wednesday(),$rs->FeldInhalt('FOZ_MOFRVON'),$rs->FeldInhalt('FOZ_MOFRBIS'));
        $OpeningHours->thursday = $this->OeffnungszeitenTag(new Thursday(),$rs->FeldInhalt('FOZ_MOFRVON'),$rs->FeldInhalt('FOZ_MOFRBIS'));
        $OpeningHours->friday = $this->OeffnungszeitenTag(new Friday(),$rs->FeldInhalt('FOZ_MOFRVON'),$rs->FeldInhalt('FOZ_MOFRBIS'));
        $OpeningHours->saturday = $this->OeffnungszeitenTag(new Saturday(),$rs->FeldInhalt('FOZ_SAVON'),$rs->FeldInhalt('FOZ_SABIS'));
        $OpeningHours->sunday = null;
        $CoreStoreModel->openingHours = $OpeningHours;

        $GeneralInspection = new GeneralInspection();
        $GeneralInspection->monday = $rs->FeldInhalt('FUT_MONTAG');
        $GeneralInspection->tuesday = $rs->FeldInhalt('FUT_DIENSTAG');
        $GeneralInspection->wednesday = $rs->FeldInhalt('FUT_MITTWOCH');
        $GeneralInspection->thursday = $rs->FeldInhalt('FUT_DONNERSTAG');
        $GeneralInspection->friday = $rs->FeldInhalt('FUT_FREITAG');
        $GeneralInspection->saturday = $rs->FeldInhalt('FUT_SAMSTAG');
        $GeneralInspection->sunday = null;
        $CoreStoreModel->generalInspection = $GeneralInspection;

        $GeoLocation = new GeoLocation();
        $GeoLocation->latitude = str_replace(',','.',$rs->FeldInhalt('FIF_GEOBREITE')); //DOUBLE
        $GeoLocation->longitude = str_replace(',','.',$rs->FeldInhalt('FIF_GEOHOEHE')); //DOUBLE
        $CoreStoreModel->geoLocation = $GeoLocation;

        $CoreStoreModel->googlePlaceId = $rs->FeldInhalt('FIF_GOOGLEID');
        $CoreStoreModel->note = $rs->FeldInhalt('FIF_ONLINEINFO');
        $CoreStoreModel->online = ($rs->FeldInhalt('FIF_ONLINEFINDBAR')=='1')?true:false; //BOOLEAN
        $CoreStoreModel->aboutUsFooter = $rs->FeldInhalt('FIF_ABOUTUSTEXT');
        $CoreStoreModel->eMailAddress = $rs->FeldInhalt('FIF_EMAIL');
        $CoreStoreModel->onlineAppointment = ($rs->FeldInhalt('FIF_TERMINVEREINBARUNGMOEGLICH')=='1')?true:false; //BOOLEAN

        return $CoreStoreModel;
    }

    /**
     * Request fuer CoreStoreData Job
     *
     * @param awisCoreStoreModel $coreStoreModel
     * @return mixed
     * @throws Exception
     */
    protected function CSDRequest(awisCoreStoreModel $coreStoreModel){

        $Url = $this->CSDUrl . self::CSD_URI_FILUPDATE;
        $coreStoreModel = [awisWerkzeuge::UTF8Code($coreStoreModel)];
        $this->_AWISRest->erstelleRequest($Url, 'JSONPOST','JSON',$coreStoreModel);
        $Antwort = $this->_AWISRest->LetzterStatusCode();
        return $Antwort;

    }

    /**
     * Request fuer Feiertage Job
     *
     * @param array $specialOpeningList
     * @return mixed
     * @throws Exception
     */
    protected function FeiertagRequest(array $specialOpeningList){

        $Url = $this->CSDUrl . self::CSD_URI_FEIERTAGE;
        $this->_AWISRest->erstelleRequest($Url, 'JSONPOST','JSON',$specialOpeningList);
        $Antwort = $this->_AWISRest->LetzterStatusCode();
        return $Antwort;

    }

    /**
     * Einspeisung der Oeffnungs- und Schliesszeiten
     *
     * @param $TagObj
     * @param $Start
     * @param $Ende
     * @return mixed
     */
    protected function OeffnungszeitenTag($TagObj, $Start, $Ende) {
        $TagObj->start = $Start.":00";
        $TagObj->end = $Ende.":00";
        return $TagObj;
    }

    /**
     * Laedt Telefonnummer, schickt sie zur Formatierung und returned sie dann so
     *
     * @param $rs
     * @param $Art
     * @return string|string[]|void|null
     */
    protected function HoleTelefonNummer($rs,$Art) {

        if ($rs->FeldInhalt('LAN_CODE') == 'AT'){ // AT-Filialen haben immer Telefonnummer (0/+43) 50 306 [Filialnummer]
            $Tel = '050306'.$rs->FeldInhalt('FIL_ID');
        } else {
            if ($rs->FeldInhalt('FIF_TELEFON') == '') { //Falls Haupttelefonnummer nicht verf�gbar dann Kundentelefonnummer verwenden
                $Tel = $rs->FeldInhalt('KUNDENTELEFON');
            } else {
                $Tel = $rs->FeldInhalt('FIF_TELEFON');
            }
        }

        return $this->FormatiereTelefonNummer($Tel,$Art,$rs->FeldInhalt('LAN_CODE'));
    }

    /**
     * Formatierung der Telefonnummer
     *
     * @param $Tel
     * @param $Art
     * @return string|string[]|void|null
     */
    protected function FormatiereTelefonNummer($Tel,$Art,$Land) {
        $Tel = preg_replace("/[^0-9]/", "", $Tel);
        if ($Tel == '') {
            return;
        }


        if (substr($Tel, 0, 2) == '00') {
            $Tel = '+' . substr($Tel, 2);
        } elseif (substr($Tel, 0, 1) == '0' and $Land == 'AT') {
            $Tel = '+43' . substr($Tel, 1);
        } elseif (substr($Tel, 0, 1) == '0' and $Land == 'DE') {
            $Tel = '+49' . substr($Tel, 1);
        } elseif (substr($Tel, 0, 1) == 4) {
            $Tel = '+' . $Tel;
        }

        if($Art==self::TELFON_NATIONAL) {
            $Tel = "0".substr($Tel,3);
        }
        $this->_Log("Ergebnis Telefonnummer $Tel",999);
        return $Tel;
    }

    /**
     * Updated den Hash
     *
     * @param $FIL_ID
     * @param $NeuerHash
     */
    protected function UpdateHash($FIL_ID, $NeuerHash){

        $this->_Log("Schreibe Hash $NeuerHash f�r $FIL_ID",999);

        $SQL = "merge";
        $SQL .= " into filialinfos dest";
        $SQL .= " using (";
        $SQL .= "   select ".$this->_DB->WertSetzen('FIF','T',$NeuerHash) . ' as NEUER_HASH ';
        $SQL .= "  , ".$this->_DB->WertSetzen('FIF','Z',$FIL_ID) . ' as FIL_ID ';
        $SQL .= " from dual ";
        $SQL .= " ) src";
        $SQL .= " on (src.FIL_ID = dest.FIF_FIL_ID and dest.fif_fit_id = 929) ";
        $SQL .= " when matched then update set";
        $SQL .= "   dest.FIF_WERT = src.NEUER_HASH";
        $SQL .= ",  dest.FIF_USER = 'awisCSD' ";
        $SQL .= ",  dest.FIF_USERDAT = sysdate ";
        $SQL .= "   where dest.FIF_FIL_ID = src.FIL_ID";
        $SQL .= "   and FIF_FIT_ID = 929";
        $SQL .= " when not matched then";
        $SQL .= "   insert ( FIF_FIL_ID, FIF_FIT_ID, FIF_WERT, FIF_READONLY, FIF_IMQ_ID, FIF_USER, FIF_USERDAT)";
        $SQL .= "   values ( ";
        $SQL .= "       src.FIL_ID,";
        $SQL .= "       929,";
        $SQL .= "       src.NEUER_HASH,";
        $SQL .= "       0,";
        $SQL .= "       4,";
        $SQL .= " 'awisCSD', sysdate ";
        $SQL .= "   )";

        $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('FIF'));
    }

    /**
     * Generiert den aktuellen Hash
     *
     * @param awisRecordset $rs
     * @return string
     */
    protected function GeneriereHash(awisRecordset $rs){
        $HashString = '';
        for($i = 1; $i <= $rs->AnzahlSpalten();$i++){
            if($rs->FeldInhalt($i)!= $rs->FeldInhalt('FIF_LETZTERHASH')) {
                $HashString .= $rs->FeldInhalt($i);
            }
        }

        return sha1($HashString);
    }

    /**
     * @param $Msg
     * @param int $Lvl
     */
    private function _Log($Msg, $Lvl = 1){
        $this->_DebugAusgabe .= $Msg . PHP_EOL;
        if($this->_DebugLevel >= $Lvl){
            echo date('c') . ' ' . $Msg . PHP_EOL;
        }
    }

    /**
     * Hauptfunktion fuer den Feiertage Job
     *
     * @throws Exception
     */
    public function LadeFeiertage(){
        $this->_Log("Lade Feiertage",999);

        $SQL = 'select FA.FIL_ID, FA.FIF_BUL_ID, FA.LAN_CODE, f.MVON, f.MBIS, f.SVON,f.SBIS ';
        $SQL .= ' from V_FILIALEN_AKTUELL FA';
        $SQL .= ' left join V_FILIALEN f';
        $SQL .= ' on f.FIL_ID = FA.FIL_ID';
        $rsFil = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsFil->EOF()) { //Gehe jede Filiale durch
            $SpecialOpeningList = array();

            $SQL = ' SELECT * FROM FEIERTAGE where ';
            $SQL .= ' XFT_TAG >= ' . $this->_DB->WertSetzen('XFT', 'D', date('d.m.Y'));
            $SQL .= ' AND (';
            $SQL .= ' XFT_LAN_CODE = ' . $this->_DB->WertSetzen('XFT', 'T', $rsFil->FeldInhalt('LAN_CODE'));
            $SQL .= ' or XFT_BUL_ID = ' . $this->_DB->WertSetzen('XFT', 'Z', $rsFil->FeldInhalt('FIF_BUL_ID'));
            $SQL .= ' or XFT_FIL_ID = ' . $this->_DB->WertSetzen('XFT', 'Z', $rsFil->FeldInhalt('FIL_ID'));
            $SQL .= ')';

            $SQL .= ' ORDER BY XFT_TAG asc';

            $rsFeiertag = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XFT'));
            while(!$rsFeiertag->EOF()){ //Jeden Feiertag fuer die Filiale durchgehen

                $SpecialOpeningModel = new awisSpecialOpeningModel();
                $SpecialOpeningModel->storeId = $rsFil->FeldInhalt("FIL_ID");
                $SpecialOpeningModel->date = date('Y-m-d', strtotime($rsFeiertag->FeldInhalt("XFT_TAG")));

                $this->loeseSonderOeffnungstageAuf($SpecialOpeningModel,$rsFeiertag,$rsFil);

                $SpecialOpeningModel->reason = $rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG');
                $SpecialOpeningModel->description = preg_replace('/.\-(?:.(?!\-))+$/','',$rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG'));

                $SpecialOpeningList[] = awisWerkzeuge::UTF8Code($SpecialOpeningModel);

                $rsFeiertag->DSWeiter();
            }
            $this->FeiertagRequest($SpecialOpeningList); //Schicke Request mit Feiertagsdaten

            $rsFil->DSWeiter();
        }

        $this->_Log("Habe Feiertage verarbeitet",999);

    }

    /**
     * Loest die Oeffnungs- und Schliessungszeiten von Sonderoeffnungstagen auf
     *
     * @param $Obj
     * @param $rsTage
     * @param $rsFil
     */
    protected function loeseSonderOeffnungstageAuf($Obj,$rsTage,$rsFil){

        $Wochentag = date('w',strtotime($rsTage->FeldInhalt("XFT_TAG"))); //0 = Sonntag, 1 = Montag, etc.
        if($Wochentag >= 1 and $Wochentag <= 5 and ($rsTage->Feldinhalt('XFT_UHRVON')=='' xor $rsTage->Feldinhalt('XFT_UHRBIS')=='')) {
            $Obj->opens = (($rsTage->Feldinhalt('XFT_UHRVON')=='')?($rsFil->Feldinhalt('MVON')):($rsTage->Feldinhalt('XFT_UHRVON'))).":00";
            $Obj->closes = (($rsTage->Feldinhalt('XFT_UHRBIS')=='')?($rsFil->Feldinhalt('MBIS')):($rsTage->Feldinhalt('XFT_UHRBIS'))).":00";
        } elseif($Wochentag == 6 and ($rsTage->Feldinhalt('XFT_UHRVON')=='' xor $rsTage->Feldinhalt('XFT_UHRBIS')=='')) {
            $Obj->opens = (($rsTage->Feldinhalt('XFT_UHRVON')=='')?($rsFil->Feldinhalt('SVON')):($rsTage->Feldinhalt('XFT_UHRVON'))).":00";
            $Obj->closes = (($rsTage->Feldinhalt('XFT_UHRBIS')=='')?($rsFil->Feldinhalt('SBIS')):($rsTage->Feldinhalt('XFT_UHRBIS'))).":00";
        } else {
            $Obj->opens = "";
            $Obj->closes = "";
        }

    }

    /**
     * Schreibt eine ERROR-Mail falls Fehler aufgetreten sind
     */
    public function schreibeERRORMail()
    {
        $this->_Werkzeuge->EMail($this->_Werkzeuge->LeseAWISParameter('awis.conf','FEHLER_MAIL_EMPFAENGER'), "ERROR CoreStoreData - DATUM " . date('d.m.Y'),
            $this->_DebugAusgabe, 2, '',
            'awis' . strval(($this->_Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) ? (awisWerkzeuge::AWIS_LEVEL_SHUTTLE) : (awisWerkzeuge::AWIS_LEVEL_ENTWICK)) . '@de.atu.eu');
    }
}