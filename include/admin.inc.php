<? 

function create_fs_array($admcon, $rname)
{
	global $awisRSZeilen;
	$filename = "dateien/".$rname.".df";
	$output = file($filename);
	array_shift($output);		//�berschriften l�schen

	if($admcon)
	{

		$SQL = "SELECT RN_NAME, FS_FILESYSTEM, FS_MOUNTPOINT, FS_UEBERWACHEN, FS_ANZEIGEN, FS_ID FROM V_FILESYSTEME ";
		$SQL = $SQL."WHERE UPPER(RN_NAME) = '".strtoupper($rname)."'";
		$rsFS = awisOpenRecordset($admcon, $SQL);
		$rsFSZeilen = $awisRSZeilen;
	
		if (count($output) != $rsFSZeilen)
		{
			print "noch nicht alle da!<br>";
			print count($output)."<br>";
			print $rsFSZeilen."<br>";
			if (!$rsFSZeilen)
			{
				// Neue RechnerID bestimmen und Eintrag in die Tabelle Rechnernamen buchen
				$SQL = "Select max(FS_RN_ID) MAXID from Filesysteme";
				$rsmaxid = awisOpenRecordset($admcon, $SQL);
				if ($awisRSZeilen != 1)
				{
					print "Alles Mist!<br>";
					print $awisRSZeilen." Zeilen gefunden!";
					exit;
				}
				else
				{
					$neurnid = $rsmaxid["MAXID"][0] + 1;
					$SQL = "Insert into Rechnernamen values(".$neurnid.", '".strtolower($rname)."')";
					awisExecute($admcon, $SQL);
				}
				
				// Neue FilesystemID bestimmen	
				$SQL = "Select max(FS_ID) MAXID from Filesysteme";
				$rsmaxid = awisOpenRecordset($admcon, $SQL);
				if ($awisRSZeilen != 1)
				{
					print "Alles Mist!<br>";
					print $awisRSZeilen." Zeilen gefunden!";
					exit;
				}
				else
				{
					$neuid = ceil($rsmaxid["MAXID"][0]/10)*10;		// Auf den n�chsten Zehnersprung runden 26 -> 30
				}
				
				// Die einzelnen Filesysteme verbuchen
				while(list(,$lines)=each($output))
				{
					$neuid++;
					
					$zeile = ereg_replace("[ ]{2,}", " ", $lines);
					$felder = explode(" ", $zeile);
					$SQL = "Insert into Filesysteme values (";
					$SQL = $SQL.$neuid.", ".$neurnid.", '".trim($felder[0])."', '".trim($felder[5])."', 1, 1)";
					
					awisExecute($admcon, $SQL);
				}
				
			}
			else
			{
				print "Es sind noch nicht alle Filesysteme angelegt! Bitte anlegen. <br>";
			}
			
			
		}

		$zeichnefs = $output;
		$ds1 = -1;
		while(list(,$lines)=each($output))
		{
			$ds1++;
//			print ($ds1.": ".$lines."<br>\n");


			//**********************************
			//* Array in Zeilen zerlegen
			//**********************************
	
			$zeile = ereg_replace("[ ]{2,}", " ", $lines);
			$felder = explode(" ", $zeile);
		

			//**********************************
			//* Daten in DB buchen
			//**********************************
	
			//Abgleich DB - DF
			$ds = array_search($felder[0], $rsFS["FS_FILESYSTEM"]);	

			if ($rsFS["FS_UEBERWACHEN"][$ds]==1)
			{
				$SQL = "SELECT COUNT(FH_FS_ID) AS ANZ FROM FILESYSTEMHISTORIE ";
				$SQL = $SQL."WHERE TO_CHAR(SYSDATE, 'YYMMDD') = TO_CHAR(FH_USERDAT, 'YYMMDD') ";
				$SQL = $SQL."AND FH_FS_ID = ".$rsFS["FS_ID"][$ds];
				$rsAnz = awisOpenRecordset($admcon, $SQL);

				if ($rsAnz["ANZ"][0] == 0)   // Nur dann buchen wenn an diesem Tag noch keine Eintr�ge vorhanden sind
				{
					$SQL = "Insert into Filesystemhistorie values (".$rsFS["FS_ID"][$ds].", ".$felder[1].", ".$felder[3].", sysdate)";
					awisExecute($admcon, $SQL);
				}
			}


			//***********************************************************
			//* Nicht anzuzeigende oder nicht in der DB gespeicherte Zeilen l�schen
			//***********************************************************

			if($ds > -1)
			{
				if($rsFS["FS_ANZEIGEN"][$ds] == 0)
				{
					unset($zeichnefs[$ds1]);
					continue;
				}
			}
			else
			{
				unset($zeichnefs[$ds1]);
				print "Filesystem ".$felder[0]." (".$rname.") fehlt in der Datenbank! <br>";
			}
		}	

		return $zeichnefs;
	}
	else
	{
		return "Error";
	} // If($admcon)	
} // End function

function create_fshist_array($admcon, $rname)
{
	if($admcon)
	{
		$SQL = "SELECT FS_UEBERWACHEN, FS_ANZEIGEN, FS_ID, ";
		$SQL = $SQL."FH_GESAMT, FH_FREI, DATUM FROM V_FILESYSTEMHISTORIE ";
		$SQL = $SQL."WHERE UPPER(RN_NAME) = '".strtoupper($rname)."' ";
		$SQL = $SQL."AND FS_ANZEIGEN = 1 ";
		$SQL = $SQL."ORDER BY RN_NAME, FS_ID, DATUM";
//		$SQL = $SQL."AND UPPER(RN_NAME) = '".strtoupper($rname)."'";
		$rsFS = awisOpenRecordset($admcon, $SQL);

		return $rsFS;
		
	} // If($admcon)	
} // End function


function create_fshist_stat_array($admcon, $rname = "alle")
{
	if($admcon)
	{
		$SQL = "SELECT RN_NAME, FS_FILESYSTEM, FS_MOUNTPOINT, FS_ID, MAX(FH_GESAMT) MAXGES, MAX(FH_FREI) MAXFREI, COUNT(DATUM) ANZ FROM V_FILESYSTEMHISTORIE ";
		if ($rname != "alle")
		{
			$SQL = $SQL."WHERE RN_NAME = '".$rname."' ";
		}
		$SQL = $SQL."GROUP BY RN_NAME, FS_FILESYSTEM, FS_MOUNTPOINT, FS_ID ";
		$rsFS = awisOpenRecordset($admcon, $SQL);

		return $rsFS;
		
	} // If($admcon)	
} // End function


?>
