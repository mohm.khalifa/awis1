<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	ArtikelPruefungenInfoStellen
* 
* 	Pflege der ArtikelPruefungenInfoStellen
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Mar. 2005
* 
****************************************************************************************************/
// Variablen
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 606);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'ArtikelPruefungenInfoStellen',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

echo '<form name=frmArtikelPruefungenInfoStellen method=post action=./ArtikelPruefungenInfoStellen.php>';

if($_POST['cmdSpeichern_x']!='')
{
	$Aenderungen = '';

	If($_POST['txtAIS_KEY_old'] == '')		// Neuer Datensatz
	{
		$SQL = "INSERT INTO ARTIKELPRUEFUNGENINFOSTELLEN(AIS_BEZEICHNUNG,AIS_USER,AIS_USERDAT)";
		$SQL .= " VALUES(";
		$SQL .= "'" . $_POST['txtAIS_BEZEICHNUNG'] . "'";
		$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
		$SQL .= " )";	
	}
	else		// Ge�nderter Datensatz
	{
		If($_POST['txtAIS_BEZEICHNUNG'] != $_POST['txtAIS_BEZEICHNUNG_old'])
		{
			$Aenderungen .= ", AIS_BEZEICHNUNG = '" . $_POST['txtAIS_BEZEICHNUNG'] . "'";
		}
	
		If($Aenderungen!='')
		{
			$SQL = "UPDATE ARTIKELPRUEFUNGENINFOSTELLEN SET " . substr($Aenderungen,2);
			$SQL .= ", AIS_USER='" . $_SERVER['PHP_AUTH_USER'] . "', AIS_USERDAT=SYSDATE";
			$SQL .= " WHERE AIS_KEY=0" . $_POST['txtAIS_KEY_old'];
		}
	}

	if($SQL!='')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("ArtikelPruefungenInfoStellen.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
}	// Speichern von �nderungen


if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM ARTIKELPRUEFUNGENINFOSTELLEN WHERE AIS_KEY='" . $_POST["txtAIS_KEY_old"] . "'";
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			var_dump($SQL);
			
			awisErrorMailLink("ArtikelPruefungenInfoStellen.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Die Stelle wurde erfolgreich gel�scht<br><br><a href=./ArtikelPruefungenInfoStellen.php>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmAIS method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtAIS_KEY_old value=" . $_POST["txtAIS_KEY_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Stelle l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	

}


$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************

if($_POST['cmdNeu_x'])		
{
	$SQL = "SELECT * FROM ARTIKELPRUEFUNGENINFOSTELLEN WHERE ROWNUM < 0";
}
else
{
	if((!isset($_GET['AIS_KEY']) and !isset($_POST['txtAIS_KEY'])) or isset($_POST['cmdTrefferListe_x']))
	{
		$SQL = "SELECT * FROM ARTIKELPRUEFUNGENINFOSTELLEN ";
	}
	else
	{
		$SQL = "SELECT * FROM ARTIKELPRUEFUNGENINFOSTELLEN WHERE AIS_KEY='". $_GET['AIS_KEY'] . $_POST['txtAIS_KEY'] ."'";
	}
	
	$SQL .= " ORDER BY AIS_BEZEICHNUNG";
}

$rsAIS = awisOpenRecordset($con, $SQL);
$rsAISZeilen = $awisRSZeilen;


	//**************************************************
	// Daten ausgeben
	//**************************************************
	
if($rsAISZeilen >1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=400 id=DatenTabelle>';
	echo '<tr><td id=FeldBez>Bezeichnung</td>';
	echo '<td id=FeldBez>Status</td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsAISZeilen;$Zeile++)
	{
		echo "<tr>";
		echo "<td id=DatenFeld><a href=./ArtikelPruefungenInfoStellen.php?AIS_KEY=" . $rsAIS['AIS_KEY'][$Zeile] . ">" .  $rsAIS['AIS_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" .  $rsAIS['AIS_STATUS'][$Zeile] . "</td>";
		
		echo '</tr>';
	}

	if(($Rechtestufe & 4)==4)		// Neuen Datensatz hinzuf�gen
	{
		print "<tr><td colspan=2><input type=image accesskey=N title='Neuer Datensatz (Alt+N)' src=/bilder/plus.png name=cmdNeu onclick=location.href='./ArtikelPruefungenInfoStellen.php'></td></tr>";
	}
	
	echo '</table>';
}
else					// einzelnen anzeigen
{
	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Weiterleitungsstellen f�r Artikelpr�fungen</h3></td>';
	echo '<td><font size=1>' . $rsAIS['AIS_USER'][0] . '<br>' . $rsAIS['AIS_USERDAT'][0] . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./ArtikelPruefungenInfoStellen.php?Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<tr>';
	echo '<td width=200 id=FeldBez>ID</td><td>';
	if(($Rechtestufe & 2)==2)
	{
		echo '' . $rsAIS['AIS_KEY'][0];
		echo "<input type=hidden name=txtAIS_KEY_old value='" . $rsAIS['AIS_KEY'][0] . "'></td>";
	}
	echo '</tr>';

			// Zeile 2
	echo '<tr>';
	echo '<td id=FeldBez>Stellenbezeichnung</td>';
	if(($Rechtestufe & 2)==2)
	{
		echo "<td><input name=txtAIS_BEZEICHNUNG size=40 value='" . $rsAIS['AIS_BEZEICHNUNG'][0] . "'>";
		echo "<input type=hidden name=txtAIS_BEZEICHNUNG_old value='" . $rsAIS['AIS_BEZEICHNUNG'][0] . "'></td>";
	}
	else
	{
		echo '<td>' . $rsAIS['AIS_BEZEICHNUNG'][0] . '</td>';
	}
	echo '</tr>';

			// Zeile 3
	echo '<tr>';
	echo '<td id=FeldBez>Status</td>';
	if(($Rechtestufe & 2)==2)
	{
		echo "<td><select name=txtAIS_STATUS>";
		echo "<option value='A' " . ($rsAIS['AIS_STATUS'][0] == 'A'?'Selected':'') . '>Aktiv</option>';
		echo "<option value='I' " . ($rsAIS['AIS_STATUS'][0] == 'I'?'Selected':'') . '>Inaktiv</option>';
		echo '</select>';
		echo "<input type=hidden name=txtAIS_STATUS_old value='" . $rsAIS['AIS_STATUS'][0] . "'></td>";
	}
	else
	{
		echo '<td>' . $rsAIS['AIS_STATUS'][0] . '</td>';
	}
	echo '</tr>';
	
	echo '<tr><td colspan=99>';
	if(($Rechtestufe & 2)==2)
	{
		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./ArtikelPruefungenInfoStellen.php?AIS_KEY=" . $rsAIS["AIS_KEY"][0] . "&Speichern=True'>";
	}
	if(($Rechtestufe & 4)==4)		// Neuen Datensatz hinzuf�gen
	{
		print " <input type=image accesskey=N title='Neuer Datensatz (Alt+N)' src=/bilder/plus.png name=cmdNeu onclick=location.href='./ArtikelPruefungenInfoStellen.php'>";
	}

	if(($Rechtestufe & 8)==8)		// Datensatz L�schen
	{
		print " <input type=image accesskey=X title='Datensatz l�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen onclick=location.href='./ArtikelPruefungenInfoStellen.php'></td>";
	}
	
	echo '</td></tr></table>';

				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtAIS_BEZEICHNUNG\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
* 
* */

if($_POST['txtZurueck']!='')
{
	$ZurueckLink = $_POST['txtZurueck'];
}
else
{
	if($_GET['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif($_GET['Zurueck']=='')
	{
		$ZurueckLink = '/index.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $_GET['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

echo '</form>';

print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=hersteller','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>