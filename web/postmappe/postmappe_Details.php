<?php
global $AWISBenutzer;
global $AWISCursorPosition;

try {
    $TextKonserven = array();
    $TextKonserven[] = array('PMP', '*');
    $TextKonserven[] = array('HEIDLER', '*');
    $TextKonserven[] = array('FIL', 'FIL_GEBIET');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $script = 'function showFilialen(intVal) {
        if (intVal == 1) {
            $( "#fildiv" ).show();
            $( "#txtFIL_ID" ).prop(\'required\', true);
        } else {
            $( "#fildiv" ).hide();
            $( "#txtFIL_ID" ).prop(\'required\', false);
        }
    }';

    $Form->SchreibeHTMLCode('<script>' . $script . '</script>');

    $Recht48000 = $AWISBenutzer->HatDasRecht(48000);

    if ($Recht48000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('./postmappe_speichern.php');
        $Param = @unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
    }
    elseif(isset($_POST['cmdLoeschen_x']))
    {
        include('./postmappe_speichern.php');
        $Param = @unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
    }

    $Form->SchreibeHTMLCode('<form name="frmHeidlerSuche" action="" method="POST"  >');

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PMP']['PMP_LAGER'] . ':', 200);
    $Daten = explode('|', $AWISSprachKonserven['PMP']['PMP_LAGER_LST']);
    $Form->Erstelle_SelectFeld('!LAGER', '', 220, true, '', '', '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PMP']['PMP_MAPPE'] . ':', 200);
    $Daten = explode('|', $AWISSprachKonserven['PMP']['PMP_MAPPE_LST']);
    $Form->Erstelle_SelectFeld('!MAPPE', '', 220, true, '', '', '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PMP']['PMP_FILID'] . ':', 200);
    $Daten = explode('|', $AWISSprachKonserven['PMP']['PMP_FIL_LST']);
    $Form->Erstelle_SelectFeld('!FILART', '', 220, true, '', '', '', '', '', $Daten, 'onchange="showFilialen(this.value)"');
    $Form->ZeileEnde();

    $Form->ZeileStart('', 'fildiv');
    $Form->Erstelle_TextLabel('', 200);
    $Form->Erstelle_MehrfachSelectFeld('!FIL_ID', [], 220, true, 'SELECT DISTINCT FIL_ID, FIL_ID|| \' \' || FIL_BEZ FROM V_FILIALEN_AKTUELL ORDER BY FIL_ID', '', '', '', '', '', '', '', [], '', 'AWIS', '', '', 100);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Trennzeile('O');
    $Form->ZeileEnde();

    $SQL = 'SELECT * FROM POSTMAPPE WHERE PMP_XBN_KEY =' . $AWISBenutzer->BenutzerID();

	$rsPMP = $DB->RecordSetOeffnen($SQL);

    //****************************************
    // Lagerkennung zu Lagernamen
    //****************************************
    $Daten = explode('|', $AWISSprachKonserven['PMP']['PMP_LAGER_LST']);

    $Lager = array();

    foreach ($Daten as $row)
    {
        $Daten2 = explode('~',$row);
        $Lager[$Daten2[0]] = $Daten2[1];
    }

    //****************************************
    // Mappenkennung zu Mappennamen
    //****************************************
    $Daten = explode('|', $AWISSprachKonserven['PMP']['PMP_MAPPE_LST']);

    $Mappe = array();

    foreach ($Daten as $row)
    {
        $Daten2 = explode('~',$row);
        $Mappe[$Daten2[0]] = $Daten2[1];
    }

    if ($rsPMP->AnzahlDatensaetze() >= 1)
    {
        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart('Druckwarteschlange', false);
        $Form->ZeileStart();
        $Link = '';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PMP']['PMP_LAGER'], 110, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PMP']['PMP_MAPPE'], 390, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PMP']['PMP_FILID'], 100, '', $Link);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsPMP->EOF() ) {

            $Filialen = implode(', ', unserialize($rsPMP->FeldInhalt('PMP_FIL_ID')));

            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('#PMP_LAGER', $Lager[$rsPMP->FeldInhalt('PMP_LAG_KZ')], 0, 110, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#PMP_MAPPE', $Mappe[$rsPMP->FeldInhalt('PMP_MAP_KZ')], 0, 390, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#PMP_FILID', $Filialen, 0, 100, false, ($DS % 2), '', '', 'T', '', $Filialen);

            $Form->ZeileEnde();

            $rsPMP->DSWeiter();
            $DS++;
        }
        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();
    }

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    if($rsPMP->AnzahlDatensaetze()>0){
        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_ds.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X','',27,27,'',true);
        $Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'W','',27,27,'',true);
    }
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
