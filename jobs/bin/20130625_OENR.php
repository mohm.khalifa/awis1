<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');

/*
 * Anforderung A000?? - ATU Nummern ihre OE-Nummern zuordnen
*/

function Protokoll($fp, $ProtArray)
{
	$zeile  = $ProtArray['ATUNR'] .';';
	$zeile .= $ProtArray['OENR'] .';';
	$zeile .= $ProtArray['KATEGORIE'] .';';
	$zeile .= $ProtArray['AKTION'] .';';
	$zeile .= $ProtArray['SQL'] .";\n";
	fputs($fp,$zeile);
	echo $zeile;
	return 0;
}

try
{
	$Anforderung = 'OENR';
	$Anforderer = 'Schmidberger';
	$QuellTabelle = 'XXX_'.$Anforderung;

	//$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130626_Staging_OENR_PROTOKOLL.csv';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130626_Produktiv_OENR_PROTOKOLL.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$zeile = "AST_ATUNR;OENUMMER;KATEGORIE;AKTION;SQL;\n";
	fputs($fp,$zeile);
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	// alles aus temp. Tabelle laden
	$SQL = 'SELECT * FROM '.$QuellTabelle;
	$SQL .= ' WHERE (ERFOLGREICH IS NULL';
	$SQL .= ' OR ERFOLGREICH = 0)';
	$SQL .= ' ORDER BY ATUNR';
	
	$rsQuelle = $DB->RecordSetOeffnen($SQL);
	
	$DB->TransaktionBegin();
	
	while (!$rsQuelle->EOF())
	{
		$ProtokollArray = array();
		$ProtokollArray['ATUNR'] = $rsQuelle->Feldinhalt('ATUNR');
		$ProtokollArray['OENR'] = $rsQuelle->Feldinhalt('OENR');
		$ProtokollArray['AKTION'] = '';
		$ProtokollArray['SQL'] = '';
		
		//Gibt es Eintr�ge f�r ATU Nummer mit OE-Nummer
		$SQL = 'select *';
		$SQL .= ' from '.$QuellTabelle;
		$SQL .= ' inner join artikelstamm on atunr=ast_atunr';
		$SQL .= ' left join oenummern on oen_her_id=her_id and oen_nummer=oenr';
		$SQL .= ' where atunr='.$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('ATUNR'));
		$SQL .= ' and oenr='.$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('OENR'));
		$SQL .= ' and her_id='.$DB->FeldInhaltFormat('N0',$rsQuelle->FeldInhalt('HER_ID'));

		$ProtokollArray['AKTION'] = 'Pr�fung ob OE-NUMMER bereits vorhanden';
		$ProtokollArray['SQL'] = $SQL; 		
		$ProtokollArray['KATEGORIE'] = 'INFO'; 		
		Protokoll($fp, $ProtokollArray);
		
		$rsOEN = $DB->RecordSetOeffnen($SQL);
		
		if (!$rsOEN->EOF())
		{
			//Pr�fen, ob OENR bereits vorhanden
			if ($rsOEN->FeldInhalt('OEN_KEY')=='')
			{
				//OENR nicht vorhanden -> anlegen
				$SQL = 'INSERT INTO OENUMMERN (';
				$SQL .= ' OEN_NUMMER';
				$SQL .= ',OEN_HER_ID';
				$SQL .= ',OEN_SUCHNUMMER';
				$SQL .= ',OEN_MUSTER';
				$SQL .= ',OEN_IMQ_ID';
				$SQL .= ',OEN_USER';
				$SQL .= ',OEN_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= '  '.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('OENR'));
				$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$rsOEN->FeldInhalt('HER_ID'));
				$SQL .= ' ,asciiwortoe('.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('OENR')).')';
				$SQL .= ' ,0';
				$SQL .= ' ,4';
				$SQL .= " ,'EDV20130613'";
				$SQL .= ' ,SYSDATE';
				$SQL .= ')';
				
				$ProtokollArray['AKTION'] = 'OE-NUMMER nicht vorhanden, Anlegen der OENR';
				$ProtokollArray['SQL'] = $SQL;
				$ProtokollArray['KATEGORIE'] = 'INSERT';
				Protokoll($fp, $ProtokollArray);
				
				$DB->Ausfuehren($SQL,'',true);
				
				$SQL = "SELECT seq_GLOBAL_KEY.CurrVal AS KEY FROM DUAL";
				$rsKEY = $DB->RecordSetOeffnen($SQL);
				$OEN_key = $rsKEY->FeldInhalt('KEY');
				
				$SQL  = 'DELETE FROM OENUMMERN';
				$SQL .= ' WHERE OEN_KEY = ' .$DB->FeldInhaltFormat('N0',$OEN_key);
				$ProtokollArray['AKTION'] = 'Undo Insert OE_NUMMER';
				$ProtokollArray['SQL'] = $SQL;
				$ProtokollArray['KATEGORIE'] = 'UNDO';
				Protokoll($fp, $ProtokollArray);
			}
			else
			{
				$OEN_key = $rsOEN->FeldInhalt('OEN_KEY');
				
				//sonst nichts machen
				$ProtokollArray['AKTION'] = 'OE-NUMMER bereits vorhanden, nichts machen';
				$ProtokollArray['SQL'] = '';
				$ProtokollArray['KATEGORIE'] = 'INFO';
				Protokoll($fp, $ProtokollArray);
			}

			//Pr�fen, ob Teileinfo Kombination 'AST' - 'OEN' vorhanden
			$SQL = 'select *';
			$SQL .= ' from teileinfos';
			$SQL .= ' where tei_ity_id1=\'AST\'';
			$SQL .= ' and tei_ity_id2=\'OEN\'';
			$SQL .= ' and tei_key1='.$DB->FeldInhaltFormat('N0',$rsOEN->FeldInhalt('AST_KEY'));
			$SQL .= ' and tei_key2='.$DB->FeldInhaltFormat('N0',$OEN_key);
			
			$ProtokollArray['AKTION'] = 'Pr�fung ob TEILEINFO bereits vorhanden';
			$ProtokollArray['SQL'] = $SQL;
			$ProtokollArray['KATEGORIE'] = 'INFO';
			Protokoll($fp, $ProtokollArray);
				
			$rsTEI = $DB->RecordSetOeffnen($SQL);

			if ($rsTEI->EOF())
			{
				$SQL  = 'insert into TEILEINFOS (';
				$SQL .= ' TEI_ITY_ID1, TEI_KEY1, TEI_SUCH1, TEI_WERT1,';
				$SQL .= ' TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2,';
				$SQL .= ' TEI_USER, TEI_USERDAT';
				$SQL .= ' ) VALUES (';
				$SQL .= ' \'AST\'';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$rsOEN->FeldInhalt('AST_KEY'));
				$SQL .= ' ,asciiwort('.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('AST_ATUNR')).')';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('AST_ATUNR'));
				$SQL .= ' ,\'OEN\'';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$OEN_key);
				$SQL .= ' ,asciiwortoe('.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('OENR')).')';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsOEN->FeldInhalt('OENR'));
				$SQL .= ',\'EDV20130613\'';
				$SQL .= ',sysdate';
				$SQL .= ')';
				
				$ProtokollArray['AKTION'] = 'TEILEINFO nicht vorhanden, Anlegen TEILEINFO';
				$ProtokollArray['SQL'] = $SQL;
				$ProtokollArray['KATEGORIE'] = 'INSERT';
				Protokoll($fp, $ProtokollArray);
				
				$DB->Ausfuehren($SQL,'',true);
				
				$SQL = "SELECT seq_TEI_KEY.CurrVal AS KEY FROM DUAL";
				$rsKEY = $DB->RecordSetOeffnen($SQL);
				$TEI_key = $rsKEY->FeldInhalt('KEY');
				
				$SQL  = 'DELETE FROM TEILEINFOS';
				$SQL .= ' WHERE TEI_KEY = ' .$DB->FeldInhaltFormat('N0',$TEI_key);
				$ProtokollArray['AKTION'] = 'Undo Insert Teileinfos Kombination AST/OEN neue Nummer';
				$ProtokollArray['SQL'] = $SQL;
				$ProtokollArray['KATEGORIE'] = 'UNDO';
				Protokoll($fp, $ProtokollArray);
			}
			else
			{
				//sonst nichts machen
				$ProtokollArray['AKTION'] = 'OE-TEILEINFO bereits vorhanden, nichts machen';
				$ProtokollArray['SQL'] = '';
				$ProtokollArray['KATEGORIE'] = 'INFO';
				Protokoll($fp, $ProtokollArray);
			}
				
    		$SQL = 'UPDATE ' .$QuellTabelle;
    		$SQL .= ' SET erfolgreich = 1 ';
    		$SQL .= ' WHERE ATUNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('ATUNR'));
    		$SQL .= ' AND OENR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('OENR'));
    		$DB->Ausfuehren($SQL,'',true);
		}
		else
		{
			$ProtokollArray['AKTION'] = 'Fehler bei Pr�fung, ob OENR vorhanden';
			$ProtokollArray['SQL'] = $SQL; 
			$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
			Protokoll($fp, $ProtokollArray);
		}
		
		$rsQuelle->DSWeiter();
	}

	//$DB->TransaktionRollback();
	$DB->TransaktionCommit();
	fclose($fp);
	
	echo ' fertig ';
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	fclose($fp);
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	fclose($fp);
}

?>
