<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RST', '%');
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_RST_IMPORTSTATUS');
    $TextKonserven[]=array('Liste','lst_RST_STATUS');
    $TextKonserven[]=array('Liste','lst_ROLLRES');
    $TextKonserven[]=array('Liste','lst_WETGRIP');
    $TextKonserven[]=array('Liste','lst_NOISECL');
	

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15300 = $AWISBenutzer->HatDasRecht(15300);
	if(($Recht15300)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$RSTParam = unserialize($AWISBenutzer->ParameterLesen('Formular_RST_Artikel'));

	if(!isset($RSTParam['BLOCK']) OR $RSTParam['RST_KEY']!=$AWIS_KEY1)
	{
		$RSTParam['BLOCK']=1;
		$RSTParam['KEY']='';
		$RSTParam['RST_KEY']=$AWIS_KEY1;
	}

	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************	}
	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY RRP_GUELTIGAB ASC';
		$RSTParam['ORDER']=$ORDERBY;
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$ORDERBY = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($ORDERBY==''?'':' ORDER BY '.$OrderBy);
		$RSTParam['ORDER']=$ORDERBY;
	}

	$SQL = 'SELECT';
	$SQL .= ' RRP_KEY,';
	$SQL .= ' RRP_RST_KEY,';
	$SQL .= ' RRP_AST_KEY,';
	$SQL .= ' RRP_ATUNR,';
	$SQL .= ' RRP_BEZEICHNUNGWW,';
	$SQL .= ' RRP_KENN1,';
	$SQL .= ' RRP_KENN2,';
	$SQL .= ' RRP_KENN_L2,';
	$SQL .= ' RRP_SUCH1,';
	$SQL .= ' RRP_SUCH2,';
	$SQL .= ' RRP_MEHRFACHKENNUNG,';
	$SQL .= ' RRP_GUELTIGAB,';
	$SQL .= ' RRP_GUELTIGBIS,';
	$SQL .= ' CASE';
	$SQL .= '  WHEN RRP_GUELTIGAB <= SYSDATE AND RRP_GUELTIGBIS < SYSDATE THEN \'font-style:italic;color:black;\'';
	$SQL .= '  WHEN RRP_GUELTIGAB <= SYSDATE AND RRP_GUELTIGBIS >= SYSDATE THEN \'\'';
	$SQL .= '  WHEN RRP_GUELTIGAB > SYSDATE THEN \'font-style:italic;color:green;\'';
	$SQL .= '  ELSE \'font-style:italic;color:red;\'';
	$SQL .= ' END as STYLE,';
	$SQL .= ' RRP_USER,';
	$SQL .= ' RRP_USERDAT,';
	$SQL .= ' row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM REIFENPFLEGE';
	$SQL .= ' WHERE RRP_RST_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['RRPListe']))
	{
	    $AWIS_KEY2=0;
	}
	elseif(isset($_GET['RRP_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['RRP_KEY']);
	}
	elseif($AWIS_KEY2=='')
	{
		if($RSTParam['RST_KEY']==$AWIS_KEY1)
		{
			$AWIS_KEY2 = $RSTParam['KEY'];
		}
		else
		{
			$RSTParam['BLOCK']=1;
			$RSTParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}

	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND RRP_KEY = 0'.$AWIS_KEY2;
		$_GET['RRP_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}

	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$RSTParam['BLOCK']=$Block;
		}
		elseif(isset($RSTParam['BLOCK']))
		{
			$Block=$RSTParam['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsRRP = $DB->RecordsetOeffnen($SQL);
	$Form->DebugAusgabe(1,$SQL);
    
    // Blockweise
    $StartZeile=0;
    if(isset($_GET['Block']))
    {
    	$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
    }
    
    $Form->Formular_Start();

    if ($AWIS_KEY2!=-1)
    {
	    $Form->ZeileStart();
	    
	    $Icons=array();
	    if((intval($Recht15300)&6)!=0 and $rsRRP->EOF())
	    {
	    	$Icons[] = array('new','./reifenstamm_Main.php?cmdAktion=Details&Seite=Artikel&RST_KEY='.$AWIS_KEY1.'&RRP_KEY=-1');
	    }
	     
	    $Form->Erstelle_ListeIcons($Icons,38,-1);
	     
	    $Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	    $Link .= '&SSort=RRP_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_ATUNR'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'],100,'',$Link);
	     
	    $Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	    $Link .= '&SSort=RRP_BEZEICHNUNGWW'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_BEZEICHNUNGWW'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNGWW'],350,'',$Link);
	     
	    $Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	    $Link .= '&SSort=RRP_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGAB'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_GUELTIGAB'],100,'',$Link);
	     
	    $Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	    $Link .= '&SSort=RRP_GUELTIGBIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGBIS'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_GUELTIGBIS'],100,'',$Link);
	     
	    $Form->ZeileEnde();
	    
		if($rsRRP->AnzahlDatensaetze()>=1 OR isset($_GET['RRPListe']) OR !isset($_GET['RRP_KEY']))
		{
	    	$DS=0;
			while(!$rsRRP->EOF())
			{
				$Form->ZeileStart();
	
				$Icons = array();
				if(intval($Recht15300&2)>0)	// Ändernrecht
				{
					$Icons[] = array('edit','../reifenpflege/reifenpflege_Main.php?cmdAktion=Details&RST_KEY='.$AWIS_KEY1.'&RRP_KEY='.$rsRRP->FeldInhalt('RRP_KEY'));
				}
				if(intval($Recht15300&4)>0)	// Ändernrecht
				{
					$Icons[] = array('delete','./reifenstamm_Main.php?cmdAktion=Details&Seite=Artikel&RST_KEY='.$AWIS_KEY1.'&Del='.$rsRRP->FeldInhalt('RRP_KEY'));
				}
					
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
	
				$Style = $rsRRP->FeldInhalt('STYLE');
				$Form->Erstelle_ListenFeld('*RRP_ATUNR',$rsRRP->FeldInhalt('RRP_ATUNR'),20,100,false,($DS%2),$Style,'','T');
				$Form->Erstelle_ListenFeld('*RRP_BEZEICHNUNGWW',$rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW'),20,350,false,($DS%2),$Style,'','T');
				$Form->Erstelle_ListenFeld('*RRP_GUELTIGAB',$rsRRP->FeldInhalt('RRP_GUELTIGAB'),20,100,false,($DS%2),$Style,'','D');
				$Form->Erstelle_ListenFeld('*RRP_GUELTIGBIS',$rsRRP->FeldInhalt('RRP_GUELTIGBIS'),20,100,false,($DS%2),$Style,'','D');
	/*
				if($rsRRP->FeldInhalt('RRP_BEMERKUNG')!='')
	            {
	                $Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsRRP->FeldInhalt('RRP_BEMERKUNG'));
	            }
	*/
				$Form->ZeileEnde();
	
				$rsRRP->DSWeiter();
				$DS++;
			}
	/*
			$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer';
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	*/
			$Form->Formular_Ende();
		}
    }
	else
	{
		$EditModus = ($Recht15300&6);
		$AWIS_KEY2 = $rsRRP->FeldInhalt('RRP_KEY');
		$RSTParam['KEY']=$AWIS_KEY2;
		$RSTParam['RST_KEY']=$AWIS_KEY1;
		
		$Form->Erstelle_HiddenFeld('RRP_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('RRP_RST_KEY',$AWIS_KEY1);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ATUNR'] . ':', 220);
		$Form->Erstelle_TextFeld('!RRP_ATUNR', ($rsRRP->FeldInhalt('RRP_ATUNR')), 6, 100, $EditModus,'','','','','','','',6);
		$AWISCursorPosition = 'txtRRP_ATUNR';
		$Form->ZeileEnde();
		
		$SQL = 'SELECT RST_LIE_NR, RST_LARTNR';
		$SQL .= ' FROM reifenstamm';
		$SQL .= ' WHERE RST_KEY = :var_N0_RST_KEY';
		$SQL .= ' ';
		$DB->SetzeBindevariable('RST','var_N0_RST_KEY',$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
		$rsRST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RST'));
		//$Form->DebugAusgabe(1,$DB->LetzterSQL());
		
		$SQL = 'SELECT AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW';
		$SQL .= ' FROM reifenpflege';
		$SQL .= ' INNER JOIN v_reifen_ast ON AST_ATUNR = RRP_ATUNR';
		$SQL .= ' WHERE AST_LIEFNR = :var_T_AST_LIEFNR';
		$SQL .= ' AND AST_LIEFARTNR = :var_T_AST_LIEFARTNR';
		$SQL .= ' AND RRP_RST_KEY IS NULL';
		$SQL .= ' ';
		$DB->SetzeBindevariable('AST','var_T_AST_LIEFNR',$rsRST->FeldInhalt('RST_LIE_NR'),awisDatenbank::VAR_TYP_TEXT);
		$DB->SetzeBindevariable('AST','var_T_AST_LIEFARTNR',$rsRST->FeldInhalt('RST_LARTNR'),awisDatenbank::VAR_TYP_TEXT);
		$rsAST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('AST'));
		//$Form->DebugAusgabe(1,$DB->LetzterSQL());

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['txt_Angelegt'].':',220);
		while(!$rsAST->EOF())
		{
			$Link ='/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY='.$rsAST->FeldInhalt('AST_KEY');
			$Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_ATUNR'),80,'',$Link, $rsAST->FeldInhalt('AST_BEZEICHNUNGWW'));
		
			$rsAST->DSWeiter();
		}
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_GUELTIGAB'].':',220);
		$Form->Erstelle_TextFeld('!RRP_GUELTIGAB',($rsRRP->FeldInhalt('RRP_GUELTIGAB')),20,200,$EditModus,'','','','D','','','',10);
		$Form->ZeileEnde();

		$Form->Trennzeile('O');
	}
	$Form->Formular_Ende();
	
	$AWISBenutzer->ParameterSchreiben('Formular_RST_Artikel',serialize($RSTParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201304301639");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201304301640");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>