<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');
require_once('filialinfos_funktionen.inc');
require_once 'awisTeamplanAPI.php';
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('PER', '%');
    $TextKonserven[] = array('PET', '%');
    $TextKonserven[] = array('Filialinfo', '%');
    $TextKonserven[] = array('Wort', 'Wochentag_%_Kurz');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5001 = $AWISBenutzer->HatDasRecht(5001);        // Seite �berhaupt
    $Recht5008 = $AWISBenutzer->HatDasRecht(5008);        // Inhalte der Seite
    $Recht5000 = $AWISBenutzer->HatDasRecht(5000);        // Personalnummernanzeige
    if (($Recht5001 & 32) == 0) {
        $Form->Fehler_KeineRechte();
        die();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILMBW'));

    $TeamplanAPI = new awisTeamplanAPI();

    //Wenn heute Montag ist, aktuelles Datum nehmen, ansonsten nehme den letzten Montag..
    $DatumMontag = date('w')==1?date('c'):date('c',strtotime('last monday', time()));
    $DatumSonntag = date('c',strtotime('next sunday', time()));

    $TeamplanErg = $TeamplanAPI->AnwesenheitenEinerFiliale($AWIS_KEY1, $DatumMontag, $DatumSonntag);

    $Form->Formular_Start();

    $Form->DebugAusgabe(1,'Presence-Response-AWIS-Model: ' . json_encode($TeamplanErg));

    if (($Recht5008 & 32) == 32) { //Alle oder nur ein Tag?
        $FeldListe = array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag');
    } else {
        $FeldListe = array(strftime("%A"));
    }

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_NACHNAME'] . ', ' . $AWISSprachKonserven['PER']['PER_VORNAME'], 250, '', '');
    if (($Recht5000 & 32) != 0) {
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_NR'], 150, '', '');
    }
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PET']['PET_TAETIGKEIT'], 250, '', '');
    if (($Recht5008 & 2) == 2) { //Anzeige Anwesend/Abwesend(1)
        foreach ($FeldListe as $Feld) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_' . $Feld . '_Kurz'], 35);
        }
    }
    $Form->ZeileEnde();


    if(count($TeamplanErg) == 0 or $TeamplanErg == false){

        $PEIKey = new awisFilialen($AWIS_KEY1);

        //*********************************************************
        //* Sortierung
        //*********************************************************
        if (!isset($_GET['SSort'])) {
            $ORDERBY = ' ORDER BY PET_Sortierung, PER_NACHNAME, PER_VORNAME';
        } else {
            $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SSort']);
        }

        $SQL = 'SELECT PER_NACHNAME, PER_NR, PER_VORNAME, PET_TAETIGKEIT, PERSONALEINSATZPLANUNG.*';
        $SQL .= ' FROM PERSONAL';
        $SQL .= ' INNER JOIN PERSONALEINSATZPLANUNG ON PER_Nr = PEP_PER_Nr';
        $SQL .= ' INNER JOIN (SELECT * FROM PERSONALTAETIGKEITEN WHERE PET_USER in (SELECT DISTINCT PER_USER FROM PERSONAL WHERE PER_FIL_ID=' . $DB->WertSetzen('PEP','N0',$AWIS_KEY1) . '))';
        $SQL .= '    ON PET_ID = PER_PET_ID and PET_LAN_WWS_KENN = PER_LAN_WWS_KENN';
        $SQL .= ' WHERE ';
        $SQL .= ' PEP_FIL_ID = ' .  $DB->WertSetzen('PEP','N0',$AWIS_KEY1);
        $SQL .= ' AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)';
        if (($PersAusschluss = $AWISBenutzer->ParameterLesen('Filialinfo: PersNr unterdruecken')) != '') {
            $SQL .= ' AND PER_NR NOT IN (' . $PersAusschluss . ')';
        }
        $SQL .= ' AND PEP_JAHR = ' . $DB->WertSetzen('PEP', 'N0', date("Y", time())) . ' AND PEP_KALENDERWOCHE = ' . $DB->WertSetzen('PEP', 'N0', date("W", time()));
        $SQL .= $ORDERBY;
        $rsPEP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('PEP'));

        $EinsatzplanungAnzeigen = true;
        //Wenn keine Einsatzplanung da ist, wenigstens das Personal einzeigen.
        if($rsPEP->AnzahlDatensaetze() == 0){
            $SQL = 'SELECT PER_NACHNAME, PER_NR, PER_VORNAME, PET_TAETIGKEIT';
            $SQL .= ' FROM PERSONAL';
            $SQL .= ' left join personaltaetigkeiten ';
            $SQL .= ' ON pet_id = per_pet_id and PET_LAN_WWS_KENN = PER_LAN_WWS_KENN ';
            $SQL .= ' WHERE PER_FIL_ID =   ' . $DB->WertSetzen('PEP','N0',$AWIS_KEY1);
            $SQL .= ' AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)';
            if (($PersAusschluss = $AWISBenutzer->ParameterLesen('Filialinfo: PersNr unterdruecken')) != '') {
                $SQL .= ' AND PER_NR NOT IN (' . $PersAusschluss . ')';
            }
            $SQL .= $ORDERBY;

            $rsPEP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('PEP'));

            $EinsatzplanungAnzeigen = false;

        }

        $Form->DebugAusgabe(1, $DB->LetzterSQL());

        $SQL = 'SELECT PEK_ID, PEK_ANZEIGESTUFE, PEK_TEXT ';
        $SQL .= ', MONTAGVON';
        $SQL .= ', MONTAGBIS';
        $SQL .= ', DIENSTAGVON';
        $SQL .= ', DIENSTAGBIS';
        $SQL .= ', MITTWOCHVON';
        $SQL .= ', MITTWOCHBIS';
        $SQL .= ', DONNERSTAGVON';
        $SQL .= ', DONNERSTAGBIS';
        $SQL .= ', FREITAGVON';
        $SQL .= ', FREITAGBIS';
        $SQL .= ', SAMSTAGVON';
        $SQL .= ', SAMSTAGBIS';
        $SQL .= ', SONNTAGVON';
        $SQL .= ', SONNTAGBIS';
        $SQL .= ' FROM V_PKUERZEL ';
        $SQL .= ' WHERE PEK_LAN_WWS_KENN = ' . $DB->WertSetzen('PEK', 'T', $PEIKey->FilialInfo('FIL_LAN_WWSKENN'));

        $rsPEK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('PEK'));
        //Aufbau des Arrays: $PKUERZEL[ID_DES_K�RZELS][TABELLENFELD]
        //ZB: $PKUERZEL['--']['SONNTAGBIS'] = '09:00';
        while (!$rsPEK->EOF()) {
            for ($i = 1; $i < $rsPEK->AnzahlSpalten(); $i++) {
                $PKuerzel[$rsPEK->FeldInhalt('PEK_ID')][$rsPEK->SpaltenNamen()[$i]] = $rsPEK->FeldInhalt($i + 1);
            }
            $rsPEK->DSWeiter();
        }
        unset($rsPEK); //Daten sind nun im Array

        $DS = 0;
        while (!$rsPEP->EOF()) {
            $Form->ZeileStart();

            $Form->Erstelle_ListenFeld('#PER_NACHNAME', $rsPEP->FeldInhalt('PER_NACHNAME') . ', ' . $rsPEP->FeldInhalt('PER_VORNAME'), 0, 250, false, ($DS % 2), '');
            if (($Recht5000 & 32) != 0) {
                $Form->Erstelle_ListenFeld('#PER_NR', $rsPEP->FeldInhalt('PER_NR'), 0, 150, false, ($DS % 2));
            }
            $Form->Erstelle_ListenFeld('#PET_TAETIGKEIT', $rsPEP->FeldInhalt('PET_TAETIGKEIT'), 0, 250, false, ($DS % 2));

            if (($Recht5008 & 2) == 2 and $EinsatzplanungAnzeigen) { //Anzeige Anwesend/Abwesend(1)
                foreach ($FeldListe as $Feld) {
                    $Feld = strtoupper($Feld);
                    //Bekanntes K�rzel?
                    if (isset($PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)])) {
                        if (($Recht5008 & 256) == 256) { //Anzeige als Klartext so wie fr�her..
                            $Text = $rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld);
                            $ToolTipp = $PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)]['PEK_TEXT'];
                            $Form->Erstelle_ListenFeld($Feld, $Text, 0, 35, false, ($DS % 2), '', '', 'T', 'L', $ToolTipp);
                            continue;
                        }

                        if (($Recht5008 & 64) == 64) { // Anzeige Anwesend/Abwesend Aufgrund aktueller Uhrzeit
                            $Von = strtotime($PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'VON']);
                            $Bis = strtotime($PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'BIS']);
                            $Aktuell = strtotime((date('H:i:s')));
                            if ($Von <= $Aktuell and $Bis >= $Aktuell) {
                                $Bild = '/bilder/icon_anwesend.png';
                            } else {
                                $Bild = '/bilder/icon_abwesend.png';
                            }
                        } else {//Aktuelle Uhrzeit ist egal
                            if ($PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'VON'] != -1) { //-1 bedeutet, dass er/sie an diesem Tag garnicht da ist
                                $Bild = '/bilder/icon_anwesend.png';
                            } else {
                                $Bild = '/bilder/icon_abwesend.png';
                            }
                        }

                        $ToolTipp = '';
                        if (($Recht5008 & 16) == 16) { //Anzeige Anwesend/Abwesend Tooltip mit Uhrzeit
                            if ($PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'VON'] != -1) {
                                $ToolTipp = 'ca. ' . $PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'VON'] . ' - ' . $PKuerzel[$rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld)][$Feld . 'BIS'];
                            }
                        }
                        if (($Recht5008 & 128) == 128) {
                            $ToolTipp .= PHP_EOL . $rsPEP->FeldInhalt('PEP_PEK_ID_' . $Feld);
                        }
                    } else { //unbekanntes K�rzel
                        $Bild = '/bilder/icon_anwesend_abwesend.png';
                        $ToolTipp = '';
                    }
                    $Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION, $Feld . $rsPEP->DSNummer(), '', $Bild, $ToolTipp, ($DS % 2), 'width: 35px;');
                }
            }
            $Form->ZeileEnde();

            $DS++;
            $rsPEP->DSWeiter();
        }
    }else{
        $DS = 0;

        $AnzeigeDaten = [];
        foreach ($TeamplanErg as $PersNr => $Tage){

            $MADaten = $TeamplanAPI->MasterDataService($PersNr);

            $Form->DebugAusgabe(1, 'Master-Data-Response-AWIS-Model: ' . json_encode($MADaten));
            $Anzeige = array();
            $Anzeige['Gefunden'] = false;

            //Mitarbeiter im Masterdata gefunden?
            if(isset($MADaten['lastName'])){
                $Anzeige['Gefunden'] = true;
                $Anzeige['Nachname'] = $MADaten['lastName'];
                $Anzeige['Vorname'] =  $MADaten['firstName'];
                $Anzeige['Taetigkeit'] =  $MADaten['occupation'];
                $DS++;
            }else{

                //Nicht im Masterdata gefunden... Deswegen im AWIS schauen
                $Form->DebugAusgabe(1,'Keine Daten f�r MA aus API gefunden, suche im AWIS: ' . $PersNr);

                $SQL = 'select * from v_personal_komplett where PERSNR = ' . $DB->WertSetzen('PER','T', $PersNr);
                $SQL .= ' and DATUM_EINTRITT <= Sysdate and (DATUM_AUSTRITT >= sysdate or DATUM_AUSTRITT is null)';

                $rsPER = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('PER'));

                if($rsPER->AnzahlDatensaetze() > 0){
                    $Anzeige['Gefunden'] = true;
                    $Anzeige['Nachname'] = $rsPER->FeldInhalt('NAME');
                    $Anzeige['Vorname'] =  $rsPER->FeldInhalt('VORNAME');
                    $Anzeige['Taetigkeit'] =  $rsPER->FeldInhalt('TAETIGKEIT');;

                }
            }

            if($Anzeige['Gefunden'] == true){
                $Anzeige['PersNr'] =  $PersNr;


                //Die Tage kommen unsortiert von Teamplan mit einem Unixtimestamp. Diesen in einen Wochentag "wandeln" f�r eigenes Array
                $AnzeigeTage = array();
                foreach ($Tage as $Time => $Wert){
                    $AnzeigeTage[date('N',$Time)] = $Wert;
                }
                unset($Tage);

                //Montag bis Sonntag durchgehen ...
                for ($i=1;$i<=7;$i++){
                    $Bild = '/bilder/icon_abwesend.png';
                    $TTT = 'X';
                    //... und schauen, ob Daten dieses Tages von Teamplan gekommen sind (was nicht immer der Fall ist)
                    if(isset($AnzeigeTage[$i]) ){
                        if ($AnzeigeTage[$i]['present']) {
                            $Bild = '/bilder/icon_anwesend.png';
                        }

                        $TTT = $AnzeigeTage[$i]['presenceCode'];
                    }

                    $Anwesenheiten[$i]['icon'] = $Bild;
                    $Anwesenheiten[$i]['ttt'] = $TTT ;
                }

                $Anzeige['Anwesenheiten'] = $Anwesenheiten;
                $AnzeigeDaten[utf8_encode($Anzeige['Nachname'])][] = $Anzeige;
            }
        }

        //Alphabetisch nach Namen sortieren...
        ksort($AnzeigeDaten);
        foreach ($AnzeigeDaten as $Key => $UnterArray) {

            foreach ($UnterArray as $Anzeige) {
                $Form->ZeileStart();

                $Form->Erstelle_ListenFeld('#PER_NACHNAME', $Anzeige['Nachname'] . ', ' . $Anzeige['Vorname'], 0, 250, false, ($DS % 2), '');
                if (($Recht5000 & 32) != 0) {
                    $Form->Erstelle_ListenFeld('#PER_NR', $Anzeige['PersNr'], 0, 150, false, ($DS % 2));
                }
                $Form->Erstelle_ListenFeld('#PET_TAETIGKEIT', $Anzeige['Taetigkeit'], 0, 250, false, ($DS % 2));

                foreach ($Anzeige['Anwesenheiten'] as $Key =>  $Anwesenheit){
                    $Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION, '', '', $Anwesenheit['icon'],$Anwesenheit['ttt'] , ($DS % 2), 'width: 35px;');
                }
                $Form->ZeileEnde();
                $DS++;
            }


        }
    }
    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200908041634");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200908041633");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>