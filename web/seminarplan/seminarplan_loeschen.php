<?php
require_once 'awisDokument.inc';
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']) AND !isset($_POST['txtSEH_KEY']))
{
	switch($_GET['Seite'])
	{
	case 'Teilnehmer':
		$Tabelle = 'SEP';
		$Key=$_POST['txtSEP_KEY'];
		$SEPKey=$_POST['txtSEP_KEY'];
	
		$AWIS_KEY1 = $Key;
	
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('SEP','SEP_TRAINING'),$_POST['txtSEP_TRAINING']);	
		$Felder[]=array($Form->LadeTextBaustein('SEP','SEP_SEMINAR'),$_POST['txtSEP_SEMINAR']);	
		break;
	case 'Feedback':
		$Tabelle = 'SEE';
		$Key=$_POST['txtSEE_KEY'];
		$SEPKey=$_POST['txtSEE_SEP_KEY'];
		
		$AWIS_KEY1 = $Key;
		
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('SEE','SEE_BEZEICHNUNG'),$rsDaten->FeldInhalt('SEE_BEZEICHNUNG'));
		break;
	case 'Kriterium':
			$Tabelle = 'SKK';
			$Key=$_POST['txtSKK_KEY'];
			$SEPKey=$_GET['SEZ_KEY'];
		
			$AWIS_KEY1 = $Key;
		
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM1'),$rsDaten->FeldInhalt('SKK_KRITERIUM1'));
			$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM2'),$rsDaten->FeldInhalt('SKK_KRITERIUM2'));
			$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM3'),$rsDaten->FeldInhalt('SKK_KRITERIUM3'));
            break;
	}
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Teilnehmer':
			$Tabelle = 'SET';
			$Key=$_GET['Del'];

			$SQL = 'SELECT SET_KEY, SET_SEP_KEY, SET_NACHNAME, SET_VORNAME';
			$SQL .= ' FROM SeminarTeilnehmer ';
			$SQL .= ' WHERE SET_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$SEPKey = $rsDaten->FeldInhalt('SET_SEP_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('SET','SET_NACHNAME'),$rsDaten->FeldInhalt('SET_NACHNAME'));
			$Felder[]=array($Form->LadeTextBaustein('SET','SET_VORNAME'),$rsDaten->FeldInhalt('SET_VORNAME'));
			break;
		 case 'Feedback':
				$Tabelle = 'SEE';
				$Key=$_GET['Del'];
			
				$SQL = 'SELECT *';
				$SQL .= ' FROM SEMINARFEEDBACK ';
				$SQL .= ' WHERE SEE_KEY=0'.$Key;
			
				$rsDaten = $DB->RecordsetOeffnen($SQL);
			
				$SEPKey = $rsDaten->FeldInhalt('SEE_SEP_KEY');
			
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('SEE','SEE_BEZEICHNUNG'),$rsDaten->FeldInhalt('SEE_BEZEICHNUNG'));
		 break;
		 case 'Kriterium':
		 	$Tabelle = 'SKK';
		 	$Key=$_GET['Del'];
			
		 	$Sprache = $AWISBenutzer->BenutzerSprache();
		 	
		 	$SQL = 'SELECT '.
				 	'case when skk_kriterium1 is null then \'\' else func_textkonserve(\'KRITERIUM1\',\'SKK\',\''.$Sprache.'\',skk_kriterium1 ) end as skk_kriterium1, '.
				 	'case when skk_kriterium2 is null then \'\' else func_textkonserve(\'KRITERIUM2\',\'SKK\',\''.$Sprache.'\',skk_kriterium2 ) end as skk_kriterium2, '.
				 	'case when skk_kriterium3 is null then \'\' else func_textkonserve(\'KRITERIUM3\',\'SKK\',\''.$Sprache.'\',skk_kriterium3 ) end as skk_kriterium3 ';
		 	$SQL .= ' FROM SEMINARKRITERIUM ';
		 	$SQL .= ' WHERE SKK_KEY=0'.$Key;
		 			
		 	$rsDaten = $DB->RecordsetOeffnen($SQL);
		 		
		 	$SEPKey = $_GET['SEZ_KEY'];
		 		
		 	$Felder=array();
		 	$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM1'),$rsDaten->FeldInhalt('SKK_KRITERIUM1'));
		 	$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM2'),$rsDaten->FeldInhalt('SKK_KRITERIUM2'));
		 	$Felder[]=array($Form->LadeTextBaustein('SKK','SKK_KRITERUM3'),$rsDaten->FeldInhalt('SKK_KRITERIUM3'));
		 	
		 break;
		 case 'Anzeige';
		    $Tabelle = 'SEA';
		    $Key=$_GET['Del'];

             $SQL = 'SELECT SEA_KEY, SEA_BEREICH, SEA_BESCHREIBUNG';
             $SQL .= ' FROM SEMINARANZEIGE ';
             $SQL .= ' WHERE SEA_KEY='.$Key;

             $rsDaten = $DB->RecordsetOeffnen($SQL);

             $SEPKey = $rsDaten->FeldInhalt('SEA_KEY');

             $Felder=array();
             $Felder[]=array($Form->LadeTextBaustein('SEA','SEA_BEREICH'),$rsDaten->FeldInhalt('SEA_BEREICH'));
             $Felder[]=array($Form->LadeTextBaustein('SEA','SEA_BEZEICHNUNG'),$rsDaten->FeldInhalt('SEA_BESCHREIBUNG'));

		 break;
	}
}elseif(isset($_GET['DocDel'])){
    $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['SEP_KEY']);
    $DOC = new awisDokument($DB, $AWISBenutzer);
    $DOC->DokumentenZuordnungLoeschen($_GET['DocDel'],'SEH',$_GET['HOTEL_SET_KEY']);
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
{
	$SQL = '';
	switch ($_POST['txtTabelle'])
	{
		case 'SEP':
			$SQL = 'DELETE FROM SEMINARPLAN WHERE sep_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
			
			break;		
		case 'SET':
			
			$SQL = 'SELECT SET_SEP_KEY, SET_NACHNAME, SET_VORNAME, SEP_TRAINING, SET_STATUS';
			$SQL.= ' FROM SEMINARTEILNEHMER ';
			$SQL.= ' INNER JOIN SEMINARPLAN ON SEP_KEY = SET_SEP_KEY WHERE SET_KEY = 0'.$_POST['txtKey'];
			$rsDaten = $DB->RecordSetOeffnen($SQL);			
			
			$SQL = 'DELETE FROM SEMINARTEILNEHMER WHERE SET_KEY = 0'.$_POST['txtKey'];								
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
						
			$Text = 'Training: '.$rsDaten->FeldInhalt('SEP_TRAINING').chr(10);
			$Text.= 'Name: '.$rsDaten->FeldInhalt('SET_NACHNAME').' '.$rsDaten->FeldInhalt('SET_VORNAME');
				
			//Freie pl�tze reduzieren
			$SQL = 'UPDATE Seminarplan SET';
			$SQL.= ' SEP_FREIEPLAETZE = SEP_FREIEPLAETZE + 1';
			$SQL.= ', SEP_USER = \''.$AWISBenutzer->BenutzerName().'\'';
			$SQL.= ', SEP_USERDAT = SYSDATE';
			$SQL.=' WHERE SEP_KEY = 0'.$_POST['txtSEPKey'];
			$Form->DebugAusgabe(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
			}

			if ($rsDaten->FeldInhalt('SET_STATUS')!='Gebucht')
			{
				if (awisWerkzeuge::awisLevel()=='ENTW')
				{
					$Email = 'thomas.riedl@de.atu.eu';
				}
				else 
				{
					$Email = 'ATUAcademy@de.atu.eu';
				}
				$SQL = 'BEGIN P_AWIS_MAIL.PROC_SEND_MAIL(\''.$Text .'\', \''.$Email.'\', \'Stornierter Seminarteilnehmer\'); COMMIT; END;';					
				
				$Form->DebugAusgabe(1,$SQL);
	
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
				}	
			}
			
			$AWIS_KEY1=$_POST['txtSEPKey'];
		case 'SEE':
			$SQL = 'DELETE FROM SEMINARFEEDBACK WHERE see_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtSEPKey'];
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
			$SQL = 'DELETE FROM SEMINARZUORD WHERE sez_see_key=0'.$_POST['txtKey'];
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
			break;
		case 'SKK':
		$SQL = 'DELETE FROM SEMINARKRITERIUM WHERE skk_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtSEPKey'];
				if($DB->Ausfuehren($SQL)===false)
				{
					awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
				}
				$SQL = 'DELETE FROM SEMINARZUORD WHERE sez_see_key=0'.$_POST['txtKey'];
		    break;
        case 'SEA':
            $SQL = 'DELETE FROM SEMINARANZEIGE WHERE sea_key='.$_POST['txtKey'];
            $AWIS_KEY1='';
            if($DB->Ausfuehren($SQL)===false)
            {
                awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
            }
            break;
        default:
            break;
	}
}

if(isset($_POST['cmdLoeschen_x']) AND isset($_POST['txtSEH_KEY'])) {

    $SQL = 'UPDATE SEMINARHOTEL SET SEH_STATUS = \'I\' WHERE SEH_KEY = '.$DB->WertSetzen('SEH', 'N0', $_POST['txtSEH_KEY'], true);
    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SEH'));

    $SQL = 'DELETE FROM SEMINARHOTELZUORD WHERE SHZ_SEH_KEY = '.$DB->WertSetzen('SHZ', 'N0', $_POST['txtSEH_KEY'], true);
    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SHZ'));

    $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_HOTEL_INAKTIV'], 5);
}

if(isset($_POST['del_relation_x'])) {
    $SQL = 'DELETE FROM SEMINARHOTELZUORD WHERE SHZ_SET_KEY = '.$DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_SET_KEY'], true);
    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SHZ'));
}

if($Tabelle!='')
{
	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./seminarplan_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

	$Form->Formular_Start();
	
	$Form->ZeileStart();		
	$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
	$Form->ZeileEnde();

	$Form->Trennzeile('O');
	
	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}
	
	$Form->Erstelle_HiddenFeld('SEPKey',$SEPKey);
	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
	$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
	$Form->ZeileEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>