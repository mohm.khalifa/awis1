<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
try
{

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $TextKonserven = array();
    $TextKonserven[] = array('VVS', '*');
    $TextKonserven[] = array('VVE', '*');
    $TextKonserven[] = array('FIL', '*');
    $TextKonserven[] = array('SONDERAKTION', '*');
    $TextKonserven[] = array('V_VERS_FORM_DETAIL', '*');
    $TextKonserven[] = array('V_VERS_FORM_RECHNUNGSDATEN', '*');
    $TextKonserven[] = array('VCB', '*');
    $TextKonserven[] = array('VEB', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    echo '<script src="versicherungenDublettenCheck.js"></script>';

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    //Wenn Button fuer Freigabe gedrueckt wurde
    if($Form->NameInArray($_POST, 'ico_ok', 0, 1) != '')
    {
        $VVS_KEY = explode('_', $Form->NameInArray($_POST, 'ico_ok', 0, 1))[2];

        $SQL = 'UPDATE VERSVORGANGSSTATUS ';
        $SQL .= ' SET VVS_VEN_KEY = 0, VVS_VCN_KEY = 0, VVS_DUBCHECK = 3';
        $SQL .= ' ,VVS_USER = \'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ' ,VVS_USERDAT = sysdate';
        $SQL .= ' WHERE VVS_KEY = ' . $DB->WertSetzen('VVS', 'N0', $VVS_KEY);

        $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('VVS'));
    }
    elseif($Form->NameInArray($_POST, 'ico_delete', 0, 1) != '')
    {
        $VVS_KEY = explode('_', $Form->NameInArray($_POST, 'ico_delete', 0, 1))[2];

        $SQL = 'UPDATE VERSVORGANGSSTATUS ';
        $SQL .= ' SET VVS_VEN_KEY = 29, VVS_VCN_KEY = 124, VVS_DUBCHECK = 4, ';
        $SQL .= ' VVS_USER = \'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ' ,VVS_USERDAT = sysdate';
        $SQL .= ' WHERE VVS_KEY = ' . $DB->WertSetzen('VVS', 'N0', $VVS_KEY);

        $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('VVS'));
    }

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT ' . $AWIS_KEY1 . ' FROM dual';
    $SQL .= '   UNION';
    $SQL .= '   SELECT vdb.vdb_ref';
    $SQL .= '   FROM versicherungen_dubletten vdb';
    $SQL .= '   WHERE vdb.vdb_vvs_key = ' . $DB->WertSetzen('VDB', 'N0', $AWIS_KEY1);
    $SQL .= '   UNION';
    $SQL .= '   SELECT vdb.vdb_vvs_key';
    $SQL .= '   FROM versicherungen_dubletten vdb';
    $SQL .= '   WHERE vdb.vdb_ref = ' . $DB->WertSetzen('VDB', 'N0', $AWIS_KEY1);

    $rsDubletten = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VDB'));

    $DubKeys = array();

    while(!$rsDubletten->EOF())
    {
        $SQL = 'SELECT ' . $rsDubletten->FeldInhalt(1) . ' FROM dual';
        $SQL .= '   UNION';
        $SQL .= '   SELECT vdb.vdb_ref';
        $SQL .= '   FROM versicherungen_dubletten vdb';
        $SQL .= '   WHERE vdb.vdb_vvs_key = ' . $DB->WertSetzen('VDB', 'N0', $rsDubletten->FeldInhalt(1));
        $SQL .= '   UNION';
        $SQL .= '   SELECT vdb.vdb_vvs_key';
        $SQL .= '   FROM versicherungen_dubletten vdb';
        $SQL .= '   WHERE vdb.vdb_ref = ' . $DB->WertSetzen('VDB', 'N0', $rsDubletten->FeldInhalt(1));

        $rsSubDubletten = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VDB'));

        while(!$rsSubDubletten->EOF())
        {
            $DubKeys[] = $rsSubDubletten->FeldInhalt(1);

            $rsSubDubletten->DSWeiter();
        }

        $rsDubletten->DSWeiter();
    }
    $DubKeys = array_unique($DubKeys);

    unset($rsDubletten);
    unset($rsSubDubletten);

    $SQL = 'SELECT * FROM VERSVORGANGSSTATUS ';
    $SQL .= ' INNER JOIN VERSCHECKSTATUSNUMMERN ';
    $SQL .= ' ON vvs_vcn_key = vcn_key ';
    $SQL .= ' INNER JOIN VERSSTATUSNUMMERN ';
    $SQL .= ' ON vvs_ven_key = ven_key ';
    $SQL .= ' WHERE VVS_KEY in (';
    $JSKeys = '';
    foreach($DubKeys as $Key)
    {
        $JSKeys .= $Key . ',';
    }
    $SQL .= $JSKeys;
    $Form->Erstelle_HiddenFeld('JSKeys', $JSKeys);

    $SQL = substr($SQL, 0, strlen($SQL) - 1);
    $SQL .= ')';

    $rsDubletten = $DB->RecordSetOeffnen($SQL);

    $FeldBreiten = array();

    $FeldBreiten['DS'] = 100;
    $FeldBreiten['FILID'] = 50;
    $FeldBreiten['KFZ_KENNZ'] = 100;
    $FeldBreiten['AKTION'] = 40;
    $FeldBreiten['WANR'] = 70;
    $FeldBreiten['VORGANGNR'] = 130;
    $FeldBreiten['VERSNR'] = 60;
    $FeldBreiten['VERSSCHEINNR'] = 150;
    $FeldBreiten['VERARBSTATUS'] = 200;
    $FeldBreiten['SCHADKZ'] = 40;
    $FeldBreiten['ZURUECKSETZEN'] = 90;

    $Form->SchreibeHTMLCode('<form name="frmDuplikate" action="versicherungen_Main.phpmdAktion=Details&Seite=Duplikate" method=POST  enctype="multipart/form-data">');

    if($rsDubletten->AnzahlDatensaetze() == 1)
    {
        echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
    }
    elseif(($rsDubletten->AnzahlDatensaetze() > 1))
    {

        $Form->ZeileStart();

        $Form->Erstelle_Liste_Ueberschrift('Datensatz', $FeldBreiten['DS'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Filnr', $FeldBreiten['FILID'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('WANR', $FeldBreiten['WANR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('SKZ', $FeldBreiten['SCHADKZ'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('KFZ_KENNZ', $FeldBreiten['KFZ_KENNZ'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('VERSSCHEINNR', $FeldBreiten['VERSSCHEINNR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('VersNr', $FeldBreiten['VERSNR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Verarbeitung', $FeldBreiten['VERARBSTATUS'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Zurücksetzen', $FeldBreiten['ZURUECKSETZEN'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Sperren', $FeldBreiten['ZURUECKSETZEN'], '', '');

        $Form->ZeileEnde();
        $DS = 0;
        while(!$rsDubletten->EOF())
        {
            $Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsDubletten->FeldInhalt('VVS_KEY'));

            $SperrSQL = 'Update Versicherungen_dubletten ';
            $SperrSQL .= ' set VDB_STATUS = 555 ';
            $SperrSQL .= ' , VDB_USER =' . $DB->WertSetzen('VDB', 'T', $AWISBenutzer->BenutzerName());
            $SperrSQL .= ' , VDB_USERDAT = sysdate ';
            $SperrSQL .= ' where VDB_REF =' . $DB->WertSetzen('VDB', 'N0', $rsDubletten->FeldInhalt('VVS_KEY'));
            $SperrSQL .= ' or vdb_vvs_key = ' . $DB->WertSetzen('VDB', 'N0', $rsDubletten->FeldInhalt('VVS_KEY'));
            $HG = ($DS % 2);
            $DB->Ausfuehren($SperrSQL, '', false, $DB->Bindevariablen('VDB'));

            $Link = './versicherungen_Main.php?cmdAktion=Details&VVS_KEY=' . $rsDubletten->FeldInhalt('VVS_KEY') . '&Seite=Duplikate';

            if($AWIS_KEY1 == $rsDubletten->FeldInhalt('VVS_KEY'))
            {
                $Form->Erstelle_ListenFeld('Datensatz', 'geoeffneter DS', 0, $FeldBreiten['DS'], false, $HG, 'color:red;', '', 'T');
            }
            else
            {
                $Form->Erstelle_ListenFeld('Datensatz', 'Duplikat', 0, $FeldBreiten['DS'], false, $HG, '', '', 'T');
            }
            $Form->Erstelle_ListenFeld('VVS_FILID', $rsDubletten->FeldInhalt('VVS_FILID'), 0, $FeldBreiten['FILID'], false, $HG, '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_WANR', $rsDubletten->FeldInhalt('VVS_WANR'), 0, $FeldBreiten['WANR'], false, $HG, '', $Link, 'T');
            $Form->Erstelle_ListenFeld('VVS_SCHADENKZ', $rsDubletten->FeldInhalt('VVS_SCHADENKZ'), 0, $FeldBreiten['SCHADKZ'], false, $HG, '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_KFZKENNZ', $rsDubletten->FeldInhalt('VVS_KFZKENNZ'), 0, $FeldBreiten['KFZ_KENNZ'], false, $HG, '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_VERSSCHEINNR', $rsDubletten->FeldInhalt('VVS_VERSSCHEINNR'), 0, $FeldBreiten['VERSSCHEINNR'], false, $HG, '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_VERSNR', $rsDubletten->FeldInhalt('VVS_VERSNR'), 0, $FeldBreiten['VERSNR'], false, $HG, '', '', 'T');
            if($rsDubletten->FeldInhalt('VVS_DUBCHECK') == 1)
            {
                $TTT = $rsDubletten->FeldInhalt('VCN_BEMERKUNG');
                $Verarbstatus = 'Vorgang offen';
                $Form->Erstelle_ListenFeld('VERARBSTATUS', strlen($Verarbstatus) > 30?substr($Verarbstatus, 0, 27) . '...':$Verarbstatus, 0, $FeldBreiten['VERARBSTATUS'], false, $HG, '', '', 'T', 'L', $TTT);

                $Icons = array();
                $Icons[] = array('ok', 'POST~' . $rsDubletten->FeldInhalt('VVS_KEY'));
                $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ZURUECKSETZEN'], $HG);

                $Icons = array();
                $Icons[] = array('delete', 'POST~' . $rsDubletten->FeldInhalt('VVS_KEY'));
                $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ZURUECKSETZEN'], $HG);
            }
            elseif($rsDubletten->FeldInhalt('VVS_DUBCHECK') == 2)
            {
                $Verarbstatus = 'Vorgang bereits verarbeitet';
                $Form->Erstelle_ListenFeld('VERARBSTATUS', $Verarbstatus, 0, $FeldBreiten['VERARBSTATUS'], false, $HG, '', '', 'T', 'L');
            }
            elseif($rsDubletten->FeldInhalt('VVS_DUBCHECK') == 3)
            {
                $Verarbstatus = 'Vorgang zurueck gesetzt';
                $Form->Erstelle_ListenFeld('VERARBSTATUS', $Verarbstatus, 0, $FeldBreiten['VERARBSTATUS'], false, $HG, '', '', 'T', 'L');
            }
            elseif($rsDubletten->FeldInhalt('VVS_DUBCHECK') == 4)
            {
                $Verarbstatus = 'Vorgang gesperrt';
                $Form->Erstelle_ListenFeld('VERARBSTATUS', $Verarbstatus, 0, $FeldBreiten['VERARBSTATUS'], false, $HG, '', '', 'T', 'L');
            }

            $Form->ZeileEnde();
            $DS++;
            $rsDubletten->DSWeiter();
        }
    }

    $Form->SchreibeHTMLCode('</form>');
}
catch(Exception $ex)
{
    if(!$Form instanceof awisFormular)
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>