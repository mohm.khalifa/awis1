<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZSZ','%');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('ZAL','ZAL_BESTELLNUMMER');
	$TextKonserven[]=array('ZAL','ZAL_BEZEICHNUNG');
	$TextKonserven[]=array('ZLA','ZLA_SORTIMENT');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10008 = $AWISBenutzer->HatDasRecht(10008);
	if($Recht10008==0)
	{
	    awisEreignis(3,1000,'Warengruppen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$DetailAnsicht=false;
$Form->DebugAusgabe(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZSZ_SORTIMENT'] = $_POST['sucZSZ_SORTIMENT'];
		$Param['ZSZ_WG1'] = $_POST['sucZSZ_WG1'];
		$Param['ZSZ_WG2'] = $_POST['sucZSZ_WG2'];
		$Param['ZSZ_WG3'] = $_POST['sucZSZ_WG3'];
		$Param['ZSZ_LIE_NR'] = $_POST['sucZSZ_LIE_NR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("ZukaufZSZSuche",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./warengruppen_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = $AWISBenutzer->ParameterLesen("ZukaufZSZSuche");
		}
		else
		{
			$Param = $AWIS_KEY1;
		}
	}
	elseif(isset($_GET['WG1']))
	{
		$Param['ZSZ_WG1'] = $_GET['WG1'];
		$Param['ZSZ_WG2'] = $_GET['WG2'];
		$Param['ZSZ_WG3'] = $_GET['WG3'];
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./warengruppen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('ZukaufZSZSuche'));
	}
	elseif(isset($_POST['cmdNeu_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('ZukaufZSZSuche'));
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['ZSZ_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZSZ_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('ZukaufZSZSuche'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('ZukaufZSZSuche'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('ZukaufZSZSuche',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY ZSZ_SORTIMENT';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT ZukaufSortimentZuordnungen.*, LIE_NAME1';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZukaufSortimentZuordnungen';
	$SQL .= ' LEFT OUTER JOIN Lieferanten ON ZSZ_LIE_NR = LIE_NR';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('ZukaufZSZSuche',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

//$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZSZ = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmwarengruppen action=./warengruppen_Main.php?cmdAktion=Details method=POST>';

	if($rsZSZ->EOF() AND !isset($_POST['cmdNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZSZ->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './warengruppen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

		$Link .= '&Sort=ZSZ_SORTIMENT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_SORTIMENT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'],100,'',$Link);
		$Link .= '&Sort=LIE_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],300,'',$Link);
		$Link .= '&Sort=ZSZ_WG1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG1'],100,'',$Link);
		$Link .= '&Sort=ZSZ_WG2'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG2'],100,'',$Link);
		$Link .= '&Sort=ZSZ_WG3'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG3'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG3'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZSZ->EOF())
		{
			$Form->ZeileStart();
			$Link = './warengruppen_Main.php?cmdAktion=Details&ZSZ_KEY=0'.$rsZSZ->FeldInhalt('ZSZ_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZSZ_SORTIMENT',$rsZSZ->FeldInhalt('ZSZ_SORTIMENT'),0,100,false,($DS%2),'',$Link);
			$Link='/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsZSZ->FeldInhalt('ZSZ_LIE_NR');
			$Form->Erstelle_ListenFeld('LIE_NAME1',$rsZSZ->FeldInhalt('LIE_NAME1'),0,300,false,($DS%2),'',$Link);
			$Form->Erstelle_ListenFeld('ZSZ_WG1',$rsZSZ->FeldInhalt('ZSZ_WG1'),0,100,false,($DS%2));
			$Form->Erstelle_ListenFeld('ZSZ_WG2',$rsZSZ->FeldInhalt('ZSZ_WG2'),0,100,false,($DS%2));
			$Form->Erstelle_ListenFeld('ZSZ_WG3',$rsZSZ->FeldInhalt('ZSZ_WG3'),0,100,false,($DS%2));
			$Form->ZeileEnde();

			$rsZSZ->DSWeiter();
			$DS++;
		}

		$Link = './warengruppen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZSZ->FeldInhalt('ZSZ_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('ZukaufZSZSuche',serialize($Param));

		echo '<input type=hidden name=txtZSZ_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./warengruppen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZSZ->FeldInhalt('ZSZ_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZSZ->FeldInhalt('ZSZ_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10008&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10008&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',200);
		$SQL = "select ast_atunr, ast_atunr as anzeige from artikelstamm where ast_atunr like 'ZUK___'";
		$Form->Erstelle_SelectFeld('ZSZ_SORTIMENT',$rsZSZ->FeldInhalt('ZSZ_SORTIMENT'),200,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$AWISCursorPosition='txtZSZ_SORTIMENT';
		$Form->ZeileEnde();

			// Warengruppe
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG1'].':',200);
		$Form->Erstelle_TextFeld('ZSZ_WG1',$rsZSZ->FeldInhalt('ZSZ_WG1'),10,370,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG2'].':',200);
		$Form->Erstelle_TextFeld('ZSZ_WG2',$rsZSZ->FeldInhalt('ZSZ_WG2'),10,370,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG3'].':',200);
		$Form->Erstelle_TextFeld('ZSZ_WG3',$rsZSZ->FeldInhalt('ZSZ_WG3'),10,370,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZSZ_BEZEICHNUNG',$rsZSZ->FeldInhalt('ZSZ_BEZEICHNUNG'),100,470,$EditRecht);
		$Form->ZeileEnde();

		// Lieferant
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_LIE_NR'].':',200);
		$SQL = 'SELECT LIE_NR, LIE_NAME1 FROM Lieferanten';
		$SQL .= ' WHERE EXISTS( SELECT * FROM ZukaufArtikelLieferanten WHERE LIE_NR = ZAL_LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZSZ_LIE_NR',$rsZSZ->FeldInhalt('ZSZ_LIE_NR'),200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();

        if($rsZSZ->FeldInhalt('ZSZ_KEY')!='')
        {
    		$SQL = 'SELECT * FROM (SELECT ZAL_KEY, ZAL_ZLA_KEY, ZAL_BESTELLNUMMER, ZAL_BEZEICHNUNG, ZLA_SORTIMENT';
            $SQL .= ', row_number() over (ORDER BY ZAL_KEY DESC) AS ZeilenNr';
            $SQL .= ' FROM ZukaufArtikelLieferanten ';
            $SQL .= ' INNER JOIN ZukaufArtikel ON ZAL_ZLA_KEY = ZLA_KEY';
        	$SQL .= ' WHERE ZAL_LIE_NR = '.$DB->WertSetzen('ZAL', 'N0', $rsZSZ->FeldInhalt('ZSZ_LIE_NR'));
        	$SQL .= ' AND ZAL_WG1 = '.$DB->WertSetzen('ZAL', 'T', $rsZSZ->FeldInhalt('ZSZ_WG1'));
        	$SQL .= ' AND ZAL_WG2 = '.$DB->WertSetzen('ZAL', 'T', $rsZSZ->FeldInhalt('ZSZ_WG2'));
        	$SQL .= ' AND ZAL_WG3 = '.$DB->WertSetzen('ZAL', 'T', $rsZSZ->FeldInhalt('ZSZ_WG3'));
    		$SQL .= ') DATEN WHERE ZeilenNr <= 30';

    		$rsZAL = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZAL'));

    		$Form->Trennzeile();
    		
    		$Form->ZeileStart();
    		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAL']['ZAL_BESTELLNUMMER'], 250);
    		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAL']['ZAL_BEZEICHNUNG'], 450);
    		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_SORTIMENT'], 250);
    		$Form->ZeileEnde();
    		
    		$DS=0;
    		while(!$rsZAL->EOF())
    		{
    		    $Form->ZeileStart();
    		    $Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLA_KEY='.$rsZAL->FeldInhalt('ZAL_ZLA_KEY');
    		    $Form->Erstelle_ListenFeld('ZAL_BESTELLNUMMER', $rsZAL->FeldInhalt('ZAL_BESTELLNUMMER'), 0, 250,false,($DS%2),'',$Link);
    		    $Form->Erstelle_ListenFeld('ZAL_BEZEICHNUNG', $rsZAL->FeldInhalt('ZAL_BEZEICHNUNG'), 0, 450,false,($DS%2));
    		    $Form->Erstelle_ListenFeld('ZLA_SORTIMENT', $rsZAL->FeldInhalt('ZLA_SORTIMENT'), 0, 250,false,($DS%2));
    		    $Form->ZeileEnde();
    		    
    		    $DS++;
    		    $rsZAL->DSWeiter();
    		}
		
        }		
		
		
	}
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10008&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10008&4)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht10008&8)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}


	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=warengruppen&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZSZ_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		$Bedingung .= ' AND (UPPER(ZSZ_SORTIMENT) ' . $DB->LIKEoderIST($Param['ZSZ_SORTIMENT'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZSZ_WG1']) AND $Param['ZSZ_WG1']!='')
	{
		$Bedingung .= ' AND (UPPER(ZSZ_WG1) ' . $DB->LIKEoderIST($Param['ZSZ_WG1'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZSZ_WG2']) AND $Param['ZSZ_WG2']!='')
	{
		$Bedingung .= ' AND (UPPER(ZSZ_WG2) ' . $DB->LIKEoderIST($Param['ZSZ_WG2'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZSZ_WG3']) AND $Param['ZSZ_WG3']!='')
	{
		$Bedingung .= ' AND (UPPER(ZSZ_WG3) ' . $DB->LIKEoderIST($Param['ZSZ_WG3'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZSZ_LIE_NR']) AND $Param['ZSZ_LIE_NR']>0)
	{
		$Bedingung .= ' AND ZSZ_LIE_NR = ' .$DB->FeldInhaltFormat('T',$Param['ZSZ_LIE_NR']) . '';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>