<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
//global $HTTP_POST_VARS;
global $_POST;
global $cmdAktion;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,500);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Auswertungen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if (isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}

awis_RegisterErstellen(50, $con, $cmdAktion);

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

