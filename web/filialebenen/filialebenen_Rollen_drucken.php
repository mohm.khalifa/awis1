<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');

global $awisRSZeilen;

ini_set('max_execution_time','9000');

$AWISBenutzer = new awisUser();

$con = awisLogon();
$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FER','FER%');
$TextKonserven[]=array('TITEL','tit_FilialebenenRollen');
$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
$TextKonserven[]=array('Wort','Seite');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

// Abfrage mit den Kunden, f�r die etwas gedruckt werden soll
$SQL = 'SELECT DISTINCT Filialebenenrollen.*';
$SQL .= ' FROM Filialebenenrollen';
$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';

$rsFBR = awisOpenRecordset($con, $SQL);
$rsFBRZeilen = $awisRSZeilen;

$Vorlagen = array('BriefpapierATU_DE_Seite_2.pdf');
$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Rollen f�r Filialebenen');
$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund

$Spalte = 20;
$Zeile = 30;

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','B',15);
$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$AWISSprachKonserven['TITEL']['tit_FilialebenenRollen']),0,0,'L',0);

$Ausdruck->_pdf->SetXY($Spalte,$Ausdruck->SeitenHoehe()-12);
$Ausdruck->_pdf->SetFont('Arial','B',6);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('TP',$AWISSprachKonserven['Wort']['Seite']).' '.$Ausdruck->_pdf->PageNo(),0,0,'L',0);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('TP',$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich']),0,0,'C',0);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('DU',date('d.m.Y H:i')),0,0,'R',0);
$Ausdruck->_pdf->Line(20,$Ausdruck->SeitenHoehe()-11,$Ausdruck->SeitenBreite()-20,$Ausdruck->SeitenHoehe()-11);

$Zeile = 40;
$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','B',9);
$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_BEZEICHNUNG']),0,0,'L',0);
$Ausdruck->_pdf->Cell(20,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_GUELTIGAB']),0,0,'L',0);
$Ausdruck->_pdf->Cell(20,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_GUELTIGBIS']),0,0,'L',0);
$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_BEMERKUNG']),0,0,'L',0);
$Zeile+=4;
$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
$Zeile++;


for($FBRZeile=0;$FBRZeile<$rsFBRZeilen;$FBRZeile++)
{
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',8);

	$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$rsFBR['FER_BEZEICHNUNG'][$FBRZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,awisFormat('D',$rsFBR['FER_GUELTIGAB'][$FBRZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,awisFormat('D',$rsFBR['FER_GUELTIGBIS'][$FBRZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$rsFBR['FER_BEMERKUNG'][$FBRZeile]),0,0,'L',0);

	$Zeile+=4;
	
	if($Zeile>$Ausdruck->SeitenHoehe()-20)
	{
		$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund
		
		$Spalte = 20;
		$Zeile = 30;
		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->SetFont('Arial','B',15);
		$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$AWISSprachKonserven['TITEL']['tit_FilialebenenRollen']),0,0,'L',0);
		
		$Zeile = 40;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->SetFont('Arial','B',9);
		$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_BEZEICHNUNG']),0,0,'L',0);
		$Ausdruck->_pdf->Cell(20,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_GUELTIGAB']),0,0,'L',0);
		$Ausdruck->_pdf->Cell(20,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_GUELTIGBIS']),0,0,'L',0);
		$Ausdruck->_pdf->Cell(50,5,awisFormat('TP',$AWISSprachKonserven['FER']['FER_BEMERKUNG']),0,0,'L',0);
		$Zeile+=4;
		$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
		$Zeile++;
	}
}

// Ende des Massendrucks?
@ob_clean();
$Ausdruck->Anzeigen();

awisLogoff($con);
?>