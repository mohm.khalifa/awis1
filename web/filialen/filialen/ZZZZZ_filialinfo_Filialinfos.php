<?
// Übergabeparameter
global $txtFIL_ID;
global $txtFIL_BEZ;
global $txtFIL_ORT;
global $txtFIL_PLZ;
global $cmdSuche;
global $cmdAktion;
global $awisRSZeilen;

//Ende Übergabeparameter
$SQL = '';				// SQL Statement für Abfragen

print "<br><form name=frmFilInfos>";

	// Parameter speichern für die Suchseite
if($cmdSuche=="Aktualisieren")
{
	$ParamListe = $txtFIL_ID . ";" . $txtFIL_BEZ . ";" . $txtFIL_ORT . ";" . $txtFIL_PLZ;
	awis_BenutzerParameterSpeichern($con, "FilialSuche", $PHP_AUTH_USER, $ParamListe);
}

$rsWMBloecke = awisOpenRecordset($con, "SELECT * FROM WebMaskenBloecke, WebMaskenRegisterBloecke, WEBMASKENREGISTER, WebMaskenBloeckeFelder WHERE XRG_ID = XRB_XRG_ID AND XRB_XWB_Key = XWB_Key AND XBF_XWB_KEY=XWB_KEY");  
$rsWMBLoeckeZeilen = $awisRSZeilen;
for($i=0;$i<$rsWMBLoeckeZeilen;$i++)
{
	$AlterXBF_XWB_KEY = $rsWMBloecke["XBF_XWB_KEY"][$i];
	
	print "<h3>" . $rsWMBloecke["XWB_UEBERSCHRIFT"][$i] . "</h3>";

	print $rsWMBloecke["XWB_HTML_START"][$i];

	$Bedingung = "FIL_ID=0" . $txtFIL_ID;
	
	$SQL = 	$rsWMBloecke["XWB_SQL"][$i] . " WHERE " . $Bedingung;
	$rsDaten = awisOpenRecordset($con, $SQL)	;

	while($AlterXBF_XWB_KEY==$rsWMBloecke["XBF_XWB_KEY"][$i])	
	{
		print $rsWMBloecke["XBF_HTML_START"][$i];

		print $rsDaten[$rsWMBloecke["XBF_FELDNAME"][$i]][0];

		print $rsWMBloecke["XBF_HTML_ENDE"][$i];	
		++$i;
	}
	
	print $rsWMBloecke["XWB_HTML_ENDE"][$i];

}

print "</form>";

?>
