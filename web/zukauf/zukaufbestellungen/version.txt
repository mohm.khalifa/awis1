#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Zukauf-Bestellungen
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.03.00;Umstellung auf die neue Oberfl&auml;che;16.02.2016;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.02.00;Unterstuetung fuer auslaendische Preise bei ATU Referenzartikel;02.08.2012;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.01.01;Anlegen von Bestellungen;12.05.2009;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.01.00;Neues Framework und Umorganisation der Daten.;20.02.2009;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.06;Doppelte Datens�tze bei mehreren gleichen Werkstattauftr�gen unterdr�ckt.;18.03.2008;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.05;Suche nach Werkstattauftr�gen erm�glicht;04.03.2008;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.04;Sonderanfragen f�r Auftr�ge;27.11.2007;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.03;Zusatzinfo mit letzter Bestellung PV/TROST;20.11.2007;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.02;Speicherfehler beseitigt und Filiale in den Bestellungen anzeigen;07.11.2007;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>
1.00.01;Erste Version;01.10.2007;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>

