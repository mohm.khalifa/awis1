<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once '../hrtools/bewerberverwaltung/bewerberverwaltung_Details_Bewerbungen_Funktionen.php';

if (isset($_GET['sucFilialnummer'])) {
    erstelleAjax($_REQUEST['Feldname'], ($_GET['sucFilialnummer'] != ''?$_GET['sucFilialnummer']:0));
} else {
    echo 'Fehler beim Abrufen der Daten:' . $ex->getMessage();
}
?>