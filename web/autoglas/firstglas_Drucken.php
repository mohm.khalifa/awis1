<?php

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

if(!isset($_GET['FGK_KEY']))
{
	die();
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('FGK','%');
$TextKonserven[]=array('FKH','%');
$TextKonserven[]=array('FGP','%');
$TextKonserven[]=array('FPH','%');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 20;

if(isset($_GET['FGK_KEY']))
{
    $FGK_KEY = base64_decode($_GET['FGK_KEY']);

    $SQL = 'Select FGKOPFDATEN.*,FGPOSITIONSDATEN.*,FILIALEN.* FROM FGKOPFDATEN INNER JOIN FGPOSITIONSDATEN ON FGK_VORGANGNR = FGP_VORGANGNR inner join FILIALEN ON FGK_FILID = FIL_ID AND FGK_KEY='.$FGK_KEY;
    $rsFGK = $DB->RecordSetOeffnen($SQL);

    $Ausdruck = new awisAusdruck('P','A4',array('BriefpapierATU_DE_Seite_2.pdf'));
    $Ausdruck->NeueSeite(0,1);

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','B',16);
    $Ausdruck->_pdf->cell(65,6,$AWISSprachKonserven['FGK']['txt_Auftragsbestaetigung'],0,0,'L',0);
    $Ausdruck->_pdf->cell(65,6,"ATU".$rsFGK->FeldInhalt('FGK_VORGANGNR'),0,0,'L',0);
    
    $Zeile = 50;

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(65,6,'FILIALE '.$rsFGK->FeldInhalt('FGK_FILID'),'LTR',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','B',20);
    $Ausdruck->_pdf->cell(125,6,'NACHDRUCK','',0,'L',0);

    $Ausdruck->_pdf->SetFont('Arial','',10);

    //Nachdruck
    //$Ausdruck->_pdf->cell(65,6,$rsFGK->FeldInhalt('FIL_BEZ'),'LTR',0,'L',0);
    //$Zeile +=1;
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,$rsFGK->FeldInhalt('FIL_BEZ'),'LR',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,$rsFGK->FeldInhalt('FIL_STRASSE'),'LR',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','LR',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,$rsFGK->FeldInhalt('FIL_PLZ').' '.$rsFGK->FeldInhalt('FIL_ORT'),'LR',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Montagedatum = substr($rsFGK->FeldInhalt('FGK_MONTAGEDATUM'),0,10);
    $Ausdruck->_pdf->cell(125,6,$AWISSprachKonserven['FGK']['txt_MONTAGE'].chr(9).$Montagedatum,'',0,'L',0);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','LBR',0,'L',0);
    $Zeile+=5;

$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_Sachbearbeiter'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_Bearbeiter'].':',0,0,'L',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_Monteur'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_Halter'].':'.chr(9).$rsFGK->FeldInhalt('FGK_KUNDENNAME'),0,0,'L',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_Kennzeichen'].':'.chr(9).$rsFGK->FeldInhalt('FGK_KFZKENNZ'),0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Versicherungsname = substr($rsFGK->FeldInhalt('FGK_VERSICHERUNG'), 0,25);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_Versicherung'].':'.chr(9).$Versicherungsname,0,0,'L',0);

$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_FIN'].':'.chr(9).$rsFGK->FeldInhalt('FGK_FAHRGESTELLNR'),0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_Versschein'].':'.chr(9).$rsFGK->FeldInhalt('FGK_VERSSCHEINNR'),0,0,'L',0);
$Zeile+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_KBANR'].':'.chr(9).$rsFGK->FeldInhalt('FGK_KBANR'),0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_SELBSTBETEILIGUNG'].':'.chr(9).$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('FGK_SB')),0,0,'L',0);
$Zeile+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_KM'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Fahrzeug = substr($rsFGK->FeldInhalt('FGK_KFZBEZ'), 0,24);
$Ausdruck->_pdf->cell(80,6,$AWISSprachKonserven['FGK']['txt_Fahrzeug'].':'.chr(9).$Fahrzeug,0,0,'L',0);
$Zeile+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(100,6,$AWISSprachKonserven['FGK']['txt_Schadentag'].':',0,0,'L',0);

$Zeile+=20;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',16);
$Ausdruck->_pdf->cell(180,6,'Hinweis zur Versicherungs-Abfrage','LTRB',0,'L',0);

$Zeile+=10;

$Ausdruck->_pdf->SetFillColor(200,200,200);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['FGK']['txt_ATUNR'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(60,6,$AWISSprachKonserven['FGK']['txt_BEZEICHNUNG'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['FGK']['txt_MENGE'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['FGK']['txt_EINHEIT'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['FGK']['txt_OEMPREIS'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['FGK']['txt_SUMMEOEM'],'LTRB',0,'L',1);
$Zeile+=5;



$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->SetFillColor(255,255,255);

$Steuer = 0.0;
$MengeOEM = 0.0;
$BetragOEM = 0.0;
$ErgebnisOEM = 0.0;
$SummeOEM = 0.0;
$OemPreisMwst = 0.0;
$OemPreisBrutto = 0.0;

while(!$rsFGK->EOF())
{
        $Steuer = $rsFGK->FeldInhalt('FGK_STEUERSATZ');
        $MengeOEM = 0.0;
        $BetragOEM = 0.0;
        $ErgebnisOEM = 0.0;

	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell(23,6,$rsFGK->FeldInhalt('FGP_ARTNR'),'LTRB',0,'L',1);
	$Ausdruck->_pdf->cell(60,6,$rsFGK->FeldInhalt('FGP_ARTBEZ'),'LTRB',0,'L',1);
	$Ausdruck->_pdf->cell(23,6,$rsFGK->FeldInhalt('FGP_ANZAHL'),'LTRB',0,'L',1);
	$MengeOEM = $rsFGK->FeldInhalt('FGP_ANZAHL');
        $MengeOEM = str_replace(',', '.', $MengeOEM);
        $Ausdruck->_pdf->cell(23,6,$rsFGK->FeldInhalt('FGP_EINHEIT'),'LTRB',0,'L',1);
       
        $BetragOEM = $DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('FGP_OEMPREIS'));
        
        $BetragOEM = str_replace('.', ',', $BetragOEM);
        $Ausdruck->_pdf->cell(23,6,$BetragOEM,'LTRB',0,'L',1);
        $BetragOEM = str_replace(',', '.', $BetragOEM);

        $ErgebnisOEM = $MengeOEM * $BetragOEM;

        //$ErgebnisOEM = number_format($ErgebnisOEM, 2);
        $ErgebnisOEM = round($ErgebnisOEM, 2);
        $SummeOEM = $SummeOEM + $ErgebnisOEM;

        $ErgebnisOEM = str_replace('.', ',', $ErgebnisOEM);
        $Ausdruck->_pdf->cell(23,6,$ErgebnisOEM,'LTRB',0,'L',1);

        $Zeile+=5;
	$rsFGK->DSWeiter();
}

$Zeile+=5;


$SummeOEM = str_replace('.',',',$SummeOEM);

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(60,6,$AWISSprachKonserven['FGK']['txt_SUMMENETTO'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);

$Ausdruck->_pdf->cell(23,6,$SummeOEM,'LTRB',0,'L',1);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(60,6,$Steuer.$AWISSprachKonserven['FGK']['txt_MWST'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);


$SummeOEM = str_replace(',','.',$SummeOEM);

$OemPreisMwst= (($SummeOEM / 100) * $Steuer);

$OemPreisMwst = round($OemPreisMwst,2);
$OemPreisMwst = str_replace('.',',',$OemPreisMwst);

$Ausdruck->_pdf->cell(23,6,$OemPreisMwst,'LTRB',0,'L',1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(60,6,$AWISSprachKonserven['FGK']['txt_SUMMEBRUTTO'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);
$Ausdruck->_pdf->cell(23,6,'-----------','LTRB',0,'L',1);

$OemPreisMwst = str_replace(',','.',$OemPreisMwst);
$OemPreisBrutto = $SummeOEM + $OemPreisMwst;

$OemPreisBrutto = round($OemPreisBrutto,2);
$OemPreisBrutto = str_replace('.',',',$OemPreisBrutto);

$Ausdruck->_pdf->cell(23,6,$OemPreisBrutto,'LTRB',0,'L',1);
//$Ausdruck->_pdf->cell(23,6,$DB->FeldInhaltFormat('N2',$OemPreisBrutto),'LTRB',0,'L',1);

$Ausdruck->Anzeigen();

}

?>