<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('CWV','CWV_KON_KEY');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[] = array('CAD', '%');
$TextKonserven[] = array('CAQ', '%');
$TextKonserven[]=array('Wort','Seite');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3716 = awisBenutzerRecht($con,3716,$AWISBenutzer->BenutzerName());
if($Recht3716==0)
{
	awisEreignis(3,1000,'CRM-Aenderungen',$AWISBenutzer->BenutzerName(),'','','');
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Kopf (Auswahl Betreuer)

awis_FORM_FormularStart();
echo '<form name=frmAenderungen method=post action=./crm_Main.php?cmdAktion=Aenderungen>';

$Param = explode(';',awis_BenutzerParameter($con, 'CRMAenderungen', $AWISBenutzer->BenutzerName()));

/*
 * $Param[0] = CAD_KON_KEY -> Betreuer
 * $Param[2] = ORDER BY Feld
 * $Param[3] = Block
 */

if(isset($_POST['cmdSuche_x']))
{
	$Param[0] = $_POST['sucSucheBetreuer'];
}
elseif(isset($_GET['CAD_KON_KEY']))
{
	$Param[0]=$_GET['CAD_KON_KEY'];	
}

awis_FORM_ZeileStart();

// Betreuer selektieren und anzeigen

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_KON_KEY'].'',150);

$SQL = "SELECT DISTINCT COALESCE(kon_key,-1) as id, ";
$SQL .= "COALESCE(TRIM(kon_name1) || ' ' || TRIM(kon_name2), '::ohne Betreuer::') AS cad_betreuer ";
$SQL .= "FROM crmadressen INNER JOIN crmakquisestand ";
$SQL .= "ON cad_key = caq_cad_key ";
$SQL .= "INNER JOIN kontakte ";
$SQL .= "ON cad_kon_key = kon_key ";
$SQL .= "INNER JOIN ";
$SQL .= "(SELECT DISTINCT cin_cad_key ";
$SQL .= "FROM crminfos ";
$SQL .= "WHERE cin_cit_id = 4004) a ";
$SQL .= "ON cad_key = cin_cad_key ";
$SQL .= "WHERE cad_status = 'K' ";
$SQL .= "ORDER BY cad_betreuer";

awis_FORM_Erstelle_SelectFeld('*SucheBetreuer',(isset($Param[0])?$Param[0]:''),200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_SchaltflaechenStart();
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
awis_FORM_SchaltflaechenEnde();

echo '</form>';

// Ende Kopf

awis_FORM_Trennzeile();

//***********************************
//Kontakte anzeigen
//***********************************
$CADBetreuerID = '';
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$sqlWhere = '';

$SQL = "SELECT cad_key,cad_name1, cad_name2, cad_strasse, cad_plz, cad_ort, ";
$SQL .= "caq_vertragsnummer, cad_hausnummer, cad_lan_code, kon_name1, ";
$SQL .= "(SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CIT_ID = 1013 AND CIN_CAD_KEY = CAD_KEY AND ROWNUM = 1) AS CIN_VERTRAGSLAND ";
$SQL .= "FROM crmadressen INNER JOIN crmakquisestand ON cad_key = caq_cad_key ";
$SQL .= "INNER JOIN (SELECT DISTINCT cin_cad_key FROM crminfos WHERE cin_cit_id = 4004 AND cin_wert = 'INFO_VERTRAGSSTATUS') ON cad_key = cin_cad_key ";
$SQL .= "INNER JOIN kontakte ON cad_kon_key = kon_key ";

if(isset($Param[0]) && $Param[0]=='-1')
{
	$sqlWhere .= ' AND cad_kon_key IS NULL';
	$CGKBetreuerID = $Param[0];
}
else
{
	$sqlWhere .= ' AND cad_kon_key is not null';
}

if(isset($Param[0]) AND $Param[0] != '')
{
	$sqlWhere .= ' AND cad_kon_key = 0'.intval($Param[0]);
}

$SQL .= ' WHERE' . substr($sqlWhere, 4);

if(!isset($_GET['Sort']))
{
	if(isset($Param[2]) AND $Param[2]!='')
	{
		$SQL .= ' ORDER BY '.$Param[2];
	}
	else
	{
		$SQL .= ' ORDER BY CAD_NAME1 DESC';
		$Param[2]='CAD_NAME1';
	}
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	$Param[2]=str_replace('~',' DESC ',$_GET['Sort']);
}

$rsCAD = awisOpenRecordset($con,$SQL);
$rsCADZeilen = $awisRSZeilen;

$Link = './crm_Main.php?cmdAktion=Aenderungen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
$Link .= '&Sort=CAD_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_NAME1')) ? '~' : '');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_NAME1'] . '/' . $AWISSprachKonserven['CAD']['CAD_NAME2'], 400, '', $Link);

awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_STRASSE'], 220);

$Link = './crm_Main.php?cmdAktion=Aenderungen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
$Link .= '&Sort=CAD_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_PLZ')) ? '~' : '');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_PLZ'], 100, '', $Link);

$Link = './crm_Main.php?cmdAktion=Aenderungen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
$Link .= '&Sort=CAD_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_ORT')) ? '~' : '');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_ORT'], 200, '', $Link);

$Link = './crm_Main.php?cmdAktion=Aenderungen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
$Link .= '&Sort=KON_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KON_NAME1')) ? '~' : '');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_KON_KEY'], 120, '', $Link);

$Link = './crm_Main.php?cmdAktion=Aenderungen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
$Link .= '&Sort=CAQ_VERTRAGSNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAQ_VERTRAGSNUMMER')) ? '~' : '');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAQ']['CAQ_VERTRAGSNUMMER'], 140, '', $Link);
awis_FORM_ZeileEnde();

// Block -> Seiten zum blaettern Ausgeben
$StartZeile=0;
if(isset($_GET['Block']))
{
	$Param[3] = $_GET['Block'];
	$StartZeile = intval($Param[3])*$MaxDSAnzahl;
}
else
{
    $Param[3] = 0;
}

// Seitenweises blaettern
if($rsCADZeilen>$MaxDSAnzahl)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

	for($i=0;$i<($rsCADZeilen/$MaxDSAnzahl);$i++)
	{
		if($i!=($StartZeile/$MaxDSAnzahl))
		{
			$Text = '&nbsp;<a href=./crm_Main.php?cmdAktion=Aenderungen'.($Param[0] !=''?'&CAD_KON_KEY='.$Param[0]:'').'&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
		else
		{
		$Text = '&nbsp;'.($i+1).'';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
	}
	
	awis_FORM_ZeileEnde();
}

for ($ADRZeile = $StartZeile; $ADRZeile < $rsCADZeilen and $ADRZeile < $StartZeile + $MaxDSAnzahl; $ADRZeile++)
{
	awis_FORM_ZeileStart();
	$Link = './crm_Main.php?cmdAktion=Details&&CAD_KEY=' . $rsCAD['CAD_KEY'][$ADRZeile] . '&VonSeite=Aenderung&Block='.$Param[3];
	awis_FORM_Erstelle_ListenFeld('CAD_NAME1', $rsCAD['CAD_NAME1'][$ADRZeile] . ($rsCAD['CAD_NAME2'][$ADRZeile] == '' ? '' : ' ' . $rsCAD['CAD_NAME2'][$ADRZeile]), 0, 400, false, ($ADRZeile % 2), '', $Link);
	awis_FORM_Erstelle_ListenFeld('CAD_STRASSE', $rsCAD['CAD_STRASSE'][$ADRZeile] . ' ' . $rsCAD['CAD_HAUSNUMMER'][$ADRZeile], 0, 220, false, ($ADRZeile % 2), '', '', 'T', '', '');
	awis_FORM_Erstelle_ListenFeld('CAD_PLZ', $rsCAD['CAD_LAN_CODE'][$ADRZeile] . '-' . $rsCAD['CAD_PLZ'][$ADRZeile], 0, 100, false, ($ADRZeile % 2), '', '', 'T', '', '');
	awis_FORM_Erstelle_ListenFeld('CAD_ORT', $rsCAD['CAD_ORT'][$ADRZeile], 0, 200, false, ($ADRZeile % 2), '', '', 'T', '', '');
	awis_FORM_Erstelle_ListenFeld('KON_NAME1', $rsCAD['KON_NAME1'][$ADRZeile], 0, 120, false, ($ADRZeile % 2), '', '');
	awis_FORM_Erstelle_ListenFeld('CAQ_VERTRAGSNUMMER', (($rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] != 'DE' AND $rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] != '') ? $rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] . '-' : '') . $rsCAD['CAQ_VERTRAGSNUMMER'][$ADRZeile], 0, 140, false, ($ADRZeile % 2), '', $Link);
	awis_FORM_ZeileEnde();
}
awis_BenutzerParameterSpeichern($con,'CRMAenderungen',$AWISBenutzer->BenutzerName(),implode(';',$Param));

?>