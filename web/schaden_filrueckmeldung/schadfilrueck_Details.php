<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$Fehler = 0;

	
	$AWISBenutzer = awisBenutzer::Init();

	$script = "<script type='text/javascript'>	
			
	function ladeSelectTitles(pName, pForm) {
		var sel = pForm.elements[pName];
		sel.title = sel.options[sel.selectedIndex].title;
	}
	</script>";

	echo $script;


	$script = "<script type='text/javascript'>
	
	function textareaAuf(obj) {
			 
    	     obj.rows ='10';
	}
	
	function textareaZu(obj) {
	
    	     obj.rows ='1';
	}
		
	function aktuellesDatum(obj) {
			 var heute = new Date();
			
    	     obj.value=heute.getDate()+'.'+('0' + (heute.getMonth()+1)).slice(-2)+'.' + heute.getFullYear();
	}
	
	function VorgangUebernehmen()
            {
	            if (document.getElementsByName('txtBES_BEARBEITER')[0].value != '::bitte w�hlen::')
                {
	               document.getElementsByName('sucBES_BEARBEITER_AB')[0].value = document.getElementsByName('txtBES_BEARBEITER')[0].value;
				 
                
                }
                else
                {
                     document.getElementsByName('sucBES_BEARBEITER_AB')[0].value = '';
                }
            }
            
     function pflichtAus() {
		document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].required = false;  // required aus
		document.getElementsByName('txtGDN_SATZARTEN')[0].required = false;  // required aus
		document.getElementsByName('txtGDF_KEY')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_INDEX')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_FELDTYP')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_FELDTYPWERT')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_FELDTYPWERT2')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_FELDTYPWERT3')[0].required = false;  // required aus
		document.getElementsByName('txtGDV_FELDTYPWERT4')[0].required = false;  // required aus
	}
	
	</script>";

	echo $script;

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('SCHAD');
	$DB->Oeffnen();

	$DB_AWIS = awisDatenbank::NeueVerbindung('AWIS');
	$DB_AWIS->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht40015 = $AWISBenutzer->HatDasRecht(40015);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBR'));

	$Save = 0;
	$FehlerFest = false;	
	
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	

	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'BEARBNRNEU';
	}

	if(isset($_GET['BEARBEITUNGSNR']))
	{
		$AWIS_KEY1 = $_GET['BEARBEITUNGSNR'];
	}

	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
	 $Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40015==0)
	{
		$Form->Fehler_KeineRechte();
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$Param['SBR_FIL_ID']=$Form->Format('N0',$_POST['txtSBR_FIL_ID'],true);
		}
		else 
		{
			if(isset($_POST['sucSBR_FIL_ID'])) {
				$Param['SBR_FIL_ID'] = $Form->Format('N0', $_POST['sucSBR_FIL_ID'], true);
			}
			$Param['FIL_GEBIET'] = $Form->Format('N0',$_POST['sucFIL_GEBIET'],true);
		}
		$Param['SBR_BEARB']=$Form->Format('N0', $_POST['sucSBR_BEARBNR'], true);
		$Param['BES_STATUS']=$Form->Format('T', $_POST['sucBES_VORGANG'], true);
		$Param['BES_AART']=isset($_POST['sucBES_AART'])?$Form->Format('N0',$_POST['sucBES_AART'],true):'';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
			include('./schadfilrueck_speichern.php');
			$Form->Hinweistext($meldung);
			$AWIS_KEY1 = $_POST['txtBEARBEITUNGSNR'];

	}
	elseif($FelderDel != '' or isset($_POST['cmdLoeschenOK']))
	{
		include('./qualitaetsmaengel_loeschen.php');
	}
	

	if(isset($_POST['cmdHinzufuegen_x']))
	{
		$Fehler = 0;

		if(ctype_digit($_POST['txtBES_PERSONAL'])){
			$SQL = "SELECT * ";
			$SQL .= "FROM PERSONAL_KOMPLETT ";
			$SQL .= "WHERE PERSNR = '".$_POST['txtBES_PERSONAL']."'";

			$rsPep = $DB_AWIS->RecordSetOeffnen($SQL);

			if (($rsPep->AnzahlDatensaetze() == 0))
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BES']['ERR_BEMERKUNG']);
				$Form->ZeileEnde();
				$Fehler++;
			}
		}
		else
		{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BES']['ERR_BEMERKUNG_NN']);
				$Form->ZeileEnde();
				$Fehler++;
		}

		if($Fehler == 0)
		{
			include('./schadfilrueck_speichern.php');
			$Form->Hinweistext($meldung);
		}

		$AWIS_KEY1 = $_POST['txtBEARBEITUNGSNR'];

	}

	$Bedingung = '';
	$SQL = 'select ANZEIGEFIL,BEARBNRNEU, ART, to_clob(FEHLERBESCHREIBUNG) AS REKL_TEXT, FILNR, VERURS_FILIALE, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ';
	$SQL .= "DIAGNOSE_WERKSTATTLEITER, DIAGNOSE_TKDL,REKLASCHEINNR,REKLASCHEINVAX,EINGABEAM,DATUMEREIGNIS,BEARBEITUNGSNRFIL,GEWICHTUNG,FABRIKAT,TYP,KFZTYP,ERSTZULASSUNG,KM,BEZEICHNUNG,MECHANIKER,AUFTRAGSDATUM,DATUMREGULIERUNG,BONNRREGULIERUNG,FORDERUNG,WEITERGELEITET_HERSTELLER, ";
	$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, GROSSKDNR, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM,BESICHTIGT,KBANR,KAUFDATUM,BONNR,LIEFERANT,LIEFARTNR,VORGANGSNR,FGLIEFERANT,FGMARKE,ZUSATZINFO,FEHLERBESCHREIBUNG,BEMINTERN,ANWALTSSACHE,WEITERGELEITETVERSICHERUNG, ";
	$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL, EINGABEDURCH_FILPERSNR, ATUNR, WERT,SBNAME,ORTEREIGNIS,ANTRAGART,AKTENZEICHENRA,RAFORDERUNG,STANDBEIGERICHT,AKTENZEITENGERICHT, ";
	$SQL .='row_number() OVER (';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= "SELECT * ";
	$SQL .= "FROM SCHAEDEN_NEW ";
	$SQL .= "INNER JOIN ANTRAGART AA ON ANTRAGART = AA.ID ";
	$SQL .= "LEFT JOIN SACHBEARBEITER SB ON BEARBEITER = SB.ID) ";

	if($AWIS_KEY1 != '')
	{
		$Bedingung .= ' AND bearbeitungsnr='.$AWIS_KEY1;
	}

	/* Wird nicht mehr ben�tigt da Filialflag
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$Bedingung.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";
		$Bedingung.= " AND ANTRAGART in (10,3,18,17)";
	}
    */

	$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}

	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	if($AWIS_KEY1 == '') {
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsErgebnis = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht

	$Form->SchreibeHTMLCode('<form name="frmSBRDetails" action="./schadfilrueck_Main.php?cmdAktion=Details" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();

	if (($rsErgebnis->AnzahlDatensaetze() > 1))
	{
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		$Gesamtbreite = '';
		// Spaltenbreiten f�r Listenansicht
		$FeldBreiten = array();
		$FeldBreiten['BEARBEITUNG'] = 150;
		$FeldBreiten['AART'] = 100;
		$FeldBreiten['KUNDE'] = 120;
		$FeldBreiten['STRASSE'] = 100;
		$FeldBreiten['PLZ'] = 100;
		$FeldBreiten['ORT'] = 100;

		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
		{
			$FeldBreiten['FILNR'] = 100;
			$FeldBreiten['TELEFON'] = 100;
			$FeldBreiten['EMAIL'] = 100;
			$FeldBreiten['CARD'] = 100;
		}

		foreach ($FeldBreiten as $value)
		{
			$Gesamtbreite += $value;
		}


		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
		//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
		// �berschrift der Listenansicht mit Sortierungslink: Filial;
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=BEARBEITUNGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEARBEITUNGSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEARB'], $FeldBreiten['BEARBEITUNG'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WERT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AART'], $FeldBreiten['AART'], '', $Link);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
		{
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=FILNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FILNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_FILNR'], $FeldBreiten['FILNR'], '', $Link);
		}
		// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=KUNDENNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KUNDENNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KUNDE'], $FeldBreiten['KUNDE'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Nachname
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=STRASSE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='STRASSE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_STRASSE'], $FeldBreiten['STRASSE'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Nachname
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PLZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_PLZ'], $FeldBreiten['PLZ'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Nachname
		$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ORT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ORT'], $FeldBreiten['ORT'], '', $Link);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
		{
			$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=TELEFON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TELFON'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_TELEFON'], $FeldBreiten['TELEFON'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=EMAIL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='EMAIL'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EMAIL'], $FeldBreiten['EMAIL'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './schadfilrueck_Main.php?cmdAktion=Details&Sort=PAN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PAN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_CARD'], $FeldBreiten['CARD'], '', $Link);
		}
		$Form->ZeileEnde();

		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsErgebnis->EOF())
		{
			if($rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') != '' )
			{
				$Geschl = 'color:blue';
			}
			else
			{
				$Geschl = '';
			}

			$Form->ZeileStart();
			$Link = '"./schadfilrueck_Main.php?cmdAktion=Details&BEARBEITUNGSNR=0'.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'"'.'style="'.$Geschl.'"';
			$TTT = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
			$Form->Erstelle_ListenFeld('BES_BEARBEITUNG', $rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['BEARBEITUNG'], false,($DS%2), '',$Link, 'T', 'L', $TTT);
			$TTT =  $rsErgebnis->FeldInhalt('WERT');
			$Form->Erstelle_ListenFeld('BES_ART',$rsErgebnis->FeldInhalt('WERT'), 0, $FeldBreiten['AART'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='') {
				$TTT = $rsErgebnis->FeldInhalt('FILNR');
				$Form->Erstelle_ListenFeld('BES_FILNR', $rsErgebnis->FeldInhalt('FILNR'), 0, $FeldBreiten['FILNR'], false, ($DS % 2), $Geschl, '', 'T', 'L', $TTT);
			}
			$TTT = $rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME');
			$Form->Erstelle_ListenFeld('BES_KUNDE',$rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME'), 0, $FeldBreiten['KUNDE'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			$TTT = $rsErgebnis->FeldInhalt('STRASSE');
			$Form->Erstelle_ListenFeld('BES_STRASSE',$rsErgebnis->FeldInhalt('STRASSE'), 0, $FeldBreiten['STRASSE'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			$TTT = $rsErgebnis->FeldInhalt('PLZ');
			$Form->Erstelle_ListenFeld('BES_PLZ',$rsErgebnis->FeldInhalt('PLZ'), 0, $FeldBreiten['PLZ'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			$TTT = $rsErgebnis->FeldInhalt('ORT');
			$Form->Erstelle_ListenFeld('BES_ORT',$rsErgebnis->FeldInhalt('ORT'), 0, $FeldBreiten['ORT'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
			{
				$TTT =  $rsErgebnis->FeldInhalt('TELEFON');
				$Form->Erstelle_ListenFeld('BES_TELEFON',$rsErgebnis->FeldInhalt('TELEFON'), 0, $FeldBreiten['TELEFON'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
				$TTT =  $rsErgebnis->FeldInhalt('EMAIL');
				$Form->Erstelle_ListenFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('EMAIL'), 0, $FeldBreiten['EMAIL'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
				$TTT =  $rsErgebnis->FeldInhalt('PAN');
				$Form->Erstelle_ListenFeld('BES_CARDNR',$rsErgebnis->FeldInhalt('PAN'), 0, $FeldBreiten['EMAIL'], false,($DS%2),$Geschl,'', 'T', 'L', $TTT);
			}

			$Form->ZeileEnde();
			$DS++;
			$rsErgebnis->DSWeiter();
		}

		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsErgebnis->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './schadfilrueck_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();

	}
	else if (($rsErgebnis->AnzahlDatensaetze() == 1)){
		if($AWIS_KEY1 == '')
		{
			$AWIS_KEY1 = '0'.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		}

		$SQL2 = 'select * from(';
		$SQL2 .=  'select historie_kennung,historie_datum ';
		$SQL2 .= 'from historie';
		$SQL2 .= ' where historie_bearbnr ='.$AWIS_KEY1;
		$SQL2 .= ' order by historie_datum desc)';
		$SQL2 .= ' where rownum = 1';
		$rsHist = $DB->RecordSetOeffnen($SQL2);

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schadfilrueck_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_KENNUNG'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_DATUM'));
		//$Form->ZeileStart();
		$Form->InfoZeile($Felder,'');
		//$Form->ZeileEnde();


		$Frame1 = true;
		$Frame2 = true;
		$Frame3 = true;
		$Frame4 = true;
		$Frame5 = true;

		if((($Recht40015&8)==8) and $rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') == '') {
			$aendern = true;
		} else {
			$aendern = false;
		}

		$Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_ALLGMEIN'], true,0, '', '', false, [], true, 'overflow: scroll');

		//PG: Wenn �ber die Schnellsuche gesucht wurde, wurden die alten Werte vom letzten Vorgang in die Felder zur�ckgeschrieben, was zu enormen Fehler gef�hrt hat.
		if (isset($_POST['cmdSuche_x']))
		{
			unset($_POST);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_BEARBNR',$rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 10, 289,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANTRAGART'] . ':',140,'font-weight:bolder');
		$SQL2 ='select ID,WERT from ANTRAGART ';
		$Form->Erstelle_SelectFeld('BES_AART',$rsErgebnis->FeldInhalt('ANTRAGART'),260,false,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel( '',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('XXX_ZEILE1','', 73, 190,false, '','', '','', 'L','','',30,'','','autocomplete=off');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_FIL',$rsErgebnis->FeldInhalt('FILNR'), 10, 40,false, '','', '','', 'L');


		if($AWIS_KEY1 != '-1')
		{
			$AWISCursorPosition = 'sucBES_FIL';
		}

		$Inhalt = '';
		$FIL_ID = '';


		$SQL  = ' Select * from( ';
		$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
		$SQL .= ' where fil_gruppe IS NOT NULL OR';
		$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
		$SQL .= ' union';
		$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
		$SQL .= ' FROM schaddev09.pseudofilialen)';
		$SQL .= ' where fil_id =0'.$rsErgebnis->FeldInhalt('FILNR');

		$rsFIL = $DB->RecordSetOeffnen($SQL);


		$Form->Erstelle_TextFeld('BES_FILILAE',$rsFIL->FeldInhalt('FIL_BEZ'), 50,250, false, '', '', '', 'T', 'L','','',50);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGABEAM'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('!BES_EINGABEAM',$AWIS_KEY1 == '-1'?date('d.m.Y',time()):$rsErgebnis->FeldInhalt('EINGABEAM'), 10, 260,false, '','', '','D', 'L','','',10,'','','autocomplete=off');

		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGANGPER'] . ':',120,'font-weight:bolder');
		$SQL2 ='select ID,MITTEL from eingangper ';
		$Form->Erstelle_SelectFeld('!BES_EINGANGPER',$rsErgebnis->FeldInhalt('EINGANGPER'),190,false,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUFIL'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_VUFIL',$rsErgebnis->FeldInhalt('VERURS_FILIALE'), 10, 40,false, '','', '','', 'L');


		$SQL  = ' Select * from( ';
		$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
		$SQL .= ' where fil_gruppe IS NOT NULL OR';
		$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
		$SQL .= ' union';
		$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
		$SQL .= ' FROM schaddev09.pseudofilialen)';
		$SQL .= ' where fil_id =0'.$rsErgebnis->FeldInhalt('VERURS_FILIALE');

		$rsFIL = $DB->RecordSetOeffnen($SQL);

		$SQL2  = 'select * from v_filialebenenrollen_aktuell';
		$SQL2  .= ' inner join kontakte on xx1_kon_key = kon_key';
		$SQL2  .= ' where xx1_fil_id='.$rsErgebnis->FeldInhalt('VERURS_FILIALE');
		$SQL2  .= ' and xx1_fer_key = 25';

		$rsGBL = $DB_AWIS->RecordSetOeffnen($SQL2);

		$Form->Erstelle_TextFeld('BES_VUFILILAE',$rsFIL->FeldInhalt('FIL_BEZ'), 50,250, false, '', '', '', 'T', 'L','','',50);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GESCHLAM'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_GESCHLAM',isset($_POST['txtBES_GESCHLAM'])?$_POST['txtBES_GESCHLAM']:$rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM'), 10, 260,false, '','', '','D', 'L','','',10,'ondblclick="aktuellesDatum(this)";','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SB'] . ':',120,'font-weight:bolder');
		$SQL2 ='select ID,SBNAME from SACHBEARBEITER where ausgeschieden = 0 order by SBNAME';
		$Form->Erstelle_SelectFeld('!BES_BEARBEITER',$rsErgebnis->FeldInhalt('SACHBEARBEITER'),190,false,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','onchange="VorgangUebernehmen();key_BES_BEARBEITER_AB(event)"','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		$Form->FormularBereichEnde();

		$Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_KUNDEKFZ'], true,0, '', '', false, [], true, 'overflow: scroll');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANREDE'] . ':',140,'font-weight:bolder');
		$SQL5 ='select ID,NAME from anrede ';
		$Form->Erstelle_SelectFeld('BES_ANREDE',$rsErgebnis->FeldInhalt('GESCHLECHT'),290,false,$SQL5,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EMAIL'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('EMAIL'), 25, 260,false, '','', '','', 'L','','',50,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FABRIKAT'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_FABRIKAT',$rsErgebnis->FeldInhalt('FABRIKAT'), 73, 190,false, '','', '','', 'L','','',30,'','','autocomplete=off');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_NAME'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('!BES_NAME',$rsErgebnis->FeldInhalt('KUNDENNAME'), 25, 289,false, '','', '','', 'L','','',50,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GROSSKDNR'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_GROSSKDNR',$rsErgebnis->FeldInhalt('GROSSKDNR'), 25, 261,false, '','', '','', 'L','','','','','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_MODELL'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_MODELL',$rsErgebnis->FeldInhalt('TYP'), 73, 190,false, '','', '','', 'L','','',30,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORNAME'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_VORNAME',$rsErgebnis->FeldInhalt('VORNAME'), 25, 290,false, '','', '','', 'L','','',50,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FAX'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_FAX',$rsErgebnis->FeldInhalt('FAX'), 73, 260,false, '','', '','', 'L','','',25,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TYP'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_TYP',$rsErgebnis->FeldInhalt('KFZTYP'), 73, 190,false, '','', '','', 'L','','',60,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STRASSE'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_STRASSE',$rsErgebnis->FeldInhalt('STRASSE'), 25, 290,false, '','', '','', 'L','','',60,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ATUCARDNR'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_PAN',$rsErgebnis->FeldInhalt('PAN'), 73, 260,false, '','', '','', 'L','','',24,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KBA'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_KBA',$rsErgebnis->FeldInhalt('KBANR'), 73, 190,false, '','', '','', 'L','','',15);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PLZORT'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_PLZ',$rsErgebnis->FeldInhalt('PLZ'), 3, 55,false, '','', '','', 'L','','',8,'','','autocomplete=off');
		$Form->Erstelle_TextFeld('BES_ORT',$rsErgebnis->FeldInhalt('ORT'), 25, 635,false, '','', '','', 'L','','',50,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EZ'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_EZ',$rsErgebnis->FeldInhalt('ERSTZULASSUNG'),10, 190,false, '','', '','D', 'L','','',10,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_TELEFON',$rsErgebnis->FeldInhalt('TELEFON'), 25, 690,false, '','', '','', 'L','','',35,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KM'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_KM',$rsErgebnis->FeldInhalt('KM'), 73, 190,false, '','', '','', 'L','','',10,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON2'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_TELEFON2',$rsErgebnis->FeldInhalt('TELEFON2'), 25, 690,false, '','', '','', 'L','','',35,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KFZKZ'] . ':',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_KENNZ',$rsErgebnis->FeldInhalt('KFZ_KENNZ'), 73, 190,false, '','', '','', 'L','','',11,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		$Form->FormularBereichEnde();

		$Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_AUFTRAG'], true,0, '', '', false, [], true, 'overflow: scroll');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNG'] . ':',140,'font-weight:bolder');
		$SQL6 ='select KENNUNG,KENNUNG,null,KENNUNG_BEZEICHNUNG from KENNUNGEN where VERALTET = 0 order by KENNUNG_BEZEICHNUNG ';
		$Form->Erstelle_SelectFeld('BES_KENNUNG',($AWIS_KEY1 == '-1')?'FEHLT':(isset($_POST['txtBES_KENNUNG'])?$_POST['txtBES_KENNUNG']:$rsErgebnis->FeldInhalt('KENNUNG')),290,false,$SQL6,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_UAUFTRAG'] . ':',140,'font-weight:bolder');
		$SQL7 ='select ART_ID,AUFTRAGSART_ATU,null,AUFTRAGSART_ATU as Titel from AUFTRAGSARTEN where veraltet = 0 order by AUFTRAGSART_ATU ';
		$Form->Erstelle_SelectFeld('BES_AUFTRAGSART_NEU',$rsErgebnis->FeldInhalt('AUFTRAGSART_ATU_NEU'),'258:190',$aendern,$SQL7,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel( '',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('XXX_ZEILE1','', 73, 190,false, '','', '','', 'L','','',30,'','','autocomplete=off');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUATUNR'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_ATUNR',$rsErgebnis->FeldInhalt('ATUNR'), 10, 290,$aendern, '','', '','', 'L','','',6,'','','autocomplete=off');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUWA'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_TextFeld('BES_VUWA',$AWIS_KEY1 =='-1'?'0':$rsErgebnis->FeldInhalt('WANR'), 10, 260,false, '','', '','', 'L','','',14,'','','autocomplete=off');
		$Form->Erstelle_TextLabel( '',120,'font-weight:bolder');
		$Form->Erstelle_TextFeld('XXX_ZEILE1','', 73, 190,false, '','', '','', 'L','','',30,'','','autocomplete=off');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		$Form->FormularBereichEnde();

		$Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_FEHLERDIAG'], true,0, '', '', false, [], true, 'overflow: scroll');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WORANARB'] . ':',140,'font-weight:bolder');
		$SQL7 ='select ID,GRUND from SCHADENSGRUND where veraltet = 0 order by Grund';
		$Form->Erstelle_SelectFeld('BES_WORANARB',$rsErgebnis->FeldInhalt('SCHADENSGRUND'),'290:190',$aendern,$SQL7,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WASBESCH'] . ':',140,'font-weight:bolder');
		$SQL8 ='select BID,SCHLAGWORT as Wert,null,SCHLAGWORT from BESCHAEDIGT where veraltet = 0 order by Schlagwort';

		$Form->Erstelle_SelectFeld('BES_WASBESCH',$rsErgebnis->FeldInhalt('BID'),'260:190',$aendern,$SQL8,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','onmouseover="ladeSelectTitles(this.name, this.form)"','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUSFALLURSACHE'] . ':',120,'font-weight:bolder');
		$SQL9 ='select ID,WERT from AUSFALLURSACHE order by wert ';
		$Form->Erstelle_SelectFeld('BES_AUSFALLURSACHE',$rsErgebnis->FeldInhalt('AUSFALLURSACHE'),190,$aendern,$SQL9,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FEHLER'] . ':',140,'font-weight:bolder');
		$Form->Erstelle_Textarea('BES_FEHLER', $rsErgebnis->FeldInhalt('FEHLERBESCHREIBUNG'), 780, 780, 1,false,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";','');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STAND'].':',140,'font-weight:bolder');
		$SQL2 ='SELECT STANDID,BESCHREIBUNG FROM STAND WHERE STANDID IN (14,15,19,22) ORDER BY BESCHREIBUNG ';
		$Form->Erstelle_SelectFeld('BES_STAND',($AWIS_KEY1 == '-1')?22:(isset($_POST['txtBES_STAND'])?$_POST['txtBES_STAND']:$rsErgebnis->FeldInhalt('STAND')),180,$aendern,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		$Form->FormularBereichEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Trennzeile('T');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_BES'] . ':', 800, 'font-weight:bolder');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$FeldBreiten = array();
		$FeldBreiten['DATUM'] = 90;
		$FeldBreiten['BEMERKUNGEN'] = 340;
		$FeldBreiten['BETRAG'] = 120;
		$FeldBreiten['ZAHLUNGSART'] = 110;
		$FeldBreiten['EINTRAG'] = 120;


		$SQL=	'select ID,DATUM,BEMERKUNGEN,BETRAG,ZAHLUNGSART,EINTRAG,BNUSERDAT, ';
		$SQL .='row_number() OVER (';
		$SQL .= ' order by ID asc';
		$SQL .= ') AS ZeilenNr from( ' ;
		$SQL .= 'select * from BEARBEITUNGSSTAND_NEW ';
		$SQL .= ' where BEARBNRNEU='.$rsErgebnis->FeldInhalt('BEARBNRNEU');



		if(isset($_GET['ID']) or $AWIS_KEY2 != '')
		{
			if(isset($_GET['ID']))
			{
				$SQL .= ' and ID ='.$_GET['ID'];
			}
			else
			{
				$SQL .= ' and ID ='.$AWIS_KEY2;
			}
		}
		//$SQL .= ' order by DATUM';
		$SQL .= ')';


		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;



		// Zum Bl�ttern in den Daten
		if (isset($_REQUEST['Block']))
		{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

		}


		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		//$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
		$rsINFO = $DB->RecordSetOeffnen($SQL);

		$Form->DebugAusgabe(1, $DB->LetzterSQL());


		if(!isset($_GET['ID']) and $AWIS_KEY2 == '') {
			$Form->ZeileStart('width:100%;');
			//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
			$Link = '';
			// �berschrift der Listenansicht mit Sortierungslink: Filiale
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_DATUM'], $FeldBreiten['DATUM'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_LANG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_LANG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEMERKUNG'], $FeldBreiten['BEMERKUNGEN'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_AKTIV,QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_AKTIV,QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BETRAG'] . ' EUR', $FeldBreiten['BETRAG'], '', '');
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ZAHLUNGSART'], $FeldBreiten['ZAHLUNGSART'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EINTRAG'], $FeldBreiten['EINTRAG'], '', $Link);

			$Form->ZeileEnde();

			$DS = 0;
			while (!$rsINFO->EOF()) {

				$Form->ZeileStart('width:100%');
				//$Form->Erstelle_ListenFeld('BES_BLANK', '', 0, $FeldBreiten['Blank'], false, ($DS % 2), '', '', 'T', 'L', '');
				$TTT = $rsINFO->FeldInhalt('DATUM');
				$Form->Erstelle_ListenFeld('BES_DATUM', $Form->Format('D', $rsINFO->FeldInhalt('DATUM')), 0, $FeldBreiten['DATUM'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$TTT = $rsINFO->FeldInhalt('BEMERKUNGEN');
				$Form->Erstelle_Textarea('BES_BEMERKUNGEN', $rsINFO->FeldInhalt('BEMERKUNGEN'), $FeldBreiten['BEMERKUNGEN'], 44, 1, true, '', '',
					'onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";', 'readonly');

				//$Form->Erstelle_ListenFeld('BES_BEMERKUNGEN',strlen($rsINFO->FeldInhalt('BEMERKUNGEN')) > 100 ? substr($rsINFO->FeldInhalt('BEMERKUNGEN'),0,99).'...':$rsINFO->FeldInhalt('BEMERKUNGEN'), 0, $FeldBreiten['BEMERKUNGEN'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsINFO->FeldInhalt('BETRAG') . ($rsINFO->FeldInhalt('BETRAG') == ''?'':' Eur');
				$Form->Erstelle_ListenFeld('BES_BETRAG', $Form->Format('N2', $rsINFO->FeldInhalt('BETRAG')) . ($rsINFO->FeldInhalt('BETRAG') == ''?'':'&euro;'), 0,
					$FeldBreiten['BETRAG'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$TTT = $rsINFO->FeldInhalt('ZAHLUNGSART');
				$Form->Erstelle_ListenFeld('BES_ZAHLUNGSART', $rsINFO->FeldInhalt('ZAHLUNGSART'), 0, $FeldBreiten['ZAHLUNGSART'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$TTT = $rsINFO->FeldInhalt('EINTRAG');
				$Form->Erstelle_ListenFeld('BES_ZAHLUNGSART', $rsINFO->FeldInhalt('EINTRAG'), 0, $FeldBreiten['EINTRAG'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

				$Form->ZeileEnde();
				$DS++;
				$rsINFO->DSWeiter();
			}



				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
			if($rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') == '') {
			    $Form->ZeileStart();
				$Form->Trennzeile('T');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();


				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DIAGNOSE'], 550, 'font-weight:bolder');
				$Form->ZeileEnde();

				$Form->ZeileStart();

				$Form->Erstelle_Textarea('!FESTSTELLUNG', ($Fehler != 0)?$_POST['txtFESTSTELLUNG']:'', 600, 80, 3, true, 'font: bold');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRAG'] . ':', 220);
				$Form->Erstelle_TextFeld('BES_BETRAG', ($Fehler != 0)?$_POST['txtBES_BETRAG']:'', 40, 116, true, '', 'text-align:right', '', 'T', 'L');
				$Form->Erstelle_TextLabel('&euro;', 160, 'font-weight:bolder');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ZAHLUNGSART'] . ':', 220);
				$SQL = 'select WERT,WERT as TextWert from ZAHLUNG_DURCH order by WERT';
				$Form->Erstelle_SelectFeld('BES_ZAHLUNGSART', ($Fehler != 0)?$_POST['txtBES_ZAHLUNGSART']:'', 116, true, $SQL,
					'~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', '', array(), '', 'SCHAD');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PERSONAL'] . ':*', 220,'font-weight:bolder;');
				$Form->Erstelle_TextFeld('!BES_PERSONAL', ($Fehler != 0)?$_POST['txtBES_PERSONAL']:'', 40, 116, true, '', 'text-align:right;', '', 'T', 'L');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('', 183,'');
				$Form->Schaltflaeche('submit', 'cmdHinzufuegen_x','', '',$AWISSprachKonserven['BES']['BES_RUECKMELDUNG'], 'W','',25,25);
				$Form->ZeileEnde();

				$Form->Erstelle_HiddenFeld('BEARBEITUNGSNR', $AWIS_KEY1);
				$Form->Erstelle_HiddenFeld('BEARBNRNEU', $rsErgebnis->FeldInhalt('BEARBNRNEU'));
				$Form->Erstelle_HiddenFeld('SBNAME', $rsErgebnis->FeldInhalt('SBNAME'));
			}
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if((($Recht40015&8)==8) and (isset($_GET['BEARBEITUNGSNR']) or $AWIS_KEY1 != '') and $rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') == '')
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S','','','','',true);
	}
	//Wenn der User in Detailmaske dann PDF Druck anzeigen
	
	if(($Recht40015&16)==16 and $AWIS_KEY1 > 0)
	{
		$Form->Schaltflaeche('href', 'cmdDrucken', '/berichte/drucken.php?XRE=64&ID='.base64_encode("&BEARB_ID="  . $AWIS_KEY1.''), '/bilder/cmd_pdf.png');

	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SBR',serialize($Param));
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	$Recht40015 = $AWISBenutzer->HatDasRecht(40015);

	if(($Recht40015&2)==2)
	{
		/*
		$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
		$Bedingung .=' group by xx1_fil_id) ';
		*/

		//Neuer SQL f�r RGZ 14.01.2015 ST
		$Bedingung .= 'AND FILNR in( select xx1_fil_id from awis.v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join awis.Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' left join awis.filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
		$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().' and xx1_fer_key in (23,25,57,26,69,72))';
		$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
		$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
		$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57,26,69,72))';
		$Bedingung .=' group by xx1_fil_id) ';

	}

	if(isset($Param['SBR_BEARB']) AND $Param['SBR_BEARB']!='')
	{
		$Bedingung .= ' AND BEARBEITUNGSNR ' . $DB->LikeOderIst($Param['SBR_BEARB']) . ' ';
	}

	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET'] !='' AND $Param['FIL_GEBIET'] !='0')
	{
		if(($Recht40015&2)==2)
		{
			//Neuer SQL f�r RGZ 14.01.2015 ST
			$Bedingung .= 'AND FILNR in( select xx1_fil_id from awis.v_filialebenenrollen_aktuell';
			$Bedingung .=' inner join awis.Filialebenen on xx1_feb_key = feb_key';
			$Bedingung .=' left join awis.filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
			$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().' and xx1_fer_key in (23,25,57,26,69,72)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
			$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
			$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57,26,69,72)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' group by xx1_fil_id) ';

		}
		elseif(($Recht40015&4)==4)
		{
			//Fehler behoben G�ltigkeiten hinzugef�gt 14.01.2015 ST
			$Bedingung .= 'AND FILNR in( select fez_fil_id from awis.filialebenenzuordnungen ';
			$Bedingung .=' where fez_feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=' and trunc(fez_gueltigab) <= trunc(sysdate)';
			$Bedingung .=' and trunc(fez_gueltigbis) >= trunc(sysdate))';
		}
	}

	if(isset($Param['SBR_FIL_ID']) AND $Param['SBR_FIL_ID']!='' AND $Param['SBR_FIL_ID']!=0)
	{
		$Bedingung .= ' AND FILNR ' . $DB->LikeOderIst($Param['SBR_FIL_ID']) . ' ';
	}
	if(isset($Param['BES_STATUS']))
	{
		if($Param['BES_STATUS']=='O')
		{
			$Bedingung .= " AND (AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";
		}
		elseif($Param['BES_STATUS']=='G')
		{
			$Bedingung .= " AND AKTEGESCHLOSSENAM is not null";
		}

        if(($Recht40015&64)==64) {
		    $Bedingung .= " AND (ANTRAGART= '4' OR ANZEIGEFIL= 1)";
        } else {
            $Bedingung .= " AND ANZEIGEFIL = 1";
        }
	}

	if(isset($Param['BES_AART']) AND $Param['BES_AART']!='' AND $Param['BES_AART'] != '0')
	{
		$Bedingung .= ' AND ANTRAGART ' . $DB->LikeOderIst($Param['BES_AART']) . ' ';
	}


	return $Bedingung;
}


?>