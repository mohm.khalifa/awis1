<?php
	//global $HTTP_GET_VARS;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $AWISBenutzer;

	$Rechte_OEN = awisBenutzerRecht($con,400);	
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $AWISBenutzer->BenutzerName()));
	$SQL = 'SELECT HER_BEZEICHNUNG, OEP_BEZEICHNUNG, OEP_ERSATZNR, OEP_PREIS, OIQ_STAND, OIQ_QUELLE, HER_ID';
	$SQL .= ' FROM OENummernPreise, Hersteller, OENUMMERNPREISIMPORTQUELLEN';
	$SQL .= ' WHERE OEP_HER_ID = HER_ID AND OEP_OIQ_NR = OIQ_NR';
//	$SQL .= " AND OEP_OEN_KEY=0" . $Artikel[0];
	$SQL .= " AND OEP_SUCHNUMMER " . awisLIKEoderIST($Artikel[1],false,false,false,2) . "";
	$SQL .= " ORDER BY OIQ_STAND DESC";


	$rsOEP = awisOpenRecordset($con, $SQL);
	if($rsOEP==FALSE)
	{
		awisErrorMailLink("oenummern_Preise.php", 2, $awisDBFehler['message']);
	}
	$rsOEPZeilen=$awisRSZeilen;

	print "<table id=DatenTabelle width=100% border=1>";
	print "<td id=FeldBez>Hersteller</td>";
	print "<td id=FeldBez>Bezeichnung</td>";	
	print "<td id=FeldBez>Ersatz-OE</td>";	
	print "<td id=FeldBez>Preis</td>";	
	print "<td id=FeldBez>Stand</td>";	
	print "<td id=FeldBez>Quelle</td>";	
	print "</tr>";
	
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		echo '<tr>';
		echo '<td>'	. $rsOEP['HER_BEZEICHNUNG'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['OEP_BEZEICHNUNG'][$OEPZeile] . '</td>';

		
		echo '<td><a href=./artikel_Main.php?cmdAktion=OENummern&Seite=Preise&OEN_NUMMER=' . $rsOEP['OEP_ERSATZNR'][$OEPZeile] . '&HER_ID=' . $rsOEP['HER_ID'][$OEPZeile] . '>';		
		echo ''	. $rsOEP['OEP_ERSATZNR'][$OEPZeile] . '</td>';
		echo '<td align=right>'	. awis_format($rsOEP['OEP_PREIS'][$OEPZeile],'Currency') . '</td>';
		echo '<td>'	. $rsOEP['OIQ_STAND'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['OIQ_QUELLE'][$OEPZeile] . '</td>';
		echo '</tr>';
	}

	print "</table>";
	unset($rsOEP);
	
?>