<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $AWISBenutzer;

	$OENRechte = awisBenutzerRecht($con,403);	
	if($OENRechte==0)
	{
		print "<hr><span class=HinweisText>Keine ausreichenden Rechte.</span>";
		awisLogoff($con);
		die();
	}
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerLAR", $AWISBenutzer->BenutzerName()));
	
	$SQL = "SELECT * FROM AWIS.OENummern, AWIS.TeileInfos, AWIS.Hersteller WHERE HER_ID = OEN_HER_ID AND OEN_KEY=TEI_KEY2 AND TEI_KEY1=0" . $Artikel[0];

	$rsOEN = awisOpenRecordset($con, $SQL);
	if($rsOEN==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsOENZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr>";
	print "<td width=30 id=FeldBez>&nbsp;</td>";
	print "<td id=FeldBez>OE-Nummer</td>";
	print "<td id=FeldBez>Hersteller</td>";
	print "<td id=FeldBez>Muster</td>";	
	print "<td id=FeldBez>Bemerkung</td>";	
	print "<td id=FeldBez>Preis</td>";
	print "</tr>";
	
	for($OEZeile=0;$OEZeile<$rsOENZeilen;$OEZeile++)
	{
		print "<tr>";

		if(($OENRechte & 2)==2)
		{
			print "<td><input name=cmdOENLoeschen_" . $rsOEN["OEN_KEY"][$OEZeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
		}
		else
		{
			print "<td></td>";
		}
		print "<td><a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY=" . $rsOEN["OEN_KEY"][$OEZeile] . ">";
		print $rsOEN["OEN_NUMMER"][$OEZeile] . "</a></td>";
		
		print "<td>" . $rsOEN["HER_BEZEICHNUNG"][$OEZeile] . "</td>";
		print "<td>" . ($rsOEN["OEN_MUSTER"][$OEZeile]==0?"Nein":"Ja") . "</td>";
		print "<td>" . $rsOEN["OEN_BEMERKUNG"][$OEZeile] . "</td>";

		$rsOEP = awisOpenRecordset($con, "SELECT * FROM AWIS.OENummernPreise WHERE OEP_OEN_KEY=0" . $rsOEN["OEN_KEY"][$OEZeile] . " ORDER BY OEP_KEY DESC");
		if($awisRSZeilen>0)
		{
			print "<td>" . awis_format($rsOEP["OEP_PREIS"][0],"Currency") . "</td>";
		}
		else
		{
			print "<td>n.v.</td>";
		}
		unset($rsOEP);
		
		print "</tr>";	
	}	// Alle Zeilen ausgeben
	
	if(($OENRechte & 2)==2)
	{
		print "<tr>";
		
		print "<td><input type=hidden name=txtOEN_KEY_0></td>";
		
		print "<td><input name=txtOEN_NUMMER size=30></td>";
		print "<td><select name=txtOEN_HER_ID>\n\r";

		$rsHER = awisOpenRecordset($con, "SELECT HER_ID, HER_BEZEICHNUNG FROM AWIS.Hersteller WHERE HER_TYP=1 ORDER BY HER_BEZEICHNUNG");
		for($HerZeile=0;$HerZeile<$awisRSZeilen;$HerZeile++)
		{
			print "\n\r<option ";
			if($HerZeile==0)
			{
				print " selected ";
			}
			print " value=" . $rsHER["HER_ID"][$HerZeile] . ">"  . $rsHER["HER_BEZEICHNUNG"][$HerZeile] . "</option>";
		}
		
		print "</select></td>";

		print "</tr>";
	}
		
	print "</table>";
	
	unset($rsLiefArt);
?>