<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once 'fpdi.php';

/***************************************
* Neue PDF Datei erstellen
***************************************/
define('FPDF_FONTPATH','font/');
$pdf = new fpdi('l','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$pdf->AddFont('Gothic');
$ATULogo = $pdf->ImportPage(1);

/***************************************
*
* Daten laden
*
***************************************/

$SQL = 'SELECT *';
$SQL .= ' FROM ARTIKELPRUEFUNGEN LEFT OUTER JOIN Lieferanten ON APR_LIE_NR = LIE_NR';
$SQL .= ' INNER JOIN Artikelstamm ON APR_AST_ATUNR=AST_ATUNR';
$SQL .= ' LEFT OUTER JOIN ARTIKELPRUEFUNGENMELDUNGEN ON APR_KEY = APM_APR_KEY';
$SQL .= " WHERE APR_KEY=0" . $_GET['APRKey'] . "";

$rsAPR = awisOpenRecordset($con,$SQL);
$rsAPRZeilen = $awisRSZeilen;

if($rsAPRZeilen==0)
{
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,270,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}
else 		// Daten gefunden
{
	NeueSeite($pdf,$Zeile,$ATULogo,15);
}	// Ende Datenaufbereitung

$LinkerRand=10;


// �berschrift setzen

$pdf->SetFont('Arial','',10);				// Schrift setzen

$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(60,6,"ATU-Nummer:",0,0,'L',0);

$pdf->setXY($LinkerRand+60,$Zeile);
$pdf->cell(60,6,"Lieferantenartikelnummer:",0,0,'L',0);

$pdf->setXY($LinkerRand+120,$Zeile);
$pdf->cell(160,6,"Artikelbezeichnung",0,0,'L',0);

// Zweite Zeile

$Zeile+=5;
$pdf->SetFont('Arial','b',14);				// Schrift setzen

$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(60,6,$rsAPR['APR_AST_ATUNR'][0],1,0,'L',0);

$pdf->setXY($LinkerRand+60,$Zeile);
$pdf->cell(60,6,$rsAPR['APR_LAR_LARTNR'][0],1,0,'L',0);

$pdf->setXY($LinkerRand+120,$Zeile);
$pdf->cell(160,6,$rsAPR['AST_BEZEICHNUNGWW'][0],1,0,'L',0);

// Dritte Zeile
$Zeile+=7;
$pdf->SetFont('Arial','',10);				// Schrift setzen

$pdf->setXY($LinkerRand+80,$Zeile);
$pdf->cell(40,6,"LF-Nr:",0,0,'L',0);

$pdf->setXY($LinkerRand+120,$Zeile);
$pdf->cell(160,6,"Lieferantenname",0,0,'L',0);

// Vierte Zeile

$Zeile+=5;
$pdf->SetFont('Arial','',14);				// Schrift setzen

$pdf->setXY($LinkerRand+80,$Zeile);
$pdf->cell(40,6,$rsAPR['LIE_NR'][0],1,0,'L',0);

$pdf->setXY($LinkerRand+120,$Zeile);
$pdf->cell(160,6,$rsAPR['LIE_NAME1'][0],1,0,'L',0);


// F�nfte Zeile

$Zeile+=7;
$pdf->SetFont('Arial','',12);				// Schrift setzen

$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(55,6,"Fehlerbeschreibung:",0,0,'R',0);

$pdf->SetFont('Arial','',14);				// Schrift setzen
$pdf->setXY($LinkerRand+60,$Zeile);
$pdf->cell(220,30,'',1);
$pdf->setXY($LinkerRand+60,$Zeile);
$pdf->MultiCell(220,6,$rsAPR['APR_FEHLERBESCHREIBUNG'][0],0,'L',0);

// Sechste Zeile

$Zeile=80;
$pdf->SetFont('Arial','',11);				// Schrift setzen

$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(90,6,"Logistikzentrum Weiden",0,0,'C',0);

$pdf->setXY($LinkerRand+90,$Zeile);
$pdf->cell(90,6,"Logistikzentrum Werl",0,0,'C',0);

$pdf->setXY($LinkerRand+180,$Zeile);
$pdf->cell(90,6,"Filialbest�nde",0,0,'C',0);

$Zeile+=5;

//******************************
// Weiden
//******************************
$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(90,60,'',1);

$pdf->setXY($LinkerRand,$Zeile);
$pdf->SetFont('Arial','b',11);				// Schrift setzen
$pdf->cell(25,6,"Menge:",0,0,'R',0);
$pdf->SetFont('Arial','',11);				// Schrift setzen
$pdf->cell(60,6,$rsAPR['APR_MENGE_N'][0],0,0,'L',0);
$pdf->setXY($LinkerRand,$Zeile+5);
$pdf->SetFont('Arial','b',11);				// Schrift setzen
$pdf->cell(25,6,"Pr�fer:",0,0,'R',0);
$pdf->SetFont('Arial','',11);				// Schrift setzen
$pdf->cell(60,6,$rsAPR['APR_PRUEFER_N'][0],0,0,'L',0);
$pdf->setXY($LinkerRand,$Zeile+10);
$pdf->SetFont('Arial','b',11);				// Schrift setzen
$pdf->cell(25,6,"Datum:",0,0,'R',0);
$pdf->SetFont('Arial','',11);				// Schrift setzen
$pdf->cell(60,6,$rsAPR['APR_DATUM_N'][0],0,0,'L',0);

$pdf->setXY($LinkerRand,$Zeile+15);
$pdf->SetFont('Arial','b',11);				// Schrift setzen
$pdf->cell(25,6,"Ergebnis:",0,0,'R',0);
$pdf->SetFont('Arial','',11);				// Schrift setzen
$pdf->setXY($LinkerRand+25,$Zeile+16);
$pdf->multicell(60,4,$rsAPR['APR_ERGEBNIS_N'][0],0,'L',0);

$pdf->setXY($LinkerRand,$Zeile+35);
$pdf->SetFont('Arial','b',11);				// Schrift setzen
$pdf->cell(25,6,"Ma�nahme:",0,0,'R',0);
$pdf->SetFont('Arial','',11);				// Schrift setzen
$pdf->setXY($LinkerRand+25,$Zeile+36);
$pdf->multicell(60,4,$rsAPR['APR_MASSNAHME_N'][0],0,'L',1);


//******************************
// Werl
//******************************
$pdf->setXY($LinkerRand+90,$Zeile);
$pdf->cell(90,60,'',1);

$pdf->setXY($LinkerRand+90,$Zeile);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Menge:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(60,6,$rsAPR['APR_MENGE_L'][0],0,0,'L',0);
$pdf->setXY($LinkerRand+90,$Zeile+5);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Pr�fer:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(60,6,$rsAPR['APR_PRUEFER_L'][0],0,0,'L',0);
$pdf->setXY($LinkerRand+90,$Zeile+10);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Datum:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(60,6,$rsAPR['APR_DATUM_L'][0],0,0,'L',0);

$pdf->setXY($LinkerRand+90,$Zeile+15);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Ergebnis:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->setXY($LinkerRand+115,$Zeile+16);
$pdf->multicell(60,4,$rsAPR['APR_ERGEBNIS_L'][0],0,'L',0);

$pdf->setXY($LinkerRand+90,$Zeile+35);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Ma�nahme:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->setXY($LinkerRand+115,$Zeile+36);
$pdf->multicell(60,4,$rsAPR['APR_MASSNAHME_L'][0],0,'L',0);

//******************************
// Filialbest�nde
//******************************

$pdf->setXY($LinkerRand+180,$Zeile);
$pdf->cell(100,60,'',1);

$pdf->setXY($LinkerRand+180,$Zeile);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Menge:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(60,6,$rsAPR['APR_FILIALBESTAND'][0],0,0,'L',0);

$pdf->setXY($LinkerRand+180,$Zeile+5);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Ma�nahme:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->setXY($LinkerRand+205,$Zeile+6);
$pdf->multicell(65,4,$rsAPR['APR_MASSNAHME_FILIALEN'][0],0,'L',0);



// Ma�nahme QS
$Zeile+=60;
$pdf->setXY($LinkerRand,$Zeile);
$pdf->multicell(280,6,$rsAPR['APR_MASSNAHME'][0],0,'L',1);
$pdf->setXY($LinkerRand,$Zeile);
$pdf->cell(280,20,'',1);


// Bearbeiter
$Zeile+=21;
$pdf->setXY($LinkerRand,$Zeile);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Bearbeiter:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(40,6,$rsAPR['APR_BEARBEITER'][0],0,0,'L',0);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Datum:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(40,6,$rsAPR['APR_BEARBEITUNGSTAG'][0],0,0,'L',0);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Meldung durch:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(40,6,$rsAPR['APR_MELDUNGDURCH'][0],0,0,'L',0);

// Lieferant informiert
$Zeile+=5;
$pdf->setXY($LinkerRand+65,$Zeile);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"Lieferant informiert am:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(40,6,$rsAPR['APR_LIEFINFORMIERTAM'][0],0,0,'L',0);
$pdf->SetFont('Arial','b',11);
$pdf->cell(25,6,"durch:",0,0,'R',0);
$pdf->SetFont('Arial','',11);
$pdf->cell(40,6,$rsAPR['APR_LIEFINFORMIERT'][0],0,0,'L',0);

// Trennlinie ziehen
$pdf->Line($LinkerRand,$Zeile+5,290,$Zeile+5);
// Kontrollk�stchen

$pdf->SetFont('Arial','',10);				// Schrift setzen

$SQL = 'SELECT * ';
$SQL .= ' FROM ARTIKELPRUEFUNGENINFOSTELLEN';
$SQL .= ' WHERE AIS_STATUS = \'A\'';
$SQL .= ' ORDER BY AIS_SORTIERUNG';

$rsAIS = awisOpenRecordset($con,$SQL,false,false);
$rsAISZeilen = $awisRSZeilen;

$Zeile = 180;
$Spalte = 0;

//$APM_BENACHRICHTIGUNGEN=explode(";",$rsAPR['APM_BENACHRICHTIGUNGEN'][0]);

for($i=0;$i<$rsAISZeilen;$i++)
{
	$pdf->setXY($LinkerRand+$Spalte,$Zeile);
	/*
	for($z=0;$z<count($APM_BENACHRICHTIGUNGEN);$z++)
	{
		if($APM_BENACHRICHTIGUNGEN[$z]==$rsAIS['AIS_KEY'][$i])
		{
			$found=true;
			break;
		}
		else
		{
			$found=false;
		}
			//$pdf->cell(5,6,($APM_BENACHRICHTIGUNGEN[$z]!=$rsAIS['AIS_KEY'][$i]?'':'X'),1,0,'R',0);
	}

	if($found)
	{
		$pdf->cell(5,6,'X',1,0,'R',0);
	}
	else
	{
		$pdf->cell(5,6,'',1,0,'R',0);
	}
	*/
	//$pdf->cell(5,6,($APM_BENACHRICHTIGUNGEN[$z]!=$rsAIS['AIS_KEY'][$i]?'':'X'),1,0,'R',0);
	$pdf->cell(5,6,(strchr(';'.$rsAPR['APM_BENACHRICHTIGUNGEN'][0].';',';'.$rsAIS['AIS_KEY'][$i].';')==''?'':'X'),1,0,'R',0);
	$pdf->cell(50,6,$rsAIS['AIS_BEZEICHNUNG'][$i],0,0,'L',0);

	$Spalte+=56;
	if($Spalte>240)
	{
		$Zeile+=6;
		$Spalte=0;
	}

}
$pdf->setXY($LinkerRand+$Spalte,$Zeile+1);
$pdf->SetFont('Arial','b',10);
$pdf->cell(100,5,'Weitergeleitet am: ' . $rsAPR['APR_WEITERGELEITETAM'][0],0,0,'l',0);




/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
$pdf->saveas($DateiName);
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";

/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite;

	if($Seite > 0)		// Fu�zeile?
	{
		$pdf->setXY($LinkerRand,285);
		$pdf->SetFont('Arial','',6);					// Schrift setzen
		$pdf->setXY(190,285);
		$pdf->Cell(170,3,'Seite ' . $Seite,0,0,'L',0);

		$pdf->setXY($LinkerRand,290);
		$pdf->SetFont('Arial','',3);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(160,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
	}
	$Seite++;

	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,270,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(190,6,"Artikel�berpr�fungen Weiden/Werl",0,0,'L',0);

	$pdf->setXY($LinkerRand,10);
	$pdf->SetFont('Arial','',6);				// Schrift setzen
	$pdf->cell(190,6,"Stand: " . date('d.m.Y'),0,0,'L',0);

	$Zeile = 15;
}
?>