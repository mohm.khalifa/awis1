<?php
global $SEP;
try {

    $SQL = 'merge into MAILVERSANDTEXTE DEST using';
    $SQL .= ' ( select ';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'C', $_POST['txtMVT_TEXT']) . '  as MVT_TEXT, ';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', $_POST['txtMVT_BETREFF']) . ' as MVT_BETREFF, ';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'TU', $_POST['txtMVT_BEREICH_PRAEFIX'] . $_POST['txtMVT_BEREICH']) . ' as MVT_BEREICH, ';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'TU', $SEP->AWISBenutzer->BenutzerName()) . ' as MVT_USER ';
    $SQL .= ' from';
    $SQL .= '     DUAL';
    $SQL .= ' )';
    $SQL .= ' SRC on (';
    $SQL .= ' DEST.MVT_BEREICH = SRC.MVT_BEREICH';
    $SQL .= ' ) when matched then';
    $SQL .= ' update';
    $SQL .= ' set DEST.MVT_TEXT = SRC.MVT_TEXT, ';
    $SQL .= ' DEST.MVT_BETREFF = SRC.MVT_BETREFF, ';
    $SQL .= ' DEST.MVT_USER = SRC.MVT_USER, ';
    $SQL .= ' DEST.MVT_USERDAT = sysdate ';
    $SQL .= ' when not matched then insert ( DEST.MVT_TEXT,DEST.MVT_BETREFF,DEST.MVT_BEREICH,DEST.MVT_SPRACHE,DEST.MVT_GUELTIGAB';
    $SQL .= ' ,DEST.MVT_USER,DEST.MVT_USERDAT ) values ( SRC.MVT_TEXT,SRC.MVT_BETREFF,SRC.MVT_BEREICH,\'DE\',SYSDATE - 1,SRC.MVT_USER,SYSDATE )';

    $SEP->DB->Ausfuehren($SQL, '', true, $SEP->DB->Bindevariablen('MVT'));

    if ($_GET['MVT_KEY'] == -1) { //Wars eine Neuanlage? Dann brauch ich die Sequenz
        $_GET['MVT_KEY'] = $SEP->DB->RecordSetOeffnen('Select SEQ_MVT_KEY.currval from dual')->FeldInhalt(1);
    }

    $SEP->Form->Hinweistext($SEP->AWISSprachKonserven['SEP']['SEP_SPEICHERNOK'], awisFormular::HINWEISTEXT_OK);

} catch (awisException $ex) {
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>