<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$RechteStufe = awisBenutzerRecht($con, 1500);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Schluessel',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

//awis_Debug('entwick',0,false,$_REQUEST);

echo "<form name=frmSuche method=post action=./schluessel_Main.php?cmdAktion=Suchliste>";

echo "<table  width=100% id=SuchMaske border=1><tr>";
	echo "<td id=FeldBez width=150>Schl&uuml;ssel-Nr</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_SchluesselNr size=25 ></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Mitarbeiter</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_Mitarbeiter size=50 ></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Filial-Nr/-Ort(steil)</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_Filiale size=50 ></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Rechnungs-Nr</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_RechnungsNr size=25 ></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Schl&uuml;sselgruppe</td>";
	echo "<td><select name=txtSuche_SLN_SLG_KEY>";
			echo '<option value=-1>Bitte w�hlen...</option>';
			//$rsSLN_SLG_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELGRUPPEN ORDER BY SLG_BEZEICHNUNG');
			$rsSLN_SLG_KEY = awisOpenRecordset($con, "SELECT SLG_BEZEICHNUNG AS BEZEICHNUNG, SLG_KEY, NULL AS SLU_KEY FROM SCHLUESSELGRUPPEN 
			UNION
			SELECT SLG_BEZEICHNUNG || NVL2(SLU_BEZEICHNUNG,' => ' || SLU_BEZEICHNUNG,'') AS BEZEICHNUNG, SLG_KEY, SLU_KEY FROM SCHLUESSELGRUPPEN JOIN SCHLUESSELUNTERGRUPPEN ON (SCHLUESSELUNTERGRUPPEN.SLU_SLG_KEY(+)=SCHLUESSELGRUPPEN.SLG_KEY) 
			ORDER BY BEZEICHNUNG");
			$rsSLN_SLG_KEYZeilen = $awisRSZeilen;
			for($SLN_SLG_KEYZeile=0;$SLN_SLG_KEYZeile<$rsSLN_SLG_KEYZeilen;$SLN_SLG_KEYZeile++)
			{
					echo '<option value=' . $rsSLN_SLG_KEY['SLG_KEY'][$SLN_SLG_KEYZeile] .','. $rsSLN_SLG_KEY['SLU_KEY'][$SLN_SLG_KEYZeile] . '>' . $rsSLN_SLG_KEY['BEZEICHNUNG'][$SLN_SLG_KEYZeile] . '</option>';
			}
			echo '</select>';
			unset($rsSLN_SLG_KEY);
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Schl&uuml;ssellieferant</td>";
	echo "<td><select name=txtSuche_SLN_SLL_KEY>";
			echo '<option value=-1>Bitte w�hlen...</option>';
			$rsSLN_SLL_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELLIEFERANTEN ORDER BY SLL_BEZEICHNUNG');
			$rsSLN_SLL_KEYZeilen = $awisRSZeilen;
			for($SLN_SLL_KEYZeile=0;$SLN_SLL_KEYZeile<$rsSLN_SLL_KEYZeilen;$SLN_SLL_KEYZeile++)
			{
					echo '<option value=' . $rsSLN_SLL_KEY['SLL_KEY'][$SLN_SLL_KEYZeile] . '>' . $rsSLN_SLL_KEY['SLL_BEZEICHNUNG'][$SLN_SLL_KEYZeile] . '</option>';
			}
			echo '</select>';
			unset($rsSLN_SLL_KEY);
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Schl&uuml;sselstatus</td>";
	echo "<td><select name=txtSuche_SLV_SLS_KEY>";
			echo '<option value=-1>Bitte w�hlen...</option>';
			$rsSLV_SLS_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELSTATUS ORDER BY SLS_BEZEICHNUNG');
			$rsSLV_SLS_KEYZeilen = $awisRSZeilen;
			for($SLV_SLS_KEYZeile=0;$SLV_SLS_KEYZeile<$rsSLV_SLS_KEYZeilen;$SLV_SLS_KEYZeile++)
			{
					echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . '>' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
			}
			echo '</select>';
			unset($rsSLV_SLS_KEY);
	echo "</tr>";
echo "</table>";

echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png title='Formularinhalt l�schen' name=cmdReset onclick=location.href='./schluessel_Main.php?Reset=True';>";

// Ausgabe er�ffnete Filiale der noch keine Schl�essel zugeordnet wurde

$SQL="select fil_id, fil_bez,EROEFFNUNGSDATUM from v_fil_vkl_gbl ";
$SQL.="where EROEFFNUNGSDATUM<=sysdate and not FIL_GRUPPE is null and fil_gruppe not in('B','5')";
$SQL.="and fil_id not in (select distinct sln_fil_id from schluesselnummern) order by fil_id";

$rsSchluesselfilialen = awisOpenRecordset($con,$SQL);
$rsSchluesselfilialenZeilen = $awisRSZeilen;

if($rsSchluesselfilialenZeilen!=0)
{
	echo "<br>";
	echo "<hr>";
	
	echo "<center><h4>Noch nicht angelegte Filialen</h4></center>";
	
	echo "<table  width=100% id=DatenTabelle border=1><tr>";
	echo "<td id=FeldBez width=150>Filiale-Nr.</td>";
	echo "<td id=FeldBez width=150>Filial-Bezeichnung</td>";
	echo "<td id=FeldBez width=150>Er&ouml;ffnungsdatum</td>";	
	echo "</tr>";
	
	for($i=0;$i<$rsSchluesselfilialenZeilen;$i++)
	{
		echo "<tr>";
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><a title=Filial-Details anzeigen href=./schluessel_Main.php?cmdAktion=Nummern&Anzeigen=TRUE&txtFIL_ID=".$rsSchluesselfilialen['FIL_ID'][$i].">".$rsSchluesselfilialen['FIL_ID'][$i]."</a></td>";
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselfilialen['FIL_BEZ'][$i]."</td>";
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselfilialen['EROEFFNUNGSDATUM'][$i]."</td>";
		echo "</tr>";
	}
	
	echo "</table>";	
}

// Ausgabe Schluesselanzahl unterschiedlich zur Filialschicht

/*$SQL="select sln_fil_id, fif_wert, schluesselgruppen.slg_bezeichnung, count(schluesselgruppen.slg_bezeichnung) as anzahl, slg_schichtsuche ";
$SQL.="from schluesselvergaben, schluesselnummern, schluesselgruppen, schluesseluntergruppen, filialinfos ";
$SQL.="where ( (schluesselvergaben.slv_sln_key = schluesselnummern.sln_key(+)) and (schluesselnummern.sln_slg_key=schluesselgruppen.slg_key(+)) ";
$SQL.="and (schluesselnummern.sln_slu_key=schluesseluntergruppen.slu_key(+))) ";
$SQL.="and (schluesselnummern.sln_fil_id=filialinfos.fif_fil_id(+)) ";
$SQL.="and fif_fit_id=20 and fif_imq_id=8 and fif_wert is not null ";
$SQL.="and slu_bezeichnung is null and sln_fil_id is not null and slg_schichtsuche is not null and slv_sls_key=2";
$SQL.="group by sln_fil_id, fif_wert, schluesselgruppen.slg_bezeichnung, slg_schichtsuche ";
$SQL.="order by sln_fil_id, slg_bezeichnung ";

//Filial-Schicht => Haupt,Filial,Werkstatt,Tresor1,Tresor2,Alarmanlage,Smartkey
$Schluessel_Menge= array("1" => array("2","2","2","2","2","2","2"),"2" => array("3","3","2","2","2","3","3"));

awis_Debug(2,$Schluessel_Menge);

$rsSchluesselSchicht = awisOpenRecordset($con,$SQL);
$rsSchluesselSchichtZeilen = $awisRSZeilen;

if($rsSchluesselSchichtZeilen!=0)
{
	echo "<hr>";
	
	echo "<center><h4>Schl&uuml;ssel-Vergabe unterschiedlich zur Filial-Schicht</h4></center>";
	
	echo "<table  width=100% id=DatenTabelle border=1><tr>";
	echo "<td id=FeldBez width=150>Filiale-Nr.</td>";
	echo "<td id=FeldBez width=150>Schicht</td>";
	echo "<td id=FeldBez width=150>Schl&uuml;ssel-Gruppe</td>";	
	echo "<td id=FeldBez width=150>Anzahl</td>";
	echo "</tr>";
	
	for($i=0;$i<$rsSchluesselSchichtZeilen;$i++)
	{
		
		if($rsSchluesselSchicht['ANZAHL'][$i]>$Schluessel_Menge[$rsSchluesselSchicht['FIF_WERT'][$i]][$rsSchluesselSchicht['SLG_SCHICHTSUCHE'][$i]])
		{
			echo "<tr>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><a title=Filial-Details anzeigen href=./schluessel_Main.php?cmdAktion=Nummern&Anzeigen=TRUE&txtFIL_ID=".$rsSchluesselSchicht['SLN_FIL_ID'][$i].">".$rsSchluesselSchicht['SLN_FIL_ID'][$i]."</a></td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['FIF_WERT'][$i]."</td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['SLG_BEZEICHNUNG'][$i]."</td>";
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">".$rsSchluesselSchicht['ANZAHL'][$i]."</td>";
			echo "</tr>";
		}
	}
	
	echo "</table>";	
}*/

echo '</form>';
?>
</body>
</html>
