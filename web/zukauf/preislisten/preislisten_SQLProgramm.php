<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $ZPL;
$DetailAnsicht = false;
$Speichern = false;

//AWIS_KEY2 setzen
if(isset($_GET['ZPP_KEY'])){
    $AWIS_KEY2 = $_GET['ZPP_KEY'];
}elseif(isset($_POST['txtZPP_KEY'])){
    $AWIS_KEY2 = $_POST['txtZPP_KEY'];
}else{
    $AWIS_KEY2 = $ZPL->Param['ZPP_KEY'];
}

$ZPL->Param['ZPP_KEY'] = $AWIS_KEY2;
$ZPL->Form->Erstelle_HiddenFeld('ZPP_KEY',$AWIS_KEY2);

//SQL f�r die Pr�fung holen:
$SQL = 'Select ZPP_PROGRAMM from zukaufpreislistenpruefprogs where ZPP_KEY =' .$ZPL->DBi->WertSetzen('ZPP','Z',$AWIS_KEY2);
$SQLProg = $ZPL->DBi->RecordSetOeffnen($SQL,$ZPL->DBi->Bindevariablen('ZPP'))->FeldInhalt(1);
$SQLProg = substr($SQLProg,4);

//Ersetzungen durchf�hren:
//Muss so komisch gemacht, werden, da ich im Stringreplace Binds benutzen will
$Erg = false;
str_replace('#ZPL_KEY_AKTUELL#','',$SQLProg,$Erg);
if($Erg){
    $SQLProg = str_replace('#ZPL_KEY_AKTUELL#',$ZPL->DBi->WertSetzen('SQL','Z',$AWIS_KEY1),$SQLProg);
}
$Erg = false;
str_replace('#ZPL_KEY_ALT#','',$SQLProg,$Erg);
if($Erg){
    $SQLProg = str_replace('#ZPL_KEY_ALT#',$ZPL->DBi->WertSetzen('SQL','Z',45),$SQLProg);
}

//Recordset �ffnen
$rsProg = $ZPL->DBi->RecordSetOeffnen($SQLProg,$ZPL->DBi->Bindevariablen('SQL'));

//�berschriften f�r die Liste malen:
$ZPL->Form->ZeileStart();
for($i=1;$i<$rsProg->AnzahlSpalten();$i++){
    $ZPL->Form->Erstelle_Liste_Ueberschrift($rsProg->SpaltenNamen()[$i],strlen($rsProg->SpaltenNamen()[$i])*13);
}
    $ZPL->Form->ZeileEnde();

//Daten der Liste malen:

while(!$rsProg->EOF()){
    $ZPL->Form->ZeileStart();
    for($i=1;$i<$rsProg->AnzahlSpalten();$i++){
        $ZPL->Form->Erstelle_ListenFeld($rsProg->SpaltenNamen()[$i],$rsProg->FeldInhalt($i+1), strlen($rsProg->SpaltenNamen()[$i]),strlen($rsProg->SpaltenNamen()[$i])*13);
    }
    $ZPL->Form->ZeileEnde();
    $rsProg->DSWeiter();
}

?>