<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('RFU','RFU_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Dateiname');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keingueltigesDatum');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3930 = $AWISBenutzer->HatDasRecht(3930);
if($Recht3930==0)
{
	$Form->Fehler_KeineRechte();    
}


if(!isset($_POST['cmd_Import_x']))
{
	$Form->SchreibeHTMLCode('<form name=frmImport method=post action=./rueckfuehrung_Main.php?cmdAktion=Import enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'].':',150);
	$Form->Erstelle_DateiUpload('Importdatei',310,30,20000000);
	$Form->ZeileEnde();


	$Form->Formular_Ende();
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image','cmd_Import','','/bilder/cmd_weiter.png',$AWISSprachKonserven['Wort']['lbl_weiter'],'W');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form');
}
else 
{
	if(isset($_FILES['Importdatei']))
	{		
		if(($fd = fopen($_FILES['Importdatei']['tmp_name'],'r'))!==false)
		{
			$Zeile = fgets($fd);
			$Zeile = trim($Zeile);
			$DateiInfo = explode(";",$Zeile);
			//$Form->DebugAusgabe(1,$DateiInfo);
			if($DateiInfo[0]==='4096' or $DateiInfo[0]==='4090' )
			{
				$Importfunktion = "import_".trim($DateiInfo[0]);
				
				$Erg = $Importfunktion($DateiInfo[1], $fd);
				
				$Form->SchreibeHTMLCode("Statusreport AWIS<br>");
				
				$Form->SchreibeHTMLCode("Datum\t".date('d.m.Y')."<br>");
				/*
				if($Erg['Code']==0)
				{
					$Form->SchreibeHTMLCode("Status\tErfolgreich importiert.<br>");
				}
				else 
				{
					$Form->SchreibeHTMLCode("Fehler\t".$Erg['Code']."<br>");
					$Form->SchreibeHTMLCode("Meldung\t".$Erg['Letzte Meldung']."<br>");
				}
				*/
				//$Form->SchreibeHTMLCode("<br>");
				//$Form->SchreibeHTMLCode("Meldungen im Detail:<br>");
				if(is_array($Erg))
				{
					$Form->SchreibeHTMLCode("<br>");
					$Form->SchreibeHTMLCode("<b>FEHLERMELDUNGEN</b><br>");
					foreach ($Erg['Fehlermeldung'] AS $Fehlermeldung)
					{
						$Form->SchreibeHTMLCode('-FEHLER- '.$Fehlermeldung."<br>");
					}
					
					$Form->SchreibeHTMLCode("<br>");
					$Form->SchreibeHTMLCode("<b>MELDUNGEN</b><br>");
					foreach ($Erg['Meldungen'] AS $Meldung)
					{
						$Form->SchreibeHTMLCode($Meldung."<br>");
					}
				}

				if ($Erg['Anforderung'] != '')
				{
					$Link='./rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$DB->FeldInhaltFormat('Z',$Erg['Anforderung']);
				}
				else 
				{
					$Link='./rueckfuehrung_Main.php?cmdAktion=Suche';
				}
					
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png');
				$Form->SchaltflaechenEnde();				
			}
		}
	}
}



/**
 * Rueckfuehrung (Typ 4090)
 *
 * @param string $Version
 * @param ressource $fd
 * @return array
 */
function import_4090($Version, $fd)
{
	global $AWISBenutzer;
	
	$Form = new awisFormular();
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array(), 'Fehlermeldung'=>array(), 'Anforderung'=>'');
	
	if($Version != "1.0")
	{
		//$Erg = array('Code'=>2, 'Letzte Meldung'=>'Falsche Dateiversion!', 'Meldungen'=>array());	
		
		$Erg['Code']=2;
		$Erg['Fehlermeldung'][]='Falsche Dateiversion';
		
		return $Erg;
	}
	
	//$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array());
	//$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array(), 'Fehlermeldung'=>array());
	
	rewind($fd);
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);
	
	$Kopf = explode(";",$Zeile);
	$Datum = $Kopf[2];
	$Bemerkung = $Kopf[3];		
	
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);
	$Ueberschriften = explode(";",$Zeile);
	$Ueberschriften = array_flip($Ueberschriften);	
	
	if(!isset($Ueberschriften['ATUNR']) or !isset($Ueberschriften['GESAMTMENGE']) or !isset($Ueberschriften['MINDESTBESTAND']) or !isset($Ueberschriften['RUECKLIEFKZ']))
	{
		//return(array('Code'=>1,'Letzte Meldung'=>'Falsche Ueberschrift','Zeile'=>0,'Meldungen'=>array()));
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Falsche Überschrift';
		
		return $Erg;		
	}

	//Neuen Rückführungskopf anlegen
	//$Erg['Meldungen'][]='Anforderung '.$Datum. ' '. $Bemerkung .' neu anlegen.';			
	
	// Daten auf Vollständigkeit prüfen
	if($Datum == '')
	{
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Es wurde kein Datum angegeben!';
		
		return $Erg;		
	}
	
	$ANFDATUM = $Form->Format('D',$Datum,false);
	$ANFDATUMFELDER = explode('.', $ANFDATUM);
	
	$Fehler = '';	
	
	if (!checkdate($ANFDATUMFELDER[1],$ANFDATUMFELDER[0],$ANFDATUMFELDER[2]))
	{
			$Fehler .= $AWISSprachKonserven['Fehler']['err_keingueltigesDatum'].'<br>';					
	}
	$Form->DebugAusgabe(1,$ANFDATUM, $ANFDATUMFELDER);
	
	
	$AktDatum = date("d.m.Y");
	$Form->DebugAusgabe(1,$AktDatum);	
	$Form->DebugAusgabe(1,strtotime($ANFDATUM));	
	$Form->DebugAusgabe(1,strtotime($AktDatum));	
	
	if (strtotime($ANFDATUM) < strtotime($AktDatum))
	{
		$Fehler .= 'Das Datum der Rückführungsaufforderung liegt in der Vergangenheit!!!';		
	}
	
	
	// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]=$Fehler;
		
		return $Erg;		
	}								
	
	$ANFNR = $ANFDATUMFELDER[2].''.$ANFDATUMFELDER[1].''.$ANFDATUMFELDER[0];
				
	$rsLFDNR = $DB->RecordSetOeffnen('SELECT NVL(MAX(RFA_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSANFORDERUNGEN WHERE RFA_NR='. $DB->FeldInhaltFormat('Z',$ANFNR));
	$LFDNR = $DB->FeldInhaltFormat('Z',$rsLFDNR->FeldInhalt('MAXLFDNR'))+1;
				
	$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGEN';
	$SQL .= '(RFA_NR,RFA_LFDNR,RFA_DATUM,RFA_BEMERKUNG,RFA_RFS_KEY,RFA_TYP,';
	$SQL .=' RFA_USER,RFA_USERDAT)';
	$SQL .= 'VALUES(';
	$SQL .= ' ' . $DB->FeldInhaltFormat('Z',$ANFNR,false);
	$SQL .= ',' . $DB->FeldInhaltFormat('Z',$LFDNR,false);	
	$SQL .= ',' . $DB->FeldInhaltFormat('D',$Datum,true);
	$SQL .= ',' . $DB->FeldInhaltFormat('T',$Bemerkung,true);
	$SQL .= ',0';
	$SQL .= ',1';
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';	
	$SQL .= ')';						

	if($DB->Ausfuehren($SQL)===false)
	{
		$Erg['Code']=5;
		$Erg['Zeile'] = $DS;
		$Erg['Fehlermeldung'][]='Fehler beim Schreiben der Rückführung '.$Datum . ' ' . $Bemerkung;
		//$Erg['Letzte Meldung']='Fehler beim Schreiben der Rückführung '.$Datum . ' ' . $Bemerkung;
		return $Erg;
	}
	else 
	{
		$Erg['Meldungen'][]='Rückführung Datum: '.$Datum .', Bemerkung: '.$Bemerkung.' wurde erfolgreich angelegt.';
	}

	$SQL = 'SELECT seq_RFA_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = $DB->RecordSetOeffnen($SQL);
	$KEY=$rsKey->FeldInhalt('KEY');
	
	$Erg['Anforderung'] = $KEY;
		
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);
	
	$DS = 1;
	
	while(!feof($fd))
	{
		$Daten = explode(";",$Zeile);		
		if($Daten[$Ueberschriften['ATUNR']]!='')
		{	
			//$Erg['Meldungen'][]='Zeile '.$DS.': Artikel '.$Daten[$Ueberschriften['ATUNR']].' neu anlegen.';			
			
			$SQL = ' SELECT * ';
			$SQL .= ' FROM Artikelstamm ';
			$SQL .= ' WHERE AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);			
			$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

			$rsAST = $DB->RecordSetOeffnen($SQL);
			if($rsAST->AnzahlDatensaetze()==0)
			{
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser nicht im Artikelstamm vorhanden ist.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser nicht im Artikelstamm vorhanden ist';
				
				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
			
			//Prüfen, ob der Artikel in dieser Rückführungsanforderung schon vorhanden ist
			$SQL = ' SELECT * ';
			$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGENPOS ';
			$SQL .= ' WHERE RFP_AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);			
			$SQL .= ' AND RFP_RFA_KEY = '.$DB->FeldInhaltFormat('N0',$KEY);			

			$rsRFP = $DB->RecordSetOeffnen($SQL);
			
			if($rsRFP->AnzahlDatensaetze()>0)
			{		
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser Artikel in der Rückführung schon vorhanden ist.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser nicht im Artikelstamm vorhanden ist';
				
				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
					
			if (!isset($Daten[$Ueberschriften['RUECKLIEFKZ']]) OR $Daten[$Ueberschriften['RUECKLIEFKZ']]=='')
			{	
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da fehlendes Rückführungskennzeichen.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';

				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;							
			}
			else 
			{	
				$SQL = ' SELECT * FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]);
				$rsRTYP = $DB->RecordSetOeffnen($SQL);								
				
				if($rsRTYP->AnzahlDatensaetze()==0)
				{
					$Erg['Code']=4;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da kein gültiges Rückführungskennzeichen.';
					//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';
	
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;							
				}
				
				$SQL = ' SELECT * ';
				$SQL .= ' FROM V_ARTIKEL_RUECKLIEFKZ WHERE AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);
				
				$rsAST = $DB->RecordSetOeffnen($SQL);
				if($rsAST->AnzahlDatensaetze()==0)
				{					
					//Rückführungskennzeichen anlegen
					$SQL = 'insert into artikelstamminfos ';
					$SQL .= '(ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) ';
					$SQL .= 'values (';
					$SQL .= '' .$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]).'';
					$SQL .= ', 122';
					$SQL .= ',' .$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]).'';
					$SQL .= ', 4 ';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ', SYSDATE)';								
	
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;
					//return $Erg;
					}				
				
				}
				elseif ($DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('RUECKLIEFKZ')) == $DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']])
						or $rsAST->FeldInhalt('GUELTIG') == '1')
				{
					$SQL = 'update artikelstamminfos set ';
					$SQL .= ' asi_wert ='.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]);
					$SQL .= ', asi_imq_id = 4';
					$SQL .= ', asi_user = \'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ', asi_userdat = SYSDATE';					
					$SQL .= ' where ASI_AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]).'';
					$SQL .= ' and ASI_AIT_ID = 122';														
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;
					//return $Erg;
					}
				}								
				else
				{
					$Erg['Code']=4;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da falsches Rückführungskennzeichen. (Aktuell: '.$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('RUECKLIEFKZ')).')';
					//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';
	
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;							
				}
			}
										
			$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGENPOS';
			$SQL .= '(RFP_RFA_KEY,RFP_AST_ATUNR,RFP_GESAMTMENGE,RFP_MINDESTBESTAND,RFP_RFS_KEY,';
			/*
			if (isset($Ueberschriften['BEMERKUNG']) AND $Daten[$Ueberschriften['BEMERKUNG']] !='')
			{
				$SQL.= ' RFP_BEMERKUNG,';
			}
			*/
			$SQL .=' RFP_USER,RFP_USERDAT)';
			$SQL .= 'VALUES(';
			$SQL .= ' '.$DB->FeldInhaltFormat('N0',$KEY);
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);
			$SQL .= ', '.$DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['GESAMTMENGE']]);
			$SQL .= ', '.$DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['MINDESTBESTAND']]);
			$SQL .= ', 0';
			/*
			if (isset($Ueberschriften['BEMERKUNG']) AND $Daten[$Ueberschriften['BEMERKUNG']] !='')
			{
				$SQL .= ', '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['BEMERKUNG']]);
			}
			*/
			$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				$Erg['Code']=5;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
				//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
				
				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
			else
			{				
				$Erg['Meldungen'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' wurde erfolgreich angelegt.';
			}		
		}
		
		$Zeile = fgets($fd);	
		$Zeile = trim($Zeile);
		$DS++;
		
	}	
	return $Erg;	
}

function import_4096($Version, $fd)
{
	global $AWISBenutzer;
	
	$Form = new awisFormular();
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array(), 'Fehlermeldung'=>array(), 'Anforderung'=>'');
	
	if($Version != "1.0")
	{
		//$Erg = array('Code'=>2, 'Letzte Meldung'=>'Falsche Dateiversion!', 'Meldungen'=>array());	
		
		$Erg['Code']=2;
		$Erg['Fehlermeldung'][]='Falsche Dateiversion';
		
		return $Erg;
	}
	
	//$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array());

	rewind($fd);
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);
	
	$Kopf = explode(";",$Zeile);		
	$Datum = $Kopf[2];
	$Bemerkung = $Kopf[3];	
	
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);	
	
	$Ueberschriften = explode(";",$Zeile);	
	$Ueberschriften = array_flip($Ueberschriften);	
	
	if(!isset($Ueberschriften['ATUNR']) or !isset($Ueberschriften['FILIALE']) or !isset($Ueberschriften['MENGE']) or !isset($Ueberschriften['RUECKLIEFKZ']))
	{
		//return(array('Code'=>1,'Letzte Meldung'=>'Falsche Ueberschrift','Zeile'=>0,'Meldungen'=>array()));
		
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Falsche Überschrift';
		
		return $Erg;
	}
	
	// Daten auf Vollständigkeit prüfen
	if($Datum == '')
	{
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Es wurde kein Datum angegeben!';
		
		return $Erg;		
	}
	
	$ANFDATUM = $Form->Format('D',$Datum,false);
	$ANFDATUMFELDER = explode('.', $ANFDATUM);
	
	$Fehler = '';
	
	if (!checkdate($ANFDATUMFELDER[1],$ANFDATUMFELDER[0],$ANFDATUMFELDER[2]))
	{
			$Fehler .= $AWISSprachKonserven['Fehler']['err_keingueltigesDatum'].'<br>';					
	}
	$Form->DebugAusgabe(1,$ANFDATUM, $ANFDATUMFELDER);
	
	
	$AktDatum = date("d.m.Y");
	$Form->DebugAusgabe(1,$AktDatum);	
	$Form->DebugAusgabe(1,strtotime($ANFDATUM));	
	$Form->DebugAusgabe(1,strtotime($AktDatum));	
	
	if (strtotime($ANFDATUM) < strtotime($AktDatum))
	{
		$Fehler .= 'Das Datum der Rückführungsaufforderung liegt in der Vergangenheit!!!';		
	}
	
	
	// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]=$Fehler;
		
		return $Erg;		
	}								
	
	$ANFNR = $ANFDATUMFELDER[2].''.$ANFDATUMFELDER[1].''.$ANFDATUMFELDER[0];
				
	$rsLFDNR = $DB->RecordSetOeffnen('SELECT NVL(MAX(RFA_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSANFORDERUNGEN WHERE RFA_NR='. $DB->FeldInhaltFormat('Z',$ANFNR));
	$LFDNR = $DB->FeldInhaltFormat('Z',$rsLFDNR->FeldInhalt('MAXLFDNR'))+1;
	
	//Neuen Rückführungskopf anlegen				
	$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGEN';
	$SQL .= '(RFA_NR,RFA_LFDNR,RFA_DATUM,RFA_BEMERKUNG,RFA_RFS_KEY,RFA_TYP,';
	$SQL .=' RFA_USER,RFA_USERDAT)';
	$SQL .= 'VALUES(';
	$SQL .= ' ' . $DB->FeldInhaltFormat('Z',$ANFNR,false);
	$SQL .= ',' . $DB->FeldInhaltFormat('Z',$LFDNR,false);	
	$SQL .= ',' . $DB->FeldInhaltFormat('D',$Datum,true);
	$SQL .= ',' . $DB->FeldInhaltFormat('T',$Bemerkung,true);
	$SQL .= ',0';
	$SQL .= ',2';
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';	
	$SQL .= ')';										

	if($DB->Ausfuehren($SQL)===false)
	{
		$Erg['Code']=5;
		$Erg['Zeile'] = $DS;
		$Erg['Fehlermeldung'][]='Fehler beim Schreiben der Rückführung '.$Datum . ' ' . $Bemerkung;
		
		return $Erg;
	}
	else 
	{
		$Erg['Meldungen'][]='Anforderung '.$Datum. ' '. $Bemerkung .' wurde neu angelegt.';
	}

	$SQL = 'SELECT seq_RFA_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = $DB->RecordSetOeffnen($SQL);
	$KEY=$rsKey->FeldInhalt('KEY');
	
	$Erg['Anforderung'] = $KEY;
		
	$Zeile = fgets($fd);
	$Zeile = trim($Zeile);

	$DS = 1;
	
	while(!feof($fd))
	{
		$Daten = explode(";",$Zeile);		
		if($Daten[$Ueberschriften['ATUNR']]!='')
		{	
			//$Erg['Meldungen'][]='Zeile '.$DS.': Artikel '.$Daten[$Ueberschriften['ATUNR']].' neu anlegen.';			
			
			$SQL = ' SELECT * ';
			$SQL .= ' FROM Artikelstamm ';
			$SQL .= ' WHERE AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);			
			$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

			$rsAST = $DB->RecordSetOeffnen($SQL);
			if($rsAST->AnzahlDatensaetze()==0)
			{
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser nicht im Artikelstamm vorhanden ist.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da dieser nicht im Artikelstamm vorhanden ist';

				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}				
			
			$SQL = ' SELECT * ';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID = '.$DB->FeldInhaltFormat('NO',$Daten[$Ueberschriften['FILIALE']]);			
			$SQL .= ' AND FIL_GRUPPE IS NOT NULL AND FIL_LAN_WWSKENN <> \'SUI\'';

			$rsFIL = $DB->RecordSetOeffnen($SQL);
			if($rsFIL->AnzahlDatensaetze()==0)
			{
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';

				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
						
			if (!isset($Daten[$Ueberschriften['RUECKLIEFKZ']]) OR $Daten[$Ueberschriften['RUECKLIEFKZ']]=='')
			{	
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' konnte nicht hinzugefügt werden, da fehlendes Rückführungskennzeichen.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';

				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;							
			}
			else 
			{	
				$SQL = ' SELECT * FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]);
				$rsRTYP = $DB->RecordSetOeffnen($SQL);								
				
				if($rsRTYP->AnzahlDatensaetze()==0)
				{
					$Erg['Code']=4;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', konnte nicht hinzugefügt werden, da kein gültiges Rückführungskennzeichen.';
					//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';
	
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;							
				}	
			
				$SQL = ' SELECT * ';
				$SQL .= ' FROM V_ARTIKEL_RUECKLIEFKZ WHERE AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);
				
				$rsAST = $DB->RecordSetOeffnen($SQL);
				if($rsAST->AnzahlDatensaetze()==0)
				{					
					//Rückführungskennzeichen anlegen
					$SQL = 'insert into artikelstamminfos ';
					$SQL .= '(ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) ';
					$SQL .= 'values (';
					$SQL .= '' .$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]).'';
					$SQL .= ', 122';
					$SQL .= ',' .$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]).'';
					$SQL .= ', 4 ';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ', SYSDATE)';								
	
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;
					//return $Erg;
					}				
				
				}
				elseif ($DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('RUECKLIEFKZ')) == $DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']])
						or $rsAST->FeldInhalt('GUELTIG') == '1')
				{
					$SQL = 'update artikelstamminfos set ';
					$SQL .= ' asi_wert ='.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RUECKLIEFKZ']]);
					$SQL .= ', asi_imq_id = 4';
					$SQL .= ', asi_user = \'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ', asi_userdat = SYSDATE';					
					$SQL .= ' where ASI_AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]).'';
					$SQL .= ' and ASI_AIT_ID = 122';														
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;
					//return $Erg;
					}
				}								
				else
				{
					$Erg['Code']=4;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' für Filiale '.$Daten[$Ueberschriften['FILIALE']].' konnte nicht hinzugefügt werden, da falsches Rückführungskennzeichen. (Aktuell: '.$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('RUECKLIEFKZ')).')';
					//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';
	
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;							
				}
			}
						
			$SQL = ' SELECT * ';
			$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGENPOS ';
			$SQL .= ' WHERE RFP_AST_ATUNR = '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);			
			$SQL .= ' AND RFP_RFA_KEY = '.$DB->FeldInhaltFormat('N0',$KEY);			

			$rsRFP = $DB->RecordSetOeffnen($SQL);
			
			if($rsRFP->AnzahlDatensaetze()==0)
			{																														
				$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGENPOS';
				$SQL .= '(RFP_RFA_KEY,RFP_AST_ATUNR,RFP_RFS_KEY,';
				$SQL .=' RFP_USER,RFP_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ' '.$DB->FeldInhaltFormat('N0',$KEY);
				$SQL .= ', '.$DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ATUNR']]);
				$SQL .= ', 0';
				$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', SYSDATE';
				$SQL .= ')';						
	
				if($DB->Ausfuehren($SQL)===false)
				{
					$Erg['Code']=5;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
					//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
					
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;
				//return $Erg;
				}
				
				$SQL = 'SELECT seq_RFP_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$KEY2=$rsKey->FeldInhalt('KEY');											
			}
			else 
			{
				$KEY2=$rsRFP->FeldInhalt('RFP_KEY');											
			}
			
			$SQL = ' SELECT * ';
			$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN ';
			$SQL .= ' WHERE RFM_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['FILIALE']]);
			$SQL .= ' AND RFM_RFP_KEY = '.$KEY2;			
						
			$rsRFM = $DB->RecordSetOeffnen($SQL);
			if($rsRFM->AnzahlDatensaetze()>0)
			{
				$Erg['Code']=4;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].' für Filiale '.$Daten[$Ueberschriften['FILIALE']].' konnte nicht hinzugefügt werden, da dieser bereits angelegt ist.';
				//$Erg['Letzte Meldung']='Zeile '.$DS.': Der Artikel '.$Daten[$Ueberschriften['ATUNR']].', für Filiale '.$Daten[$Ueberschriften['FILIALE']].'konnte nicht hinzugefügt werden, da die Filiale nicht vorhanden ist';

				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
						
			$SQL = 'INSERT INTO RUECKFUEHRUNGSANFPOSFILMENGEN';
			$SQL .= '(RFM_RFP_KEY,RFM_FIL_ID,RFM_MENGE,RFM_RFS_KEY,';
			$SQL .=' RFM_USER,RFM_USERDAT)';
			$SQL .= 'VALUES(';
			$SQL .= ' '.$DB->FeldInhaltFormat('N0',$KEY2);
			$SQL .= ', '.$DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['FILIALE']]);
			$SQL .= ', '.$DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['MENGE']]);
			$SQL .= ', 0';
			$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				$Erg['Code']=5;
				$Erg['Zeile'] = $DS;
				$Erg['Fehlermeldung'][]='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
				//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
				
				$Zeile = fgets($fd);	
				$Zeile = trim($Zeile);
				$DS++;
				continue;
				//return $Erg;
			}
			else
			{				
				$Erg['Meldungen'][]='Zeile '.$DS.': Artikel '.$Daten[$Ueberschriften['ATUNR']].' wurde erfolgreich angelegt.';								
			}
			
			$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS SET RFP_GESAMTMENGE=(SELECT SUM(RFM_MENGE) FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_RFP_KEY='.$KEY2.') WHERE RFP_KEY='.$KEY2;
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200810171114,$SQL,2);
			}
			
			$SQL = 'UPDATE rueckfuehrungsanforderungen SET';			
			$SQL .= ' RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1)';			
			$SQL .= ' WHERE RFA_key=0' . $KEY . '';
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200907210857,$SQL,2);
			}
			
		}
		
		$Zeile = fgets($fd);	
		$Zeile = trim($Zeile);
		$DS++;
	}
	
	return $Erg;
}

?>
