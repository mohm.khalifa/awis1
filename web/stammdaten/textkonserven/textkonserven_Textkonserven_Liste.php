<?php
global $XTX;
$SQL = 'Select * from textkonserven ';
$SQL .='where XTX_BEREICH in (';

foreach ($_POST['sucXTX_BEREICH'] as $Bereich){
   $SQL .= $XTX->DB->WertSetzen('XTX','T',$Bereich) .',';
}
$SQL = substr($SQL,0,-1);
$SQL .= ')';

$rsXTX = $XTX->DB->RecordSetOeffnen($SQL,$XTX->DB->Bindevariablen('XTX'));


$Sprachen = $_POST['sucXTX_SPRACHEN'];

foreach ($Sprachen as $Sprache){
    $Breiten["XTX_TEXT_$Sprache"] = 400;
}
$Breiten['XTX_KENNUNG'] = 150;


$XTX->Form->ZeileStart();
foreach ($Sprachen as $Sprache){
    $Inhalt = $XTX->VerfuegbareSprachenArray(false)[$Sprache];
    $Inhalt .= '<image class="lock" status="locked" id="lock_"' .$Sprache.'" src="/bilder/cmd_schloss_zu.png" style="height: 20px; width: 20px;"'."sprache='$Sprache'".'>';
    $XTX->Form->Erstelle_Liste_Ueberschrift($Inhalt,$Breiten["XTX_TEXT_$Sprache"]);
}


if(isset($_POST['txtXTX_SYSTEMINFOS'])){
    $XTX->Form->Erstelle_Liste_Ueberschrift($XTX->AWISSprachKonserven['XTX']["XTX_KENNUNG"],$Breiten["XTX_KENNUNG"]);
}

$XTX->Form->ZeileEnde();
$DS = 0;
while(!$rsXTX->EOF()){
    $XTX->Form->ZeileStart();
    foreach ($Sprachen as $Sprache){
        $Feldname = "XTX_TEXT_$Sprache";
        $TextFeldName = $rsXTX->FeldInhalt('XTX_BEREICH').'_Y_'.$rsXTX->FeldInhalt('XTX_KENNUNG').'_Y_'.$Sprache;
        $TextFeldNameDE = $rsXTX->FeldInhalt('XTX_BEREICH').'_Y_'.$rsXTX->FeldInhalt('XTX_KENNUNG').'_Y_'.'DE';
        //Erstelle_Listenfeld hat nicht alle Parameter von Erstelle_TextFeld.
        $Klassenzusatz = ($DS%2)==0?'ListenfeldHell':'ListenfeldDunkel';
        $XTX->Form->Erstelle_TextFeld($TextFeldName,$rsXTX->FeldInhalt($Feldname),$Breiten[$Feldname]/6,$Breiten[$Feldname],true,'','','','T','L','','','','','',"sprache='$Sprache' FeldNameDE='$TextFeldNameDE'",$Klassenzusatz);
    }

    if(isset($_POST['txtXTX_SYSTEMINFOS'])){
        $XTX->Form->Erstelle_ListenFeld('XTX_KENNUNG',$rsXTX->FeldOderPOST('XTX_KENNUNG'),$Breiten['XTX_KENNUNG']/6,$Breiten['XTX_KENNUNG'],false,($DS%2));
    }


    $XTX->Form->ZeileEnde();

    $DS++;
    $rsXTX->DSWeiter();
}
