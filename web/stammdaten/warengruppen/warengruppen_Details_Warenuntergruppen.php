<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once 'warengruppen_funktionen.inc';
try {

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('WGR', '%');
    $TextKonserven[] = array('WUG', '%');
    $TextKonserven[] = array('WGI', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $WUG = new warengruppen_funktionen();

    $Funktionen = new warengruppen_funktionen();

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_WGI'));

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht621 = $AWISBenutzer->HatDasRecht(621);

    $awisWerkzeug = new awisWerkzeuge();

    //*********************************************************
    //* Sortierung
    //*********************************************************

    if (!isset($_GET['SortWUG'])) {
        if (isset($Param['ORDERWUG']) AND $Param['ORDERWUG'] != '') {
            $ORDERBY = 'ORDER BY ' . $Param['ORDERWUG'];
        } else {
            $ORDERBY = ' ORDER BY WUG_ID asc';
            $Param['ORDERWUG'] = 'WUG_ID asc';
        }
    } else {
        $Param['ORDERWUG'] = str_replace('~', ' DESC ', $_GET['SortWUG']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDERWUG'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************

    $Bedingung = $WUG->erstelleWUGBedingung($Param);

    $SQL = 'SELECT warenuntergruppen.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM warenuntergruppen';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY2 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('WUG', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $rsWUG = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('WUG'));

    $AWISBenutzer->ParameterSchreiben('Formular_WGI', serialize($Param));

    //********************************************************
    // Daten anzeigen
    //********************************************************
    if ($rsWUG->AnzahlDatensaetze() > 1 AND !isset($_GET['WUG_ID']))                        // Liste anzeigen
    {
        $DetailAnsicht = false;

        $SpaltenBreite = array();
        $SpaltenBreite['WUG_ID'] = 100;
        $SpaltenBreite['WUG_BEZEICHNUNG'] = 400;

        $Form->ZeileStart();
        $Link = './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=WUG_ID' . ((isset($_GET['SortWUG']) AND ($_GET['SortWUG'] == 'WUG_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WUG']['WUG_ID'], $SpaltenBreite['WUG_ID'], '', $Link);
        $Link = './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=WUG_BEZEICHNUNG' . ((isset($_GET['SortWUG']) AND ($_GET['SortWUG'] == 'WUG_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WUG']['WUG_BEZEICHNUNG'], $SpaltenBreite['WUG_BEZEICHNUNG'], '', $Link);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsWUG->EOF()) {
            $Form->ZeileStart();

            $Link = './warengruppen_Main.php?cmdAktion=Details&WGR_ID=' . $AWIS_KEY1 . '&WUG_ID=' . $rsWUG->FeldInhalt('WUG_ID') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Form->Erstelle_ListenFeld('WUG_ID', $rsWUG->FeldInhalt('WUG_ID'), 0, $SpaltenBreite['WUG_ID'], false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('WUG_BEZEICHNUNG', $rsWUG->FeldInhalt('WUG_BEZEICHNUNG'), 0, $SpaltenBreite['WUG_BEZEICHNUNG'], false, ($DS % 2), '', '', 'T', 'L');
            $Form->ZeileEnde();

            $rsWUG->DSWeiter();
            $DS++;
        }

        $Link = './warengruppen_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else //Ein DS
    {

        $DetailAnsicht = true;
        $AWIS_KEY2 = $rsWUG->FeldInhalt('WUG_ID');
        $Param['WUG_ID'] = $AWIS_KEY2;

        $Form->Erstelle_HiddenFeld('WUG_ID', $AWIS_KEY2);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./warengruppen_Main.php?cmdAktion=Details" . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Ebene3'])?'&Ebene3=' . $_GET['Ebene3']:'') . (isset($_GET['WGR_ID'])?'&WGR_ID=' . $_GET['WGR_ID']:'') . "&WUGListe=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsWUG->FeldInhalt('WUG_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsWUG->FeldInhalt('WUG_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht621 & 2) != 0);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WUG']['WUG_ID'] . ':', 170);
        $Form->Erstelle_TextFeld('!WUG_ID', $rsWUG->FeldInhalt('WUG_ID'), 10, 600, false, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WUG']['WUG_BEZEICHNUNG'] . ':', 170);
        $Form->Erstelle_TextFeld('!WUG_BEZEICHNUNG', $rsWUG->FeldInhalt('WUG_BEZEICHNUNG'), 10, 600, false, '', '', '', 'T');
        $Form->ZeileEnde();

        $RegisterSeite = (isset($_GET['Ebene3'])?$_GET['Ebene3']:(isset($_POST['Ebene3'])?$_POST['Ebene3']:'Warenuntergruppeninfos'));

        $Register = new awisRegister(623);
        $Register->ZeichneRegister($RegisterSeite);
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>