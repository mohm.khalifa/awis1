<?php
global $con;
global $Recht10001;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZUB','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','Altteilwert');
$TextKonserven[]=array('Wort','AktuellesSortiment');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','KeineDatenVorhanden');
$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10001 = awisBenutzerRecht($con,10001,$AWISBenutzer->BenutzerName());		// Artikelstamm
$Recht10002 = awisBenutzerRecht($con,10002,$AWISBenutzer->BenutzerName());		// Bestellungen
if(($Recht10001&32)==0)
{
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	die();
}

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$SQL = 'SELECT Zukaufbestellungen.*, ZWA_WANR';
$SQL .= ' FROM Zukaufbestellungen ';
$SQL .= ' LEFT OUTER JOIN ZukaufWerkstattAuftraege ON ZUB_KEY = ZWA_ZUB_KEY';
$SQL .= ' WHERE ZUB_ZLA_KEY=0'.$AWIS_KEY1;

if(!isset($_GET['SSort']))
{
	$SQL .= ' ORDER BY ZUB_BESTELLDATUM DESC';
}
else
{
	$SortFelder = explode(';',$_GET['SSort']);
	$OrderBy = '';
	foreach($SortFelder AS $SortFeld)
	{
		$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
	}
	$SQL .= ($OrderBy==''?'':' ORDER BY '.$OrderBy);
}

$rsZUB = awisOpenRecordset($con, $SQL);
$rsZUBZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

if($rsZUBZeilen>0)
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZUB_BESTELLDATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_BESTELLDATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'],120,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZUB_FIL_ID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_FIL_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'],80,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZUB_BESTELLMENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_BESTELLMENGE'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'],150,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZUB_EINHEITENPROPREIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_EINHEITENPROPREIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'],150,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZUB_PREISBRUTTO'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_PREISBRUTTO'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'],200,'',$Link);
	if(($Recht10002&32)==32)
	{
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_PREISANGELIEFERT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_PREISANGELIEFERT'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'],200,'',$Link);
	}
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZUBZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZUBZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe&Block='.$i.(isset($_GET['SSort'])?'&Sort='.$_GET['SSort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	$Menge = 0;
	$Vorgaenge = 0;

	for($ZUBZeile=$StartZeile;$ZUBZeile<$rsZUBZeilen and $ZUBZeile<$StartZeile+$MaxDSAnzahl;$ZUBZeile++)
	{
		awis_FORM_ZeileStart();
		$Link='/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$rsZUB['ZUB_KEY'][$ZUBZeile];
		awis_FORM_Erstelle_ListenFeld('ZUB_BESTELLDATUM',$rsZUB['ZUB_BESTELLDATUM'][$ZUBZeile],0,120,false,($ZUBZeile%2),'',$Link,'D');
		$Link='/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsZUB['ZUB_FIL_ID'][$ZUBZeile];	// TODO: Sp�ter auf die Hersteller
		awis_FORM_Erstelle_ListenFeld('ZUB_FIL_ID',$rsZUB['ZUB_FIL_ID'][$ZUBZeile],0,80,false,($ZUBZeile%2),'',$Link,'T','L');
		$Link = '';
		$Style='';
		if(stripos($rsZUB['ZUB_ARTIKELBEZEICHNUNG'][$ZUBZeile],'ALTTEILWERT')!==false)
		{
			$Style='color:#F00FFF;font-weight:bold;';
		}
		awis_FORM_Erstelle_ListenFeld('ZUB_BESTELLMENGE',$rsZUB['ZUB_BESTELLMENGE'][$ZUBZeile],0,150,false,($ZUBZeile%2),$Style,$Link,'Z','L',$AWISSprachKonserven['Wort']['Altteilwert']);
		awis_FORM_Erstelle_ListenFeld('ZUB_EINHEITENPROPREIS',$rsZUB['ZUB_EINHEITENPROPREIS'][$ZUBZeile],0,150,false,($ZUBZeile%2),'',$Link,'Z','L');
		awis_FORM_Erstelle_ListenFeld('ZUB_PREISBRUTTO',$rsZUB['ZUB_PREISBRUTTO'][$ZUBZeile],0,200,false,($ZUBZeile%2),'',$Link,'N2','L');
		if(($Recht10002&32)==32)
		{
			awis_FORM_Erstelle_ListenFeld('ZUB_PREISANGELIEFERT',$rsZUB['ZUB_PREISANGELIEFERT'][$ZUBZeile],0,200,false,($ZUBZeile%2),'',$Link,'N2','L');
		}


		if(stripos($rsZUB['ZUB_ARTIKELBEZEICHNUNG'][$ZUBZeile],'ALTTEILWERT')===false)
		{
			$Menge += $rsZUB['ZUB_BESTELLMENGE'][$ZUBZeile];
			$Vorgaenge++;
		}

		awis_FORM_ZeileEnde();
	}
		awis_FORM_ZeileStart();
		$Text = $AWISSprachKonserven['Wort']['ZusammenfassungBestellungen'];
		$Text = str_replace('#MENGE#',$Menge,$Text);
		$Text = str_replace('#VORGAENGE#',$Vorgaenge,$Text);
		awis_FORM_Erstelle_ListenFeld('FILLER1',$Text,0,920,false,($ZUBZeile%2),'','','');
		awis_FORM_ZeileEnde();

		awis_FORM_FormularEnde();
}
else
{
	awis_FORM_Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
}

awis_FORM_FormularEnde();

?>