<?php
/****************************************************************************************************
* 
* 	Lexikonsuche
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Sept 2004
* 
****************************************************************************************************/
// Variablen

if(isset($_POST['cmdNeu_x']))
{
	include('./lexikon_Bearbeiten.php');
	die();
}

global $RechteStufe;
global $AWISBenutzer;
global $con;
global $awisRSZeilen;

$Zeit = awis_ZeitMessung(0);

$SQL = 'SELECT * FROM Lexikon';
$Bedingung='';

$Suchbegriff = (isset($_POST['txtSuchbegriff'])?$_POST['txtSuchbegriff']:'') . (isset($_GET['txtSuchbegriff'])?$_GET['txtSuchbegriff']:'');

if((isset($_POST['txtBegriff']) AND $_POST['txtBegriff']!='') 
	OR (isset($_GET['txtBegriff']) AND $_GET['txtBegriff']!=''))
{
	$Bedingung .= " OR UPPER(LEX_Begriff) " . awisLIKEoderIST('%'.$Suchbegriff.'%',TRUE,FALSE,FALSE,0) . "";
}
if((isset($_POST['txtAbk']) AND $_POST['txtAbk']!='') 
	OR (isset($_GET['txtBegriff']) AND $_GET['txtBegriff']!=''))
{
	$Bedingung .= " OR UPPER(LEX_Abkuerzung) " . awisLIKEoderIST($Suchbegriff,TRUE,FALSE,FALSE,0) . "";
}
if((isset($_POST['txtVolltext']) AND $_POST['txtVolltext']!='')
	OR (isset($_GET['txtBegriff']) AND $_GET['txtBegriff']!=''))
{
	$Bedingung .= " OR UPPER(LEX_Beschreibung) " . awisLIKEoderIST('%' . $Suchbegriff . '%',TRUE,FALSE,FALSE,0) . "";
}
if((isset($_POST['txtSammelBegriff']) AND $_POST['txtSammelBegriff']!='')
	OR (isset($_GET['txtSammelBegriff']) AND $_GET['txtSammelBegriff']!=''))
{
	$Bedingung .= " OR UPPER(LEX_Sammelbegriffe) " . awisLIKEoderIST('%' . $Suchbegriff . '%',TRUE,FALSE,FALSE,0) . "";
}
if((isset($_POST['txtHersteller']) AND $_POST['txtHersteller']!='')
	OR (isset($_GET['txtHersteller']) AND $_GET['txtHersteller']!=''))
{
	$Bedingung .= " OR UPPER(LEX_Hersteller) " . awisLIKEoderIST('%' . $Suchbegriff . '%',TRUE,FALSE,FALSE,0) . "";
}


if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

$SQL .= ' ORDER BY LEX_BEGRIFF';

$MaxRec = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
$SQL = "SELECT * FROM (" . $SQL . ") WHERE ROWNUM <= " . $MaxRec;

$rsLexikon = awisOpenRecordset($con,$SQL);
$rsLexikonZeilen = $awisRSZeilen;

if($rsLexikonZeilen==0)		// Keine Daten
{
	echo 'Zu dem Suchbegriff ' . $_POST['txtSuchbegriff'] . ' wurde kein Eintrag gefunden.';
}
else
{
	// �berschrift aufbauen

	if(($RechteStufe&2)==2)		// Bearbeitung
	{
		echo '<form name=frmLexikon method=post action=./lexikon_Bearbeiten.php>';
	}
	echo "<table  width=100% id=DatenTabelle border=1><tr>";

	if(($RechteStufe&2)==2)		// Bearbeitung
	{
		echo '<td id=FeldBez></td>';
	}
	echo "<td id=FeldBez>Begriff</td>";
	echo "<td id=FeldBez>Abk�rzung</td>";
	echo "<td id=FeldBez>Beschreibung</td>";
	echo "<td id=FeldBez>Hersteller</td>";
	echo "<td id=FeldBez>Sammelbegriff(e)</td>";
	echo "<td id=FeldBez>Info</td>";
	
	echo "</tr>";
	
	for($i=0;$i<$rsLexikonZeilen;$i++)
	{
		echo '<tr>';
		if(($RechteStufe&2)==2)	
		{
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><input type=image name=cmdEdit_' . $rsLexikon['LEX_KEY'][$i] . ' src=/bilder/aendern.png></td>';
		}
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsLexikon['LEX_BEGRIFF'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsLexikon['LEX_ABKUERZUNG'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsLexikon['LEX_BESCHREIBUNG'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsLexikon['LEX_HERSTELLER'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsLexikon['LEX_SAMMELBEGRIFFE'][$i] . '</td>';

		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>';
		if($rsLexikon['LEX_ABBILDUNG'][$i]!='' OR $rsLexikon['LEX_ANHANG'][$i]!='')
		{
			if($rsLexikon['LEX_ANHANG'][$i]!='')
			{
				echo '<a href=' . awis_BenutzerParameter($con,'PfadLexikonAnhang' ,$AWISBenutzer->BenutzerName()) . $rsLexikon['LEX_ANHANG'][$i] . '><img src=/bilder/pdf.png border=0></a>' ;
			}
			
			if($rsLexikon['LEX_ABBILDUNG'][$i]!='')
			{
				echo '<br><a href=' . awis_BenutzerParameter($con,'PfadLexikonAbbildung' ,$AWISBenutzer->BenutzerName()) . $rsLexikon['LEX_ABBILDUNG'][$i] . '><img src=/bilder/abbildung.png border=0 alt=\'Bild anzeigen\'></a>' ;
			}
		}
		echo '</td></tr>';
		
	}
		
	echo "</table>";

	if(($RechteStufe&4)==4)
	{
		echo "<input type=image accesskey=n name=cmdNeu title='Hinzuf�gen (Alt+n)' src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}

	if(($RechteStufe&2)==2)		// Bearbeitung
	{
		echo '</form>';
	}

	echo '<br><font size=2>' . $rsLexikonZeilen . ' Datens�tze gefunden. Verarbeitungszeit: ' . sprintf('%0.6f',awis_ZeitMessung(1)) . ' Sekunden. Max. werden ' . $MaxRec . ' S�tze gezeigt.</font>';

}

?>

