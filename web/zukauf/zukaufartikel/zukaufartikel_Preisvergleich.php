<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('ZLA','ZLA_*');
	$TextKonserven[]=array('LIE','LIE_*');
	$TextKonserven[]=array('WGR','WGR_ID');
	$TextKonserven[]=array('WUG','WUG*');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10016 = $AWISBenutzer->HatDasRecht(10016);
	if($Recht10016==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Zukauf_Preisvergleich'));
	
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdWeiter_x']) OR isset($_POST['cmdExport_x']))
	{
		$Param = array();
		$Param['ZLH_KEY']=isset($_POST['txtZLH_KEY'])?$_POST['txtZLH_KEY']:array();
		$Param['WUG_ID']=isset($_POST['txtWUG_ID'])?$_POST['txtWUG_ID']:array();
		$Param['LIE_NR']=isset($_POST['txtLIE_NR'])?$_POST['txtLIE_NR']:array();
		$Param['ABVERKAUF']=isset($_POST['txtABVERKAUF'])?$_POST['txtABVERKAUF']:'';
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['ZLH_KEY']))
		{
		    $Param['ZLH_KEY']=array();
		    $Param['WUG_ID']=array();
		    $Param['LIE_NR']=array();
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	
	if(isset($_POST['cmdExport_x']))
	{
	    // Daten exportieren
	    echo '<script type="text/javascript">';
	    echo '$.ajax({method: "POST",';
	    echo ' url: "./zukaufartikel_preisvergleich_export.php"';
	    echo '})';
	    echo '.done(function( msg ) {';
	    echo ' $("#ZUKDatenExport").html(msg);';
	    echo '});';
	     
	    echo '</script>';
	}
	
	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZLA_ARTIKELNUMMER';
			$Param['ORDER']='ZLA_ARTIKELNUMMER';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
    if($Bedingung=='')
    {
        $Bedingung = ' AND ZAL_KEY = -1';       // Leere Daten erzwingen, wenn (noch) keine Bedingungen angegeben wurden
        $Bedingung .= ' AND AST_WUG_KEY = -1';       // Leere Daten erzwingen, wenn (noch) keine Bedingungen angegeben wurden
    }
    
	$SQL = ' SELECT * FROM (SELECT ZAL_LIE_NR,ZAP_VKPR,ZAP_EK, WUG_ID, WUG_BEZEICHNUNG';
	$SQL .= ' FROM ZUKAUFARTIKEL';
	$SQL .= ' INNER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZAL_ZLA_KEY = ZLA_KEY';
	$SQL .= ' INNER JOIN Lieferanten ON ZAL_LIE_NR = LIE_NR';
	$SQL .= ' INNER JOIN v_ZukaufArtikelPreise ON ZAP_ZAL_KEY = ZAL_KEY';
	$SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = ZLA_AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN WARENUNTERGRUPPEN ON WUG_KEY = AST_WUG_KEY ';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$SQL .= ' ) PIVOT ( AVG((ZAP_VKPR - ZAP_EK) / ZAP_VKPR) AS RABATT FOR(ZAL_LIE_NR) ';
	$SQL .= ' IN (';
	if(isset($Param['LIE_NR']) AND !empty($Param['LIE_NR']))
	{
	    $i=0;
	    foreach($Param['LIE_NR'] AS $LIE_NR)
	    {
            $SQL .= (($i++==0)?"'":",'") . $LIE_NR . "'";
	    }
	}
	$SQL .= '))';
    $SQL .= ' ORDER BY 1';
    
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsZLA = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('ZLA'));
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name="frmZukaufartikelPreisvergleich" action="./zukaufartikel_Main.php?cmdAktion=Preisvergleich" method="POST">';

	$Form->Formular_Start();

	// Parameter
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':', 200);
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG ';
	$SQL .= ' FROM Zukauflieferantenhersteller';
	$SQL .= ' ORDER BY 2';
	$Form->Erstelle_MehrfachSelectFeld('ZLH_KEY', $Param['ZLH_KEY'],300,true, $SQL, '', '', 1, '', array(),'','',array(),'','','',$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NR'].':', 200,'','','');
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM LIEFERANTEN';
	$SQL .= ' INNER JOIN LIEFERANTENINFOS ON LIE_NR = LIN_LIE_NR AND LIN_ITY_KEY = 1 AND LIN_WERT = 1';
	$SQL .= ' ORDER BY 2';
	$Form->Erstelle_MehrfachSelectFeld('LIE_NR', $Param['LIE_NR'],300,true, $SQL, '', '', 1, '', array(),'','',array(),'','','',$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WGR']['WGR_ID'].':', 200,'','','');
	$SQL = 'SELECT WUG_KEY, WUG_ID || \' - \' || WUG_BEZEICHNUNG ';
	$SQL .= ' FROM WARENUNTERGRUPPEN';
	$SQL .= ' WHERE WUG_WGR_ID = \'03\'';
	$SQL .= ' ORDER BY 2';
	$Form->Erstelle_MehrfachSelectFeld('WUG_ID', $Param['WUG_ID'],300,true, $SQL, '', '', 1, '', array(),'','',array(),'','','',$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Bestellte Artikel:', 200,'','','');
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	$Form->Erstelle_SelectFeld('ABVERKAUF',$Param['ABVERKAUF'],200,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
	
	$Form->ZeileEnde();
	
	$Form->Trennzeile();
	// Daten
	
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WUG']['WUG_ID'], 100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WUG']['WUG_BEZEICHNUNG'], 300);
    for($i=3;$i<=$rsZLA->AnzahlSpalten();$i++)
    {
        $Form->Erstelle_Liste_Ueberschrift(substr($rsZLA->FeldInfo($i, 'Name'),1,4), 150);
    }
    $Form->ZeileEnde();
    $DS=0;
	while(!$rsZLA->EOF())
	{
	   $Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	   $Form->Erstelle_ListenFeld('#WUG', $rsZLA->FeldInhalt('WUG_ID'), 0, 100, false, ($DS%2));
	   $Form->Erstelle_ListenFeld('#WUG_BEZEICHNUNG', $rsZLA->FeldInhalt('WUG_BEZEICHNUNG'), 0, 300, false, ($DS%2));
	   for($i=3;$i<=$rsZLA->AnzahlSpalten();$i++)
	   {
	       $Form->Erstelle_ListenFeld('#RABATT', $rsZLA->FeldInhalt($i), 0, 150,false,($DS%2),'','','N4');	       
	   }
	   $Form->ZeileEnde();
	   $rsZLA->DSWeiter();
	   $DS++;
	}
	
	if(isset($_POST['cmdExport_x']))
	{
	    $Form->ZeileStart();
	    echo '<div id="ZUKDatenExport"><img src="/bilder/ajax-loader.gif" border=0>&nbsp;&nbsp;Export wird ausgef&uuml;hrt...</div>';
	    $Form->ZeileEnde();
	}
	
	$Form->Formular_Ende();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	$Form->Schaltflaeche('image', 'cmdExport', '', '/bilder/cmd_export_xls.png', $AWISSprachKonserven['Wort']['lbl_export'], 'X');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);

	$AWISBenutzer->ParameterSchreiben('Formular_Zukauf_Preisvergleich',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
	
	if(isset($Param['ZLH_KEY']) AND !empty($Param['ZLH_KEY']))
	{
	    $Bedingung .= ' AND (';
	    $TeilBedingung = '';
	    foreach($Param['ZLH_KEY'] AS $ZLH_KEY)
	    {
            $TeilBedingung .= ' OR ZLA_ZLH_KEY = ' . $DB->WertSetzen('ZLA', 'N0', $ZLH_KEY);
	    }
	    $Bedingung .= substr($TeilBedingung,4) . ')';
	}
	

	if(isset($Param['LIE_NR']) AND !empty($Param['LIE_NR']))
	{
	    $Bedingung .= ' AND (';
	    $TeilBedingung = '';
	    foreach($Param['LIE_NR'] AS $LIE_NR)
	    {
            $TeilBedingung .= ' OR ZAL_LIE_NR = ' . $DB->WertSetzen('ZLA', 'T', $LIE_NR);
	    }
	    $Bedingung .= substr($TeilBedingung,4) . ')';
	}

	if(isset($Param['WUG_ID']) AND !empty($Param['WUG_ID']))
	{
	    $Bedingung .= ' AND (AST_WUG_KEY IS NULL OR (';
	    $TeilBedingung = '';
	    foreach($Param['WUG_ID'] AS $WUG_ID)
	    {
            $TeilBedingung .= ' OR AST_WUG_KEY = ' . $DB->WertSetzen('ZLA', 'T', $WUG_ID);
	    }
	    $Bedingung .= substr($TeilBedingung,4) . '))';
	}

	if(isset($Param['ABVERKAUF']) AND $Param['ABVERKAUF']!='')
	{
	    $Bedingung .= ' AND '.($Param['ABVERKAUF']==0?'NOT':'').' EXISTS(SELECT * FROM zukaufbestellungen WHERE zub_zla_key = zla_key)';
	}
	
	if($Bedingung!='')
	{
	    $Bedingung .= ' AND ZAP_VKPR > 0';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>