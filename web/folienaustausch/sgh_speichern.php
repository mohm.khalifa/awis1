<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $Key;


$TextKonserven=array();
$TextKonserven[]=array('VVS','*');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_Hilfe');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');

try
{
   $Form = new awisFormular();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
   $AWISBenutzer = awisBenutzer::Init();

   $flagSpeichereBetragsKuerzung = false;
   $flag=false;
   $abgleichsGrund = "";


   $Form->DebugAusgabe(1,$_POST);

        foreach($_POST as $key => $value)
        {
           if ($_POST[$key] == 'on')
           {
               $Felder = explode(';',$Form->NameInArray($_POST, 'txtAuswahl_',1,1));
               $Form->DebugAusgabe(1,$Felder);
               $ZeichenLaenge = strlen($Felder[0]);

               $ZeichenLaenge - 10;

               $Key = substr($Felder[0],11,$ZeichenLaenge);

               $flag = true;
           }
           else
           {
               
           }
         }

         if(isset($_POST['txtAbgleichsgrund']))
         {
             if($_POST['txtAbgleichsgrund'] == 6 || $_POST['txtAbgleichsgrund'] == 7)
             {
                $flag = false;
             }
             elseif ($_POST['txtAbgleichsgrund'] == 8)		// FZ EDIT vom 20120823: EDV-Anforderung v. Julia (Zeile hinzugefügt)
             {												// FZ EDIT vom 20120823: EDV-Anforderung v. Julia (Zeile hinzugefügt)
             	$flag = true;								// FZ EDIT vom 20120823: EDV-Anforderung v. Julia (Zeile hinzugefügt)
             }												// FZ EDIT vom 20120823: EDV-Anforderung v. Julia (Zeile hinzugefügt)
         }


         if(isset($_POST['txtAbgleichsgrund']))
         {
           $abgleichsGrund = $_POST['txtAbgleichsgrund'];
         }
         elseif(isset($_POST['txtAbgleichsgrundSpeichern']))
         {
             $abgleichsGrund = $_POST['txtAbgleichsgrundSpeichern'];
             $flagSpeichereBetragsKuerzung = true;
             $flag = true;
        }

         if($flag == true)
         {
           //Doppelter Begründungseintrag
           if($abgleichsGrund == 2 || $abgleichsGrund == 3 || $abgleichsGrund == 4 || $abgleichsGrund == 5)
           {
             
             $SQL = "SELECT * FROM SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO', $_POST['txtKDSKEY']);

             $rsDienstleister = $DB->RecordSetOeffnen($SQL);

             $SQL = "INSERT INTO SGHDIENSTLEISTERABGL (SGS_RNRSGH,SGS_RNRDAT,SGS_RNRTRM,SGS_FILNR,";
             $SQL .= "SGS_WANR,SGS_EINBAUDATUM,SGS_ATUNR,SGS_MENGE,SGS_BETRAG,SGS_KFZKENNZ,";
             $SQL .= "SGS_MAPPE,SGS_LIEFERANTENNR,SGS_USER,SGS_USERDAT) VALUES (";
             $SQL .= ' ' . $DB->FeldInhaltFormat('T',
             $rsDienstleister->FeldInhalt('SGD_RNRSGH'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_RNRDAT'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_RNRTRM'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_FILNR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_WANR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_EINBAUDATUM'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_ATUNR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsDienstleister->FeldInhalt('SGD_MENGE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsDienstleister->FeldInhalt('SGD_BETRAG'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_KFZKENNZ'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_MAPPE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_LIEFERANTENNR'),false);
             $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
             $SQL .= ',SYSDATE';
             $SQL .= ')';

             if($DB->Ausfuehren($SQL)===false) {
             }

	     $SQL = "SELECT seq_SGS_KEY.CurrVal AS KEY FROM DUAL";
	     $rsKey = $DB->RecordSetOeffnen($SQL);
             	
             $rsSGS_KEY = $rsKey->FeldInhalt('KEY');

             $SQL = "Select * from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO', $Key);

             $rsKassendaten = $DB->RecordSetOeffnen($SQL);

             $SQL = "INSERT INTO SGHKASSENDATENABGL (SKS_FILID,SKS_DATUM,SKS_ZEIT,";
             $SQL .= "SKS_BSA,SKS_WANR,SKS_ARTNR,SKS_MENGE,SKS_UMSATZ,";
             $SQL .= "SKS_KFZKENNZ,SKS_DWHAUDIT,SKS_DWHINSDATE,SKS_WAGENDAT,SKS_USER,SKS_USERDAT) VALUES (";
             $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_FILID'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DATUM'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ZEIT'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_BSA'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_WANR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ARTNR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_MENGE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsKassendaten->FeldInhalt('SGK_UMSATZ'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_KFZKENNZ'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_DWHAUDIT'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DWHINSDATE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_WAGENDAT'),false);
             $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
             $SQL .= ',SYSDATE';
             $SQL .= ')';

             if($DB->Ausfuehren($SQL)===false) {
             }
		
	     $SQL = "SELECT seq_SKS_KEY.CurrVal AS KEY FROM DUAL";
	     $rsKey = $DB->RecordSetOeffnen($SQL);
             	
             $rsSKS_KEY = $rsKey->FeldInhalt('KEY');

              $SQL = "INSERT INTO SGHABGLEICH (SGA_SGS_KEY,SGA_SKS_KEY,SGA_TYP,";
              $SQL .= "SGA_ABGLEICHDATUM,SGA_USER,SGA_USERDAT) VALUES (";
              $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
              $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsSKS_KEY,false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$abgleichsGrund,false);
              $SQL .= ',SYSDATE';
              $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
              $SQL .= ',SYSDATE';
              $SQL .= ')';

              if($DB->Ausfuehren($SQL)===false) {
              }

              if(isset($_POST['txtAuswahlBegruendung']))
              {
              if($_POST['txtAuswahlBegruendung'] == 'on' && $_POST['txtBegruendung'] != "")
              {
                //Speichere Begruendung
                $SQL = "INSERT INTO SGHBEGRUENDUNG (SBG_SGS_KEY,SBG_BEGRUENDUNG,";
                $SQL .= "SBG_USER,SBG_USERDAT) VALUES (";
                $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtBegruendung'],false);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($DB->Ausfuehren($SQL)===false) {
                }
              }
              }
              

             
              $SQL = "Delete from SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO',$rsDienstleister->FeldInhalt('SGD_KEY'));


              if($DB->Ausfuehren($SQL)===false) {
              }

              $SQL = "Delete from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_KEY'));

              if($DB->Ausfuehren($SQL)===false) {
              }

              $DETAILANSICHTSGH = true;
              $DETAILANSICHTKASSENDATEN = false;
              $FLAGKEINEAUSWAHL = false;

            }
            elseif($abgleichsGrund == 8)
            {
              $FLAGBETRAGSKUERZUNG = true;

              if($flagSpeichereBetragsKuerzung == true)
              {
              
              $SQL = "SELECT * FROM SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO', $_POST['txtSGHKEY']);

              $rsDienstleister = $DB->RecordSetOeffnen($SQL);

              $Differenz = 0.0;
              $BetragOLD = str_replace(',','.',$_POST['oldSGD_BETRAG']);
              $BetragNeu = str_replace(',','.',$_POST['txtSGD_BETRAG']);

              $Differenz = $BetragOLD - $BetragNeu;

              $SQL = "INSERT INTO SGHDIENSTLEISTERABGL (SGS_RNRSGH,SGS_RNRDAT,SGS_RNRTRM,SGS_FILNR,";
              $SQL .= "SGS_WANR,SGS_EINBAUDATUM,SGS_ATUNR,SGS_MENGE,SGS_BETRAG,SGS_BETRAGGEKUERZT,SGS_KFZKENNZ,";
              $SQL .= "SGS_MAPPE,SGS_LIEFERANTENNR,SGS_USER,SGS_USERDAT) VALUES (";
              $SQL .= ' ' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_RNRSGH'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_RNRDAT'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_RNRTRM'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_FILNR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_WANR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_EINBAUDATUM'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_ATUNR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsDienstleister->FeldInhalt('SGD_MENGE'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('N2',str_replace('.',',',$BetragNeu),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('N2',str_replace('.',',',$Differenz),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_KFZKENNZ'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_MAPPE'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_LIEFERANTENNR'),false);
              $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
              $SQL .= ',SYSDATE';
              $SQL .= ')';

             if($DB->Ausfuehren($SQL)===false) {
             }
              
             $SQL = "SELECT seq_SGS_KEY.CurrVal AS KEY FROM DUAL";
	     $rsKey = $DB->RecordSetOeffnen($SQL);
	     
	     $rsSGS_KEY = $rsKey->FeldInhalt('KEY');
	      
	     $SQL = "Select * from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtKDSKEY']);

             $rsKassendaten = $DB->RecordSetOeffnen($SQL);

             $SQL = "INSERT INTO SGHKASSENDATENABGL (SKS_FILID,SKS_DATUM,SKS_ZEIT,";
             $SQL .= "SKS_BSA,SKS_WANR,SKS_ARTNR,SKS_MENGE,SKS_UMSATZ,";
             $SQL .= "SKS_KFZKENNZ,SKS_DWHAUDIT,SKS_DWHINSDATE,SKS_WAGENDAT,SKS_USER,SKS_USERDAT) VALUES (";
             $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_FILID'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DATUM'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ZEIT'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_BSA'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_WANR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ARTNR'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_MENGE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsKassendaten->FeldInhalt('SGK_UMSATZ'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_KFZKENNZ'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_DWHAUDIT'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DWHINSDATE'),false);
             $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_WAGENDAT'),false);
             $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
             $SQL .= ',SYSDATE';
             $SQL .= ')';

             if($DB->Ausfuehren($SQL)===false) {
            }
             
	     $SQL = "SELECT seq_SKS_KEY.CurrVal AS KEY FROM DUAL";
	     $rsKey = $DB->RecordSetOeffnen($SQL);
	     $rsSKS_KEY = $rsKey->FeldInhalt('KEY');

	      $SQL = "INSERT INTO SGHABGLEICH (SGA_SGS_KEY,SGA_SKS_KEY,SGA_TYP,";
              $SQL .= "SGA_ABGLEICHDATUM,SGA_USER,SGA_USERDAT) VALUES (";
              $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
              $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsSKS_KEY,false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T','8',false);
              $SQL .= ',SYSDATE';
              $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
              $SQL .= ',SYSDATE';
              $SQL .= ')';

              if($DB->Ausfuehren($SQL)===false) {
              }

                  if(isset($_POST['txtAuswahlBegruendung']))
                  {

                      if($_POST['txtAuswahlBegruendung'] == 'on' && $_POST['txtBegruendung'] != "")
                      {
                        //Speichere Begruendung
                        $SQL = "INSERT INTO SGHBEGRUENDUNG (SBG_SGS_KEY,SBG_BEGRUENDUNG,";
                        $SQL .= "SBG_USER,SBG_USERDAT) VALUES (";
                        $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
                        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtBegruendung'],false);
                        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                        if($DB->Ausfuehren($SQL)===false) {
                        }
                      }
                  }

              
              
              $SQL = "Delete from SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO',$rsDienstleister->FeldInhalt('SGD_KEY'));


              if($DB->Ausfuehren($SQL)===false) {
              }
              
              $SQL = "Delete from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_KEY'));

              if($DB->Ausfuehren($SQL)===false) {
              }

              $DETAILANSICHTSGH = true;
              $DETAILANSICHTKASSENDATEN = false;
              $FLAGKEINEAUSWAHL = false;
              $FLAGBETRAGSKUERZUNG = false;
              

              }
            }elseif($abgleichsGrund == 9)
            {
                 $SQL = "Select * from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO', $Key);

                 $rsKassendaten = $DB->RecordSetOeffnen($SQL);

                 $SQL = "INSERT INTO SGHKASSENDATENRABATT (SKR_FILID,SKR_DATUM,SKR_ZEIT,";
                 $SQL .= "SKR_BSA,SKR_WANR,SKR_ARTNR,SKR_MENGE,SKR_UMSATZ,";
                 $SQL .= "SKR_KFZKENNZ,SKR_DWHAUDIT,SKR_DWHINSDATE,SKR_WAGENDAT,SKR_USER,SKR_USERDAT) VALUES (";
                 $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_FILID'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DATUM'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ZEIT'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_BSA'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_WANR'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ARTNR'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_MENGE'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsKassendaten->FeldInhalt('SGK_UMSATZ'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_KFZKENNZ'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_DWHAUDIT'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_DWHINSDATE'),false);
                 $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsKassendaten->FeldInhalt('SGK_WAGENDAT'),false);
                 $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                 $SQL .= ',SYSDATE';
                 $SQL .= ')';

                 //echo $SQL;
                 
                 if($DB->Ausfuehren($SQL)===false) {
                 }

                 //Loeschen aus Kassendaten
                 $SQL = "Delete from SGHKASSENDATEN WHERE SGK_KEY=".$DB->FeldInhaltFormat('NO',$rsKassendaten->FeldInhalt('SGK_KEY'));

                 if($DB->Ausfuehren($SQL)===false) {
                 }
                 
                 $DETAILANSICHTSGH = true;
                 $DETAILANSICHTKASSENDATEN = true;
                 $FLAGKEINEAUSWAHL = true;
                 $FLAGBETRAGSKUERZUNG = false;
	     }
          }
          else
          {
            if($abgleichsGrund == 7 || $abgleichsGrund == 6)
            {
              $SQL = "SELECT * FROM SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO', $_POST['txtKDSKEY']);

              $rsDienstleister = $DB->RecordSetOeffnen($SQL);

              $SQL = "INSERT INTO SGHDIENSTLEISTERABGL (SGS_RNRSGH,SGS_RNRDAT,SGS_RNRTRM,SGS_FILNR,";
              $SQL .= "SGS_WANR,SGS_EINBAUDATUM,SGS_ATUNR,SGS_MENGE,SGS_BETRAG,SGS_KFZKENNZ,";
              $SQL .= "SGS_MAPPE,SGS_LIEFERANTENNR,SGS_USER,SGS_USERDAT) VALUES (";
              $SQL .= ' ' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_RNRSGH'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_RNRDAT'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_RNRTRM'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_FILNR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_WANR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDienstleister->FeldInhalt('SGD_EINBAUDATUM'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_ATUNR'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsDienstleister->FeldInhalt('SGD_MENGE'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsDienstleister->FeldInhalt('SGD_BETRAG'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_KFZKENNZ'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_MAPPE'),false);
              $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_LIEFERANTENNR'),false);
              $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
              $SQL .= ',SYSDATE';
              $SQL .= ')';

              if($DB->Ausfuehren($SQL)===false) {
              }
	  
	       $SQL = "SELECT seq_SGS_KEY.CurrVal AS KEY FROM DUAL";
	       $rsKey = $DB->RecordSetOeffnen($SQL);
	       $rsSGS_KEY = $rsKey->FeldInhalt('KEY');

	     	$SQL = "INSERT INTO SGHABGLEICH (SGA_SGS_KEY,SGA_SKS_KEY,SGA_TYP,";
             $SQL .= "SGA_ABGLEICHDATUM,SGA_USER,SGA_USERDAT) VALUES (";
             $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
             $SQL .= ',' . $DB->FeldInhaltFormat('NO',0,false);
             $SQL .= ',' . $DB->FeldInhaltFormat('T',$abgleichsGrund,false);
             $SQL .= ',SYSDATE';
             $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
             $SQL .= ',SYSDATE';
             $SQL .= ')';

             if($DB->Ausfuehren($SQL)===false) {
             }

             if(isset($_POST['txtAuswahlBegruendung']))
             {

                 if($_POST['txtAuswahlBegruendung'] == 'on' && $_POST['txtBegruendung'] != "")
                 {
                    $SQL = "INSERT INTO SGHBEGRUENDUNG (SBG_SGS_KEY,SBG_BEGRUENDUNG,";
                    $SQL .= "SBG_USER,SBG_USERDAT) VALUES (";
                    $SQL .= ' ' . $DB->FeldInhaltFormat('NO',$rsSGS_KEY,false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtBegruendung'],false);
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if($DB->Ausfuehren($SQL)===false) {
                    }
                  }
             }

             $SQL = "Delete from SGHDIENSTLEISTER WHERE SGD_KEY=".$DB->FeldInhaltFormat('NO',$rsDienstleister->FeldInhalt('SGD_KEY'));

             if($DB->Ausfuehren($SQL)===false) {
             }

             $FLAGKEINEAUSWAHL = false;
             $DETAILANSICHTSGH = true;
             $DETAILANSICHTKASSENDATEN = false;

            }
            
            else
            {
              $DETAILANSICHTSGH = false;
              $DETAILANSICHTKASSENDATEN = true;
              $FLAGKEINEAUSWAHL = true;

              $flag = false;
            }
          }

}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>