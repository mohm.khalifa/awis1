<?php
//**************************************************************
// Liefert die Daten f�r das Verbundfeld in CRM Details
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	if(!isset($_GET['FILTER']))
	{
		die('##PARAM##');
	}
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$SQL = "SELECT DISTINCT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') || ', ' || CAD_ORT AS CADNAME ";
	$SQL .= " FROM CRMADRESSEN ";
	$Bedingung = '';
	
	if($_GET['FILTER']!='')
	{
	//	$Bedingung .= ' AND UPPER(CAD_NAME1) '.$DB->LikeOderIst(strtoupper($_GET['FILTER']).'*');
		$Bedingung .= ' AND ASCIIWORT(CAD_NAME1) '.$DB->LikeOderIst(strtoupper($_GET['FILTER']).'*',awisDatenbank::AWIS_LIKE_ASCIIWORT);
	}
	if($_GET['WERT']!='')
	{
		$Bedingung .=' OR  CAD_KEY = '.$_GET['WERT'];
	}
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE '.substr($Bedingung,4);
	}
	$SQL .= " ORDER BY CAD_NAME1, CAD_NAME2";
	
	$rsCAD=$DB->RecordSetOeffnen($SQL);
	
	if(isset($_GET['WERT']))
	{
		echo $_GET['WERT'];
	}
	echo '#~#';		// Trennzeichen
	$DS=0;
	if($_GET['ZUSATZ']!='')
	{
		$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
		}
		else 
		{
			echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
		}
	}
	while(!$rsCAD->EOF())
	{
		if(++$DS>200 AND $rsCAD->FeldInhalt('CAD_KEY')==$_GET['WERT'])
		{
			if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
			{
				echo '<option value="'.$rsCAD->FeldInhalt('CAD_KEY').'">'.$rsCAD->FeldInhalt('CADNAME').'</option>';
			}
			else 
			{
				echo '#+#'.$rsCAD->FeldInhalt('CAD_KEY').'#+#'.$rsCAD->FeldInhalt('CADNAME').'';
			}
		}
		elseif($DS<200)
		{
			if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
			{
				echo '<option value="'.$rsCAD->FeldInhalt('CAD_KEY').'">'.$rsCAD->FeldInhalt('CADNAME').'</option>';
			}
			else 
			{
				echo '#+#'.$rsCAD->FeldInhalt('CAD_KEY').'#+#'.$rsCAD->FeldInhalt('CADNAME').'';
			}
		}
		
		$rsCAD->DSWeiter();
	}
	if(++$DS>200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="">...</option>';
		}
		else 
		{
			echo '#+##+#...';
		}
		break;
	}
}
catch (awisException $ex)
{
	echo $ex->getMessage().'<br>';
	echo $ex->getSQL().'<br>';
}
?>