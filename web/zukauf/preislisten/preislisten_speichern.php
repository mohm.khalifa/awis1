<?php
global $ZPL;
global $AWIS_KEY1;
try {
    if ($AWIS_KEY1==-1) { //Neuer DS

        $ZPL->RechteMeldung(4);

        $SQL  ='INSERT';
        $SQL .=' INTO ZUKAUFPREISLISTEN';
        $SQL .='   (';
        $SQL .='     ZPL_LIE_NR,';
        $SQL .='     ZPL_PFAD,';
        $SQL .='     ZPL_BEZEICHNUNG,';
        $SQL .='     ZPL_STATUS,';
        $SQL .='     ZPL_USER,';
        $SQL .='     ZPL_USERDAT';
        $SQL .='    )';
        $SQL .='    VALUES';
        $SQL .='    (';
        $SQL .=$ZPL->DBi->WertSetzen('ZPL','T',$_POST['txtZPL_LIE_NR']);
        $SQL .=', '.$ZPL->DBi->WertSetzen('ZPL','T',$_POST['txtZPL_PFAD']);
        $SQL .=', '.$ZPL->DBi->WertSetzen('ZPL','T',$_POST['txtZPL_BEZEICHNUNG']);
        $SQL .=', 1';
        $SQL .= ','.$ZPL->DBi->WertSetzen('ZPL','T',$ZPL->AWISBenutzer->BenutzerName());
        $SQL .= ', sysdate';
        $SQL .='    )';

        $ZPL->DBi->Ausfuehren($SQL,'',true,$ZPL->DBi->Bindevariablen('ZPL'));

        $AWIS_KEY1 = $ZPL->DBi->RecordSetOeffnen('SELECT SEQ_ZPL_KEY.currval from dual')->FeldInhalt(1);

        $ZPL->addZukaufpreislistenProtokoll($AWIS_KEY1,1);

        $Meldung = '';

    } else { //gešnderter DS
        return;
        $ZPL->RechteMeldung(2);
        $SQL  ='UPDATE ZPLILITAETSGARANTIEN';
        $SQL .=' SET';
        $SQL .='   ZPL_NAME1                = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_NAME1']);
        $SQL .=' , ZPL_NAME2                = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_NAME2']);
        $SQL .=' , ZPL_NAME3                = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_NAME3']);
        $SQL .=' , ZPL_FIL_ID               = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_FIL_ID']);
        $SQL .=' , ZPL_DATUM                = '.$ZPL->DB->WertSetzen('ZPL','DU',$_POST['txtZPL_DATUM']);
        $SQL .=' , ZPL_STRASSE              = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_STRASSE']);
        $SQL .=' , ZPL_POSTLEITZAHL         = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_POSTLEITZAHL']);
        $SQL .=' , ZPL_ORT                  = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_ORT']);
        $SQL .=' , ZPL_LAN_ATUSTAATS_NR     = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_LAN_ATUSTAATS_NR']);
        $SQL .=' , ZPL_KFZ_KENNZEICHEN      = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_KFZ_KENNZEICHEN']);
        $SQL .=' , ZPL_ERSTZULASSUNG        = '.$ZPL->DB->WertSetzen('ZPL','D',$_POST['txtZPL_ERSTZULASSUNG']);
        $SQL .=' , ZPL_KM_STAND             = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_KM_STAND']);
        $SQL .=' , ZPL_VERS_NUMMER          = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_VERS_NUMMER']);
        $SQL .=' , ZPL_VERS_GUELTIG_AB      = '.$ZPL->DB->WertSetzen('ZPL','D',$_POST['txtZPL_VERS_GUELTIG_AB']);
        $SQL .=' , ZPL_VERS_GUELTIG_BIS     = '.$ZPL->DB->WertSetzen('ZPL','D',$_POST['txtZPL_VERS_GUELTIG_BIS']);
        $SQL .=' , ZPL_GESAMTUMSATZ         = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_GESAMTUMSATZ']);
        $SQL .=' , ZPL_BEMERKUNG            = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_BEMERKUNG']);
        $SQL .=' , ZPL_ATUCARDKUNDE         = '.$ZPL->DB->WertSetzen('ZPL','T',isset($_POST['txtZPL_ATUCARDKUNDE'])?'1':'0');
        $SQL .=' , ZPL_USER                 = ' . $ZPL->DB->WertSetzen('ZPL','T',$ZPL->AWISBenutzer->BenutzerName()) . '';
        $SQL .=' , ZPL_USERDAT              = sysdate';
        $SQL .=' , ZPL_ANR_ID               = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_ANR_ID']);
        $SQL .=' , ZPL_TITEL                = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_TITEL']);
        $SQL .=' , ZPL_KFZ_KENNZEICHEN_KOMP = '.$ZPL->DB->WertSetzen('ZPL','T',awisWerkzeuge::ArtNrKomprimiert($_POST['txtZPL_KFZ_KENNZEICHEN']));
        $SQL .=' , ZPL_BSA                  = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_BSA']);
        $SQL .=' , ZPL_VORGANGSNUMMER       = '.$ZPL->DB->WertSetzen('ZPL','T',$_POST['txtZPL_VORGANGSNUMMER']);
        $SQL .=' WHERE ZPL_KEY                = ' . $ZPL->DB->WertSetzen('ZPL','N0',$AWIS_KEY1,false);

        $ZPL->DB->Ausfuehren($SQL, '', true, $ZPL->DB->Bindevariablen('ZPL'));
        $Meldung = $ZPL->AWISSprachKonserven['ZPL']['ZPL_UPDATE_OK'];
    }
    if($Meldung){
        $ZPL->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $ZPL->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $ZPL->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $ZPL->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>