<?php
//**************************************************************
// Liefert die Daten für den Serienbrief des CRM
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');



// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('BEW','%');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$FeldBreiten = array();
$FeldBreiten['Aktion'] = 100;
$FeldBreiten['Alt'] = 170;
$FeldBreiten['Neu'] = 170;
$FeldBreiten['User'] = 150;
$FeldBreiten['Userdat'] = 150;

// Infozeile zusammenbauen
$Felder = array();
$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a title='".$AWISSprachKonserven['BEW']['BEW_HISTCLOSE']."' onclick=closeHist('".$_GET['Tabelle']."') target='_self' name='BWH_HISTCLOSE'><img border=0 src=/bilder/icon_delete.png></a>");
$Form->InfoZeile($Felder,'');

$SQL = 'Select ';
$SQL .= '(select hrf_firmierung from bewerberhrzuord ';
$SQL .= 'inner join hrfirmierung on hrf_key = bwh_hrf_key where bwh_key='.$_GET['KEY'].') as Firmierung,';
$SQL .= '(select hrt_such from bewerberinfos ';
$SQL .= 'inner join HRTAETIGKEITEN on hrt_key = bif_wert where bif_bwh_key='.$_GET['KEY'].' and bif_bit_id=3) as Taetigkeit,';
$SQL .= '(select bif_wert from bewerberinfos where bif_bwh_key='.$_GET['KEY'].' and bif_bit_id=2) as Eingangsdatum from dual';



$rsBWH = $DB->RecordSetOeffnen($SQL);

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_FIRMIERUNG'] . ':',112);
$Form->Erstelle_TextFeld('HRK_FIRMIERUNG',$rsBWH->FeldInhalt('FIRMIERUNG'), 10, 85,false, '','', '','', 'L');
	
$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_TAETIGKEITEN'] . ':',73);
$Form->Erstelle_TextFeld('HRK_VORNAME',$rsBWH->FeldInhalt('TAETIGKEIT'), 10, 200,false, '','', '','', 'L');

$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_EINGANGSDATUM'] . ':',123);
$Form->Erstelle_TextFeld('HRK_GEBURTSDATUM',$Form->Format('D',$rsBWH->FeldInhalt('EINGANGSDATUM')), 10, 100,false, '','', '','', 'L');


$Form->ZeileEnde();


$SQL2 = 'Select BEH_AKTION,BEH_ALT,BEH_NEU,BEH_USER,BEH_USERDAT ';
$SQL2 .= ' from ';
$SQL2 .= 'BEWERBERHISTORY where BEH_KEY='.$_GET['KEY'];
$SQL2 .= ' and BEH_AKTION = \'Status\'';;
$SQL2 .= ' order by BEH_USERDAT DESC';

$rsHist = $DB->RecordSetOeffnen($SQL2);
	


$Form->ZeileStart('width:100%;');
$Link= '';
// Überschrift der Listenansicht mit Sortierungslink: Filiale
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_AKTION'], $FeldBreiten['Aktion'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_ALT'], $FeldBreiten['Alt'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_NEU'], $FeldBreiten['Neu'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_USER'], $FeldBreiten['User'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_USERDAT'], $FeldBreiten['Userdat'], '', $Link);
$Form->ZeileEnde();
$DS2 = 0;
if($rsHist->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('<div id=srcHist style="width: 100%; height: 180px;  overflow-y: scroll;">');
}

while(! $rsHist->EOF())
{
	$Form->ZeileStart('width:100%');
	$TTT = $rsHist->FeldInhalt('BEH_AKTION');
	$Form->Erstelle_ListenFeld('BEH_AKTION', $rsHist->FeldInhalt('BEH_AKTION'), 0, $FeldBreiten['Aktion'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_ALT');
	$Form->Erstelle_ListenFeld('BEH_ALT',strlen($rsHist->FeldInhalt('BEH_ALT')) > 20 ? substr($rsHist->FeldInhalt('BEH_ALT'),0,17).'...':$rsHist->FeldInhalt('BEH_ALT'), 0, $FeldBreiten['Alt'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_NEU');
	$Form->Erstelle_ListenFeld('BEH_NEU',strlen($rsHist->FeldInhalt('BEH_NEU')) > 20 ? substr($rsHist->FeldInhalt('BEH_NEU'),0,17).'...':$rsHist->FeldInhalt('BEH_NEU'), 0, $FeldBreiten['Neu'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_USER');
	$Form->Erstelle_ListenFeld('BEH_USER',$rsHist->FeldInhalt('BEH_USER'), 0, $FeldBreiten['User'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_USERDAT');
	$Form->Erstelle_ListenFeld('BEH_USERDAT',$rsHist->FeldInhalt('BEH_USERDAT'), 0, $FeldBreiten['Userdat'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$Form->ZeileEnde();
	$DS2++;
	$rsHist->DSWeiter();
}

if($rsHist->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('</div>') ;
}


?>