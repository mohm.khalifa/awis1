<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Preislistenpr�fungen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
<meta name="author" content="Sacha Kerres">
<meta http-equiv="cache-control" content="no-cache">
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,2000);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Preislistenpr�fungen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$cmdAktion='';

if(isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}

awis_RegisterErstellen(2000, $con, $cmdAktion);

print "<br><hr><img type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "&nbsp;<input type=image title='Hilfe (Alt+h)' alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=preislisten&HilfeBereich=" . $cmdAktion . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>";

awislogoff($con);



function PPHStatusText($StatusID)
{
	switch ($StatusID) {
		case 0:
			return '<font color=red>Nicht gepr�ft.</font>';
			break;
		case 1:
			return '<font color=green>Erfolgreich.</font>';
			break;
		case 2:
			return '<font color=red>Fehler.</font>';
			break;
		default:
			return '::unbekannt::';
			break;
	}
}
?>
</body>
</html>

