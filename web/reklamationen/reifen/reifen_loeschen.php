<?php
global $AWIS_KEY1;
global $REX;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$REX->Form = new awisFormular();
	$REX->AWISBenutzer = awisBenutzer::Init();
	$REX->DB = awisDatenbank::NeueVerbindung('AWIS');
	$REX->DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtREX_KEY']))
		{
			$Tabelle = 'REX';
			$Key=$_POST['txtREX_KEY'];

			$SQL = 'SELECT REX_KEY, REX_REKLAMATIONSDATUM, REX_AST_ATUNR';
			$SQL .= ' FROM REIFENREKLAMATIONEN ';
			$SQL .= ' WHERE REX_KEY=0'.$Key;

			$rsDaten = $REX->DB->RecordsetOeffnen($SQL);

			$REXKey = $rsDaten->FeldInhalt('REX_KEY');

			$Felder=array();
			$Felder[]=array($REX->Form->LadeTextBaustein('REX','REX_REKLAMATIONSDATUM'),$rsDaten->FeldInhalt('REX_REKLAMATIONSDATUM'));
			$Felder[]=array($REX->Form->LadeTextBaustein('REX','REX_AST_ATUNR'),$rsDaten->FeldInhalt('REX_AST_ATUNR'));
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'REX':
				$SQL = 'DELETE FROM REIFENREKLAMATIONEN WHERE REX_key=0'.$_POST['txtKey'];
				break;
			default:
				break;
		}

		if($SQL !='') {
            $REX->DB->Ausfuehren($SQL);
            $REX->Form->Hinweistext($REX->AWISSprachKonserven['REX']['REX_DEL_OK'],awisFormular::HINWEISTEXT_WARNUNG);
            $AWIS_KEY1=-1;
        }
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $REX->Form->LadeTexte($TextKonserven);

		$REX->Form->SchreibeHTMLCode('<form name=frmLoeschen action=./reifen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$REX->Form->Formular_Start();
		$REX->Form->ZeileStart();
		$REX->Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$REX->Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$REX->Form->ZeileStart();
			$REX->Form->Erstelle_TextLabel($Feld[0].':',150);
			$REX->Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$REX->Form->ZeileEnde();
		}

		$REX->Form->Erstelle_HiddenFeld('REXKey',$REXKey);
		$REX->Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$REX->Form->Erstelle_HiddenFeld('Key',$Key);

		$REX->Form->Trennzeile();

		$REX->Form->ZeileStart();
		$REX->Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$REX->Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$REX->Form->ZeileEnde();

		$REX->Form->SchreibeHTMLCode('</form>');

		$REX->Form->Formular_Ende();
        $REX->Param['KEY'] = $REXKey;
		die();
	}
}
catch (awisException $ex)
{
	$REX->Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$REX->Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$REX->Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>