<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Meldung;

try {
    $AWISBenutzer = awisBenutzer::Init();
    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Form = new awisFormular();

    $EU = new eurechnungen_funktionen();

    $Form->Formular_Start();
    $Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./eurechnungen_Main.php?cmdAktion=Details>");
    $PDFButton = false;
    $Param = array();

    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (isset($_POST['cmdSuche_x'])) {
        $Param['ERE_RECHNUNGSNUMMER'] = $Form->Format('T', $_POST['sucERE_RECHNUNGSNUMMER'], true);
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
        $AWIS_KEY1 = '';
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        $Param['ERE_RECHNUNGSNUMMER'] = $AWIS_KEY1;
    } elseif (isset($_POST['cmdLoeschenOK'])) {
        $AWIS_KEY1 = '';
        include('./eurechnungen_loeschen.php');
    }
    elseif(isset($_GET['sucERK_ID'])){
        $AWIS_KEY1 = $_GET['sucERK_ID'];
    }elseif (isset($_POST['txtERK_ID'])) {
        $AWIS_KEY1 = $_POST['txtERK_ID'];
    } elseif (isset($_GET['ERK_ID'])) //Von Detail zu Unterdetail
    {
        $AWIS_KEY1 = $_GET['ERK_ID'];
    } elseif (isset($_POST['txtERK_ID'])) {
        $AWIS_KEY1 = $_POST['txtERK_ID'];
    }
   else{ //nicht �ber die Suche gekommen.
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_EURechnungen'));
    }

    if (isset($_GET['ERP_KEY'])) {
        $AWIS_KEY2 = $_GET['ERP_KEY'];
        $Form->Erstelle_HiddenFeld('ERP_KEY', $AWIS_KEY2);
    }  elseif (isset($_POST['txtERP_KEY'])) {
        $AWIS_KEY2 = $_POST['txtERP_KEY'];
    }

    if(isset($_GET['DEL']) and $_GET['ERP_KEY']){
        include"eurechnungen_loeschen.php";
        $AWIS_KEY2 = '';
    }

    if (isset($_POST['cmdLoeschen_x'])) {
        $EU->WirklichLoeschen();
    }
    if(isset($_POST['txtEKU_NAME'])){
        include('eurechnungen_Kunden_speichern.php');
        $AWIS_KEY1 = -1;
    }
    if (isset($_POST['cmdSpeichern_x'])) {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_EURechnungen'));
        include('eurechnungen_speichern.php');
        if(isset($_POST['sucERK_ID'])){
            $AWIS_KEY1 = $_POST['sucERK_ID'];
        }
    }
    
    $Form->Erstelle_HiddenFeld('ERP_KEY', $AWIS_KEY2);
    //Sortierung:
    $Sort = 'ERK_ID desc';
    if (isset($_GET['Sort']) and $_GET['Sort'] != '') {

        $Sort = str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //Pagenation:
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        if (!isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }
    }
    $ZeilenProSeite = 1;
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;

    $EU->BedingungErstellen($Param);
    $MaxDS = $EU->rsERE($EU::ReturnZeilenanzahl);

    $rsERE = $EU->rsERE($EU::ReturnBlaetternRS);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());

    // Spaltenbreiten f�r Listenansicht
    $FeldBreiten = array();
    $FeldBreiten['ERK_ID'] = 150;
    $FeldBreiten['ERK_FIL_ID'] = 60;
    $FeldBreiten['ERK_DATUM'] = 140;
    $FeldBreiten['EKU_NAME'] = 140;


    if($Meldung!=''){
        $Form->Hinweistext($Meldung);
    }
    if ($AWIS_KEY1 == '') {  //Listenansicht

        $Form->ZeileStart();
        $Link = './eurechnungen_Main.php?cmdAktion=Details&Sort=ERK_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ERK_ID'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERK_ID'], $FeldBreiten['ERK_ID'], '', $Link);

        $Link = './eurechnungen_Main.php?cmdAktion=Details&Sort=EKU_NAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_NAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['EKU_NAME'], $FeldBreiten['EKU_NAME'], '', $Link);

        $Link = './eurechnungen_Main.php?cmdAktion=Details&Sort=ERK_FIL_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ERK_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERK_FIL_ID'], $FeldBreiten['ERK_FIL_ID'], '', $Link);

        $Link = './eurechnungen_Main.php?cmdAktion=Details&Sort=ERK_DATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ERK_DATUM'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERK_DATUM'], $FeldBreiten['ERK_DATUM'], '', $Link);

        $Form->ZeileEnde();
        $DS = 0;    // f�r Hintergrundfarbumschaltung
        while (!$rsERE->EOF()) {
            $Hintergrund = $DS % 2;


            $Form->ZeileStart();
            //Normaler DS
            $Link = './eurechnungen_Main.php?cmdAktion=Details&ERK_ID=' . $rsERE->FeldInhalt('ERK_ID') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $TTT = $rsERE->FeldInhalt('ERK_ID');
            $Form->Erstelle_ListenFeld('ERK_ID', $rsERE->FeldInhalt('ERK_ID'), 0, $FeldBreiten['ERK_ID'], false, $Hintergrund, '', $Link, 'T', 'L', $TTT);

            $TTT = $rsERE->FeldInhalt('EKU_NAME');
            $Form->Erstelle_ListenFeld('EKU_NAME', $rsERE->FeldInhalt('EKU_NAME'), 0, $FeldBreiten['EKU_NAME'], false, $Hintergrund, '', '', 'T', 'L', $TTT);


            $TTT = $rsERE->FeldInhalt('ERK_FIL_ID');
            $Form->Erstelle_ListenFeld('ERK_FIL_ID', $rsERE->FeldInhalt('ERK_FIL_ID'), 0, $FeldBreiten['ERK_FIL_ID'], false, $Hintergrund, '', '', 'T', 'L', $TTT);

            $TTT = $rsERE->FeldInhalt('ERK_DATUM');
            $Form->Erstelle_ListenFeld('ERK_DATUM', $rsERE->FeldInhalt('ERK_DATUM'), 0, $FeldBreiten['ERK_DATUM'], false, $Hintergrund, '', '', 'D', 'L', $TTT);


            $Form->ZeileEnde();
            $DS++;
            $rsERE->DSWeiter();
        }
        $Link = './eurechnungen_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } elseif ($AWIS_KEY1 != '') {

        echo '<script>
            $(document).ready(function () {
                key_ERK_ID(event);
                });
                </script>';

        // Spaltenbreiten
        $FeldBreiten = array();
        $FeldBreiten['Werte'] = 220;
        $FeldBreiten['Labels'] = 220;

        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./eurechnungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $EU->_AWISSprachkonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsERE->FeldInhalt('ERE_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsERE->FeldInhalt('ERE_USERDAT'));
        $Form->InfoZeile($Felder, '');

        //Key f�rs speichern in hiddenfeld stecken
        $Form->Erstelle_HiddenFeld('ERK_ID', $AWIS_KEY1);

        if (($Recht47000 & 4) == 4) {
            $Aendern = true;
        } else {
            $Aendern = 'disabled';
        }
        //UnterdetailDS wird Bearbeitet? Dann darf Kopf nicht ge�ndert werden
        if (intval($AWIS_KEY2)) {
            $Aendern = 'disabled';
        }

        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart($EU->_AWISSprachkonserven['ERE']['ERE_KOPF'], true);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($EU->_AWISSprachkonserven['ERE']['ERK_ID'] . ':', $FeldBreiten['Labels']);
        $Form->AuswahlBox('!ERK_ID', 'box1', '', 'ERE_Daten', 'sucEKU_USTIDNR,txtERK_ID', $FeldBreiten['Werte'], $FeldBreiten['Werte'] / 10,
            isset($_POST['sucERK_ID'])?$_POST['sucERK_ID']:$rsERE->FeldInhalt('ERK_ID'), 'T', true, '', '', '', 0, '', '', '', 0, '', 'autocomplete=off ' . ($AWIS_KEY1==-1?$Aendern:'disabled') );
        $Form->Erstelle_TextLabel($EU->_AWISSprachkonserven['ERE']['EKU_USTIDNR'] . ':', $FeldBreiten['Labels']);
        $Form->AuswahlBox('!EKU_USTIDNR', 'box1', '', 'ERE_Daten', 'sucERK_ID', $FeldBreiten['Werte'], $FeldBreiten['Werte'] / 10,
            isset($_POST['sucEKU_USTIDNR'])?$_POST['sucEKU_USTIDNR']:$rsERE->FeldInhalt('EKU_USTIDNR'), 'T', true, '', '', '', 0, '', '', '', 0, '', 'autocomplete=off ' .($AWIS_KEY1>0?'disabled':$Aendern) );
        $Form->ZeileEnde();

        $Form->AuswahlBoxHuelle('box1', '', '', '');

        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();

        //Bei Neuanlage des Kopfes, keine Positionstabelle anzeigen
        if ($AWIS_KEY1 >= 1) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($EU->_AWISSprachkonserven['ERE']['ERP_RECHPOS'],100,'font-weight: bold');
            $Form->ZeileEnde();
            $Form->Trennzeile('O');

            $rsERP = $EU->rsERP($EU::ReturnRecordset, $AWIS_KEY2);
            if ($AWIS_KEY2 == '') {

                $FeldBreiten['ICONS'] = 40;
                $FeldBreiten['ERP_POSITION'] = 50;
                $FeldBreiten['ERP_AST_ATUNR'] = 100;
                $FeldBreiten['ERP_MENGE'] = 100;
                $FeldBreiten['ERP_BETRAG_NETTO'] = 150;
                $FeldBreiten['Betrag'] = 150;

                $Form->ZeileStart();
                $Icons = array();
                $Icons[] = array('blank', '');
                $Icons[] = array('new', './eurechnungen_Main.php?cmdAktion=Details&ERK_ID=' . $AWIS_KEY1 . '&ERP_KEY=-1');
                $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ICONS'],-2);
                $Form->Erstelle_ListeIcons(array(),20,-2);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_POSITION'], $FeldBreiten['ERP_POSITION']);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_AST_ATUNR'], $FeldBreiten['ERP_AST_ATUNR']);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_MENGE'], $FeldBreiten['ERP_MENGE']);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_EINZELPREIS_BRUTTO'], $FeldBreiten['ERP_BETRAG_NETTO']);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_EINZELPREIS_NETTO'], $FeldBreiten['ERP_BETRAG_NETTO']);
                $Form->Erstelle_Liste_Ueberschrift($EU->_AWISSprachkonserven['ERE']['ERP_BETRAG_NETTO_LBL'], $FeldBreiten['Betrag']);
                $Form->ZeileEnde();
                $DS = 0;
                $Form->ZeileStart();
                $Form->SortierListeStart('ERP','',array(),$Aendern);
                while (!$rsERP->EOF()) {
                    $HG = ($DS++ % 2);
                    $Icons = array();
                    $Icons[] = array('delete', './eurechnungen_Main.php?cmdAktion=Details&DEL&ERK_ID=' . $AWIS_KEY1 . '&ERP_KEY=' . $rsERP->FeldInhalt('ERP_KEY'));
                    $Icons[] = array('edit', './eurechnungen_Main.php?cmdAktion=Details&EDIT&ERK_ID=' . $AWIS_KEY1 . '&ERP_KEY=' . $rsERP->FeldInhalt('ERP_KEY'));

                    $Form->SortierListenZeileStart($HG,20,''.$rsERP->FeldInhalt('ERP_KEY'),$Aendern);
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ICONS'], $HG);
                    $Form->Erstelle_ListenFeld('ERP_POSITION', $rsERP->DSNummer()+1, $FeldBreiten['ERP_POSITION'] / 10, $FeldBreiten['ERP_POSITION'], false, $HG);
                    $Form->Erstelle_ListenFeld('ERP_AST_ATUNR', $rsERP->FeldInhalt('ERP_AST_ATUNR'), $FeldBreiten['ERP_AST_ATUNR'] / 10, $FeldBreiten['ERP_AST_ATUNR'], false, $HG);
                    $Form->Erstelle_ListenFeld('ERP_MENGE', $rsERP->FeldInhalt('ERP_MENGE'), $FeldBreiten['ERP_MENGE'] / 10, $FeldBreiten['ERP_MENGE'], false, $HG);
                    $Form->Erstelle_ListenFeld('ERP_VK_NEU', $rsERP->FeldInhalt('ERP_VK_NEU','N2'), $FeldBreiten['ERP_BETRAG_NETTO'] / 10, $FeldBreiten['ERP_BETRAG_NETTO'],false, $HG);
                    $Form->Erstelle_ListenFeld('ERP_VK_NETTO', $rsERP->FeldInhalt('ERP_VK_NETTO','N2'), $FeldBreiten['ERP_BETRAG_NETTO'] / 10, $FeldBreiten['ERP_BETRAG_NETTO'],false, $HG);
                    $Form->Erstelle_ListenFeld('Betrag', $rsERP->FeldInhalt('ERP_BETRAG_NETTO','N2'), $FeldBreiten['Betrag'] / 10,
                        $FeldBreiten['Betrag'], false, $HG,'','','N2');

                    $Form->SortierListenZeileEnde();
                    $rsERP->DSWeiter();
                    $PDFButton = true;
                }
                $Form->SortierListeEnde();
                $Form->ZeileEnde();
            } else { //Neue oder bestehende Positon

                echo '<script>
                $(document).ready(function () {
                    key_ERP_AST_ATUNR(event);
                });
                </script>';

                $Felder = array();
                $Felder[] = array(
                    'Style' => 'font-size:smaller;',
                    'Inhalt' => "<a href=./eurechnungen_Main.php?cmdAktion=Details&ERK_ID=$AWIS_KEY1 accesskey=T title='" . $EU->_AWISSprachkonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
                );
                $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsERE->FeldInhalt('ERE_USER'));
                $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsERE->FeldInhalt('ERE_USERDAT'));
                $Form->InfoZeile($Felder, '');

                $FeldBreiten['ERP_AST_ATUNR'] = 175;

                if($AWIS_KEY2==-1){
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($EU->_AWISSprachkonserven['ERE']['ERE_POSTYP'] . ':', $FeldBreiten['Labels']);
                    $ListenDaten = explode("|",$EU->_AWISSprachkonserven['ERE']['ERE_LST_POSTYP']);
                    $Form->Erstelle_SelectFeld('!POS_TYP',1,$FeldBreiten['ERP_AST_ATUNR'].':'.$FeldBreiten['ERP_AST_ATUNR'],true,'','',1,'','',$ListenDaten);
                    $Form->ZeileEnde();
                }else{
                    $Form->Erstelle_HiddenFeld('POS_TYP',-1);
                }

                $Form->ZeileStart();
                $Form->Erstelle_HiddenFeld('ERK_ID', $AWIS_KEY1);
                $Form->Erstelle_TextLabel($EU->_AWISSprachkonserven['ERE']['ERP_AST_ATUNR'] . ':', $FeldBreiten['Labels']);
                $Form->AuswahlBox('!ERP_AST_ATUNR', 'box2', '', 'ERE_ATUNR', 'txtERK_ID,txtERP_KEY,txtPOS_TYP', $FeldBreiten['ERP_AST_ATUNR'], $FeldBreiten['Werte'] / 10,
                    isset($_POST['sucERP_AST_ATUNR'])?$_POST['sucERP_AST_ATUNR']:$rsERP->FeldInhalt('ERP_AST_ATUNR'), 'T', true, '', '', '', 0, '', '', '', 0, '',
                    'autocomplete=off');
                $Form->ZeileEnde();


                $Form->AuswahlBoxHuelle('box2', '', '', '');
            }
        }
    } else {
        $Form->ZeileStart();
        $Form->Hinweistext($EU->_AWISSprachkonserven['Fehler']['err_keineDaten']);
        $Form->ZeileEnde();
    }

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $EU->_AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');

    //Speichernbutton wenn: ERK_ID: Im DetailDatensatz | DSNeu: Bei neu | cmdSpeichern: Nachdem gespeichert wurde.
    if (($Recht47000 & 4) == 4 and $AWIS_KEY1!='') {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $EU->_AWISSprachkonserven['Wort']['lbl_speichern'], 'S');
        if (($Recht47000 & 8) == 8 AND $AWIS_KEY1 > 0) {
            $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $EU->_AWISSprachkonserven['Wort']['lbl_loeschen'], 'L', '', 27, 27, '', true);
        }
    } elseif (($Recht47000 & 2) == 2) // Hinzuf�gen immer dann, wenn Recht und kein Speichern da ist
    {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $EU->_AWISSprachkonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    //PDF Button wird nur angezeigt, wenn Positionen vorhanden sind ($PDFButton)
    if(($Recht47000&16)==16 and $PDFButton){
        $Form->Schaltflaeche('href', 'cmdDSNeu', '/berichte/drucken.php?ID=62&XRE=62&ERK_ID='.$AWIS_KEY1, '/bilder/cmd_PDF.png', $EU->_AWISSprachkonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');

    if (!isset($_GET['ERK_ID'])) {
        $AWISBenutzer->ParameterSchreiben('Formular_EURechnungen', serialize($Param));
    }

} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>