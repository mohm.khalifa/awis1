<?php

/**
 * Gibt den �bergeben SQL als PHP-Code zur�ck
 * 
 * @param String $SQL
 * @param String $Variable
 * @return String 
 */
function SQLzuPHP($SQL, $Variable = "SQL", $NutzerErsetzen = false) {
    require_once('awisBenutzer.inc');
    
    try
    {
        $AWISBenutzer = awisBenutzer::Init();
        $BenutzerName = $AWISBenutzer->BenutzerName(1);
    }
    catch (Exception $ex)
    {
        die($ex->getMessage());
    }
    //Sonderzeichen maskieren

    If (strlen(trim($Variable))<1)                                  //Ersetzen der Semikolons, etc.
    {
        $Variable = "SQL";
    }
    $SQL = trim(preg_replace('/\s\s+/', "\r\n", str_replace("\r\n", " ", $SQL)));
    $PHP = trim(str_replace("';'","Semikolon-Ersatz",$SQL));
    $PHP = str_replace(";", "",$PHP);
    $PHP = str_replace("Semikolon-Ersatz","';'",$PHP);
    $PHP = addslashes($PHP);
    $PHP = "$" . "$Variable  ='" . $PHP;

    //Formate f�r Abfrage

    $Formate = array();
    $Formate[] = "T";
    $Formate[] = "CL";
    $Formate[] = "TW";
    $Formate[] = "TU";
    $Formate[] = "TL";
    $Formate[] = "T8";
    $Formate[] = "TP";
    $Formate[] = "TS";
    $Formate[] = "T0";
    $Formate[] = "Z";
    $Formate[] = "D";
    $Formate[] = "DU";
    $Formate[] = "U";
    $Formate[] = "BLOB";

    if (isset($_POST["txtBindevariablebenutzen"]) and $_POST["txtBindevariablebenutzen"] == "on") { //Replace von Bindevariablen
        while (!(strpos($PHP, ":var_") === false)) {
            preg_match('/\b(var_\w+)\b/', $PHP, $Bindevar);
            $Bindevar2 = explode("_", $Bindevar[0]);
            $Format = $Bindevar2[1];
            $Wertname = implode("_", array_slice($Bindevar2, 2));
            if (!(in_array($Format, $Formate)) AND !(substr($Format, 0, 1) == "N" and substr($Format, 1))) { //Falls das Format nicht �bereinstimmt/ nicht N(Zahl) ist -> Format auf Text stellen
                $Format = "T";
                $Wertname = implode("_", array_slice($Bindevar2, 1));
            }
            $PHP = preg_replace('/'.':'. $Bindevar[0].'/', '\'. $DB->WertSetzen(\'' . $_POST["txtBindevariablensatz"] . '\',\'' . $Format . '\',$' . $Wertname . ');', $PHP,1);
        }
    }

    If ($NutzerErsetzen == true)
    {
        $PHP = str_replace("\'$BenutzerName\'", "' . \$AWISBenutzer->BenutzerName() . '", $PHP);                //Ersetzung der Benutzernamen zur AWIS-Funktion, wenn Haken gesetzt wurde
    }
    
    $PHP = str_replace("\n", "$" . "$Variable .=' ",$PHP);              //Einsetzen der Variable und Semikolon am Ende einer Zeile
    $PHP = str_replace("\r", "';\r\n",$PHP);

    if(!(substr($PHP,-1)== ';')) {
        $PHP = $PHP . "';";
    }

    while(!(strpos($PHP, ";';") === false)) {                   //Zum Korrigieren von Fehlern, die durch Bindevariablen entstehen (, wenn mehrere Bindevariablen vorkommen)
        $PHP = preg_replace("/[;]['][;]/" , ';' , $PHP, 1);
    }

    while(!(strpos($PHP, "; =") === false)) {                   //Zum Korrigieren von Fehlern, die durch Bindevariablen entstehen (,wenn sie vor dem '=' kommen)
        $PHP = preg_replace("/[;][ ][=]/" , ' . \' =' , $PHP, 1);
    }

    while(!(strpos($PHP, "=' '") === false)) {                   //Zum Korrigieren von Fehlern, die durch Bindevariablen entstehen (,wenn sie vor dem '=' kommen)
        $PHP = preg_replace("/[=]['][ ]['][.]/" , '=' , $PHP, 1);
    }

    if (isset($_POST["txtAusfuehrenRSerzeugen"]) and ($_POST["txtAusfuehrenRSerzeugen"] == "on" and $_POST["txtOeffnungsart"] == "1")) {   //Wenn "RecordSetOeffnen" ausgew�hlt wurde bei der Auswahlbox
        $PHP = $PHP . "\n \n";
        $PHP = $PHP . '$rs'.((isset($_POST["txtBindevariablensatz"]) and isset($_POST["txtBindevariablebenutzen"]))?($_POST["txtBindevariablensatz"]):('')) .' = $DB->RecordSetOeffnen($'.$Variable. ((isset($_POST["txtBindevariablensatz"]) and isset($_POST["txtBindevariablebenutzen"]))?(', $DB->Bindevariablen(\''.$_POST["txtBindevariablensatz"].'\')'):('')).');';
    }

    if (isset($_POST["txtAusfuehrenRSerzeugen"]) and ($_POST["txtAusfuehrenRSerzeugen"] == "on" and $_POST["txtOeffnungsart"] == "2")) {    //Wenn "Ausf�hren" ausgew�hlt wurde bei der Auswahlbox
        $PHP = $PHP . "\n \n";
        $PHP = $PHP . '$DB->Ausfuehren($'.$Variable. ((isset($_POST["txtBindevariablensatz"]) and isset($_POST["txtBindevariablebenutzen"]))?(',\'\',true,$DB->Bindevariablen(\''.$_POST["txtBindevariablensatz"].'\')'):('')).');';
    }
    return $PHP;
}

/**
 * Gibt den �bergebenen PHP-Code als SQL zur�ck
 * 
 * @param String $PHP
 * @param String $Variable
 * @return String
 */
function PHPzuSQL($PHP, $Variable = ""){

    if ($Variable == ""){                                                           //Entfernen der Hochkommata, der Variable, etc.
        $Variable = trim(substr(trim($PHP),1,strpos(trim($PHP), "=")-1));
    }
    $SQL = trim(str_replace('\';', '', $PHP));
    $SQL = trim(str_replace('";', '', $SQL));
    $SQL = trim(str_replace('<br />', "", nl2br($SQL)));
    $SQL = str_replace("\'", 'Hochkommata-Replacement', $SQL);
    //Sonderzeichen demaskieren
    $SQL = stripslashes($SQL);
    $SQL = preg_replace('/(\$' . $Variable . "\s?\.?=\s?[\"\'])/",'',$SQL);
    $SQL = preg_replace('/(\$' . $Variable . " [.][=])/",'',$SQL);
    $SQL = str_replace('Hochkommata-Replacement', '\'', $SQL);
    $SQL = preg_replace('/([=] [\'][.])/' ,'=', $SQL);
    $SQL = preg_replace('/([.] [\'] [=])/' ,'=', $SQL);


    while (!(strpos($SQL, "WertSetzen") === false)) {                                                       //Ersetzung von "WertSetzen" zu ":var_"
        preg_match('/([$]DB->WertSetzen[(][-a-zA-Z0-9$_,>()\'"\s]*[)]{1}[;]{1})/u', $SQL, $WertSetzenKomplett);

        //Zu ersetzende Dinge

        $Replace = array();
        $Replace[] = '(';
        $Replace[] = ')';
        $Replace[] = '"';
        $Replace[] = '\'';
        $Replace[] = '$DB->WertSetzen';
        $Replace[] = '$';

        $WertSetzenOhneZusatzzeichen = str_replace($Replace, '', $WertSetzenKomplett[1]);       //Ersetzen der im Array vorkommenden Dinge

        $WertSetzenGetrennt = explode(',', $WertSetzenOhneZusatzzeichen);                      //Trennung zu Array

        $WertSetzenGetrennt[1] = str_replace(' ', '', $WertSetzenGetrennt[1]);    //Entfernung von Leerzeichen bei der Bezeichnung und dem Format
        $WertSetzenGetrennt[2] = str_replace(' ', '', $WertSetzenGetrennt[2]);

        preg_match('/AWIS[\S\s]*/',$WertSetzenGetrennt[2],$WertSetzenAWISFunktion);       //Ersetzung von AWIS-Funktionen mit "Bindevariable"
        if(isset($WertSetzenAWISFunktion[0])) {
            $WertSetzenAWISFunktion[0] = "Bindevariable";
        }

        $SQL = preg_replace('/([$]DB->WertSetzen[(][-a-zA-Z0-9$_>(),\'"\s]*[)]{1}[;]{1})/', ':var_' . $WertSetzenGetrennt[1] . '_' . (isset($WertSetzenAWISFunktion[0])?$WertSetzenAWISFunktion[0]:$WertSetzenGetrennt[2]), $SQL, 1);
        $WertSetzenAWISFunktion = array();           //Leeren von array um Fehler bei mehreren "WertSetzen" zu vermeiden
    }

    $Replace2 = array();                            //Entfernung von unn�tigen Zeichen
    $Replace2[] = ".";
    $Replace2[] = "'";
    $SQL = str_replace($Replace2,'',$SQL);

    $SQL = trim(preg_replace('/\s\s+/', "\n", preg_replace('/[\s]*,/',',',str_replace("\n", " ", $SQL))));       //Entfernung von unn�tigen Leerzeichen

    $SQL = str_replace(';','',$SQL);
    return $SQL . ";";
}

?>