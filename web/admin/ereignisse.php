<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Ereignisanzeige</title>
<link rel=stylesheet type=text/css href=ATU.css>
</head>

<body>
<? 

require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("ATU_Header.php");	// Kopfzeile

global $awisRSZeilen;


if(!(awisHatDasRecht(1) || $REMOTE_ADDR=="125.0.3.20"))
{
	awisEreignis(3,1000,'Ereignisanzeige',"$PHP_AUTH_USER",'','','');
	die("Keine ausreichenden Rechte!");
}

echo '<form name=frmAnzeige method=get action=' . $_SERVER["PHP_SELF"] . '>';

if (!isset($_GET["alles"]) || $_GET["alles"] != "ja") {
	echo '<input type=submit value="Alles anzeigen">';
	echo '<input type="hidden" name="alles" value="ja">';
}else{
	echo '<input type=submit value="Akt. Monat anzeigen">';
	echo '<input type="hidden" name="alles" value="nein">';
}
echo "</form>";

echo "<p>";

$con = awisLogon();
if($con)
{
	$Bedingung="";
	if(!isset($_GET["alles"]) || $_GET["alles"] != "ja") {
		$Bedingung=" AND TO_CHAR(XER_DATUM,'MM.YYYY')=TO_CHAR(SYSDATE,'MM.YYYY') ";	
	}
	
	$rsEreignisse = awisOpenRecordset($con, "SELECT * FROM Ereignisse, EreignisArten WHERE XEA_NR = XER_XEA_NR ".$Bedingung." ORDER BY XER_DATUM DESC");
	$rsEreignisseZeilen = $awisRSZeilen;
	
	print "<table border=1 width=100%><tr><th id=FeldBez>Typ</th><th id=FeldBez>Ereignis</th><th id=FeldBez>Benutzer</th><th id=FeldBez>Computer</th><th id=FeldBez>Zeit</th><th id=FeldBez>Beschreibung</th></tr>";
	
	for($DS=0;$DS<$rsEreignisseZeilen;$DS++)
	{
		print "<tr><td align=center>";
			// Bild
		switch(intval($rsEreignisse["XER_TYP"][$DS]))
		{
			case 1:
				print "<img src=../bilder/info.gif width=16 height=16 alt=I>";
				break;
			case 2:
				print "<img src=../bilder/warnung.gif width=16 height=16 alt=W>";
				break;
			case 3:
				print "<img src=../bilder/fehler.gif width=16 height=16 alt=F>";
				break;
			case 4:
				print "<img src=../bilder/schwererfehler.gif width=16 height=16 alt=S>";
				break;
		}
		print "</td>";

			// Bezeichnung des Fehlers
		print "<td>" . $rsEreignisse["XEA_BEZEICHNUNG"][$DS] . "</td>";
			// Benutzername
		print "<td>" . $rsEreignisse["XER_BENUTZER"][$DS] . "</td>";
		print "<td>" . $rsEreignisse["XER_COMPUTER"][$DS] . "</td>";
		print "<td>" . $rsEreignisse["XER_DATUM"][$DS] . "</td>";
		print "<td>" . $rsEreignisse["XER_MELDUNG"][$DS] . "</td>";
		print "</tr>";
	}


	print "</table>";

	awisLogoff($con);			// Wieder abmelden


	print "<a href='./index.php'><font size=2>Zur�ck</font></a>";

}



?>
</body>
</html>
