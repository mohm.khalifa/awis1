<?
/**
 * filialinfo_Main.php
 *
 * Zentrale Seite f�r die Filiolinformationen
 *
 * @author Sacha Kerres
 * @version 24051001
 * @package FILIALEN
 * @uses register.inc.php
 * @uses db.inc.php
 * @uses sicherheit.inc.php
 * @uses awis_forms.inc.php
 */
global $AWISSprache;
global $CursorFeld;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 100);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Filialen: Suchen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}


// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Personalnummer');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('FIL','%');
$TextKonserven[]=array('Liste','lst_ALLE_0');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
// lokale Variablen
$filSuche = '';		//Parameter der Anwender

// Aktuelle Filiale zurcksetzen
awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "");

echo "<br>";

echo "<form name=frmSuche method=post action=./filialinfo_Main.php?cmdAktion=Filialinfos>";


$filSuche = explode(";",awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName()));
if(count($filSuche)<10)
{
	for($i=count($filSuche);$i<10;$i++)
	{
		$filSuche[$i]='';
	}
}

awis_FORM_FormularStart();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].":",200,'');
awis_FORM_Erstelle_TextFeld('FIL_ID',($filSuche[4]=='on'?$filSuche[0]:''),10,100,true,'','','','Z');
$CursorFeld='txtFIL_ID';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_BEZ'].":",200,'');
awis_FORM_Erstelle_TextFeld('FIL_BEZ',($filSuche[4]=='on'?$filSuche[1]:''),40,400,true,'','','','T');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_STRASSE'].":",200,'');
awis_FORM_Erstelle_TextFeld('FIL_STRASSE',($filSuche[4]=='on'?$filSuche[9]:''),40,400,true,'','','','T');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ORT'].":",200,'');
awis_FORM_Erstelle_TextFeld('FIL_ORT',($filSuche[4]=='on'?$filSuche[2]:''),40,400,true,'','','','T');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_PLZ'].":",200,'');
awis_FORM_Erstelle_TextFeld('FIL_PLZ',($filSuche[4]=='on'?$filSuche[3]:''),10,100,true,'','','','T');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_LAN_WWSKENN'].":",200,'');
$SQL = "SELECT LAN_WWSKENN, LAN_LAND FROM LAENDER ORDER BY 1";
$BitteWaehlen = $AWISSprachKonserven['Liste']['lst_ALLE_0'];
awis_FORM_Erstelle_SelectFeld('FIL_LAND','',400,true,$con,$SQL,$BitteWaehlen,'','LAN_LAND','','','');
awis_FORM_ZeileEnde();



//echo "<table id=SuchMaske>";

//echo "<tr><td width=220>".$AWISSprachKonserven['FIL']['FIL_ID'].":</td><td><input name=txtFIL_ID accesskey=n size=5 value=\"" . ($filSuche[4]=='on'?$filSuche[0]:'') . "\"></td></tr>";
//echo "<tr><td width=220>".$AWISSprachKonserven['FIL']['FIL_BEZ'].":</td><td><input name=txtFIL_BEZ accesskey=f size=35 value=\"" . ($filSuche[4]=='on'?$filSuche[1]:'') . "\"></td></tr>";
//echo "<tr><td width=220>".$AWISSprachKonserven['FIL']['FIL_STRASSE'].":</td><td><input name=txtFIL_STRASSE accesskey=o size=35 value=\"" . ($filSuche[4]=='on'?$filSuche[9]:'') . "\"></td></tr>";
//echo "<tr><td width=220>".$AWISSprachKonserven['FIL']['FIL_ORT'].":</td><td><input name=txtFIL_ORT accesskey=o size=35 value=\"" . ($filSuche[4]=='on'?$filSuche[2]:'') . "\"></td></tr>";
//echo "<tr><td width=220>".$AWISSprachKonserven['FIL']['FIL_PLZ'].":</td><td><input name=txtFIL_PLZ accesskey=p size=10 value=\"" . ($filSuche[4]=='on'?$filSuche[3]:'') . "\"></td></tr>";

/*
echo '<tr><td width=220>'.$AWISSprachKonserven['FIL']['FIL_LAN_WWSKENN'].':</td><td><select name=txtFIL_LAND>';
$rsFilialen_Land = awisOpenRecordset($con, "SELECT LAN_WWSKENN, LAN_LAND FROM LAENDER ORDER BY 1");
$rsFilialen_LandZeilen = $awisRSZeilen;
echo "<option value=0>::ALLE::</option>";
for($Filialen_LandZeile=0;$Filialen_LandZeile<$rsFilialen_LandZeilen;$Filialen_LandZeile++)
{
	echo "<option value=" . $rsFilialen_Land['LAN_WWSKENN'][$Filialen_LandZeile] . ">" . $rsFilialen_Land['LAN_WWSKENN'][$Filialen_LandZeile] . " - " . $rsFilialen_Land['LAN_LAND'][$Filialen_LandZeile] . "</option>";
}
echo "</select></td></tr>";
*/

If($RechteStufe & 2)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['Mitarbeitername'].":",200,'');
	awis_FORM_Erstelle_TextFeld('FIL_Mitarbeiter',($filSuche[4]=='on'?$filSuche[5]:''),20,200,true,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['Vorname'].":",130,'');
	awis_FORM_Erstelle_TextFeld('FIL_MitarbeiterVorname',($filSuche[4]=='on'?$filSuche[6]:''),20,200,true,'','','','T');
	awis_FORM_ZeileEnde();

	//echo "<tr><td width=220>&nbsp;</td></tr>";
	//echo '<tr><td width=220>'.$AWISSprachKonserven['FIL']['Mitarbeitername'].':</td><td><input name=txtFIL_Mitarbeiter accesskey=M size=30 value="' . ($filSuche[4]=='on'?$filSuche[5]:'') . "\"></td>";
	//echo '<td width=220 align=right>'.$AWISSprachKonserven['FIL']['Vorname'].':</td><td><input name=txtFIL_MitarbeiterVorname size=30 value="' . ($filSuche[4]=='on'?$filSuche[6]:'') . "\"></td></tr>";
	If($RechteStufe & 4)
	{
		//echo "<tr><td width=220>Personalnummer</td><td><input name=txtFIL_MitarbeiterPersnr size=10 value=\"" . ($filSuche[4]=='on'?$filSuche[7]:'') . "\"></td></tr>";
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Personalnummer'].":",200,'');
		awis_FORM_Erstelle_TextFeld('FIL_MitarbeiterPersnr',($filSuche[4]=='on'?$filSuche[7]:''),10,100,true,'','','','T');
		awis_FORM_ZeileEnde();
	}
}
else
{
	echo "<input type=hidden name=txtFIL_Mitarbeiter value=''>";
}

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].":",200,'');
awis_FORM_Erstelle_Checkbox('AuswahlSpeichern',$filSuche[4],30,true,'on','');
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();

echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png title='Formularinhalt l&ouml;schen' name=cmdReset onclick=location.href='./filialinfo_Main.php?Reset=True';>";

echo "<input type=hidden name=cmdAktion value=Filialinfos>";

echo "</form>";

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>

