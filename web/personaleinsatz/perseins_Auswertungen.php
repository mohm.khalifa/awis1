<?php
global $awisDBFehler;
global $awisRSZeilen;
global $Params;
global $rsMIT;
global $AWISBenutzer;

ini_set('max_execution_time','120');

define('FPDF_FONTPATH','font/');
require 'fpdi.php';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con, 1304);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze Auswertungen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}
// Legt die Bereiche fest (1=VERTIEB, 2=ATU Academy
$MBEBereiche = awisBenutzerRecht($con, 1306);
if($MBEBereiche==0)		// Wg. Umstellung!! -> Rausnehmen, wenn Rechte vergeben wurden
{
	$MBEBereiche=1;
}
$MBEBereicheAnz = substr_count(sprintf('%b',$MBEBereiche),'1');		// Anzahl Bereiche

awis_Debug(1,$_REQUEST);

if(!isset($_POST['txtListe']))
{
	echo '<form name=frmAuswertungen method=post action=./perseins_Main.php?cmdAktion=Auswertungen>';
	echo '<table border=0 width=300>';

	/******* Datum *******/

	echo '<tr>';
	echo '<td>Datum vom</td>';
	echo '<td><input name=txtDatumVom size=9 tabindex=20 value=' . date('01.01.Y') . '>';
	echo '</tr><tr><td>bis<td><input name=txtDatumBis size=9 tabindex=21 value=' . date('31.12.Y') . '></td>';
	echo '</tr>';
	
	/******* Regionalzentren *******/
	
	$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschr�nkungen nach RZ
	$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $AWISBenutzer->BenutzerName()));
	$RZUserListe = '';
	for($i=0;$i<16;$i++)
	{
		if(($RZListeStufe&pow(2,$i))==pow(2,$i))
		{
			$RZUserListe .= ';' . $RZListe[$i+1];
		}
	}
	$RZUserListe .= ';';

	echo '<tr>';
	echo '<td>Regionalzentrum</td>';
	echo '<td>';
	$rsREZ = awisOpenRecordset($con,"SELECT * FROM Regionalzentren ORDER BY REZ_BEZEICHNUNG");
	$rsREZZeilen = $awisRSZeilen;
	echo "<select name=txtMIT_REZ_ID tabindex=40>";
	
	if($RZUserListe==';')
	{
		echo '<option value=0>::alle Regionalzentren::</option>';
	}
	
	for($i=0; $i<$rsREZZeilen;$i++)
	{
		if($RZUserListe!=';')
		{
			if(strstr($RZUserListe,';'.$rsREZ["REZ_ID"][$i].';')=='')
			{
				continue;
			}
		}
		echo "<option ";
		if($rsREZ["REZ_ID"][$i] == ($Params[0]=="on"?$Params[5]:''))
		{
	  		echo " selected ";
		}
		echo "value=" . $rsREZ["REZ_ID"][$i] . ">". $rsREZ["REZ_BEZEICHNUNG"][$i] . "</option>";
	}
	echo "</select></td></tr>";

	/******* Filiale *******/
	echo '<tr>';
	echo '<td>Filiale</td>';
	echo '<td><input name=txtFIL_ID size=5 tabindex=30 value=' . ($Params[0]=="on"?$Params[4]:'')  . '></td>';
	echo '</tr>';

	
	/******* Mitarbeiter ********/
	echo '<tr>';
	echo '<td>Mitarbeiter</td>';
	echo '<td>';
	echo '<select name=txtMEI_MIT_KEY tabindex=10>';
	echo '<option value=-1>::alle::</option>';

	
	$SQL = "SELECT * ";
	$SQL .= " FROM Mitarbeiter";
	$Bedingung = "";
	if(($MBEBereiche&1)==1)			// Vertrieb
	{
		$Bedingung .= " OR  MIT_Bereich='Vertrieb'";
	}
	if(($MBEBereiche&2)==2)			// ATU Academy
	{
		$Bedingung .= " OR  MIT_Bereich='ATU Academy'";
	}
	if(($MBEBereiche&4)==4)			// Filial-QS erstellt am 21.02.06 CA
	{
		$Bedingung .= " OR  MIT_Bereich='Filial-QS'";
	}
	if(($MBEBereiche&8)==8)			// Gro�kunden erstellt am 21.06.06 CA
	{
		$Bedingung .= " OR  MIT_Bereich='Gro�kunden'";
	}
	if(($MBEBereiche&16)==16)			// Category Management erstellt am 19.04.07 CA
	{
		$Bedingung .= " OR  MIT_Bereich='Category Management'";
	}
	if(($MBEBereiche&32)==32)			// Smart-Repair erstellt am 19.11.07 CA
	{
		$Bedingung .= " OR  MIT_Bereich='Smart-Repair'";
	}
	if(($MBEBereiche&64)==64)			// Service-Europa-L erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-L'";
	}
	if(($MBEBereiche&128)==128)			// Service-Europa-TSB erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-TSB'";
	}
	if(($MBEBereiche&256)==256)			// Service-Europa-WT erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-WT'";
	}
	if(($MBEBereiche&512)==512)			// Service-Europa-HUB erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-HUB'";
	}
	if(($MBEBereiche&1024)==1024)		// Service-Europa-TWB erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-TWB'";
	}
	if(($MBEBereiche&2048)==2048)		// Service-Europa-DLM erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-DLM'";
	}
	if(($MBEBereiche&4096)==4096)		// Service-Europa-PM erstellt am 06.02.08 TR
	{
		$Bedingung .= " OR  MIT_Bereich='Service-Europa-PM'";
	}						
	
	if(($RechteStufe&2)==2)	// Einschr�nkung Technik-�bersicht
	{
		$Bedingung .= ' AND MIT_MTA_ID IN (30,40,50)';
	}
	
	$SQL .= " WHERE " . substr($Bedingung,4);
	
	$SQL .= ' ORDER BY MIT_BEZEICHNUNG';

	$rsPER = awisOpenRecordset($con, $SQL);
	$rsPERZeilen = $awisRSZeilen;

	for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
	{
		if($RZUserListe!=';')
		{
			if(strstr($RZUserListe,';'.$rsPER['MIT_REZ_ID'][$PerZeile].';')=='')
			{
				continue;
			}
		}
		echo "<option value='" . $rsPER['MIT_KEY'][$PerZeile] . "'";
		echo ">" . $rsPER['MIT_BEZEICHNUNG'][$PerZeile] . "</option>";
	}
	echo '</select>';
	echo '</td></tr>';	
	unset($rsPER);

	
	
	/******* Mitarbeitert�tigkeiten ********/
	echo '<tr>';
	echo '<td>T�tigkeit</td>';
	echo '<td>';
	echo '<select name=txtMIT_MTA_ID tabindex=10>';
	echo '<option value=-1>::alle::</option>';
	$rsPER = awisOpenRecordset($con, "SELECT * FROM MitarbeiterTaetigkeiten ORDER BY MTA_BEZEICHNUNG");
	$rsPERZeilen = $awisRSZeilen;

	for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
	{
		if((($RechteStufe&2)==2 AND ($rsPER['MTA_ID'][$PerZeile]==30 OR $rsPER['MTA_ID'][$PerZeile]==40 OR $rsPER['MTA_ID'][$PerZeile]==50)) OR ($RechteStufe&2)!=2)
		{
			echo "<option value='" . $rsPER['MTA_ID'][$PerZeile] . "'";
			echo ">" . $rsPER['MTA_BEZEICHNUNG'][$PerZeile] . "</option>";
		}
	}
	echo '</select>';
	echo '</td></tr>';	
	unset($rsPER);
	
	
	
	echo '<tr><td colspan=2><hr></td></tr>'	;	
	/************* Auswertungen *************/
	echo '<tr>';
	echo '<td>Auswertung</td><td>';
	echo '<select name=txtListe>';
	
	if(($RechteStufe&1)==1 OR ($RechteStufe&2)==2)
	{
		echo '<option value=1>Jahres�bersicht</option>';
	}
	if(($RechteStufe&4)==4)
	{
		echo '<option value=4>Samstagsauswertung</option>';
	}
	if(($RechteStufe&8)==8)
	{
		echo '<option value=8>Gebiets�bersicht</option>';
	}
	if(($RechteStufe&16)==16)
	{
		echo '<option value=16>Gebiets�bersicht Ausland</option>';
	}
	if(($RechteStufe&32)==32)
	{
		echo '<option value=32>Jahres�bersicht nach KW</option>';
	}
	if(($RechteStufe&64)==64)
	{
		echo '<option value=64>Urlaubsplan (alt/neu)</option>';
	}
	
	echo '</select></td></tr>';

	/*************** Bereich ********************/
				/*********** Einsatzbereich ************/
	if($MBEBereicheAnz>1)
	{
		echo '<tr>';
		echo '<td>Einsatzbereich</td>';
		echo '<td>';
		echo "<select name=txtMIT_BEREICH>";
		if(($MBEBereiche & 1)==1)			// Vertrieb
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='Vertrieb'?' selected':'') . '>Vertrieb</option>';
		}
		if(($MBEBereiche & 2)==2)			// ATU Academy
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"ATU Academy"'?' selected':'') . '>ATU Academy</option>';
		}
		if(($MBEBereiche & 4)==4)			// Filial-QS erstellt am 21.02.06 CA
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='Filial-QS'?' selected':'') . '>Filial-QS</option>';
		}
		if(($MBEBereiche & 8)==8)			// Grosskunden erstellt am 24.02.07 SK
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='Gro�kunden'?' selected':'') . '>Gro�kunden</option>';
		}
		if(($MBEBereiche & 16)==16)			// Category Management erstellt am 19.04.07 CA
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Category Management"'?' selected':'') . '>Category Management</option>';
		}
		if(($MBEBereiche & 32)==32)			// Smart-Repair erstellt am 19.11.07 CA
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Smart-Repair"'?' selected':'') . '>Smart-Repair</option>';
		}
		if(($MBEBereiche & 64)==64)			// Service-Europa-L erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-L"'?' selected':'') . '>Service-Europa-L</option>';
		}
		if(($MBEBereiche & 128)==128)			// Service-Europa-TSB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-TSB"'?' selected':'') . '>Service-Europa-TSB</option>';
		}
		if(($MBEBereiche & 256)==256)			// Service-Europa-WT erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-WT"'?' selected':'') . '>Service-Europa-WT</option>';
		}
		if(($MBEBereiche & 512)==512)			// Service-Europa-HUB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-HUB"'?' selected':'') . '>Service-Europa-HUB</option>';
		}
		if(($MBEBereiche & 1024)==1024)			// Service-Europa-TWB erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-TWB"'?' selected':'') . '>Service-Europa-TWB</option>';
		}
		if(($MBEBereiche & 2048)==2048)			// Service-Europa-DLM erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-DLM"'?' selected':'') . '>Service-Europa-DLM</option>';
		}									
		if(($MBEBereiche & 4096)==4096)			// Service-Europa-PM erstellt am 06.02.08 TR
		{
			echo '<option' . ($rsMIT['MIT_BEREICH'][0]=='"Service-Europa-PM"'?' selected':'') . '>Service-Europa-PM</option>';
		}	
		echo "</select></td></tr>";
	}
	else 
	{
		//echo '<input type=Hidden name=txtMIT_BEREICH value=' . ((($MBEBereiche & 1)==1)?'Vertrieb':'ATU Academy') . '>';
		// ertellt am 21.02.06 CA
		echo '<input type=Hidden name=txtMIT_BEREICH value=';
		switch ($MBEBereiche) {
			case 1:
				echo 'Vertrieb';
				break;
			case 2:
				echo '"ATU Academy"';
				break;
			case 4:
				echo 'Filial-QS';
				break;
			case 8:
				echo 'Gro�kunden';
				break;
			case 16:
				echo '"Category Management"';
				break;
			case 32:
				echo '"Smart-Repair"';
				break;
			case 64:
				echo '"Service-Europa-L"';
				break;
			case 128:
				echo '"Service-Europa-TSB"';
				break;
			case 256:
				echo '"Service-Europa-WT"';
				break;
			case 512:
				echo '"Service-Europa-HUB"';
				break;
			case 1024:
				echo '"Service-Europa-TWB"';
				break;
			case 2048:
				echo '"Service-Europa-DLM"';
				break;
			case 4096:
				echo '"Service-Europa-PM"';
				break;																								
		}
		echo '>';
	}		
	
	
	echo '</table>';

	echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

	echo '</form>';
}
else			// Listen anzeigen
{
	$Summen = array();	
	$RasterZeile=9999;
		/********************************************
		* 
		* Jahres�bersicht
		* 
		********************************************/
	if($_POST['txtListe']=='1')
	{
		$SQL = "select MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0) AS MEAKEY, MEA_BEZEICHNUNG, DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, to_char(MEI_TAG,'RRRR') AS Jahr, to_char(MEI_TAG,'MM') AS Monat, COUNT(*) AS Anzahl, MIT_KEY ";
		$SQL .= ' FROM Mitarbeiter INNER JOIN MitarbeiterEinsaetze ON MIT_KEY = MEI_MIT_KEY';
		$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
		$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';
		$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
		$SQL .= ' WHERE MEA_KEY <> 9';
		$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

		if($_POST['txtDatumVom']!='')		//txtDatumVom
		{
			$SQL .= " AND MEI_TAG>=TO_DATE('" . $_POST['txtDatumVom'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtDatumBis']!='')		//txtDatumBis
		{
			$SQL .= " AND MEI_TAG<=TO_DATE('" . $_POST['txtDatumBis'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtFIL_ID']!='')		//txtFIL_ID
		{
			$SQL .= " AND MEI_FIL_ID=" . $_POST['txtFIL_ID'] . "";
		}	
		if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
		{
			$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
		}	
		if($_POST['txtMEI_MIT_KEY']!='-1')
		{
			$SQL .= " AND MEI_MIT_KEY=" . $_POST['txtMEI_MIT_KEY'] . "";
		}
		if($_POST['txtMIT_MTA_ID']!='-1')
		{
			$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
		}
		
		if(($RechteStufe&2)==2)	// Einschr�nkung Technik-�bersicht
		{
			$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
		}
		$SQL .= " GROUP BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'MM'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
		$SQL .= " ORDER BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'MM'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
	
		awis_Debug(1,$SQL);
		
		$rsMEI = awisOpenRecordset($con, $SQL );
		$rsMEIZeilen = $awisRSZeilen;

		$pdf = new fpdi('l','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
		$ATULogo = $pdf->ImportPage(1);				

		if($rsMEIZeilen==0)
		{
			$pdf->addpage();
			$pdf->SetFont('Arial','',10);
			$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
		}
		
		$RasterZeile=9999;
		$LetzterMitarbeiter = '';
		$LetztesJahr = '';
		$LetztesREZ='';
		$LetztesMEA='';
		
		for($i=0;$i<$rsMEIZeilen;$i++)
		{
				// Seitenwechsel?
			if($LetztesJahr!=$rsMEI['JAHR'][$i] OR $LetztesREZ!=$rsMEI['REZ_BEZEICHNUNG'][$i] OR $LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
			{
				$LetztesJahr=$rsMEI['JAHR'][$i];
				$LetzterMitarbeiter=$rsMEI['MIT_KEY'][$i];
				$LetztesREZ=$rsMEI['REZ_BEZEICHNUNG'][$i];
				$LetztesMEA='';

				$RasterZeile = 9999;	// Neue Seite erzwingen
			}

				// Neue Seite beginnen
			if($RasterZeile > 190)	
			{
				foreach($Summen AS $Pos=>$Wert)
				{
					$pdf->setXY((13*12)+100,$Pos-10);
					$pdf->cell(20,6,$Wert,1,0,'R',0);
				}
				$Summen = array();		// Neue Summen

				$pdf->addpage();
				$pdf->useTemplate($ATULogo,270,4,20);

				$pdf->SetFont('Arial','',6);
				$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);
	
				$RasterZeile = 10;
				$pdf->SetFont('Arial','',20);
				$pdf->text(10,$RasterZeile,"Filialbesuche vom " . $_POST['txtDatumVom'] . ' bis ' . $_POST['txtDatumBis']);
				$RasterZeile = 40;

				$pdf->setXY(270,12);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);
	
				$pdf->SetFillColor(240,240,240);
				$pdf->SetDrawColor(0,0,0);
				$pdf->setXY(10,$RasterZeile-10);
	
				$pdf->SetFont('Arial','',12);
				$pdf->cell(102,6,$rsMEI['MIT_BEZEICHNUNG'][$i] . ' (' . $rsMEI['REZ_BEZEICHNUNG'][$i] .')',1,0,'L',1);
	
				$pdf->SetFont('Arial','',10);
				for($Std=1;$Std<=12;$Std++)
				{
					$pdf->setXY(($Std*12)+100,$RasterZeile-10);
					$pdf->SetDrawColor(0,0,0);
					$pdf->cell(12,6,date('M',mktime(0,0,0,$Std,1,2005)),1,0,'C',1);
				}
				$pdf->setXY(($Std*12)+100,$RasterZeile-10);
				$pdf->SetDrawColor(0,0,0);
				$pdf->cell(20,6,'Gesamt',1,0,'C',1);
				
			}

					// Neue Zeile
			if($LetztesMEA != $rsMEI['MEAKEY'][$i])
			{
				if($RasterZeile>180)
				{
					$RasterZeile=9999;
					$i--;
					continue;
				}
				$pdf->setXY(10,$RasterZeile-4);
				$pdf->cell(102,6,$rsMEI['MEA_BEZEICHNUNG'][$i] . ' ' . $rsMEI['FILIALE'][$i],1,0,'L',1);
				$pdf->line(112,$RasterZeile-4,276,$RasterZeile-4);				
				$pdf->line(112,$RasterZeile+2,276,$RasterZeile+2);
				
				$RasterZeile+=6;
				$LetztesMEA = $rsMEI['MEAKEY'][$i];
			}
					// Auf den Monat positionieren
			$pdf->setXY(($rsMEI['MONAT'][$i]*12)+100,$RasterZeile-10);
			$pdf->cell(12,6,$rsMEI['ANZAHL'][$i],1,0,'C',0);

			if (isset($Summen[$RasterZeile]))
			{
				$Summen[$RasterZeile] += $rsMEI['ANZAHL'][$i];
			}
			else 
			{
				$Summen[$RasterZeile] = $rsMEI['ANZAHL'][$i];
			}			
		}		// Ende for
		foreach($Summen AS $Pos=>$Wert)
		{
			$pdf->setXY((13*12)+100,$Pos-10);
			$pdf->cell(20,6,$Wert,1,0,'R',0);
		}
		$Summen = array();		// Neue Summen


	}	// Ende Jahres�bersicht
		/********************************************
		* 
		* Samstagsauswertung
		* 
		********************************************/
	elseif($_POST['txtListe']=='4')
	{
		$SQL = "select MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY, MEA_BEZEICHNUNG, ";
		$SQL .= " DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, to_char(MEI_TAG,'RRRR') AS Jahr, to_char(MEI_TAG,'MM') AS Monat, MIT_KEY, MEI_TAG ";
		$SQL .= ' FROM Mitarbeiter INNER JOIN MitarbeiterEinsaetze ON MIT_KEY = MEI_MIT_KEY';
		$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
		$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';
		$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
		$SQL .= " WHERE MEA_KEY <> 9 AND to_char(MEI_TAG,'D')=6";
		$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

		if($_POST['txtDatumVom']!='')		//txtDatumVom
		{
			$SQL .= " AND MEI_TAG>=TO_DATE('" . $_POST['txtDatumVom'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtDatumBis']!='')		//txtDatumBis
		{
			$SQL .= " AND MEI_TAG<=TO_DATE('" . $_POST['txtDatumBis'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtFIL_ID']!='')		//txtFIL_ID
		{
			$SQL .= " AND MEI_FIL_ID=" . $_POST['txtFIL_ID'] . "";
		}	
		if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
		{
			$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
		}	
		if($_POST['txtMEI_MIT_KEY']!='-1')
		{
			$SQL .= " AND MEI_MIT_KEY=" . $_POST['txtMEI_MIT_KEY'] . "";
		}
		if($_POST['txtMIT_MTA_ID']!='-1')
		{
			$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
		}

		$SQL .= ' ORDER BY MIT_REZ_ID, MIT_KEY, MEI_TAG, MEI_VONXTZ_ID';
		
		$rsMEI = awisOpenRecordset($con, $SQL );
		$rsMEIZeilen = $awisRSZeilen;


		$pdf = new fpdi('p','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
		$ATULogo = $pdf->ImportPage(1);				

		if($rsMEIZeilen==0)
		{
			$pdf->addpage();
			$pdf->SetFont('Arial','',10);
			$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
		}

		$RasterZeile=9999;
		$LetzterMitarbeiter = '';
		$LetztesJahr = '';
		
		for($i=0;$i<$rsMEIZeilen;$i++)
		{
				// Seitenwechsel?
			if($LetztesJahr!=$rsMEI['JAHR'][$i] OR $LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
			{
				if($LetztesJahr!='')
				{
					$pdf->setXY(10,$RasterZeile+2);
					$pdf->cell(110,6,'Gesamt',1,0,'R',0);
					for($SumNr=0;$SumNr<4;$SumNr++)
					{
						$pdf->setXY(($SumNr*10)+120,$RasterZeile+2);
						$pdf->cell(10,6,$Summen[$SumNr],1,0,'C',0);
					}
				}
				$Summen = array();		// Neue Summen

				$LetztesJahr=$rsMEI['JAHR'][$i];
				$LetzterMitarbeiter=$rsMEI['MIT_KEY'][$i];

				$RasterZeile = 9999;	// Neue Seite erzwingen
			}

				// Neue Seite beginnen
			if($RasterZeile > 190)	
			{

				$pdf->addpage();
				$pdf->useTemplate($ATULogo,180,4,20);
	
				$pdf->setXY(10,5);
				$pdf->SetFont('Arial','',16);
				$pdf->cell(190,6,"Einsatz Samstage vom " . $_POST['txtDatumVom'] . ' bis ' . $_POST['txtDatumBis'],0,0,'C',0);

				$pdf->setXY(10,10);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(190,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

				$pdf->setXY(180,12);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);

				$RasterZeile = 40;
	
				$pdf->SetFillColor(240,240,240);
				$pdf->SetDrawColor(0,0,0);
				$pdf->setXY(10,$RasterZeile-10);
	
				$pdf->SetFont('Arial','',12);
				$pdf->cell(150,5,$rsMEI['MIT_BEZEICHNUNG'][$i] . ' (' . $rsMEI['REZ_BEZEICHNUNG'][$i] .')',1,0,'L',1);
	
				$pdf->SetFont('Arial','',9);
				$pdf->setXY(10,$RasterZeile-4);
				$pdf->cell(20,5,'Datum',1,0,'C',1);
				$pdf->setXY(30,$RasterZeile-4);
				$pdf->cell(100,5,'T�tigkeit',1,0,'C',1);
				$pdf->setXY(130,$RasterZeile-4);
				$pdf->cell(10,5,'Frei',1,0,'C',1);
				$pdf->setXY(140,$RasterZeile-4);
				$pdf->cell(10,5,'Urlaub',1,0,'C',1);
				$pdf->setXY(150,$RasterZeile-4);
				$pdf->cell(10,5,'Krank',1,0,'C',1);
			}
					// Auf den Monat positionieren
			$RasterZeile+=5;
			$pdf->setXY(10,$RasterZeile-4);
			$LetzterTag = $rsMEI['MEI_TAG'][$i];
			$pdf->cell(20,5,str_replace('-','.',$rsMEI['MEI_TAG'][$i]),1,0,'R',0);
			$pdf->setXY(30,$RasterZeile-4);

			$Text='';
			for($j=$i;$j<$rsMEIZeilen;$j++)
			{
				if($LetzterTag != $rsMEI['MEI_TAG'][$j])
				{
					break;
				}
				$Text .= ' / ' . $rsMEI['MEA_BEZEICHNUNG'][$j];
				if($rsMEI['MEA_KEY'][$j]==8)		// Filiale
				{
					$Text .= ' ' . $rsMEI['FILIALE'][$j];
				}
				if(isset($rsMEI['MEI_BEMERKUNG'][$j]) AND $rsMEI['MEI_BEMERKUNG'][$j]!='')
				{
					$Text .= ' (' . $rsMEI['MEI_BEMERKUNG'][$j] . ')';
				}
			}
			$Text = substr($Text, 3);
			$MehrEintraege = strpos($Text, '/');		// Sind mehr Eintr�ge vorhanden? (-> FREI nur dann werten, wenn es aleine steht)
			$i=$j-1;
			
			$pdf->cell(100,5,$Text,1,0,'L',0);
			$Text = '';

	
			$Summen[0] ++;	// Gesamtanzahl
			$pdf->setXY(130,$RasterZeile-4);
			if($rsMEI['MEA_KEY'][$i]==4 and $MehrEintraege==0)			// Frei
			{
				$Summen[1] ++;
				$Text = 'X';
			}
			$pdf->cell(10,5,$Text,1,0,'C',0);
			$Text = '';

			$pdf->setXY(140,$RasterZeile-4);
			if($rsMEI['MEA_KEY'][$i]==3)			// Urlaub
			{
				$Summen[2] ++;
				$Text = 'X';
			}
			$pdf->cell(10,5,$Text,1,0,'C',0);
			$Text = '';

			$pdf->setXY(150,$RasterZeile-4);
			if($rsMEI['MEA_KEY'][$i]==5)			// Krank
			{
				$Summen[3] ++;
				$Text = 'X';
			}
			$pdf->cell(10,5,$Text,1,0,'C',0);
			$Text = '';
		}		// Ende for

			// Summe f�r den Letzten Mitarbeiter
		$pdf->setXY(10,$RasterZeile+2);
		$pdf->cell(110,5,'Gesamt',1,0,'R',0);
		for($SumNr=0;$SumNr<4;$SumNr++)
		{
			$pdf->setXY(($SumNr*10)+120,$RasterZeile+2);
			$pdf->cell(10,5,$Summen[$SumNr],1,0,'C',0);
		}

	}	// Ende Auswertung Samstag
		/********************************************
		* 
		* Gebiets�bersicht
		* 
		********************************************/
	elseif($_POST['txtListe']=='8')
	{
		include "./perseins_Auswertungen_Gebietsuebersicht.php";
	}	// Ende Gebiets�bersicht
	elseif($_POST['txtListe']=='16')
	{
		include "./perseins_Auswertungen_GebietsuebersichtAusland.php";
	}	// Ende Gebiets�bersicht
	elseif($_POST['txtListe']=='32')		// Wochenuebersicht
	{
		include "./perseins_Auswertungen_Kalenderwoche.php";					
	}
	elseif($_POST['txtListe']=='64')		// Urlaubsplan
	{
		include "./perseins_Auswertungen_Urlaubsplan.php";					
	}
	// PDF Datei speichern
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	//$pdf->saveas($DateiName);
	$pdf->Output($DateiName,'F');
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";
	
	//echo '<br><a target=_new href=/dateien_anzeigen.php?Bereich=Export&Typ=pdf&Datei=Personaleinsatz.pdf>PDF Datei �ffnen</a>';
	echo "<br><br><a href=./perseins_Main.php?cmdAktion=Auswertungen>Neue Auswertung</a>";

}

awislogoff($con);
?>