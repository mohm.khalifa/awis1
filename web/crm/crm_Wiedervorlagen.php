<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('CWV','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','Auswahl_ALLE');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3703 = awisBenutzerRecht($con,3703,$AWISBenutzer->BenutzerName());
if($Recht3703==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_FORM_FormularStart();
echo '<form name=frmWiedervorlagen action=./crm_Main.php?cmdAktion=Wiedervorlagen method=post>';

$EditRecht=(($Recht3703&6)!=0);

$SQL = 'SELECT CRMWiedervorlagen.*, KON_NAME1 || \', \' || KON_Name2 AS KONName ';
$SQL .= ' ,CAD_NAME1, CAD_PLZ';
$SQL .= ' FROM CRMWiedervorlagen ';
$SQL .= ' INNER JOIN CRMAdressen ON CWV_CAD_KEY = CAD_KEY';
$SQL .= ' LEFT OUTER JOIN Kontakte ON CWV_KON_KEY = KON_KEY';

$Bedingung = '';

// Wenn dieses Recht fehlr, d�rfen nur eigene angezeigt werden
if(($Recht3703&16)==0)
{
	$Bedingung  .= ' AND CWV_KON_KEY=0'.$AWISBenutzer->BenutzerKontaktKEY();
}

$Param = array_fill(0,10,'');

if(isset($_POST['cmdSuche_x']))
{
	$Param = (isset($_POST['txtauchZukuenftige'])?'1':'').';'.intval($_POST['txtKON_KEY']);
	awis_BenutzerParameterSpeichern($con,'SucheCRMWiedervorlagen',$AWISBenutzer->BenutzerName(),$Param);
	$Param = explode(';',$Param);
}
elseif(isset($_GET['Liste']) OR isset($_GET['Block']) OR isset($_GET['CWVSort']))
{
	$Param = explode(';',awis_BenutzerParameter($con,'SucheCRMWiedervorlagen',$AWISBenutzer->BenutzerName()));
}


if($Param[0]!='1')
{
	$Bedingung .= ' AND CWV_DATUM <= TRUNC(SYSDATE)';		
}


if($Param[1]!='' and $Param[1]!=0)
{
	$Bedingung .= ' AND CWV_KON_KEY=0'.intval($Param[1]);
}

if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung, 4);	
}

if(!isset($_GET['CWVSort']))
{
	$SQL .= ' ORDER BY KON_NAME1, KON_NAME2, CWV_DATUM DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CWVSort']);
}
$rsCWV = awisOpenRecordset($con,$SQL);
$rsCWVZeilen = $awisRSZeilen;
awis_Debug(1,$Param,$SQL,$_GET,$_POST);
// Auswahl auch f�r zuk�nftige
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['txtAuchZukuenftige'],250);
awis_FORM_Erstelle_Checkbox('auchZukuenftige',($Param[0]==1?'on':''),10,true,'on');
awis_FORM_ZeileEnde();

if(($Recht3703&16)!=0)	// Nur wer alle sehen darf, bekommt eine Auswahl
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_KON_KEY'],250);
	$SQL = " SELECT DISTINCT KON_KEY, KON_NAME1 || ', ' || COALESCE(KON_NAME2,'') AS KONTAKTNAME";
	$SQL .= ' FROM CRMWiedervorlagen ';
	$SQL .= ' INNER JOIN Kontakte ON KON_KEY = CWV_KON_KEY';
	$SQL .= ' ORDER BY KONTAKTNAME';
	awis_FORM_Erstelle_SelectFeld('KON_KEY',$Param[1],200,true,$con,$SQL,$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	awis_FORM_ZeileEnde();
}

// �berschriftszeile
awis_FORM_ZeileStart();

$Link = './crm_Main.php?cmdAktion=Wiedervorlagen'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Link .= '&CWVSort=CWV_DATUM'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_DATUM'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_DATUM'],150,'',$Link);

$Link = './crm_Main.php?cmdAktion=Wiedervorlagen'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Link .= '&CWVSort=CAD_NAME1'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CAD_NAME1'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_NAME1'],250,'',$Link);

$Link = './crm_Main.php?cmdAktion=Wiedervorlagen'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Link .= '&CWVSort=CAD_PLZ'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CAD_PLZ'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_PLZ'],100,'',$Link);

$Link = './crm_Main.php?cmdAktion=Wiedervorlagen'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Link .= '&CWVSort=CWV_BEMERKUNG'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_BEMERKUNG'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_BEMERKUNG'],400,'',$Link);

$Link = './crm_Main.php?cmdAktion=Wiedervorlagen'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Link .= '&CWVSort=CWV_KON_KEY'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_KON_KEY'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_KON_KEY'],250,'',$Link);
 
awis_FORM_ZeileEnde();


	// Blockweise
$StartZeile=0;
if(isset($_GET['Block']))
{
	$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
}

	// Seitenweises bl�ttern
if($rsCWVZeilen>$MaxDSAnzahl)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

	for($i=0;$i<($rsCWVZeilen/$MaxDSAnzahl);$i++)
	{
		if($i!=($StartZeile/$MaxDSAnzahl))
		{
			$Text = '&nbsp;<a href=./crm_Main.php?cmdAktion=Wiedervorlagen&Block='.$i.(isset($_GET['CWVSort'])?'&CWVSort='.$_GET['CWVSort']:'').'>'.($i+1).'</a>';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
		else
		{
			$Text = '&nbsp;'.($i+1).'';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
	}
	awis_FORM_ZeileEnde();
}

for($CWVZeile=$StartZeile;$CWVZeile<$rsCWVZeilen and $CWVZeile<$StartZeile+$MaxDSAnzahl;$CWVZeile++)
{
	awis_FORM_ZeileStart();
	$Link= './crm_Main.php?cmdAktion=Details&&CAD_KEY=0'.$rsCWV['CWV_CAD_KEY'][$CWVZeile];
	awis_FORM_Erstelle_ListenFeld('CWV_DATUM',$rsCWV['CWV_DATUM'][$CWVZeile],20,150,false,($CWVZeile%2),'',$Link,'T');
	awis_FORM_Erstelle_ListenFeld('CAD_NAME1',$rsCWV['CAD_NAME1'][$CWVZeile],20,250,false,($CWVZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('CAD_PLZ',$rsCWV['CAD_PLZ'][$CWVZeile],20,100,false,($CWVZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('CWV_BEMERKUNG',$rsCWV['CWV_BEMERKUNG'][$CWVZeile],1,400,false,($CWVZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('KONNAME',$rsCWV['KONNAME'][$CWVZeile],20,250,false,($CWVZeile%2),'','','T');
	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();

awis_FORM_SchaltflaechenStart();
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
awis_FORM_SchaltflaechenEnde();

echo '</form>';
?>
