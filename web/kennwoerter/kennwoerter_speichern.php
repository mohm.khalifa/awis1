<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('KEW','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','KennwoerterStimmenNichtUeberein');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	$TXT_Speichern = $Form->LadeTexte($TextKonserven);

	$AWIS_KEY1 = (isset($_POST['txtKEW_KEY'])?$_POST['txtKEW_KEY']:0);

	//***********************************************************************************
	//** Kennwoerter
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtKEW_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$Fehler='';
		$AWIS_KEY1 = $_POST['txtKEW_KEY'];
		if($_POST['txtKEWBENUTZERKENNWORT1']!==$_POST['txtKEWBENUTZERKENNWORT2'])
		{
			$Fehler .= $TXT_Speichern['Meldung']['KennwoerterStimmenNichtUeberein'];
		}
		// Daten auf Vollst�ndigkeit pr�fen
		$Pflichtfelder = array('KEW_BEZEICHNUNG','KEW_BEREICH','KEW_BENUTZERNAME');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['KEW'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}

		$rsKEW = $DB->RecordSetOeffnen('SELECT * FROM Kennwoerter WHERE KEW_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
		if($rsKEW->EOF())
		{
			$SQL = 'INSERT INTO Kennwoerter';
			$SQL .= '(KEW_BEZEICHNUNG,KEW_XBN_KEY,KEW_BEREICH,KEW_BENUTZERNAME,KEWBENUTZERKENNWORT';
			$SQL .= ',KEW_KUNDENNUMMER,KEW_BEMERKUNG';
			$SQL .= ',KEW_USER,KEW_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtKEW_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$AWISBenutzer->BenutzerID(),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKEW_BEREICH'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKEW_BENUTZERNAME'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKEWBENUTZERKENNWORT1'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKEW_KUNDENNUMMER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKEW_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200909151906);
			}
			$SQL = 'SELECT seq_KEW_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			if($_POST['oldKEWBENUTZERKENNWORT1']!=$_POST['txtKEWBENUTZERKENNWORT1'])
			{
				$FeldListe = ', KEW_BENUTZERKENNWORT = '.$DB->FeldInhaltFormat('T',$_POST['txtKEWBENUTZERKENNWORT1']);
			}
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsKEW->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsKEW->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsKEW->FeldInfo($FeldName,'TypKZ'),$rsKEW->FeldInhalt($FeldName),true);

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsKEW->FeldInhalt('KEW_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Kennwoerter SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', KEW_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', KEW_userdat=sysdate';
				$SQL .= ' WHERE KEW_key=0' . $_POST['txtKEW_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',808181117,$SQL,2);
				}
			}
		}
	}


//*******************************************************************************************
//* Kennwortzugriffe
//*******************************************************************************************

	$Felder = $Form->NameInArray($_POST,'txtKEZ',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY2=$_POST['txtKEZ_KEY'];
		$rsKEZ = $DB->RecordSetOeffnen('SELECT * FROM KennwoerterZugriffe WHERE KEZ_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtKEZ_KEY'],false));

		if($rsKEZ->EOF())		// Neue Zuordnung
		{
			$Fehler = '';
				// Daten auf Vollst�ndigkeit pr�fen
			$Pflichtfelder = array('KEZ_XXX_KEY');

			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['AST'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}

			$SQL = 'INSERT INTO KennwoerterZugriffe';
			$SQL .= '(KEZ_KEW_KEY, KEZ_XXX_KEY, KEZ_AENDERN';
			$SQL .= ',KEZ_USER, KEZ_USERDAT)';
			$SQL .= 'VALUES (';
			$SQL .= '' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtKEZ_XXX_KEY'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtKEZ_AENDERN'],false);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('KEZ speichern',909031332,$SQL,4);
			}
			$SQL = 'SELECT seq_KEZ_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->Feldinhalt('KEY');

		}
		else 					// ge�nderter Artikel
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{

					$WertNeu=$DB->FeldInhaltFormat($rsKEZ->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsKEZ->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsKEZ->FeldInfo($FeldName,'TypKZ'),$rsKEZ->FeldInhalt($FeldName),true);
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsKEZ->FeldInhalt('KEZ_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				die();
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE KennwoerterZugriffe SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', KEZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', KEZ_userdat=sysdate';
				$SQL .= ' WHERE KEZ_KEY=0' . $_POST['txtKEZ_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),909131431,$SQL,2);
				}
			}
			$AWIS_KEY2=$_POST['txtKEZ_KEY'];
		}
	}



	//******************************************************************
	//* Gruppen f�r Kennworter
	//******************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtKZG_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY1 = $_POST['txtKZG_KEY'];
		$TextKonserven[]=array('KZG','KZG_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler='';
		$Pflichtfelder = array('KZG_BEZEICHNUNG');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['KZG'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}

		$rsKZG = $DB->RecordSetOeffnen('SELECT * FROM KENNWOERTERGRUPPEN WHERE KZG_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1));
		if($rsKZG->EOF())
		{
			$SQL = 'INSERT INTO KENNWOERTERGRUPPEN';
			$SQL .= '(KZG_XBN_KEY,KZG_BEZEICHNUNG,KZG_BEMERKUNG';
			$SQL .= ',KZG_USER,KZG_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtKZG_XBN_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKZG_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtKZG_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200909151906);
			}
			//Sequenz aus den Benutzern
			$SQL = 'SELECT seq_XBN_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsKZG->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsKZG->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsKZG->FeldInfo($FeldName,'TypKZ'),$rsKZG->FeldInhalt($FeldName),true);

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsKZG->FeldInhalt('KZG_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE KENNWOERTERGRUPPEN SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', KZG_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', KZG_userdat=sysdate';
				$SQL .= ' WHERE KZG_key=0' . $_POST['txtKZG_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',808181117,$SQL,2);
				}
			}
		}
	}



	//******************************************************************
	//* Gruppen f�r AP f�r Lieferanten
	//******************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtKZM_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY2 = $_POST['txtKZM_KEY'];
		$TextKonserven[]=array('KZM','KZM_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
$Form->DebugAusgabe(1,$_POST);
		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler='';
		$Pflichtfelder = array('KZM_XBN_KEY');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['KZM'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}

		$rsKZM = $DB->RecordSetOeffnen('SELECT * FROM KENNWOERTERGRUPPENMITGLIEDER WHERE KZM_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtKZM_KEY']));
		if($rsKZM->EOF())
		{
			$SQL = 'INSERT INTO KENNWOERTERGRUPPENMITGLIEDER';
			$SQL .= '(KZM_KZG_KEY, KZM_XBN_KEY,KZM_MITGLIEDVON, KZM_MITGLIEDBIS';
			$SQL .= ',KZM_USER,KZM_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' '. $DB->FeldInhaltFormat('N0',$_POST['txtKZM_KZG_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtKZM_XBN_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtKZM_MITGLIEDVON'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtKZM_MITGLIEDBIS'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern'.$SQL, 200910201154);
			}
			$SQL = 'SELECT seq_KZM_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsKZM->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsKZM->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsKZM->FeldInfo($FeldName,'TypKZ'),$rsKZM->FeldInhalt($FeldName),true);

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsKZM->FeldInhalt('KZM_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE KENNWOERTERGRUPPENMITGLIEDER SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', KZM_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', KZM_userdat=sysdate';
				$SQL .= ' WHERE KZM_key=0' . $_POST['txtKZM_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',808181117,$SQL,2);
				}
			}
		}
	}



}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>