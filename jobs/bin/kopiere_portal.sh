#!/bin/bash
#
# Skript zum Datenaustausch mit dem Portal
#
# Autor: Sacha Kerres
# Datum: 16.12.2009
#
########################################################################

#ZIEL_SERVER=10.100.129.1
ZIEL_SERVER=atuportal.de

PFAD_LOKAL=/daten/daten/portal/
PFAD_REMOTE=/Daten/daten

DATEI_AENDERUNG=portalzugang.dat
DATEI_STAND=portalzugang.akt
DATEI_INFO=infos.xml
DATEI_LIMIT=personallimits.dat

if test -f $PFAD_LOKAL/$DATEI_AENDERUNG; then
  scp $PFAD_LOKAL/$DATEI_AENDERUNG  $ZIEL_SERVER:$PFAD_REMOTE/$DATEI_AENDERUNG.$(date +%j%H%M) && mv $PFAD_LOKAL/$DATEI_AENDERUNG $PFAD_LOKAL/$DATEI_AENDERUNG.versand
fi

if ! test -f $PFAD_LOKAL/$DATEI_STAND; then
  scp $ZIEL_SERVER:$PFAD_REMOTE/$DATEI_STAND  $PFAD_LOKAL
fi 

#
# Infodatei mit den Inhalten
#
scp $ZIEL_SERVER:$PFAD_REMOTE/$DATEI_INFO  $PFAD_LOKAL

#
# Personallimit kopieren
#
if test -f $PFAD_LOKAL/$DATEI_LIMIT; then
  scp $PFAD_LOKAL/$DATEI_LIMIT $ZIEL_SERVER:$PFAD_REMOTE/
  mv -f $PFAD_LOKAL/$DATEI_LIMIT $PFAD_LOKAL/$DATEI_LIMIT.$(date +%a)
fi

