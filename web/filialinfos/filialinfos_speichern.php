<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$Form->DebugAusgabe(1,$_POST);
	
	
	$AWIS_KEY1 = $_POST['txtFIL_ID'];

	
	//***********************************************
	// Speicherung von Lacktagen

    $Felder = $Form->NameInArray($_POST, 'txtLacktage_',2,1);
    if(count($Felder) > 0){
        $LackTage=0;
        $LackTageAlt=0;

        if($Felder!='')
        {
            $BitWert = 0;
            foreach($Felder AS $Feld)
            {
                $FeldTeile = explode('_',$Feld);
                $BitWert |= (int)$FeldTeile[1];
            }

            $LackTage=$BitWert;
        }


        $FIT_ID = 851;
        $SQL = 'SELECT FIF_KEY, FIF_WERT FROM Filialinfos ';
        $SQL .= ' WHERE FIF_FIT_ID = '.$DB->WertSetzen('FIF','N0',$FIT_ID).' AND FIF_FIL_ID = '.$DB->WertSetzen('FIF', 'N0',$_POST['txtFIL_ID'],true);
        $rsFIF = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIF'));
        if($rsFIF->EOF())
        {
            $SQL = 'INSERT INTO Filialinfos';
            $SQL .= ' (FIF_FIL_ID, FIF_FIT_ID, FIF_WERT, FIF_IMQ_ID, FIF_USER, FIF_USERDAT)';
            $SQL .= ' VALUES(';
            $SQL .= ' '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID'],true);
            $SQL .= ','.$DB->FeldInhaltFormat('N0',$FIT_ID,true);
            $SQL .= ','.$LackTage;
            $SQL .= ',4';
            $SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
            $SQL .= ', sysdate)';
        }
        elseif($rsFIF->FeldInhalt('FIF_WERT') != $LackTage){
            $SQL = ' UPDATE Filialinfos';
            $SQL .= ' SET FIF_WERT = '.$LackTage;
            $SQL .= ' , FIF_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
            $SQL .= ' , FIF_USERDAT = SYSDATE';
            $SQL .= ' WHERE FIF_KEY = '.$rsFIF->FeldInhalt('FIF_KEY');
            $SQL .= ' AND FIF_FIT_ID = '.$FIT_ID;
        }

        $DB->Ausfuehren($SQL,'',true);
    }


	// Ende Lacktage
	//***********************************************
	
	$Felder = $Form->NameInArray($_POST,'txtFIT_WERT_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);

	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);

			$FIT_ID = $FeldTeile[2];
			$FIF_KEY = $FeldTeile[3];
			$DatenTyp = $FeldTeile[4];
			$WertAlt = $DB->FeldInhaltFormat($DatenTyp,$_POST['old'.substr($Feld,3)]);
			$WertNeu = $DB->FeldInhaltFormat($DatenTyp,$_POST[$Feld]);
			$Speichern = true;

			if($FIF_KEY == '')
			{
				$SQL = 'SELECT * FROM Filialinfos WHERE FIF_FIT_ID = '.$DB->FeldInhaltFormat('N0',$FIT_ID,true).' AND FIF_FIL_ID = '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID'],true);
				$rsFIF = $DB->RecordSetOeffnen($SQL);
				if(!$rsFIF->EOF())
				{
					$Speichern = false;
				}
			}

			$DB->TransaktionBegin();
			if($Speichern AND $WertAlt!==$WertNeu)
			{
				if($FIF_KEY == '')			// Neuer Eintrag
				{
					$SQL = 'INSERT INTO Filialinfos';
					$SQL .= ' (FIF_FIL_ID, FIF_FIT_ID, FIF_WERT, FIF_IMQ_ID, FIF_USER, FIF_USERDAT)';
					$SQL .= ' VALUES(';
					$SQL .= ' '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID'],true);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$FIT_ID,true);
					$SQL .= ','.$WertNeu;
					$SQL .= ',4';
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
					$SQL .= ', sysdate)';
				}
				else
				{
					$SQL = 'SELECT FIT_PROTOKOLL FROM FilialInfostypen2 WHERE FIT_ID = '.$FIT_ID;
					$rsFIT = $DB->RecordSetOeffnen($SQL);
					if($rsFIT->FeldInhalt('FIT_PROTOKOLL')==1)
					{
						$SQL = 'INSERT INTO FilialInfos_H';
						$SQL .= '(H00_FIF_KEY, H00_WERT, H00_FIT_ID, H00_FIL_ID, H00_READONLY, H00_IMQ_ID';
						$SQL .= ', H00_USER, H00_USERDAT, H00_CMDTYPE)';
						$SQL .= ' SELECT FIF_KEY, FIF_WERT, FIF_FIT_ID, FIF_FIL_ID, FIF_READONLY, FIF_IMQ_ID';
						$SQL .= ', FIF_USER, FIF_USERDAT, \'U\'';
						$SQL .= ' FROM FilialInfos';
						$SQL .= ' WHERE FIF_KEY = '.$FIF_KEY;

						$DB->Ausfuehren($SQL,'',true);
					}

					$SQL = ' UPDATE Filialinfos';
					$SQL .= ' SET FIF_WERT = '.$WertNeu;
					$SQL .= ' , FIF_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
					$SQL .= ' , FIF_USERDAT = SYSDATE';
					$SQL .= ' WHERE FIF_KEY = '.$FIF_KEY;
					$SQL .= ' AND FIF_FIT_ID = '.$FIT_ID;
				}

				$DB->Ausfuehren($SQL,'',true);

			}
			$DB->TransaktionCommit();
		}
	}


	//*****************************************************************************
	//*
	//* Zukauflieferanten
	//*
	//*****************************************************************************
	$Felder = $Form->NameInArray($_POST, 'txtFLI_',1,1);

	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('FLI','FLI_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('FLI_LIE_NR','FLI_SORTIERUNG','FLI_GUELTIGAB','FLI_GUELTIGBIS');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FLI'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtFLI_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO FilialenLieferanten';
			$SQL .= '(FLI_LIE_NR,FLI_FIL_ID,FLI_GUELTIGAB,FLI_GUELTIGBIS,FLI_SORTIERUNG,FLI_LVK_KEY,FLI_LIEFERART';
			$SQL .= ', FLI_BESTELLZEITMODO,FLI_BESTELLZEITFR,FLI_BESTELLZEITSA';
			$SQL .= ',FLI_USER, FLI_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtFLI_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFLI_GUELTIGAB'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFLI_GUELTIGBIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_SORTIERUNG'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_LVK_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_LIEFERART'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtFLI_BESTELLZEITMODO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtFLI_BESTELLZEITFR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtFLI_BESTELLZEITSA'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			$DB->Ausfuehren($SQL);

			$SQL = 'SELECT seq_FLI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsFLI = $DB->RecordSetOeffnen('SELECT * FROM FilialenLieferanten WHERE FLI_key=' . $_POST['txtFLI_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$rsFLI->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsFLI->FeldInhalt('FLI_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE FilialenLieferanten SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', FLI_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', FLI_userdat=sysdate';
				$SQL .= ' WHERE FLI_key=0' . $_POST['txtFLI_KEY'] . '';
				$DB->Ausfuehren($SQL,'',true);

				$AWIS_KEY2 = $_POST['txtFLI_KEY'];
			}
		}
	}

	// Kundennummern in der Filiale f�r den Lieferanten
	
	$Felder = $Form->NameInArray($_POST, 'txtLKD_',1,1);

	if($Felder!='' AND $_POST['txtLKD_KUNDENNR']!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('LKD','LKD_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('LKD_LIE_NR','LKD_FIL_ID','LKD_KUNDENNR');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['LKD'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtLKD_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO LieferantenKundennummern';
			$SQL .= '(LKD_LIE_NR,LKD_FIL_ID,LKD_KUNDENNR,LKD_KENNWORT,LKD_ZUSATZ1,LKD_VERWENDUNG';
			$SQL .= ',LKD_USER, LKD_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtLKD_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_KUNDENNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_KENNWORT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_ZUSATZ1'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtLKD_VERWENDUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			$DB->Ausfuehren($SQL,'',true);
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsLKD = $DB->RecordSetOeffnen('SELECT * FROM LieferantenKundennummern WHERE LKD_key=' . $_POST['txtLKD_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$rsLKD->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsLKD->FeldInhalt('LKD_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE LieferantenKundennummern SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', LKD_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', LKD_userdat=sysdate';
				$SQL .= ' WHERE LKD_key=0' . $_POST['txtLKD_KEY'] . '';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

	//*****************************************************************************
	//*
	//* ATU Karten
	//*
	//*****************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST,'txtCGK_KONTAKT',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,15));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_KONTAKT='.($_POST[$Feld]=='::bitte w�hlen::'?'null':intval($_POST[$Feld]));
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

	$Felder = explode(';',$Form->NameInArray($_POST,'txtCGK_BEMERKUNG',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_BEMERKUNG=\''.$_POST[$Feld].'\'';
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}
	
	
	//*****************************************************************************
	//*
	//* Entsorgerinfos
	//*
	//*****************************************************************************
	if(isset($_POST['txtSBT_KEY']))
	{
		$Felder = explode(';',$Form->NameInArray($_POST,'oldCHKSBI_',1,1));
		$SQL = '';
		$SBTDAYS = array();
		$Update = 0;
		foreach($Felder AS $Feld)
		{	
			$Key = substr($Feld,10,2);
			if(isset($_POST['txt'.substr($Feld,3)]) and ($_POST[$Feld]!=$_POST['txt'.substr($Feld,3)]))
			{
				$SBTDAYS['SBT_'.$Key] = '1';
				$Update = 1;
			}
			else if(isset($_POST['txt'.substr($Feld,3)]) and ($_POST[$Feld]==$_POST['txt'.substr($Feld,3)]))
			{
				$SBTDAYS['SBT_'.$Key] = '1';
			}
			else if(!isset($_POST['txt'.substr($Feld,3)]) and ($_POST[$Feld]==1))
			{
				$SBTDAYS['SBT_'.$Key] = '0';
				$Update = 1;
			}
			else
			{
				$SBTDAYS['SBT_'.$Key] = '0';
			}
		}
		
		
		
		if($_POST['txtSBI_Entsorger'] == '::bitte w�hlen::')
		{
			$Entsorger = '0';
		}
		else
		{
			$Entsorger = $_POST['txtSBI_Entsorger'];
		}
		
		if($_POST['oldSBI_Entsorger']!=$Entsorger)
		{
			$Update = 1;
		}
		
		
		if(floatval($_POST['txtSBT_KEY'])==0)
		{			
			$Fehler = '';
			$SQL = 'INSERT INTO sbabholtage';
			$SQL .= '(SBT_FIL_ID,SBT_MO,SBT_DI,SBT_MI,SBT_DO,SBT_FR,SBT_SA,SBT_SBN_KEY';
			$SQL .= ',SBT_USER,SBT_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_MO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_DI'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_MI'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_DO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_FR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$SBTDAYS['SBT_SA'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$Entsorger,true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			$DB->Ausfuehren($SQL,'',true);
			
			
		}
		else 
		{
			if($Update)
			{
				$SQL = 'UPDATE SBABHOLTAGE';
				$SQL .= ' SET SBT_MO='.$SBTDAYS['SBT_MO'];
				$SQL .= ' ,SBT_DI='.$SBTDAYS['SBT_DI'];
				$SQL .= ' ,SBT_MI='.$SBTDAYS['SBT_MI'];
				$SQL .= ' ,SBT_DO='.$SBTDAYS['SBT_DO'];
				$SQL .= ' ,SBT_FR='.$SBTDAYS['SBT_FR'];
				$SQL .= ' ,SBT_SA='.$SBTDAYS['SBT_SA'];
				$SQL .= ' ,SBT_SBN_KEY='.$Entsorger;
				$SQL .= ' ,SBT_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,SBT_USERDAT=SYSDATE';
				$SQL .= ' WHERE SBT_KEY='.$_POST['txtSBT_KEY'];
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}
	
	
	//*****************************************************************************
	//*
	//* Kartenterminals
	//*
	//*****************************************************************************
   
	if (isset($_POST['txtKZT_KEY']))
	{
	  
	    $TextKonserven[]=array('KNZ','KNZ_%');
	    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
	    
        //Pr�fe ob eingabe fertig und plausibel
        $Fehler = false;
    
    
        //TerminalID:
        if(isset($_POST['txtKNZ_TERMINALID']) and strlen($_POST['txtKNZ_TERMINALID']<= 0))
        {
            $Fehler = true;
            $Form->ZeileStart();
            $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_ERR_TERMINALID']);
            $Form->ZeileEnde();
        }
    
        //G�ltig ab:
        if(!isset($_POST['txtKNZ_GUELTIGAB']) or $Form->Format('D',$_POST['txtKNZ_GUELTIGAB']== '01.01.1970') or strlen($Form->Format('D',$_POST['txtKNZ_GUELTIGAB']))<=0 )
        {
            $Fehler = true;
            $Form->ZeileStart();
            $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_ERR_GUELTIG_AB']);
            $Form->ZeileEnde();
        }
    
    
        //G�ltig bis:
        if(!isset($_POST['txtKNZ_GUELTIGBIS']) and $Form->Format('D',$_POST['txtKNZ_GUELTIGBIS']== '01.01.1970') or strlen($Form->Format('D',$_POST['txtKNZ_GUELTIGAB']))<=0 )
        {
            $Fehler = true;
            $Form->ZeileStart();
            $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_ERR_GUELTIG_BIS']);    
            $Form->ZeileEnde();
        }
    
      
        if (!$Fehler)
        {
            //G�ltig bis muss gr��er als G�ltig ab

            if(strtotime($_POST['txtKNZ_GUELTIGBIS']) < strtotime($_POST['txtKNZ_GUELTIGAB']))
            {
                $Fehler = true;
                $Form->ZeileStart();
                $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_ERR_GUELTIG_UNPLAUSIBEL']);
                $Form->ZeileEnde();
            }
    
            //Es darf eine g�ltige Terminal ID nicht zweimal geben. (Unique Constraint nicht m�glich, da eine Terminal-ID durchaus doppelt vergeben werden kann, jedoch nur einmal als G�ltig
            $SQL  ='select * from (SELECT';
            $SQL .=' KZT_KEY ,';
            $SQL .=' KZT_FIL_ID ,';
            $SQL .=' KZT_TERMINAL_ID ,';
            $SQL .=' KZT_GUELTIG_AB ,';
            $SQL .=' KZT_GUELTIG_BIS ,';
            $SQL .=' KZT_USER ,';
            $SQL .=' KZT_USERDAT ,';
            $SQL .=' case when KZT_GUELTIG_AB <= trunc(sysdate) and KZT_GUELTIG_BIS >= trunc(sysdate) then 1 else 0 end as gueltig ';
            $SQL .=' FROM';
            $SQL .=' kartenzahlungenterminals)';
            $SQL .= ' WHERE KZT_GUELTIG_BIS >= \''. $_POST['txtKNZ_GUELTIGAB'] . '\' and KZT_TERMINAL_ID = \''.(isset($_POST['txtKNZ_TERMINALID'])?$_POST['txtKNZ_TERMINALID']:0) . '\'';
            
            $Form->DebugAusgabe(1,$SQL);
            
            $rsFehlerPruefung = $DB->RecordSetOeffnen($SQL);
            if ($rsFehlerPruefung->AnzahlDatensaetze()>0)
            {
                $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_ERR_TERMINAL_VORHANDEN']);
                $Fehler = true;
            }
        }
      
        if (!$Fehler)
        {
          //Kein Fehler -> Speichern
          //Update oder Insert? 
          if ($_POST['txtKZT_KEY']==-1)
          {
              //Neuer DS
              $SQLInsert  ='insert into kartenzahlungenterminals (KZT_FIL_ID ,';
              $SQLInsert .=' KZT_TERMINAL_ID ,';
              $SQLInsert .=' KZT_GUELTIG_AB ,';
              $SQLInsert .=' KZT_GUELTIG_BIS ,';
              $SQLInsert .=' KZT_USER ,';
              $SQLInsert .=' KZT_USERDAT) values';
              $SQLInsert .=' (';
              $SQLInsert .=' '. $AWIS_KEY1 . ' ,';
              $SQLInsert .=' \'' . $_POST['txtKNZ_TERMINALID'] .'\' ,';
              $SQLInsert .=' \'' . $_POST['txtKNZ_GUELTIGAB'] .'\' ,';
              $SQLInsert .=' \'' . $_POST['txtKNZ_GUELTIGBIS'] .'\' ,';
              $SQLInsert .=' \'' . $AWISBenutzer->BenutzerName() . '\',';
              $SQLInsert .=' sysdate)';
              $Form->DebugAusgabe(1,$SQLInsert);
              $DB->Ausfuehren($SQLInsert,'',true);
              
          }
          elseif ($_POST['txtKZT_KEY']!=-1)
          {
              //Updaten
              $SQLUpdate  = ' Update kartenzahlungenterminals';
              $SQLUpdate .= ' set KZT_GUELTIG_AB = \'' . $Form->Format('D0',$_POST['txtKNZ_GUELTIGAB']) . '\',';
              $SQLUpdate .= ' KZT_GUELTIG_BIS = \''. $Form->Format('D0',$_POST['txtKNZ_GUELTIGBIS']).'\',';
              $SQLUpdate .= ' KZT_USER = \'' . $AWISBenutzer->BenutzerName() . '\',';
              $SQLUpdate .= ' KZT_USERDAT = sysdate';
              $SQLUpdate .= ' WHERE KZT_KEY =  ' . $_POST['txtKZT_KEY'] ;
              $Form->DebugAusgabe(1,$SQLUpdate);
              $DB->Ausfuehren($SQLUpdate,'',true);
              
          }
          $Form->Hinweistext($TXT_Speichern['KNZ']['KNZ_SPEICHERN_OK']);           
        }
     
	}

	if(isset($_POST['txtGSR_RAHMENVERTR']))
	{
		$GST_BEREICH = '';
		if($_POST['txtGSR_VERTRAGSART'] == 0)
		{
			$GST_BEREICH = 'Strom';
			$BEREICHSKRZ = 'txtSVT_';
		}
		else
		{
			$GST_BEREICH = 'Gas';
			$BEREICHSKRZ = 'txtGVT_';
		}

		$Felder = $Form->NameInArray($_POST,$BEREICHSKRZ,1,1);
		$Felder = explode(';',$Felder);

		$SQL = '';
		$GSR_KEY = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if($_POST['txtRAV_KEY'] == -1) {
				$GSR_KEY = $_POST['txtRAV_RAHMENVERTR'];

			}
			else {
				$GSR_KEY = $_POST['txtRAV_KEY'];
			}
			if ($_POST['old' . $FeldName] !== $_POST['txt' . $FeldName]) {
				$SQL = 'merge into gsvinfos gsi';
				$SQL .= ' using (select gst_key from gsvinfotypen';
				$SQL .= ' where GST_BEREICH ='.$DB->WertSetzen('GSI', 'T',$GST_BEREICH);
				$SQL .= ' and gst_feldname = '.$DB->WertSetzen('GSI', 'T',$FeldName).')GST ';
				$SQL .= ' on (gsi.gsi_gst_key = gst.gst_key and gsi_xxx_key = '.$DB->WertSetzen('GSI', 'N0',$_POST['txtFIL_ID']).' and gsi.gsi_gsr_key='.$DB->WertSetzen('GSI', 'N0',$GSR_KEY).')';
				$SQL .= ' WHEN MATCHED THEN';
				$SQL .= ' update set gsi.gsi_wert='.$DB->WertSetzen('GSI', 'T',$_POST['txt' . $FeldName]);
				$SQL .= ' WHEN NOT MATCHED THEN';
				$SQL .= ' INSERT (gsi.GSI_GSR_KEY,gsi.GSI_XXX_KEY,gsi.GSI_GST_KEY,gsi.GSI_WERT)';
				$SQL .= ' values('.$DB->WertSetzen('GSI', 'N0',$GSR_KEY).','.$DB->WertSetzen('GSI', 'N0',$_POST['txtFIL_ID']).',gst.gst_key,'.$DB->WertSetzen('GSI', 'T',$_POST['txt' . $FeldName]).')';
				
				$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('GSI', true));
			}
		}
	}

	//*****************************************************************************
	//* 20120430,ho
	//* �nderung Telefon/Telefax Fachbetriebe in Reiter BetreuungExtern
	//*
	//*****************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST,'txtFAB_TELEFON',1,1));
	$SQL = '';
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			if(isset($_POST[$Feld]))
			{
				if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
				{
					$Key = intval(substr($Feld,14));
					$SQL = 'UPDATE FACHBETRIEBE';
					$SQL .= ' SET FAB_TELEFON=' .$DB->FeldInhaltFormat('T',$_POST[$Feld]) .',';
					$SQL .= ' FAB_USER=\''.$AWISBenutzer->BenutzerName().'\',';
					$SQL .= ' FAB_USERDAT=SYSDATE';
					$SQL .= ' WHERE FAB_KEY=0'.$Key.'';
					$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
				}
			}
		}
	}
	
	$Felder = explode(';',$Form->NameInArray($_POST,'txtFAB_TELEFAX',1,1));
	$SQL = '';
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			if(isset($_POST[$Feld]))
			{
				if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
				{
					$Key = intval(substr($Feld,14));
					$SQL = 'UPDATE FACHBETRIEBE';
					$SQL .= ' SET FAB_TELEFAX=' .$DB->FeldInhaltFormat('T',$_POST[$Feld]) .',';
					$SQL .= ' FAB_USER=\''.$AWISBenutzer->BenutzerName().'\',';
					$SQL .= ' FAB_USERDAT=SYSDATE';
					$SQL .= ' WHERE FAB_KEY=0'.$Key.'';
					$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
				}
			}
		}
	}
	
	//*****************************************************************************
	//* 20120510,ho
	//* �nderung Zuordnung der Fachbetriebe im Reiter BetreuungExtern
	//*
	//*****************************************************************************
	$Felder = $Form->NameInArray($_POST, 'txtFFA_',1,1);

	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('FFA','FFA_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('FFA_GUELTIGAB','FFA_GUELTIGBIS');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FFA'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtFFA_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO Fachbetriebefilialen ';
			$SQL .= '(FFA_FAB_KEY,FFA_FIL_ID,FFA_GUELTIGAB,FFA_GUELTIGBIS,';
			$SQL .= ' FFA_USER, FFA_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtFFA_FAB_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFFA_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFFA_GUELTIGAB'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFFA_GUELTIGBIS'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			$Form->DebugAusgabe(1,$SQL);
			$DB->Ausfuehren($SQL,'',true);
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsFFA = $DB->RecordSetOeffnen('SELECT * FROM Fachbetriebefilialen WHERE FFA_key=' . $_POST['txtFFA_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$rsFFA->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsFFA->FeldInhalt('FFA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Fachbetriebefilialen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', FFA_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', FFA_userdat=sysdate';
				$SQL .= ' WHERE FFA_key=0' . $_POST['txtFFA_KEY'] . '';
				$Form->DebugAusgabe(1,$SQL);
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

    //***********************************************************************************
    //** Filialkompetenzen speichern
    //***********************************************************************************
    if(isset($_POST['txtFIK_BEZ'])) {

        $FIT_ID = 920;
        $DB->TransaktionBegin();

        $SQL = 'INSERT INTO FILIALINFOS(FIF_FIL_ID, FIF_FIT_ID, FIF_WERT, FIF_READONLY, FIF_IMQ_ID, FIF_USER, FIF_USERDAT)';
        $SQL .= ' VALUES (' . $DB->WertSetzen('FIF', 'N0', $_POST['txtFIL_ID']) .','. $FIT_ID .','. $DB->WertSetzen('FIF', 'T', $_POST['txtFIK_BEZ']) . ', 1, 4,' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName()) . ', SYSDATE)';
        try {
            $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('FIF'));
            $DB->TransaktionCommit();
        } catch (Exception $e) {
            $DB->TransaktionRollback();
            $Form->Fehler_Anzeigen('SpeicherFehler', 1, 'MELDEN', 5, '20018111214233');
        }
    }

    //***********************************************************************************
    //** �ffnungszeiten speichern
    //***********************************************************************************
    if(isset($_POST['txtFZP_FOZ_KEY'])) {

        $Modell = $_POST['txtFZP_FOZ_KEY'][0]; //Ist zwar ein Array, weil Mehrfachselect. Es kommt aber immer nur eins.

        $SQL = 'INSERT INTO FILIALENOEFFNUNGSZEITENPFLEGE(FZP_FIL_ID, FZP_FOZ_KEY, FZP_GUELTIG_AB, FZP_USER, FZP_USERDAT)';
        $SQL .= ' VALUES (' ;
        $SQL .=  $DB->WertSetzen('FIF', 'N0', $_POST['txtFIL_ID']) .', ';
        $SQL .=  $DB->WertSetzen('FIF', 'N0', $Modell) .', ';
        $SQL .=  $DB->WertSetzen('FIF', 'D', $_POST['txtFZP_GUELTIG_AB']) .', ';
        $SQL .=  $DB->WertSetzen('FIF', 'T', $AWISBenutzer->BenutzerName()) .', ';
        $SQL .= ' sysdate)';
        try {
            $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('FIF'));

        } catch (Exception $e) {
            if(strpos($e->getMessage(),'UID_FZP_FIL_ID_GUELTIG')!==false){
                $Form->Fehler_Anzeigen('UID_FZP_FIL_ID_GUELTIG', '', 'MELDEN', 5, '20018111214233');
            }else{
                $Form->Fehler_Anzeigen('SpeicherFehler', '', 'MELDEN', 5, '20018111214233');
            }

        }
    }
}

catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>