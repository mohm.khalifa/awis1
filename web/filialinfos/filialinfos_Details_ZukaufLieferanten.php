<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('FLI','%');
	$TextKonserven[]=array('LKD','%');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('LVK','LVK_NAME1');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Link_Route');
	$TextKonserven[]=array('Wort','Link_Maps');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5017= $AWISBenutzer->HatDasRecht(5017);		// Filialenlieferanten
	$Recht605= $AWISBenutzer->HatDasRecht(605);		// Stammdaten-Lieferanten
	if(($Recht5017&4)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILFLI'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
	$INET_LINK_Route =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Route');

	if(isset($_GET['FLI_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['FLI_KEY']);
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY FLI_SORTIERUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}

	$SQL = 'SELECT FILIALENLIEFERANTEN.*, LIE_NAME1, LieferantenVerkaufshaeuser.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM FILIALENLIEFERANTEN';
	$SQL .= ' INNER JOIN LIEFERANTEN ON FLI_LIE_NR = LIE_NR';
	$SQL .= ' LEFT OUTER JOIN LieferantenVerkaufshaeuser ON FLI_LVK_KEY = LVK_KEY';
	$SQL .= ' WHERE FLI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND FLI_KEY = '.$AWIS_KEY2;
	}

	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_FILFLI',serialize($Param));
		}
		elseif(isset($Param['BLOCK']) AND $Param['FIL_ID']==$AWIS_KEY1)
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		if($Block==0)
		{
			$Block=1;
		}
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$SQL, $Param);
	$rsFLI = $DB->RecordSetOeffnen($SQL);

	$Param['FIL_ID']=$AWIS_KEY1;
	$AWISBenutzer->ParameterSchreiben('Formular_FILFLI',serialize($Param));

	if(($rsFLI->EOF() AND (!isset($_GET['FLI_KEY']) OR $_GET['FLI_KEY']!=-1)) OR $rsFLI->AnzahlDatensaetze()>1 OR isset($_GET['ListeFLI']))						// Liste anzeigen
	{
		$DetailAnsicht = false;

		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['FLI_LIE_NR']=80;
			$Breiten['LIE_NAME1']=300;
			$Breiten['FLI_PLZ']=60;
			$Breiten['FLI_SORTIERUNG']=90;
			$Breiten['LVK_NAME1']=150;
			$Breiten['LVK_STRASSE']=0;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['FLI_LIE_NR']=80;
			$Breiten['LIE_NAME1']=300;
			$Breiten['FLI_PLZ']=60;
			$Breiten['FLI_SORTIERUNG']=90;
			$Breiten['LVK_STRASSE']=200;
		}
		else
		{
			$Breiten['FLI_LIE_NR']=80;
			$Breiten['LIE_NAME1']=350;
			$Breiten['FLI_PLZ']=60;
			$Breiten['FLI_SORTIERUNG']=90;
			$Breiten['LVK_STRASSE']=350;
		}

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Icons = array();
		if(($Recht5017&2)==2)
		{
			$Icons[] = array('new','./filialinfos_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&FLI_KEY=-1','g');
		}
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FLI_LIE_NR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FLI_LIE_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLI']['FLI_LIE_NR'],$Breiten['FLI_LIE_NR'],'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=LIE_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],$Breiten['LIE_NAME1'],'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FLI_SORTIERUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FLI_SORTIERUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLI']['FLI_SORTIERUNG'],$Breiten['FLI_SORTIERUNG'],'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FLI_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FLI_GUELTIGAB'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLI']['FLI_GUELTIGAB'],90,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FLI_GUELTIGBIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FLI_GUELTIGBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLI']['FLI_GUELTIGBIS'],90,'',$Link);
		if($Breiten['LVK_STRASSE']>0)
		{
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLI']['FLI_LVK_KEY'],$Breiten['LVK_STRASSE'],'');
		}
		$Form->ZeileEnde();

		$SQL = 'SELECT FIL_STRASSE, FIL_ORT, FIL_PLZ, LAN_CODE';
		$SQL .= ' FROM Filialen';
		$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		$SQL .= ' WHERE FIL_ID = '.$AWIS_KEY1;
		$rsFIL = $DB->RecordSetOeffnen($SQL);

		$DS=0;
		$ZeigeInaktive = $AWISBenutzer->ParameterLesen('Zukauflieferanten: Zeige inaktive Zuordnungen');
		while(!$rsFLI->EOF())
		{
			$LiefAktiv = true;

			$LiefAktiv = true;
			if($Form->PruefeDatum($rsFLI->FeldInhalt('FLI_GUELTIGAB'),0,0,true)>time()
				OR $Form->PruefeDatum($rsFLI->FeldInhalt('FLI_GUELTIGBIS'),0,0,true)<time()
				)
			{
				if($ZeigeInaktive==0)
				{
					$rsFLI->DSWeiter();
					continue;
				}
				$LiefAktiv = false;
			}

			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Icons = array();
			if(($Recht5017&4)==4)
			{
				$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FLI_KEY='.$rsFLI->FeldInhalt('FLI_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			}
			if(($Recht5017&8)==8)
			{
				$Icons[] = array('delete','./filialinfos_Main.php?cmdAktion=Details&Seite=Zukauflieferanten&Del='.$rsFLI->FeldInhalt('FLI_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Link = '';
			if($Recht605>0)
			{
				$Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsFLI->FeldInhalt('FLI_LIE_NR');
			}

			$Text = (strlen($rsFLI->FeldInhalt('FLI_LIE_NR'))>(int)($Breiten['FLI_LIE_NR']/$ListenSchriftFaktor)?substr($rsFLI->FeldInhalt('FLI_LIE_NR'),0,(int)($Breiten['FLI_LIE_NR']/$ListenSchriftFaktor)-2).'..':$rsFLI->FeldInhalt('FLI_LIE_NR'));
			$Form->Erstelle_ListenFeld('FLI_LIE_NR',$Text,0,$Breiten['FLI_LIE_NR'],false,($DS%2),($LiefAktiv?'':'font-style:italic'),$Link,'T','L',($Text!=$rsFLI->FeldInhalt('FLI_LIE_NR')?$rsFLI->FeldInhalt('FLI_LIE_NR'):''));
			$Text = (strlen($rsFLI->FeldInhalt('LIE_NAME1').' '.$rsFLI->FeldInhalt('FLI_HAUSNUMMER'))>(int)($Breiten['LIE_NAME1']/$ListenSchriftFaktor)?substr($rsFLI->FeldInhalt('LIE_NAME1').' '.$rsFLI->FeldInhalt('FLI_HAUSNUMMER'),0,(int)($Breiten['LIE_NAME1']/$ListenSchriftFaktor)-2).'..':$rsFLI->FeldInhalt('LIE_NAME1').' '.$rsFLI->FeldInhalt('FLI_HAUSNUMMER'));
			$Form->Erstelle_ListenFeld('LIE_NAME1',$Text,0,$Breiten['LIE_NAME1'],false,($DS%2),($LiefAktiv?'':'font-style:italic'),'','T','L',$rsFLI->FeldInhalt('LIE_NAME1').' '.$rsFLI->FeldInhalt('FLI_HAUSNUMMER'));
			$Form->Erstelle_ListenFeld('FLI_SORTIERUNG',$rsFLI->FeldInhalt('FLI_SORTIERUNG'),0,$Breiten['FLI_SORTIERUNG'],false,($DS%2),'','','T','L',$rsFLI->FeldInhalt('FLI_PLZ').' '.$rsFLI->FeldInhalt('FLI_SORTIERUNG'));
			$Form->Erstelle_ListenFeld('FLI_GUELTIGAB',$rsFLI->FeldInhalt('FLI_GUELTIGAB'),0,90,false,($DS%2),($LiefAktiv?'':'font-style:italic'),'','D','L','');
			$Form->Erstelle_ListenFeld('FLI_GUELTIGBIS',$rsFLI->FeldInhalt('FLI_GUELTIGBIS'),0,90,false,($DS%2),($LiefAktiv?'':'font-style:italic'),'','D','L');
			$Ort = $rsFLI->FeldInhalt('LVK_ORT').', '.$rsFLI->FeldInhalt('LVK_STRASSE');
			$Text = (strlen($Ort)>(int)($Breiten['LVK_STRASSE']/$ListenSchriftFaktor)?substr($Ort,0,(int)($Breiten['LVK_STRASSE']/$ListenSchriftFaktor)-2).'..':$Ort);
			if($Breiten['LVK_STRASSE']>0)
			{
				$Form->Erstelle_ListenFeld('LVK_STRASSE',$Text,0,$Breiten['LVK_STRASSE'],false,($DS%2),($LiefAktiv?'':'font-style:italic'),'','T','L');
			}

			$Parameter = array('$NAME1'=>urlencode($rsFLI->FeldInhalt('LVK_NAME1')),
						   '$NAME2'=>urlencode($rsFLI->FeldInhalt('LVK_NAME2')),
						   '$STRASSE'=>urlencode($rsFLI->FeldInhalt('LVK_STRASSE')),
						   '$HAUSNUMMER'=>'',
						   '$PLZ'=>urlencode($rsFLI->FeldInhalt('LVK_PLZ')),
						   '$ORT'=>urlencode($rsFLI->FeldInhalt('LVK_ORT')),
						   '$LAN_CODE'=>urlencode($rsFLI->FeldInhalt('LVK_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Karte,$Parameter);
			$Form->Erstelle_ListenBild('href','#MAPS',$Link,'/bilder/icon_globus.png',$AWISSprachKonserven['Wort']['Link_Maps'],($DS%2),'',16,16,20,'','NEU');
			$Parameter = array(
							'$STRASSE1'=>urlencode($rsFIL->FeldInhalt('FIL_STRASSE')),
						   	'$HAUSNUMMER1'=>'',
						   	'$PLZ1'=>urlencode($rsFIL->FeldInhalt('FIL_PLZ')),
						   	'$LAND1'=>urlencode($rsFIL->FeldInhalt('LAN_CODE')),
							'$STRASSE2'=>urlencode(utf8_encode($rsFLI->FeldInhalt('LVK_STRASSE'))),
						   	'$HAUSNUMMER2'=>'',
						   	'$PLZ2'=>urlencode($rsFLI->FeldInhalt('LVK_PLZ')),
						   	'$LAND2'=>urlencode($rsFLI->FeldInhalt('LVK_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Route,$Parameter);
			$Form->Erstelle_ListenBild('href','#MAPS',$Link,'/bilder/icon_route.png',$AWISSprachKonserven['Wort']['Link_Route'],($DS%2),'',16,16,20,'','NEU');

			$Form->ZeileEnde();

			$rsFLI->DSWeiter();
			$DS++;
		}

		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else
	{
		$AWIS_KEY2 = $rsFLI->FeldInhalt('FLI_KEY');


		$Param['KEY']=$AWIS_KEY2;
		$AWISBenutzer->ParameterSchreiben('Formular_FILFLI',serialize($Param));

		echo '<input type=hidden name=txtFLI_KEY value='.$AWIS_KEY2. '>';
		echo '<input type=hidden name=txtFLI_FIL_ID value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./filialinfos_Main.php?cmdAktion=Details&ListeFLI".(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFLI->FeldInhalt('FLI_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFLI->FeldInhalt('FLI_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$EditModus = ($Recht5017&6);

		$Form->ZeileStart();

		$Link = '';
		if($Recht605>0)
		{
			$Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsFLI->FeldInhalt('FLI_LIE_NR');
		}

		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_LIE_NR'].':',180,'',$Link);
		$SQL = 'SELECT LIE_NR, LIE_NAME1';
		$SQL .= ' FROM Lieferanten';
		$SQL .= ' INNER JOIN LieferantenInfos ON LIE_NR = LIN_LIE_NR AND LIN_ITY_KEY = 1 AND LIN_WERT = 1';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('FLI_LIE_NR',$rsFLI->FeldInhalt('FLI_LIE_NR'),200,$EditModus,$SQL,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_GUELTIGAB'].':',180);
		$Form->Erstelle_TextFeld('FLI_GUELTIGAB',$rsFLI->FeldInhalt('FLI_GUELTIGAB'),10,150,$EditModus,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_GUELTIGBIS'].':',100);
		$Form->Erstelle_TextFeld('FLI_GUELTIGBIS',$rsFLI->FeldInhalt('FLI_GUELTIGBIS'),10,150,$EditModus,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_SORTIERUNG'].':',180);
		$Form->Erstelle_TextFeld('FLI_SORTIERUNG',$rsFLI->FeldInhalt('FLI_SORTIERUNG'),10,150,$EditModus,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_LVK_KEY'].':',160);
		$AktuelleDaten = ($rsFLI->FeldInhalt('FLI_LVK_KEY')==''?array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']):array($rsFLI->FeldInhalt('FLI_LVK_KEY').'~'.$rsFLI->FeldInhalt('LVK_ORT').', '.$rsFLI->FeldInhalt('LVK_STRASSE')));
		$Form->Erstelle_SelectFeld('FLI_LVK_KEY',$rsFLI->FeldInhalt('FLI_LVK_KEY'),'500',$EditModus,'***LVK_Daten;txtFLI_LVK_KEY;LIE_NR=*txtFLI_LIE_NR&WERT='.$rsFLI->FeldInhalt('FLI_LVK_KEY'),'','','','',$AktuelleDaten,'','');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_LIEFERART'].':',180);
		$Daten = explode("|",$AWISSprachKonserven['FLI']['lst_FLI_LIEFERART']);
		$Form->Erstelle_SelectFeld('FLI_LIEFERART',$rsFLI->FeldInhalt('FLI_LIEFERART'),200,$EditModus,'','','','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_BESTELLZEITMODO'].':',180);
		$Form->Erstelle_TextFeld('FLI_BESTELLZEITMODO',$rsFLI->FeldInhalt('FLI_BESTELLZEITMODO'),10,150,$EditModus,'','','','T');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_BESTELLZEITFR'].':',180);
		$Form->Erstelle_TextFeld('FLI_BESTELLZEITFR',$rsFLI->FeldInhalt('FLI_BESTELLZEITFR'),10,150,$EditModus,'','','','T');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLI']['FLI_BESTELLZEITSA'].':',180);
		$Form->Erstelle_TextFeld('FLI_BESTELLZEITSA',$rsFLI->FeldInhalt('FLI_BESTELLZEITSA'),10,150,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$Recht10014 = $AWISBenutzer->HatDasRecht(10014);
		if(($Recht10014 & 1)==1 AND $rsFLI->FeldInhalt('FLI_LIE_NR')!='')
		{
			$Form->Trennzeile('L');

			$EditModus = (($Recht10014&2)==2);

			$SQL = ' SELECT *';
			$SQL .= ' FROM LIEFERANTENKUNDENNUMMERN';
			$SQL .= ' WHERE LKD_LIE_NR = '.$DB->FeldInhaltFormat('T',$rsFLI->FeldInhalt('FLI_LIE_NR'));
			$SQL .= ' AND LKD_FIL_ID = '.$AWIS_KEY1;
			$rsLKD = $DB->RecordSetOeffnen($SQL);

			$Form->Erstelle_HiddenFeld('LKD_KEY',$rsLKD->FeldInhalt('LKD_KEY'));
			$Form->Erstelle_HiddenFeld('LKD_LIE_NR',$rsFLI->FeldInhalt('FLI_LIE_NR'));
			$Form->Erstelle_HiddenFeld('LKD_FIL_ID',$AWIS_KEY1);
			$Form->Erstelle_HiddenFeld('LKD_VERWENDUNG',1);

				// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKD->FeldInhalt('LKD_USER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKD->FeldInhalt('LKD_USERDAT'));
			$Form->InfoZeile($Felder,'');


			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['LKD']['LKD_KUNDENNR'].':',180);
			$Form->Erstelle_TextFeld('LKD_KUNDENNR',$rsLKD->FeldInhalt('LKD_KUNDENNR'),15,150,$EditModus,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['LKD']['LKD_KENNWORT'].':',180);
			$Form->Erstelle_TextFeld('LKD_KENNWORT',$rsLKD->FeldInhalt('LKD_KENNWORT'),20,150,$EditModus,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['LKD']['LKD_ZUSATZ1'].':',180);
			$Form->Erstelle_TextFeld('LKD_ZUSATZ1',$rsLKD->FeldInhalt('LKD_ZUSATZ1'),10,150,$EditModus,'','','','T');
			$Form->ZeileEnde();

		}

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>