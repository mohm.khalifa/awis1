<?php

class ArrayOfSuchAnfrageArtikel implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SuchAnfrageArtikel[] $SuchAnfrageArtikel
     */
    protected $SuchAnfrageArtikel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SuchAnfrageArtikel[]
     */
    public function getSuchAnfrageArtikel()
    {
      return $this->SuchAnfrageArtikel;
    }

    /**
     * @param SuchAnfrageArtikel[] $SuchAnfrageArtikel
     * @return ArrayOfSuchAnfrageArtikel
     */
    public function setSuchAnfrageArtikel(array $SuchAnfrageArtikel = null)
    {
      $this->SuchAnfrageArtikel = $SuchAnfrageArtikel;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SuchAnfrageArtikel[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SuchAnfrageArtikel
     */
    public function offsetGet($offset)
    {
      return $this->SuchAnfrageArtikel[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SuchAnfrageArtikel $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SuchAnfrageArtikel[] = $value;
      } else {
        $this->SuchAnfrageArtikel[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SuchAnfrageArtikel[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SuchAnfrageArtikel Return the current element
     */
    public function current()
    {
      return current($this->SuchAnfrageArtikel);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SuchAnfrageArtikel);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SuchAnfrageArtikel);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SuchAnfrageArtikel);
    }

    /**
     * Countable implementation
     *
     * @return SuchAnfrageArtikel Return count of elements
     */
    public function count()
    {
      return count($this->SuchAnfrageArtikel);
    }

}
