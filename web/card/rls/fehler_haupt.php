<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<html>
<head>
<title>Fehler</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>

<body>
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center">
<?php
echo "<table border=\"1\" align=\"center\" width=\"100%\">
<tr><td colspan=\"2\" align=\"center\">
<h2>Fehler!</h2></td></tr>
<tr><td><b>Fehlerart:</b></td><td>".stripslashes($strFehlerart)."</td></tr>
<tr><td><b>Fehlertext:</b></td><td>".stripslashes($strFehlertext)."</td></tr>
<tr><td><b>Datei:</b></td><td>".stripslashes($strDatei)."</td></tr>"
.($intZeile==null?'':("<tr><td><b>Zeile:</b></td><td>".stripslashes($intZeile)."</td></tr>")).
"<tr><td><b>Benutzer:</b></td><td>".stripslashes($strBenutzer)."</td></tr>"
.($strSql==''?'':("<tr><td><b>SQL-Anweisung:</b></td><td><nobr>".stripslashes(str_replace(",",",<wbr>",$strSql))."<nobr></td></tr>")).
"</table>";
?>
</td></tr></table>
</body>
</html>