<?php
global $ReifenRechte;
global $con;
global $awisDBFehler;
global $AWISBenutzer;
global $awisRSZeilen;

if ($ReifenRechte == 0) {
    awisEreignis(3, 1000, 'Reifen', $AWISBenutzer->BenutzerName(), '', '', '');
    die("Keine ausreichenden Rechte!");
}

// Suchfelder in der Suchen-Seite

$RechteAST = awisBenutzerRecht($con, 400); // Artikelstamminfos
$Recht451 = awisBenutzerRecht($con, 451); // Bestaende
                                          //
                                          // Lokale Variablen
$Bedingung = ''; // Bedingung f�r SQL
$SQL = ''; // SQL Abfragestring
$ZeitVerbrauch = time();
// Aktualisieren wurde gew�hlt

If (isset($_POST['cmdSuche_x']) and $_POST['cmdSuche_x'] != '') {
    $Params = "";
    $Params .= $_POST['txtBreite']; // 0
    $Params .= ";" . $_POST['txtQSchnitt']; // 1
    $Params .= ";" . $_POST['txtDMesser']; // 2
    $Params .= ";" . $_POST['txtLLKW']; // 3
    $Params .= ";" . $_POST['txtLIndex']; // 4
    $Params .= ";" . $_POST['txtLIndex2']; // 5
    $Params .= ";" . $_POST['txtSpeedIndex']; // 6
    $Params .= ";" . $_POST['txtMaxVK']; // 7
    $Params .= ";" . $_POST['txtATUNr']; // 8
    $Params .= ";" . $_POST['txtBez']; // 9
    $Params .= ";" . (isset($_POST['txtSort']) ? $_POST['txtSort'] : ''); // 10
    $Params .= ";" . $_POST['txtTyp']; // 11
    $Params .= ";" . (isset($_POST['txtCCSort']) ? $_POST['txtCCSort'] : ''); // 12
    $Params .= ";" . (isset($_POST['txtReifSpd']) ? $_POST['txtReifSpd'] : ''); // 13
    $Params .= ";" . (isset($_POST['txtAuswahlSpeichern']) ? $_POST['txtAuswahlSpeichern'] : ''); // Alle gespeicherten Parameter wieder anzeigen
    $Params .= ";" . $_POST['txtHersteller']; // 15
    $Params .= ";" . (isset($_POST['txtKBListe']) ? $_POST['txtKBListe'] : ''); // 16
    $Params .= ";" . (isset($_POST['txtHerstellerZuerst']) ? $_POST['txtHerstellerZuerst'] : ''); // 17
    $Params .= ";" . (isset($_POST['txtLAN_CODE']) ? $_POST['txtLAN_CODE'] : ''); // 18
    $Params .= ";" . (isset($_POST['txtFahrzeuge']) ? $_POST['txtFahrzeuge'] : ''); // 19
    $Params .= ";" . (isset($_POST['txtReifLoad']) ? $_POST['txtReifLoad'] : ''); // 20
    $Params .= ";" . (isset($_POST['txtKBPreise']) ? $_POST['txtKBPreise'] : ''); // 21
    $Params .= ";" . $_POST['txtFiliale']; // 22
    $Params .= ";" . $_POST['txtROLLRES']; // 23
    $Params .= ";" . $_POST['txtWETGRIP']; // 24
    $Params .= ";" . $_POST['txtNOISECL']; // 25
    $Params .= ";" . $_POST['txtNOISEPE']; // 26
    
    awis_BenutzerParameterSpeichern($con, "ReifenSuche", $AWISBenutzer->BenutzerName(), $Params);
    $Params = explode(";", $Params);
    
    awis_BenutzerParameterSpeichern($con, "ArtikelStammSucheFiliale", $AWISBenutzer->BenutzerName(), $_POST['txtFiliale']);
} else // die gespeicherten Werte
{
    $Params = explode(";", awis_BenutzerParameter($con, "ReifenSuche", $AWISBenutzer->BenutzerName()));
    
    // Pr�fen, ob Filiale angemeldet ist
    $UserFilialen = awisBenutzerFilialen($con, $AWISBenutzer->BenutzerName(), 2);
    if ($UserFilialen != '') {
        $Params[22] = $UserFilialen;
    }
}

if ($Params[22] != '') {
    // Land zu der eingegebenen Filiale ermitteln
    $BindeVariablen = array();
    $BindeVariablen['var_N0_fil_id'] = $Params[22];
    
    $SQL = 'SELECT LAN_CODE, FIL_GRUPPE';
    $SQL .= ' FROM FILIALEN';
    $SQL .= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
    $SQL .= ' WHERE FIL_ID = :var_N0_fil_id';
    
    // ************************************
    // echo $SQL.'<br>';
    // ************************************
    
    $rsLAN = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
    $rsLANAnz = $awisRSZeilen;
    
    if ($rsLANAnz == 0) {
        echo 'Ung�ltige Filiale!';
        die();
    } else {
        $LAN_CODE = $rsLAN['LAN_CODE'][0];
        $FIL_GRUPPE = $rsLAN['FIL_GRUPPE'][0];
        
        if ($Params[18] != $LAN_CODE) {
            $Params[18] = $LAN_CODE;
        }
    }
} else {
    $FIL_GRUPPE = - 1;
}
$SuchOperatorLoadIndex = ($Params[20] == 'on' ? '>=' : '=');

// Bildschirmbreite f�r die Darstellung
$BildschirmBreite = awis_BenutzerParameter($con, "BildschirmBreite", $AWISBenutzer->BenutzerName());
// Farbcodes f�r KBListe
$FarbCodes = awis_BenutzerParameter($con, "ReifenFarbCodes", $AWISBenutzer->BenutzerName());

// Landesauswahl f�r die Preise
$Bedingung = " AND RPR_LAN_CODE='" . $Params[18] . "'";
// SK: 31.05.2011: Pr�fen, ob der Reifen in dem Land zugelassen ist!
switch ($Params[18]) {
    case 'DE':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 200) = \'J\'';
        break;
    case 'AT':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 201) = \'J\'';
        break;
    case 'CZ':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 202) = \'J\'';
        break;
    case 'NL':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 203) = \'J\'';
        break;
    case 'IT':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 204) = \'J\'';
        break;
    case 'CH':
        $Bedingung .= ' AND (SELECT DISTINCT asi_wert FROM artikelstamminfos WHERE asi_ast_atunr = rei_ast_atunr AND asi_ait_id = 205) = \'J\'';
        break;
}

// Parameter speichern
if ($Params[11] == "1" or $Params[11] == "2") {
    awis_BenutzerParameterSpeichern($con, "ReifenTyp", $AWISBenutzer->BenutzerName(), $Params[11]);
}

if ($Params[0] != '') {
    $Bedingung .= " AND REI_BREITE = " . awis_format($Params[0], "US-Zahl", ",");
}

// *********************************************************
// Querschnitt
// *********************************************************
If ($Params[1] != '') {
    if ($Params[1] == '80') // 80 und leer
{
        $Bedingung .= " AND (REI_Querschnitt IS NULL OR REI_Querschnitt = '80')";
    } else {
        $Bedingung .= " AND REI_Querschnitt LIKE '%" . $Params[1] . "%'";
    }
}
// *********************************************************
// InnenDurchmesser in Zoll
// *********************************************************
If ($Params[2] != "") {
    $Bedingung .= " AND REI_InnenDurchmesser = " . awis_format($Params[2], "US-Zahl", ",");
}

// *********************************************************
// LLKW
// *********************************************************
If ($Params[3] != '') {
    if (strtoupper($Params[3]) == 'C') {
        $Bedingung .= " AND (REI_LLKW = '" . strtoupper($Params[3]) . "' OR REI_LLKW = 'CP')";
    } else {
        $Bedingung .= " AND REI_LLKW = '" . strtoupper($Params[3]) . "'";
    }
}

// *********************************************************
// LoadIndex
// *********************************************************

If ($Params[4] != '' and $Params[5] != '') {
    $Bedingung .= " AND ((to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Params[4] . " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $Params[5] . ")";
    $Bedingung .= " OR (to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Params[5] . " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $Params[4] . "))";
} else {
    If ($Params[4] != '') {
        $Bedingung .= " AND to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Params[4];
    }
    
    If ($Params[5] != '') {
        $Bedingung .= " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $Params[5];
    }
}

// *********************************************************
// SpeedIndex
// *********************************************************

If ($Params[6] != '' and $Params[6] != '#-#') {
    If ($Params[13] != '') {
        $Bedingung .= " AND (REI_SPI_SYMBOL IS NULL OR SPI_ID >= (SELECT SpeedIndex.SPI_ID FROM SpeedIndex WHERE SpeedIndex.SPI_Symbol = '" . strtoupper($Params[6]) . "'))";
    } else {
        $Bedingung .= " AND (SPI_ID = (SELECT SpeedIndex.SPI_ID FROM SpeedIndex WHERE SpeedIndex.SPI_Symbol = '" . strtoupper($Params[6]) . "'))";
    }
}

// *********************************************************
// MaxVK
// *********************************************************

If ($Params[7] != '') {
    $Bedingung .= " AND RPR_AKTSORT <= " . str_replace(',', '.', $Params[7]);
}

// *********************************************************
// ATU-Nummer
// *********************************************************
If ($Params[8] != "") {
    $Bedingung .= " AND REI_AST_ATUNr " . awisLIKEoderIST($Params[8], 1);
}

// *********************************************************
// Rollwiderstand
// *********************************************************
If ($Params[23] != '' and $Params[23] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID, RIN_WERT as ROLLRES FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=10 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[23] . "'))";
}

// *********************************************************
// Nasshaftungsklasse
// *********************************************************
If ($Params[24] != '' and $Params[24] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=11 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[24] . "'))";
}

// *********************************************************
// Rollger�uschklasse
// *********************************************************
If ($Params[25] != '' and $Params[25] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=13 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[25] . "'))";
}

// *********************************************************
// Rollger�usch(dB)
// *********************************************************
If ($Params[26] != '' and $Params[26] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=12 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[26] . "'))";
}

// *********************************************************
// Reifentyp
// *********************************************************
If ($Params[11] != "" and $Params[11] != '0') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=1 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[11] . "'))";
}

// *********************************************************
// Bezeichnung
// *********************************************************
If ($Params[9] != '') {
    $Bedingung .= " AND UPPER(REI_Bezeichnung) " . strtoupper(awisLIKEoderIST("%" . $Params[9] . "%", 1)) . "";
}

// *********************************************************
// Hersteller
// *********************************************************
If ($Params[15] != '') {
    //Mehr Hersteller?
    if(strpos($Params[15],'+')!==false){
        $Herst = explode('+',$Params[15]);
        $Bedingung .= ' AND (';
        $BedingungOr = '';
        foreach ($Herst as $H){
            $BedingungOr .= ' OR UPPER(REI_HERSTELLER) ' . strtoupper(awisLIKEoderIST($H . "%", true)) ;
        }

        $Bedingung .= substr($BedingungOr,3) . ')';

    }else{
        $Bedingung .= " AND UPPER(REI_HERSTELLER) " . strtoupper(awisLIKEoderIST($Params[15] . "%", true)) . "";
    }

}

// *********************************************************
// sonstige Bedingungen
// *********************************************************
If ($Params[12] == 'on') {
    $Bedingung .= " AND (RPR_K <> 'N' OR RPR_K IS NULL) AND RPR_VK > 0 ";
}

// CA 18.09.07 - Globale BEdingung. Ausblenden von Sonderartikel AST_SUCH2.

if (! ($ReifenRechte & 2) == 2) {
    $Bedingung .= " AND EXISTS (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE REI_AST_ATUNR=AST_ATUNR AND AST_SUCH2 IS NULL)";
}

// *********************************************************
// Sortierung
// *********************************************************

If ($Params[10] !== '') {
    $SortFelder = array(
        'AST_ATUNR',
        'VK',
        'AKTSORT',
        'SPI_SYMBOL',
        'HERSTELLER'
    );
    
    If ($Params[12] == 'on') {
        If ($Params[10] != 3 and $SortFelder[$Params[10]] != '') // Alle bis auf Speed-Index
{
            $Bedingung .= " ORDER BY REI_AusLauf DESC, SPI_ID, REI_";
            $Bedingung .= $SortFelder[$Params[10]];
        } else // Bei Speed-Index spezielle Sortierung
{
            $Bedingung .= " ORDER BY REI_AusLauf DESC, SPI_ID, REI_AST_ATUNR";
        }
    } else {
        If ($Params[10] != 3 and $Params[12] != 'on' and $SortFelder[$Params[10]] != '') {
            $Bedingung .= " ORDER BY REI_";
            $Bedingung .= strtoupper($SortFelder[$Params[10]]);
        } else {
            $Bedingung .= " ORDER BY SPI_ID, REI_AST_ATUNR";
        }
    }
}

// **************************************************************************************************
// * Bestandsinfos fuer mehrere Filialen anzeigen
// * -> nur f�r Ausl�nder (Recht) + Franchise-Fil. (FIL_GRUPPE 5) + Zentrale (FIL_GRUPPE -1)
// **************************************************************************************************
if ($ReifenRechte & 4 || $FIL_GRUPPE == 5 || $FIL_GRUPPE == - 1) {
    echo '<form name="frmReifen" method="post" action="./reifen_Main.php?cmdAktion=Reifen">';
    echo 'Vortagesbest&auml;nde anzeigen f&uuml;r folgende Filialen:';
    
    $BestandsFIL_ID = isset($_POST['txtFIL_ID']) ? $_POST['txtFIL_ID'] : awis_BenutzerParameter($con, 'Reifen-Bestandsfilialen', $AWISBenutzer->BenutzerName());
    
    echo '<input title="Tragen Sie hier eine oder mehrere Filialnummern durch Komma getrennt ein." type=input name=txtFIL_ID value="' . $BestandsFIL_ID . '">';
    echo '<Script Language=JavaScript>';
    echo 'document.getElementsByName("txtFIL_ID")[0].focus();';
    echo '</Script>';
    
    echo '</form>';
    echo '<hr>';
} else {
    $BestandsFIL_ID = '';
}

// ********************************************************************
// SQL Abfrage zusammenstellen
// ********************************************************************

print "<b>Angezeigte Preise gelten f�r " . DLookup('LAN_LAND', 'LAENDER', "LAN_CODE='" . $Params[18] . "'", $con) . "</b><br>";
if ($Params[16] != 'on') {
    // Filialindividuellen Preis ber�cksichtigen
    if ($Params[22] != '') {
        $SQL = ' select min(aktion) as MinAktion, max(aktion) as MaxAktion from (';
        $SQL .= ' select rei_ast_atunr, RST_FELGENRIPPE, rpr_aktion, fib_vk, CASE WHEN COALESCE(fib_vk,rpr_vk,0)=0 THEN rpr_aktion ELSE (((100 - nvl(rpr_rabatt,0)) / 100) * nvl(fib_vk,rpr_vk)) END as aktion';
        $SQL .= ' FROM v_ReifenListe';
        $SQL .= ' left join filialbestand on fib_ast_atunr = rei_ast_atunr and fib_fil_id = ' . $Params[22];
        $SQL .= " " . (substr($Bedingung, 0, 4) == " AND" ? "WHERE " . substr($Bedingung, 5) : $Bedingung);
        $SQL .= ")";
    } else {
        $SQL = "SELECT DISTINCT MIN(RPR_AKTION) AS MinAktion, MAX(RPR_AKTION) AS MaxAktion FROM v_ReifenListe";
        $SQL .= " " . (substr($Bedingung, 0, 4) == " AND" ? "WHERE " . substr($Bedingung, 5) : $Bedingung);
    }
    
    awis_Debug(1, $SQL);
    $rsReifen = awisOpenRecordset($con, $SQL);
    print "<b>Aktions-VK" . ($Params[22] == '' ? '' : ' f&uuml;r die Filiale ' . $Params[22]) . " von " . awis_format($rsReifen["MINAKTION"][0], "Currency") . " bis " . awis_format($rsReifen["MAXAKTION"][0], "Currency") . "</b><br>";
    
    print "Folgende Lieferanten f�hren die gesuchte Gr��e:<br>";
    
    $SQL = "SELECT DISTINCT REI_ID, REI_AST_ATUNR, REI_HERSTELLER, REI_BREITE, REI_QUERSCHNITT, REI_BAUART, REI_INNENDURCHMESSER, REI_LLKW, REI_LOADINDEX, ";
    $SQL .= " REI_LOADINDEX2, REI_SPI_SYMBOL, REI_RF, REI_BEZEICHNUNG, RPR_K, RPR_VK, RPR_AKTION, RPR_AKTIONSKENN, RPR_AKTIONTEXT, RST_FELGENRIPPE, ";
    $SQL .= " RPR_AKTSORT, RPR_RABATT, RPR_KBPREIS, REI_BESTELLKZ, SPDIDX, REI_AUSLAUF, SPI_SYMBOL, SPI_ID, REI_ZUSATZBEMERKUNG, REI_VK ";
    $SQL .= " ,REI_AKTSORT, REI_K, REI_LOADINDEX3, REI_SPI_SYMBOL2, REI_ZUSBEM2, REI_ZUSBEM3,  ";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin10 WHERE rin10.rin_rei_id = aa.rei_id AND rin10.rin_rnt_id=10) AS ROLLRES,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin11 WHERE rin11.rin_rei_id = aa.rei_id AND rin11.rin_rnt_id=11) AS WETGRIP,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin12 WHERE rin12.rin_rei_id = aa.rei_id AND rin12.rin_rnt_id=12) AS NOISEPE,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin13 WHERE rin13.rin_rei_id = aa.rei_id AND rin13.rin_rnt_id=13) AS NOISECL,";
    $SQL .= " (SELECT VKM_ZIEL FROM VKPRIOMAPPING WHERE VKM_QUELLE=(SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR=aa.REI_AST_ATUNR AND ASI_AIT_ID=270) AND VKM_GUELTIG=1) AS VKPRIO,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin14 WHERE rin14.rin_rei_id = aa.rei_id AND rin14.rin_rnt_id=14) AS WINTERTAUGLICHKEIT,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin30 WHERE rin30.rin_rei_id = aa.rei_id AND rin30.rin_rnt_id=30) AS RIN_INFO";
    $SQL .= " FROM v_ReifenListe aa";
    $SQL .= " " . (substr($Bedingung, 0, 4) == " AND" ? "WHERE " . substr($Bedingung, 5) : $Bedingung);
    awis_Debug(1, $SQL);
    
    $rsReifen = awisOpenRecordset($con, $SQL);
    $rsReifenAnz = $awisRSZeilen;
    
    $Hersteller = array();
    for ($i = 0; $i < $rsReifenAnz; $i ++) {
        if ($rsReifen["REI_HERSTELLER"][$i] == "RUNDERNEUERT") {
            $rsReifen["REI_HERSTELLER"][$i] = "<i>" . $rsReifen["REI_HERSTELLER"][$i] . "</i>";
        }
        
        if (in_array($rsReifen["REI_HERSTELLER"][$i], $Hersteller) == FALSE) {
            array_push($Hersteller, $rsReifen["REI_HERSTELLER"][$i]);
        }
    }
    array_multisort($Hersteller);
    print "<b>" . implode(", ", $Hersteller) . "</b><br>";
} else // Daten f�r die KB Liste
{
    $SQL = "SELECT DISTINCT DECODE(SUBSTR(REI_AST_ATUNR,3),'0000',TO_CHAR(REI_ID),REI_AST_ATUNR) AS REISONDERID, REI_AST_ATUNR, REI_HERSTELLER, REI_BREITE, REI_QUERSCHNITT, REI_BAUART, REI_INNENDURCHMESSER, REI_LLKW, REI_LOADINDEX, ";
    $SQL .= "REI_LOADINDEX2, REI_SPI_SYMBOL, REI_RF, REI_BEZEICHNUNG, RPR_K, RPR_VK, RPR_AKTION, RPR_AKTIONSKENN, RPR_AKTIONTEXT, ";
    $SQL .= "RPR_RABATT, RPR_KBPREIS, REI_BESTELLKZ, SPDIDX, REI_AUSLAUF, SPI_SYMBOL, SPI_ID, REI_ZUSATZBEMERKUNG, RPR_AKTSORT, REI_VK, ";
    $SQL .= "REI_LOADINDEX3, REI_SPI_SYMBOL2, REI_ZUSBEM2, REI_ZUSBEM3, REI_K, REI_AKTSORT, ";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin10 WHERE rin10.rin_rei_id = aa.rei_id AND rin10.rin_rnt_id=10) AS ROLLRES,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin11 WHERE rin11.rin_rei_id = aa.rei_id AND rin11.rin_rnt_id=11) AS WETGRIP,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin12 WHERE rin12.rin_rei_id = aa.rei_id AND rin12.rin_rnt_id=12) AS NOISEPE,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin13 WHERE rin13.rin_rei_id = aa.rei_id AND rin13.rin_rnt_id=13) AS NOISECL,";
    $SQL .= " (SELECT VKM_ZIEL FROM VKPRIOMAPPING WHERE VKM_QUELLE=(SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR=aa.REI_AST_ATUNR AND ASI_AIT_ID=270) AND VKM_GUELTIG=1) AS VKPRIO,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin14 WHERE rin14.rin_rei_id = aa.rei_id AND rin14.rin_rnt_id=14) AS WINTERTAUGLICHKEIT,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin30 WHERE rin30.rin_rei_id = aa.rei_id AND rin30.rin_rnt_id=30) AS RIN_INFO";
    $SQL .= " FROM v_ReifenListeKB aa";
    $SQL .= " " . (substr($Bedingung, 0, 4) == " AND" ? "WHERE " . substr($Bedingung, 5) : $Bedingung);
    
    $rsReifen = awisOpenRecordset($con, $SQL);
    $rsReifenAnz = $awisRSZeilen;
}
// *****************************************
// Spaltenbreiten festlegen
// *****************************************

$SpBreiteFaktor = $BildschirmBreite / 1024;
$SpBreiteTabelle = (1024 * $SpBreiteFaktor) - 40;
$SpBreiteArtnr = 80 * $SpBreiteFaktor;
$SpBreiteHerst = 80 * $SpBreiteFaktor;
$SpBreiteRabatt = 60;
$SpBreiteBez = 165;

$SpBreiteRabatt = 60 * $SpBreiteFaktor;
$SpBreiteBez = 165 * $SpBreiteFaktor;

$BestandZusatz = false;
if(($ReifenRechte&16384)!=0 ){
    if($BestandsFIL_ID >0) {
        $NBFil = $BestandsFIL_ID;
        if (strpos($NBFil, ',') !== false) {
            $NBFil = explode(',', $NBFil)[0];
        }
        //Nachbarfilialen ermitteln
        $SQL = 'SELECT DATEN.* ';
        $SQL .= ' FROM(';
        $SQL .= 'SELECT CASE WHEN FEG_FIL_ID_VON = 0' . $NBFil . ' THEN FEG_FIL_ID_NACH';
        $SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
        $SQL .= ',FIL_BEZ,FIL_STRASSE,FIL_PLZ,FIL_ORT,FIL_LAGERKZ';
        $SQL .= ' FROM FILIALENENTFERNUNGEN';
        $SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = ' . $NBFil . ' THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID AND FIL_ID < 900';
        $SQL .= ' WHERE FEG_FIL_ID_VON = 0' . $NBFil;
        $SQL .= ' OR FEG_FIL_ID_NACH = 0' . $NBFil;
        $SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
        $SQL .= ') DATEN ';
        $SQL .= ' WHERE FEG_ENTFERNUNG <= 50';
        $SQL .= ' AND (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND FIF_IMQ_ID = 32 AND ROWNUM = 1) IS NOT NULL';

        $rsFilialenNB = awisOpenRecordset($con, $SQL);
        $rsFilialenZeilenNB = $awisRSZeilen;
        $Nachbarn = '0';
        $Entfernung = array();
        for ($Zeile = 0; $Zeile < $rsFilialenZeilenNB; $Zeile++) {
            $Nachbarn .= ',' . $rsFilialenNB['FEG_FIL_ID'][$Zeile];
            $Entfernung[$rsFilialenNB['FEG_FIL_ID'][$Zeile]] = $rsFilialenNB['FEG_ENTFERNUNG'][$Zeile];
        }
        $BestandZusatz = true;
    }
}

If ($rsReifenAnz > 0) // Liste gefunden
{
    print "<table width=" . $SpBreiteTabelle . " cellspacing=0 cellpadding=0 border=1 >";
    print "<tr><td id=FeldBez width=30></td>";
    print "<td id=FeldBez width=" . $SpBreiteArtnr . ">ATUNr</td>";
    
    print "<td id=FeldBez colspan=9 width=360>Reifenkennzeichen</td>";
    
    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=32 height=32 src=/bilder/Kraftstoff_32px.png title='Kraftstoffeffizienz'></td>";
    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=32 height=32 src=/bilder/Nass_32px.png title='Nasshaftungsklasse'></td>";
    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=32 height=32 src=/bilder/Roll_3_32px.png title='Rollger�uschklasse'></td>";
    print "<td id=FeldBez width=20 align=center title='Rollger�usch in dB'>dB</td>";
    
    print "<td id=FeldBez width=" . $SpBreiteBez . ">Bezeichnung</td>";
    print "<td id=FeldBez width=20 title='Kennung'>K</td>";
    
    if (($ReifenRechte & 1024) == 1024) {
        print "<td id=FeldBez width=20 title='Auslaufartikel-Kennzeichen'>K2</td>";
    }
    print "<td id=FeldBez width=50 align=right>VK&nbsp;";
    echo "<img width=20 height=15 src=/bilder/Flagge_" . $Params[18] . ".gif title='Preise f�r " . $Params[18] . "'></td>";
    print "<td id=FeldBezBold align=center width=" . $SpBreiteRabatt . " title='Rabatt in %'>%</td>";
    print "<td id=FeldBez width=50>Aktion</td>";
    print "<td id=FeldBez width=50>FR</td>";
    print "<td id=FeldBez width=32 align=center>";
    echo "<img width=32 height=32 src=/bilder/DreizackigerBerg.png title='Schneeflockensymbol f�r garantierte Wintertauglichkeit'></td>";
    print "<td id=FeldBez width=200 align=center>INFO</td>";

    if($BestandZusatz){
        print "<td id=FeldBez width=50>Bestand</td>";
    }

    if(($ReifenRechte&16384) == 16384){
        print "<td id=FeldBez width=50>Weiden</td>";
        print "<td id=FeldBez width=50>Werl</td>";
    }

    print "</tr>";
    
    $LetzterSpeedIndex = "";
    $AuslaufBlock = False;
    
    $ReifenAnz = $rsReifenAnz;
    
    for ($i = 0; $i < $rsReifenAnz; $i ++) {
        
        if ($Params[12] == 'on') // Nur f�r CallCenter Blockbildung
{
            if ($rsReifen["REI_AUSLAUF"][$i] == 'A' and ! $AuslaufBlock) {
                print "<tr><td id=UeberschriftKlein colspan=18 align=center>Auslaufartikel</td></tr>";
                $AuslaufBlock = True;
            }
            
            // Anzeige f�r Speedindex
            if ($LetzterSpeedIndex != $rsReifen["SPI_ID"][$i]) {
                print "<tr><td id=UeberschriftKlein colspan=18 align=center>Speedindex: " . $rsReifen["SPDIDX"][$i] . "</td></tr>";
                $LetzterSpeedIndex = $rsReifen["SPI_ID"][$i];
            }
        }
        
        print "<tr>";
        echo '<td ' . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . '>' . $rsReifen["VKPRIO"][$i] . '</td>';
        
        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
        
        $rsSuch2 = awisOpenRecordset($con, "SELECT AST_SUCH2 FROM ArtikelStamm WHERE AST_ATUNR=:var_T_ast_atunr", true, false, $BindeVariablen);
        
        if ($rsReifen["REI_HERSTELLER"][$i] == "<i>RUNDERNEUERT</i>") {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " ><b>";
            // Wenn der Benutzer das Recht nicht hat, Sonderartikel zu sehen, kein Link auf Artikelseite, da
            // diese Artikel f�r diese Benutzer nicht sichtbar sind (TR 22.01.08)
            If (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                echo $rsReifen["REI_AST_ATUNR"][$i];
            } else {
                if ($RechteAST > 0) {
                    echo '<a href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                } else {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                }
            }
            echo "</b></td>";
        } else {
            $Code = ""; // Wird auch beim Preis abgefragt
            
            if (strchr($FarbCodes, "~" . $rsReifen["REI_BESTELLKZ"][$i])) {
                $Code = strstr(strstr($FarbCodes, $rsReifen["REI_BESTELLKZ"][$i]), ":");
                $Code = substr($Code, strchr($Code, ":") + 1, 6);
                print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " ><font color=#$Code>";
                
                If (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                } else {
                    if ($RechteAST >= 0) {
                        echo '<a href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '&Zurueck=/reifen/reifen_Main.php?cmdAktion=Reifen>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                    } else {
                        echo $rsReifen["REI_AST_ATUNR"][$i];
                    }
                }
                echo '</font></td>';
            } else {
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss");
                
                // ******************
                // Bestaende zeigen
                // ******************
                
                $FilialenListe = array();
                $ToolTipp = '';
                
                $ToolTipp = 'Best&auml;nde in ';
                
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 10;
                
                $SQL = 'SELECT DISTINCT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];
                
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 13;
                
                $SQL = 'SELECT DISTINCT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }

                $ToolTipp .= 'Weiden: ' . $Bestand;
                $BestandWeiden = $Bestand;
                
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 11;
                
                $SQL = 'SELECT DISTINCT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];
                
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 14;
                $SQL = 'SELECT DISTINCT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }
                $ToolTipp .= ' / Werl: ' . $Bestand;
                $BestandWerl = $Bestand;
                
                if ($BestandsFIL_ID != '') {
                    $BestandsFIL_ID = str_replace(';', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('/', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('.', ',', $BestandsFIL_ID);
                    $FilListe = explode(',', $BestandsFIL_ID);
                    foreach ($FilListe as $FilID) {
                        if ($FilID != 0) {
                            $FilialenListe[] = (int) ($FilID);
                        }
                    }
                    if (! empty($FilialenListe)) {
                        $AnzFilListe = count($FilialenListe);

                        $BindeVariablen = array();
                        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                        
                        $SQL = 'SELECT COALESCE(SUM(FIB_BESTAND),0) as Bestand, FIL_ID';
                        $SQL .= " FROM FILIALEN ";
                        $SQL .= " LEFT OUTER JOIN FilialBestand ON FIL_ID = FIB_FIL_ID AND FIB_AST_ATUNR = :var_T_ast_atunr";
                        $SQL .= " WHERE ";
                        if ($AnzFilListe === 1) {
                            $BindeVariablen['var_N0_fil_id'] = implode(',', $FilialenListe);
                            $SQL .= " FIL_ID = :var_N0_fil_id";
                        } else {
                            $SQL .= " FIL_ID IN (" . implode(',', $FilialenListe) . ')';
                        }
                        $SQL .= " GROUP BY FIL_ID ";
                        $SQL .= " ORDER BY FIL_ID ";

                        $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        $rsBestaendeAnz = $awisRSZeilen;
                        if ($rsBestaendeAnz > 0) {
                            $ToolTipp .= ', Vortagsbest&auml;nde in ';
                        }
                        $FilialenToolTipp = '';
                        $Bestande = array();
                        for ($Bestand = 0; $Bestand < $rsBestaendeAnz; $Bestand ++) {
                            $FilialenToolTipp .= ' / Filiale ' . $rsBestaende['FIL_ID'][$Bestand] . ': ' .$rsBestaende['BESTAND'][$Bestand];
                            $Bestande[] = array('FIL'=>$rsBestaende['FIL_ID'][$Bestand], 'BESTAND' => $rsBestaende['BESTAND'][$Bestand]);
                        }
                        $ToolTipp .= substr($FilialenToolTipp, 3);
                    }
                }
                echo ' title="' . $ToolTipp . '"';
                echo " ><b>";
                awis_BenutzerParameterSpeichern($con, 'Reifen-Bestandsfilialen', $AWISBenutzer->BenutzerName(), implode(',', $FilialenListe));
                
                If (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                } else {
                    if ($RechteAST > 0) {
                        echo '<a target="__new" href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '&Zurueck=/reifen/reifen_Main.php?cmdAktion=Reifen>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                    } else {
                        echo $rsReifen["REI_AST_ATUNR"][$i];
                    }
                }
                echo "</b></td>";
            }
        }
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=left width=33>" . $rsReifen["REI_BREITE"][$i] . "</td>";
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=left width=20>&nbsp;" . ($rsReifen["REI_QUERSCHNITT"][$i] != "" ? $rsReifen["REI_QUERSCHNITT"][$i] : "&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_BAUART"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=20>&nbsp;" . $rsReifen["REI_INNENDURCHMESSER"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=15>&nbsp;" . $rsReifen["REI_LLKW"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_LOADINDEX"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_LOADINDEX2"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=25>&nbsp;" . $rsReifen["REI_SPI_SYMBOL"][$i] . "</td>";
        
        // Eingef�gt TR 30.04.2007
        if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '' or $rsReifen["REI_ZUSBEM3"][$i] != '' or $rsReifen["REI_ZUSATZBEMERKUNG"][$i] != '') {
            if ($rsReifen["REI_LOADINDEX3"][$i] != '') {
                print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_LOADINDEX3"][$i];
            }
            
            if ($rsReifen["REI_SPI_SYMBOL2"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '') {
                    print "/" . $rsReifen["REI_SPI_SYMBOL2"][$i];
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_SPI_SYMBOL2"][$i];
                }
            }
            
            if ($rsReifen["REI_RF"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '') {
                    print "/<b>" . $rsReifen["REI_RF"][$i] . "</b>";
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsReifen["REI_RF"][$i] . "</b>";
                }
            }
            
            if ($rsReifen["REI_ZUSBEM2"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '') {
                    print "/<b>" . $rsReifen["REI_ZUSBEM2"][$i] . "</b>";
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsReifen["REI_ZUSBEM2"][$i] . "</b>";
                }
            }
            
            if ($rsReifen["REI_ZUSBEM3"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '') {
                    print "/" . $rsReifen["REI_ZUSBEM3"][$i];
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_ZUSBEM3"][$i];
                }
            }
            
            if ($rsReifen["REI_ZUSATZBEMERKUNG"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '' or $rsReifen["REI_ZUSBEM3"][$i] != '') {
                    print "/" . $rsReifen["REI_ZUSATZBEMERKUNG"][$i];
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_ZUSATZBEMERKUNG"][$i];
                }
            }
            print "</td>";
        } 

        else {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;</td>";
        }
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["ROLLRES"][$i] != "" ? $rsReifen["ROLLRES"][$i] : "&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["WETGRIP"][$i] != "" ? $rsReifen["WETGRIP"][$i] : "&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["NOISECL"][$i] != "" ? $rsReifen["NOISECL"][$i] : "&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["NOISEPE"][$i] != "" ? $rsReifen["NOISEPE"][$i] : "&nbsp;") . "</td>";
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsReifen["REI_BEZEICHNUNG"][$i] . "</td>";
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " ><b>" . (isset($rsReifen["REI_K"][$i]) ? $rsReifen["REI_K"][$i] : '') . "</b></td>";
        
        // Zus�tzliches Kennzeichen aus dem Artikelstamm
        // Artikelstamminfos, Wert 70
        if (($ReifenRechte & 1024) == 1024) {
            
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
            $BindeVariablen['var_N0_asi_ait_id'] = 70;
            
            $rsKenn2 = awisOpenRecordset($con, "SELECT DISTINCT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=:var_N0_asi_ait_id AND ASI_AST_ATUNR=:var_T_ast_atunr", true, false, $BindeVariablen);
            if ($rsKenn2['ASI_WERT'][0] == 'A') {
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsKenn2['ASI_WERT'][0] . "</td>";
            } else {
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >&nbsp;</td>";
            }
        }
        
        $VK = $rsReifen["RPR_VK"][$i];
        
        if ($Params[22] != "") {
            // VK aus Filialbestand-Tabelle hernehmen??
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
            $BindeVariablen['var_N0_fil_id'] = $Params[22];
            
            $rsVKFil = awisOpenRecordset($con, "SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id", true, false, $BindeVariablen);
            $rsVKAnz = $awisRSZeilen;
            
            if ($rsVKAnz > 0) {
                $VK = $rsVKFil["FIB_VK"][0];
            }
        }
        
        if ($Code != "") {
            if ($rsSuch2['AST_SUCH2'][0] != '') {
                if ($rsReifen["RPR_AKTION"][$i] != '0') {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>" . ($VK > 0 ? awis_format($VK, 'Standardzahl,2') . ' ' : '') . "i.V." . "</font></b></td>";
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><font color=#$Code></font></td>";
                }
            } else {
                print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><font color=#$Code>" . awis_format($VK, 'Standardzahl,2') . "</font></td>";
            }
        } else {
            if ($rsSuch2['AST_SUCH2'][0] != '') {
                if ($rsReifen["RPR_AKTION"][$i] != '0') {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>" . ($VK > 0 ? awis_format($VK, 'Standardzahl,2') . ' ' : '') . "i.V." . "</font></b></td>";
                } else {
                    print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right></td>";
                }
            } else {
                print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right>" . awis_format($VK, 'Standardzahl,2') . "</td>";
            }
        }
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><center><b><font color=#FF0000>" . ($rsReifen["RPR_RABATT"][$i] == 0 ? '' : awis_format($rsReifen["RPR_RABATT"][$i], 'Standardzahl,1')) . "</font></b></center></td>";
        
        // SK; 10.09.2009
        // Problemartikel (Reifen ohne Kennung S) anzeigen
        
        require_once ('awisBenutzer.inc');
        $XX = awisBenutzer::Init();
        $FilZugriff = $XX->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
        
        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
        $BindeVariablen['var_N0_pri_ity_key'] = 16;
        $BindeVariablen['var_N0_pra_kategorie'] = 1;
        
        $SQL = 'SELECT PRI_WERT AS ZERTIFIKAT';
        $SQL .= ' FROM PROBLEMARTIKEL';
        $SQL .= ' INNER JOIN PROBLEMARTIKELINFOS ON PRI_PRA_KEY = PRA_KEY AND PRI_ITY_KEY = :var_N0_pri_ity_key';
        $SQL .= ' WHERE PRA_AST_ATUNR=:var_T_ast_atunr';
        $SQL .= ' AND PRA_KATEGORIE =:var_N0_pra_kategorie'; // NUR REIFEN BIS <= 185
        
        $ProblemReifenKZ = '';
        $Link = '';
        $rsPAR = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
        
        if ($awisRSZeilen > 0) {
            $ProblemReifenKZ = '!!!';
            $Link = '';
            if ($rsPAR["ZERTIFIKAT"][0] != '') {
                $ProblemReifenKZ = 'Z!';
                $Link = '/dokumentanzeigen.php?bereich=reifenzertifikate&dateiname=' . $rsPAR["ZERTIFIKAT"][0] . '&erweiterung=pdf';
            }
        }
        
        if (isset($rsReifen["RPR_AKTIONTEXT"][$i]) and $rsReifen["RPR_AKTIONTEXT"][$i] != '') {
            echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . $rsReifen["RPR_AKTIONTEXT"][$i] . "</font></b></td>";
        } elseif ($rsSuch2['AST_SUCH2'][0] != '') {
            if ($rsReifen["RPR_VK"][$i] == '0') {
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>";
            } elseif ($rsReifen["RPR_AKTION"][$i] != '0') {
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . ($rsReifen["RPR_AKTION"][$i] == 0 ? '' : awis_format($rsReifen["RPR_AKTION"][$i], 'Standardzahl,2')) . "</font></b></td>";
            } else {
                echo '<td ' . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . '></td>';
            }
        } else {
            
            $Aktion = 0;
            // Spalte Aktion berechnen
            if ($rsReifen["RPR_RABATT"][$i] != '0') {
                $Aktion = ((100 - awis_format($rsReifen["RPR_RABATT"][$i], 'Standardzahl,2')) / 100);
                $Aktion = str_replace(',', '.', $Aktion) * str_replace(',', '.', $VK);
                
                echo "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . ($Aktion == 0 ? '' : awis_format($Aktion, 'Standardzahl,2')) . "</font></b></td>";
            } else {
                echo '<td ' . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . '></td>';
            }
        }
        
        print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsReifen["RST_FELGENRIPPE"][$i] . "</td>";

        if($rsReifen['WINTERTAUGLICHKEIT'][$i] !==  NULL and $rsReifen['WINTERTAUGLICHKEIT'][$i] == 'X'){
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . 'X' . "</td>";
        } else {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . '' . "</td>";
        }

        if($rsReifen['RIN_INFO'][$i] !==  NULL and $rsReifen['RIN_INFO'][$i] != ""){
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsReifen['RIN_INFO'][$i] . "</td>";
        } else {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . '' . "</td>";
        }

        if($BestandZusatz){
            $TT = '';

            //Wurden im oberen Feld mehrere Filialen eingegeben, den Rest als Tooltip anzeigen
            if(count($Bestande)>1){
                for($ii=1;$ii<count($Bestande);$ii++){
                    $TT .= 'Fil: ' . $Bestande[$ii]['FIL'] . ' = ' . $Bestande[$ii]['BESTAND'] . PHP_EOL;
                }
            }

            //Nachbarn ermitteln
            if(isset($Nachbarn)){
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];

                $SQL = 'SELECT COALESCE(SUM(FIB_BESTAND),0) as Bestand, FIL_ID';
                $SQL .= " FROM FILIALEN ";
                $SQL .= " LEFT OUTER JOIN FilialBestand ON FIL_ID = FIB_FIL_ID AND FIB_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= " WHERE ";
                $SQL .= " FIL_ID IN (" . $Nachbarn . ')';
                $SQL .= " GROUP BY FIL_ID ";
                $SQL .= " ORDER BY FIL_ID ";

                $rsBestaendeNB = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $TT .= '++Nachbarfilialen++'.PHP_EOL;
                $Color = 0;
                for ($ii = 0; $ii < $rsBestaendeAnz; $ii ++) {
                    $TT .= 'Fil: ' . $rsBestaendeNB['FIL_ID'][$ii].'('.$Entfernung[$rsBestaendeNB['FIL_ID'][$ii]].'km)'. ' = ' . $rsBestaendeNB['BESTAND'][$ii] . PHP_EOL;
                    if( $rsBestaendeNB['BESTAND'][$ii]>0){
                        $Color++;
                    }
                }

            }

            $Bestande[0]['BESTAND']>=1?$Color=-1:'';
            if($Color >= 1) {
                $Color = 'yellow';
            }elseif($Color == -1) {
                $Color = 'green';
            }else {
                $Color = '';
            }

            print "<td style='background-color: ".$Color."' title='".$TT."'" . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $Bestande[0]['BESTAND'] . "</td>";

        }

        if(($ReifenRechte&16384) == 16384){
            $Color = ($BestandWeiden>0?'green':'');
            print "<td style='background-color: ".$Color."' title='"."'" . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $BestandWeiden . "</td>";
            $Color = ($BestandWerl>0?'green':'');
            print "<td style='background-color: ".$Color."' title='"."'" . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $BestandWerl . "</td>";
        }

        echo "</tr>";
    }
    print "</table>";
    $ZeitVerbrauch = time() - $ZeitVerbrauch;
    print "<br><font size=1>Es wurden $ReifenAnz Reifen in $ZeitVerbrauch Sekunden gefunden. ";
} else // Nur eine Filiale gefunden, Daten anzeigen
{
    print "<span class=HinweisText>Es wurden keine passenden Reifen f�r die Auswahl gefunden.</span>";
}

// ******************************************************************************
function ReifenTypen($REIID, $con, $SonderID)
{
    global $awisRSZeilen;
    
    if ($SonderID != $REIID and $SonderID != '') // Sondernummer XX0000
{
        $BindeVariablenFunc = array();
        $BindeVariablenFunc['var_N0_rei_id'] = $SonderID;
        $BindeVariablenFunc['var_N0_rin_rnt_id'] = 1;
        
        $rsReifenTypen = awisOpenRecordset($con, "SELECT DISTINCT RIN_WERT FROM ReifenInfos, Reifen WHERE REI_ID = RIN_REI_ID AND REI_ID=:var_N0_rei_id AND RIN_RNT_ID=:var_N0_rin_rnt_id ORDER BY RIN_WERT", true, false, $BindeVariablenFunc);
    } else {
        $BindeVariablenFunc = array();
        $BindeVariablenFunc['var_T_ast_atunr'] = $REIID;
        $BindeVariablenFunc['var_N0_rin_rnt_id'] = 1;
        
        $rsReifenTypen = awisOpenRecordset($con, "SELECT DISTINCT RIN_WERT FROM ReifenInfos, Reifen WHERE REI_ID = RIN_REI_ID AND REI_AST_ATUNR=:var_T_ast_atunr AND RIN_RNT_ID=:var_N0_rin_rnt_id ORDER BY RIN_WERT", true, false, $BindeVariablenFunc);
    }
    
    for ($rt = 0; $rt < $awisRSZeilen; $rt ++) {
        if ($rsReifenTypen["RIN_WERT"][$rt] != '') {
            $erg .= $rsReifenTypen["RIN_WERT"][$rt];
        }
    }
    
    return $erg;
}
?>