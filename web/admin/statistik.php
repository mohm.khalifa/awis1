<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<link rel=stylesheet type=text/css href=ATU.css>
</head>

<body>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

include ("ATU_Header.php");	// Kopfzeile
$con = awisLogon();

	// Beginn Hauptmen�
print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Apache Webalizer-Statistiken</h1></td><td align=right>Anmeldename:".$AWISBenutzer->BenutzerName()."</td></tr></table>";


					// �berschrift der Tabelle
print "<table border=0 width=100%><tr><th align=left id=FeldBez>Statistik</th><th align=left id=FeldBez>Beschreibung</th></tr>";

$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);
awislogoff($con);


	if(($RechteStufe&1)==1)
	{
		print "<tr><td><a href=/logs/statistik/index.html>Zugriffsstatistik alt</a></td><td>Apachestatistiken (alte Version).</td></tr>";
		print "<tr><td><a href=/logs/statistik_gesamt/index.html>Zugriffsstatistik gesamt</a></td><td>Apachestatistiken: alle Server alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
		print "<tr><td><a href=/logs/statistik_reduziert/index.html>Zugriffsstatistik bereinigt</a></td><td>Apachestatistiken: alle Server ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";

		$Level = awisLeseAWISParameter('awis.conf','AWIS_LEVEL');
	
		switch($Level)
		{
			case 'PROD':
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx003_gesamt/index.html>svweblx003 gesamt</a></td><td>Apachestatistiken: Server svweblx003 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx003_reduziert/index.html>svweblx003 bereinigt</a></td><td>Apachestatistiken: Server svweblx003 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";
		
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx004_gesamt/index.html>svweblx004 gesamt</a></td><td>Apachestatistiken: Server svweblx004 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx004_reduziert/index.html>svweblx004 bereinigt</a></td><td>Apachestatistiken: Server svweblx004 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";
		
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx005_gesamt/index.html>svweblx005 gesamt</a></td><td>Apachestatistiken: Server svweblx005 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx005_reduziert/index.html>svweblx005 bereinigt</a></td><td>Apachestatistiken: Server svweblx005 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";
		
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx006_gesamt/index.html>svweblx006 gesamt</a></td><td>Apachestatistiken: Server svweblx006 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx006_reduziert/index.html>svweblx006 bereinigt</a></td><td>Apachestatistiken: Server svweblx006 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";

				break;
			case 'STAG':
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx010_gesamt/index.html>svweblx010 gesamt</a></td><td>Apachestatistiken: Server svweblx010 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx010_reduziert/index.html>svweblx010 bereinigt</a></td><td>Apachestatistiken: Server svweblx010 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";
		
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx020_gesamt/index.html>svweblx020 gesamt</a></td><td>Apachestatistiken: Server svweblx020 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx020_reduziert/index.html>svweblx020 bereinigt</a></td><td>Apachestatistiken: Server svweblx020 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";

				break;
			case 'ENTW':
				print "<tr><td colspan=2>&nbsp;</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx009_gesamt/index.html>svweblx009 gesamt</a></td><td>Apachestatistiken: Server svweblx009 alle Hits (zur Einsch&auml;tzung des Datenverkehrs).</td></tr>";
				print "<tr><td><a href=/logs/statistik_svweblx009_reduziert/index.html>svweblx009 bereinigt</a></td><td>Apachestatistiken: Server svweblx009 ohne Bilder, ....(zur Einsch&auml;tzung der Useraktivit&auml;ten)</td></tr>";

				break;
			default:				
				break;
		}
		
	}
	

print "</table>";	//Ende Hauptmen�
print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='../index.php';>";

if(($RechteStufe&1)==1)
{
 include("debug_info.php");
 //print "<br><a href=/phpinfo.php>php info</a>";
 //print "<br><a href=/stats/index.html>Zugriffsstatistik</a>";
}

?>

</body>
</html>

