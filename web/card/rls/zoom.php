<?php
require_once("card/card_allgparam.inc.php");	// allg. Parameter
?>
<html>
<head>
	<title>Zoom</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>
	<script language="JavaScript">
	
	//Wird bei onload aufgerufen
	function init()
	{
		document.getElementById("textanzeige").value = unescape("<?php echo str_replace(chr(13).chr(10),"\\n",$strFeldinhalt); ?>");
		
		if(<?php echo $intLesen; ?>==1)	 //Readonly setzen, falls Ausgangsfeld readonly oder disabled gesetzt ist
		{
			document.getElementById("textanzeige").setAttribute("readOnly",true);
		}	
	}
	
	//Wird bei onkeyup aufgerufen
	function laengepruef()
	{
		//alert(window.event.keyCode);
		var feldlaenge = <?php echo $intFeldlaenge; ?>;
		
		var obj = document.getElementById("textanzeige");
		
		if(obj.value.length>feldlaenge)		//Wenn aktueller Text gr��er als die Feldl�nge ist
		{
			obj.value=obj.value.substr(0,feldlaenge);
		}
	}
	
	//Wird beim schliessen des Fensters ausgel�st
	
	function fensterzu()
	{
		window.returnValue=document.getElementById("textanzeige").value.substr(0,<?php echo $intFeldlaenge; ?>);
		self.close();
	}
	</script>
	
</head>
<body onload="init();" onunload="fensterzu();">
<table cellpadding="0" cellspacing="0" align="center" width="100%" height="100%">
<tr><td>
	<table cellpadding="0" cellspacing="0" align="center">
	<tr><td>
		<textarea id="textanzeige" onkeyup="laengepruef();"
		oncontextmenu="return false;"
		onkeydown="if((document.getElementById('textanzeige').value.length>=<?php echo $intFeldlaenge; ?>&&window.event.keyCode!=8&&window.event.keyCode!=46)||window.event.keyCode==13){return false;}"></textarea>
		<?php //window.event.keyCode: 8 = R�cktaste, 46 = Entf-Taste, 13 = Enter-Taste, Tastendruck dieser Tasten wird nicht ausgef�hrt?>
	</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td align="center"><input type="image" src="/bilder/zurueck.png" alt="Fenster schlie&szlig;en" class="quadrat" onclick="fensterzu();"></td></tr>
	</table>
</td></tr>
</table>
</body>
</html>
