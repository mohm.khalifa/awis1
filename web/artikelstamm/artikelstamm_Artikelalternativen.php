<?php
/**
 * Zukauf
 *
 * Wird nicht in den Filialen angezeigt => keine Bildschirmbreitenanpassung
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200901
 *
 */
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
require_once 'artikelstamm_Artikelalternativen_Funktionen.inc';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('AAL', '%');
$TextKonserven[] = array('AST', 'AST_ATUNR');
$TextKonserven[] = array('AST', 'AST_BEZEICHNUNGWW');
$TextKonserven[] = array('AST', 'AST_BEZEICHNUNG');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'Altteilwert');
$TextKonserven[] = array('Wort', 'AktuellesSortiment');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
$TextKonserven[] = array('Wort', 'ZusammenfassungBestellungen');
$TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht462 = $AWISBenutzer->HatDasRecht(462);            // Recht f�r die Lieferantenartikel
    if ($Recht462 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (isset($_GET['AAL_KEY']) and !isset($_GET['Del'])) {
        $AWIS_KEY2 = $_GET['AAL_KEY'];
    } elseif (isset($_POST['txtAAL_KEY'])) {
        $AWIS_KEY2 = $_POST['txtAAL_KEY'];
    } else {
        $AWIS_KEY2 = '';
    }

    if (isset($_GET['SSort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SSort']);
    } else {
        $ORDERBY = ' ORDER BY AAL_ALTERNATIV_AST_ATUNR';
    }

    $SQL = 'SELECT AAL_KEY ,';
    $SQL .= '   AAL_AST_ATUNR ,';
    $SQL .= '   AST_BEZEICHNUNG ,';
    $SQL .= '   AST_BEZEICHNUNGWW ,';
    $SQL .= '   AAL_ALTERNATIV_AST_ATUNR ,';
    $SQL .= '   AAL_USER ,';
    $SQL .= '   AAL_USERDAT';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM ARTIKELALTERNATIVEN';
    $SQL .= ' LEFT JOIN ARTIKELSTAMM';
    $SQL .= ' ON ARTIKELALTERNATIVEN.AAL_ALTERNATIV_AST_ATUNR = ARTIKELSTAMM.AST_ATUNR';

    if ($AWIS_KEY2 != '') {
        $SQL .= ' WHERE AAL_KEY = ' . $DB->WertSetzen('AAL', 'T', $AWIS_KEY2, false);
    } else {
        $SQL .= ' WHERE AAL_AST_ATUNR = (select ast_atunr from artikelstamm where ast_key = ' . $DB->WertSetzen('AAL', 'T', $AWIS_KEY1, false) . ')';
    }

    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('AAL', false));
    // Zum Bl�ttern in den Daten
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;

    $SQL .= $ORDERBY;

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

    $rsAAL = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('AAL', true));
    $Form->Formular_Start();

    if ($AWIS_KEY2 == '') //Liste
    {
        $Spaltenbreiten['AAL_AST_ATUNR'] = 120;
        $Spaltenbreiten['AST_BEZEICHNUNGWW'] = 450;
        $Spaltenbreiten['AST_BEZEICHNUNG'] = 450;

        $Form->ZeileStart();
        if (($Recht462 & 4) == 4) //Hinzuf�gen Recht
        {
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen&AAL_KEY=-1', 'A', $AWISSprachKonserven['AAL']['AAL_NEU']);
            $Form->Erstelle_ListeIcons($Icons, 20, -2);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=AAL_AST_ATUNR' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'AAL_AST_ATUNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], $Spaltenbreiten['AAL_AST_ATUNR'], '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=AST_BEZEICHNUNGWW' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'AST_BEZEICHNUNGWW'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], $Spaltenbreiten['AST_BEZEICHNUNGWW'], '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=AST_BEZEICHNUNG' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'AST_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNG'], $Spaltenbreiten['AST_BEZEICHNUNG'], '', $Link);

        $Form->ZeileEnde();

        // Blockweise
        $StartZeile = 0;
        if (isset($_GET['Block'])) {
            $StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
        }

        $Menge = 0;
        $Vorgaenge = 0;
        $Mengen = array();

        $DS = 0;
        while (!$rsAAL->EOF()) {
            $Form->ZeileStart();
            if (($Recht462 & 8) == 8) //L�schen Recht
            {
                $Icons = array();
                $Icons[] = array(
                    'delete',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen&Del&AAL_KEY=' . $rsAAL->FeldInhalt('AAL_KEY'),
                    '',
                    $AWISSprachKonserven['AAL']['AAL_DEL']
                );
                $Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));
            }
            $Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR=' . $rsAAL->FeldInhalt('AAL_ALTERNATIV_AST_ATUNR');

            $Form->Erstelle_ListenFeld('AAL_ALTERNATIV_AST_ATUNR', $rsAAL->FeldInhalt('AAL_ALTERNATIV_AST_ATUNR'), 0, $Spaltenbreiten['AAL_AST_ATUNR'], false, ($DS % 2), '', $Link,
                'T');
            $Link = '';
            $Form->Erstelle_ListenFeld('AAL_AST_BEZEICHNUNGWW', $rsAAL->FeldInhalt('AST_BEZEICHNUNGWW'), 0, $Spaltenbreiten['AST_BEZEICHNUNGWW'], false, ($DS % 2), '', $Link, 'T',
                'L');
            $Link = '';
            $Form->Erstelle_ListenFeld('AAL_AST_BEZEICHNUNG', $rsAAL->FeldInhalt('AST_BEZEICHNUNG'), 0, $Spaltenbreiten['AST_BEZEICHNUNG'], false, ($DS % 2), '', $Link, 'T', 'L');

            $Form->ZeileEnde();

            $rsAAL->DSWeiter();
            $DS++;
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Artikelalternativen';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else //Neuer
    {
        $AWISCursorPosition = 'sucAAL_ALTERNATIV_AST_ATUNR';
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AAL']['AAL_ALTERNATIV_AST_ATUNR'] . ': ', 150);
        $Form->AuswahlBox('!AAL_ALTERNATIV_AST_ATUNR', 'Box1', '', 'AAL_Artikelalternativen', '', 100, 10, $rsAAL->FeldInhalt('AAL_ALTERNATIV_AST_ATUNR'), 'T',
            ($Recht462 & 2) == 2, '', '', '', 0, '', '', '', 0, '', '', 'oninput;');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AAL']['AAL_BEIDSEITIG'] . ': ', 150);
        $Form->Erstelle_Checkbox('AAL_BEIDSEITIG', 'on', 50, ($Recht462 & 2) == 2);
        $Form->ZeileEnde();

        ob_start();

        $AAL = new artikelstamm_Artikelalternativen_Funktionen();
        $AAL->erstelleArtikelalternativenAjax($rsAAL->FeldInhalt('AAL_ALTERNATIV_AST_ATUNR'));

        $Form->AuswahlBoxHuelle('Box1', '', '', ob_get_clean());
    }

    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201320");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201321");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>