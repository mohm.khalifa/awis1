<?php

class ArrayOfPartnerBestellBestaetigungV3 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PartnerBestellBestaetigungV3[] $PartnerBestellBestaetigungV3
     */
    protected $PartnerBestellBestaetigungV3 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PartnerBestellBestaetigungV3[]
     */
    public function getPartnerBestellBestaetigungV3()
    {
      return $this->PartnerBestellBestaetigungV3;
    }

    /**
     * @param PartnerBestellBestaetigungV3[] $PartnerBestellBestaetigungV3
     * @return ArrayOfPartnerBestellBestaetigungV3
     */
    public function setPartnerBestellBestaetigungV3(array $PartnerBestellBestaetigungV3 = null)
    {
      $this->PartnerBestellBestaetigungV3 = $PartnerBestellBestaetigungV3;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PartnerBestellBestaetigungV3[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PartnerBestellBestaetigungV3
     */
    public function offsetGet($offset)
    {
      return $this->PartnerBestellBestaetigungV3[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PartnerBestellBestaetigungV3 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->PartnerBestellBestaetigungV3[] = $value;
      } else {
        $this->PartnerBestellBestaetigungV3[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PartnerBestellBestaetigungV3[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PartnerBestellBestaetigungV3 Return the current element
     */
    public function current()
    {
      return current($this->PartnerBestellBestaetigungV3);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PartnerBestellBestaetigungV3);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PartnerBestellBestaetigungV3);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PartnerBestellBestaetigungV3);
    }

    /**
     * Countable implementation
     *
     * @return PartnerBestellBestaetigungV3 Return count of elements
     */
    public function count()
    {
      return count($this->PartnerBestellBestaetigungV3);
    }

}
