<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $SK;

$GespeicherteFelder = array();
if (isset($SK->Param['GespeicherteFelder'])) {
    $GespeicherteFelder = $SK->Param['GespeicherteFelder'];
}

$SK->Form->Formular_Start();

foreach ($SK->FilterFelder as $Feld) {
    $Feldtyp = substr($Feld, 0, 3);
    $Feld = substr($Feld, 3);
    $Bereich = explode('_', $Feld)[0];
    $SK->Form->ZeileStart();
    $SK->Form->Erstelle_TextLabel($SK->AWISSprachKonserven[$Bereich][$Feld] . ': ', 190);

    switch ($Feldtyp) {
        case 'txt':
            $Operatoren = '=~=|like~like|<>~<>|not like~not like';
            $Operatoren = explode('|', $Operatoren);
            $SK->Form->Erstelle_SelectFeld($Feld . '_operator', '', '250:120', true, '', $SK->BitteWaehlen, '', '', '', $Operatoren);

            $SK->Form->Erstelle_TextFeld($Feld, '', 20, 200, true);
            break;

        case 'cmb':
            $Operatoren = '=~=|<>~<>';
            $Operatoren = explode('|', $Operatoren);
            $SK->Form->Erstelle_SelectFeld($Feld . '_operator', '', '250:120', true, '', $SK->BitteWaehlen, '', '', '', $Operatoren);

            switch ($Feld) {
                case 'WGR_ID':
                    $SQL = 'select wgr_id, wgr_id||\' \'||wgr_bezeichnung from warengruppen order by 1 asc';
                    $SK->Form->Erstelle_SelectFeld('~' . $Feld, '', '500:200', true, $SQL, $SK->BitteWaehlen, '', '', '', '');
                    break;
                case 'WUG_KEY':
                    $SQL = 'select wug_KEY, WUG_WGR_ID || \' - \' || wug_id||\' \'||wug_bezeichnung from WARENUNTERGRUPPEN order by WUG_WGR_ID, WUG_ID asc';
                    $SK->Form->Erstelle_SelectFeld('~' . $Feld, '', '500:200', true, $SQL, $SK->BitteWaehlen, '', '', '', '');
                    break;
                case 'LIE_NR':
                    $SK->Form->Erstelle_AutocompleteFeld($Feld, '', 200, true, 'autocomplete_LIE_NR', 3, $SK->BitteWaehlen, '', '');
                    break;
                default:
                    $SQL = '';
            }

            break;
    }

    $SK->Form->ZeileEnde();
}

$SK->Form->ZeileStart();
$SK->Form->Trennzeile('L');
$SK->Form->ZeileEnde();

$SK->Form->ZeileStart();
$SK->Form->Erstelle_TextLabel($SK->AWISSprachKonserven['Wort']['BedingungHinzufuegen'], 0);
$SK->Form->ZeileEnde();

$SK->Form->ZeileStart();
$SK->Form->Erstelle_TextLabel('AUTOMATISCH', 110, 'font-weight: bold;');
$SK->Form->Schaltflaeche('image', 'BedinungAdd', '', '/bilder/cmd_neu.png', 'hinzuf�gen', '', '', 20, 20, '', true);

$SK->Form->ZeileEnde();
if (isset($_POST['BedinungAdd_x'])) {
    foreach ($SK->FilterFelder as $Feld) {
        $Feld = str_replace('cmb', 'txt', $Feld);
        //Feld da und Wert enthalten?
        if (isset($_POST[$Feld]) and $_POST[$Feld] != '' and $_POST[$Feld] != $SK->BitteWaehlen) {
            if ($_POST[$Feld . '_operator'] != '' and $_POST[$Feld . '_operator'] != $SK->BitteWaehlen) {
                $GespeicherteFelder[] = array('Feld' => $Feld, 'Wert' => $_POST[$Feld], 'Operator' => $_POST[$Feld . '_operator']);
            }
        }
    }
}
$Del = $SK->Form->NameInArray($_POST, 'BedinungEntf_', 0, 1);
//Wurde gel�scht?

if ($Del !== false) {
    $IDX = explode('_', $Del)[1];
    unset($GespeicherteFelder[$IDX]);
}

foreach ($GespeicherteFelder as $Key => $Feld) {
    $SK->Form->ZeileStart();
    $SK->Form->Schaltflaeche('image', 'BedinungEntf_' . $Key, '', '/bilder/icon_delete.png', 'entfernen', '', '', 20, 20, '', true);
    $Bereich = substr(explode('_', $Feld['Feld'])[0], 3);
    $SK->Form->Erstelle_TextLabel($SK->AWISSprachKonserven[$Bereich][substr($Feld['Feld'], 3)], 250);
    $SK->Form->Erstelle_TextLabel($Feld['Operator'], 190);
    $SK->Form->Erstelle_TextLabel($Feld['Wert'], 100);
    $SK->Form->ZeileEnde();
}

$SK->Form->Formular_Ende();

$SK->Param['GespeicherteFelder'] = $GespeicherteFelder;

echo '</div>';
$SK->Form->SchaltflaechenStart();
$SK->Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $SK->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
if (count($GespeicherteFelder) > 0) {
    $SK->Form->Schaltflaeche('image', 'cmdStep_2', '', '/bilder/cmd_dsweiter.png', $SK->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
}
$SK->Form->Erstelle_TextLabel('', 50, '');
$SK->Form->Erstelle_TextLabel('<b>Ist-Zustand</b>', 100, 'padding-top: 7px;');
$SK->Form->Erstelle_Checkbox('IstZustand', (isset($_POST['txtIstZustand'])?1:0), 30, true, 1, 'padding-top: 5px;');
$SK->Form->Erstelle_Bild('default', 'HilfeISTZustand', '', '/bilder/info.png', $SK->AWISSprachKonserven['AST']['AST_HILFE_SK_IST_ZUSTAND'], 'padding-top: 5px;', 20, 20);
$SK->Form->SchaltflaechenEnde();

$SK->Form->SchreibeHTMLCode('</form>');

?>