<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('REL','%');	
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Wort','Gesamt');
	$TextKonserven[]=array('Wort','Soll');
	$TextKonserven[]=array('Wort','Ist');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $AWIS_KEY1;
	global $FIL_ID;
	global $LFDNR;
	
	$Recht3602 = $AWISBenutzer->HatDasRecht(3602);
	if($Recht3602==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

	if (isset($_GET['REL_FIL_ID']) AND isset($_GET['REL_LFDNR'])) 
	{
		$FIL_ID=intval($_GET['REL_FIL_ID']);		
		$LFDNR=intval($_GET['REL_LFDNR']);		
	}	
	
	$Spalte = 10;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Lieferschein');
	
	$Zeile=_Erzeuge_Seite_Lieferschein();
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT *';
	$SQL .=' FROM Raedereinlagerungen';	
	$SQL .=' WHERE REL_FIL_ID=0'.$FIL_ID;	
	$SQL .=' AND REL_LFDNR=0'.$LFDNR;		
	$SQL .= ' ORDER BY REL_NR ASC';
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsREL = $DB->RecordSetOeffnen($SQL);
	$rsRELZeilen = $rsREL->AnzahlDatensaetze();
	
	if($rsRELZeilen==0)		// Keine Daten
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}		
		
	for($RELZeile=0;$RELZeile<$rsRELZeilen;$RELZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-20)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Lieferschein();
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);
		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(35,6,$rsREL->FeldInhalt('REL_NR'),1,0,'L',0);					
		
		$Ausdruck->_pdf->setXY($Spalte+35,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$Form->Format('D', $rsREL->FeldInhalt('REL_DATUMSCAN')),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+60,$Zeile);
		$Ausdruck->_pdf->cell(30,6,$rsREL->FeldInhalt('REL_MENGE'),1,0,'L',0);
		
		$Ausdruck->_pdf->setXY($Spalte+90,$Zeile);
		$Ausdruck->_pdf->cell(10,6,(strtoupper($rsREL->FeldInhalt('REL_BESCHAEDIGT_VL'))=='ON'?'X':''),1,0,'C',0);
		
		$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);
		$Ausdruck->_pdf->cell(10,6,(strtoupper($rsREL->FeldInhalt('REL_BESCHAEDIGT_VR'))=='ON'?'X':''),1,0,'C',0);
		
		$Ausdruck->_pdf->setXY($Spalte+110,$Zeile);
		$Ausdruck->_pdf->cell(10,6,(strtoupper($rsREL->FeldInhalt('REL_BESCHAEDIGT_HL'))=='ON'?'X':''),1,0,'C',0);
		
		$Ausdruck->_pdf->setXY($Spalte+120,$Zeile);
		$Ausdruck->_pdf->cell(10,6,(strtoupper($rsREL->FeldInhalt('REL_BESCHAEDIGT_HR'))=='ON'?'X':''),1,0,'C',0);
		
		$Ausdruck->_pdf->setXY($Spalte+130,$Zeile);
		$Ausdruck->_pdf->cell(30,6,$rsREL->FeldInhalt('REL_BEMERKUNG'),1,0,'L',0);				
		
		$Zeile+=6;
	
		$rsREL->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Lieferschein()
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;
	global $Form;	
	global $AWIS_KEY1;
	global $FIL_ID;
	global $LFDNR;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	if ($Seite == 1)
	{
		// �berschrift		
		
		$SQL = 'SELECT DISTINCT REL_LFDNR, REL_FIL_ID';
		$SQL .=' FROM Raedereinlagerungen REL';		
		$SQL .=' WHERE REL_FIL_ID=0'.$FIL_ID;		
		$SQL .=' AND REL_LFDNR=0'.$LFDNR;				
		
		$rsREL = $DB->RecordSetOeffnen($SQL);		
		
		$Ausdruck->_pdf->SetFont('Arial','B',14);			
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);			
		$Ausdruck->_pdf->Cell(180,6,'Lieferschein zur R�dereinlagerung von Filiale: '.$rsREL->FeldInhalt('REL_FIL_ID'),0,0,'L',0);
		$Zeile+=15;			
		$Ausdruck->_pdf->SetFont('Arial','B',10);
					
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['REL']['REL_LFDNR'].':',0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$rsREL->FeldInhalt('REL_LFDNR'),0,0,'L',0);
		$Zeile+=6;										
	}
	else 
	{
		$Zeile+=15;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(35,12,$AWISSprachKonserven['REL']['REL_NR'],1,0,'L',1);	
			
	$Ausdruck->_pdf->setXY($Spalte+35,$Zeile);
	$Ausdruck->_pdf->Cell(25,12,$AWISSprachKonserven['REL']['REL_DATUMSCAN'],1,0,'L',1);	
	
	$Ausdruck->_pdf->setXY($Spalte+60,$Zeile);
	$Ausdruck->_pdf->cell(30,12,$AWISSprachKonserven['REL']['REL_MENGE'],1,0,'L',1);	
			
	$Ausdruck->_pdf->setXY($Spalte+90,$Zeile);
	$Ausdruck->_pdf->cell(40,6,$AWISSprachKonserven['REL']['REL_BESCHAEDIGT'],1,0,'C',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+130,$Zeile);
	$Ausdruck->_pdf->cell(30,12,$AWISSprachKonserven['REL']['REL_BEMERKUNG'],1,0,'L',1);		
	
	$Zeile = $Zeile + 6;
	
	$Ausdruck->_pdf->setXY($Spalte+90,$Zeile);
	$Ausdruck->_pdf->cell(10,6,"VL",1,0,'C',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);
	$Ausdruck->_pdf->cell(10,6,"VR",1,0,'C',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+110,$Zeile);
	$Ausdruck->_pdf->cell(10,6,"HL",1,0,'C',1);				
	
	$Ausdruck->_pdf->setXY($Spalte+120,$Zeile);
	$Ausdruck->_pdf->cell(10,6,"HR",1,0,'C',1);					
			
	$Zeile+=6;	
	
	return $Zeile;
}
?>

