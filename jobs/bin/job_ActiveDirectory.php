<?php
require_once 'awisActiveDirectory.inc';

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $ImpExpDiffPruefung = true;
    $Schwellwertpruefung = true;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--schwellwertpruefung':
                        $Schwellwertpruefung = $argv[++$i];
                        break;
                    case '--impexpdiffruefung':
                        $ImpExpDiffPruefung = $argv[++$i];
                        break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --Schwellwertpruefung <bool>       Abweichung des letzten Exports zum aktuellen Exports darf maximal 10% Betragen. Kann hiermit deaktiviert werden';
                            echo '  --ImpExpDiffPruefung <bool>        Es m�ssen genausoviele User exportiert werden wie importiert wuden. Kann hiermit deaktivert werden.';
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }

    $AD = new awisActiveDirectory($Benutzer);
    $AD->DebugLevel($DebugLevel);
    $AD->ImpExpDiffPruefung = $ImpExpDiffPruefung;
    $AD->Schwellwertpruefung = $Schwellwertpruefung;


    if(!$AD->StarteZentrale()){
        $AD->_Werkzeuge->EMail($AD->EMAIL_Infos, 'ERROR Activedirectoryimport Zentralimport WICHTIG!!!!!!', 'Nicht behandelter Fehler aufgetreten', 2, '', 'awis@de.atu.eu');
    }

    if(!$AD->StarteFiliale()){
        $AD->_Werkzeuge->EMail($AD->EMAIL_Infos, 'ERROR Activedirectoryimport GL WL', 'Nicht behandelter Fehler aufgetreten', 2, '', 'awis@de.atu.eu');
    }
echo 'fertig';
}
catch (Exception $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;
 
}catch (awisException $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'SQL:'.$ex->getSQL().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;

}
?>
