/**
 * Created by gebhardt_p on 18.07.2016.
 */
$(document).ready(function () {

    $('#txtGDN_NACHRICHTENTYPEN').on('change', function () {
        var sel = $('#txtGDN_NACHRICHTENTYPEN');
        if (sel.val() >= 1) {
            $('#Satzarten_box').show();
            lade_txtGDN_SATZARTEN(document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].value, '');
            document.getElementsByName('txtGDN_SATZARTEN')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDN_SATZARTEN')[0].setAttribute('required','');
            $('#Felder_box').hide();
            $('#Feldoptionen_box').hide();
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
        } else {
            $('#Satzarten_box').hide();
            $('#Felder_box').hide();
            $('#Feldoptionen_box').hide();
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
            $('#Feldpfad_box').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].value = '';
            document.getElementsByName('txtGDN_SATZARTEN')[0].className = 'InputText';
            document.getElementsByName('txtGDN_SATZARTEN')[0].removeAttribute('required');
        }
    });

    $('#txtGDN_SATZARTEN').on('change', function () {
        var sel = $('#txtGDN_SATZARTEN');
        if (sel.val() >= 1) {
            $('#Felder_box').show();
            lade_txtGDF_KEY(document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].value, '');
            document.getElementsByName('txtGDF_KEY')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDF_KEY')[0].setAttribute('required','');
            $('#Feldoptionen_box').hide();
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
        } else {
            $('#Felder_box').hide();
            $('#Feldoptionen_box').hide();
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
            $('#Feldpfad_box').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].value = '';
            document.getElementsByName('txtGDF_KEY')[0].className = 'InputText';
            document.getElementsByName('txtGDF_KEY')[0].removeAttribute('required');
        }
    });

    $('#txtGDF_KEY').on('change', function () {
        var sel = $('#txtGDF_KEY');
        if (sel.val() >= 1) {
            document.getElementsByName('txtGDV_FELDTYP')[0].selectedIndex = 0;
            document.getElementsByName('txtGDV_FELDTYP')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDV_FELDTYP')[0].setAttribute('required','');
            $('#Feldoptionen_box').show();
            $('#Feldpfad_box').show();
            var NTyp = document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].options[document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].selectedIndex].text;
                NTyp = NTyp.split('~');
            var SArt = document.getElementsByName('txtGDN_SATZARTEN')[0].options[document.getElementsByName('txtGDN_SATZARTEN')[0].selectedIndex].text;
                SArt = SArt.split('~');
            var FWert = document.getElementsByName('txtGDF_KEY')[0].options[document.getElementsByName('txtGDF_KEY')[0].selectedIndex].text;
                FWert = FWert.split('~');
            document.getElementsByName('txtVSF_GDVPFAD')[0].value = document.getElementsByName('txtRELEASE')[0].value+'#'+NTyp[0]+'#'+SArt[0]+'#'+FWert[1];
            document.getElementsByName('txtGDVPFAD')[0].value = document.getElementsByName('txtRELEASE')[0].value+'#'+NTyp[0]+'#'+SArt[0]+'#'+FWert[1];

            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
        } else {
            $('#Feldoptionen_box').hide();
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
            $('#Feldpfad_box').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].value = '';
            document.getElementsByName('txtGDV_FELDTYP')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYP')[0].removeAttribute('required');
        }
    });

    $('#txtGDV_FELDTYP').on('change', function () {
        var sel = $('#txtGDV_FELDTYP');
        document.getElementsByName('txtGDV_FELDTYPWERT')[0].value = '';
        document.getElementsByName('txtGDV_FELDTYPWERT2')[0].value = '';
        document.getElementsByName('txtGDV_FELDTYPWERT3')[0].value = '';
        document.getElementsByName('txtGDV_FELDTYPWERT4')[0].value = '';
        if (sel.val() == 1) {
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].setAttribute('required','');
            $('#FeldTypWert_box').show();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].removeAttribute('required');
            document.getElementById('FeldSubmit').hidden = true;
            document.getElementById('FeldSubmit2').hidden = true;
            document.getElementById('FeldSubmit3').hidden = true;
            document.getElementById('FeldSubmit4').hidden = true;
        }
        else if (sel.val() == 2){
            $('#FeldTypWert_box2').show();
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].setAttribute('required','');
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].removeAttribute('required');
            document.getElementById('FeldSubmit2').hidden = false;
        }
        else if (sel.val() == 3){
            $('#FeldTypWert_box3').show();
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].setAttribute('required','');
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box4').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].removeAttribute('required');
            document.getElementById('FeldSubmit3').hidden = false;
        }
        else if (sel.val() == 4){
            $('#FeldTypWert_box4').show();
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].className = 'InputTextPflicht';
            document.getElementsByName('txtGDV_FELDTYPWERT4')[0].setAttribute('required','');
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT2')[0].removeAttribute('required');
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].className = 'InputText';
            document.getElementsByName('txtGDV_FELDTYPWERT3')[0].removeAttribute('required');
            document.getElementById('FeldSubmit4').hidden = false;
        }

        else {
            $('#FeldTypWert_box').hide();
            $('#FeldTypWert_box2').hide();
            $('#FeldTypWert_box3').hide();
            $('#FeldTypWert_box4').hide();
        }
    });

    $('#cmdFELDTYPWERT').on('click', function () {
        var position = document.getElementsByName('txtGDV_FELDTYPWERT')[0].style.top;
        document.getElementById('FeldTypWertOptionen').style.top = position ;	// Position Div an Datenzeile festmachen.
        document.getElementById('FeldTypWertOptionen').style.left =  499;
        $('#FeldTypWertOptionen').show();
        ladeDiv();

    });

    $('#txtTABELLE').delegate('change', function () {
        var sel = $('#txtTABELLE');
        if (sel.val() >= 1) {
            $('#Tabellenfeld_box').show();
            lade_txtTABELLENFELD(document.getElementsByName('txtTabelle')[0].value, '');
        } else {
            $('#Tabellenfeld_box').hide();
        }
    });
});


function ladeDiv() {
    var test = null;
    var Typ = 1;
    if (window.XMLHttpRequest) {
        test = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        try {
            test = new ActiveXObject('Msxml2.XMLHTTP.6.0');
        } catch (e) {
            try {
                test = new ActiveXObject('Msxml2.XMLHTTP.3.0');
            }
            catch (e) {
            }
        }
    }
    if (test == null) {
        alert('Ihr Browser unterstützt kein Ajax!');
    }

    test.open('GET', '/daten/versicherungen_feldoptionen.php?OptionsTyp=' + Typ);
    test.send(null);

    test.onreadystatechange = function () {
        if (test.readyState == 4 && test.status == 200) {
            var responseText = test.responseText;
            document.getElementById('FeldTypWertOptionen').innerHTML = test.responseText; // Wert von Seite per Ajax ins Div laden
            $("#FeldTypWertOptionen").html(responseText);
            $("#FeldTypWertOptionen").find("script").each(function (i) {
                eval($(this).text());
            });
        }
    };

}

function closeDiv() {
    $('#FeldTypWertOptionen').hide();
}

function ladeTabellenFelder() {
    var sel = $('#txtTABELLE');
    if (sel.val() != '') {
        $('#Tabellenfeld_box').show();
        lade_txtTABELLENFELD(document.getElementsByName('txtTABELLE')[0].value, '');
    } else {
        $('#Tabellenfeld_box').hide();
    }
}

function uebernehmeTabellenfeld() {
    var val = $('#txtTABELLE option:selected').text() + '.' + $('#txtTABELLENFELD  option:selected').text();
    $('#txtGDV_FELDTYPWERT').val(val);
    document.getElementsByName('txtFELDTYPWERT')[0].value = val;
    closeDiv();
    document.getElementById('FeldSubmit').hidden = false;
}
