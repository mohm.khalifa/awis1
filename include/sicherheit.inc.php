<?php
$awisRechte="";
$awisBackURL = "index.html";

/*******************************************************************************
*
* Liest Rechte f�r einen Benutzer aus
*
* //VERALTET
*******************************************************************************/

function awisRechteLesen()
{
	GLOBAL $awisRechte;			// String f�r die Benutzerrechte
	global $AWISBenutzer;
	
	$con = awisLogon();			// Verbindung �ffnen
  
//	$rsBenutzer = awisOpenRecordset($con, "SELECT * FROM AWIS.v_AccountRechte WHERE XBL_LOGIN='" . strtoupper($AWISBenutzer->BenutzerName()) . "' ORDER BY XRC_ID");
	
	
	$SQL = "SELECT * FROM AWIS.v_AccountRechte ";
	$SQL .= " WHERE XBL_LOGIN=:var_T_XBL_LOGIN";
	$SQL .= " ORDER BY XRC_ID";
	
	$BindeVariablen = array();
	$BindeVariablen['var_T_XBL_LOGIN']= strtoupper($AWISBenutzer->BenutzerName());
	$rsBenutzer = awisOpenRecordset($con, $SQL, false, false, $BindeVariablen);
//echo '*';
	while(list($FeldName, $Feld) = each($rsBenutzer))
	{
		if($FeldName=="XRC_ID")
		{
			for($i=0;$i<count($Feld);$i++)
			{
				$awisRechte .= ";" . $Feld[$i];			// Recht anf�gen
			}
			$awisRechte .= ";";
		}
	}
	
	awisLogoff($con);			// Wieder abmelden
}

/*******************************************************************************
*
* Pr�ft, ob ein Benutzer ein bestimmtes Recht hat
*
* VERALTET!!!
* 
*******************************************************************************/

function awisHatDasRecht($Recht)
{
	GLOBAL $awisRechte;

	if($awisRechte=='')				// Rechte noch nicht ausgelesen
	{
		awisRechteLesen();
	}	
	return strstr($awisRechte,';'.$Recht.';')!=FALSE;

}

/**
* Liefert eine Benutzer-Rechtestufe
*
* @author Sacha Kerres
* @param ressource $con
* @param int $Recht
* @return int
* @version 1.0
*  
******************************************************/
function awisBenutzerRecht($con, $Recht)
{
	global $awisRSZeilen;
	global $AWISBenutzer;

	//$SQL = "SELECT XBA_STUFE AS STUFE FROM AWIS.v_AccountRechte WHERE XBL_LOGIN='" . strtoupper($AWISBenutzer->BenutzerName()) . "' AND XBA_XRC_ID=0" . $Recht;
	
	$SQL = "SELECT XBA_STUFE AS STUFE FROM AWIS.v_AccountRechte ";
	$SQL .= " WHERE XBL_LOGIN=:var_T_XBL_LOGIN";
	$SQL .= " AND XBA_XRC_ID=:var_N0_XBA_XRC_ID";
	$SQL .= " ORDER BY XRC_ID";
	
	$BindeVariablen = array();
	$BindeVariablen['var_T_XBL_LOGIN']= strtoupper($AWISBenutzer->BenutzerName());
	$BindeVariablen['var_N0_XBA_XRC_ID']= $Recht;
	
	$rsRecht = awisOpenRecordset($con, $SQL, false, false, $BindeVariablen);
	$rsRechtZeilen = $awisRSZeilen;
	
	$Stufe = 0;
	for($BR=0;$BR<$rsRechtZeilen;$BR++)
	{
		$Stufe = $Stufe | $rsRecht["STUFE"][$BR];
//print "$Recht, $BR: $Stufe, ;" . $rsRecht["STUFE"][$BR] . '--' . $SQL;
	}

	return $Stufe;
}

/******************************************************
* Ereignisse schreiben
******************************************************/

Function awisEreignis($Typ, $ErrNr, $P1='', $P2='', $P3='', $P4='', $P5='')
{
	global $AWISBenutzer;

	$con = awisLogon();
	if($con != FALSE)
	{
		if(isset($_SERVER['REMOTE_ADDR']))
		{
			$Remote_Addr=$_SERVER['REMOTE_ADDR'];
		}
		else{
			$Remote_Addr='REMOTE_ADDR nicht definiert';
		}
		
		awisExecute($con, "BEGIN SCHREIBEEREIGNISSE($Typ, $ErrNr, '" . $Remote_Addr . "', '" . $AWISBenutzer->BenutzerName() . "', '$P1', '$P2', '$P3', '$P4', '$P5'); END;");
    	awisLogoff($con);
	}
}

/******************************************************
* Berechnet das Pa�wort aus dem aktuellen Usernamen
* 
* (Nur zur Einwahl in den DB-Server)
******************************************************/
function awisBenutzerPwd()
{
	global $AWISBenutzer;

	return $AWISBenutzer->BenutzerName();
}

/*********************************************************
* 
* Liefert einen Namen f�r eine Rechte-ID
* 
*********************************************************/
function awisRechteName($con, $id)
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_xrc_id']=$id;
	
	$rsRecht = awisOpenRecordset($con, "SELECT * FROM AWIS.Rechte WHERE XRC_ID=:var_N0_xrc_id",true,false,$BindeVariablen);
	return $rsRecht["XRC_RECHT"][0];
}

//*******************************************************************************
// AWIS User Objekt erstellen
//*******************************************************************************
require_once('awisUser.php');
global $AWISBenutzer;
$AWISBenutzer=new awisUser();
?>