<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20081006
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZAL','ZAL_BESTELLNUMMER');
	$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10001=$AWISBenutzer->HatDasRecht(10001);		// Rechte des Mitarbeiters
    if($Recht10001==0)
    {
    	$DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
    	$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
    	die();
    }	
	
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./zukaufartikel_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZLA'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	// Artikelnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZLA_ARTIKELNUMMER',($Param['SPEICHERN']=='on'?$Param['ZLA_ARTIKELNUMMER']:''),30,300,true);
	$AWISCursorPosition = 'sucZLA_ARTIKELNUMMER';
	$Form->ZeileEnde();

	// Artikelbezeichnung
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*ZLA_BEZEICHNUNG',($Param['SPEICHERN']=='on'?$Param['ZLA_BEZEICHNUNG']:''),30,300,true);
	$Form->ZeileEnde();

	// Artikelbezeichnung
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLI_KEY'].':',190);
	$SQL = 'SELECT LIE_NR, LIE_NAME1 FROM Lieferanten';
	$SQL .= ' WHERE EXISTS( SELECT * FROM ZukaufArtikelLieferanten WHERE LIE_NR = ZAL_LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('*LIE_NR',($Param['SPEICHERN']=='on'?$Param['LIE_NR']:''),'300:290',true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG || COALESCE(\', \' || ZLH_KUERZEL,\'\') FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG, ZLH_KUERZEL';
	$Form->Erstelle_SelectFeld('*ZLA_ZLH_KEY',($Param['SPEICHERN']=='on'?$Param['ZLA_ZLH_KEY']:''),'300:290',true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',190);
	$Form->Erstelle_TextFeld('*ZSZ_SORTIMENT',($Param['SPEICHERN']=='on'?$Param['ZSZ_SORTIMENT']:''),20,300,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',190);
	$Form->Erstelle_TextFeld('*ZLA_AST_ATUNR',($Param['SPEICHERN']=='on'?$Param['ZLA_AST_ATUNR']:''),20,300,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['Nummer'].':',190);
	$Form->Erstelle_TextFeld('*Nummer',($Param['SPEICHERN']=='on'?$Param['ZAI_ID']:''),20,300,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BESTELLNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZAL_BESTELLNUMMER',($Param['SPEICHERN']=='on'?$Param['ZAL_BESTELLNUMMER']:''),20,300,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_TAUSCHTEIL'].':',190);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	$Form->Erstelle_SelectFeld('*ZLA_TAUSCHTEIL',($Param['SPEICHERN']=='on'?$Param['ZLA_TAUSCHTEIL']:''),300,true,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();

	$Form->Trennzeile();

	// Auswahl kann gesZLAchert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht10001&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=zukaufartikel&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180836");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180825");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>