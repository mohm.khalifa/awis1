<?php
@ini_set('memory_limit','2048M');
require_once('awisDokumente.php');

if(isset($_GET['dockey']))
{
	$DOC = new awisDokumente();
	$Datei = $DOC->DokumentPfad(intval($_GET['dockey']),substr($_GET['doctab'],0,3));
	if($Datei===false)
	{

	}
	else
	{
		@ob_clean();

		//header("Cache-Control: no-cache, must-revalidate");
		//header("Expires: 01 Jan 2000");
		header('Pragma: public');
		header('Cache-Control: max-age=0');
		header('Content-type: application/'.$Datei['Erweiterung']);
		header('Content-Disposition: attachment; filename="'.$Datei['Bezeichnung'].'.'.$Datei['Erweiterung'].'"');
		readfile($Datei['Pfad']);
	}
}
elseif(isset($_GET['dateiname']))
{
	$Pfad = '/daten/web/dokumente/nichtgefunden.pdf';
	$Erweiterung = 'pdf';						
	
	switch ($_GET['bereich'])
	{
		case 'sonstiges':
			if(is_file('/daten/web/dokumente/sonstiges/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
				$Pfad = '/daten/web/dokumente/sonstiges/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
				$Erweiterung = $_GET['erweiterung'];
			}
			break;
		case 'seminarplan':
			if(is_file('/daten/web/dokumente/seminarplan/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
				$Pfad = '/daten/web/dokumente/seminarplan/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
				$Erweiterung = $_GET['erweiterung'];
			}
			break;
		case 'seminarplananfahrt':
		    if(is_file('/daten/web/dokumente/seminarplan/Anfahrtsbeschreibungen/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
			    $Pfad = '/daten/web/dokumente/seminarplan/Anfahrtsbeschreibungen/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
			    $Erweiterung = $_GET['erweiterung'];
			}
			break;
		case 'seminarziele':
		    if(is_file('/daten/web/dokumente/seminarplan/Lernzielblaetter/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
		    {

		        $Pfad = '/daten/web/dokumente/seminarplan/Lernzielblaetter/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
		        $Erweiterung = $_GET['erweiterung'];
		    }
		    break;
		case 'reifenzertifikate':
			if(is_file('/daten/web/dokumente/reifenzertifikate/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
				$Pfad = '/daten/web/dokumente/reifenzertifikate/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
				$Erweiterung = $_GET['erweiterung'];
			}
			break;
		case 'einbaupreislisten':
			if (isset($_GET['land']))
			{
				if(is_file('/daten/web/dokumente/Einbaupreislisten/'.$_GET['land'].'/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
				{
					$Pfad = '/daten/web/dokumente/Einbaupreislisten/'.$_GET['land'].'/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
					$Erweiterung = $_GET['erweiterung'];					
				}					
			}			
			break;
		case 'kdtelefonie':
			if(is_file('/daten/web/dokumente/kdtelefonie/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
				$Pfad = '/daten/web/dokumente/kdtelefonie/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
				$Erweiterung = $_GET['erweiterung'];
			}
			break;
		case 'itanforderungen':
			if(is_file('/daten/daten/ATU-Alle/11-Informationsdokumente/01-Zentrale/Kostenstellen/02_Kostenstellen/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
			{
				$Pfad = '/daten/daten/ATU-Alle/11-Informationsdokumente/01-Zentrale/Kostenstellen/02_Kostenstellen/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
				$Erweiterung = $_GET['erweiterung'];
			}
			break;
        case 'postfachleitfaden':
            if(is_file('/daten/web/dokumente/tickets/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
            {
                $Pfad = '/daten/web/dokumente/tickets/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
                $Erweiterung = $_GET['erweiterung'];
            }
            break;
        case 'organisationsanweisungundcheckliste':
            if(is_file('/daten/web/dokumente/tickets/'.$_GET['dateiname'].'.'.$_GET['erweiterung']))
            {
                $Pfad = '/daten/web/dokumente/tickets/'.$_GET['dateiname'].'.'.$_GET['erweiterung'];
                $Erweiterung = $_GET['erweiterung'];
            }
            break;
        case 'datenexporte':
            $AbfragenOrdner = $_GET['ordner'] . '/';
            if(is_file('/daten/web/dokumente/datenexport/'.$AbfragenOrdner.$_GET['dateiname']));
            {
                $Pfad = '/daten/web/dokumente/datenexport/'.$AbfragenOrdner.$_GET['dateiname'];
                $Erweiterung = $_GET['erweiterung'];
            }
            break;
        case 'fremdkaufauswertung':
            if(is_file('/daten/web/dokumente/fremdkaufauswertung/fremdkauf.csv'));
            {
                $Pfad = '/daten/web/dokumente/fremdkaufauswertung/fremdkauf.csv';
            }
            break;
	}
	if(!is_file($Pfad))
	{
		$Pfad = '/daten/web/dokumente/nichtgefunden.pdf';
	}
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header('Content-type: application/'.$_GET['erweiterung']);
	header('Content-Disposition: attachment; filename="'.$_GET['dateiname'].'.'.$_GET['erweiterung'].'"');
	readfile($Pfad);
}
?>