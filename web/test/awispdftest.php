<?php
	/*
	 *	This is a rather extensive example to show you, what's all possible.
	 *	On my notebook ( a PentiumII w/ 366MHz ) this script takes app. 6 sec
	 *	for processing and a few more seconds for rendering in the PDF viewer.
	 *	For getting an impression, what certain objects do, just comment all 
	 *	others out, maybe have a look at the debug-output...you know the drill ;-)
	 *
	 *	If you have major problems with this example, please use the
	 *	forum on the sourceforge.net-projectsite (see README) or send me
	 *	an email, but I would like to ask you to use the forum, I usually go
	 *	there at least once a day.
	 *	Have fun!
	 */

	// If you experience timeouts running this script, uncomment this
	// command to extend the execution-time.
	// set_time_limit( 0 );

	// First include the wrapper. Every other class is included there
	include( "pc4p_init.inc" );
	require_once("db.inc.php");		// DB-Befehle
	require_once("admin.inc.php");

	// Uncomment this define, to get a tree-view of your PDF. WARNING!!!
	// This view can get HUGE!!!!!
	// define( "PC4PDEBUG", 1 );

	// Uncomment this define, to get a grid on your document. You may check the
	// positioning with this feature. The number you enter will be taken as grid-width
	// define( "PC4PGRID", 5 );

/*	// This function may look a bit awkward, but with GDLib < 2.0 the functionality is
	// somewhat limited
	function draw_smilie()
	{
		$im = ImageCreate( 50, 50 );
		$white = ImageColorAllocate ($im, 255, 255, 255);
		$yellow = ImageColorAllocate ($im, 255, 204, 0);
		$black = ImageColorAllocate ($im, 0, 0, 0);
		ImageArc( $im, 25, 25, 45, 45, 0, 360, $black );
		ImageArc( $im, 25, 25, 42, 42, 0, 360, $black );
		ImageArc( $im, 17, 17, 2, 2, 0, 360, $black );
		ImageArc( $im, 33, 17, 2, 2, 0, 360, $black );
		ImageFill( $im, 25, 25, $yellow );
		ImageArc( $im, 17, 17, 5, 5, 0, 360, $black );
		ImageArc( $im, 33, 17, 5, 5, 0, 360, $black );
		ImageArc( $im, 25, 25, 30, 30, 0, 180, $black );
		ImageArc( $im, 25, 25, 28, 28, 0, 180, $black );
		return $im;
	}
*/

$admcon = awisAdminLogon();

if($admcon)
{

	$rname = "atlx03su64";
//	$rsStat = create_fshist_stat_array($admcon,$rname);
	$rsDaten = create_fshist_array($admcon, $rname);


	// Create yourself a PDF...
	$PDF = &pc4p_create_pdf( array( "Author" => "Alexander Wirtz", "Title" => "PC4P-Test" ) );

	// The following parameters have to be set, prior to the first creation of a page:
	// Set a different version of Acrobat...here version 5
	$PDF->pc4p_set_compatibility( "1.4" );

	// Set your serial for PDFlib
	// $PDF->pc4p_set_serial( "I-Have-No-Serial" );

	// Use a temporary file instead of creating the PDF in the memory...better for large files
	// $PDF->pc4p_set_tempfile( "/tmp" );
	// $PDF->pc4p_set_tempfile( "C:/WINDOWS/TEMP" );

	// Set for maximum compression
	$PDF->pc4p_set_compression( 9 );

	// "Normal" display, usually in a plugin
	//$PDF->pc4p_set_savepdf( "inline", "pc4p.pdf" );
	// Force download of the File
	// $PDF->pc4p_set_savepdf( "attached", "pc4p.pdf" );
	// Save the file in your filesystem
	$PDF->pc4p_set_savepdf( "drive", "tabelle.pdf" );

	// ...and within this PDF a page with a DIN A4-format
	$Page1 = &pc4p_create_page( $PDF, "a4", "landscape" );
	$Page1->pc4p_set_margin(array("top"=>20, "bottom"=>20, "left"=>30, "right"=>10));
	$Page1->pc4p_set_newpage( 1 );
	$bookmark1 = $Page1->pc4p_add_bookmark( "The example for PC4P", 0, 1 );
	$bookmark2 = $Page1->pc4p_add_bookmark( "First Page", $bookmark1, 1 );

	// Pagenumber for the page
	$pagenum = &pc4p_create_object( $Page1->header, "pagenumber" );
	$pagenum->pc4p_set_text( "p. %P" );
	$pagenum->pc4p_set_alignment( "center" );

	// And a third table...
	$table3 = &pc4p_create_object( $Page1, "table" );
	$table3->pc4p_create_tablematrix( 2 );
	$table3->pc4p_set_width( 400 );

	// ...with a tableheader...
	$table3->pc4p_set_tableheader( 0 );
	$table4 = &pc4p_create_object( $table3->cell[ 0 ][ 0 ] , "table" );
	$table4->pc4p_create_tablematrix( 2, 1 );
	$table4->pc4p_set_cellfillcolor( $table4->cell[ 0 ][ 0 ], "lime" );
	$table4->pc4p_set_cellfillcolor( $table4->cell[ 1 ][ 0 ], "#008080" );
		$text1 = &pc4p_create_object( $table4->cell[ 0 ][ 0 ] , "text" );
		$text1->pc4p_set_alignment( "center" );
		$text1->pc4p_set_text( "This" );
		$text2 = &pc4p_create_object( $table4->cell[ 1 ][ 0 ] , "text" );
		$text2->pc4p_set_alignment( "center" );
		$text2->pc4p_set_text( "is" );

	$text3 = &pc4p_create_object( $table3->cell[ 1 ][ 0 ] , "text" );
	$text3->pc4p_set_alignment( "center" );
	$text3->pc4p_set_text( "the table-header" );
	// ...and lots of rows!
//	for( $i = 1; $i < 200; $i++) {
	for( $i = 0; $i < count($rsDaten["FS_ID"]); $i++) {
		$table3->pc4p_add_tablerow();
			$text1 = &pc4p_create_object(  $table3->cell[ 0 ][ $i ], "text" );
			$text1->pc4p_set_alignment( "right" );
			$text1->pc4p_set_text( $rsDaten["FH_FREI"][$i] );
			$text2 = &pc4p_create_object(  $table3->cell[ 1 ][ $i ], "text" );
			$text2->pc4p_set_alignment( "left" );
			$text2->pc4p_set_text( " a table - ".$i );
	}
	$table3->pc4p_set_tableborder();

	// Now I draw the PDF and (hopefully) get the output
	$PDF->pc4p_draw();
}	
print "Fertsch! ".time();
?>
