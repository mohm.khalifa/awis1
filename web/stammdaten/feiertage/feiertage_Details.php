<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XFT','%');
	$TextKonserven[]=array('FIL','FIL_BEZ');
	$TextKonserven[]=array('BUL','BUL_BUNDESLAND');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht22000 = $AWISBenutzer->HatDasRecht(22000);
	if($Recht22000==0)
	{
	    awisEreignis(3,1000,'Feiertage',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XFT'));
	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='XFT_TAG DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}

	if(isset($_POST['cmdExport_x']))
	{
		$SQL = 'SELECT PFA_FIL_ID,PFA_AST_ATUNR,PFA_ARTIKELBEZEICHNUNG,PFA_BESCHREIBUNG,PFA_MBW_KEY';
		$SQL .= ',PFA_DATUM,PFA_PREIS,PFA_DLNR,PFA_DLPREIS,PFA_KOMPLETTNR,PFA_KOMPLETTPREIS';
		$SQL .= ',PFA_ERMITTLUNGSSTATUS,PFA_BEMERKUNG,PFA_USER,PFA_USERDAT';
		$SQL .= ' FROM PREISABFRAGENPREISE';
		$SQL .= ' WHERE PFA_XFT_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtXFT_KEY'],false);
		$rsPFA = $DB->RecordSetOeffnen($SQL);
		
		$DateiName = '/tmp/Preisabfrage_'.$DB->FeldInhaltFormat('N0',$_POST['txtXFT_KEY'],false).'.csv';
		$fp = fopen($DateiName,'w+');
		foreach($rsPFA->Feldliste() AS $FeldNr=>$FeldName)
		{
			fputs($fp,$AWISSprachKonserven['PFA'][strtoupper($FeldName)]."\t");
		}
		fputs($fp,PHP_EOL);

		while(!$rsPFA->EOF())
		{
			foreach($rsPFA->Feldliste() AS $FeldNr=>$FeldName)
			{
				$Daten = $Form->Format($rsPFA->FeldInfo($FeldName,'TypKZ'),$rsPFA->FeldInhalt($FeldName));
				fputs($fp,$Daten."\t");
			}
			fputs($fp,"\n");

			$rsPFA->DSWeiter();
		}
		fclose($fp);
		require_once 'awisMailer.inc';		
		$Mail = new awisMailer($DB, $AWISBenutzer);
		$Mail->Betreff('Ergebnis der Preisabfrage');
		$Mail->Absender('awis@de.atu.eu');
		$Mail->AdressListe(awisMailer::TYP_TO,$AWISBenutzer->EMailAdresse(),false,awisMailer::PRUEFAKTION_KEINE);
		$Mail->Text('Anbei Ihre ausgew&auml;hlten Daten.',awisMailer::FORMAT_HTML);
		$Mail->Anhaenge($DateiName, 'Preisabfrage.csv');
		if($Mail->MailSenden()===false)
		{
			$Mail->MailInWarteschlange($AWISBenutzer);
		}
		unlink($DateiName);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Daten werden per E-Mail gesendet.', 500, 'Hinweis');
		$Form->ZeileEnde();
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtXFT_KEY'],false);
	}
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['XFT_BEZEICHNUNG'] = $_POST['sucXFT_BEZEICHNUNG'];
		$Param['FIL_ID'] = $_POST['sucFIL_ID'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='XFT_TAG DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_XFT",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./feiertage_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./feiertage_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XFT'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XFT'));
	}
	elseif(isset($_GET['XFT_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['XFT_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XFT'));
	}
	elseif($AWIS_KEY1==0) 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='XFT_TAG DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY XFT_TAG DESC';
			$Param['ORDER']='XFT_TAG DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT Feiertage.*';
	$SQL .= ', FIL_BEZ, BUL_BUNDESLAND';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Feiertage';
	$SQL .= ' LEFT OUTER JOIN Filialen ON XFT_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN BUNDESLAENDER ON XFT_BUL_ID = BUL_ID';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsXFT = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmFeiertage action=./feiertage_Main.php?cmdAktion=Details method=POST enctype="multipart/form-data">');

	if($rsXFT->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Param['BLOCK']=1;		// Zur�ck setzen, falls es einfach den Block nicht mehr gibt.
	}
	elseif($rsXFT->AnzahlDatensaetze()>1 AND !isset($_GET['XFT_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();
		$Param['KEY']='';

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './feiertage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XFT_TAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XFT_TAG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XFT']['XFT_TAG'],110,'',$Link);
		$Link = './feiertage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XFT_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XFT_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XFT']['XFT_BEZEICHNUNG'],390,'',$Link);
		$Link = './feiertage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XFT_LAN_CODE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XFT_LAN_CODE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XFT']['XFT_LAN_CODE'],100,'',$Link);
		$Link = './feiertage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=BUL_BUNDESLAND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BUL_BUNDESLAND'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BUL']['BUL_BUNDESLAND'],200,'',$Link);
		$Link = './feiertage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FIL_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_BEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],190,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsXFT->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './feiertage_Main.php?cmdAktion=Details&XFT_KEY=0'.$rsXFT->FeldInhalt('XFT_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#XFT_TAG',$rsXFT->FeldInhalt('XFT_TAG'),0,110,false,($DS%2),'',$Link,'D');
			$Form->Erstelle_ListenFeld('#XFT_BEZEICHNUNG',$rsXFT->FeldInhalt('XFT_BEZEICHNUNG'),0,390,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#XFT_LAN_CODE',$rsXFT->FeldInhalt('XFT_LAN_CODE'),0,100,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#BUL_BUNDESLAND',$rsXFT->FeldInhalt('BUL_BUNDESLAND'),0,200,false,($DS%2),'','');
			$Link = $rsXFT->FeldInhalt('FIL_BEZ')!=''?'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsXFT->FeldInhalt('XFT_FIL_ID'):'';
			$Form->Erstelle_ListenFeld('#FIL_BEZ',$rsXFT->FeldInhalt('FIL_BEZ'),0,190,false,($DS%2),'',$Link);
			if($rsXFT->FeldInhalt('XFT_FILAUSNAHME')!='')
			{
				$Form->Erstelle_HinweisIcon('icon_exclamation',20,($DS%2), $AWISSprachKonserven['XFT']['XFT_FILAUSNAHME'].': '.$rsXFT->FeldInhalt('XFT_FILAUSNAHME'));
			}
			$Form->ZeileEnde();

			$rsXFT->DSWeiter();
			$DS++;
		}

		$Link = './feiertage_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsXFT->FeldInhalt('XFT_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));

		$Form->Erstelle_HiddenFeld('XFT_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./feiertage_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXFT->FeldInhalt('XFT_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXFT->FeldInhalt('XFT_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht22000&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht22000&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_TAG'].':',200);
		$Form->Erstelle_TextFeld('!XFT_TAG',$rsXFT->FeldInhalt('XFT_TAG'),10,200,$EditRecht,'','','','D');
		$AWISCursorPosition = 'txtXFT_TAG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_BEZEICHNUNG'].':',200,'','');
		$Form->Erstelle_TextFeld('!XFT_BEZEICHNUNG',$rsXFT->FeldInhalt('XFT_BEZEICHNUNG'),60,300,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_LAN_CODE'].':',200);
		$SQL = 'SELECT LAN_CODE, LAN_LAND';
		$SQL .= ' FROM LAENDER';
		$SQL .= ' WHERE LAN_WWSKENN IS NOT NULL';
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('XFT_LAN_CODE',$rsXFT->FeldInhalt('XFT_LAN_CODE'),200,$EditRecht,$SQL,'','','','',array('~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']));
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_BUL_ID'].':',180);
		if($rsXFT->FeldInhalt('XFT_BUL_ID')=='')
		{
			$Daten = '~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'];
		}
		else
		{
			$Daten = $rsXFT->FeldInhalt('XFT_BUL_ID').'~'.$rsXFT->FeldInhalt('BUL_BUNDESLAND');
		}
		$Form->Erstelle_SelectFeld('XFT_BUL_ID',$rsXFT->FeldInhalt('XFT_BUL_ID'),'500:370',$EditRecht,'***BUL_Daten;txtXFT_BUL_ID;LAN_CODE=*txtXFT_LAN_CODE&ZUSATZ='.'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],$Daten,'','','');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_FIL_ID'].':',200,'','');
		$Form->Erstelle_TextFeld('XFT_FIL_ID',$rsXFT->FeldInhalt('XFT_FIL_ID'),5,300,$EditRecht,'','','','T','','','',4);
		$Form->ZeileEnde();
/*
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XFT']['XFT_FILAUSNAHME'].':',200,'','');
		$Form->Erstelle_Textarea('XFT_FILAUSNAHME',$rsXFT->FeldInhalt('XFT_FILAUSNAHME'),800,100,2,$EditRecht);
		$Form->ZeileEnde();
*/

		$Form->Formular_Ende();

	}

	//awis_Debug(1, $Param, $Bedingung, $rsXFT, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../stammdaten_Main.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht22000&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht22000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if(($Recht22000&8) == 8 AND $DetailAnsicht)		// L�schen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}
/*
	if(($Recht22000&4) == 4  AND $DetailAnsicht)		// Export der Daten
	{
		$Form->Schaltflaeche('image', 'cmdExport', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export'], '');
	}
*/
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND XFT_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['XFT_BEZEICHNUNG']) AND $Param['XFT_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND UPPER(XFT_BEZEICHNUNG) ' . $DB->LIKEoderIST($Param['XFT_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND XFT_TAG >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND XFT_TAG <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}
	if(isset($Param['FIL_ID']) AND $Param['FIL_ID']!='')
	{
		$Bedingung .= ' AND XFT_FIL_ID = ' . $DB->FeldInhaltFormat('N0',$Param['FIL_ID']) . ' ';
	}
	return $Bedingung;
}
?>