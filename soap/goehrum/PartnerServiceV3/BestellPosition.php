<?php

class BestellPosition extends Serialize
{

    /**
     * @var int $PositionsNr
     */
    protected $PositionsNr = null;

    /**
     * @var int $EinspeiserNr
     */
    protected $EinspeiserNr = null;

    /**
     * @var string $EinspeiserArtikelNr
     */
    protected $EinspeiserArtikelNr = null;

    /**
     * @var float $BestellMenge
     */
    protected $BestellMenge = null;

    /**
     * @param int $PositionsNr
     * @param int $EinspeiserNr
     * @param float $BestellMenge
     */
    public function __construct($PositionsNr, $EinspeiserNr, $EinspeiserArtikelnummer, $BestellMenge)
    {
      $this->PositionsNr = $PositionsNr;
      $this->EinspeiserNr = $EinspeiserNr;
      $this->BestellMenge = $BestellMenge;
      $this->EinspeiserArtikelNr = $EinspeiserArtikelnummer;
    }

    /**
     * @return int
     */
    public function getPositionsNr()
    {
      return $this->PositionsNr;
    }

    /**
     * @param int $PositionsNr
     * @return BestellPosition
     */
    public function setPositionsNr($PositionsNr)
    {
      $this->PositionsNr = $PositionsNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getEinspeiserNr()
    {
      return $this->EinspeiserNr;
    }

    /**
     * @param int $EinspeiserNr
     * @return BestellPosition
     */
    public function setEinspeiserNr($EinspeiserNr)
    {
      $this->EinspeiserNr = $EinspeiserNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getEinspeiserArtikelNr()
    {
      return $this->EinspeiserArtikelNr;
    }

    /**
     * @param string $EinspeiserArtikelNr
     * @return BestellPosition
     */
    public function setEinspeiserArtikelNr($EinspeiserArtikelNr)
    {
      $this->EinspeiserArtikelNr = $EinspeiserArtikelNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getBestellMenge()
    {
      return $this->BestellMenge;
    }

    /**
     * @param float $BestellMenge
     * @return BestellPosition
     */
    public function setBestellMenge($BestellMenge)
    {
      $this->BestellMenge = $BestellMenge;
      return $this;
    }

}
