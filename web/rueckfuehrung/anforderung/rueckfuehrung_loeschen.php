<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;
global $LAND;

try
{

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';
//$LAND='';
$RFA_KEY='';
$RFP_KEY='';
$RFM_KEY='';
$LOESCHART='';

$Form->DebugAusgabe(1,$_REQUEST);

if(isset($_GET['RF_TYP']))
{
	$RUECKFUEHRUNGSTYP=$_GET['RF_TYP'];
}

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Bereich
{
	$Tabelle = 'RFA';
	$Key=$_POST['txtRFA_KEY'];
	$RFA_KEY=$_POST['txtRFA_KEY'];

	$Felder=array();

}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Positionen':
			if(isset($_GET['RFM_KEY']))
			{
				$Tabelle = 'RFM';
				$Key=$_GET['RFM_KEY'];
		
				$SQL = 'SELECT RFP_AST_ATUNR, RFM_FIL_ID, RFM_MENGE, RFP_KEY, RFP_RFA_KEY, RFM_KEY';
				$SQL .= ' FROM Rueckfuehrungsanforderungenpos join Rueckfuehrungsanfposfilmengen on (rfp_key=rfm_rfp_key)';
				$SQL .=' WHERE RFP_KEY=0'.$_GET['Del'];
				$SQL .=' AND RFM_KEY=0'.$Key;
				
				$rsDaten = $DB->RecordSetOeffnen($SQL);
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RFP','RFP_AST_ATUNR'),$rsDaten->FeldInhalt('RFP_AST_ATUNR'));
				$Felder[]=array($Form->LadeTextBaustein('RFM','RFM_FIL_ID'),$rsDaten->FeldInhalt('RFM_FIL_ID'));
				$Felder[]=array($Form->LadeTextBaustein('RFM','RFM_MENGE'),$rsDaten->FeldInhalt('RFM_MENGE'));
				$RFA_KEY = $rsDaten->FeldInhalt('RFP_RFA_KEY');
				$RFP_KEY = $rsDaten->FeldInhalt('RFP_KEY');
				$RFM_KEY = $rsDaten->FeldInhalt('RFM_KEY');
			}
			else 
			{
				$Tabelle = 'RFP';
				$Key=$_GET['Del'];
		
				$SQL = 'SELECT RFP_AST_ATUNR, RFP_GESAMTMENGE, RFP_MINDESTBESTAND, RFP_KEY, RFP_RFA_KEY';
				$SQL .= ' FROM Rueckfuehrungsanforderungenpos';
				$SQL .=' WHERE RFP_KEY=0'.$Key;
				
				$rsDaten = $DB->RecordSetOeffnen($SQL);
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RFP','RFP_AST_ATUNR'),$rsDaten->FeldInhalt('RFP_AST_ATUNR'));
				$RFA_KEY = $rsDaten->FeldInhalt('RFP_RFA_KEY');
			}
			break;			
		case 'Filialen':
			$Tabelle = 'RFL';
			if(isset($_GET['RFL_KEY']))
			{
				$Key=$_GET['Del'];			
				
				$SQL = 'SELECT RFL_FIL_ID, RFL_KEY, RFL_RFA_KEY';
				$SQL .= ' FROM Rueckfuehrungsanffilialen';
				$SQL .=' WHERE RFL_KEY=0'.$Key;
				$Form->DebugAusgabe(1,$SQL);
				//die();
				$rsDaten = $DB->RecordSetOeffnen($SQL);
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RFL','RFL_FIL_ID'),$rsDaten->FeldInhalt('RFL_FIL_ID'));
				$RFA_KEY = $rsDaten->FeldInhalt('RFL_RFA_KEY');		
				$LAND=$_GET['LAND'];
				$LOESCHART='FILIALE';
			}
			else
			{
				$Key=$_GET['Del'];			

				$SQL = 'SELECT RFL_FIL_ID, RFL_KEY, RFL_RFA_KEY';
				$SQL .= ' FROM Rueckfuehrungsanffilialen';
				$SQL .=' WHERE RFL_RFA_KEY=0'.$Key;
				
				$rsDaten = $DB->RecordSetOeffnen($SQL);
				$Felder=array();
				//$Felder[]=array($Form->LadeTextBaustein('RFL','RFL_FIL_ID'),$rsDaten->FeldInhalt('RFL_FIL_ID'));
				$Felder[]=array($Form->LadeTextBaustein('RFL','RFL_FIL_ID'),$_GET['LAND']);
				$RFA_KEY = $rsDaten->FeldInhalt('RFL_RFA_KEY');								
				$LOESCHART='LAND';
				$LAND=$_GET['LAND'];
			}
			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'RFA':
			$SQL = 'DELETE FROM rueckfuehrungsanforderungen WHERE RFA_KEY=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;
		case 'RFP':
			$SQL = 'DELETE FROM rueckfuehrungsanforderungenpos WHERE RFP_KEY=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtRFA_KEY'];
			break;
		case 'RFM':
			$SQL = 'DELETE FROM rueckfuehrungsanfposfilmengen WHERE RFM_KEY=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtRFA_KEY'];
			$AWIS_KEY2=$_POST['txtRFP_KEY'];			
			break;
		case 'RFL':
			$Form->DebugAusgabe(1,$LAND,'HALLO');
			
			if ($_POST['txtLOESCHART']=='LAND')
			{
				$SQL = 'DELETE FROM rueckfuehrungsanffilialen';
				$SQL.= ' WHERE RFL_FIL_ID in (select fil_id from filialen where fil_lan_wwskenn=\''.$_POST['txtLAND'].'\')';
				$SQL.= ' AND RFL_RFA_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtRFA_KEY'];				
			}
			else 
			{
				$SQL = 'DELETE FROM rueckfuehrungsanffilialen WHERE RFL_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtRFA_KEY'];				
				$LAND=$_POST['txtLAND'];
			}

			break;	
		default:
			break;
	}

	if($SQL !='')
	{
		if($DB->Ausfuehren($SQL)===false)
		{
			throw new awisException('Fehler beim Löschen',200809181402,$SQL,2);
		}

		if($AWIS_KEY1!='0')
		{
			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';
			
			$Form->DebugAusgabe(1,$SQL);
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200810141434,$SQL,2);
			}
		}
		
		If($_POST['txtRueckfuehrungstyp']=='2' && $_POST['txtRFM_KEY']!='')
		{
			$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS SET RFP_GESAMTMENGE=(SELECT SUM(RFM_MENGE) FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_RFP_KEY='.$_POST['txtRFP_KEY'].') WHERE RFP_KEY='.$_POST['txtRFP_KEY'];
					
			$Form->DebugAusgabe(1,$SQL);
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809241402,$SQL,2);
			}
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Form->SchreibeHTMLCode('<form name="frmLoeschen" action="./rueckfuehrung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'" method="POST">');
	$Form->SchreibeHTMLCode('<span class="HinweisText">'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>');

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': <b>'.$Feld[1].'</b>';
	}

	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
	$Form->Erstelle_HiddenFeld('Key',$Key);
	$Form->Erstelle_HiddenFeld('RFA_KEY',$RFA_KEY);
	$Form->Erstelle_HiddenFeld('RFP_KEY',$RFP_KEY);
	$Form->Erstelle_HiddenFeld('RFM_KEY',$RFM_KEY);
	$Form->Erstelle_HiddenFeld('LAND',$LAND);
	$Form->Erstelle_HiddenFeld('LOESCHART',$LOESCHART);
	$Form->Erstelle_HiddenFeld('Rueckfuehrungstyp',$RUECKFUEHRUNGSTYP);

	$Form->SchreibeHTMLCode('<br><input type="submit" name="cmdLoeschenOK" value="'.$TXT_AdrLoeschen['Wort']['Ja'].'">');
	$Form->SchreibeHTMLCode('&nbsp;<input type="submit" name="cmdLoeschenAbbrechen" value="'.$TXT_AdrLoeschen['Wort']['Nein'].'">');

	$Form->SchreibeHTMLCode('</form>');
	die();
}

}
catch (awisException $ex)
{
	$Form->DebugAusgabe(1,$ex->getSQL());
	$Form->Fehler_Anzeigen('LöschenFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
catch (Exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getSQL());
	$Form->Fehler_Anzeigen('LöschenFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>