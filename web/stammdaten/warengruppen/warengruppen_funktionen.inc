<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class warengruppen_funktionen
{
    private $_Form;
    private $_DB;
    private $_Benutzer;
    private $_Recht621;
    private $_AWISSprachKonserven;


    function __construct()
    {
        $this->_Benutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
        $this->_Recht621 = $this->_Benutzer->HatDasRecht(621);

        $TextKonserven[] = array('WGI', '%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven, $this->_Benutzer->BenutzerSprache());
    }

    public function erstelleInfos($Typ = '')
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./warengruppen_Main.php?cmdAktion=Details" . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Ebene3'])?'&Ebene3=' . $_GET['Ebene3']:'') . ($AWIS_KEY1 != ''?'&WGR_ID=' . $AWIS_KEY1:'') . ($AWIS_KEY2 != ''?'&WUG_ID=' . $AWIS_KEY2:'') . "&WUGListe=True accesskey=T title='" . $this->_AWISSprachKonserven ['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
        $this->_Form->InfoZeile($Felder, '');

        //Wenn in Zukunft noch mehr Infos angezeigt werden m�ssen, macht diese Funktion auch Sinn,
        //da dann abgefragt werden kann, f�r welchen Typ (WGR o. WUG) diese Info z�hlt
        $this->_erstelleLieferantenreihenfolge();
    }


    private function _erstelleLieferantenreihenfolge()
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;

        if (isset($_GET['WGI_LIE_NEU'])) {

            ob_start();

            $this->_Form->AuswahlBoxHuelle('TecDocHinweis', '', '', ob_get_clean());

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['WGI']['WGI_LIE_NR'] . ':', 120, '', '');
            $SQL = 'SELECT LIE_NR, LIE_NR || \' - \' || LIE_NAME1 AS Anzeige FROM Lieferanten ORDER BY LIE_NAME1';
            $this->_Form->Erstelle_AutocompleteFeld('LAR_LIE_NR', '', 90, true, 'autocomplete_LIE_NR', 3, '', '', 'Test', '', 'ListenFeldDunkel');

            $this->_Form->AuswahlBox('LIE_NR_TECDOC', 'TecDocHinweis', '', 'TecDocHinweis', 'txtLAR_LIE_NR', 100, 10, '', 'T', true, '', '', 'display: none', '', '', '', '', 0, '',
                '');

            $this->_Form->ZeileEnde();

            return;
        }

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['WGI']['WGI_LIEFERANTENREIHENFOLGE'] . ': ', 100, 'font-weight:bold');
        $this->_Form->ZeileEnde();

        $SQL = 'SELECT WGI_WGR_ID,';
        $SQL .= '   WGI_WUG_ID,';
        $SQL .= '   WGI_LIE_NR,';
        $SQL .= '   WGI_LIE_NR_WERTIGKEIT,';
        $SQL .= '   WGI_USER,';
        $SQL .= '   WGI_USERDAT,';
        $SQL .= '   WGI_TECDOC,';
        $SQL .= '   WGI_LIE_NAME,';
        $SQL .= '   WGI_KEY_LIE_NR,';
        $SQL .= '   WGI_KEY_WERTIGKEIT';
        $SQL .= ' FROM V_WARENGRUPPEN_LIE_NR ';
        $SQL .= ' where WGI_WGR_ID = ' . $this->_DB->WertSetzen('INF', 'T', $AWIS_KEY1);
        if ($AWIS_KEY2 > 0) {
            $SQL .= ' and WGI_WUG_ID = ' . $this->_DB->WertSetzen('INF', 'T', $AWIS_KEY2);
        }else{
            $SQL .= ' and WGI_WUG_ID is null ';
        }

        $SQL .= ' ORDER BY WGI_LIE_NR_WERTIGKEIT asc';

        $rsLief = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('INF'));
        $this->_Form->DebugAusgabe(1, $this->_DB->LetzterSQL());

        $SpaltenBreiten = array();
        $SpaltenBreiten['WGI_LIE_NEU'] = 20;
        $SpaltenBreiten['WGI_LIE_NR'] = 70;
        $SpaltenBreiten['WGI_TECDOC'] = 100;
        $SpaltenBreiten['WGI_LIE_NAME'] = 300;
        $SpaltenBreiten['DEL'] = 16;

        $this->_Form->ZeileStart();

        $EditSortierliste = (($this->_Recht621 & 128) == 128);

        $Icons = array();
        if (($this->_Recht621 & 32) == 32) { //Neuanlage?
            $Icons[] = array(
                'new',
                './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Ebene3'])?'&Ebene3=' . $_GET['Ebene3']:'') . (isset($_GET['WGR_ID'])?'&WGR_ID=' . $_GET['WGR_ID']:'') . (isset($_GET['WUG_ID'])?'&WUG_ID=' . $_GET['WUG_ID']:'') . '&WGI_LIE_NEU'
            );
        }
        $this->_Form->Erstelle_ListeIcons($Icons, $SpaltenBreiten['WGI_LIE_NEU'], -2, $this->_AWISSprachKonserven['WGI']['WGI_LIE_NEU']);

        //Sortierliste hat vorne ein Icon zum Soriteren, In der �berschrift muss an dieser Stelle ein Platzhalter sein
        if ($EditSortierliste) {
            $this->_Form->Erstelle_Liste_Ueberschrift('', $SpaltenBreiten['WGI_LIE_NEU']);
        }

        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['WGI']['WGI_LIE_NR'], $SpaltenBreiten['WGI_LIE_NR']);
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['WGI']['WGI_TECDOC'], $SpaltenBreiten['WGI_TECDOC']);
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['WGI']['WGI_LIE_NAME'], $SpaltenBreiten['WGI_LIE_NAME']);

        $this->_Form->ZeileEnde();

        $this->_Form->SortierListeStart('LIE_NR', '', array(), $EditSortierliste);

        $DS = 0;
        while (!$rsLief->EOF()) {
            $HG = ($DS++ % 2);
            $this->_Form->SortierListenZeileStart($HG, $SpaltenBreiten['WGI_LIE_NEU'], $rsLief->FeldInhalt('WGI_KEY_WERTIGKEIT'), $EditSortierliste);

            $Icons = array();
            if (($this->_Recht621 & 32) == 32) {
                $Icons[] = array(
                    'delete',
                    './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Ebene3'])?'&Ebene3=' . $_GET['Ebene3']:'') . ($AWIS_KEY1 != ''?'&WGR_ID=' . $AWIS_KEY1:'') . ($AWIS_KEY2 != ''?'&WUG_ID=' . $AWIS_KEY2:'') . '&Del&WGI_KEY_LIE_NR=' . $rsLief->FeldInhalt('WGI_KEY_LIE_NR')
                );
            }

            $this->_Form->Erstelle_ListeIcons($Icons, $SpaltenBreiten['WGI_LIE_NEU'], $HG, $this->_AWISSprachKonserven['WGI']['WGI_LIE_NEU']);

            $this->_Form->Erstelle_ListenFeld('WGI_LIE_NR', $rsLief->FeldInhalt('WGI_LIE_NR'), $SpaltenBreiten['WGI_LIE_NR'] / 10, $SpaltenBreiten['WGI_LIE_NR'], false, $HG);
            $this->_Form->Erstelle_ListenFeld('WGI_TECDOC', $rsLief->FeldInhalt('WGI_TECDOC'), $SpaltenBreiten['WGI_TECDOC'] / 10, $SpaltenBreiten['WGI_TECDOC'], false, $HG);
            $this->_Form->Erstelle_ListenFeld('WGI_LIE_NAME', $rsLief->FeldInhalt('WGI_LIE_NAME'), $SpaltenBreiten['WGI_LIE_NAME'] / 10, $SpaltenBreiten['WGI_LIE_NAME'], false,
                $HG);

            $this->_Form->SortierListenZeileEnde();
            $rsLief->DSWeiter();
        }

        $this->_Form->SortierListeEnde();
    }

    public function erstelleWGRBedingung($Param = array())
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;
        global $DB;

        $Bedingung = '';

        if ($AWIS_KEY1 != '') {
            $Bedingung .= ' AND WGR_ID = ' . $DB->WertSetzen('WGR', 'T', $AWIS_KEY1);
            $Param['WHERE'] = $Bedingung;

            return $Bedingung;
        }

        $Param['WHERE'] = $Bedingung;

        return $Bedingung;
    }

    public function erstelleWUGBedingung($Param = array())
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;
        global $DB;

        $Bedingung = '';

        if ($AWIS_KEY1 != '') {
            $Bedingung .= ' AND WUG_WGR_ID = ' . $DB->WertSetzen('WUG', 'T', $AWIS_KEY1);
            $Param['WHEREWUG'] = $Bedingung;
        }

        if ($AWIS_KEY2 != 0) {
            $Bedingung .= ' AND WUG_ID = ' . $DB->WertSetzen('WUG', 'T', $AWIS_KEY2);
            $Param['WHEREWUG'] = $Bedingung;

            return $Bedingung;
        }

        if (isset($Param['WUG_ID']) and $Param['WUG_ID'] != '') {
            $Bedingung .= ' AND WUG_ID = ' . $DB->WertSetzen('WUG', 'T', $Param['WUG_ID']);
            $Param['WHEREWUG'] = $Bedingung;

            return $Bedingung;
        }

        $Param['WHEREWUG'] = $Bedingung;

        return $Bedingung;
    }


    public function insertWGI($WGT_ID, $WGI_WERT, $WGI_WGI_KEY = '')
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;
        global $DB;

        $SQL = 'INSERT';
        $SQL .= ' INTO WARENGRUPPENINFOS';
        $SQL .= '   (';
        $SQL .= '     WGI_WGR_ID,';
        $SQL .= '     WGI_WUG_ID,';
        $SQL .= '     WGI_WGT_ID,';
        $SQL .= '     WGI_WERT,';
        $SQL .= '     WGI_USER,';
        $SQL .= '     WGI_USERDAT,';
        $SQL .= '     WGI_WGI_KEY';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= ' ' . $DB->WertSetzen('WGI', 'T', $AWIS_KEY1);
        $SQL .= ', ' . $DB->WertSetzen('WGI', 'T', $AWIS_KEY2);
        $SQL .= ', ' . $DB->WertSetzen('WGI', 'T', $WGT_ID);
        $SQL .= ', ' . $DB->WertSetzen('WGI', 'T', $WGI_WERT);
        $SQL .= ', ' . $DB->WertSetzen('WGI', 'T', $this->_Benutzer->BenutzerName());
        $SQL .= ',sysdate ';
        $SQL .= ', ' . $DB->WertSetzen('WGI', 'T', $WGI_WGI_KEY);
        $SQL .= '   )';

        try {

            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('WGI'));

            $WGI_KEY = 'select seq_wgi_key.currval from dual';

            return $DB->RecordSetOeffnen($WGI_KEY)->FeldInhalt(1);
        } catch (awisException $e) {
            $this->_Form->Fehler_Anzeigen('INTERN', $e->getMessage(), 'MELDEN', -5, '201702011331');

            return false;
        }
    }

    public function updateWGI($WGI_KEY, $WGT_WERT)
    {
        global $DB;

        $SQL = 'UPDATE WARENGRUPPENINFOS';
        $SQL .= ' SET';
        $SQL .= '  WGI_WERT    = ' . $DB->WertSetzen('WGI', 'T', $WGT_WERT);
        $SQL .= ' , WGI_USER    = ' . $DB->WertSetzen('WGI', 'T', $this->_Benutzer->BenutzerName());
        $SQL .= ' , WGI_USERDAT = sysdate';
        $SQL .= ' WHERE WGI_KEY   = ' . $DB->WertSetzen('WGI', 'T', $WGI_KEY);

        try {
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('WGI'));

            return true;
        } catch (awisException $e) {

            $this->_Form->Fehler_Anzeigen('INTERN', $e->getMessage(), 'MELDEN', -5, '201702011334');

            return false;
        }
    }

}