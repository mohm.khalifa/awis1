--Erstellung der Tabelle, der Trigger, etc.

CREATE TABLE SEMINARHOTEL
(
  SEH_KEY NUMBER NOT NULL
, SEH_SET_KEY NUMBER NOT NULL
, SEH_LAN_CODE VARCHAR2(5)
, SEH_NAME VARCHAR2(100)
, SEH_ORT VARCHAR2(100)
, SEH_PLZ VARCHAR2(10)
, SEH_STRASSE VARCHAR2(100)
, SEH_HAUSNR VARCHAR2(10)
, SEH_USER VARCHAR2(20)
, SEH_USERDAT DATE DEFAULT SYSDATE
, CONSTRAINT PK_SEH_KEY PRIMARY KEY
  (
    SEH_KEY
  )
  ENABLE
);
CREATE SEQUENCE  "AWIS"."SEQ_SEH_KEY"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;

ALTER TABLE SEMINARHOTEL
ADD CONSTRAINT REF_SEH_LAN_CODE FOREIGN KEY
(
  SEH_LAN_CODE
)
REFERENCES LAENDER
(
  LAN_CODE
)
ON DELETE SET NULL ENABLE;

ALTER TABLE SEMINARHOTEL
ADD CONSTRAINT REF_SEH_SET_KEY FOREIGN KEY
(
  SEH_SET_KEY
)
REFERENCES SEMINARTEILNEHMER
(
  SET_KEY
)
ON DELETE CASCADE ENABLE;

COMMENT ON COLUMN SEMINARHOTEL.SEH_KEY IS 'Einmaliger KEY zur eindeutigen Selektierung';

COMMENT ON COLUMN SEMINARHOTEL.SEH_SET_KEY IS 'Jeweiliger KEY zu dem Teilnehmer aus der Seminarteilnehmer-Tabelle';

COMMENT ON COLUMN SEMINARHOTEL.SEH_LAN_CODE IS 'Jeweilges Laender Code zu dem Land des Hotels aus der LAENDER-Tabelle';

COMMENT ON COLUMN SEMINARHOTEL.SEH_NAME IS 'Name des Hotels';

COMMENT ON COLUMN SEMINARHOTEL.SEH_ORT IS 'Ort des Hotels';

COMMENT ON COLUMN SEMINARHOTEL.SEH_PLZ IS 'Postleitzahl des Ortes';

COMMENT ON COLUMN SEMINARHOTEL.SEH_STRASSE IS 'Strasse des Hotels';

COMMENT ON COLUMN SEMINARHOTEL.SEH_HAUSNR IS 'Hausnummer des Hotels';

COMMENT ON COLUMN SEMINARHOTEL.SEH_USER IS 'Letzter Bearbeiter des Datensatzes';

COMMENT ON COLUMN SEMINARHOTEL.SEH_USERDAT IS 'Letzter Bearbeitungszeitpunkt';

CREATE OR REPLACE TRIGGER TRG_SEQ_SEH_KEY
BEFORE INSERT ON SEMINARHOTEL
FOR EACH ROW
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.SEH_KEY IS NULL THEN
      SELECT SEQ_SEH_KEY.NEXTVAL INTO :NEW.SEH_KEY FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

--Rechtestufen

INSERT INTO RECHTESTUFEN (XRS_XRC_ID,XRS_BIT,XRS_BESCHREIBUNG) VALUES ('4200','18','Seminarteilnehmer Hotel anzeigen');
INSERT INTO RECHTESTUFEN (XRS_XRC_ID,XRS_BIT,XRS_BESCHREIBUNG) VALUES ('4200','19','Seminarteilnehmer Hotel hinterlegen');

--Tabellenk�rzel

INSERT INTO TABELLENKUERZEL (XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE, XTN_VERSION) VALUES ('SEH','SEMINARHOTEL','ORACLE',1);

--Textkonserven
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SET_H','SET','K�rzel f�r die Spalte der Hotels der Teilnehmer','H');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('ttt_H_DATENSAETZE','SET','TTT f�r die Datensaetze der Spalte H bei den Teilnehmern','Hotelinformationen');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_HOTELNAME','SEH','TextLabel f�r den Hotelnamen','Hotelname:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_LAND','SEH','TextLabel f�r das Land','Land:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_STRASSE','SEH','TextLabel f�r die Sta�e','Stra�e:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_HAUSNUMMER','SEH','TextLabel f�r die Hausnummer','Hausnummer:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_ORT','SEH','TextLabel f�r den Ort','Ort:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_PLZ','SEH','TextLabel f�r die Postleitzahl','Postleitzahl:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_','SEH','','');

SET DEFINE OFF;
INSERT INTO MAILVERSANDTEXTE (MVT_BEREICH, MVT_SPRACHE, MVT_BETREFF,MVT_TEXT,MVT_GUELTIGAB,MVT_USER,MVT_USERDAT) values ('SET_HOTEL_BESTAETIGUNG','DE','Hotelbest�tigung','Sehr geehrte Damen und Herren,

anbei erhalten Sie die Reservierungsbest�tigung des Hotels zu Ihrem Seminar #SET_NAME# vom #SET_VON# bis #SET_BIS# #SET_ORT#. Wir haben Ihnen gem�� Ihrer R�ckmeldung ein Zimmer mit Fr�hst�ck reserviert.

Wir bitten Sie diese Daten zu pr�fen. #SET_LINK#

Sollte eine �nderung des Zeitraums notwendig sein oder Sie nicht am Seminar teilnehmen, bitten wir Sie dies direkt �ber AWIS - Seminarplan zu best�tigen oder nochmals anzupassen.

Sonntagsanreisen, die kurzfristig nicht angetreten werden k�nnen, bitten wir selbstst�ndig im Hotel abzusagen um hier �No-Show-Geb�hren� zu vermeiden.

F�r Fragen stehen wir Ihnen gerne zur Verf�gung.
','01.01.1970','ADMIN',SYSDATE);

--Textkonserven zu den Mails
INSERT INTO PROGRAMMPARAMETER (XPP_ID,XPP_BEZEICHNUNG,XPP_DEFAULT,XPP_UEBERSCHREIBBAR,XPP_BEMERKUNG,XPP_KODIERT,XPP_DATENTYP) VALUES (4220,'SEHABSENDER','academy.hotel@de.atu.eu',255,'MailAbsender f�r die Hotelreservierungsmails',0,'T');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('mail_OrtWort', 'SEH','Wort, dass nur bei vorhandenen Ort angezeigt wird.','in');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_INFOHINTERLEGEN','SEH','Information hinterlegen f�r','Information hinterlegen f�r: ');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ( 'SEH_UNDFUER','SEH','Und f�r','Und f�r: ');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ( 'SEH_HOTELDATEN','SEH','Hoteldaten','Hoteldaten: ');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ( 'SEH_GOOGLEMAPS','SEH','In Google-Maps �ffnen','In Google-Maps �ffnen');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ( 'SEH_NEUEDATHOCH','SEH','Neue Datei hochladen','Neue Datei hochladen');
SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_HINWEIS_KEINHOTEL','SEH','Kein Hotel ben�tigt','<b>Hinweis: </b>Der Teilnehmer gab bei der Anmeldung an, dass kein Hotel ben�tigt wird.');
SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('SEH_HINWEIS_KEINEBESTAETIGUNG','SEH','Teilnehmer hat Einladung nicht angenommen','<b>Hinweis: </b>Der Teilnehmer hat die Einladung nicht angenommen. ');
SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG,XTX_TEXT_DE) values ('mail_Link','SEH','Der Link zur Hotelreservierungsseite','#SEH_DOMAIN#/seminarplan/seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&HOTEL_SET_KEY=#SEH_SET_KEY#');

INSERT INTO "AWIS"."PROGRAMMPARAMETER" (XPP_ID, XPP_BEZEICHNUNG, XPP_DEFAULT, XPP_UEBERSCHREIBBAR, XPP_BEMERKUNG, XPP_KODIERT, XPP_DATENTYP) VALUES ('4221', 'SEHAbsender', 'academy.hotel@de.atu.eu', '255', 'MailAbsender f�r die Hotelreservierungsmails', '0', 'T');