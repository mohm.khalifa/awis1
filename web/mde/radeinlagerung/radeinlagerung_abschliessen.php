<?php
global $AWIS_KEY1;
global $FIL_ID;
global $LFDNR;


$TextKonserven=array();
$TextKonserven[]=array('REL','REL_%');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Abbrechen');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','Filialbestand');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FehlerAbschluss');
$TextKonserven[]=array('Fehler','err_EingabeGroesserNull');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('REL','HinweisAbgeschlossen');
$TextKonserven[]=array('REL','WirklichAbschliessen');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$TXT_Abschliessen = $Form->LadeTexte($TextKonserven);

$Art= '';
$Hinweis = '';
$Form->DebugAusgabe(1,$_POST);
if(isset($_POST['cmd_Abschliessen_x']))
{			
	$FIL_ID = $_POST['txtREL_FIL_ID'];
	$LFDNR = $_POST['txtREL_LFDNR'];
			
	$Art = 'Filiale';
	
	//Pr�fen, ob die Lieferung evtl. schon abgeschlossen ist
	$SQL = 'SELECT min(rel_status) as STATUS';	
	$SQL .=' FROM Raedereinlagerungen REL';
	$SQL .=' WHERE REL_FIL_ID = 0'.intval($FIL_ID);	
	$SQL .=' AND REL_LFDNR = 0'.intval($LFDNR);	
	$Form->DebugAusgabe(1,$SQL);		
		
	$rsREL = $DB->RecordSetOeffnen($SQL);
	
	if ($rsREL->FeldInhalt('STATUS')>=10)
	{										
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['REL']['HinweisAbgeschlossen']);
		$Form->ZeileEnde();
		
		$Link='./radeinlagerung_Main.php?cmdAktion=Details&REL_FIL_ID='.$FIL_ID.'&REL_LFDNR='.$LFDNR;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdAbbrechen',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();				
		die();	
	}			
	
}
elseif(isset($_POST['cmdAbschliessenOK']))	// Abschluss durchf�hren
{	
	//$AWIS_KEY1 = $_POST['txtKey'];
	
	$SQL = '';
	switch ($_POST['txtArt'])
	{
		case 'Filiale':	
			
			//Kopf auf Abgeschlossen setzen
			$SQL = 'UPDATE Raedereinlagerungen';
			$SQL .= ' SET REL_Status=12';
			$SQL .= ', REL_user=\''.$AWISBenutzer->BenutzerName().'\'';			
			$SQL .= ' , REL_UserDat=SYSDATE';
			$SQL .= ' , REL_Datumabschluss=SYSDATE';
			$SQL .= ' WHERE REL_FIL_ID=0'.intval($_POST['txtFIL_ID']);			
			$SQL .= ' AND REL_LFDNR=0'.intval($_POST['txtLFDNR']);			
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('radeinlagerung_abschliessen_1',1,$awisDBError['messages'],'');
			}	
											
	}	
}

if($Art!='')
{
	$Form->SchreibeHTMLCode('<form name=frmAbschliessen action=./radeinlagerung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Hinweistext($TXT_Abschliessen['REL']['WirklichAbschliessen']);
	$Form->ZeileEnde();			
	
	if($Hinweis!='')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($Hinweis);
		$Form->ZeileEnde();				
	}	
	
	//$Form->Erstelle_HiddenFeld('LKK_Key',$LKKKey);
	$Form->Erstelle_HiddenFeld('LFDNR',$_POST['txtREL_LFDNR']);
	$Form->Erstelle_HiddenFeld('FIL_ID',$_POST['txtREL_FIL_ID']);	
	$Form->Erstelle_HiddenFeld('Art',$Art);
	//$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdAbschliessenOK','','',$TXT_Abschliessen['Wort']['Ja'],'','');
	$Form->Schaltflaeche('submit','cmdAbschliessenAbbrechen','','',$TXT_Abschliessen['Wort']['Nein'],'');	
	$Form->ZeileEnde();
	
	
	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>