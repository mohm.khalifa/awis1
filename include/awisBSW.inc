<?php

require_once 'awisMailer.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('awisJobs.inc');

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 27.09.2016
 * Time: 15:23
 */
class awisBSW
{
    //F�r Import + Export
    private $_AWISBenutzer;
    private $_DB;
    private $_ProtokollDB;
    private $_DebugLevel = 99;
    public $MD5Check = true;
    private $_EMAIL_Infos = array('shuttle@de.atu.eu');
    private $_DebugText='';
    private $_awisJobs;

    //F�r Import
    const ImpPfad = '/win/data_out/BSW/';
    const ImpSeperator = ';';
    const ImpPfadArchiv = '/daten/daten/werkstattauftraege/BSW/ImportOnlineshop/';

    //F�r Export
    const ExpPfad = '/daten/daten/werkstattauftraege/BSW/Exportdaten/';
    const XJO_KEY = 90; // Key f�r den BSW-ExportJob
    private $_EXPZeitraumVon = '';
    private $_EXPZeitraumBis = '';


    /**
     * @var awisRecordset
     */
    private $_rsADE;

    function __construct($Benutzer)
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        //Eigene Datenbankverbindung f�r das Protokoll, da der Import in einer eigenen Transaktion l�uft
        $this->_ProtokollDB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_ProtokollDB->Oeffnen();
        $this->_awisJobs = new awisJobs();
    }

    function __destruct()
    {
        echo $this->_DebugText;
    }

    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .=  ': ' . $Typ;
            $this->_DebugText .=  ': ' . $Text;
            $this->_DebugText .=  PHP_EOL;
        }
    }


    /**
     * Importiert die Daten aus dem Onlineshop
     * @Attention AX Daten kommen �ber den Filsysextrakt
     *
     * @return bool
     */
    public function Import()
    {
        $Werkzeug = new awisWerkzeuge();

        $Werkzeug->EMail($this->_EMAIL_Infos, 'OK BSWImport BEGINNT', '', 1, '', 'awis@de.atu.eu');
        $Protokoll['DateiANZ'] = 0;
        $Protokoll['DSANZ'] = 0;
        $Commit = true;
        $this->_DB->TransaktionBegin();

        $BSWDateien = scandir(self::ImpPfad); //Ordner "files" auslesen

        foreach ($BSWDateien as $Datei) { // Ausgabeschleife
            if(substr($Datei,0,6)=='BSWONL'){
                $Protokoll['DateiANZ'] ++;
                //Pr�fsumme der Datei pr�fen
                if($this->_PruefeImportProtokoll(self::ImpPfad . $Datei)){
                    $XDI_KEY = $this->_SchreibeImportProtokoll(self::ImpPfad . $Datei);
                    //Datei �ffnen
                    $fd = fopen(self::ImpPfad . $Datei, 'r');
                    //Zeilenweise durchgehen
                    while (!feof($fd)) {
                        $Zeile = fgets($fd);
                        if($Zeile==''){ //Leerzeilen igonieren
                            continue;
                        }
                        //F�r den einfacheren Umgang, in ein Array stecken
                        $Zeile = explode(self::ImpSeperator,$Zeile);
                        if(count($Zeile)==9){ //9 Spalten m�ssen vorhanden sein
                            $SQL  ='INSERT';
                            $SQL .=' INTO BSW';
                            $SQL .='   (';
                            $SQL .='     BSW_FIL_ID,';
                            $SQL .='     BSW_DATUM,';
                            $SQL .='     BSW_ZEIT,';
                            $SQL .='     BSW_BSA,';
                            $SQL .='     BSW_BONNR,';
                            $SQL .='     BSW_KARTENNUMMER,';
                            $SQL .='     BSW_UMSATZ,';
                            $SQL .='     BSW_MWST,';
                            $SQL .='     BSW_IMQ_ID,';
                            $SQL .='     BSW_USER,';
                            $SQL .='     BSW_USERDAT,';
                            $SQL .='     BSW_XDI_KEY';
                            $SQL .='   )';
                            $SQL .='   VALUES';
                            $SQL .='   (';
                            $SQL .= $this->_DB->WertSetzen('BSW','T',substr($Zeile[0],1));
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','D',$Zeile[1]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','T',$Zeile[2]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','T',$Zeile[3]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','T',$Zeile[4]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','T',$Zeile[5]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','W',$Zeile[6]);
                            $SQL .= ', ' . $this->_DB->WertSetzen('BSW','W',$Zeile[7]);
                            $SQL .= ', 131072' ; //IMQ E-Commerce
                            $SQL .= ', \'AWIS\'';
                            $SQL .= ', sysdate';
                            $SQL .= ', '. $this->_DB->WertSetzen('BSW','W',$XDI_KEY);
                            $SQL .='   )';

                            try{
                                $Protokoll['DSANZ']++;
                                $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('BSW',true));
                            }catch (awisException $e){
                                $Commit = false;
                                $this->_handleErrors($e,implode(';',$Zeile));
                            }catch (Exception $e){
                                $this->_handleErrors($e,implode(';',$Zeile));
                                $Commit = false;
                            }
                        }else{
                            $this->debugAusgabe('Zeile hat nicht gen�gend spalten ',1,'FEHLER');
                            $this->debugAusgabe(implode(';',$Zeile),1,'FEHLER');
                            $Commit = false;
                        }
                    }
                }else{//Nicht commiten, da es die Datei Bereits gegeben hat!
                    $Commit = false;
                }
                rename(self::ImpPfad . $Datei, self::ImpPfadArchiv . date('Ymd') . '_' . $Datei);
            }
        };


        if($Protokoll['DateiANZ']==0){
            $this->debugAusgabe('Ich konnte keine Datei finden :( ',1,'Info');
            $Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR BSWImport', 'Keine Datei zum importieren vorhanden...', 2, '', 'awis@de.atu.eu');
        }else{

            if($Commit) {
                $this->_DB->TransaktionCommit();
                $this->debugAusgabe('Fertig und commitet! Anzahl Dateien ' . $Protokoll['DateiANZ'] . ' Anzahl Datens�tze ' . $Protokoll['DSANZ'], 1, 'Info');
                $Werkzeug->EMail($this->_EMAIL_Infos, 'OK BSWImport', $this->_DebugText, 1, '', 'awis@de.atu.eu');
            }else{
                $this->_DB->TransaktionRollback();
                $this->debugAusgabe('Fehler aufgetreten und rollbackt! Logdatei komplett durchlesen! Anzahl Dateien ' . $Protokoll['DateiANZ'] . ' Anzahl Datens�tze ' . $Protokoll['DSANZ'], 1, 'Info');
                $Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR BSWImport', 'RUNTER SCROLLEN!' . PHP_EOL .  $this->_DebugText, 1, '', 'awis@de.atu.eu');
            }
        }
    }


    private function _PruefeImportProtokoll($Dateiname){

        if($this->MD5Check==false){
            return true;
        }
        $SQL  ='SELECT XDI_KEY,';
        $SQL .='   XDI_BEREICH,';
        $SQL .='   XDI_DATEINAME,';
        $SQL .='   XDI_DATUM,';
        $SQL .='   XDI_MD5,';
        $SQL .='   XDI_BEMERKUNG,';
        $SQL .='   XDI_USER,';
        $SQL .='   XDI_USERDAT';
        $SQL .=' FROM IMPORTPROTOKOLL ';
        $SQL .= ' WHERE XDI_BEREICH = \'BSW\' ';
        $SQL .= ' AND XDI_MD5 = ' .$this->_DB->WertSetzen('XDI','T',sha1_file($Dateiname));

        $rsXDI = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XDI'));

        if($rsXDI->AnzahlDatensaetze()>0){
            $this->debugAusgabe('Laut Pr�fsumme, wurde die Datei ' . $Dateiname . ' bereits importiert. 
            XDI_KEY: ' . $rsXDI->FeldInhalt('XDI_KEY') . ' 
            XDI_DATEINAME: ' . $rsXDI->FeldInhalt('XDI_DATEINAME') . ' 
            XDI_DATUM: ' . $rsXDI->FeldInhalt('XDI_DATUM') . ' 
            Sollte es sich hierbei um einen Fehler handeln, Job erneut mit dem Parameter --ignore md5 aufrufen',1,'Fehler');
            return false;
        }else{
            $this->debugAusgabe('Pr�fsumme der Datei unbekannt. ',999,'Debug');
            return true;
        }
    }


    /**
     * Schreibt die MD5 ins ImportProtokoll
     * @param $Dateiname
     */
    private function _SchreibeImportProtokoll($Dateiname){
        $SQL  ='INSERT';
        $SQL .=' INTO IMPORTPROTOKOLL';
        $SQL .='   (';
        $SQL .='     XDI_BEREICH,';
        $SQL .='     XDI_DATEINAME,';
        $SQL .='     XDI_DATUM,';
        $SQL .='     XDI_MD5,';
        $SQL .='     XDI_USER,';
        $SQL .='     XDI_USERDAT';
        $SQL .='   )';
        $SQL .='   VALUES';
        $SQL .='   (';
        $SQL .='     \'BSW\'';
        $SQL .=',' . $this->_ProtokollDB->WertSetzen('XDI','T',$Dateiname);
        $SQL .=', sysdate ';
        $SQL .=',' . $this->_ProtokollDB->WertSetzen('XDI','T',sha1_file($Dateiname));
        $SQL .=', \'AWIS\'';
        $SQL .=', sysdate ';
        $SQL .='   )';

        $this->_ProtokollDB->Ausfuehren($SQL,'',true,$this->_ProtokollDB->Bindevariablen('XDI'));
        $this->debugAusgabe('Importiere Datei:' . $Dateiname,1,'Info');
        $SQL = ' select seq_xdi_key.currval from dual';

        return $this->_ProtokollDB->RecordSetOeffnen($SQL)->FeldInhalt(1);
    }

    /**
     * Ermittelt den Zeitrraum
     */
    private function _ermittleExportZeitraum(){

        $Jahr = $this->_letztesJahr();
        $Monat = $this->_letztesMonat();
        $this->_EXPZeitraumVon = '01.' . $Monat  . '.' . $Jahr;

        $this->_EXPZeitraumBis = date('t', strtotime($this->_EXPZeitraumVon)) . '.' . $Monat .'.'. $Jahr;
        $this->debugAusgabe('Habe Exportzeitraum ermittelt: ' . $this->_EXPZeitraumVon . ' bis ' . $this->_EXPZeitraumBis, 1, 'Info');
    }

    private function _letztesMonat(){
        if((date('m')) == 1){ //Letztes Monat im Dezember? Dann mimm das letzte Jahr
            $Monat = 12;
        }else{
            $Monat =  date('m') - 1;
        }
        $Monat = str_pad($Monat,2,'0',STR_PAD_LEFT);
        return $Monat;
    }

    private function _letztesJahr(){
        if((date('m')) == 1){ //Jetzt Januar? Dann war letztes Monat ein Jahr weniger
            $Jahr =  date('Y') -1;
        }else {
            $Jahr =  date('Y');
        }
        return $Jahr;

    }

    private function _naechstesMonat() {
        if((date('m')) == 12){ //aktuelles Monat ist Dezember? Dann nimm Januar
            $Monat = 1;
        }else{
            $Monat =  date('m') + 1;
        }
        $Monat = str_pad($Monat,2,'0',STR_PAD_LEFT);
        return $Monat;
    }

    private function _naechstesJahr() {
        if((date('m')) == 12){ //Jetzt Dezember? Dann ist naechtes Monat ein Jahr mehr
            $Jahr =  date('Y') +1;
        }else {
            $Jahr =  date('Y');
        }
        return $Jahr;
    }

    private function _ermittleNaechsteLaufzeit () {
        $datum = '03.';
        $datum .= $this->_naechstesMonat() . '.';
        $datum .= $this->_naechstesJahr();

        return $datum;
    }

    /**
     * Exportiert
     */
    public function Export()
    {
        // ExportZeitraum ermitteln
        $this->_ermittleExportZeitraum();


        $SQL = 'SELECT BSW_KEY,';
        $SQL .= '   BSW_FIL_ID,';
        $SQL .= '   BSW_DATUM,';
        $SQL .= '   BSW_ZEIT,';
        $SQL .= '   BSW_BSA,';
        $SQL .= '   BSW_BONNR,';
        $SQL .= '   BSW_KARTENNUMMER,';
        $SQL .= '   BSW_UMSATZ,';
        $SQL .= '   BSW_MWST,';
        $SQL .= '   BSW_IMQ_ID,';
        $SQL .= '   BSW_USER,';
        $SQL .= '   BSW_USERDAT,';
        $SQL .= '   BSW_XDI_KEY';
        $SQL .= ' FROM BSW';
        $SQL .= ' WHERE BSW_EXPORTDATUM is null ';
        $SQL .= ' AND trunc(BSW_DATUM) <= ' . $this->_DB->WertSetzen('BSW','D',$this->_EXPZeitraumBis);

        $rsBSW = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('BSW'));

        if ($rsBSW->AnzahlDatensaetze() > 0) {
            $this->debugAusgabe('Beginne Export:');
            $ExpDatei = 'atu_' . $this->_letztesMonat() . $this->_letztesJahr();
            if(file_exists(self::ExpPfad.$ExpDatei)){
                $this->debugAusgabe('Es liegen Reste vom letzten Export herum. Breche ab.. Pfad der Reste: ' .self::ExpPfad,1, 'FEHLER');
                die;
            }
            $Datei = fopen(self::ExpPfad.$ExpDatei,'w+');
            $UpdateKeys = '0';
            while(!$rsBSW->EOF()){
                $Zeile = '';
                $UpdateKeys .= ', ' .$rsBSW->FeldInhalt('BSW_KEY');

                //WKZ immer Konstant 978 (=Euro)
                $Zeile .= '978';

                //PFNR BSW-Partnerfirmennummer (3415+lfd Fil-Nummer)
                $Zeile .= '3415' . str_pad($rsBSW->FeldInhalt('BSW_FIL_ID'),4,'0',STR_PAD_LEFT);

                //Terminal-ID (wird mit Nullen gef�llt)
                $Zeile .= '00000000';

                //Kartenpr�fix (Konstant: 928000132)
                $Zeile .= '928000132';

                //KNR = Kartennummer BSW-Mitglied
                $Kartennummer = $rsBSW->FeldInhalt('BSW_KARTENNUMMER');
                $Kartennummer = substr($Kartennummer,9,10);
                $Zeile .= $Kartennummer;

                //DAT = Datum im folgenden Format: YYMMDD
                $DAT = date('ymd', strtotime($rsBSW->FeldInhalt('BSW_DATUM')));
                $Zeile .=$DAT;

                //ZT = Zeit im folgenden Format: HHMM
                $ZT = $rsBSW->FeldInhalt('BSW_ZEIT');
                $ZT = str_pad(substr($ZT,2,4),4,'0',STR_PAD_LEFT);
                $Zeile .= $ZT;

                //BETRAG im folgenden Format: 12 stellig ohne Komma mit vorrangestellten nullen
                $Betrag = $rsBSW->FeldInhalt('BSW_UMSATZ','N2');
                $Betrag = str_replace(',','',$Betrag);
                $Betrag = str_replace('.','',$Betrag);
                $Betrag = str_replace('-','',$Betrag);
                $Betrag = str_pad($Betrag,12,'0',STR_PAD_LEFT);
                $Zeile .= $Betrag;

                // 'MWST im folgenden Format: 12 stellig ohne Komma mit vorrangestellten nullen
                $MWST = str_replace(',','',$rsBSW->FeldInhalt('BSW_MWST','N2'));
                $MWST = str_replace('.','',$MWST);
                $MWST = str_replace('-','',$MWST);
                $MWST = str_pad($MWST,12,'0',STR_PAD_LEFT);
                $Zeile .= $MWST;

                //BON = ATU-Bonnummer
                $BonNr = str_pad(substr($rsBSW->FeldInhalt('BSW_BONNR'),-7),10,'0',STR_PAD_LEFT);
                $Zeile .= $BonNr;

                //TA = Transaktionsart (01 = negativ / 00 = positiv)
                if($rsBSW->FeldInhalt('BSW_UMSATZ')<0){
                    $Zeile .= '01';
                }else{
                    $Zeile .= '00';
                }

                //WA = Warenart(-gruppe) 1-stellig; wird genullt
                $Zeile .= 1;

                //LineFeed
                $Zeile .= chr(13).chr(10);
                $this->debugAusgabe('Exportiere Zeile:  ' . $Zeile,999, 'Debug');

                fwrite($Datei,$Zeile);

                $rsBSW->DSWeiter();
            }
            fclose($Datei);

            //In TXT umbennenen
            rename(self::ExpPfad.$ExpDatei,self::ExpPfad.$ExpDatei.'.txt');
            $this->debugAusgabe('Export fertig. Habe Datei in ' . self::ExpPfad.$ExpDatei.'.txt umbenannt',1, 'Info');
            $SQL = 'update BSW ';
            $SQL .= 'set BSW_EXPORTDATUM = sysdate ';
            $SQL .= 'where BSW_EXPORTDATUM is null ';
            $SQL .= 'and trunc(BSW_DATUM) <= ' . $this->_DB->WertSetzen('BSW', 'D', $this->_EXPZeitraumBis);

            $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('BSW'));

            $Mail = new awisMailer($this->_DB,$this->_AWISBenutzer);
            $Mail->LoescheAdressListe();
            $Mail->Absender('shuttle@de.atu.eu');
            $Mail->AdressListe(awisMailer::TYP_TO,'edv@avs.de');

            $SQL = 'SELECT MVT_BETREFF, MVT_TEXT, MVT_SMS';
            $SQL .= ' FROM mailversandtexte';
            $SQL .= ' WHERE MVT_Bereich = '.$this->_DB->WertSetzen('MVT', 'T', 'BSW_MAIL');
            $SQL .= ' AND MVT_Sprache = '.$this->_DB->WertSetzen('MVT', 'T', 'DE');
            $rsMVT = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('MVT'));
            $Mail->Text($rsMVT->FeldInhalt('MVT_TEXT'),awisMailer::FORMAT_HTML);
            $Mail->Betreff($rsMVT->FeldInhalt('MVT_BETREFF'));
            $Mail->SetzeBezug('BSW',0);
            $Mail->AnhaengeLoeschen();
            $Mail->Anhaenge(self::ExpPfad.$ExpDatei.'.txt',$ExpDatei.'.txt',false);
            $Mail->MailInWarteschlange();

        }else{
            $this->debugAusgabe('Keine Daten zum exportieren vorhanden!',1, 'FEHLER'); ;
        }

        // Job soll jedes Monat am 3ten laufen. Daher wird im Scheduler die n�chste Laufzeit gesetzt
        $this->_awisJobs->SetzeNaechstenStart(self::XJO_KEY, $this->_ermittleNaechsteLaufzeit());
    }


    /**
     * @param $e awisException
     */
    private function _handleErrors($e, $Zusatzinfo = '')
    {
        if (strpos($e->getMessage(), 'UID_BSW_DATUM_BONNR_KART') !== false) {
            $this->debugAusgabe('UID_BSW_DATUM_BONNR_KART verletzt. Datensatz bereits vorhanden!',1,'Fehler');
        } else {
            $this->debugAusgabe('Nicht behandelter Fehler aufgetreten! ' . $e->getMessage(),1,'Fehler');
        }

        if($Zusatzinfo!=''){
            $this->debugAusgabe('Zusatzinfo: ' . $Zusatzinfo,1,'Fehler');
        }

    }

    public function getNaechsteStartzeit () {
        return $this->_ermittleNaechsteLaufzeit();
    }

}
