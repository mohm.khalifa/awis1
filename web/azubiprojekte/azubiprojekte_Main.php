<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    global $Funktionen;
    require_once('azubiprojekte_funktionen.php');

    try {
        $Funktionen = new azubiprojekte_funktionen();
        $Version = 3;
        echo "<link rel=stylesheet type=text/css href=" . $Funktionen->AWISBenutzer->CSSDatei($Version) . ">";
    } catch (Exception $ex) {
        die($ex->getMessage());
    }

    echo '<title>' . $Funktionen->AWISSprachKonserven['TITEL']['tit_Azubiprojekte'] . '</title>';

    if (isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'OffenePos') {
        echo '<meta http-equiv="refresh" content="5">';
    }
    ?>
</head>
<body>
<?php
include("awisHeader$Version.inc");    // Kopfzeile

try {
    $Funktionen->RechteMeldung(1);
    $Register = new awisRegister(65000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));
} catch (Exception $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180830");
    } else {
        $Funktionen->Form->SchreibeHTMLCode('AWIS: ' . $ex->getMessage());
    }
}
?>
</body>
</html>