<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;
global $CursorFeld;

$TextKonserven = array();
$TextKonserven[]=array('CKM','%');
$TextKonserven[]=array('CKD','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','Seite');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3707 = awisBenutzerRecht($con,3707,$AWISBenutzer->BenutzerName());
if($Recht3707==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_FORM_FormularStart();
echo '<form name=frmKonditionen action=./crm_Main.php?cmdAktion=Konditionen method=post  enctype="multipart/form-data">';

$EditRecht=(($Recht3707&6)!=0);

if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;';
	include('./crm_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "CRMSuche" , $AWISBenutzer->BenutzerName());
	}
	else 
	{
		$Param = $AWIS_KEY1;
	}
}
elseif(isset($_POST['cmdSpeichern_x']) and !isset($_POST['datImport']))
{
	include('./crm_speichern.php');
}


$SQL = 'SELECT COUNT(*) AS ANZ, CKM_BEREICH';
$SQL .= ' FROM CRMKonditionsModelle';
$SQL .= ' WHERE CKM_STATUS=\'A\'';
$SQL .= ' GROUP BY CKM_BEREICH';

$rsDaten = awisOpenRecordset($con, $SQL);
$rsDatenZeilen = $awisRSZeilen;
$Bereiche = explode('|',$AWISSprachKonserven['CKM']['lst_CKM_BEREICH']);
$CKMBereiche = array();
foreach($Bereiche as $Bereich)
{
	$Daten =explode('~',$Bereich);
	$CKMBereiche[$Daten[0]]=$Daten[1];
}
// Uebersicht ueber die Modelle in den einzelnen Bereichen anzeigen
for($DatenZeile=0;$DatenZeile<$rsDatenZeilen;$DatenZeile++)
{
	awis_FORM_ZeileStart();
		
	awis_FORM_Erstelle_TextLabel($CKMBereiche[$rsDaten['CKM_BEREICH'][$DatenZeile]].':',150);
	awis_FORM_Erstelle_TextLabel($rsDaten['ANZ'][$DatenZeile],40);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['wrd_KonditionsModelle'],100);
	
	awis_FORM_ZeileEnde();	
}

awis_FORM_FormularEnde();

$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Warengruppen'));
echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';
awis_RegisterErstellen(3703, $con, $RegisterSeite);

awis_FORM_SchaltflaechenStart();

if(($Recht3707&6)!==0)		//
{
	awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}
awis_FORM_SchaltflaechenEnde();

echo '</form>';


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>