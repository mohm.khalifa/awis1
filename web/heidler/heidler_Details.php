<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    require_once 'heidler_funktionen.php';
    
	$Functions = new heidler_funktionen();
    $UserFilialen = $Functions->_AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
    $mwstSatzDE = 100 + $Functions->mwstSatz();

	$Recht45000 = $Functions->_AWISBenutzer->HatDasRecht(45000);

	//Filialuser die ohne Aktion kommen, bekommen keine Parameter aus der Datenbank, da ansonsten die letzte Suche geladen wird.
	if($UserFilialen and !isset($_GET['cmdAktion'])){
        $Param = array();
    }else{
        $Param = unserialize($Functions->_AWISBenutzer->ParameterLesen('Formular_Heidler'));
    }
    $Param['ORDER'] = isset($Param['ORDER'])?$Param['ORDER']:'DATUM_KOMMISSIONIERUNG desc';
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht45000==0)
	{
		$Functions->_Form->Fehler_KeineRechte();
	}
	
	if(isset($_GET['AUFTRAG_ID']) and isset($_GET['LAGERKZ']))
	{
		$AWIS_KEY1 = $_GET['AUFTRAG_ID'] . $_GET['LAGERKZ'] . $_GET['TRACKINGNR'];
	}
	
	if(isset($_POST['cmdSuche_x']))
	{
        $Param['LAGER']= (isset($_POST['sucLAGER'])?$Functions->_Form->Format('T',$_POST['sucLAGER'],true):'');
		$Param['KNDNR'] = 	(isset($_POST['sucKNDNR'])?$Functions->_Form->Format('T',$_POST['sucKNDNR'],true):'');
		$Param['EMPF_LKZ']= (isset($_POST['sucEMPF_LKZ'])?$Functions->_Form->Format('T',$_POST['sucEMPF_LKZ'],true):'');
		$Param['TRACKING_NR']= (isset($_POST['sucTRACKING_NR'])?$Functions->_Form->Format('T',$_POST['sucTRACKING_NR'],true):'');
        $Param['ATU_NR']= (isset($_POST['sucATU_NR'])?$Functions->_Form->Format('T',$_POST['sucATU_NR'],true):'');
        $Param['DATUM_KOMMISSIONIERUNG_VON']= (isset($_POST['sucDATUM_KOMMISSIONIERUNG_VON'])?$Functions->_Form->Format('T',$_POST['sucDATUM_KOMMISSIONIERUNG_VON'],true):'');
        $Param['DATUM_KOMMISSIONIERUNG_BIS']= (isset($_POST['sucDATUM_KOMMISSIONIERUNG_BIS'])?$Functions->_Form->Format('T',$_POST['sucDATUM_KOMMISSIONIERUNG_BIS'],true):'');
        $Param['VERSANDART']= (isset($_POST['sucVERSANDART'])?$Functions->_Form->Format('T',$_POST['sucVERSANDART'],true):'');
        $Param['MANDANT']= (isset($_POST['sucMANDANT'])?$Functions->_Form->Format('T',$_POST['sucMANDANT'],true):'');
        $Param['HEIDLER_MANDANT']=(isset($_POST['sucHEIDLER_MANDANT'])?$Functions->_Form->Format('T', $_POST['sucHEIDLER_MANDANT'],true):'');
        $Param['SATZKENNUNG']= (isset($_POST['sucSATZKENNUNG'])?$Functions->_Form->Format('T',$_POST['sucSATZKENNUNG'],true):'');
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['Block']=1;
        $Param['ORDER']=' DATUM_KOMMISSIONIERUNG DESC';
	}

    $SQL  ='SELECT SATZKENNUNG ,';
    $SQL .=' KNDNR ,';
    $SQL .=' EMPF_NAME1 ,';
    $SQL .=' EMPF_NAME2 ,';
    $SQL .=' EMPF_NAME3 ,';
    $SQL .=' EMPF_STRASSE ,';
    $SQL .=' EMPF_LKZ ,';
    $SQL .=' EMPF_PLZ ,';
    $SQL .=' EMPF_ORT ,';
    $SQL .=' TRACKING_NR ,';
    $SQL .=' ATU_NR ,';
    $SQL .=' REFERENZNR ,';
    $SQL .=' KENN1 ,';
    $SQL .=' LAGERKZ ,';
    $SQL .=' DATUM_KOMMISSIONIERUNG ,';
    $SQL .=' MENGE ,';
    $SQL .=' VERBUCHTE_MENGE ,';
    $SQL .=' AUFTRAG_ID ,';
    $SQL .=' LABEL_GENDATE ,';
    $SQL .=' VERSANDART ,';
    $SQL .=' GEWICHT_NETTO ,';
    $SQL .=' PACKSTKNR ,';
    $SQL .=' MANDANT ,';
    $SQL .=' HEIDLER_MANDANT ,';
    $SQL .=' VERSANDART_WORT ,';
    $SQL .=' PREIS ,';
    $SQL .=' ARTIKELBEZEICHNUNG ,';
    $SQL .=' ARTIKELBEZEICHNUNGWW,';
    $SQL .=' row_number() OVER (  order by ' . $Param['ORDER'] . ') as ZEILENNR';
    $SQL .=' FROM (SELECT  trim(SATZKENNUNG) as SATZKENNUNG ,';
    $SQL .=' KNDNR ,';
    $SQL .=' EMPF_NAME1 ,';
    $SQL .=' EMPF_NAME2 ,';
    $SQL .=' EMPF_NAME3 ,';
    $SQL .=' EMPF_STRASSE ,';
    $SQL .=' EMPF_LKZ ,';
    $SQL .=' EMPF_PLZ ,';
    $SQL .=' EMPF_ORT ,';
    $SQL .=' TRACKING_NR ,';
    $SQL .=' ATU_NR ,';
    $SQL .=' REFERENZNR ,';
    $SQL .=' KENN1 ,';
    $SQL .=' LAGERKZ ,';
    $SQL .=' DATUM_KOMMISSIONIERUNG ,';
    $SQL .=' MENGE ,';
    $SQL .=' VERBUCHTE_MENGE ,';
    $SQL .=' AUFTRAG_ID ,';
    $SQL .=' LABEL_GENDATE ,';
    $SQL .=' VERSANDART ,';
    $SQL .=' GEWICHT_NETTO ,';
    $SQL .=' PACKSTKNR ,';
    $SQL .=' MANDANT ,';
    $SQL .=' HEIDLER_MANDANT ,';
    $SQL .=' CASE VERSANDART WHEN \'1\' THEN \'NOX\'';
    $SQL .=' WHEN \'4\' THEN \'NOX\'';
    $SQL .=' ELSE \'DPD\' END as VERSANDART_WORT ,';
    $SQL .=' (nvl(AST_VK,0) / 119) * 80 as PREIS ,';
    $SQL .=' AST_BEZEICHNUNG as ARTIKELBEZEICHNUNG ,';
    $SQL .=' AST_BEZEICHNUNGWW as ARTIKELBEZEICHNUNGWW';
    $SQL .=' FROM MV_HEIDLER ';
    $SQL .=' LEFT JOIN ARTIKELSTAMM ON ATU_NR = AST_ATUNR';


    if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '')
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4) . ' )';
		}
	}

	if($AWIS_KEY1 != '')
	{
		if(isset($_GET['AUFTRAG_ID']) or $AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE AUFTRAG_ID||LAGERKZ||TRACKING_NR=\'' . $AWIS_KEY1 . '\')';
		}
		else
		{
			$SQL .= ' AND AUFTRAG_ID||LAGERKZ||TRACKING_NR=\'' . $AWIS_KEY1 . '\')';
		}
	}

	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;

	if (isset($_REQUEST['Block']))
	{
			$Block = $Functions->_Form->Format('N0', $_REQUEST['Block'], false);
	}
	elseif(isset($Param['Block']))
	{
		$Block = $Param['Block'];
	}
	$ZeilenProSeite = $Functions->_AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $Functions->_DB->ErmittleZeilenAnzahl($SQL,$Functions->_DB->Bindevariablen('HEI',false));

	$Functions->_Form->DebugAusgabe(1, $SQL);
    if (!isset($AWIS_KEY1) OR $AWIS_KEY1 == '') {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr >='. $Functions->_DB->WertSetzen('HEI','N0',$StartZeile);
        $SQL .= ' AND ZeilenNr<(' . $Functions->_DB->WertSetzen('HEI','N0',($StartZeile + $ZeilenProSeite)) .')';
    }

	$rsHeidler = $Functions->_DB->RecordSetOeffnen($SQL,$Functions->_DB->Bindevariablen('HEI',true));

	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['AUFTRAG_ID'] = 90;
    $FeldBreiten['KNDNR'] = 80;
    $FeldBreiten['DATUM_KOMMISSIONIERUNG'] = 170;
    $FeldBreiten['ATU_NR'] = 80;
    $FeldBreiten['REFERENZNR'] = 100;
    $FeldBreiten['GEWICHT_NETTO'] = 80;
    $FeldBreiten['PACKSTKNR'] = 100;
    $FeldBreiten['TRACKING_NR'] = 270;
    $FeldBreiten['EMPF_PLZ'] = 70;
    $FeldBreiten['STATUS'] = 310;
    $FeldBreiten['VERSANDART'] = 100;
    $FeldBreiten['SATZKENNUNG'] = 100;
    $FeldBreiten['MANDANT'] = 100;
    $FeldBreiten['HEIDLER_MANDANT'] = 100;
    $FeldBreiten['PREIS'] = 50;
    $FeldBreiten['ARTIKELBEZEICHNUNG'] = 270;

	$Functions->_Form->Formular_Start();

	$Functions->_Form->DebugAusgabe(1,$UserFilialen);

	if (($rsHeidler->AnzahlDatensaetze() > 1))
	{
		$Functions->_Form->ZeileStart('display: inline-block');
		$Link = './heidler_Main.php?cmdAktion=Details&Sort=AUFTRAG_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AUFTRAG_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['AUFTRAG_ID'], $FeldBreiten['AUFTRAG_ID'], '', $Link);

        if ($UserFilialen == '') {
            $Link = './heidler_Main.php?cmdAktion=Details&Sort=KNDNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KNDNR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['KNDNR'], $FeldBreiten['KNDNR'], '', $Link);
        }
        
        $Link = './heidler_Main.php?cmdAktion=Details&Sort=DATUM_KOMMISSIONIERUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='DATUM_KOMMISSIONIERUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'], $FeldBreiten['DATUM_KOMMISSIONIERUNG'], '', $Link);
        
        $Link = './heidler_Main.php?cmdAktion=Details&Sort=ATU_NR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ATU_NR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['ATU_NR'], $FeldBreiten['ATU_NR'], '', $Link);

        if(($Recht45000&128)==128){
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['ARTIKELBEZEICHNUNG'], $FeldBreiten['ARTIKELBEZEICHNUNG']);
        }

        $Link = './heidler_Main.php?cmdAktion=Details&Sort=REFERENZNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='REFERENZNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['REFERENZNR'], $FeldBreiten['REFERENZNR'], '', $Link);

        if ($UserFilialen == '') {
            $Link = './heidler_Main.php?cmdAktion=Details&Sort=GEWICHT_NETTO'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GEWICHT_NETTO'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['GEWICHT_NETTO'], $FeldBreiten['GEWICHT_NETTO'], '', $Link);
        }

        $Link = './heidler_Main.php?cmdAktion=Details&Sort=TRACKING_NR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRACKING_NR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['TRACKING_NR'], $FeldBreiten['TRACKING_NR'], '', $Link);

        $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['STATUS'], $FeldBreiten['STATUS'], '', $Link);

        if ($UserFilialen != '') {
            $Link = './heidler_Main.php?cmdAktion=Details&Sort=VERSANDART' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VERSANDART'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['HEIDLER_VERSANDART'], $FeldBreiten['VERSANDART'], '', $Link);
        }

        if ($UserFilialen == '') {
            $Link = './heidler_Main.php?cmdAktion=Details&Sort=EMPF_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EMPF_PLZ')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['EMPF_PLZ_SHORT'], $FeldBreiten['EMPF_PLZ'], '', $Link);

            $Link = './heidler_Main.php?cmdAktion=Details&Sort=PREIS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'PREIS')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['PREIS'], $FeldBreiten['PREIS'], '', $Link);

            if(($Recht45000&1024)==1024) {
                $Link = './heidler_Main.php?cmdAktion=Details&Sort=MANDANT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MANDANT')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['MANDANT'], $FeldBreiten['MANDANT'], 'R', $Link);

                $Link = './heidler_Main.php?cmdAktion=Details&Sort=HEIDLER_MANDANT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HEIDLER_MANDANT')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['HEIDLER_MANDANT'], $FeldBreiten['HEIDLER_MANDANT'], 'R', $Link);

            }

        }
        $Functions->_Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsHeidler->EOF())
		{
			$Functions->_Form->ZeileStart('display: inline-block','Zeile"ID="Zeile_'.$rsHeidler->FeldInhalt('AUFTRAG_ID'));
			
            $Link = './heidler_Main.php?cmdAktion=Details&AUFTRAG_ID='.str_replace('#', '%23', $rsHeidler->FeldInhalt('AUFTRAG_ID')) . '&LAGERKZ=' . $rsHeidler->FeldInhalt('LAGERKZ') . '&TRACKINGNR=' . $rsHeidler->FeldInhalt('TRACKING_NR') .  (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $rsHeidler->FeldInhalt('AUFTRAG_ID');
			$Functions->_Form->Erstelle_ListenFeld('AUFTRAG_ID', $rsHeidler->FeldInhalt('AUFTRAG_ID'), 0, $FeldBreiten['AUFTRAG_ID'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);

            if ($UserFilialen == '') {
                $TTT = $rsHeidler->FeldInhalt('KNDNR');
                $Functions->_Form->Erstelle_ListenFeld('KNDNR', $rsHeidler->FeldInhalt('KNDNR'), 0, $FeldBreiten['KNDNR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
            }
            
            $TTT =  $rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG');
            $Functions->_Form->Erstelle_ListenFeld('DATUM_KOMMISSIONIERUNG',$rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG'), 0, $FeldBreiten['DATUM_KOMMISSIONIERUNG'], false, ($DS%2), '','', 'T', 'L', $TTT);
            
            $TTT =  $rsHeidler->FeldInhalt('ATU_NR');
            $Functions->_Form->Erstelle_ListenFeld('ATU_NR',$rsHeidler->FeldInhalt('ATU_NR'), 0, $FeldBreiten['ATU_NR'], false, ($DS%2), '','', 'T', 'L', $TTT);

            if(($Recht45000&128)==128) {
                $TTT = ($rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG')!= "")?$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG'):$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNGWW');
                $Functions->_Form->Erstelle_ListenFeld('ARTIKELBEZEICHNUNG', ($rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG')!= "")?$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG'):$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNGWW'), 0, $FeldBreiten['ARTIKELBEZEICHNUNG'], false, ($DS % 2), '', '', 'TS', 'L', $TTT);
            }

            $TTT =  $rsHeidler->FeldInhalt('REFERENZNR');
            $Functions->_Form->Erstelle_ListenFeld('REFERENZNR',$rsHeidler->FeldInhalt('REFERENZNR'), 0, $FeldBreiten['REFERENZNR'], false, ($DS%2), '','', 'T', 'L', $TTT);

            if ($UserFilialen == '') {
                $TTT =  $rsHeidler->FeldInhalt('GEWICHT_NETTO');
                $Functions->_Form->Erstelle_ListenFeld('GEWICHT_NETTO',$rsHeidler->FeldInhalt('GEWICHT_NETTO'), 0, $FeldBreiten['GEWICHT_NETTO'], false, ($DS%2), '','', 'N2', 'L', $TTT);
            }

            $TrackingNrs = explode('|', $rsHeidler->FeldInhalt('TRACKING_NR'));
            
            $Feldinhalt = '';
            $Popup = ' target="_blank"';
            for ($i = 0; $i < count($TrackingNrs); $i++) {
                if($TrackingNrs[$i] != '') {
                    if ($i == 2 and count($TrackingNrs) > 2) {
                        $Feldinhalt .= ', <b>...</b>';
                        break;
                    } else {
                        $Feldinhalt .= ', ';
                        if (($rsHeidler->FeldInhalt('VERSANDART') == '1' or $rsHeidler->FeldInhalt('VERSANDART') == '4') and strpos($rsHeidler->FeldInhalt('TRACKING_NR'), 'Fehleretikett') === false) { // NOX
                            $Feldinhalt .= '<a href="https://tcs-ords.app.nox.express/ords/stcs/STCS_TRACK_AND_TRACE.Info?pBarcode=' . $TrackingNrs[$i] . '"' . $Popup . '>' . $TrackingNrs[$i] . '</a>';
                        } elseif ($rsHeidler->FeldInhalt('VERSANDART') == '2' and strpos($rsHeidler->FeldInhalt('TRACKING_NR'), 'Fehleretikett') === false) { // DPD
                            $Feldinhalt .= '<a href="http://tracking.dpd.de/parcelstatus?query=' . $TrackingNrs[$i] . '&locale=de_DE"' . $Popup . '>' . $TrackingNrs[$i] . '</a>';
                        } else {
                            $Feldinhalt .= $TrackingNrs[$i];
                        }
                    }
                }
            }
            
            $TTT = $rsHeidler->FeldInhalt('TRACKING_NR');
            $Functions->_Form->Erstelle_ListenFeld('TRACKING_NR', substr($Feldinhalt, 1), 0, $FeldBreiten['TRACKING_NR'], false, ($DS%2), '','', 'TS', 'L', $TTT);

            $Status = $Functions->PaketStatusKurz($rsHeidler->FeldInhalt('VERSANDART'), $rsHeidler->FeldInhalt('TRACKING_NR'));
            $Functions->_Form->Erstelle_ListenFeld('STATUS', (strlen($Status) > 48 ? substr($Status, 0, 47) . '..' : $Status), 0, $FeldBreiten['STATUS'], false, ($DS%2), '','', 'TS', 'L', str_replace('"', '\'', $Status));

            if ($UserFilialen == '') {
                $TTT = $rsHeidler->FeldInhalt('EMPF_PLZ');
                $Functions->_Form->Erstelle_ListenFeld('EMPF_PLZ', $rsHeidler->FeldInhalt('EMPF_PLZ'), 0, $FeldBreiten['EMPF_PLZ'], false, ($DS%2), '','', 'TS', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('PREIS', 'N2');
                $Functions->_Form->Erstelle_ListenFeld('PREIS', $rsHeidler->FeldInhalt('PREIS','N2'), 0, $FeldBreiten['PREIS'], false, ($DS%2), '','', 'N2', 'R', $TTT);

                if(($Recht45000&1024)==1024) {
                    $TTT = $rsHeidler->FeldInhalt('MANDANT');
                    $Functions->_Form->Erstelle_ListenFeld('MANDANT', $rsHeidler->FeldInhalt('MANDANT'), 0, $FeldBreiten['MANDANT'], false, ($DS%2), '','', 'TS', 'R', $TTT);

                    $TTT = $rsHeidler->FeldInhalt('HEIDLER_MANDANT');
                    $Functions->_Form->Erstelle_ListenFeld('HEIDLER_MANDANT', $rsHeidler->FeldInhalt('HEIDLER_MANDANT'), 0, $FeldBreiten['HEIDLER_MANDANT'], false, ($DS%2), '','', 'TS', 'C', $TTT);
                    }
            }

            if (!$UserFilialen == '') {
                $TTT = $rsHeidler->FeldInhalt('VERSANDART_WORT');
                $Functions->_Form->Erstelle_ListenFeld('VERSANDART_WORT', $rsHeidler->FeldInhalt('VERSANDART_WORT'), 0, $FeldBreiten['VERSANDART'], false, ($DS % 2), '', '', 'TS', 'L', $TTT);
            }

			$Functions->_Form->ZeileEnde();
			$DS++;
			$rsHeidler->DSWeiter();
		}
		$Link = './heidler_Main.php?cmdAktion=Details';
		$Functions->_Form->ZeileStart();
        $Functions->_Form->SchreibeHTMLCode('<form name="frmDetails" action="' . $Link . '" method=POST  enctype="multipart/form-data">');
		$Functions->_Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        $Functions->_Form->SchreibeHTMLCode('</form>');
        $Functions->_Form->ZeileEnde();
	}
	elseif($rsHeidler->AnzahlDatensaetze() == 1)
	{
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./heidler_Main.php?cmdAktion=Details accesskey=T title='".$Functions->AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Functions->_Form->InfoZeile($Felder,'');
	    
        $LabelBreite = 200;
        
		$Functions->_Form->ZeileStart();
		$Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['AUFTRAG_ID'] . ':',$LabelBreite);
		$Functions->_Form->Erstelle_TextFeld('AUFTRAG_ID',$rsHeidler->FeldInhalt('AUFTRAG_ID'),65,170,false);
		$Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['KNDNR'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('KNDNR',$rsHeidler->FeldInhalt('KNDNR'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['EMPF_NAME1'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('EMPF_NAME1',$rsHeidler->FeldInhalt('EMPF_NAME1'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        if(trim($rsHeidler->FeldInhalt('EMPF_NAME2')) != '') {
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel('',$LabelBreite);
            $Functions->_Form->Erstelle_TextFeld('EMPF_NAME2',$rsHeidler->FeldInhalt('EMPF_NAME2'),65,170,false);
            $Functions->_Form->ZeileEnde();
        }
        
        if(trim($rsHeidler->FeldInhalt('EMPF_NAME3')) != '') {
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel('',$LabelBreite);
            $Functions->_Form->Erstelle_TextFeld('EMPF_NAME3',$rsHeidler->FeldInhalt('EMPF_NAME3'),65,170,false);
            $Functions->_Form->ZeileEnde();
        }
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['EMPF_ADRESSE'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('EMPF_STRASSE',$rsHeidler->FeldInhalt('EMPF_STRASSE'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel('',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('EMPF_PLZ',$rsHeidler->FeldInhalt('EMPF_PLZ'),20,65,false);
        $Functions->_Form->Erstelle_TextFeld('EMPF_ORT',$rsHeidler->FeldInhalt('EMPF_ORT'),65,300,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['ATU_NR'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('ATU_NR',$rsHeidler->FeldInhalt('ATU_NR'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        if (trim($rsHeidler->FeldInhalt('ATU_NR')) == '??????') {
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['REFERENZNR'] . ':',$LabelBreite);
            $Functions->_Form->Erstelle_TextFeld('REFERENZNR',$rsHeidler->FeldInhalt('REFERENZNR'),65,170,false);
            $Functions->_Form->ZeileEnde();
        }
        if(($Recht45000&128)==128) {

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['ARTIKELBEZEICHNUNG'] . ':',$LabelBreite);
            $Functions->_Form->Erstelle_TextFeld('ARTIKELBEZEICHNUNG',($rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG')!= "")?$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNG'):$rsHeidler->FeldInhalt('ARTIKELBEZEICHNUNGWW'),65,600,false);
            $Functions->_Form->ZeileEnde();
        }
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['MENGE'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('MENGE',$rsHeidler->FeldInhalt('MENGE'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['VERBUCHTE_MENGE'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('VERBUCHTE_MENGE',$rsHeidler->FeldInhalt('VERBUCHTE_MENGE'),65,170,false);
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['SATZKENNUNG'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('SATZKENNUNG',$rsHeidler->FeldInhalt('SATZKENNUNG'),65,170,false);
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['HEIDLER_VERSANDART'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('HEIDLER_VERSANDART',$rsHeidler->FeldInhalt('VERSANDART_WORT'),65,170,false);
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['MANDANT'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('MANDANT',$rsHeidler->FeldInhalt('MANDANT'),65,170,false);
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['HEIDLER_MANDANT'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('HEIDLER_MANDANT',$rsHeidler->FeldInhalt('HEIDLER_MANDANT'),65,170,false);
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['TRACKING_NR'] . ':',$LabelBreite);
        $TrackingNrs = explode('|', $rsHeidler->FeldInhalt('TRACKING_NR'));
        $ErsteZeile = true;
        foreach ($TrackingNrs as $TrackingNr) {
            $Link = '';
            if(trim($TrackingNr) != '') {
                if (($rsHeidler->FeldInhalt('VERSANDART') == '1' or $rsHeidler->FeldInhalt('VERSANDART') == '4') and strpos($rsHeidler->FeldInhalt('TRACKING_NR'), 'Fehleretikett') === false) { // NOX
                    $Link .= '*https://tcs-ords.app.nox.express/ords/stcs/STCS_TRACK_AND_TRACE.Info?pBarcode=' . $TrackingNr;
                } elseif ($rsHeidler->FeldInhalt('VERSANDART') == '2' and strpos($rsHeidler->FeldInhalt('TRACKING_NR'), 'Fehleretikett') === false) { // DPD
                    $Link .= '*http://tracking.dpd.de/parcelstatus?query=' . $TrackingNr . '&locale=de_DE';
                }
                
                if ($ErsteZeile == true) {
                    $ErsteZeile = false;
                    $Functions->_Form->Erstelle_TextLabel($TrackingNr, 100, '', $Link);
                    $Functions->_Form->ZeileEnde();
                } else {
                    $Functions->_Form->ZeileStart();
                    $Functions->_Form->Erstelle_TextLabel('',$LabelBreite);
                    $Functions->_Form->Erstelle_TextLabel($TrackingNr, 100, '', $Link);
                    $Functions->_Form->ZeileEnde();
                }
            }
        }
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['KENN1'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('KENN1',$rsHeidler->FeldInhalt('KENN1'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['LAGERKZ'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('LAGERKZ',$rsHeidler->FeldInhalt('LAGERKZ'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'] . ':',$LabelBreite);
        $Functions->_Form->Erstelle_TextFeld('DATUM_KOMMISSIONIERUNG',$rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG'),65,170,false);
        $Functions->_Form->ZeileEnde();
        
        if (trim($rsHeidler->Feldinhalt('LABEL_GENDATE')) != '') {
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['LABEL_GENDATE'] . ':',$LabelBreite);
            $Functions->_Form->Erstelle_TextFeld('LABEL_GENDATE',$rsHeidler->FeldInhalt('LABEL_GENDATE'),65,170,false);
            $Functions->_Form->ZeileEnde();
        }

        $Functions->_Form->Erstelle_HiddenFeld('TrackingNr', $rsHeidler->FeldInhalt('TRACKING_NR'));
        $Functions->_Form->Erstelle_HiddenFeld('Versandart', $rsHeidler->FeldInhalt('VERSANDART'));

        $Functions->_Form->FormularBereichStart();
        $Functions->_Form->FormularBereichInhaltStart('Paketstatus', false, 30, 'heidler_paketstatus', 'box1', false, ['txtVersandart', "txtTrackingNr"]);
        $Functions->_Form->AuswahlBoxHuelle('box1');
        $Functions->_Form->FormularBereichInhaltEnde();
        $Functions->_Form->FormularBereichEnde();
	}
	else
	{
		$Functions->_Form->ZeileStart();
		$Functions->_Form->Hinweistext($Functions->AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Functions->_Form->ZeileEnde();
	}
	
	$Functions->_Form->Erstelle_HiddenFeld('AUFTRAG_ID',$AWIS_KEY1);
	$Functions->_Form->Formular_Ende();
	
	$Functions->_Form->SchaltflaechenStart();
	$Functions->_Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Functions->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	$Functions->_Form->SchaltflaechenEnde();
	
	$Functions->_AWISBenutzer->ParameterSchreiben('Formular_Heidler',serialize($Param));
}
catch (awisException $ex)
{
	if($Functions->_Form instanceof awisFormular)
	{
			$Functions->_Form->DebugAusgabe(1, $ex->getSQL());
			$Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Functions->_Form instanceof awisFormular)
	{
		
		$Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param array $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	$Functions = new heidler_funktionen();
    
	$Bedingung = '';
    
    if(isset($Param['LAGER']) AND $Param['LAGER']!='')
    {
        $Bedingung .= 'AND LAGERKZ ' . $Functions->_DB->LikeOderIst($Param['LAGER'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
    }
    $UserFilialen = $Functions->_AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if ($UserFilialen != '') {
        $Bedingung .= 'AND LTRIM(KNDNR, \'0\') = LTRIM(' . $Functions->_DB->WertSetzen('HEI','T',$UserFilialen) . ', \'0\') ';
    } elseif(isset($Param['KNDNR']) AND $Param['KNDNR']!='')
	{
		$Bedingung .= 'AND LTRIM(KNDNR, \'0\') ' . $Functions->_DB->LikeOderIst(ltrim($Param['KNDNR'], '0'), awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
	}
    if(isset($Param['EMPF_LKZ']) AND $Param['EMPF_LKZ']!='')
    {
        $Bedingung .= 'AND EMPF_LKZ ' . $Functions->_DB->LikeOderIst($Param['EMPF_LKZ'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
    }
    if(isset($Param['TRACKING_NR']) AND $Param['TRACKING_NR']!='')
    {
        $Bedingung .= 'AND TRACKING_NR ' . $Functions->_DB->LikeOderIst($Param['TRACKING_NR'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
    }
    if(isset($Param['ATU_NR']) AND $Param['ATU_NR']!='')
    {
        $Bedingung .= 'AND (ATU_NR ' . $Functions->_DB->LikeOderIst($Param['ATU_NR'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' OR REFERENZNR ' . $Functions->_DB->LikeOderIst($Param['ATU_NR'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ')';
    }
    if(isset($Param['DATUM_KOMMISSIONIERUNG_VON']) AND $Param['DATUM_KOMMISSIONIERUNG_VON']!='')
    {
        $Bedingung .= 'AND TRUNC(DATUM_KOMMISSIONIERUNG) >= to_date(' . $Functions->_DB->WertSetzen('HEI','T',$Param['DATUM_KOMMISSIONIERUNG_VON']) . ') ';
    }
    if(isset($Param['DATUM_KOMMISSIONIERUNG_BIS']) AND $Param['DATUM_KOMMISSIONIERUNG_BIS']!='')
    {
        $Bedingung .= 'AND TRUNC(DATUM_KOMMISSIONIERUNG) <= to_date(' . $Functions->_DB->WertSetzen('HEI','T',$Param['DATUM_KOMMISSIONIERUNG_BIS'])  . ') ';
    }
    if(isset($Param['VERSANDART']) AND $Param['VERSANDART']!='' AND $Param['VERSANDART']!='%')
    {
        $Bedingung .= 'AND VERSANDART IN (' . $Param['VERSANDART'] . ') ';
    }
    if(isset($Param['SATZKENNUNG']) AND $Param['SATZKENNUNG']!='' AND $Param['SATZKENNUNG'] != $Functions->AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
    {
        $Bedingung .= 'AND TRIM(SATZKENNUNG) ' . $Functions->_DB->LikeOderIst($Param['SATZKENNUNG'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
    }
    if(isset($Param['MANDANT']) AND $Param['MANDANT']!='')
    {
        $Bedingung .= 'AND MANDANT ' . $Functions->_DB->LikeOderIst($Param['MANDANT'], awisDatenbank::AWIS_LIKE_UPPER,'HEI') . ' ';
    }
    if(isset($Param['HEIDLER_MANDANT']) AND $Param['HEIDLER_MANDANT']!='')
    {
        $Bedingung .= 'AND HEIDLER_MANDANT ' . $Functions->_DB->LikeOderIst($Param['HEIDLER_MANDANT'], awisDatenbank::AWIS_LIKE_UPPER, 'HEI') . ' ';
    }

	return $Bedingung;
}

?>