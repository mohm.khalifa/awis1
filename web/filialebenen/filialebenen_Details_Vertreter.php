<?php
require_once('awisFilialEbenen.inc');

global $con;
global $AWISSprache;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('FRZ','%');
$TextKonserven[]=array('FER','%');
$TextKonserven[]=array('Wort','Filialebene');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','Seite');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3806 = awisBenutzerRecht($con,3806,$AWISBenutzer->BenutzerName());
if($Recht3806==0)
{
    awisEreignis(3,1000,'Filialebenenrollen',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	//echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
}

awis_FORM_FormularStart();


$EditRecht=(($Recht3806&6)!=0);

awis_FORM_ZeileStart();

if(($Recht3806&1)>0)
{
	if(!isset($_GET['FRV_KEY']) and $_GET['FRZ_KEY'] != 0)
	{
		$SQL = 'SELECT * FROM FILIALEBENENROLLENVERTRETER ';
		$SQL .= 'INNER JOIN KONTAKTE ON KON_KEY = FRV_KON_KEY ';
		$SQL .= 'WHERE FRV_FRZ_KEY='.$_GET['FRZ_KEY'];
		
		if(!isset($_GET['FRVSort']))
		{
			$SQL .= ' ORDER BY FRV_DATUMVON,FRV_DATUMBIS ASC';
		}
		else
		{
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['FRVSort']);
		}
		
		$MaxDSAnzahl = awis_BenutzerParameter($con, 'AnzahlDatensaetzeProListe', $AWISBenutzer->BenutzerName());
		$rsFRV = awisOpenRecordset($con,$SQL);
		$rsFRVZeilen = $awisRSZeilen;
			
		if(($Recht3806&4)>0)
		{
			$Icons[] = array('new','./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$AWIS_KEY2.'&FRV_KEY=0');
			awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
		}
		elseif(($Recht3806&2)>0 or ($Recht3806&8)>0)
		{
			$Link = '';
			awis_FORM_Erstelle_Liste_Ueberschrift('',40,'',$Link);
		}
		$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$AWIS_KEY2;
		$Link .= '&FRVSort=KON_NAME1'.((isset($_GET['FRVSort']) AND ($_GET['FRVSort']=='KON_NAME1'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],200,'',$Link);
		$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$AWIS_KEY2;
		$Link .= '&FRVSort=FRV_DATUMVON'.((isset($_GET['FRVSort']) AND ($_GET['FRVSort']=='FRV_DATUMVON'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'],120,'',$Link);
		$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$AWIS_KEY2;
		$Link .= '&FRVSort=FRV_DATUMBIS'.((isset($_GET['FRVSort']) AND ($_GET['FRVSort']=='FRV_DATUMBIS'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'],100,'',$Link);
			
		awis_FORM_ZeileEnde();
	
		// Blockweise
		$StartZeile = 0;
		if (isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
		}	
		
		for ($FRVZeile = $StartZeile; $FRVZeile < $rsFRVZeilen and $FRVZeile < $StartZeile + $MaxDSAnzahl; $FRVZeile++)
		{
			//for($FRVZeile=0;$FRVZeile<$rsFRVZeilen;$FRVZeile++)
			//{
				awis_FORM_ZeileStart();
				$Icons = array();
				if(($Recht3806&2)>0)	// �ndernrecht
				{
					$Icons[] = array('edit','./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$AWIS_KEY2.'&FRV_KEY='.$rsFRV['FRV_KEY'][$FRVZeile]);
				}
				if(($Recht3806&8)>0)	// L�schen
				{
					$Icons[] = array('delete','./filialebenen_Main.php?cmdAktion=Details&Seite=Vertreter&DelFRV='.$rsFRV['FRV_KEY'][$FRVZeile]);
				}
				if(($Recht3806&2)>0 or ($Recht3806&4)>0 or ($Recht3806&8)>0)
				{
					awis_FORM_Erstelle_ListeIcons($Icons,38,($FRVZeile%2));
				}
				$Style='';
				if(awis_PruefeDatum($rsFRV['FRV_DATUMVON'][$FRVZeile],1,1) > strtotime(date('d.m.Y'),time()))
				{
					$Style='font-style:italic;color:#0266C8';
				}
				if(awis_PruefeDatum($rsFRV['FRV_DATUMBIS'][$FRVZeile],1,1) < strtotime(date('d.m.Y',time())))
				{
					$Style='font-style:italic;color:#FF2020';
				}
				awis_FORM_Erstelle_ListenFeld('#KON_NAME',$rsFRV['KON_NAME1'][$FRVZeile].', '.$rsFRV['KON_NAME2'][$FRVZeile],20,200,false,($FRVZeile%2),$Style,'','T');
				awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGAB',$rsFRV['FRV_DATUMVON'][$FRVZeile],30,120,false,($FRVZeile%2),$Style,'','D');
				awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGBIS',$rsFRV['FRV_DATUMBIS'][$FRVZeile],30,100,false,($FRVZeile%2),$Style,'','D');
				awis_FORM_ZeileEnde();
			}
			if ($rsFRVZeilen > $MaxDSAnzahl)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'], 50, '');
			
				for ($i = 0; $i < ($rsFRVZeilen / $MaxDSAnzahl); $i++)
				{
				if ($i != ($StartZeile / $MaxDSAnzahl))
				{
				$Text = '&nbsp;<a href=./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY=33028&Block=' . $i . (isset($_GET['FRVSort']) ? '&FRVSort=' . $_GET['FRVSort'] : '') . '>' . ($i + 1) . '</a>';
						awis_FORM_Erstelle_TextLabel($Text, 30, '');
				}
				else
				{
				$Text = '&nbsp;' . ($i + 1) . '';
						awis_FORM_Erstelle_TextLabel($Text, 30, '');
				}
				}
				awis_FORM_ZeileEnde();
			}
		}
		elseif ($_GET['FRZ_KEY'] != 0)
		{
			$SQL = 'SELECT * FROM FILIALEBENENROLLENVERTRETER ';
			$SQL .= 'INNER JOIN KONTAKTE ON KON_KEY = FRV_KON_KEY ';
			$SQL .= 'WHERE FRV_KEY='.$_GET['FRV_KEY'];
				
			$rsFRV = awisOpenRecordset($con,$SQL);
			$rsFRVZeilen = $awisRSZeilen;
				
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY=".$AWIS_KEY2." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
			if($rsFRVZeilen > 0)
			{
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsFRV['FRV_USER'][0]));
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsFRV['FRV_USERDAT'][0]));
			}
			awis_FORM_InfoZeile($Felder,'');
			
			echo '<input name=txtFRV_KEY type=hidden value=0'.(isset($rsFRV['FRV_KEY'][0])?$rsFRV['FRV_KEY'][0]:'').'>';
			echo '<input name=txtFRZ_KEY type=hidden value=0'.(isset($AWIS_KEY2)?$AWIS_KEY2:'').'>';
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_KON_KEY'].':',150);
			$SQL = 'select KON_KEY, KON_NAME1 || COALESCE(\', \'||KON_NAME2,\'\') AS KON_NAME ';
			$SQL .= ' FROM Kontakte ';
			$SQL .= ' WHERE (KON_STATUS=\'A\' OR KON_KEY = '.($_GET['FRV_KEY']==0?'0':$rsFRV['FRV_KON_KEY'][0]).')';
			$SQL .= ' AND KON_PER_NR IS NOT NULL';
			$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
			awis_FORM_Erstelle_SelectFeld('FRV_KON_KEY',($_GET['FRV_KEY']==0?(isset($_POST['txtFRV_KON_KEY'])?$_POST['txtFRV_KON_KEY']:''):(isset($_POST['txtFRV_KON_KEY'])?$_POST['txtFRV_KON_KEY']:$rsFRV['FRV_KON_KEY'][0])),300,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			awis_FORM_ZeileEnde();
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'].':',150);
			awis_FORM_Erstelle_TextFeld('FRV_GUELTIGAB',(isset($rsFRV['FRV_DATUMVON'][0])?$rsFRV['FRV_DATUMVON'][0]:(isset($_POST['txtFRV_GUELTIGAB'])?$_POST['txtFRV_GUELTIGAB']:'')),10,200,($Recht3806&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
			awis_FORM_ZeileEnde();
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'].':',150);
			awis_FORM_Erstelle_TextFeld('FRV_GUELTIGBIS',(isset($rsFRV['FRV_DATUMBIS'][0])?$rsFRV['FRV_DATUMBIS'][0]:''),10,200,($Recht3806&6),'','','','D','','',awis_PruefeDatum('31.12.2030'));
			awis_FORM_ZeileEnde();
			
		}
}
	awis_FORM_FormularEnde();

?>