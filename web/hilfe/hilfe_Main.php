<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');
$AWISBenutzer = awisBenutzer::Init();

// Dateiname zusammenbauen

$Verzeichnis = '';
if($AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING )!='')
{
	$Verzeichnis = '/FILIALE';
}
if(isset($_GET['ID']))
{
	$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().$Verzeichnis.'/'.$_GET['ID'].'.pdf';
	if(!is_file($DateiName))
	{
			$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().'/'.$_GET['ID'].'.pdf';
	}
	if(!is_file($DateiName))
	{
		$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().'/0.pdf';
	}
}
else
{
	$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().$Verzeichnis.'/'.$_GET['HilfeThema'].'_'.$_GET['Aktion'].'_'.strtolower($_GET['Seite']).'.pdf';
	if(!is_file($DateiName))
	{
		$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().$Verzeichnis.'/'.$_GET['HilfeThema'].'_'.$_GET['Aktion'].'.pdf';
		if(!is_file($DateiName))
		{
			$DateiName = $_SERVER['DOCUMENT_ROOT'].'/hilfe/'.$AWISBenutzer->BenutzerSprache().$Verzeichnis.'/'.$_GET['HilfeThema'].'.pdf';
			if(!is_file($DateiName))
			{
				$DateiName='';
			}
		}
	}
}


if($DateiName!='' and is_file($DateiName))
{
	ob_clean();
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header("Content-type-Header: application/pdf");
	header("Content-transfer-Encoding: binary");
	header("Content-Disposition: attachment; filename=AWIS-Hilfe.PDF");
	header("Content-type: application/pdf");

	readfile($DateiName);
}
else
{
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$Form = new awisFormular();
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TITEL','tit_Hilfe');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','HilfeOeffnen');
	$TextKonserven[]=array('Hilfe','%');
	$TextKonserven[]=array('Fehler','err_keineHilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	try
	{
		$Form = new awisFormular();

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Form->SchreibeHTMLCode('<table width=100%>');
		$Form->SchreibeHTMLCode('<tr><td width=50%><font style="font-size:larger;">AWIS - '.$AWISSprachKonserven['TITEL']['tit_Hilfe'].'</font></td>');
		$Form->SchreibeHTMLCode('<td style="text-align:right;"><img src="/bilder/atulogo_2008.png" border=0></td>');
		$Form->SchreibeHTMLCode('</tr></table>');
		$Form->ZeileEnde();

		$Form->Trennzeile('D');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Hilfe']['ModulHilfe'],0,'font-size:large;');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Fehler']['err_keineHilfe'],0,'Hinweis');
		$Form->ZeileEnde();

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Hilfe']['AllgemeineHilfe'],0,'font-size:large;');
		$Form->ZeileEnde();
		$DateiName = '/hilfe/'.$AWISBenutzer->BenutzerSprache().$Verzeichnis.'/allgemein.pdf';
		if(!is_file($_SERVER['DOCUMENT_ROOT'].$DateiName))
		{
			$DateiName='';
		}
		$Form->ZeileStart();
		if($DateiName!='')
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['HilfeOeffnen'],0,'',$DateiName);
		}
		else
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Fehler']['err_keineHilfe'],0,'Hinweis');
		}
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('script', 'cmdSchliessen', "onclick=window.close();", '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->SchaltflaechenEnde();
	}
	catch (Exception $ex)
	{
		if($Form instanceof awisFormular)
		{
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
		}
		else
		{
			echo 'AWIS: '.$ex->getMessage();
		}
	}
}
?>