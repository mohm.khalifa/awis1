<?php

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisPerseinsberichte
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	protected $_DB = null;
	
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	protected $_Form = null;

	/**
	 * AWIS Werkzeugobjekt
	 *
	 * @var awisWerkzeuge
	 */
	protected $_AWISWerkzeug = null;

	/**
	 * AWIS Benutzer
	 *
	 * @var awisBenutzer
	 */
	protected $_AWISBenutzer = null;

	/**
	 * Werkzeuge
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeuge = null;

	/**
	 * Debugausgaben
	 *
	 * @var int
	 */
	protected $_DebugLevel = 0;

    /**
     * Protokolldatei f�r die Ausgaben
     */
	const PROTOKOLLDATEI = '/var/log/preisabfragen.log';

	/**
	 * Konstruktor
	 *
	 * @param string $Benutzer
	 */
	public function __construct($Benutzer)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();
		
		$this->_Form = new awisFormular();

		$this->_AWISWerkzeug = new awisWerkzeuge();
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
	}

	/**
	 * Liest oder setzt den Debuglevel
	 *
	 * @param int $NeuerLevel
	 * @return int
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);

			if($this->_DebugLevel > 0)
			{
				file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Debuglevel auf '.$this->_DebugLevel.' gesetzt'.PHP_EOL,FILE_APPEND);
			}
		}

		return $this->_DebugLevel;
	}
	
	public function Auswertung_A()
	{		
		$fp = fopen('/daten/daten/pccommon/Riedl/Test.csv','w+');	
		
		//Alle Filialen selektieren
		$SQL = 'SELECT DISTINCT PEI_FIL_ID, FIL_BEZ ';
		$SQL.= ', filialrolle(PEI_FIL_ID,23,SYSDATE, \'Name\') AS FRZ_ROLLE_RL';		
		$SQL.= ', filialrolle(PEI_FIL_ID,25,SYSDATE ,\'Name\') AS FRZ_ROLLE_GBL';
		$SQL.= ' FROM PERSONALEINSAETZE ';
		$SQL.= ' LEFT JOIN FILIALEN ON FIL_ID = PEI_FIL_ID ';
		$SQL.= ' WHERE PEI_PBE_KEY = 1 AND PEI_PEZ_STATUS=1';
		$SQL.= ' ORDER BY PEI_FIL_ID';
		$rsFil = $this->_DB->RecordSetOeffnen($SQL);
		
		while (!$rsFil->EOF())
		{
			//Alle Bereichte zu der Filiale selektieren	
			$SQL = 'SELECT * FROM PERSONALEINSAETZE WHERE PEI_PBE_KEY = 1 AND PEI_FIL_ID='.$rsFil->FeldInhalt('PEI_FIL_ID').' AND PEI_PEZ_STATUS=1 ORDER BY PEI_DATUM';
			$rsPEI = $this->_DB->RecordSetOeffnen($SQL);
		
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,';');
			
			//�berschrift ausgeben
			while (!$rsPEI->EOF())
			{
				//fputs($fp,'Key:'.$rsPEI->FeldInhalt('PEI_KEY') .' - ');	
				fputs($fp,$this->_Form->Format('D', $rsPEI->FeldInhalt('PEI_DATUM')));
				fputs($fp,';');
				
				$rsPEI->DSWeiter();
			}
		
			
			//**********************************************************
			//A1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'A1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 2 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
				
			//**********************************************************
			//A2
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'A2');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 3 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 5 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B2
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B2');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 6 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B3
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B3');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 7 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B4
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B4');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 8 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B5
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B5');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 9 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//B6
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'B6');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 10 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//C1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'C1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 12 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//D1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'D1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 14 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//D2
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'D2');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 15 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//D3
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'D3');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 16 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//E1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'E1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 18 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			//**********************************************************
			//F1
			//**********************************************************
			fputs($fp,"\r\n");
			fputs($fp, 'Filiale: '.$rsFil->FeldInhalt('PEI_FIL_ID'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FIL_BEZ'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_RL'));
			fputs($fp,';');
			fputs($fp, $rsFil->FeldInhalt('FRZ_ROLLE_GBL'));
			fputs($fp,';');
			fputs($fp,'F1');
			fputs($fp,';');
		
			//zur�ck zum 1.
			$rsPEI->DSErster();
		
			while (!$rsPEI->EOF())
			{
				$SQL = 'select decode(pez_wert,1, \'OK\', 0, \'NICHT OK\', \'\') as wert from personaleinsberichtszuord ';
				$SQL.= ' where pez_pbf_id = 400 and pez_pei_key = '.$rsPEI->FeldInhalt('PEI_KEY');
				
				$rsPEZ = $this->_DB->RecordSetOeffnen($SQL);
				
				fputs($fp, $rsPEZ->FeldInhalt('WERT'));
				fputs($fp, ';');
				
				$rsPEI->DSWeiter();
			}
			
			fputs($fp,"\r\n");
			$rsFil->DSWeiter();
		}
	}
	
}