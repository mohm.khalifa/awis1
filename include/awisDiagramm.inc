<script>

</script>
<?php

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 01.12.2016
 * Time: 18:39
 */
class awisDiagramm
{
    private $_DiagrammName;
    private $_DiagrammTyp;

    function __construct()
    {

        echo ' <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <link rel="stylesheet" href="/css/3/awisDiagramm.css">
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
                 <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
                 <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>';
    }

    /**
     * Baut aus einem SQL ein Array f�r das Kreisdiagram,
     * @param        $SQL
     * @param        $Bindevariablen
     * @param string $Verbindung
     * @return json
     */
    private function _KreisDiagrammDaten($SQL,$Bindevariablen,$Verbindung='AWIS'){
        require_once 'awisDatenbank.inc';

        $DB = awisDatenbank::NeueVerbindung($Verbindung);
        $DB->Oeffnen();
        $Daten = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
        $Return = array();
        while(!$Daten->EOF()){

            $Return[] = array('label'=> $Daten->FeldInhalt(1),'value' => $Daten->FeldInhalt(2));

            $Daten->DSWeiter();
        }

        $Return = json_encode($Return);
        return $Return;
    }

    /**
     * Erstellt ein Kreisdiagramm
     *
     * @param        $Name
     * @param        $SQL
     * @param array  $Bindevariablen
     * @param        $Hoehe
     * @param        $Breite
     * @param string $Ueberschrift
     * @param string $Fusszeile
     * @param string $Verbindung
     */
    public function ErstelleKreisDiagramm($Name, $SQL,$Bindevariablen=array(), $Hoehe, $Breite, $Ueberschrift = '',$Fusszeile='',$Verbindung='AWIS')
    {

        echo '<div class="DiagrammBox" style="height: ' . $Hoehe . 'px; width: ' . $Breite . 'px">';

        if($Ueberschrift!=''){
            echo '<div class="DiagrammUeberschrift">' . $Ueberschrift . '</div>';
        }

        echo '<div class="DiagrammInhalt" id="' . $Name . '"></div>';

        echo '<script>' . PHP_EOL;
        echo 'var ' . $Name . ' = new Morris.Donut({' . PHP_EOL;
        echo ' element: \'' . $Name . '\', ';
        echo ' data: ' . $this->_KreisDiagrammDaten($SQL,$Bindevariablen,$Verbindung);
        echo '   }); ';

        echo ' </script> ';

        if($Fusszeile!=''){
            echo '<div class="DiagrammFusszeile">' . $Fusszeile . '</div>';
        }

        echo '</div>'; //DiagrammBox
    }

    /**
     * Erstellt ein Liniendiagramm
     * @param        $Name
     * @param        $DatenSeite
     * @param array  $ZusatzFelder ist noch nicht ausprogrammiert
     * @param        $Hoehe
     * @param        $Breite
     * @param        $xKey
     * @param        $yKey
     * @param string $xBezeichnung
     * @param string $yBezeichnung
     * @param string $Ueberschrift
     * @param string $Fusszeile
     * @param int    $SekRefresh
     */
    public function ErstelleLinienDiagramm($Name, $DatenSeite,array $ZusatzFelder = array(), $Hoehe, $Breite, $xKey, $yKey, $xBezeichnung = '', $yBezeichnung = '', $Ueberschrift = '',$Fusszeile='',$SekRefresh=0)
    {
        echo '<div class="DiagrammBox" style="height: ' . $Hoehe . 'px; width: ' . $Breite . 'px">';

        if($Ueberschrift!=''){
            echo '<div class="DiagrammUeberschrift">' . $Ueberschrift . '</div>';
        }

        echo '<div class="DiagrammInhalt" id="' . $Name . '"></div>';

        echo '<script>' . PHP_EOL;
        echo 'var ' . $Name . ' = new Morris.Line({' . PHP_EOL;
        echo ' element: \'' . $Name . '\', ';
        echo ' parseTime: false, ' . PHP_EOL; //Morris kommt mit deutschen Datums nicht zurecht
        echo ' resize: true, ' . PHP_EOL;
        echo ' hideHover: \'auto\', ' . PHP_EOL; //Labels nur bei MouseOver anzeigen
        echo ' data: [{'.$xKey.': 0,'.$yKey.': 0} ], '; //Daten kommen durch Ajax nachgeladen
        echo 'xkey: \'' . $xKey . '\', ';
        echo '  ykeys: [\'' . $yKey . '\'], ';
        echo '  labels: [\'' . $yBezeichnung . '\'] ';
        echo '   }); ';

        if($SekRefresh > 0){
            echo 'setInterval(" AktualisiereDiagramm(' . $Name . '); ", '.$SekRefresh*1000 . ');';
        }

        echo ' AktualisiereDiagramm(' . $Name . ');
            
                function AktualisiereDiagramm(diagramm){
                    $.ajax({
                    type: "GET",
                    dataType: \'json\',
                    url: "/daten/diagramme/'.$DatenSeite.'.php",
                    data: {  }
                    })
                    .done(function( data ) {
                 
                    //Antwort ans Diagramm senden und hoffen, dass alles gut geht
                    diagramm.setData(data  );
                    })
                    .fail(function() {
                    alert( "Unerwarteter Fehler" );
                    });
                }
            </script> 
';

        if($Fusszeile!=''){
            echo '<div class="DiagrammFusszeile">' . $Fusszeile . '</div>';
        }

        echo '</div>'; //DiagrammBox
    }
}