<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	ini_set('max_execution_time',600);

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PRH','%');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	// Parameter
	$Trenner=";";

	@ob_clean();

	//header("Cache-Control: no-cache, must-revalidate");
	//header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="parameterhistorie_export.csv"');
	
	
	if(isset($_GET['ID']))                  // Parameter kommen base64 kodiert
	{
		// Parameter werden durch & getrennt
		$ParameterListe = explode('&',base64_decode($_GET['ID']));
		foreach($ParameterListe AS $Parameter)
		{
			// Jeder Parameter hat das Format <Param>=<Operator>~<Wert>
			$Parameter = explode('=',$Parameter);
			$Param[$Parameter[0]]=(isset($Parameter[1])?urldecode($Parameter[1]):'');
		}
	}
	else
	{
		die();
	}
	
	$FeldlisteUeberschrift = 'PRH_DATUM,PRH_TABELLE,PRH_ID,PRH_AKTION,PRH_BEARBEITER';
	$FeldListeSelect = 'DATUM, TABLE_NAME, ID, ACTION, UPDATEDBY';
	
	$FeldWerteUeberschrift='';
	foreach (explode(',', $FeldlisteUeberschrift) as $value)
	{
		$FeldWerteUeberschrift = $FeldWerteUeberschrift.$AWISSprachKonserven['PRH'][$value].';';
	}
	$FeldWerteUeberschrift = rtrim($FeldWerteUeberschrift, ';');

	$SQL = ' SELECT ' . $FeldListeSelect;
	$SQL .= ' FROM ZZZ_TEST_COND_HISTORIE ';
	
	// Alle Parameter auswerten
	// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
	//			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
	$Bedingung = "";

	
	if(isset($Param['PRH_DATUM_VON']) AND $Param['PRH_DATUM_VON']!='')
	{
		$Bedingung .= ' AND DATUM >= ' . $DB->FeldInhaltFormat('D',$Param['PRH_DATUM_VON']);
	}
	if (isset($Param['PRH_DATUM_BIS']) AND $Param['PRH_DATUM_BIS'] != '')
	{
		$Bedingung .= ' AND DATUM <= ' . $DB->FeldInhaltFormat('D',$Param['PRH_DATUM_BIS']);
	}
	
	if(isset($Param['PRH_TABELLE']) AND $Param['PRH_TABELLE']!='')
	{
		$Bedingung .= ' AND TABLE_NAME = ' . $DB->FeldInhaltFormat('T',$Param['PRH_TABELLE']);
	}
	if(isset($Param['PRH_ID']) AND $Param['PRH_ID']!='')
	{
		$Bedingung .= ' AND ID = ' . $DB->FeldInhaltFormat('N0',$Param['PRH_ID']);
	}
	
	if(isset($Param['PRH_AKTION']) AND $Param['PRH_AKTION']!='')
	{
		$Bedingung .= ' AND ACTION = ' . $DB->FeldInhaltFormat('T',$Param['PRH_AKTION']);
	}
	
	if(isset($Param['PRH_BEARBEITER']) AND $Param['PRH_BEARBEITER']!='')
	{
		$Bedingung .= ' AND UPDATEDBY = ' . $DB->FeldInhaltFormat('T',$Param['PRH_BEARBEITER']);
	}
	
	if ($Bedingung != "")
	{
		$SQL .= " WHERE " . substr($Bedingung, 4);
	}

	$SQL .= " Order by DATUM DESC";
	
	$Abfrage = $SQL;
	// Blockgr��e festlegen
	$Blockgroesse = 5000;

	for($i=1;$i<999;$i++)
	{
		$SQL = 'SELECT ABFRAGE2.* FROM (';
		$SQL .= 'SELECT ABFRAGE1.*, row_number() over(order by 1) AS zeile';
		$SQL .= ' FROM ('.$Abfrage.') ABFRAGE1';
		$SQL .= ') ABFRAGE2 WHERE zeile >= '.(($i-1)*$Blockgroesse).' AND zeile < '.(($i)*$Blockgroesse);

		$rsDaten = $DB->RecordSetOeffnen($SQL);

		if($rsDaten->AnzahlDatensaetze()==0)
		{
			break;
		}
		if($i==1)		// Beim ersten Mal den Kopf schreiben
		{
			$Felder = $rsDaten->SpaltenNamen();
			$Zeile = '';
			
			$Zeile = $FeldWerteUeberschrift;
			echo $Zeile."\n";
		}

		while(!$rsDaten->EOF())
		{
			$Zeile = '';
			foreach($Felder AS $FeldName)
			{
				if($FeldName!='ZEILE')
				{
					$Text = str_replace("\n",'',$rsDaten->FeldInhalt($FeldName));
					$Text = str_replace("\r",'',$Text);
					$Text = str_replace("\n",'',$Text);
					$Text = str_replace("\t",'',$Text);
					$Zeile .= $Trenner.$Text;
				}
			}
			echo substr($Zeile,1)."\n";

			$rsDaten->DSWeiter();
		}
	}
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>