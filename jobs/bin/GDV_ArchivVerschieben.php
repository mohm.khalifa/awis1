<?php

require_once('awisDatenbank.inc');

$objArchiv = new GDV_ArchivVerschieben();
$objArchiv->verschiebeVorgangsdateien();


class GDV_ArchivVerschieben
{
	private $_DB;
	private $_quellPfad = '/daten/daten/versicherungen/zu_archivieren/';
	private $_pfadarchiv = '/daten/daten/versicherungen/archiv/gdv_dateien/';

	private $_filid;
	private $_vorgangnr;

	public function __construct()
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();
	}

	public function verschiebeVorgangsdateien()
	{
		$quelldateien = scandir($this->_quellPfad);

		foreach($quelldateien as $datei)
		{
			$dateiInfo = pathinfo($this->_quellPfad . $datei);
				
			if($dateiInfo['filename'] != '' && $dateiInfo['filename'] != '.' && $dateiInfo['filename'] != '..')
			{
				$posUnterstrich = strpos($dateiInfo['filename'], '_');

				$filid = substr($dateiInfo['filename'],0,$posUnterstrich);
				$this->_filid = $filid;

				$vorgang_nr = substr($dateiInfo['filename'],$posUnterstrich + 1);
				$this->_vorgangnr = $vorgang_nr;

				$PfadZiel = $this->_pfadarchiv . $filid . '/';
				
				try
				{
					$this->verschiebeDatei($this->_quellPfad, $PfadZiel, $dateiInfo['filename']);
				}
				catch (Exception $e)
				{
					echo 'Fehler beim verschieben der Datei' . $dateiInfo['filename'] . ' Message: ' . $e->getMessage() . '\n';
				}
			}
		}
	}

	private function verschiebeDatei($PfadQuelle, $PfadZiel, $DateiName)
	{
		if(!file_exists($PfadZiel))
		{
			mkdir($PfadZiel);
		}

		if(file_exists($PfadQuelle . $DateiName))
		{
			if(!copy($PfadQuelle . $DateiName, $PfadZiel . $DateiName))
			{
				throw new Exception('Fehler beim kopieren der Datei');
			}
			else
			{
				if(file_exists($PfadZiel . $DateiName))
				{
					if(!unlink($PfadQuelle . $DateiName))
					{
						throw new Exception('Fehler beim loeschen der Datei');
					}
				}
			}
		}
	}





}

?>