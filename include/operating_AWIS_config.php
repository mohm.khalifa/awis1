<?php

require_once("db.inc.php");

global $AWISWebSrv;
global $AWISAppSrv;

$Level = awisLeseAWISParameter('awis.conf','AWIS_LEVEL');
	
switch($Level)
{
	case 'PROD':
		$AWISWebSrv = "https://awis.server.atu.de";
		$AWISAppSrv = "svapplx013.server.atu.de";
		break;
	case 'STAG':
		$AWISWebSrv = "https://awis-staging.server.atu.de";
		$AWISAppSrv = "svapplx015.server.atu.de";
		break;
	case 'ENTW':
		$AWISWebSrv = "https://awis-entwick.server.atu.de";
		$AWISAppSrv = "svapplx015.server.atu.de";
		break;
	default:
		$AWISWebSrv = "https://awis-entwick.server.atu.de";
		$AWISAppSrv = "svapplx015.server.atu.de";
		break;
}

?>
