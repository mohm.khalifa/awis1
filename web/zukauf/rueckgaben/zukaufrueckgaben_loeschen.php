<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtZUL_KEY']))
		{
			$Tabelle = 'ZUL';
			$Key=$_POST['txtZUL_KEY'];

			$SQL = 'SELECT ZUL_KEY, ZUL_NUMMER, ZUL_DATUM';
			$SQL .= ' FROM Zukauflieferscheine ';
			$SQL .= ' WHERE ZUL_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$ZULKey = $rsDaten->FeldInhalt('ZUL_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ZUL','ZUL_NUMMER'),$rsDaten->FeldInhalt('ZUL_NUMMER'));
			$Felder[]=array($Form->LadeTextBaustein('ZUL','ZUL_DATUM'),$rsDaten->FeldInhalt('ZUL_DATUM'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Rueckgaben':
					$Tabelle = 'ZRU';
					$Key=$_GET['Del'];
					

					$SQL = 'SELECT ZRU_KEY, ZRU_ZUL_KEY, ZRU_DATUM, ZRU_MENGE';
					$SQL .= ' FROM Zukaufrueckgaben ';
					$SQL .= ' WHERE ZRU_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);
					
					$ZULKey=$rsDaten->FeldInhalt('ZRU_ZUL_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZRU','ZRU_DATUM'),$rsDaten->FeldInhalt('ZRU_DATUM'));
					$Felder[]=array($Form->LadeTextBaustein('ZRU','ZRU_MENGE'),$rsDaten->FeldInhalt('ZRU_MENGE'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'ZUL':
				$SQL = 'DELETE FROM Zukauflieferantenhersteller WHERE ZUL_key=0'.$_POST['txtKey'];
				$AWIS_KEY1='';
				break;
			case 'ZRU':
				$SQL = 'DELETE FROM Zukaufrueckgaben WHERE ZRU_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtZULKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('zukaufrueckgaben_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('ZULKey',$ZULKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>