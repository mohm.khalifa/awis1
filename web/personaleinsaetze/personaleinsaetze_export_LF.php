<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$ExportFormat = 1;
	$Anzahl = 0;
  
	$DateiName = 'Filialbericht_GBL';
	  
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	 
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
	        break;
	    case 2:                 // Excel 2007
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
	        break;
	}
	 
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setTitle(utf8_encode('Filialbericht GBL'));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode('Filialbericht GBL'));
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode('Filialbericht GBL'));
	$XLSXObj->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	
	
	$SQLPEIKEY  ='select pbf_pbf_id,xtx_text_de,pez_wert,pez_bemerkung,PEI_DATUM,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS,PEI_FIL_ID from personaleinsaetze ';
	$SQLPEIKEY .='inner join Personaleinsberichtsfelder ON pei_pbe_key = pbf_pbe_key ';
	$SQLPEIKEY .='left join Personaleinsberichtszuord on pbf_id = pez_pbf_id and Pei_Key = Pez_Pei_Key ';
	$SQLPEIKEY .='inner join textkonserven ON XTX_KENNUNG = PBF_BEZEICHNUNG AND XTX_BEREICH = \'LF\' ';
	$SQLPEIKEY .='inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
	$SQLPEIKEY .='where pei_key ='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	$SQLPEIKEY .=' AND PBF_PBF_ID not in (406)';
	$SQLPEIKEY .=' AND PBF_ID <> 406';
	$SQLPEIKEY .=' ORDER BY PBF_SORTIERUNG';
	
	//echo $SQLPEIKEY;
	$rsPEI = $DB->RecordSetOeffnen($SQLPEIKEY);
	
	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	
	//Überschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y H:i:s',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$SpaltenNr=0;
	$ZeilenNr=3;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'Filiale: '.$rsPEI->FeldInhalt('PEI_FIL_ID'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	
	$SpaltenNr=0;
	$ZeilenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'Zeit : '.$rsPEI->FeldInhalt('ZEITVON').' Uhr - '.$rsPEI->FeldInhalt('ZEITBIS').' Uhr');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	
	$SpaltenNr=0;
	$ZeilenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'Datum: '.substr($rsPEI->FeldInhalt('PEI_DATUM'),0,10));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	
	$SpaltenNr=0;
	$ZeilenNr++;
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'GBL: '.utf8_encode(FuelleFelder(407)));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	

	$SpaltenNr=0;
	$ZeilenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'GL: '.utf8_encode(FuelleFelder(408)));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$SpaltenNr=0;
	$ZeilenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'WL: '. utf8_encode(FuelleFelder(409)));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	
	$ZeilenNr=10;
	$SpaltenNr = 1;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Antwort');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Bemerkung');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	while(!$rsPEI->EOF())
	{
		$SpaltenNr = 0;
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($rsPEI->Feldinhalt('XTX_TEXT_DE')),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getAlignment()->setWrapText(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$XLSXObj->getActiveSheet()->getColumnDimensionByColumn($SpaltenNr)->setWidth(50);
		
		if($rsPEI->Feldinhalt('PBF_PBF_ID') == 0)
		{
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		}
		else 
		{
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(false);
		}
		
		$SpaltenNr++;
		
		if($rsPEI->Feldinhalt('PEZ_WERT') == '1')
		{
			$Wert = 'ok';
		}
		else if($rsPEI->Feldinhalt('PEZ_WERT') == '0')
		{
			$Wert = 'nicht ok';
		}
		else
		{
			$Wert = '';
		}
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Wert),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getColumnDimensionByColumn($SpaltenNr)->setWidth(8);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($rsPEI->Feldinhalt('PEZ_BEMERKUNG')),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getAlignment()->setWrapText(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$XLSXObj->getActiveSheet()->getColumnDimensionByColumn($SpaltenNr)->setWidth(50);
		
		//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		
		
		$ZeilenNr++;
		
		$rsPEI->DSWeiter();
	}
	

	
	 //for($S='A';$S<='H';$S++)
	 //{
	   //$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	 //}
	 
	
	
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="Filialbericht_GBL.xls"');
	        $DateiEndung = '.xls';
	        break;
	    case 2:                 // Excel 2007
	         
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="Filialbericht_GBL.xlsx"');
	        $DateiEndung = '.xlsx';
	        break;
	}
	 
	

	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
	        break;
	    case 2:                 // Excel 2007
	        $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
	        break;
	}
	//var_dump($Lagerkennung);
	//$objWriter->save('/daten/daten/pccommon/Gebhardt/' . $DateiName . '.xls');
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
	$Anzahl++;
	$rsPEI->DSWeiter();
	


}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}

function FuelleFelder($ID)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;

	$rsAuswertung = $DB->RecordSetOeffnen($SQL);

	return $rsAuswertung->FeldInhalt('PEZ_WERT');
}


?>