<? 
require_once('assoc_array2xml.php');
require_once("register.inc.php");		// DB-Befehle

function awis_FilialNachbarnPlz($txtPlz, $anzahl = 0)
{
	global $con;

	$converter= new assoc_array2xml;
	if($anzahl == 0)
	{
		$anzahl = awis_BenutzerParameter($con, "FilialNachbarn", $_SERVER['PHP_AUTH_USER']);
	}
	if($anzahl == 0)
	{
		$anzahl = 1;
	} 
	$url = "http://filialsuche.server.atu.de/filialsuche?plz=" . $txtPlz . "&lkz=D&anzahl=" . $anzahl;
	$fp = @file($url);

	$nix = "";
	for ($i=0; $i <= count($fp); $i++)
	{
		$nix .= chop($fp[$i]);
	}

	$filialen=$converter->xml2array($nix, 'filiale');

	$result = $filialen["suchergebnis"]["results"]["filiale1"]["nr"];
	if ($filialen["suchergebnis"]["status"] == "OK")
	{
		for ($i=2; $i <= $anzahl; $i++)
		{
			$tmpFil = $filialen["suchergebnis"]["results"]["filiale" . $i];
			$result .= ", " . $tmpFil["nr"];
// 			echo $tmpFil["name"] . " (" . $tmpFil["nr"] . ")<br>";
		}
	}
	else
	{
		echo "Alles Mist!";
		die();
	}

	return $result;
}

function awis_FilialNachbarnFilID($txtFilID, $anzahl = 0)
{
	global $con;
	global $awisRSZeilen;

	$SQL = "Select FIL_PLZ from Filialen ";
	$SQL .= "where fil_id = " . $txtFilID;
	$rsResult = awisOpenRecordset($con, $SQL);
	if ($awisRSZeilen <> 1)
	{
		echo "Fehler in awis_FilialNachbarnFilID";
		return 0;
	}
	else
	{
		return awis_FilialNachbarnPlz($rsResult["FIL_PLZ"][0], $anzahl);
	}
}
// echo "<hr>";
// var_dump($filialen["suchergebnis"]["results"]);

?>
