<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'versvermittlungen_funktionen.inc';

    global $AWISCursorPosition;
    global $VBA;

    try {
    $VBA = new versvermittlungen_funktionen();

    echo "<link rel=stylesheet type=text/css href='" . $VBA->AWISBenutzer->CSSDatei(3) . "'>";

    if(isset($_POST['cmdExportXLSX_x']))
    {
        include('./versvermittlungen_exportieren.php');
    }

    echo '<title>' . $VBA->AWISSprachKonserven['TITEL']['tit_versvermittlungen'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$VBA->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(9800);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$VBA->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($VBA->Form instanceof awisFormular) {
        $VBA->Form->DebugAusgabe(1, $ex->getSQL());
        $VBA->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $VBA->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($VBA->Form instanceof awisFormular) {
        $VBA->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>