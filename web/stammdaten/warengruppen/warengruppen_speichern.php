<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $WG = new warengruppen_funktionen();

    $Meldung = '';

    //##########################################################
    //Lieferantenanlage
    //##########################################################
    if (isset($_GET['WGI_LIE_NEU']) and $_POST['txtLAR_LIE_NR'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $SQL = 'select count(*)+1 as WertigkeitNeu from V_WARENGRUPPEN_LIE_NR';
        $SQL .= ' where WGI_WGR_ID = ' . $DB->WertSetzen('NEU', 'T', $AWIS_KEY1);
        if ($AWIS_KEY2 != '') {
            $SQL .= ' and WGI_WUG_ID = ' . $DB->WertSetzen('NEU', 'T', $AWIS_KEY2);
        }
        $rsNeu = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('NEU', true));

        //Lieferant inserten
        $WGI_KEY = $WG->insertWGI(2, $_POST['txtLAR_LIE_NR']);
        //Wertigkeit inserten
        if ($WGI_KEY != false) {
            $WG->insertWGI(1, $rsNeu->FeldInhalt(1), $WGI_KEY);
            $Meldung = $AWISSprachKonserven['WGI']['WGI_SPEICHERN_OK'];
        }
    }

    //##########################################################
    //Lieferantenreihenfolge aktualisieren
    //##########################################################
    $NeueReihenfolge = $Form->NameInArray($_POST, 'newLIE_NR', 2, 1);
    if (count($NeueReihenfolge) > 0) {
        foreach ($NeueReihenfolge as $Feld) {
            $alt = $_POST['old' . substr($Feld, 3)];
            $neu = $_POST['new' . substr($Feld, 3)];
            $WGI_KEY = substr($Feld, 9);

            if ($alt != $neu) {
                $WG->updateWGI($WGI_KEY, $neu);
                $Meldung = $AWISSprachKonserven['WGI']['WGI_SPEICHERN_OK'];
            }
        }
    }

    if ($Meldung != '') {
        $Form->Hinweistext($Meldung);
    }
} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>