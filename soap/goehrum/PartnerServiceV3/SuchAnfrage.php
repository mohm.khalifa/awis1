<?php

class SuchAnfrage extends Serialize
{

    /**
     * @var int $Kundennummer
     */
    protected $Kundennummer = null;

    /**
     * @var ArrayOfSuchAnfrageArtikel $Anfrage
     */
    protected $Anfrage = null;

    /**
     * @param int $Kundennummer
     */
    public function __construct($Kundennummer)
    {
      $this->Kundennummer = $Kundennummer;
    }

    /**
     * @return int
     */
    public function getKundennummer()
    {
      return $this->Kundennummer;
    }

    /**
     * @param int $Kundennummer
     * @return SuchAnfrage
     */
    public function setKundennummer($Kundennummer)
    {
      $this->Kundennummer = $Kundennummer;
      return $this;
    }

    /**
     * @return ArrayOfSuchAnfrageArtikel
     */
    public function getAnfrage()
    {
      return $this->Anfrage;
    }

    /**
     * @param ArrayOfSuchAnfrageArtikel $Anfrage
     * @return SuchAnfrage
     */
    public function setAnfrage($Anfrage)
    {
      $this->Anfrage = $Anfrage;
      return $this;
    }

}
