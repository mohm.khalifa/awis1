<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{
	$TextKonserven = array();
	$TextKonserven[]=array('CRM','%');
	$TextKonserven[]=array('CRM','wrd_ZusatzText%');
	$TextKonserven[]=array('CRM','wrd_AktionSchreiben');
	$TextKonserven[]=array('CAD','txt_SucheBetreuer%');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Wort','txt_Konditionsmodell');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();			
		
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht15101 = $AWISBenutzer->HatDasRecht(15101);
	if($Recht15101==0)
	{
		$Form->Fehler_KeineRechte();
	}


	$Fehler=false;

	if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='')
	{
		if ($_POST['sucFIL_ID']=='' or $_POST['sucWGR_ID']=='')
		{		
			$Fehler=true;
		}	
		else
		{
			$Param = array();
			$Param['FIL_ID']=$_POST['sucFIL_ID'];
			$Param['WGR_ID']=$_POST['sucWGR_ID'];
			
			$AWISBenutzer->ParameterSchreiben("Formular_Reifen_Preislistendruck",serialize($Param));		
		}
	}
	else 
	{
		$Param = array();
		$Param['FIL_ID']='';
		$Param['WGR_ID']='';
		
		$AWISBenutzer->ParameterSchreiben("Formular_Reifen_Preislistendruck",serialize($Param));		
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen("Formular_Reifen_Preislistendruck"));	

	$Form->SchreibeHTMLCode('<form name=frmMassendruck method=post action=./reifen_Main.php?cmdAktion=Preislisten>');

	$Form->Formular_Start();

	if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='' AND $Fehler==false)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Preisliste �ffnen',200,'','./reifen_DruckPreisliste.php?Druckart=2');
		$Form->ZeileEnde();
		$Form->Trennzeile();
	}

	//Pr�fen, ob Filiale angemeldet ist
	$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if ($UserFilialen!='')
	{
		echo '<input type=hidden name=sucFIL_ID value='.$UserFilialen.'>';	
	}
	else 
	{
		$Form->ZeileStart();
		//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',250);
		$Form->Erstelle_TextLabel('Filiale:',250);
		//awis_FORM_Erstelle_TextFeld('*FIL_ID',(isset($Param['FIL_ID'])?$Param['FIL_ID']:''),10,50,true);
		$Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,50,true);
		$Form->ZeileEnde();
	}

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Warengruppe:',250);
	$SQL = "select WGR_ID, WGR_BEZEICHNUNG";
	$SQL .= ' FROM Warengruppen';
	$SQL .= ' WHERE WGR_ID IN (\'01\', \'02\', \'23\')';
	$SQL .= ' ORDER BY WGR_BEZEICHNUNG';
	//awis_FORM_Erstelle_SelectFeld('*WGR_ID',(isset($Param['WGR_ID'])?$Param['WGR_ID']:''),250,true,$con,$SQL);
	$Form->Erstelle_SelectFeld('*WGR_ID',$Param['WGR_ID'],250,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	
	$Form->Trennzeile();
	
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	$Form->SchaltflaechenEnde();
	
	$Form->Formular_Ende();
	
	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201103300848");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201103300848");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>