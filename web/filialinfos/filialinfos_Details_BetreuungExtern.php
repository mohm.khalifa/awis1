<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FilialeGeschlossen;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');	
	$TextKonserven[]=array('FAB','%');
	$TextKonserven[]=array('FFA','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Link_Route');
	$TextKonserven[]=array('Wort','Link_Maps');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5018= $AWISBenutzer->HatDasRecht(5018);		// Externe Betreuung
	
	if(($Recht5018&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILFFA'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	//$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
	$INET_LINK_Route =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Route');

	if(isset($_GET['FFA_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['FFA_KEY']);
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY FAB_BEREICH, FAB_NAME1';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}
	
	$SQL = 'SELECT FFA.*, FAB.*';	
	$SQL.= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL.= ' FROM FACHBETRIEBEFILIALEN FFA';	
	$SQL.= ' LEFT JOIN FACHBETRIEBE FAB ON FFA_FAB_KEY = FAB_KEY';	
	$SQL.= ' WHERE trunc(FFA_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FFA_GUELTIGBIS) >= trunc(SYSDATE)';
	$SQL.= ' AND FFA_FIL_ID = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND FFA_KEY = '.$AWIS_KEY2;
	}

	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_FILFFA',serialize($Param));
		}
		elseif(isset($Param['BLOCK']) AND $Param['FIL_ID']==$AWIS_KEY1)
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		if($Block==0)
		{
			$Block=1;
		}
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$Form->DebugAusgabe(1,$SQL);
	$rsFFA = $DB->RecordSetOeffnen($SQL);

	$Param['FIL_ID']=$AWIS_KEY1;
	$AWISBenutzer->ParameterSchreiben('Formular_FILFFA',serialize($Param));
	
	if (isset($_GET['ListeFFA']) or !isset($_GET['FFA_KEY']))
	{
		$DetailAnsicht = false;

		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['FAB_BEREICH']=100;
			$Breiten['FAB_NAME1']=250;
			$Breiten['FAB_TELEFON']=150;
			$Breiten['FAB_TELEFAX']=150;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['FAB_BEREICH']=100;
			$Breiten['FAB_NAME1']=250;
			$Breiten['FAB_TELEFON']=150;
			$Breiten['FAB_TELEFAX']=150;
		}
		else
		{
			$Breiten['FAB_BEREICH']=100;
			$Breiten['FAB_NAME1']=250;
			$Breiten['FAB_TELEFON']=150;
			$Breiten['FAB_TELEFAX']=150;
		}

		$Form->Formular_Start();

		$Form->ZeileStart();
		
		if(($Recht5018&4)==4)
		{
			$Icons = array();
			$Icons[] = array('new','./filialinfos_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&FFA_KEY=-1','g');
			$Form->Erstelle_ListeIcons($Icons,38,0);
		}				

		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FAB_BEREICH'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FAB_BEREICH'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_BEREICH'],$Breiten['FAB_BEREICH'],'',$Link);		
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FAB_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FAB_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_NAME1'].'/'.$AWISSprachKonserven['FAB']['FAB_ANSPRECHP1'],$Breiten['FAB_NAME1'],'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FAB_TELEFON'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FAB_TELEFON'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_TELEFON'],$Breiten['FAB_TELEFON'],'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FAB_TELEFAX'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FAB_TELEFAX'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_TELEFAX'],$Breiten['FAB_TELEFAX'],'',$Link);
		$Form->ZeileEnde();

		//Nur F�r Routenanzeige		
		$SQL = 'SELECT FIL_STRASSE, FIL_ORT, FIL_PLZ, LAN_CODE';
		$SQL .= ' FROM Filialen';
		$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		$SQL .= ' WHERE FIL_ID = '.$AWIS_KEY1;
		$rsFIL = $DB->RecordSetOeffnen($SQL);	
		
		$DS=0;		
		while(!$rsFFA->EOF())
		{			
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			if((($Recht5018&2)==2) or (($Recht5018&8)==8) or (($Recht5018&4)==4))
			{
				$Icons = array();
			
				if(($Recht5018&2)==2)
				{
					$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FFA_KEY='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
				}
				if(($Recht5018&8)==8)
				{
					$Icons[] = array('delete','./filialinfos_Main.php?cmdAktion=Details&Seite=BetreuungExtern&Del='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
				}
						
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			}
			
			$Link = '';						

			$Form->Erstelle_ListenFeld('FAB_BEREICH',$rsFFA->FeldInhalt('FAB_BEREICH'),0,$Breiten['FAB_BEREICH'],false,($DS%2),'','','T','L','');
			if ($rsFFA->FeldInhalt('FAB_BEREICH')=='Buchbinder')
			{
					$Form->Erstelle_ListenFeld('FAB_ANSPRECHP1',$rsFFA->FeldInhalt('FAB_ANSPRECHP1'),0,$Breiten['FAB_NAME1'],false,($DS%2),'','','T','L','');
			}
			else 
			{
				$Form->Erstelle_ListenFeld('FAB_NAME1',$rsFFA->FeldInhalt('FAB_NAME1'),0,$Breiten['FAB_NAME1'],false,($DS%2),'','','T','L','');
			}
			
			$EditModus = $FilialeGeschlossen==1?0:(($Recht5018&16)==16);
			$FeldName = 'FAB_TELEFON' .$rsFFA->FeldInhalt('FAB_KEY');
			$Form->Erstelle_ListenFeld($FeldName,$rsFFA->FeldInhalt('FAB_TELEFON'),0,$Breiten['FAB_TELEFON'],$EditModus,($DS%2),'','','T','L','');
			$FeldName = 'FAB_TELEFAX' .$rsFFA->FeldInhalt('FAB_KEY');
			$Form->Erstelle_ListenFeld($FeldName,$rsFFA->FeldInhalt('FAB_TELEFAX'),0,$Breiten['FAB_TELEFAX'],$EditModus,($DS%2),'','','T','L','');			

			$Parameter = array('$NAME1'=>urlencode($rsFFA->FeldInhalt('FAB_NAME1')),
						   '$NAME2'=>'',			//urlencode($rsFFA->FeldInhalt('FAB_NAME2')),
						   '$STRASSE'=>urlencode($rsFFA->FeldInhalt('FAB_STRASSE')),
						   '$HAUSNUMMER'=>'',
						   '$PLZ'=>urlencode($rsFFA->FeldInhalt('FAB_PLZ')),
						   '$ORT'=>urlencode($rsFFA->FeldInhalt('FAB_ORT')),
						   '$LAN_CODE'=>urlencode($rsFFA->FeldInhalt('FAB_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Karte,$Parameter);
			$Form->Erstelle_ListenBild('href','#MAPS',$Link,'/bilder/icon_globus.png',$AWISSprachKonserven['Wort']['Link_Maps'],($DS%2),'',16,16,20,'','NEU');
			$Parameter = array(
							'$STRASSE1'=>urlencode($rsFIL->FeldInhalt('FIL_STRASSE')),
						   	'$HAUSNUMMER1'=>'',
						   	'$PLZ1'=>urlencode($rsFIL->FeldInhalt('FIL_PLZ')),
						   	'$LAND1'=>urlencode($rsFIL->FeldInhalt('LAN_CODE')),
							'$STRASSE2'=>urlencode(utf8_encode($rsFFA->FeldInhalt('FAB_STRASSE'))),
						   	'$HAUSNUMMER2'=>'',
						   	'$PLZ2'=>urlencode($rsFFA->FeldInhalt('FAB_PLZ')),
						   	'$LAND2'=>urlencode($rsFFA->FeldInhalt('FAB_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Route,$Parameter);
			$Form->Erstelle_ListenBild('href','#MAPS',$Link,'/bilder/icon_route.png',$AWISSprachKonserven['Wort']['Link_Route'],($DS%2),'',16,16,20,'','NEU');
			$Form->ZeileEnde();

			$rsFFA->DSWeiter();
			$DS++;
		}

		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else
	{
		$AWIS_KEY2 = $rsFFA->FeldInhalt('FFA_KEY');

		$Param['KEY']=$AWIS_KEY2;
		$AWISBenutzer->ParameterSchreiben('Formular_FILFFA',serialize($Param));

		echo '<input type=hidden name=txtFFA_KEY value='.$AWIS_KEY2. '>';
		echo '<input type=hidden name=txtFFA_FIL_ID value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./filialinfos_Main.php?cmdAktion=Details&ListeFFA".(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFFA->FeldInhalt('FFA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFFA->FeldInhalt('FFA_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$EditModus = ($FilialeGeschlossen==1?0:($Recht5018&6));
		$Form->Formular_Start();

		$Form->ZeileStart();

		$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_NAME1'].':',150,'','');
		$SQL = 'SELECT FAB_KEY, FAB_NAME1 || \', \' || FAB_STRASSE || \', \' || FAB_PLZ || \', \' || FAB_ORT';
		$SQL .= ' FROM Fachbetriebe';		
		$SQL .= ' ORDER BY FAB_NAME1';
		$Form->Erstelle_SelectFeld('FFA_FAB_KEY',$rsFFA->FeldInhalt('FFA_FAB_KEY'),200,$EditModus,$SQL,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FFA']['FFA_GUELTIGAB'].':',150);
		$Form->Erstelle_TextFeld('!FFA_GUELTIGAB',$rsFFA->FeldInhalt('FFA_GUELTIGAB'),10,150,$EditModus,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FFA']['FFA_GUELTIGBIS'].':',100);
		$Form->Erstelle_TextFeld('!FFA_GUELTIGBIS',$rsFFA->FeldInhalt('FFA_GUELTIGBIS'),10,150,$EditModus,'','','','D');
		$Form->ZeileEnde();
				
		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002241128");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002241128");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>