<?php
/**
 * Unterseite unter Lieferantenartikel(-sets)
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   20080420
 *
 */
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LAR', '%');
    $TextKonserven[] = array('LIE', 'LIE_NAME1');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht452 = $AWISBenutzer->HatDasRecht(452);            // Recht f�r die Lieferantenartikel
    $Recht451 = $AWISBenutzer->HatDasRecht(451);            // Recht f�r die Lieferantenartikel

    if ($Recht452 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Bedingung = '';
    $SQL = 'SELECT Lieferantenartikel.*, LIE_NAME1, LIE_NAME2, TEI_USER, TEI_USERDAT, TEI_KEY';
    $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 1 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=0' . $AWIS_KEY1 . ' AND ROWNUM=1) AS ALI_WERT_1';
    $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 2 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=0' . $AWIS_KEY1 . ' AND ROWNUM=1) AS ALI_WERT_2';
    $SQL .= ' FROM Lieferantenartikel';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY1 = LAR_KEY and TEI_KEY2 = 0' . $AWIS_KEY2 . ' ';
    $SQL .= ' LEFT OUTER JOIN Lieferanten ON LAR_LIE_NR = LIE_NR ';
    $SQL .= ' LEFT OUTER JOIN ArtikelPruefPersonal ON APP_ID = LAR_APP_ID';

    if (($Recht451 & 1) != 1) {
        $Bedingung .= ' AND TEI_USER=\'WWS\'';
    }

    // Artikel von gesperrten Lieferanten nicht anzeigen
    if (($Recht452 & 2048) != 2048) {
        $Bedingung .= ' AND (SELECT LIN_WERT FROM LieferantenInfos WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY = 17) IS NULL';
    }

    if (isset($_GET['LAS_KEY'])) {
        $Bedingung .= ' AND LAR_KEY = 0' . $DB->FeldInhaltFormat('Z', $_GET['LAS_KEY']);
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['LARSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['LARSort']);
    } else {
        $SQL .= ' ORDER BY  lar_lie_nr, lar_lartnr';
    }
    $rsLAR = $DB->RecordSetOeffnen($SQL);

    if (!isset($_GET['LAS_KEY']))                    // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();

        if (($Recht452 & 4)) {
            $Icons[] = array(
                'new',
                './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel&LAS_KEY=0',
                'g',
                $AWISSprachKonserven['LAR']['wrd_LieferantenartikelHinzufuegen']
            );
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel';
        $Link .= '&LARSort=LAR_LARTNR' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LAR_LARTNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_LARTNR'], 220, '', $Link);
        if ($MitReklInfo) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_ReklInfo'], 50);
        }
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel';
        $Link .= '&LARSort=LAR_LIE_NR' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LAR_LIE_NR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_LIE_NR'], 100, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel';
        $Link .= '&LARSort=LIE_NAME1' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LIE_NAME1'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'], 400, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_LARTeileInfo'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_Aenderungsdatum'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_LARZuordnung'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_REKLAMATIONSKENNUNG'], 30);

        $Form->ZeileEnde();

        $DS = 0;
        $rsLARZeilen = $rsLAR->AnzahlDatensaetze();
        $rsLAR = $rsLAR->Tabelle();

        for ($LARZeile = 0; $LARZeile < $rsLARZeilen; $LARZeile++) {
            $ZweitArtikel = false;
            $LAR = array();
            $LAR[0] = array($rsLAR['TEI_USER'][$LARZeile], $rsLAR['TEI_USERDAT'][$LARZeile], $rsLAR['TEI_KEY'][$LARZeile]);
            $LAR[1] = array_fill(0, 3, '');
            // Zwei gleiche Artikel, Nur die Benutzer sind unterschiedlich
            if (isset($rsLAR["LAR_LARTNR"][$LARZeile + 1]) AND $rsLAR["LAR_LARTNR"][$LARZeile] == $rsLAR["LAR_LARTNR"][$LARZeile + 1] AND $rsLAR['LAR_LIE_NR'][$LARZeile] == $rsLAR['LAR_LIE_NR'][$LARZeile + 1]) {
                $LAR[($rsLAR['TEI_USER'] == 'WWS')?0:1] = array($rsLAR['TEI_USER'][$LARZeile], $rsLAR['TEI_USERDAT'][$LARZeile], $rsLAR['TEI_KEY'][$LARZeile]);
                $LAR[($rsLAR['TEI_USER'] == 'WWS')?0:1] = array($rsLAR['TEI_USER'][$LARZeile + 1], $rsLAR['TEI_USERDAT'][$LARZeile + 1], $rsLAR['TEI_KEY'][$LARZeile + 1]);
                $ZweitArtikel = true;    // Kennzeichnen, dass es zwei Zuordnungen gibt
                $LARZeile++;
            }

            $Form->ZeileStart();
            $Icons = array();

            if (($Recht452 & (2 + 4 + 64)) != 0)    // �ndernrecht
            {
                $Icons[] = array(
                    'edit',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel&LAS_KEY=' . $rsLAR['LAR_KEY'][$LARZeile]
                );
            }
            if (($Recht452 & 8))    // �ndernrecht
            {
                if ($LAR[0][0] != 'WWS' OR $LAR[1][0] != '')        // WWS Daten k�nnen nicht gel�scht werden
                {
                    $LARDELKEY = ($LAR[0][0] != 'WWS'?$LAR[0][2]:$LAR[1][2]);
                    $Icons[] = array(
                        'delete',
                        './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0' . $AWIS_KEY2 . '&Unterseite=Lieferantenartikel&Del=' . $LARDELKEY . '&Key=' . $AWIS_KEY1
                    );
                }
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

            // Lieferantenset suchen
            $SQL = 'SELECT COUNT(T1.TEI_SUCH1) AS Teile';
            $SQL .= " FROM TeileInfos T1 ";
            $SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
            $SQL .= " AND T1.TEI_WERT2 " . $DB->LIKEoderIST($rsLAR['LAR_LARTNR'][$LARZeile]);

            $rsLAS = $DB->RecordsetOeffnen($SQL);
            if ($rsLAS->FeldInhalt('TEILE') > 0) {
                $ToolTipp = str_replace('$1', $rsLAS->FeldInhalt('TEILE'), $AWISSprachKonserven['LAR']['ttt_SetBestehendAus']);
                $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAR['LAR_LARTNR'][$LARZeile], 20, 190, false, ($DS % 2), 'font-style:italic;', '', 'T', 'L', $ToolTipp);
                $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAS->FeldInhalt('TEILE'), 2, 30, false, ($DS % 2), '', '', 'T', 'L', $ToolTipp);
            } else {
                $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAR['LAR_LARTNR'][$LARZeile], 22, 220, false, ($DS % 2), '', '', 'T');
            }

            // Reklamationsinfo anzeigen
            if ($MitReklInfo) {
                $Link = '';
                $DateiName = realpath('../dokumente/lieferanten/reklamationen') . '/lief_rekl_' . $rsLAR['LAR_LIE_NR'][$LARZeile] . '.pdf';
                if (file_exists($DateiName)) {
                    $Link = '/dokumente/lieferanten/reklamationen/lief_rekl_' . $rsLAR['LAR_LIE_NR'][$LARZeile] . '.pdf';
                    $Form->Erstelle_ListenBild('href', '*LAR_INFO', $Link, '/bilder/pdf.png', $AWISSprachKonserven['LAR']['ttt_ReklInfo'], ($DS % 2), '', 16, 16, 50, 'C');
                } else {
                    $Form->Erstelle_ListenFeld('*filler', '', 0, 50, false, ($DS % 2));
                }
            }

            $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $rsLAR['LAR_LIE_NR'][$LARZeile];
            $Form->Erstelle_ListenFeld('*LAR_LIE_NR', $rsLAR['LAR_LIE_NR'][$LARZeile], 10, 100, false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('*LIE_NAME1', $rsLAR['LIE_NAME1'][$LARZeile], 10, 400, false, ($DS % 2), '', '', 'T');

            // Zusatzinfo in Kurzform
            $Anzeige = '';
            if ($rsLAR["LAR_ALTERNATIVARTIKEL"][$LARZeile] != 0) {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_AKT'];
            }
            if ($rsLAR["LAR_AUSLAUFARTIKEL"][$LARZeile] != 0) {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_AUSL'];
            }
            /*		if($rsLAR["LAR_ALTELIEFNR"][$LARZeile]!=0)
                    {
                        $Anzeige .= ', '.$AWISSprachKonserven['LAR']['txt_LARTeileInfo_ALT'];
                    }
            */
            if ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] != 0) {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_PROB'];
            }
            if ($rsLAR["LAR_GEPRUEFT"][$LARZeile] != 0) {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_GEPR'];
            }
            if ($rsLAR["LAR_MUSTER"][$LARZeile] != 0) {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_M'];
            }
            if ($rsLAR["LAR_BEMERKUNGEN"][$LARZeile] . '' != '') {
                $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_B'];
            }
            $Form->Erstelle_ListenFeld('*ZUSATZ', substr($Anzeige, 2), 10, 100, false, ($DS % 2), '', '', 'T');

            $UserNamenTooltipp = ($LAR[1][1] == ''?$LAR[0][1]:$LAR[0][1] . '/' . $LAR[1][1]);
            $Form->Erstelle_ListenFeld('*TEI_USERDAT', ($LAR[1][1] == ''?$LAR[0][1]:$LAR[1][1]), 10, 100, false, ($DS % 2), '', '', 'D', '', $UserNamenTooltipp);
            // Benutzernamen abk�rzen, damit es in die Liste passt. Den vollst�ndigen Namen als ToolTipp
            $UserNamenTooltipp = ($LAR[0][0] . ($LAR[1][0] == ''?'':'/' . $LAR[1][0]));
            $UserNamen = ($LAR[0][0] . (substr($LAR[1][0], 0, 5) == ''?'':'/' . substr($LAR[1][0], 0, 5)));
            $Form->Erstelle_ListenFeld('*USER', $UserNamen, 10, 100, false, ($DS % 2), '', '', 'T', '', $UserNamenTooltipp);

            $Form->Erstelle_ListenFeld('*ALI_WERT_2', $rsLAR['ALI_WERT_2'][$LARZeile], 10, 30, false, ($DS % 2), 'color:#FF0000', '', 'T', 'C',
                $AWISSprachKonserven['LAR']['ttt_LAR_REKLAMATIONSKENNUNG']);

            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('*FILLER', '', 10, 258, false, ($DS % 2));
            $Form->Erstelle_ListenFeld('LAR_BEZWW', $rsLAR['LAR_BEZWW'][$LARZeile], 10, 604, false, ($DS % 2));
            $Form->ZeileEnde();

            $DS++;
        }
        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtLAS_KEY type=hidden value=0' . $rsLAR->FeldInhalt('LAR_KEY') . '>';
        // Importquelle muss bei der �nderung unbedingt eine 4 bekommen!
        echo '<input name=txtLAS_IMQ_ID type=hidden value=' . (intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) | 4) . '>';
        echo '<input name=oldLAS_IMQ_ID type=hidden value=' . (($rsLAR->FeldInhalt('LAR_IMQ_ID') != '')?$rsLAR->FeldInhalt('LAR_IMQ_ID'):'0') . '>';

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0" . $AWIS_KEY2 . "&Unterseite=Lieferantenartikel&LARListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsLAR->FeldInhalt('LAR_USER')));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsLAR->FeldInhalt('LAR_USERDAT')));
        $Form->InfoZeile($Felder, '');

        $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
        $rsLAR->DSWeiter();
        $NaechsterLAR = $rsLAR->Datensatz();
        if ($rsLAR->EOF()) {
            $rsLAR->DSLetzter();
        } else {
            $rsLAR->DSZurueck();
        }

        $EditModus = ($NaechsterLAR['TEI_USER'] != ''?false:($rsLAR->FeldInhalt('TEI_USER') != 'WWS'?($Recht452 & 6):false));
        $EditModus2 = ($Recht452 & 6);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_LARTNR'] . ':', 150);
        $Form->Erstelle_TextFeld('LAS_LARTNR', $rsLAR->FeldInhalt('LAR_LARTNR'), 20, 200, ((intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) & 2) == 0?$EditModus:false));
        $AWISCursorPosition = ($EditModus?'txtLAS_LARTNR':$AWISCursorPosition);
        $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . ($rsLAR->FeldInhalt('LAR_LIE_NR') != ''?$rsLAR->FeldInhalt('LAR_LIE_NR'):0);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_LIE_NR'] . ':', 150, '', $Link);
        $SQL = 'SELECT LIE_NR, LIE_NR || \' - \' || LIE_NAME1 AS Anzeige FROM Lieferanten ORDER BY LIE_NAME1';
        $Form->Erstelle_SelectFeld('LAS_LIE_NR', $rsLAR->FeldInhalt('LAR_LIE_NR'), 0, $EditModus, $SQL);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEMERKUNGEN'] . ':', 150);
        $Form->Erstelle_Textarea('LAS_BEMERKUNGEN', $rsLAR->FeldInhalt('LAR_BEMERKUNGEN'), 0, 100, 4, ($EditModus | $EditModus2));
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_REKLAMATIONSKENNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('LAS_REKLAMATIONSKENNUNG', $rsLAR->FeldInhalt('LAS_REKLAMATIONSKENNUNG'), 2, 50, $EditModus);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_ALTERNATIVARTIKEL'] . ':', 150, 'text-align:right');
        $Form->Erstelle_SelectFeld('LAS_ALTERNATIVARTIKEL', $rsLAR->FeldInhalt('LAR_ALTERNATIVARTIKEL'), 70, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_AUSLAUFARTIKEL'] . ':', 150, 'text-align:right');
        $Form->Erstelle_SelectFeld('LAS_AUSLAUFARTIKEL', $rsLAR->FeldInhalt('LAR_AUSLAUFARTIKEL'), 70, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEKANNTWW'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAR_BEKANNTWW', ((intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) & 2) == 0?'0':'1'), 0, false, '', '', '', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_MUSTER'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAS_MUSTER', $rsLAR->FeldInhalt('LAR_MUSTER'), 70, $EditModus, '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_GEPRUEFT'] . ':', 150);
        $Form->Erstelle_TextFeld('LAS_GEPRUEFT', $rsLAR->FeldInhalt('LAR_GEPRUEFT'), 10, 100, ($EditModus | $EditModus2), '', '', '', 'D');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_APP_ID'] . ':', 150, 'text-align:right');
        $SQL = 'SELECT APP_ID, APP_NAME FROM Artikelpruefpersonal';
        $SQL .= ' WHERE (app_id = 0' . $rsLAR->FeldInhalt('LAR_APP_ID') . ' OR app_aktivbis > SYSDATE)';
        $SQL .= ' ORDER BY app_name';
        $Form->Erstelle_SelectFeld('LAS_APP_ID', $rsLAR->FeldInhalt('LAR_APP_ID'), 0, ($EditModus | $EditModus2), $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);

        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEZWW'] . ':', 150, '', '', $rsLAR->FeldInhalt('ALI_WERT_1'));
        $Form->Erstelle_TextFeld('LAS_BEZWW', $rsLAR->FeldInhalt('LAR_BEZWW'), 80, 0, ($EditModus | $EditModus2), '', '', '', 'T', 'L',
            $AWISSprachKonserven['LAR']['ttt_LAR_BEZWW'], '', 50);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_PROBLEMARTIKEL'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAS_PROBLEMARTIKEL', $rsLAR->FeldInhalt('LAR_PROBLEMARTIKEL'), 70, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_PROBLEMBESCHREIBUNG'] . ':', 150);
        $Form->Erstelle_Textarea('LAS_PROBLEMBESCHREIBUNG', $rsLAR->FeldInhalt('LAR_PROBLEMBESCHREIBUNG'), 0, 100, 4, ($EditModus | $EditModus2));
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060007");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>