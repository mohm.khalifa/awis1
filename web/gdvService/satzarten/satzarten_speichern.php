<?php
/**
 * Speichern Stammdatenpflege GDV-Satzarten
 *
 * @author Sch�ffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */

require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $Fehler;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{

	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$NachrichtenTyp = '';

	if (isset($FelderNew) and $FelderNew != '') //Neuanlage
	{
		if(isset($_POST['txtGDN_NACHRICHTENTYPEN']) and $_POST['txtGDN_NACHRICHTENTYPEN'] != ''){
			$SQL = 'select GDN_NUMMER from gdvnachrichtentypen ';
			$SQL .= ' where gdn_key ='.$_POST['txtGDN_NACHRICHTENTYPEN'];
			$NachrichtenTyp = $rsNT = $DB->RecordSetOeffnen($SQL);
		}
		else
		{
			$NachrichtenTyp = '';
		}


			$SQL =' insert into verssatzarten ';
			$SQL .=' (VSA_VERSNR,VSA_GDN_NUMMER,VSA_GDS_NUMMER,VSA_USER,VSA_USERDAT)';
			$SQL .=' values';
			$SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtAWIS_KEY1']);
			$SQL .=', '.$DB->FeldInhaltFormat('N0',$NachrichtenTyp);
			$SQL .=', '.$DB->FeldInhaltFormat('N0',$_POST['txtGDN_SATZARTEN']);
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate ';
			$SQL .= ' )';


			$DB->Ausfuehren($SQL,'',true);
			$Fehler = false;
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
