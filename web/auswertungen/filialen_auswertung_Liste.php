<?php
//*********************************
// Auswertung Gesamtliste
//*********************************
// Variablen
global $_POST;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);

$FeldListe = array('FIL_ID','EROEFFNUNG','FIL_BEZ','LAN_LAND', 'FIL_STRASSE', 'FIL_PLZ', 'FIL_ORT', 'TELEFON',
	'FAXNUMMER', 'GL1', 'GL2', 'GEBIETSLEITER', 'VERKAUFSLEITER',  'MO_FR_VON', 'MO_FR_BIS',
	'SA_VON' , 'SA_BIS', 'FIL_GRUPPE', 'FIL_LAGERKZ', 'REGIONALZENTRUM', 'GEBIET', 'TKDL');

print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=Liste>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	$SQL = 'SELECT Filialen.*, LAN_CODE, LAN_LAND, GEBIETSLEITER, VERKAUFSLEITER';
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=34 AND FIF_IMQ_ID=32) AS Eroeffnung";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=1 AND FIF_IMQ_ID=2) AS TELEFON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=2 AND FIF_IMQ_ID=2) AS FAXNUMMER";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=23 AND FIF_IMQ_ID=2) AS GL1";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=24 AND FIF_IMQ_ID=2) AS GL2";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=121 AND FIF_IMQ_ID=32) AS MO_FR_VON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=122 AND FIF_IMQ_ID=32) AS MO_FR_BIS";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=123 AND FIF_IMQ_ID=32) AS SA_VON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=124 AND FIF_IMQ_ID=32) AS SA_BIS";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=26) AS Regionalzentrum ,
			(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=21) AS Gebiet ,
			NVL(
				(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=28),
				(SELECT Mitarbeiter.MIT_BEZEICHNUNG FROM Mitarbeiter INNER JOIN VerkaufsGebiete ON MIT_KEY = VKG_TKDL_MIT_KEY
					WHERE
				MIT_REZ_ID=(SELECT FIF_WERT FROM (SELECT FIF_WERT, FIF_FIL_ID FROM FilialInfos WHERE FIF_FIT_ID=26 ORDER BY FIF_IMQ_ID) WHERE FIF_FIL_ID=Filialen.FIL_ID AND ROWNUM=1)
				AND VKG_ID=(SELECT FIF_WERT FROM (SELECT FIF_WERT, FIF_FIL_ID FROM FilialInfos WHERE FIF_FIT_ID=21 ORDER BY FIF_IMQ_ID) WHERE FIF_FIL_ID=Filialen.FIL_ID AND ROWNUM=1)
				)) AS TKDL";
	$SQL .= ' FROM Filialen INNER JOIN  V_FIL_VKL_GBL ON V_FIL_VKL_GBL.FIL_ID = Filialen.FIL_ID';
	$SQL .= ' LEFT OUTER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' WHERE FIL_LAN_WWSKENN IS NOT NULL';
	$SQL .= ' ORDER BY Filialen.FIL_ID';
//awis_Debug(1,$SQL);
	$rsFIL= awisOpenRecordset($con, $SQL);
	$rsFILZeilen= $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	foreach($FeldListe AS $Feld)
	{
		fputs($fd,$Feld.";");
	}
	fputs($fd,"\n");

	for($DSNr=0;$DSNr<$rsFILZeilen;$DSNr++)
	{
		foreach($FeldListe AS $Feld)
		{
			fputs($fd, $rsFIL[$Feld][$DSNr] . ";");
		}

		fputs($fd,"\n");
	}
	fclose($fd);

	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Liste mit wichtigen Daten zur Filiale.</li>';
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

