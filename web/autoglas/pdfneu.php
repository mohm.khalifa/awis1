<?php

/**
 * Funktions-/Scriptbeschreibung:
 * Script erzeugt eine PDF-Datei mit einer Liste �ber alle aktuell gesperrten AFG-Vorg�nge
 *
 */

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once 'fpdf.php';
//require_once 'awisBerichte.inc';

class firstglassGesperrt
{
	private $_DB;
	private $_fpdf;
	private $_recKopf;
	private $_recPos;
	private $_recKasse;
	
	public function __construct()
	{
		$this->_DB = awisDatenbank::NeueVerbindung ( 'AWIS' );
		$this->_DB->Oeffnen ();
		$this->_fpdf = new FPDF();
	}
	public function ListeDaten()
	{
		//*****************************************
		// Daten f�r Liste laden
		//*****************************************
		$SQL = 'SELECT * ';
		$SQL .= 'FROM fgkopfdaten a INNER JOIN ';
		$SQL .= '(SELECT fgk_vorgangnr, kassedatum ';
		$SQL .= '  FROM (SELECT distinct fgk_kennung, fgk_vorgangnr, min(fka_datum) as kassedatum ';
		$SQL .= '         FROM fgkopfdaten a ';
		$SQL .= '         INNER JOIN fkassendaten b ON a.fgk_vorgangnr = b.fka_aemnr ';
		$SQL .= '         WHERE fgk_fgn_key = 6 ';
		$SQL .= '         GROUP BY fgk_kennung, fgk_vorgangnr ';
		$SQL .= '         ORDER BY kassedatum)) b ON a.fgk_vorgangnr = b.fgk_vorgangnr ';
		$SQL .= 'ORDER BY kassedatum';
		$this->_recKopf = $this->_DB->RecordSetOeffnen($SQL);
		
		$SQL = 'SELECT * ';
		$SQL .= 'FROM fgpositionsdaten ';
		$SQL .= 'WHERE fgp_fgk_key = ' . $this->_recKopf->FeldInhalt('FGK_KEY');
		$this->_recPos = $this->_DB->RecordSetOeffnen($SQL);
		
		$SQL = 'SELECT * ';
		$SQL .= 'FROM fkassendaten ';
		$SQL .= 'WHERE fka_aemnr = \'' . $this->_recKopf->FeldInhalt('FGK_VORGANGNR') . '\'';
		$this->_recKasse = $this->_DB->RecordSetOeffnen($SQL);
		
		$this->ErzeugePDF();
	}
	public function ErzeugePDF()
	{
		$Spalte = 15;
		$Zeile = 30;
		
		$zwischenSumPos = 0;
		$endSumPos = 0;
		$anzahl = 0;
		$oempreis = 0;
		$steuersatz = 0;
		
		$zwischenSumKasse = 0;
		$endSumKasse = 0;
		$wanr = '';
		$datumKasse = '';
		
		// PDF Attribute setzen
		$this->_fpdf->SetTitle('FirstGlassGesperrt');
		$this->_fpdf->SetMargins(10, 10, 10);
		$this->_fpdf->SetFont('Arial', 'B', 10);
		$this->_fpdf->AliasNbPages();
		$this->_fpdf->SetFillColor(210,210,210);
	
		// Seite erstellen
		$this->NeueSeite();
	
		while (!$this->_recKopf->EOF())
		{
			while (!$this->_recPos->EOF())
			{
				$anzahl = str_replace(',', '.', $this->_recPos->FeldInhalt('FGP_ANZAHL'));
				$oempreis = str_replace(',', '.', $this->_recPos->FeldInhalt('FGP_OEMPREIS'));
				$zwischenSumPos = $zwischenSumPos + ($anzahl * $oempreis);
				$this->_recPos->DSWeiter();
			}
			$steuersatz = $this->_recKopf->FeldInhalt('FGK_STEUERSATZ');
			$endSumPos = round($zwischenSumPos * (1 + ($steuersatz / 100)), 2);
			
			// Kassendaten aufbereiten (-> Summe bilden wenn bereits Umbuchen stattfanden und daher zu einem Vorgang mehrere KassenDS vorhanden sind
			
			while (!$this->_recKasse->EOF())
			{
				$arrSearch = array(',', '-');
				$arrReplace = array('.', '');
			
				$zwischenSumKasse = $zwischenSumKasse + (str_replace($arrSearch, $arrReplace, $this->_recKasse->FeldInhalt('FKA_MENGE')) * str_replace(',', '.', $this->_recKasse->FeldInhalt('FKA_BETRAG')));
				$wanr = $this->_recKasse->FeldInhalt('FKA_WANR');
				$datumKasse = $this->_recKasse->FeldInhalt('FKA_DATUM');
			
				$this->_recKasse->DSWeiter();
			}
			$datumK = substr($datumKasse, 0, -8);
			$endSumKasse = round($zwischenSumKasse, 2);
			$diffBetrag = round($endSumKasse - $endSumPos, 2);
			
			$this->_fpdf->setXY($Spalte, $Zeile);
			
			$ursprungx = $this->_fpdf->GetX();
			$ursprungy = $this->_fpdf->GetY();
			
			$this->_fpdf->cell(35, 5, $this->_recKopf->FeldInhalt('FGK_VORGANGNR'), 0, 0, 'L', 0);
			//Betrag (Kasse)
			$this->_fpdf->cell(15, 5, $endSumKasse, 0, 0, 'R', 0);
			$this->_fpdf->cell(15, 5, $wanr, 0, 0, 'L', 0);
			$this->_fpdf->cell(15, 5, $endSumPos, 0, 0, 'R', 0);
			$this->_fpdf->cell(15, 5, $diffBetrag, 0, 0, 'R', 0);
			$this->_fpdf->cell(45, 5, substr($this->_recKopf->FeldInhalt('FGK_VERSICHERUNG'), 0, 20), 0, 0, 'L', 0);
			$this->_fpdf->cell(25, 5, $datumK, 0, 0, 'C', 0);
			
			$this->_fpdf->SetFont('Courier', 'B', 7);
			$this->_fpdf->Multicell(105, 5, $this->_recKopf->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
			$this->_fpdf->SetFont('Arial', 'B', 10);
			
			$multicellY = $this->_fpdf->GetY();
			
			$h�he = $multicellY - $ursprungy;
			
			$this->_fpdf->setXY($Spalte, $Zeile);
			
			$this->_fpdf->cell(35, $h�he, '', 'LTRB', 0, 'L', 0);
			$this->_fpdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
			$this->_fpdf->cell(15, $h�he, '', 'LTRB', 'L', 0);
			$this->_fpdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
			$this->_fpdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
			$this->_fpdf->cell(45, $h�he, '', 'LTRB', 'L', 0);
			$this->_fpdf->cell(25, $h�he, '', 'LTRB', 'C', 0);
			$Zeile += $h�he;
			
			$this->_recKopf->DSWeiter();
		}
	
		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
	}
	private function NeueSeite()
	{
		$this->_fpdf->AddPage();
	}
	public function Footer()
	{
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		
	}
	private function Ueberschrift()
	{
		//�berschrift
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['TabelleUeberschrift-Groesse']*1);
		$this->Cell(15,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_FIL_ID']),1,0,'R',1);
		$this->Cell(60,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['FIL']['FIL_BEZ']),1,0,'L',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADS']['ADS_EWCCODE_kurz']),1,0,'L',1);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_DATUMABSCHLUSS']),1,0,'R',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADP']['ADP_MENGE']),1,0,'R',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['ADK']['ADK_LAGERKZ']),1,0,'R',1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*1.4;
	}
}
/*
ini_set('max_execution_time', 300);

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[] = array('FGK', '%');
$TextKonserven[] = array('FKH', '%');
$TextKonserven[] = array('FKA', '%');
$TextKonserven[] = array('FGP', '%');
$TextKonserven[] = array('FPH', '%');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

//-----------------------------------------------------------------------------------------------------------
// Seitenparameter festlegen und Listen�berschrift erstellen



$Ausdruck = new awisAusdruck('L', 'A4', array('BriefpapierATU_DE_Seite_2_quer.pdf'));
$Ausdruck->NeueSeite(0, 1);
$this->_fpdf->SetFont('Arial', 'B', 10);

$Spalte = 15;
$Zeile = 30;
$count = 0;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(35, 5, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 5, 'Kasse', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(15, 5, 'WANR', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 5, 'FG', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(15, 5, 'Diff.', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(45, 5, 'Versicherung', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(25, 5, 'Datum(Kasse)', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(105, 5, 'Bemerkung', 'LTRB', 0, 'L', 0);
//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// Daten f�r Liste selektieren und Berechnen und Listenzeilen erstellen

$SQL = "SELECT * FROM fgkopfdaten a INNER JOIN ";
$SQL .= "(select fgk_vorgangnr, kassedatum ";
$SQL .= "from ";
$SQL .= "(select distinct fgk_kennung, fgk_vorgangnr, min(fka_datum) as kassedatum ";
$SQL .= "from fgkopfdaten a inner join fkassendaten b on a.fgk_vorgangnr = b.fka_aemnr ";
$SQL .= "where fgk_fgn_key = 6 group by fgk_kennung, fgk_vorgangnr order by kassedatum)) b ON a.fgk_vorgangnr = b.fgk_vorgangnr ";
$SQL .= "order by kassedatum";

$rsKopfdaten = $DB->RecordSetOeffnen($SQL);


while (!$rsKopfdaten->EOF())
{
    // Positionsdaten aufbereiten (-> Summe bilden + Steuer hinzuaddieren)
    $SQL = 'SELECT * FROM fgpositionsdaten WHERE fgp_fgk_key = ' . $rsKopfdaten->FeldInhalt('FGK_KEY');

    $rsPosDaten = $DB->RecordSetOeffnen($SQL);

    $zwischenSumPos = 0.00;
    $endSumPos = 0.00;

    while (!$rsPosDaten->EOF())
    {
        $zwischenSumPos = $zwischenSumPos + (str_replace(',', '.', $rsPosDaten->FeldInhalt('FGP_ANZAHL')) * str_replace(',', '.', $rsPosDaten->FeldInhalt('FGP_OEMPREIS')));

        $rsPosDaten->DSWeiter();
    }

    $endSumPos = round($zwischenSumPos * (1 + ($rsKopfdaten->FeldInhalt('FGK_STEUERSATZ') / 100)), 2);

    // Kassendaten aufbereiten (-> Summe bilden wenn bereits Umbuchen stattfanden und daher zu einem Vorgang mehrere KassenDS vorhanden sind
    $SQL = 'SELECT * FROM fkassendaten WHERE fka_aemnr = \'' . $rsKopfdaten->FeldInhalt('FGK_VORGANGNR') . '\'';

    $rsKassenDaten = $DB->RecordSetOeffnen($SQL);

    $zwischenSumKasse = 0.00;
    $endSumKasse = 0.00;
    $wanr = '';
    $datumKasse = '';

    while (!$rsKassenDaten->EOF())
    {
        $arrSearch = array(',', '-');
        $arrReplace = array('.', '');

        $zwischenSumKasse = $zwischenSumKasse + (str_replace($arrSearch, $arrReplace, $rsKassenDaten->FeldInhalt('FKA_MENGE')) * str_replace(',', '.', $rsKassenDaten->FeldInhalt('FKA_BETRAG')));
        $wanr = $rsKassenDaten->FeldInhalt('FKA_WANR');
        $datumKasse = $rsKassenDaten->FeldInhalt('FKA_DATUM');

        $rsKassenDaten->DSWeiter();
    }
    $datumK = substr($datumKasse, 0, -8);
    $endSumKasse = round($zwischenSumKasse, 2);
    $diffBetrag = round($endSumKasse - $endSumPos, 2);

    //---------------------------------------------------------------------------
    // Listenzeile erstellen
    
    if ($count == 0)
    {
        $Zeile += 5;
    }
    

    if ($count < 13)
    {
    	$Ausdruck->_pdf->setXY($Spalte, $Zeile);

        $ursprungx = $Ausdruck->_pdf->GetX();
        $ursprungy = $Ausdruck->_pdf->GetY();
        
        $Ausdruck->_pdf->cell(35, 5, $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), 0, 0, 'L', 0);
        //Betrag (Kasse)
        $Ausdruck->_pdf->cell(15, 5, $endSumKasse, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, $wanr, 0, 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, $endSumPos, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, $diffBetrag, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(45, 5, substr($rsKopfdaten->FeldInhalt('FGK_VERSICHERUNG'), 0, 20), 0, 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 5, $datumK, 0, 0, 'C', 0);

        $Ausdruck->_pdf->SetFont('Courier', 'B', 7);
        $Ausdruck->_pdf->Multicell(105, 5, $rsKopfdaten->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
        $Ausdruck->_pdf->SetFont('Arial', 'B', 10);
        
        $multicellY = $Ausdruck->_pdf->GetY();
        
        $h�he = $multicellY - $ursprungy;
        
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        
        $Ausdruck->_pdf->cell(35, $h�he, '', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'L', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
		$Ausdruck->_pdf->cell(45, $h�he, '', 'LTRB', 'L', 0);
        $Ausdruck->_pdf->cell(25, $h�he, '', 'LTRB', 'C', 0);
        $Zeile += $h�he;
        
    }
    else
    {
        // neue Seite beginnt (count zur�cksetzen und Listen�berschrift ausgeben)
        $Zeile = 30;
        $count = 0;
        $Ausdruck->NeueSeite(0, 1);

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(35, 5, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, 'Kasse', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, 'WANR', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, 'FG', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, 'Diff.', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(45, 5, 'Versicherung', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 5, 'Datum(Kasse)', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(105, 5, 'Bemerkung', 'LTRB', 0, 'L', 0);

        if ($count == 0)
        {
            $Zeile += 5;
        }

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        
        $ursprungx = $Ausdruck->_pdf->GetX();
        $ursprungy = $Ausdruck->_pdf->GetY();
        
        $Ausdruck->_pdf->cell(35, 5, $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), 0, 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, $endSumKasse, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, $wanr, 0, 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, $endSumPos, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, $diffBetrag, 0, 0, 'R', 0);
        $Ausdruck->_pdf->cell(45, 5, substr($rsKopfdaten->FeldInhalt('FGK_VERSICHERUNG'), 0, 20), 0, 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 5, $datumK, 0, 0, 'C', 0);
        
        $Ausdruck->_pdf->SetFont('Courier', 'B', 7);
        $Ausdruck->_pdf->Multicell(105, 5, $rsKopfdaten->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
        $Ausdruck->_pdf->SetFont('Arial', 'B', 10);
        
        $multicellY = $Ausdruck->_pdf->GetY();
        $h�he = $multicellY - $ursprungy;
        
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        
        $Ausdruck->_pdf->cell(35, $h�he, '', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'L', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
        $Ausdruck->_pdf->cell(15, $h�he, '', 'LTRB', 'R', 0);
        $Ausdruck->_pdf->cell(45, $h�he, '', 'LTRB', 'L', 0);
        $Ausdruck->_pdf->cell(25, $h�he, '', 'LTRB', 'C', 0);
        $Zeile += $h�he;
    }

    //die();
    $count++;
    $rsKopfdaten->DSWeiter();

    //---------------------------------------------------------------------------
}

$Ausdruck->Anzeigen();

*/

//
//----------------------------------------------------------------------------------------------------------
?>