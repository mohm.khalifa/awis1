<?php
/**
 * Klasse fuer die Verwaltung der MDE Daten
 * 
 * Mit Hilfe dieser Klasse werden die Basisfunktionalitaeten fuer die
 * Verwaltung der MDE Daten zur Verfuegung gestellt. Hierbei wird vor
 * allem der Import und Export der Daten, NICHT das Schreiben in eine
 * Datenbank realisiert.
 * 
 * @author Sacha Kerres
 * @version 20070802
 * @copyright ATU Auto Teile Unger
 * 
 *
 */
require_once('awisINFO.php');
require_once('db.inc.php');
require_once('awis_forms.inc.php');

class awisMDE
{
	/**
	 * Debugmodus aktiv/inaktiv
	 * Hiermit werden Ausgaben fuer die Kommandozeile erzeugt.
	 *
	 * @var int
	 */
	private $_Debug=0;			// Debug Infos ausgeben
	
	/**
	 * Pfad fuer die Lieferkontroll-Dateien
	 *
	 * @var string
	 */
	private $_LKPfad = '';
	
	/**
	 * Pfad fuer die Bestandsabfrage-Dateien
	 *
	 * @var string
	 */
	private $_BAPfad = '';
	
	/**
	 * Pfad fuer die Raedereinlagerung-Dateien
	 *
	 * @var string
	 */
	private $_REPfad = '';
	
	/**
	 * Pfad fuer die Raederauslagerung-Dateien
	 *
	 * @var string
	 */
	private $_RAPfad = '';
	
	/**
	 * Pfad fuer die R�ckf�hrungs-Dateien (Filiale)
	 *
	 * @var string
	 */
	private $_RFPfad = '';
	
	/**
	 * Pfad fuer die R�ckf�rhungs-Dateien (Zentrale)
	 *
	 * @var string
	 */
	private $_RZPfad = '';
	
	/**
	 * Objekt f�r die AWISInfo - Klasse
	 *
	 * @var AWISInfo
	 */
	private $_AWISParams;
	
	/**
	 * Mailserver fuer Benachrichtigungem
	 *
	 * @var string
	 */
	private $_MailServer='';
	
	/**
	 * AWIS Benutzer
	 *
	 * @var awisUser
	 */
	private $_AWISBenutzer;
	/**
	 * Erzeugung des Objekts
	 *
	 * @param int $Debug (Debugmodus fuer Ausgaben)
	 * @author Sacha Kerres
	 * @version 20070802
	 */
	public function __construct($Debug=0,$LoginName='')
	{
		$this->_Debug = $Debug;		// Debugmodus fuer Ausgaben setzen
		
		$this->_AWISBenutzer = new awisUser($LoginName);
		$this->_AWISParams = new awisINFO();
		
		// Mailserver fuer Benachrichtigungen
		$this->_MailServer = $this->_AWISParams->LeseAWISParameter('awis.conf','MAIL_SERVER');
		// Pfad fuer die Lieferkonztrolle
		$this->_LKPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_LIEFERKONTROLLE');
		//Pfad fuer die Bestandsabfrage
		$this->_BAPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_BESTANDSABFRAGE');
		//Pfad fuer die R�dereinlagerungen
		$this->_REPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_RAEDEREINLAGERUNG');		
		//Pfad fuer die R�derauslagerungen
		$this->_RAPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_RAEDERAUSLAGERUNG');		
		//Pfad fuer die R�ckf�hrungen (Filiale)
		$this->_RFPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_RUECKFUEHRUNGFILIALE');					   
		//Pfad fuer die R�ckf�hrungen (Zentrale)
		$this->_RZPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf','PFAD_RUECKFUEHRUNGZENTRALE');		
		if($this->_Debug)
		{
			echo 'Pfad LK:'.$this->_LKPfad.PHP_EOL;
		}
		
		
		if($this->_Debug)
		{
			echo 'Init: OK'.PHP_EOL;
		}
	}
	
	/**
	 * Funktion sucht die Lieferabgleichsdateien und traegt sie in die Datenbank ein
	 *
	 */
	public function LeseLieferabgleich()
	{
		global $awisRSZeilen;
		try 
		{
			$LieferscheinKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_LIEFERSCHEIN');
			if($LieferscheinKZ=='')
			{
				$LieferscheinKZ = 'LI';
			}
			
			if($this->_Debug)
			{
				$DebugPrefix = $LieferscheinKZ.date("YmdHis");
				//echo  $DebugPrefix.' Pr�fung auf Lieferscheine beginnt'.PHP_EOL;
			}
			
			// Alle Dateien vom Verzeichnis lesen
			$LIDateien = new DirectoryIterator($this->_LKPfad);
			foreach ($LIDateien AS $Datei)
			{
				//folgendes soll sicherstellen, dass die Datei nicht verarbeitet wird, w�hrend
				//der Scanner noch �bertr�gt 
				if($Datei->getCTime() > strtotime('now - 5 minutes'))
				{
					#continue;
				}
				
				if(substr($Datei,0,strlen($LieferscheinKZ))==$LieferscheinKZ)
				{
					if($this->_Debug)
					{
						echo $DebugPrefix.' Datei: '.$Datei.PHP_EOL;
					}
	
					$Filiale = substr($Datei,strlen($LieferscheinKZ),4);
					
					$con= awisLogon();
					
					// Pruefsumme der Datei pr�fen
					$Pruefsumme = md5_file($this->_LKPfad.'/'.$Datei);
					$SQL = 'SELECT * FROM LieferkontrolleProtokoll';
					$SQL .= ' WHERE LKP_FIL_ID = 0'.$Filiale;
					$SQL .= ' AND LKP_Pruefsumme=\''.$Pruefsumme.'\'';

					$rsLKP=awisOpenRecordset($con,$SQL);					
					
					if($awisRSZeilen!=0)
					{
						// Protokoll updaten
						$Key = $rsLKP['LKP_KEY'][0];
						$Zaehler = $rsLKP['LKP_ERGEBNIS'][0];
						$Zaehler++;
						$SQL = 'Update LieferkontrolleProtokoll';
						$SQL .= ' SET LKP_Zeit=SYSDATE';
						$SQL .= ' ,LKP_Ergebnis='.$Zaehler;
						$SQL .= " ,LKP_Meldung='Wiederholung'";
						$SQL .= " ,LKP_USER='".getenv('HOST')."'";
						$SQL .= ' ,LKP_USERDAT=SYSDATE';
						$SQL .= ' WHERE LKP_KEY='.$Key;		
						awisExecute($con,$SQL);
						
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...');
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							if ($Zaehler<=50) //einfache "Loop"erkennung
							{
								if ($Zaehler > 10)
								{
									$this->_AWISParams->EMail($EMail,'MDE FEHLER - LIEFERKONTROLLE - ' .$Datei .' BEREITS EINGELESEN (LOOP!)','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
								}
								else
								{
									$this->_AWISParams->EMail($EMail,'MDE FEHLER - LIEFERKONTROLLE - ' .$Datei .' BEREITS EINGELESEN','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
								}
							}
						}
						if(is_file($this->_LKPfad.'/~'.$Datei))
						{
							unlink($this->_LKPfad.'/~'.$Datei);
						}
						if (rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$Datei))
						{
							if($this->_Debug)
							{				
								echo $DebugPrefix.' Datei wurde auf ~'.$Datei.' umbenannt'.PHP_EOL;
							}
						}
						else
						{
							if($this->_Debug)
							{				
								echo $DebugPrefix.' Error: Datei wurde NICHT auf ~'.$Datei.' umbenannt'.PHP_EOL;
							}
						}
					
						continue;
					}
					
					// Aktuellen Lieferschein ermitteln
					$SQL='SELECT LKK_KEY, LKK_LIEFERSCHEINNR, LKK_DATUMKOMM';
					$SQL.=' FROM Lieferkontrollenkopf';
					$SQL.=' WHERE LKK_STATUS=\'O\'';
					$SQL.=' AND LKK_FIL_ID=0'.$Filiale;
					$SQL.=' AND ROWNUM=1';
					$rsLKK = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen==0)//Keine Daten
					{
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei '.$Datei.' wird umbenannt, kein offener Lieferschein.'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							$this->_AWISParams->EMail($EMail,'MDE FEHLER - LIEFERKONTROLLE - ' .$Filiale .' KEIN OFFENER LIEFERSCHEIN','F�r die Filiale '.$Filiale.' liegt kein offener Lieferschein vor.',3);
						}
						syslog(LOG_WARNING,'MDE - F�r die Filiale '.$Filiale.' liegt kein offener Lieferschein vor.');
						$DATUM = date("YmdHis");
						rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$DATUM.$Datei);
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
						}
						continue;
					}
					else 
					{
						$Lieferschein = $rsLKK['LKK_LIEFERSCHEINNR'][0];
						//$Lieferschein = $rsLKK->FeldInhalt('LKK_LIEFERSCHEINNR');
						$Datumkomm = $rsLKK['LKK_DATUMKOMM'][0];
						$Key = $rsLKK['LKK_KEY'][0];
					}
					
					// Datei komplett lesen
					$Daten = file($this->_LKPfad.'/'.$Datei);
					foreach($Daten AS $Zeile)
					{
						$Felder = explode(';',$Zeile);

						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}

						if($Felder[3]=='')		// Keine ATU Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine ATU Nummer im Datensatz');
							continue;	
						}
						else //Pr�fen, ob g�ltige ATUNR
						{
							$BindeVariablen=array();
							$BindeVariablen['var_T_ast_atunr']=$Felder[3];
							
							$SQL = ' SELECT AST_ATUNR ';
							$SQL .= ' FROM Artikelstamm ';
							$SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
							$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';
							
							$rsAST = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
							$rsASTZeilen=$awisRSZeilen;
							if ($rsASTZeilen == 0)
							{
								syslog(LOG_WARNING,'MDE Warnung: Keine g�ltige ATU Nummer');
								continue;	
							}
						}
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_lik_lkk_key']=$Key;
						$BindeVariablen['var_T_ast_atunr']=$Felder[3].'';
							
						$SQL = 'SELECT LIK_KEY';
						$SQL .= ' FROM Lieferkontrollen';
						$SQL .= ' WHERE LIK_LKK_KEY=:var_N0_lik_lkk_key';
						//$SQL .= ' WHERE LIK_FIL_ID=0'.$Filiale.'';
						//$SQL .= ' AND LIK_LIEFERSCHEINNR=0'.awis_FeldInhaltFormat('Z',$Lieferschein.'',false);
						$SQL .= ' AND LIK_AST_ATUNR=:var_T_ast_atunr';
						//$SQL .= ' AND LIK_STATUS<10';

						$rsLIK = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{
							$SQL = 'INSERT INTO Lieferkontrollen';
							$SQL .= '(LIK_FIL_ID,LIK_QUELLE,LIK_AST_ATUNR,LIK_LIEFERSCHEINNR,LIK_DATUMKOMM,LIK_DATUMSCAN';
							$SQL .= ',LIK_MENGESOLL,LIK_MENGEIST,LIK_MENGEKORREKTUR,LIK_KORREKTURGRUND,LIK_STATUS';
							$SQL .= ',LIK_USER,LIK_USERDAT,LIK_LKK_KEY,LIK_MENGESCAN)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('Z',$Felder[0],false);
							$SQL .= ',\'X\'';
							$SQL .= ','.awis_FeldInhaltFormat('T',$Felder[3],false);
							$SQL .= ','.awis_FeldInhaltFormat('Z',$Lieferschein.'',false);
							$SQL .= ',\''.$Datumkomm.'\'';
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[2]);
							$SQL .= ',0';
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[4]);
							$SQL .= ',0';
							$SQL .= ',null';
							$SQL .= ',1';
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';
							$SQL .= ','.awis_FeldInhaltFormat('N',$Key);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[4]);
							$SQL .= ')';
							
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Lieferkontrolldaten:'.$SQL.'-'.$Felder[0].'-');
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Insert der Lieferkontrolldaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else 						// Aktualisieren
						{
							$SQL = 'UPDATE Lieferkontrollen';
							$SQL .= ' SET LIK_MengeIst=LIK_MengeIst+'.awis_FeldInhaltFormat('N',$Felder[4]);
							$SQL .= ' ,LIK_MengeScan=LIK_MengeScan+'.awis_FeldInhaltFormat('N',$Felder[4]);
							$SQL .= ' ,LIK_DatumScan='.awis_FeldInhaltFormat('D',$Felder[2]);
							$SQL .= ',LIK_STATUS= CASE WHEN LIK_MENGESOLL=(LIK_MengeIst+'.awis_FeldInhaltFormat('N',$Felder[4],false).') THEN 10 ELSE 1 END';
							$SQL .= ',LIK_USER=\''.getenv('HOST').'\'';
							$SQL .= ',LIK_USERDAT=SYSDATE';
							$SQL .= ' WHERE LIK_KEY=0'.$rsLIK['LIK_KEY'][0];
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Lieferkontrolldaten:'.$SQL);
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Update der Lieferkontrolldaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:'.$SQL,16);
							}
						}
					}
					
					//Wenn keine Differenzen mehr bestehen, LSNR automatisch abschliessen
					$BindeVariablen=array();
					$BindeVariablen['var_N0_lik_lkk_key']=intval($Key);
					
					$SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_MENGEIST<>LIK_MENGESOLL AND LIK_LKK_KEY = :var_N0_lik_lkk_key';
					$rsLIK=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
					$rsLIKZeilen = $awisRSZeilen;
					//awis_debug(1,$rsLIK['ANZAHLDIFF'][0]);
					if ($rsLIK['ANZAHLDIFF'][0]==0)
					{
						$BindeVariablen=array();
						$BindeVariablen['var_N0_lkk_key']=intval($Key);
					
						$SQL = 'UPDATE LIEFERKONTROLLENKOPF SET LKK_STATUS = \'A\' WHERE LKK_KEY = :var_N0_lkk_key';
						if(awisExecute($con,$SQL,true,false,0,$BindeVariablen)===false)
						{
							$DATUM = date("YmdHis");
							rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$DATUM.$Datei);
							syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Lieferkontrolldaten:'.$SQL);
							if($this->_Debug)
							{
							    echo $DebugPrefix.' MDE - Fehler beim Update Status der Lieferkontrolldaten'.PHP_EOL;
								echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
							}
							throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:'.$SQL,16);
						}
					}
							
					// Erfolg schreiben
					$SQL = 'INSERT INTO LieferkontrolleProtokoll';
					$SQL .= '(LKP_Datei,LKP_Zeit,LKP_Pruefsumme,LKP_FIL_ID,LKP_Ergebnis,LKP_Meldung,LKP_LIEFERSCHEINNR,LKP_User,LKP_UserDat)';
					$SQL .= 'VALUES(';	
					$SQL .= awis_FeldInhaltFormat('T',$Datei,false);
					$SQL .=',SYSDATE';
					$SQL .= ','.awis_FeldInhaltFormat('T',$Pruefsumme,false);
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Filiale,false);
					$SQL .= ',1';
					$SQL .= ',null';
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Lieferschein,false);
					$SQL .= ',\''.getenv('HOST').'\'';
					$SQL .=',SYSDATE';
					$SQL .= ')';		
					
					if(awisExecute($con,$SQL)===false)
					{
						$DATUM = date("YmdHis");
                        rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/~'.$DATUM.$Datei);
						syslog(LOG_WARNING,'MDE - Fehler beim Speichern des Protokolls:'.$SQL);
						if($this->_Debug)
						{
						    echo $DebugPrefix.' MDE - Fehler beim Speichern des Protokolls'.PHP_EOL;
							echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
						}
						throw new Exception('Fehler beim Speichern des Protokolls:'.$SQL,15);
					}
					
					// Datei umbenennen
					if(is_file($this->_LKPfad.'/_'.$Datei))
					{
						unlink($this->_LKPfad.'/_'.$Datei);
					}
					if (rename($this->_LKPfad.'/'.$Datei,$this->_LKPfad.'/_'.$Datei))
					{
						syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
						if($this->_Debug)
						{
							echo $DebugPrefix.' '.$Datei.' wurde auf _'.$Datei.' umbenannt'.PHP_EOL;
						}
					}
					else
					{
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' konnte nicht umbenannt werden.');
						if($this->_Debug)
						{
							echo $DebugPrefix.' Error: '.$Datei.' wurde NICHT auf _'.$Datei.' umbenannt'.PHP_EOL;
						}
					}
					/*
					$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					if($EMail!='')
					{
						$EMail = explode(';',$EMail);
						$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
					}
					*/
				}
			}
			if($this->_Debug)
			{
				//echo $DebugPrefix.' Pr�fung auf Lieferscheine beendet'.PHP_EOL;
			}
			
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Lieferscheinabgleich-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}
	
		/**
	 * Funktion traegt die Bestandsabfrage - Daten in die Datenbank ein
	 *
	 */
	public function ImportBestandsabfrage()
	{
		global $awisRSZeilen;
		try 
		{
			$BestandsabfrageKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_BESTANDSABFRAGE');
			if($BestandsabfrageKZ=='')
			{
				$BestandsabfrageKZ = 'BA';
			}
			
			// Alle Dateien vom Verzeichnis lesen
			$BADateien = new DirectoryIterator($this->_BAPfad);
			foreach ($BADateien AS $Datei)
			{
				if(substr($Datei,0,strlen($BestandsabfrageKZ))==$BestandsabfrageKZ)
				{
					if($this->_Debug)
					{
						echo 'Datei: '.$Datei.PHP_EOL;
					}
	
					$Filiale = substr($Datei,strlen($BestandsabfrageKZ),4);
					$Scannerid = substr($Datei,strlen($BestandsabfrageKZ)+4,1);
					
					$con= awisLogon();
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					$BindeVariablen['var_N0_baf_scannerid']=$Scannerid;
					
					$SQL = 'DELETE FROM Bestandsabfragen where BAF_FIL_ID = :var_N0_fil_id AND BAF_SCANNERID = :var_N0_baf_scannerid';
						
					if(awisExecute($con,$SQL,true,false,0,$BindeVariablen)===false)
					{
						syslog(LOG_WARNING,'MDE - Fehler beim L�schen der alten Bestandsabfragedaten:'.$SQL.'-'.$Felder[0].'-');
						throw new Exception('Fehler beim L�chen der alten Bestandsabfragedaten:'.$SQL.'-'.$Felder[0].'-',17);
					}
					
					// Datei komplett lesen
					$Daten = file($this->_BAPfad.'/'.$Datei);
					foreach($Daten AS $Zeile)
					{
						$Felder = explode(';',$Zeile);
				
						$Felderanzahl = 0;	
						$Felderanzahl = count($Felder);
	
						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}
						
						$ATUNR = substr($Felder[2],0,6);
						
						if($Felder[2]=='')		// Keine ATU Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine ATU Nummer im Datensatz');
							continue;	
						}
						else //Pr�fen, ob g�ltige ATUNR
						{
							$BindeVariablen=array();
							$BindeVariablen['var_T_ast_atunr']=$ATUNR;
							
							$SQL = ' SELECT AST_ATUNR ';
							$SQL .= ' FROM Artikelstamm ';
							$SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
							$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';
							
							$rsAST = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
							$rsASTZeilen=$awisRSZeilen;
							if ($rsASTZeilen == 0)
							{
								syslog(LOG_WARNING,'MDE Warnung: Keine g�ltige ATU Nummer');
								continue;	
							}
						}
						
						//$ATUNR = substr($Felder[2],0,6);
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
						$BindeVariablen['var_T_ast_atunr']=$ATUNR;
							
						$SQL = 'SELECT BAF_KEY';						
						$SQL .= ' FROM Bestandsabfragen';
						$SQL .= ' WHERE BAF_FIL_ID=:var_N0_fil_id';
						$SQL .= ' AND BAF_AST_ATUNR=:var_T_ast_atunr';
						//$SQL .= ' AND LIK_LIEFERSCHEINNR=0'.awis_FeldInhaltFormat('Z',$Lieferschein.'',false);
						
						$rsLIK = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{
													
							//Neue Datei Speichern
							$SQL = 'INSERT INTO Bestandsabfragen';
							$SQL .= '(BAF_FIL_ID,BAF_SCANNERID,BAF_AST_ATUNR,BAF_DATUMSCAN,BAF_USER,BAF_USERDAT,BAF_MENGESCAN)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('Z',$Felder[0],false);
							$SQL .= ','.$Scannerid;
							$SQL .= ','.awis_FeldInhaltFormat('T',$ATUNR,false);
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[1],false);
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';

							if (intval($Felderanzahl) >= 4)
							{
								$SQL .= ','.awis_FeldInhaltFormat('Z',$Felder[3],false);
							}
							else
							{
								$SQL .= ','.awis_FeldInhaltFormat('Z',0,false);	
							}

							$SQL .= ')';
								
							if(awisExecute($con,$SQL)===false)
							{
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Bestandsabfragedaten:'.$SQL.'-'.$Felder[0].'-');
								throw new Exception('Fehler beim Speichern der Bestandsabfragedaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else    // Aktualisieren
                        {
                                $SQL = 'UPDATE Bestandsabfragen';
                                $SQL .= ' SET BAF_DatumScan='.awis_FeldInhaltFormat('D',$Felder[1]);
                                
                                if (intval($Felderanzahl) >= 4)
                                {
                                	$SQL .= ' ,BAF_MengeScan=BAF_MengeScan+'.awis_FeldInhaltFormat('N',$Felder[3]);
                                }
                                
                                $SQL .= ',BAF_USER=\''.getenv('HOST').'\'';
                                $SQL .= ',BAF_USERDAT=SYSDATE';
                                $SQL .= ' WHERE BAF_KEY=0'.$rsLIK['BAF_KEY'][0];
                                
                                if(awisExecute($con,$SQL)===false)
                                {
                                        $DATUM = date("YmdHis");
                                        rename($this->_BAPfad.'/'.$Datei,$this->_BAPfad.'/~'.$DATUM.$Datei);
                                        syslog(LOG_WARNING,'MDE - Fehler beim Speichern der BAdaten:'.$SQL);
                                        throw new Exception('Fehler beim Speichern der BAdaten:'.$SQL,16);
                        	    }
                        }
					}
					
					// Datei umbenennen
					if(is_file($this->_BAPfad.'/_'.$Datei))
					{
						unlink($this->_BAPfad.'/_'.$Datei);
					}
					rename($this->_BAPfad.'/'.$Datei,$this->_BAPfad.'/_'.$Datei);
					/*
					$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					if($EMail!='')
					{
						$EMail = explode(';',$EMail);
						$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
					}
					*/
					syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
				}
			}
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Bestandsabfrage-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}
	
		/**
	 * Funktion sucht die Raedereinlagerungsdateien und traegt sie in die Datenbank ein
	 *
	 */
	public function LeseRaedereinlagerung()
	{
		global $awisRSZeilen;
		try 
		{
			$RaedereinlagerungKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_RAEDEREINLAGERUNG');
			if($RaedereinlagerungKZ=='')
			{
				$RaedereinlagerungKZ = 'RE';
			}
			
			// Alle Dateien vom Verzeichnis lesen
			$REDateien = new DirectoryIterator($this->_REPfad);
			foreach ($REDateien AS $Datei)
			{
				if(substr($Datei,0,strlen($RaedereinlagerungKZ))==$RaedereinlagerungKZ)
				{
					if($this->_Debug)
					{
						echo 'Datei: '.$Datei.PHP_EOL;
					}
	
					$Filiale = substr($Datei,strlen($RaedereinlagerungKZ),4);
					
					$con= awisLogon();
					
					// Pruefsumme der Datei pr�fen
					$Pruefsumme = md5_file($this->_REPfad.'/'.$Datei);
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					$BindeVariablen['var_T_rep_pruefsumme']=$Pruefsumme;
					
					$SQL = 'SELECT * FROM RaedereinlagerungProtokoll';
					$SQL .= ' WHERE REP_FIL_ID = :var_N0_fil_id';
					$SQL .= ' AND REP_Pruefsumme= :var_T_rep_pruefsumme';

					$rsREP=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);					
					
					if($awisRSZeilen!=0)
					{
						if($this->_Debug)
						{
							echo 'Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							$this->_AWISParams->EMail($EMail,'MDE FEHLER - RAEDEREINLAGERUNG - ' .$Datei .' BEREITS EINGELESEN','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
						}
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...');
						if(is_file($this->_REPfad.'/~'.$Datei))
						{
							unlink($this->_REPfad.'/~'.$Datei);
						}
						rename($this->_REPfad.'/'.$Datei,$this->_REPfad.'/~'.$Datei);
						continue;
					}
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					
					$SQL='SELECT NVL(MAX(REL_LFDNR),0) as MAXLFDNR';
					$SQL.=' FROM Raedereinlagerungen';
					$SQL.=' WHERE REL_STATUS<10';
					$SQL.=' AND REL_FIL_ID=:var_N0_fil_id';
					//$SQL.=' AND ROWNUM=1';
					$rsREL = awisOpenRecordset($con, $SQL,true,false,$BindeVariablen);
					
					//Keine Aktueller Lieferschein vorhanden Daten
					//if($awisRSZeilen==0)
					if($rsREL['MAXLFDNR'][0]=='0' or $awisRSZeilen==0)
					{
						//N�chste LFDNR von der Filiale ermitteln
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
					
						$SQL='SELECT NVL(MAX(REL_LFDNR),0) as MAXLFDNR';						
						$SQL.=' FROM Raedereinlagerungen';						
						$SQL.=' WHERE REL_FIL_ID=:var_N0_fil_id';
						//$SQL.=' AND ROWNUM=1';
						$rsREL = awisOpenRecordset($con, $SQL,true,false,$BindeVariablen);
						
						//Noch keine Daten f�r die Filiale vorhanden
						//if($awisRSZeilen==0)
						if($rsREL['MAXLFDNR'][0]=='0' or $awisRSZeilen==0)
						{	
							$Laufendenr = 1;
						}
						else
						{
							$Laufendenr = $rsREL['MAXLFDNR'][0] + 1;
						}
					}
					else 
					{
						$Laufendenr = $rsREL['MAXLFDNR'][0];
					}
					
					// Datei komplett lesen
					$Daten = file($this->_REPfad.'/'.$Datei);
					foreach($Daten AS $Zeile)
					{
						$Felder = explode(';',$Zeile);

						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}

						if($Felder[2]=='')		// Keine Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine Einlagerungsnummer im Datensatz');
							continue;	
						}
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
						$BindeVariablen['var_N0_rel_lfdnr']=$Laufendenr;
						$BindeVariablen['var_T_rel_nr']=$Felder[2].'';
						
						$SQL = 'SELECT REL_KEY';
						$SQL .= ' FROM Raedereinlagerungen';
						$SQL .= ' WHERE REL_FIL_ID=:var_N0_fil_id';
						$SQL .= ' AND REL_LFDNR=:var_N0_rel_lfdnr';
						$SQL .= ' AND REL_NR=:var_T_rel_nr';

						$rsREL = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{
							$SQL = 'INSERT INTO Raedereinlagerungen';
							$SQL .= '(REL_FIL_ID,REL_NR,REL_MENGE,REL_LFDNR,REL_DATUMSCAN,REL_STATUS';
							$SQL .= ',REL_MENGESCAN,REL_USER,REL_USERDAT,REL_DATUMUEBERTRAGUNG)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('Z',$Felder[0],false);
							$SQL .= ','.awis_FeldInhaltFormat('T',$Felder[2],false);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('Z',$Laufendenr.'',false);
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ',1';
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);							
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';
							$SQL .=',SYSDATE';
							$SQL .= ')';
							
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                                                rename($this->_REPfad.'/'.$Datei,$this->_REPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Raedereinlagerungdaten:'.$SQL.'-'.$Felder[0].'-');
								throw new Exception('Fehler beim Speichern der Raedereinlagerungdaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else 						// Aktualisieren
						{
							$SQL = 'UPDATE Raedereinlagerungen';
							$SQL .= ' SET REL_MENGE=REL_MENGE+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,REL_MENGESCAN=REL_MENGESCAN+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,REL_DATUMSCAN='.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ' ,REL_STATUS= 1';
							$SQL .= ' ,REL_USER=\''.getenv('HOST').'\'';
							$SQL .= ', REL_USERDAT=SYSDATE';
							$SQL .= ', REL_DATUMUEBERTRAGUNG=SYSDATE';
							$SQL .= ' WHERE REL_KEY=0'.$rsREL['REL_KEY'][0];
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                                                rename($this->_REPfad.'/'.$Datei,$this->_REPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Raedereinlagerungdaten:'.$SQL);
								throw new Exception('Fehler beim Speichern der Raedereinlagerungdaten:'.$SQL,16);
							}
						}
					}
					
					// Erfolg schreiben
					$SQL = 'INSERT INTO RaedereinlagerungProtokoll';
					$SQL .= '(REP_Datei,REP_Zeit,REP_Pruefsumme,REP_FIL_ID,REP_Ergebnis,REP_Meldung,REP_LFDNR,REP_USER,REP_USERDAT)';
					$SQL .= 'VALUES(';	
					$SQL .= awis_FeldInhaltFormat('T',$Datei,false);
					$SQL .=',SYSDATE';
					$SQL .= ','.awis_FeldInhaltFormat('T',$Pruefsumme,false);
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Filiale,false);
					$SQL .= ',0';
					$SQL .= ',null';
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Laufendenr,false);
					$SQL .= ',\''.getenv('HOST').'\'';
					$SQL .=',SYSDATE';
					$SQL .= ')';		
					
					if(awisExecute($con,$SQL)===false)
					{
						$DATUM = date("YmdHis");
                                                rename($this->_REPfad.'/'.$Datei,$this->_REPfad.'/~'.$DATUM.$Datei);
						syslog(LOG_WARNING,'MDE - Fehler beim Speichern des Protokolls:'.$SQL);
						throw new Exception('Fehler beim Speichern des Protokolls:'.$SQL,15);
					}
					
					// Datei umbenennen
					if(is_file($this->_REPfad.'/_'.$Datei))
					{
						unlink($this->_REPfad.'/_'.$Datei);
					}
					rename($this->_REPfad.'/'.$Datei,$this->_REPfad.'/_'.$Datei);
					/*
					$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					if($EMail!='')
					{
						$EMail = explode(';',$EMail);
						$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
					}
					*/
					syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
				}
			}
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Raedereinlagerungs-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}
	
	/* Funktion sucht die Raedereinlagerungsdateien und traegt sie in die Datenbank ein
	 *
	 */
	public function LeseRaederauslagerung()
	{
		global $awisRSZeilen;
		try 
		{
			$RaederauslagerungKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_RAEDERAUSLAGERUNG');
			if($RaederauslagerungKZ=='')
			{
				$RaederauslagerungKZ = 'RA';
			}
			
			// Alle Dateien vom Verzeichnis lesen
			$RADateien = new DirectoryIterator($this->_RAPfad);
			foreach ($RADateien AS $Datei)
			{
				if(substr($Datei,0,strlen($RaederauslagerungKZ))==$RaederauslagerungKZ AND substr($Datei,0,3)!='RAN')
				{
					if($this->_Debug)
					{
						echo 'Datei: '.$Datei.PHP_EOL;
					}
	
					$Filiale = substr($Datei,strlen($RaederauslagerungKZ),4);
					
					$con= awisLogon();
					
					// Pruefsumme der Datei pr�fen
					$Pruefsumme = md5_file($this->_RAPfad.'/'.$Datei);
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					$BindeVariablen['var_T_rap_pruefsumme']=$Pruefsumme;
					
					$SQL = 'SELECT * FROM RaederauslagerungProtokoll';
					$SQL .= ' WHERE RAP_FIL_ID = :var_N0_fil_id';
					$SQL .= ' AND RAP_Pruefsumme= :var_T_rap_pruefsumme';

					$rsRAP=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);					
					
					if($awisRSZeilen!=0)
					{
						if($this->_Debug)
						{
							echo 'Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							$this->_AWISParams->EMail($EMail,'MDE FEHLER - RAEDERAUSLAGERUNG - ' .$Datei .' BEREITS EINGELESEN','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
						}
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...');
						if(is_file($this->_RAPfad.'/~'.$Datei))
						{
							unlink($this->_RAPfad.'/~'.$Datei);
						}
						rename($this->_RAPfad.'/'.$Datei,$this->_RAPfad.'/~'.$Datei);
						continue;
					}
					
					// Aktuellen Lieferschein ermitteln
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					
					$SQL='SELECT NVL(MAX(RAL_LFDNR),0) as MAXLFDNR';
					$SQL.=' FROM Raederauslagerungen';
					$SQL.=' WHERE RAL_STATUS<10';
					$SQL.=' AND RAL_FIL_ID=:var_N0_fil_id';
					//$SQL.=' AND ROWNUM=1';
					$rsRAL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
					
					//Keine Aktueller Lieferschein vorhanden Daten
					//if($awisRSZeilen==0)
					if($rsRAL['MAXLFDNR'][0]=='0' or $awisRSZeilen==0)
					{
						//N�chste LFDNR von der Filiale ermitteln
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
					
						$SQL='SELECT NVL(MAX(RAL_LFDNR),0) as MAXLFDNR';						
						$SQL.=' FROM Raederauslagerungen';						
						$SQL.=' WHERE RAL_FIL_ID=:var_N0_fil_id';
						//$SQL.=' AND ROWNUM=1';
						$rsRAL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
						
						//Noch keine Daten f�r die Filiale vorhanden
						//if($awisRSZeilen==0)
						if($rsRAL['MAXLFDNR'][0]=='0' or $awisRSZeilen==0)
						{					
							$Laufendenr = 1;
						}
						else
						{
							$Laufendenr = $rsRAL['MAXLFDNR'][0] + 1;
						}
					}
					else 
					{
						$Laufendenr = $rsRAL['MAXLFDNR'][0];
					}
					
					// Datei komplett lesen
					$Daten = file($this->_RAPfad.'/'.$Datei);
					foreach($Daten AS $Zeile)
					{
						$Felder = explode(';',$Zeile);

						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}

						if($Felder[2]=='')		// Keine Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine Einlagerungsnummer im Datensatz');
							continue;	
						}
						
						
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
						$BindeVariablen['var_N0_ral_lfdnr']=$Laufendenr;
						$BindeVariablen['var_T_ral_nr']=$Felder[2].'';
						
						$SQL = 'SELECT RAL_KEY';
						$SQL .= ' FROM Raederauslagerungen';
						$SQL .= ' WHERE RAL_FIL_ID=:var_N0_fil_id';
						$SQL .= ' AND RAL_LFDNR=:var_N0_ral_lfdnr';
						$SQL .= ' AND RAL_NR=:var_T_ral_nr';

						$rsRAL = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{
							$SQL = 'INSERT INTO Raederauslagerungen';
							$SQL .= '(RAL_FIL_ID,RAL_NR,RAL_MENGE,RAL_LFDNR,RAL_DATUMSCAN,RAL_STATUS';
							$SQL .= ',RAL_MENGESCAN,RAL_USER,RAL_USERDAT,RAL_DATUMUEBERTRAGUNG)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('Z',$Felder[0],false);
							$SQL .= ','.awis_FeldInhaltFormat('T',$Felder[2],false);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('Z',$Laufendenr.'',false);
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ',1';
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);							
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';
							$SQL .=',SYSDATE';
							$SQL .= ')';
							
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                                                rename($this->_RAPfad.'/'.$Datei,$this->_RAPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Raederauslagerungdaten:'.$SQL.'-'.$Felder[0].'-');
								throw new Exception('Fehler beim Speichern der Raederauslagerungdaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else 						// Aktualisieren
						{
							$SQL = 'UPDATE Raederauslagerungen';
							$SQL .= ' SET RAL_MENGE=RAL_MENGE+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RAL_MENGESCAN=RAL_MENGESCAN+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RAL_DATUMSCAN='.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ' ,RAL_STATUS= 1';
							$SQL .= ' ,RAL_USER=\''.getenv('HOST').'\'';
							$SQL .= ', RAL_USERDAT=SYSDATE';
							$SQL .= ', RAL_DATUMUEBERTRAGUNG=SYSDATE';
							$SQL .= ' WHERE RAL_KEY=0'.$rsRAL['RAL_KEY'][0];
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                                                rename($this->_RAPfad.'/'.$Datei,$this->_RAPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Raederauslagerungdaten:'.$SQL);
								throw new Exception('Fehler beim Speichern der Raederauslagerungdaten:'.$SQL,16);
							}
						}						
					}
					
					// Erfolg schreiben
					$SQL = 'INSERT INTO RaederauslagerungProtokoll';
					$SQL .= '(RAP_Datei,RAP_Zeit,RAP_Pruefsumme,RAP_FIL_ID,RAP_Ergebnis,RAP_Meldung,RAP_LFDNR,RAP_USER,RAP_USERDAT)';
					$SQL .= 'VALUES(';	
					$SQL .= awis_FeldInhaltFormat('T',$Datei,false);
					$SQL .=',SYSDATE';
					$SQL .= ','.awis_FeldInhaltFormat('T',$Pruefsumme,false);
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Filiale,false);
					$SQL .= ',0';
					$SQL .= ',null';
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Laufendenr,false);
					$SQL .= ',\''.getenv('HOST').'\'';
					$SQL .=',SYSDATE';
					$SQL .= ')';		
					
					if(awisExecute($con,$SQL)===false)
					{
						$DATUM = date("YmdHis");
                                                rename($this->_RAPfad.'/'.$Datei,$this->_RAPfad.'/~'.$DATUM.$Datei);
						syslog(LOG_WARNING,'MDE - Fehler beim Speichern des Protokolls:'.$SQL);
						throw new Exception('Fehler beim Speichern des Protokolls:'.$SQL,15);
					}
					
					// Datei umbenennen
					if(is_file($this->_RAPfad.'/_'.$Datei))
					{
						unlink($this->_RAPfad.'/_'.$Datei);
					}
					rename($this->_RAPfad.'/'.$Datei,$this->_RAPfad.'/_'.$Datei);
					/*
					$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					if($EMail!='')
					{
						$EMail = explode(';',$EMail);
						$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
					}
					*/
					syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
				}
			}
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Raederauslagerungs-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}
	
	/**
	 * Funktion sucht die Rueckfuehrungsdaten von den Filialen und traegt sie in die Datenbank ein
	 *
	 */
	public function LeseRueckfuehrung()
	{
		global $awisRSZeilen;
		try 
		{
			$RueckfuehrungsKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_RUECKFUEHRUNGFIL');
			if($RueckfuehrungsKZ=='')
			{
				$RueckfuehrungsKZ = 'RU';
			}
			if($this->_Debug)
			{
				$DebugPrefix = $RueckfuehrungsKZ.'F'.date("YmdHis");
				//echo  $DebugPrefix.' Pr�fung auf R�ckf�hrung FILIALE beginnt'.PHP_EOL;
			}
			// Alle Dateien vom Verzeichnis lesen
			$RFDateien = new DirectoryIterator($this->_RFPfad);
			foreach ($RFDateien AS $Datei)
			{
				if(substr($Datei,0,strlen($RueckfuehrungsKZ))==$RueckfuehrungsKZ)
				{
					if($this->_Debug)
					{
						echo $DebugPrefix.' Datei: '.$Datei.PHP_EOL;
					}
	
					$Filiale = substr($Datei,strlen($RueckfuehrungsKZ),4);
					
					$con= awisLogon();
					
					// Pruefsumme der Datei pr�fen
					$Pruefsumme = md5_file($this->_RFPfad.'/'.$Datei);
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_fil_id']=$Filiale;
					$BindeVariablen['var_T_rpf_pruefsumme']=$Pruefsumme;
					
					$SQL = 'SELECT * FROM RueckfuehrungsProtokollFiliale';
					$SQL .= ' WHERE RPF_FIL_ID = :var_N0_fil_id';
					$SQL .= ' AND RPF_Pruefsumme= :var_T_rpf_pruefsumme';

					$rsRPF=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);					
					
					if($awisRSZeilen!=0)
					{
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							//$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
							$this->_AWISParams->EMail($EMail,'MDE FEHLER - RUECKFUEHRUNG (FILIALE) - '.$Datei .' BEREITS EINGELESEN','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
						}
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...');
						if(is_file($this->_RFPfad.'/~'.$Datei))
						{
							unlink($this->_RFPfad.'/~'.$Datei);
						}
						rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/~'.$Datei);
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei wurde auf ~'.$Datei.' umbenannt'.PHP_EOL;
						}
						
						continue;
					}					
					
					//Datei komplett lesen
					$Daten = file($this->_RFPfad.'/'.$Datei);
					foreach($Daten AS $Zeile)
					{
						$Felder = explode(';',$Zeile);

						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}

						if($Felder[2]=='')		// Keine ATU Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine ATU Nummer im Datensatz');
							continue;	
						}
						else //Pr�fen, ob g�ltige ATUNR
						{
							$BindeVariablen=array();
							$BindeVariablen['var_T_ast_atunr']=$Felder[2];
							
							$SQL = ' SELECT AST_ATUNR ';
							$SQL .= ' FROM Artikelstamm ';
							$SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
							$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';
							
							$rsAST = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
							if ($awisRSZeilen == 0)
							{
								syslog(LOG_WARNING,'MDE Warnung: Keine g�ltige ATU Nummer');
								continue;	
							}
						}
						
						$BoxArt = '';
						
						if ($Felder[4]=='')
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine Boxart im Datensatz');
							continue;	
						}
						else 
						{
							if ($Felder[4]=='GB')
							{
								$BoxArt = 1;
							}
							elseif ($Felder[4]=='RP')
							{
								$BoxArt = 2;
							}
							elseif ($Felder[4]=='ST')
							{
								$BoxArt = 3;
							}
						}
						
	
						//Aktuelle R�ckf�hrung ermitteln
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
						$BindeVariablen['var_N0_rfk_boxart']=$BoxArt;
						
						$SQL = 'SELECT RFK_KEY';
						$SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';					
						$SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
						$SQL .= ' WHERE RFK_FIL_ID=:var_N0_fil_id';
						$SQL .= ' AND RFS_STATUS = 30';
						$SQL .= ' AND RFK_BOXART = :var_N0_rfk_boxart';
						if (trim($Felder[5])!='')
						{
							$BindeVariablen['var_T_rfk_boxean']=trim($Felder[5]);
							
							$SQL .= ' AND RFK_BOXEAN = :var_T_rfk_boxean';
						}
						else
						{
							$SQL .= ' AND RFK_BOXEAN is null';
						}
						
						//echo $SQL;			
							
						$rsRFK = awisOpenRecordset($con, $SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)//Keine Daten
						{
							//Neuen R�ckf�hrungskopf anlegen						
							$rsRFS = awisOpenRecordset($con,'SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=30');
							$Status = $rsRFS['RFS_KEY'][0];
				
							$BindeVariablen=array();
							$BindeVariablen['var_N0_fil_id']=$Felder[0];
							
							$rsLFDNR = awisOpenRecordset($con,'SELECT NVL(MAX(RFK_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSFILIALMENGENKOPF WHERE RFK_FIL_ID=:var_N0_fil_id',true,false,$BindeVariablen);
							$LFDNR= $rsLFDNR['MAXLFDNR'][0]+1;
											
							$SQL = 'INSERT INTO RueckFuehrungsFilialMengenKopf';
							$SQL .= '(RFK_FIL_ID, RFK_LFDNR, RFK_RFS_KEY, RFK_BOXART, RFK_BOXEAN';
							$SQL .= ',RFK_USER, RFK_USERDAT';
							$SQL .= ')VALUES (';
							$SQL .= '' . awis_FeldInhaltFormat('Z',$Felder[0],false);
							$SQL .= ' ,' . awis_FeldInhaltFormat('Z',$LFDNR,true);
							$SQL .= ' ,' . awis_FeldInhaltFormat('Z',$Status,true);
							$SQL .= ' ,' . awis_FeldInhaltFormat('Z',$BoxArt,false);
							$SQL .= ' ,' . awis_FeldInhaltFormat('T',trim($Felder[5]),true);
							$SQL .= ',\''.getenv('HOST').'\'';						
							$SQL .= ',SYSDATE';
							$SQL .= ')';
						
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
								rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-');
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Insert der Rueckfuehrungsdaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern Kopf der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
	
							$SQL = 'SELECT seq_RFK_KEY.CurrVal AS KEY FROM DUAL';
							$rsKey = awisOpenRecordset($con, $SQL);
							$Rueckfuehrungkey=$rsKey['KEY'][0];					
																
						}
						else 
						{
							$Rueckfuehrungkey = $rsRFK['RFK_KEY'][0];
						}
					
						$BindeVariablen=array();
						$BindeVariablen['var_N0_fil_id']=$Filiale;
						$BindeVariablen['var_N0_rff_rfk_key']=$Rueckfuehrungkey;
						$BindeVariablen['var_T_ast_atunr']=$Felder[2];
						
						$SQL = 'SELECT RFK_KEY, RFF_KEY, RFF_AST_ATUNR';
						$SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';
						$SQL .= ' LEFT JOIN Rueckfuehrungsfilialmengenpos on RFF_RFK_KEY = RFK_KEY';
						$SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
						$SQL .= ' WHERE RFK_FIL_ID=:var_N0_fil_id';
						$SQL .= ' AND RFF_RFK_KEY=:var_N0_rff_rfk_key';
						$SQL .= ' AND RFF_AST_ATUNR=:var_T_ast_atunr';
						$SQL .= ' AND RFS_STATUS = 30';

						$rsRFF = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{							
							$SQL = 'INSERT INTO RueckfuehrungsfilialmengenPos';
							$SQL .= '(RFF_AST_ATUNR,RFF_MENGEFILIALE,RFF_MENGESCANFILIALE,RFF_DATUMSCANFILIALE';
							$SQL .= ',RFF_RFK_KEY,RFF_USER,RFF_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('T',$Felder[2],false);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ','.$Rueckfuehrungkey;
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';
							$SQL .= ')';
							
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-');
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Insert POS der Rueckfuehrungsdaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else 						// Aktualisieren
						{
							$SQL = 'UPDATE RueckfuehrungsfilialmengenPos';
							$SQL .= ' SET RFF_MengeFiliale=RFF_MengeFiliale+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RFF_MengeScanFiliale=RFF_MengeScanFiliale+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RFF_DatumScanFiliale='.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ',RFF_USER=\''.getenv('HOST').'\'';
							$SQL .= ',RFF_USERDAT=SYSDATE';
							$SQL .= ' WHERE RFF_KEY=0'.$rsRFF['RFF_KEY'][0];
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
								rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL);
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Update POS der Rueckfuehrungsdaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL,16);
							}
						}
					}
					
					// Erfolg schreiben
					$SQL = 'INSERT INTO RueckfuehrungsProtokollFiliale';
					$SQL .= '(RPF_Datei,RPF_Zeit,RPF_RFK_KEY,RPF_Pruefsumme,RPF_FIL_ID,RPF_Ergebnis,RPF_Meldung,RPF_User,RPF_UserDat)';
					$SQL .= 'VALUES(';	
					$SQL .= awis_FeldInhaltFormat('T',$Datei,false);
					$SQL .=',SYSDATE';
					$SQL .= ','.$Rueckfuehrungkey;
					$SQL .= ','.awis_FeldInhaltFormat('T',$Pruefsumme,false);
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Filiale,false);
					$SQL .= ',0';
					$SQL .= ',null';
					$SQL .= ',\''.getenv('HOST').'\'';
					$SQL .=',SYSDATE';
					$SQL .= ')';		
					
					if(awisExecute($con,$SQL)===false)
					{
						$DATUM = date("YmdHis");
                        rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/~'.$DATUM.$Datei);
						syslog(LOG_WARNING,'MDE - Fehler beim Speichern des Protokolls:'.$SQL);
						if($this->_Debug)
						{
						    echo $DebugPrefix.' MDE - Fehler beim Speichern des Protokolls'.PHP_EOL;
							echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
						}
						throw new Exception('Fehler beim Speichern des Protokolls:'.$SQL,15);
					}
					
					// Datei umbenennen
					if(is_file($this->_RFPfad.'/_'.$Datei))
					{
						unlink($this->_RFPfad.'/_'.$Datei);
					}
					rename($this->_RFPfad.'/'.$Datei,$this->_RFPfad.'/_'.$Datei);
					if($this->_Debug)
					{
						echo $DebugPrefix.' Datei wurde auf _'.$Datei.' umbenannt'.PHP_EOL;
					}
					
					//$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					//if($EMail!='')
					//{
					//	$EMail = explode(';',$EMail);
						//$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
						//$this->_AWISParams->EMail('thomas.riedl@de.atu.eu','MDE OK - RUECKFUHRUNG (FILIALE) DATEI ERFOGREICH EINGELESEN','Datei '.$Datei.' erfolgreich eingelesen.',2);						
					//}
					
					syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
					
				}
			}
			if($this->_Debug)
			{
				//echo  $DebugPrefix.' Pr�fung auf R�ckf�hrung FILIALE beendet'.PHP_EOL;
			}
			
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Rueckfuehrungs-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}
	
	/**
	 * Funktion sucht die Rueckfuehrungsdaten von der Zentrale und traegt sie in die Datenbank ein
	 *
	 */
	public function LeseRueckfuehrungZentrale()
	{
		global $awisRSZeilen;
		try 
		{
			$RueckfuehrungZentraleKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf','ERWEITERUNG_RUECKFUEHRUNGZEN');
			if($RueckfuehrungZentraleKZ=='')
			{
				$RueckfuehrungZentraleKZ = 'RU';
			}
			if($this->_Debug)
			{
				$DebugPrefix = $RueckfuehrungZentraleKZ.'Z'.date("YmdHis");
				//echo  $DebugPrefix.' Pr�fung auf R�ckf�hrung ZENTRALE beginnt'.PHP_EOL;
			}
			
			// Alle Dateien vom Verzeichnis lesen
			$RZDateien = new DirectoryIterator($this->_RZPfad);
			foreach ($RZDateien AS $Datei)
			{
				if(substr($Datei,0,strlen($RueckfuehrungZentraleKZ))==$RueckfuehrungZentraleKZ)
				{
					if($this->_Debug)
					{
						echo $DebugPrefix.' Datei: '.$Datei.PHP_EOL;
					}
	
					$con= awisLogon();
					
					// Pruefsumme der Datei pr�fen
					$Pruefsumme = md5_file($this->_RZPfad.'/'.$Datei);
					
					$BindeVariablen=array();
					$BindeVariablen['var_T_rpz_pruefsumme']=$Pruefsumme;
					
					$SQL = 'SELECT * FROM RueckfuehrungsProtokollZen';
					//$SQL .= ' WHERE RPZ_FIL_ID = 0'.$Filiale;
					$SQL .= ' WHERE RPZ_Pruefsumme=:var_T_rpz_pruefsumme';

					$rsRPZ=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);					
					
					if($awisRSZeilen!=0)
					{
						if($this->_Debug)
						{
							echo $DebugPrefix.'Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...'.PHP_EOL;
						}
						$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_FEHLER');
						if($EMail!='')
						{
							$EMail = explode(';',$EMail);
							//$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
							$this->_AWISParams->EMail($EMail,'MDE FEHLER - RUECKFUEHRUNG (ZENTRALE) - '.$Datei.' BEREITS EINGELESEN','Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...',3);
							
						}
						syslog(LOG_WARNING,'MDE - Datei '.$Datei.' wurde bereits eingelesen. Wird uebersprungen...');
						if(is_file($this->_RZPfad.'/~'.$Datei))
						{
							unlink($this->_RZPfad.'/~'.$Datei);
						}
						rename($this->_RZPfad.'/'.$Datei,$this->_RZPfad.'/~'.$Datei);
						if($this->_Debug)
						{
							echo $DebugPrefix.' Datei wurde auf ~'.$Datei.' umbenannt'.PHP_EOL;
						}

						continue;
					}
					
					
					//Datei komplett lesen
					$Daten = file($this->_RZPfad.'/'.$Datei);
					$Fehler=0; //Fehlerz�hler bei Dateiverarbeitung
					foreach($Daten AS $Zeile)
					{
						$Zeile = trim($Zeile);
						if ($Zeile == '')
						{
							continue;
						}

						$Felder = explode(';',$Zeile);

						if($Felder[0]=='')
						{
							continue;			// Leerzeilen ueberspringen
						}

						if($Felder[2]=='')		// Keine ATU Nummer -> ignorieren
						{
							syslog(LOG_WARNING,'MDE Warnung: Keine ATU Nummer im Datensatz');
							continue;	
						}
						else //Pr�fen, ob g�ltige ATUNR
						{
							$BindeVariablen=array();
							$BindeVariablen['var_T_ast_atunr']=$Felder[2];
							
							$SQL = ' SELECT AST_ATUNR ';
							$SQL .= ' FROM Artikelstamm ';
							$SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
							$SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';
							
							$rsAST = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
							if ($awisRSZeilen == 0)
							{
								syslog(LOG_WARNING,'MDE Warnung: Keine g�ltige ATU Nummer');
								continue;	
							}
						}
						
						$Rueckfuehrungskey='';
						$Filiale='';

						//Aktuelle R�ckf�hrung ermitteln
						$BindeVariablen=array();
						$BindeVariablen['var_rfk_rueckfuehrungsnr']=$Felder[0];
						
						$SQL = 'SELECT RFK_KEY, RFK_FIL_ID';
						$SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';					
						$SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
						$SQL .= ' WHERE RFK_RUECKFUEHRUNGSNR=:var_rfk_rueckfuehrungsnr';
						$SQL .= ' AND RFS_STATUS = 31';
							
						$rsRFK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
						if($awisRSZeilen==0)//Keine Daten
						{
							$Fehler++;
							syslog(LOG_WARNING,'MDE - Zu der R�ckf�hrungsnummer '.$Felder[0].' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor.');
							$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO_RF');
                            if($EMail!='')
                            {
								$EMail = explode(';',$EMail);
								$this->_AWISParams->EMail($EMail,'MDE FEHLER - RUECKFUEHRUNG (ZENTRALE) - '.$Felder[0],'Zu der R�ckf�hrungsnummer '.$Felder[0].', ATUNR:'.$Felder[2].', Menge: '.awis_FeldInhaltFormat('N',$Felder[3]).' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor',2);
							}
							if($this->_Debug)
							{
								echo $DebugPrefix.'MDE - Zu der R�ckf�hrungsnummer '.$Felder[0].' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor.'.PHP_EOL;
							}
							continue;
						}
						else 
						{							
							$Rueckfuehrungskey = $rsRFK['RFK_KEY'][0];
							$Filiale = $rsRFK['RFK_FIL_ID'][0];							
						}												
						
						//Pr�fen, ob die ATUNR in der R�ckf�rhug vorhanden ist
						$BindeVariablen=array();
						$BindeVariablen['var_N0_rff_rfk_key']=$Rueckfuehrungskey;
						$BindeVariablen['var_T_ast_atunr']=$Felder[2].'';
						
						$SQL = 'SELECT RFF_KEY';
						$SQL .= ' FROM Rueckfuehrungsfilialmengenpos';
						$SQL .= ' WHERE RFF_RFK_KEY=:var_N0_rff_rfk_key';
						$SQL .= ' AND RFF_AST_ATUNR=:var_T_ast_atunr';

						$rsRFF = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
						if($awisRSZeilen==0)		// Keine Daten
						{
							$SQL = 'INSERT INTO Rueckfuehrungsfilialmengenpos';
							$SQL .= '(RFF_AST_ATUNR,RFF_MENGEZENTRALE,RFF_MENGESCANZENTRALE,RFF_DATUMSCANZENTRALE,RFF_RFK_KEY';
							$SQL .= ',RFF_USER,RFF_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ''.awis_FeldInhaltFormat('T',$Felder[2],false);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ','.awis_FeldInhaltFormat('D',$Felder[1]);
							$SQL .= ','.$Rueckfuehrungskey;
							$SQL .= ',\''.getenv('HOST').'\'';
							$SQL .=',SYSDATE';
							$SQL .= ')';
							
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
                                rename($this->_RZfad.'/'.$Datei,$this->_RZPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-');
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Insert POS der Rueckfuehrungsdaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL.'-'.$Felder[0].'-',17);
							}
						}
						else 						// Aktualisieren
						{
							$SQL = 'UPDATE Rueckfuehrungsfilialmengenpos';
							$SQL .= ' SET RFF_MengeZentrale=RFF_MengeZentrale+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RFF_MengeScanZentrale=RFF_MengeScanZentrale+'.awis_FeldInhaltFormat('N',$Felder[3]);
							$SQL .= ' ,RFF_DatumScanZentrale='.awis_FeldInhaltFormat('D',$Felder[1]);
							//$SQL .= ',LIK_STATUS= CASE WHEN LIK_MENGESOLL=(LIK_MengeIst+'.awis_FeldInhaltFormat('N',$Felder[4],false).') THEN 10 ELSE 1 END';
							$SQL .= ',RFF_USER=\''.getenv('HOST').'\'';
							$SQL .= ',RFF_USERDAT=SYSDATE';
							$SQL .= ' WHERE RFF_KEY=0'.$rsRFF['RFF_KEY'][0];
							if(awisExecute($con,$SQL)===false)
							{
								$DATUM = date("YmdHis");
								rename($this->_RZPfad.'/'.$Datei,$this->_RZPfad.'/~'.$DATUM.$Datei);
								syslog(LOG_WARNING,'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL);
								if($this->_Debug)
								{
								    echo $DebugPrefix.' MDE - Fehler beim Update POS der Rueckfuehrungsdaten'.PHP_EOL;
									echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
								}
								throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:'.$SQL,16);
							}
						}
					}
					
					if ($Fehler<>0)
					{
						$DATUM = date("YmdHis");
						copy($this->_RZPfad.'/'.$Datei,$this->_RZPfad.'/Fehler/~'.$DATUM.$Datei);
						echo $DebugPrefix.' Datei wurde auf /Fehler/~'.$DATUM.$Datei.' kopiert'.PHP_EOL;
					}
					
					// Erfolg schreiben
					$SQL = 'INSERT INTO RueckfuehrungsProtokollZen';
					$SQL .= '(RPZ_Datei,RPZ_Zeit,RPZ_RFK_KEY,RPZ_Pruefsumme,RPZ_FIL_ID,RPZ_Ergebnis,RPZ_Meldung,RPZ_User,RPZ_UserDat)';
					$SQL .= 'VALUES(';	
					$SQL .= awis_FeldInhaltFormat('T',$Datei,false);
					$SQL .=',SYSDATE';
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Rueckfuehrungskey,true);
					$SQL .= ','.awis_FeldInhaltFormat('T',$Pruefsumme,false);
					$SQL .= ','.awis_FeldInhaltFormat('Z',$Filiale,true);
					$SQL .= ',0';
					$SQL .= ',null';
					$SQL .= ',\''.getenv('HOST').'\'';
					$SQL .=',SYSDATE';
					$SQL .= ')';		
					
					if(awisExecute($con,$SQL)===false)
					{
						$DATUM = date("YmdHis");
                        rename($this->_RZPfad.'/'.$Datei,$this->_RZPfad.'/~'.$DATUM.$Datei);
						syslog(LOG_WARNING,'MDE - Fehler beim Speichern des Protokolls:'.$SQL);
						if($this->_Debug)
						{
						    echo $DebugPrefix.' MDE - Fehler beim Speichern des Protokolls'.PHP_EOL;
							echo $DebugPrefix.' Datei wurde auf ~'.$DATUM.$Datei.' umbenannt'.PHP_EOL;
						}
						throw new Exception('Fehler beim Speichern des Protokolls:'.$SQL,15);
					}
					
					// Datei umbenennen
					if(is_file($this->_RZPfad.'/_'.$Datei))
					{
						unlink($this->_RZPfad.'/_'.$Datei);
					}
					rename($this->_RZPfad.'/'.$Datei,$this->_RZPfad.'/_'.$Datei);
					if($this->_Debug)
					{
						echo $DebugPrefix.' Datei wurde auf _'.$Datei.' umbenannt'.PHP_EOL;
					}
					//$EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf','MAIL_ADRESSEN_INFO');
					//if($EMail!='')
					//{
					//	$EMail = explode(';',$EMail);
						//$this->_AWISParams->EMail($EMail,'MDE','Datei '.$Datei.' erfolgreich eingelesen.',2);
					//	$this->_AWISParams->EMail('thomas.riedl@de.atu.eu','MDE OK - RUECKFUEHRUNG (ZENTRALE) DATEI ERFOLGREICH EINGELESEN ','Datei '.$Datei.' erfolgreich eingelesen.',2);
					//}
					
					syslog(LOG_INFO,'MDE - Datei '.$Datei.' erfolgreich eingelesen.');
				}
			}
			if($this->_Debug)
			{
				//echo  $DebugPrefix.' Pr�fung auf R�ckf�hrung ZENTRALE beendet'.PHP_EOL;
			}
		}
		
		
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{
				
			}
			throw new Exception('Fehler beim Lesen der Rueckfuehrungs-Daten: '.$ex->getMessage().' - '.$ex->getCode(),12);
		}
				
	}	
}
//************ ENDE KLASSE awisMDE **********************//

// Testroutine

if(isset($argv[2]) and $argv[2]=='TEST')
{
	echo PHP_EOL.'======================================='.PHP_EOL;
	echo 'TEST Routine fuer awisMDE Klasse.'.PHP_EOL;
	echo '======================================='.PHP_EOL;
	
	try 
	{
		$MDE = new awisMDE(1,$argv[1]);
		
		$MDE->LeseLieferabgleich();
	}
	catch (Exception $ex)
	{
		
		echo PHP_EOL.'awisMDE: Fehler: '.$ex->getMessage().PHP_EOL;
		
	}
	
	
	echo '======================================='.PHP_EOL;
	echo 'TEST Routine abgeschlossen.'.PHP_EOL;
	echo '======================================='.PHP_EOL;
}
	
?>
