<?xml version="1.0" standalone="yes"?>
<? PHP
  header( 'Content-type: application/vnd.mozilla.xul+xml' );
  if( isset( $_GET['user'] ) )
    $user = $_GET['user'];
  else
    $user = 'Gast';
?>
<window xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">
  <groupbox orient="horizontal">
    <caption label="Meine erste XUL-Anwendung"/>
    <button label="Hallo XUL" onclick="alert('Hallo <?PHP echo $user; ?>!')" flex="1"/>
    <button label="Hallo <?PHP echo $user; ?>" onclick="alert('Hallo XUL!')" flex="2"/>
  </groupbox>
</window>