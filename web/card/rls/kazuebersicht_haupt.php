<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
	echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>
</script>
<html>
<head>
<title>&Uuml;bersicht Transfer Kontoausz&uuml;ge</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>
<?php

//Datensatz als 'nicht �bernommen' buchen

if(isset($strLoeschId)&&$strLoeschId!='')
{
	$strSql="update kontoauszuege set IKA_TRANSFER_IN_KONTOAUSZUEGE=2,ika_aend_benutzer='".$strRec_Aend_Benutzer."',ika_aend_datum=sysdate where ika_id=".$strLoeschId;
	abfrage("ora",$strSql);
}


$strVariante='';
$strFilter='';
$strSqlFilter='';

if(isset($strAuswahl)&&$strAuswahl!='offen')
{
	switch($strAuswahl)
	{
		case "ausgeglichen":
		$strSqlFilter=" where (betr_offen=0 and ika_rlsbetrag != 0)";
		$strFilter="Filter: ".$strAuswahl;
		break;
		case "alle":
		$strSqlFilter='';
		$strFilter="Filter: ".$strAuswahl;
		break;
		default: $strSqlFilter='';
	}
}
else
{
	$strSqlFilter=" where (betr_offen!=0 or ika_rlsbetrag = 0)";
	$strVariante=' (offen)';
}

if(isset($strDatum))
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where ika_buchungsdatum='".$strDatum."'";
	}
	else
	{
		$strSqlFilter.=" and ika_buchungsdatum='".$strDatum."'";
	}
	
	if($strFilter=='')
	{
		$strFilter="Filter: ".$strDatum;
	}
	else
	{
		$strFilter.="/".$strDatum;
	}
}

if(isset($strBetrag))
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where ika_betrag=".str_replace(',','.',$strBetrag)."";
	}
	else
	{
		$strSqlFilter.=" and ika_betrag=".str_replace(',','.',$strBetrag)."";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: ".eurobetrag($strBetrag);
	}
	else
	{
		$strFilter.="/".eurobetrag($strBetrag);
	}	
}

if(isset($strBank)&&$strBank!='alle')
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where BAN_KUERZEL='".$strBank."'";
	}
	else
	{
		$strSqlFilter.=" and BAN_KUERZEL='".$strBank."'";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: ".$strBank;
	}
	else
	{
		$strFilter.="/".$strBank;
	}	
}

if(isset($strAuftraggeber)&&$strAuftraggeber!='')
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where (upper(IKA_AUFTRAGGEBER1) like '%".strtoupper($strAuftraggeber)."%' or upper(IKA_AUFTRAGGEBER2) like '%".strtoupper($strAuftraggeber)."%')";
	}
	else
	{
		$strSqlFilter.=" and (upper(IKA_AUFTRAGGEBER1) like '%".strtoupper($strAuftraggeber)."%' or upper(IKA_AUFTRAGGEBER2) like '%".strtoupper($strAuftraggeber)."%')";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: Suchmuster \"".$strAuftraggeber."\"";
	}
	else
	{
		$strFilter.="/Suchmuster \"".$strAuftraggeber."\"";
	}	
}

if($strFilter!='')
	$strFilter=' ('.$strFilter.')';

$intScrollanzahl = 3;

$strDb = "ora";

$strSql = "select * from (
				select 
				(ika_rlsbetrag -(select nvl(sum(kaa_betrag),0) from kontoauszuege_aufteilung where kaa_kontoauszugid=ika_id and kaa_loesch=0)) betr_offen,
				IKA_ID, IKA_AUSZUGNR, IKA_KONTONR, IKA_BANK, IKA_AUSZUGSDATUM, IKA_BUCHUNGSDATUM, 
				IKA_VALUTADATUM, IKA_BETRAG, IKA_WAEHRUNG, IKA_TEXTSCHLUESSEL, IKA_BUCHUNGSTEXT, IKA_VERWENDUNGSZWECK1, 
				IKA_VERWENDUNGSZWECK2, IKA_VERWENDUNGSZWECK3, IKA_KUNDENREFERENZ, IKA_STARTSALDO, IKA_SCHLUSSALDO, IKA_AUFTRAGGEBER1, 
				IKA_AUFTRAGGEBER2, IKA_AUFTRAGGEBER_BLZ, IKA_AUFTRAGGEBER_KONTO, IKA_RLSBETRAG, 
				nvl(BA_BUCHUNGSART,'Buchungsart fehlt') BA_BUCHUNGSART,
				nvl(ban_kuerzel,'') ban_kuerzel
				from kontoauszuege, buchungsarten, bank
				where IKA_BUCHUNGSART = BA_BUCHUNGSARTID(+)
				and ika_transfer_in_kontoauszuege=1 and ika_loesch=0
				and (ika_kontonr=ban_kontonr(+) and ika_bank=ban_blz(+))
				)".$strSqlFilter." order by ika_id";

include("card/card_blaettern_haupt.inc.php");	//Bl�ttern

$arrKazTransfer = abfrage("ora",$strSqlScroll);
$intKazZeilen = $arrKazTransfer["zeilen"];
$arrKazTransferDs = $arrKazTransfer["trefferarray"];

?>
<script language="javascript">

function init()
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
	
	document.ondblclick = zoom;		//Zoomfenster
}

//R�ckgabe der ausgew�hlten Kontoauszugsid an aufrufendes Fenster mahngebuehren_haupt.php

function auswahlid(intId)
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Auswahl Einzelsatz �bersicht Kontoausz�ge");	//Zeitmessung
	<?php } ?>
	parent.window.returnValue = intId;
	parent.self.close();
}

function filtern()
{
	var parameter = "intDialog=1";
	
	<?php echo dialog("kazuebersicht_filter.php"); ?>

	if(rueckgabe!='undefined')
	{
		var arrRueck = rueckgabe.split(";");
					
		strFilter='&strAuswahl='+arrRueck[1];
		
		if(arrRueck[0]!='')
		{
			strFilter+='&strDatum='+arrRueck[0];
		}
		
		if(arrRueck[2]!='')
		{
			strFilter+='&strBetrag='+arrRueck[2];
		}
		
		if(arrRueck[3]!='')
		{
			strFilter+='&strAuftraggeber='+arrRueck[3];
		}
		
		if(arrRueck[4]!='')
		{
			strFilter+='&strBank='+arrRueck[4];
		}
		
		strQuery = querystringlesen();
				
		intPos = strQuery.indexOf("&strAuswahl");
		
		if(intPos!=-1)
		{
			strLoesch = strQuery.slice(intPos);
			querystringverkuerzen(strLoesch);
		}
				
		querystringerweitern(strFilter);
		
		location.href='<?php echo $PHP_SELF; ?>?'+querystringlesen();
	}
}

function loeschen(strLoeschId)
{
	var Rueck = confirm("Soll der Datensatz wirklich als\n'nicht �bernommen' gebucht werden?");
	if(Rueck)
		location.href='<?php echo $PHP_SELF; ?>?strLoeschId='+strLoeschId+'&'+querystringlesen();
}




</script>
</head>
<body onload="init();" style="overflow:hidden;">
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center" valign="top">
	<table cellspacing="0" cellpadding="0" border="0">
	<tr><td class="zwzeile">&nbsp;</td></tr>
	<tr><td align="center">
	Kontoausz&uuml;ge<?php echo $strVariante;?>: <span class="titel"><?php echo $strPosition.$strFilter; ?></span></td></tr>
	<tr><td class="zwzeile">&nbsp;</td></tr>
	</table>
	<?php if($intKazZeilen)
	{
	?>
	<table cellspacing="0" cellpadding="0">
		<tr><td colspan="6" class="zwzeile"><hr></td></tr>
		<tr><td class="titel">Valuta:</td><td class="titel">Datum:</td><td class="titel">Auftraggeber/Bankverbindung:</td>
		<td class="titel">Verwendungszweck:</td>
		<td class="titel">Betrag:</td>
		<td class="titel">RLS-Betrag:</td>
		</tr>
		<tr><td colspan="6" class="zwzeile"><hr></td></tr>
		
			<?php
			for($i=0;$i<$arrKazTransfer["zeilen"];$i++)
			{
				//Zeile 1
				echo "<tr>";
				//echo "<td><img src=\"/bilder/eingabe_ok.png\" class=\"quadrat\" alt=\"Datensatz ausw&auml;hlen\" onclick=\"auswahlid('".$arrKazDs["KAZ_KONTOAUSZUGID"][$i]."');\"></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"val\" value=\"".
				$arrKazTransferDs["IKA_VALUTADATUM"][$i]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"dat\" value=\"".
				$arrKazTransferDs["IKA_BUCHUNGSDATUM"][$i]."\" readonly></td>";
				echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"ag\" readonly>".
				$arrKazTransferDs["IKA_AUFTRAGGEBER1"][$i]."\n".
				$arrKazTransferDs["IKA_AUFTRAGGEBER2"][$i]."</textarea></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"ba\" value=\"".
				$arrKazTransferDs["BA_BUCHUNGSART"][$i]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"betr\" value=\"".
				eurobetrag($arrKazTransferDs["IKA_BETRAG"][$i])."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"betrrls\" value=\"".
				eurobetrag($arrKazTransferDs["IKA_RLSBETRAG"][$i])."\"";
				if(str_replace(",",".",$arrKazTransferDs["IKA_RLSBETRAG"][$i])==0)
					echo " style=\"color:red;\"";
				echo " readOnly></td>";
				echo "</tr>";
				//Zeile 2
				echo "<tr>";
				echo "<td colspan=\"2\" class=\"text\" rowspan=\"2\" align=\"center\" style=\"vertical-align:bottom;\">";
				echo "<span class=\"titel\">Bank/KAZ:</span><br>";
				echo "<input type=\"text\" class=\"bank\" value=\"".$arrKazTransferDs["BAN_KUERZEL"][$i]."\" readonly>";
				echo "<input type=\"text\" class=\"kaz\" value=\"".
				$arrKazTransferDs["IKA_AUSZUGNR"][$i]."/".$arrKazTransferDs["IKA_AUSZUGSDATUM"][$i]."\" readonly>";
				echo "</td>";
				echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"verw\" readonly>".
				$arrKazTransferDs["IKA_VERWENDUNGSZWECK1"][$i]."\n".
				$arrKazTransferDs["IKA_VERWENDUNGSZWECK2"][$i]."\n".
				$arrKazTransferDs["IKA_VERWENDUNGSZWECK3"][$i]."</textarea></td>";
				echo "<td rowspan=\"2\" class=\"text\" style=\"text-align:center;vertical-align:bottom;\"";
				echo "<span style=\"font-weight:bold;\">offen:</span><br><input type=\"text\" class=\"betroffen\" id=\"betroffen\" value=\"".
				eurobetrag($arrKazTransferDs["BETR_OFFEN"][$i])."\"";
				if(str_replace(",",".",$arrKazTransferDs["BETR_OFFEN"][$i])!=0)
					echo " style=\"color:red;\"";
				echo " readOnly></td>";
				echo "<td class=\"zeile2\">&nbsp;</td></tr>";
				//Zeile 3
				echo "<tr>";
				echo "<td class=\"text\"><textarea class=\"agb\" readonly>".
				$arrKazTransferDs["IKA_AUFTRAGGEBER_KONTO"][$i]."\n".
				$arrKazTransferDs["IKA_AUFTRAGGEBER_BLZ"][$i]."</textarea></td>";
				echo "<td style=\"text-align:center;vertical-align:middle;\">";
				echo "<table border=\"0\"><tr><td>";
				echo "<img src=\"/bilder/eingabe_ok.png\" class=\"quadrat\" alt=\"Datensatz ausw&auml;hlen\" onclick=\"auswahlid('".$arrKazTransferDs["IKA_ID"][$i]."');\">";
				echo "</td>";
				echo "<td>";
				echo "<img src=\"/bilder/muelleimer.png\" class=\"quadrat\" alt=\"Datensatz als nicht &uuml;bernommen buchen\" onclick=\"loeschen('".$arrKazTransferDs["IKA_ID"][$i]."');\">";
				echo "</td></tr></table>";
				echo "</td></tr>";
				echo "<tr><td colspan=\"6\" class=\"zwzeile\"><hr></td></tr>";
				
			}
			
		?>
		</table>
		<?php
		}	//Ende if
		?>
</td></tr></table>
</body>
</html>
<?php 
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>
