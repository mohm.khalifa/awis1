<?php
require_once('/daten/include/awisPraktikumsvereinbarungMail.inc');

try {
    $Benutzer = 'awis_jobs';
    $DebugLevel = 0;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
        }
    }

    $PVS = new awisPraktikum_Mail($DebugLevel,$Benutzer);
    $PVS->StarteVerarbeitung();

} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
}
?>

