<?php


 function autoload_a4eb36f7c258060e0e4d2937967345fb($class)
{
    if(!defined('__DIR__')){
        define('__DIR__',dirname(__FILE__));
    }

    $classes = array(
        'BestellArchiv' => __DIR__ . '/BestellArchiv.php',
        'AuftraegeProTag' => __DIR__ .'/AuftraegeProTag.php',
        'AuftraegeProTagResponse' => __DIR__ .'/AuftraegeProTagResponse.php',
        'AuftragsListe' => __DIR__ .'/AuftragsListe.php',
        'ArrayOfPartnerBestellBestaetigungV3' => __DIR__ .'/ArrayOfPartnerBestellBestaetigungV3.php',
        'PartnerBestellBestaetigungV3' => __DIR__ . '/PartnerBestellBestaetigungV3.php',
        'Versandart' => __DIR__ . '/Versandart.php',
        'ArrayOfBestaetigungPosition' => __DIR__ .'/ArrayOfBestaetigungPosition.php',
        'BestaetigungPosition' => __DIR__ . '/BestaetigungPosition.php',
        'Serialize' => __DIR__ . '/Serialize.php',
        'OrderLieferadresse' => __DIR__ . '/OrderLieferadresse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_a4eb36f7c258060e0e4d2937967345fb');

// Do nothing. The rest is just leftovers from the code generation.
{
}
