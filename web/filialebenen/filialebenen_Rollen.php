<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FER','%');
$TextKonserven[]=array('FRB','FRB_BEZEICHNUNG');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3802 = awisBenutzerRecht($con,3802,$AWISBenutzer->BenutzerName());
if($Recht3802==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT FER_KEY FROM (SELECT FER_KEY ';
	$SQL .= ' FROM Filialebenenrollen';
	$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
	$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';
	$SQL .= ' ORDER BY FER_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsFER = awisOpenRecordset($con, $SQL);
	if(isset($rsFER['FER_KEY'][0]))
	{
		$AWIS_KEY1=$rsFER['FER_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtFER_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT FER_KEY FROM (SELECT FER_KEY ';
	$SQL .= ' FROM Filialebenenrollen';
	$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
	$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';
	$SQL .= ' ORDER BY FER_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsFER = awisOpenRecordset($con, $SQL);
	if(isset($rsFER['FER_KEY'][0]))
	{
		$AWIS_KEY1=$rsFER['FER_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtFER_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./filialebenen_Rollen_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./filialebenen_Rollen_speichern.php');
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$AWIS_KEY1=-1;
}
elseif(isset($_GET['FER_KEY']))
{
	$AWIS_KEY1= floatval($_GET['FER_KEY']);
}
elseif(isset($_POST['txtFER_KEY']))
{
	$AWIS_KEY1 = floatval($_POST['txtFER_KEY']);
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	if(!isset($_GET['FERListe']))
	{
		$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuelleFilialRollen',$AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$SQL = 'SELECT DISTINCT Filialebenenrollen.*, FRB_BEZEICHNUNG';
$SQL .= ' FROM Filialebenenrollen';
$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';

$Bedingung='';
if($AWIS_KEY1!=0)
{
	$Bedingung.=' AND FER_KEY=0'.$AWIS_KEY1;
}

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY FER_Bezeichnung';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}
awis_Debug(1,$SQL);
// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
$rsFER = awisOpenRecordset($con, $SQL);
$rsFERZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
/*if($rsFERZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Filialebenenrollen gefunden.</span>';
}
*/
if($rsFERZeilen>1)						// Liste anzeigen
{
	echo '<form name=frmFilialebenenrollen action=./filialebenen_Main.php?cmdAktion=Rollen method=POST  enctype="multipart/form-data">';

	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './filialebenen_Main.php?cmdAktion=Rollen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FER_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FER_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'],350,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Rollen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FER_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FER_BEMERKUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEMERKUNG'],550,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Rollen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FRB_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FRB_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRB']['FRB_BEZEICHNUNG'],300,'',$Link);
	awis_FORM_ZeileEnde();

	if($rsFERZeilen>$MaxDSAnzahl)
	{
//		awis_FORM_Hinweistext('Ausgabe begrenzt auf '.$MaxDSAnzahl.'. Insgesamt '.$rsFERZeilen.' Zeilen gefunden.');
//		$rsFERZeilen=$MaxDSAnzahl;
	}

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsFERZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsFERZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./filialebenen_Main.php?cmdAktion=Rollen&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($FERZeile=$StartZeile;$FERZeile<$rsFERZeilen and $FERZeile<$StartZeile+$MaxDSAnzahl;$FERZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './filialebenen_Main.php?cmdAktion=Rollen&FER_KEY='.$rsFER['FER_KEY'][$FERZeile].'';
		awis_FORM_Erstelle_ListenFeld('FER_BEZEICHNUNG',$rsFER['FER_BEZEICHNUNG'][$FERZeile],0,350,false,($FERZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('FER_BEMERKUNG',$rsFER['FER_BEMERKUNG'][$FERZeile],0,550,false,($FERZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('FRB_BEZEICHNUNG',$rsFER['FRB_BEZEICHNUNG'][$FERZeile],0,300,false,($FERZeile%2),'',$Link);
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3802&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	awis_FORM_SchaltflaechenEnde();
	
	echo '</form>';
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	awis_FORM_FormularStart();
	echo '<form name=frmFilialebenenrollen action=./filialebenen_Main.php?cmdAktion=Rollen method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsFER['FER_KEY'][0])?$rsFER['FER_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuelleFilialRollen" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);
	echo '<input type=hidden name=txtFER_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Rollen&FERListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFER['FER_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFER['FER_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht3802&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FER_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsFER['FER_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtFER_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_FRB_KEY'].':',150);
	$SQL = 'SELECT FRB_KEY, FRB_BEZEICHNUNG';
	$SQL .= ' FROM FILIALEBENENROLLENBEREICHE ';
	$SQL .= ' INNER JOIN FILIALEBENENROLLENBERZUGRIFFE ON FBR_FRB_KEY = FRB_KEY';
	$SQL .= ' WHERE FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	
	awis_FORM_Erstelle_SelectFeld('FER_FRB_KEY',(isset($rsFER['FER_FRB_KEY'][0])?$rsFER['FER_FRB_KEY'][0]:''),500,$EditRecht,$con,$SQL);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_GUELTIGAB'].':',150);
	awis_FORM_Erstelle_TextFeld('FER_GUELTIGAB',($AWIS_KEY1===0?'':$rsFER['FER_GUELTIGAB'][0]),10,120,$EditRecht,'','','','D','L','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_GUELTIGBIS'].':',100);
	awis_FORM_Erstelle_TextFeld('FER_GUELTIGBIS',($AWIS_KEY1===0?'':$rsFER['FER_GUELTIGBIS'][0]),10,150,$EditRecht,'','','','D','L','',awis_PruefeDatum('31.12.2030'));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FER_BEMERKUNG',($AWIS_KEY1===0?'':$rsFER['FER_BEMERKUNG'][0]),50,350,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
/*	
	if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0)
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Mitglieder'));
//		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3802, $con, $RegisterSeite);
	}*/

	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3802&6)!=0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	if(($Recht3802&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht3802&8)==8 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}	
	if(($Recht3802&8)==8 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('href', 'cmdDrucken', './filialebenen_Rollen_drucken.php', '/bilder/drucker.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
	}	

	if(!isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
		awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
	}
	
	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsFER, $_POST, $rsFER, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>