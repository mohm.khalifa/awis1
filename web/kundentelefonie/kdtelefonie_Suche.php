<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht26000;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$TextKonserven = array();
	$TextKonserven[]=array('KTA','*');
	$TextKonserven[]=array('PBM','PBM_XBN_KEY');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','Abschlussdatum');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Offen');
	$TextKonserven[]=array('Wort','Alle');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Auswertung');
	$TextKonserven[]=array('Liste','lst_PEI_AUSWERTUNGEN');
	$TextKonserven[]=array('Liste','lst_Auftrag_Status');
	$TextKonserven[]=array('Liste','lst_Auftrag_Status_All');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','FilialenAnzeigen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswertung');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht26000 = $AWISBenutzer->HatDasRecht(26000);

	if($Recht26000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	//$Form->DebugAusgabe(1,$_POST);

		$Form->SchreibeHTMLCode('<form name="frmkdtelefoniesuche" action="./kdtelefonie_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');

		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KTA'));

		$Form->Formular_Start();

		// Datumsbereich festlegen
		/*
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*KTA_DATUM_VOM',($Param['SPEICHERN']=='on'?$Param['KTA_DATUM_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('01.01.Y'))),10);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',200);
		$Form->Erstelle_TextFeld('*KTA_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['KTA_DATUM_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('31.12.Y'))),10);
		$Form->ZeileEnde();
		*/
		//Filiale
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['FILIALE'].':',200);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$rsFil = $DB->RecordSetOeffnen($SQL);
			if ($rsFil->AnzahlDatensaetze() == 1)
			{
				$Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ID'),200);
				echo '<input type=hidden name="sucKTA_FIL_ID" value="'.$rsFil->FeldInhalt('FIL_ID').'">';
			}
			else
			{
				$Form->Erstelle_SelectFeld('*KTA_FIL_ID',($Param['SPEICHERN']=='on'?$Param['KTA_FIL_ID']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			}
		}
		else
		{
			$Form->Erstelle_TextFeld('*KTA_FIL_ID',($Param['SPEICHERN']=='on'?$Param['KTA_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
		}
		$Form->ZeileEnde();

		// Gebiet
		$FEBKeys = array();
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
			
			$SQL = 'SELECT * from KDTELEFONIEBEARBEITERINFOS';
			$SQL .= ' WHERE KTI_INFORMATION = \'GRUPPE\'';
			$rsGR = $DB->RecordSetOeffnen($SQL);
			while(!$rsGR->EOF())
			{
				//$Form->DebugAusgabe(1,$rsGR->FeldInhalt('KTI_WERT'));
				$SQL = 'SELECT * from BENUTZERGRUPPENBEZ';
				$SQL .= ' INNER JOIN Benutzergruppen ON XBG_XBB_KEY = XBB_KEY';
				$SQL .= ' INNER JOIN Benutzer on xbg_xbn_key = xbn_key';
				$SQL .= ' WHERE XBB_BEZ = \''.$rsGR->FeldInhalt('KTI_WERT').'\'';
				$rsGRBEZ = $DB->RecordSetOeffnen($SQL);
				//$Form->DebugAusgabe(1,$rsGRBEZ->FeldInhalt('XBG_XBN_KEY'));
				while(!$rsGRBEZ->EOF())
				{
					if($rsGRBEZ->FeldInhalt('XBG_XBN_KEY') == $AWISBenutzer->BenutzerID())
					{
						$KTB = $rsGR->FeldInhalt('KTI_KTB_ID');
						$SQL = 'SELECT * from KDTELEFONIEBEARBEITER';
						$SQL .= ' WHERE KTB_ID = '.$KTB;
						$rsBA = $DB->RecordSetOeffnen($SQL);
						
						if (!$rsBA->EOF())
						{
							$FEBKeys[]=$rsBA->FeldInhalt('KTB_FEB_KEY');
						}
						//$Form->DebugAusgabe(1,$Param['GRUPPE']);
					}
					$rsGRBEZ->DSWeiter();
				}
				$rsGR->DSWeiter();
			}
			
		//Alle Gebiete anzeigen
			if (($Recht26000&2)==2)
			{
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
				$SQL .= ' FROM Filialebenen';
				$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= ' ORDER BY 2';
			}
			elseif(isset($KTB))
			{
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
				$SQL .= ' FROM Filialebenen';
				$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= ' AND EXISTS';
				$SQL .= '  (select DISTINCT XX1_FEB_KEY';
				$SQL .= '  FROM V_FILIALEBENENROLLEN_AKTUELL';
				$SQL .= '  WHERE  XX1_FEB_KEY = FEB_KEY';
				$i=0;
				foreach ($FEBKeys as $FEB_key)
				{
					if ($i==0)
					{
						$SQL .= " AND (xx1_pfadkey || '/' like '%/".$FEB_key."/%'";
					}
					else
					{
						$SQL .= " OR xx1_pfadkey || '/' like '%/".$FEB_key."/%'";
					}
					$i++;
				}
				$SQL .= '))';
				$SQL .= ' ORDER BY 2';
			}
			else
			{
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
				$SQL .= ' FROM Filialebenen';
				$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= ' AND EXISTS';
				$SQL .= '  (select DISTINCT XX1_FEB_KEY';
				$SQL .= '  FROM V_FILIALEBENENROLLEN_AKTUELL';
				$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
				$SQL .= '  WHERE  XX1_FEB_KEY = FEB_KEY';
				$SQL .= '  AND XX1_KON_KEY ='.$AWISBenutzer->BenutzerKontaktKEY();
				$SQL .= ')';
				$SQL .= ' ORDER BY 2';
					
				//echo $SQL;
				//die();
			}
			$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
			$Form->ZeileEnde();
		}		
		//Bericht
		if (($Recht26000&8)==8)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_STATUS'].':',200);
			$Daten = $AWISSprachKonserven['Liste']['lst_Auftrag_Status_All'];
			//$Form->DebugAusgabe(1,$Daten);
			$Daten = explode('|',$Daten);
			$Form->Erstelle_SelectFeld('*KTA_STATUS',($Param['SPEICHERN']=='on'?$Param['KTA_STATUS']:'0'),200,true,'',$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','',$Daten);
			$Form->ZeileEnde();
		}
		else
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_STATUS'].':',200);
			$Daten = $AWISSprachKonserven['Liste']['lst_Auftrag_Status'];
			//$Form->DebugAusgabe(1,$Daten);
			$Daten = explode('|',$Daten);
			$Form->Erstelle_SelectFeld('*KTA_STATUS',($Param['SPEICHERN']=='on'?$Param['KTA_STATUS']:''),200,true,'','~'.$AWISSprachKonserven['Wort']['Offen'],'','','',$Daten);
			$Form->ZeileEnde();
		}
		
		if (($Recht26000&4)==4)
		{
			// Bis auf FilialebeneAuswahl kann gespeichert werden
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AlleAnzeigen'].':',200);
			$Form->Erstelle_Checkbox('*AlleAnzeigen',($Param['SPEICHERN']=='on'?$Param['ALLE']:''),20,true,'on');
			$Form->ZeileEnde();
		}

		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
		//$Form->Schaltflaeche('image', 'cmdMail', '', '/bilder/cmd_statistik.png', $AWISSprachKonserven['Wort']['Auswertung'], 'W');
		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>