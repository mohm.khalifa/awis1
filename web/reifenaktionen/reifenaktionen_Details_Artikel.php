<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RST', '%');
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_RST_IMPORTSTATUS');
    $TextKonserven[]=array('Liste','lst_RST_STATUS');
    $TextKonserven[]=array('Liste','lst_ROLLRES');
    $TextKonserven[]=array('Liste','lst_WETGRIP');
    $TextKonserven[]=array('Liste','lst_NOISECL');
	

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15200 = $AWISBenutzer->HatDasRecht(15200);
	if(($Recht15200)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$RRAParam = unserialize($AWISBenutzer->ParameterLesen('Formular_RRA_Artikel'));

	$Form->DebugAusgabe(1,$RRAParam);
	
	if(isset($_GET['sucRRA_ARTIKEL']))
	{
		$AWIS_KEY1 = $_GET['sucRRA_ARTIKEL'];
	}
	
	
	if(!isset($RRAParam['BLOCK']) OR $RRAParam['RRA_KEY']!=$AWIS_KEY1)
	{
		$RRAParam['BLOCK']=1;
		$RRAParam['KEY']='';
		$RRAParam['RRA_KEY']=$AWIS_KEY1;
	}

	$Form->DebugAusgabe(1,$RRAParam);
	
	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY RRP_ATUNR ASC';
		$RRAParam['ORDER']=$ORDERBY;
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$ORDERBY = '';
		foreach($SortFelder AS $SortFeld)
		{
			$ORDERBY .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($ORDERBY==''?'':' ORDER BY '.$ORDERBY);
		$RRAParam['ORDER']=$ORDERBY;
	}

	$SQL  = ' SELECT ';
	$SQL .= '  a.rrp_key';
	$SQL .= ' ,a.rrp_atunr';
	$SQL .= ' ,a.rrp_bezeichnungww';
	$SQL .= ' ,row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';
	$SQL .= ' SELECT DISTINCT';
	$SQL .= '  rrp_key';
	$SQL .= ' ,rrp_atunr';
	$SQL .= ' ,rrp_bezeichnungww';
	$SQL .= ' FROM v_reifenpflege rrp';
	$SQL .= ' LEFT JOIN v_reifenpflegepreise rpp ON rpp.rpp_rrp_key = rrp.rrp_key';
	$SQL .= ' INNER JOIN reifenstamm rst ON rst.rst_key=rrp.rrp_rst_key';
	$SQL .= ' INNER JOIN reifenaktionen rra ON ';
	$SQL .= ' 	(';
	$SQL .= ' 		(rra.rra_wgr_id = rrp.rrp_wgr_id OR rra.rra_wgr_id IS NULL)';
	$SQL .= ' 		AND (rra.rra_wug_id = rrp.rrp_wug_id OR rra.rra_wug_id IS NULL)';
	$SQL .= ' 		AND (rra.rra_kenn1 = rrp.rrp_kenn1 OR rra.rra_kenn1 IS NULL)';
	$SQL .= ' 		AND (rra.rra_reh_key = rst.rst_reh_key OR rra.rra_reh_key IS NULL)';
	$SQL .= " 		AND (rra.rra_atunrliste like '%' || rrp.rrp_atunr || '%' OR rra.rra_atunrliste IS NULL OR length(rra.rra_atunrliste) = 0)";
	$SQL .= ' 		AND (rra.rra_lan_code = rpp.rpp_lan_code OR rra.rra_lan_code IS NULL)';
	$SQL .= ' 	)';
	$SQL .= ' WHERE rra.rra_key=0'.$AWIS_KEY1;
	$SQL .= ' ) a';

	if(isset($_GET['RRPListe']))
	{
	    $AWIS_KEY2=0;
	}
	elseif(isset($_GET['RRP_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['RRP_KEY']);
	}
	elseif($AWIS_KEY2=='')
	{
		if($RRAParam['RRA_KEY']==$AWIS_KEY1)
		{
			$AWIS_KEY2 = $RRAParam['KEY'];
		}
		else
		{
			$RRAParam['BLOCK']=1;
			$RRAParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}

	if($AWIS_KEY2<>0)
	{
		//$SQL .= ' AND RRP_KEY = 0'.$AWIS_KEY2;
		$_GET['RRP_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	
	if(intval($AWIS_KEY2)<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$RRAParam['BLOCK']=$Block;
		}
		elseif(isset($RRAParam['BLOCK']))
		{
			$Block=$RRAParam['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
		$RRAParam['BLOCK']=$Block;
	}

	$SQL .= $ORDERBY;
	$rsRRP = $DB->RecordsetOeffnen($SQL);
	$Form->DebugAusgabe(1,$SQL);
    $Form->Formular_Start();

    $Form->ZeileStart();

    $Link = './reifenaktionen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
    $Link .= '&SSort=RRP_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_ATUNR'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'],100,'',$Link);
    
    $Link = './reifenaktionen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
    $Link .= '&SSort=RRP_BEZEICHNUNGWW'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_BEZEICHNUNGWW'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNGWW'],350,'',$Link);
    
    $Form->ZeileEnde();
    
    // Blockweise
    $StartZeile=0;
    if(isset($_GET['Block']))
    {
    	$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
    }
    
    $RRAParam['KEY']='';
    $RRAParam['RRA_KEY']=$AWIS_KEY1;
    
    $DS=0;
    
	if($rsRRP->AnzahlDatensaetze()>=1 OR isset($_GET['RRPListe']) OR !isset($_GET['RRP_KEY']))
	{
		while(!$rsRRP->EOF())
		{
			$Form->ZeileStart();

			//$Style = $rsRRP->FeldInhalt('STYLE');
			$Style = '';
            $Link = '../reifenpflege/reifenpflege_Main.php?cmdAktion=Details&RRP_KEY=' . $rsRRP->FeldInhalt('RRP_KEY') . '';
			$Form->Erstelle_ListenFeld('*RRP_ATUNR',$rsRRP->FeldInhalt('RRP_ATUNR'),20,100,false,($DS%2),$Style,$Link,'T');
			$Form->Erstelle_ListenFeld('*RRP_BEZEICHNUNGWW',$rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW'),20,350,false,($DS%2),$Style,'','T');
			$Form->ZeileEnde();

			$rsRRP->DSWeiter();
			$DS++;
		}

		$Link = './reifenaktionen_Main.php?cmdAktion=Details&Seite=Artikel';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsRRP->AnzahlDatensaetze()<1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht15200&6);
		$AWIS_KEY2 = $rsRRP->FeldInhalt('RRP_KEY');
		$RRAParam['KEY']=$AWIS_KEY2;
		$RRAParam['RRA_KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('RRA_KEY',$AWIS_KEY1);
		$Form->Erstelle_HiddenFeld('RRP_KEY',$AWIS_KEY2);

		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
		
		$Form->Formular_Ende();
	}

	$AWISBenutzer->ParameterSchreiben('Formular_RRA_Artikel',serialize($RRAParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306301739");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306301740");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>