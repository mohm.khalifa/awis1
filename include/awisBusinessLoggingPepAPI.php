<?php
require_once 'awisBusinessLoggingAPI.php';

class AWISBusinessLoggingPEPAPI extends AWISBusinessLoggingAPI
{
    const URL_BUSINESS_LOGGING = 'https://dsp.atu.de/service-java-springboot-112-pep-bl/api/v1/createHeader';
    const URL_BUSINESS_LOGGING_TEST = 'https://dst.atu.de/service-java-springboot-112-pep-bl/api/v1/createHeader';
    const SERVICE_NR_DEFAULT = 10000; //@link https://developer.atu.de/confluence/display/SOA/Definition+-+Service+Nummern

    function __construct($ServiceNummer = self::SERVICE_NR_DEFAULT)
    {
        parent::__construct($ServiceNummer);
        $this->initUrls(self::URL_BUSINESS_LOGGING, self::URL_BUSINESS_LOGGING_TEST);
    }




}