<?php
require_once 'awisWerkzeuge.inc';
require_once 'awisRest.inc';
require_once 'awisFormular.inc';

/**
 * Klasse um die BusinessLogging-Services auf der DSP anzusprechen.
 * Klasse sollte abgeleitet werden. @see AWISBusinessLoggingPEPAPI
 *
 * Class AWISBusinessLoggingAPI
 */
class AWISBusinessLoggingAPI
{
    const ROUTE_CREATE_HEADER = "/api/v1/createHeader";
    const ROUTE_LOG_ENTRY = "/api/v1/logEntry";

    private $_URL_BUSINESS_LOGGING = '';

    protected $_awisRest;
    protected $_awisWerkzeuge;
    protected $_Form;

    protected $_ServiceNr;


    function __construct($ServiceNr)
    {
        $this->_awisRest = new awisRest('awisTeamplanAPI');
        $this->_awisWerkzeuge = new awisWerkzeuge();
        $this->_Form = new awisFormular();
        $this->_ServiceNr = $ServiceNr;
    }

    protected function initUrls($TestUrl, $ProdUrl){
        $Level = $this->_awisWerkzeuge->awisLevel();

        //Wenn nicht Prod
        if($Level != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV and $Level != awisWerkzeuge::AWIS_LEVEL_SHUTTLE){
            $this->_URL_BUSINESS_LOGGING = $TestUrl;
        }else{
            $this->_URL_BUSINESS_LOGGING = $ProdUrl;
        }

    }

    public function ErstelleLogheader($Subject, $BusinessKey){
        $Parameter = array();
        $Parameter['processSubject'] = $Subject;
        $Parameter['businessKey'] = $BusinessKey;


        $Erg = $this->_awisRest->erstelleRequest($this->_URL_BUSINESS_LOGGING . self::ROUTE_CREATE_HEADER,'JSONPOST','JSON',$Parameter);

        if(!isset($Erg['value'])  or (isset($Erg['errors']) and $Erg['errors'] != '' )){
            return false;
        }

        return $Erg['value'];
    }

    public function ErstelleLogeintrag($UniqueProcessId, $ProcessStepName, $ResultTextShort, $ResultDescription, $ResultCode){
        $Parameter = array();
        $Parameter['processStepName'] = $ProcessStepName;
        $Parameter['resultTextShort'] = $ResultTextShort;
        $Parameter['resultDescription'] = $ResultDescription;
        $Parameter['resultCode'] = $ResultCode;
        $Parameter['serviceNr'] = $this->_ServiceNr;



        $Erg = $this->_awisRest->erstelleRequest($this->_URL_BUSINESS_LOGGING . self::ROUTE_LOG_ENTRY . '?uniqueProcessId=' . $UniqueProcessId,'JSONPOST','JSON',$Parameter);

        if(!isset($Erg['value']) or (isset($Erg['value']) and $Erg['value'] != 'OK' )){
            return false;
        }
        return true;
    }


}