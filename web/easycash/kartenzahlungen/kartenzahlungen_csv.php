<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	ini_set('max_execution_time',600);

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KNZ','%');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	// Parameter
	$Trenner=";";

	@ob_clean();

	//header("Cache-Control: no-cache, must-revalidate");
	//header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="kartenzahlungen_export.csv"');
	
	
	if(isset($_GET['ID']))                  // Parameter kommen base64 kodiert
	{
		// Parameter werden durch & getrennt
		$ParameterListe = explode('&',base64_decode($_GET['ID']));
		foreach($ParameterListe AS $Parameter)
		{
			// Jeder Parameter hat das Format <Param>=<Operator>~<Wert>
			$Parameter = explode('=',$Parameter);
			$Param[$Parameter[0]]=(isset($Parameter[1])?urldecode($Parameter[1]):'');
		}
		//var_dump($Param);
	}
	else
	{
		die();
	}
	
	$FeldlisteUeberschrift = 'KNZ_PDF_FILNR,KNZ_FILIALE,KNZ_DATUM,KNZ_ART,KNZ_BETRAG,KNZ_PDF_TERMINALID,KNZ_TRACENR,KNZ_PDF_BLZ,KNZ_KTONR';
	$FeldListeSelect = 'KNZ_FILNR, FIL_BEZ, KNZ_DATUM, KNZ_ART, KNZ_BETRAG, KNZ_TERMINALID, KNZ_TRACENR, KNZ_BLZ, KNZ_KTONR';
	
	$FeldWerteUeberschrift='';
	foreach (explode(',', $FeldlisteUeberschrift) as $value)
	{
		$FeldWerteUeberschrift = $FeldWerteUeberschrift.$AWISSprachKonserven['KNZ'][$value].';';
	}
	$FeldWerteUeberschrift = rtrim($FeldWerteUeberschrift, ';');

	$SQL = ' SELECT ' . $FeldListeSelect;
	$SQL .= ' FROM KARTENZAHLUNGEN knz';
 	$SQL .= ' INNER JOIN FILIALEN fil';
 	$SQL .= ' ON fil.FIL_ID = knz.KNZ_FILNR';
	
	// Alle Parameter auswerten
	// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
	//			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
	$Bedingung = "";
	if (isset($Parameter['KNZ_KEY']) AND $Parameter['KNZ_KEY'] != '' )
	{
		$Param = explode('~',$Parameter['KNZ_KEY']);
		//$Param = str_replace($Parameter['KNZ_KEY'], ';', ',');
			
		$foo = str_replace(';', ',', $Parameter['KNZ_KEY']);
		$Bedingung .= " AND KNZ_KEY in (" . $foo . ")";
		//echo ("Parameter". $Parameter['KNZ_KEY'] . "<br>" . "Foo: " . $foo . "<br>" . "Bedingung: " . $Bedingung);
	}
	
	if (isset($Parameter['KNZ_FILNR']) AND $Parameter['KNZ_FILNR'] != '')
	{
		$Bedingung .= " AND KNZ_FILNR = " . $Parameter['KNZ_FILNR'];
	}
	if(isset($Parameter['KNZ_DATUM_VON']) AND $Parameter['KNZ_DATUM_VON']!='')
	{
		$Bedingung .= ' AND KNZ_DATUM >= ' . $this->_DB->FeldInhaltFormat('D',$Parameter['KNZ_DATUM_VON']);
	}
	if (isset($Parameter['KNZ_DATUM_BIS']) AND $Parameter['KNZ_DATUM_BIS'] != '')
	{
		$Bedingung .= ' AND KNZ_DATUM <= ' . $this->_DB->FeldInhaltFormat('D',$Parameter['KNZ_DATUM_BIS']);
	}
	
	if(isset($Parameter['KNZ_BETRAG']) AND $Parameter['KNZ_BETRAG']!='')
	{
		$Bedingung .= ' AND KNZ_BETRAG = ' . $this->_DB->FeldInhaltFormat('N2',$Parameter['KNZ_BETRAG']);
	}
	if(isset($Parameter['KNZ_ART']) AND $Parameter['KNZ_ART']!='')
	{
		$Bedingung .= ' AND KNZ_ART = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_ART']);
	}
	
	if(isset($Parameter['KNZ_BLZ']) AND $Parameter['KNZ_BLZ']!='')
	{
		$Bedingung .= ' AND KNZ_BLZ = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_BLZ']);
	}
	
	if(isset($Parameter['KNZ_KTONR']) AND $Parameter['KNZ_KTONR']!='')
	{
		$Bedingung .= ' AND KNZ_KTONR = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_KTONR']);
	}
	
	if ($Bedingung != "")
	{
		$SQL .= " WHERE " . substr($Bedingung, 4);
	}
	
	//var_dump($SQL);
	//die();
	
	$Abfrage = $SQL;
	// Blockgr��e festlegen
	$Blockgroesse = 5000;

	for($i=1;$i<999;$i++)
	{
		$SQL = 'SELECT ABFRAGE2.* FROM (';
		$SQL .= 'SELECT ABFRAGE1.*, row_number() over(order by 1) AS zeile';
		$SQL .= ' FROM ('.$Abfrage.') ABFRAGE1';
		$SQL .= ') ABFRAGE2 WHERE zeile >= '.(($i-1)*$Blockgroesse).' AND zeile < '.(($i)*$Blockgroesse);

		$rsDaten = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1, $SQL);
		if($rsDaten->AnzahlDatensaetze()==0)
		{
			break;
		}
		if($i==1)		// Beim ersten Mal den Kopf schreiben
		{
			$Felder = $rsDaten->SpaltenNamen();
			$Zeile = '';
			
			$Zeile = $FeldWerteUeberschrift;
			echo $Zeile."\n";
		}

		while(!$rsDaten->EOF())
		{
			$Zeile = '';
			foreach($Felder AS $FeldName)
			{
				if($FeldName!='ZEILE')
				{
					$Text = str_replace("\n",'',$rsDaten->FeldInhalt($FeldName));
					$Text = str_replace("\r",'',$Text);
					$Text = str_replace("\n",'',$Text);
					$Text = str_replace("\t",'',$Text);
					$Zeile .= $Trenner.$Text;
				}
			}
			echo substr($Zeile,1)."\n";

			$rsDaten->DSWeiter();
		}
	}
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>