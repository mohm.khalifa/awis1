<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;
global $CursorFeld;

$KonditionenBereich = 1;
$KonditionenBereichBezeichnung = 'Warengruppen';

$TextKonserven = array();
$TextKonserven[]=array('CKM','%');
$TextKonserven[]=array('TTT','CKM_KONDBLATT');
$TextKonserven[]=array('CKD','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','CKM_EINSATZSTUFE');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3707 = awisBenutzerRecht($con,3707,$AWISBenutzer->BenutzerName());
if($Recht3707==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Daten = explode('|',$AWISSprachKonserven['Liste']['CKM_EINSATZSTUFE']);
foreach($Daten AS $EinsatzStufe)
{
	$EinsatzStufe = explode('~',$EinsatzStufe);
	$EinsatzStufen[$EinsatzStufe[0]]=$EinsatzStufe[1];
}

$SQL = 'SELECT *';
$SQL .= ' FROM CRMKonditionsModelle';
$SQL .= ' WHERE CKM_Bereich = '.$KonditionenBereich;
if(isset($_GET['CKMKEY']))
{
	$SQL .= ' AND CKM_KEY=0'.intval($_GET['CKMKEY']);
}
elseif(isset($AWIS_KEY1) AND $AWIS_KEY1>0)
{
	$SQL .= ' AND CKM_KEY=0'.intval($AWIS_KEY1);
}


if(!isset($_GET['CKMSort']))
{
	$SQL .= ' ORDER BY CKM_Bezeichnung';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CKMSort']);
}



$rsCKM = awisOpenRecordset($con,$SQL);
$rsCKMZeilen = $awisRSZeilen;


if($rsCKMZeilen > 1 or isset($_GET['CKMListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	if(($Recht3707&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&CKMKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'';
	$Link .= '&CKMSort=CKM_BEZEICHNUNG'.((isset($_GET['CKMSort']) AND ($_GET['CKMSort']=='CKM_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKM']['CKM_BEZEICHNUNG'],250,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKM']['CKM_EINSATZSTUFE'],450,'','');


	awis_FORM_ZeileEnde();

	for($CKMZeile=0;$CKMZeile<$rsCKMZeilen;$CKMZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3707&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&CKMKEY='.$rsCKM['CKM_KEY'][$CKMZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&Del='.$rsCKM['CKM_KEY'][$CKMZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CKMZeile%2));

	awis_FORM_Erstelle_ListenFeld('#CKM_BEZEICHNUNG',$rsCKM['CKM_BEZEICHNUNG'][$CKMZeile],20,250,false,($CKMZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('#CKM_EINSATZSTUFE',$EinsatzStufen[$rsCKM['CKM_EINSATZSTUFE'][$CKMZeile]],20,450,false,($CKMZeile%2),'','','T');


	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCKM_KEY type=hidden value=0'.(isset($rsCKM['CKM_KEY'][0])?$rsCKM['CKM_KEY'][0]:'').'>';
	echo '<input name=txtCKM_BEREICH type=hidden value='.$KonditionenBereich.'>';

	$AWIS_KEY1 = (isset($rsCKM['CKM_KEY'][0])?$rsCKM['CKM_KEY'][0]:'');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Konditionen&Seite=".$KonditionenBereichBezeichnung."&CKMListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKM['CKM_KEY'][0])?$rsCKM['CKM_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKM['CKM_KEY'][0])?$rsCKM['CKM_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CKM_BEZEICHNUNG',(isset($rsCKM['CKM_BEZEICHNUNG'][0])?$rsCKM['CKM_BEZEICHNUNG'][0]:''),40,300,($Recht3707&6));
	$CursorFeld = 'CKM_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CKM_BEMERKUNG',(isset($rsCKM['CKM_BEMERKUNG'][0])?$rsCKM['CKM_BEMERKUNG'][0]:''),80,300,($Recht3707&6));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_BEZEICHNUNG_OPAL'].':',150);
	awis_FORM_Erstelle_TextFeld('CKM_BEZEICHNUNG_OPAL',(isset($rsCKM['CKM_BEZEICHNUNG_OPAL'][0])?$rsCKM['CKM_BEZEICHNUNG_OPAL'][0]:''),80,300,($Recht3707&6));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_BASIS'].':',150);
	awis_FORM_Erstelle_TextFeld('CKM_BASIS',(isset($rsCKM['CKM_BASIS'][0])?$rsCKM['CKM_BASIS'][0]:''),80,300,($Recht3707&6));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_EINSATZSTUFE'].':',150);
	$Daten = explode("|",$AWISSprachKonserven['Liste']['CKM_EINSATZSTUFE']);
	awis_FORM_Erstelle_SelectFeld('CKM_EINSATZSTUFE',(isset($rsCKM['CKM_EINSATZSTUFE'][0])?$rsCKM['CKM_EINSATZSTUFE'][0]:''),100,($Recht3707&6),$con,'','','3','','',$Daten,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_STATUS'].':',150);
	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_AktivInaktiv']);
	awis_FORM_Erstelle_SelectFeld('CKM_STATUS',(isset($rsCKM['CKM_STATUS'][0])?$rsCKM['CKM_STATUS'][0]:''),100,($Recht3707&6),$con,'','','3','','',$Daten,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKM']['CKM_KONDBLATT'].':',150,'','',$AWISSprachKonserven['TTT']['CKM_KONDBLATT']);
	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('CKM_KONDBLATT',(isset($rsCKM['CKM_KONDBLATT'][0])?$rsCKM['CKM_KONDBLATT'][0]:''),100,($Recht3707&6),$con,'','','3','','',$Daten,'');
	awis_FORM_ZeileEnde();

	if(isset($rsCKM['CKM_KEY'][0]))
	{
		awis_FORM_Trennzeile();

		$SQL = ' SELECT * ';
		$SQL .= ' FROM CRMKonditionsModellDetails';
		$SQL .= ' WHERE CKD_CKM_KEY=0'.$AWIS_KEY1;

		if(isset($_GET['CKDKEY']))
		{
			$SQL .= ' AND CKD_KEY = 0'.intval($_GET['CKDKEY']);
		}
		$SQL .= ' ORDER BY CKD_Bereich, CKD_Sortierung, CKD_Bezeichnung';

		$rsCKD = awisOpenRecordset($con, $SQL);
		$rsCKDZeilen = $awisRSZeilen;

		if(isset($_GET['CKDKEY']))
		{
			echo '<input name=txtCKD_KEY value=0'.(isset($rsCKD['CKD_KEY'][0])?$rsCKD['CKD_KEY'][0]:'').' type=hidden>';
			echo '<input name=txtCKD_CKM_KEY value=0'.$AWIS_KEY1.' type=hidden>';

			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Konditionen&Seite=".$KonditionenBereichBezeichnung."&CKMKEY=".$rsCKM['CKM_KEY'][0]."&CKDListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKD['CKD_KEY'][0])?$rsCKD['CKD_USER'][0]:'')));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKD['CKD_KEY'][0])?$rsCKD['CKD_USERDAT'][0]:'')));
			awis_FORM_InfoZeile($Felder,'');

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_BEREICH'].':',150);
			awis_FORM_Erstelle_TextFeld('CKD_BEREICH',(isset($rsCKD['CKD_BEREICH'][0])?$rsCKD['CKD_BEREICH'][0]:''),30,200,($Recht3707&6),'');
			$CursorFeld='CKD_BEREICH';
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_BEZEICHNUNG'].':',150);
			awis_FORM_Erstelle_TextFeld('CKD_BEZEICHNUNG',(isset($rsCKD['CKD_BEZEICHNUNG'][0])?$rsCKD['CKD_BEZEICHNUNG'][0]:''),30,200,($Recht3707&6),'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_BEMERKUNG'].':',150);
			awis_FORM_Erstelle_TextFeld('CKD_BEMERKUNG',(isset($rsCKD['CKD_BEMERKUNG'][0])?$rsCKD['CKD_BEMERKUNG'][0]:''),80,500,($Recht3707&6),'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_TEXT'].':',150);
			awis_FORM_Erstelle_TextFeld('CKD_TEXT',(isset($rsCKD['CKD_TEXT'][0])?$rsCKD['CKD_TEXT'][0]:''),20,200,($Recht3707&6),'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_SORTIERUNG'].':',150);
			awis_FORM_Erstelle_TextFeld('CKD_SORTIERUNG',(isset($rsCKD['CKD_SORTIERUNG'][0])?$rsCKD['CKD_SORTIERUNG'][0]:''),3,200,($Recht3707&6),'','','','N0','','',10,3);
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKD']['CKD_STATUS'].':',150);
			$StatusText = explode("|",$AWISSprachKonserven['Liste']['lst_AktivInaktiv']);
			awis_FORM_Erstelle_SelectFeld('CKD_STATUS',(isset($rsCKD['CKD_STATUS'][0])?$rsCKD['CKD_STATUS'][0]:''),130,($Recht3707&6),$con,'','','A','','',$StatusText);
			awis_FORM_ZeileEnde();
		}
		else
		{
			awis_FORM_ZeileStart();

			if(($Recht3707&6)>0)
			{
				$Icons[] = array('new','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&CKMKEY=0'.$AWIS_KEY1.'&CKDKEY=0');
				awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
			}

			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKD']['CKD_BEREICH'],200,'','');
			$Link = './crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'';
			$Link .= '&CKDSort=CKD_BEZEICHNUNG'.((isset($_GET['CKDSort']) AND ($_GET['CKDSort']=='CKD_BEZEICHNUNG'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKD']['CKD_BEZEICHNUNG'],350,'',$Link);
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKD']['CKD_BEMERKUNG'],350,'','');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKD']['CKD_TEXT'],200,'','');

			awis_FORM_ZeileEnde();

			for($CKDZeile=0;$CKDZeile<$rsCKDZeilen;$CKDZeile++)
			{
				awis_FORM_ZeileStart();
				$Icons = array();
				if(($Recht3707&6)>0)	// Ändernrecht
				{
					$Icons[] = array('edit','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&CKMKEY=0'.$AWIS_KEY1.'&CKDKEY='.$rsCKD['CKD_KEY'][$CKDZeile]);
					$Icons[] = array('delete','./crm_Main.php?cmdAktion=Konditionen&Seite='.$KonditionenBereichBezeichnung.'&CKMKEY=0'.$AWIS_KEY1.'&Del='.$rsCKD['CKD_KEY'][$CKDZeile]);
				}
				awis_FORM_Erstelle_ListeIcons($Icons,38,($CKDZeile%2));

				awis_FORM_Erstelle_ListenFeld('#CKD_BEREICH',$rsCKD['CKD_BEREICH'][$CKDZeile],20,200,false,($CKDZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('#CKD_BEZEICHNUNG',$rsCKD['CKD_BEZEICHNUNG'][$CKDZeile],20,350,false,($CKDZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('#CKD_BEMERKUNG',$rsCKD['CKD_BEMERKUNG'][$CKDZeile],20,350,false,($CKDZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('#CKD_TEXT',$rsCKD['CKD_TEXT'][$CKDZeile],10,200,false,($CKDZeile%2),'','','T');


				awis_FORM_ZeileEnde();
			}
		}
	}

	awis_FORM_FormularEnde();
}
?>