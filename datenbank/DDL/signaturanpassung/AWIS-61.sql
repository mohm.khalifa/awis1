--------------------------------------------------------
--  Textkonserven anpassen
--------------------------------------------------------
SET DEFINE OFF;
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, Arnaud Vuye'
WHERE XTX_KENNUNG = 'Geschaeftsfuehrer_ATU_Handel'
AND XTX_BEREICH   = 'Wort';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung), Andreas Schmidt'
WHERE XTX_KENNUNG = 'Geschaeftsfuehrer_ATU_Filiale'
AND XTX_BEREICH   = 'Wort';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihre Personalabteilung<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye'
WHERE XTX_KENNUNG = 'BEW_SIGNATUR_ATU'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Pers�nlich haftende Gesellschafterin:
A.T.U Auto-Teile-Unger GmbH, Weiden i.d.OPf.
Sitz: Weiden i.d.OPf., HRB 745
Gesch�ftsf�hrer:
J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, Arnaud Vuye'
WHERE XTX_KENNUNG = 'BEW_PDF_1_FOOTER_SPALTE2'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Pers�nlich haftende Gesellschafterin:
A.T.U Auto-Teile-Unger GmbH, Weiden i.d.OPf.
Sitz: Weiden i.d.OPf., HRB 745
Gesch�ftsf�hrer:
J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt'
WHERE XTX_KENNUNG = 'BEW_PDF_2_FOOTER_SPALTE2'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = '<br>
Freundliche Gr��e<br>
<br>
Ihre Personalabteilung <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt'
WHERE XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_1'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = '<br>
Freundliche Gr��e<br>
<br>
Ihre Personalabteilung<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye'
WHERE XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_2'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = '<br>
Freundliche Gr��e<br>
<br>
Personalentwicklung / Ausbildung <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Ulrike Zintl: +49 (0) 9 61 - 3 06 - 6038<br>
<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt'
WHERE XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_AZUBI_1'
AND XTX_BEREICH   = 'BEW';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = '<br>
Freundliche Gr��e<br>
<br>
Personalentwicklung / Ausbildung <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Ulrike Zintl: +49 (0) 9 61 - 3 06 - 6038<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye'
WHERE XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_AZUBI_2'
AND XTX_BEREICH   = 'BEW';
commit;

--------------------------------------------------------
--  Mailversandtexte anpassen
--------------------------------------------------------
UPDATE MAILVERSANDTEXTE
SET MVT_TEXT = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>

<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	font-size:12.0pt;
	font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
span.E-MailFormatvorlage17
	{mso-style-type:personal-compose;
	font-family:Arial;
	color:windowtext;}
span.E-MailFormatvorlage19
	{mso-style-type:personal;
	font-family:Arial;
	color:windowtext;}
span.E-MailFormatvorlage20
	{mso-style-type:personal;
	font-family:Arial;
	color:windowtext;}
@page Section1
	{size:595.3pt 841.9pt;
	margin:1.0in 1.25in 1.0in 1.25in;}
div.Section1
	{page:Section1;}
-->
</style>

</head>

<body lang=EN-US link=blue vlink=purple>

<div class=Section1>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>Sehr geehrte Damen und Herren,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>anbei senden wir Ihnen folgende Dateien mit
Mobilit�tsgarantieinhabern:<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>atu_dat_660_avd.txt���� 06 Monate G�ltigkeit<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>atu_dat_666_avd.txt���� 12 Monate G�ltigkeit<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>atu_dat_667_avd.txt���� 24 Monate G�ltigkeit<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>atu_dat_668_avd.txt���� 12 Monate G�ltigkeit (Roller)<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>Mit freundlichen Gr��en<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>i. A. Manuel Prem</span></font><font face="Arial Unicode MS"><span
lang=DE style=''font-family:"Arial Unicode MS"''><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>Finanzbuchhaltung</span></font><span lang=DE><o:p></o:p></span></p>

<p class=MsoNormal><font size=1 face=Arial><span lang=DE style=''font-size:8.0pt;
font-family:Arial''>___________________________________________________________</span></font><span
lang=DE><o:p></o:p></span></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>&nbsp;<o:p></o:p></span></font></p>

<p class=MsoNormal><b><font size=2 face=Arial><span lang=DE style=''font-size:
10.0pt;font-family:Arial;font-weight:bold''>A.T.U Auto-Teile-Unger<o:p></o:p></span></font></b></p>

<p class=MsoNormal><b><font size=2 face=Arial><span style=''font-size:10.0pt;
font-family:Arial;font-weight:bold''>GmbH &amp; Co. KG<o:p></o:p></span></font></b></p>

<p class=MsoNormal><font size=2 face=Arial><span style=''font-size:10.0pt;
font-family:Arial''>Dr.-Kilian-Str. </span></font><font size=2 face=Arial><span
lang=DE style=''font-size:10.0pt;font-family:Arial''>11<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>92637 Weiden i. d. OPf.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>&nbsp;<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''>Tel.:&nbsp;&nbsp;&nbsp;&nbsp; +49 (0) 9 61 - 3 06 58 37</span></font><span
lang=DE><o:p></o:p></span></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=FR style=''font-size:10.0pt;
font-family:Arial''>Fax:&nbsp;&nbsp;&nbsp;&nbsp; +49 (0) 9 61 - 3 06 9 34 58 37</span></font><o:p></o:p></p>

<p class=MsoNormal><font size=3 face="Times New Roman"><span lang=FR
style=''font-size:12.0pt''>&nbsp;</span></font><font size=2 face=Arial><span
lang=FR style=''font-size:10.0pt;font-family:Arial''><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><a href="mailto:anne.proske@de.atu.eu"
title="mailto:anne.proske@de.atu.eu"><span lang=FR><span
title="mailto:anne.proske@de.atu.eu">manuel.prem@de.atu.eu</span></span></a></span></font><o:p></o:p></p>

<p class=MsoNormal><u><font size=2 color=blue face=Arial><span lang=FR
style=''font-size:10.0pt;font-family:Arial;color:blue''><a
href="http://www.atu.eu" title="http://www.atu.eu/"><span lang=EN-US><span
title="http://www.atu.eu/"><span title="http://www.atu.eu/">www.atu.eu</span></span></span></a></span></font></u><o:p></o:p></p>

<p class=MsoNormal><font size=2 face=Arial><span lang=DE style=''font-size:10.0pt;
font-family:Arial''><a href="http://www.facebook.com/ATU"><span lang=EN-US>www.facebook.com/ATU</span></a></span></font><o:p></o:p></p>

<p class=MsoNormal><font size=3 face="Times New Roman"><span style=''font-size:
12.0pt''>&nbsp;<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>Sitz: Weiden i. d. OPf., Amtsgericht
Weiden i. d. OPf., HRA 2012<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=EN-GB
style=''font-size:8.0pt;font-family:Arial;color:black''>UST-ID Nr. DE814195392, WEEE-Nr.
</span></font><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>DE53789710<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>Pers�nlich haftende
Gesellschafterin:<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>AFM Autofahrerfachmarkt
Gesch�ftsf�hrungs GmbH<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>Sitz: Weiden i. d. OPf., Amtsgericht
Weiden i. d. OPf., HRB 2842<o:p></o:p></span></font></p>

<p class=MsoFooter style=''margin:0in;margin-bottom:.0001pt''><font size=1
color=black face=Arial><span lang=DE style=''font-size:8.0pt;font-family:Arial;
color:black''>Gesch�ftsf�hrer:J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<o:p></o:p></span></font></p>

<p class=MsoFooter style=''margin:0in;margin-bottom:.0001pt''><font size=1
color=black face=Arial><span lang=DE style=''font-size:8.0pt;font-family:Arial;
color:black''>Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert,<o:p></o:p></span></font></p>

<p class=MsoFooter style=''margin:0in;margin-bottom:.0001pt''><font size=1
color=black face=Arial><span lang=DE style=''font-size:8.0pt;font-family:Arial;
color:black''>Arnaud Vuye<o:p></o:p></span></font></p>

<p class=MsoFooter style=''margin:0in;margin-bottom:.0001pt''><font size=1
color=black face=Arial><span lang=DE style=''font-size:8.0pt;font-family:Arial;
color:black''>____________________________________________________________________</span></font><font
size=1><span lang=DE style=''font-size:8.0pt''><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=3 face="Times New Roman"><span lang=DE
style=''font-size:12.0pt''>&nbsp;</span></font><font size=2 color=black
face=Arial><span lang=DE style=''font-size:10.0pt;font-family:Arial;color:black''><o:p></o:p></span></font></p>

<p class=MsoNormal><font size=1 color=black face=Arial><span lang=DE
style=''font-size:8.0pt;font-family:Arial;color:black''>Die in dieser E-Mail enthaltenen
Nachrichten und Anh�nge sind ausschlie�lich f�r den bezeichneten Adressaten
bestimmt. Sie k�nnen rechtlich gesch�tzte, vertrauliche Informationen
enthalten. Falls Sie nicht der bezeichnete Empf�nger oder zum Empfang dieser
E-Mail nicht berechtigt sind, ist die Verwendung, Vervielf�ltigung oder
Weitergabe der Nachrichten und Anh�nge untersagt; bitte informieren Sie in
diesem Fall unverz�glich den Absender und vernichten Sie diese E-Mail. Anlagen
dieser E-Mail k�nnen Software-Viren enthalten, die Ihr Computersystem gef�hrden
oder besch�digen k�nnten. A.T.U Auto-Teile-Unger hat jede m�gliche Vorkehrung
getroffen, um dieses Risiko zu minimieren, kann jedoch keine Haftung f�r
Sch�den �bernehmen, die durch einen Software-Virus verursacht wurden. Wir d�rfen
Sie bitten, eigene Sicherheitssysteme zu aktivieren, bevor Sie Anlagen �ffnen.</span></font><span
lang=DE><o:p></o:p></span></p>

<p class=MsoNormal><font size=3 face="Times New Roman"><span lang=DE
style=''font-size:12.0pt''><o:p>&nbsp;</o:p></span></font></p>

</div>

</body>

</html>'
WHERE MVT_BEREICH = 'MOB_AVD_MAIL';
commit;