<?php

//Blanko -> Formular VisitCheck

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('MQC','%');
$TextKonserven[]=array('FLB','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 10;

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');


if(isset($_GET['PEZ_XXX_KEY']) && $_GET['PEZ_XXX_KEY'] == '0')
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 20);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Meisterwerkstatt Qualit�ts-Check','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(60,5,'Erstellt: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Herausgebener: Organisation','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 17.09.2010','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.1','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.2','LBTR',0,'C',0);
$Zeile+=13;
$Spalte-=30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['MQC']['MQC_PRUEFER'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_FILNR'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(30,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$AWISSprachKonserven['MQC']['MQC_FILIALE'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(42,4,'','B',0,'C',0);
//$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CV']['CV_Datum'].':','',0,'C',0);
//$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_AUFTRAGSNUMMER'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(60,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_DATUM'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(60,4,'','B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
//$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_MECHANIKER'].':','',0,'L',0);
//$Ausdruck->_pdf->cell2(60,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_WERKSTATTLEITER'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(60,4,'','B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(60,4,$AWISSprachKonserven['MQC']['MQC_DIENSTLEISTUNG'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(75,4,'','B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'B',10);
$Ausdruck->_pdf->cell2(150,4,$AWISSprachKonserven['FLB']['HinweisQSCheck'],'',0,'L',0);
//$Ausdruck->_pdf->cell2(75,4,'','B',0,'L',0);

$Zeile+=8;


$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(100,5,'A. '.$AWISSprachKonserven['MQC']['MQC_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_JA'],'LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_NEIN'],'LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'1. '.$AWISSprachKonserven['MQC']['MQC_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 90, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 90, 3, 3),'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'2. '.$AWISSprachKonserven['MQC']['MQC_A2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 95, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 95, 3, 3),'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'3. '.$AWISSprachKonserven['MQC']['MQC_A3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 100, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 100, 3, 3),'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'4. '.$AWISSprachKonserven['MQC']['MQC_A4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 105, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 105, 3, 3),'LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(100,5,'B. '.$AWISSprachKonserven['MQC']['MQC_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_JA'],'LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_NEIN'],'LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'1. '.$AWISSprachKonserven['MQC']['MQC_B1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 115, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 115, 3, 3),'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'2. '.$AWISSprachKonserven['MQC']['MQC_B2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 120, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 120, 3, 3),'LTRB',0,'C',0);

$Zeile+=15;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(10,5,'POS','LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(75,5,'Bezeichnung','LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'OK','LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(85,5,'Bemerkung','LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'1.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 140, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'2.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 145, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'3.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 150, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'4.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 155, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'5.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 160, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'6.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 165, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'7.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 170, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'8.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 175, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'9.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C9'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 180, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'10.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C10'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 185, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'11.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C11'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 190, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'12.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C12'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 195, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'13.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C13'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 200, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'14.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C14'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 205, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'15.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C15'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 210, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'16.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C16'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 215, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'17.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C17'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 220, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);
/*
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'18.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C18'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);
*/
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'18.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C19'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'19.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C20'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 230, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'20.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C21'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 235, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'21.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C22'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 240, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'22.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C23'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 245, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'23.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C24'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 250, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'24.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C25'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 255, 3, 3),'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(85,5,'','LTRB',0,'C',0);

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(75,5,'Weitere Kommentare:','',0,'L',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(180,5,'','LBTR',0,'L',0);


}elseif(isset($_GET['PEZ_XXX_KEY']) != '0')
{


/*
$SQL = 	'Select PEZ_KEY,PEZ_XXX_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= ',Xbn_Vollername, FIL_BEZ ';
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = PEZ_XXX_KEY ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= ' Inner Join Benutzer On Xbn_Key=Pei_Xbn_Key';
$SQL .= ' inner join FILIALEN ON PEI_FIL_ID = FIL_ID';
$SQL .= ' WHERE PEZ_XXX_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEZ_XXX_KEY']);
*/

$SQL = 'Select * From FILIALBERICHTE';
$SQL .= ' Inner Join Personaleinsberichtszuord  On Flb_Key = Pez_Xxx_Key'; 
$SQL .= ' inner join FILIALEN ON FLB_FIL_ID = FIL_ID WHERE PEZ_XXX_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEZ_XXX_KEY']);

$rsBericht = $DB->RecordSetOeffnen($SQL);	
	
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 20);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Meisterwerkstatt Qualit�ts-Check','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(60,5,'Erstellt: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Herausgebener: Organisation','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 17.09.2010','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.1','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.2','LBTR',0,'C',0);
$Zeile+=13;
$Spalte-=30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['MQC']['MQC_PRUEFER'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('FLB_PRUEFER'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_FILNR'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(30,4,$rsBericht->FeldInhalt('FLB_FIL_ID'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$AWISSprachKonserven['MQC']['MQC_FILIALE'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(42,4,$rsBericht->FeldInhalt('FIL_BEZ'),'B',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_AUFTRAGSNUMMER'].':','',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=53';

$rsAuftragsnr = $DB->RecordSetOeffnen($SQL);

$Ausdruck->_pdf->cell2(60,4,$rsAuftragsnr->FeldInhalt('PEZ_WERT'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_DATUM'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(60,4,substr($rsBericht->FeldInhalt('FLB_DATUM'),0,10),'B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
/*
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_MECHANIKER'].':','',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=54';

$rsMechaniker = $DB->RecordSetOeffnen($SQL);


$Ausdruck->_pdf->cell2(60,4,$rsMechaniker->FeldInhalt('PEZ_WERT'),'B',0,'L',0);
*/
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['MQC']['MQC_WERKSTATTLEITER'].':','',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=55';

$rsWerkstattleiter = $DB->RecordSetOeffnen($SQL);

$Ausdruck->_pdf->cell2(60,4,$rsWerkstattleiter->FeldInhalt('PEZ_WERT'),'B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(60,4,$AWISSprachKonserven['MQC']['MQC_DIENSTLEISTUNG'].':','',0,'L',0);
$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=56';

$rsDienstleistung = $DB->RecordSetOeffnen($SQL);

$Ausdruck->_pdf->cell2(75,4,$rsDienstleistung->FeldInhalt('PEZ_WERT'),'B',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'B',10);
$Ausdruck->_pdf->cell2(150,4,$AWISSprachKonserven['FLB']['HinweisQSCheck'],'',0,'L',0);

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(100,5,'A. '.$AWISSprachKonserven['MQC']['MQC_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_JA'],'LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_NEIN'],'LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'1. '.$AWISSprachKonserven['MQC']['MQC_A1'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=21';

//echo $SQL;

$rsA1 = $DB->RecordSetOeffnen($SQL);

if($rsA1->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 90, 3, 3),'LTRB',0,'C',0);  	
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,90,127,93);
  $Ausdruck->_pdf->Line(127, 90, 124, 93);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 90, 3, 3),'LTRB',0,'C',0); 
}

if($rsA1->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 90, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,90,147,93);
  $Ausdruck->_pdf->Line(147, 90, 144, 93);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 90, 3, 3),'LTRB',0,'C',0);
}


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'2. '.$AWISSprachKonserven['MQC']['MQC_A2'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=22';

//echo $SQL;

$rsA2 = $DB->RecordSetOeffnen($SQL);

if($rsA2->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 95, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,95,127,98);
  $Ausdruck->_pdf->Line(127, 95, 124, 98);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 95, 3, 3),'LTRB',0,'C',0);
}

if($rsA2->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 95, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,95,147,98);
  $Ausdruck->_pdf->Line(147, 95, 144, 98);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 95, 3, 3),'LTRB',0,'C',0);
}

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'3. '.$AWISSprachKonserven['MQC']['MQC_A3'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=23';

//echo $SQL;

$rsA3 = $DB->RecordSetOeffnen($SQL);

if($rsA3->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 100, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,100,127,103);
  $Ausdruck->_pdf->Line(127, 100, 124, 103);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 100, 3, 3),'LTRB',0,'C',0);
}

if($rsA3->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 100, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,100,147,103);
  $Ausdruck->_pdf->Line(147, 100, 144, 103);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 100, 3, 3),'LTRB',0,'C',0);
}

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'4. '.$AWISSprachKonserven['MQC']['MQC_A4'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=24';

//echo $SQL;

$rsA4 = $DB->RecordSetOeffnen($SQL);

if($rsA4->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 105, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,105,127,108);
  $Ausdruck->_pdf->Line(127, 105, 124, 108);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 105, 3, 3),'LTRB',0,'C',0);
}

if($rsA4->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 105, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,105,147,108);
  $Ausdruck->_pdf->Line(147, 105, 144, 108);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 105, 3, 3),'LTRB',0,'C',0);
}

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(100,5,'B. '.$AWISSprachKonserven['MQC']['MQC_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_JA'],'LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['MQC']['MQC_NEIN'],'LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'1. '.$AWISSprachKonserven['MQC']['MQC_B1'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=26';

$rsB1 = $DB->RecordSetOeffnen($SQL);

if($rsB1->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 115, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,115,127,118);
  $Ausdruck->_pdf->Line(127, 115, 124, 118);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 115, 3, 3),'LTRB',0,'C',0);
}

if($rsB1->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 115, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,115,147,118);
  $Ausdruck->_pdf->Line(147, 115, 144, 118);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 115, 3, 3),'LTRB',0,'C',0);
}


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(100,5,'2. '.$AWISSprachKonserven['MQC']['MQC_B2'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=27';

$rsB2 = $DB->RecordSetOeffnen($SQL);

if($rsB2->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 120, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(124,120,127,123);
  $Ausdruck->_pdf->Line(127, 120, 124, 123);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(124, 120, 3, 3),'LTRB',0,'C',0);
}

if($rsB2->FeldInhalt('PEZ_WERT') == '0')
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 120, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(144,120,147,123);
  $Ausdruck->_pdf->Line(147, 120, 144, 123);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(144, 120, 3, 3),'LTRB',0,'C',0);
}

$Zeile+=15;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(185,185,185);
$Ausdruck->_pdf->cell2(10,5,'POS','LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(75,5,'Bezeichnung','LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'OK','LTRB',0,'C',1);
$Ausdruck->_pdf->cell2(85,5,'Bemerkung','LTRB',0,'C',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'1.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C1'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=29';

$rsC1 = $DB->RecordSetOeffnen($SQL);

if($rsC1->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 140, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,140,106.5,143);
  $Ausdruck->_pdf->Line(106.5, 140, 103.5, 143);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 140, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'2.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C2'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=30';

$rsC2 = $DB->RecordSetOeffnen($SQL);

if($rsC2->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 145, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,145,106.5,148);
  $Ausdruck->_pdf->Line(106.5, 145, 103.5, 148);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 145, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'3.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C3'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=31';

$rsC3 = $DB->RecordSetOeffnen($SQL);

if($rsC3->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 150, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,150,106.5,153);
  $Ausdruck->_pdf->Line(106.5, 150, 103.5, 153);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 150, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC3->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC3->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'4.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C4'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=32';

$rsC4 = $DB->RecordSetOeffnen($SQL);

if($rsC4->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 155, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,155,106.5,158);
  $Ausdruck->_pdf->Line(106.5, 155, 103.5, 158);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 155, 3, 3),'LTRB',0,'C',0);
	
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC4->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC4->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'5.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C5'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=33';

$rsC5 = $DB->RecordSetOeffnen($SQL);

if($rsC5->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 160, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,160,106.5,163);
  $Ausdruck->_pdf->Line(106.5, 160, 103.5, 163);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 160, 3, 3),'LTRB',0,'C',0);
	
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC5->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC5->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'6.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C6'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=34';

$rsC6 = $DB->RecordSetOeffnen($SQL);

if($rsC6->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 165, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,165,106.5,168);
  $Ausdruck->_pdf->Line(106.5, 165, 103.5, 168);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 165, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC6->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC6->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'7.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C7'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=35';

$rsC7 = $DB->RecordSetOeffnen($SQL);

if($rsC7->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 170, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,170,106.5,173);
  $Ausdruck->_pdf->Line(106.5, 170, 103.5, 173);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 170, 3, 3),'LTRB',0,'C',0);
}

$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC7->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC7->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'8.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C8'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=36';

$rsC8 = $DB->RecordSetOeffnen($SQL);

if($rsC8->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 175, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,175,106.5,178);
  $Ausdruck->_pdf->Line(106.5, 175, 103.5, 178);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 175, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC8->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC8->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'9.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C9'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=37';

$rsC9 = $DB->RecordSetOeffnen($SQL);

if($rsC9->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 180, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,180,106.5,183);
  $Ausdruck->_pdf->Line(106.5, 180, 103.5, 183);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 180, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC9->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC9->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'10.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C10'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=38';

$rsC10 = $DB->RecordSetOeffnen($SQL);

if($rsC10->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 185, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,185,106.5,188);
  $Ausdruck->_pdf->Line(106.5, 185, 103.5, 188);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 185, 3, 3),'LTRB',0,'C',0);
}

$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC10->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC10->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'11.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C11'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=39';

$rsC11 = $DB->RecordSetOeffnen($SQL);

if($rsC11->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 190, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,190,106.5,193);
  $Ausdruck->_pdf->Line(106.5, 190, 103.5, 193);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 190, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC11->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC11->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'12.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C12'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=40';

$rsC12 = $DB->RecordSetOeffnen($SQL);

if($rsC12->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 195, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,195,106.5,198);
  $Ausdruck->_pdf->Line(106.5, 195, 103.5, 198);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 195, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC12->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC12->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'13.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C13'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=41';

$rsC13 = $DB->RecordSetOeffnen($SQL);

if($rsC13->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 200, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,200,106.5,203);
  $Ausdruck->_pdf->Line(106.5, 200, 103.5, 203);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 200, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC13->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC13->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'14.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C14'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=42';

$rsC14 = $DB->RecordSetOeffnen($SQL);

if($rsC14->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 205, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,205,106.5,208);
  $Ausdruck->_pdf->Line(106.5, 205, 103.5, 208);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 205, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC14->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC14->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'15.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C15'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=43';

$rsC15 = $DB->RecordSetOeffnen($SQL);

if($rsC15->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 210, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,210,106.5,213);
  $Ausdruck->_pdf->Line(106.5, 210, 103.5, 213);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 210, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC15->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC15->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'16.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C16'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=44';

$rsC16 = $DB->RecordSetOeffnen($SQL);

if($rsC16->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 215, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,215,106.5,218);
  $Ausdruck->_pdf->Line(106.5, 215, 103.5, 218);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 215, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC16->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC16->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'17.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C17'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=45';

$rsC17 = $DB->RecordSetOeffnen($SQL);

if($rsC17->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 220, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,220,106.5,223);
  $Ausdruck->_pdf->Line(106.5, 220, 103.5, 223);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 220, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC17->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC17->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);
/*
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'18.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C18'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=46';

$rsC18 = $DB->RecordSetOeffnen($SQL);

if($rsC18->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,225,106.5,228);
  $Ausdruck->_pdf->Line(106.5, 225, 103.5, 228);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC18->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC18->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);
*/
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'18.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C19'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=47';

$rsC19 = $DB->RecordSetOeffnen($SQL);

if($rsC19->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,225,106.5,228);
  $Ausdruck->_pdf->Line(106.5, 225, 103.5, 228);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 225, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC19->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC19->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'19.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C20'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=48';

$rsC20 = $DB->RecordSetOeffnen($SQL);

if($rsC20->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 230, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,230,106.5,233);
  $Ausdruck->_pdf->Line(106.5, 230, 103.5, 233);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 230, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC20->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC20->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'20.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C21'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=49';

$rsC21 = $DB->RecordSetOeffnen($SQL);

if($rsC21->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 235, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,235,106.5,238);
  $Ausdruck->_pdf->Line(106.5, 235, 103.5, 238);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 235, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC21->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC21->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'21.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C22'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=50';

$rsC22 = $DB->RecordSetOeffnen($SQL);

if($rsC22->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 240, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,240,106.5,243);
  $Ausdruck->_pdf->Line(106.5, 240, 103.5, 243);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 240, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC22->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC22->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'22.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C23'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=51';

$rsC23 = $DB->RecordSetOeffnen($SQL);

if($rsC23->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 245, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,245,106.5,248);
  $Ausdruck->_pdf->Line(106.5, 245, 103.5, 248);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 245, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC23->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC23->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'23.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C24'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=52';

$rsC24 = $DB->RecordSetOeffnen($SQL);

if($rsC24->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 250, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,250,106.5,253);
  $Ausdruck->_pdf->Line(106.5, 250, 103.5, 253);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 250, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC24->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC24->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,5,'24.','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(75,5,$AWISSprachKonserven['MQC']['MQC_C25'],'LTRB',0,'L',0);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=59';

$rsC24 = $DB->RecordSetOeffnen($SQL);

if($rsC24->FeldInhalt('PEZ_WERT') == '1')
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 255, 3, 3),'LTRB',0,'C',0);
  $Ausdruck->_pdf->SetLineWidth(1); 
  $Ausdruck->_pdf->Line(103.5,255,106.5,258);
  $Ausdruck->_pdf->Line(106.5, 255, 103.5, 258);
  $Ausdruck->_pdf->SetLineWidth(0); 
}
else 
{
  $Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(103.5, 255, 3, 3),'LTRB',0,'C',0);
}
$Ausdruck->_pdf->SetFont($Schriftart,'',6);
$Text = substr($rsC24->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC24->FeldInhalt('PEZ_BEMERKUNG'),70,70);
$Ausdruck->_pdf->cell2(85,5,$Text,'LTRB','L',1);

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(75,5,'Weitere Kommentare:','',0,'L',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_XXX_KEY='.$_GET['PEZ_XXX_KEY'];
$SQL .= ' AND PEZ_PBF_ID=58';

$rsKommentar = $DB->RecordSetOeffnen($SQL);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(180,5,$rsKommentar->FeldInhalt('PEZ_WERT'),'LBTR',0,'L',0);

$Zeile+=6;
$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_XXX_KEY=".$_GET['PEZ_XXX_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');

echo "Gesamtanzahl:".$Gesamtanzahl;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_XXX_KEY=".$_GET['PEZ_XXX_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);

$Falsch = $GesamtFalsch->FeldInhalt('ANZ');

echo "Falsch".$Falsch;

$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 

if($Prozentsatz >= 90 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GR�N)','',0,'R',0);
	
}
elseif($Prozentsatz >= 80 && $Prozentsatz <= 90)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GELB)','',0,'R',0);
	
}
elseif($Prozentsatz < 80)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=13;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(180,5,'(ROT)','',0,'C',0);
	//$Ausdruck->_pdf->Image($file, $x, $y,$w,$h);
}
$Zeile+=4;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(180,5,'Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	
}

$Ausdruck->Anzeigen();

?>