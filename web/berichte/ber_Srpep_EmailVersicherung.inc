<?php
require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
global $AWISBenutzer;
class ber_Srpep_EmailVersicherung extends awisBerichte
{
	private $pdf_vorlage = '';
	private $Zeilenmerker = '';
	private $Spaltenmerker = '';
	private $letzte_positon = '';
	private $Summe = 0.00;
	
	/**
	 * Klasse initialisieren
	 * @param unknown $AWISBenutzer
	 * @param unknown $DB
	 * @param unknown $ReportID
	 */
	public function __construct($AWISBenutzer, $DB, $ReportID)
	{
	    // Es darf kein ob_clean() ausgeführt werden, weil innerhalb der Seite aufgerufen wird 
	    parent::__construct($AWISBenutzer, $DB, $ReportID,false);
	}
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
	    $TextKonserven = array();
		$TextKonserven[]=array('PKS','%');
		$TextKonserven[]=array('Wort','lbl_zurueck');
		$TextKonserven[]=array('Wort','lbl_speichern');
		$TextKonserven[]=array('Wort','lbl_hinzufuegen');
		$TextKonserven[]=array('Wort','txt_BitteWaehlen');
		$TextKonserven[]=array('Wort','lbl_trefferliste');
		$TextKonserven[]=array('Fehler','err_keineDaten');
		$TextKonserven[] = array('Ausdruck', 'txt_HinweisFormularIntern');
		$TextKonserven[] = array('Wort', 'Seite');
		
		
		$this->_Dateiname = $Parameter['DATEINAME'];
		
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_SRP'));

		
		// Daten für den Bericht laden
       	$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
       	
       	$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
       	//echo $Parameter['PKS_KEY'];
       	$SQL  = 'Select pks.*, a.bez as Hersteller, b.bez as Modell, c.bez as Typ from Parkschaeden pks '; 
       	$SQL .= ' left join kfz_herst a on PKS_KHERNR = KHERNR ';
       	$SQL .= ' left join kfz_modell b on PKS_KMODNR = KMODNR ';
       	$SQL .= ' left join kfz_typ c on PKS_KTYPNR = KTYPNR ';
       	$SQL .= ' where PKS_KEY = ' . $this->_DB ->WertSetzen('PKM', 'N0', $Parameter['PKS_KEY']);
       	$this->_rsDaten = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('PKM',true));
       	//echo $this->_DB->LetzterSQL();
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
	  
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		//$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		
		$this->AliasNbPages();
		$this->SetAutoPageBreak(true,30);
		
		// Startpositionen setzen
		
		//$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->NeueSeite();
        		
		//Überschrift Versicherungsnehmer
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']+3);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_PDF_VERSICHERUNGSNEHMER']));
		
		//Versicherungsscheinnummer
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_VERSSCHEINNR'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_VERSSCHEINNR'), 0, 0, 'L', 0);
		
		
		//Vorname
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_VORNAME'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_VORNAME'), 0, 0, 'L', 0);
		
		
		//Nachname
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_NACHNAME'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_NACHNAME'), 0, 0, 'L', 0);
		
		//ZwischenÜberschrift Anschrift
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['UEBERSCHRIFT_ANSCHRIFT']));
		
	
		//Straße
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_STRASSE'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_STRASSE'), 0, 0, 'L', 0);
		
		//PLZ
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_PLZ'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_PLZ'), 0, 0, 'L', 0);
		
		//ORT
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_ORT'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_ORT'), 0, 0, 'L', 0);
		
		//ZwischenÜberschrift Telefonnummer
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['UEBERSCHRIFT_TELEFON']));
		
		
		//Telefon
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_TELEFON'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_TELEFON'), 0, 0, 'L', 0);
		
		//MOBIL
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_MOBIL'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_MOBIL'), 0, 0, 'L', 0);
		
		//Zwischenlinie
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(self::Seitenbreite()-($this->_Parameter['LinkerRand']*2), $this->_Parameter['Zeilenhoehe'], '' , 'B', 0, 'L', 0);
		
		
		//Überschrift Schadenserfassung
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']+3);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['UEBERSCHRIFT_SCHADERF']));
		
		
		//Schadenart
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);		
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_SCHADENART'] . ':'));		
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$Daten = explode('|',$this->_AWISSprachkonserven['PKS']['PKS_LST_SCHADENART']);
		if($this->_rsDaten->FeldInhalt('PKS_SCHADENART') != '')
		{
			$Value = substr($Daten[$this->_rsDaten->FeldInhalt('PKS_SCHADENART')-1],2);
		}
		else 
		{
			$Value = '';
		}
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $Value, 0, 0, 'L', 0);
		
		//Schadensposition 
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_POS'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$Daten = explode('|',$this->_AWISSprachkonserven['PKS']['PKS_LST_POS']);
		
		if($this->_rsDaten->FeldInhalt('PKS_SCHADENPOS') != '')
		{
			$Value = substr($Daten[$this->_rsDaten->FeldInhalt('PKS_SCHADENPOS')-1],2);
		}
		else
		{
			$Value = '';
		}
		
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$Value, 0, 0, 'L', 0);
		
		//Schadengröße
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_GROESSE'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		
		$Daten = explode('|',$this->_AWISSprachkonserven['PKS']['PKS_LST_GROESSE']);
		
		if($this->_rsDaten->FeldInhalt('PKS_GROESSE') != '')
		{
			$Value = substr($Daten[$this->_rsDaten->FeldInhalt('PKS_GROESSE')-1],2);
		}
		else
		{
			$Value = '';
		}
		
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$Value, 0, 0, 'L', 0);
		
		//Lack
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_LACK'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$Daten = explode('|',$this->_AWISSprachkonserven['PKS']['PKS_LST_LACK']);
		
		if($this->_rsDaten->FeldInhalt('PKS_FARBE') != '')
		{
			$Value = substr($Daten[$this->_rsDaten->FeldInhalt('PKS_FARBE')-1],2);
		}
		else
		{
			$Value = '';
		}
		
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$Value, 0, 0, 'L', 0);
		
		
		//Notizfeld
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_NOTIZ'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		//$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_NOTIZ'), 0, 0, 'L', 0);
		$this->MultiCell(self::Seitenbreite()-90,$this->_Parameter['Zeilenhoehe'],$this->_rsDaten->FeldInhalt('PKS_NOTIZ'),0,'L',0);
		
		
		//Datum
		$this->_Zeile = $this->GetY();
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_ERFDATUM'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D',$this->_rsDaten->FeldInhalt('PKS_ERFDATUM')), 0, 0, 'L', 0);

		
		//ZwischenÜberschrift Fahrzeug
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['UEBERSCHRIFT_FAHRZEUG']));
		
		//Hersteller
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_HERSTELLER'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('HERSTELLER'), 0, 0, 'L', 0);
		
		
		//Modell
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
				
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_MODELL'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('MODELL'), 0, 0, 'L', 0);

		//Typ
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_TYP'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('TYP'), 0, 0, 'L', 0);
		
		//Kennzeichen
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_KFZKENN'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_KFZKENN'), 0, 0, 'L', 0);
		
		//Zwischenlinie
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(self::Seitenbreite()-($this->_Parameter['LinkerRand']*2), $this->_Parameter['Zeilenhoehe'], '' , 'B', 0, 'L', 0);
		
		//Überschrift Kundengespräch mit Leiterlack
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']+3);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['UEBERSCHRIFT_KDGESPRAECH']));
		
		
		//Kundenrückruf
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_DATUMRR'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D',$this->_rsDaten->FeldInhalt('PKS_DATUMRR')), 0, 0, 'L', 0);
		
		if($this->_rsDaten->FeldInhalt('PKS_STATUS') == 2)
		{
				//Grund Ablehnung
				$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
				$this->_Spalte = $this->_Parameter['StartPosWerte'];
				
				$this->SetXY($this->_Spalte, $this->_Zeile);
				$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
				$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_GRUNDABL'] . ':'));
				$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
				$this->MultiCell(self::Seitenbreite()-90,$this->_Parameter['Zeilenhoehe'],$this->_rsDaten->FeldInhalt('PKS_GRUNDABL'),0,'L',0);

		}
		elseif($this->_rsDaten->FeldInhalt('PKS_STATUS') == 3)
		{
			//Grund Ablehnung
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			$this->_Spalte = $this->_Parameter['StartPosWerte'];
				
			$this->SetXY($this->_Spalte, $this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
			$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['PKS']['PKS_GRUNDSTORNO'] . ':'));
			$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
			$this->MultiCell(self::Seitenbreite()-90,$this->_Parameter['Zeilenhoehe'],$this->_rsDaten->FeldInhalt('PKS_GRUNDSTORNO'),0,'L',0);
		}
		$this->_Zeile = $this->GetY();
		//$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		//$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDaten->FeldInhalt('PKS_GRUNDABL'), 0, 0, 'L', 0);
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_SRP'));
		$Recht34000 = $this->_AWISBenutzer->HatDasRecht(34000);
		
		
		$this->FertigStellen($Ausgabeart);   
	}
	
	
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
	    $this->AddPage();
	}
	
	/**
	 * Automatische Fußzeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		
		
	    //Oberer Rand
	    $this->HauptrahmenEndeY = $this->_Parameter['Seite1-Oben'];
	    $this->HauptrahmenEndeX = self::Seitenbreite() - $this->_Parameter['LinkerRand'];    
	    $this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenEndeY);
	    
		//Koordinaten holen
		$this->HauptrahmenAnfangY = $this->_Parameter['Seite1-Oben'];
		$this->HauptrahmenEndeY = $this->GetY();
		$this->HauptrahmenEndeX = self::Seitenbreite() - $this->_Parameter['LinkerRand'];
		
		//Linker Rand
		$this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->_Parameter['LinkerRand'],$this->HauptrahmenAnfangY);
				
		//Rechter Rand
		$this->Line($this->HauptrahmenEndeX,$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenAnfangY);
		
		//Unterer Rand
		$this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenEndeY);
	
		$this->SetFont('Arial','',6);
		
		//$this->SetXY(0,self::SeitenHoehe()-5);
		//$this->Cell(self::Seitenbreite(),$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Ausdruck']['txt_HinweisFormularIntern']),0,0,'C','');
		
		$this->SetXY(self::SeitenBreite()-50,self::SeitenHoehe()-5);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
		 
	
	
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
			$this->useTemplate($pdf_vorlage);
		}
		$this->_Zeile = $this->_Parameter['SeitenzaehlerOben'];
		$this->_Spalte = $this->_Parameter['SeitenzaehlerRechts'];
		$this->SetFont($this->_Parameter['Schriftart'], '', 8);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo().' von {nb}',0 ,0 ,'L');
		
		
		$this->_Zeile = $this->_Parameter['Seite1-Oben']+ $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		//$this->Cell(10, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo().' von {nb}',0 ,0 ,'L');
		//$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['UEBERSCHRIFT_QMM']));
	
		/*
		if($this->PageNo()!=1)
		{
			//$this->_Zeile = $this->_Zeile + $this->_Parameter['Zeilenhoehe'];
			$this->_Zeile = $this->GetY();
			$this->SetXY($this->_Spalte, $this->_Zeile);
		}
		*/
		
	}
	
	public function FertigStellen($Ausgabeart)
	{
	    //*********************************************************************
	    // Berichtsende -> Zurückliefern
	    //*********************************************************************

	    switch($Ausgabeart)
	    {
	        case 1:			// PDF im extra Fenster -> Download         
	            $this->Output($this->_BerichtsName.'.pdf','D');	            
	            break;
	        case 2:			// Standard-Stream
	            $this->Output($this->_BerichtsName.'.pdf','I');
	            break;
	        case 3:			// Als String zurückliefern
	            return($this->Output($this->_BerichtsName.'.pdf','S'));
	            break;
	        case 4:         // Als Datei speichern
	            $this->Output($this->_Dateiname,'F');
	            break;
	    }
	}
	
}

?>