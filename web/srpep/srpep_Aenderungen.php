<?php
/**
 * Ge�nderte Termine anzeigen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091008
 *
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFilialen.inc');
global $AWISCursorPosition;
global $SRFunktionen;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('WTE','%');
	$TextKonserven[]=array('WTA','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','HinweisZuruecksetzen');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht11004=$AWISBenutzer->HatDasRecht(11004);		// Rechte des Mitarbeiters
	if($Recht11004==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	if(isset($_POST['cmdSpeichern_x']))
	{
		include './srpep_speichern.php';
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Dienstleistungen>");
	$Form->Formular_Start();

	$SQL = 'SELECT DISTINCT WTE_KEY, WTE_STATUS, WTE_WANR, WTE_KUNDENFZVON, WTE_KUNDENFZBIS, WTE_FIL_ID';
	$SQL .= ', CASE WHEN WTE_Status = 8 THEN \'Storniert\' ELSE WTA_AENDERUNGEN END AS WTA_AENDERUNGEN, WTA_KUNDENFZVON';
	$SQL .= ' FROM WerkstattTermine';
	$SQL .= '  LEFT OUTER JOIN WERKSTATTTERMINEAENDERUNGEN ON WTE_KEY = WTA_WTE_KEY';
	$SQL .= '  INNER JOIN V_FILIALEBENENROLLEN ON WTE_FIL_ID = XX1_FIL_ID';
	$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
	$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND FRZ_GUELTIGAB <= trunc(SYSDATE) AND FRZ_GUELTIGBIS >= trunc(sysdate)';
	$SQL .= '  INNER JOIN FILIALEBENENZUORDNUNGEN on XX1_FEZ_KEY = FEZ_KEY AND FEZ_GUELTIGAB <= trunc(SYSDATE) AND FEZ_GUELTIGBIS  ' . $SRFunktionen->GueltigBis();
	$SQL .= ' WHERE WTE_KUNDENFZVON>=TRUNC(SYSDATE)';
	if(($Recht11004&4)==0)		// Darf jemand alle sehen oder nur seine?
	{
		$SQL .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();
	}
	$SQL .= ' AND (WTE_ExternerStatus = \'L\' OR WTA_KEY IS NOT NULL)';

	$SQL .= ' ORDER BY WTE_KUNDENFZVON, WTE_FIL_ID';

	$rsWTE = $DB->RecordSetOeffnen($SQL);
$Form->DebugAusgabe(1,$SQL);
	$EditModus = ($Recht11004&6);
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_KUNDENFZVON'],160);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_FIL_ID'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_WANR'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTA']['WTA_AENDERUNGEN'],350);
	$Form->ZeileEnde();

	$DS=0;
	$AWISCursorPosition = '';
	while(!$rsWTE->EOF())
	{
		//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Form->ZeileStart();
		$Termin = $Form->PruefeDatum($rsWTE->FeldInhalt('WTE_KUNDENFZVON'),false,false,true)-(60*60*24);

		$Link = (($rsWTE->FeldInhalt('WTE_STATUS')==8)?'':'./srpep_Main.php?cmdAktion=Planung&FIL_ID='.$rsWTE->FeldInhalt('WTE_FIL_ID').'&TAG='.$Form->Format('D',$rsWTE->FeldInhalt('WTE_KUNDENFZVON')));

		$Form->Erstelle_ListenFeld('WTE_KUNDENFZVON',$rsWTE->FeldInhalt('WTE_KUNDENFZVON'),8,160,false,($DS%2),'','','DU','L',$AWISSprachKonserven['WTA']['WTA_KUNDENFZVON'].': '.$Form->Format('DU',$rsWTE->FeldInhalt('WTA_KUNDENFZVON')));
		$Form->Erstelle_ListenFeld('WTE_FIL_ID',$rsWTE->FeldInhalt('WTE_FIL_ID'),0,100,false,($DS%2),'','','T');
		$Form->Erstelle_ListenFeld('WTE_WANR',$rsWTE->FeldInhalt('WTE_WANR'),0,100,false,($DS%2),'',$Link,'T');
		$Form->Erstelle_ListenFeld('WTE_WANR',substr($rsWTE->FeldInhalt('WTA_AENDERUNGEN'),0,40).'...',0,350,false,($DS%2),'','','T','L',$rsWTE->FeldInhalt('WTA_AENDERUNGEN'));
		$Form->ZeileEnde();

		$rsWTE->DSWeiter();
		$DS++;
	}

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200912171119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200912171120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>