<?php
global $AWISCursorPosition;

try
{

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RFA','%');
	$TextKonserven[]=array('AST','AST_ATUNR');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','NurAktive');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Ja');
	$TextKonserven[]=array('Wort','Nein');
	$TextKonserven[]=array('Wort','BereitsBearbeitet');
	$TextKonserven[]=array('RFM','RFM_FIL_ID');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('RFS','lst_RFA_RFS_STATUS');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht3900=$AWISBenutzer->HatDasRecht(3900);
	if($Recht3900==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Recht3910=$AWISBenutzer->HatDasRecht(3910);
	if($Recht3910==0)
	{
		$Form->Fehler_KeineRechte();
	}

	// Parameter bei Suche zur�cksetzen
	$AWISBenutzer->ParameterSchreiben('RFA_POS_Blaettern',serialize(0));
	//$AWISBenutzer->ParameterSchreiben('RFASuche',serialize(''));	

	$Form->SchreibeHTMLCode('<form name="frmSuche" method="POST" action="./rueckfuehrung_Main.php?cmdAktion=Details">');

	/**********************************************
	* * Eingabemaske
	***********************************************/
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('RFASuche'));

	$Form->Formular_Start();


	$Filiale=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	
	$Form->ZeileStart();
	if($Filiale!='')
	{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFM']['RFM_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('*RFM_FIL_ID',$Filiale,20,200,false,'','','','T');
	}
	$Form->Erstelle_HiddenFeld('RFM_FIL_ID',$Filiale);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_NR'].':',190);
	$Form->Erstelle_TextFeld('*RFA_NR','',20,200,true,'','','','T');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_DATUM'].':',190);
	$Form->Erstelle_TextFeld('*RFA_DATUM','',20,200,true,'','','','D');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'].':',190);
	$Form->Erstelle_TextFeld('*AST_ATUNR','',20,200,true,'','','','T');
	$Form->ZeileEnde();
	
	if(($Recht3910&32)!=32)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',190);
		$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFA_RFS_STATUS']);
		$Form->Erstelle_SelectFeld('*RFS_STATUS','',100,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$StatusText,'');
		//$SQL = 'SELECT RFS_KEY, RFS_BEZEICHNUNG FROM RueckFuehrungsStatus WHERE RFS_STATUS<30 ORDER BY RFS_STATUS';
		//$Form->Erstelle_SelectFeld('*RFS_KEY','',100,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
		$Form->ZeileEnde();
		$Form->Erstelle_HiddenFeld('RFC_Wert','-1');
	}
	else{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['BereitsBearbeitet'].':',190);
		$Value=array('on~'.$AWISSprachKonserven['Wort']['Ja'],'off~'.$AWISSprachKonserven['Wort']['Nein']);
		$Form->Erstelle_SelectFeld('*RFC_Wert','',120,true,'','-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Value,'');
		$Form->ZeileEnde();
	}
	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');		
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht3900&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rueckfuehrungsaufforderung&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>