<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $Recht28000 = $AWISBenutzer->HatDasRecht(28000);
    if ($Recht28000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    $Form->DebugAusgabe(1, __FILE__ . ': AWIS_KEY1: ' . $AWIS_KEY1);

    //AWIS_KEY1
    if ($AWIS_KEY1 == -1)
    {//Neuer KopfDS

        $SQL = 'INSERT';
        $SQL .= ' INTO GUTSCHEINKUNDEEXTERN';
        $SQL .= '   (';
        $SQL .= '     GKU_ANREDE,';
        $SQL .= '     GKU_TITEL,';
        $SQL .= '     GKU_NAME1,';
        $SQL .= '     GKU_NAME2,';
        $SQL .= '     GKU_KONZERN,';
        $SQL .= '     GKU_STRASSE,';
        $SQL .= '     GKU_HN,';
        $SQL .= '     GKU_POSTFACH,';
        $SQL .= '     GKU_POSTFACHPLZ,';
        $SQL .= '     GKU_LAN_CODE,';
        $SQL .= '     GKU_PLZ,';
        $SQL .= '     GKU_ORT,';
        $SQL .= '     GKU_TELEFON,';
        $SQL .= '     GKU_MOBILTELEFON,';
        $SQL .= '     GKU_FAX,';
        $SQL .= '     GKU_EMAIL,';
        $SQL .= '     GKU_WEBSITE,';
        $SQL .= '     GKU_STEUERNUMMER,';
        $SQL .= '     GKU_USTID,';
        $SQL .= '     GKU_HANDELSREGISTER,';
        $SQL .= '     GKU_BEMERKUNG,';
        $SQL .= '     GKU_STATUS,';
        $SQL .= '     GKU_VERTRAGNR,';
        $SQL .= '     GKU_USER,';
        $SQL .= '     GKU_USERDAT';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '     ' . $DB->WertSetzen('GKU', 'N0', $_POST['txtGKU_ANREDE']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_TITEL']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_NAME1']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_NAME2']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_KONZERN']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STRASSE']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_HN']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_POSTFACH']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_POSTFACHPLZ']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_LAN_CODE']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_PLZ']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_ORT']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_TELEFON']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_MOBILTELEFON']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_FAX']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_EMAIL']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_WEBSITE']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STEUERNUMMER']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_USTID']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_HANDELSREGISTER']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_BEMERKUNG']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STATUS']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_VERTRAGNR']);
        $SQL .= '     ,' . $DB->WertSetzen('GKU', 'T', $AWISBenutzer->BenutzerName());;
        $SQL .= '    , sysdate';
        $SQL .= '   )';

        $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('GKU'));
        $Form->DebugAusgabe(1, $DB->LetzterSQL());
        $AWIS_KEY1 = $DB->RecordSetOeffnen('select seq_gku_key.currval from dual')->FeldInhalt(1);
        $_POST['txtGKU_KEY'] = $AWIS_KEY1;
    }
    elseif ($AWIS_KEY1 > 0 and $AWIS_KEY2 == '')
    {//Gešnderter KopfDS. KopfDS Kann nur gešndert wreden, wenn man nicht in der Positionsanlage/Bearbeitung war

        
        $Update = 'UPDATE GUTSCHEINKUNDEEXTERN';
        $Update .= ' SET ';
        $Update .= '  GKU_ANREDE    = ' . $DB->WertSetzen('GKU', 'N0', $_POST['txtGKU_ANREDE']);
        $Update .= ' , GKU_TITEL     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_TITEL']);
        $Update .= ' , GKU_NAME1       = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_NAME1']);
        $Update .= ' , GKU_NAME2    = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_NAME2']);
        $Update .= ' , GKU_KONZERN     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_KONZERN']);
        $Update .= ' , GKU_STRASSE        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STRASSE']);
        $Update .= ' , GKU_HN        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_HN']);
        $Update .= ' , GKU_POSTFACH = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_POSTFACH']);
        $Update .= ' , GKU_POSTFACHPLZ     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_POSTFACHPLZ']);
        $Update .= ' , GKU_LAN_CODE       = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_LAN_CODE']);
        $Update .= ' , GKU_PLZ    = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_PLZ']);
        $Update .= ' , GKU_ORT     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_ORT']);
        $Update .= ' , GKU_TELEFON        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_TELEFON']);
        $Update .= ' , GKU_MOBILTELEFON        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_MOBILTELEFON']);
        $Update .= ' , GKU_FAX = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_FAX']);
        $Update .= ' , GKU_EMAIL     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_EMAIL']);
        $Update .= ' , GKU_WEBSITE       = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_WEBSITE']);
        $Update .= ' , GKU_STEUERNUMMER    = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STEUERNUMMER']);
        $Update .= ' , GKU_USTID     = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_USTID']);
        $Update .= ' , GKU_HANDELSREGISTER        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_HANDELSREGISTER']);
        $Update .= ' , GKU_BEMERKUNG        = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_BEMERKUNG']);
        $Update .= ' , GKU_STATUS = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_STATUS']);
        $Update .= ' , GKU_VERTRAGNR = ' . $DB->WertSetzen('GKU', 'T', $_POST['txtGKU_VERTRAGNR']);
        $Update .= ' , GKU_USER       = ' . $DB->WertSetzen('GKU', 'T', $AWISBenutzer->BenutzerName()) . '';
        $Update .= ' , GKU_USERDAT    = sysdate';
        $Update .= ' WHERE GKU_KEY      = ' . $DB->WertSetzen('GKU', 'N0', $AWIS_KEY1);

        $DB->Ausfuehren($Update, '', '', $DB->Bindevariablen('GKU'));

    }

    $Form->DebugAusgabe(1, $DB->LetzterSQL());
}
catch (awisException $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
    $Form->DebugAusgabe(1, $DB->LetzterSQL());
} catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $DB->LetzterSQL());
}

?>