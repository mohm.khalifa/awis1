<?php
//**************************************************************
// Liefert die Verkaufshäuser für die Zukauflieferanten
// je nach ausgewähltem Lieferanten
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['LIE_NR']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abhängigkeit der Rechte anzeigen
$SQL = 'SELECT DISTINCT LVK_KEY AS KEY, LVK_ORT || \', \' || LVK_STRASSE AS ANZEIGE ';
$SQL .= ' FROM LIEFERANTENVERKAUFSHAEUSER';
$SQL .= ' WHERE LVK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_GET['LIE_NR']);
$SQL .= ' AND ((LVK_DATUMVOM <= SYSDATE AND LVK_DATUMBIS >= SYSDATE)';
$SQL .= ' OR LVK_KEY = '.$DB->FeldInhaltFormat('N0',$_GET['WERT'],false).')';
$SQL .= ' ORDER BY 2';

$rsLVK = $DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsLVK->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsLVK->FeldInhalt('KEY').'">'.$rsLVK->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsLVK->FeldInhalt('KEY').'#+#'.$rsLVK->FeldInhalt('ANZEIGE').'';
	}

	$rsLVK->DSWeiter();
}
?>