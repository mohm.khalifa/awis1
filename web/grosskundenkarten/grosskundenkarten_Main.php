<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    require_once '../grosskundenkarten/grosskundenkarten_funktionen.php';

    global $AWISCursorPosition;        // Aus AWISFormular

    try {
        $Funktionen = new grosskundenkarten_funktionen();
        $Version = 3;
        echo "<link rel=stylesheet type=text/css href=" . $Funktionen->AWISBenutzer->CSSDatei($Version) . ">";

    } catch (Exception $ex) {
        die($ex->getMessage());
    }

    if (isset($_POST['cmdPDF_x'])) {
        $_GET['XRE'] = 78;
        $_GET['ID'] = 'bla';

        require_once '/daten/web/berichte/drucken.php';
    }

    echo '<title>' . $Funktionen->AWISSprachkonserven['TITEL']['tit_Grosskundenkarten'] . '</title>';
    ?>
</head>
<body>

<?php
include("awisHeader$Version.inc");    // Kopfzeile
echo '<script type="application/javascript" src="grosskundenkarten.js"></script>';
try {

    if ($Funktionen->Recht == 0) {
        $Funktionen->Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200812180900");
        die();
    }

    $Register = new awisRegister(59000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $Funktionen->Form->SetzeCursor($AWISCursorPosition);
} catch (Exception $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180830");
    } else {
        $Funktionen->Form->SchreibeHTMLCode('AWIS: ' . $ex->getMessage());
    }
}
?>
</body>
</html>