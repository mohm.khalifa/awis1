<?php
global $AWISCursorPosition;        // Zum Cursor setzen
require_once('awisDatenbank.inc');
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('KundenKfzSuche', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AktuellesSortiment');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'KeineZuordnungGefunden');
    $TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Werkzeuge = new awisWerkzeuge();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht13001 = $AWISBenutzer->HatDasRecht(13001);
    if ($Recht13001 == 0) {
        awisEreignis(3, 1000, 'KundenKfzSuche', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $RemoteDB = '';
    $RemoteConn = '';

    if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) {
        $RemoteDB = '@COM_DE.ATU.DE';
        $RemoteConn = 'COM_DE';
    } else {
        $RemoteDB='@COM_TEST_DE.ATU.DE';
        $RemoteConn='COM_TEST_DE';
    }

    $DBCom = awisDatenbank::NeueVerbindung($RemoteConn);
    $DBCom->Oeffnen();

    $BindeVariablen = array();

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $DetailAnsicht = false;
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KundenKfzSuche'));

    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['KFZ'] = $_POST['sucKFZ'];
        if (isset($Param['KFZ']) && $Param['KFZ'] != '') {
            $BindeVariablen['var_T_kfz'] = $DB->FeldInhaltFormat('T', $Param['KFZ']);
        }

        $Param['VERTRAGSNUMMER'] = $_POST['sucVERTRAGSNUMMER'];
        if (isset($Param['VERTRAGSNUMMER']) && $Param['VERTRAGSNUMMER'] != '') {
            $BindeVariablen['var_N0_vertragsnummer'] = $DB->FeldInhaltFormat('Z', $Param['VERTRAGSNUMMER']);
        }

        $Param['LEASINGNUMMER'] = $_POST['sucLEASINGNUMMER'];

        $Param['FAHRER_ID'] = $_POST['sucFAHRER_ID'];

        $Param['IMPDAT'] = $_POST['sucIMPDAT'];
        if (isset($Param['IMPDAT']) && $Param['IMPDAT'] != '') {
            $BindeVariablen['var_impdat'] = $Form->Format('D', $Param['IMPDAT']);
        }

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern']) ? 'on' : '';

        $Param['BindeVariablen'] = serialize($BindeVariablen);
        $AWISBenutzer->ParameterSchreiben("Formular_KundenKfzSuche", serialize($Param));
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {

        $BindeVariablen = unserialize($Param['BindeVariablen']);
        if (!isset($Param['KEY'])) {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
            $AWISBenutzer->ParameterSchreiben('Formular_KundenKfzSuche', serialize($Param));
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['KEY'] = 0;

            if (isset($Param['VERTRAGSNUMMER']) && $Param['VERTRAGSNUMMER'] != '') {
                $BindeVariablen['var_N0_vertragsnummer'] = $DB->FeldInhaltFormat('Z', $Param['VERTRAGSNUMMER']);
            }

            if (isset($Param['IMPDAT']) && $Param['IMPDAT'] != '') {
                $BindeVariablen['var_impdat'] = $Form->Format('D', $Param['IMPDAT']);
            }
        }

    }
    $Form->DebugAusgabe(1, $BindeVariablen);
    //*********************************************************
    //* Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        if ($Param['ORDER'] != '') {
            $ORDERBY = 'ORDER BY ' . $Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY KFZ';
            $Param['ORDER'] = 'KFZ';
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);


    $SQL = 'SELECT k.* ';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_AKTUELL k';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $Form->DebugAusgabe(1, $SQL);

    // Zum Bl�ttern in den Daten
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK'] = $Block;
        $AWISBenutzer->ParameterSchreiben('Formular_KundenKfzSuche', serialize($Param));
    } elseif (isset($Param['BLOCK'])) {
        $Block = $Param['BLOCK'];
    }

    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;

    $MaxDS = $DBCom->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);


    $SQL .= $ORDERBY;

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $rsKFZSuche = $DBCom->RecordsetOeffnen($SQL, $BindeVariablen);

    $AWISBenutzer->ParameterSchreiben('Formular_KundenKfzSuche', serialize($Param));

    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmKundenKfzSuche action=./kundenkfzsuche_Main.php?cmdAktion=Details method=POST>';

    if ($rsKFZSuche->EOF())        // Keine Meldung bei neuen Datens�tzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsKFZSuche->AnzahlDatensaetze() >= 1)                        // Liste anzeigen
    {
        $DetailAnsicht = false;
        $Form->Formular_Start();

        $Form->ZeileStart('font-size:11pt');

        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=VERTRAGSNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VERTRAGSNUMMER')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['VERTRAGSNUMMER'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=KFZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KFZ')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['KFZ'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=LEASINGNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LEASINGNUMMER')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['LEASINGNUMMER'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=LEASING_ENDE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LEASING_ENDE')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['LEASING_ENDE'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=IMPDAT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'IMPDAT')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['IMPDAT'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=TRANSAKTIONSID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'TRANSAKTIONSID')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['TRANSAKTIONSID'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=KUK_IMD_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KUK_IMD_ID')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['KUK_IMD_ID'], 150, '', $Link);
        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=ZUSATZ_1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ZUSATZ_1')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['FAHRER_ID'], 150, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsKFZSuche->EOF()) {
            //$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
            $Form->ZeileStart('font-size:11pt');

            $Form->Erstelle_ListenFeld('VERTRAGSNUMMER', $rsKFZSuche->FeldInhalt('VERTRAGSNUMMER'), 12, 150, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('KFZ', $rsKFZSuche->FeldInhalt('KFZ'), 12, 150, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('LEASINGNUMMER', $rsKFZSuche->FeldInhalt('LEASINGNUMMER'), 12, 150, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('LEASING_ENDE', $rsKFZSuche->FeldInhalt('LEASING_ENDE'), 12, 150, false, ($DS % 2), '', '', 'DT');
            $Form->Erstelle_ListenFeld('IMPDAT', $rsKFZSuche->FeldInhalt('IMPDAT'), 12, 150, false, ($DS % 2), '', '', 'DT');
            $Form->Erstelle_ListenFeld('TRANSAKTIONSID', $rsKFZSuche->FeldInhalt('TRANSAKTIONSID'), 12, 150, false, ($DS % 2), '', '', 'Z');
            $Form->Erstelle_ListenFeld('KUK_IMD_ID', $rsKFZSuche->FeldInhalt('KUK_IMD_ID'), 12, 150, false, ($DS % 2), '', '', 'Z');
            $Form->Erstelle_ListenFeld('ZUSATZ_1', $rsKFZSuche->FeldInhalt('ZUSATZ_1') == (null || '' || "") ? $AWISSprachKonserven['KundenKfzSuche']['KEINE_FAHRER_ID']
                : $rsKFZSuche->FeldInhalt('ZUSATZ_1'), 12, 150, false, ($DS % 2), '', '', 'T');

            $Form->ZeileEnde();

            $rsKFZSuche->DSWeiter();
            $DS++;
        }

        $Link = './kundenkfzsuche_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');


    if ($AWISCursorPosition != '') {
        $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
        $Form->SchreibeHTMLCode("document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();");
        $Form->SchreibeHTMLCode('</Script>');
    }

    $DBCom->Schliessen();
    $DB->Schliessen();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201109071633");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201109071634");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
    global $DB;

    $Bedingung = '';

    if (isset($Param['KFZ']) AND $Param['KFZ'] != '') {
        $Bedingung .= " AND UPPER(KFZ) LIKE REGEXP_REPLACE(REGEXP_REPLACE(UPPER(:var_T_kfz),'[[:punct:]]','%'),'[[:space:]]','%') ";
    }

    if (isset($Param['VERTRAGSNUMMER']) AND $Param['VERTRAGSNUMMER'] != '') {
        $Bedingung .= ' AND REGEXP_LIKE(VERTRAGSNUMMER,:var_N0_vertragsnummer) ';
    }

    if (isset($Param['LEASINGNUMMER']) AND $Param['LEASINGNUMMER'] != '') {
        $Bedingung .= ' AND LEASINGNUMMER' . $DB->LikeOderIst($Param['LEASINGNUMMER']) . ' ';
    }

    if (isset($Param['FAHRER_ID']) AND $Param['FAHRER_ID'] != '') {
        $Bedingung .= ' AND ZUSATZ_1' . $DB->LikeOderIst($Param['FAHRER_ID']) . ' ';
    }

    if (isset($Param['IMPDAT']) AND $Param['IMPDAT'] != '') {
        $Bedingung .= " AND IMPDAT=to_date(:var_impdat,'DD.MM.YYYY') ";
    }

    $Param['WHERE'] = $Bedingung;

    return $Bedingung;
}

?>