<?php
// Variablen
global $HTTP_POST_VARS;
global $HTTP_GET_VARS;
global $awisDBFehler;

$RechteStufe = awisBenutzerRecht($con, 505);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Atunr ohne Oenr',$_SERVER['PHP_AUTH_USER'],'','','');
   die("Keine ausreichenden Rechte!");
}

//$SQL = "Select AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW, AST_KENNUNG, WUG_WGR_ID, WUG_BEZEICHNUNG ";
$SQL = "Select * ";
$SQL .= "From V_ARTNR_OHNE_OENR_WG03 ";
$SQL .= "Order by AST_ATUNR ";
$rsAOO = awisOpenRecordset($con, $SQL);
if($rsAOO==FALSE)
{
	awisErrorMailLink("artnr_ohne_oenr_Liste.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsAOOZeilen=$awisRSZeilen;

//****************************************************
// 
// Ausgabe der Daten
//
// 	-> txtFormat: liste -> HTML
//                csv   -> CSV-Datei
//****************************************************

if($HTTP_POST_VARS['txtFormat']=='liste' OR $HTTP_POST_VARS['txtFormat']=='')		// Liste auf dem Bildschirm
{
	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr>";
	print "<td id=FeldBez>ATU-Nr</td>";
	print "<td id=FeldBez>Bezeichnung WWS</td>";
	print "<td id=FeldBez>Kenn1</td>";
	print "<td id=FeldBez>Wug</td>";
	print "<td id=FeldBez>Sortiment</td>";
	print "</tr>";
	
	$RechteStufe = awisBenutzerRecht($con, 400);
	for($AOOZeile=0;$AOOZeile<$rsAOOZeilen;$AOOZeile++)
//	for($AOOZeile=0;$AOOZeile<10;$AOOZeile++)
	{
		echo '<tr>';
		if($RechteStufe==0)
		{
			echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsAOO['AST_ATUNR'][$AOOZeile] . '</a></td>';
		}
		else
		{
			echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../ATUArtikel/artikel_Main.php?Key=' . $rsAOO['AST_KEY'][$AOOZeile] . '&cmdAktion=ArtikelInfos>' . $rsAOO['AST_ATUNR'][$AOOZeile] . '</a></td>';
		}
		echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsAOO['AST_BEZEICHNUNGWW'][$AOOZeile] . '</td>';
		echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsAOO['AST_KENNUNG'][$AOOZeile] . '</td>';
		echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=right>' . $rsAOO['WUG_WGR_ID'][$AOOZeile] . '</td>';
		echo '<td ' . (($AOOEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsAOO['WUG_BEZEICHNUNG'][$AOOZeile] . '</td>';
		echo '</tr>';
	}
	
	print "</table>";
}
elseif($HTTP_POST_VARS['txtFormat']=='csv')		// Liste auf dem Bildschirm
{
		// Dateinamen generieren
	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	fputs($fd, "Artnr;Bez;Kenn1;Sortiment;SortBez\n");

	for($AOOZeile=0;$AOOZeile<$rsAOOZeilen;$AOOZeile++)
	{
		fputs($fd, '' . $rsAOO['AST_ATUNR'][$AOOZeile]);
		fputs($fd, ';' . $rsAOO['AST_BEZEICHNUNGWW'][$AOOZeile]);
		fputs($fd, ';' . $rsAOO['AST_KENNUNG'][$AOOZeile]);
		fputs($fd, ';' . $rsAOO['WUG_WGR_ID'][$AOOZeile]);
		fputs($fd, ';' . $rsAOO['WUG_BEZEICHNUNG'][$AOOZeile]);
		fputs($fd, "\n");
	}

		// Fertig, Datei schlie�en
	fclose($fd);
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';

}
unset($rsOEP);


?>

