<?php

/**
 * 
 * @author Henry Ott
 * @copyright ATU Auto Teile Unger
 * @version 20130715
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FAB', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_KEINE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht27000 = $AWISBenutzer->HatDasRecht(27000);
    if ($Recht27000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_GET);
    
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_FAB"));
	
    $Form->DebugAusgabe(1,$Param);
    
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['KEY'] = 0;
        $Param['ORDER'] = '';
        $Param['BLOCK'] = '';
        
        $Param['FAB_KEY'] = $_POST['txtFAB_KEY'];
        $Param['FAB_BEREICH'] = $_POST['txtFAB_BEREICH'];
        $Param['FAB_PLZ'] = $_POST['sucFAB_PLZ'];
        $Param['FAB_ORT'] = $_POST['sucFAB_ORT'];
        
        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');

        $AWISBenutzer->ParameterSchreiben("Formular_FAB", serialize($Param));
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include('./fachbetriebe_loeschen.php');
    }
    elseif (isset($_POST['cmdSpeichern_x']) or isset($_GET['ok']))
    {
        include('./fachbetriebe_speichern.php');
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
        $AWIS_KEY1 = -1;
    }
    elseif (isset($_GET['FAB_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['FAB_KEY']);
    }
    else   // Letzten Benutzer suchen
    {
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_FAB',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
    }

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' FAB_NAME1';
        	$Param['ORDER'] = $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER'] = $ORDERBY;
    }

    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = _BedingungErstellen($Param, $BindeVariablen);

	//********************************************************
	// Daten suchen
	//********************************************************
	
    $SQL  = ' SELECT fab.*';
    if ($AWIS_KEY1 <= 0)
    {
        $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    }
    $SQL .= ' FROM fachbetriebe fab';    
    
    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .= ' ORDER BY ' . $ORDERBY;
    //$Form->DebugAusgabe(1,$SQL);
    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK']=$Block;
    }
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $Form->DebugAusgabe(1,$SQL);
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if ($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    
    $rsFAB = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmFachbetriebe action=./fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
	if ($rsFAB->EOF() AND !isset($_POST['cmdDSNeu_x']) AND !isset($_GET['FAB_KEY']))  // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht27000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	    $Form->SchaltflaechenEnde();
    }
    elseif (($rsFAB->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();//('font-size:10pt');
    	
        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_NAME1')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_NAME1'], 280, '', $Link);

        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_LAN_CODE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_LAN_CODE')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_LAN_CODE'], 50, '', $Link);
        
        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_PLZ')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_PLZ'], 100, '', $Link);
        
        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_ORT')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_ORT'], 280, '', $Link);
        
        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_STRASSE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_STRASSE')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_STRASSE'], 280, '', $Link);
        
        $Link = './fachbetriebe_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=FAB_BEREICH' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FAB_BEREICH')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FAB']['FAB_BEREICH'], 200	, '', $Link);
        
        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsFAB->EOF())
        {
            $Form->ZeileStart();//('font-size:10pt');
            $Link = './fachbetriebe_Main.php?cmdAktion=Details&FAB_KEY=' . $rsFAB->FeldInhalt('FAB_KEY') . '';

            $Style = '';//$rsFAB->FeldInhalt('STYLE');
            $Form->Erstelle_ListenFeld('FAB_NAME1', $rsFAB->FeldInhalt('FAB_NAME1'), 50, 280, false, ($DS % 2), $Style, $Link, '', '', '');
            $Form->Erstelle_ListenFeld('FAB_LAN_CODE', $rsFAB->FeldInhalt('FAB_LAN_CODE'), 2, 50, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('FAB_PLZ', $rsFAB->FeldInhalt('FAB_PLZ'), 5, 100, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('FAB_ORT', $rsFAB->FeldInhalt('FAB_ORT'), 50, 280, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('FAB_STRASSE', $rsFAB->FeldInhalt('FAB_STRASSE'), 50, 280, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('FAB_BEREICH', $rsFAB->FeldInhalt('FAB_BEREICH'), 50, 200, false, ($DS % 2), $Style, '', '', '', '');

			$Form->ZeileEnde();

            $rsFAB->DSWeiter();
            $DS++;
        }

        $Link = './fachbetriebe_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht27000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->SchaltflaechenEnde();
    }
    else          // Eine einzelner Datensatz
    {
        $Form->SchreibeHTMLCode('<form name=frmFachbetriebe action=./fachbetriebe_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsFAB->FeldInhalt('FAB_KEY');
        $Param['KEY']=$AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben("Formular_FAB", serialize($Param));
        $EditRecht = ($Recht27000&2!=0);
        $Form->Erstelle_HiddenFeld('FAB_KEY', $AWIS_KEY1);

        $Form->Formular_Start();
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    	$Form->DebugAusgabe(1,$AWIS_KEY1);

        // Infozeile
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./fachbetriebe_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFAB->FeldInhalt('FAB_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFAB->FeldInhalt('FAB_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_BEREICH'] . ':', 150);
        $Daten = explode("|",$AWISSprachKonserven['FAB']['lst_FAB_BEREICH']);
        $Form->Erstelle_SelectFeld('!FAB_BEREICH',$rsFAB->FeldInhalt('FAB_BEREICH'), 200,$EditRecht,'',$OptionBitteWaehlen,'','','',$Daten);
        $Form->ZeileEnde();
        $AWISCursorPosition = 'txtFAB_BEREICH';
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_NAME1'] . ':', 150);
        $Form->Erstelle_TextFeld('!FAB_NAME1', ($rsFAB->FeldInhalt('FAB_NAME1')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_NAME2'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_NAME2', ($rsFAB->FeldInhalt('FAB_NAME2')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_ANSPRECHP1'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_ANSPRECHP1', ($rsFAB->FeldInhalt('FAB_ANSPRECHP1')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_LAN_CODE'].':',150);
        $SQL = ' SELECT LAN_CODE, LAN_LAND FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL ORDER BY 1';
        $Form->Erstelle_SelectFeld('!FAB_LAN_CODE',$rsFAB->FeldInhalt('FAB_LAN_CODE'),"100:120",$EditRecht,$SQL,$OptionBitteWaehlen);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_PLZ'] . ':', 150);
        $Form->Erstelle_TextFeld('!FAB_PLZ', ($rsFAB->FeldInhalt('FAB_PLZ')),5, 100, $EditRecht,'','','','','','','',5);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_ORT'] . ':', 150);
        $Form->Erstelle_TextFeld('!FAB_ORT', ($rsFAB->FeldInhalt('FAB_ORT')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_STRASSE'] . ':', 150);
        $Form->Erstelle_TextFeld('!FAB_STRASSE', ($rsFAB->FeldInhalt('FAB_STRASSE')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_TELEFON'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_TELEFON', ($rsFAB->FeldInhalt('FAB_TELEFON')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_TELEFAX'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_TELEFAX', ($rsFAB->FeldInhalt('FAB_TELEFAX')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_MOBILNUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_MOBILNUMMER', ($rsFAB->FeldInhalt('FAB_MOBILNUMMER')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_EMAIL'] . ':', 150);
        $Form->Erstelle_TextFeld('FAB_EMAIL', ($rsFAB->FeldInhalt('FAB_EMAIL')),50, 300, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht27000 & 6) != 0) // Bearbeiten/Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	
	    if (($Recht27000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	
	    if (($Recht27000 & 8) == 8 AND !isset($_POST['cmdDSNeu_x']))
	    {
	        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	    }
	
	    $Form->SchaltflaechenEnde();
    }

    $Form->SchreibeHTMLCode('</form>');
    
    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $AWISBenutzer->ParameterSchreiben("Formular_FAB", serialize($Param));

    $Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201307151012");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201307151013");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param, &$BindeVariablen)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= 'AND FAB_KEY = :var_N0_FAB_KEY';
        $BindeVariablen['var_N0_FAB_KEY'] = $AWIS_KEY1;
        return $Bedingung;
    }
    
    if (isset($Param['FAB_KEY']) AND floatval($Param['FAB_KEY'])!=0)
    {
    	$Bedingung .= 'AND FAB_KEY = :var_N0_FAB_KEY';
        $BindeVariablen['var_N0_FAB_KEY'] = $Param['FAB_KEY'];
        return $Bedingung;
    }

    if (isset($Param['FAB_BEREICH']) AND $Param['FAB_BEREICH'] != '')
    {
    	$Bedingung .= 'AND FAB_BEREICH = :var_T_FAB_BEREICH';
        $BindeVariablen['var_T_FAB_BEREICH'] = $Param['FAB_BEREICH'];
   	}
    
    if (isset($Param['FAB_PLZ']) AND $Param['FAB_PLZ'] != '')
    {
    	//$Bedingung .= 'AND FAB_PLZ = :var_T_FAB_PLZ';
        //$BindeVariablen['var_T_FAB_PLZ'] = $Param['FAB_PLZ'];
        $Bedingung .= ' AND FAB_PLZ '.$DB->LIKEoderIST($Param['FAB_PLZ']);
   	}
   	
    if (isset($Param['FAB_ORT']) AND $Param['FAB_ORT'] != '')
    {
    	//$Bedingung .= 'AND FAB_ORT = :var_T_FAB_ORT';
        //$BindeVariablen['var_T_FAB_ORT'] = $Param['FAB_ORT'];
    	$Bedingung .= ' AND UPPER(FAB_ORT) '.$DB->LIKEoderIST($Param['FAB_ORT'],awisDatenbank::AWIS_LIKE_UPPER);
    }
   	return $Bedingung;
}

?>