<?php

global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('sgh_import.php');
require_once('sgh_abgleich.php');


try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SGD','*');
    $TextKonserven[]=array('SGK','*');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    	
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht9600 = $AWISBenutzer->HatDasRecht(9600);
    
    if($Recht9600==0)
    {
        $DB->ProtokollEintrag(awisDatenbank::EREIGNIS_FEHLER, 1000, $AWISBenutzer->BenutzerName(),'Folienaustausch - Modul',$AWISBenutzer->BenutzerName());
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        die();
    }
   	
		
	
   $Form->Formular_Start();
   
   //$import = new SGHImport();
   //$import->starteImport();
   //$abgleich = new SGHAbgleich();
   //$abgleich->starteAbgleich();

   $Form->Formular_Ende();
    
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}

?>