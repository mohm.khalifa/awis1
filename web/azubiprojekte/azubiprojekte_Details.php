<?php
global $Funktionen;

$Funktionen->RechteMeldung(2);

$Funktionen->AWIS_KEY1 = 0;

$Bedingung = "";

$SQL = "";
$SQL .= "SELECT t.*, b.AZB_BEREICHNAME FROM AZUBIPROJEKTETEILNEHMER t ";

if (isset($_POST['cmdSuche_x'])){

    $Funktionen->Param['AZT_AZB_STUFE']=$_POST['sucAZB_STUFE'];
    $Funktionen->Param['AZT_PERSNR']=$_POST['sucAZT_PERSNR'];
    $Funktionen->Param['AZT_FILNR']=$_POST['sucAZT_FILNR'];
    $Funktionen->Param['AZT_NAME']=$_POST['sucAZT_NAME'];
    $Funktionen->Param['AZT_VORNAME']=$_POST['sucAZT_VORNAME'];

    $Funktionen->Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

} elseif(isset($_GET['AZT_KEY'])) {
    $Funktionen->AWIS_KEY1 = $_GET['AZT_KEY'];

}
$Bedingung = $Funktionen->BedingungErstellen();

$Join = " LEFT JOIN AZUBIPROJEKTEBEREICHE b ";
$Join .= " ON t.AZT_AZB_STUFE = b.AZB_STUFE ";

if (!isset($_GET['Sort'])) {
    $Order = ' ORDER BY t.AZT_PERSNR ASC, t.AZT_AZB_STUFE ASC';
} else {
    $Order = ' ORDER BY AZT_' . str_replace('~', ' DESC ', $_GET['Sort']);
}

$SQL = $SQL.$Join.$Bedingung.$Order;
$rsAZT = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen('AZT'));

if($rsAZT->AnzahlDatensaetze() == 1 and !isset($_GET['AZT_KEY'])) {
    $Funktionen->AWIS_KEY1 = $rsAZT->FeldInhalt('AZT_KEY');
} elseif ($rsAZT->AnzahlDatensaetze() == 0) {
    $Funktionen->AWIS_KEY1 = NULL;
}

if($Funktionen->AWIS_KEY1 === 0) {

    $Feldbreiten = array();
    $Feldbreiten['AZT_STUFE']=200;
    $Feldbreiten['AZT_FILNR']=150;
    $Feldbreiten['AZT_PERSNR']=150;
    $Feldbreiten['AZT_NAME']=200;
    $Feldbreiten['AZT_VORNAME']=200;

    $Funktionen->Form->ZeileStart();

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=PERSNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'PERSNR')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['Wort']['Personalnummer'], $Feldbreiten['AZT_PERSNR'], '', $Link);

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=NAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'NAME')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['AZT']['AZT_NAME'], $Feldbreiten['AZT_NAME'], '', $Link);

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=VORNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VORNAME')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['AZT']['AZT_VORNAME'], $Feldbreiten['AZT_VORNAME'], '', $Link);

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=FILNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FILNR')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['AZT']['AZT_FILNR'], $Feldbreiten['AZT_FILNR'], '', $Link);

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=AZB_STUFE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AZB_STUFE')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['AZT']['AZT_STUFE'], $Feldbreiten['AZT_STUFE'], '', $Link);

    $Funktionen->Form->ZeileEnde();

    $DS = 0;    // f�r Hintergrundfarbumschaltung
    while (!$rsAZT->EOF()) {
        $Funktionen->Form->ZeileStart();

        $Link = "./azubiprojekte_Main.php?cmdAktion=Details&AZT_KEY=" . $rsAZT->FeldInhalt('AZT_KEY');
        $Funktionen->Form->Erstelle_ListenFeld('AZT_PERSNR', $rsAZT->FeldInhalt('AZT_PERSNR'), 0,  $Feldbreiten['AZT_PERSNR'], false, ($DS % 2), '', $Link);
        $Funktionen->Form->Erstelle_ListenFeld('AZT_NAME', $rsAZT->FeldInhalt('AZT_NAME'), 0,  $Feldbreiten['AZT_NAME'], false, ($DS % 2));
        $Funktionen->Form->Erstelle_ListenFeld('AZT_VORNAME', $rsAZT->FeldInhalt('AZT_VORNAME'), 0,  $Feldbreiten['AZT_VORNAME'], false, ($DS % 2));
        $Funktionen->Form->Erstelle_ListenFeld('AZT_FILNR', $rsAZT->FeldInhalt('AZT_FILNR'), 0,  $Feldbreiten['AZT_FILNR'], false, ($DS % 2));
        $Funktionen->Form->Erstelle_ListenFeld('AZT_AZB_STUFE', $rsAZT->FeldInhalt('AZB_BEREICHNAME'), 0,  $Feldbreiten['AZT_STUFE'], false, ($DS % 2));

        $DS++;
        $Funktionen->Form->ZeileEnde();
        $rsAZT->DSWeiter();
    }

} elseif($Funktionen->AWIS_KEY1 > 0){

    $Labelbreiten = array();
    $Labelbreiten['AZT_STUFE']=250;
    $Labelbreiten['AZT_PERSNR']=250;
    $Labelbreiten['AZT_FILNR']=250;
    $Labelbreiten['AZT_NAME_VORNAME']=250;
    $Labelbreiten['AZT_TSHIRT_GROESSE']=250;
    $Labelbreiten['AZT_EMAIL']=250;
    $Labelbreiten['AZT_TEL_NR']=250;

    $Feldbreiten = array();
    $Feldbreiten['AZT_STUFE']=400;
    $Feldbreiten['AZT_PERSNR']=300;
    $Feldbreiten['AZT_FILNR']=300;
    $Feldbreiten['AZT_NAME_VORNAME']=300;
    $Feldbreiten['AZT_TSHIRT_GROESSE']=300;
    $Feldbreiten['AZT_EMAIL']=300;
    $Feldbreiten['AZT_TEL_NR']=300;

    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./azubiprojekte_Main.php?cmdAktion=Details accesskey=T title='" . $Funktionen->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsAZT->FeldInhalt('AZT_USER'));
    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsAZT->FeldInhalt('AZT_USERDAT'));
    $Funktionen->Form->InfoZeile($Felder);

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_STUFE'].":",$Labelbreiten['AZT_STUFE']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_AZB_STUFE', $rsAZT->FeldInhalt('AZB_BEREICHNAME'), 400, $Feldbreiten['AZT_STUFE']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_PERSNR'].":",$Labelbreiten['AZT_PERSNR']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_PERSNR', $rsAZT->FeldInhalt('AZT_PERSNR'), 200, $Feldbreiten['AZT_PERSNR']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_FILNR'].":",$Labelbreiten['AZT_FILNR']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_FILNR', $rsAZT->FeldInhalt('AZT_FILNR'), 200, $Feldbreiten['AZT_FILNR']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_NAME'].", ".$Funktionen->AWISSprachKonserven['AZT']['AZT_VORNAME'].":",$Labelbreiten['AZT_NAME_VORNAME']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_NAME_VORNAME', $rsAZT->FeldInhalt('AZT_NAME')." ".$rsAZT->FeldInhalt('AZT_VORNAME'), 200, $Feldbreiten['AZT_NAME_VORNAME']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_TSHIRT_GROESSE'].":",$Labelbreiten['AZT_TSHIRT_GROESSE']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_TSHIRT_GROESSE', $rsAZT->FeldInhalt('AZT_TSHIRT_GROESSE'), 200, $Feldbreiten['AZT_TSHIRT_GROESSE']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_EMAIL'].":",$Labelbreiten['AZT_EMAIL']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_EMAIL', $rsAZT->FeldInhalt('AZT_EMAIL'), 200, $Feldbreiten['AZT_EMAIL']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_TEL_NR'].":",$Labelbreiten['AZT_TEL_NR']);
    $Funktionen->Form->Erstelle_TextFeld('AZT_TEL_NR', $rsAZT->FeldInhalt('AZT_TEL_NR'), 200, $Feldbreiten['AZT_TEL_NR']);
    $Funktionen->Form->ZeileEnde();
} else {
    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_KEINE_DS'],awisFormular::HINWEISTEXT_HINWEIS);
    $Funktionen->Form->ZeileEnde();
}

$Funktionen->Form->SchaltflaechenStart();
$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Funktionen->Form->SchaltflaechenEnde();