<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
    
	$AWISBenutzer = awisBenutzer::Init('');
    $Form = new awisFormular();
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
    $DBSCHAD->Oeffnen();
    
	
	$Recht40016 = $AWISBenutzer->HatDasRecht(40016);
	
	
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	
	
	$Param['BES_EXPORT'] = $Form->Format('N0',$_POST['sucExport'],true);
	$Param['BES_BEARBEITUNG'] = $Form->Format('T',$_POST['sucBES_BEARBEITUNG'],true);
	$Param['BES_KENNUNG'] = $Form->Format('T',$_POST['sucBES_KENNUNG'],true);
	$Param['BES_ATUNR'] = $Form->Format('T',$_POST['sucBES_ATUNR'],true);
	$Param['BES_LIEFERANT'] = $Form->Format('T',$_POST['sucBES_LIEFERANT'],true);
	$Param['BES_STAND'] = $Form->Format('T',$_POST['sucBES_STAND'],true);
	$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	$AWISBenutzer->ParameterSchreiben('Formular_BES_Export',serialize($Param));
	
	
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$ExportFormat = 1;
	$Anzahl = 0;
  
	$DateiName = 'Aufgabenliste';
	  
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	 
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
	        break;
	    case 2:                 // Excel 2007
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
	        break;
	}
	 
	$XLSXObj = new PHPExcel();
	$XLSXObj->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$XLSXObj->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToPage(true);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$XLSXObj->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	$XLSXObj->getProperties()->setCreator(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setTitle(utf8_encode('Aufgabenliste'));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode('Aufgabenliste'));
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode('Aufgabenliste'));
	
	$SQL  = "SELECT distinct S.*,AA.*,SB.*,(select listagg(v.schadensnr,',') within group(order by 1) from zahlungen z left join vers_new v on z.zuvsid = v.VS_ID where z.bearbeitungsnr = s.bearbeitungsnr) as VERSNR ";
    $SQL .= ',W.WV_GRUND ';
	$SQL .= "FROM SCHAEDEN_NEW S ";
	$SQL .= "LEFT JOIN ANTRAGART AA ON ANTRAGART = AA.ID ";
	$SQL .= "LEFT JOIN SACHBEARBEITER SB ON BEARBEITER = SB.ID ";
	if($_POST['sucBES_AUFGABE'] == 0)
	{
		$SQL2 = ' Select * from Sachbearbeiter where AWIS_USER='.$AWISBenutzer ->BenutzerID();
		$rsSB = $DBSCHAD->RecordSetOeffnen($SQL2);
		$User = $rsSB->FeldInhalt('SBNAME'); 
	}
	else 
	{
		$SQL2 = ' Select * from Sachbearbeiter where ID='.$_POST['sucBES_AUFGABE'];
		$rsSB = $DBSCHAD->RecordSetOeffnen($SQL2);
		$User = $rsSB->FeldInhalt('SBNAME');
	}	
	$SQL .=	"LEFT JOIN WIEDERVORLAGEN_NEW W on W.BEARBEITUNGSNR = S.BEARBEITUNGSNR ";
	$SQL .=	"where WV_AN ='".$User."'";
	$SQL .=	" and (trunc(WV_DATUM) = trunc(sysdate) or trunc(WV_DATUM) < trunc(sysdate))";

	$rsVorgaenge = $DBSCHAD->RecordSetOeffnen($SQL,$DB->Bindevariablen('SBV',true));
	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	
	
	
	//�berschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode($rsSB->FeldInhalt('SBNAME').' '.$rsSB->FeldInhalt('SBVORNAME')));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	

	

	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y H:i:s',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$ZeilenNr++;
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Bearbeitungs-Nr.');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Eing. am');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Antragsart');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Fil-Nr');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Kundenname');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'KFZ-Kenn.');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;

	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WV-Grund');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	

	while(!$rsVorgaenge->EOF()) 
	{    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('BEARBEITUNGSNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	    
	    
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	   
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('WERT'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr++;
	     
	   
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('N0',$rsVorgaenge->FeldInhalt('FILNR'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
	    
	    $SpaltenNr++;
	     
	   
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr++;
	     
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('KFZ_KENNZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsVorgaenge->FeldInhalt('WV_GRUND'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	    $SpaltenNr = 0;
	    $ZeilenNr++;
	    
        $rsVorgaenge->DSWeiter();
	}

	
	for($S='A';$S<='G';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	 
	
	
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="Aufgabenliste.xls"');
	        $DateiEndung = '.xls';
	        break;
	    case 2:                 // Excel 2007
	         
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="Aufgabenliste.xlsx"');
	        $DateiEndung = '.xlsx';
	        break;
	}
	 
	

	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
	        break;
	    case 2:                 // Excel 2007
	        $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
	        break;
	}
	//var_dump($Lagerkennung);
	//$objWriter->save('/daten/daten/pccommon/Gebhardt/' . $DateiName . '.xls');
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
	$Anzahl++;
	
		
}
catch(exception $ex)
{
	$Text = $ex->getMessage();
	
	
	$Absender = 'awisstag@de.atu.eu';
	$Betreff = "OK Personaleinsaetze-Export-Excel " . date('ymd');
	$Text = $Anzahl . ' Daten exportiert  >>/var/log/awis/Personaleinsaetze.log';
	$Empfaenger = 'shuttle@de.atu.eu';
	$Mail->AnhaengeLoeschen();
	$Mail->LoescheAdressListe();
	$Mail->DebugLevel(0);
	$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
	$Mail->Absender($Absender);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
	$Mail->SetzeBezug('TES',0); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
	$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
	$Mail->MailSenden();
}




?>