<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	//Statuswechsel durch die Listenansicht.
	if (isset($_GET['button']))
	{
	    if(isset($_GET['VERALTET']))
    	{
    	    $Aktiv = '';
    	    if($_GET['VERALTET'] == '1')
    	    {
    	        $Aktiv = 0;
    	    }
    	    else
    	    {
    	        $Aktiv = 1;
    	    }
    	    $SQL = "UPDATE BESCHAEDIGT ";
    	    $SQL .="SET VERALTET =".$Aktiv;
    	    $SQL .= ',  BES_USER=\''.$AWISBenutzer->BenutzerName().'\'';
    	    $SQL .= ',  BES_USERDAT=SYSDATE';
    	    $SQL .=" WHERE BID = " .$_GET['button'];
    	 
	        $DBSCHAD->Ausfuehren($SQL);
	    }
	    elseif(isset($_GET['FILIALEN']))
	    {
	        $Sichtbar = '';
	        if($_GET['FILIALEN'] == '1')
	        {
	            $Sichtbar = 0;
	        }
	        else
	        {
	            $Sichtbar = 1;
	        }
	        $SQL = "UPDATE BESCHAEDIGT ";
	        $SQL .="SET SICHTBAR_FILIALEN =".$Sichtbar;
	        $SQL .= ',  BES_USER=\''.$AWISBenutzer->BenutzerName().'\'';
	        $SQL .= ',  BES_USERDAT=SYSDATE';
	        $SQL .=" WHERE BID = " .$_GET['button'];
	        
	        $DBSCHAD->Ausfuehren($SQL);
	    } 
	}
	
	
	if ($AWIS_KEY1 == '-1') //Neuanlage
	{
	   
	    $SQL ='insert into BESCHAEDIGT ';
	    $SQL .=' (SCHLAGWORT,VERALTET,SICHTBAR_FILIALEN,BES_USER,BES_USERDAT, BID)';
	    $SQL .=' values';
	    $SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtBES_SCHLAGWORT']);
	   
	    if(isset($_POST['txtCHK_AKTIV']))
	    {
	        $SQL .= ',0';
	    }
	    else
	    {
	        $SQL .= ',1';
	    }
	    if(isset($_POST['txtCHK_SICHTBAR_FILIALEN']))
	    {
	    	$SQL .= ',1';
	    }
	    else
	    {
	    	$SQL .= ',0';
	    }
	    
	    $SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
	    $SQL .=',sysdate, seq_beschaedigt_id.nextval)';
	    	
	    $DBSCHAD->Ausfuehren($SQL,'',true);
	    $AWIS_KEY1='';
	    $SpeichernOK = true;
	    $Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
	}
	elseif($AWIS_KEY1 > '0')
	{		
	    
			$SQL ='update BESCHAEDIGT ';
			$SQL .= ' set SCHLAGWORT='.$DB->FeldInhaltFormat('T',$_POST['txtBES_SCHLAGWORT']);
			$SQL .= ' ';
				
			if(isset($_POST['txtCHK_AKTIV']))
			{
				$SQL .= ' ,VERALTET=0';
			}
			else
			{
				$SQL .= ' ,VERALTET=1';
			}
			
			if(isset($_POST['txtCHK_SICHTBAR_FILIALEN']))
			{
			    $SQL .= ', SICHTBAR_FILIALEN=1';
			}
			else
			{
			    $SQL .= ', SICHTBAR_FILIALEN=0';
			}
				
			
			$SQL .= ', BES_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', BES_USERDAT=SYSDATE';
			$SQL .=' where BID='.$AWIS_KEY1;
			$DBSCHAD->Ausfuehren($SQL,'',true);		
			$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			$SpeichernOK = true;
	}
}
catch (awisException $ex)
{
    
    if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.UNIQUE_SCHLAGWORT) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
        $Fehler = 1;
        $Hinweis = 'Die Kennung befindet sich schon in der Datenbank.';
       
        $Form->Hinweistext($Hinweis);
    }
    else
    {
        $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
        $Form->DebugAusgabe(1,$ex->getSQL());
    }


	
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
