<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>

</head>

<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

clearstatcache();

include ("ATU_Header.php");	// Kopfzeile

global $awisRSZeilen;
global $awisDBFehler;

awis_ZeitMessung(0);		// Zeit setzen

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1700);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Sonderausweis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

	$SQL='SELECT SAW_KEY, SAW_TAETIGKEIT FROM SONDERAUSWEISE WHERE SAW_TAETIGKEIT_DECRYPT IS NULL';
	$rsSAW = awisOpenRecordset($con,$SQL);
	$rsSAWZeilen = $awisRSZeilen;
	
	for($SAWZeile=0;$SAWZeile<$rsSAWZeilen;$SAWZeile++)
	{
		$SQL="UPDATE SONDERAUSWEISE SET SAW_TAETIGKEIT_DECRYPT='".awisDecrypt($con,$rsSAW['SAW_TAETIGKEIT'][$SAWZeile])."' WHERE SAW_KEY=0".$rsSAW['SAW_KEY'][$SAWZeile];
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("sonderausweis_Decrypt.php", 2, $awisDBFehler);
			awisLogoff($con);
			echo "Es wurden ".$SAWZeile." ge�ndert";
			die();
		}		
	}

	echo "Es wurden ".$SAWZeile." ge�ndert";
	
awislogoff($con);

echo '<br><font size=1>Diese Seite wurde in ' . sprintf('%.4f', awis_ZeitMessung(1)) . ' Sekunden erstellt.';

?>
</body>
</html>

