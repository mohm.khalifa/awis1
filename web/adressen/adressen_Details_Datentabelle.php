<?php
global $Recht3000;
global $CursorFeld;	
global $AWISCursorPosition;	// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{
    if(isset($_GET['Del']) AND isset($_GET['Seite']) AND $_GET['Seite']=='Infos')
    {
        include('./adressen_loeschen.php');
    }
    
    $TextKonserven = array();
    $TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprache = $AWISBenutzer->ParameterLesen('AnzeigeSprache');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
		
    $Recht3000 = $AWISBenutzer->HatDasRecht(3000);
    if($Recht3000==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
    }
    
    if($AWIS_KEY1!='')		// ADR_KEY
    {
        $Form->Formular_Start();
        $EditModus=($Recht3000&2);
        if($EditModus)
        {
            $AWISCursorPosition='';
        }
        if(isset($_GET['Edit']))
        {
            $rsADI = $DB->RecordSetOeffnen('SELECT * FROM AdressenInformationen WHERE ADI_KEY=0'.(isset($_GET['Edit'])?$_GET['Edit']:''));
              
            if($_GET['Edit']==0)
            {
                $FeldListe = array();
                foreach ($rsADI AS $Felder=>$Liste)			// Bei einem neuen Datensatz ein leeres Recordset bauen
                {
                    $FeldListe[$Felder]=array('');
                }
                $rsADI = $FeldListe;
            }
    
            $SQL = 'SELECT *';
            $SQL .= ' FROM (SELECT ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT ';
            $SQL .= ' FROM ADRESSENINFOTYPEN';
            $SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
            $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
            $SQL .= ' INNER JOIN Adressen on ADR_KEY='.$AWIS_KEY1;
            $SQL .= ' and Adressen.ADR_AZG_KEY = ADT_AZG_KEY';
            $SQL .= ' WHERE ADT_VERWENDUNG=4';
            $SQL .= ' GROUP BY ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT';
            $SQL .= ') DATEN ';
            $SQL .= ' LEFT OUTER JOIN AdressenInformationen ON adi_xxx_key = 0'.$AWIS_KEY1.' AND adi_adt_key = adt_key AND ADI_DATENZEILE = 0'.floatval($_GET['Edit']);
            $SQL .= ' ORDER BY  ADT_SORTIERUNG, ADT_INFORMATION';
            
            $rsADI = $DB->RecordSetOeffnen($SQL);
            $rsADIZeilen = $rsADI->AnzahlDatensaetze();
    
            // Infozeile zusammenbauen
            $Felder = array();
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Details&Seite=Datentabelle accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADI->FeldInhalt('ADI_USER')));
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADI->FeldInhalt('ADI_USERDAT')));
            $Form->InfoZeile($Felder,'');
    
            // Eine Nummer suchen (falls neue Infos eingefügt wurden)
            $DatenZeile=0;
            for($Zeile=0;$Zeile<$rsADIZeilen;$Zeile++)
            {
                if($rsADI->FeldInhalt('ADI_DATENZEILE')!='')
                {
                    $DatenZeile=$rsADI->FeldInhalt('ADI_DATENZEILE');
                    break;
                }
                $rsADI->DSWeiter();
            }
            
            $rsADI->DSErster();
            
            for($ADIZeile=0;$ADIZeile<$rsADIZeilen;$ADIZeile++)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($rsADI->FeldInhalt('ADT_INFORMATION').':',200,'');
                if($rsADI->FeldInhalt('ADT_DATENQUELLE')!='')
                {
                    switch(substr($rsADI->FeldInhalt('ADT_DATENQUELLE'),0,3))
                    {
                        case 'TXT':
                            $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
                            $Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
                            $Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
                            $Form->Erstelle_SelectFeld('XXX_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$DatenZeile.'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
                        break;
                        case 'SQL':
                            $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
                            $Form->Erstelle_SelectFeld('XXX_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$DatenZeile.'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'',$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                        break;
                        default:
                            $Form->Erstelle_TextFeld('XXX_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$DatenZeile.'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'),'','',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
                        break;
                    }
                }
                else
                {
                    $Form->Erstelle_TextFeld('XXX_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$DatenZeile.'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'));
			    }
		
			     if($AWISCursorPosition=='')
			     {
				    $AWISCursorPosition='txtXXX_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$DatenZeile.'_'.$rsADI->FeldInhalt('ADI_KEY');
			     }
			     $Form->ZeileEnde();
			     $rsADI->DSWeiter();
            }
    
        }
        else
        {
            $Form->ZeileStart();
            if(($Recht3000&4)==4)
            {
    			$Link = '<a href=./adressen_Main.php?cmdAktion=Details&Seite=Datentabelle&Edit=0><img border=0 src=/bilder/icon_new.png></a>';
    			$Form->Erstelle_Liste_Ueberschrift($Link,38,'background-color:transparent;');
		    }
            else
            {
                $Form->Erstelle_Liste_Ueberschrift('',38);
            }
                $SQL = 'SELECT Distinct ADRESSENINFOTYPEN.*';
                $SQL .= ' FROM ADRESSENINFOTYPEN';
                $SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
                $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
                $SQL .= ' INNER JOIN Adressen on ADR_KEY='.$AWIS_KEY1;
                $SQL .= ' and Adressen.ADR_AZG_KEY = ADT_AZG_KEY';
                $SQL .= ' WHERE ADT_VERWENDUNG = 4';
                
                if(isset($_GET['Edit']))
        		{
        			$SQL .= ' AND ADI_KEY <> 0'.$_GET['Edit'];
        		}
        		
		        $SQL .= ' ORDER BY ADT_SORTIERUNG, ADT_INFORMATION';
		        
        		$rsADT = $DB->RecordSetOeffnen($SQL);
                $rsADTZeilen = $rsADT->AnzahlDatensaetze();
                for($ADTZeile=0;$ADTZeile<$rsADTZeilen;$ADTZeile++)
        		{
        		    $Form->Erstelle_Liste_Ueberschrift($rsADT->FeldInhalt('ADT_INFORMATION'),$rsADT->FeldInhalt('ADT_BREITE'));
                    $rsADT->DSWeiter();
        		}
    
			    // Erst mal alle Zeilen bestimmen
            	$SQL = 'SELECT DISTINCT ADI_DATENZEILE';
        		$SQL .= ' FROM (SELECT ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT ';
                $SQL .= ' FROM ADRESSENINFOTYPEN';
        		$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
        		$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
        		$SQL .= ' INNER JOIN Adressen on ADR_KEY='.$AWIS_KEY1;
                $SQL .= ' and Adressen.ADR_AZG_KEY = ADT_AZG_KEY';
            	$SQL .= ' WHERE ADT_VERWENDUNG=4';
                $SQL .= ' GROUP BY ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT';
        		$SQL .= ') DATEN ';
        		$SQL .= ' INNER JOIN AdressenInformationen ON adi_xxx_key = 0'.$AWIS_KEY1.' AND adi_adt_key = adt_key';
        		$SQL .= ' ORDER BY  ADI_DATENZEILE';

        		$rsZeilen = $DB->RecordSetOeffnen($SQL);
        		$rsZeilenZeilen = $rsZeilen->AnzahlDatensaetze();
    
        		for($Zeile=0;$Zeile<$rsZeilenZeilen;$Zeile++)
        		{
        				// Zu jeder Zeile die Daten holen
        			$SQL = 'SELECT *';
        			$SQL .= ' FROM (SELECT ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT ';
        			$SQL .= ' FROM ADRESSENINFOTYPEN';
            		$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
            		$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
            		$SQL .= ' INNER JOIN Adressen on ADR_KEY='.$AWIS_KEY1;
            		$SQL .= ' and Adressen.ADR_AZG_KEY = ADT_AZG_KEY';
            		$SQL .= ' WHERE ADT_VERWENDUNG=4';
            		$SQL .= ' GROUP BY ADT_KEY, ADT_INFORMATION, ADT_SORTIERUNG, ADT_DATENQUELLE, ADT_ZEICHEN, ADT_BREITE, ADT_FORMAT';
            		$SQL .= ') DATEN ';
            		$SQL .= ' LEFT OUTER JOIN AdressenInformationen ON adi_xxx_key = 0'.$AWIS_KEY1.' AND adi_adt_key = adt_key';
            		$SQL .= '  AND adi_datenzeile=0'.$rsZeilen->FeldInhalt('ADI_DATENZEILE');
        			$SQL .= ' ORDER BY  ADT_SORTIERUNG, ADT_INFORMATION';
            
            		$rsADI = $DB->RecordSetOeffnen($SQL);
            		$rsADIZeilen = $rsADI->AnzahlDatensaetze();
        
        			$DatenZeile=0;
        			
        			for($i=0;$i<$rsADIZeilen;$i++)
            		{
            			    if($rsADI->FeldInhalt('ADI_DATENZEILE')!='')
            			    {
                			    $DatenZeile=$rsADI->FeldInhalt('ADI_DATENZEILE');
                			    break;
            			    }
            			    $rsADI->DSWeiter();
            		}
            
    		        $rsADI->DSErster();
    		
            		for($ADIZeile=0;$ADIZeile<$rsADIZeilen;$ADIZeile+=$rsADTZeilen)
            		{
            		    $Form->ZeileStart();
            			$Icons = array();
        				$Icons[0] = array('edit','./adressen_Main.php?cmdAktion=Details&Seite=Datentabelle&Edit='.$DatenZeile);
        				
        				if(($Recht3000&8)==8)	// Löschrecht?
        				{
        					$Icons[] = array('delete','./adressen_Main.php?cmdAktion=Details&Seite=Datentabelle&Del='.$DatenZeile);
        				}
                
    				    $Form->Erstelle_ListeIcons($Icons,38);
    
        				// Hier die einzelnen Datensätze anzeigen
        				for($i=$ADIZeile;$i<$ADIZeile+$rsADTZeilen;$i++)
        				{
        				    $Form->Erstelle_ListenFeld('#WERT',$rsADI->FeldInhalt('ADI_WERT')!=''?$rsADI->FeldInhalt('ADI_WERT'):'',0,$rsADI->FeldInhalt('ADT_BREITE'),false,($ADIZeile%2),'','',$rsADI->FeldInhalt('ADT_FORMAT'));
        				    $rsADI->DSWeiter();
        				}
                    
    				    $Form->ZeileEnde();
    			     }
    			     $rsZeilen->DSWeiter();
        		}
        }
    	$Form->Formular_Ende();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>