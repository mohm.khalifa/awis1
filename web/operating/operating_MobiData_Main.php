<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once '../operating/operating_funktionen.php';

global $AWISCursorPosition;		// Aus AWISFormular

try {
    $MDI = new operating_funktionen();
    echo "<link rel=stylesheet type=text/css href=" . $MDI->AWISBenutzer->CSSDatei($AWISVersion) .">";

} catch (Exception $ex) {
    die($ex->getMessage());
}

echo '<title>'.$MDI->AWISSprachkonserven['TITEL']['tit_MobiDataOperating'].'</title>';
?>
</head>
<body>
<script src="/jquery_ie11.js"></script>
<script src="/jquery.scrollUp.min.js"></script>
<script src="/popup.js"></script>
<script src="/formularBereich.js"></script>
<script src="/jquery.tooltipster.js"></script>
<script src="/jquery-ui.js"></script>
<link rel=stylesheet type=text/css href="/css/jquery-ui.css">
<link rel=stylesheet type=text/css href="/css/tooltipster.css">
<link rel=stylesheet type=text/css href="/css/tooltipster-punk.css">
<link rel=stylesheet type=text/css href="/css/popup.css">
<?php
include ("awisHeader$AWISVersion.inc");	// Kopfzeile

try
{
    $Form = new awisFormular(3);

    if($AWISBenutzer->HatDasRecht(3128)==0)
    {
        $Form->Fehler_Anzeigen('Rechte','','MELDEN',1,"200907281613");
        die('Keine Rechte');
    }

    $Register = new awisRegister(3128);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    if($AWISCursorPosition!=='')
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
        echo '</Script>';
    }
}
catch (Exception $ex)
{
    echo  $ex->getMessage();
}
?>
</body>
</html>
