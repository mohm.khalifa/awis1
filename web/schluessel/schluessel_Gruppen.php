<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Rechtestufe = awisBenutzerRecht($con, 1501);
		//  1=Einsehen
		//	2=Hinzuf�gen
		//	4=Bearbeiten
		//  8=L�schen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schluessel',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

if(isset($_REQUEST['txtSLG_KEY_loeschen']) && $_REQUEST['txtSLG_KEY_loeschen']!='')
{
		$SQL='SELECT * FROM SCHLUESSELNUMMERN WHERE SLN_SLG_KEY=0'.$_REQUEST['txtSLG_KEY_loeschen'];
		$rsSLG_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLG_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		$SQL='SELECT * FROM SCHLUESSELUNTERGRUPPEN WHERE SLU_SLG_KEY=0'.$_REQUEST['txtSLG_KEY_loeschen'];
		$rsSLGSLU_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLGSLU_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		if($rsSLG_KEY_LOESCHENZeilen>0 || $rsSLGSLU_KEY_LOESCHENZeilen>0)
		{
				echo "<span class=HinweisText>Datensatz kann nicht gel&ouml;scht werden. L&ouml;sen Sie erst die Zuordnung auf.</span>";
		}
		else
		{
				$SQL='DELETE FROM SCHLUESSELGRUPPEN WHERE SLG_KEY=0'.$_REQUEST['txtSLG_KEY_loeschen'];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
				awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
				}
		}
}

if(isset($_REQUEST['txtSLU_KEY_loeschen']) && $_REQUEST['txtSLU_KEY_loeschen']!='')
{
		$SQL='SELECT * FROM SCHLUESSELNUMMERN WHERE SLN_SLU_KEY=0'.$_REQUEST['txtSLU_KEY_loeschen'];
		$rsSLU_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLU_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		if($rsSLU_KEY_LOESCHENZeilen>0)
		{
				echo "<span class=HinweisText>Datensatz kann nicht gel&ouml;scht werden. L&ouml;sen Sie erst die Zuordnung auf.</span>";
		}
		else
		{
				$SQL='DELETE FROM SCHLUESSELUNTERGRUPPEN WHERE SLU_KEY=0'.$_REQUEST['txtSLU_KEY_loeschen'];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
				awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
				}
		}
}

$txtSLGKEY=(isset($_REQUEST['txtSLG_KEY'])?$_REQUEST['txtSLG_KEY']:''); //Kein Unterschied ob POST oder GET
$txtSLU_SLGKEY=(isset($_REQUEST['txtSLU_SLG_KEY'])?$_REQUEST['txtSLU_SLG_KEY']:''); //Kein Unterschied ob POST oder GET
$txtSLUKEY=(isset($_REQUEST['txtSLU_KEY'])?$_REQUEST['txtSLU_KEY']:''); //Kein Unterschied ob POST oder GET

if(isset($_POST['cmdTrefferliste_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLGKEY=0;
}

if(isset($_POST['cmdTrefferlisteUG_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLUKEY=0;
}

if(isset($_POST['cmdZurueckUG_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLU_SLGKEY='';
}

if(isset($_POST['cmdSpeichern_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
		if($txtSLGKEY>0) // UPDATE-Anweisung
		{		
				//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
				//--->
		
				$SQL='SELECT * FROM SCHLUESSELGRUPPEN WHERE SLG_KEY='.$txtSLGKEY;
				$rsSchluesselgruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesselgruppenZeilen = $awisRSZeilen;
				
				$SQL='';
				$txtHinweis='';
				
				if($rsSchluesselgruppenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
				}
				
				if($_POST['txtSLG_BEZEICHNUNG'] != $_POST['txtSLG_BEZEICHNUNG_old'])
				{
						if($rsSchluesselgruppen['SLG_BEZEICHNUNG'][0] != $_POST['txtSLG_BEZEICHNUNG_old'])
						{
								$txtHinweis.=',Bezeichnung von '.$_POST['txtSLG_BEZEICHNUNG_old']. ' auf '.$rsSchluesselgruppen['SLG_BEZEICHNUNG'][0];
						}
						else
						{
								$SQL.=',SLG_BEZEICHNUNG=\''.$_POST['txtSLG_BEZEICHNUNG'].'\'';
						}
				}
				
				if($_POST['txtSLG_BEMERKUNG'] != $_POST['txtSLG_BEMERKUNG_old'])
				{
						if($rsSchluesselgruppen['SLG_BEMERKUNG'][0] != $_POST['txtSLG_BEMERKUNG_old'])
						{
								$txtHinweis.=',BEMERKUNG von '.$_POST['txtSLG_BEMERKUNG_old']. ' auf '.$rsSchluesselgruppen['SLG_BEMERKUNG'][0];
						}
						else
						{
								$SQL.=',SLG_BEMERKUNG=\''.substr($_POST['txtSLG_BEMERKUNG'],0,255).'\'';
						}
				}
				
				//<---
				
				// Update- Befehl
				
				if($txtHinweis=='' && $SQL!='')
				{
						
						$SQL.=',SLG_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						
						$SQL.=',SLG_USERDAT=SYSDATE';
				
						$SQL='UPDATE SCHLUESSELGRUPPEN SET ' .substr($SQL,1).' WHERE SLG_KEY=0'.$txtSLGKEY;
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
				}
				elseif($txtHinweis!='')
				{
						echo $txtHinweis;
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSchluesselgruppen['SLG_USER'][0] ." ge�ndert !</span>");
				}
		}
		else // INSERT-Anweisung
		{

				if($_POST['txtSLG_BEZEICHNUNG']=='')
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen eine Bezeichnung angeben!</span>");
				}
		
				$SQL='INSERT INTO SCHLUESSELGRUPPEN (SLG_BEZEICHNUNG,SLG_BEMERKUNG,SLG_USER,SLG_USERDAT) ';
				$SQL.='VALUES(\''.$_POST['txtSLG_BEZEICHNUNG'].'\',\''.substr($_POST['txtSLG_BEMERKUNG'],0,255).'\',\''.$AWISBenutzer->BenutzerName().'\',SYSDATE)';
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$rsSLG_KEY = awisOpenRecordset($con,'SELECT SEQ_SLG_KEY.CURRVAL AS KEY FROM DUAL');
				
				$txtSLGKEY=$rsSLG_KEY['KEY'][0];
		}
}

if(isset($_POST['cmdSpeichernUG_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
	awis_Debug(1,$_REQUEST);
	
		if($txtSLUKEY>0) // UPDATE-Anweisung
		{		
				//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
				//--->
		
				$SQL='SELECT * FROM SCHLUESSELUNTERGRUPPEN WHERE SLU_KEY='.$txtSLUKEY;
				$rsSchluesseluntergruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesseluntergruppenZeilen = $awisRSZeilen;
				
				$SQL='';
				$txtHinweis='';
				
				if($rsSchluesseluntergruppenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
				}
				
				if($_POST['txtSLU_BEZEICHNUNG'] != $_POST['txtSLU_BEZEICHNUNG_old'])
				{
						if($rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][0] != $_POST['txtSLU_BEZEICHNUNG_old'])
						{
								$txtHinweis.=',Bezeichnung von '.$_POST['txtSLU_BEZEICHNUNG_old']. ' auf '.$rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][0];
						}
						else
						{
								$SQL.=',SLU_BEZEICHNUNG=\''.$_POST['txtSLU_BEZEICHNUNG'].'\'';
						}
				}
				
				if($_POST['txtSLU_BEMERKUNG'] != $_POST['txtSLU_BEMERKUNG_old'])
				{
						if($rsSchluesseluntergruppen['SLU_BEMERKUNG'][0] != $_POST['txtSLU_BEMERKUNG_old'])
						{
								$txtHinweis.=',BEMERKUNG von '.$_POST['txtSLU_BEMERKUNG_old']. ' auf '.$rsSchluesseluntergruppen['SLU_BEMERKUNG'][0];
						}
						else
						{
								$SQL.=',SLU_BEMERKUNG=\''.substr($_POST['txtSLU_BEMERKUNG'],0,255).'\'';
						}
				}
				
				//<---
				
				// Update- Befehl
				
				if($txtHinweis=='' && $SQL!='')
				{
						
						$SQL.=',SLU_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						
						$SQL.=',SLU_USERDAT=SYSDATE';
				
						$SQL='UPDATE SCHLUESSELUNTERGRUPPEN SET ' .substr($SQL,1).' WHERE SLU_KEY=0'.$txtSLUKEY;
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
				}
				elseif($txtHinweis!='')
				{
						echo $txtHinweis;
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSchluesseluntergruppen['SLU_USER'][0] ." ge�ndert !</span>");
				}
		}
		else // INSERT-Anweisung
		{

				if($_POST['txtSLU_BEZEICHNUNG']=='')
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen eine Bezeichnung angeben!</span>");
				}
		
				$SQL='INSERT INTO SCHLUESSELUNTERGRUPPEN (SLU_SLG_KEY,SLU_BEZEICHNUNG,SLU_BEMERKUNG,SLU_USER,SLU_USERDAT) ';
				$SQL.='VALUES('.$_POST['txtSLU_SLG_KEY'].',\''.$_POST['txtSLU_BEZEICHNUNG'].'\',\''.substr($_POST['txtSLU_BEMERKUNG'],0,255).'\',\''.$AWISBenutzer->BenutzerName().'\',SYSDATE)';
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Gruppen.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$rsSLU_KEY = awisOpenRecordset($con,'SELECT SEQ_SLU_KEY.CURRVAL AS KEY FROM DUAL');
				
				$txtSLUKEY=$rsSLU_KEY['KEY'][0];
		}
}

if($txtSLU_SLGKEY=='' && $txtSLUKEY=='') 
{
		if($txtSLGKEY!=0 && ($Rechtestufe&2)==2) // Formular f�r die Bearbeitung der Datens�tze
		{
				$SQL='SELECT * FROM SCHLUESSELGRUPPEN WHERE SLG_KEY=0'.$txtSLGKEY;
				
				$rsSchluesselgruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesselgruppenZeilen = $awisRSZeilen;
				
				if($rsSchluesselgruppenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Es wurde kein Eintrag gefunden!</span>");
				}
				else
				{
						echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
						
						echo "<table  width=100% id=DatenTabelle border=1><tr>";
						echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferliste title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td id=FeldBez width=20%>Name</td>";
						echo "<td id=TabellenZeileGrau width=80%><input name=txtSLG_BEZEICHNUNG size=68 value='" . $rsSchluesselgruppen['SLG_BEZEICHNUNG'][0] . "'><input type=hidden	name=txtSLG_BEZEICHNUNG_old  value='" . $rsSchluesselgruppen['SLG_BEZEICHNUNG'][0] . "'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td id=FeldBez valign=top>Bemerkung</td>";
						echo "<td id=TabellenZeileGrau><textarea name=txtSLG_BEMERKUNG cols=50 rows=5>" . $rsSchluesselgruppen['SLG_BEMERKUNG'][0] . "</textarea></td><input type=hidden name=txtSLG_BEMERKUNG_old  value='" . $rsSchluesselgruppen['SLG_BEMERKUNG'][0] . "'></td>";
						echo "</tr>";
						echo "</table>";
						
						echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
						echo "<input type=hidden name=txtSLG_KEY value=".$rsSchluesselgruppen['SLG_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
						echo "</form>";
				
				}
		}
		elseif((isset($_POST['cmdHinzufuegen_x']) && $_POST['cmdHinzufuegen_x'])  && ($Rechtestufe&4)==4) // Formular f�r das Hinzuf�gen von Datens�tzen
		{
				echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
				
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferliste title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez width=20%>Name</td>";
				echo "<td id=TabellenZeileGrau width=80%><input name=txtSLG_BEZEICHNUNG size=68></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez valign=top>Bemerkung</td>";
				echo "<td id=TabellenZeileGrau><textarea name=txtSLG_BEMERKUNG cols=50 rows=5></textarea></td>";
				echo "</tr>";
				echo "</table>";
							
				echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
				echo "<input type=hidden name=txtSLG_KEY value=0>";
				echo "</form>";
		}
		else // Formular f�r die Ausgabe der Gruppen-Liste
		{
		
				echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
				
				if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
				{
						echo "<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf�gen (Alt+N)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen'>";
						echo "<hr>";
				}
				
				$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
				
				$SQL='SELECT * FROM SCHLUESSELGRUPPEN ORDER BY SLG_BEZEICHNUNG ASC';
				
				$rsSchluesselgruppen='';
				$rsSchluesselgruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesselgruppenZeilen = $awisRSZeilen;
				
				if($rsSchluesselgruppenZeilen==0)		// Keine Daten
				{
					echo "<br><span class=HinweisText>Es wurde kein Eintrag gefunden!</span>";
				}
				else
				{
					// �berschrift aufbauen
					echo "<table  width=100% id=DatenTabelle border=1><tr>";
					
					echo "<td id=FeldBez>Name</td>";
					echo "<td id=FeldBez>Bemerkung</td>";
					echo "<td id=FeldBez>Untergruppen</td>";
					if(($Rechtestufe&8)==8){
						echo "<td id=FeldBez></td>";
				   }
					echo "</tr>";
					
					for($i=0;$i<$rsSchluesselgruppenZeilen;$i++)
					{
						echo '<tr>';
						if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
						{
								echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=./schluessel_Main.php?cmdAktion=Gruppen&txtSLG_KEY=' . $rsSchluesselgruppen['SLG_KEY'][$i] . '>' . $rsSchluesselgruppen['SLG_BEZEICHNUNG'][$i] . '</a></td>';
						}
						else
						{
								echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselgruppen['SLG_BEZEICHNUNG'][$i] . '</td>';
						}
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselgruppen['SLG_BEMERKUNG'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=./schluessel_Main.php?cmdAktion=Gruppen&txtSLU_SLG_KEY=' . $rsSchluesselgruppen['SLG_KEY'][$i] . '>Anzeigen</a></td>';
						if(($Rechtestufe&8)==8){
								echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./schluessel_Main.php?cmdAktion=Gruppen&txtSLG_KEY_loeschen=". $rsSchluesselgruppen['SLG_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
						}
						echo '</tr>';
						
					}
						
					print "</table>";
					
					if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
					{
						echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
					}
					else
					{
					echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
					}
				}
				
				echo '</form>';
		}
}
else
{
		if($txtSLUKEY!=0 && ($Rechtestufe&2)==2) // Formular f�r die Bearbeitung der Datens�tze
		{
				$SQL='SELECT * FROM SCHLUESSELUNTERGRUPPEN WHERE SLU_KEY=0'.$txtSLUKEY;
				$SQL.=' ORDER BY SLU_BEZEICHNUNG ASC';
				
				$rsSchluesseluntergruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesseluntergruppenZeilen = $awisRSZeilen;
				
				if($rsSchluesseluntergruppenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Es wurde kein Eintrag gefunden!</span>");
				}
				else
				{
						echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
						
						echo "<table  width=100% id=DatenTabelle border=1><tr>";
						echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferlisteUG title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td id=FeldBez width=20%>Name</td>";
						echo "<td id=TabellenZeileGrau width=80%><input name=txtSLU_BEZEICHNUNG size=68 value='" . $rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][0] . "'><input type=hidden	name=txtSLU_BEZEICHNUNG_old  value='" . $rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][0] . "'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td id=FeldBez valign=top>Bemerkung</td>";
						echo "<td id=TabellenZeileGrau><textarea name=txtSLU_BEMERKUNG cols=50 rows=5>" . $rsSchluesseluntergruppen['SLU_BEMERKUNG'][0] . "</textarea></td><input type=hidden name=txtSLU_BEMERKUNG_old  value='" . (isset($rsSchluesseluntergruppen['SLG_BEMERKUNG'][0])?$rsSchluesseluntergruppen['SLG_BEMERKUNG'][0]:'') . "'></td>";
						echo "</tr>";
						echo "</table>";
						
						echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichernUG>";
						echo "<input type=hidden name=txtSLU_KEY value=".$rsSchluesseluntergruppen['SLU_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
						echo "<input type=hidden name=txtSLU_SLG_KEY value=".$rsSchluesseluntergruppen['SLU_SLG_KEY'][0].">";
						echo "</form>";
				}
		}
		elseif((isset($_POST['cmdHinzufuegenUG_x']) && $_POST['cmdHinzufuegenUG_x'])  && ($Rechtestufe&4)==4) // Formular f�r das Hinzuf�gen von Datens�tzen
		{
				echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
				
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferlisteUG title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez width=20%>Name</td>";
				echo "<td id=TabellenZeileGrau width=80%><input name=txtSLU_BEZEICHNUNG size=68></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez valign=top>Bemerkung</td>";
				echo "<td id=TabellenZeileGrau><textarea name=txtSLU_BEMERKUNG cols=50 rows=5></textarea></td>";
				echo "</tr>";
				echo "</table>";
							
				echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichernUG>";
				echo "<input type=hidden name=txtSLU_KEY value=0>";
				echo "<input type=hidden name=txtSLU_SLG_KEY value=".$_REQUEST['txtSLU_SLG_KEY'].">";
				echo "</form>";
		}
		else // Formular f�r die Ausgabe der Gruppen-Liste
		{
		
				echo '<form name=frmGruppen method=post action=./schluessel_Main.php?cmdAktion=Gruppen>';
				
				if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
				{
						echo "<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegenUG title='Hinzuf�gen (Alt+N)' onclick=location.href='./schluessel_Main.php?cmdAktion=Gruppen&txtSLU_SLG_KEY=0".$txtSLU_SLGKEY."'>";
						echo "<input type=hidden name=txtSLU_SLG_KEY value=".$_REQUEST['txtSLU_SLG_KEY'].">";	
				}
				
				$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
				
				$SQL='SELECT * FROM SCHLUESSELUNTERGRUPPEN WHERE SLU_SLG_KEY='.$txtSLU_SLGKEY;
				$SQL.=' ORDER BY SLU_BEZEICHNUNG ASC';
				
				$rsSchluesseluntergruppen = awisOpenRecordset($con,$SQL);
				$rsSchluesseluntergruppenZeilen = $awisRSZeilen;
				
				if($rsSchluesseluntergruppenZeilen==0)		// Keine Daten
				{
					echo "<br><span class=HinweisText>Es wurde kein Eintrag gefunden!</span>";
				}
				else
				{
					// �berschrift aufbauen
					echo "<table  width=100% id=DatenTabelle border=1><tr>";
					
					echo "<td id=FeldBez>Name</td>";
					echo "<td id=FeldBez>Bemerkung</td>";
					if(($Rechtestufe&8)==8){
						echo "<td id=FeldBez></td>";
				   }
					
					echo "</tr>";
					
					for($i=0;$i<$rsSchluesseluntergruppenZeilen;$i++)
					{
						echo '<tr>';
						if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
						{
								echo '<td id=TabellenZeileGrau><a href=./schluessel_Main.php?cmdAktion=Gruppen&txtSLU_KEY=' . $rsSchluesseluntergruppen['SLU_KEY'][$i] . '>' . $rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][$i] . '</a></td>';
						}
						else
						{
								echo '<td id=TabellenZeileGrau>' . $rsSchluesseluntergruppen['SLU_BEZEICHNUNG'][$i] . '</td>';
						}
						echo '<td id=TabellenZeileGrau>' . $rsSchluesseluntergruppen['SLU_BEMERKUNG'][$i] . '</td>';
						if(($Rechtestufe&8)==8){
								echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./schluessel_Main.php?cmdAktion=Gruppen&txtSLU_KEY_loeschen=". $rsSchluesseluntergruppen['SLU_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
						}
						echo '</tr>';
						
					}
						
					print "</table>";
					
					if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
					{
						echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
					}
					else
					{
					echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
					}
				}
				
				//print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueckUG onclick=location.href='./schluessel_Main.php&cmdAktion=Gruppen';>";
				print "<br><hr><input type=image alt=Zur�ck src=/bilder/sc_lupe.png name=cmdZurueckUG onclick=location.href='./schluessel_Main.php&cmdAktion=Gruppen';>";
				
				echo '</form>';
		}
}

?>
</body>
</html>
