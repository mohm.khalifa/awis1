<?php
require_once 'awisArtikelstammLieferantenartikelFunktionen.inc';
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('LAR', '%');
$TextKonserven[] = array('LIE', 'LIE_NAME1');
$TextKonserven[] = array('LIE', 'LieferantenSperre_%');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
    $SchnellanlagePos = $AWISBenutzer->ParameterLesen('Artikelstamm_SchnellAnlagePosition');


    $Recht452 = $AWISBenutzer->HatDasRecht(452);        // Recht f�r Lieferantenartikel
    if ($Recht452 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if ($BildschirmBreite < 1024) {
        $FeldBreiten['LIE_NAME1'] = 105;
        $FeldBreiten['LIE_NAME1_ZEICHEN'] = 8;

        if (($Recht452 & 1024) != 1024) {
            $FeldBreiten['LIE_NAME1'] = 255;
            $FeldBreiten['LIE_NAME1_ZEICHEN'] = 22;
        }

        $FeldBreiten['LAR_LARTNR'] = 100;
        $FeldBreiten['LAR_LARTNR_ZEICHEN'] = 9;
        $FeldBreiten['ZUORDNUNG'] = 70;
    } elseif ($BildschirmBreite < 1280) {
        $FeldBreiten['LIE_NAME1'] = 195;
        $FeldBreiten['LIE_NAME1_ZEICHEN'] = 20;
        $FeldBreiten['LAR_LARTNR'] = 200;
        $FeldBreiten['LAR_LARTNR_ZEICHEN'] = 17;
        $FeldBreiten['ZUORDNUNG'] = 100;
    } else {
        $FeldBreiten['LIE_NAME1'] = 395;
        $FeldBreiten['LIE_NAME1_ZEICHEN'] = 36;
        $FeldBreiten['LAR_LARTNR'] = 220;
        $FeldBreiten['LAR_LARTNR_ZEICHEN'] = 19;
        $FeldBreiten['ZUORDNUNG'] = 100;
    }

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht451 = $AWISBenutzer->HatDasRecht(451);        // Sonderanzeigen

    $MitReklInfo = $AWISBenutzer->ParameterLesen("ReklamationsInfoLieferanten");
    $MitProblemartikeln = $AWISBenutzer->ParameterLesen("Lieferantenartikel: Problemartikel anzeigen");
    $MitImportartikeln = $AWISBenutzer->ParameterLesen("Lieferantenartikel mit Zuordnung Import anzeigen");

    if (($Recht452 & 512) and isset($_GET['TEI'])) {
        $SQL = 'UPDATE teileinfos';
        $SQL .= ' SET TEI_USER = ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
        $SQL .= ' , TEI_USERDAT=SYSDATE';
        $SQL .= ' WHERE TEI_KEY = 0' . $DB->FeldInhaltFormat('N0', $_GET['TEI']);

        $DB->Ausfuehren($SQL);
    }

    $AWISWerkzeug = new awisWerkzeuge();

    $Bedingung = '';

    $BindeVariablen = array();
    $BindeVariablen['var_N0_ali_ast_key'] = $AWIS_KEY1;
    $BindeVariablen['var_N0_tei_key1'] = $AWIS_KEY1;

    $SQL = 'SELECT * FROM (';
    $SQL .= 'SELECT Lieferantenartikel.*, Lieferanten.*, TEI_USER, TEI_USERDAT, TEI_KEY';
    $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 1 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=:var_N0_ali_ast_key AND ROWNUM=1) AS ALI_WERT_1';
    $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 2 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=:var_N0_ali_ast_key AND ROWNUM=1) AS ALI_WERT_2';
    $SQL .= ', (SELECT LIN_WERT FROM LieferantenInfos WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY = 17) AS LIESPERRE';
    $SQL .= ', (SELECT listagg(LIN_WERT,\', \') within group (order by LIN_LIE_NR, LIN_ITY_KEY) as LIN_WERT  FROM LieferantenInfos WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY = 317) AS LIN_TECDOCNR';
    $SQL .= ', (SELECT LAI_WERT FROM Lieferantenartikelinfos WHERE LAI_LIT_ID = 1 AND LAI_LAR_KEY = LAR_KEY AND ROWNUM=1) AS LARTNRSPERRE';
    $SQL .= ', (SELECT TII_WERT FROM TEILEINFOSINFOS WHERE TII_ITY_KEY = 318 AND TII_TEI_KEY = TEI_KEY AND ROWNUM=1) AS LARTNRSMARTKATEXPORT';
    $SQL .= ' FROM Lieferantenartikel';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY2 = LAR_KEY and TEI_KEY1 = :var_N0_tei_key1';
    $SQL .= ' LEFT OUTER JOIN Lieferanten ON LAR_LIE_NR = LIE_NR ';
    $SQL .= ' LEFT OUTER JOIN ArtikelPruefPersonal ON APP_ID = LAR_APP_ID';
    $SQL .= ' )';

    if (($Recht451 & 1) != 1) {
        $Bedingung .= ' AND TEI_USER=\'WWS\'';
    }

    // Artikel von gesperrten Lieferanten nicht anzeigen
    if (($Recht452 & 2048) != 2048) {
        $Bedingung .= ' AND (SELECT LIN_WERT FROM LieferantenInfos WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY = 17) IS NULL';
        $Bedingung .= ' AND (LARTNRSPERRE IS NULL OR LARTNRSPERRE = 0)';
    }

    if (isset($_GET['Unterseite'])) {
        $AWIS_KEY2 = $AWISBenutzer->ParameterLesen('AktuellerLAR');
    }
    if (isset($_GET['LAR_KEY'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('Z', $_GET['LAR_KEY'], false);
        $BindeVariablen['var_N0_lar_key'] = $AWIS_KEY2;
        $Bedingung .= ' AND LAR_KEY = :var_N0_lar_key';
    }
    if (isset($_POST['txtLARKey'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('Z', $_POST['txtLARKey'], false);
        $BindeVariablen['var_N0_lar_key'] = $AWIS_KEY2;
        $Bedingung .= ' AND LAR_KEY = :var_N0_lar_key';
    }
    if ($AWIS_KEY2 != 0) {
        $BindeVariablen['var_N0_lar_key'] = $AWIS_KEY2;
        $Bedingung .= ' AND LAR_KEY = :var_N0_lar_key';
    }

    //********************************************************************************************
    //* Ausblenden von bestimmten Artikeln
    //********************************************************************************************
    if (($Recht452 & 32) != 32)            // Problemartikel anzeigen
    {
        $Bedingung .= ' AND LAR_PROBLEMARTIKEL = 0';
    }

    if ($MitImportartikeln == 0) {
        $Bedingung .= ' AND TEI_USER!=\'Import\'';
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['LARSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['LARSort']) . ', lar_lartnr, CASE WHEN TEI_USER = \'WWS\' THEN 2 ELSE 1 END';
    } else {
        $SQL .= ' ORDER BY  lar_lie_nr, lar_lartnr, CASE WHEN TEI_USER = \'WWS\' THEN 2 ELSE 1 END';
    }

    $Form->DebugAusgabe(1, $SQL, $AWIS_KEY1, $AWIS_KEY2);

    $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

    if (!isset($_GET['LAR_KEY']) AND $AWIS_KEY2 == 0)                    // Liste anzeigen
    {

        $Form->ZeileStart();

        if (($Recht452 & (2 + 4 + 8 + 64)) != 0)    // Kein �ndernrecht
        {
            $Icons = array();
            if (($Recht452 & 4)) {
                $Icons[] = array(
                    'new',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0',
                    'g',
                    $AWISSprachKonserven['LAR']['wrd_LieferantenartikelHinzufuegen']
                );
            }
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel';
        $Link .= '&LARSort=LAR_LARTNR' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LAR_LARTNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_LARTNR'], $FeldBreiten['LAR_LARTNR'], '', $Link);

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel';
        $Link .= '&LARSort=LAR_LIE_NR' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LAR_LIE_NR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_LIE_NR'], 90, '', $Link);
        if (($Recht452 & 8192) == 8192) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_LARTNRSMARTKATEXPORT'], 30, '', '', $AWISSprachKonserven['LAR']['LAR_LARTNRSMARTKATEXPORT_TTT'],
                'C');
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel';
        $Link .= '&LARSort=LIE_NAME1' . ((isset($_GET['LARSort']) AND ($_GET['LARSort'] == 'LIE_NAME1'))?'~':'');
        $Daten = (strlen($AWISSprachKonserven['LIE']['LIE_NAME1']) > $FeldBreiten['LIE_NAME1_ZEICHEN'] - 2?substr($AWISSprachKonserven['LIE']['LIE_NAME1'], 0,
                $FeldBreiten['LIE_NAME1_ZEICHEN'] - 2) . '...':$AWISSprachKonserven['LIE']['LIE_NAME1']);
        $Form->Erstelle_Liste_Ueberschrift($Daten, $FeldBreiten['LIE_NAME1'], '', $Link);

        if (($Recht452 & 1024) == 1024) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_LARTeileInfo'], 150);
        }
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_Aenderungsdatum'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['txt_LARZuordnung'], $FeldBreiten['ZUORDNUNG']);

        if (($Recht452 & 16)) {
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAR']['LAR_REKLAMATIONSKENNUNG'], 40);
        }

        $Form->ZeileEnde();

        $DS = 0;
        $rsLARZeilen = $rsLAR->AnzahlDatensaetze();
        $rsLAR = $rsLAR->Tabelle();

        $BindeVariablen = array();
        $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;
        $rsAST = $DB->RecordSetOeffnen('SELECT AST_ATUNR FROM Artikelstamm WHERE AST_KEY = :var_N0_ast_key', $BindeVariablen);
        $HauptLieferant = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 40, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
        $HauptLieferantenArtikel = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 41, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
        if($SchnellanlagePos==1){
            _Schnellanlage($Form,$Recht452,$FeldBreiten);
        }
        for ($LARZeile = 0; $LARZeile < $rsLARZeilen; $LARZeile++) {
            $ZweitArtikel = false;
            $LAR = array();
            $LAR[0] = array($rsLAR['TEI_USER'][$LARZeile], $rsLAR['TEI_USERDAT'][$LARZeile], $rsLAR['TEI_KEY'][$LARZeile], $rsLAR['LARTNRSMARTKATEXPORT'][$LARZeile]);
            $LAR[1] = array_fill(0, 4, '');
            // Zwei gleiche Artikel, Nur die Benutzer sind unterschiedlich
            if (isset($rsLAR["LAR_LARTNR"][$LARZeile + 1]) AND strcmp($rsLAR["LAR_LARTNR"][$LARZeile],
                    $rsLAR["LAR_LARTNR"][$LARZeile + 1]) == 0 AND $rsLAR['LAR_LIE_NR'][$LARZeile] == $rsLAR['LAR_LIE_NR'][$LARZeile + 1]
            ) {
                $LAR[($rsLAR['TEI_USER'][$LARZeile] == 'WWS')?0:1] = array($rsLAR['TEI_USER'][$LARZeile], $rsLAR['TEI_USERDAT'][$LARZeile], $rsLAR['TEI_KEY'][$LARZeile], $rsLAR['LARTNRSMARTKATEXPORT'][$LARZeile]);
                $LAR[($rsLAR['TEI_USER'][$LARZeile] == 'WWS')?0:1] = array(
                    $rsLAR['TEI_USER'][$LARZeile + 1],
                    $rsLAR['TEI_USERDAT'][$LARZeile + 1],
                    $rsLAR['TEI_KEY'][$LARZeile + 1],
                    $rsLAR['LARTNRSMARTKATEXPORT'][$LARZeile + 1]
                );

                $ZweitArtikel = true;    // Kennzeichnen, dass es zwei Zuordnungen gibt
                $LARZeile++;
            }

            $ArtikelAnzeigen = true;
            if ((($rsLAR['ALI_WERT_2'][$LARZeile] != '' OR $rsLAR['LAR_PROBLEMARTIKEL'][$LARZeile] != 0) AND $MitProblemartikeln == 0)) {
                $ArtikelAnzeigen = false;
            }
            $LARDELKEY = '';
            // Soll der Artikel angezeigt werden?
            if ($ArtikelAnzeigen) {
                $Form->ZeileStart();

                if (($Recht452 & (2 + 4 + 8 + 64)) != 0)    // Kein �ndernrecht
                {
                    $Icons = array();
                    if (($Recht452 & (2 + 4 + 64)) != 0)    // �ndernrecht
                    {
                        $Icons[] = array('edit', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=' . $rsLAR['LAR_KEY'][$LARZeile]);
                    }
                    if (($Recht452 & 8))    // �ndernrecht
                    {

                        if ($LAR[0][0] != 'WWS' OR $LAR[1][0] != '')        // WWS Daten k�nnen nicht gel�scht werden
                        {
                            if ($LAR[0][0] != 'WWS') {
                                if ($LAR[0][2] == 'XXX') {
                                    $LARDELKEY = $LAR[0][2];
                                } elseif ($LAR[1][2] == 'XXX') {
                                    $LARDELKEY = $LAR[1][2];
                                } else {
                                    $LARDELKEY = $LAR[0][2];
                                }
                            } else {
                                $LARDELKEY = $LAR[1][2];
                            }
                        }
                    }
                    $Form->Erstelle_ListeIcons($Icons, 19, ($DS % 2));
                    if ($LARDELKEY != '') {
                        $Form->Erstelle_ListenLoeschPopUp('./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LartPopUpDel=' . $LARDELKEY . '&Key=' . $AWIS_KEY1,
                            $LARDELKEY, 19, '', '', ($DS % 2));
                    } else {
                        $Form->Erstelle_ListenFeld('', '', 0, 19, false, ($DS % 2));
                    }
                }

                $StyleLARTNR = '';
                if ($rsLAR['LARTNRSPERRE'][$LARZeile] == '1') {
                    $StyleLARTNR = 'color:red;';
                }

                // Lieferantenset suchen
                $BindeVariablen = array();
                $BindeVariablen['var_N0_lar_key'] = $rsLAR['LAR_KEY'][$LARZeile];

                $SQL = 'SELECT COUNT(T1.TEI_SUCH1) AS Teile';
                $SQL .= " FROM TeileInfos T1 ";
                $SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
                $SQL .= " AND T1.TEI_KEY2 = :var_N0_lar_key";

                $rsLAS = $DB->RecordsetOeffnen($SQL, $BindeVariablen);
                if ($rsLAS->FeldInhalt('TEILE') > 0) {
                    $ToolTipp = str_replace('$1', $rsLAS->FeldInhalt('TEILE'), $AWISSprachKonserven['LAR']['ttt_SetBestehendAus']);
                    $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAR['LAR_LARTNR'][$LARZeile], 20, $FeldBreiten['LAR_LARTNR'] - 30, false,
                        ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] == 1?2:($DS % 2)), 'font-style:italic;' . ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] != 1?$StyleLARTNR:''), '', 'T',
                        'L', $ToolTipp);
                    $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAS->FeldInhalt('TEILE'), 2, 30, false, ($DS % 2), '', '', 'T', 'L', $ToolTipp);
                } else {
                    $Form->Erstelle_ListenFeld('*LAR_LARTNR', $rsLAR['LAR_LARTNR'][$LARZeile], 22, $FeldBreiten['LAR_LARTNR'], false,
                        ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] == 1?2:($DS % 2)), ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] != 1?$StyleLARTNR:''), '', 'T');
                }

                $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $rsLAR['LAR_LIE_NR'][$LARZeile];
                $Hintergrund = (($HauptLieferant[0]['Wert'] == $rsLAR["LAR_LIE_NR"][$LARZeile] AND $HauptLieferantenArtikel[0]['Wert'] == $rsLAR['LAR_LARTNR'][$LARZeile])?3:($DS % 2));
                $Style = '';
                $ToolTipp = '';
                if ($rsLAR['LIESPERRE'][$LARZeile] != '') {
                    $Style = 'color:red;';
                    $ToolTipp = $AWISSprachKonserven['LIE']['LieferantenSperre_' . $rsLAR['LIESPERRE'][$LARZeile]];
                }

                if ($rsLAR['LIN_TECDOCNR'][$LARZeile] == '') {
                    $Style .= strpos($Style,'background-color')===false?'background-color:orange; ':'';
                    $ToolTipp = explode('.', $Form->LadeTextBaustein('LAR', 'LAR_HINWEIS_KEIN_TECDOC'))[0] . '. ' . PHP_EOL . $ToolTipp;
                } else {
                    $ToolTipp = $Form->LadeTextBaustein('LIN', 'LIN_TecDocNr') . ': ' . $rsLAR['LIN_TECDOCNR'][$LARZeile] . ' ' . $ToolTipp;
                }

                $Form->Erstelle_ListenFeld('*LAR_LIE_NR', $rsLAR['LAR_LIE_NR'][$LARZeile], 10, 90, false, $Hintergrund, $Style . $StyleLARTNR, '', 'T', '', $ToolTipp);
                $Daten = (strlen($rsLAR['LIE_NAME1'][$LARZeile]) > $FeldBreiten['LIE_NAME1_ZEICHEN'] - 2?substr($rsLAR['LIE_NAME1'][$LARZeile], 0,
                        $FeldBreiten['LIE_NAME1_ZEICHEN'] - 2) . '...':$rsLAR['LIE_NAME1'][$LARZeile]);

                if (($Recht452 & 8192) == 8192) {
                    // PG 27.10.2017: Wenn ein Artikel im AWIS entsteht und danach von der VAX kommt, gibt es zwei Teileinfos.
                    // Deswegen analog zur Zuordnungsanzeige auch beim Smartkat beide Infos beachten.
                    $LARTNRSMARTKATEXPORT = ($LAR[0][3] OR $LAR[1][3]);
                    $Form->Erstelle_ListenCheckbox('LARTNRSMARTKATEXPORT_' . $rsLAR['TEI_KEY'][$LARZeile], $LARTNRSMARTKATEXPORT, 30,
                        (($Recht452 & 16384) == 16384), 1, '', '', '', '', ($DS % 2));
                }

                $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $rsLAR['LAR_LIE_NR'][$LARZeile];
                $Form->Erstelle_ListenFeld('*LIE_NAME1', $Daten, 10, $FeldBreiten['LIE_NAME1'], false, ($DS % 2), '', $Link, 'T', '', $rsLAR['LIE_NAME1'][$LARZeile]);

                if (($Recht452 & 1024) == 1024) {
                    // Zusatzinfo in Kurzform
                    $Anzeige = '';
                    if ($rsLAR["LAR_ALTERNATIVARTIKEL"][$LARZeile] != 0) {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_AKT'];
                    }
                    if ($rsLAR["LAR_AUSLAUFARTIKEL"][$LARZeile] != 0) {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_AUSL'];
                    }

                    if ($rsLAR["LAR_PROBLEMARTIKEL"][$LARZeile] != 0) {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_PROB'];
                    }
                    if ($rsLAR["LAR_GEPRUEFT"][$LARZeile] != 0) {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_GEPR'];
                    }
                    if ($rsLAR["LAR_MUSTER"][$LARZeile] != 0) {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_M'];
                    }
                    if ($rsLAR["LAR_BEMERKUNGEN"][$LARZeile] . '' != '') {
                        $Anzeige .= ', ' . $AWISSprachKonserven['LAR']['txt_LARTeileInfo_B'];
                    }
                    $Form->Erstelle_ListenFeld('*ZUSATZ', substr($Anzeige, 2), 10, 150, false, ($DS % 2), '', '', 'T', '', substr($Anzeige, 2));
                }

                $Link = false;
                if (($Recht452 & 512) and $LAR[0][0] == 'Import') {
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&TEI=' . $LAR[0][2];
                } elseif (($Recht452 & 512) AND $LAR[1][0] == 'Import') {
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&TEI=' . $LAR[1][2];
                }
                $UserNamenTooltipp = ($LAR[1][1] == ''?$LAR[0][1]:$LAR[0][1] . '/' . $LAR[1][1]);
                $Form->Erstelle_ListenFeld('*TEI_USERDAT', ($LAR[1][1] == ''?$LAR[0][1]:$LAR[1][1]), 10, 100, false, ($DS % 2), '', '', 'D', '', $UserNamenTooltipp);
                // Benutzernamen abk�rzen, damit es in die Liste passt. Den vollst�ndigen Namen als ToolTipp
                $UserNamenTooltipp = ($LAR[0][0] . ($LAR[1][0] == ''?'':'/' . $LAR[1][0]));
                $UserNamen = ($LAR[0][0] . (substr($LAR[1][0], 0, 5) == ''?'':'/' . substr($LAR[1][0], 0, 5)));
                $Form->Erstelle_ListenFeld('*USER', $UserNamen, 10, $FeldBreiten['ZUORDNUNG'], false, ($DS % 2), '', $Link, 'T', '', $UserNamenTooltipp);

                if (($Recht452 & 16)) {
                    $Form->Erstelle_ListenFeld('*ALI_WERT_2', $rsLAR['ALI_WERT_2'][$LARZeile], 10, 40, false, ($DS % 2), 'color:#FF0000', '', 'T', 'C',
                        $AWISSprachKonserven['LAR']['ttt_LAR_REKLAMATIONSKENNUNG']);
                }

                $Form->ZeileEnde();
                $DS++;
            }
        }

        if($SchnellanlagePos==0){
            _Schnellanlage($Form,$Recht452,$FeldBreiten);
        }
    } else        // Einer oder keiner
    {

        echo '<input name=txtLAR_KEY type=hidden value=0' . ($rsLAR->FeldInhalt('LAR_KEY') != ''?$rsLAR->FeldInhalt('LAR_KEY'):'') . '>';
        // Importquelle muss bei der �nderung unbedingt eine 4 bekommen!
        echo '<input name=txtLAR_IMQ_ID type=hidden value=' . (($rsLAR->FeldInhalt('LAR_IMQ_ID') != '')?(intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) | 4):'4') . '>';
        echo '<input name=oldLAR_IMQ_ID type=hidden value=' . (($rsLAR->FeldInhalt('LAR_IMQ_ID') != '')?$rsLAR->FeldInhalt('LAR_IMQ_ID'):'0') . '>';

        $AWIS_KEY2 = $rsLAR->FeldInhalt('LAR_KEY');
        $AWISBenutzer->ParameterSchreiben('AktuellerLAR', $AWIS_KEY2);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LARListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLAR->FeldInhalt('LAR_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLAR->FeldInhalt('LAR_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);

        $rsLAR->DSWeiter();
        $NaechsterLAR = $rsLAR->Datensatz();
        if ($rsLAR->EOF()) {
            $rsLAR->DSLetzter();
        } else {
            $rsLAR->DSZurueck();
        }
        $EditModus = ($NaechsterLAR['TEI_USER'] != ''?false:($rsLAR->FeldInhalt('TEI_USER') != 'WWS'?($Recht452 & 6):false));
        $EditModus2 = ($Recht452 & 6);           // Berbeiten oder Hinzuf�gen
        $EditModus3 = ($Recht452 & 8192);        // Recht NUR den Auslaufartikel zu setzen

        ob_start();
        $Hinweis = new awisArtikelstammLieferantenartikelFunktionen();
        $Hinweis->TecDocHinweis($rsLAR->FeldInhalt('LAR_LIE_NR'));

        $Form->AuswahlBoxHuelle('TecDocHinweis', '', '', ob_get_clean());

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_LARTNR'] . ':', 150);
        $Form->Erstelle_TextFeld('LAR_LARTNR', ($rsLAR->FeldInhalt('LAR_LARTNR') != ''?$rsLAR->FeldInhalt('LAR_LARTNR'):''), 20, 200,
            ($rsLAR->FeldInhalt('LAR_IMQ_ID') != ''?((intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) & 2) == 0?$EditModus:false):true));
        $AWISCursorPosition = ($EditModus?'txtLAR_LARTNR':$AWISCursorPosition);
        $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . ($rsLAR->FeldInhalt('LAR_LIE_NR'));
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_LIE_NR'] . ':', 120, '', '');
        $SQL = 'SELECT LIE_NR, LIE_NR || \' - \' || LIE_NAME1 AS Anzeige FROM Lieferanten ORDER BY LIE_NAME1';
        $Form->Erstelle_AutocompleteFeld('LAR_LIE_NR', isset($_REQUEST['txtLAR_LIE_NR'])?$_REQUEST['txtLAR_LIE_NR']:$rsLAR->FeldInhalt('LAR_LIE_NR'), 90, true, 'autocomplete_LIE_NR', 1, '', '', '', '', 'ListenFeldDunkel',
            'onchange="key_TecDocHinweis(this);"',array('txtWGR_ID_Vorschlag','txtWUG_ID_Vorschlag'));
        $Form->AuswahlBox('TecDocHinweis','TecDocHinweis','','TecDocHinweis','txtLAR_LIE_NR',1,1,$rsLAR->FeldInhalt('LAR_LIE_NR'),'T',true,'','','','','','','',0,'display:none');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEMERKUNGEN'] . ':', 150);
        $Form->Erstelle_Textarea('LAR_BEMERKUNGEN', $rsLAR->FeldInhalt('LAR_BEMERKUNGEN'), 0, 100, 4, ($EditModus | $EditModus2 | $EditModus3));
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_REKLAMATIONSKENNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('ALI_WERT_2', $rsLAR->FeldInhalt('ALI_WERT_2'), 2, 50, $EditModus);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_ALTERNATIVARTIKEL'] . ':', 150, 'text-align:right');
        $Form->Erstelle_SelectFeld('LAR_ALTERNATIVARTIKEL', $rsLAR->FeldInhalt('LAR_ALTERNATIVARTIKEL'), 70, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_AUSLAUFARTIKEL'] . ':', 150, 'text-align:right');
        $Form->Erstelle_SelectFeld('LAR_AUSLAUFARTIKEL', $rsLAR->FeldInhalt('LAR_AUSLAUFARTIKEL'), 70, ($EditModus | $EditModus2 | $EditModus3), '', '', '0', '', '', $JaNeinFeld);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEKANNTWW'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAR_BEKANNTWW', ($rsLAR->FeldInhalt('LAR_IMQ_ID') != ''?((intval($rsLAR->FeldInhalt('LAR_IMQ_ID')) & 2) == 0?'0':'1'):''), 0, false, '', '', '',
            '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_MUSTER'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAR_MUSTER', $rsLAR->FeldInhalt('LAR_MUSTER'), 50, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);

        //***********************************************************
        //Sperre
        //***********************************************************
        if ($Recht452 & 4096) {
            $BindeVariablen = array();
            $BindeVariablen['var_N0_lai_lar_key'] = $AWIS_KEY2;

            $SQL = 'SELECT LAI_WERT, LAI_KEY';
            $SQL .= ' FROM Lieferantenartikelinfostypen';
            $SQL .= ' LEFT OUTER JOIN Lieferantenartikelinfos ON LIT_ID = LAI_LIT_ID AND LAI_LAR_KEY=:var_N0_lai_lar_key';
            $SQL .= ' WHERE LIT_ID = 1';
            $rsLAI = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

            $Erg = array(array('Key' => 0, 'Wert' => ''));    // Defaultarray ist leer
            $Elemente = 0;
            if (!$rsLAI->EOF()) {
                $Wert = $Form->Format('T', ($rsLAI->FeldInhalt('LAI_WERT') != ''?$rsLAI->FeldInhalt('LAI_WERT'):''));
                $Erg[$Elemente++] = array('Key' => ($rsLAI->FeldInhalt('LAI_KEY') != ''?$rsLAI->FeldInhalt('LAI_KEY'):'0'), 'Wert' => $Wert);
            }
            $Wert = $Erg;
            $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
            $Form->Erstelle_TextLabel('Gesperrt:', 150,'text-align: right');
            $Form->Erstelle_SelectFeld('LAI_1_' . $Wert[0]['Key'], $Wert[0]['Wert'], 70, ($Recht452 & 4096), '', '', '0', '', '', $JaNeinFeld);
        }
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_GEPRUEFT'] . ':', 150);
        $Form->Erstelle_TextFeld('LAR_GEPRUEFT', $rsLAR->FeldInhalt('LAR_GEPRUEFT'), 10, 150, ($EditModus | $EditModus2), '', '', '', 'D');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_APP_ID'] . ':', 150, 'text-align:right');
        $SQL = 'SELECT APP_ID, APP_NAME FROM Artikelpruefpersonal';
        $SQL .= ' WHERE (app_id = 0' . ($rsLAR->FeldInhalt('LAR_APP_ID')) . ' OR app_aktivbis > SYSDATE)';
        $SQL .= ' ORDER BY app_name';
        $Form->Erstelle_SelectFeld('LAR_APP_ID', $rsLAR->FeldInhalt('LAR_APP_ID'), 0, ($EditModus | $EditModus2), $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);

        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_BEZWW'] . ':', 150, '', '', ($rsLAR->FeldInhalt('ALI_WERT_1') != ''?$rsLAR->FeldInhalt('ALI_WERT_1'):''));
        $Form->Erstelle_TextFeld('LAR_BEZWW', (($rsLAR->FeldInhalt('LAR_BEZWW') != '')?$rsLAR->FeldInhalt('LAR_BEZWW'):''), 80, 0, ($EditModus | $EditModus2), '', '', '', 'T', 'L',
            $AWISSprachKonserven['LAR']['ttt_LAR_BEZWW'], '', 50);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_PROBLEMARTIKEL'] . ':', 150);
        $Form->Erstelle_SelectFeld('LAR_PROBLEMARTIKEL', $rsLAR->FeldInhalt('LAR_PROBLEMARTIKEL'), 70, ($EditModus | $EditModus2), '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAR']['LAR_PROBLEMBESCHREIBUNG'] . ':', 150);
        $Form->Erstelle_Textarea('LAR_PROBLEMBESCHREIBUNG', $rsLAR->FeldInhalt('LAR_PROBLEMBESCHREIBUNG'), 0, 100, 4, ($EditModus | $EditModus2), 'color:#FF0000');
        $Form->ZeileEnde();

        if ($rsLAR->FeldInhalt('LAR_KEY') != '') {
            $Register = new awisRegister(452);
            $Register->ZeichneRegister((isset($_GET['Unterseite'])?$_GET['Unterseite']:'OENummern'));
        }
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081006010");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * @param awisFormular $Form
 * @param $Recht452
 * @param $FeldBreiten
 */
function _Schnellanlage($Form, $Recht452,$FeldBreiten){
    global $AWISCursorPosition;
    if (($Recht452 & 4) == 4) {

        $Form->ZeileStart();
        $Form->Erstelle_HiddenFeld('LAR_KEY', 0);
        $Icons = array();
        $Icons[] = array('add', 'POST~Lart');
        $Form->Erstelle_ListeIcons($Icons, 38, 1);
        $Form->Erstelle_ListenFeld('LAR_LARTNR', '', 32, $FeldBreiten['LAR_LARTNR'], true, 1, '', '', 'T', 'L');
        $AWISCursorPosition = 'txtLAR_LARTNR';
        $Form->Erstelle_AutocompleteFeld('LAR_LIE_NR', '', 95, true, 'autocomplete_LIE_NR', 1, '', '', 'Test', '', 'ListenFeldDunkel','onchange="key_TecDocHinweis(this);"',array('txtWGR_ID_Vorschlag','txtWUG_ID_Vorschlag'));
        $Form->AuswahlBox('TecDocHinweis','TecDocHinweis','','TecDocHinweis','txtLAR_LIE_NR',1,1,'','T',true,'','','display:none','','','','',0,'');
        $Form->Erstelle_ListenCheckbox('LARTNRSMARTKATEXPORT_0' , 1, 30,true, 1, '', '', '', '', 1);
        $Form->ZeileEnde();
        $Form->AuswahlBoxHuelle('TecDocHinweis', '', '');

    }
}
?>