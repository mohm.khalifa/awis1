<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('LIK','%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('Wort','EANNummer');
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht3600 = $AWISBenutzer->HatDasRecht(3600);
	if($Recht3600==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');
	
	global $AWIS_KEY1;

	if (isset($_GET['LKK_KEY'])) 
	{
		$AWIS_KEY1=$_GET['LKK_KEY'];		
	}	
	
	$Spalte = 20;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Lieferschein');
	
	$Zeile=_Erzeuge_Seite_Lieferschein();
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT DISTINCT LKK_FIL_ID, LKK_LieferscheinNr, LIK_AST_ATUNR, LIK_MENGESOLL, LKK_DATUMKOMM, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, AST_BEZEICHNUNGWW';
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .= 'INNER JOIN Lieferkontrollen LIK on LIK_LKK_KEY = LKK_KEY';
	$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = LIK_AST_ATUNR';
	$SQL .= ' LEFT JOIN FILIALEN ON FIL_ID = LIK_FIL_ID';
	$SQL .=' WHERE LKK_KEY=0'.$AWIS_KEY1;	
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsLIK = $DB->RecordSetOeffnen($SQL);
	$rsLIKZeilen = $rsLIK->AnzahlDatensaetze();
	
	if($rsLIKZeilen==0)		// Keine Daten
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}		
		
	for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-20)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Lieferschein();
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);
		
		//HAUPTEAN ermitteln
		$BindeVariablen=array();
		$BindeVariablen['var_T_ast_atunr']=$rsLIK->FeldInhalt('LIK_AST_ATUNR');
		$BindeVariablen['var_N0_asi_ait_id']=250;
		
		$SQL = 'SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = :var_T_ast_atunr AND ASI_AIT_ID = :var_N0_asi_ait_id';
		$rsASI=$DB->RecordSetOeffnen($SQL,$BindeVariablen);		
		$EAN = $rsASI->FeldInhalt('ASI_WERT');			
		
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_AST_ATUNR'),1,0,'L',0);	
			
		$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->cell(90,6,$rsLIK->FeldInhalt('AST_BEZEICHNUNGWW'),1,0,'L',0);
			
		$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->cell(50,6,$EAN,1,0,'L',0);
						
		$Ausdruck->_pdf->setXY($Spalte+155,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsLIK->FeldInhalt('LIK_MENGESOLL'),1,0,'R',0);			
		
		$Zeile+=6;
	
		$rsLIK->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Lieferschein()
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	global $AWIS_KEY1;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	if ($Seite == 1)
	{			
		$SQL = 'SELECT DISTINCT LKK_FIL_ID, LKK_LIEFERSCHEINNR, LKK_DATUMKOMM, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT';
		$SQL .=' FROM LieferkontrollenKopf LKK';
		$SQL .=' INNER JOIN Lieferkontrollen on LIK_LKK_KEY = LKK_KEY';
		$SQL .= ' LEFT JOIN FILIALEN ON FIL_ID = LIK_FIL_ID';
		$SQL .=' WHERE LKK_KEY=0'.$AWIS_KEY1;				
		
		$rsLIKKopf = $DB->RecordSetOeffnen($SQL);							
									
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['LIK']['LIK_LIEFERSCHEIN'],0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
					
		$Ausdruck->_pdf->SetFont('Arial','B',10);		
		$Ausdruck->_pdf->setXY($Spalte+65,$Zeile);
		$Ausdruck->_pdf->cell(15,6,'Nr.: ',0,0,'L',0);						
		$Ausdruck->_pdf->cell(30,6,$rsLIKKopf->FeldInhalt('LKK_LIEFERSCHEINNR'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+65,$Zeile);
		$Ausdruck->_pdf->cell(15,6,'Datum: ',0,0,'L',0);						
		$Ausdruck->_pdf->cell(30,6,$Form->Format('D', $rsLIKKopf->FeldInhalt('LKK_DATUMKOMM')),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->cell(40,6,$rsLIKKopf->FeldInhalt('FIL_BEZ'),0,0,'L',0);
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(40,6,$rsLIKKopf->FeldInhalt('FIL_STRASSE'),0,0,'L',0);
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(40,6,$rsLIKKopf->FeldInhalt('FIL_PLZ').' '.$rsLIKKopf->FeldInhalt('FIL_ORT'),0,0,'L',0);
						
	}
	else 
	{
		$Zeile+=5;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);		
	$Zeile=$Zeile+10;		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);				
		
	$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
	$Ausdruck->_pdf->cell(90,6,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
	$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['Wort']['EANNummer'],1,0,'L',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+155,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['Wort']['Menge'],1,0,'L',1);															
			
	$Zeile+=6;	
	
	return $Zeile;
}
?>

