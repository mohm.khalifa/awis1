<?php

global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');

try
{
        
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SGD','*');
    $TextKonserven[]=array('SGK','*');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht9600 = $AWISBenutzer->HatDasRecht(9600);
    
    if($Recht9600==0)
    {
        $DB->ProtokollEintrag(awisDatenbank::EREIGNIS_FEHLER, 1000, $AWISBenutzer->BenutzerName(),'Folienaustausch - Modul',$AWISBenutzer->BenutzerName());
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        die();
    }

    $DetailAnsichtRechnung = false;
    $Rechnungsdruck = false;

    $RechnungsBetrag = 0.0;

    $Form->SchreibeHTMLCode("<form name=frmWeiter method=post action=./sgh_Main.php?cmdAktion=Pruefprotokoll>");

    $Form->DebugAusgabe(1,$_POST);

    if(isset($_POST['cmdZurueck_x']))
    {
        $DetailAnsichtRechnung = false;
    }

    if(isset($_POST['cmdSuche_x']))
    {
        if(isset($_POST["txtSGS_BELEGNR"]) && isset($_POST["txtGesamtbetrag"]) && $_POST['txtSGS_BELEGNR'] != "" && $_POST['txtGesamtbetrag'] != "")
        {
            $DetailAnsichtRechnung = true;
        }
        else
        {
            $DetailAnsichtRechnung = false;
        }
    }

    $Form->Formular_Start();

    

    if($DetailAnsichtRechnung == false)
    {
        $Form->Trennzeile('O');
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel("Belegnummer:", 120);
        $Form->Erstelle_TextFeld('SGS_BELEGNR', '', '', 500,true);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel("Gesamtbetrag:", 120);
        $Form->Erstelle_TextFeld('Gesamtbetrag', '', '', 500,true);
        $Form->ZeileEnde();
    }
    elseif($DetailAnsichtRechnung == true)
    {
        $Form->Trennzeile('O');
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel("Rechnungsnr:", 120);
        $Form->Erstelle_TextFeld('Belegnr', $_POST['txtSGS_BELEGNR'], 120, 120);

        $SQL = " Select bb.SGS_RNRSGH,bb.SGS_RNRDAT,count(aa.SGA_TYP) AS ANZAHL,SUM(bb.sgs_betrag) AS Summe,SUM(bb.sgs_betraggekuerzt) As DIFFERENZ,";
        $SQL .= " aa.SGA_TYP from SGHABGLEICH aa INNER JOIN SGHDIENSTLEISTERABGL bb ON aa.SGA_SGS_KEY = bb.SGS_KEY";
        $SQL .= " INNER JOIN SGHABGLEICHSGRUENDE cc ON aa.SGA_TYP = cc.SGG_KEY WHERE SGS_RNRSGH=".$DB->FeldInhaltFormat('NO', $_POST['txtSGS_BELEGNR']);
        $SQL .= " GROUP BY bb.SGS_RNRSGH,bb.SGS_RNRDAT, aa.sga_typ ORDER BY SGA_TYP";

        $rsDatenAbgleich = $DB->RecordSetOeffnen($SQL);

        $Form->Erstelle_TextLabel("Rechnungsdatum:", 120);
        $Form->Erstelle_TextFeld('Belegdatum', $rsDatenAbgleich->FeldInhalt('SGS_RNRDAT'), 200, 200);
        $Form->Trennzeile('O');
        $Form->Erstelle_TextLabel("Der Beleg wurde wie folgt abgeglichen:", 350);
        $Form->Trennzeile('L');
        $Form->Trennzeile('O');
        $Form->Erstelle_TextLabel("Automatischer Abgleich:", 200);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Trennzeile('O');

      $countEintrag = 0;
      $abglBetrag = 0;

      while(!$rsDatenAbgleich->EOF())
      {
        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 1)
        {
            $Form->Erstelle_TextFeld('anzahlAutoAbgleich', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Positionen durch automatischen Abgleich abgeglichen", 400);
            $Form->Erstelle_TextFeld('autoAbglBetrag', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            //$Form->Erstelle_TextFeld($Name, $Wert, $Zeichen, $Breite, $AendernRecht, $Style, $InputStyle, $Link, $Format, $Ausrichtung, $ToolTippText);
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            
        }

        if($countEintrag == 0)
        {
          $Form->Trennzeile('O');
          $Form->Erstelle_TextLabel("Manueller Abgleich:", 200);
          $Form->ZeileEnde();
          $Form->Trennzeile('O');
        }
        
        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 2)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlBetrag', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /Betrag falsch", 400);
            $Form->Erstelle_TextFeld('manAbglBetragFalsch', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));

            $Form->ZeileEnde();
        }

        
        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 3)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlWANR', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /WANR falsch", 400);
            $Form->Erstelle_TextFeld('manAbglWANRFalsch', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Form->ZeileEnde();
        }

        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 4)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlATUNR', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /ATUNR falsch", 400);
            $Form->Erstelle_TextFeld('manAbglATUNRFalsch', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Form->ZeileEnde();
        }

        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 5)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlKFZKennz', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /KFZKennz falsch", 400);
            $Form->Erstelle_TextFeld('manAbglKFZKennzFalsch', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Form->ZeileEnde();
        }

        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 6)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlSonstiges', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /Sonstiges", 400);
            $Form->Erstelle_TextFeld('manAbglSonstiges', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Form->ZeileEnde();
        }

        
        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 7)
        {
            $Form->Trennzeile('O');
            $Form->Erstelle_TextLabel("Rechnungskürzung:", 200);
            $Form->ZeileEnde();
            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlRechnungskuerzung', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /Kompl. Position", 400);
            $Form->Erstelle_TextFeld('manAbglRechnungskuerzung', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Form->ZeileEnde();
        }

        if($rsDatenAbgleich->FeldInhalt('SGA_TYP') == 8)
        {
            //var_dump($rsDatenAbgleich);

            $Differenz = 0.0;
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('anzahlRechnungskuerzung', $rsDatenAbgleich->FeldInhalt('ANZAHL'), 80, 80);
            $Form->Erstelle_TextLabel("Manueller Abgleich /Betragskürzung", 400);
            $Form->Erstelle_TextFeld('manAbglRechnungskuerzung', $Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')), 13, 60,false,'','','','N2','R');
            $abglBetrag = $abglBetrag + str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('SUMME')));
            $Differenz = str_replace(',', '.',$Form->Format('N2',$rsDatenAbgleich->FeldInhalt('DIFFERENZ')));
            $Form->Erstelle_TextLabel("",20);
            $Form->Erstelle_TextFeld("Differenz","-".str_replace('.', ',',$Differenz),80,80);
            $abglBetrag = $abglBetrag + $Differenz;
            $Form->ZeileEnde();
        }

        $countEintrag++;
        $rsDatenAbgleich->DSWeiter();
      }

      $BetragAbgleich = 0;
      $RechnungsBetrag = 0;
      $BetragDifferenz = 0;

      $Form->ZeileStart();
      $Form->Trennzeile('L');
      $Form->Erstelle_TextLabel("", 280);
      $Form->Erstelle_TextLabel("Betrag (Netto):", 200);
      $Form->Erstelle_TextFeld('abglBetrag', $Form->Format('N2',$abglBetrag), 13, 60,false,'','','','N2','R');
      $BetragAbgleich = str_replace(',', '.', $abglBetrag);
      $Form->ZeileEnde();
      $Form->ZeileStart();
      $Form->Erstelle_TextLabel("", 280);
      $Form->Erstelle_TextLabel("Mwst:", 200);
      $Mwst = 0.0;
      $Mwst = $abglBetrag /100 * 19;
      $Form->Erstelle_TextFeld('Mwst', $Mwst, 13, 60,false,'','','','N2','R');
      //$BetragAbgleich = str_replace(',', '.', $abglBetrag);
      $Form->ZeileEnde();
      $Form->ZeileStart();
      $Form->Trennzeile('L');
      $Form->Erstelle_TextLabel("", 280);
      $Form->Erstelle_TextLabel("Betrag (Brutto):", 200);
      $betragBrutto = 0.0;
      $betragBrutto = $abglBetrag + $Mwst;
      $Form->Erstelle_TextFeld('BetragBrutto', $Form->Format('N2',$betragBrutto), 13, 60,false,'','','','N2','R');
      $BetragAbgleich = str_replace(',', '.', $betragBrutto);
      $Form->ZeileEnde();
      $Form->ZeileStart();
      $Form->Erstelle_TextLabel("", 280);
      $Form->Erstelle_TextLabel("Rechnungsbetrag:", 200);
      $Form->Erstelle_TextFeld('Rechnungsbetrag', $Form->Format('N2',$_POST['txtGesamtbetrag']), 13, 60,false,'','','','N2','R');
      $RechnungsBetrag = str_replace(',', '.', $_POST['txtGesamtbetrag']);
      $Form->ZeileEnde();
      $Form->ZeileStart();
      $Form->Trennzeile('P');
      $BetragDifferenz = $Form->Format('N2',$BetragAbgleich) - $Form->Format('N2',$RechnungsBetrag);
      $Form->Erstelle_TextLabel("", 280);
      $Form->Erstelle_TextLabel("Differenz:", 200);
      $BetragDifferenz = str_replace('.',',',$BetragDifferenz);
      $Form->Erstelle_TextFeld('abglBetrag', $Form->Format('N2',$BetragDifferenz), 13, 60,false,'','','','N2','R');

      if($BetragDifferenz == 0)
      {
        $Rechnungsdruck = true;
      }
      else
      {
        $Rechnungsdruck = false;
      }
    }

    $Form->Formular_Ende();
    $Form->SchaltflaechenStart();
    if($DetailAnsichtRechnung == false)
    {
        $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
        $Form->Schaltflaeche('image','cmdSuche','','/bilder/cmd_weiter.png',$AWISSprachKonserven['Wort']['lbl_weiter'],'W');
    }

    if($Rechnungsdruck == true)
    {
        $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
        $Form->Schaltflaeche('image','cmdZurueck','','/bilder/cmd_dszurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
        //PDF - Rechnungsdruck
        $Form->Schaltflaeche('href','cmd_Pruefprotokoll','./sgh_BelastungDrucken.php?BNR='.$_POST['txtSGS_BELEGNR']."&Rechnungsbetrag=".$_POST['txtGesamtbetrag'],'/bilder/cmd_pdf.png');
        
    }
    elseif($Rechnungsdruck == false && $DetailAnsichtRechnung != false)
    {
        $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
        $Form->Schaltflaeche('image','cmdZurueck','','/bilder/cmd_dszurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    }



    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');

}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}

?>