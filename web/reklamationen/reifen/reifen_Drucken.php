<?php
global $AWIS_KEY1;
global $REX;
try {
    
    $REX->Form->Formular_Start();
    echo '<form name=frmreifen action=./reifen_Main.php?cmdAktion=Drucken method=POST>';
    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_DRUCKTYPEN'].': ',170);
    $Daten = explode('|',$REX->AWISSprachKonserven['REX']['REX_LST_DRUCKTYPEN']);
    $REX->Form->Erstelle_SelectFeld('Drucktyp',isset($_POST['txtDrucktyp'])?$_POST['txtDrucktyp']:'','100:100',true,'','','','','',$Daten,'onchange="filter();"');
    $REX->Form->ZeileEnde();

    $Script = '
        <script>
        $(document).ready(function(){
            filter();
        });
   
        function filter(){
            if($("#txtDrucktyp option:selected").val() == 1){
                $("#filter").hide();
            }else{
                $("#filter").show();
            }
            
        }
        
        //Alle anchecken..
        $( document ).ready(function() {
            $("#txtcheckAlle").change(function() {
                $(".anzeigen_chk").prop(\'checked\',  $("#txtcheckAlle").prop(\'checked\'));
            });  
        });  
        </script>
    ';

    echo $Script;



    $REX->Form->SchreibeHTMLCode('<div id="filter">');
    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_HERSTELLER'].': ',170);
    $SQL = 'select distinct reh_bezeichnung, reh_bezeichnung from V_REIFENHERSTELLER order by 1 asc';
    $REX->Form->Erstelle_MehrfachSelectFeld('REX_HERSTELLER',isset($_POST['txtREX_HERSTELLER'])?$_POST['txtREX_HERSTELLER']:array(),'355:600',true,$SQL,'','','','');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['Wort']['DatumVom'].':',170);
    $REX->Form->Erstelle_TextFeld('REX_REKLAMATIONSDATUM_VON',isset($_POST['txtREX_REKLAMATIONSDATUM_VON'])?$_POST['txtREX_REKLAMATIONSDATUM_VON']:'',10,150,true,'','','','D','','',date('d')-1 . '.'. date('m') . '.'. date('Y'));
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['Wort']['DatumBis'].':',100);
    $REX->Form->Erstelle_TextFeld('REX_REKLAMATIONSDATUM_BIS',isset($_POST['txtREX_REKLAMATIONSDATUM_BIS'])?$_POST['txtREX_REKLAMATIONSDATUM_BIS']:'',10,150,true,'','','','D','','',date('d.m.Y'));
    $REX->Form->ZeileEnde();
    $REX->Form->SchreibeHTMLCode('</div>');
    $Drucken = false;
    if(isset($_POST['txtDrucktyp'])){
        $REX->Form->ZeileStart();
        $REX->Form->Trennzeile('L');
        $REX->Form->ZeileEnde();

        $REX->Form->ZeileStart();
        $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_ZU_DRUCKEN'],200,'font-weight: bold; font-size: 15px');
        $REX->Form->ZeileEnde();
        $SQL  ='SELECT ';
        $SQL .='   REX_KEY,';
        $SQL .='   REX_REKLAMATIONSDATUM,';
        $SQL .='   REX_WERK,';
        $SQL .='   REX_AST_ATUNR,';
        $SQL .='   REX_HERSTELLER,';
        $SQL .='   REX_P,';
        $SQL .='   REX_EM,';
        $SQL .='   REX_RA,';
        $SQL .='   REX_LAN_CODE,';
        $SQL .='   REX_PROFILTIEFE,';
        $SQL .='   REX_STATUS,';
        $SQL .='   REX_USER,';
        $SQL .='   REX_USERDAT';
        $SQL .=' FROM REIFENREKLAMATIONEN ';
        if($_POST['txtDrucktyp'] == 1){ //Alle nicht gedruckten
            $SQL .= 'WHERE REX_STATUS !=\'G\' ';
        }elseif($_POST['txtDrucktyp'] == 2){ //Nach Hersteller und Datum
            $SQL .= ' WHERE REX_REKLAMATIONSDATUM >= ' . $REX->DB->WertSetzen('REX','D',$_POST['txtREX_REKLAMATIONSDATUM_VON']);
            $SQL .= ' and REX_REKLAMATIONSDATUM <= ' . $REX->DB->WertSetzen('REX','D',$_POST['txtREX_REKLAMATIONSDATUM_BIS']);
            if(isset($_POST['txtREX_HERSTELLER'])){
                $Hersteller = mb_strtoupper(implode("','",$_POST['txtREX_HERSTELLER']));
                $SQL .= " and upper(REX_HERSTELLER) in ('$Hersteller')" ;
            }
        }

        $rsREX = $REX->DB->RecordSetOeffnen($SQL,$REX->DB->Bindevariablen('REX'));

        if($rsREX->EOF()){
            $REX->Form->ZeileStart();
            $REX->Form->Hinweistext($REX->AWISSprachKonserven['Fehler']['err_keineDaten']);
            $REX->Form->ZeileEnde();
        }else{
            $FeldBreiten = array();
            $FeldBreiten['REX_REKLAMATIONSDATUM'] = 150;
            $FeldBreiten['REX_AST_ATUNR'] = 150;
            $FeldBreiten['REX_EM'] = 150;
            $FeldBreiten['REX_HERSTELLER'] = 150;
            $FeldBreiten['REX_STATUS'] = 50;

            $REX->Form->ZeileStart();
            ob_start();
            $REX->Form->Erstelle_Checkbox('checkAlle', 1, 20,true,1,'','','');
            $Inhalt = ob_get_clean();
            $REX->Form->Erstelle_Liste_Ueberschrift($Inhalt,20,'padding-left: 5px;padding-top: 2px;');
            $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Link .= '&Sort=REX_REKLAMATIONSDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_REKLAMATIONSDATUM'))?'~':'');
            $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_REKLAMATIONSDATUM'], $FeldBreiten['REX_REKLAMATIONSDATUM'], '', $Link);
            $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Link .= '&Sort=REX_AST_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_AST_ATUNR'))?'~':'');
            $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_AST_ATUNR'], $FeldBreiten['REX_AST_ATUNR'], '', $Link);
            $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Link .= '&Sort=REX_EM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_EM'))?'~':'');
            $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_EM'], $FeldBreiten['REX_EM'], '', $Link);
            $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Link .= '&Sort=REX_HERSTELLER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_HERSTELLER'))?'~':'');
            $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_HERSTELLER'], $FeldBreiten['REX_HERSTELLER'], '', $Link);
            $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Link .= '&Sort=REX_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_STATUS'))?'~':'');
            $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_STATUS'], $FeldBreiten['REX_STATUS'], '', $Link);
            $REX->Form->ZeileEnde();
            $DS = 0;
            while (!$rsREX->EOF()) {
                $HG = ($DS % 2);
                $REX->Form->ZeileStart();
                $REX->Form->Erstelle_ListenCheckbox('DruckKey[]',((isset($_POST['txtDruckKey']) and in_array($rsREX->FeldInhalt('REX_KEY'),$_POST['txtDruckKey']))?$rsREX->FeldInhalt('REX_KEY'):$rsREX->FeldInhalt('REX_KEY')),20,true,$rsREX->FeldInhalt('REX_KEY'),'','','','',$HG,false,'','anzeigen_chk');
                $Link = './reifen_Main.php?cmdAktion=Details&REX_KEY=0' . $rsREX->FeldInhalt('REX_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
                $REX->Form->Erstelle_ListenFeld('REX_REKLAMATIONSDATUM', $rsREX->FeldInhalt('REX_REKLAMATIONSDATUM'), 0, $FeldBreiten['REX_REKLAMATIONSDATUM'], false, $HG, '', $Link, 'D');
                $REX->Form->Erstelle_ListenFeld('REX_AST_ATUNR', $rsREX->FeldInhalt('REX_AST_ATUNR'), 0, $FeldBreiten['REX_AST_ATUNR'], false, $HG, '', '', 'T', 'L');
                $REX->Form->Erstelle_ListenFeld('REX_EM', $rsREX->FeldInhalt('REX_EM'), 0, $FeldBreiten['REX_EM'], false, $HG, '', '', 'T', 'L');
                $REX->Form->Erstelle_ListenFeld('REX_HERSTELLER', $rsREX->FeldInhalt('REX_HERSTELLER'), 0, $FeldBreiten['REX_HERSTELLER'], false, $HG, '', '', 'T', 'L');
                if ($rsREX->FeldInhalt('REX_STATUS') == 'G') {
                    $Icons = array("flagge_gruen", '', "", $REX->AWISSprachKonserven['REX']['REX_STATUS_G']);
                } else {
                    $Icons = array("flagge_rot", '', "", $REX->AWISSprachKonserven['REX']['REX_STATUS_ungleich_G']);
                }

                $REX->Form->Erstelle_ListeIcons(array($Icons), $FeldBreiten['REX_STATUS'], $HG, '', 'align: center');
                $REX->Form->ZeileEnde();

                $rsREX->DSWeiter();
                $DS++;
            }
            $Drucken = true;
        }
    }

    $REX->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $REX->Form->SchaltflaechenStart();
    $REX->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $REX->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $REX->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $REX->AWISSprachKonserven['Wort']['lbl_weiter'], 'W','',27,27,'',true);
    if($Drucken){
        $REX->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $REX->AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
    }
    $REX->Form->SchaltflaechenEnde();

    $REX->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($REX->Form instanceof awisFormular) {
        $REX->Form->DebugAusgabe(1, $ex->getSQL());
        $REX->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $REX->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($REX->Form instanceof awisFormular) {
        $REX->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>