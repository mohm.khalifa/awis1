<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try 
{
	$TextKonserven = array();
	$TextKonserven[]=array('GSD','%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_aendern');


    $Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht28000 = $AWISBenutzer->HatDasRecht(28000);
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	if($Recht28000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	// Parameter
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = array();
	$Param['AUFTRAGNR']=$_GET['GSK_AUFTRAGNR'];
	$Param['GSP_KEY']=$_GET['GSP_KEY'];
	$Param['Sort']='';
	$Param['ORDER']='';
	$Param['Block']='1';

	// Sortierung
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY GPD_GUTSCHEINNR DESC';
			$Param['ORDER']='GPD_GUTSCHEINNR DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//Daten holen
	$SQL_POSGS = 'SELECT gs.GPD_KEY';
	$SQL_POSGS .= ', gs.GPD_GUTSCHEINNR';
	$SQL_POSGS .= ', gs.GPD_PRUEFZ';
	$SQL_POSGS .= ', gs.GPD_POSITION';
	$SQL_POSGS .= ', gs.GPD_AUFTRAGNR';
	$SQL_POSGS .= ', gs.GPD_DATUM';
	$SQL_POSGS .= ', gs.GPD_KOMMENTAR';
	$SQL_POSGS .= ', gs.GPD_BUCHART';
	$SQL_POSGS .= ', gs.GPD_STATUS';
	$SQL_POSGS .= ', gs.GPD_STATUS_MSG';
	$SQL_POSGS .= ', gs.GPD_USER';
	$SQL_POSGS .= ', gs.GPD_USERDAT';
	$SQL_POSGS .= ', gs.GPD_TRANSID';
	$SQL_POSGS .= ', gs.GPD_STATUS_CODE';
	$SQL_POSGS .= ', gs.GPD_UNIT';
	$SQL_POSGS .= ', gs.GPD_CARD_PIN';
	$SQL_POSGS .= ', gs.GPD_AMOUNT';
	$SQL_POSGS .= ', pos.GSP_KEY';
	$SQL_POSGS .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL_POSGS .= ' FROM GUTSCHEINE gs';
	$SQL_POSGS .= ' INNER JOIN GUTSCHEINPOSDATEN pos ON gs.GPD_AUFTRAGNR = pos.GSP_AUFTRAGNR';
	$SQL_POSGS .= ' and gs.GPD_POSITION = pos.GSP_POSITION';
	$SQL_POSGS .= ' AND pos.GSP_KEY =' . ($Param['GSP_KEY']!==0?$Param['GSP_KEY']:$_GET['GSP_KEY']);
	$SQL_POSGS .= ' and gs.GPD_AUFTRAGNR=\''. ($Param['AUFTRAGNR']!==0?$Param['AUFTRAGNR']:$_GET['GSK_AUFTRAGNR']) . '\'';
	$SQL_POSGS .= ' GROUP BY gs.GPD_KEY';
	$SQL_POSGS .= ', gs.GPD_GUTSCHEINNR';
	$SQL_POSGS .= ', gs.GPD_PRUEFZ';
	$SQL_POSGS .= ', gs.GPD_POSITION';
	$SQL_POSGS .= ', gs.GPD_AUFTRAGNR';
	$SQL_POSGS .= ', gs.GPD_DATUM';
	$SQL_POSGS .= ', gs.GPD_KOMMENTAR';
	$SQL_POSGS .= ', gs.GPD_BUCHART';
	$SQL_POSGS .= ', gs.GPD_STATUS';
	$SQL_POSGS .= ', gs.GPD_STATUS_MSG';
	$SQL_POSGS .= ', gs.GPD_USER';
	$SQL_POSGS .= ', gs.GPD_USERDAT';
	$SQL_POSGS .= ', gs.GPD_TRANSID';
	$SQL_POSGS .= ', gs.GPD_STATUS_CODE';
	$SQL_POSGS .= ', gs.GPD_UNIT';
	$SQL_POSGS .= ', gs.GPD_CARD_PIN';
	$SQL_POSGS .= ', gs.GPD_AMOUNT';
	$SQL_POSGS .= ', pos.GSP_KEY';

	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['Block'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL_POSGS);
	$SQL_POSGS = "SELECT * FROM ($SQL_POSGS) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsGPD = $DB->RecordSetOeffnen($SQL_POSGS);
	$rsGPDZeilen = $rsGPD->AnzahlDatensaetze();

	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['GPD_POSITION'] = 100;
	$FeldBreiten['GSK_AUFTRAGNR']=150;
	$FeldBreiten['GPD_GUTSCHEINNR']=200;
	$FeldBreiten['GPD_KOMMENTAR']=250;
	$FeldBreiten['GPD_STATUS_MSG']=150;
	$FeldBreiten['GPD_BUCHART']=100;
	$FeldBreiten['GPD_STATUS']=100;
	$FeldBreiten['GPD_AUSWAHL']=100;
	$FeldBreiten['GPD_AUSWAHLALLE']=250;

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . (isset($Param['GSP_KEY'])?($Param['GSP_KEY']):$rsGPD->FeldInhalt('GSP_KEY')
			. (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:''));
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=". $Link .">");
	$Form->Formular_Start();

	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR=" . ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GPD_AUFTRAGNR')) . " title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Form->InfoZeile($Felder,'');

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_AUSWAHLALLE'],$FeldBreiten['GPD_AUSWAHLALLE']);
	$Form->Erstelle_Checkbox('AUSWAHL_ALLE','',$FeldBreiten['GPD_AUSWAHLALLE'],true,'on','','','','');
	$Form->ZeileEnde();
	$Form->Trennzeile('O');

	$Form->ZeileStart();
	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_POSITION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_POSITION'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_POSITIONEN'], $FeldBreiten['GPD_POSITION'], '', $Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_AUFTRAGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_AUFTRAGNR'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_AUFTRAGNUMMER'], $FeldBreiten['GSK_AUFTRAGNR'],'',$Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_GUTSCHEINNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_GUTSCHEINNR'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_GUTSCHEINNUMMER'], $FeldBreiten['GPD_GUTSCHEINNR'],'',$Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_KOMMENTAR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_KOMMENTAR'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_POS_KOMMENTAR'], $FeldBreiten['GPD_KOMMENTAR'],'',$Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_STATUS_MSG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_STATUS_MSG'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_FEHLERTEXT'], $FeldBreiten['GPD_STATUS_MSG'],'',$Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_BUCHART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_BUCHART'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BUCHUNGSART'], $FeldBreiten['GPD_BUCHART'], '', $Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
		. '&Sort=GPD_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_STATUS'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_STATUS'], $FeldBreiten['GPD_STATUS'], '', $Link);

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . ($Param['GSP_KEY'] !== $Param['GSP_KEY']?'':$rsGPD->FeldInhalt('GSP_KEY')
			. '&Sort=GPD_BUCHART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GPD_BUCHART'))?'~':'')) . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_AUSWAHL'], $FeldBreiten['GPD_AUSWAHL'], '', $Link);

	$Form->ZeileEnde();

	$DS=0;
	for ($GPDZeile = 0; $GPDZeile < $rsGPDZeilen; $GPDZeile++)
	{
		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('GPD_POSITION', $rsGPD->FeldInhalt('GPD_POSITION'), 0, $FeldBreiten['GPD_POSITION'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_AUFTRAGNR', $rsGPD->FeldInhalt('GPD_AUFTRAGNR'), 0, $FeldBreiten['GSK_AUFTRAGNR'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_GUTSCHEINNR', $rsGPD->FeldInhalt('GPD_GUTSCHEINNR'), 0, $FeldBreiten['GPD_GUTSCHEINNR'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_KOMMENTAR', $rsGPD->FeldInhalt('GPD_KOMMENTAR'), 0, $FeldBreiten['GPD_KOMMENTAR'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_STATUS_MSG', $rsGPD->FeldInhalt('GPD_STATUS_MSG'), 0, $FeldBreiten['GPD_STATUS_MSG'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_BUCHART', $rsGPD->FeldInhalt('GPD_BUCHART'), 0, $FeldBreiten['GPD_BUCHART'], false, ($DS % 2), '', '');
		$Form->Erstelle_ListenFeld('GPD_STATUS', $rsGPD->FeldInhalt('GPD_STATUS'), 0, $FeldBreiten['GPD_STATUS'], false, ($DS % 2), '', '');
		$Form->Erstelle_Checkbox('GPD_KEY_'.$rsGPD->FeldInhalt('GPD_KEY'),'',$FeldBreiten['GPD_AUSWAHL'],true,'on','','','','');
		$Form->ZeileEnde();

		$rsGPD->DSWeiter();
		$DS++;
	}

	$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($Param['AUFTRAGNR'] !== ''?$Param['AUFTRAGNR']:$rsGPD->FeldInhalt('GSP_AUFTRAGNR'))
		.'&Seite=Gutscheine&GSP_KEY=' . (isset($Param['GSP_KEY'])?($Param['GSP_KEY']):$rsGPD->FeldInhalt('GSP_KEY')
			. (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:''));
	$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

	$Form->Formular_Ende();


	// Schaltfl�chen f�r dieses Register
	$Form->SchaltflaechenStart();
	if(($Recht28000&4) == 4)
	{
		//Schaltfl�che Aktiv solange anzeigen bis alle GS aktiv oder deaktiviert sind
		//Schaltfl�che Deaktiv solange anzeigen bis alle GS deaktiviert sind
		//Pr�fung in Speichern: A -> S OK
		//						S -> S OK
		//						A -> A OK
		//						S -> A Nicht OK

		$Form->Schaltflaeche('image', 'cmdAktiv', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['GSD']['GSD_AKTIV'], 'A');
		$Form->Schaltflaeche('image', 'cmdDeaktiv', '', '/bilder/cmd_ds.png', $AWISSprachKonserven['GSD']['GSD_DEAKTIV'], 'D');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');



}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201402081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201402081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}







?>