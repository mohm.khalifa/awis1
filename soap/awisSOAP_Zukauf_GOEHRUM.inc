<?php
require_once('/daten/soap/awisSOAP_Zukauf.inc');
require_once '/daten/soap/goehrum/PartnerServiceV3/goehrum_autoload.php';
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisZukaufArtikel_goehrum
    extends awisZukaufArtikel
{
    /**
     * Lieferantennummer
     *
     */
    const LIE_NR = '9173';
    /**
     * WSDL Datei f?r die Abrufe der Daten
     *
     */
    const WSDL_DATEI = 'http://webdienste01.goehrum.de/PartnerServiceV3.asmx?wsdl';

    const WSDL_DATEI_TEST = 'http://webdienste01.goehrum.de/PartnerServiceV3.asmx?wsdl';

    /**
     * Lieferart bei goehrum: Sofortige Abholung
     *
     * @var int
     */
    const LIEFERART_STANDARD = 'Standard';


    private $_SOAP_Benutzer = '';
    private $_SOAP_Kennwort = '';

    /**
     * E-Mailadressen f?r eine Bestellbestaetigung
     *
     * @var string
     */
    private $_EMAIL_BESTAETIGUNG = 'shuttle@de.atu.eu';        // Defaultwert
    /**
     * Service von Goehrum
     *
     * @var PartnerserviceV3
     */
    private $_SOAPService;

    /**
     * Ergebnisdaten als XML
     *
     * @var SimpleXMLElement
     */
    private $_Daten;

    /**
     * Anmldedaten
     *
     * @var string
     */
    private $_LoginDaten;

    /**
     * Standardlieferart f?r diese Filie
     *
     * @var int
     */
    private $_StandardLieferart = 2;
    /**
     * Texte mit den Verf?gbarkeiten
     *
     * @var array
     */
    private $_TexteVerfuegbarkeit = array();


    public function __construct($XML = false, $FIL_ID = 0)
    {
        parent::__construct();

        try {
            $this->_FIL_ID = $FIL_ID;
            ini_set('allow_call_time_pass_reference', 'true');

            if ($XML === false) {
                $this->_Daten = new awisSimpleXMLElement('<Daten><Anfragezeit>' . date('c') . '</Anfragezeit></Daten>');
            } else {
                $this->_Daten = $XML;
            }

            ini_set('default_socket_timeout', 180);            // Wartezeit f?r eine Antwort

            $WerkZeug = new awisWerkzeuge();
            file_put_contents($this->_Protokollverzeichnis . '/goehrum_init.log', '---------------------------------------' . PHP_EOL, FILE_APPEND);
            file_put_contents($this->_Protokollverzeichnis . '/goehrum_init.log', "Neue Anfrage " . date('c') . "...\n", FILE_APPEND);

            if ($WerkZeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK) {
                $this->_SOAPService = new PartnerserviceV3(array(), self::WSDL_DATEI_TEST);
            } else {
                $this->_SOAPService = new PartnerserviceV3(array(), self::WSDL_DATEI);
            }

            if ($this->_SOAPService === false) {
                throw new Exception('Kein Kontakt zum goehrum Server', 'A1');
            }

            $this->_AnmeldeDaten = $this->_LeseKundenNummer(9173, $this->_FIL_ID);

            if (!$this->_AnmeldeDaten) {
                $Daten = $this->_Daten->addChild('Fehler');
                $Daten->addChild('Nummer', 0);
                $Daten->addCData('Text', 'Keine Anmeldedaten f?r Filiale ' . $this->_FIL_ID);
            }
        } catch (SoapFault $ex) {
            $Daten = $this->_Daten->addChild('Fehler');
            $Daten->addChild('Nummer', 0);
            $Daten->addCData('Text', $ex);
        } catch (SoapFault $ex) {
            $Daten = $this->_Daten->addChild('Fehler');
            $Daten->addChild('Nummer', 0);
            $Daten->addCData('Text', $ex);
        }
    }

    /**
     * Pr?fung eines Artikelpreises und eines Bestands f?r goehrum
     *
     * @param array $ArtikelInfos
     * @return SimpleXMLElement
     */
    public function ArtikelVerfuegbarkeit(array $ArtikelInfos, $DebugInfos = false)
    {

        try {
            $i = 0;
            /*****************************************
             * 1. Artikel, welcher in der Maske gesucht wurde, in unserem Artikelstamm suchen...
             *****************************************/

            if ($DebugInfos == true) {
                $DebugXML = $this->_Daten->addChild('Debug');
            }

            file_put_contents($this->_Protokollverzeichnis . '/goehrum_anfrage.log', "\n" . 'Neue Anfrage:' . date('c') . PHP_EOL, FILE_APPEND);

            $SQL = 'SELECT DISTINCT ZAL_BESTELLNUMMER, ZHK_CODE, ZLH_TECDOCID';
            $SQL .= ' FROM ZUKAUFARTIKELLIEFERANTEN';
            $SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZAL_ZLA_KEY = ZLA_KEY';
            $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLA_ZLH_KEY = ZLH_KEY';
            $SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZHK_ZLH_KEY = ZLH_KEY AND ZHK_LIE_NR = \'' . self::LIE_NR . '\'';
            $SQL .= ' WHERE ZAL_LIE_NR = \'' . self::LIE_NR . '\'';
            $Bedingung = '';

            $FeldName = 'bestellartikelnummer';            // Suchfeld f?r die Anfragen
            if (isset($ArtikelInfos['artikelnummer'])) {
                if (is_array($ArtikelInfos['artikelnummer'])) {
                    foreach ($ArtikelInfos['artikelnummer'] AS $ArtNr) {
                        $Bedingung .= ' OR  ( ZLA_ARTIKELNUMMER = \'' . $ArtNr . '\')';
                        $ArtikelNummer[] = $ArtNr;
                    }
                } else {
                    $Bedingung .= ' OR  ( ZLA_ARTIKELNUMMER = \'' . $ArtikelInfos['artikelnummer'] . '\')';
                    $ArtikelNummer = $ArtikelInfos['artikelnummer'];
                }
            }
            if (isset($ArtikelInfos['bestellnummer'])) {
                $Bedingung .= ' OR ( ZAL_BESTELLNUMMER = \'' . $ArtikelInfos['bestellnummer'] . '\')';
                $ArtikelNummer = $ArtikelInfos['bestellnummer'];
            }
            if (isset($ArtikelInfos['eannummer'])) {
                $Bedingung .= ' OR ( EXISTS (SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZAL_KEY AND ZAI_AID_NR = 1 AND ZAI_ID = \'' . $ArtikelInfos['eannummer'] . '\'))';
                $ArtikelNummer = $ArtikelInfos['eannummer'];
            }
            if (isset($ArtikelInfos['oenummer'])) {
                $Bedingung .= ' OR ( EXISTS (SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZAL_KEY AND ZAI_AID_NR = 2 AND ZAI_ID = \'' . $ArtikelInfos['oenummer'] . '\'))';
                $ArtikelNummer = $ArtikelInfos['oenummer'];
            }

            $SQL .= ' AND (' . substr($Bedingung, 4) . ')';
            if (isset($ArtikelInfos['zlh_key']) AND $ArtikelInfos['zlh_key'] > 0) {
                $SQL .= ' AND ( ZLA_ZLH_KEY = \'' . $ArtikelInfos['zlh_key'] . '\')';
            }

            if (isset($DebugXML)) {
                $DebugXML->addChild('SQL', $SQL);
            }

            $rsZAL = $this->_DB->RecordSetOeffnen($SQL);
            $SuchArt = 1;
            if ($rsZAL->EOF()) {
                $SuchArt = 2;
                $ArtikelNummer = $ArtikelInfos['artikelnummer'];
                if (isset($ArtikelInfos['zlh_key']) AND $ArtikelInfos['zlh_key'] != '') {
                    $SQL = 'SELECT ZHK_CODE';
                    $SQL .= ' FROM ZUKAUFHERSTELLERCODES';
                    $SQL .= ' WHERE ZHK_LIE_NR = \'' . self::LIE_NR . '\'';
                    $SQL .= ' AND ZHK_ZLH_KEY = 0' . $ArtikelInfos['zlh_key'];
                    $rsZHK = $this->_DB->RecordSetOeffnen($SQL);
                    $TecDocID = $rsZHK->FeldInhalt('ZHK_CODE');
                    $DebugXML->addChild('SQL2', $SQL);
                } else {
                    $TecDocID = '';
                }
            }


            $Artikelliste = $this->_Daten->addChild('Artikelliste');
            $Artikelliste->addChild('Treffer', 0);

            $WeiterSuchen = true;
            while ($WeiterSuchen) {
                if ($SuchArt == 2) {
                    $WeiterSuchen = false;
                } elseif (isset($rsZAL)) {
                    if ($rsZAL->EOF()) {
                        break;
                    }
                    $ArtikelNummer = $rsZAL->FeldInhalt('ZAL_BESTELLNUMMER');
                    $TecDocID = $rsZAL->FeldInhalt('ZLH_TECDOCID');
                    $rsZAL->DSWeiter();
                }

                /*****************************************
                 * 2. Gefundene Artikel beim Lieferanten anfragen
                 *****************************************/

                $SuchAnfrage = new SuchAnfrage($this->_AnmeldeDaten['KDNR']);

                $ArrayOfSuchanfrage = new ArrayOfSuchAnfrageArtikel();

                $Artikel = array();
                $DebugXML->addChild('Anfrage' . ++$i, 'Artikelnummer ' . $ArtikelNummer . ' TecDoc: ' . $TecDocID . ' Menge: ' . $ArtikelInfos['menge'] . ' Suchart: ' . $SuchArt);
                $Artikel[] = new SuchAnfrageArtikel($TecDocID, $ArtikelNummer, $ArtikelInfos['menge']);

                $ArrayOfSuchanfrage->setSuchAnfrageArtikel($Artikel);

                $SuchAnfrage->setAnfrage($ArrayOfSuchanfrage);
                $SuchAnfrage->setKundennummer($this->_AnmeldeDaten['KDNR']);
                $Bestandsauskunft = new Bestandsauskunft($SuchAnfrage);

                $BestandsauskunftResponse = $this->_SOAPService->Bestandsauskunft($Bestandsauskunft);
                $BestandsauskunftResult = $BestandsauskunftResponse->getBestandsauskunftResult();

                $ArtikelTreffer = $BestandsauskunftResult->getBestandV3();

                file_put_contents($this->_Protokollverzeichnis . '/goehrum_anfrage.log', 'Anfrage fuer die Artikelsuche: Artikelnummer:' . $ArtikelNummer . ' TecDocId: ' . $TecDocID . "\n", FILE_APPEND);

                /*****************************************
                 * 3. Antwort auswerten und R?ckgabe bauen
                 *****************************************/
                if (is_array($ArtikelTreffer)) { //Artikel gefunden
                    file_put_contents($this->_Protokollverzeichnis . '/goehrum_anfrage.log', 'Gefundene Artikel:' . serialize($ArtikelTreffer) . "\n", FILE_APPEND);
                    $Pos = 1;
                    foreach ($ArtikelTreffer as $Treffer) {
                        if ($Treffer->getOriginalArtikel()->getEinspeiserNr() == 0) { //Kein Einspeiser = Kein Artikel
                            $ArtikelTreffer = null;
                            $Fehler = $this->_Daten->addChild('Fehler');
                            $Fehler->addChild('Text', $Treffer->getOriginalArtikel()->getInfotext());
                            $Fehler->addChild('Nummer', -1);
                            continue;
                        }
                        $Artikelliste->Treffer = ((int)$Artikelliste->Treffer) + 1; //Treffer um eins erh?hen

                        $Artikel[$Pos] = $Pos;
                        $XMLArtikelTreffer = $Artikelliste->addChild('ArtikelTreffer');

                        $XMLArtikel = $Artikelliste->addChild('Artikel');

                        /****************************
                         * 4. Hersteller aus Antwort suchen
                         ****************************/
                        $SQL = 'SELECT ZHK_KEY, ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
                        $SQL .= ' FROM ZUKAUFHERSTELLERCODES';
                        $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZHK_ZLH_KEY = ZLH_KEY';
                        $SQL .= ' WHERE ZHK_CODE = \'' . $Treffer->getOriginalArtikel()->getEinspeiserNr() . '\'';
                        $SQL .= ' AND ZHK_LIE_NR = \'' . self::LIE_NR . '\'';
                        $rsZHK = $this->_DB->RecordSetOeffnen($SQL);

                        if (isset($DebugXML)) {
                            $DebugXML->addChild('HerstellersuchSQL', $SQL);
                        }


                        /********************
                         * 5. Artikel aus Antwort bei uns im Artikelstamm suchen
                         ********************/
                        $SQL = 'SELECT ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG, ZLA_KEY, ZLA_AST_ATUNR, ZAL_KEY, ZLA_ZLH_KEY';
                        $SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS VP';
                        $SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=300 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS VPZUKAUF';
                        $SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
                        $SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = \'DE\' AND ATW_KENNUNG = ASI_WERT';
                        $SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
                        $SQL .= ', (SELECT AST_VK FROM Artikelstamm WHERE AST_ATUNR = ZLA_AST_ATUNR ) AS AST_VK';
                        $SQL .= ' FROM Zukaufartikel';
                        $SQL .= ' LEFT OUTER JOIN Zukaufartikellieferanten ON zla_key = zal_zla_key AND zal_lie_nr = \'' . self::LIE_NR . '\'';
                        $SQL .= ' WHERE ZLA_ARTIKELNUMMER = \'' . awisWerkzeuge::ArtNrKomprimiert($Treffer->getOriginalArtikel()->getEinspeiserArtikelNr()) . '\'';
                        if (isset($DebugXML)) {
                            $DebugXML->addChild('ArtikelsuchSQL', $SQL);
                        }

                        $rsZLA = $this->_DB->RecordSetOeffnen($SQL);

                        $Hilf = $XMLArtikelTreffer->addChild('Zukaufartikel');
                        $Hilf->addChild('ZLA_KEY', ($rsZLA->FeldInhalt('ZLA_KEY') == '' ? '' : $rsZLA->FeldInhalt('ZLA_KEY')));
                        $Hilf->addChild('ZLA_ZLH_KEY', ($rsZLA->FeldInhalt('ZLA_ZLH_KEY') == '' ? '' : $rsZLA->FeldInhalt('ZLA_ZLH_KEY')));
                        $Hilf->addChild('ZAL_KEY', ($rsZLA->FeldInhalt('ZAL_KEY') == '' ? '' : $rsZLA->FeldInhalt('ZAL_KEY')));
                        $Hilf->addChild('ZLA_ARTIKELNUMMER', ($rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER') == '' ? '' : $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER')));
                        $Hilf->addCData('ZLA_BEZEICHNUNG', ($rsZLA->FeldInhalt('ZLA_BEZEICHNUNG') == '' ? '' : ($rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'))));
                        $Hilf->addChild('ZLA_AST_ATUNR', ($rsZLA->FeldInhalt('ZLA_AST_ATUNR') == '' ? '' : $rsZLA->FeldInhalt('ZLA_AST_ATUNR')));
                        $Hilf->addChild('ATU_VK', $rsZLA->FeldInhalt('AST_VK'));

                        $XMLArtikelTreffer->addChild('Artikelnummer', awisWerkzeuge::ArtNrKomprimiert($Treffer->getOriginalArtikel()->getEinspeiserArtikelNr()));
                        $XMLArtikelTreffer->addChild('Katalognummer', $Treffer->getOriginalArtikel()->getEinspeiserArtikelNr());
                        $XMLArtikelTreffer->addCData('Artikelbezeichnung', $rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'));
                        $XMLArtikelTreffer->addChild('Herstellercode', $Treffer->getOriginalArtikel()->getEinspeiserNr());

                        $XMLArtikelTreffer->addChild('Herstellername', $this->_getHerstellerName($Treffer->getOriginalArtikel()->getEinspeiserNr()));
                        $XMLArtikelTreffer->addChild('EAN-Nummer', 'EAN');
                        $XMLArtikelTreffer->addChild('OE-Nummer', 'OENummer');
                        $XMLArtikelTreffer->addChild('Bestellnummer', $Treffer->getOriginalArtikel()->getEinspeiserArtikelNr());
                        $XMLArtikelTreffer->addChild('Mengeneinheit', $Treffer->getOriginalArtikel()->getMEH());

                        $XMLArtikelTreffer->addChild('Tauschteil', ((string)$Treffer->getOriginalArtikel()->getInfotext() == 'Tauschteil' ? 1 : 0));


                        $Hilf = $XMLArtikelTreffer->addChild('Hersteller');

                        $Hilf->addChild('ZLH_KEY', $rsZHK->FeldInhalt('ZLH_KEY'));
                        $Hilf->addChild('ZHK_KEY', $rsZHK->FeldInhalt('ZHK_KEY'));
                        $Hilf->addChild('ZLH_BEZEICHNUNG', $rsZHK->FeldInhalt('ZLH_BEZEICHNUNG'));
                        $Hilf->addChild('ZLH_KUERZEL', $rsZHK->FeldInhalt('ZLH_KUERZEL'));


                        $Hilf = $XMLArtikelTreffer->addChild('Warengruppe');
                        $Hilf->addChild('ZSZ_KEY', 0);
                        $Hilf->addChild('ZSZ_SORTIMENT', 0);

                        $Hilf = $XMLArtikelTreffer->addChild('Finanzen');
                        $Hilf->addChild('MWS_ID', self::MWST_DE_STANDARD);

                        $rsMWS = $this->_DB->RecordSetOeffnen('SELECT MWS_SATZ from mehrwertsteuersaetze where mws_id = 0' . self::MWST_DE_STANDARD);
                        $Mehrwertsteuer[self::MWST_DE_STANDARD] = ($this->_DB->FeldInhaltFormat('N4', $rsMWS->FeldInhalt('MWS_SATZ'), false) / 100.0);

                        $Hilf->addChild('EK', $this->_DB->FeldInhaltFormat('N2', $Treffer->getOriginalArtikel()->getNettopreis()));
                        $Hilf->addChild('MwSt', $this->_DB->FeldInhaltFormat('N2', $Mehrwertsteuer[(int)$Hilf->MWS_ID]));
                        $Hilf->addChild('VP', $this->_DB->FeldInhaltFormat('N0', $Treffer->getOriginalArtikel()->getVPE()));
                        $Hilf->addChild('ATUVP',
                            $this->_DB->FeldInhaltFormat('N0', (float)$rsZLA->FeldInhalt('VP')));                        // 13.1.2014, neu f?r die Preisanzeige
                        $Hilf->addChild('ATUPreiseinheit', $this->_DB->FeldInhaltFormat('N0', (float)$rsZLA->FeldInhalt('VPZUKAUF')));        // 17.2.2014, neu f?r die Preisanzeige bei falschen ATU Artikeln
                        $Hilf->addChild('Preiseinheit', $this->_DB->FeldInhaltFormat('N0', $Treffer->getOriginalArtikel()->getMEH()));
                        $Hilf->addChild('VK', $this->ErmittleKundenVK($rsZLA->FeldInhalt('ZLA_KEY'), $this->_DB->FeldInhaltFormat('N2', $Treffer->getOriginalArtikel()->getNettopreis()),
                            $this->_DB->FeldInhaltFormat('N2', $Treffer->getOriginalArtikel()->getBruttopreis()), $Mehrwertsteuer[(int)$Hilf->MWS_ID]));

                        $Liefermenge = $Treffer->getOriginalArtikel()->getZentrallager();
                        $Anfragemenge = $Treffer->getOriginalArtikel()->getAnfrageMenge();
                        if ($Liefermenge >= $Anfragemenge) {
                            $VerfuegbarkeitsText = $this->_TexteVerfuegbarkeit['AvailabilityGreen'];
                        } elseif ($Liefermenge > 0) {
                            $VerfuegbarkeitsText = $this->_TexteVerfuegbarkeit['AvailabilityGreenYellow'];
                        } elseif ($Liefermenge <= 0) {
                            $VerfuegbarkeitsText = $this->_TexteVerfuegbarkeit['AvailabilityRed'];
                        }

                        $Hilf1 = $Hilf->addChild('Verkaufshaus');
                        $Hilf1->addChild('Lieferid', '');
                        $Hilf1->addChild('Liefermenge', $Liefermenge);
                        $Hilf1->addCData('Lieferanzeige', ($VerfuegbarkeitsText));

                        $Hilf = $XMLArtikelTreffer->addChild('Verfuegbarkeit');
                        $Hilf->addChild('Liefermenge', $Liefermenge);

                        $Pos++;
                    }

                } else { //Keine Artikel gefunden
                    $ArtikelTreffer = null;
                    $Fehler = $this->_Daten->addChild('Fehler');
                    $Fehler->addChild('Text', 'Fehler bei der Bestellabfrage');
                    $Fehler->addChild('Nummer', -1);
                }

            }
            file_put_contents($this->_Protokollverzeichnis . '/goehrum_anfrage.log', $this->_FIL_ID . '; Bestell-Array: ' . serialize($XMLArtikelTreffer) . "\n", FILE_APPEND);
        } catch (Exception $ex) {
            $Fehler = $this->_Daten->addChild('Fehler');
            $Fehler->addChild('Nummer', $ex->getCode());
            $Fehler->addChild('Text', $ex->getMessage());
            $Fehler->addChild('Zeile', $ex->getLine());
            $Fehler->addChild('SQL', $SQL);
        }

        return $this->_Daten;
    }

    private function _getHerstellerName($TecDocNr)
    {
        $SQL = 'Select ZLH_BEZEICHNUNG from ZUKAUFHERSTELLERCODES ';
        $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ';
        $SQL .= ' ON ZLH_KEY = ZHK_ZLH_KEY';
        $SQL .= ' WHERE (ZHK_CODE = ' . $this->_DB->WertSetzen('HLK', 'T', $TecDocNr);
        $SQL .= ' OR ZLH_TECDOCID = ' . $this->_DB->WertSetzen('HLK', 'T', $TecDocNr) . ' )';
        $SQL .= ' AND ZHK_LIE_NR = ' . $this->_DB->WertSetzen('HLK', 'T', self::LIE_NR);

        $rs = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('HLK'));

        return $rs->FeldInhalt(1);
    }

    /**
     * Bestellt die angegegeben Artikel
     *
     * @param array $Artikelliste
     * @param string $WANr
     * @param string $Versandart
     * @param string $Sachbearbeiter
     */
    public function Bestellen(awisSimpleXMLElement $Artikelliste, $WANr, $Versandart, $Sachbearbeiter = 'AWIS')
    {
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_anfrage.log', $this->_FIL_ID . '; Bestellung: ' . $WANr . "\n", FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "\n\nNeue Bestellung " . date('d.m.Y H:i:s') . " \n", FILE_APPEND);

        // Best?tigungs-Email holen
        $this->_EMAIL_BESTAETIGUNG = awisWerkzeuge::LeseAWISParameter('zukauf.conf', 'EMAIL_BESTAETIGUNG');


        $AnzPositionen = 0;

        $this->_Daten->addChild('Bestellstatus', '##1##');        // Allgemeiner Status f?r Bestellt. Wird sp?ter genauer gesetzt.
        $this->_Daten->addChild('Benutzer', $Sachbearbeiter);
        $BestellteArtikel = $this->_Daten->addChild('Positionen');
        $NichtBestellt = 0;

        //AWIS Versandarten fuer Guehrum Uebersetzen
        switch ($Versandart) {
            case 1: //Abholung
                $Versandart = Versandart::Abholung;
                break;
            case 2: //Lieferung
                $Versandart = Versandart::Standard;
                break;
            case 3: //Express. Wird von G?hrum nicht angeboten und sollte daher nicht vorkommen. F?r den Fall falls doch --> Standard
            default:
                $Versandart = Versandart::Standard;
        }

        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "\n---------------------------------------------------" . PHP_EOL, FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Versandart:" . $Versandart . PHP_EOL, FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Sachbearbeiter:" . $Sachbearbeiter . PHP_EOL, FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "---------------------------------------------------" . PHP_EOL, FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Artikelliste:" . $Artikelliste->asXML() . PHP_EOL, FILE_APPEND);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "---------------------------------------------------" . PHP_EOL, FILE_APPEND);


        //Bestellung initialisieren
        $PartnerBestellungV3 = new PartnerBestellungV3($this->_AnmeldeDaten['KDNR'], 0, new dateTime(), $Versandart);
        $PartnerBestellungV3->setBestellReferenz($WANr);


        //Artikelarray bauen
        $ArrayOfBestellpositionen = new ArrayOfBestellPosition();
        $Artikel = array();
        $Keys = array();

        $PosNr = 1;
        foreach ($Artikelliste->Artikel as $Bestellposition) {
            //file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Bestellposition:" . serialize($Bestellposition) . PHP_EOL, FILE_APPEND);
            file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Bestellnummer:" . $Bestellposition->Bestellnummer . PHP_EOL, FILE_APPEND);
            file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', "Artikelbezeichnung:" . $Bestellposition->Artikelbezeichnung . PHP_EOL, FILE_APPEND);
            $Keys[$PosNr] = $Bestellposition->Key;
            $Artikel[] = new BestellPosition($PosNr++, $Bestellposition->Hlka, $Bestellposition->Bestellnummer, $Bestellposition->Menge);


        }
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_bestellungen.log', ob_get_clean() . PHP_EOL, FILE_APPEND);

        $ArrayOfBestellpositionen->setBestellPosition($Artikel);

        $PartnerBestellungV3->setPositionen($ArrayOfBestellpositionen);

        //Bestellung aufgeben
        $Bestellung = new Bestellung($PartnerBestellungV3);


        //Antwort auswerten
        $BestellungResponse = $this->_SOAPService->Bestellung($Bestellung);

        $BestellungResponse = $BestellungResponse->getBestellungResult();
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_debug.log', "Feedback: " . $BestellungResponse->getFeedback() . PHP_EOL, FILE_APPEND);


        if ($BestellungResponse->getBestellungAngelegt()) { //Bestellung wurde angelegt
            $BestellterArtikel = $BestellteArtikel->addChild('Artikel');
            $this->_Daten->addChild('Auftragsnummer', (string)$BestellungResponse->getVerarbeitungsnummer());

            foreach ($BestellungResponse->getPositionen() as $BestPos) {

                $this->_ArtikelZusatzInfos((string)$BestPos->getEinspeiserArtikelNr(), $BestPos->getEinspeiserNr());
                $BestellterArtikel->addCData('Artikelbezeichnung', $this->_rsArtikelZusatzInfos->FeldInhalt('ZLA_BEZEICHNUNG'));
                $BestellterArtikel->addChild('Bestellnummer', (string)$BestPos->getEinspeiserArtikelNr());


                if ($BestPos->getEinspeiserNr() != '') { //TecDocArtikel?
                    $Bestellposition = (string)$BestPos->getEinspeiserArtikelNr() . '-' . (string)$BestPos->getEinspeiserNr();
                } else { //Eigenartikel
                    $Bestellposition = (string)$BestPos->getEinspeiserArtikelNr() . '-+GOE+';
                }

                $BestellterArtikel->addChild('Menge', $BestPos->getBestellMenge());
                $BestellterArtikel->addChild('Key', $Keys[$BestPos->getPositionsNr()]);
                $FeedBack = '';
                if ($BestPos->getBestellPositionAngelegt()) { //Bestellposition wurde angelegt
                    $BestellterArtikel->addChild('Liefermenge', $BestPos->getBestellMenge());
                    if (mb_strtoupper(trim($BestPos->getFeedback())) != 'INFO:') { //G�hrum liefert immer "Info:". Egal ob danach noch was kommt oder nicht
                        $FeedBack = $BestPos->getFeedback();
                    }
                    $BestellterArtikel->addChild('Rueckstand', 0);
                    $BestellterArtikel->addChild('Status', 1);
                    $this->_Daten->Bestellstatus = '##1##';
                } else { //Bestellposition konnte in Bestellung nicht angelegt werden
                    $BestellterArtikel->addChild('Liefermenge', 0);
                    $BestellterArtikel->addChild('Rueckstand', $BestPos->getBestellMenge());
                    $BestellterArtikel->addChild('Status', 2);
                    $this->_Daten->Bestellstatus = '##4##';
                    $FeedBack = 'Bestellposition wurde nicht beim Lieferanten angelegt. Feedback :' . $BestPos->getFeedback();
                }

                $BestellterArtikel->addCData('Statustext', $FeedBack);

            }

        } else { //Bestellung konnte nicht angelegt werden
            $Fehler = $this->_Daten->addChild('Fehler');
            $Fehler->addChild('Text', utf8_encode($BestellungResponse->getFeedback()));
            $Fehler->addChild('Nummer', $BestellungResponse->getVerarbeitungsnummer());
        }

        ob_start();
        var_dump($this->_Daten);
        file_put_contents($this->_Protokollverzeichnis . '/goehrum_debug.log', "Daten: " . ob_get_clean() . PHP_EOL, FILE_APPEND);

        return $this->_Daten;

    }


    /**
     * @var awisRecordset $_rsArtikelZusatzInfos
     */
    private $_rsArtikelZusatzInfos;

    private function _ArtikelZusatzInfos($Bestellnummer = '', $HerstellerCode = '')
    {
        if ($Bestellnummer != '') {
            $SQL = 'SELECT DISTINCT * ';
            $SQL .= ' FROM ZUKAUFARTIKELLIEFERANTEN';
            $SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZAL_ZLA_KEY = ZLA_KEY';
            $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLA_ZLH_KEY = ZLH_KEY';
            $SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZHK_ZLH_KEY = ZLH_KEY ';//AND ZHK_LIE_NR = \'' . self::LIE_NR . '\'';
            $SQL .= ' WHERE ZAL_LIE_NR = \'' . self::LIE_NR . '\'';
            $SQL .= ' AND ZHK_CODE = ' . $this->_DB->WertSetzen('ZAL', 'T', $HerstellerCode);
            $SQL .= ' AND ZAL_BESTELLNUMMER = ' . $this->_DB->WertSetzen('ZAL', 'T', $Bestellnummer);

            $this->_rsArtikelZusatzInfos = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ZAL'));
        }

        if (!is_a($this->_rsArtikelZusatzInfos, 'awisRecordset')) { //Kein Recordset?
            return false;
        } elseif ($this->_rsArtikelZusatzInfos->AnzahlDatensaetze() < 1) { //Nix gefunden?
            return false;
        }

        return $this->_rsArtikelZusatzInfos;

    }


}

?>