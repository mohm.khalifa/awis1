<?php
require_once("register.inc.php");
require_once("sicherheit.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("awis_forms.inc.php");		// DB-Befehle

$con = awisLogon();

$fp = fopen('/daten/webdaten/logs/access_log_AWIS','r');
if($fp!==false)
{
	$Infos=array();
	$Zeile = fgets($fp);

	while($Zeile!==false)
	{
		$Pos = strpos($Zeile,' - ');
		if($Pos!==false)
		{
			$Infos['XAL_COMPUTER']=substr($Zeile,0,$Pos);
	
			$Zeile = substr($Zeile,$Pos+3);
		
		
			$Pos = strpos($Zeile,'[');	
			if($Pos!==false)
			{
				$Infos['XAL_USER']=trim(substr($Zeile,0,$Pos));
		
				$Zeile = substr($Zeile,$Pos+1);
				
				$Pos = strpos($Zeile,' ');	
				if($Pos!==false)
				{
					$Wert = trim(substr($Zeile,0,$Pos));
					$Wert = strtotime(str_replace('/','-',substr($Wert,0,11)).' '.substr($Wert,12));
					if($Wert>-1)
					{
						$Infos['XAL_DATUM']=date('d.m.Y H:i:s',$Wert);
	
						$Pos = strpos($Zeile,']');		
						$Zeile = substr($Zeile,$Pos+3);
						
						$Pos = strpos($Zeile,' ');	
						if($Pos!==false)
						{
							$Infos['XAL_ZUGRIFF']=trim(substr($Zeile,0,$Pos));
					
							$Zeile = substr($Zeile,$Pos+1);
							
							$Pos = strpos($Zeile,' HTTP/');	
							if($Pos!==false)
							{
								$Infos['XAL_LINK']=trim(substr($Zeile,0,$Pos));
						
								$Zeile = substr($Zeile,$Pos+1);
						
								$Pos = strpos($Zeile,'"');	
								if($Pos!==false)
								{
									$Infos['XAL_REQUESTTYP']=trim(substr($Zeile,0,$Pos));
							
									$Zeile = substr($Zeile,$Pos+2);
									$Pos = strpos($Zeile,' ');	
									if($Pos!==false)
									{
										$Infos['XAL_STATUS']=intval(trim(substr($Zeile,0,$Pos)));
								
										$Zeile = substr($Zeile,$Pos+1);
										$Pos = strpos($Zeile,' ');	
										if($Pos!==false)
										{
											$Infos['XAL_GROESSE']=intval(trim(substr($Zeile,0,$Pos)));
									
											$Zeile = substr($Zeile,$Pos+1);
											
											$Rest = explode('"',$Zeile);
											$Infos['XAL_AUFRUF']=$Rest[1];
											$Infos['XAL_BROWSER']=$Rest[3];
										}
									}
								}						
							}				
						}			
					}
				}		
			}
		}
		$SQL = 'INSERT INTO accesslog(';
		$SQL .= 'XAL_COMPUTER,XAL_USER,XAL_DATUM,XAL_ZUGRIFF,XAL_LINK,XAL_REQUESTTYP,XAL_STATUS,XAL_GROESSE,XAL_AUFRUF,XAL_BROWSER';
		$SQL .= ')VALUES(';
		$SQL .= awisFeldFormat('T',$Infos['XAL_COMPUTER'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_USER'],'DB',false);
		$SQL .= ','.awisFeldFormat('DT',$Infos['XAL_DATUM'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_ZUGRIFF'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_LINK'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_REQUESTTYP'],'DB',false);
		$SQL .= ','.awisFeldFormat('Z',$Infos['XAL_STATUS'],'DB',false);
		$SQL .= ','.awisFeldFormat('Z',$Infos['XAL_GROESSE'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_AUFRUF'],'DB',false);
		$SQL .= ','.awisFeldFormat('T',$Infos['XAL_BROWSER'],'DB',false);
		$SQL .=')';
		
		awisExecute($con,$SQL);
	
		$Zeile = fgets($fp);
	}	
	
	fclose($fp);
	
	
	
//var_dump($Infos);
	
}
awisLogoff($con);

?>