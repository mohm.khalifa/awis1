<?
/**
 * Suchmaske f�r Visitenkarten
 *
 * @author Patrick Gebhardt
 * @copyright ATU
 * @version 201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS'); 
	$DB->Oeffnen();
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	//$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('VSK','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht35000=$AWISBenutzer->HatDasRecht(35000);
	if($Recht35000==0)
	{
	    $Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./visitenkarten_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	//var_dump($AWISBenutzer->BenutzerID());
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Visitenkarten'));
    
	//var_dump($Param);
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();
	
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILID'].':',190);
	    $Form->Erstelle_TextFeld('*VSK_FILIDWERT', preg_replace('/^0+/', '', $FilZugriff),25,200,false);
	    $Form->Erstelle_HiddenFeld('VSK_FILID',preg_replace('/^0+/', '', $FilZugriff));
	    $Form->ZeileEnde();
	}
	else
	{
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILID'].':',190);
	    $Form->Erstelle_TextFeld('*VSK_FILID',($Param['SPEICHERN']=='on'?$Param['VSK_FILID']:''),25,200,true);
	    $Form->ZeileEnde();
	    
	}
	/*
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_VORNAME'].':',190);
	$Form->Erstelle_TextFeld('*VSK_VORNAME',($Param['SPEICHERN']=='on'?$Param['VSK_VORNAME']:''),25,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_NACHNAME'].':',190);
	$Form->Erstelle_TextFeld('*VSK_NACHNAME',($Param['SPEICHERN']=='on'?$Param['VSK_NACHNAME']:''),25,200,true);
	$Form->ZeileEnde();
	*/
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');


	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>