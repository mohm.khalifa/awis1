<?php
require_once('awis_forms.inc.php');
global $con;
global $AWIS_KEY1;
global $AWISSprache;
global $AWISBenutzer;
global $SpeichernButton;

	//*******************************************
	// Nachbarfilialen
	//*******************************************
$TXT_Baustein = array();
$TXT_Baustein[]=array('CGK','%');
$TXT_Baustein[]=array('Wort','lbl_weiter');
$TXT_Baustein[]=array('Wort','lbl_speichern');
$TXT_Baustein[]=array('Wort','lbl_trefferliste');
$TXT_Baustein[]=array('Wort','txt_BitteWaehlen');
$TXT_Baustein[]=array('Liste','lst_TelefonischPersoenlich');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);
//awis_Debug(1,$AWIS_KEY1);

$Recht121 = awisBenutzerRecht($con,121);

if(!$AWISBenutzer->FilialZugriff($AWIS_KEY1) AND (($Recht121&4)==0))
{
     die("Keine ausreichenden Rechte!");
}

// Beschränkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
//var_dump($UserFilialen);

if($UserFilialen!='' AND intval($AWIS_KEY1) <> intval($UserFilialen))
{
	echo ("<span class=HinweisText>Sie haben keine Berechtigung die ATU - Karten Daten von dieser Filiale anzusehen.</span>");
	die ();
}

$KEY = (isset($_GET['CGKKEY'])?$_GET['CGKKEY']:(isset($_POST['txtCGK_KEY'])?$_POST['txtCGK_KEY']:''));

$SpeichernButton = false;
$EditRech=false;

if(($Recht121 & 16)==16)
{
	$SpeichernButton = true;
	$EditRecht=true;
}

if(isset($_POST['cmdSpeichern_x']))
{
	include './filialinfo_speichern.php';
}


$SQL = 'SELECT * FROM';
$SQL.= ' (SELECT CRM.*';
$SQL.= ', CASE WHEN SUBSTR(CGK_NACHNAME,1,3)=\'***\' THEN CGK_FIRMA1 ';
$SQL.= '       WHEN CGK_VORNAME IS NULL THEN \'KZ: \' || CGK_NACHNAME ';
$SQL.= '       ELSE CGK_NACHNAME ';
$SQL.= '  END AS ANZEIGENAME';
$SQL.= ', DECODE(CGK_KONTAKTPERSONANREDE_ID, 1, \'Herr\', 2, \'Frau\', \'\') ||';
$SQL.= ' NVL2(CGK_KONTAKTPERSONTITEL, \' \' || CGK_KONTAKTPERSONTITEL, \'\') ||';
$SQL.= ' NVL2(CGK_KONTAKTPERSONVN, \' \' || CGK_KONTAKTPERSONVN, \'\') ||';
$SQL.= ' NVL2(CGK_KONTAKTPERSONNN, \' \' || CGK_KONTAKTPERSONNN, \'\')  AS ANSPRECHPARTNER';
$SQL.= ' , CGK_HAUSNR || \' \' || CGK_HNR2 as HAUSNUMMER';
$SQL.= ', CGK_FIRMA1 ||';
$SQL.= ' NVL2(CGK_FIRMA2, \' \' || CGK_FIRMA2, \'\') ||';
$SQL.= ' NVL2(CGK_FIRMA3, \' \' || CGK_FIRMA3, \'\')  AS FIRMA';
$SQL.= ' , (SELECT MAX(CGK_LASTTRANSACTIONDATE) FROM CRMGewerbekunden';
$SQL.= ' WHERE CGK_ACCOUNT_ID = CRM.CGK_ACCOUNT_ID) AS MAXLASTTRANSACTIONDATE';
$SQL.= ' , (SELECT MAX(CGK_LASTPRODUCTIONDATE) FROM CRMGewerbekunden';
$SQL.= ' WHERE CGK_ACCOUNT_ID = CRM.CGK_ACCOUNT_ID) AS MAXLASTPRODUCTIONDATE';
$SQL.= ' FROM CRMGewerbekunden CRM';
$SQL.= ' WHERE CGK_ORIG_STORE_ID=0'.$AWIS_KEY1;
$SQL.= ' AND CGK_ACCOUNT_TY = 3';
$SQL.= ' AND CGK_DBM_KARTENART = \'HK\'';
//$SQL.= ' AND CGK_ISCARDREPLACED = \'N\'';
$SQL.= ' AND CGK_PLANTDATE >= (SYSDATE - 100)';
//$SQL.= ' AND ((NVL(CGK_KONTAKT,0) <> 1) OR (NVL(CGK_KONTAKT,0) = 1 AND CGK_USERDATFILIALE >= (SYSDATE - 5)))';
$SQL.= ' AND CGK_CUST_TY_ID = 3';
$SQL.= ' AND CGK_RECSTATE = \'N\')';
$SQL.= ' WHERE MAXLASTTRANSACTIONDATE IS NULL';
$SQL.= ' AND MAXLASTPRODUCTIONDATE IS NOT NULL';
$SQL.= ' AND MAXLASTPRODUCTIONDATE <= (SYSDATE - 5)';
//$SQL .= ' OR (CGK_AC_DBM_UPDATE_DT > (SYSDATE - 300))';
//$SQL .= ' AND (CGK_AC_DBM_UPDATE_DT > (SYSDATE - 300)';
//$SQL .= ' AND CGK_PLANTDATE <= (SYSDATE - 5)';
//$SQL .= ' AND CGK_OPAL_KARTENART IN (\'HK\',\'ZK\')';


if($KEY!='')
{
	$SQL .= ' AND CGK_KEY = 0'.awis_FeldInhaltFormat('Z',$KEY);
}
if(!isset($_GET['CGKSort']))
{
	$SQL .= ' ORDER BY CGK_FIRST_CONTACT_DT DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CGKSort']);
}
awis_Debug(1, $SQL);
$rsCGK = awisOpenRecordset($con,$SQL);
$rsCGKZeilen = $awisRSZeilen;

if($KEY=='' OR isset($_GET['CGKListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	$Link = './filialinfo_Main.php?cmdAktion=Details&Seite=Karten';
	$Link .= '&CGKSort=CGK_FIRST_CONTACT_DT'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_FIRST_CONTACT_DT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRST_CONTACT_DT'],120,'',$Link);
	$Link = './filialinfo_Main.php?cmdAktion=Details&Seite=Karten';
	$Link .= '&CGKSort=CGK_FIRMA1'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_FIRMA1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRMA'],320,'',$Link);
	$Link = './filialinfo_Main.php?cmdAktion=Details&Seite=Karten';
	$Link .= '&CGKSort=CGK_VERTRAGSART'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_VERTRAGSART'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_VERTRAGSART'],300,'',$Link);

	awis_FORM_ZeileEnde();

	for($CGKZeile=0;$CGKZeile<$rsCGKZeilen;$CGKZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		$Icons[] = array('edit','./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Karten&CGKKEY='.$rsCGK['CGK_KEY'][$CGKZeile]);
		awis_FORM_Erstelle_ListeIcons($Icons,20,($CGKZeile%2));

		$ToolTipp='';
		//$ToolTipp=(isset($rsCGK['CGK_LOCK_REASON'][0])?$AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].': '.$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:'');
		//$ToolTipp.=($rsCGK['CGK_CARDBLOCKSTATUS'][$CGKZeile]!=1?($ToolTipp==''?'':'/').$AWISSprachKonserven['CGK']['CGK_CARDBLOCKSTATUS_TEXT'].': '.$rsCGK['CGK_CARDBLOCKSTATUS_TEXT'][$CGKZeile]:'');
		//awis_Debug(1,$rsCGK['CGK_LOCK_REASON'][0], $rsCGK['CGK_CARDLOCKREASON_TEXT'][0], $rsCGK['CGK_CARDBLOCKSTATUS'][$CGKZeile], $ToolTipp);
		//awis_FORM_Erstelle_ListenFeld('#CGK_FIRST_CONTACT_DT',$rsCGK['CGK_FIRST_CONTACT_DT'][$CGKZeile],20,100,false,($CGKZeile%2),($ToolTipp==''?'':'color:red;'),'','D','',$ToolTipp);
		awis_FORM_Erstelle_ListenFeld('#CGK_PLANTDATE',$rsCGK['CGK_PLANTDATE'][$CGKZeile],20,100,false,($CGKZeile%2),($ToolTipp==''?'':'color:red;'),'','D','',$ToolTipp);
		awis_FORM_Erstelle_ListenFeld('#FIRMA1',$rsCGK['CGK_FIRMA1'][$CGKZeile],20,320,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_VERTRAGSART',$rsCGK['CGK_VERTRAGSART'][$CGKZeile],20,300,false,($CGKZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCGK_KEY type=hidden value=0'.(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'').'>';
	echo '<input name=txtCGK_CGK_KEY type=hidden value=0'.$AWIS_KEY1.'>';

	$AWIS_KEY2 = (isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Karten&CGKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_AC_DBM_UPDATE_DT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ACCOUNT_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ACCOUNT_ID',(isset($rsCGK['CGK_ACCOUNT_ID'][0])?$rsCGK['CGK_ACCOUNT_ID'][0]:''),10,450,false,'','','','T');

	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_KONTAKT'].':',150);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_TelefonischPersoenlich']);
	awis_FORM_Erstelle_SelectFeld('CGK_KONTAKT_'.$rsCGK['CGK_KEY'][0],(isset($rsCGK['CGK_KONTAKT'][0])?$rsCGK['CGK_KONTAKT'][0]:''),200,$EditRecht,$con,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLANTDATE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PLANTDATE',(isset($rsCGK['CGK_PLANTDATE'][0])?$rsCGK['CGK_PLANTDATE'][0]:''),10,450,false,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_BEMERKUNG'].':',150);

	awis_FORM_Erstelle_Textarea('CGK_BEMERKUNG_'.$rsCGK['CGK_KEY'][0],(isset($rsCGK['CGK_BEMERKUNG'][0])?$rsCGK['CGK_BEMERKUNG'][0]:''),300,50,2,$EditRecht,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_TITLE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_TITLE_1',(isset($rsCGK['CGK_TITLE_1'][0])?$rsCGK['CGK_TITLE_1'][0]:''),10,400,false,'','','','T');
	//awis_Debug(1,$rsCGK['CGK_FIRMA1'][0]);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRMA'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRMA',(isset($rsCGK['FIRMA'][0])?$rsCGK['FIRMA'][0]:''),10,400,false,'','','','T');
	//awis_Debug(1,$rsCGK['CGK_FIRMA1'][0]);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_STRASSE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_STRASSE',(isset($rsCGK['CGK_STRASSE'][0])?$rsCGK['CGK_STRASSE'][0]:''),10,300,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_HAUSNUMMER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_HAUSNUMMER',(isset($rsCGK['HAUSNUMMER'][0])?$rsCGK['HAUSNUMMER'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLZ'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PLZ',(isset($rsCGK['CGK_PLZ'][0])?$rsCGK['CGK_PLZ'][0]:''),10,300,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ORT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ORT',(isset($rsCGK['CGK_ORT'][0])?$rsCGK['CGK_ORT'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PHONE_H'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PHONE_H',(isset($rsCGK['CGK_PHONE_H'][0])?$rsCGK['CGK_PHONE_H'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PHONE_M'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PHONE_M',(isset($rsCGK['CGK_PHONE_M'][0])?$rsCGK['CGK_PHONE_M'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	$Link = ($rsCGK['CGK_EMAIL'][0]!=''?'mailto:'.$rsCGK['CGK_EMAIL'][0]:'');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_EMAIL'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_EMAIL',(isset($rsCGK['CGK_EMAIL'][0])?$rsCGK['CGK_EMAIL'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ANSPRECHPARTNER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ANSPRECHPARTNER',(isset($rsCGK['ANSPRECHPARTNER'][0])?$rsCGK['ANSPRECHPARTNER'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	//awis_FORM_ZeileStart();
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRSTTRANSACTIONDATE'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_FIRSTTRANSACTIONDATE',(isset($rsCGK['CGK_FIRSTTRANSACTIONDATE'][0])?$rsCGK['CGK_FIRSTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LASTTRANSACTIONDATE'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_LASTTRANSACTIONDATE',(isset($rsCGK['CGK_LASTTRANSACTIONDATE'][0])?$rsCGK['CGK_LASTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	//awis_FORM_ZeileEnde();
	//
	//awis_FORM_ZeileStart();
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLIMIT'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_CARDLIMIT',(isset($rsCGK['CGK_CARDLIMIT'][0])?$rsCGK['CGK_CARDLIMIT'][0]:''),10,120,false,'','','','N0');
	//awis_FORM_ZeileEnde();
	//
	//awis_FORM_ZeileStart();
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_SALES_STORE_ID'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_SALES_STORE_ID',(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:''),10,120,false,'','','','T');
	//$SQL = 'SELECT FIL_BEZ FROM FILIALEN WHERE FIL_ID=0'.(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:'');
	//$rsFIL = awisOpenRecordset($con, $SQL);
	//awis_FORM_Erstelle_TextFeld('*FIL_BEZ',(isset($rsFIL['FIL_BEZ'][0])?$rsFIL['FIL_BEZ'][0]:''),10,300,false,'','','','T');
	//awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_DT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LOCK_DT',(isset($rsCGK['CGK_LOCK_DT'][0])?$rsCGK['CGK_LOCK_DT'][0]:''),10,120,false,'','','','D');
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_REASON'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_LOCK_REASON',(isset($rsCGK['CGK_LOCK_REASON'][0])?$rsCGK['CGK_LOCK_REASON'][0]:''),10,120,false,'','','','T');
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].':',160);
	//awis_FORM_Erstelle_TextFeld('*CGK_CARDLOCKREASON_TEXT',(isset($rsCGK['CGK_CARDLOCKREASON_TEXT'][0])?$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();


	awis_FORM_FormularEnde();
}
?>