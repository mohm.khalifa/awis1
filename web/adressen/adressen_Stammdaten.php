<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $awisRSZeilen;
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

try 
{
    $TextKonserven = array();
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Wort','lbl_speichern');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht3002 = $AWISBenutzer->HatDasRecht(3002);
    if($Recht3002==0)
    {
        awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }
    
    $Form->Formular_Start();
    $EditRecht=(($Recht3002&6)!=0);
    
    if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include('./adressen_stammdaten_loeschen.php');
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('./adressen_stammdaten_speichern.php');
    }
    
    $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Infos'));
    echo '<input type=hidden name=Seite value='. $Form->Format('T',$RegisterSeite,'DB',false).'>';
    $Register = new awisRegister(3002);
    $Register->ZeichneRegister($RegisterSeite);
    $Form->Formular_Ende();
    
    if($AWISCursorPosition!='')
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
        echo '</Script>';
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>