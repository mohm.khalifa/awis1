<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once('jpgraph/jpgraph_barcode.php');

global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Rechtestufe = awisBenutzerRecht($con, 1700);
		//  1=Einsehen
		//	2=Hinzuf�gen
		//	4=Bearbeiten
		//  8=L�schen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Sonderausweis',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$Gruppenrecht = awisBenutzerRecht($con, 1701);
		//  1=externe Abrechnungen
		//	2=Zeitarbeitsfirmen
		//	4=externe Reinigungsfirmen
		//  8=Sonderausweise
		//  16=Sonstige Filialen
		//  32=Besucher/Lieferanten
		//  64=PDF erstellen		
		//  128=Zeitarbeitsfirmen Ausland
		//  256=externe Abrechnungen 2
		
if($Gruppenrecht==0)
{
    awisEreignis(3,1000,'Sonderausweis_Gruppenrecht',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Gruppenrechte!</span>");
}

static $EAN_ZUSATZ='288888';

awis_Debug(1,$_REQUEST);

if(!(isset($_REQUEST['txtSAW_KEY'])) || $_REQUEST['txtSAW_KEY']=='')
{
	$txtSAWKEY = intval(substr(awis_NameInArray($_POST,'cmdBearbeiten_'),14));
}
else
{
	$txtSAWKEY=$_REQUEST['txtSAW_KEY'];
}


if(isset($_POST['cmdTrefferliste_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSAWKEY=0;
}

if(isset($_REQUEST['txtPer_Nr']) && $_REQUEST['txtPer_Nr']!='')
{
	$PerNr=$_REQUEST['txtPer_Nr'];
}


if(isset($_POST['cmdLoeschen_x']) && $_REQUEST['txtSAW_KEY_loeschen']!='')
{
	$rsSonderausweis_Loeschen = awisOpenRecordset($con, "SELECT * FROM SONDERAUSWEISE WHERE SAW_KEY=". $_REQUEST['txtSAW_KEY_loeschen'] ."");
	$rsSonderausweis_LoeschenZeilen = $awisRSZeilen;
	
	if($rsSonderausweis_LoeschenZeilen==0)
	{
		awisLogoff($con);
		die();
	}
	else
	{
		$SQL="INSERT INTO SONDERAUSWEISEHISTORIE (SAH_PER_NR, SAH_PER_NR_ALT, ";
		$SQL.="SAH_FIL_ID, SAH_BEREICH, SAH_ANR_ID, SAH_FIRMA, SAH_NAME, SAH_VORNAME, ";
		$SQL.="SAH_STRASSE, SAH_LAN_CODE, SAH_PLZ, SAH_ORT, SAH_EINTRITT, SAH_AUSTRITT, ";
		$SQL.="SAH_TAETIGKEIT, SAH_SAG_KEY, SAH_GEBURTSDATUM, SAH_TELEFON, SAH_MOBIL, SAH_FAX, ";
		$SQL.="SAH_EMAIL, SAH_GUELTIGAB, SAH_GUELTIGBIS, SAH_EAN, SAH_USER, SAH_USERDAT, ";
		$SQL.="SAH_ZEIT_TAG, SAH_ZEITKONTO, SAH_AKTIV, SAH_KOSTENSTELLE, SAH_BEMERKUNG, SAH_LOESCHUSER, SAH_LOESCHUSERDAT) ";
		$SQL.="VALUES ('".$rsSonderausweis_Loeschen['SAW_PER_NR'][0]."', '".$rsSonderausweis_Loeschen['SAW_PER_NR_ALT'][0]."', '".$rsSonderausweis_Loeschen['SAW_FIL_ID'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_BEREICH'][0]."', ".$rsSonderausweis_Loeschen['SAW_ANR_ID'][0].", '".$rsSonderausweis_Loeschen['SAW_FIRMA'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_NAME'][0]."', '".$rsSonderausweis_Loeschen['SAW_VORNAME'][0]."', '".$rsSonderausweis_Loeschen['SAW_STRASSE'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_LAN_CODE'][0]."', '".$rsSonderausweis_Loeschen['SAW_PLZ'][0]."', '".$rsSonderausweis_Loeschen['SAW_ORT'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_EINTRITT'][0]."', '".$rsSonderausweis_Loeschen['SAW_AUSTRITT'][0]."', '".$rsSonderausweis_Loeschen['SAW_TAETIGKEIT'][0]."',";
		$SQL.="".$rsSonderausweis_Loeschen['SAW_SAG_KEY'][0].", '".$rsSonderausweis_Loeschen['SAW_GEBURTSDATUM'][0]."', '".$rsSonderausweis_Loeschen['SAW_TELEFON'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_MOBIL'][0]."', '".$rsSonderausweis_Loeschen['SAW_FAX'][0]."', '".$rsSonderausweis_Loeschen['SAW_EMAIL'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_GUELTIGAB'][0]."', '".$rsSonderausweis_Loeschen['SAW_GUELTIGBIS'][0]."', '".$rsSonderausweis_Loeschen['SAW_EAN'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_USER'][0]."', '".$rsSonderausweis_Loeschen['SAW_USERDAT'][0]."', '".$rsSonderausweis_Loeschen['SAW_ZEIT_TAG'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_ZEITKONTO'][0]."', '".$rsSonderausweis_Loeschen['SAW_AKTIV'][0]."', '".$rsSonderausweis_Loeschen['SAW_KOSTENSTELLE'][0]."',";
		$SQL.="'".$rsSonderausweis_Loeschen['SAW_BEMERKUNG'][0]."', '".$AWISBenutzer->BenutzerName()."', SYSDATE)";
		
		awis_Debug(1,$SQL);
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("sonderausweis_Detail.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
		else
		{
			$txtSAWKEY=0;
			awis_BenutzerParameterSpeichern($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName(),"");
			
			if(awisExecute($con, "DELETE FROM SONDERAUSWEISE WHERE SAW_KEY=".$_REQUEST['txtSAW_KEY_loeschen'])===FALSE)
			{
				awisErrorMailLink("sonderausweis_Detail.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
			
		}
	}
}

if(isset($_POST['cmdSpeichern_x']))
{
	$EAN_CLASS= new BarcodeEncode_EAN13(); // Erzeugen der EAN-Nummer bzw. Pr�fung der Personal-Nummer

	$EAN_NR=$EAN_ZUSATZ.$PerNr;

	if($EAN_CLASS->Validate($EAN_NR))
	{
	$EAN_CODE=$EAN_CLASS->Enc($EAN_NR);
	$EAN_SAVE=$EAN_CODE->iData;
	}
	else
	{
		die("<span class=HinweisText>Ung&uuml;ltige EAN-Nummer -> ". $EAN_NR ."! Pr&uuml;fen sie die Personal-Nummer!</span>");
	}
	
	if($_REQUEST['txtAWGruppe']!='') // Pr�fung der Ausweisgruppe und der Personalnummern G�ltigkeit
	{
		$rsPersNrValid = awisOpenRecordset($con, "SELECT * FROM SONDERAUSWEISGRUPPEN WHERE SAG_KEY=". $_REQUEST['txtAWGruppe'] ."");
		$rsPersNrValidZeilen = $awisRSZeilen;
		
		if(!($PerNr>=$rsPersNrValid['SAG_NUMMERNKREISVON'][0] && $PerNr<=$rsPersNrValid['SAG_NUMMERNKREISBIS'][0]))
		{
			unset($rsPersNrValid);
			die("<span class=HinweisText>Die Personal-Nummer ist ung&uuml;tig! ".$PerNr." ".$rsPersNrValid['SAG_NUMMERNKREISVON'][0]."-".$rsPersNrValid['SAG_NUMMERNKREISBIS'][0]."</span>");
		}
		unset($rsPersNrValid);
	}
	else 
	{
		die("<span class=HinweisText>Keine Ausweisgruppe angegeben!</span>");
	}
	
	if($_REQUEST['txtPer_Nr_alt']!='') // Pr�fung der alten Personalnummer asl Numeric-Wert und Stellenanzahl von 6-7
	{
		if(!is_numeric($_REQUEST['txtPer_Nr_alt']) || (strlen($_REQUEST['txtPer_Nr_alt'])<6 || strlen($_REQUEST['txtPer_Nr_alt'])>7))
		{
			die("<span class=HinweisText>Personal-Nr. ALT ist ung&uuml;trig!</span>");
		}
	}
	
	if($_REQUEST['txtFil_ID']!='')
	{
		if(!is_numeric($_REQUEST['txtFil_ID'])) // Pr�fen der Filial-Nummer auf Numeric-Wert
		{
			die("<span class=HinweisText>Filial-Nummer ist ung&uuml;trig!</span>");
		}
	}
	
	if($_REQUEST['txtName']=='')
	{
		die("<span class=HinweisText>Kein Name angegeben!</span>");	
	}
	
	if($_REQUEST['txtVorname']=='')
	{
		die("<span class=HinweisText>Kein Vorname angegeben!</span>");	
	}
	
	if($_REQUEST['txtEintritt']=='')
	{
		die("<span class=HinweisText>Kein Eintrittsdatum angegeben!</span>");	
	}
	
	if(($_REQUEST['txtTaetigkeitNeu']!='' && $_REQUEST['txtTaetigkeit']!=''))// || ($_REQUEST['txtTaetigkeitNeu']=='' && $_REQUEST['txtTaetigkeit']=='')) // Pr�fung ob T�tigkeit angegeben
	{
		die("<span class=HinweisText>Keine eindeutige Auswahl oder keine T&auml;tigkeit angegeben!</span>");
	}
	else
	{
		if($_REQUEST['txtTaetigkeitNeu']!='')
		{
			$Taetigkeit=$_REQUEST['txtTaetigkeitNeu'];
		}
		elseif($_REQUEST['txtTaetigkeit']!='')
		{
			$Taetigkeit=$_REQUEST['txtTaetigkeit'];
		}
		else 
		{
			$Taetigkeit='';
		}
	}
	
	
	/*
	E-Mail - G�ltigkeit pr�fen
	if (!eregi("^[a-zA-Z0-9]*[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$",$_REQUEST['txtEMail']))
	{	
		die("<span class=HinweisText>Keine g&uuml;ltige E-Mail Adressen angegeben!</span>");
	}
	*/
	
	// BenutzerParameter setzen f�r Detail-Maske bei Hinzuf�gen
	
	$Params[0]=$PerNr;
	$Params[1]=$_REQUEST['txtPer_Nr_alt'];
	$Params[2]=$_REQUEST['txtFil_ID'];
	$Params[3]=$_REQUEST['txtBereich'];
	$Params[4]=$_REQUEST['txtAnrede'];
	$Params[5]=str_replace("'","''",$_REQUEST['txtFirma']);
	$Params[6]=str_replace("'","''",$_REQUEST['txtName']);
	$Params[7]=str_replace("'","''",$_REQUEST['txtVorname']);
	$Params[8]=str_replace("'","''",$_REQUEST['txtStra�e']);
	$Params[9]=$_REQUEST['txtLAN_CODE'];
	$Params[10]=$_REQUEST['txtPLZ'];
	$Params[11]=str_replace("'","''",$_REQUEST['txtOrt']);
	$Params[12]=awis_format($_REQUEST['txtEintritt'],'Datum');
	$Params[13]=awis_format($_REQUEST['txtAustritt'],'Datum');
	$Params[14]=$_REQUEST['txtTaetigkeitNeu'];
	$Params[15]=awis_format($_REQUEST['txtGebDatum'],'Datum');
	$Params[16]=$_REQUEST['txtTelefon'];
	$Params[17]=$_REQUEST['txtMobil'];
	$Params[18]=$_REQUEST['txtFax'];
	$Params[19]=$_REQUEST['txtEMail'];
	$Params[20]=awis_format($_REQUEST['txtGueltigAb'],'Datum');
	$Params[21]=awis_format($_REQUEST['txtGueltigBis'],'Datum');
	$Params[22]=$_REQUEST['txtAWGruppe'];
	$Params[23]=$_REQUEST['txtTaetigkeit'];
	//$Params[24]=str_replace(',','.',$_REQUEST['txtZeitTag']);
	$Params[24]=($_REQUEST['txtZeitTag']==''?'NULL':str_replace(',','.',$_REQUEST['txtZeitTag']));
	$Params[25]=$_REQUEST['txtZeitkonto'];
	$Params[26]=$_REQUEST['txtAktiv'];
	$Params[27]=substr($_REQUEST['txtBemerkung'],0,1000);
	$Params[28]=$_REQUEST['txtKostenstelle'];
	
	awis_BenutzerParameterSpeichern($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName(), implode(';',$Params));
		
	if($txtSAWKEY>0) // UPDATE - Anweisung
	{
		//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
		//--->

		$SQL='SELECT * FROM SONDERAUSWEISE WHERE SAW_KEY=0'.$txtSAWKEY;
		$rsSonderausweis = awisOpenRecordset($con,$SQL);
		$rsSonderausweisZeilen = $awisRSZeilen;
		
		$SQL='';
		$txtHinweis='';
		
		if($rsSonderausweisZeilen==0)		// Keine Daten
		{
				awislogoff($con);
				die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
		}
		
		if($_POST['txtPer_Nr'] != $_POST['txtPer_Nr_old'])
		{
			
				if($rsSonderausweis['SAW_PER_NR'][0] != $_POST['txtPer_Nr_old'])
				{
						$txtHinweis.=',Personalnummer von '.$_POST['txtPer_Nr_old']. ' auf '.$rsSonderausweis['SAW_PER_NR'][0];
				}
				else
				{
						$SQL.=',SAW_PER_NR=\''.$_POST['txtPer_Nr'].'\'';
				}
		}
		
		if($_POST['txtPer_Nr_alt'] != $_POST['txtPer_Nr_alt_old'])
		{
			
				if($rsSonderausweis['SAW_PER_NR_ALT'][0] != $_POST['txtPer_Nr_alt_old'])
				{
						$txtHinweis.=',Personalnummer ALT von '.$_POST['txtPer_Nr_alt_old']. ' auf '.$rsSonderausweis['SAW_PER_NR_ALT'][0];
				}
				else
				{
						$SQL.=',SAW_PER_NR_ALT=\''.$_POST['txtPer_Nr_alt'].'\'';
				}
		}
		
		if($_POST['txtFil_ID'] != $_POST['txtFil_ID_old'])
		{
			
				if($rsSonderausweis['SAW_FIL_ID'][0] != $_POST['txtFil_ID_old'])
				{
						$txtHinweis.=',Filialnummer von '.$_POST['txtFil_ID_old']. ' auf '.$rsSonderausweis['SAW_FIL_ID'][0];
				}
				else
				{
						$SQL.=',SAW_FIL_ID='.($_POST['txtFil_ID']==''?'NULL':$_POST['txtFil_ID']);
				}
		}
		
		if($_POST['txtBereich'] != $_POST['txtBereich_old'])
		{
			
				if($rsSonderausweis['SAW_BEREICH'][0] != $_POST['txtBereich_old'])
				{
						$txtHinweis.=',Bereich von '.$_POST['txtBereich_old']. ' auf '.$rsSonderausweis['SAW_BEREICH'][0];
				}
				else
				{
						$SQL.=',SAW_BEREICH=\''.$_POST['txtBereich'].'\'';
				}
		}
		
		if($_POST['txtAnrede'] != $_POST['txtAnrede_old'])
		{
			
				if($rsSonderausweis['SAW_ANR_ID'][0] != $_POST['txtAnrede_old'])
				{
						$txtHinweis.=',Anrede von '.$_POST['txtAnrede_old']. ' auf '.$rsSonderausweis['SAW_ANR_ID'][0];
				}
				else
				{
						$SQL.=',SAW_ANR_ID='.$_POST['txtAnrede'];
				}
		}
		
		if($_POST['txtFirma'] != $_POST['txtFirma_old'])
		{
				if($rsSonderausweis['SAW_FIRMA'][0] != $_POST['txtFirma_old'])
				{
						$txtHinweis.=',Frima von '.$_POST['txtFirma_old']. ' auf '.$rsSonderausweis['SAW_FIRMA'][0];
				}
				else
				{
						$SQL.=',SAW_FIRMA=\''.str_replace("'","''",$_POST['txtFirma']).'\'';
				}
		}
		
		if($_POST['txtName'] != $_POST['txtName_old'])
		{
				if($rsSonderausweis['SAW_NAME'][0] != $_POST['txtName_old'])
				{
						$txtHinweis.=',Name von '.$_POST['txtName_old']. ' auf '.$rsSonderausweis['SAW_NAME'][0];
				}
				else
				{
						$SQL.=',SAW_NAME=\''.str_replace("'","''",$_POST['txtName']).'\'';
				}
		}
		
		if($_POST['txtVorname'] != $_POST['txtVorname_old'])
		{
				if($rsSonderausweis['SAW_VORNAME'][0] != $_POST['txtVorname_old'])
				{
						$txtHinweis.=',Vorname von '.$_POST['txtVorname_old']. ' auf '.$rsSonderausweis['SAW_VORNAME'][0];
				}
				else
				{
						$SQL.=',SAW_VORNAME=\''.str_replace("'","''",$_POST['txtVorname']).'\'';
				}
		}
		
		if($_POST['txtStra�e'] != $_POST['txtStra�e_old'])
		{
				if($rsSonderausweis['SAW_STRASSE'][0] != $_POST['txtStra�e_old'])
				{
						$txtHinweis.=',Stra�e von '.$_POST['txtStra�e_old']. ' auf '.$rsSonderausweis['SAW_STRASSE'][0];
				}
				else
				{
						$SQL.=',SAW_STRASSE=\''.str_replace("'","''",$_POST['txtStra�e']).'\'';
				}
		}
		
		if($_POST['txtLAN_CODE'] != $_POST['txtLAN_CODE_old'])
		{
				if($rsSonderausweis['SAW_LAN_CODE'][0] != $_POST['txtLAN_CODE_old'])
				{
						$txtHinweis.=',Landeskennzeichen von '.$_POST['txtLAN_CODE_old']. ' auf '.$rsSonderausweis['SAW_LAN_CODE'][0];
				}
				else
				{
						$SQL.=',SAW_LAN_CODE=\''.$_POST['txtLAN_CODE'].'\'';
				}
		}
		
		if($_POST['txtPLZ'] != $_POST['txtPLZ_old'])
		{
				if($rsSonderausweis['SAW_PLZ'][0] != $_POST['txtPLZ_old'])
				{
						$txtHinweis.=',PLZ von '.$_POST['txtPLZ_old']. ' auf '.$rsSonderausweis['SAW_PLZ'][0];
				}
				else
				{
						$SQL.=',SAW_PLZ=\''.$_POST['txtPLZ'].'\'';
				}
		}
		
		if($_POST['txtOrt'] != $_POST['txtOrt_old'])
		{
				if($rsSonderausweis['SAW_ORT'][0] != $_POST['txtOrt_old'])
				{
						$txtHinweis.=',Ort von '.$_POST['txtOrt_old']. ' auf '.$rsSonderausweis['SAW_ORT'][0];
				}
				else
				{
						$SQL.=',SAW_ORT=\''.str_replace("'","''",$_POST['txtOrt']).'\'';
				}
		}
		
		if($_POST['txtEintritt'] != $_POST['txtEintritt_old'])
		{
				if($rsSonderausweis['SAW_EINTRITT'][0] != $_POST['txtEintritt_old'])
				{
						$txtHinweis.=',Eintritt von '.$_POST['txtEintritt_old']. ' auf '.$rsSonderausweis['SAW_EINTRITT'][0];
				}
				else
				{
						$SQL.=',SAW_EINTRITT=to_date(\''.$_POST['txtEintritt'].'\',\'DD.MM.RR\')';
				}
		}

		if($_POST['txtAustritt'] != $_POST['txtAustritt_old'])
		{
				if($rsSonderausweis['SAW_AUSTRITT'][0] != $_POST['txtAustritt_old'])
				{
						$txtHinweis.=',Austritt von '.$_POST['txtAustritt_old']. ' auf '.$rsSonderausweis['SAW_AUSTRITT'][0];
				}
				else
				{
						$SQL.=',SAW_AUSTRITT=to_date(\''.$_POST['txtAustritt'].'\',\'DD.MM.RR\')';
				}
		}
		
		if($_POST['txtAWGruppe'] != $_POST['txtAWGruppe_old'])
		{	
			if($rsSonderausweis['SAW_SAG_KEY'][0] != $_POST['txtAWGruppe_old'])
			{
					$txtHinweis.=',Ausweisgruppe von '.$_POST['txtAWGruppe_old']. ' auf '.$rsSonderausweis['SAW_SAG_KEY'][0];
			}
			else
			{
					$SQL.=',SAW_SAG_KEY='.$_POST['txtAWGruppe'];
			}
		}
		
		if($Taetigkeit != $_POST['txtTaetigkeit_old'])
		{
			if($rsSonderausweis['SAW_TAETIGKEIT'][0] != $_POST['txtTaetigkeit_old'])
			{
					$txtHinweis.=',T&auml;tigkeit von '.$_POST['txtTaetigkeit_old']. ' auf '.$rsSonderausweis['SAW_TAETIGKEIT'][0];
			}
			else
			{
					$SQL.=',SAW_TAETIGKEIT=\''.$Taetigkeit.'\'';
			}
		}
		
		if($_POST['txtGebDatum'] != $_POST['txtGebDatum_old'])
		{
				if($rsSonderausweis['SAW_GEBURTSDATUM'][0] != $_POST['txtGebDatum_old'])
				{
						$txtHinweis.=',Geburtsdatum von '.$_POST['txtGebDatum_old']. ' auf '.$rsSonderausweis['SAW_GEBURTSDATUM'][0];
				}
				else
				{
						$SQL.=',SAW_GEBURTSDATUM=to_date(\''.$_POST['txtGebDatum'].'\',\'DD.MM.RR\')';
				}
		}
		
		if($_POST['txtTelefon'] != $_POST['txtTelefon_old'])
		{
				if($rsSonderausweis['SAW_TELEFON'][0] != $_POST['txtTelefon_old'])
				{
						$txtHinweis.=',Telefon von '.$_POST['txtTelefon_old']. ' auf '.$rsSonderausweis['SAW_TELEFON'][0];
				}
				else
				{
						$SQL.=',SAW_TELEFON=\''.$_POST['txtTelefon'].'\'';
				}
		}
		
		if($_POST['txtMobil'] != $_POST['txtMobil_old'])
		{
				if($rsSonderausweis['SAW_MOBIL'][0] != $_POST['txtMobil_old'])
				{
						$txtHinweis.=',Mobil von '.$_POST['txtMobil_old']. ' auf '.$rsSonderausweis['SAW_MOBIL'][0];
				}
				else
				{
						$SQL.=',SAW_MOBIL=\''.$_POST['txtMobil'].'\'';
				}
		}
		
		if($_POST['txtFax'] != $_POST['txtFax_old'])
		{
				if($rsSonderausweis['SAW_FAX'][0] != $_POST['txtFax_old'])
				{
						$txtHinweis.=',FAX von '.$_POST['txtFax_old']. ' auf '.$rsSonderausweis['SAW_FAX'][0];
				}
				else
				{
						$SQL.=',SAW_FAX=\''.$_POST['txtFax'].'\'';
				}
		}
		
		if($_POST['txtEMail'] != $_POST['txtEMail_old'])
		{
				if($rsSonderausweis['SAW_EMAIL'][0] != $_POST['txtEMail_old'])
				{
						$txtHinweis.=',E-Mail von '.$_POST['txtEMail_old']. ' auf '.$rsSonderausweis['SAW_EMAIL'][0];
				}
				else
				{
						$SQL.=',SAW_EMAIL=\''.$_POST['txtEMail'].'\'';
				}
		}
		
		if($_POST['txtGueltigAb'] != $_POST['txtGueltigAb_old'])
		{
				if($rsSonderausweis['SAW_GUELTIGAB'][0] != $_POST['txtGueltigAb_old'])
				{
						$txtHinweis.=',G&uuml;ltig Ab von '.$_POST['txtGueltigAb_old']. ' auf '.$rsSonderausweis['SAW_GUELTIGAB'][0];
				}
				else
				{
						$SQL.=',SAW_GUELTIGAB=to_date(\''.$_POST['txtGueltigAb'].'\',\'DD.MM.RR\')';
				}
		}
		
		if($_POST['txtGueltigBis'] != $_POST['txtGueltigBis_old'])
		{
				if($rsSonderausweis['SAW_GUELTIGBIS'][0] != $_POST['txtGueltigBis_old'])
				{
						$txtHinweis.=',G&uuml;ltig Bis von '.$_POST['txtGueltigBis_old']. ' auf '.$rsSonderausweis['SAW_GUELTIGBIS'][0];
				}
				else
				{
						$SQL.=',SAW_GUELTIGBIS=to_date(\''.$_POST['txtGueltigBis'].'\',\'DD.MM.RR\')';
				}
		}
		
		if($_POST['txtZeitTag'] != $_POST['txtZeitTag_old'])
		{
				if($rsSonderausweis['SAW_ZEIT_TAG'][0] != $_POST['txtZeitTag_old'])
				{
						$txtHinweis.=',Zeit Tag von '.$_POST['txtZeitTag_old']. ' auf '.$rsSonderausweis['SAW_ZEIT_TAG'][0];
				}
				else
				{
						$SQL.=',SAW_ZEIT_TAG='.($_POST['txtZeitTag']==''?'NULL':str_replace(',','.',$_POST['txtZeitTag']));
				}
		}
		
		if($_POST['txtZeitkonto'] != $_POST['txtZeitkonto_old'])
		{
				if($rsSonderausweis['SAW_ZEITKONTO'][0] != $_POST['txtZeitkonto_old'])
				{
						$txtHinweis.=',Zeitkonto von '.$_POST['txtZeitkonto_old']. ' auf '.$rsSonderausweis['SAW_ZEITKONTO'][0];
				}
				else
				{
						$SQL.=',SAW_ZEITKONTO=\''.$_POST['txtZeitkonto'].'\'';
				}
		}
		
		if($_POST['txtAktiv'] != $_POST['txtAktiv_old'])
		{
				if($rsSonderausweis['SAW_AKTIV'][0] != $_POST['txtAktiv_old'])
				{
						$txtHinweis.=',Aktiv von '.$_POST['txtAktiv_old']. ' auf '.$rsSonderausweis['SAW_AKTIV'][0];
				}
				else
				{
						$SQL.=',SAW_AKTIV=\''.$_POST['txtAktiv'].'\'';
				}
		}
		
		if($_POST['txtBemerkung'] != urldecode($_POST['txtBemerkung_old']))
		{
				if($rsSonderausweis['SAW_BEMERKUNG'][0] != urldecode($_POST['txtBemerkung_old']))
				{
						$txtHinweis.=',BEMERKUNG von '.$_POST['txtBemerkung_old']. ' auf '.$rsSonderausweis['SAW_BEMERKUNG'][0];
				}
				else
				{
						$SQL.=',SAW_BEMERKUNG=\''.substr($_POST['txtBemerkung'],0,1000).'\'';
				}
		}
		
		if($_POST['txtKostenstelle'] != $_POST['txtKostenstelle_old'])
		{
			
				if($rsSonderausweis['SAW_KOSTENSTELLE'][0] != $_POST['txtKostenstelle_old'])
				{
						$txtHinweis.=',Kostenstelle von '.$_POST['txtKostenstelle_old']. ' auf '.$rsSonderausweis['SAW_KOSTENSTELLE'][0];
				}
				else
				{
						$SQL.=',SAW_KOSTENSTELLE='.($_POST['txtKostenstelle']==''?'NULL':'RPAD('.$_POST['txtKostenstelle'].',6,0)');
				}
		}
		
		//<---
		
		// Update- Befehl
		
		if($txtHinweis=='' && $SQL!='')
		{
				$SQL.=',SAW_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				
				$SQL.=',SAW_USERDAT=SYSDATE';
		
				$SQL='UPDATE SONDERAUSWEISE SET ' .substr($SQL,1).' WHERE SAW_KEY=0'.$txtSAWKEY;
				
				awis_Debug(1,$SQL);
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("sonderausweis_Detail.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				$txtSAWKEY=0;
				awis_BenutzerParameterSpeichern($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName(),"");
		}
		elseif($txtHinweis!='')
		{
				echo $txtHinweis;
				awislogoff($con);
				
				die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSonderausweis['SAW_USER'][0] ." ge�ndert !</span>");
		}
		
	}
	else // INSERT-Anweisung
	{
		
		$SQL="INSERT INTO SONDERAUSWEISE (SAW_PER_NR, SAW_PER_NR_ALT, SAW_FIL_ID, SAW_BEREICH, SAW_ANR_ID, ";
	  	$SQL.="SAW_FIRMA, SAW_NAME, SAW_VORNAME, SAW_STRASSE, SAW_LAN_CODE, SAW_PLZ, SAW_ORT, SAW_EINTRITT, SAW_AUSTRITT, SAW_TAETIGKEIT, ";
	   	$SQL.="SAW_SAG_KEY, SAW_GEBURTSDATUM, SAW_TELEFON, SAW_MOBIL, SAW_FAX, SAW_EMAIL, ";
	   	$SQL.="SAW_GUELTIGAB, SAW_GUELTIGBIS, SAW_EAN, SAW_USER, SAW_USERDAT, SAW_ZEIT_TAG, SAW_ZEITKONTO, SAW_AKTIV, SAW_BEMERKUNG, SAW_KOSTENSTELLE) ";
		$SQL.="VALUES ('". $Params[0] ."' , '".$Params[1]."' , '".$Params[2]."', '".$Params[3]."', '".$Params[4]."', '".$Params[5]."', '".$Params[6]."', '".$Params[7]."', '".$Params[8]."', '".$Params[9]."', '".$Params[10]."', '".$Params[11]."',";
    	$SQL.="'".$Params[12]."', '".$Params[13]."', '".$Taetigkeit."', '".$Params[22]."', '".$Params[15]."', '".$Params[16]."', '".$Params[17]."', '".$Params[18]."', '".$Params[19]."', '".$Params[20]."', '".$Params[21]."', $EAN_SAVE, '".$AWISBenutzer->BenutzerName()."' , SYSDATE , ".$Params[24].", '".$Params[25]."', '".$Params[26]."', '".$Params[27]."', ".($Params[28]==''?'NULL':'RPAD('.$Params[28].',6,0)').")";

    	awis_Debug(1,$SQL);
    	
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("sonderausweis_Detail.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
		else
		{
			$txtSAWKEY=0;
			awis_BenutzerParameterSpeichern($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName(),"");
			$rsSAW_KEY = awisOpenRecordset($con,'SELECT SEQ_SAW_KEY.CURRVAL AS KEY FROM DUAL');
			//$txtSAWKEY=$rsSAW_KEY['KEY'][0];
		}
	}
	
	
}

if(isset($_POST['cmdHinzufuegen_x']) && ($Rechtestufe&4)==4)
{
	$Params=explode(';',awis_BenutzerParameter($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName()));

	echo "<center><b>Sonderausweis anlegen</b></center>";
	
	if($_REQUEST['txtSAG_KEY']!='')
	{	
		
		$SQL="select min(saw_per_nr_next) as per_nr_next ";
		$SQL.="from ";
		$SQL.="( ";
		$SQL.="select saw_per_nr, lag(saw_per_nr, 1, (Select sag_nummernkreisvon from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].")-1) ";
		$SQL.="over (order by saw_per_nr) + 1 saw_per_nr_next, ";
		$SQL.="(saw_per_nr-lag(saw_per_nr, 1, (Select sag_nummernkreisvon from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].")) over (order by saw_per_nr)) diff ";
		$SQL.="from (select saw_per_nr from sonderausweise union Select sag_nummernkreisbis from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].") ";
		$SQL.="where saw_per_nr>=(Select sag_nummernkreisvon from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].") ";
		$SQL.="and saw_per_nr<=(Select sag_nummernkreisbis from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].") ";
		$SQL.=") ";
		$SQL.="where diff>1";
		
		awis_Debug(1,$SQL);
		
		//$SQL="select min(saw_pers_nr_next) as per_nr_next from ";
		//$SQL.="(select saw_per_nr+1 as saw_pers_nr_next from sonderausweise where (saw_per_nr+1) ";
		//$SQL.="not in (select saw_per_nr from sonderausweise) ";
		//$SQL.="and saw_per_nr>=(Select sag_nummernkreisvon from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].") ";
		//$SQL.="and saw_per_nr<=(Select sag_nummernkreisbis from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY']."))";
		
		/*$SQL="select sah_per_nr, months_between(SYSDATE,max(SAH_LOESCHUSERDAT)) from sonderausweisehistorie ";
		$SQL.="where sah_per_nr in (select saw_per_nr+1 as saw_pers_nr_next from sonderausweise where (saw_per_nr+1) ";
		$SQL.="not in (select saw_per_nr from sonderausweise) ";
		$SQL.="and saw_per_nr>=(Select sag_nummernkreisvon from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].") ";
		$SQL.="and saw_per_nr<=(Select sag_nummernkreisbis from sonderausweisgruppen where sag_key=".$_REQUEST['txtSAG_KEY'].")) ";
		$SQL.="and months_between(SYSDATE,SAH_LOESCHUSERDAT)>24 ";
		$SQL.="group by sah_per_nr ";
		$SQL.="order by sah_per_nr asc";*/
		
		$rsSAW_PER_NR_NEXT = awisOpenRecordset($con,$SQL);
		$rsSAW_PER_NR_NEXTZeilen = $awisRSZeilen;
		
		$Params[0]=$rsSAW_PER_NR_NEXT['PER_NR_NEXT'][0];
		$Params[22]=$_REQUEST['txtSAG_KEY'];
	}
		
	echo '<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Detail>';
		
	echo "<table  width=100% id=DatenTabelle border=1>";
	echo "<tr>";
	echo "<td colspan=2 align=right><a href=./sonderausweis_Main.php?cmdAktion=Liste><img border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></a></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Personal-Nr.</td>";
	echo "<td id=TabellenZeileGrau><input name=txtPer_Nr value=".(empty($Params[0])?"''":$Params[0]) ." size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Personal-Nr. ALT</td>";
	echo "<td id=TabellenZeileGrau><input name=txtPer_Nr_alt value=".(empty($Params[1])?"''":$Params[1]) ." size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Filial-Nummer</td>";
	echo "<td id=TabellenZeileGrau><input name=txtFil_ID value=".(empty($Params[2])?"''":$Params[2]) ." size=5></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Bereich</td>";
	echo "<td><select name=txtBereich>";
	echo "<option value=''>Bitte w�hlen...</option>";
		echo "<option value='Verkauf' ".(empty($Params[3])?"''":($Params[3]=='Verkauf'?"selected='selected'":"''")).">Verkauf</option>";
		echo "<option value='Werkstatt' ".(empty($Params[3])?"''":($Params[3]=='Werkstatt'?"selected='selected'":"''")).">Werkstatt</option>";
		echo "<option value='Sonstiges' ".(empty($Params[3])?"''":($Params[3]=='Sonstiges'?"selected='selected'":"''")).">Sonstiges</option>";
	echo "</select></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Anrede</td>";
	echo "<td><select name=txtAnrede>";
	echo "<option value=0>Bitte w�hlen...</option>";
		$rsAnrede = awisOpenRecordset($con, 'SELECT * FROM Anreden ORDER BY ANR_ID');
		$rsAnredeZeilen = $awisRSZeilen;
		if(empty($Params[4]))
		{
			for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
			{
					echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . ">" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
			}
		}
		else
		{
			for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
			{
				if($Params[4]==$rsAnrede['ANR_ID'][$AnredeZeile])
				{
					echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
				}
				else
				{
					echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . ">" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
				}
			}
			
		}
		
	echo '</select></td>';
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Firma</td>";
	echo "<td id=TabellenZeileGrau><input name=txtFirma value=".(empty($Params[5])?"''":$Params[5]) ." size=50></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Name</td>";
	echo "<td id=TabellenZeileGrau><input name=txtName value=".(empty($Params[6])?"''":$Params[6]) ." size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Vorname</td>";
	echo "<td id=TabellenZeileGrau><input name=txtVorname value=".(empty($Params[7])?"''":$Params[7]) ." size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Stra�e</td>";
	echo "<td id=TabellenZeileGrau><input name=txtStra�e value=".(empty($Params[8])?"''":$Params[8]) ." size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>PLZ / Ort</td>";
	echo "<td id=TabellenZeileGrau><select name=txtLAN_CODE>";
		$rsLKZ = awisOpenRecordset($con, 'SELECT LAN_CODE,LAN_AUTOKZ FROM Laender where LAN_AUTOKZ is not null');
		$rsLKZZeilen = $awisRSZeilen;
		
		if(empty($$Params[9]))
		{	
			for($LKZZeile=0;$LKZZeile<$rsLKZZeilen;$LKZZeile++)
			{
				if($rsLKZ['LAN_CODE'][$LKZZeile]=='DE')
				{
						echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . " selected='selected'>" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
				}
				else 
				{
					echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . ">" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
				}
			}
		}
		else
		{
			for($LKZZeile=0;$LKZZeile<$rsLKZZeilen;$LKZZeile++)
			{
				if($Params[9]==$rsLKZ['LAN_CODE'][$LKZZeile])
				{
						echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . " selected='selected'>" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
				}
				else 
				{
					echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . ">" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
				}
			}
		}
	echo "</select><input name=txtPLZ value=".(empty($Params[10])?"''":$Params[10]) ." size=6>&nbsp;<input name=txtOrt value=".(empty($Params[11])?"''":$Params[11]) ." size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Eintritt / Austritt</td>";
	echo "<td id=TabellenZeileGrau><input name=txtEintritt ".(empty($Params[12])?"value='":"value='".$Params[12]) ."' size=10>&nbsp;<input name=txtAustritt value=".(empty($Params[13])?"''":$Params[13]) ." size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Ausweisgruppe</td>";
	echo "<td><select name=txtAWGruppe>";
	echo "<option value=''>Bitte w�hlen...</option>";
		$rsAWGruppe = awisOpenRecordset($con, "SELECT * FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT ORDER BY SAG_KEY");
		$rsAWGruppeZeilen = $awisRSZeilen;
		
		if(empty($Params[22]))
		{
			if($rsAWGruppeZeilen==1)
			{
				echo "<option value=" . $rsAWGruppe['SAG_KEY'][0] . " selected='selected' title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][0]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][0]."'>" . $rsAWGruppe['SAG_GRUPPE'][0] . "</option>";
			}
			else
			{
				for($AWGruppeZeile=0;$AWGruppeZeile<$rsAWGruppeZeilen;$AWGruppeZeile++)
				{
					echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
				}
			}
		}
		else
		{
			for($AWGruppeZeile=0;$AWGruppeZeile<$rsAWGruppeZeilen;$AWGruppeZeile++)
			{
				if($Params[22]==$rsAWGruppe['SAG_KEY'][$AWGruppeZeile])
				{
					echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " selected='selected' title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][0]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][0]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
				}
				else
				{
					echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
				}
			}
			
		}
		
	echo '</select></td>';
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>T&auml;tigkeit</td>";
	echo "<td id=TabellenZeileGrau><select name=txtTaetigkeit>";
	echo "<option value=''>Bitte w�hlen...</option>";
		$rsTaetigkeit = awisOpenRecordset($con, "SELECT DISTINCT(SAW_TAETIGKEIT) AS TAETIGKEIT FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT) ORDER BY 1 NULLS FIRST");
		$rsTaetigkeitZeilen = $awisRSZeilen;
		
		$Params[23]=$Params[23];
		
		if(empty($Params[23]))
		{		
			for($TaetigkeitZeile=0;$TaetigkeitZeile<$rsTaetigkeitZeilen;$TaetigkeitZeile++)
			{
				echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
			}
		}
		else
		{
			for($TaetigkeitZeile=0;$TaetigkeitZeile<$rsTaetigkeitZeilen;$TaetigkeitZeile++)
			{
				if($Params[23]==$rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile])
				{
					echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "' selected='selected'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
				}
				else
				{
					echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
				}
			}
		}
	echo "</select>";
	echo "&nbsp;<input name=txtTaetigkeitNeu value=".(empty($Params[14])?"''":$Params[14]) ." size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Geburtsdatum</td>";
	echo "<td id=TabellenZeileGrau><input name=txtGebDatum value=".(empty($Params[15])?"''":$Params[15]) ." size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Telefon / Mobil</td>";
	echo "<td id=TabellenZeileGrau><input name=txtTelefon value=".(empty($Params[16])?"''":$Params[16]) ." size=25>&nbsp;<input name=txtMobil value=".(empty($Params[17])?"''":$Params[17]) ." size=25></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>FAX / E-MAIL</td>";
	echo "<td id=TabellenZeileGrau><input name=txtFax value=".(empty($Params[18])?"''":$Params[18]) ." size=25>&nbsp;<input name=txtEMail value=".(empty($Params[19])?"''":$Params[19]) ." size=25></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>G&uuml;ltig ab / G&uuml;ltig bis</td>";
	echo "<td id=TabellenZeileGrau><input name=txtGueltigAb value=".(empty($Params[20])?"''":$Params[20]) ." size=10>&nbsp;<input name=txtGueltigBis value=".(empty($Params[21])?"''":$Params[21]) ." size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Zeit Tag</td>";
	echo "<td id=TabellenZeileGrau><input name=txtZeitTag value=".(empty($Params[24])?"''":$Params[24]) ." size=5></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Zeitkonto</td>";
	echo "<td><select name=txtZeitkonto>";
		echo "<option value='J' selected='selected'>Ja</option>";
		echo "<option value='N'>Nein</option>";
	echo "</select></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Aktiv</td>";
	echo "<td><select name=txtAktiv>";
		echo "<option value='0' selected='selected'>Ja</option>";
		echo "<option value='1'>Nein</option>";
	echo "</select></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td width=200 id=FeldBez>Kostenstelle</td>";
	echo "<td id=TabellenZeileGrau><input name=txtKostenstelle value=".(empty($Params[28])?"''":$Params[28]) ." size=6></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez valign=top>Bemerkung</td>";
	echo "<td><textarea name=txtBemerkung cols=50 rows=3>".(empty($Params[27])?"":$Params[27]) ."</textarea></td>";
	echo "</tr>";
	echo "</table>";
				
	echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo "<input type=hidden name=txtSAW_KEY value=0>";
	
	echo "</form>";
	
}
elseif($txtSAWKEY!=0 && ($Rechtestufe&2)==2)
{
	$SQL='SELECT * FROM SONDERAUSWEISE, SONDERAUSWEISGRUPPEN WHERE SAW_SAG_KEY=SAG_KEY(+) AND SAW_KEY=0'.$txtSAWKEY;
		
	$rsSonderausweis = awisOpenRecordset($con,$SQL);
	$rsSonderausweisZeilen = $awisRSZeilen;
	
	if($rsSonderausweisZeilen==0)		// Keine Daten
	{
		echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
	}
	else
	{
		//echo "<h3>Sonderausweis bearbeiten</h3>";
		echo "<center><b>Sonderausweis bearbeiten</b></center>";
		
		if(($Gruppenrecht&64)==64 && $rsSonderausweis['SAW_AKTIV'][0]!=1)
		{
			echo "&nbsp;<a href=./sonderausweis_pdf.php?txtSAW_KEY=" . $txtSAWKEY . "><img border=0 src=/bilder/pdf_gross.png name=cmdErstelleSonderausweis title='Ausweis erstellen'></a>";
		}
		
		echo '<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Detail>';
			
		echo "<table  width=100% id=DatenTabelle border=1>";
		echo "<tr>";
		echo "<td colspan=2 align=right><a href=./sonderausweis_Main.php?cmdAktion=Liste><img border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></a></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Personal-Nr.</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPer_Nr value='".$rsSonderausweis['SAW_PER_NR'][0] ."' size=10><input type=hidden name=txtPer_Nr_old value=".$rsSonderausweis['SAW_PER_NR'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Personal-Nr. ALT</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPer_Nr_alt value='". $rsSonderausweis['SAW_PER_NR_ALT'][0] ."' size=10><input type=hidden name=txtPer_Nr_alt_old value=".$rsSonderausweis['SAW_PER_NR_ALT'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Filial-Nummer</td>";
		echo "<td id=TabellenZeileGrau><input name=txtFil_ID value='". $rsSonderausweis['SAW_FIL_ID'][0] ."' size=5><input type=hidden name=txtFil_ID_old value='".$rsSonderausweis['SAW_FIL_ID'][0] ."'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Bereich</td>";
		echo "<td><select name=txtBereich>";
		echo "<option value=''>Bitte w�hlen...</option>";
			echo "<option value='Verkauf' ".(empty($rsSonderausweis['SAW_BEREICH'][0])?"''":($rsSonderausweis['SAW_BEREICH'][0]=='Verkauf'?"selected='selected'":"''")).">Verkauf</option>";
			echo "<option value='Werkstatt' ".(empty($rsSonderausweis['SAW_BEREICH'][0])?"''":($rsSonderausweis['SAW_BEREICH'][0]=='Werkstatt'?"selected='selected'":"''")).">Werkstatt</option>";
			echo "<option value='Sonstiges' ".(empty($rsSonderausweis['SAW_BEREICH'][0])?"''":($rsSonderausweis['SAW_BEREICH'][0]=='Sonstiges'?"selected='selected'":"''")).">Sonstiges</option>";
		echo "</select><input type=hidden name=txtBereich_old value=".$rsSonderausweis['SAW_BEREICH'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Anrede</td>";
		echo "<td><select name=txtAnrede>";
		echo "<option value=0>Bitte w�hlen...</option>";
			$rsAnrede = awisOpenRecordset($con, 'SELECT * FROM Anreden ORDER BY ANR_ID');
			$rsAnredeZeilen = $awisRSZeilen;
			if(empty($rsSonderausweis['SAW_ANR_ID'][0]))
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
						echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . ">" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
				}
			}
			else
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
					if($rsSonderausweis['SAW_ANR_ID'][0]==$rsAnrede['ANR_ID'][$AnredeZeile])
					{
						echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
					}
					else
					{
						echo "<option value=" . $rsAnrede['ANR_ID'][$AnredeZeile] . ">" . $rsAnrede['ANR_ANREDE'][$AnredeZeile] . "</option>";
					}
				}
				
			}
			
		echo "</select><input type=hidden name=txtAnrede_old value=".$rsSonderausweis['SAW_ANR_ID'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Firma</td>";
		echo "<td id=TabellenZeileGrau><input name=txtFirma value=\"". $rsSonderausweis['SAW_FIRMA'][0] ."\" size=50><input type=hidden name=txtFirma_old value=\"".$rsSonderausweis['SAW_FIRMA'][0] ."\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Name</td>";
		echo "<td id=TabellenZeileGrau><input name=txtName value=\"". $rsSonderausweis['SAW_NAME'][0] ."\" size=30><input type=hidden name=txtName_old value=\"".$rsSonderausweis['SAW_NAME'][0] ."\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Vorname</td>";
		echo "<td id=TabellenZeileGrau><input name=txtVorname value=\"". $rsSonderausweis['SAW_VORNAME'][0] ."\" size=30><input type=hidden name=txtVorname_old value=\"".$rsSonderausweis['SAW_VORNAME'][0] ."\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Stra�e</td>";
		echo "<td id=TabellenZeileGrau><input name=txtStra�e value=\"". $rsSonderausweis['SAW_STRASSE'][0] ."\" size=30><input type=hidden name=txtStra�e_old value=\"".$rsSonderausweis['SAW_STRASSE'][0] ."\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>PLZ / Ort</td>";
		echo "<td id=TabellenZeileGrau><select name=txtLAN_CODE>";
			$rsLKZ = awisOpenRecordset($con, 'SELECT LAN_CODE,LAN_AUTOKZ FROM Laender where LAN_AUTOKZ is not null');
			$rsLKZZeilen = $awisRSZeilen;
			
			if(empty($rsSonderausweis['SAW_LAN_CODE'][0]))
			{	
				for($LKZZeile=0;$LKZZeile<$rsLKZZeilen;$LKZZeile++)
				{
					if($rsLKZ['LAN_CODE'][$LKZZeile]=='DE')
					{
							echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . " selected='selected'>" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
					}
					else 
					{
						echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . ">" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
					}
				}
			}
			else
			{
				for($LKZZeile=0;$LKZZeile<$rsLKZZeilen;$LKZZeile++)
				{
					if($rsSonderausweis['SAW_LAN_CODE'][0]==$rsLKZ['LAN_CODE'][$LKZZeile])
					{
							echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . " selected='selected'>" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
					}
					else 
					{
						echo "<option value=" . $rsLKZ['LAN_CODE'][$LKZZeile] . ">" . $rsLKZ['LAN_AUTOKZ'][$LKZZeile] . "</option>";
					}
				}
			}
		echo "</select><input type=hidden name=txtLAN_CODE_old value=".$rsSonderausweis['SAW_LAN_CODE'][0] .">
		<input name=txtPLZ value='". $rsSonderausweis['SAW_PLZ'][0] ."' size=6><input type=hidden name=txtPLZ_old value='".$rsSonderausweis['SAW_PLZ'][0] ."'>&nbsp;
		<input name=txtOrt value='". $rsSonderausweis['SAW_ORT'][0] ."' size=30></td>";
		echo "<td id=TabellenZeileGrau><input type=hidden name=txtOrt_old value=\"".$rsSonderausweis['SAW_ORT'][0] ."\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Eintritt / Austritt</td>";
		echo "<td id=TabellenZeileGrau><input name=txtEintritt value='". $rsSonderausweis['SAW_EINTRITT'][0] ."' size=10><input type=hidden name=txtEintritt_old value=".$rsSonderausweis['SAW_EINTRITT'][0] .">&nbsp;
		<input name=txtAustritt value='". $rsSonderausweis['SAW_AUSTRITT'][0] ."' size=10><input type=hidden name=txtAustritt_old value=".$rsSonderausweis['SAW_AUSTRITT'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Ausweisgruppe</td>";
		echo "<td><select name=txtAWGruppe>";
		echo "<option value=''>Bitte w�hlen...</option>";
			$rsAWGruppe = awisOpenRecordset($con, "SELECT * FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT ORDER BY SAG_KEY");
			$rsAWGruppeZeilen = $awisRSZeilen;
			
			if(empty($rsSonderausweis['SAW_SAG_KEY'][0]))
			{
				if($rsAWGruppeZeilen==1)
				{
					echo "<option value=" . $rsAWGruppe['SAG_KEY'][0] . " selected='selected' title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][0]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][0]."'>" . $rsAWGruppe['SAG_GRUPPE'][0] . "</option>";
				}
				else
				{
					for($AWGruppeZeile=0;$AWGruppeZeile<$rsAWGruppeZeilen;$AWGruppeZeile++)
					{
						echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
					}
				}
			}
			else
			{
				for($AWGruppeZeile=0;$AWGruppeZeile<$rsAWGruppeZeilen;$AWGruppeZeile++)
				{
					if($rsSonderausweis['SAW_SAG_KEY'][0]==$rsAWGruppe['SAG_KEY'][$AWGruppeZeile])
					{
						echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " selected='selected' title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][0]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][0]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
					}
					else
					{
						echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
					}
				}
				
			}
			
		echo "</select><input type=hidden name=txtAWGruppe_old value=".$rsSonderausweis['SAW_SAG_KEY'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>T&auml;tigkeit</td>";
		echo "<td id=TabellenZeileGrau><select name=txtTaetigkeit>";
		echo "<option value=''>Bitte w�hlen...</option>";
			$rsTaetigkeit = awisOpenRecordset($con, "SELECT DISTINCT(SAW_TAETIGKEIT) AS TAETIGKEIT FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT) ORDER BY 1 NULLS FIRST");
			$rsTaetigkeitZeilen = $awisRSZeilen;
			
			$Taetigkeit_Decrypt=$rsSonderausweis['SAW_TAETIGKEIT'][0];
			
			if(empty($Taetigkeit_Decrypt))
			{		
				for($TaetigkeitZeile=0;$TaetigkeitZeile<$rsTaetigkeitZeilen;$TaetigkeitZeile++)
				{
					echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
				}
			}
			else
			{
				for($TaetigkeitZeile=0;$TaetigkeitZeile<$rsTaetigkeitZeilen;$TaetigkeitZeile++)
				{
					if($Taetigkeit_Decrypt==$rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile])
					{
						echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "' selected='selected'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
					}
					else
					{
						echo "<option value='" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
					}
				}
			}
		echo "</select><input type=hidden name=txtTaetigkeit_old value='".$rsSonderausweis['SAW_TAETIGKEIT'][0] ."'>";
		echo "&nbsp;<input name=txtTaetigkeitNeu size=30></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Geburtsdatum</td>";
	 	echo "<td id=TabellenZeileGrau><input name=txtGebDatum value='". $rsSonderausweis['SAW_GEBURTSDATUM'][0] ."' size=10><input type=hidden name=txtGebDatum_old value=".$rsSonderausweis['SAW_GEBURTSDATUM'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Telefon / Mobil</td>";
		echo "<td id=TabellenZeileGrau><input name=txtTelefon value='". $rsSonderausweis['SAW_TELEFON'][0] ."' size=25><input type=hidden name=txtTelefon_old value='".$rsSonderausweis['SAW_TELEFON'][0] ."'>&nbsp;
		<input name=txtMobil value='". $rsSonderausweis['SAW_MOBIL'][0] ."' size=25><input type=hidden name=txtMobil_old value='".$rsSonderausweis['SAW_MOBIL'][0] ."'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>FAX / E-MAIL</td>";
		echo "<td id=TabellenZeileGrau><input name=txtFax value='". $rsSonderausweis['SAW_FAX'][0] ."' size=25><input type=hidden name=txtFax_old value='".$rsSonderausweis['SAW_FAX'][0] ."'>&nbsp;
		<input name=txtEMail value='". $rsSonderausweis['SAW_EMAIL'][0] ."' size=25><input type=hidden name=txtEMail_old value='".$rsSonderausweis['SAW_EMAIL'][0] ."'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>G&uuml;ltig ab / G&uuml;ltig bis</td>";
		echo "<td id=TabellenZeileGrau><input name=txtGueltigAb value='". $rsSonderausweis['SAW_GUELTIGAB'][0] ."' size=10><input type=hidden name=txtGueltigAb_old value=".$rsSonderausweis['SAW_GUELTIGAB'][0] .">&nbsp;
		<input name=txtGueltigBis value='". $rsSonderausweis['SAW_GUELTIGBIS'][0] ."' size=10><input type=hidden name=txtGueltigBis_old value=".$rsSonderausweis['SAW_GUELTIGBIS'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Zeit Tag</td>";
		echo "<td id=TabellenZeileGrau><input name=txtZeitTag value='". $rsSonderausweis['SAW_ZEIT_TAG'][0] ."' size=10><input type=hidden name=txtZeitTag_old value=".$rsSonderausweis['SAW_ZEIT_TAG'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Zeitkonto</td>";
		echo "<td><select name=txtZeitkonto>";
			echo "<option value='J' ".(empty($rsSonderausweis['SAW_ZEITKONTO'][0])?"''":($rsSonderausweis['SAW_ZEITKONTO'][0]=='J'?"selected='selected'":"''")).">Ja</option>";
			echo "<option value='N' ".(empty($rsSonderausweis['SAW_ZEITKONTO'][0])?"''":($rsSonderausweis['SAW_ZEITKONTO'][0]=='N'?"selected='selected'":"''")).">Nein</option>";
		echo "</select><input type=hidden name=txtZeitkonto_old value=".$rsSonderausweis['SAW_ZEITKONTO'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Aktiv</td>";
		echo "<td><select name=txtAktiv>";
			echo "<option value='0' ".($rsSonderausweis['SAW_AKTIV'][0]=='0'?"selected='selected'":"''").">Ja</option>";
			echo "<option value='1' ".($rsSonderausweis['SAW_AKTIV'][0]=='1'?"selected='selected'":"''").">Nein</option>";
		echo "</select><input type=hidden name=txtAktiv_old value=".$rsSonderausweis['SAW_AKTIV'][0] ."></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=200 id=FeldBez>Kostenstelle</td>";
		echo "<td id=TabellenZeileGrau><input name=txtKostenstelle value='". $rsSonderausweis['SAW_KOSTENSTELLE'][0] ."' size=6><input type=hidden name=txtKostenstelle_old value='".$rsSonderausweis['SAW_KOSTENSTELLE'][0] ."'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez valign=top>Bemerkung</td>";
		echo "<td><textarea name=txtBemerkung cols=50 rows=3>".$rsSonderausweis['SAW_BEMERKUNG'][0]."</textarea><input type=hidden name=txtBemerkung_old value=". urlencode($rsSonderausweis['SAW_BEMERKUNG'][0]) ."></td>";
		echo "</tr>";
		echo "</table>";
		
		echo "<hr>";
		echo "<table width=100%><tr>";
		echo "<td align=left><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern></td>";
		if (($Rechtestufe&8)==8)
		{
			echo "<td align=right><input type=image accesskey=L title='L�schen (Alt+L)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen></td>";
			echo "<input type=hidden name=txtSAW_KEY_loeschen value=".$rsSonderausweis['SAW_KEY'][0].">";
		}
		echo "</tr></table>";
		echo "<input type=hidden name=txtSAW_KEY value=".$rsSonderausweis['SAW_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
		echo "</form>";
	
	}
	
}
elseif(isset($_POST['cmdAnzeigen_x']))
{
	$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
	
	echo '<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Detail>';
		
		if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
		{
				echo "&nbsp;<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf&uuml;gen (Alt+N)' onclick=location.href='./sonderausweis_Main.php?cmdAktion=Detail'>";
				
				$SQL="SELECT SAG_KEY, SAG_GRUPPE, SAG_NUMMERNKREISVON, SAG_NUMMERNKREISBIS FROM SONDERAUSWEISGRUPPEN ";
				$SQL.="WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT ORDER BY SAG_KEY";
				
				$rsSonderausweisgruppe = awisOpenRecordset($con,$SQL);
				$rsSonderausweisgruppeZeilen = $awisRSZeilen;
				
				if($rsSonderausweisgruppeZeilen>1)
				{
					echo "&nbsp;<select name=txtSAG_KEY>";	
						echo "<option value=''>Bitte w&auml;hlen...</option>";
						for($SonderausweisgruppeZeile=0;$SonderausweisgruppeZeile<$rsSonderausweisgruppeZeilen;$SonderausweisgruppeZeile++)
						{
							echo "<option value=" . $rsSonderausweisgruppe['SAG_KEY'][$SonderausweisgruppeZeile] . " title='AW-Nr.:".$rsSonderausweisgruppe['SAG_NUMMERNKREISVON'][$SonderausweisgruppeZeile]." - ".$rsSonderausweisgruppe['SAG_NUMMERNKREISBIS'][$SonderausweisgruppeZeile]."'>" . $rsSonderausweisgruppe['SAG_GRUPPE'][$SonderausweisgruppeZeile] . "</option>";
						}
					echo "</select>";
				}
			
				echo '<hr>';
		}
		
		//$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
		
		$SQL="SELECT * FROM SONDERAUSWEISE, SONDERAUSWEISGRUPPEN ";
		$SQL.="WHERE SAW_SAG_KEY = SAG_KEY(+) AND SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT) AND SAW_PER_NR='".$_REQUEST['txtPer_Nr']."' ";
		$SQL.="ORDER BY SAW_PER_NR";
		
		$rsSonderausweis = awisOpenRecordset($con,$SQL);
		$rsSonderausweisZeilen = $awisRSZeilen;
		
		if($rsSonderausweisZeilen==0)		// Keine Daten
		{
				echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
			if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
			{
				echo "<td id=FeldBez></td>";
			}					
			echo "<td id=FeldBez>Personal-Nr</td>";
			echo "<td id=FeldBez>Personal-Nr ALT</td>";
			echo "<td id=FeldBez>EAN-Nummer</td>";
			//echo "<td id=FeldBez>Filiale</td>";
			echo "<td id=FeldBez>T&auml;tigkeit</td>";
			echo "<td id=FeldBez>Gruppe</td>";
			echo "<td id=FeldBez>Eintritt</td>";
			echo "<td id=FeldBez>Austritt</td>";
			/*if(($Rechtestufe&8)==8){
					echo "<td id=FeldBez></td>";
			}*/
			if(($Gruppenrecht&64)==64){
					echo "<td id=FeldBez></td>";
			}
			//echo "<td id=FeldBez>Bemerkung</td>";
			echo "</tr>";
			
			for($i=0;$i<$rsSonderausweisZeilen;$i++)
			{
				if($i<$AwisBenuPara)
				{
					echo '<tr>';
					
					if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
					{
							//echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . "><a href=./sonderausweis_Main.php?cmdAktion=Detail&txtSAW_KEY=" . $rsSonderausweis['SAW_KEY'][$i] . "><img border=0 src=/bilder/aendern.png name=cmdBearbeiten title='Bearbeiten'></a></td>";
							echo "<td align=center " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . "><input type=image border=0 src=/bilder/aendern.png accesskey=b name=cmdBearbeiten_".$rsSonderausweis['SAW_KEY'][$i]." title='Bearbeiten (Alt+B)'></td>";
					}
				
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_PER_NR'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_PER_NR_ALT'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_EAN'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_TAETIGKEIT'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAG_GRUPPE'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_EINTRITT'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSonderausweis['SAW_AUSTRITT'][$i] . '</td>';
					/*if(($Rechtestufe&8)==8){
						echo "<td align=center " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./sonderausweis_Main.php?cmdAktion=Detail&txtSAW_KEY_loeschen=". $rsSonderausweis['SAW_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
					}*/
					if(($Gruppenrecht&64)==64){
						echo "<td align=center " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href=./sonderausweis_pdf.php?txtSAW_KEY=" . $rsSonderausweis['SAW_KEY'][$i] . "><img border=0 src=/bilder/pdf.png name=cmdErstelleSonderausweis title='Ausweis erstellen'></a>";
					}
					echo '</tr>';
				}
			}
			print "</table>";
			
			if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.4f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
			}
			else
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.4f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
	echo '</form>';	
}
elseif(isset($_POST['cmdLieferanten_x']))
{
	echo "<h2>Besucher- / Lieferantenausweis</h2><hr>";
		
	echo '<form name=frmSonderausweis_Lieferanten method=post action=./sonderausweis_pdf.php>';
	echo '<table id=DatenTabelle border=1>';
	echo '<tr><td id=FeldBez  width=20>Ausweistyp</td>';
	echo '<td><select name=txtAusweis_Typ>';
	echo "<option value=0>::Auswahl::</option>";
	echo "<option value=1>Besucher</option>";
	echo "<option value=2>Lieferanten</option>";
	echo "</select></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Nummer von / Nummer bis</td>";
	echo "<td id=TabellenZeileGrau><input name=txtNummerVon size=3>&nbsp;<input name=txtNummerBis size=3></td>";
	echo "</tr>";
	echo '</table>';
	echo "<hr>&nbsp;<input type=image accesskey=E title='Erstellen (Alt+E)' src=/bilder/gruener_haken.png name=cmdErstelleLieferanten>";
	echo '</form>';
}
elseif(isset($_POST['cmdRabatt_x']))
{
	echo "<h2>Rabattausweis</h2><hr>";
		
	echo '<form name=frmSonderausweis_Rabatt method=post action=./sonderausweis_pdf.php>';
	echo '<table id=DatenTabelle border=1>';
	echo "<tr>";
	echo "<td id=FeldBez>Name 1</td>";
	echo "<td id=TabellenZeileGrau><input name=txtName1 size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Name 2</td>";
	echo "<td id=TabellenZeileGrau><input name=txtName2 size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Name 3</td>";
	echo "<td id=TabellenZeileGrau><input name=txtName3 size=30></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Nummer von / Nummer bis</td>";
	echo "<td id=TabellenZeileGrau><input name=txtNummerVon size=3 value=1>&nbsp;<input name=txtNummerBis size=3 value=1></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Gueltig bis</td>";
	echo "<td id=TabellenZeileGrau><input name=txtGueltigBis size=10></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Rabatt-Text</td>";
	echo "<td id=TabellenZeileGrau><input name=txtRabattText size=20></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez>Rabatt-EAN</td>";
	echo "<td id=TabellenZeileGrau><input name=txtRabattEAN size=13></td>";
	echo "</tr>";
	echo '</table>';
	echo "<hr>&nbsp;<input type=image accesskey=E title='Erstellen (Alt+E)' src=/bilder/gruener_haken.png name=cmdErstelleRabatt>";
	echo '</form>';
}
else
{
	echo '<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Detail>';
	
	awis_BenutzerParameterSpeichern($con, "Sonderausweis_Anlegen", $AWISBenutzer->BenutzerName(),"");
	
	echo '<table>';
	echo '<tr><td width=20>Personalnummer&nbsp;</td>';
	echo '<td><select name=txtPer_Nr>';
	
	$rsPersonalnummern = awisOpenRecordset($con, "SELECT SAW_PER_NR FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT) ORDER BY SAW_PER_NR ASC");
	$rsPersonalnummernZeilen = $awisRSZeilen;
	
	echo "<option value=0>::Auswahl::</option>";
	for($PersonalnummernZeile=0;$PersonalnummernZeile<$rsPersonalnummernZeilen;$PersonalnummernZeile++)
	{
		echo "<option value=" . $rsPersonalnummern['SAW_PER_NR'][$PersonalnummernZeile] . ">" . $rsPersonalnummern['SAW_PER_NR'][$PersonalnummernZeile] . "</option>";
	}
	echo "</select></td>";
	echo "<td>&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png accesskey=a name=cmdAnzeigen title='Anzeigen (Alt+A)'></td>";
	echo "</tr>";
	echo '</table>';
	if(($Rechtestufe&4)==4)
	{
		echo '<hr>';
		echo "&nbsp;<input type=image border=0 src=/bilder/plus.png accesskey=h name=cmdHinzufuegen title='Hinzuf&uuml;gen (Alt+H)'>";
		
		$SQL="SELECT SAG_KEY, SAG_GRUPPE, SAG_NUMMERNKREISVON, SAG_NUMMERNKREISBIS FROM SONDERAUSWEISGRUPPEN ";
		$SQL.="WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT ORDER BY SAG_KEY";
		
		$rsSonderausweisgruppe = awisOpenRecordset($con,$SQL);
		$rsSonderausweisgruppeZeilen = $awisRSZeilen;
		
		if($rsSonderausweisgruppeZeilen>1)
		{
			echo "&nbsp;<select name=txtSAG_KEY>";	
				echo "<option value=''>Bitte w&auml;hlen...</option>";
				for($SonderausweisgruppeZeile=0;$SonderausweisgruppeZeile<$rsSonderausweisgruppeZeilen;$SonderausweisgruppeZeile++)
				{
					echo "<option value=" . $rsSonderausweisgruppe['SAG_KEY'][$SonderausweisgruppeZeile] . " title='AW-Nr.:".$rsSonderausweisgruppe['SAG_NUMMERNKREISVON'][$SonderausweisgruppeZeile]." - ".$rsSonderausweisgruppe['SAG_NUMMERNKREISBIS'][$SonderausweisgruppeZeile]."'>" . $rsSonderausweisgruppe['SAG_GRUPPE'][$SonderausweisgruppeZeile] . "</option>";
				}
			echo "</select>";
		}
	}
	if(($Gruppenrecht&32)==32)
	{
		echo "<hr>";
		echo "&nbsp;<input type=image border=0 src=/bilder/pdf_gross.png name=cmdLieferanten title='Besucher- / Lieferantenausweis erstellen'>&nbsp;Besucher- / Lieferantenausweise";
	}
	if(($Gruppenrecht&512)==512)
	{
		echo "<hr>";
		echo "&nbsp;<input type=image border=0 src=/bilder/pdf_gross.png name=cmdRabatt title='Rabattausweis erstellen'>&nbsp;Rabattausweise";
	}
	unset($rsPersonalnummern);
	
	echo '</form>';
}    
?>
</body>
</html>