<?php

// Variablen
global $awisDBFehler;
global $awisRSZeilen;
global $con;
global $AWISBenutzer;

if(isset($_POST['cmdDrucken_x']))
{
	include './perseins_EinsaetzeListe.php';
	die;
}

$RechteKontakte = awisBenutzerRecht($con, 150);
$RechteFilialInfo = awisBenutzerRecht($con, 100);
$RechteStufeMitarbeiter = awisBenutzerRecht($con, 1301);
$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}
$MBEBereiche = awisBenutzerRecht($con, 1306);
if($MBEBereiche==0)		// Wg. Umstellung!! -> Rausnehmen, wenn Rechte vergeben wurden
{
	$MBEBereiche=1;
}
//awis_Debug(1,$MBEBereiche);
//awis_Debug(1,$_REQUEST);

$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschr�nkungen nach RZ
$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $AWISBenutzer->BenutzerName()));
$RZUserListe = '';
for($i=0;$i<16;$i++)
{
	if(($RZListeStufe&pow(2,$i))==pow(2,$i))
	{
		$RZUserListe .= ';' . $RZListe[$i+1];
	}
}
$RZUserListe .= ';';


$EingabeFeld='';		// F�r Cursor Positionierung
$Params = explode(";",awis_BenutzerParameter($con, "Personaleinsatz_Suche", $AWISBenutzer->BenutzerName()));

/********************************
* Parameter speichern
********************************/
if(isset($_POST['cmdSuche_x']))
{
	$Params[0] = (isset($_POST['txtAuswahlSpeichern'])?$_POST['txtAuswahlSpeichern']:'');
	$Params[1] = $_POST['txtMIT_KEY'];
	$Params[2] = ($_POST['txtDatumVom']!=''?awis_format($_POST['txtDatumVom'], 'Datum'):'');
	$Params[3] = ($_POST['txtDatumBis']!=''?awis_format($_POST['txtDatumBis'], 'Datum'):'');
	$Params[4] = $_POST['txtFIL_ID'];
	$Params[5] = $_POST['txtMIT_REZ_ID'];
	$Params[6] = $_POST['txtMEI_MEA_KEY'];
	$Params[7] = $_POST['txtMIT_MTA_ID'];
	$Params[8] = $_POST['txtMIT_BEREICH'];

	awis_BenutzerParameterSpeichern($con, "Personaleinsatz_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}
elseif(isset($_GET['Tag']))		// Zum Bearbeiten aus den Problemf�llen heraus
{
	$Params[0] = '';
	$Params[1] = $_GET['MIT_KEY'];
	$Params[2] = $_GET['Tag'];
	$Params[3] = $_GET['Tag'];
	$Params[4] = '';
	$Params[5] = '';
	$Params[6] = '';
	$Params[7] = '';
}

/********************************
* Formulardaten speichern
********************************/

if(isset($_POST['cmdSpeichern_x']))
{
	//awis_Debug(1,$_REQUEST);
	include './perseins_Speichern.php';
}

if(awis_NameInArray($_POST, 'cmdLoeschen_')!='')
{
	//awis_Debug(1,$_POST);
	
	$DELKey = intval(substr(awis_NameInArray($_POST,'cmdLoeschen_'),12));
	
	//awis_Debug(1,$DELKey);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$SQL = "DELETE FROM MitarbeiterEinsaetze WHERE MEI_KEY=0" . $DELKey;
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("perseins_Main.php", 2, $awisDBFehler['message']);
			die();
		}

		die('<br>Der Eintrag wurde erfolgreich gel�scht<br><br><a href=./perseins_Main.php?cmdAktion=Einsaetze>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		echo "<form name=frmHBG method=post>";

		echo "<input type=hidden name=cmdLoeschen_" . $DELKey . "_x>";
		echo "<input type=hidden name=txtMEI_KEY value=" . $DELKey . ">";

		echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Eintrag ";
		echo " vom " . $_POST['txtMEI_TAG'];
		echo " l�schen m�chten?</span><br><br>";
		echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

		echo "</form>";
		awisLogoff($con);
		die();
	}
}

/********************************
* Formular aufbauen
********************************/

echo "<form name=frmMitarbeiter method=post action=./perseins_Main.php?cmdAktion=Einsaetze>";
echo '<table class=DatenTabelle border=1 width=100%>';

/********************************************
* Daten hinzuf�gen
********************************************/

$Key = 0;		// Gibt an, welcher KEY ge�ndert werden soll (0=Keiner)
if((($RechteStufe&4)==4 OR ($RechteStufe&2)==2 AND awis_NameInArray($_POST,'cmdAendern_'))
	AND !($Params[8]=='Service-Europa-PM' AND ($RechteStufe&131072)==131072)
	AND !($Params[8]=='Service-Europa-DLM' AND ($RechteStufe&65536)==65536)
	AND !($Params[8]=='Service-Europa-TWB' AND ($RechteStufe&32768)==32768)
	AND !($Params[8]=='Service-Europa-HUB' AND ($RechteStufe&16384)==16384)
	AND !($Params[8]=='Service-Europa-WT' AND ($RechteStufe&8192)==8192)
	AND !($Params[8]=='Service-Europa-TSB' AND ($RechteStufe&4096)==4096)
	AND !($Params[8]=='Service-Europa-L' AND ($RechteStufe&2048)==2048)
	AND !($Params[8]=='Smart-Repair' AND ($RechteStufe&1024)==1024)
	AND !($Params[8]=='Category Management' AND ($RechteStufe&512)==512)
	AND !($Params[8]=='Gro�kunden' AND ($RechteStufe&256)==256)
	AND !($Params[8]=='Filial-QS' AND ($RechteStufe&128)==128)
	AND !($Params[8]=='ATU Academy' AND ($RechteStufe&64)==64)
	AND !($Params[8]=='Vertrieb' AND ($RechteStufe&32)==32))		// Neue hinzuf�gen?
{
	if(awis_NameInArray($_POST,'cmdAendern_'))
	{
		$Key = intval(substr(awis_NameInArray($_POST,'cmdAendern_'),11));
		$rsMEI = awisOpenRecordset($con, 'SELECT * FROM MitarbeiterEinsaetze WHERE MEI_KEY=' . $Key);
		if($awisRSZeilen<=0)
		{
			$Key=0;
		}
	}

	echo '<tr>';
	echo '<td class=FeldBez>&nbsp;</td>';
	echo '<td class=FeldBez>Mitarbeiter</td>';
	echo '<td class=FeldBez>Datum</td>';
	echo '<td class=FeldBez>Zeit von</td>';
	echo '<td class=FeldBez>Zeit bis</td>';
	echo '<td class=FeldBez>Kategorie</td>';
	echo '<td class=FeldBez>Filiale</td>';
	echo '<td class=FeldBez>Bemerkung</td>';
	echo '</tr>';

	echo '<tr>';

	echo '<td>';
	echo " <input type=image accesskey=S title='Speichern (Alt+S)' title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '</td>';
		// Mitarbeiter
	echo '<td>';

	If($Key != 0)		// Datensatz �ndern
	{
		echo '<input type=Hidden name=txtMEI_KEY value=' . $Key . '>';

		echo '<input type=Hidden name=txtMEI_MIT_KEY_old value=' . $rsMEI['MEI_MIT_KEY'][0] . '>';
		echo '<input type=Hidden name=txtMEI_TAG_old value=' . str_replace('-','.',$rsMEI['MEI_TAG'][0]) . '>';
		echo '<input type=Hidden name=txtMEI_VONXTZ_ID_old value=' . $rsMEI['MEI_VONXTZ_ID'][0] . '>';
		echo '<input type=Hidden name=txtMEI_BISXTZ_ID_old value=' . $rsMEI['MEI_BISXTZ_ID'][0] . '>';
		echo '<input type=Hidden name=txtMEI_MEA_KEY_old value=' . $rsMEI['MEI_MEA_KEY'][0] . '>';
		echo '<input type=Hidden name=txtMEI_FIL_ID_old value=' . $rsMEI['MEI_FIL_ID'][0] . '>';
		echo "<input type=Hidden name=txtMEI_BEMERKUNG_old value='" . stripcslashes($rsMEI['MEI_BEMERKUNG'][0]) . "'>";
	}
	$EingabeFeld='txtMEI_MIT_KEY';


	$SQL = "SELECT * ";
	$SQL .= " FROM Mitarbeiter";
	$SQL .= " WHERE MIT_STATUS<>'S'";
	$Bedingung = '';

	if(($MBEBereiche&1)==1 AND !($RechteStufe&32)==32)			// Vertrieb
	{
		$Bedingung .= "  OR MIT_Bereich='Vertrieb'";
	}
	if(($MBEBereiche&2)==2 AND !($RechteStufe&64)==64)			// ATU Academy
	{
		$Bedingung .= "  OR MIT_Bereich='ATU Academy'";
	}
	if(($MBEBereiche&4)==4 AND !($RechteStufe&128)==128)			// Filial-QS - erstellt am 21.02.06 CA
	{
		$Bedingung .= "  OR MIT_Bereich='Filial-QS'";
	}
	if(($MBEBereiche&8)==8 AND !($RechteStufe&256)==256)			// Gro�kunden - erstellt am 06.06.06 CA
	{
		$Bedingung .= "  OR MIT_Bereich='Gro�kunden'";
	}
	if(($MBEBereiche&16)==16 AND !($RechteStufe&512)==512)			// Category Management - erstellt am 19.04.07 CA
	{
		$Bedingung .= "  OR MIT_Bereich='Category Management'";
	}
	if(($MBEBereiche&32)==32 AND !($RechteStufe&1024)==1024)			// smart-Repair - erstellt am 19.11.07 CA
	{
		$Bedingung .= "  OR MIT_Bereich='Smart-Repair'";
	}
	if(($MBEBereiche&64)==64 AND !($RechteStufe&2048)==2048)			// Service-Europa-L - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-L'";
	}
	if(($MBEBereiche&128)==128 AND !($RechteStufe&4096)==4096)			// Service-Europa-TSB - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-TSB'";
	}
	if(($MBEBereiche&256)==256 AND !($RechteStufe&8192)==8192)			// Service-Europa-WT - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-WT'";
	}
	if(($MBEBereiche&512)==512 AND !($RechteStufe&16384)==16384)		// Service-Europa-HUB - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-HUB'";
	}
	if(($MBEBereiche&1024)==1024 AND !($RechteStufe&32768)==32768)		// Service-Europa-TWB - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-TWB'";
	}
	if(($MBEBereiche&2048)==2048 AND !($RechteStufe&65536)==65536)		// Service-Europa-DLM - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-DLM'";
	}
	if(($MBEBereiche&4096)==4096 AND !($RechteStufe&131072)==131072)	// Service-Europa-PM - erstellt am 06.02.08 TR
	{
		$Bedingung .= "  OR MIT_Bereich='Service-Europa-PM'";
	}						
	if($Bedingung!='')
	{
		$SQL .= " AND (" . substr($Bedingung,4) . ')';
	}

	$SQL .= " ORDER BY MIT_BEZEICHNUNG";

	$rsPER = awisOpenRecordset($con, $SQL);
	$rsPERZeilen = $awisRSZeilen;

	echo '<select name=txtMEI_MIT_KEY tabindex=10>';

	for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
	{
		if($RZUserListe!=';')
		{
			if(strstr($RZUserListe,';'.$rsPER['MIT_REZ_ID'][$PerZeile].';')=='')
			{
				continue;
			}
		}
		echo "<option value='" . $rsPER['MIT_KEY'][$PerZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsPER['MIT_KEY'][$PerZeile] == $rsMEI['MEI_MIT_KEY'][0])
			{
				echo ' selected ';
			}
		}
		elseif(isset($_POST['txtMEI_MIT_KEY']) && $rsPER['MIT_KEY'][$PerZeile]==$_POST['txtMEI_MIT_KEY'])
		{
				echo ' selected ';
		}
		echo ">" . $rsPER['MIT_BEZEICHNUNG'][$PerZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';
	unset($rsPER);

	/******* Datum *******/
	if($Key != 0)
	{
		echo '<td><input name=txtMEI_TAG size=8 tabindex=20 value=' . str_replace('-','.',$rsMEI['MEI_TAG'][0]) . '></td>';
	}
	else
	{
		echo '<td><input name=txtMEI_TAG size=8 tabindex=20 value=' . date('d.m.Y') . '></td>';
	}

	/******* Zeitvon *******/
	echo '<td>';
	echo '<select name=txtMEI_VONXTZ_ID tabindex=30>';
	$rsXTZ = awisOpenRecordset($con, "SELECT * FROM TagesZeiten ORDER BY XTZ_ID");
	$rsXTZZeilen = $awisRSZeilen;

	for($XTZZeile=0;$XTZZeile<$rsXTZZeilen;$XTZZeile++)
	{
		echo "<option value='" . $rsXTZ['XTZ_ID'][$XTZZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsXTZ['XTZ_ID'][$XTZZeile] == $rsMEI['MEI_VONXTZ_ID'][0])
			{
				echo ' selected ';
			}
		}
		elseif(isset($_POST['txtMEI_BISXTZ_ID']) && $rsXTZ['XTZ_ID'][$XTZZeile] == $_POST['txtMEI_BISXTZ_ID'])
		{
				echo ' selected ';
		}

		echo ">" . $rsXTZ['XTZ_TAGESZEIT'][$XTZZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';

	/******* ZeitBis *******/
	echo '<td>';
	echo '<select name=txtMEI_BISXTZ_ID  tabindex=40>';

	for($XTZZeile=0;$XTZZeile<$rsXTZZeilen;$XTZZeile++)
	{
		echo "<option value='" . $rsXTZ['XTZ_ID'][$XTZZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsXTZ['XTZ_ID'][$XTZZeile] == $rsMEI['MEI_BISXTZ_ID'][0])
			{
				echo ' selected ';
			}
		}
		elseif(isset($_POST['txtMEI_BISXTZ_ID']) && $rsXTZ['XTZ_ID'][$XTZZeile] == $_POST['txtMEI_BISXTZ_ID'])
		{
				echo ' selected ';
		}
		echo ">" . $rsXTZ['XTZ_TAGESZEIT'][$XTZZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';
	unset($rsXTZ);

	/******* Einsatzart *******/
	echo '<td>';
	echo '<select name=txtMEI_MEA_KEY  tabindex=50>';
	$rsMEA = awisOpenRecordset($con, "SELECT * FROM MITARBEITEREINSAETZEARTEN ORDER BY MEA_BEZEICHNUNG");
	$rsMEAZeilen = $awisRSZeilen;

	for($MEAZeile=0;$MEAZeile<$rsMEAZeilen;$MEAZeile++)
	{
		echo "<option value='" . $rsMEA['MEA_KEY'][$MEAZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsMEA['MEA_KEY'][$MEAZeile] == $rsMEI['MEI_MEA_KEY'][0])
			{
				echo ' selected ';
			}
		}
		elseif(isset($_POST['txtMEI_MEA_KEY']) && $rsMEA['MEA_KEY'][$MEAZeile] == $_POST['txtMEI_MEA_KEY'])
		{
				echo ' selected ';
		}
		echo ">" . $rsMEA['MEA_BEZEICHNUNG'][$MEAZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';
	unset($rsMEA);

	/******* Filiale *******/
	if($Key != 0)
	{
		echo '<td><input name=txtMEI_FIL_ID size=5 tabindex=55 value=' . $rsMEI['MEI_FIL_ID'][0] . '></td>';
	}
	else
	{
		echo '<td><input name=txtMEI_FIL_ID size=5 tabindex=55></td>';
	}

	/******* Bemerkung *******/
	if($Key != 0)
	{
		/*//Ge�ndert am 11.01.06 durch CA - Unerw�nschte Zeichen in der Ausgabe entfernt
		$Bem = str_replace("\"", "", $rsMEI['MEI_BEMERKUNG'][0]);
		$Bem = str_replace("'", "", $Bem);
		$Bem = str_replace("\\", "", $Bem);
		$Bem = str_replace("<", "", $Bem);
		$Bem = str_replace(">", "", $Bem);

		echo "<td><input name=txtMEI_BEMERKUNG size=50 tabindex=60 value='" . $Bem. "'></td>";*/
		echo "<td><input name=txtMEI_BEMERKUNG size=50 tabindex=60 value='" . stripcslashes($rsMEI['MEI_BEMERKUNG'][0]) . "'></td>";
	}
	else
	{
		echo '<td><input name=txtMEI_BEMERKUNG size=50 tabindex=60></td>';
	}

	echo '</tr>';
	if($Key != 0)
	{
		echo '<tr><td colspan=99><font size=2>Zuletzt ge�ndert am ' . str_replace('-', '.', $rsMEI['MEI_USERDAT'][0]) . ' durch ' . $rsMEI['MEI_USER'][0] . '.';
		echo '</font></td></tr>';
	}
}
echo '</table>';
/**********************************
* Ende neuer Eintrag
**********************************/

/*********************************************************************
* Daten anzeigen
*********************************************************************/
if($Key == 0)		// Bei Datensatz �ndern ==> keine weiteren Daten anzeigen
{
	echo '<table class=DatenTabelle border=1 width=100%>';
	echo '<tr><td Class=FeldBez colspan=9>Gefundene Eintr�ge</td></tr>';

	echo '<tr>';
	echo '<td Class=FeldBez>';
	echo "<input type=image accesskey=p title='Drucken (Alt+P)' src=/bilder/drucker.png name=cmdDrucken>";
	echo '</td>';

	echo '<td Class=FeldBez width=90>Kurzwahl</td>';
	echo '<td Class=FeldBez>Mitarbeiter</td>';
	echo '<td Class=FeldBez>Tag</td>';
	echo '<td Class=FeldBez>von</td>';
	echo '<td Class=FeldBez>bis</td>';
	echo '<td Class=FeldBez>Ort</td>';
	echo '<td Class=FeldBez>Bemerkung</td>';
	echo '</tr>';

	$SQL = 'SELECT DISTINCT MitarbeiterEinsaetze.*, Filialen.FIL_BEZ, Mitarbeiter.*, Personal.PER_NR, ';
	$SQL .= ' MitarbeiterEinsaetzeArten.*, TZVon.XTZ_TAGESZEIT AS ZeitVon, ';
	$SQL .= ' TZBis.XTZ_TAGESZEIT AS ZeitBis';
	$SQL .= ' FROM MitarbeiterEinsaetze INNER JOIN Mitarbeiter ON MEI_MIT_KEY = MIT_KEY';
	$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
	$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
	$SQL .= ' INNER JOIN TagesZeiten TZVon ON MEI_VonXTZ_ID = TZVon.XTZ_ID';
	$SQL .= ' INNER JOIN TagesZeiten TZBis ON MEI_BisXTZ_ID = TZBis.XTZ_ID';
	$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';


	$Bedingung = '';
	if($Params[1]!='0')		//MIT_KEY
	{
		$Bedingung .= ' AND MEI_MIT_KEY=0' . $Params[1];
	}
	if($Params[2]!='')		//txtDatumVom
	{
		$Bedingung .= " AND MEI_TAG>=TO_DATE('" . $Params[2] . "','DD.MM.RRRR')";
	}
	if($Params[3]!='')		//txtDatumBis
	{
		$Bedingung .= " AND MEI_TAG<=TO_DATE('" . $Params[3] . "','DD.MM.RRRR')";
	}
	if($Params[4]!='')		//txtFIL_ID
	{
		$Bedingung .= " AND MEI_FIL_ID=" . $Params[4] . "";
	}
	if($Params[5]!='0' AND $Params[5]!='')		//Regionalzentren
	{
		$Bedingung .= " AND MIT_REZ_ID=" . $Params[5] . "";
	}
	if($Params[6]!='-1' AND $Params[6]!='')		//Regionalzentren
	{
		$Bedingung .= " AND MEI_MEA_KEY=" . $Params[6] . "";
	}
	if($Params[7]!='-1' AND $Params[7]!='')		//T�tigkeit
	{
		$Bedingung .= " AND MIT_MTA_ID=" . $Params[7] . "";
	}

	if($Params[8]!='')
	{
		$Bedingung .= " AND MIT_BEREICH='" . $Params[8] . "'";
	}

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$SQL .= ' ORDER BY MIT_BEZEICHNUNG, MEI_Tag, MEI_VonXTZ_ID';
	awis_Debug(1,$SQL);
	$rsMEI = awisOpenRecordset($con, $SQL);
	$rsMEIZeilen = $awisRSZeilen;

	for($MeiZeile=0;$MeiZeile<$rsMEIZeilen;$MeiZeile++)
	{
		echo '<tr>';

		echo '<td width=50 style=padding:0;margin:0;white-space:nowrap;>';
		if((awis_format($rsMEI['MEI_TAG'][$MeiZeile],'DatumsWert') >(time()-(60*60*48))) OR ($RechteStufe&16)==16)
		{
			if(($RechteStufe&4)==4
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-PM' AND ($RechteStufe&131072)==131072)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-DLM' AND ($RechteStufe&65536)==65536)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-TWB' AND ($RechteStufe&32768)==32768)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-HUB' AND ($RechteStufe&16384)==16384)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-WT' AND ($RechteStufe&8192)==8192)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-TSB' AND ($RechteStufe&4096)==4096)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-L' AND ($RechteStufe&2048)==2048)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Smart-Repair' AND ($RechteStufe&1024)==1024)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Category Management' AND ($RechteStufe&512)==512)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Gro�kunden' AND ($RechteStufe&256)==256)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Filial-QS' AND ($RechteStufe&128)==128)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='ATU Academy' AND ($RechteStufe&64)==64)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Vertrieb' AND ($RechteStufe&32)==32))
			{
				echo "<input type=image accesskey=A title='�ndern (Alt+A)' src=/bilder/aendern.png name=cmdAendern_" . $rsMEI['MEI_KEY'][$MeiZeile] . ">";
			}
			if(($RechteStufe&8)==8
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-PM' AND ($RechteStufe&131072)==131072)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-DLM' AND ($RechteStufe&65536)==65536)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-TWB' AND ($RechteStufe&32768)==32768)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-HUB' AND ($RechteStufe&16384)==16384)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-WT' AND ($RechteStufe&8192)==8192)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-TSB' AND ($RechteStufe&4096)==4096)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Service-Europa-L' AND ($RechteStufe&2048)==2048)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Smart-Repair' AND ($RechteStufe&1024)==1024)			
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Category Management' AND ($RechteStufe&512)==512)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Gro�kunden' AND ($RechteStufe&256)==256)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Filial-QS' AND ($RechteStufe&128)==128)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='ATU Academy' AND ($RechteStufe&64)==64)
				AND !($rsMEI['MIT_BEREICH'][$MeiZeile]=='Vertrieb' AND ($RechteStufe&32)==32))
			{
				print "<input type=image accesskey=x title='L�schen (Alt+X)' src=/bilder/muelleimer.png name=cmdLoeschen_" . $rsMEI['MEI_KEY'][$MeiZeile] . ">";
			}
		}
		echo '</td>';


		$rsKurzwahl = awisOpenRecordset($con, 'SELECT * FROM KontakteKommunikation WHERE KKO_KOT_Key IN (6,2) AND KKO_KON_Key=0' . $rsMEI['MIT_KON_KEY'][$MeiZeile] . ' ORDER BY KKO_KOT_KEY DESC');

		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';
		if($awisRSZeilen==0)
		{
			echo '::unbekannt::';
		}
		else
		{
			for($Kurzwahl=0;$Kurzwahl<$awisRSZeilen;$Kurzwahl++)
			{
				if($Kurzwahl>0)
				{
					echo '<br>';
				}
				if($rsKurzwahl['KKO_KOT_KEY'][$Kurzwahl]==6)	// Handy
				{
					echo '<img src=/bilder/handy.png ' . " onMouseover=\"window.status='Tel. " . $rsKurzwahl['KKO_WERT'][$Kurzwahl] . ".';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
				}
				elseif($rsKurzwahl['KKO_KOT_KEY'][$Kurzwahl]==2)	// Telefon
				{
					echo '<img src=/bilder/telefon.png ' . " onMouseover=\"window.status='Tel. " . $rsKurzwahl['KKO_WERT'][$Kurzwahl] . ".';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
				}
				if($RechteKontakte>0)
				{
					echo '<a href=/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey=' . $rsKurzwahl['KKO_KON_KEY'][$Kurzwahl] . '>' . $rsKurzwahl['KKO_KURZWAHL'][$Kurzwahl] . '</a>';
				}
				else
				{
					echo $rsKurzwahl['KKO_KURZWAHL'][$Kurzwahl] . '';
				}
			}
		}
		echo '</td>';

//		echo '<td>';
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';
		if($RechteStufeMitarbeiter>0)
		{
			echo '<a href=./perseins_Main.php?cmdAktion=Mitarbeiter&MIT_KEY=' . $rsMEI['MEI_MIT_KEY'][$MeiZeile] . '>';
			echo $rsMEI['MIT_BEZEICHNUNG'][$MeiZeile] . '</a>';
		}
		else
		{
			echo $rsMEI['MIT_BEZEICHNUNG'][$MeiZeile];
		}
		echo '</td>';


//		echo '<td>';
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';

		echo str_replace('-','.',$rsMEI['MEI_TAG'][$MeiZeile]) . '</td>';
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' align=right>' . $rsMEI['ZEITVON'][$MeiZeile] . '</td>';
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' align=right>' . $rsMEI['ZEITBIS'][$MeiZeile] . '</td>';

		if($rsMEI['MEI_MEA_KEY'][$MeiZeile]==8)		// Filale oder
		{
			echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' >' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '&nbsp;';

			if($RechteFilialInfo>0)
			{
				echo '<a href=/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsMEI['MEI_FIL_ID'][$MeiZeile] . '>' . $rsMEI['MEI_FIL_ID'][$MeiZeile] . '</a> (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';
			}
			else
			{
				echo $rsMEI['MEI_FIL_ID'][$MeiZeile] . ' (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';
			}
			echo '</td>';
		}
		else
		{
			echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '</td>';
		}

		/*//Ge�ndert am 11.01.06 durch CA - Unerw�nschte Zeichen in der Ausgabe entfernt
		$Bem = str_replace("\"", "", $rsMEI['MEI_BEMERKUNG'][$MeiZeile]);
		$Bem = str_replace("'", "", $Bem);
		$Bem = str_replace("\\", "", $Bem);
		$Bem = str_replace("<", "", $Bem);
		$Bem = str_replace(">", "", $Bem);

		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . $Bem . '</td>';	*/
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . stripslashes($rsMEI['MEI_BEMERKUNG'][$MeiZeile]) . '</td>';

		echo '</tr>';
	}


	echo '</table>';
	//awis_Debug(1,$rsMEI);
}
else		// Daten werden ge�ndert
{
	echo "<input accesskey=x title='Abbrechen (Alt+X)' type=image src=/bilder/abbrechen.png title='Abbrechen (Alt+X)' name=cmdAbbrechen>";
}
/*********************************************************************
* Ende Daten anzeigen
*********************************************************************/

echo '</form>';

/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
