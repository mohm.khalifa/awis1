/* css_card_haupt */


body									{	margin:5px; }		/*Randabstand im Browserfenster*/



<?php



require_once("card/css/css_card_iframe.php");		//Breitenwerte f�r I-Frame werden in der Datei css_cardiframe.php gepflegt

//Aufgrund der Angabe der Schriftgr�sse in px wird die Elementh�he (input, select, button u.a.) ermittelt

switch($intSchriftgroesse)
{
	case 12:	//Werte f�r die Breite der Spalten im Hauptformular (werden �ber css zugewiesen)
				
				$intHpt_bes=$intIfr_rgnr;			//Breite der Spalte Beschriftung Adressdaten in der Hauptseite
				$intHpt_adrdat=250;					//Breite des Textfeldes der Spalte Werte Adressdaten in der Hauptseite
				$intHpt_adrdatz=$intHpt_adrdat+0;	//Breite der Spalte Werte Adressdaten in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_abst=150;					//Breite der Spalte Abstand (zwischen Adressdaten und Mahndaten) in der Hauptseite
				$intHpt_mahng=110;					//Breite der Spalte Mahnungen (Beschriftung) in der Hauptseite
				$intHpt_bet=87;						//Breite der Betragsfelder
				$intHpt_dat=$intIfr_dat;			//Breite des Textfeldes der Spalte Werte Mahnungen in der Hauptseite
				$intHpt_datz=$intHpt_dat+0;			//Breite der Spalte Werte Mahnungen in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_madat=100;					//Breite des Listenfeldes Mahndatum in der Hauptseite
				$intHpt_button=20;					//Breite der Spalte Button in der Hauptseite
				$intZwischenhoehe=2;				//H�he der Zwischenzeilen in der Hauptseite
				$intBemBreite=560;					//Breite Textarea Bemerkungen
				$intBemHoehe=50;					//H�he Textarea Bemerkungen
				break;
				
	case 16:	//Werte f�r die Breite der Spalten im Hauptformular (werden �ber css zugewiesen)

				$intHpt_bes=$intIfr_rgnr;			//Breite der Spalte Beschriftung Adressdaten in der Hauptseite
				$intHpt_adrdat=300;					//Breite des Textfeldes der Spalte Werte Adressdaten in der Hauptseite
				$intHpt_adrdatz=$intHpt_adrdat+0;	//Breite der Spalte Werte Adressdaten in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_abst=170;					//Breite der Spalte Abstand (zwischen Adressdaten und Mahndaten) in der Hauptseite
				$intHpt_mahng=180;					//Breite der Spalte Mahnungen (Beschriftung) in der Hauptseite
				$intHpt_bet=106;					//Breite der Betragsfelder
				$intHpt_dat=$intIfr_dat;			//Breite des Textfeldes der Spalte Werte Mahnungen in der Hauptseite
				$intHpt_datz=$intHpt_dat+0;			//Breite der Spalte Werte Mahnungen in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_madat=100;					//Breite des Listenfeldes Mahndatum in der Hauptseite
				$intHpt_button=20;					//Breite der Spalte Button in der Hauptseite
				$intZwischenhoehe=2;				//H�he der Zwischenzeilen in der Hauptseite
				$intBemBreite=705;					//Breite Textarea Bemerkungen
				$intBemHoehe=50;					//H�he Textarea Bemerkungen
				break;
			
	default:	//Werte f�r die Breite der Spalten im Hauptformular (werden �ber css zugewiesen)

				$intHpt_bes=$intIfr_rgnr;			//Breite der Spalte Beschriftung Adressdaten in der Hauptseite
				$intHpt_adrdat=300;					//Breite des Textfeldes der Spalte Werte Adressdaten in der Hauptseite
				$intHpt_adrdatz=$intHpt_adrdat+0;	//Breite der Spalte Werte Adressdaten in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_abst=120;					//Breite der Spalte Abstand (zwischen Adressdaten und Mahndaten) in der Hauptseite
				$intHpt_mahng=160;					//Breite der Spalte Mahnungen (Beschriftung) in der Hauptseite
				$intHpt_bet=50;						//Breite der Betragsfelder
				$intHpt_dat=$intIfr_dat;			//Breite des Textfeldes der Spalte Werte Mahnungen in der Hauptseite
				$intHpt_datz=$intHpt_dat+0;			//Breite der Spalte Werte Mahnungen in der Hauptseite (kann gr��er als das Textfeld sein)
				$intHpt_madat=100;					//Breite des Listenfeldes Mahndatum in der Hauptseite
				$intHpt_button=20;					//Breite der Spalte Button in der Hauptseite
				$intZwischenhoehe=2;				//H�he der Zwischenzeilen in der Hauptseite
				$intBemBreite=300;					//Breite Textarea Bemerkungen
				$intBemHoehe=50;					//H�he Textarea Bemerkungen
}

?>
	/* Werte f�r Hauptseite card_haupt.php */
	
	.titel									{	font-weight:bold; }
	
	.hpt_bes								{	width:<?php echo $intHpt_bes; ?>px;
												font-weight:bold; }
	.hpt_adrdat								{	width:<?php echo $intHpt_adrdat; ?>px; }
	.hpt_adrdatz							{	width:<?php echo $intHpt_adrdatz; ?>px; }
	.hpt_abst								{	width:<?php echo $intHpt_abst;	?>px; }
	.hpt_mahng								{	width:<?php echo $intHpt_mahng; ?>px;
												font-weight:bold; }
	.hpt_mahngz								{	width:200px; }
	.hpt_dat								{	width:<?php echo $intHpt_dat; ?>px; }
	.hpt_bet								{	width:<?php echo $intHpt_bet; ?>px;
												text-align:right; }
	.hpt_datz								{	width:<?php echo $intHpt_datz; ?>px; }
	.hpt_madat								{   width:<?php echo $intHpt_madat; ?>px; }
	.hpt_button								{	width:<?php echo $intHpt_button; ?>px; }
	.hpt_speibut							{	height:<?php echo $intElementhoehe; ?>px;
												width:<?php echo $intElementhoehe; ?>px;
												visibility:hidden; }
	.hpt_zwischen							{	font-size:<?php echo $intZwischenhoehe; ?>px;
												height:<?php echo $intZwischenhoehe; ?>px;
												background-color:#777777; }

	/* Werte f�r die Beschriftung des I-Frame */
	
	td.bes									{	font-weight:bold;
												border-width:0px; }
	#bes1									{	width:<?php echo $intIfr_rgnr+2*$intRahmenbreite;?>px; }
	#bes2									{	width:<?php echo $intIfr_betr+2*$intRahmenbreite;?>px; }
	#bes3									{	width:<?php echo $intIfr_dat+2*$intRahmenbreite;?>px; }
	#bes4									{	width:<?php echo $intIfr_buart+2*$intRahmenbreite;?>px; }
	
	#rec_bemerkungen,#td_rec_bemerkungen	{	width:<?php echo $intBemBreite; ?>px;
												height:<?php echo $intBemHoehe; ?>px; }