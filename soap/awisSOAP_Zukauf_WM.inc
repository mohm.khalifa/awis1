<?php
require_once('/daten/soap/awisSOAP_Zukauf.inc');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisZukaufArtikel_WM extends awisZukaufArtikel
{
	/**
	 * Lieferantennummer
	 *
	 */
	private $_LIE_NR = '0585';

	/**
	 * Standardland fuer diesen Lieferanten (fuer die Testfiliale)
	 * @var string
	 */
	private $_StandardLand = 'DE';
	/**
	 * WSDL Datei f�r die Abrufe der Daten
	 *
	 */

	const WSDL_DATEI  = 'http://wmatuservice.wmkat.de?wsdl';
	//const WSDL_DATEI_TEST  = 'http://testwmatuservice.wmkat.de?wsdl';
	const WSDL_DATEI_TEST  = 'http://testwmatuservice.wmkat.de/Service.asmx?WSDL';

	/**
	 * Lieferart bei WM
	 *
	 */
	const LIEFERART_ABHOLUNG = 'Abholung';
	/**
	 * Lieferart bei WM
	 *
	 */
	const LIEFERART_ZUSTELLUNG = 'Lieferung';

    /**
     * Benutzername
     * @var string
     */
	private $_SOAP_Benutzer = '4650024';			// Kundennummer
    /**
     * Kennwort
     * @var string
     */
	private $_SOAP_Kennwort = '481cBAf1';			// Kennwort
    /**
     * Zusatzkennung
     * @var string
     */
	private $_SOAP_Zusatz = 'atuwsvctest';			// Testsystem

	/**
	 * Debuglevel f�r alle
	 *
	 * @var int
	 */
	protected $_DebugLevel = 0;

	/**
	 * E-Mailadressen f�r eine Bestellbestaetigung
	 *
	 * @var string
	 */
	private $_EMAIL_BESTAETIGUNG = 'shuttle@de.atu.eu';		// Defaultwert
	/**
	 * SOAP Client f�r die Aufrufe
	 *
	 * @var SoapClient
	 */
	private $_SOAPClient;

	/**
	 * Ergebnisdaten als XML
	 *
	 * @var SimpleXMLElement
	 */
	private $_Daten;

	/**
	 * Anmldedaten
	 *
	 * @var string
	 */
	private $_LoginDaten;

	/**
	 * Standardlieferart f�r diese Filie
	 *
	 * @var int
	 */
	private $_StandardLieferart = 0;
	/**
	 * Texte mit den Verf�gbarkeiten
	 *
	 * @var array
	 */
	private $_TexteVerfuegbarkeit = array();

	/**
	 * Land f�r die Preise und die MwSt
	 * @var string
	 */
	private $_FIL_LAN_CODE = 'DE';

	/**
	 * Header f�r die Anfragen
	 *
	 * @var simpleXMLElement
	 */
	private $_AnfrageParams = null;


	protected $_awisWerkzeuge;

	public function __construct($XML = false, $FIL_ID = 0, $Debuglevel = 0, $LIE_NR='0585')
	{
		parent::__construct();

		try
		{
		    $this->_awisWerkzeuge = new awisWerkzeuge();
			$this->_DebugLevel = $Debuglevel;
			$this->_FIL_ID = $FIL_ID;
			$this->_LIE_NR = $LIE_NR;

file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"\n\nStarte Anfrage ".$this->_LIE_NR.' - '.$this->_FIL_ID."\n",FILE_APPEND);

			ini_set('allow_call_time_pass_reference','true');

			if($XML===false)
			{
				$this->_Daten = new awisSimpleXMLElement('<Daten><Anfragezeit>'.date('c').'</Anfragezeit></Daten>');
			}
			else
			{
				$this->_Daten = $XML;
			}

			$Params = array('trace'=>1					// Protokoll
				, 'encoding'=>'utf-8'
				, 'exceptions' =>1
				, 'soap_version'   => SOAP_1_1
				);

			if($FIL_ID==995)		// Testfiliale ins Testsystem
			{
				$this->_SOAPClient = new SoapClient(self::WSDL_DATEI_TEST,$Params);
			}
			else
			{
				$this->_SOAPClient = new SoapClient(self::WSDL_DATEI,$Params);
			}

			$this->_AnmeldeDaten = $this->_LeseKundenNummer($this->_LIE_NR , $this->_FIL_ID);
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"\n\nKundennummer ".$this->_AnmeldeDaten['KDNR'].' - '.$this->_AnmeldeDaten['ZUSATZ']."\n",FILE_APPEND);

			if($this->_AnmeldeDaten===false)
			{
				$this->_AnmeldeDaten['KDNR']=$this->_SOAP_Benutzer;
				$this->_AnmeldeDaten['KENNWORT']=$this->_SOAP_Kennwort;
				$this->_AnmeldeDaten['ZUSATZ']=$this->_SOAP_Zusatz;
				$this->_awisWerkzeuge->EMail('shuttle@de.atu.eu','Filiale ohne User bei WM '.$this->_LIE_NR,'Fuer die Filiale '.$this->_FIL_ID.' ist kein Benutzer hinterlegt.');
			}
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"\n\nKundennummer ".$this->_AnmeldeDaten['KDNR'].' - '.$this->_AnmeldeDaten['ZUSATZ']."\n",FILE_APPEND);

			if($FIL_ID=='995')
			{
				// Testsystem manuell setzen, da es fuer unterschiedliche Laender verwendet wird
				$this->_FIL_LAN_CODE = $this->_StandardLand;
			}
			else
			{
				$SQL = 'SELECT LAN_CODE';
				$SQL .= ' FROM Laender ';
				$SQL .= ' INNER JOIN Filialen ON FIL_LAN_WWSKENN = LAN_WWSKENN';
				$SQL .= ' WHERE FIL_ID = '.$FIL_ID;
				$rsLAN = $this->_DB->RecordSetOeffnen($SQL);
				$this->_FIL_LAN_CODE = $rsLAN->FeldInhalt('LAN_CODE');
			}

            // Hier gehts weiter

            // 1. Anmeldung
            /**
             	<WMOBS version="2.0">
				<Request applicationId="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" functionNr="1000"	version="1.0"/>
				<Parameter customerNr="???????" userName="Mustername" password="musterpasswort"/>
				</WMOBS>
            */
			
			
			/**
			 * Antwort
			 * <WMOBS version="2.0">
				<Response errorNr="0" errorMessage="" />
				<Data>
				<cart nr="????" />
				</Data>
					</WMOBS>	
			*/

			
            // 2. Stammdaten pr�fen
		}
		catch (SoapFault $ex)
		{
			$Daten = $this->_Daten->addChild('Fehler');
			$Daten->addChild('Nummer',0);
			$Daten->addCData('Text',$ex);
		}

		return $this->_Daten->asXML();
	}

	public function __Daten()
	{
		return $this->_Daten->asXML();
	}
	/**
	 * Abmelden vom System
	 *
	 * Damit der "interne" Warenkorb bei WM gel�scht
	 */
	public function __destruct()
	{
        // TODO: Pr�fen, ob das notwendig ist
        /**
         * <WMOBS version="2.0">
<Request applicationId="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" functionNr="1010"
version="1.0" cartNr="????"/>
<Parameter deleteCart="False" />
</WMOBS>
         */
	}


	/**
	 * Erzeugt den Basis Header f�r alle Anfragen
	 *
	 */
	private function AnfrageHeader()
	{
		// Parameter f�r die Anfrage
        // TODO: pr�fen, ob das notwendig ist
		$this->_AnfrageParams = '';/*/*new awisSimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?>');*/
	}


	/**
	 * Pr�fung eines Artikelpreises und eines Bestands f�r WM
	 *
	 * @param array $ArtikelInfos
	 * @return SimpleXMLElement
	 */
	public function ArtikelVerfuegbarkeit(array $ArtikelInfos, $DebugInfos = false)
	{
		try
		{
			if($DebugInfos==true)
			{
				$DebugXML = $this->_Daten->addChild('Debug');
			}
			if($this->_DebugLevel>0)
			{
				echo ' ARTINFOS '.var_dump($ArtikelInfos).'<hr>';
			}
			file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"Preisanfrage".date('c')."\n",FILE_APPEND);

			// Alle Artikel suchen
			// WM: �ber TecDoc-Einspeiser
			$SQL = 'SELECT DISTINCT ZAL_BESTELLNUMMER, ZLA_ARTIKELNUMMER, ZLH_KUERZEL, ZLH_TECDOCID';
			$SQL .= ' FROM ZUKAUFARTIKEL';
			$SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLA_ZLH_KEY = ZLH_KEY';
			$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_ZLA_KEY = ZLA_KEY AND ZAL_LIE_NR = \''.$this->_LIE_NR.'\'';

			$Bedingung = '';

			if(isset($ArtikelInfos['artikelnummer']))
			{
				if(is_array($ArtikelInfos['artikelnummer']))
				{
					foreach($ArtikelInfos['artikelnummer'] AS $ArtNr)
					{
						$Bedingung .= ' OR  ( ZLA_ARTIKELNUMMER = \''.$ArtNr. '\')';
						$ArtikelNummer[] = $ArtNr;
					}
				}
				else
				{
					$Bedingung .= ' OR  ( ZLA_ARTIKELNUMMER = \''.$ArtikelInfos['artikelnummer']. '\')';
					$ArtikelNummer = $ArtikelInfos['artikelnummer'];
				}
			}
			if(isset($ArtikelInfos['bestellnummer']))
			{
				$Bedingung .= ' OR ( ZAL_BESTELLNUMMER = \''.$ArtikelInfos['bestellnummer']. '\')';
				$ArtikelNummer = $ArtikelInfos['bestellnummer'];
			}
			if(isset($ArtikelInfos['eannummer']))
			{
				$Bedingung .= ' OR ( EXISTS (SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZAL_KEY AND ZAI_AID_NR = 1 AND ZAI_ID = \''.$ArtikelInfos['eannummer']. '\'))';
				$ArtikelNummer = $ArtikelInfos['eannummer'];
			}
			if(isset($ArtikelInfos['oenummer']))
			{
				$Bedingung .= ' OR ( EXISTS (SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZAL_KEY AND ZAI_AID_NR = 2 AND ZAI_ID = \''.$ArtikelInfos['oenummer']. '\'))';
				$ArtikelNummer = $ArtikelInfos['oenummer'];
			}

			$SQL .= ' WHERE ('.substr($Bedingung,4).')';
			//$SQL .= ' AND (ZAL_BESTELLNUMMER IS NOT NULL OR ZLH_TECDOCID IS NOT NULL)';

			if(isset($ArtikelInfos['zlh_key']) AND $ArtikelInfos['zlh_key'] > 0)
			{
				$SQL .= ' AND ( ZLA_ZLH_KEY = \''.$ArtikelInfos['zlh_key']. '\')';
			}

			if($this->_DebugLevel>0)
			{
				echo 'Anfrage Artikel:'.$SQL.'<br>';
			}
			if(isset($DebugXML))
			{
				$DebugXML->addChild('SQL',$SQL);
			}

			// Daten �ffnen
			$rsZAL = $this->_DB->RecordSetOeffnen($SQL);
			//file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"-> Anfrage mit ".$SQL."\n",FILE_APPEND);

			// Trefferliste vorbereiten
			$Artikelliste = $this->_Daten->addChild('Artikelliste');
			$Artikelliste->addChild('Treffer',0);


            /*
<?xml version="1.0" encoding="utf-8"?>
             * <CheckAvailabilityRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
             * <LoginInformation>
             *  <CustomerNumber>123456</CustomerNumber>
             *  <Username>username</Username>
             *  <Password>password</Password>
             * </LoginInformation>
             * <ArticleNumber>3122157</ArticleNumber>
             * <Amount>1</Amount>
             * </CheckAvailabilityRequest>
*/

			// Alle gesuchten Artikel m�ssen einzeln angefragt werden
            $XMLText = '<?xml version="1.0" encoding="utf-8"?>';
            $XMLText .= '<CheckAvailabilityRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $XMLText .= '<LoginInformation>';
            $XMLText .= '<CustomerNumber>'.$this->_AnmeldeDaten['KDNR'].'</CustomerNumber>';
            $XMLText .= '<Username>'.$this->_AnmeldeDaten['ZUSATZ'].'</Username>';
            $XMLText .= '<Password>'.$this->_AnmeldeDaten['KDKENNWORTR'].'</Password>';
            $XMLText .= '</LoginInformation>';

			$ArtikelAnzahl = 0;
            $ArtikelListe = array();
			while(!$rsZAL->EOF())
			{
                $XMLAnfrage = $XMLText;
                $XMLAnfrage .= '<ArticleNumber>'.$rsZAL->FeldInhalt('ZAL_BESTELLNUMMER').'</ArticleNumber>';
                $XMLAnfrage .= '<Amount>'.$ArtikelInfos['menge'].'</Amount>';
                $XMLAnfrage .= '</CheckAvailabilityRequest>';

file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"-> Starte Anfrage:".$XMLAnfrage."\n",FILE_APPEND);

                try
                {
                    $Param = array('inputData'=>$XMLAnfrage);
                    $Erg = $this->_SOAPClient->CheckAvailability($Param);
                    if($this->_DebugLevel>0)
                    {
                        echo 'Antwort Artikelverfuegbarkeit(seriell):'.serialize($Erg).'<hr>';
                    }
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',  serialize($Erg)."\n",FILE_APPEND);
die();

                    if(!is_array($Erg->PreisanfrageResult))
                    {
                        $Daten = array($Erg->PreisanfrageResult);
                    }
                    else
                    {
                        $Daten = $Erg->PreisanfrageResult;
                    }

                    foreach($Daten AS $Antwort)
                    {
                        if(array_search((string)$Antwort->Artikelnummer.'~'.(string)$Antwort->Hersteller, $ArtikelListe)===false)
                        {
                            $ArtikelListe[(string)$Antwort->Artikelnummer.'~'.(string)$Antwort->Hersteller]=$Antwort;
                            $ArtikelAnzahl++;
                        }
                    }
                }
                catch(Exception $ex)
                {
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log', 'FEHLER bei der Anfrage:'. serialize($ex)."\n",FILE_APPEND);
//                    awisWerkzeuge::EMail('shuttle@de.atu.eu','Fehler bei WM Anfrage bei Filiale '.$this->_FIL_ID,'Anfrage des Artikels '.$Param['Artikelnummer'].' hat einen Fehler geliefert: '.$ex->getMessage());
                }

                // TODO: Abfrage aufbauen
				$rsZAL->DSWeiter();
			}

file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',  $ArtikelAnzahl.' eindeutige Artikel gefunden.'."\n",FILE_APPEND);
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',  serialize($ArtikelListe)."\n",FILE_APPEND);

/*
 * - <PreisanfrageResponse xmlns="http://atu.service.wm.de">
- <PreisanfrageResult>
  <Artikelnummer>J3603055</Artikelnummer>
  <Hersteller>55</Hersteller>
  <Artikelbestellnummer>01734812</Artikelbestellnummer>
  <Verpackungseinheit>0</Verpackungseinheit>
  <Tauschteil>0</Tauschteil>
  <TauschteilWert>0</TauschteilWert>
  <Warengruppe>01735</Warengruppe>
  <MwSt>voller Satz</MwSt>
  <Grosshandelsbrutto>103.4</Grosshandelsbrutto>
  <Einkaufspreisbasis>28.93</Einkaufspreisbasis>
  <Preiseinheit>1</Preiseinheit>
  <EAN>4029416132729</EAN>
  <Gewicht>1.488</Gewicht>
- <Lieferungen>
- <PreisanfrageLieferung>
  <Verkaufshaus>101</Verkaufshaus>
  <Liefermenge>1</Liefermenge>
  <Zeitpunkt>2011-02-23 14:00</Zeitpunkt>
  <Bemerkung>sofort verf�gbar</Bemerkung>
  </PreisanfrageLieferung>
  </Lieferungen>
  </PreisanfrageResult>
  </PreisanfrageResponse>
 *
 */
			//file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',$this->_AnfrageParams->asXML(),FILE_APPEND);
			// Parameter werden als XML Text �bergeben!
			if($ArtikelAnzahl>0)
			{
				foreach($ArtikelListe AS $ID=>$ArtikelAntwort)
				{
file_put_contents($this->_Protokollverzeichnis.'/WM_anfrage.log',"  -> Artikel: ".(string)$ArtikelAntwort->Artikelnummer."\n",FILE_APPEND);

					if(isset($ArtikelAntwort->Artikelnummer))		// Artikel
					{
                        $SQL = 'SELECT ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG, ZLA_ZLH_KEY, ZLH_TECDOCID';
                        $SQL .= ' FROM Zukaufartikel';
                        $SQL .= ' INNER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
                        $SQL .= ' WHERE ZLA_ARTIKELNUMMER = \''.(string)$ArtikelAntwort->Artikelnummer.'\'';
                        $SQL .= ' AND ZLH_TECDOCID = \''.(string)$ArtikelAntwort->Hersteller.'\'';
                        $rsAST = $this->_DB->RecordSetOeffnen($SQL);
                        if($rsAST->EOF())
                        {
                        }

                        if(!$rsAST->EOF() AND $rsAST->AnzahlDatensaetze()==1)
                        {
                            $ArtikelNummer = $rsAST->FeldInhalt('ZLA_ARTIKELNUMMER');
                            $ArtikelBezeichnung = $rsAST->FeldInhalt('ZLA_BEZEICHNUNG');
                            $ArtikelHersteller = $rsAST->FeldInhalt('ZLH_TECDOCID');
                        }
                    }
                    else
                    {
                        $ArtikelNummer = (string)$ArtikelAntwort->Artikelnummer;
                        $ArtikelBezeichnung = '';//utf8_decode((string)$ArtikelAntwort->ArtikelBezeichnung);
                        $ArtikelHersteller = (string)$ArtikelAntwort->Hersteller;
                    }


                    $Artikelliste->Treffer = ((int)$Artikelliste->Treffer)+1;
                    $ArtikelTreffer = $Artikelliste->addChild('ArtikelTreffer');

                    // Hersteller ermitteln
                    //   es sollte immer der TecDoc Einspeiser kommen
                    // 		wenn nichts gefunden nochmal beim Lieferanten suchen
                    // 	TODO: Teste, ob das Sinn macht
                    $SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
                    $SQL .= ' FROM ZUKAUFLIEFERANTENHERSTELLER ';
                    $SQL .= ' WHERE ZLH_TECDOCID = \''.(string)$ArtikelAntwort->Hersteller.'\'';

                    $DebugXML->addCData('SQLZLH1',$SQL);
                    $rsZHK = $this->_DB->RecordSetOeffnen($SQL);
                    if($rsZHK->EOF())
                    {
                        $SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
                        $SQL .= ' FROM ZUKAUFLIEFERANTENHERSTELLER ';
                        $SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = \''.$this->_LIE_NR .'\'';
                        $SQL .= ' WHERE ZLH_KUERZEL = \''.(string)$ArtikelHersteller.'\'';
                        $rsZHK = $this->_DB->RecordSetOeffnen($SQL);
                    }

                    $SQL = 'SELECT ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG, ZLA_TAUSCHTEIL, ZLA_KEY, ZLA_AST_ATUNR, ZAL_KEY, ZLA_ZLH_KEY';
                    $SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS VP';
                    $SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
                    $SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = \''.$this->_FIL_LAN_CODE.'\' AND ATW_KENNUNG = ASI_WERT';
                    $SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
                    $SQL .= ', (SELECT AST_VK FROM Artikelstamm WHERE AST_ATUNR = ZLA_AST_ATUNR ) AS AST_VK';
                    $SQL .= ' FROM Zukaufartikel';
                    $SQL .= ' LEFT OUTER JOIN Zukaufartikellieferanten ON zla_key = zal_zla_key AND zal_lie_nr = \''.$this->_LIE_NR .'\'';
                    $SQL .= ' WHERE ZLA_ARTIKELNUMMER = \''.$ArtikelNummer.'\'';
                    $SQL .= ' AND ZLA_ZLH_KEY = 0'.$rsZHK->FeldInhalt('ZLH_KEY');
                    if(isset($DebugXML))
                    {
                        $DebugXML->addChild('ArtikelsuchSQL',$SQL);
                    }

                    $rsZLA = $this->_DB->RecordSetOeffnen($SQL);

                    $Hilf = $ArtikelTreffer->addChild('Zukaufartikel');
                    $Hilf->addChild('ZLA_KEY',($rsZLA->FeldInhalt('ZLA_KEY')==''?'':$rsZLA->FeldInhalt('ZLA_KEY')));
                    $Hilf->addChild('ZLA_ZLH_KEY',($rsZLA->FeldInhalt('ZLA_ZLH_KEY')==''?'':$rsZLA->FeldInhalt('ZLA_ZLH_KEY')));
                    $Hilf->addChild('ZAL_KEY',($rsZLA->FeldInhalt('ZAL_KEY')==''?'':$rsZLA->FeldInhalt('ZAL_KEY')));
                    $Hilf->addChild('ZLA_ARTIKELNUMMER',($rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER')==''?'':$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER')));
                    $Hilf->addCData('ZLA_BEZEICHNUNG',($rsZLA->FeldInhalt('ZLA_BEZEICHNUNG')==''?'':($rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'))));
                    $Hilf->addChild('ZLA_AST_ATUNR',($rsZLA->FeldInhalt('ZLA_AST_ATUNR')==''?'':$rsZLA->FeldInhalt('ZLA_AST_ATUNR')));

                    $ArtikelTreffer->addChild('Artikelnummer',$ArtikelAntwort->Artikelnummer);
                    $ArtikelTreffer->addChild('Katalognummer',$ArtikelAntwort->Artikelbestellnummer);
                    // TODO: Bezeichnung von WM nehmen
                    //$ArtikelTreffer->addCData('Artikelbezeichnung',($ArtikelAntwort->Article->Bezeichnung1));
                    $ArtikelTreffer->addCData('Artikelbezeichnung',$ArtikelBezeichnung);

                    //$ArtikelTreffer->addChild('Herstellercode',$ArtikelAntwort->Article->Hersteller);
                    $ArtikelTreffer->addChild('Herstellercode',$ArtikelHersteller);
                    $ArtikelTreffer->addChild('Herstellername','');
                    $ArtikelTreffer->addChild('EAN-Nummer',$ArtikelAntwort->EAN);
                    $ArtikelTreffer->addChild('OE-Nummer','');
                    // Bestellnummer muss beide Werte beinhalten, damit ich die Daten wieder finde f�r die Bestellung
                    $ArtikelTreffer->addChild('BestellIdLieferant',$ArtikelAntwort->Artikelbestellnummer);
                    $ArtikelTreffer->addChild('Bestellnummer',$ArtikelAntwort->Artikelnummer);
                    $ArtikelTreffer->addChild('Mengeneinheit','');

                    switch((string)$ArtikelAntwort->Tauschteil)
                    {
                        case 'J':			// Altteilwert
                            $ArtikelTreffer->addChild('Tauschteil','1');
                            break;
                        default:
                            $ArtikelTreffer->addChild('Tauschteil','1');
                    }

                    // Gewicht mit speichern als Option
                    $ArtikelTreffer->addChild('Gewicht',$this->_DB->FeldInhaltFormat('N4',$ArtikelAntwort->Gewicht));

                    $Hilf = $ArtikelTreffer->addChild('Hersteller');

                    $Hilf->addChild('ZLH_KEY',$rsZHK->FeldInhalt('ZLH_KEY'));
                    $Hilf->addChild('ZHK_KEY',$rsZHK->FeldInhalt('ZHK_KEY'));
                    $Hilf->addCData('ZLH_BEZEICHNUNG',$rsZHK->FeldInhalt('ZLH_BEZEICHNUNG'));
                    $Hilf->addCData('ZLH_KUERZEL',$rsZHK->FeldInhalt('ZLH_KUERZEL'));

                    $SQL = 'SELECT ZSZ_KEY, ZSZ_SORTIMENT';
                    $SQL .= ' FROM  ZUKAUFSORTIMENTZUORDNUNGEN';
                    $WG=$this->_DB->FeldInhaltFormat('T',$ArtikelAntwort->Warengruppe,true);
                    $SQL .= ' WHERE ZSZ_WG1 '.($WG=='null'?'IS NULL':' = '.$WG).'';
                    $SQL .= ' AND ZSZ_WG2 IS NULL';
                    $SQL .= ' AND ZSZ_WG3 IS NULL';
                    $SQL .= ' AND ZSZ_LIE_NR = \''.$this->_LIE_NR.'\'';
                    if(isset($DebugXML))
                    {
                        $DebugXML->addChild('WarengruppenSQL',$SQL);
                    }

                    $rsZSZ = $this->_DB->RecordSetOeffnen($SQL);
                    if($rsZSZ->EOF() AND $ArtikelAntwort->MainMaterialGroup!='')
                    {
                        $SQL = 'INSERT INTO Zukaufsortimentzuordnungen';
                        $SQL .= '(ZSZ_WG1,ZSZ_WG2,ZSZ_WG3,ZSZ_SORTIMENT,ZSZ_LIE_NR,ZSZ_BEZEICHNUNG,ZSZ_USER,ZSZ_USERDAT)';
                        $SQL .= ' VALUES(';
                        $SQL .= ' '.$this->_DB->FeldInhaltFormat('T',$ArtikelAntwort->Warengruppe,false);
                        $SQL .= ', null';
                        $SQL .= ', null';
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',self::DEFAULT_SORTIMENT);
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_LIE_NR);
                        $SQL .= ', null';
                        $SQL .= ', \'Preisanfrage\'';
                        $SQL .= ', SYSDATE)';

                        if($this->_DB->Ausfuehren($SQL)!==false)
                        {
                            $this->_awisWerkzeuge->EMail('shuttle@de.atu.eu','Neues Sortiment','Es wurde fuer den Lieferanten '.$this->_LIE_NR.' bei der Verf�gbarkeitsanfrage ein neues Sortiment mit der Kennung '.$ArtikelAntwort->Warengruppe.' angelegt. Der Artikel ist: '.$ArtikelAntwort->Artikelnummer);

                            $SQL = 'SELECT ZSZ_KEY, ZSZ_SORTIMENT';
                            $SQL .= ' FROM  ZUKAUFSORTIMENTZUORDNUNGEN';
                            $SQL .= ' WHERE ZSZ_WG1 = \''.''.'\'';
                            $SQL .= ' AND ZSZ_WG2 = \''.''.'\'';
                            $SQL .= ' AND ZSZ_WG3 = \''.''.'\'';
                            $SQL .= ' AND ZSZ_LIE_NR = \''.$this->_LIE_NR.'\'';
                            $rsZSZ = $this->_DB->RecordSetOeffnen($SQL);
                        }
                    }

                    $Hilf = $ArtikelTreffer->addChild('Warengruppe');
                    $Hilf->addChild('ZSZ_KEY',$rsZSZ->FeldInhalt('ZSZ_KEY'));
                    $Hilf->addChild('ZSZ_SORTIMENT',$rsZSZ->FeldInhalt('ZSZ_SORTIMENT'));

                    $Hilf = $ArtikelTreffer->addChild('Finanzen');
                    switch($ArtikelAntwort->Mwst)
                    {
                        case 'voller Satz':
                            $Hilf->addChild('MWS_ID',4);
                            break;
                        default:
                            $Hilf->addChild('MWS_ID',1);
                            break;
                    }

                    // MwSt Satz direkt aus der Datenbank lesen
                    if(!isset($Mehrwertsteuer[(int)$Hilf->MWS_ID]))
                    {
                        $rsMWS = $this->_DB->RecordSetOeffnen('SELECT MWS_SATZ from mehrwertsteuersaetze where mws_id = 0'.$Hilf->MWS_ID);
                        $Mehrwertsteuer[(int)$Hilf->MWS_ID] = ($this->_DB->FeldInhaltFormat('N4',$rsMWS->FeldInhalt('MWS_SATZ'),false)/100.0);
                    }
                    $Hilf->addChild('VKPR',(float)$ArtikelAntwort->Grosshandelsbrutto);
                    $Hilf->addChild('EKPR',(float)$ArtikelAntwort->Einkaufspreisbasis);
                    $Hilf->addChild('Rabatt1',abs((float)$ArtikelAntwort->Rabatt1));
                    $Hilf->addChild('Rabatt2',abs((float)$ArtikelAntwort->Rabatt2));
                    $Hilf->addChild('Altteilwert',(float)$ArtikelAntwort->TauschteilWert);

                    $EK = (float)$ArtikelAntwort->Einkaufspreisbasis
                            *(1-abs(((float)$ArtikelAntwort->Rabatt1)/100.0))
                            *(1-abs(((float)$ArtikelAntwort->Rabatt2)/100.0));
                    $Hilf->addChild('EK',$this->_DB->FeldInhaltFormat('N2',$EK));
                    $Hilf->addChild('MwSt',$this->_DB->FeldInhaltFormat('N4',$Mehrwertsteuer[(int)$Hilf->MWS_ID]));
                    switch((string)$ArtikelAntwort->Preiseinheit)
                    {
                        case '0':
                            $Hilf->addChild('VP',1);
                            break;
                        default:
                            $Hilf->addChild('VP',1);
                            break;
                    }

                    $Hilf->addChild('Preiseinheit',$this->_DB->FeldInhaltFormat('N0',$ArtikelAntwort->Preiseinheit));

                    $Hilf->addChild('VK',$this->ErmittleKundenVK($rsZLA->FeldInhalt('ZLA_KEY'),$this->_DB->FeldInhaltFormat('N2',$EK),$this->_DB->FeldInhaltFormat('N2',$ArtikelAntwort->Grosshandelsbrutto),$Mehrwertsteuer[(int)$Hilf->MWS_ID],$this->_FIL_LAN_CODE));

                    // Verf�gbarkeiten werden nicht immer geliefert!
                    if(!isset($ArtikelAntwort->Lieferungen->PreisanfrageLieferung))
                    {
                        $Hilf = $ArtikelTreffer->addChild('Verfuegbarkeit');
                        $Hilf->addChild('Liefermenge',0);
                        $Hilf->addChild('NurTelefonisch',0);

                        $Hilf1 = $Hilf->addChild('Verkaufshaus');
                        $Hilf1->addChild('Lieferid','');
                        $Hilf1->addChild('Liefermenge',0);
                        $Hilf1->addCData('Lieferanzeige','kein Bestand gemeldet.');
                        $Hilf1->addCData('Lieferanzeigetooltipp','Der Lieferant hat bei diesem Artikel keinen Bestand gemeldet.');
                        $Hilf1->addCData('Lieferort','');
                    }
                    else
                    {
                        if(!is_array($ArtikelAntwort->Lieferungen->PreisanfrageLieferung))
                        {
                            $Daten = array($ArtikelAntwort->Lieferungen->PreisanfrageLieferung);
                        }
                        else
                        {
                            $Daten = $ArtikelAntwort->Lieferungen->PreisanfrageLieferung;
                        }
                        // Alle Eintr�ge pr�fen
                        $Hilf = $ArtikelTreffer->addChild('Verfuegbarkeit');
                        $Hilf->addChild('Liefermenge',($Liefermenge));
                        $Hilf->addChild('NurTelefonisch',0);

                        foreach($Daten AS $Lieferungen)
                        {

                            $Liefermenge = $this->_DB->FeldInhaltFormat('Nx',$Lieferungen->Liefermenge);
                            $Rueckstandsmenge = $Menge-(float)$this->_DB->FeldInhaltFormat('N4',(string)$Lieferungen->Liefermenge);
                            $VerfuegbarkeitsText = utf8_decode((isset($Lieferungen->Bemerkung)?$Lieferungen->Bemerkung.', ':'')).$Lieferungen->Zeitpunkt;

                            $Hilf1 = $Hilf->addChild('Verkaufshaus');
                            $Hilf1->addChild('Lieferid','');
                            $Hilf1->addChild('Liefermenge',$Liefermenge);
                            $Hilf1->addCData('Lieferanzeige',($VerfuegbarkeitsText));
                            $Hilf1->addCData('Lieferanzeigetooltipp','');

                            $SQL = 'SELECT COALESCE(LVK_ANZEIGE,LVK_ORT) AS Lieferort ';
                            $SQL .= ' FROM lieferantenverkaufshaeuser';
                            $SQL .= ' WHERE lvk_lie_nr = '.$this->_DB->FeldInhaltFormat('T',$this->_LIE_NR);
                            $SQL .= ' AND lvk_kennung = '.$this->_DB->FeldInhaltFormat('T',(string)$Lieferungen->Verkaufshaus,true);
                            $rsLVK = $this->_DB->RecordsetOeffnen($SQL);

                            $Hilf1->addCData('Lieferort',$rsLVK->FeldInhalt('LIEFERORT'));
                        }
                    }
				}
			}

 		}
		catch (Exception $ex)
		{
			$Fehler = $this->_Daten->addChild('Fehler');
			$Fehler->addChild('Nummer',$ex->getCode());
			$Fehler->addChild('Text',$ex->getMessage());
			$Fehler->addChild('Zeile',$ex->getLine());
			$Fehler->addChild('SQL',$SQL);
		}
		return $this->_Daten;
	}


	/**
	 * Bestellt die angegegeben Artikel
	 *
	 * @param array $Artikelliste
	 * @param string $WANr
	 * @param string $Versandart
	 * @param string $Sachbearbeiter
	 */
	public function Bestellen(awisSimpleXMLElement $Artikelliste, $WANr, $Versandart, $Sachbearbeiter='AWIS')
	{
file_put_contents('/var/log/wm_bestellungen.log',"\n\nNeue Bestellung ".date('d.m.Y H:i:s')." \n",FILE_APPEND);
		// Best�tigungs-Email
		$this->_EMAIL_BESTAETIGUNG = awisWerkzeuge::LeseAWISParameter('zukauf.conf','EMAIL_BESTAETIGUNG');

        /*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:atu="http://atu.service.wm.de">
<soapenv:Header/>
<soapenv:Body>
<atu:Bestellung>
<atu:Kundennummer></atu:Kundennummer>
<atu:Filialkennung>0076</atu:Filialkennung>
<atu:Artikelnummer>J3603055</atu:Artikelnummer>
<atu:ArtikelnummerTyp>TecDoc</atu:ArtikelnummerTyp>
<atu:Hersteller>55</atu:Hersteller>
<atu:Menge>10</atu:Menge>
<atu:WANummer>?</atu:WANummer>
<atu:Artikelbestellnummer>01734812</atu:Artikelbestellnummer>
<atu:Lieferart>Lieferung</atu:Lieferart>
</atu:Bestellung>
</soapenv:Body>
</soapenv:Envelope>
        */

		switch ($Versandart)
		{
			case 1:		// Abholung
				$Versandart = self::LIEFERART_ABHOLUNG;
				break;
			case 2:		// Zustellung
				$Versandart = self::LIEFERART_ZUSTELLUNG;
				// Standard ist: leer lassen, WM entscheidet!
				break;
    		}

		$AnzPositionen = 0;

		$this->_Daten->addChild('Bestellstatus','##1##');		// Allgemeiner Status f�r Bestellt. Wird sp�ter genauer gesetzt.
		$this->_Daten->addChild('Benutzer',$Sachbearbeiter);
		$BestellteArtikel = $this->_Daten->addChild('Positionen');
		$NichtBestellt = 0;

file_put_contents('/var/log/wm_bestellungen.log',"\nVersandart:".$Versandart,FILE_APPEND);
file_put_contents('/var/log/wm_bestellungen.log',"\nSachbearbeiter:".$Sachbearbeiter,FILE_APPEND);

        $Param  = array('Kundennummer'=>''
                                ,'Filialkennung'=>  $this->_AnmeldeDaten['ZUSATZ']
                                ,'Artikelnummer'=>''
                                ,'ArtikelnummerTyp'=>''
                                ,'Hersteller'=>''
                                ,'Menge'=>0
                                ,'WANummer'=>$WANr
                                ,'Artikelbestellnummer'=>''
                                ,'Lieferart'=>'Lieferung'
                    );

        // Bei Werthenbach muss jeder Artikel einzeln bestellt werden!
        $NichtBestellt=0;
        $i=0;
		foreach ($Artikelliste->Artikel AS $Bestellposition)
		{
            $Param['Menge'] = (string)$Bestellposition->Menge;
            $Param['ArtikelnummerTyp']='TecDoc';

            $Param['Artikelnummer'] = $Bestellposition->Bestellnummer;
            $Param['Artikelbestellnummer'] = $Bestellposition->BestellIdLieferant;
            $Param['Hersteller']=(string)$Bestellposition->Hlka;
            $Param['Lieferart']=$Versandart;

file_put_contents('/var/log/wm_bestellungen.log',"\nBestelluebermittlung:".serialize($Bestellposition),FILE_APPEND);
file_put_contents('/var/log/wm_bestellungen.log',"\nBestelluebermittlung:".serialize($Param),FILE_APPEND);

            try
            {
                $Erg = $this->_SOAPClient->Bestellung($Param);
                //if($this->_DebugLevel>0)
                {
                    file_put_contents('/var/log/wm_bestellungen.log',"\nBestelluebermittlung Antwort :".serialize($Erg),FILE_APPEND);
                }

/*
 <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://www.w3.org/2001/XMLSchema">
<SOAP-ENV:Body>
<BestellungResponse xmlns="http://atu.service.wm.de">
<BestellungResult>
<Bestellnummer>?</Bestellnummer>
<Artikelbestellnummer>01734812</Artikelbestellnummer>
<Wunschmenge>10</Wunschmenge>
<Liefermenge>2</Liefermenge>
<Lieferzeitpunkt>2010-12-14 14:00</Lieferzeitpunkt>
<Rueckstand>7</Rueckstand>
<RueckstandZeitpunkt>2010-12-15 14:00</RueckstandZeitpunkt>
<Fehlmenge>1</Fehlmenge>
<FehlmengeHinweis>nicht auf Lager</FehlmengeHinweis>
<Verkaufshaus>101</Verkaufshaus>
<Lieferart>Lieferung</Lieferart>
</BestellungResult>
</BestellungResponse>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
 */
                if(isset($Erg->BestellungResult))
                {
                    $BestellErgebnis = $Erg->BestellungResult;
                    if($this->_DebugLevel>8)
                    {
                        echo 'Bestellposition:'.$BestellErgebnis.'<br>';
                    }

                    $BestellterArtikel = $BestellteArtikel->addChild('Artikel');
                    $BestellterArtikel->addChild('Bestellnummer',$Bestellposition->Bestellnummer);      // Im zweiten Teil ist die LiferantenArtikelnummer
                    $BestellterArtikel->addCData('Artikelbezeichnung',(string)$Bestellposition->Artikelbezeichnung);
                    $BestellterArtikel->addChild('Menge',(int)$BestellErgebnis->Wunschmenge);
                    $BestellterArtikel->addChild('Key',(string)$Bestellposition->Key);

                    //$LagerText = substr((isset($Werte['LiefermengeText'])?$Werte['LiefermengeText']:'').','.(isset($Werte['RueckstandText'])?$Werte['RueckstandText']:''),1);

                    $BestellterArtikel->addCData('Statustext',(string)$BestellErgebnis->Lieferzeitpunkt);

                    if((int)$BestellErgebnis->Liefermenge==0)
                    {
                        $NichtBestellt++;
                        $BestellterArtikel->addChild('Liefermenge',0);
                    }
                    else
                    {
                        $BestellterArtikel->addChild('Liefermenge',$BestellErgebnis->Liefermenge);
                    }

                    $BestellterArtikel->addChild('Rueckstand',(int)$BestellErgebnis->Rueckstand);
                    $BestellterArtikel->addChild('Fehlmenge',(int)$BestellErgebnis->Fehlmenge);

                    if((int)$BestellterArtikel->Liefermenge==(int)$BestellErgebnis->Wunschmenge)			// Alles wird geliefert
                    {
                        $BestellterArtikel->addChild('Status',1);       // Alles lieferbar
                    }
                    elseif((int)$BestellterArtikel->Rueckstand >0)
                    {
                        $BestellterArtikel->addChild('Status',0);       // Es wurde nicht alles geliefert
                    }
                    elseif((int)$BestellterArtikel->Liefermenge>(int)$BestellErgebnis->Wunschmenge)
                    {
                        $BestellterArtikel->addChild('Status',3);		// Warnung
                    }
                    else
                    {
                        $BestellterArtikel->addChild('Status',2);
                    }

                    $AnzPositionen++;
                }
                else        // Fehler bei der Anfrage
                {

                }
            }
            catch(Exception $ex)
            {
                file_put_contents('/var/log/wm_bestellungen.log',"\nFEHLER Antwort :".serialize($Erg)."\n".$ex->getMessage(),FILE_APPEND);
                file_put_contents('/var/log/wm_bestellungen.log',"\n".serialize($this->_SOAPClient->__getLastResponse())."\n",FILE_APPEND);

                $Fehler = $this->_Daten->addChild('Fehler');
                $Fehler->addChild('Text',utf8_encode($ex->getMessage()));
                $Fehler->addChild('Nummer',$ex->getCode());
            }
		}
		file_put_contents('/var/log/wm_bestellungen.log',"\nBestellpositionen:".$BestellteArtikel->asXML().PHP_EOL,FILE_APPEND);

        if($NichtBestellt>0 AND $NichtBestellt < $AnzPositionen)
        {
            $this->_Daten->Bestellstatus = '##2##';		// Wird von der Oberfl�che ausgewertet
        }
        elseif($NichtBestellt == $AnzPositionen)
        {
            $this->_Daten->Bestellstatus = '##3##';		// Wird von der Oberfl�che ausgewertet
        }

		return $this->_Daten;
	}

	public function Debuglevel($NeuerLevel=null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = (int)$NeuerLevel;
		}


		return $this->_DebugLevel;
	}
}
?>