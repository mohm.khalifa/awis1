<?php
global $con;
global $awisRSZeilen;
global $CursorFeld;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
$BSBreite = awis_BenutzerParameter($con, "BildschirmBreite" , $_SERVER['PHP_AUTH_USER']);

$TextKonserven = array();
$TextKonserven[]=array('MFI','MFI_%');
$TextKonserven[]=array('Liste','lst_MFI_RANKING');
$TextKonserven[]=array('FIL','FIL_BEZ');
$TextKonserven[]=array('Wort','km');
$TextKonserven[]=array('Liste','lst_JaNein');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010);

$MaxDSAnzahl = 500;

$SQL = 'SELECT *';
$SQL .= ' FROM mitbewerberfilialen INNER JOIN Filialen ON mfi_fil_id = fil_id';
$SQL .= ' WHERE mfi_mbw_key=0'.$Param;
if(isset($_GET['Edit']))
{
	$SQL .= ' AND mfi_key=0'.$_GET['Edit'];
}
elseif(isset($_GET['Neu']))
{
	$SQL .= ' AND mfi_key=0';
}

$rsMFI = awisOpenRecordset($con, $SQL);
$rsMFIZeilen = $awisRSZeilen;

//**************************************
// Liste anzeigen
//**************************************
if($rsMFIZeilen > 1 OR !isset($_GET['Edit']))
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3010&16)==16)	// Filialen zuordnen
	{
		$Icons[] = array('new','./mitbewerber_Main.php?cmdAktion=Details&Seite=Filialen&Edit=0');
		awis_FORM_Erstelle_ListeIcons($Icons,34,-1);
	}
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MFI']['MFI_FIL_ID'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['FIL']['FIL_BEZ'],250);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MFI']['MFI_ENTFERNUNG'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MFI']['MFI_RANKING'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MFI']['MFI_SICHTBAR'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MFI']['MFI_BEMERKUNG'],($BSBreite<1024?250:400));
	awis_FORM_ZeileEnde();

	if(!$rsMFIZeilen>$MaxDSAnzahl)
	{
		$rsMFIZeilen=$MaxDSAnzahl;
	}
	for($rsMFIZeile=0;$rsMFIZeile<$rsMFIZeilen;$rsMFIZeile++)
	{
		awis_FORM_ZeileStart();

		$Icons = array();
		if(($Recht3010&16)==16)	// Ändernrecht
		{
			$Icons[0] = array('edit','./mitbewerber_Main.php?cmdAktion=Details&Seite=Filialen&Edit='.$rsMFI['MFI_KEY'][$rsMFIZeile]);
		}
		if(($Recht3010&8192)==8192)	// Ändernrecht
		{
			$Icons[1] = array('delete','./mitbewerber_Main.php?cmdAktion=Details&Seite=Filialen&Del='.$rsMFI['MFI_KEY'][$rsMFIZeile]);
		}
		if(count($Icons)>0)
		{
			awis_FORM_Erstelle_ListeIcons($Icons,34,($rsMFIZeile%2));
		}

		$Link='/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsMFI['MFI_FIL_ID'][$rsMFIZeile];
		awis_FORM_Erstelle_ListenFeld('MFI_FIL_ID',$rsMFI['MFI_FIL_ID'][$rsMFIZeile],0,100,false,($rsMFIZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('FIL_BEZ',$rsMFI['FIL_BEZ'][$rsMFIZeile],0,250,false,($rsMFIZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MFI_ENTFERNUNG',$rsMFI['MFI_ENTFERNUNG'][$rsMFIZeile],0,100,false,($rsMFIZeile%2),'','','N2','R');
		$Ranking = '';
		$Liste = explode('|',$TXT_TextBaustein['Liste']['lst_MFI_RANKING']);
		foreach($Liste as $Eintrag)
		{
			$Eintrag = explode('~',$Eintrag);
			if($Eintrag[0]==isset($rsMFI['MFI_RANKING'][0]))
			{
				$Ranking = $Eintrag[1];
				break;
			}
		}
		awis_FORM_Erstelle_ListenFeld('MFI_RANKING',$Ranking,0,200,false,($rsMFIZeile%2));
		awis_FORM_Erstelle_ListenFeld('MFI_SICHTBAR',$rsMFI['MFI_SICHTBAR'][$rsMFIZeile],0,100,false,($rsMFIZeile%2),'','','n0','Z');
		awis_FORM_Erstelle_ListenFeld('MFI_BEMERKUNG',$rsMFI['MFI_BEMERKUNG'][$rsMFIZeile],0,($BSBreite<1024?250:400),false,($rsMFIZeile%2),'','');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 			// einen Datensatz anzeigen
{
	awis_FORM_FormularStart();

	echo '<input type=hidden name=txtMFI_KEY value='.(isset($rsMFI['MFI_KEY'][0])?$rsMFI['MFI_KEY'][0]:'').'>';

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MFI']['MFI_FIL_ID'],150,'');
	awis_FORM_Erstelle_TextFeld('MFI_FIL_ID',(isset($rsMFI['MFI_FIL_ID'][0])?$rsMFI['MFI_FIL_ID'][0]:''),5,100,(($Recht3010&8192)!=0)or(!isset($rsMFI['MFI_FIL_ID'][0])),'','','');
	awis_FORM_Erstelle_TextFeld('FIL_BEZ',(isset($rsMFI['FIL_BEZ'][0])?$rsMFI['FIL_BEZ'][0]:''),5,500,false,'','','');
	$CursorFeld='txtMFI_FIL_ID';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MFI']['MFI_ENTFERNUNG'],150,'');
	awis_FORM_Erstelle_TextFeld('MFI_ENTFERNUNG',(isset($rsMFI['MFI_ENTFERNUNG'][0])?$rsMFI['MFI_ENTFERNUNG'][0]:''),5,0,($Recht3010&6),'','','');
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['Wort']['km'],0,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MFI']['MFI_RANKING'],150,'');
	$Liste = explode('|',$TXT_TextBaustein['Liste']['lst_MFI_RANKING']);
	$UserFilialen=awisBenutzerFilialen($con,$_SERVER["PHP_AUTH_USER"],2);
	$FilRanking = ($UserFilialen=='' OR strpos(','.$UserFilialen.',',','.$rsMFI['MFI_FIL_ID'][0].',')!==false);
	awis_FORM_Erstelle_SelectFeld('MFI_RANKING',(isset($rsMFI['MFI_RANKING'][0])?$rsMFI['MFI_RANKING'][0]:''),300,$FilRanking,$con,'','','','','',$Liste,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MFI']['MFI_SICHTBAR'],150,'');
	$Liste = explode("|",$TXT_TextBaustein['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('MFI_SICHTBAR',(isset($rsMFI['MFI_SICHTBAR'][0])?$rsMFI['MFI_SICHTBAR'][0]:''),300,(($Recht3010&32768)!=0),$con,'','','','','',$Liste,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MFI']['MFI_BEMERKUNG'],150,'');
	awis_FORM_Erstelle_TextFeld('MFI_BEMERKUNG',(isset($rsMFI['MFI_BEMERKUNG'][0])?$rsMFI['MFI_BEMERKUNG'][0]:''),80,0,($Recht3010&6),'','','');
	awis_FORM_ZeileEnde();



	awis_FORM_FormularEnde();
}




?>