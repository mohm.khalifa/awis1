<?php
global $awisRSZeilen;
global $awisDBFehler;
global $con;
global $AWISBenutzer;

// eigene Stufe speichern
$TelefonverzeichnisStufe = awisBenutzerRecht($con, 151);
$AenderungsRecht = awisBenutzerRecht($con, 152);
$Recht150 = awisBenutzerRecht($con, 150);

/*********************************
 *
 *    Speichern von �nderungen
 *
 **********************************/

$txtKONKey = (isset($_REQUEST['txtKONKey'])?$_REQUEST['txtKONKey']:(isset($txtKONKey)?$txtKONKey:''));
$txtKIN_WERT = (isset($_REQUEST['txtKIN_WERT'])?$_REQUEST['txtKIN_WERT']:(isset($txtKIN_WERT)?$txtKIN_WERT:''));

//PG: Durch &KON_KEY=0 hatte man sofort Adminrechte.. Rechte Abfrage fehlte komplett
if (isset($_REQUEST['DSNeu']) OR isset($_POST["cmdDSNeu_x"]) OR $txtKONKey <= 0) {
    if ((($Recht150 & 2) != 2)) {
        awisEreignis(3, 1000, 'Telefonbuch', $AWISBenutzer->BenutzerName(), '', '', '');
        die("Sie haben nicht die erforderlichen Rechte um diese Aktion auszuf�hren.");
    }
}

if (isset($_POST["cmdSpeichern_x"])) {
    if ($txtKONKey == 0)            // Neu hinzuf�gen
    {
        // N�chsten KEY ermitteln
        // Datensatz speichern
        $SQL = "INSERT INTO Kontakte(KON_STRASSE, KON_LAN_CODE, KON_PLZ, KON_ORT, KON_BEMERKUNG,KON_BEMERKUNGEMPFANG,KON_ADRESSEAUSBLENDEN,";
        $SQL .= "KON_KKA_KEY, KON_ZUSTAENDIGKEIT, KON_NAME1, KON_NAME2, KON_GEB_ID, KON_KBE_KEY, KON_LISTEN";
        $SQL .= ",KON_PER_NR, KON_STATUS, KON_USER, KON_USERDAT)";

        // Benutzerfelder
        $SQL .= "VALUES('" . $_POST["txtKON_STRASSE"] . "'";
        $SQL .= ", '" . $_POST["txtKON_LAN_CODE"] . "'";
        $SQL .= ", '" . $_POST["txtKON_PLZ"] . "'";
        $SQL .= ", '" . $_POST["txtKON_ORT"] . "'";
        $SQL .= ", '" . $_POST["txtKON_BEMERKUNG"] . "'";
        $SQL .= ", '" . $_POST["txtKON_BEMERKUNGEMPFANG"] . "'";
        $SQL .= ", '" . $_POST["txtKON_ADRESSEAUSBLENDEN"] . "'";

        // Adminfelder
        $SQL .= ", '" . $_POST["txtKON_KKA_KEY"] . "'";
        $SQL .= ", '" . $_POST["txtKON_ZUSTAENDIGKEIT"] . "'";

        $SQL .= ", '" . $_POST["txtKON_NAME1"] . "'";
        $SQL .= ", '" . $_POST["txtKON_NAME2"] . "'";
        $SQL .= ", '" . $_POST["txtKON_GEB_ID"] . "'";
        $SQL .= ", '" . $_POST["txtKON_KBE_KEY"] . "'";

        $foo = 0;
        $foo += (isset($_POST["chkListe_Telefon"])?$_POST["chkListe_Telefon"]:'0');
        $foo += (isset($_POST["chkListe_Filiale"])?$_POST["chkListe_Filiale"]:'0');
        $SQL .= ", 0" . $foo . "";

        $SQL .= ", '" . $_POST["txtKON_PER_NR"] . "'";
        $SQL .= ", '" . $_POST["txtKON_STATUS"] . "'";
        $SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
        $SQL .= ", SYSDATE";

        $SQL .= " )";

        $Erg = awisExecute($con, $SQL);
        if ($Erg == false) {
            awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
        } else {
            $SQL = "SELECT SEQ_KON_KEY.CURRVAL AS KEY FROM DUAL";
            $rsNextVal = awisOpenRecordset($con, $SQL);
            $txtKONKey = $rsNextVal["KEY"][0];
            unset($rsNextVal);
        }
    } else {
        if (isset($_POST["txtKON_STRASSE"]))        // Gibt es �nderungsfelder
        {
            $SQL = "UPDATE Kontakte";

            // Benutzerfelder
            $SQL .= " SET KON_STRASSE='" . $_POST["txtKON_STRASSE"] . "'";
            $SQL .= ", KON_LAN_CODE='" . $_POST["txtKON_LAN_CODE"] . "'";
            if (isset($_POST["txtKON_PER_NR"])) {
                $SQL .= ", KON_PER_NR='" . $_POST["txtKON_PER_NR"] . "'";
            }
            if (isset($_POST["txtKON_STATUS"])) {
                $SQL .= ", KON_STATUS='" . $_POST["txtKON_STATUS"] . "'";
            }
            $SQL .= ", KON_PLZ='" . $_POST["txtKON_PLZ"] . "'";
            $SQL .= ", KON_ORT='" . $_POST["txtKON_ORT"] . "'";
            $SQL .= ", KON_BEMERKUNG='" . str_replace("\\\\", "\\",
                    str_replace('\\"', '"', $_POST["txtKON_BEMERKUNG"])) . "'";
            $SQL .= ", KON_BEMERKUNGEMPFANG='" . str_replace("\\\\", "\\",
                    str_replace('\\"', '"', $_POST["txtKON_BEMERKUNGEMPFANG"])) . "'";
            $SQL .= ", KON_ADRESSEAUSBLENDEN = " . (isset($_POST["txtKON_ADRESSEAUSBLENDEN"]) && $_POST["txtKON_ADRESSEAUSBLENDEN"] == 1?1:0) . "";
            // Adminfelder
            if ($_POST["txtKON_KKA_KEY"] <> '') {
                // Stufe 2
                $SQL .= ", KON_KKA_KEY='" . $_POST["txtKON_KKA_KEY"] . "'";
                $SQL .= ", KON_ZUSTAENDIGKEIT='" . str_replace("\\\\", "\\",
                        str_replace('\\"', '"', $_POST["txtKON_ZUSTAENDIGKEIT"])) . "'";
                $SQL .= ", KON_KBE_KEY=" . $_POST["txtKON_KBE_KEY"] . "";
            }

            if ($_POST["txtKON_NAME1"] <> '') {
                // Stufe 4

                $SQL .= ", KON_NAME1='" . $_POST["txtKON_NAME1"] . "'";
                $SQL .= ", KON_NAME2='" . $_POST["txtKON_NAME2"] . "'";
                $SQL .= ", KON_GEB_ID='" . $_POST["txtKON_GEB_ID"] . "'";

                // DURCHWAHL
                // FAX-DURCHWAHL
            }
            // Falls die Bearbeitung erlaubt ist!
            if (($AenderungsRecht & 32) == 32) {
                $foo = 0;
                if (isset($_POST["chkListe_Telefon"])) {
                    $foo += $_POST["chkListe_Telefon"];
                }
                if (isset($_POST["chkListe_Filiale"])) {
                    $foo += $_POST["chkListe_Filiale"];
                }
                $SQL .= ", KON_LISTEN=0" . $foo . "";
            }

            $SQL .= ", KON_USER='" . $AWISBenutzer->BenutzerName() . "'";
            $SQL .= ", KON_USERDAT = SYSDATE";

            $SQL .= " WHERE KON_KEY = 0" . $txtKONKey;
            awisExecute($con, $SQL);
        }
    }

    //  Abteilungen SPEICHERN
    $SQL = "";
    if (isset($_POST['txtKON_KAB_KEY']) AND ($_POST['txtKON_KAB_KEY'] > 0)) {
        $SQL = "INSERT INTO KontakteAbteilungenZuordnungen(KZA_KON_KEY, KZA_KAB_KEY, KZA_BEMERKUNG, KZA_LISTENHINWEIS, KZA_USER, KZA_USERDAT)";
        $SQL .= " VALUES(";
        $SQL .= "" . $txtKONKey;
        $SQL .= ", " . $_POST['txtKON_KAB_KEY'];
        $SQL .= ", '" . $_POST['txtKZA_BEMERKUNG'] . "'";
        $SQL .= ", '" . $_POST['txtKZA_LISTENHINWEIS'] . "'";
        $SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
        $SQL .= ", SYSDATE";
        $SQL .= ")";

        awisExecute($con, $SQL);
    }    // Abteilungen


//Abteilungen, f�r die der Kontakt zust�ndig ist speichern

    $SQL = "";
    if(isset($_POST['txtKAB_ITKONTAKT'])) {

        $BindeVariablen = array();
        $BindeVariablen['txtKONKEY'] = $txtKONKey;
        $BindeVariablen['txtKAB_ITKONTAKT']= $_POST['txtKAB_ITKONTAKT'];

        If (($_POST['txtKAB_ITKONTAKT']) > 0) {
            $SQL = " INSERT INTO KONTAKTEINFOS(KIN_KON_KEY, KIN_ITY_KEY, KIN_WERT)";
            $SQL .= " VALUES(";
            $SQL .= ':txtKONKEY';
            $SQL .= ", 322 ";
            $SQL .= ', :txtKAB_ITKONTAKT';
            $SQL .= ")";

            awisExecute($con, $SQL,TRUE,false,0,$BindeVariablen);
        }
    }
    //  Systembenutzer SPEICHERN
    $SQL = "";
    If (isset($_POST['txtKON_XBE_KEY']) AND $_POST['txtKON_XBE_KEY'] > 0) {
        $SQL = "UPDATE Benutzer";
        $SQL .= " SET XBN_KON_KEY = " . $txtKONKey;
        $SQL .= " ,XBN_USER = '" . $AWISBenutzer->BenutzerName() . "'";
        $SQL .= " ,XBN_USERDAT = SYSDATE";
        $SQL .= " WHERE XBN_KEY = " . $_POST['txtKON_XBE_KEY'];

        awisExecute($con, $SQL);
    }

    // KFZ SPEICHERN
    $SQL = "";
    $Autos = awis_NameInArray($_POST, 'txtKFA_KENNZEICHEN_', 1);
    if($Autos!==false){
        $Autos = explode(';', $Autos);
    }else{
        $Autos = array();
    }

    foreach ($Autos as $Eintrag) {
        $ID = substr($Eintrag, 19);

        if ($ID == 0)        // Neuer Eintrag
        {
            if (($_POST['txtKFA_KENNZEICHEN_0'] != '' OR $_POST['txtKFA_FAHRZEUGTYP_0'] != '')) {
                $SQL = "INSERT INTO KontakteFahrzeuge(KFA_KON_KEY, KFA_KENNZEICHEN, KFA_FAHRZEUGTYP, KFA_USER, KFA_USERDAT) ";
                $SQL .= " VALUES(" . $txtKONKey . ",'" . $_POST['txtKFA_KENNZEICHEN_0'] . "','";
                $SQL .= $_POST['txtKFA_FAHRZEUGTYP_0'] . "','" . $AWISBenutzer->BenutzerName() . "',SYSDATE)";
                if (!awisExecute($con, $SQL)) {
                    awis_Debug(1, $SQL);
                }
            }
        } else {
            if ($_POST['txtKFA_KENNZEICHEN_' . $ID] !== '' OR $_POST['txtKFA_FAHRZEUGTYP_' . $ID] !== '') {
                $BindeVariablen = array();
                $BindeVariablen['var_N0_kfa_key'] = intval('0' . $ID);
                $rsKFA = awisOpenRecordset($con, "SELECT * FROM KontakteFahrzeuge WHERE KFA_KEY=:var_N0_kfa_key", true,
                    false, $BindeVariablen);

                if (ISSET($_POST['txtKFA_FAHRZEUGTYP_' . $ID]) AND ($_POST['txtKFA_FAHRZEUGTYP_' . $ID] != @$rsKFA[0]['KFA_FAHRZEUGTYP'] or $_POST['txtKFA_KENNZEICHEN_' . $ID] != @$rsKFA[0]['KFA_KENNZEICHEN'])) {
                    $SQL = "UPDATE KontakteFahrzeuge SET KFA_KENNZEICHEN='" . $_POST['txtKFA_KENNZEICHEN_' . $ID] . "'";
                    $SQL .= ", KFA_FAHRZEUGTYP='" . $_POST['txtKFA_FAHRZEUGTYP_' . $ID] . "'";
                    $SQL .= ", KFA_USER='" . $AWISBenutzer->BenutzerName() . "'";
                    $SQL .= ", KFA_USERDAT = SYSDATE";
                    $SQL .= " WHERE KFA_KEY=0" . $ID;
                    if (!awisExecute($con, $SQL)) {
                        awis_Debug(1, $SQL);
                    }
                }
            } else {
                $BindeVariablen = array();
                $BindeVariablen['var_N0_kfa_key'] = intval('0' . $ID);
                $SQL = 'DELETE FROM KontakteFahrzeuge WHERE KFA_KEY=:var_N0_kfa_key';
                if (!awisExecute($con, $SQL, true, false, 0, $BindeVariablen)) {
                    awis_Debug(1, $SQL);
                }
            }
        }
    }

    // Kontakte-Infos speichern

    $SQL = "";
    foreach ($_POST as $Eintrag => $EintragWert) {
        if (strpos($Eintrag, "txtKKO_KOT_KEY") !== false) {
            $Nr = substr($Eintrag, 15);
            if ($Nr == '0') {
                if ($_POST["txtKKO_KOT_KEY_$Nr"] > 0) {
                    $SQL = "INSERT INTO KONTAKTEKOMMUNIKATION(KKO_KOT_KEY, KKO_KON_KEY, KKO_WERT, KKO_KURZWAHL, KKO_BEMERKUNG, KKO_USER, KKO_USERDAT)";
                    $SQL .= " VALUES (" . $_POST["txtKKO_KOT_KEY_$Nr"];
                    $SQL .= "," . $txtKONKey;
                    $SQL .= ",'" . $_POST["txtKKO_WERT_$Nr"] . "'";
                    $SQL .= ",'" . $_POST["txtKKO_KURZWAHL_$Nr"] . "'";
                    $SQL .= ",'" . $_POST["txtKKO_BEMERKUNG_$Nr"] . "'";
                    $SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
                    $SQL .= ",SYSDATE)";
                } else {
                    $SQL = "";    // Keine Aktion ausl�sen
                }
            } else {
                $SQL = "UPDATE KONTAKTEKOMMUNIKATION SET KKO_KOT_KEY=" . $_POST["txtKKO_KOT_KEY_$Nr"];
                $SQL .= ", KKO_KON_KEY=" . $txtKONKey;
                $SQL .= ", KKO_WERT='" . $_POST["txtKKO_WERT_$Nr"] . "'";
                $SQL .= ", KKO_KURZWAHL='" . $_POST["txtKKO_KURZWAHL_$Nr"] . "'";
                $SQL .= ", KKO_BEMERKUNG='" . $_POST["txtKKO_BEMERKUNG_$Nr"] . "'";
                $SQL .= ", KKO_USER='" . $AWISBenutzer->BenutzerName() . "'";
                $SQL .= ", KKO_USERDAT=SYSDATE";

                $SQL .= " WHERE KKO_Key=" . $Nr;
            }

            if ($SQL != '') {
                awisExecute($con, $SQL);
            }

        }    // Kontakt-Parameter
        elseif (strpos($Eintrag, "txtNUR_KKO_BEMERKUNG_") !== false)        // Nur �nderung der Bemerkung
        {
            $Nr = substr($Eintrag, 21);
            if ($Nr > '0') {
                $SQL = "UPDATE KONTAKTEKOMMUNIKATION SET ";
                $SQL .= " KKO_BEMERKUNG='" . $_POST["txtNUR_KKO_BEMERKUNG_$Nr"] . "'";
                $SQL .= ", KKO_USER='" . $AWISBenutzer->BenutzerName() . "'";
                $SQL .= ", KKO_USERDAT=SYSDATE";
                $SQL .= " WHERE KKO_Key=" . $Nr;

                awisExecute($con, $SQL);
            }
        }
    }    // Alles Durchsuchen

}        // �nderungen speichern

//***********************************************
//
// Daten l�schen
//
//***********************************************

// KFZ L�schen
foreach ($_POST as $Eintrag => $EintragWert) {
    if (strpos($Eintrag, "cmdDelKFA_") !== false) {
        $SQL = (substr($Eintrag, 10));
        $SQL = substr($SQL, 0, strlen($SQL) - 2);
        $SQL = "DELETE FROM KontakteFahrzeuge WHERE KFA_KEY=0" . $SQL;

        awisExecute($con, $SQL);
    }
}

// Kontakt-Info l�schen
foreach ($_POST as $Eintrag => $EintragWert) {
    if (strpos($Eintrag, "cmdDelKKO_") !== false) {
        $SQL = (substr($Eintrag, 10));
        $SQL = substr($SQL, 0, strlen($SQL) - 2);
        $SQL = "DELETE FROM KontakteKommunikation WHERE KKO_KEY=0" . $SQL;

        awisExecute($con, $SQL);
    }
}

// Kontakt-Abteilung l�schen

foreach ($_POST as $Eintrag => $EintragWert) {
    if (strpos($Eintrag, "cmdDelKZA_") !== false) {
        $SQL = explode('~', substr($Eintrag, 10));

        $SQL = "DELETE FROM KontakteAbteilungenZuordnungen WHERE KZA_KON_KEY=0" . $SQL[0] . ' AND KZA_KAB_KEY=0' . intval($SQL[1]);

        awisExecute($con, $SQL);
    }
}

// Kontakt-Abteilungsansprechpartner l�schen

foreach ($_POST as $Eintrag => $EintragWert) {
    if (strpos($Eintrag, "cmdDelKZB_") !== false) {
        $SQL = explode('~', substr($Eintrag, 10));

        $SQL = "DELETE FROM Kontakteinfos WHERE KIN_KON_KEY=0" . $SQL[0] . ' AND KIN_WERT=0' . intval($SQL[1]);

        awisExecute($con, $SQL);
    }
}

// Kontakt gesamt l�schen

if (isset($_GET['Loeschen']) OR isset($_POST["cmdDSLoeschen_x"])) {
    if (!isset($_GET['Loeschen']) OR $_GET['Loeschen'] == "") {
        echo "<p align=center class=Hinweistext>Wollen Sie den Kontakt wirklich l�schen?</p>";
        echo "<table width=100% border=0><tr><td align=center><a href=./telefon_Main.php?cmdAktion=Liste&Loeschen=Ja&txtKONKey=" . $txtKONKey . ">Ja</a>";
        echo "&nbsp;&nbsp;&nbsp;<a href=./telefon_Main.php?cmdAktion=Liste&txtKONKey=" . $txtKONKey . ">Nein</a>";
        echo "</td></tr></table>";

        awislogoff($con);

        die;        // Seite beenden
    } else {
        $SQL = "DELETE FROM Kontakte WHERE KON_KEY=0" . $txtKONKey;
        awisExecute($con, $SQL);
        echo "<p align=center class=Hinweistext>Der Kontakt wurde erfolgreich gel�scht.</p>";
        echo "<p align=center><a href=./telefon_Main.php?cmdAktion=Suche>Weiter</a></p>";

        awislogoff($con);

        die;        // Seite beenden
    }
}

//***********************************************
//
// Beginn Hauptmaske aufbauen
//
//***********************************************

echo "<form name=frmTelefon method=POST action=./telefon_Main.php?cmdAktion=Liste>";

// Telefondaten �ndern erlaubt?
if ($AenderungsRecht > 0) {
    $NurLesen = false;
} else {
    $NurLesen = true;
}

$SQL = "";


$BindeVariablen = array();
$BindeVariablen['var_N0_kon_key'] = $txtKONKey;
$SQL = "SELECT DISTINCT KONTAKTE.*, TO_CHAR(KON_USERDAT, 'DD.MM.RRRR HH24:MI') AS AenderungsZeit, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, XBN_KEY ";
$SQL .= ", GEB_BEZEICHNUNG ";
$SQL .= " FROM Kontakte, KontakteBEREICHE, KontakteKATEGORIEN, GEBAEUDE, BENUTZER ";
$SQL .= " WHERE ((KBE_KEY(+) = KON_KBE_KEY AND KON_KKA_KEY = KKA_KEY(+)) AND GEB_ID(+) = KON_GEB_ID ";
$SQL .= " AND XBN_KON_KEY(+) = KON_KEY AND KON_KEY=:var_N0_kon_key)";

if (isset($_REQUEST['DSNeu']) OR isset($_POST["cmdDSNeu_x"]) OR $txtKONKey == -1) {
    $rsKommunikationZeilen = 0;
    $txtKONKey = 0;
} else {
    $rsKontakte = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
    $rsKontakteZeilen = $awisRSZeilen;
}

if ((isset($rsKontakteZeilen) AND $rsKontakteZeilen == 0) AND (!isset($_POST["cmdDSNeu_x"]) AND $txtKONKey == -1)) {
    echo "<span class=HinweisText>Es wurden keine passenden Kontakte f�r die Auswahl gefunden.</span>";
} else {
    if (isset($rsKontakteZeilen) AND $rsKontakteZeilen == 0) {
        $EigeneAendern = true;
        $AenderungsRecht = 65535;
    }

    $BindeVariablen = array();
    $BindeVariablen['var_N0_kon_key'] = intval('0' . $txtKONKey);
    $rsKommunikation = awisOpenRecordset($con,
        "SELECT * FROM KONTAKTEKOMMUNIKATION,KOMMUNIKATIONSTYPEN WHERE KKO_KOT_KEY = KOT_KEY AND KKO_KON_KEY=:var_N0_kon_key ORDER BY KOT_SORTIERUNG, KOT_TYP, KKO_WERT",
        true, false, $BindeVariablen);
    $rsKommunikationZeilen = $awisRSZeilen;

    $EigeneAendern = false;
    $IDs = awisBenutzerID($AWISBenutzer->BenutzerName(), true);

    for ($i = 0; $i < count($IDs); $i++) {
        if (isset($rsKontakte["XBN_KEY"][0]) AND $rsKontakte["XBN_KEY"][0] == $IDs[$i]) {
            $EigeneAendern = true;
            break;
        }
    }

    // �nderungsberechtigungen
    echo "<table width=100% border=0><tr>";
    echo "<td align=right><font size=1>" . (isset($rsKontakte['KON_USER'][0])?$rsKontakte['KON_USER'][0]:'') . '<br>' . (isset($rsKontakte['AENDERUNGSZEIT'][0])?$rsKontakte['AENDERUNGSZEIT'][0]:'') . '</td>';
    echo "<td width=100 align=right><img name=cmdListe border=0 src=/bilder/NeueListe.png accesskey=t title='Trefferliste (Alt+T)' onclick=location.href='./telefon_Main.php?cmdAktion=Liste'></td></tr></table>";

    if ($AenderungsRecht > 0 OR $EigeneAendern == true) {

        // *********************************
        // Schaltfl�chen zum �ndern
        // *********************************

        echo "<table border=0 width=100%><tr><td><font size=5pt>Kontaktpflege</font></td><td align=right><input type=image title=\"Speichern (Alt+S)\" src=/bilder/diskette.png name=cmdSpeichern accesskey=s>";
        if ((awisBenutzerRecht($con, 150) & 4) == 4) {
            echo "&nbsp;<input type=image alt=\"L�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdDSLoeschen  accesskey=x onclick=location.href='./telefon_Main.php?" . $_SERVER['QUERY_STRING'] . "&Loeschen=True'>";
        }
        echo "</td></tr></table><hr>";
    }

    echo '<table border=0 width=100%>';
    echo ' <tr>';

    // Zeile1
    if ($AenderungsRecht & 4)        // Recht Stufe2
    {
        echo "  <td width=150><input type=hidden name=txtKONKey value=0" . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:'') . ">Name1:</td><td><input type=text size=30 name=txtKON_NAME1 value='" . (isset($rsKontakte["KON_NAME1"][0])?$rsKontakte["KON_NAME1"][0]:'') . "'>Name2:<input size=25 name=txtKON_NAME2 size=30 value='" . (isset($rsKontakte["KON_NAME2"][0])?$rsKontakte["KON_NAME2"][0]:'') . "'></td>";
    } else {
        echo "  <td width=150><input type=hidden name=txtKONKey value=" . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:'') . ">Name:</td><td><b>" . (isset($rsKontakte["KON_NAME1"][0])?$rsKontakte["KON_NAME1"][0]:'') . " " . (isset($rsKontakte["KON_NAME2"][0])?$rsKontakte["KON_NAME2"][0]:'') . "</td>";
    }

    echo "  <td width=80>Durchwahl:</td><td>";
    $tels = explode(', ', Kommunikationseintrag($rsKommunikation, 9, 11));
    $lastElement = end($tels);
    foreach ($tels as $tel) {
        if (preg_match('/MOBILE/', strtoupper($_SERVER['HTTP_USER_AGENT']))) {
            echo "<a href=\"tel:0961306" . $tel . "\"><img src=\"/bilder/callBtn.png\" alt=\"Call\" style=\"vertical-align: sub\"/><b>" . $tel . "</b></a>";
        } else {
            echo "<b>" . $tel . "</b>";
        }
        if ($tel != $lastElement) {
            echo ', ';
        }
    }
    echo "</td></tr>";

    // Zeile2
    if ($EigeneAendern OR ($AenderungsRecht & 1) == 1) {
        echo "  <tr><td width=150>Strasse:</td><td><input size=40 type=text name=txtKON_STRASSE value='" . (isset($rsKontakte["KON_STRASSE"][0])?$rsKontakte["KON_STRASSE"][0]:'') . "'></td>";
    } else {
        echo '  <tr><td width=150>Strasse:</td><td><b>';
        if (isset($rsKontakte["KON_ADRESSEAUSBLENDEN"][0]) AND $rsKontakte["KON_ADRESSEAUSBLENDEN"][0] == 1)            // Daten verstecken f�r die Allgemeinheit
        {
            echo '<font size=2><i>::Adresse ausgeblendet::</i></font>';
        } else {
            echo '' . (isset($rsKontakte["KON_STRASSE"][0])?$rsKontakte["KON_STRASSE"][0]:'');
        }
        echo '</b></td>';
    }

    echo "<td width=150>Fax-Durchwahl:</td><td><b>" . Kommunikationseintrag($rsKommunikation, 10) . "</b></td>";
    echo "</tr>";

    // Zeile3
    if ($EigeneAendern OR ($AenderungsRecht & 1) == 1) {
        echo "  <tr><td width=150>Land/Plz/Ort:</td><td colspan=2>";
        echo "<Select size=1 name=txtKON_LAN_CODE>";
        $rsLaender = awisOpenRecordset($con, "SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_CODE");
        $rsLaenderZeilen = $awisRSZeilen;
        for ($i = 0; $i < $rsLaenderZeilen; $i++) {
            echo "<option ";
            if ((isset($rsKontakte["KON_LAN_CODE"][0]) AND $rsKontakte["KON_LAN_CODE"][0] == $rsLaender["LAN_CODE"][$i]) OR ($txtKONKey == 0 AND $rsLaender["LAN_CODE"][$i] == 'DE')) {
                echo " selected ";
            }
            echo " value='" . $rsLaender["LAN_CODE"][$i] . "'>" . $rsLaender["LAN_CODE"][$i] . "</option>";
        }
        echo "</select>";

        echo " <input size=8 type=text name=txtKON_PLZ value='" . (isset($rsKontakte["KON_PLZ"][0])?$rsKontakte["KON_PLZ"][0]:'') . "'>";
        echo " <input size=40 type=text name=txtKON_ORT value='" . (isset($rsKontakte["KON_ORT"][0])?$rsKontakte["KON_ORT"][0]:'') . "'>";
        echo " <input type=checkbox name=txtKON_ADRESSEAUSBLENDEN value='1' " . (isset($rsKontakte["KON_ADRESSEAUSBLENDEN"][0]) && $rsKontakte["KON_ADRESSEAUSBLENDEN"][0] == 0?'':'checked') . ">&nbsp;Adresse ausblenden";
        echo "</td>";
    } else {
        echo '<tr><td width=150>Land/Plz/Ort:</td><td><b>';
        if (isset($rsKontakte["KON_ADRESSEAUSBLENDEN"][0]) AND $rsKontakte["KON_ADRESSEAUSBLENDEN"][0] == '1') {
            echo '<font size=2><i>::Adresse ausgeblendet::</i></font>';
        } else {
            echo '' . (isset($rsKontakte["KON_LAN_CODE"][0])?$rsKontakte["KON_LAN_CODE"][0]:'') . (!isset($rsKontakte["KON_LAN_CODE"][0]) || $rsKontakte["KON_LAN_CODE"][0] == ''?'':'-') . (isset($rsKontakte["KON_PLZ"][0])?$rsKontakte["KON_PLZ"][0]:'') . " " . (isset($rsKontakte["KON_ORT"][0])?$rsKontakte["KON_ORT"][0]:'');
        }
        echo '</b></td>';
    }

    // Zeile4
    if (($AenderungsRecht & 2)==2)        // Recht Stufe2
    {
        echo "  <tr><td width=150>Kategorie:</td><td>";
        echo "<select name=txtKON_KKA_KEY size=1>";
        $BindeVariablen = array();
        $BindeVariablen['var_N0_stufe'] = $TelefonverzeichnisStufe;
        $rsTKA = awisOpenRecordset($con,
            "SELECT * FROM KontakteKATEGORIEN WHERE BITAND(KKA_KZG_ID,:var_N0_stufe)>0 ORDER BY KKA_BEZEICHNUNG", true,
            false, $BindeVariablen);
        for ($j = 0; $j < $awisRSZeilen; $j++) {
            echo "<option ";
            if (isset($rsKontakte["KON_KKA_KEY"][0]) AND $rsTKA["KKA_KEY"][$j] == $rsKontakte["KON_KKA_KEY"][0]) {
                echo " selected ";
            }
            echo "value=" . $rsTKA["KKA_KEY"][$j] . ">" . $rsTKA["KKA_BEZEICHNUNG"][$j] . "</option>";
        }

        unset($rsTKA);
        echo "</select></td><td></td>";
    } else {
        echo "  <tr><td width=150>Kategorie:</td><td><b>" . (isset($rsKontakte["KKA_BEZEICHNUNG"][0])?$rsKontakte["KKA_BEZEICHNUNG"][0]:'') . "</b></td><td></td>";
    }
    echo "</tr>";

    // Zeile 4a

    if (($AenderungsRecht & 2)==2)        // Recht Stufe2
    {
        $BindeVariablen = array();
        $BindeVariablen['var_N0_stufe'] = $TelefonverzeichnisStufe;
        $rsBereiche = awisOpenRecordset($con,
            "SELECT * FROM KontakteBereiche WHERE BITAND(KBE_KZG_ID,:var_N0_stufe)>0 ORDER BY KBE_BEZEICHNUNG", true,
            false, $BindeVariablen);
        $rsBereicheZeilen = $awisRSZeilen;

        echo "<tr><td width=180>B<u>e</u>reich:</td>";
        echo "<td><select name=txtKON_KBE_KEY accesskey='e' size=1 tabindex=10>";
        for ($i = 0; $i < $rsBereicheZeilen; $i++) {
            echo "<option ";
            if (isset($rsKontakte["KON_KBE_KEY"][0]) AND $rsBereiche["KBE_KEY"][$i] == $rsKontakte["KON_KBE_KEY"][0]) {
                echo " selected ";
            }
            echo " value=" . $rsBereiche["KBE_KEY"][$i] . ">" . $rsBereiche["KBE_BEZEICHNUNG"][$i] . "</option>";
        }
        echo "</select>";
        unset($rsBereiche);        // Speicher freigeben
    }

    // Zeile6
    if ($AenderungsRecht & 2)        // Recht Stufe2
    {
        echo "  <tr><td width=150>Zust�ndigkeit:</td><td><input size=80 type=text name=txtKON_ZUSTAENDIGKEIT value='" . (isset($rsKontakte["KON_ZUSTAENDIGKEIT"][0])?$rsKontakte["KON_ZUSTAENDIGKEIT"][0]:'') . "'></td><td></td>";
    } else {
        echo "  <tr><td width=150>Zust�ndigkeit:</td><td><b>" . (isset($rsKontakte["KON_ZUSTAENDIGKEIT"][0])?$rsKontakte["KON_ZUSTAENDIGKEIT"][0]:'') . "</b></td><td></td>";
    }
    echo "</tr>";

    // Zeile7
    if ($AenderungsRecht & 4)        // Recht Stufe2
    {
        echo "  <tr><td width=150>ATU-Geb�ude:</td><td>";
        echo "<select name=txtKON_GEB_ID size=1>";
        $rsGEB = awisOpenRecordset($con, "SELECT * FROM Gebaeude ORDER BY GEB_BEZEICHNUNG");
        for ($j = 0; $j < $awisRSZeilen; $j++) {
            echo "<option ";
            if (isset($rsKontakte["KON_GEB_ID"][0]) AND $rsGEB["GEB_ID"][$j] == $rsKontakte["KON_GEB_ID"][0]) {
                echo " selected ";
            }
            echo "value=" . $rsGEB["GEB_ID"][$j] . ">" . $rsGEB["GEB_BEZEICHNUNG"][$j] . "</option>";
        }

        unset($rsGEB);
        echo "</select></td><td></td>";
    } else {
        echo "  <tr><td width=150>ATU-Geb�ude:</td><td><b>" . (isset($rsKontakte["GEB_BEZEICHNUNG"][0])?$rsKontakte["GEB_BEZEICHNUNG"][0]:'') . "</b></td><td></td>";
    }
    echo "</tr>";

    // Zeile8
    if ($EigeneAendern OR ($AenderungsRecht & 1) == 1) {
        echo "  <tr><td width=150>Bemerkung:</td><td><input size=80 type=text name=txtKON_BEMERKUNG value='" . (isset($rsKontakte["KON_BEMERKUNG"][0])?$rsKontakte["KON_BEMERKUNG"][0]:'') . "'></td>";
    } else {
        echo "  <tr><td width=150>Bemerkung:</td><td><b>" . (isset($rsKontakte["KON_BEMERKUNG"][0])?$rsKontakte["KON_BEMERKUNG"][0]:'') . "</b></td><td></td>";
    }
    echo "</tr>";

    if (($AenderungsRecht & 8192) == 8192) {
        echo "  <tr><td width=150 valign=top>Bemerkung Empfang:</td><td><textarea cols=80 line=3 name=txtKON_BEMERKUNGEMPFANG>" . (isset($rsKontakte["KON_BEMERKUNGEMPFANG"][0])?$rsKontakte["KON_BEMERKUNGEMPFANG"][0]:'') . "</textarea></td></tr>";
    }

    // Sacha Kerres, 01.07.2008
    if ((intval($AenderungsRecht) & 16384) == 16384)    // Personalnummer nur f�r den Empfang
    {
        echo "  <tr><td width=150 valign=top>Personalnummer:</td><td><input type=text name=txtKON_PER_NR value=\"" . (isset($rsKontakte["KON_PER_NR"][0])?$rsKontakte["KON_PER_NR"][0]:'') . "\">";
        $BindeVariablen = array();
        $BindeVariablen['var_N0_per_nr'] = (isset($rsKontakte["KON_PER_NR"][0])?$rsKontakte["KON_PER_NR"][0]:'');
        $SQL = 'SELECT PER_NACHNAME, PER_VORNAME,PER_AUSTRITT';
        $SQL .= ' FROM V_PERSONALSTAMM';
        $SQL .= ' WHERE PER_NR = :var_N0_per_nr';
        $rsPER = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
        if ($awisRSZeilen > 0 and $rsPER['PER_AUSTRITT'][0] == '') {
            echo $rsPER['PER_NACHNAME'][0] . ', ' . $rsPER['PER_VORNAME'][0];
        } elseif($awisRSZeilen > 0 and $rsPER['PER_AUSTRITT'][0] != '') {
            echo '<img src="/bilder/icon_warnung.png" title="Hinterlegte Personalnummer hat ein Austrittsdatum!">';
        } elseif(isset($rsKontakte["KON_PER_NR"][0]) and $rsKontakte["KON_PER_NR"][0] != ''){
            echo '<img src="/bilder/icon_warnung.png" title="Hinterlegte Personalnummer existiert nicht im Personalstamm!">';
        } else {
            echo '<img src="/bilder/icon_warnung.png" title="Bitte Personalnummer hinterlegen!">';
        }
        echo "</td></tr>";

        // Auswahl eines Benutzers f�r diesen Kontakt
        // SK 06.12.2011
        echo "  <tr><td width=150 valign=top>Systembenutzer:</td>";

        $BindeVariablen = array();
        $SQL = 'SELECT XBN_KEY, COALESCE(XBN_VOLLERNAME,XBN_NAME) AS XBN_VOLLERNAME, XBN_KON_KEY';
        $SQL .= ' FROM BENUTZER';
        $SQL .= ' LEFT OUTER JOIN KONTAKTE ON XBN_KON_KEY = KON_KEY';
        if (isset($rsKontakte["KON_KEY"][0])) {
            $SQL .= ' WHERE ((XBN_KON_KEY IS NULL AND XBN_STATUS = \'A\' AND XBN_SYSTEMUSER = 0) OR XBN_KON_KEY = :var_N0_KON_KEY)';
            $BindeVariablen['var_N0_KON_KEY'] = intval('0' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:0));
        } else {
            $SQL .= ' WHERE ((XBN_KON_KEY IS NULL AND XBN_STATUS = \'A\' AND XBN_SYSTEMUSER = 0))';
        }
        $SQL .= ' AND XBN_VOLLERNAME NOT LIKE \'Filiale%\'';
        $SQL .= ' ORDER BY 2';
        $rsXBN = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
        $rsBereicheZeilen = $awisRSZeilen;
        echo "<td><select name=txtKON_XBE_KEY size=1 tabindex=10>";
        $DefEintrag = true;
        for ($i = 0; $i < $rsBereicheZeilen; $i++) {
            echo "<option ";
            if ($rsXBN["XBN_KON_KEY"][$i] === (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:0)) {
                echo " selected ";
                $DefEintrag = false;
            }
            echo " value=" . $rsXBN["XBN_KEY"][$i] . ">" . $rsXBN["XBN_VOLLERNAME"][$i] . "</option>";
        }
        if ($DefEintrag) {
            echo '<option selected value=0>::Bitte zuordnen::</option>';
        }
        echo "</select>";

        $SQL = 'SELECT XBN_KEY, XBN_VOLLERNAME FROM Benutzer WHERE XBN_KON_KEY = ' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:0);
        $rsKON = awisOpenRecordset($con, $SQL, true, false);
        if ($awisRSZeilen > 1) {
            $UserListe = '';
            for ($i = 0; $i < $awisRSZeilen; $i++) {
                $UserListe .= ', ' . $rsKON['XBN_VOLLERNAME'][$i];
            }
            echo '<img src="/bilder/icon_warnung.png" title="Falsche Benutzerzuordnung: Bitte UHD die angezeigten Benutzer melden: ' . substr($UserListe,
                    1) . '">';
        }
        echo "</td></tr>";

        // Status des Kontakts
        echo "  <tr><td width=150 valign=top>Status:</td><td><select type=text name=txtKON_STATUS>";
        echo '<option ' . ((isset($rsKontakte["KON_STATUS"][0])?$rsKontakte["KON_STATUS"][0]:'') == 'V'?'selected':'') . ' value="V">Versteckt</option>';
        echo '<option ' . ((isset($rsKontakte["KON_STATUS"][0])?$rsKontakte["KON_STATUS"][0]:'') == 'I'?'selected':'') . ' value="I">Inaktiv</option>';
        echo '<option ' . ((isset($rsKontakte["KON_STATUS"][0])?$rsKontakte["KON_STATUS"][0]:'A') == 'A'?'selected':'') . ' value="A">Aktiv</option>';
        echo "</select></td></tr>";
    }

    // Zeile9
    // Listenzugeh�rigkeit
    if (($AenderungsRecht & 32) == 32) {
        echo '<tr><td width=150>Listenzugeh�rigkeit:</td>';

        echo '<td>';
        echo '<input type=checkbox name=chkListe_Telefon value=1 ';
        if (isset($rsKontakte['KON_LISTEN'][0]) AND ($rsKontakte['KON_LISTEN'][0] & 1) == 1) {
            echo ' checked';
        }
        echo '>&nbsp;Telefonliste</td></tr>';

        echo '<td>&nbsp;</td><td>';
        echo '<input type=checkbox name=chkListe_Filiale value=2 ';
        if (isset($rsKontakte['KON_LISTEN'][0]) AND ($rsKontakte['KON_LISTEN'][0] & 2) == 2) {
            echo ' checked';
        }
        echo '>&nbsp;Filialliste</td></tr>';

        echo '</tr>';
    }

    echo "</table>";

    /**************************************************
     * Abteilungen, in denen ein Mitarbeiter ist
     **************************************************/
    echo "<table>";

    if (($AenderungsRecht & 2)==2)        // Recht Stufe2
    {
        echo "  <tr><td colspan=1 class=FeldBez>Abteilungen</td><td class=FeldBez>Bemerkung</td><td class=FeldBez>Listenhinweis</td></tr><tr><td>";

        $BindeVariablen = array();
        $BindeVariablen['var_N0_stufe'] = $TelefonverzeichnisStufe;
        $rsABT = awisOpenRecordset($con,
            "SELECT * FROM KontakteAbteilungen WHERE BITAND(KAB_KZG_ID,:var_N0_stufe)>0 ORDER BY KAB_ABTEILUNG", true,
            false, $BindeVariablen);
        $rsABTZeilen = $awisRSZeilen;
        if ($EigeneAendern OR ($AenderungsRecht & 2) == 2) {
            echo "  <tr><td>";
            echo "<select name=txtKON_KAB_KEY size=1>";
            echo "<option value=-1>::Abteilung hinzuf�gen::</option>";
            for ($j = 0; $j < $rsABTZeilen; $j++) {
                echo "<option value=" . $rsABT["KAB_KEY"][$j] . ">" . $rsABT["KAB_ABTEILUNG"][$j] . "</option>";
            }
            echo "</select>";
            echo "</td>";
            echo "<td><input name=txtKZA_BEMERKUNG value='' size=50></td>";
            echo "<td><input name=txtKZA_LISTENHINWEIS value='' size=45></td>";
            echo "</tr>";
        }
        unset($rsABT);

        // Alle Abteilungen anzeigen
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval('0' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:''));
        $rsKZA = awisOpenRecordset($con,
            "SELECT * FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY=:var_N0_kon_key",
            true, false, $BindeVariablen);

        $rsKZAZeilen = $awisRSZeilen;

        for ($j = 0; $j < $rsKZAZeilen; $j++) {
            echo '<tr><td width=200>';
            echo '<a href=./telefon_Main.php?cmdAktion=Liste&KAB_KEY=0' . $rsKZA["KAB_KEY"][$j] . '>' . $rsKZA["KAB_ABTEILUNG"][$j] . '</a></td>';
            echo '<td width=300>' . $rsKZA['KZA_BEMERKUNG'][$j] . '</td>';
            echo '<td width=300>' . $rsKZA['KZA_LISTENHINWEIS'][$j] . '</td>';

            if ($EigeneAendern OR ($AenderungsRecht & 2) == 2) {
                echo "<td><input type=image title='Abteilungszugeh�rigkeit zu " . $rsKZA['KAB_ABTEILUNG'][$j] . " l�schen' src=/bilder/muelleimer.png name=cmdDelKZA_" . $rsKZA["KZA_KON_KEY"][$j] . '~' . $rsKZA["KZA_KAB_KEY"][$j] . " ></td>";
            }
        }
    } else {
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval('0' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:''));
        $rsKZA = awisOpenRecordset($con,
            'SELECT KAB_ABTEILUNG, KAB_KEY, KZA_BEMERKUNG, KZA_LISTENHINWEIS FROM KontakteAbteilungenZuordnungen INNER JOIN  KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY=:var_N0_kon_key',
            true, false, $BindeVariablen);
        $rsKZAZeilen = $awisRSZeilen;
        $Abteilungen = '';
        for ($j = 0; $j < $rsKZAZeilen; $j++) {
            $Abteilungen .= ', <a href=./telefon_Main.php?cmdAktion=Liste&KAB_KEY=0' . $rsKZA["KAB_KEY"][$j] . '>' . $rsKZA["KAB_ABTEILUNG"][$j] . '</a>' . $rsKZA["KZA_BEMERKUNG"][$j] . '';
        }
        unset($rsKZA);

        echo "  <tr><td width=150>Abteilung(en):</td><td><b>" . substr($Abteilungen, 1) . "</b></td><td></td>";
    }
    echo "</tr>";
    echo "</table>";

    echo "<table>";

//Abteilungszuordnungen f�r OE - Mitarbeiter

    if ($AenderungsRecht & 32768)       // Recht Stufe 15
    {
        //schreibt die Teil�berschrift
        echo "<tr><td colspan=1 class=FeldBez>IT-Zust�ndigkeit</td><td></td><td></td></tr><tr><td>";

        //holt die Liste mit den Abteilungen aus der Datenbank (KONTAKTEABTEILUNGEN KAB)
        $BindeVariablen = array();
        $BindeVariablen['var_N0_stufe'] = $TelefonverzeichnisStufe;
        $BindeVariablen['KON_KEY'] = isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:0;

        $SQL2 = 'SELECT * FROM KONTAKTEABTEILUNGEN ';
        $SQL2 .= ' WHERE KAB_KEY NOT IN ( ';
        $SQL2 .= 'SELECT KIN_WERT FROM KONTAKTEINFOS WHERE KIN_KON_KEY = :KON_KEY';
        $SQL2 .= ') AND BITAND ( KAB_KZG_ID, :var_N0_stufe ) > 0 ';
        $SQL2 .= 'ORDER BY KAB_ABTEILUNG ';

        $rsABT = awisOpenRecordset($con,
            $SQL2, true,
            false, $BindeVariablen);
        $rsABTZeilen = $awisRSZeilen;


        if (($AenderungsRecht & 32768) == 32768) {
            echo "  <tr><td>";
            echo "<select name=txtKAB_ITKONTAKT size=1>";
            echo "<option value=-1>::Abteilung hinzuf�gen::</option>";
            for ($j = 0; $j < $rsABTZeilen; $j++) {
                echo "<option value=" . $rsABT["KAB_KEY"][$j] . ">" . $rsABT["KAB_ABTEILUNG"][$j] . "</option>";
            }
            echo "</select>";
        }
        unset($rsABT);

        // Alle Abteilungen anzeigen
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval('0' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:''));
        $rsKZA = awisOpenRecordset($con,
            "SELECT * FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY=:var_N0_kon_key",
            true, false, $BindeVariablen);
        $rsKZA2 = awisOpenRecordset($con,
            "SELECT * FROM Kontakteinfos KIN INNER JOIN KontakteAbteilungen KAB ON KIN.KIN_WERT = KAB.KAB_KEY WHERE KIN_KON_KEY=:var_N0_kon_key",
            true, false, $BindeVariablen);

        $rsKZAZeilen = $awisRSZeilen;

        for ($j = 0; $j < $rsKZAZeilen; $j++) {
            echo '<tr><td width=200>';
            echo '<a href=./telefon_Main.php?cmdAktion=Liste&KAB_KEY=0' . $rsKZA2["KAB_KEY"][$j] . '>' . $rsKZA2["KAB_ABTEILUNG"][$j] . '</a></td>';

            if (($AenderungsRecht & 32768) == 32768) {
                echo "<td><input type=image title='Zust�ndigkeit zu " . $rsKZA2['KAB_ABTEILUNG'][$j] . " l�schen' src=/bilder/muelleimer.png name=cmdDelKZB_" . $rsKZA["KZA_KON_KEY"][0] . '~' . $rsKZA2["KAB_KEY"][$j] . " ></td>";
            }
        }
    } else {
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval('0' . (isset($rsKontakte["KON_KEY"][0])?$rsKontakte["KON_KEY"][0]:''));
        $rsKZA2 = awisOpenRecordset($con,
            "SELECT * FROM Kontakteinfos KIN INNER JOIN KontakteAbteilungen KAB ON KIN.KIN_WERT = KAB.KAB_KEY WHERE KIN_KON_KEY=:var_N0_kon_key",
            true, false, $BindeVariablen);
        $rsKZAZeilen = $awisRSZeilen;
        $Abteilungen = '';

        unset($rsKZA);

    }
    echo "</tr>";
    echo "</table>";
    //*****************************************
    //
    // Zeile9, Fahrzeuge
    //
    //*****************************************

    if (($Recht150 & 128) == 128) {
        echo "<table width=100%>";
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval($txtKONKey);
        $rsKFZ = awisOpenRecordset($con, "SELECT * FROM KONTAKTEFAHRZEUGE WHERE KFA_KON_KEY=:var_N0_kon_key", true,
            false, $BindeVariablen);
        echo " <tr><td colspan=99><hr></td></tr>";        // Trennzeile
        echo "  <tr><td colspan=2 class=FeldBez>Fahrzeuge</td></tr>";
        echo "<tr><td><table border=0>";
        echo " <tr><td class=FeldBez>KFZ-Kennzeichen</td><td align=left class=FeldBez>Fahrzeugtyp</td></tr>";
        for ($j = 0; $j < sizeof($rsKFZ["KFA_KEY"]); $j++) {
            if (($AenderungsRecht & 1) == 1) {
                echo "  <tr><td><input size=20 type=text name=txtKFA_KENNZEICHEN_" . $rsKFZ["KFA_KEY"][$j] . " value='" . $rsKFZ["KFA_KENNZEICHEN"][$j] . "'></td>";
                echo "<td><input size=40 type=text name=txtKFA_FAHRZEUGTYP_" . $rsKFZ["KFA_KEY"][$j] . " value='" . $rsKFZ["KFA_FAHRZEUGTYP"][$j] . "'></td>";
                echo "<td><input type=image title='KFZ l�schen' src=/bilder/muelleimer.png name=cmdDelKFA_" . $rsKFZ["KFA_KEY"][$j] . " ></td>";
                echo "</tr>";
            } else {
                echo "<tr><td><b>" . $rsKFZ["KFA_KENNZEICHEN"][$j] . "</td><td>" . $rsKFZ["KFA_FAHRZEUGTYP"][$j] . "</b></td></tr>";
            }
        }
        // Neues KFZ hinzuf�gen
        if ($EigeneAendern OR ($AenderungsRecht & 1) == 1) {
            echo "  <tr><td><input size=20 type=text name=txtKFA_KENNZEICHEN_0 value=''></td>";
            echo "<td><input size=40 type=text name=txtKFA_FAHRZEUGTYP_0 value=''></td>";
            echo "<td></td>";
        }

        echo "</table>   </td><td></td>";

        unset($rsKFZ);
        echo "</tr></table>";
    }

    //*****************************************
    //
    // Block mit Kommunikation
    //
    //*****************************************

    // Auswahlliste
    $BindeVariablen = array();
    $BindeVariablen['var_N0_stufe'] = $TelefonverzeichnisStufe;
    $rsKOT = awisOpenRecordset($con,
        "SELECT * FROM KommunikationsTypen WHERE BITAND(KOT_KZG_ID,:var_N0_stufe)>0 ORDER BY KOT_SORTIERUNG, KOT_TYP",
        true, false, $BindeVariablen);
    $rsKOTZeilen = $awisRSZeilen;

    echo "<table width=100%>";
    echo " <tr><td colspan=99><hr></td></tr>";        // Trennzeile
    echo "  <tr><td>Erreichbarkeit:<b><table border=1 width=100%><tr>";

    $EigeneAendern = false; //Erreichbarkeit darf von User nicht selbst ge�ndert werden!!! TR 19.11.10
    if (($EigeneAendern OR ($AenderungsRecht & 16) == 16)) {
        echo "  <td id=FeldBez width=25></td>";
    }
    echo "  <td id=FeldBez width=200>Art</td><td id=FeldBez width=100>Wert</td><td id=FeldBez width=200>Kurzwahl</td><td id=FeldBez >Bemerkung</td>";
    if (($EigeneAendern OR ($AenderungsRecht & 16) == 16)) {
        echo "<td id=FeldBez></td>";
    }
    echo "</tr>";

    for ($i = 0; $i < $rsKommunikationZeilen; $i++) {
        if (($EigeneAendern OR ($AenderungsRecht & 24) > 0) OR ($rsKommunikation["KOT_KEY"][$i] != 9 and $rsKommunikation["KOT_KEY"][$i] != 10)) {
            if (($EigeneAendern OR ($AenderungsRecht & 16) == 16) AND ($rsKommunikation["KOT_KZG_ID"][$i] & $TelefonverzeichnisStufe)) {
                if ($rsKommunikation["KOT_KZG_ID"][$i] == 1) {
                    echo "<tr><td width=25>";
                    echo "<img title='�ffentliche Daten' src=/bilder/Schloss_offen.png>";
                } // Private Daten
                elseif ($rsKommunikation["KOT_KZG_ID"][$i] == 1024) {
                    echo "<tr><td width=25>";
                    echo "<img title='Private Daten'  src=/bilder/Schloss_zu.png>";
                } // �ffentliche, durch Empfang gepflegt
                elseif ($rsKommunikation["KOT_KZG_ID"][$i] == 4096 AND ($TelefonverzeichnisStufe & 8192) == 0) {
                    continue;
                } elseif ($rsKommunikation["KOT_KZG_ID"][$i] == 4096 AND ($TelefonverzeichnisStufe & 8192) == 8192) {
                    echo "<tr><td width=25>";
                    echo "<img title='Gesch�tzte, �ffentliche Daten' src=/bilder/Schloss_offen_gold.png>";
                } // Nur vom Empfang einzusehen
                else {
                    echo "<tr><td width=25>";
                    echo "<img title='Gesch�tzte Daten' src=/bilder/Schloss_zu_gold.png>";
                }

                echo "</td><td><select name=txtKKO_KOT_KEY_" . $rsKommunikation["KKO_KEY"][$i] . " size=1>";
                for ($j = 0; $j < $rsKOTZeilen; $j++) {
                    if (((isset($rsKommunikation["KOT_KZG_ID"][$j])?$rsKommunikation["KOT_KZG_ID"][$j]:0) & $TelefonverzeichnisStufe) == (isset($rsKommunikation["KOT_KZG_ID"][$j])?$rsKommunikation["KOT_KZG_ID"][$j]:0)) {
                        echo "<option ";
                        if ($rsKOT["KOT_KEY"][$j] == $rsKommunikation["KKO_KOT_KEY"][$i]) {
                            echo " selected ";
                        }
                        echo "value=" . $rsKOT["KOT_KEY"][$j] . ">" . $rsKOT["KOT_TYP"][$j] . "</option>";
                    }
                }

                echo "</select>";

                echo "</td>";

                echo "<td><input name=txtKKO_WERT_" . $rsKommunikation["KKO_KEY"][$i] . " size=40 value='" . $rsKommunikation["KKO_WERT"][$i] . "'></td>";
                echo "<td><input name=txtKKO_KURZWAHL_" . $rsKommunikation["KKO_KEY"][$i] . " size=8 value='" . $rsKommunikation["KKO_KURZWAHL"][$i] . "'></td>";
                echo "<td><input name=txtKKO_BEMERKUNG_" . $rsKommunikation["KKO_KEY"][$i] . " size=50 value='" . $rsKommunikation["KKO_BEMERKUNG"][$i] . "'></td>";

                echo "<td><input type=image alt=Kontakt_l�schen src=/bilder/muelleimer.png name=cmdDelKKO_" . $rsKommunikation["KKO_KEY"][$i] . "></td>";

                echo "</tr>";
            } elseif (($rsKommunikation["KOT_KZG_ID"][$i] & $TelefonverzeichnisStufe)) {
                if (($EigeneAendern AND $rsKommunikation["KOT_KZG_ID"][$i] == 1024) OR ($rsKommunikation["KOT_KZG_ID"][$i] == 1)) {
                    echo "<tr><td><b>" . $rsKommunikation["KOT_TYP"][$i] . "</b></td>";
                    echo "<td><b>" . $rsKommunikation["KKO_WERT"][$i] . "</b></td>";
                    echo "<td><b>" . $rsKommunikation["KKO_KURZWAHL"][$i] . "</b></td><td><b>";
                    if (($AenderungsRecht & 8) == 8) {
                        echo "<input name=txtNUR_KKO_BEMERKUNG_" . $rsKommunikation["KKO_KEY"][$i] . " size=50 value='" . $rsKommunikation["KKO_BEMERKUNG"][$i] . "'>";
                    } else {
                        echo $rsKommunikation["KKO_BEMERKUNG"][$i];
                    }
                    echo "</b></td></tr>";
                }
            }
        }
    }

    if ($EigeneAendern OR ($AenderungsRecht & 64) == 64) {
        echo "<tr><td></td><td>";

        echo "<select name=txtKKO_KOT_KEY_0 size=1>";
        echo "<option value=0 selected>Neuer Kontakt...</option>";
        for ($j = 0; $j < $rsKOTZeilen; $j++) {
            if (((isset($rsKommunikation["KOT_KZG_ID"][$j])?$rsKommunikation["KOT_KZG_ID"][$j]:0) & $TelefonverzeichnisStufe) == (isset($rsKommunikation["KOT_KZG_ID"][$j])?$rsKommunikation["KOT_KZG_ID"][$j]:0)) {
                echo "<option ";
                if ($rsKOT["KOT_KEY"][$j] == (isset($rsKommunikation["KKO_KOT_KEY"][$i])?$rsKommunikation["KKO_KOT_KEY"][$i]:'')) {
                    echo " selected ";
                }
                echo "value=" . $rsKOT["KOT_KEY"][$j] . ">" . $rsKOT["KOT_TYP"][$j] . "</option>";
            }
        }

        echo "</select></td>";

        $KKOWert = isset($rsKommunikation["KKO_WERT"][$i])?$rsKommunikation["KKO_WERT"][$i]:'';
        if (preg_match('/.*@.*/', $KKOWert)) {
            $KKOWert = '<a href=mailto:' . $KKOWert . '>';
        }

        echo "<td><input name=txtKKO_WERT_0 size=40 value='" . $KKOWert . "'></td>";
        echo "<td><input name=txtKKO_KURZWAHL_0 size=8 value='" . (isset($rsKommunikation["KKO_KURZWAHL"][$i])?$rsKommunikation["KKO_KURZWAHL"][$i]:'') . "'></td>";
        echo "<td><input name=txtKKO_BEMERKUNG_0 size=50 value='" . (isset($rsKommunikation["KKO_BEMERKUNG"][$i])?$rsKommunikation["KKO_BEMERKUNG"][$i]:'') . "'></td>";
        echo "<td></td>";
        echo "</tr>";
    }

    echo "</table>   </td><td></td>";
    unset($rsKommunikation);
    echo "</tr></table>";

    //*****************************************************
    // Aktuelle Einsaetze
    //*****************************************************

    $Recht154 = awisBenutzerRecht($con, 154);
    if ($Recht154 > 0) {
        $BindeVariablen = array();
        $BindeVariablen['var_N0_kon_key'] = intval($txtKONKey);

        $SQL = 'SELECT DISTINCT Personaleinsaetze.*, xbn_name || coalesce(\', \'||xbn_vorname,\'\') AS Mitarbeiter, XBN_KON_KEY';
        $SQL .= ', XTZVON.XTZ_TAGESZEIT AS ZEITVON,  XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, PEA_BEZEICHNUNG, FIL_BEZ';
        $SQL .= ' FROM Personaleinsaetze';
        $SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
        $SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
        $SQL .= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
        $SQL .= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
        $SQL .= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
        $SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
        $SQL .= ' LEFT OUTER JOIN Filialebenenrollenzuordnungen ON FRZ_KON_KEY = KON_KEY AND FRZ_GUELTIGAB <= sysdate AND FRZ_GUELTIGBIS >= sysdate ';
        $SQL .= ' WHERE kon_key = :var_N0_kon_key';
        $SQL .= ' AND PEI_DATUM >= (TRUNC(SYSDATE)-1)';
        if (($Recht154 & 2) == 0) {
            $SQL .= ' AND PEI_DATUM <= (SYSDATE+2)';
        }
        if (($Recht154 & 8) == 0) {
            $SQL .= ' AND (FRZ_FER_KEY IS NULL OR FRZ_FER_KEY <> 23)';            // 23=RL
        }
        $SQL .= ' ORDER BY PEI_DATUM, PEI_VONXTZ_ID';

        $rsPEI = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
        $rsPEIZeilen = $awisRSZeilen;

        echo '<table border=1 width=100%>';
        echo "<tr><td colspan=5>Aktueller Einsatzplan:</td><tr>";
        echo '<tr><td id=FeldBez>Datum</td>';
        echo '<td id=FeldBez>von</td>';
        echo '<td id=FeldBez>bis</td>';
        echo '<td id=FeldBez>Einsatz</td>';
        echo '<td id=FeldBez>Filiale</td>';
        echo '</tr>';

        if ($rsPEIZeilen > 0) {

            for ($PEIZeile = 0; $PEIZeile < $rsPEIZeilen; $PEIZeile++) {
                echo '<tr>';
                echo '<td>';
                if ($Recht154 & 4) {
                    echo '<a href=/personaleinsaetze/personaleinsaetze_Main.php?cmdAktion=Details&PEI_KEY=0' . $rsPEI['PEI_KEY'][$PEIZeile];
                    echo '>';
                    echo $rsPEI['PEI_DATUM'][$PEIZeile];
                    echo '</a>';
                } else {
                    echo $rsPEI['PEI_DATUM'][$PEIZeile];
                }
                echo '</td>';

                echo '<td>';
                echo $rsPEI['ZEITVON'][$PEIZeile];
                echo '</td>';

                echo '<td>';
                echo $rsPEI['ZEITBIS'][$PEIZeile];
                echo '</td>';

                echo '<td>';
                echo $rsPEI['PEA_BEZEICHNUNG'][$PEIZeile];
                echo '</td>';

                echo '<td>';
                echo $rsPEI['FIL_BEZ'][$PEIZeile];
                echo '</td>';

                echo '</tr>';
            }

            echo '</table>';
        }    // Sind Daten da?
        else {
            echo '<tr><td colspan=5><span class=Hinweistext>Keine Daten vorhanden.</span></td></tr>';
        }
    }
}    // Daten gefunden?

unset($rsKontakte);

echo "</form>";

/*************************************************************
 *
 * Cursor in das erste Feld setzen
 *
 *************************************************************/

if ($AenderungsRecht > 0 OR $EigeneAendern == true) {
    echo '<Script Language=JavaScript>';
    if ($AenderungsRecht & 8) {
        echo "document.getElementsByName('txtKON_BEMERKUNG')[0].focus();";
    } else {
        echo "document.getElementsByName('txtKON_NAME1')[0].focus();";
    }
    echo '</Script>';
}

//************************************************************************************************
//	Hilfsfunktionen
//************************************************************************************************

function Kommunikationseintrag($rsKommunikation, $Typ, $Typ1 = 0)
{

    $erg = "";

    for ($i = 0; $i < sizeof($rsKommunikation["KKO_KOT_KEY"]); $i++) {
        if ($rsKommunikation["KKO_KOT_KEY"][$i] == $Typ OR $rsKommunikation["KKO_KOT_KEY"][$i] == $Typ1) {
            $erg .= ", " . $rsKommunikation["KKO_WERT"][$i];
        }
    }

    return substr($erg, 2);
}

?>