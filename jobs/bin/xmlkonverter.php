<?php
require_once('/daten/include/awisKonvertierung.inc');
require_once('/daten/include/awisMailer.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;

    $Quelle = '';
    $Ziel = '';

    for($i=1;$i<$argc;$i++)
    {

            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--quelle':
                            $Quelle = $argv[++$i];
                            break;
                    case '--ziel':
                            $Ziel = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;

                            echo PHP_EOL;
                            die();
            }
    }

    if($JobId!=0)
    {
        $Job = new awisJobProtokoll($JobId, 'Starte XML Konvertierung...');
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    
    $ZUK = new awisKonvertierung($AWISBenutzer,$DB);
    $ZUK->DebugLevel($DebugLevel);

    if($JobId!=0)
    {
        $Job->SchreibeLog(3,1,'Suche XML-Dateien...');
    }

        // Artikelstamm und Bestand
    $ZUK->KonvertiereTradeByte();

    if($JobId!=0)
    {
        $Job->SchreibeLog(1,0,'Ende des Jobs.');
    }
}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
?>

