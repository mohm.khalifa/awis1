<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Speicherverbrauch</title>
<link rel=stylesheet type=text/css href=/ATU.css>
</head>

<body>
<? 

require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("ATU_Header.php");	// Kopfzeile

if(!(awisHatDasRecht(10)))
{
	awisEreignis(3,1000,'Benutzerverwaltung',"$PHP_AUTH_USER",'','','');
	die("Keine ausreichenden Rechte!");
}

$con = awisLogon();

if($con)
{

	$rsObjekte = awisOpenRecordset($con, "SELECT SEGMENT_NAME, SEGMENT_TYPE, TABLESPACE_NAME, BYTES, EXTENTS FROM USER_SEGMENTS");
	$rsObjekteZeilen = $awisRSZeilen;


	//***********************************************
	//Ausgabe
	//***********************************************


	print "<p>Ich habe ".$rsObjekteZeilen." Zeilen gefunden!";
	print "</p>";


	print "<table border=1 width=100%><tr><th id=FeldBez>Name</th><th id=FeldBez>Typ</th><th id=FeldBez>Tablespace</th><th id=FeldBez>Bytes</th><th id=FeldBez>Extents</th></tr>";
	
	for($DS=0;$DS<$rsObjekteZeilen;$DS++)
	{	
		print "<tr>";
		print "<td>" . $rsObjekte["SEGMENT_NAME"][$DS] . "</td>";
		print "<td>" . $rsObjekte["SEGMENT_TYPE"][$DS] . "</td>";
		print "<td>" . $rsObjekte["TABLESPACE_NAME"][$DS] . "</td>";
		print "<td>" . $rsObjekte["BYTES"][$DS] . "</td>";
		print "<td>" . $rsObjekte["EXTENTS"][$DS] . "</td>";
		print "</tr>";


	}
	print"</table>";

	awisLogoff($con);
//	OCILogoff($consys);
}
else
{
	print "System-Verbindung hat nicht geklappt!!!";
}
?>
</BODY>
</HTML>
