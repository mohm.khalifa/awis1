<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $DB;

try
{
    require_once('awisDatenbank.inc');
    require_once('awisFormular.inc');

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GKU','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[] = array('Wort', 'lbl_speichern');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[] = array('Fehler', 'err_keineRechte');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht28000=$AWISBenutzer->HatDasRecht(28000);

	if($Recht28000==0)
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./gutschein_Main.php?cmdAktion=Kunde enctype=\"multipart/form-data\">");
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GKU'));

	if (!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'GKU_KEY';
	}
	$Seite = 'Suche';

	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{        // wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
	}

	//Ein einzelner Datensatz
	if (isset($_GET['GKU_KEY']))
	{
		$AWIS_KEY1 = $_GET['GKU_KEY'];
		$Seite = 'Details';
	}
	elseif (isset($_POST['txtGKU_KEY']))
	{
		$AWIS_KEY1 = $_POST['txtGKU_KEY'];
		$Seite = 'Details';
	}

	if (isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = -1;
		$Seite = 'Details';
	}

	if (isset($_GET['Sort']))
	{
		$AWIS_KEY1 = -1;
		$Seite = 'Details';
	}

	if (isset($_POST['cmdSpeichern_x']))
	{
		include('./gutschein_Kunde_speichern.php');
	}

	if ($Recht28000 == 0)
	{
		$Form->Fehler_KeineRechte();
	}

	if (isset($_POST['cmdSuche_x']))
	{ //�ber die Suche gekommen
		$Param['GKU_KEY'] = $Form->Format('N0', $_POST['sucGKU_KEY'], true);
		$Param['GKU_VERTRAGNR'] = $Form->Format('T', $_POST['sucGKU_VERTRAGNR'],true);
		$Param['GKU_NAME1'] = $Form->Format('T', $_POST['sucGKU_NAME1'], true);
		$Param['GKU_KONZERN'] = $Form->Format('N0', $_POST['sucGKU_KONZERN'], true);
		$Param['GKU_ORT'] = $Form->Format('T', $_POST['sucGKU_ORT'], true);
		$Param['GKU_STATUS'] = $Form->Format('T', $_POST['sucGKU_STATUS'], true);
		$Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$Seite = 'Details';
	}

	$Block=1;
	if (isset($_REQUEST['Block']))
	{ //Gebl�ttert oder "Zur�ck zur Liste"
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
		$Seite = 'Details';
	}

	$Form->Formular_Start();
	
	if ($Seite == 'Details')
	{ //Detailseite anzeigen
		$Form->Erstelle_HiddenFeld('GKU_KEY', $AWIS_KEY1);

		//Grund Select
		$SQL  ='SELECT kun.GKU_KEY,';
		$SQL .='   kun.GKU_ANREDE,';
		$SQL .='   kun.GKU_TITEL,';
		$SQL .='   kun.GKU_NAME1,';
		$SQL .='   kun.GKU_NAME2,';
		$SQL .='   kun.GKU_KONZERN,';
		$SQL .='   kun.GKU_STRASSE,';
		$SQL .='   kun.GKU_HN,';
		$SQL .='   kun.GKU_POSTFACH,';
		$SQL .='   kun.GKU_POSTFACHPLZ,';
		$SQL .='   kun.GKU_LAN_CODE,';
		$SQL .='   kun.GKU_PLZ,';
		$SQL .='   kun.GKU_ORT,';
		$SQL .='   kun.GKU_TELEFON,';
		$SQL .='   kun.GKU_MOBILTELEFON,';
		$SQL .='   kun.GKU_FAX,';
		$SQL .='   kun.GKU_EMAIL,';
		$SQL .='   kun.GKU_WEBSITE,';
		$SQL .='   kun.GKU_STEUERNUMMER,';
		$SQL .='   kun.GKU_USTID,';
		$SQL .='   kun.GKU_HANDELSREGISTER,';
		$SQL .='   kun.GKU_BEMERKUNG,';
		$SQL .='   kun.GKU_STATUS,';
		$SQL .='   kun.GKU_VERTRAGNR,';
		$SQL .='   row_number() over (ORDER BY kun.GKU_KEY ASC) AS ZeilenNr';
		$SQL .=' FROM GUTSCHEINKUNDEEXTERN kun';
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest

		if ($Bedingung != '') {
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}

		$MaxDS = 1;
		$ZeilenProSeite=1;
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('GKU', false));

		if ($Param['GKU_KEY'] == '')
		{
			$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
		}

		$rsGKU = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('GKU', true));

		if ($AWIS_KEY1 == '' and $rsGKU->AnzahlDatensaetze() == 1)
		{
			$AWIS_KEY1 = $rsGKU->FeldInhalt('GKU_KEY');
		}
		
		if (($rsGKU->AnzahlDatensaetze() > 1) or ($rsGKU->AnzahlDatensaetze() == 1 and (isset($_REQUEST['Block']) or isset($_REQUEST['Sort']))))
		{ //Liste anzeigen
			$Seite = 'Liste';
			$Felder = array();
			$Felder[] = array(
				'Style' => 'font-size:smaller;',
				'Inhalt' => '<a href="./gutschein_Main.php?cmdAktion=Kunde" accesskey="S" title=' . '><img border=0 src=/bilder/cmd_trefferliste.png></a>'
			);
			$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
			$Form->InfoZeile($Felder, '');

			$FeldBreiten['ICONS'] = 40;
			$FeldBreiten['GKU_KUNDENNR'] = 150;
			$FeldBreiten['GKU_VERTRAGNR'] = 150;
			$FeldBreiten['GKU_NAME1'] = 150;
			$FeldBreiten['GKU_KONZERN'] = 200;
			$FeldBreiten['GKU_ORT'] = 150;
			$FeldBreiten['GKU_STATUS'] = 80;

			$Form->ZeileStart();
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_KUNDENNR'], $FeldBreiten['GKU_KUNDENNR'], '', $Link);
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_VERTRAGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_VERTRAGNR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_VERTRAGNR'], $FeldBreiten['GKU_VERTRAGNR'], '', $Link);
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_NAME1'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_NAME1'], $FeldBreiten['GKU_NAME1'], '', $Link);
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_KONZERN' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_KONZERN'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_KONZERN'], $FeldBreiten['GKU_KONZERN'], '', $Link);
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_ORT'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_ORT'], $FeldBreiten['GKU_ORT'], '', $Link);
			$Link = './gutschein_Main.php?cmdAktion=Kunde&Sort=GKU_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GKU_STATUS'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GKU']['GKU_STATUS'], $FeldBreiten['GKU_STATUS'], '', $Link);
			$Form->ZeileEnde();

			$DS = 0;
			while (!$rsGKU->EOF())
			{
				$HG = ($DS % 2);
				$Edit = './gutschein_Main.php?cmdAktion=Kunde&DEL&GKU_KEY=' . $rsGKU->FeldInhalt('GKU_KEY');

				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('GKU_KUNDENNR', $rsGKU->FeldInhalt('GKU_KEY'), $FeldBreiten['GKU_KUNDENNR'] / 10, $FeldBreiten['GKU_KUNDENNR'], false, $HG, '', $Edit);
				$Form->Erstelle_ListenFeld('GKU_VERTRAGNR',$rsGKU->FeldInhalt('GKU_VERTRAGNR'),$FeldBreiten['GKU_VERTRAGNR'] / 10, $FeldBreiten['GKU_VERTRAGNR'],false,$HG,'',$Edit);
				$Form->Erstelle_ListenFeld('GKU_NAME1', $rsGKU->FeldInhalt('GKU_NAME1'), $FeldBreiten['GKU_NAME1'] / 10, $FeldBreiten['GKU_NAME1'], false, $HG,'','');
				$Form->Erstelle_ListenFeld('GKU_KONZERN', $rsGKU->FeldInhalt('GKU_KONZERN'), $FeldBreiten['GKU_KONZERN'] / 10, $FeldBreiten['GKU_KONZERN'], false, $HG,'','');
				$Form->Erstelle_ListenFeld('GKU_ORT', $rsGKU->FeldInhalt('GKU_ORT'), $FeldBreiten['GKU_ORT'] / 10, $FeldBreiten['GKU_ORT'], false, $HG,'','');
				$Form->Erstelle_ListenFeld('GKU_STATUS', $rsGKU->FeldInhalt('GKU_STATUS'), $FeldBreiten['GKU_STATUS'] / 10, $FeldBreiten['GKU_STATUS'], false, $HG,'','');
				$Form->ZeileEnde();
				$rsGKU->DSWeiter();
			}
			$Link = './gutschein_Main.php?cmdAktion=Kunde';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		}
		elseif ($AWIS_KEY1 != '')
		{ //Neuer oder bestehender Kunde
			$Seite = 'Details';
			$Felder = array();

			$Felder[] = array(
				'Style' => 'font-size:smaller;',
				'Inhalt' => '<a href="' . './gutschein_Main.php?cmdAktion=Kunde&Block=1' . '" accesskey="S" title=' . '><img border=0 src=/bilder/cmd_trefferliste.png></a>'
			);
			$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
			$Form->InfoZeile($Felder, '');
			$aendern = true;

			$FeldBreiten['Labels'] = 200;
			$FeldBreiten['Werte'] = 300;
			$FeldBreiten['GKU_PLZ'] = 60;
			$FeldBreiten['GKU_ORT_LABEL'] = 30;
			$FeldBreiten['GKU_STRASSE_LABEL'] = 150;
			$FeldBreiten['GKU_HN'] = 50;
			$FeldBreiten['GKU_ORT'] = $FeldBreiten['Werte'] - $FeldBreiten['GKU_ORT_LABEL'] - $FeldBreiten['GKU_PLZ'] - $FeldBreiten['GKU_ORT_LABEL'] + 3;
			$FeldBreiten['GKU_STRASSE'] = $FeldBreiten['Werte'] - $FeldBreiten['GKU_STRASSE_LABEL'] - $FeldBreiten['GKU_HN'] - $FeldBreiten['GKU_STRASSE_LABEL'] + 3;

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KUNDENNR'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_KUNDENNR', isset($_POST['txtGKU_KUNDENNR'])?$_POST['txtGKU_KUNDENNR']:$rsGKU->FeldInhalt('GKU_KEY'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], false, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_VERTRAGNR'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_VERTRAGNR', isset($_POST['txtGKU_VERTRAGNR'])?$_POST['txtGKU_VERTRAGNR']:$rsGKU->FeldInhalt('GKU_VERTRAGNR'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_ANREDE'] . ':', $FeldBreiten['Labels']);
			$Daten = explode('|', $AWISSprachKonserven['GKU']['lst_GKU_ANREDE']);
			$Form->Erstelle_SelectFeld('!GKU_ANREDE', isset($_POST['txtGKU_ANREDE'])?$_POST['txtGKU_ANREDE']:$rsGKU->FeldInhalt('GKU_ANREDE'), $FeldBreiten['Werte'] . ';' . (
					$FeldBreiten['Werte'] - 25), $aendern, '','', '', '', '', $Daten, '');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_TITEL'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_TITEL', isset($_POST['txtGKU_TITEL'])?$_POST['txtGKU_TITEL']:$rsGKU->FeldInhalt('GKU_TITEL'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_NAME1'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_NAME1', isset($_POST['txtGKU_NAME1'])?$_POST['txtGKU_NAME1']:$rsGKU->FeldInhalt('GKU_NAME1'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_NAME2'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_NAME2', isset($_POST['txtGKU_NAME2'])?$_POST['txtGKU_NAME2']:$rsGKU->FeldInhalt('GKU_NAME2'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KONZERN'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_KONZERN', isset($_POST['txtGKU_KONZERN'])?$_POST['txtGKU_KONZERN']:$rsGKU->FeldInhalt('GKU_KONZERN'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_STRASSE'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_STRASSE', isset($_POST['txtGKU_STRASSE'])?$_POST['txtGKU_STRASSE']:$rsGKU->FeldInhalt('GKU_STRASSE'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_HN'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_HN', isset($_POST['txtGKU_HN'])?$_POST['txtGKU_HN']:$rsGKU->FeldInhalt('GKU_HN'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_PLZ'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_PLZ', isset($_POST['txtGKU_PLZ'])?$_POST['txtGKU_PLZ']:$rsGKU->FeldInhalt('GKU_PLZ'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_ORT'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_ORT', isset($_POST['txtGKU_ORT'])?$_POST['txtGKU_ORT']:$rsGKU->FeldInhalt('GKU_ORT'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_LAN_CODE'] . ':', $FeldBreiten['Labels']);
			$SQL_lst = 'SELECT LAN_CODE, LAN_LAND AS Land';
			$SQL_lst .= ' FROM LAENDER';
			$Form->Erstelle_SelectFeld('GKU_LAN_CODE',(isset($_POST['txtGKU_LAN_CODE'])?$_POST['txtGKU_LAN_CODE']:$rsGKU->FeldInhalt('GKU_LAN_CODE')),
				$FeldBreiten['Werte'],true,$SQL_lst,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_POSTFACH'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_POSTFACH', isset($_POST['txtGKU_POSTFACH'])?$_POST['txtGKU_POSTFACH']:$rsGKU->FeldInhalt('GKU_POSTFACH'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_POSTFACHPLZ'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_POSTFACHPLZ', isset($_POST['txtGKU_POSTFACHPLZ'])?$_POST['txtGKU_POSTFACHPLZ']:$rsGKU->FeldInhalt('GKU_POSTFACHPLZ'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_TELEFON'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('!GKU_TELEFON', isset($_POST['txtGKU_TELEFON'])?$_POST['txtGKU_TELEFON']:$rsGKU->FeldInhalt('GKU_TELEFON'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_MOBILTELEFON'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_MOBILTELEFON', isset($_POST['txtGKU_MOBILTELEFON'])?$_POST['txtGKU_MOBILTELEFON']:$rsGKU->FeldInhalt('GKU_MOBILTELEFON'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_FAX'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_FAX', isset($_POST['txtGKU_FAX'])?$_POST['txtGKU_FAX']:$rsGKU->FeldInhalt('GKU_FAX'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_EMAIL'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_EMAIL', isset($_POST['txtGKU_EMAIL'])?$_POST['txtGKU_EMAIL']:$rsGKU->FeldInhalt('GKU_EMAIL'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_WEBSITE'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_WEBSITE', isset($_POST['txtGKU_WEBSITE'])?$_POST['txtGKU_WEBSITE']:$rsGKU->FeldInhalt('GKU_WEBSITE'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_STEUERNUMMER'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_STEUERNUMMER', isset($_POST['txtGKU_STEUERNUMMER'])?$_POST['txtGKU_STEUERNUMMER']:$rsGKU->FeldInhalt('GKU_STEUERNUMMER'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_USTID'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_USTID', isset($_POST['txtGKU_USTID'])?$_POST['txtGKU_USTID']:$rsGKU->FeldInhalt('GKU_USTID'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_HANDELSREGISTER'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_HANDELSREGISTER', isset($_POST['txtGKU_HANDELSREGISTER'])?$_POST['txtGKU_HANDELSREGISTER']:$rsGKU->FeldInhalt('GKU_HANDELSREGISTER'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_BEMERKUNG'] . ':', $FeldBreiten['Labels']);
			$Form->Erstelle_TextFeld('GKU_BEMERKUNG', isset($_POST['txtGKU_BEMERKUNG'])?$_POST['txtGKU_BEMERKUNG']:$rsGKU->FeldInhalt('GKU_BEMERKUNG'), $FeldBreiten['Werte'] / 10,
				$FeldBreiten['Werte'], $aendern, '', '', '', 'T');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_STATUS'] . ':', $FeldBreiten['Labels']);
			$Daten = explode('|', $AWISSprachKonserven['GKU']['lst_GKU_STATUS']);
			$Form->Erstelle_SelectFeld('!GKU_STATUS', isset($_POST['txtGKU_STATUS'])?$_POST['txtGKU_STATUS']:$rsGKU->FeldInhalt('GKU_STATUS'), $FeldBreiten['Werte'] . ';' . ($FeldBreiten['Werte'] - 25), $aendern, '','', '', '', '', $Daten, '');
			$Form->ZeileEnde();
		}
		else
		{
			$Seite = 'Fehler';
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
	}
	elseif ($Seite == 'Suche')//Suchmaske
	{
		//Kundennummer
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KUNDENNR'].':',190);
		$Form->Erstelle_TextFeld('*GKU_KEY',($Param['SPEICHERN']=='on'?$Param['GKU_KEY']:''),25,200,true);
		$Form->ZeileEnde();

		//Vertragsnummer
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_VERTRAGNR'].':',190);
		$Form->Erstelle_TextFeld('*GKU_VERTRAGNR',($Param['SPEICHERN']=='on'?$Param['GKU_VERTRAGNR']:''),25,200,true);
		$Form->ZeileEnde();

		//Name1
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_NAME1'].':',190);
		$Form->Erstelle_TextFeld('*GKU_NAME1',($Param['SPEICHERN']=='on'?$Param['GKU_NAME1']:''),25,200,true);
		$Form->ZeileEnde();

		//Konzern	SELECT-Feld
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KONZERN'].':',190);
		$Form->Erstelle_TextFeld('*GKU_KONZERN',($Param['SPEICHERN']=='on'?$Param['GKU_KONZERN']:''),25,200,true);
		$Form->ZeileEnde();

		//Ort
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_ORT'].':',190);
		$Form->Erstelle_TextFeld('*GKU_ORT',($Param['SPEICHERN']=='on'?$Param['GKU_ORT']:''),25,200,true);
		$Form->ZeileEnde();

		//Status 	Aktiv oder Inaktiv
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_STATUS'],190);
		$Daten = explode('|', $AWISSprachKonserven['GKU']['lst_GKU_STATUS']);
		$Form->Erstelle_SelectFeld('*GKU_STATUS', ($Param['SPEICHERN']=='on'?$Param['GKU_STATUS']:''), 250, true, '','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten, '');
		$Form->ZeileEnde();

		//Auswahl speichern
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
		$Form->ZeileEnde();
	}

	$AWISBenutzer->ParameterSchreiben('Formular_GKU', serialize($Param));
	$Form->Formular_Ende();
	$Form->Trennzeile('O');

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$ZurueckLink = './gutschein_Main.php?cmdAktion=Kunde';
	if ($Seite == 'Suche')
	{
		$ZurueckLink = '/index.php';
	}
	$Form->Schaltflaeche('href', 'cmd_zurueck', $ZurueckLink, '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if ($Seite == 'Suche')
	{
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	}
	if ($Seite == 'Liste' or $Seite == 'Suche')
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if ($Seite == 'Details') {

		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201701041702");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201701041602");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
function _BedingungErstellen($Param)
{

	global $DB;
	global $AWIS_KEY1;

	$Bedingung = '';

	if ($AWIS_KEY1 != '') {
		$Bedingung .= ' and GKU_KEY= ' . $DB->WertSetzen('GKU', 'N0', $AWIS_KEY1);
	}

	if (isset($Param['GKU_NAME1']) AND $Param['GKU_NAME1'] != '')
	{
		$Bedingung .= ' AND upper(GKU_NAME1) like ' . $DB->WertSetzen('GKU', 'T', mb_strtoupper('%' . $Param['GKU_NAME1'] . '%'));
	}
	if (isset($Param['GKU_KONZERN']) AND $Param['GKU_KONZERN'] != '')
	{
		$Bedingung .= ' AND upper(GKU_KONZERN) like ' . $DB->WertSetzen('GKU', 'T', mb_strtoupper('%' . $Param['GKU_KONZERN'] . '%'));
	}
	if (isset($Param['GKU_ORT']) AND $Param['GKU_ORT'] != '')
	{
		$Bedingung .= ' AND upper(GKU_ORT) like ' . $DB->WertSetzen('GKU', 'T', mb_strtoupper('%' . $Param['GKU_ORT'] . '%'));
	}
	if (isset($Param['GKU_STATUS']) AND $Param['GKU_STATUS'] != '')
	{
		$Bedingung .= ' AND upper(GKU_STATUS) like ' . $DB->WertSetzen('GKU', 'T', mb_strtoupper('%' . $Param['GKU_STATUS'] . '%'));
	}



	return $Bedingung;
}
?>