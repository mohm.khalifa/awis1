<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';
require_once 'awisMailer.inc';

class azubiprojekte_funktionen
{

    /**
     * @var awisFormular
     */
    public $Form;

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var awisMailer
     */
    public $awisMailer;

    /**
     * @var awisBenutzer
     */
    public $AWISBenutzer;

    /**
     * Recht f�r die Maske
     *
     * @var int
     */
    public $Recht65000;

    /**
     * AWISSprachKonserven
     *
     * @var array
     */
    public $AWISSprachKonserven;

    /**
     * CursorPosition der Seite
     *
     * @var String
     */
    public $AWISCursorPosition;

    /**
     * Parameter der Seite
     *
     * @var mixed
     */
    public $Param;

    /**
     * Datensatzkey
     *
     * @var string
     */
    public $AWIS_KEY1;

    /**
     * mailtexte_funktionen constructor.
     */
    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht65000 = $this->AWISBenutzer->HatDasRecht(65000);

        if(isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'Detail' and $_GET['cmdAktion'] == 'Suche')
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('AZTSuche'));

        $TextKonserven = array();
        $TextKonserven[] = array('AZT', '%');
        $TextKonserven[] = array('AZB', '%');
        $TextKonserven[] = array('TITEL', 'tit_Azubiprojekte');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('Wort', 'Status');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_reset');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'Personalnummer');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
        $this->awisMailer = new awisMailer($this->DB,$this->AWISBenutzer);
    }

    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
        $this->AWISBenutzer->ParameterSchreiben('AZTSuche', serialize($this->Param));
    }

    /**
     * Pr�ft die Rechte
     *
     * @param int $Bit
     */
    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht65000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'AZT'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    /**
     * Erstellt die Bedingung
     *
     * Legende der K�rzel: D = Details, M = MailVersand
     *
     * @param string $Seitenkuerzel Der Seitenk�rzel. Bei mehreren Bedingungen auf einer Seite m�ssen diese nummeriert werden.
     * @param mixed $Zusatzwert Zusatzwert, falls ben�tigt
     * @return string
     */
    public function BedingungErstellen(){
        $return = '';

        if ($this->AWIS_KEY1 > 0 and (isset($_GET["AZB_KEY"]) or isset($_POST["txtAZB_KEY"]))){
            $return .= " and AZB_KEY =" . $this->DB->WertSetzen('AZB', 'Z', $this->AWIS_KEY1);
        } elseif ($this->AWIS_KEY1 > 0){
            $return .= " and AZT_KEY =" . $this->DB->WertSetzen('AZT', 'Z', $this->AWIS_KEY1);
        }

        if (isset($_GET["AZB_KEY"]) and isset($_GET["AZB_KEY"])>0){
            $return .= " and AZB_KEY =" . $this->DB->WertSetzen('AZB', 'Z',$_GET["AZB_KEY"]);
            $this->AWIS_KEY1 = $_GET["AZB_KEY"];
        }

        if (isset($this->Param['AZT_AZB_STUFE']) and $this->Param['AZT_AZB_STUFE'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
            $return .= " and AZT_AZB_STUFE =" . $this->DB->WertSetzen('AZT', 'Z', $this->Param['AZT_AZB_STUFE']);
        }

        if (isset($this->Param['AZT_PERSNR']) and $this->Param['AZT_PERSNR'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
            $return .= " and AZT_PERSNR " . $this->DB->LikeOderIst($this->Param['AZT_PERSNR'], 0, 'AZT');
        }
        if (isset($this->Param['AZT_FILNR']) and $this->Param['AZT_FILNR'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
            $return .= " and AZT_FILNR " . $this->DB->LikeOderIst($this->Param['AZT_FILNR'], 0, 'AZT');
        }

        if (isset($this->Param['AZT_VORNAME']) and $this->Param['AZT_VORNAME'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
            $return .= " and AZT_VORNAME " . $this->DB->LikeOderIst($this->Param['AZT_VORNAME'], 0, 'AZT');
        }

        if (isset($this->Param['AZT_NAME']) and $this->Param['AZT_NAME'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
            $return .= " and AZT_NAME " . $this->DB->LikeOderIst($this->Param['AZT_NAME'], 0, 'AZT');
        }

        if ($return <> "") {
            $return = substr_replace($return, " where ", 0, 4);
        }
        return $return;
    }

    /**
     * Versendet Mails
     *
     * @param string $MVT_BEREICH MVT_BEREICH der Mailversandtext-Tabelle
     * @param string $Empfaenger Kuerzel f�r den Empf�nger der Mail; F = Filiale, O = Optionale Mail-Adresse
     * @param string $XPP_ABSENDER XPP_BEZEICHNUNG des Parameters der Absender-Mail
     * @param int $TextFormat Format des Textes -> Siehe awisMailer
     * @throws Exception
     */

    public function MailVersenden($MVT_BEREICH, $Empfaenger = "", $XPP_ABSENDER = "", $TextFormat = awisMailer::FORMAT_TEXT)
    {

        $this->awisMailer->LoescheAdressListe();

        $SQL = "SELECT MVT_TEXT, MVT_BETREFF from MAILVERSANDTEXTE WHERE MVT_BEREICH =" . $this->DB->WertSetzen('AZT', 'T', $MVT_BEREICH);
        $rsMailText = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('AZT'));


        $this->awisMailer->Betreff($rsMailText->FeldInhalt('MVT_BETREFF'));


        $Text = $this->MailReplacements($rsMailText->FeldInhalt('MVT_TEXT'));
        $this->awisMailer->Text($Text, $TextFormat);


        if ($XPP_ABSENDER <> "") {
            $this->awisMailer->Absender($this->AWISBenutzer->ParameterLesen($XPP_ABSENDER));
        } else {
            $this->awisMailer->Absender($this->AWISBenutzer->ParameterLesen("AZT_STANDART_ABSENDER"));
        }

        foreach(explode(';',$Empfaenger) as $Empfaenger) {
            $this->awisMailer->AdressListe(awisMailer::TYP_TO, $Empfaenger);
        }

        $this->awisMailer->SetzeBezug('AZT', $this->AWIS_KEY1);
        $this->awisMailer->MailInWarteschlange();

    }

    /**
     * Ersetzung der Platzhalter in dem Mailtext / -betreff
     *
     * @param string $Text
     * @return string
     */
    public function MailReplacements($Text = ""){
        $SQL = "SELECT t.*, b.* from AZUBIPROJEKTETEILNEHMER t left join azubiprojektebereiche b on t.AZT_AZB_STUFE = b.AZB_STUFE".$this->BedingungErstellen();
        $rsAZT = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('AZT'));

        $Text = str_replace("#AZT_NACHNAME#",$rsAZT->FeldInhalt('AZT_NAME'), $Text);
        $Text = str_replace("#AZT_VORNAME#",$rsAZT->FeldInhalt('AZT_VORNAME'), $Text);
        $Text = str_replace("#AZT_BEREICHNAME#",$rsAZT->FeldInhalt('AZB_BEREICHNAME'), $Text);

        return $Text;
    }

    public function LadeBereichInfoTexte() {
        $Script = "<script>".PHP_EOL;
        $Script .= "$(document).ready(function(){".PHP_EOL;
        $ScriptFunction = " $('#txtAZB_STUFE').change(function(){".PHP_EOL;
        $ScriptFunction .= " Aendere();".PHP_EOL;
        $ScriptFunction .= " })".PHP_EOL;
        $ScriptFunction .= " function Aendere() {".PHP_EOL;
        $SQL = "Select AZB_STUFE, AZB_INFOTEXT from AZUBIPROJEKTEBEREICHE WHERE AZB_STATUS = 'A'";
        $rsAZBINFO = $this->DB->RecordSetOeffnen($SQL);
        while(!$rsAZBINFO->EOF()) {

            $i = $rsAZBINFO->FeldInhalt("AZB_STUFE");

            $this->Form->ZeileStart('','AZB_INFOTEXT_' . $i);
            $this->Form->Erstelle_TextFeld('AZB_PUFFER_'.$i, '',0,290);
            $this->Form->Hinweistext($rsAZBINFO->FeldInhalt('AZB_INFOTEXT'), awisFormular::HINWEISTEXT_BENACHRICHTIGUNG,"width: 940px");
            $this->Form->ZeileEnde();

            $Script .= " $('#AZB_INFOTEXT_".$i."').hide();".PHP_EOL;
            $ScriptFunction .= "  $('#AZB_INFOTEXT_".$i."').hide();".PHP_EOL;
            $ScriptFunction .= "  var e = document.getElementById(\"txtAZB_STUFE\")".PHP_EOL;
            $ScriptFunction .= "  if(e.options[e.selectedIndex].value == ".$i.") {".PHP_EOL;
            $ScriptFunction .= "   $('#AZB_INFOTEXT_".$i."').show();".PHP_EOL;
            $ScriptFunction .= "  }".PHP_EOL;
            $rsAZBINFO->DSWeiter();
        }
        $ScriptFunction .= " }".PHP_EOL;
        if (isset($_POST['cmdSpeichern_x'])) {
            $i = $_POST['txtAZB_STUFE'];
            $Script .= "$('#AZB_INFOTEXT_".$i."').show();".PHP_EOL;
        }
        $Script .= $ScriptFunction;
        $Script .= "});";
        $Script .= "</script>";

        echo $Script;
    }
}

?>