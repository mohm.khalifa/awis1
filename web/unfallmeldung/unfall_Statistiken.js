
$(document).ready(function () {
    toggleUFA_OPTION('FIL');
    toggleUFA_OPTION('DATUM');

    $('#txtUFA_FIL_OPTION').change(function () {
        toggleUFA_OPTION('FIL');
    });

    $('#txtUFA_DATUM_OPTION').change(function () {
        toggleUFA_OPTION('DATUM');
    });



});


function toggleUFA_OPTION(bereich) {

    //Erstmal alle Filaldivs ausblenden (haben alle die Klasse nach Schema: OPTION_<bereich>)
    $('.OPTION_'+bereich).hide();

    //Danach nur die ausgewählte Option einblenden (haben alle eine ID nach folgendem Schema: DIV_<Option Value>)

    console.log($('#txtUFA_'+bereich+'_OPTION').val());
    $('#DIV_' + $('#txtUFA_'+bereich+'_OPTION').val()).show();
}