--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into BERICHTE
SET DEFINE OFF;
Insert into BERICHTE (XRE_KEY,XRE_BEZEICHNUNG,XRE_KLASSENNAME,XRE_FORMAT,XRE_AUSRICHTUNG,XRE_VORLAGE,XRE_XRC_ID,XRE_STUFE,XRE_PROTOKOLLEINTRAG,XRE_STATUS,XRE_BEMERKUNG,XRE_USER,XRE_USERDAT) values ('73','Praktikantenmappe','ber_PraktikantenMappe','A4','P','Praktikantenmappe.pdf','18000','4','1','A','Praktikantenmappe GirlsBoysDay','Tobias Sch�ffler',to_date('29.03.2017 08:28:21','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTE (XRE_KEY,XRE_BEZEICHNUNG,XRE_KLASSENNAME,XRE_FORMAT,XRE_AUSRICHTUNG,XRE_VORLAGE,XRE_XRC_ID,XRE_STUFE,XRE_PROTOKOLLEINTRAG,XRE_STATUS,XRE_BEMERKUNG,XRE_USER,XRE_USERDAT) values ('72','Sch�lerpraktikum-GirlsBoys','ber_PraktikantGBD','A4','P','BoysGirlsDay.pdf','18000','4','1','A','Informationsdokumete Sch�lerpraktikum-GirlsBoysDay','Tobias Sch�ffler',to_date('29.03.2017 08:26:56','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into BERICHTEPARAMETER
SET DEFINE OFF;
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1022','72','LinkerRand','10','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1023','72','Seite1-Oben','10','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1024','72','Schriftart','Arial','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1025','72','Schriftgroesse','10','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1026','72','Zeilenhoehe','4','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1027','72','KopfzeileSchriftgroesse','16','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1037','72','StartSeite1','42','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1038','72','SpalteFiliale','35','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1039','72','BreiteBetreuer','55','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1040','72','BreiteBisRechts','160','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1041','72','BreiteAussen','19','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1042','72','StartSeite3','50','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1043','72','Schriftklein','9','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1044','72','StartSeite4','38.5','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1045','72','SpalteKopf','70','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1046','72','Seite4Unterschrift','270','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1047','72','SchriftgroesseGross','18','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1048','72','StartSeite5','70','Tobias Sch�ffler',to_date('31.03.2017 13:07:15','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1016','73','LinkerRand','10','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1017','73','Seite1-Oben','10','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1018','73','Schriftart','Arial','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1019','73','Schriftgroesse','14','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1020','73','Zeilenhoehe','8','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1021','73','KopfzeileSchriftgroesse','16','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1028','73','SpalteSeite1','68','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1029','73','StartSeite1','69','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1030','73','SpalteSeite2','38','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1031','73','StartSeite2','205.5','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1032','73','Schriftklein','9','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1033','73','BetreuerSeite2','18','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1034','73','SpalteSeite4','49','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1035','73','StartSeite4','45.5','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('1036','73','Spalte2Seite4','140','Tobias Sch�ffler',to_date('31.03.2017 13:06:42','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
SET DEFINE OFF;

CREATE OR REPLACE FORCE VIEW "AWIS"."V_PRAKTIKUMSVEREINBARUNG" ("PVS_KEY", "PVS_EINGABENKORREKT", "PVS_PART", "PVS_ANWESENDTAGE", "PVS_TAETIGKEIT", "PVS_FREISTNOETIG", "PVS_PGRUND", "PVS_BETRRATINFIL", "PVS_ASCHUHTYP", "PVS_ASCHUHGR", "PVS_EINVEINGEHOLT", "PVS_ANR_ID", "F_NR", "F_BEZ", "F_STRASSE", "F_PLZ", "F_ORT", "F_TEL", "F_FAX", "F_MAIL", "F_LAND", "B_PERSNR", "B_NAME", "B_VORNAME", "GL1_ANREDE_ID", "GL1_ANREDE", "GL1", "GL2_ANREDE_ID", "GL2_ANREDE", "GL2", "WL1_ANREDE_ID", "WL1_ANREDE", "WL1", "WL2_ANREDE_ID", "WL2_ANREDE", "WL2", "RL_ANREDE_ID", "RL_ANREDE", "RL", "GBL_ANREDE_ID", "GBL_ANREDE", "GBL", "GBL_MAIL", "P_ANREDE", "P_BEWERBERNR", "P_NAME", "P_VORNAME", "P_STRASSE", "P_HAUSNR", "P_ADRZUSATZ", "P_PLZ", "P_ORT", "P_TEL", "P_GEBDATUM", "P_MAIL", "P_ZEITVON", "P_ZEITBIS", "P_DATUMVON", "P_DATUMBIS", "P_AKTIV", "P_GUTSCHEIN", "P_KOMMENTAR", "BT_NAME", "BT_STRASSE", "BT_HAUSNR", "BT_ADRZUSATZ", "BT_PLZ", "BT_ORT", "BT_KONPERSON", "BT_KONTEL", "BT_KONMAIL", "BT_AKTIV", "S_USER", "S_USERDAT") AS 
  SELECT pvs_key        AS pvs_key,
    pvs_eingabenkorrekt AS pvs_eingabenkorrekt,
    pvs_part            AS pvs_part,
    pvs_anwesendtage    AS pvs_anwesendtage,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'Taetigkeit'
    ) AS pvs_taetigkeit,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'FreistNoetig'
    ) AS pvs_freistnoetig,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'PGrund'
    ) AS pvs_pgrund,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'BetrRatInFil'
    ) AS pvs_betrratinfil,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'ASchuhtyp'
    ) AS pvs_aschuhtyp,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'ASchuhgr'
    ) AS pvs_aschuhgr,
    (SELECT pvi_wert
    FROM praktikumsvereinbarunginfos,
      informationstypen
    WHERE pvs_key = pvi_pvs_key
    AND ity_key   = pvi_ity_key
    AND SUBSTR(ity_information, 9) LIKE 'EinvEingeholt'
    )           AS pvs_einveingeholt,
    pvs_anr_id  AS pvs_anr_id,
    pvs_fil_id  AS f_nr,
    fil_bez     AS f_bez,
    fil_strasse AS f_strasse,
    fil_plz     AS f_plz,
    fil_ort     AS f_ort,
    (SELECT fif_wert FROM filialinfos WHERE fif_fil_id = FIL_ID AND fif_fit_id = 1 AND ROWNUM = 1) as F_Tel, 
    (SELECT fif_wert FROM filialinfos WHERE fif_fil_id = FIL_ID AND fif_fit_id = 2 AND ROWNUM = 1) as F_Fax,    
    (select lpad(FIL_ID,4,'0') || FUNC_emailendung_filiale(FIL_ID) from dual) as F_Mail,
    (SELECT lan_code FROM laender WHERE lan_wwskenn = fil_lan_wwskenn
    )              AS f_land,
    pvs_per_persnr AS b_persnr,
    name           AS b_name,
    vorname        AS b_vorname,
    (SELECT anr_id
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 74
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl1_anrede_id,
    (SELECT anr_anrede
      || ' '
      || per_nachname
      || ' '
      || per_vorname
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 74
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl1_anrede,
    (SELECT per_nachname
      || ' '
      || per_vorname
    FROM personal
    WHERE per_nr =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 74
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl1,
    (SELECT anr_id
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 75
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl2_anrede_id,
    (SELECT anr_anrede
      || ' '
      || per_nachname
      || ' '
      || per_vorname
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 75
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl2_anrede,
    (SELECT per_nachname
      || ' '
      || per_vorname
    FROM personal
    WHERE per_nr =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 75
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS gl2,
    (SELECT anr_id
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 76
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl1_anrede_id,
    (SELECT anr_anrede
      || ' '
      || per_nachname
      || ' '
      || per_vorname
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 76
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl1_anrede,
    (SELECT per_nachname
      || ' '
      || per_vorname
    FROM personal
    WHERE per_nr =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 76
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl1,
    (SELECT anr_id
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 77
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl2_anrede_id,
    (SELECT anr_anrede
      || ' '
      || per_nachname
      || ' '
      || per_vorname
    FROM personal
    INNER JOIN anreden
    ON per_anr_id = anr_id
    WHERE per_nr  =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 77
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl2_anrede,
    (SELECT per_nachname
      || ' '
      || per_vorname
    FROM personal
    WHERE per_nr =
      (SELECT fif_wert
      FROM filialinfos
      WHERE fif_fil_id = pvs_fil_id
      AND fif_fit_id   = 77
      AND fif_imq_id   = 8
      AND ROWNUM       = 1
      )
    ) AS wl2,
    (SELECT a.anr_id
    FROM anreden a
    WHERE a.anr_id =
      (SELECT p.per_anr_id
      FROM personal p
      WHERE (p.per_nachname
        || ' '
        || p.per_vorname) =
        (SELECT filialrolle(FIL_ID, 23, SYSDATE ,'Name') FROM dual
        )
      AND ROWNUM = 1
      )
    ) AS rl_anrede_id,
    (SELECT a.anr_anrede
      || ' '
      || filialrolle(FIL_ID, 23, SYSDATE ,'Name')
    FROM anreden a
    WHERE a.anr_id =
      (SELECT p.per_anr_id
      FROM personal p
      WHERE (p.per_nachname
        || ' '
        || p.per_vorname) =
        (SELECT filialrolle(FIL_ID, 23, SYSDATE ,'Name') FROM dual
        )
      AND ROWNUM = 1
      )
    )                                        AS rl_anrede,
    filialrolle(FIL_ID, 23, SYSDATE, 'Name') AS rl,
    (SELECT a.anr_id
    FROM anreden a
    WHERE a.anr_id =
      (SELECT p.per_anr_id
      FROM personal p
      WHERE (p.per_nachname
        || ' '
        || p.per_vorname) =
        (SELECT filialrolle(FIL_ID, 25, SYSDATE ,'Name') FROM dual
        )
      AND ROWNUM = 1
      )
    ) AS gbl_anrede_id,
    (SELECT a.anr_anrede
      || ' '
      || filialrolle(FIL_ID, 25, SYSDATE ,'Name')
    FROM anreden a
    WHERE a.anr_id =
      (SELECT p.per_anr_id
      FROM personal p
      WHERE (p.per_nachname
        || ' '
        || p.per_vorname) =
        (SELECT filialrolle(FIL_ID, 25, SYSDATE ,'Name') FROM dual
        )
      AND ROWNUM = 1
      )
    )                                        AS gbl_anrede,
    filialrolle(FIL_ID, 25, SYSDATE ,'Name') AS gbl,
    (SELECT kko_wert
    FROM filialebenenrollenzuordnungen aa,
      filialebenen bb,
      kontakte cc,
      (SELECT * FROM kontaktekommunikation WHERE kko_kot_key = 7
      ) ff
    WHERE aa.frz_xxx_key = bb.feb_key
    AND aa.frz_kon_key   = cc.kon_key
    AND cc.kon_key       = ff.kko_kon_key(+)
    AND aa.frz_fer_key   = 25
    AND cc.kon_name1
      || ' '
      || cc.kon_name2 =
      (SELECT p.per_nachname
        || ' '
        || p.per_vorname
      FROM personal p
      WHERE (p.per_nachname
        || ' '
        || p.per_vorname) =
        (SELECT filialrolle(FIL_ID, 25, SYSDATE ,'Name') FROM dual
        )
      AND ROWNUM = 1
      )
    AND ROWNUM = 1
    )               AS gbl_mail,
    anr_anrede      AS p_anrede,
    pvs_bewerbernr  AS p_bewerbernr,
    pvs_name        AS p_name,
    pvs_vorname     AS p_vorname,
    pvs_strasse     AS p_strasse,
    pvs_hausnr      AS p_hausnr,
    pvs_adrzusatz   AS p_adrzusatz,
    pvs_plz         AS p_plz,
    pvs_ort         AS p_ort,
    pvs_tel         AS p_tel,
    pvs_gebdatum    AS p_gebdatum,
    pvs_mail        AS p_Mail,
    pvs_zeitvon     AS p_zeitvon,
    pvs_zeitbis     AS p_zeitbis,
    pvs_datumvon    AS p_datumvon,
    pvs_datumbis    AS p_datumbis,
    pvs_aktiv       AS p_aktiv,
    pvs_gutschein       AS p_gutschein,
    pvs_kommentar       AS p_kommentar,
    pvs_btname      AS bt_name,
    pvs_btstrasse   AS bt_strasse,
    pvs_bthausnr    AS bt_hausnr,
    pvs_btadrzusatz AS bt_adrzusatz,
    pvs_btplz       AS bt_plz,
    pvs_btort       AS bt_ort,
    pvs_btkonperson AS bt_konperson,
    pvs_btkontel    AS bt_kontel,
    pvs_btkonmail   AS bt_konmail,
    pvs_btaktiv     AS bt_aktiv,
    pvs_user        AS s_user,
    pvs_userdat     AS s_userdat
  FROM praktikumsvereinbarung
  INNER JOIN anreden
  ON anr_id = pvs_anr_id
  INNER JOIN filialen
  ON fil_id = pvs_fil_id
  INNER JOIN personal_komplett
  ON persnr = pvs_per_persnr;
