INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Gesamtanzahl', 'Wort', 'Gesamtanzahl', 'Gesamtanzahl');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Aktion', 'Wort', 'Aktion', 'Aktion');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Bemerkung', 'Wort', 'Bemerkung', 'Bemerkung');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Einzelwert', 'Wort', 'Einzelwert', 'Einzelwert');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Operation', 'Wort', 'Operation', 'Operation');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Gesamtwert', 'Wort', 'Gesamtwert', 'Gesamtwert');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Land', 'Wort', 'Land', 'Land');
INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE) VALUES('Monat', 'Wort', 'Monat', 'Monat');
COMMIT;

INSERT INTO "AWIS"."RECHTE" (XRC_ID, XRC_RECHT) VALUES ('28010', 'Gutschein-Datenexporte');
COMMIT;

INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('28010', '0', 'Gutschein - Datenexport');
COMMIT;


INSERT INTO EXPORTDATEN(XDE_KEY, XDE_XRC_ID, XDE_STUFE, XDE_BEZEICHNUNG, XDE_BESCHREIBUNG, XDE_SQL, XDE_STATUS, XDE_USER, XDE_USERDAT)
VALUES(85, 28010, 1, 'Gutschein - Datenexport für Finanzbuchhaltung', 'Liefert den monatlichen Datenexport für die Finanzbuchhaltung',
'Select
kopf.GSK_KUNDE_INTERN || ext.GKU_KONZERN || '_' || vor.GSV_BEZEICHNUNG as Aktion,
pos.GSP_MENGE as Gesamtanzahl,
pos.GSP_BETRAG as Einzelwert,
pos.GSP_MENGE * pos.GSP_BETRAG as Gesamtwert,
case when kopf.GSK_STATUS = 'E' then
'erstellt und freigeschalten'
else 'erstellt'
End as Operation,
trunc(TO_DATE(kopf.GSK_USERDAT, 'DD.MM.YY HH24:MI:SS')) as Datum,
land.LAN_LAND as Land,
kopf.GSK_FREITEXT as Bemerkung,
trim(to_char(TO_DATE(kopf.GSK_USERDAT, 'DD.MM.YY HH24:MI:SS'),'MONTH')) as Monat,
trim(to_char(TO_DATE(kopf.GSK_USERDAT, 'DD.MM.YY HH24:MI:SS'),'YYYY')) as Jahr
From GUTSCHEINKOPFDATEN kopf
inner join GUTSCHEINVORLAGE vor on kopf.GSK_GSV_KEY = vor.GSV_KEY
inner join LAENDER land on vor.GSV_LAND = land.LAN_ATU_STAAT_NR
left join GUTSCHEINKUNDEEXTERN ext on kopf.GSK_GKU_KEY = ext.GKU_KEY
inner join GUTSCHEINPOSDATEN pos on kopf.GSK_AUFTRAGNR = pos.GSP_AUFTRAGNR',
'A', 'Tamara Bannert', SYSDATE);
COMMIT;



INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'AKTION', 'T', 100, 150, null, '10', 'Tamara Bannert', SYSDATE, 'Wort:Aktion', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'GESAMTANZAHL', 'N2', 10, 50, null, '20', 'Tamara Bannert', SYSDATE, 'Wort:Gesamtanzahl', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'EINZELWERT', 'N2', 10, 50, null, '30', 'Tamara Bannert', SYSDATE, 'Wort:Einzelwert', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'GESAMTWERT', 'N2', 10, 50, null, '40', 'Tamara Bannert', SYSDATE, 'Wort:Gesamtwert', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'OPERATION', 'T', 50, 100, null, '50', 'Tamara Bannert', SYSDATE, 'Wort:Operation', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'DATUM', 'D', 20, 50, null, '60', 'Tamara Bannert', SYSDATE, 'Wort:Datum', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'LAND', 'T', 50, 100, null, '70', 'Tamara Bannert', SYSDATE, 'Wort:Land', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'BEMERKUNG', 'T', 100, 150, null, '80', 'Tamara Bannert', SYSDATE, 'Wort:Bemerkung', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'MONAT', 'T', 50, 100, 'TXT:Liste:lst_Monat', '90', 'Tamara Bannert', SYSDATE, 'Wort:Monat', 0, 1);
INSERT INTO EXPORTDATENFELDER(XDF_XDE_KEY, XDF_FELDNAME, XDF_DATENTYP, XDF_FELDLAENGE, XDF_SPALTENBREITE, XDF_DATENQUELLE,
XDF_SORTIERUNG, XDF_USER, XDF_USERDAT, XDF_TEXTKONSERVE, XDF_PFLICHTFELD, XDF_FILTERFELD)
VALUES(85, 'JAHR', 'T', 50, 100, null, '100', 'Tamara Bannert', SYSDATE, 'Wort:Jahr', 0, 1);
COMMIT;
