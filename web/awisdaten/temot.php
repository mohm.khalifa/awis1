<?php
//************************************************************************************************************
// Datenschnittstelle: TWS
//************************************************************************************************************
//
// Aufraggeber: Sven Conrad
// Auftrag:     Juni 2007
//************************************************************************************************************
// Parameter:	FIL_ID		Filiale
//              GBL_PERS_NR Gebietsleiter
//              VKL_PERS_NR Verkaufsleiter
//************************************************************************************************************
// Datenformat: ASCII, Trennzeichen angebbar
//************************************************************************************************************
// Fehlercodes: ###RECHTE###       Kein ausreichendes Recht f�r diese Aktion
//************************************************************************************************************
require_once('db.inc.php');
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
global $awisRSZeilen;
global $awisRSInfo;
global $AWISBenutzer;


$con = awisLogon();

$Recht4000 = awisBenutzerRecht($con,4000);
if(($Recht4000&2)==0)
{
    awisEreignis(3,1000,'TEMOT-Export',$AWISBenutzer->BenutzerName(),'','','');
    echo '###RECHTE###';
	die();
}

// Optionale Zusatzparameter
if(isset($_GET['TRENNER']))
{
	$Trenner=$_GET['TRENNER'];
}
else
{
	$Trenner=';';
}

if(isset($_GET['ZEILENTRENNER']))
{
	$ZeilenTrenner=$_GET['ZEILENTRENNER'];
}
else
{
	$ZeilenTrenner="\r\n";
}


$SQL = 'select SUBSTR(ZUB_EXTERNEID,1,1) AS Firma, ZUB_BESTELLDATUM, ZUB_Hersteller, ZUB_ArtikelNummer, ZUB_ArtikelBezeichnung,  ';
$SQL .= ' ZLA_AST_ATUNR, ZUB_FIL_ID, ZWA_DATUM, AST_KENNUNG,';
$SQL .= ' (SELECT ASI_WERT FROM Artikelstamminfos WHERE ASI_AST_ATUNR = ZLA_AST_ATUNR AND ASI_AIT_ID=70) AS KENN2 ';
$SQL .= ', ZUB_PREISANGELIEFERT, ZLA_EK*(1-(ZLA_RABATT1/100)) AS ZLA_EK, ZLA_RABATT1';
$SQL .= ', ZUB_BESTELLMENGE, ZWA_MENGE, ZUB_PREISBRUTTO, ZWA_VK, ZWA_NACHLASS, ZWA_UMSATZ';
$SQL .= '';
$SQL .= ' from zukaufbestellungen';
$SQL .= ' INNER JOIN zukauflieferantenartikel ON zub_ZLA_KEY = ZLA_KEY';
$SQL .= ' LEFT OUTER JOIN artikelstamm ON AST_ATUNR = ZLA_AST_ATUNR';
$SQL .= ' LEFT OUTER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
$Bedingung = '';
if(isset($_GET['FIL_ID']))
{
	$Bedingung .= ' AND ZUB_FIL_ID='.intval($_GET['FIL_ID']);
}
if(isset($_GET['DATUM_VOM']))
{
	$Bedingung  .= " AND zub_bestelldatum >= ".awis_FeldInhaltFormat('DU',$_GET['DATUM_VOM'])."";
}
if(isset($_GET['DATUM_BIS']))
{
	$Bedingung  .= " AND zub_bestelldatum <= ".awis_FeldInhaltFormat('DU',$_GET['DATUM_BIS'])."";
}
if(isset($_GET['FIRMA']))
{
	$Bedingung  .= " AND SUBSTR(ZUB_EXTERNEID,1,1)= '".substr($_GET['FIRMA'],0,1)."'";
}
if(isset($_GET['AST_ATUNR']))
{
	$Bedingung  .= " AND AST_ATUNR = '".substr($_GET['AST_ATUNR'],0,1)."'";
}
if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung,4);
}
$rsDaten = awisOpenRecordset($con,$SQL);
$rsDatenZeilen = $awisRSZeilen;

/*
$xml = '<?xml version="1.0"?>';
$xml .= '<TemotDaten>';
*/

$Zeile='';
foreach($awisRSInfo AS $Feld)
{
	$Zeile .= $Trenner.$Feld['Name'];
}

//ob_clean();
header("Content-type-Header: application/csv");
header("Content-transfer-Encoding: binary");
header("Content-Disposition: attachment; filename=temot.csv");
header("Content-type: application/csv");

//�berschrift ausgeben
echo substr($Zeile,strlen($Trenner)).$ZeilenTrenner;

for($DatenZeile=0;$DatenZeile<$rsDatenZeilen;$DatenZeile++)
{
//	$xml .= '<Bestellung>';
	
	$Zeile='';
	foreach($awisRSInfo AS $Feld)
	{
		$Zeile .= $Trenner.$rsDaten[$Feld['Name']][$DatenZeile];
/*		$xml.='<'.$Feld['Name'].'>';
		$xml.=$rsDaten[$Feld['Name']][$DatenZeile];
		$xml.='</'.$Feld['Name'].'>';
*/		
	}
	echo substr($Zeile,strlen($Trenner)).$ZeilenTrenner;
	
	//$xml .= '</Bestellung>';
}	

//$xml .= '</TemotDaten>';
//echo $xml;
?>