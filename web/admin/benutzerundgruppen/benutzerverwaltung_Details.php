<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('XBN','%');
$TextKonserven[]=array('XBL','XBL_LOGIN');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Liste','lst_JaNein');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
$Recht1 = $AWISBenutzer->HatDasRecht(1);
if($Recht1==0)
{
    awisEreignis(3,1000,'Benutzerverwaltung',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/cmd_zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//********************************************************
// Parameter ?
//********************************************************
$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

$Param = array_fill(0,10,'');

if(isset($_POST['cmdSuche_x']))
{
	$Param['KEY']=0;			// Key
	$Param['XBN_NAME']=$_POST['sucXBN_NAME'];
	$Param['XBN_VORNAME']=$_POST['sucXBN_VORNAME'];
	$Param['XBN_STATUS']=$_POST['sucXBN_STATUS'];
	$Param['KAB_KEY']=$_POST['sucKAB_KEY'];
	$Param['XBL_LOGIN']=$_POST['sucXBL_LOGIN'];

	$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

	$Param['ORDERBY']='';
	$Param['BLOCK']='';
	$Param['KEY']=0;

	$AWISBenutzer->ParameterSchreiben("XBNSuche",serialize($Param));
	$AWISBenutzer->ParameterSchreiben("AktuellerXBN",'');
}
elseif(isset($_POST['cmdDSZurueck_x']))
{
	$Param = unserialize($AWISBenutzer->ParameterLesen('XBNSuche'));
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND XBN_NAME < '.$DB->FeldInhaltFormat('T',$_POST['txtXBN_NAME']);

	$SQL = 'SELECT XBN_KEY FROM (SELECT XBN_KEY FROM Benutzer';
	$SQL .= ' WHERE '.substr($Bedingung,4);
	$SQL .= ' ORDER BY XBN_NAME DESC';
	$SQL .= ') WHERE ROWNUM = 1';

	$rsXBN = $DB->RecordSetOeffnen($SQL);
	if(!$rsXBN->EOF())
	{
		$AWIS_KEY1=$rsXBN->FeldInhalt('XBN_KEY');
	}
	else
	{
		$AWIS_KEY1 = $_POST['txtXBN_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$Param = unserialize($AWISBenutzer->ParameterLesen('XBNSuche'));
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND XBN_NAME > '.$DB->FeldInhaltFormat('T',$_POST['txtXBN_NAME']);

	$SQL = 'SELECT XBN_KEY FROM (SELECT XBN_KEY FROM Benutzer';
	$SQL .= ' WHERE '.substr($Bedingung,4);
	$SQL .= ' ORDER BY XBN_NAME ASC';
	$SQL .= ') WHERE ROWNUM = 1';
	$rsXBN = $DB->RecordSetOeffnen($SQL);
	if(!$rsXBN->EOF())
	{
		$AWIS_KEY1=$rsXBN->FeldInhalt('XBN_KEY');
	}
	else
	{
		$AWIS_KEY1 = $_POST['txtXBN_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./benutzerverwaltung_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./benutzerverwaltung_speichern.php');
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$AWIS_KEY1 = -1;
}
elseif(isset($_GET['XBN_KEY']))
{
	$AWIS_KEY1 = $Form->Format('N0',$_GET['XBN_KEY']);
}
else 		// Letzten Benutzer suchen
{
	if(!isset($_GET['Liste']))
	{
		$AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerXBN");
	}
	else
	{
		$AWISBenutzer->ParameterSchreiben('AktuellerXBN',0);
	}

}

$Param = unserialize($AWISBenutzer->ParameterLesen("XBNSuche"));
//********************************************************
// Bedingung erstellen
//********************************************************
$Bedingung = _BedingungErstellen($Param);

//*****************************************************************
// Sortierung aufbauen
//*****************************************************************

if(!isset($_GET['Sort']))
{
	if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
	{
		$ORDERBY = $Param['ORDERBY'];
	}
	else
	{
		$ORDERBY = ' XBN_NAME';
	}
}
else
{
	$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
}

//*****************************************************************
// Abfrage erstellen -> Zeilennummer wg. Block
//*****************************************************************
$SQL = 'SELECT Benutzer.*';
$SQL .= ', (SELECT XBL_LOGIN FROM BenutzerLogins WHERE XBL_XBN_KEY = XBN_KEY AND ROWNUM=1) AS Login';
if($AWIS_KEY1<=0)
{
	$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
}
$SQL .= ' FROM Benutzer';
$SQL .= ' LEFT OUTER JOIN Kontakte ON xbn_kon_key = kon_key';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}
$SQL .= ' ORDER BY '.$ORDERBY;

//************************************************
// Aktuellen Datenblock festlegen
//************************************************
$Block = 0;
if(isset($_REQUEST['Block']))
{
	$Block = $Form->Format('N0',$_REQUEST['Block'],false);
}
elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
{
	$Block = intval($Param['BLOCK']);
}

//************************************************
// Zeilen begrenzen
//************************************************
$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
$StartZeile = ($Block*$ZeilenProSeite)+1;
$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
//$Form->DebugAusgabe(1,$SQL,$Param,$AWIS_KEY1);
//*****************************************************************
// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
//*****************************************************************
if($AWIS_KEY1<=0)
{
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
}
$Form->DebugAusgabe(1, $SQL);
$rsXBN = $DB->RecordSetOeffnen($SQL);

//*****************************************************************
// Aktuelle Parameter sichern
//*****************************************************************
$Param['ORDERBY']=$ORDERBY;
$Param['KEY']=$AWIS_KEY1;
$Param['BLOCK']=$Block;
$AWISBenutzer->ParameterSchreiben("XBNSuche",serialize($Param));

//awisFormular::DebugAusgabe(1,$SQL,$Param,$_POST,$Bedingung,$Block);

$Form->SchreibeHTMLCode('<form name=frmBenutzer action=./benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

//********************************************************
// Daten anzeigen
//********************************************************
if($rsXBN->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Benutzer gefunden.</span>';
}
elseif($rsXBN->AnzahlDatensaetze()>1)						// Liste anzeigen
{

	$Form->Formular_Start();

	$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	$Link = './benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=XBN_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBN_NAME'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_NAME'],300,'',$Link);
	$Link = './benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=XBN_VORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBN_VORNAME'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_VORNAME'],250,'',$Link);
	$Link = './benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=XBN_VOLLERNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBN_VOLLERNAME'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'],400,'',$Link);
	$Link = './benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=LOGIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LOGIN'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBL']['XBL_LOGIN'],200,'',$Link);
	$Form->ZeileEnde();
	$DS=0;

	while(!$rsXBN->EOF())
	{
		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './benutzerverwaltung_Main.php?cmdAktion=Details&XBN_KEY='.$rsXBN->FeldInhalt('XBN_KEY').'';
		$Form->Erstelle_ListenFeld('XBN_NAME',$rsXBN->FeldInhalt('XBN_NAME'),0,300,false,($DS%2),'',$Link);
		$Form->Erstelle_ListenFeld('XBN_VORNAME',$rsXBN->FeldInhalt('XBN_VORNAME'),0,250,false,($DS%2),'','');
		$Form->Erstelle_ListenFeld('XBN_VOLLERNAME',$rsXBN->FeldInhalt('XBN_VOLLERNAME'),0,400,false,($DS%2),'','');
		$Form->Erstelle_ListenFeld('LOGIN',$rsXBN->FeldInhalt('LOGIN'),0,200,false,($DS%2),'','');
		$Form->ZeileEnde();

		$rsXBN->DSWeiter();
	}

	$Link = './benutzerverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht1&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=benutzerverwaltung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();


}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	//echo '<table>';
	$AWIS_KEY1 = $rsXBN->FeldInhalt('XBN_KEY');

	$AWISBenutzer->ParameterSchreiben("AktuellerXBN",$AWIS_KEY1);
	$AWISBenutzer->ParameterSchreiben("AktuellerXBN",$AWIS_KEY1);
	$Form->Erstelle_HiddenFeld('XBN_KEY',$AWIS_KEY1);

	$Form->Formular_Start();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./benutzerverwaltung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsXBN->FeldInhalt('XBN_USER')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsXBN->FeldInhalt('XBN_USERDAT')));
	$Form->InfoZeile($Felder,'');

	$EditRecht=(($Recht1&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_ANREDE'].':',150);
	$AnredenText = explode("|",$AWISSprachKonserven['XBN']['lst_XBN_ANREDE']);
	$Form->Erstelle_SelectFeld('XBN_ANREDE',($rsXBN->FeldInhalt('XBN_ANREDE')),100,$EditRecht,'','','3','','',$AnredenText,'');
	if($EditRecht)
	{
		$AWISCursorPosition='txtXBN_ANREDE';
	}
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_NAME'].':',150);
	$Form->Erstelle_TextFeld('XBN_NAME',($rsXBN->FeldInhalt('XBN_NAME')),50,350,$EditRecht);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_VORNAME'].':',150);
	$Form->Erstelle_TextFeld('XBN_VORNAME',($rsXBN->FeldInhalt('XBN_VORNAME')),50,350,$EditRecht);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'].':',150);
	$Form->Erstelle_TextFeld('XBN_VOLLERNAME',($rsXBN->FeldInhalt('XBN_VOLLERNAME')),60,350,$EditRecht);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_STATUS'].':',150);
	$KateogrieAlt = explode("|",$AWISSprachKonserven['XBN']['lst_XBN_STATUS']);
	$Form->Erstelle_SelectFeld('XBN_STATUS',($rsXBN->FeldInhalt('XBN_STATUS')),100,$EditRecht,'','','A','','',$KateogrieAlt,'');
	$Form->ZeileEnde();

	if((floatval($Recht1)&64)!=0)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_SYSTEMUSER'].':',150);
		$JaNeinAnzeige = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('XBN_SYSTEMUSER',($rsXBN->FeldInhalt('XBN_SYSTEMUSER')),100,$EditRecht,'','','','','',$JaNeinAnzeige,'');
		$Form->ZeileEnde();
	}

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_KON_KEY'].':',150);
	$SQL = 'SELECT KON_KEY, KON_NAME1 || \', \' || COALESCE(KON_NAME2,\'\') AS KONNAME FROM Kontakte ';
	$SQL .= ' WHERE KON_STATUS = \'A\' OR KON_KEY = 0'.$rsXBN->FeldInhalt('XBN_KON_KEY');
	$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';	
	$Form->Erstelle_ListenBild('href','TelefonVZ','/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey='.$rsXBN->FeldInhalt('XBN_KON_KEY'),'/bilder/icon_telephone.png','Zum Telefonverzeichnis','','',16,16,20,'L','_blank');
	$Form->Erstelle_SelectFeld('XBN_KON_KEY',($rsXBN->FeldInhalt('XBN_KON_KEY')),100,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');	
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_FILIALEN'].':',150);
	$Form->Erstelle_TextFeld('XBN_FILIALEN',($rsXBN->FeldInhalt('XBN_FILIALEN')),60,350,$EditRecht,'','','','T','',$AWISSprachKonserven['XBN']['ttt_XBN_FILIALEN']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_BEMERKUNG'].':',150);
	$Form->Erstelle_Textarea('XBN_BEMERKUNG',($rsXBN->FeldInhalt('XBN_BEMERKUNG')),350,100,6,$EditRecht);
	$Form->ZeileEnde();

	if($rsXBN->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['txt_KopieVon'].':',150);
		$SQL = 'SELECT XBN_KEY, XBN_NAME || COALESCE(\', \' || XBN_VORNAME,\'\') AS ANZEIGE';
		$SQL .= ' FROM Benutzer ';
		$SQL .= ' WHERE XBN_STATUS = \'A\'';
		$SQL .= ' AND XBN_NAME not like \'fil%\'';
		$SQL .= ' ORDER BY XBN_NAME, XBN_VORNAME';
		$Form->Erstelle_SelectFeld('KOPIEVON','',100,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();


	if(!isset($_POST['cmdDSNeu_x']))
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Logins'));

		$RegDet = new awisRegister(101);
		$RegDet->ZeichneRegister($RegisterSeite);
	}


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

	if(($Recht1&(2+4+256))!==0)		//
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht1&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	/*
	if(($Recht1&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}
	*/
	$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=benutzerverwaltung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if(floatval($AWIS_KEY1) <> 0)
	{
		$Bedingung = 'AND XBN_KEY = '.$AWIS_KEY1;
		return $Bedingung;
	}

	if(isset($Param['XBN_NAME']) AND $Param['XBN_NAME']!='')
	{
		$Bedingung .= 'AND (UPPER(XBN_NAME) ' . $DB->LikeOderIst($Param['XBN_NAME'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'OR  UPPER(XBN_VOLLERNAME) ' .$DB->LikeOderIst($Param['XBN_NAME'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'OR EXISTS (SELECT XBL_LOGIN FROM BenutzerLogins WHERE XBL_XBN_KEY = XBN_KEY AND XBL_LOGIN ' .$DB->LikeOderIst($Param['XBN_NAME'].'%',awisDatenbank::AWIS_LIKE_UPPER).')';
		$Bedingung .= ')';
	}
	if(isset($Param['XBN_VORNAME']) AND $Param['XBN_VORNAME']!='')
	{
		$Bedingung .= 'AND (UPPER(XBN_VORNAME) ' . $DB->LikeOderIst($Param['XBN_VORNAME'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'OR  UPPER(XBN_VOLLERNAME) ' .$DB->LikeOderIst($Param['XBN_VORNAME'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['XBN_STATUS']) AND $Param['XBN_STATUS']!='')
	{
		$Bedingung .= 'AND XBN_STATUS = ' . $DB->FeldInhaltFormat('T',$Param['XBN_STATUS']) . ' ';
	}
	if(isset($Param['XBL_LOGIN']) AND $Param['XBL_LOGIN']!='')
	{
		$Bedingung .= 'AND EXISTS(SELECT * FROM BenutzerLogins WHERE XBL_LOGIN ' . $DB->LikeOderIst($Param['XBL_LOGIN'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ' AND XBL_XBN_KEY = XBN_KEY) ';
	}
	if(isset($Param['KAB_KEY']) AND floatval($Param['KAB_KEY'])!=0)
	{
		if(floatval($Param['KAB_KEY'])==-1)
		{
			$Bedingung .= 'AND KON_KAB_KEY IS NULL ';
		}
		else
		{
			$Bedingung .= 'AND KON_KAB_KEY = ' . $DB->FeldInhaltFormat('N0',$Param[4]) . ' ';
		}
	}

	if(($AWISBenutzer->HatDasRecht(1)&(64))==0)
	{
		$Bedingung .= ' AND XBN_SYSTEMUSER = 0';
	}

	return $Bedingung;
}