<?php
try
{
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');


	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_FILES);
	
	$AWISWerkzeuge = new awisWerkzeuge();
	
	$Statistik['gesamt']=0;
	$Statistik['neu']=0;
	$Statistik['geaendert']=0;
	// Pr�fsumme ermitten, um zu pr�fen, ob die Datei schon mal importiert wurde
	$MD5 = md5_file($_FILES['IMPORT']['tmp_name']);
	$SQL = 'SELECT *';
	$SQL .= ' FROM IMPORTPROTOKOLL';
	$SQL .= ' WHERE XDI_BEREICH = \'RRP\'';
	$SQL .= ' AND XDI_MD5 = '.$DB->FeldInhaltFormat('T',$MD5);
	$rsXDI = $DB->RecordSetOeffnen($SQL);
	$Form->DebugAusgabe(1,$DB->LetzterSQL());
//	die();
	
//	if(!$rsXDI->EOF())
	if (1==2)
	{
		// Dateien k�nnen immer nur einmal eingelesen werden
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datei wurde bereits importiert.', 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Benutzer:'.$rsXDI->FeldInhalt('XDI_USER'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datum:'.$rsXDI->FeldInhalt('XDI_DATUM'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Dateiname:'.$rsXDI->FeldInhalt('XDI_DATEINAME'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->Trennzeile();
		$Form->Formular_Ende();
	}
	else
	{
		$DB->TransaktionBegin();
		
		$Datei = $_FILES['IMPORT']['tmp_name'];

		// Datei �ffnen
		$XLSXObj = PHPExcel_IOFactory::load($Datei);

				// Blatt untersuchen
		$BlattObj = $XLSXObj->getActiveSheet();
		// Kenner lesen
		$Kenner = $BlattObj->getCell('A1')->getValue();
		
		if($Kenner != 'AWIS')
		{
			$Form->Erstelle_TextLabel('Falsche Importdatei.'.$Kenner, 800);
		}
		else
		{
			$Form->Erstelle_TextLabel('Daten werden importiert...', 800);

			$SQL = 'INSERT INTO IMPORTPROTOKOLL(';
			$SQL .= 'XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5';
			$SQL .= ', XDI_USER,XDI_USERDAT)VALUES (';
			$SQL .= ' \'RRP\'';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$_FILES['IMPORT']['name']);
			$SQL .= ', SYSDATE';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$MD5);
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ', SYSDATE)';
			$DB->Ausfuehren($SQL);
			$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			$SQL = 'SELECT seq_XDI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$XDI_KEY=$rsKey->FeldInhalt('KEY');
			
			// �berschriften lesen
			$Ueberschrift=array();
			$Zeile = 2;
			$Spalte = 'A';
			$Zelle = $Spalte.$Zeile;
			while($BlattObj->getCell($Zelle)->getValue()!='')
			{
				$Ueberschrift[$BlattObj->getCell($Zelle)->getValue()]=$Spalte++;
				$Zelle = $Spalte.$Zeile;
			}
						
			$Form->DebugAusgabe(1,$Ueberschrift);
			
			$FEHLER = false;
			
			$SQL = 'SELECT LAN_CODE FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL';
			$rsLAN=$DB->RecordSetOeffnen($SQL);
			$GueltigeLaender=$rsLAN->Tabelle();
			
				
			// Jetzt die Daten
			$Zeile=3;
			while ($BlattObj->getCell($Ueberschrift['KEY'].$Zeile)->getValue()!='')
			{
				
			    // Pr�froutinen f�r die Daten
                $Land = $BlattObj->getCell($Ueberschrift['LAND'].$Zeile)->getValue();
                if(!in_array($Land, $GueltigeLaender['LAN_CODE']))
                {
                     echo '<br>(W) Zeile '.$Zeile.': falsches Land: '.$Land.'. Verwende DE';
                     $Land = 'DE';
                }
			    
				$GueltigAb = $BlattObj->getCell($Ueberschrift['GUELTIGAB'].$Zeile)->getValue();
				if($GueltigAb!='')
				{
					if(is_numeric($GueltigAb) AND $GueltigAb >30000 AND $GueltigAb <99999)
					{
						$GueltigAb = date('d.m.Y',strtotime('01-01-1900 + '.$GueltigAb .' days - 2 days'));
					}
					else
					{
						$GueltigAb = $Form->PruefeDatum($GueltigAb,false,false,false);
					}
				}
				else
				{
                    echo '<br>(W) Zeile '.$Zeile.': ung�ltiges Datum ab, verwende heutiges Datum!';
					$GueltigAb = date('d.m.Y');
				}

				$VK = $DB->FeldInhaltFormat('N2',KodiereText($BlattObj->getCell($Ueberschrift['VK'].$Zeile)->getCalculatedValue()));

				//*********************************************************************************
				// Daten importieren
				//*********************************************************************************
				$SQL =  'MERGE INTO reifenpflegepreise dest';
				$SQL .= ' USING (';
				$SQL .= ' SELECT ';
				$SQL .= '  '.$DB->FeldInhaltFormat('Z',KodiereText($BlattObj->getCell($Ueberschrift['KEY'].$Zeile)->getValue())) .' AS RPP_RRP_KEY';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('T',KodiereText($Land)) .' AS RPP_LAN_CODE';
				$SQL .= ' ,'.$VK.' AS RPP_VK';
				$SQL .= ' ,'.$DB->FeldInhaltFormat('D',$GueltigAb) .' AS RPP_GUELTIGAB';
				$SQL .= ' ,SYSDATE AS RPP_USERDAT';
				$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName()).' AS RPP_USER';
				$SQL .= ' FROM DUAL) src ';
				$SQL .= ' ON (';
				$SQL .= '  src.rpp_rrp_key=dest.rpp_rrp_key ';
				$SQL .= '  AND src.rpp_lan_code=dest.rpp_lan_code ';
				$SQL .= '  AND src.rpp_gueltigab=dest.rpp_gueltigab';
				$SQL .= '  )';
				$SQL .= ' WHEN NOT MATCHED THEN ';
				$SQL .= ' INSERT (dest.RPP_RRP_KEY,dest.RPP_LAN_CODE,dest.RPP_VK,';
				$SQL .= '  dest.RPP_GUELTIGAB,dest.RPP_USER,dest.RPP_USERDAT)';
				$SQL .= ' VALUES (';
				$SQL .= '  src.RPP_RRP_KEY';
				$SQL .= ' ,src.RPP_LAN_CODE';
				$SQL .= ' ,src.RPP_VK';
				$SQL .= ' ,src.RPP_GUELTIGAB';
				$SQL .= ' ,src.RPP_USER';
				$SQL .= ' ,src.RPP_USERDAT';
				$SQL .= ' )';
				$SQL .= ' WHEN MATCHED THEN';
				$SQL .= '  UPDATE';
				$SQL .= '  SET dest.RPP_VK='.$VK;
				$SQL .= '  ,dest.RPP_USER=src.RPP_USER';
				$SQL .= '  ,dest.RPP_USERDAT=src.RPP_USERDAT';
				$SQL .= '  WHERE dest.RPP_RRP_KEY=src.RPP_RRP_KEY';
				$SQL .= '  AND dest.RPP_LAN_CODE=src.RPP_LAN_CODE';
				$SQL .= '  AND dest.RPP_GUELTIGAB=src.RPP_GUELTIGAB';

				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
				$Statistik['gesamt']++;
				
				$Zeile++;
			}
		}
		$DB->TransaktionCommit();

		$Form->Erstelle_TextLabel('-- Verarbeitete Daten:'.$Statistik['gesamt'], 800);
	}
}
catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL(),$ex->getLine());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

/**
 * Kodierung des Textes, damit Umlaute richtig gespeichert werden
 * @param string $Text
 */
function KodiereText($Text)
{
    $Erg = iconv('UTF-8','WINDOWS-1252',$Text);
    return ($Erg);
}
?>