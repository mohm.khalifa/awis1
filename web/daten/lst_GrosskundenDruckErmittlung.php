<?php
require_once '/daten/web/grosskundenkarten/grosskundenkarten_funktionen.php';
require_once 'awisRest.inc';

$awisRest = new awisRest('GK_Druckermittlung');
$awisRest->verarbeiteRequest();

$GK = new grosskundenkarten_funktionen();



$SQL = 'SELECT DISTINCT KND.EAN_NR FROM '.$GK->holeKartenTabelle().' KND
                LEFT JOIN EXPRABATTZUORDNUNGEN ON ERZ_KUNDEN_NR = KND.KUNDEN_NR 
                LEFT JOIN '.$GK->holeDestTabelle().' LAND ON KND.KUNDEN_NR = LAND.KUNDEN_NR 
                WHERE KND.KUNDEN_NR IN (' . $_GET['KDNR'] . ') AND KND.GEDRUCKT = 1';


$rsGK = $GK->DB->RecordSetOeffnen($SQL);

$Return = array();
while(!$rsGK->EOF()){
    $Return[] = ['KDNR'=> $_GET['KDNR'],'EAN'=>$rsGK->FeldInhalt('EAN_NR')];
    $rsGK->DSWeiter();
}

$awisRest->sendeResponse($Return,awisRest::AntwortTyp_JSON);