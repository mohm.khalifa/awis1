<?php
/**
*
* Routinen zum Aufbauen von Masken
*
*
* Autor: Sacha Kerres
************************************************************************************************************
*
* Aenderungen
*
* 18.07.2006	Sacha Kerres			Debugmeldung in awis_RegisterErstellen() nur im Fehlerfall ausgeben
* 07.12.2006	Sacha Kerres			Funktionen für FORM eingefügt
*
*
***********************************************************************************************************/

/*****************************************************************************
* Funktion baut den Anfang eines Registers auf
* Doku: 10.9.2003
*****************************************************************************/
function awis_RegisterAufbauen($Anzahl, $Beschriftung, $Aktives)
{
    $Ret = "";		// Räckgabe-Wert

    $Ret = "<table id=LeererRahmen border=0 cellspacing=0 cellpadding=0>";

    $Ret = $Ret . "<tr>";

	    // Alle Register aufbauen

    for($i=0;$i<$Anzahl;$i++)
    {
		if($Aktives==$i+1)
		{
		    $RegTyp = 1;
		}
		else
		{
		    $RegTyp = 2;
		}
		$Ret = $Ret . _awis_RegisterBlatt($RegTyp,$Beschriftung[$i][1],$Beschriftung[$i][0],$Beschriftung[$i][2]);
    }

    $Ret = $Ret . _awis_RegisterBlatt(3,"","","");

    $Ret = $Ret . "</tr>";

    $Ret = $Ret . "<tr id=Register>";

    for($i=0;$i<$Anzahl;$i++)
    {
        $Ret = $Ret . "<td id=Register colspan=3 bgcolor=";
		If($i+1==$Aktives)
		{
	    	$Ret = $Ret . "#C0C0C0";
		}
		else
		{
		    $Ret = $Ret . "#000000";
		}

		// Trennlinie zwischen den Registerblättern und dem Datenbereich
		$Ret = $Ret . " height=1 style=border-top-color:#000000; border-top-width=0px;";
        $Ret = $Ret . "border-bottom-color:#000000; border-bottom-width=0px;";
        $Ret = $Ret . "border-left-width=0px;";
        $Ret = $Ret . "border-right-width=0px;";
        $Ret = $Ret . "border-style:solid></td>";
    }	// Ende for

	// Trennlinie zwischen Daten und Registerblättern
    $Ret = $Ret . "<td id=RegisterTeiler></td>";

    $Ret = $Ret . "</tr>";

	// Zelle fär den Datenbereich
    $Ret = $Ret . "<tr><td id=RegisterInhalt height=200 valign=top colspan=". (($Anzahl*3)+1) .">";

    return $Ret;
}

/*****************************************************************************
* Funktion schließt den Aufbau eines Registers ab
*
* Interne Funktion !
*****************************************************************************/
function awis_RegisterAbschliessen()
{
    $Ret = "</td></tr></table>";
    return $Ret;
}


/******************************************************************************
* Registerblatt aufbauen
*
* Interne Funktion !
******************************************************************************/
function _awis_RegisterBlatt($Typ, $Link, $Beschriftung, $Bemerkung)
{
    $Ret = "";		// Räckgabewert

    If($Typ==1)		// Aktiv
    {
        $Ret = "<td nowrap id=Register width=5><img border=0 align=top valign=top src=/bilder/liEckeG.gif></td>";

		$Ret .= "<td nowrap id=RegisterAktiv ";
		$Ret .= ' title="'.$Bemerkung.'"';
		$Ret .= " width=50>";

		$Beschriftung = str_replace("<u>","",$Beschriftung);
		$Beschriftung = str_replace("</u>","",$Beschriftung);

		$Ret .= $Beschriftung . "</td>";

		$Ret .= "<td nowrap id=Register><img border=0 align=top valign=top src=/bilder/reEckeG.gif></td>";

    }
    elseif($Typ==2) 	// Inaktiv
    {
        $Ret = "<td id=Register width=5><img border=0 align=top valign=top src=/bilder/liEcke.gif></td>";

		$Ret .= "<td id=RegisterInaktiv nowrap width=50>";

		if(strpos($Beschriftung,"<u>")>=0)
		{
			$accessKey = substr($Beschriftung,strpos($Beschriftung,"<u>")+3,1);
		}

		$Ret .= "<a id=RegisterLink href=" . $Link . " onmouseout=" . chr(34) . "window.status='AWIS'; return true" . chr(34);
		If($accessKey!="")
		{
			$Ret .= " accesskey=" . $accessKey;
		}
		$Ret .= ' onmouseover="' . chr(34) . "window.status='" . $Bemerkung . "'; return true" . chr(34);
		$Ret .= ' title="'.$Bemerkung.'"';
		$Ret .= ">".$Beschriftung ."</a></td>";

		$Ret = $Ret . "<td nowrap id=Register><img border=0 align=top valign=top src=/bilder/reEcke.gif></td>";
    }
    elseif($Typ==3)	// Leeres Register (als Abschluss)
    {
        $Ret = "<td id=RegisterAbschluss width=99999></td>";
    }

    return $Ret;
}


/*****************************************************************************
*
* Funktion liefert den Dateinamen f�r die CSS-Datei
*
*	Parameter: $Benutzername, string
*
*****************************************************************************/
function awis_CSSDatei($Benutzername)
{
   If(is_file("/daten/web/css/" . strtolower($Benutzername) . ".css"))
   {
	   return "/css/" . strtolower($Benutzername) . ".css";
   }
   else
   {
	   return "/css/ATU.css";
   }
}

/******************************************************************************
*
*	BenutzerParameter auslesen
*
*	Parameter:	$con			AktiveVerbindung
*			$ParameterWert		Name des Parameters
*			$BenutzerParameter	Name des Anwenders oder eindeutige Benutzer-ID
*
******************************************************************************/

function awis_BenutzerParameter($con, $ParameterWert, $BenutzerName)
{
    global $awisRSZeilen;

    if(session_id()=='')
    {
    	session_start();
    }
    
    $BindeVariablen=array();
    $BindeVariablen['var_T_xpp_bezeichnung']=$ParameterWert;
    
    $__SQL = "SELECT DISTINCT XBP_WERT, XBL_LOGIN, XPP_ID from AWIS.V_Benutzerparameter";
	if(intval($BenutzerName)>0)
	{
		$BindeVariablen['var_N0_xbn_key']=$BenutzerName;
		$__SQL = $__SQL . " where XBN_KEY = :var_N0_xbn_key";
	}
	else
	{
		$BindeVariablen['var_T_xbl_login']=strtoupper($BenutzerName);
		$__SQL = $__SQL . " where XBL_LOGIN = :var_T_xbl_login";
	}

    $__SQL = $__SQL . " and XPP_BEZEICHNUNG = :var_T_xpp_bezeichnung";
    $__SQL = $__SQL . " UNION ALL SELECT DISTINCT XPP_DEFAULT, 'ZZZZZZZZZZZZZZ', XPP_ID from AWIS.ProgrammParameter";
    $__SQL = $__SQL . " WHERE XPP_BEZEICHNUNG = :var_T_xpp_bezeichnung";
    $__SQL = $__SQL . " ORDER BY XBL_LOGIN";

//    print "<hr>" . $__SQL . "<hr>";

    $rsParameter = awisOpenRecordset($con, $__SQL, true, false, $BindeVariablen);

    If($awisRSZeilen > 0)
    {
    	// Gibt es den Parameter als Session?
    	// --> wenn ja, den Session-Wert und nicht den DB Wert liefern
    	if(isset($_SESSION['XBP_'.$rsParameter["XPP_ID"][0]]))
    	{
    		//awis_Debug(1,$_SESSION['XBP_'.$rsParameter["XPP_ID"][0]],$_SESSION,$rsParameter["XBP_WERT"][0],session_id(),'Session !!');
    		return $_SESSION['XBP_'.$rsParameter["XPP_ID"][0]];
    	}
        return $rsParameter["XBP_WERT"][0];
    }
    else
    {
        return "##KEINE DATEN##:" . $__SQL . "##";
    }
}

/******************************************************************************
*
*	BenutzerParameter neu setzen
*
*	Parameter:	$con			AktiveVerbindung
*			$ParameterWert		Name des Parameters
*			$BenutzerParameter	Name des Anwenders
*			$NeuerWert		Neuer Wert
*	Räckgabe:	-keine-
*
******************************************************************************/

function awis_BenutzerParameterSpeichern($con, $ParameterWert, $BenutzerID, $NeuerWert)
{
    global $awisRSZeilen;


    //********************************************
    // Session
    //********************************************

    if(session_id()=='')
    {
    	session_start();
    }

	if(is_numeric($ParameterWert))
	{
		$PPID = $ParameterWert;
	}
	else
	{
		$BindeVariablen=array();
		$BindeVariablen['var_T_xpp_bezeichnung']=$ParameterWert;
		
	    $rsParameter = awisOpenRecordset($con, "SELECT XPP_ID FROM AWIS.ProgrammParameter WHERE XPP_Bezeichnung=:var_T_xpp_bezeichnung",false,false,$BindeVariablen);
	    $PPID = $rsParameter['XPP_ID'][0];
	    if(is_null($PPID))
	    {
	    	awis_Debug(1,"Programmparameter " . $ParameterWert . " existiert nicht!");
	    }
	}

	if(intval($BenutzerID)==-1)			// Zentralen Wert ändern
	{
		$BindeVariablen=array();
		$BindeVariablen['var_xpp_default']=$NeuerWert;
		$BindeVariablen['var_N0_xpp_id']=$PPID;
		
		$SQL = "UPDATE AWIS.ProgrammParameter SET XPP_DEFAULT = :var_xpp_default WHERE XPP_ID=:var_N0_xpp_id";
		awisExecute($con, $SQL,true,false,0,$BindeVariablen);
	}
	else
	{
		if(intval($BenutzerID)==0)
		{
			$BenutzerID=awisBenutzerID($BenutzerID);
		}
	    
	    $BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']='0'.$BenutzerID;
		$BindeVariablen['var_N0_xpp_id']=$PPID;
		
		$SQL = "SELECT DISTINCT XBP_Wert from AWIS.Benutzerparameter";
	    $SQL = $SQL . " WHERE (XBP_XBN_KEY = :var_N0_xbn_key)";
	    $SQL = $SQL . " AND XBP_XPP_ID = :var_N0_xpp_id";

	    $rsParameter = awisOpenRecordset($con, $SQL,true,false,$BindeVariablen);
	    
	    $BindeVariablen=array();
		$BindeVariablen['var_xbp_wert']=$NeuerWert;
		$BindeVariablen['var_N0_xpp_id']=$PPID;
		$BindeVariablen['var_N0_xbn_key']=$BenutzerID;

	    If($awisRSZeilen > 0)
	    {
	    	$SQL = "UPDATE AWIS.BenutzerParameter SET XBP_WERT = :var_xbp_wert WHERE XBP_XPP_ID=:var_N0_xpp_id";
			$SQL = $SQL . " AND XBP_XBN_KEY=:var_N0_xbn_key";
	    }
	    elseif($BenutzerID!=0)
	    {
	    	$SQL = "INSERT INTO AWIS.BenutzerParameter(XBP_WERT, XBP_XPP_ID, XBP_XBN_Key)";
			$SQL = $SQL . " VALUES(:var_xbp_wert, :var_N0_xpp_id, :var_N0_xbn_key)";
	    }
		awisExecute($con, $SQL,true,false,0,$BindeVariablen);

		if($BenutzerID == awisBenutzerID())
		{
			// Parameter in der Session ändern / anlegen
			$_SESSION['XBP_'.$PPID]=$NeuerWert;
		}
	}
}


/******************************************************************************************************
*
*	Liefert eine Beschriftung fär einen Feldnamen
*
* 		Es wird ein einzelner Name fär ein Feld gesucht
*
* 	$con		Offene Verbindung
* 	$Feldname	Name des Feldes
* 	$SatzNr		(optional) Nummer des Namenssatzes
*
******************************************************************************************************/

function awis_FeldnameBez($con, $Feldname, $SatzNr = 1)
{
    global $awisRSZeilen;

    $BindeVariablen=array();
    $BindeVariablen['var_T_xfn_feld']=$Feldname;
    $BindeVariablen['var_N0_xfn_satznr']=intval('0' . $SatzNr);
    $rsFeld = awisOpenRecordset($con,"SELECT * FROM AWIS.Feldnamen WHERE XFN_FELD=:var_T_xfn_feld AND XFN_SatzNr=:var_N0_xfn_satznr",true,false,$BindeVariablen);
    If($awisRSZeilen==0)
    {
        return $Feldname;
    }
    return $rsFeld["XFN_BEZEICHNUNG"][0];
}

/**
*
*	awis_RegisterErstellen()
*
*	@desc Baut eine Registerseite auf.
*
*	@author Sacha Kerres
*	@param int $xwm_id
*	@param ressource $con
*	@param string $AktiveSeite
*	@return void
*	@version 1.0
*
*****************************************************************************************************/

function awis_RegisterErstellen($xwm_id, $con, $AktiveSeite='')
{
	global $awisRSZeilen;
	global $cmdAktion;
	global $AWISSprache;

	if($AktiveSeite!='')
	{
		$cmdAktion=$AktiveSeite;
	}
	$Seite = 0;
	$BindeVariablen=array();
	$BindeVariablen['var_N0_xwm_id']=intval('0'.$xwm_id);
	$rsRegisterSeiten = awisOpenRecordset($con, "SELECT * FROM AWIS.WebMaskenRegister WHERE XRG_XWM_ID=:var_N0_xwm_id ORDER BY XRG_ID",true,false,$BindeVariablen);
	$rsRegisterSeitenAnz = $awisRSZeilen;
	$RegisterBeschriftung = array();

	for($RegAnzahl=0;$RegAnzahl<$rsRegisterSeitenAnz;$RegAnzahl++)
	{
		if($rsRegisterSeiten["XRG_XRC_ID"][$RegAnzahl]!=0)
		{
			if(awisBenutzerRecht($con,$rsRegisterSeiten["XRG_XRC_ID"][$RegAnzahl])==0)
			{
				continue;		// Seite ausblenden
			}
		}

		if(!isset($AWISSprache) OR $AWISSprache=='DE')
		{
			$RegisterBeschriftung[$Seite][0]=$rsRegisterSeiten["XRG_BESCHRIFTUNG"][$RegAnzahl];
			$RegisterBeschriftung[$Seite][1]=$rsRegisterSeiten["XRG_AKTION"][$RegAnzahl];
			$RegisterBeschriftung[$Seite][2]=$rsRegisterSeiten["XRG_BEMERKUNG"][$RegAnzahl];
			$RegisterBeschriftung[$Seite][3]=$rsRegisterSeiten["XRG_SEITE"][$RegAnzahl];
			$RegisterBeschriftung[$Seite++][4]=$rsRegisterSeiten["XRG_BEZEICHNUNG"][$RegAnzahl];
		}
		else	// Übersetzen
		{
			$TextKonserven = array();
			$TextKonserven[]=array('Register','reg_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]);
			$TextKonserven[]=array('Register','bem_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]);
			$RegisterTexte = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

			$RegisterBeschriftung[$Seite][0]=(isset($RegisterTexte['Register']['reg_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]])?$RegisterTexte['Register']['reg_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]]:$rsRegisterSeiten["XRG_BESCHRIFTUNG"][$RegAnzahl]);
			$RegisterBeschriftung[$Seite][1]=$rsRegisterSeiten["XRG_AKTION"][$RegAnzahl];
			$RegisterBeschriftung[$Seite][2]=(isset($RegisterTexte['Register']['bem_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]])?$RegisterTexte['Register']['bem_'.$rsRegisterSeiten["XRG_ID"][$RegAnzahl]]:$rsRegisterSeiten["XRG_BEMERKUNG"][$RegAnzahl]);
			$RegisterBeschriftung[$Seite][3]=$rsRegisterSeiten["XRG_SEITE"][$RegAnzahl];
			$RegisterBeschriftung[$Seite++][4]=$rsRegisterSeiten["XRG_BEZEICHNUNG"][$RegAnzahl];
		}
	}

			// Erste Seite einblenden
	if($cmdAktion=="Suche" or $cmdAktion=="")
	{
		print awis_RegisterAufbauen($Seite, $RegisterBeschriftung, 1);
		if(is_file($RegisterBeschriftung[0][3]))
		{
			include($RegisterBeschriftung[0][3]);
		}
		else
		{
			awis_Debug(1,'Kann Datei '.$RegisterBeschriftung[0][3].' nicht finden.',$RegisterBeschriftung);
		}
	}
	else		// weitere Seiten aufbauen
	{
		for($i=0;$i<$Seite;$i++)
		{
			if($cmdAktion==$RegisterBeschriftung[$i][4])
			{
				print awis_RegisterAufbauen($Seite, $RegisterBeschriftung, $i+1);
				if(isset($RegisterBeschriftung["XRG_SEITE"][$i]) AND strpos($RegisterBeschriftung["XRG_SEITE"][$i],"?"))
				{
					$incSeite=$RegisterBeschriftung["XRG_SEITE"][$i] . "&" . $_SERVER['QUERY_STRING'];
				}
				else
				{
					$incSeite=$RegisterBeschriftung[$i][3];
				}

				include($incSeite);
				break;
			}
		}
		if($i>$Seite)		// Keine Seite gefunden
		{
			print "<!-- FALSCHE BESCHRIFTUNG: $cmdAktion-->";
		}

		if($Seite==0)
		{
			print awis_RegisterAufbauen($Seite, $RegisterBeschriftung, 1);
			if(!is_file($RegisterBeschriftung[0][3]))
			{
				echo 'FEHLER: Kann Datei #'.$RegisterBeschriftung[0][3].'# nicht finden.';
			}
			else
			{
				include($RegisterBeschriftung[0][3]);
			}
		}
	}

	print awis_RegisterAbschliessen();
}

/**
*
*	awis_format()
*
*	@desc Formatiert Ausdräcke
*	Zielformate:
* 						Currency
* 						SortDatum
* 						Standardzahl
* 						Prozent
* 						US-Zahl
* 						OracleDatum
						Datum
						DatumsWert
*
*	@author Sacha Kerres
*	@param mixed $Ausdruck
*	@param string $Format
*	@param string $KommaZeichen
*	@return string
*	@version 1.0
*
*****************************************************************************************************/
function awis_format($Ausdruck, $Format, $KommaZeichen = ',')
{
	$Erg = '';
	$NK = 0;

	if($Format=='Currency')
	{
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,2,',','.') . '&nbsp;&euro;';
	}
	elseif($Format=='SortDatum')
	{
		$Erg = substr($Ausdruck,6) . substr($Ausdruck,3,2) . substr($Ausdruck,0,2);
	}
	elseif(substr($Format,0,12)=='Standardzahl')
	{
		$NK = substr($Format,13,9);
		if($NK=='')
		{
			$NK=2;
		}
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,$NK,',','.') . '';
		$Erg = str_replace("-","&minus;" , $Erg );
	}
	elseif(substr($Format,0,7)=='Prozent')
	{
		$NK = substr($Format,8,9);
		if($NK=='')
		{
			$NK=2;
		}
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,$NK,',','.') . '%';
	}
	elseif(substr($Format,0,7)=='US-Zahl')
	{
		$NK = substr($Format,8,9);
		if($NK=='')
		{
			$NK=2;
		}
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,$NK,'.','') . '';
	}
	elseif($Format=='OracleDatum')
	{
		if($Ausdruck!='')
		{
			$DatText = str_replace('-','/',$Ausdruck);
			$DatText = str_replace(',','/',$DatText);
			$DatText = str_replace('.','/',$DatText);
			$Dat = explode('/',str_replace('.','/',$DatText));
			if($Dat[2]=='')
			{
				$Dat[2]=date('Y');
			}
			elseif($Dat[2]>38 AND $Dat[2]<70)
			{
				$Dat[2]='20'.$Dat[2];
			}
			$Datum = mktime(0,0,0,$Dat[1],$Dat[0],$Dat[2]);
			if($Erg>-1)		// Fehlerhafte Umwandlung
			{
				$Erg = date('d.m.Y',$Datum);
			}
		}
		else 	// Leeres Datum zuräckgeben
		{
			$Erg = '';
		}

	}
	elseif($Format=='DatumsWert')
	{
		if($Ausdruck!='')
		{
			$DatText = str_replace('-','/',$Ausdruck);
			$DatText = str_replace(',','/',$DatText);
			$DatText = str_replace('.','/',$DatText);
			$Dat = explode('/',str_replace('.','/',$DatText));

			if($Dat[2]=='')
			{
				$Dat[2]=date('Y');
			}
			elseif($Dat[2]>38 AND $Dat[2]<70)
			{
				$Dat[2]='20'.$Dat[2];
			}
			$Erg = mktime(0,0,0,$Dat[1],$Dat[0],$Dat[2]);
		}
		else 	// Leeres Datum zuräckgeben
		{
			$Erg = 0;
		}
	}
	elseif($Format=='Datum')
	{
		if($Ausdruck!='')
		{
			$DatText = str_replace('-','/',$Ausdruck);
			$DatText = str_replace(',','/',$DatText);
			$DatText = str_replace('.','/',$DatText);
			$Dat = explode('/',str_replace('.','/',$DatText));

			if($Dat[2]=='')
			{
				$Dat[2]=date('Y');
			}
			elseif($Dat[2]>38 and strlen($Dat[2])==2)
			{
				$Dat[2]='19'.$Dat[2];
			}
			elseif(strlen($Dat[2])==2)
			{
				$Dat[2]='20'.$Dat[2];
			}

			$Erg = substr('00'.$Dat[0],-2).'.'.substr('00'.$Dat[1],-2).'.'.$Dat[2];
		}
		else 	// Leeres Datum zuräckgeben
		{
			$Erg = '';
		}
	}
	else
	{
		$Erg = $Ausdruck;
	}

	return $Erg;

}

/**
*
* awis_FeldNamenListe()
*
* 	@desc Funktion fällt eine Liste mit Feldnamen mit den Feldbezeichnungen
*
*	@author Sacha Kerres
*	@param ressource $con
*	@param string $FeldListe
*	@param int $SatzNr=1
*	@return array
*	@version 1.0

******************************************************************************************/
function awis_FeldNamenListe($con, &$FeldListe, $SatzNr = 1)
{
	global $awisRSZeilen;

	$AnzahlFelder = sizeof($FeldListe);

	for($i=0;$i<=$AnzahlFelder;$i++)
	{
		$SQL .= "', '"	. $FeldListe[$i];
	}
	$BindeVariablen=array();
	$BindeVariablen['var_N0_xfn_satznr']=$SatzNr;
	$SQL = "SELECT * FROM AWIS.FeldNamen WHERE XFN_FELD IN(" . substr($SQL,2) . "') AND XFN_SATZNR=:var_N0_xfn_satznr";
	$rsNamen = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

	for($j=0;$j<=$AnzahlFelder;$j++)
	{
		for($i=0;$i<=$awisRSZeilen;$i++)
		{
			if($rsNamen["XFN_FELD"][$i] == $FeldListe[$j])		// Name gefunden
			{
				$Arr[$FeldListe[$j]] = array(($rsNamen["XFN_BEZEICHNUNG"][$i]==''?$FeldListe[$j]:$rsNamen["XFN_BEZEICHNUNG"][$i]),$rsNamen["XFN_BEMERKUNG"][$i]);
				break;
			}
		}
		if($i>$awisRSZeilen)	// Nichts gefunden
		{
			$Arr[$FeldListe[$j]] = array($FeldListe[$j],'');
		}
	}

	return $Arr;
}

/**
*
* awisLagerText()
*
* 	Liefert den Namen des Lagers aus dem Kennzeichen
*
*	@author Sacha Kerres
*	@param string $LagerKennzeichen
*	@return string
*	@version 1.0
*
**************************************************************************************/
function awis_LagerText($LagerKennzeichen)
{
	if($LagerKennzeichen=="N")
	{
		return "Weiden";
	}
	elseif($LagerKennzeichen=="L")
	{
		return "Werl";
	}
	else
	{
		return $LagerKennzeichen;
	}
}

/**
*
* awis_NameInArray
*
* 	Sucht einen Namen in einem Array und liefert den ersten gefundenen Namen zuräck
*
*	@author Sacha Kerres
*	@param array $VarListe
*	@param string $SuchVar
*	@param int $Liste=0
*   @param int $SuchTyp (0=exakt, 1=Anfang)
*	@return string
*	@version 1.0
*
**************************************************************************************/
function awis_NameInArray($VarListe, $SuchVar, $Liste=0, $SucheTyp=0)
{
	$Erg = '';

	foreach(array_keys($VarListe) as $Eintrag)
	{
		if($SucheTyp==0)		// exakt
		{
			if(strstr($Eintrag, $SuchVar))
			{
				if($Liste==0)
				{
					return $Eintrag;
				}
				else
				{
					$Erg .= ';' . $Eintrag;
				}
			}
		}
		elseif($SucheTyp==1)
		{
			if(strstr(substr($Eintrag,0,strlen($SuchVar)), $SuchVar))
			{
				if($Liste==0)
				{
					return $Eintrag;
				}
				else
				{
					$Erg .= ';' . $Eintrag;
				}
			}
		}
	}
	return substr($Erg,1);
}

/**
*
* awis_UserExportDateiName
*
* 	@return string
* 	@desc Liefert einen Dateinamen fär einen Export
*	@param string $Erweiterung
*	@author Sacha Kerres
*	@version 1.0
*	@return string
*
**************************************************************************************/
function awis_UserExportDateiName($Erweiterung)
{
	$ZeichenKette = 'MQIJAHSWERDTYZHBCBASMKNSDJHWUHUOIPLMKBDZCTZSVXNAJAGVZSZCVLIDCGEZLEOJNCENCEUWAGVCDRUIKMNBCVDTUIINFDSEFHHZZFTZCTXEWYRYHTCKVOIZGBVRSDWE';

	$UserID = awisBenutzerID();
	$DateiName = substr(substr($ZeichenKette,ord($UserID[0])) . strtoupper($UserID . '_' . awisBenutzerName()),-25,23);

	for($i=0;$i<strlen($DateiName);$i++)
	{
		$DateiName[$i] = $ZeichenKette[ord($DateiName[$i])-30];
	}

	$DateiName = '/daten/web/export/' . strtolower($DateiName) . $Erweiterung;

	return($DateiName);
}

/**
*
*  awis_Zeitmessung()
*
*    Liefert eine Zeitmessung in Microsekunden fär einen Zeitbereich
*		Startzeit wird mit Parameter gesteuert
*
*   @param int $Modus (0=init, 1=Ausgabe)
*	@return float Zeit
*   @author Sacha Kerres
*	@version 1.0
*
**/
function awis_ZeitMessung($Modus = 0)
{
	static $StartZeit;
	static $EndeZeit;

	switch($Modus)
	{
		case 0:			// 'init'
			$StartZeit= explode(' ',microtime());
			break;
		case 1:			// 'erg':
			$EndeZeit= explode(' ',microtime());
			return (((real)$EndeZeit[1]+(real)($EndeZeit[0]))-((real)$StartZeit[1]+(real)($StartZeit[0])));
			break;
	}
}

/**
*
* Funktion erzeugt eine DEBUG Ausgabe
*
*  @author Sacha Kerres
*  @param int $Methode (1=var_dump,2=print_r,3=echo,10=var_dump+html,11=print_r+html,12=echo+html, Negative Werte: Ausrliche Serverinfo)
*  @param mixed Werte
*  @version 1.0
**********************************************************************************/
function awis_Debug($Methode=1,$Werte='')
{
	global $AWISBenutzer;

	$Start = 1;
	$ServerInfo = 0;
	if($Methode==0)
	{
		$Methode = 1;
	}

	echo '<div class=AWISDebugFenster>';
	if($Methode<0)
	{
		$ServerInfo=1;
		$Methode=-$Methode;
	}

	if(!is_int($Methode))
	{
		$Methode=0;
		$Start = 0;
	}

	if(strcasecmp($AWISBenutzer->BenutzerName(),'entwick')==0
		OR strcasecmp($AWISBenutzer->BenutzerName(),'schmidt_p')==0
		OR strcasecmp($AWISBenutzer->BenutzerName(),'pschmidt')==0
		OR strcasecmp($AWISBenutzer->BenutzerName(),'riedl_t')==0
		OR strcasecmp($AWISBenutzer->BenutzerName(),'argauer_c')==0
		OR strcasecmp($AWISBenutzer->BenutzerName(),'awis_jobs')==0)
	{
		if($Methode<10)		// Ausgabe im Text
		{
			echo '<hr><table border=1><tr><td id=FeldBez align=center>AWIS-DEBUG</td></tr>';
		}
		else 				// Als Kommentar
		{
			echo "\n<!--";
		}
		for($i=$Start;$i<func_num_args();$i++)
		{
			switch ($Methode)
			{
				case 1:
					echo '<tr><td class=HinweisText>';
					var_dump(func_get_arg($i));
					echo '</td></tr>';
					break;
				case 2:
					echo '<tr><td class=HinweisText>';
					print_r(func_get_arg($i));
					echo '</td></tr>';
					break;
				case 3:
					echo '<tr><td class=HinweisText>';
					echo func_get_arg($i);
					echo '</td></tr>';
					break;
				case 10:
					echo "\n";
					var_dump(func_get_arg($i));
					echo "\n";
					break;
				case 11:
					echo "\n";
					print_r(func_get_arg($i));
					echo "\n";
					break;
				case 12:
					echo "\n";
					echo func_get_arg($i);
					echo "\n";
					break;
			}
		}
		if($Methode<10)
		{
			if($ServerInfo==1)
			{
				echo '</table><hr><table border=1><tr>';
				echo '<tr><td id=FeldBez>Datei:</td><td id=FeldBez>' . $_SERVER["PATH_TRANSLATED"] . '</td></tr>';
				echo '<tr><td id=FeldBez>PHP-Version:</td><td id=FeldBez>' . phpversion() . '</td></tr>';
				echo '<tr><td id=FeldBez>Include-Pfad:</td><td id=FeldBez>' . ini_get('include_path') . '</td></tr>';
				$con = awisLogon();
				echo '<tr><td id=FeldBez valign=top>Oracle-version:</td><td id=FeldBez>' . ociserverversion($con) . '</td></tr>';
				awisLogoff($con);
				echo '<tr><td id=FeldBez>db.inc.php:</td><td id=FeldBez>' . strftime('%d.%m.%Y %T',filectime('/daten/include/db.inc.php')) . '</td></tr>';
				echo '<tr><td id=FeldBez>register.inc.php:</td><td id=FeldBez>' . strftime('%d.%m.%Y %T',filectime('/daten/include/register.inc.php')) . '</td></tr>';
			}
			echo '</table><hr>';
		}
		else
		{
			echo "-->\n";
		}

		echo '</div>';
	}
}

/**
 * Setzt die Statuszeile, z.B. fär Links
 *
 * @param text $TextMouseOver
 * @param text $TextMouseOut
 */
function awis_StatusZeile($TextMouseOver='', $TextMouseOut='')
{
	return ' onmouseover="window.status=\''.$TextMouseOver.'\';return true;" onmouseout="window.status=\''.$TextMouseOut.'\';return true;"';
}



/**
 * Lädt eine einzelne Textkonserve
 *
 * @param Connection $con
 * @param string $Kennung
 * @param string $Bereich
 * @param string $Sprache = 'DE'
 * @return string
 */
function awis_TextKonserve($con, $Kennung, $Bereich, $Sprache='DE')
{
	global $awisRSZeilen;
	$Text = '';

	$BindeVariablen=array();
	$BindeVariablen['var_T_xtx_kennung']=$Kennung;
	$BindeVariablen['var_T_xtx_bereich']=$Bereich;
	$rsText = awisOpenRecordset($con,'SELECT XTX_TEXT_'.$Sprache.' FROM Textkonserven WHERE XTX_Kennung=:var_T_xtx_kennung AND XTX_Bereich=:var_T_xtx_bereich', true, false, $BindeVariablen);

	if($awisRSZeilen==0)
	{
		$Text = '##'.$Kennung.'##';
	}
	elseif($rsText['XTX_TEXT_'.$Sprache][0]=='')
	{
		$Text = '~~' . $rsText['XTX_TEXT_DE'][0] . '~~';
	}
	else
	{
		$Text = '' . $rsText['XTX_TEXT_'.$Sprache][0] . '';
	}

	return($Text);
}

/**
 * Lädt mehrere Textkonserven in ein Array und gibt dies zurück.
 *
 * @param Connection $con
 * @param array $Textkonserven[Bereich][Kennung]
 * @param string $Sprache
 * @return array
 */
function awis_LadeTextKonserven($con, $Textkonserven, $Sprache)
{
	global $awisRSZeilen;
	$Erg = array();

	$SQL = 'SELECT XTX_BEREICH, XTX_KENNUNG, ';
	$SQL .= ' CASE WHEN XTX_TEXT_'.$Sprache.' IS NULL THEN XTX_TEXT_DE ELSE XTX_TEXT_'.$Sprache.' END AS XTX_TEXT ';
	$SQL .= ' FROM Textkonserven WHERE ';
	$Bedingung = '';
	foreach($Textkonserven as $Konserve)
	{
		$Bedingung .= ' OR (XTX_BEREICH=\'' . $Konserve[0] . '\'';
		if(strstr($Konserve[1],'%')!==false)
		{
			$Bedingung .= ' AND XTX_KENNUNG LIKE \'' . $Konserve[1] . '\')';
		}
		else
		{
			$Bedingung .= ' AND XTX_KENNUNG=\'' . $Konserve[1] . '\')';
		}
	}
	$SQL .= substr($Bedingung,3);

	$rsText = awisOpenRecordset($con,$SQL);
	$rsTextZeilen = $awisRSZeilen;
	for($TextZeile=0;$TextZeile<$rsTextZeilen;$TextZeile++)
	{
		$Erg[$rsText['XTX_BEREICH'][$TextZeile]][$rsText['XTX_KENNUNG'][$TextZeile]]=$rsText['XTX_TEXT'][$TextZeile];
	}

	return($Erg);
}

?>