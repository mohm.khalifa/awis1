<?php
global $con;
global $Recht10001;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZAI','%');
$TextKonserven[]=array('AID','AID_BEZEICHNUNG');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','KeineDatenVorhanden');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10001 = awisBenutzerRecht($con,10001,$AWISBenutzer->BenutzerName());		// Artikelstamm
if(($Recht10001&1)==0)
{
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	die();
}

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$SQL = 'SELECT ZukaufLieferantenArtikelIds.*, AID_BEZEICHNUNG';
$SQL .= ' FROM ZukaufLieferantenArtikelIds ';
$SQL .= ' INNER JOIN ArtikelStammIdTypen ON AID_NR = ZAI_AID_NR';
$SQL .= ' WHERE ZAI_ZLA_KEY=0'.$AWIS_KEY1;

if(!isset($_GET['SSort']))
{
	$SQL .= ' ORDER BY AID_BEZEICHNUNG, ZAI_ID';
}
else 
{
	$SortFelder = explode(';',$_GET['SSort']);
	$OrderBy = '';
	foreach($SortFelder AS $SortFeld)
	{
		$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
	}
	$SQL .= ($OrderBy==''?'':' ORDER BY '.$OrderBy);
}

$rsZAI = awisOpenRecordset($con, $SQL);
$rsZAIZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

if($rsZAIZeilen>0)
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZAI_ID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_ID'],250,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=AID_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='AID_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AID']['AID_BEZEICHNUNG'],250,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZAI_USER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_USER'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_USER'],150,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&SSort=ZAI_USERDAT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_USERDAT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_USERDAT'],100,'',$Link);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZAIZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZAIZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	
	for($ZAIZeile=$StartZeile;$ZAIZeile<$rsZAIZeilen and $ZAIZeile<$StartZeile+$MaxDSAnzahl;$ZAIZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('#ZAI_ID',$rsZAI['ZAI_ID'][$ZAIZeile],0,250,false,($ZAIZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#AID_BEZEICHNUNG',$rsZAI['AID_BEZEICHNUNG'][$ZAIZeile],0,250,false,($ZAIZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#ZAI_USER',$rsZAI['ZAI_USER'][$ZAIZeile],0,150,false,($ZAIZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#ZAI_USERDAT',$rsZAI['ZAI_USERDAT'][$ZAIZeile],0,100,false,($ZAIZeile%2),'','','D');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 
{
	awis_FORM_Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
}

awis_FORM_FormularEnde();

?>