<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('FEG','FEG_ENTFERNUNG');
	$TextKonserven[]=array('Wort','Eroeffnung');
	$TextKonserven[]=array('Wort','Umzug');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('FIK','FIK_BEZEICHNUNG');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);
	if(($Recht5001&8)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILMBW'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $SQL = 'SELECT DATEN.* ';
    $SQL .= ',(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND ROWNUM = 1) AS EROEFFNUNG';
    $SQL .= ',(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 104 AND FIF_FIL_ID=FEG_FIL_ID AND ROWNUM = 1) AS UMZUG';
    $SQL .= ',(SELECT listagg(fik_bezeichnung,\', \') within group (order by fik_bezeichnung) FROM filialinfos INNER JOIN filialkompetenzentypen ON filialinfos.fif_wert = filialkompetenzentypen.fik_key WHERE fif_fit_id = 920 AND fif_fil_id = feg_fil_id) AS fik_bez';
    $SQL .= ' FROM(';
    $SQL .= 'SELECT CASE WHEN FEG_FIL_ID_VON = ' . $DB->WertSetzen('FEG', 'N0', $AWIS_KEY1) . ' THEN FEG_FIL_ID_NACH';
    $SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
    $SQL .= ',FIL_BEZ,FIL_STRASSE,FIL_PLZ,FIL_ORT,FIL_LAGERKZ';
    $SQL .= ' FROM FILIALENENTFERNUNGEN';
    $SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = ' . $DB->WertSetzen('FEG', 'N0', $AWIS_KEY1) . ' THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID AND FIL_ID < 900';
    $SQL .= ' WHERE FEG_FIL_ID_VON = ' . $DB->WertSetzen('FEG', 'N0', $AWIS_KEY1);
    $SQL .= ' OR FEG_FIL_ID_NACH = ' . $DB->WertSetzen('FEG', 'N0', $AWIS_KEY1);
    $SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
    $SQL .= ') DATEN ';
    $SQL .= ' WHERE ROWNUM <= ' . ($AWISBenutzer->ParameterLesen('FilialNachbarn'));
    $SQL .= ' AND (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND FIF_IMQ_ID = 32 AND ROWNUM = 1) IS NOT NULL';

    try {
        $rsFEG = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FEG'));
    } catch (Exception $exception) {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201811120854");
    }

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ID'],60);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],200);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_STRASSE'],200);
	//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_PLZ'],60);

	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ORT'],100);
	//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Eroeffnung'],100);
	//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Umzug'],120);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_LAGERKZ'],50);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEG']['FEG_ENTFERNUNG'],50);
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIK']['FIK_BEZEICHNUNG'],500);

	$Form->ZeileEnde();

	$FEGZeile=0;
	while(!$rsFEG->EOF())
	{
		$Form->ZeileStart();
		$Link = './filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsFEG->FeldInhalt('FEG_FIL_ID');
		$Form->Erstelle_ListenFeld('*FIL_ID',$rsFEG->FeldInhalt('FEG_FIL_ID'),0,60,false,($FEGZeile%2),'',$Link);
		$Form->Erstelle_ListenFeld('*FIL_BEZ',$rsFEG->FeldInhalt('FIL_BEZ'),0,200,false,($FEGZeile%2),'','','T');
		$Form->Erstelle_ListenFeld('*FIL_STRASSE',$rsFEG->FeldInhalt('FIL_STRASSE'),0,200,false,($FEGZeile%2),'','','T');
		$Form->Erstelle_ListenFeld('*FIL_ORT',$rsFEG->FeldInhalt('FIL_ORT'),0,100,false,($FEGZeile%2),'','','T');
		$Form->Erstelle_ListenFeld('*FIL_LAGERKZ',$rsFEG->FeldInhalt('FIL_LAGERKZ'),0,50,false,($FEGZeile%2),'','','T');
		$Form->Erstelle_ListenFeld('*FEG_ENTFERNUNG',$Form->Format('N1',$rsFEG->FeldInhalt('FEG_ENTFERNUNG')),0,50,false,($FEGZeile%2),'','','T');
        $Form->Erstelle_ListenFeld('*FIK_BEZ',$rsFEG->FeldInhalt('FIK_BEZ'),0,500,false,($FEGZeile%2),'','','T');

		$Form->ZeileEnde();

		$FEGZeile++;
		$rsFEG->DSWeiter();
	}

	$Form->Formular_Ende();
}
catch (Exception $ex)
{

}
?>