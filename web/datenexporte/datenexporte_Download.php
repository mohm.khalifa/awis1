<?php
global $AWISBenutzer;

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('XAB','%');
    $TextKonserven[]=array('Wort','lbl_speichern');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht4001=$AWISBenutzer->HatDasRecht(4001);
    if($Recht4001==0)
    {
        awisEreignis(3,1000,'XDA',$AWISBenutzer->BenutzerName(),'','','');
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    if(isset($_GET['Del'])) {
        include './datenexporte_loeschen.php';
    }elseif(isset($_POST['cmdSpeichern_x'])){
        $AWISBenutzer->ParameterSchreiben('DatenexporteDownloadsLoeschZeitraum', $_POST['txtDatenexporteDownloadsLoeschZeitraum']);
    }

    $SQL = 'SELECT c.xde_bezeichnung, a.XAB_BEZEICHNUNG, b.xab_xbn_key, a.xab_key';
    $SQL .= ' FROM ExportAbfragen a INNER JOIN';
    $SQL .= ' (';
    $SQL .= ' SELECT xab_key, xab_xbn_key';
    $SQL .= ' FROM ExportAbfragen';
    $SQL .= ' WHERE xab_xbn_key = ' . $DB->WertSetzen('XAB','N0', $AWISBenutzer->BenutzerID());
    $SQL .= ' UNION ALL';
    $SQL .= ' SELECT xan_xab_key, xan_xbn_key';
    $SQL .= ' FROM ExportAbfragenBenutzer';
    $SQL .= ' WHERE xan_xbn_key = ' . $DB->WertSetzen('XAB','N0', $AWISBenutzer->BenutzerID());
    $SQL .= ' ) b';
    $SQL .= ' ON a.xab_key = b.xab_key';
    $SQL .= ' INNER JOIN ExportDaten c';
    $SQL .= ' ON a.xab_xde_key = c.xde_key';

    $rsXAB = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('XAB'));
    $Form->DebugAusgabe($DB->LetzterSQL());

    $pfad = '/daten/web/dokumente/datenexport/';


    $Form->Formular_Start();
    $Form->SchreibeHTMLCode('<form name="frmDatenexportdownload" method="post" action="./datenexporte_Main.php?cmdAktion=Download'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'"');


    $AnzTage = $AWISBenutzer->ParameterLesen('DatenexporteDownloadsLoeschZeitraum',true);

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_AUTOMATISCH_LOESCHEN'],390,'font-weight:bolder');
    $Daten = explode('|',$AWISSprachKonserven['XAB']['XAB_LST_ANZ_TAGE']);
    $Form->Erstelle_SelectFeld('DatenexporteDownloadsLoeschZeitraum',$AnzTage,50,true,'','','30','','',$Daten);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_TAGE'],50 ,'font-weight:bolder');
    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S','',20,20);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    if($rsXAB->AnzahlDatensaetze() > 0){
        $Icons[] = array('del','./datenexporte_Main.php?cmdAktion=Download&XDZ_KEY=-1');
        $Form->Erstelle_ListeIcons($Icons,38,-1);

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('Datenexport', 440);
        $Form->Erstelle_Liste_Ueberschrift('Abfragenname', 400);
        $Form->Erstelle_Liste_Ueberschrift('Dateiname', 400);
        $Form->Erstelle_Liste_Ueberschrift('Datum', 200);
        $Form->ZeileEnde();

        $DS=0;
        while (!$rsXAB->EOF()) {
            if(file_exists($pfad . $rsXAB->FeldInhalt('XAB_KEY') . '/')) {
                $dir = opendir($pfad . $rsXAB->FeldInhalt('XAB_KEY') . '/');
                while (($file = readdir($dir)) !== false) {
                    if($file != '.' && $file != '..') {
                        $fileTime = filemtime($pfad . $rsXAB->FeldInhalt('XAB_KEY') . '/' . $file);
                        $fileTime = date("d.m.Y H:i:s", $fileTime);

                        $Form->ZeileStart();
                        $Icons = array();
                        $Icons[] = array('delete','./datenexporte_Main.php?cmdAktion=Download&Del='.$rsXAB->FeldInhalt('XAB_KEY').'&File='.urlencode($file));
                        $Form->Erstelle_ListeIcons($Icons,38,($DS%2));

                        $Form->Erstelle_ListenFeld('XDE_BEZEICHNUNG',$rsXAB->FeldInhalt('XDE_BEZEICHNUNG'),0,400,false,($DS%2),'','','T','L');
                        $Form->Erstelle_ListenFeld('XAB_BEZEICHNUNG',$rsXAB->FeldInhalt('XAB_BEZEICHNUNG'),0,400,false,($DS%2),'','','T','L');
                        $Link = '/dokumentanzeigen.php?dateiname='.urlencode($file).'&erweiterung=csv&bereich=datenexporte&ordner='.$rsXAB->FeldInhalt('XAB_KEY');
                        $Form->Erstelle_ListenFeld('DATEINAME', $file, 0,400,false,($DS%2),'',$Link,'T','L');
                        $Form->Erstelle_ListenFeld('DATEIDATUM', $fileTime, 0,200,false,($DS%2),'','','DU','L');
                        $Form->ZeileEnde();

                        $DS++;
                    }
                }
                closedir($dir);
            }

            $rsXAB->DSWeiter();
        }
    }else{
        $Form->ZeileStart();
        $Form->Hinweistext($AWISSprachKonserven['XAB']['XAB_KEINE_DOWNLOADS']);
        $Form->ZeileEnde();
    }


    $Form->SchaltflaechenStart();
    $Form->SchaltflaechenEnde();
    $Form->Formular_Ende();
    $Form->SchreibeHTMLCode('</form>');



}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(2, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
    }
    else
    {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}