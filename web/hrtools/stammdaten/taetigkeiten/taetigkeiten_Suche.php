<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
	//$TextKonserven[]=array('QMM','*');
	$TextKonserven[]=array('BEW','*');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	


	$Recht39002 = $AWISBenutzer->HatDasRecht(39001);

	if($Recht39002==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

		$Form->SchreibeHTMLCode('<form name="frmHRTsuche" action="./taetigkeiten_Main.php?cmdAktion=Stammdaten" method="POST"  >');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_HRT'));

		$Form->Formular_Start();
		
		/**
		 *  Firmierung
		 */
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_FIRMIERUNG'] . ':',200);
		
		$SQL ='select hrf_key,hrf_firmierung from ';
		$SQL .= '(SELECT hrf_key,hrf_firmierung,hrf_aktiv ';
		$SQL .= 'FROM hrfirmierung ';
		$SQL .=	'union '; 
		$SQL .= 'select -1 as hrf_key,\'nicht zugeordnet\' as hrf_firmierung,1 as hrf_aktiv from dual) ';			
		$SQL .= 'where hrf_aktiv = 1 ';
		$SQL .=' order by hrf_key';
		
		
		$Form->Erstelle_SelectFeld('*BEW_FIRMIERUNG',($Param['SPEICHERN']=='on'?$Param['HRT_FIRMIERUNG']:''),150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','');
		$Form->ZeileEnde();
		
		/**
		 * Tätigkeiten
		 */
		
		$SpKey = '';
		$SpSuch = $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		if($Param['SPEICHERN']=='on' and $Param['HRT_TAETIGKEITEN'] != '')
		{
			$SQL2 = ' select * from hrtaetigkeiten';
			$SQL2 .= ' where hrt_key='.$Param['HRT_TAETIGKEITEN'];
			$SQL2 .= ' and hrt_aktiv=1';
			$rsHRTSp = $DB->RecordSetOeffnen($SQL2);
			if($rsHRTSp->AnzahlDatensaetze() != 0)
			{
				$SpKey = $rsHRTSp->FeldInhalt('HRT_KEY');
				$SpSuch = $rsHRTSp->FeldInhalt('HRT_SUCH');
			}
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_TAETIGKEITEN'].':',180);
		$Form->Erstelle_SelectFeld('*BEW_TAETIGKEITEN',$Param['HRT_TAETIGKEITEN'], 500, true,'***BEW_Daten;sucBEW_TAETIGKEITEN;BEW_FIRMIERUNG=*sucBEW_FIRMIERUNG&ZUSATZ=~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],(($Param['HRT_TAETIGKEITEN'] == '0' or $Param['SPEICHERN']=='')?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']: $SpKey.'~'.$SpSuch),'', '', '');
		$Form->ZeileEnde();
		
		/**
		 *  Aktiv/Inaktiv
		 */
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['HRT_AKTIVSUCH'] . ':',200);
		$Daten = explode('|',$AWISSprachKonserven['BEW']['HRT_LST_AKTIV']);
		$Form->Erstelle_SelectFeld('*BEW_AKTIV',($Param['SPEICHERN']=='on'?$Param['HRT_AKTIV']:''),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		if(($Recht39002&4)==4)
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		
			
		
		
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
		$Form->SchreibeHTMLCode('</form>');
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>