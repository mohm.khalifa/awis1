<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucRST_SUCHE']))
{
	var_dump($_GET);
	die();
}

try
{
	$DS=0;

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$Suchbegriffe = explode(',',$_GET['sucRST_SUCHE']);
	
	$SQL = 'SELECT';
	$SQL .= ' RST_KEY';
	$SQL .= ' ,REH_BEZEICHNUNG';
	$SQL .= ' ,RST_LIE_NR';
	$SQL .= ' ,RST_LARTNR';
	$SQL .= ' ,RST_BEZEICHNUNG';
	$SQL .= ' ,RST_BREITE';
	$SQL .= ' ,RST_QUERSCHNITT';
	$SQL .= ' ,RST_BAUART';
	$SQL .= ' ,RST_INNENDURCHMESSER';
	$SQL .= ' ,RST_LOADINDEX';
	$SQL .= ' ,RST_SPEEDINDEX';
	$SQL .= ' ,RRP_ATUNR';
	$SQL .= ' FROM REIFENSTAMM';
	$SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY=RST_REH_KEY';
	$SQL .= ' LEFT JOIN REIFENPFLEGE ON RST_KEY=RRP_RST_KEY';
	
	$Bedingung = ' AND RRP_KEY IS NULL'; //Nur nicht verkn�pfte Reifen
	
	//Bezeichnung
	if(isset($Suchbegriffe[0]) AND $Suchbegriffe[0]!='')
	{
		$Bedingung .= ' AND UPPER(REH_BEZEICHNUNG) '.$DB->LikeOderIst($Suchbegriffe[0].'*',awisDatenbank::AWIS_LIKE_UPPER);
	}
	
	//Breite
	if(isset($Suchbegriffe[1]) AND $Suchbegriffe[1]!='')
	{
		$Bedingung .= ' AND RST_BREITE = '.$DB->FeldInhaltFormat('N0',$Suchbegriffe[1]);
	}
	
	//Querschnitt
	if(isset($Suchbegriffe[2]) AND $Suchbegriffe[2]!='')
	{
		$Bedingung .= ' AND RST_QUERSCHNITT = '.$DB->FeldInhaltFormat('T',$Suchbegriffe[2]);
	}
	
	//Bauart
	if(isset($Suchbegriffe[3]) AND $Suchbegriffe[3]!='')
	{
		$Bedingung .= ' AND UPPER(RST_BAUART) = UPPER('.$DB->FeldInhaltFormat('T',$Suchbegriffe[3]).')';
	}

	//Innendurchmesser
	if(isset($Suchbegriffe[4]) AND $Suchbegriffe[4]!='')
	{
		$Bedingung .= ' AND RST_INNENDURCHMESSER = '.$DB->FeldInhaltFormat('T',$Suchbegriffe[4]);
	}
	
	
	//Bezeichnung
	if(isset($Suchbegriffe[5]) AND $Suchbegriffe[5]!='')
	{
		$Bedingung .= ' AND RST_BEZEICHNUNG '.$DB->LikeOderIst($Suchbegriffe[5].'*',awisDatenbank::AWIS_LIKE_UPPER);
	}
	
	if($Bedingung != '')
	{
		$SQL .= ' WHERE '.substr($Bedingung, 4);
	}
	else 
	{
		die();
	}	
	$rsDaten = $DB->RecordSetOeffnen($SQL);

	$FelderBreiten=array(120,70,140,80,30,30,30,30,30,60,250);
	$FelderAusrichtung=array('L','L','L','L','L');
	$Felder=array('REH_BEZEICHNUNG','RST_LIE_NR','RST_LARTNR','RRP_ATUNR','RST_BREITE','RST_QUERSCHNITT','RST_BAUART','RST_INNENDURCHMESSER','RST_LOADINDEX','RST_SPEEDINDEX','RST_BEZEICHNUNG');
	
	$TextKonserven = array();
	$TextKonserven[] = array('RST','*');
	$TextKonserven[] = array('RRP','*');
	$TextKonserven[] = array('REH','REH*');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift('',20);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'], 120, '', '');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LIE_NR'], 70, '', '');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LARTNR'], 140, '', '');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'], 80, '', '');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['txt_Reifenkennzeichen'],230, '', '');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'],250, '', '');
	$Form->ZeileEnde();
	
	$i=0;
	while(!$rsDaten->EOF())
	{
		$Form->ZeileStart('','Zeile'.$DS);
		echo '<div class='.(($DS%2)==0?'ListenFeldHell':'ListenFeldDunkel').'  style="width: 20px;">';
		echo '<input type=radio name="'.$_GET['Feldname'].'" value="'.$rsDaten->FeldInhalt('RST_KEY').'"';
		if($rsDaten->AnzahlDatensaetze()==1)
		{
			echo ' checked';
		}

		echo ' onchange="sucrst_suche_ZeileMarkieren('.$DS.')"';

		echo '></div>';

		$i=0;
		foreach($Felder AS $Feld)
		{
			echo '<div class='.(($DS%2)==0?'ListenFeldHell':'ListenFeldDunkel').' style="width:'.$FelderBreiten[$i].'px;';

			switch($FelderAusrichtung[$i])
			{
				case 'L':
					echo 'text-align:left;';
					break;
				case 'R':
					echo 'text-align:right;';
					break;
				case 'Z':
					echo 'text-align:center;';
					break;
				default:
					echo 'text-align:right;';
					break;
			}

			echo '">';
			echo $rsDaten->FeldInhalt($Felder[$i]);
			$i++;
			echo '</div>';
		}

		$DS++;
		$Form->ZeileEnde();

		$rsDaten->DSWeiter();
	}
		
}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>