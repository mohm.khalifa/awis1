<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

$Tabelle= '';


if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedr�ckt
{
		$Tabelle = 'SACHBEARBEITERZUORD';
		$Felder = explode(';',$FelderDel);
		$Feld = explode('_',$Felder[1]);
		$Key=$Feld[2];	
		$SQL  = ' Select * from SACHBEARBEITERZUORD sbz ';
		$SQL .= ' inner join Sachbearbeiter sb on sb.id = sbz.sp_key ';
		$SQL .= ' where sbz.id ='.$Key;
		$rsDaten = $DBSCHAD->RecordSetOeffnen($SQL);
			
		
		/**
		 * Werte f�r Frage 'Wirklich l�schen?' holen
		**/
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('BES','BES_SAMMEL'),$rsDaten->FeldInhalt('SBNAME'));

}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'SACHBEARBEITERZUORD':
			$SQL = 'DELETE FROM SACHBEARBEITERZUORD WHERE id='.$_POST['txtKEY']; // Sachbearbeiter Zuord l�schen
			
			if($DBSCHAD->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
			$SQL ='update Sachbearbeiter ';
			$SQL .= 'set SBUSER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', SBUSERDAT=SYSDATE';
			$SQL .=' where ID='.$_POST['txtSB_KEY'];
			$DBSCHAD->Ausfuehren($SQL,'',true);
		break;	
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./sachbearbeiter_Main.php?cmdAktion=Details&ID='.$rsDaten->FeldInhalt('SB_KEY').' method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('SB_KEY',$rsDaten->FeldInhalt('SB_KEY'));
			$Form->Erstelle_HiddenFeld('BES_NAME',$_POST['txtBES_NAME']);
			$Form->Erstelle_HiddenFeld('BES_VORNAME',$_POST['txtBES_VORNAME']);
			$Form->Erstelle_HiddenFeld('BES_KUERZEL',$_POST['txtBES_KUERZEL']);
			$Form->Erstelle_HiddenFeld('BES_GUELTIG',$_POST['txtBES_GUELTIG']);
			$Form->Erstelle_HiddenFeld('BES_AWISUSR',$_POST['txtBES_AWISUSR']);
			if(isset($_POST['txtCHK_AKTIV']))
			{
				$Check = 0;
			}
			else 
			{
				$Check = 1;
			}
			$Form->Erstelle_HiddenFeld('CHK_AKTIV',$Check);
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
				
			$Form->Trennzeile();
				
			/**
			 * L�schen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
				
			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>