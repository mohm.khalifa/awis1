<?php
//**************************************************************
// Liefert die Daten für den Serienbrief des CRM
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');



// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('QMM','%');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$FeldBreiten = array();
$FeldBreiten['Aktion'] = 100;
$FeldBreiten['Alt'] = 170;
$FeldBreiten['Neu'] = 170;
$FeldBreiten['User'] = 150;
$FeldBreiten['Userdat'] = 150;

// Infozeile zusammenbauen
$Felder = array();
$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a title='".$AWISSprachKonserven['QMM']['QMH_HISTCLOSE']."' onclick='closeHist()' target='_self' name='QMH_HISTCLOSE'><img border=0 src=/bilder/icon_delete.png></a>");
$Form->InfoZeile($Felder,'');

$SQL  = ' Select * from QMPLAENE ';
$SQL .= ' inner join QMKRITERIEN on QMP_QMK_KEY=QMK_KEY ';
$SQL .= 'where QMP_KEY='.$_GET['KEY'];

$rsQMP = $DB->RecordSetOeffnen($SQL);

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_FIL_ID'] . ':',50);
$Form->Erstelle_TextFeld('QMP_FILIALE',$rsQMP->FeldInhalt('QMP_FIL_ID'), 10, 40,false, '','', '','', 'L');
	
$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_KRITERIUM'] . ':',70);
$Form->Erstelle_TextFeld('QMP_KRITERIUM',$rsQMP->FeldInhalt('QMK_LANG'), 10, 250,false, '','', '','', 'L');

$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_ERHEBDAT'] . ':',70);
$Form->Erstelle_TextFeld('QMM_ERHEBDAT',$Form->Format('D',$rsQMP->FeldInhalt('QMP_ERHEBDAT')), 10, 50,false, '','', '','', 'L');


$Form->ZeileEnde();


$SQL  = ' Select * from QMHISTORY ';
$SQL .= 'where QMH_QMP_KEY='.$_GET['KEY'];
$SQL .= ' order by QMH_USERDAT DESC';
	
$rsQMH = $DB->RecordSetOeffnen($SQL);


$Form->ZeileStart('width:100%;');
$Link= '';
// Überschrift der Listenansicht mit Sortierungslink: Filiale
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMH_AKTION'], $FeldBreiten['Aktion'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMH_ALT'], $FeldBreiten['Alt'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMH_NEU'], $FeldBreiten['Neu'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMH_USER'], $FeldBreiten['User'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMH_USERDAT'], $FeldBreiten['Userdat'], '', $Link);
$Form->ZeileEnde();
$DS2 = 0;
if($rsQMH->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('<div id=srcHist style="width: 100%; height: 180px;  overflow-y: scroll;">');
}
while(! $rsQMH->EOF())
{
	$Form->ZeileStart('width:100%');
	$TTT = $rsQMH->FeldInhalt('QMH_AKTION');
	$Form->Erstelle_ListenFeld('QMH_AKTION', $rsQMH->FeldInhalt('QMH_AKTION'), 0, $FeldBreiten['Aktion'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsQMH->FeldInhalt('QMH_ALT');
	$Form->Erstelle_ListenFeld('QMH_ALT',strlen($rsQMH->FeldInhalt('QMH_ALT')) > 20 ? substr($rsQMH->FeldInhalt('QMH_ALT'),0,17).'...':$rsQMH->FeldInhalt('QMH_ALT'), 0, $FeldBreiten['Alt'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsQMH->FeldInhalt('QMH_NEU');
	$Form->Erstelle_ListenFeld('QMH_NEU',strlen($rsQMH->FeldInhalt('QMH_NEU')) > 20 ? substr($rsQMH->FeldInhalt('QMH_NEU'),0,17).'...':$rsQMH->FeldInhalt('QMH_NEU'), 0, $FeldBreiten['Neu'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsQMH->FeldInhalt('QMH_USER');
	$Form->Erstelle_ListenFeld('QMH_USER',$rsQMH->FeldInhalt('QMH_USER'), 0, $FeldBreiten['User'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsQMH->FeldInhalt('QMH_USERDAT');
	$Form->Erstelle_ListenFeld('QMH_USERDAT',$rsQMH->FeldInhalt('QMH_USERDAT'), 0, $FeldBreiten['Userdat'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$Form->ZeileEnde();
	$DS2++;
	$rsQMH->DSWeiter();
}
if($rsQMH->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('</div>') ;
}

?>