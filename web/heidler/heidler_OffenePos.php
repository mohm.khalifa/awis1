<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    $Script = '<script>
        $(document).ready(function() {
            setTimeout(refreshDaten(), 10000);
        });
    
        function refreshDaten() {
            heidler_aktualisierung(this);
            setTimeout(refreshDaten, 10000);
        }
    </script>';

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('HEIDLER', '%');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_nox');
    $TextKonserven[] = array('Wort', 'lbl_dpd');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $AWISBenutzer = awisBenutzer::Init();

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $HeidlerFunctions = new heidler_funktionen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht45000 = $AWISBenutzer->HatDasRecht(45000);

    if ($Recht45000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (isset($_POST['cmdLoeschen_x'])) {
        require_once 'heidler_OffenePos_loeschen.php';
    }

    if (isset($_POST['cmdNOX_x'])) {
        $HeidlerFunctions->SwitchVersender(1); // 1 = NOX
    }

    if (isset($_POST['cmdDPD_x'])) {
        $HeidlerFunctions->SwitchVersender(2); // 2 = DPD
    }

    $Param = array();
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Heidler'));

    if (isset($_GET['Sort']) and $_GET['Sort'] != '') {        // wenn GET-Sort, dann nach diesen Feld sortieren
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    }

    if (isset($_REQUEST['Block']) and $_REQUEST['Block'] != '') {
        $Param['BLOCK'] = $_REQUEST['Block'];
    }

    $Form->SchreibeHTMLCode($Script);
    $Form->SchreibeHTMLCode("<form name=frmOffenePos method=post action=./heidler_Main.php?cmdAktion=OffenePos>");

    $Form->Formular_Start();
    $Form->Erstelle_HiddenFeld('Block', (isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')?$_REQUEST['Block']:1);
    $Form->FormularBereichStart();
    $Form->FormularBereichInhaltStart($AWISSprachKonserven['HEIDLER']['OFFENEPOS'], true, 10, 'heidler_aktualisierung', 'box1', false, ['txtBlock'], false);
    $Form->AuswahlBoxHuelle('box1');
    $Form->FormularBereichInhaltEnde();
    $Form->FormularBereichInhaltStart($AWISSprachKonserven['HEIDLER']['ALTDATEN']);
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'], 200);
    $Form->Erstelle_TextFeld('!ALTDATUM', '', 200, 200, true, '', '', '', 'D');
    $Form->ZeileEnde();
    $Form->FormularBereichInhaltEnde();
    $Form->FormularBereichEnde();
    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
    if (($Recht45000&64) == 64) {
        $Form->Schaltflaeche('image', 'cmdNOX', '', '/bilder/cmd_nox.png', $AWISSprachKonserven['Wort']['lbl_nox'], 'N','','','','',true);
        $Form->Schaltflaeche('image', 'cmdDPD', '', '/bilder/cmd_dpd.png', $AWISSprachKonserven['Wort']['lbl_dpd'], 'D','','','','',true);
    }
    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {

    if ($Form instanceof awisFormular) {

        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;
    global $Form;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Bedingung = '';

    if (isset($Param['LAGER']) AND $Param['LAGER'] != '') {
        $Bedingung .= 'AND LAGERKZ ' . $DB->LikeOderIst($Param['LAGER']) . ' ';
    }
    if (isset($Param['KNDNR']) AND $Param['KNDNR'] != '') {
        $Bedingung .= 'AND KNDNR ' . $DB->LikeOderIst($Param['KNDNR']) . ' ';
    }
    if (isset($Param['EMPF_LKZ']) AND $Param['EMPF_LKZ'] != '') {
        $Bedingung .= 'AND EMPF_LKZ ' . $DB->LikeOderIst($Param['EMPF_LKZ']) . ' ';
    }
    if (isset($Param['TRACKING_NR']) AND $Param['TRACKING_NR'] != '') {
        $Bedingung .= 'AND TRACKING_NR ' . $DB->LikeOderIst($Param['TRACKING_NR']) . ' ';
    }
    if (isset($Param['ATU_NR']) AND $Param['ATU_NR'] != '') {
        $Bedingung .= 'AND ATU_NR ' . $DB->LikeOderIst($Param['ATU_NR']) . ' ';
    }
    if (isset($Param['DATUM_KOMMISSIONIERUNG']) AND $Param['DATUM_KOMMISSIONIERUNG'] != '') {
        $Bedingung .= 'AND DATUM_KOMMISSIONIERUNG LIKE \'' . $Param['DATUM_KOMMISSIONIERUNG'] . '%\'';
    }

    return $Bedingung;
}

?>