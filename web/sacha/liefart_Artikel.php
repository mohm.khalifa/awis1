<?
/****************************************************************************************************
* 
* 	Artikel-Liste f�r Lieferantenartikel
* 
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt. 2003
* 
****************************************************************************************************/
// Variablen
global $PHP_AUTH_USER;	// Anmeldename
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff

$LARRechtestufe = awisBenutzerRecht($con, 402) ;

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

		//********************************************************************
		// Aktualisieren gew�hlt?
		// Block muss identisch sein zu artikel_Artikel.php !!!!
		//********************************************************************
if(isset($HTTP_POST_VARS['cmdSuche_x']))
{
	$Param = $HTTP_POST_VARS['txtAuswahlSpeichern'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_ATUNR'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_Exakt'];		// Exakte Suche
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
	
	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $PHP_AUTH_USER , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $PHP_AUTH_USER, "");
	
	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $PHP_AUTH_USER));
    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerLAR", $PHP_AUTH_USER));
}

	// SQL Anweisung erstellen
$SQL = "SELECT DISTINCT LAR_KEY, LAR_LARTNR, LIE_NAME1, LAR_BEZWW, LAR_REKLAMATIONSKENNUNG";
$TabellenListe = " FROM AWIS.Lieferantenartikel, AWIS.Lieferanten ";

$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($HTTP_GET_VARS["LARKEY"]) OR $Artikel[0]!='')
{
	if(isset($HTTP_GET_VARS["LARKEY"]))
	{
		$LARKEY = $HTTP_GET_VARS["LARKEY"];
//		awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $PHP_AUTH_USER, $LARKEY);
	}
	else
	{
		$LARKEY=$Artikel[0];
	}
	$Bedingung = " AND LAR_KEY = " . $LARKEY;

//**********************************************************
// Daten speichern
//**********************************************************

	if(awis_NameInArray($HTTP_POST_VARS, "cmdSpeichern")!='')
	{

		$SQL = "UPDATE LieferantenArtikel SET LAR_BEMERKUNGEN='" . substr($HTTP_POST_VARS['txtLAR_BEMERKUNGEN'],0,255) . "'";
		$SQL .= ",LAR_ALTERNATIVARTIKEL=" . $HTTP_POST_VARS['txtLAR_ALTERNATIVARTIKEL'];
		$SQL .= ",LAR_ALTELIEFNR=" . $HTTP_POST_VARS['txtLAR_ALTELIEFNR'];
//????		$SQL .= ",LAR_AUSLAUFARTIKEL=" . $HTTP_POST_VARS['txtLAR_AUSLAUFARTIKEL'];
		$SQL .= ",LAR_PROBLEMARTIKEL=" . $HTTP_POST_VARS['txtLAR_PROBLEMARTIKEL'];
		$SQL .= ",LAR_PROBLEMBESCHREIBUNG='" . $HTTP_POST_VARS['txtLAR_PROBLEMBESCHREIBUNG'] . "'";
		$SQL .= ",LAR_GEPRUEFT=TO_DATE('" . $HTTP_POST_VARS['txtLAR_GEPRUEFT'] . "','DD.MM.RRRR')";
		$SQL .= ",LAR_APP_ID=" . $HTTP_POST_VARS['txtLAR_APP_ID'] . "";
		$SQL .= ",LAR_MUSTER=" . $HTTP_POST_VARS['txtLAR_MUSTER'] . "";
		$SQL .= ",LAR_USER='" . $_SERVER['PHP_AUTH_USER'] . "', LAR_USERDAT=SYSDATE";
		$SQL .= " WHERE LAR_KEY=0" . $HTTP_POST_VARS["txtLAR_KEY"];

		$Erg = awisExecute($con, $SQL);
		if($Erg === FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler['message']);		
		}
	}  // Ende Speichern von Daten
}
													//***********************************************************
elseif(!isset($HTTP_GET_VARS["LARKEY"]))			// Artikel suchen (Liste anzeigen, oder einen)
													//***********************************************************
{
	$DSAnz=0;		// Insgesamt gelesene Zeilen

//	var_dump($Param);
	
	if($Param[6]!='')		// LiefNr
	{
		if($Param[7] == 'on')
		{
			$Bedingung .= " AND LAR_LARTNR " . awisLIKEoderIST($Param[3],true) . "";
		}
		else
		{
//			$TabellenListe .= ", TeileInfos ";
//			$Bedingung .= " AND TEI_KEY2 = LAR_KEY AND TEI_SUCH2 " . awisLIKEoderIST($Param[3],True, False, False) . "";
// TODO: Neues Feld in der Maske: LAR_SUCH_LARTNR
			$Bedingung .= " AND SUCHWORT(LAR_LARTNR)" . awisLIKEoderIST($Param[3],true,false,true,false) . "";
		}	
	}
	if($Param[1] != '')		// Artikel-Nummer
	{
		$TabellenListe .= ", TeileInfos ";
		$Bedingung .= " AND (TEI_KEY2 = LAR_KEY AND TEI_KEY1 IN (SELECT AST_KEY FROM AWIS.ArtikelStamm WHERE AST_ATUNR " . awisLIKEoderIST($Param[1],True, False, False) . "))";
	}
	if($Param[9] != '')		// OE-Nummer
	{
		if(strstr($TabellenListe,"TeileInfos")=='')
		{
			$TabellenListe .= ", TeileInfos ";
		}
		$Bedingung .= " AND (TEI_KEY1 = LAR_KEY AND TEI_KEY2 IN (SELECT OEN_KEY FROM AWIS.OENUMMERN WHERE OEN_NUMMER " . awisLIKEoderIST($Param[3],True, False, False) . "))";
	}

	if($Bedingung == '')	// Keine vern�nftige Bedingung
	{
		die('<span class=HinweisText>Diese Suchkombination ist nicht m�glich</span>');
	}

	$SQL .= $TabellenListe . " WHERE LAR_LIE_NR = LIE_NR(+) AND (" . substr($Bedingung,5) . ") ORDER BY LAR_LARTNR";

	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;

	if($rsArtikelZeilen == 1)	
	{
		$DSAnz=-1;			// Einzelanzeige
		$LARKEY = $rsArtikel["LAR_KEY"][0];
	}
	else
	{
		print "<table width=100% border=1>";
		
		print "\n<tr>";	// �berschrift
		print "\n<td id=FeldBez>Artikel-Nummer</td>";
		print "\n<td id=FeldBez>Lieferant</td>";
		print "\n<td id=FeldBez>Bezeichnung</td>";
		print "\n<td id=FeldBez>Rekl.-Kenn.</td>";
		print "</tr>\n";

 		for($LARNr=0;$LARNr<$rsArtikelZeilen;$LARNr++)		
		{
			print "<tr>";

			print "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][$LARNr] . ">";
			print "" . $rsArtikel["LAR_LARTNR"][$LARNr] . "</a></td>";
			print "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LIE_NAME1"][$LARNr] . "</td>";
			print "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LAR_BEZWW"][$LARNr] . "</td>";
			print "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LAR_REKLAMATIONSKENNUNG"][$LARNr] . "</td>";
			
			print "</tr>";		
			if($LARNr==500)
			{
				echo '<tr><td colspan=99><span Class=HinweisText>Das Maximum von ' . $LARNr . ' Zeilen ist erreicht</span></td></tr>';
				break;
			}
		}
		
		print "</table>";
		print "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $DSAnz Artikel gefunden.</font>";

	}
}


/*********************************************************************
* 
* Datensatz l�schen oder zur�cksetzen
* 
**********************************************************************/

if(awis_NameInArray($HTTP_POST_VARS, "cmdLARLoeschen")!='' AND !isset($HTTP_POST_VARS["cmdSpeichernAbbruch"]))
{
	if(isset($HTTP_POST_VARS["cmdSpeichernBestaetigung"]))
	{
		if($HTTP_POST_VARS['txtLAR_BEKANNTWW']=='0')
		{
			$SQL = "DELETE FROM AWIS.LieferantenArtikel WHERE LAR_KEY=" . $HTTP_POST_VARS['txtLAR_KEY'];
		}
		else
		{
			$SQL = "UPDATE LieferantenArtikel SET LAR_USER='LOESCHEN' WHERE LAR_KEY=" . $HTTP_POST_VARS['txtLAR_KEY'];
		}
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);			
			die();
		}
	}
	else
	{
		print "<form name=frmArtikel method=post>";
				
		print "<input type=hidden name=txtLAR_KEY value='" . $HTTP_POST_VARS['txtLAR_KEY'] . "'>";
		print "<input type=hidden name=txtLAR_BEKANNTWW value='" . $HTTP_POST_VARS['txtLAR_BEKANNTWW'] . "'>";
		print "<input type=hidden name=cmdLARLoeschen>";

		print "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Lieferantenartikel " . ($HTTP_POST_VARS['txtLAR_BEKANNTWW']==0?'l�schen':'zur�cksetzen') . " wollen?</span><br><br>";
		print "<input accesskey=j type=submit value=\"Ja, Aktion ausf�hren\" name=cmdSpeichernBestaetigung>";
		print "<input accesskey=n type=submit value=\"Nein, Aktion nicht ausf�hren\" name=cmdSpeichernAbbruch>";
		
		print "</form>";
		awisLogoff($con);		
		die();
	}
}

//********************************************************************
// Einzelner Datensatz -> Formular anzeigen
//********************************************************************
//die("1");

if($DSAnz==-1)
{
	awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $PHP_AUTH_USER, $LARKEY);
	print "<form name=frmLieferantenArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerLAR", $PHP_AUTH_USER));

	$SQL = "SELECT LIEFERANTENARTIKEL.*, LIE_NAME1, ";
	$SQL .= "APP_NAME ";
	$SQL .= " FROM AWIS.LIEFERANTENARTIKEL, AWIS.LIEFERANTEN, AWIS.ARTIKELPRUEFPERSONAL ";
	$SQL .= " WHERE LAR_KEY=0" . $LARKEY;
	$SQL .= " AND LAR_LIE_NR = LIE_NR(+) AND APP_ID(+) = LAR_APP_ID";
	
//print "<hr>$SQL<hr>";
	$rsArtikel = awisOpenRecordset($con, $SQL);
	if($rsArtikel==FALSE)
	{
		awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
		awisLogoff($con);		
		die();
	}

	if($awisRSZeilen==1)		// Nur ein Datensatz gefunden
	{
		print "<table border=0 width=100%>";
		print "<tr><td><font size=5><b>Lieferanten Artikel " . $rsArtikel['LAR_LARTNR'][0] . "</b></font></td>";
		print "<td width=100><font size=1>" . $rsArtikel["LAR_USER"][0] ."<br>"  . $rsArtikel["LAR_USERDAT"][0] . "</font></td>";
		print "<td width=40 backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&Liste=True';></td>";
		if($rsArtikel["LAR_USER"][0]=='LOESCHEN')
		{
			print "<tr><td><span class=HinweisText>Dieser Artikel wird beim n�chsten Datenabgleich zur�ckgesetzt. Um dies zu verhindern, speichern Sie den Artikel erneut ab (ALT+S).</span></font></td>";
		}
		print "</tr></table>";

	
			// Zeile 1
		print "<table border=1 width=100% >";
		print "<colgroup><col width=120><col width=230><col width=120><col width=*></colgroup>";
		print "<tr>";
		print "<td id=FeldBez>Nummer</td><td><input type=hidden name=txtLAR_KEY value=". $rsArtikel['LAR_KEY'][0] . ">" . $rsArtikel['LAR_LARTNR'][0] . "</td>";
		print "<td id=FeldBez>Bezeichnung</td><td>" . $rsArtikel['LAR_BEZWW'][0] . "</td>";
		print "</tr>";	
		print "</table>";


			// Zeile 1a
		print "<table border=1 width=100% >";
		print "<colgroup><col width=120><col width=230><col width=120><col width=*><col width=120><col width=*><col width=120><col width=*></colgroup>";
		print "<td id=FeldBez>Reklamation</td><td>" . ($rsArtikel["LAR_REKLAMATIONSKENNUNG"][0]==''?'- -':$rsArtikel["LAR_REKLAMATIONSKENNUNG"][0]) . "</td>";
		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			print "<td id=FeldBez>Alternativ</td><td>";
			print "<select name=txtLAR_ALTERNATIVARTIKEL>";
			print "<option value=0 " . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==0?'selected':'') . ">Nein</option>";
			print "<option value=1 " . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==1?'selected':'') . ">Ja</option>";
			print "</select></td>";
			
			
			print "<td id=FeldBez>Alte Nr</td><td>";
			print "<select name=txtLAR_ALTELIEFNR>";
			print "<option value=0 " . ($rsArtikel['LAR_ALTELIEFNR'][0]==0?'selected':'') . ">Nein</option>";
			print "<option value=1 " . ($rsArtikel['LAR_ALTELIEFNR'][0]==1?'selected':'') . ">Ja</option>";
			print "</select></td>";
		}
		else
		{
			print "<td id=FeldBez>Alternativ</td><td>" . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==0?'Nein':'Ja') . "</td>";
			print "<td id=FeldBez>Alte Nr</td><td>" . ($rsArtikel['LAR_ALTELIEFNR'][0]==0?'Nein':'Ja') . "</td>";
		}
		print "<td id=FeldBez><input type=hidden name=txtLAR_BEKANNTWW value='" . $rsArtikel['LAR_BEKANNTWW'][0] . "'>WWS-Artikel</td><td>" . ($rsArtikel['LAR_BEKANNTWW'][0]==1?'Ja':'Nein') . "</td>";
		print "</tr>";	
		print "</table>";

	
			// Zeile 1b
		print "<table border=1 width=100% >";
		print "<colgroup><col width=140><col width=60><col width=*></colgroup>";
		print "<td id=FeldBez>Lieferant</td>";
		print "<td>" . $rsArtikel['LAR_LIE_NR'][0] . "</td>";
		print "<td>" . $rsArtikel['LIE_NAME1'][0] . "</td>";
		print "</table>";

			// Zeile 2
		print "<table border=1 width=100% >";
		print "<colgroup><col width=140><col width=60><col width=100><col width=*></colgroup>";
		print "<tr>";

		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			print "<td id=FeldBez>Problemartikel</td><td><select name=txtLAR_PROBLEMARTIKEL>";
			print "<option value=0 " . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'selected':'') . ">Nein</option>";
			print "<option value=1 " . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==1?'selected':'') . ">Ja</option>";
			print "</select></td>";
	
			print "" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'<td id=FeldBez>Problem':'<td bgcolor=#FF0000>Problem');
			echo "<td><input name=txtLAR_PROBLEMBESCHREIBUNG size=70 value='" . $rsArtikel['LAR_PROBLEMBESCHREIBUNG'][0] . "'></td>";
		}
		else
		{
			print "<td id=FeldBez>Problemartikel</td><td>" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'Nein':'Ja') . "</td>";
			print "" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'<td id=FeldBez>Problem':'<td bgcolor=#FF0000>Problem') . "</td><td>" . $rsArtikel['LAR_PROBLEMBESCHREIBUNG'][0] . "</td>";
		}
			
		print "</tr>";	
		print "</table>";
	
			// Zeile 3
		print "<table border=1 width=100% >";
		print "<colgroup><col width=140><col width=60><col width=70><col width=*><col width=100><col width=60></colgroup>";
		print "<tr>";
		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			print "<td id=FeldBez>Gepr�ft</td><td><input name=txtLAR_GEPRUEFT size=11 value='" . $rsArtikel['LAR_GEPRUEFT'][0] . "'></td>";

			echo '<td id=FeldBez>durch';
			if(awisBenutzerRecht($con, 601)>0)		// Stammdaten: Pr�fpersonal
			{
				echo '<a href=/stammdaten/pruefpersonal.php?APPID='. $rsArtikel['LAR_APP_ID'][0] . '&Zurueck=artikel_LIEF&LARKEY=' . $rsArtikel['LAR_KEY'][0] . '><img border=0 src=/bilder/suche.png alt=Pr�fpersonal></a>';
			}
			
			echo '</td><td><select name=txtLAR_APP_ID>';
			$rsAPP = awisOpenRecordset($con, "SELECT APP_ID, APP_NAME FROM AWIS.ArtikelPruefpersonal WHERE APP_AKTIVBIS >= SYSDATE ORDER BY APP_NAME");
			for($_i=0;$_i<$awisRSZeilen;$_i++)
			{
				print "<option value=" . $rsAPP['APP_ID'][$_i] . " " . ($rsArtikel['LAR_APP_ID'][0]==$rsAPP['APP_ID'][$_i]?'selected':'') . ">" . $rsAPP['APP_NAME'][$_i] . "</option>";
			}
			print "</select></td>";
			unset($rsAPP);
						
			print "<td id=FeldBez>Muster</td><td><select name=txtLAR_MUSTER>";
			print "<option value=0 " . ($rsArtikel['LAR_MUSTER'][0]==0?'selected':'') . ">Nein</option>";
			print "<option value=1 " . ($rsArtikel['LAR_MUSTER'][0]==1?'selected':'') . ">Ja</option>";
			print "</select></td>";
		}
		else
		{
			print "<td id=FeldBez>Gepr�ft</td><td>" . $rsArtikel['LAR_GEPRUEFT'][0] . "</td>";
			print "<td id=FeldBez>durch</td><td>" . $rsArtikel['APP_NAME'][0] . "</td>";
			print "<td id=FeldBez>Muster</td><td>" . ($rsArtikel['LAR_MUSTER'][0]==0?'Nein':'Ja') . "</td>";
		}
		print "</tr>";	
		print "</table>";
	
				// Zeile 4
		print "<table border=1 width=100%>";
		print "<colgroup><col width=140></col><col width=*></col></colgroup>";
		print "<tr>";
		If(($LARRechtestufe & 2)==2)
		{
			print "<td id=FeldBez>Bemerkung</td><td><textarea cols=90 rows=3 type=text name=txtLAR_BEMERKUNGEN>" . $rsArtikel['LAR_BEMERKUNGEN'][0] . "</textarea></td>";
		}
		else
		{
			print "<td id=FeldBez>Bemerkung</td><td>" . $rsArtikel['LAR_BEMERKUNGEN'][0] . "</td>";
		}
		print "</tr>";	
		print "</table>";

		print "<hr>";
		
		$rsGNr = awisOpenRecordset($con, "SELECT * FROM AWIS.Gebrauchsnummern, TEILEINFOS WHERE GNR_KEY = TEI_KEY2 AND TEI_KEY1 =0" . $rsArtikel["LAR_KEY"][0]);
		$rsGNRZeilen = $awisRSZeilen;
		
		if($rsGNRZeilen>0)
		{
			print "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
			for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
			{
				print "<td>Gebrauchsnummer:</td>";
				print "<td><input name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
				print "</td>";
				print "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
				print "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
				print "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
			}
			print "</tr></table>";
		}
		else
		{
			print "\n<!--Keine Gebrauchsnummern gefunden-->\n";
		}

		// Unterregister mit abh�ngigen Daten
		$cmdAktion = ($HTTP_GET_VARS["Seite"]==''?"ATUArtikel":$HTTP_GET_VARS["Seite"]);
		awis_RegisterErstellen(42, $con);


		If(($LARRechtestufe & 2)==2)
		{
			print "<input type=hidden name=Speichern Value=True>";
			print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";

			if($rsArtikel['LAR_BEKANNTWW'][0]==0)			// L�schen m�glich, da kein WWS Artikel
			{
				print "&nbsp;<input type=image alt=\"L�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdLARLoeschen accesskey=x onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][0] . "&Loeschen=True'>";
			}
			else
			{
				print "&nbsp;<input type=image alt=\"Informationen l�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdLARLoeschen accesskey=x onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][0] . "&Loeschen=True'>";
			}

						// Cursor in das erste Feld setzen
			print "<Script Language=JavaScript>";
			print "document.getElementsByName(\"txtLAR_ALTERNATIVARTIKEL\")[0].focus();";
			print "</Script>";

		}
					
	}
	else		// Gar keinen gefunden
	{
		print "<span class=HinweisText>Keine Lieferantenartikel gefunden</span>";
	}
			
	unset($rsGNr);

}

print "</form>";


/********************************************************************************************************************
* 
* _ArtikelInfo(ID, Recordset, ArtikelNummer)
* 
*********************************************************************************************************************/

function _ArtikelInfo($ID, $rsOEI, $LARKey, $FeldName)
{
	for($_i=0;$rsOEI["OEI_LAR_KEY"][$_i]!='';$_i++)	
	{
		if($rsOEI["OEI_LAR_KEY"][$_i]==$LARKey AND $rsOEI["OEI_OIT_ID"][$_i]==$ID)
		{
			return $rsOEI[$FeldName][$_i];
		}
	}
	return('');
}