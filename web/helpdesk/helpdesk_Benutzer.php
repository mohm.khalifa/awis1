<?php
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;


$RechteStufe = awisBenutzerRecht($con, 1900);

if(($RechteStufe & 28)==0)
{
    awisEreignis(3,1900,'Helpdesk-Benutzer',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

	//****************************************
	// �bersicht
	//****************************************
if(isset($_POST['cmdUebersicht_x']))
{
	unset($_POST['txtXBN_KEY']);
}

	//****************************************
	// Daten speichern
	//****************************************
if(isset($_POST['cmdSave_x']))
{
	if($_POST['txtXBN_KEY']==0)
	{
		$Fehler = false;

		if($_POST['txtXBN_NAME']=='')
		{
			echo '<span class=HinweisText>Sie m�ssen einen eindeutigen Benutzernamen angeben!</span>';
			$Fehler = true;
		}
		if($_POST['txtXBN_VOLLERNAME']=='')
		{
			echo '<span class=HinweisText>Sie m�ssen den vollst�ndigen Namen angeben!</span>';
			$Fehler = true;
		}
		$rsXBN = awisOpenRecordset($con,'SELECT * FROM Benutzer WHERE UPPER(XBN_NAME)=\'' . strtoupper($_POST['txtXBN_NAME']) . '\'');
		if($awisRSZeilen>0)
		{
			echo '<span class=HinweisText>Sie m�ssen einen eindeutigen Benutzernamen angeben!';
			echo '<br>Der Name ' . $_POST['XBN_NAME'] . ' wurde f�r den Anwender ' . $rsXBN['XBN_VOLLERNAME'][0] . ' schon verwendet!</span>';
			$Fehler = true;
		}


		if(!$Fehler)
		{
			$SQL = 'INSERT INTO Benutzer';
			$SQL .= '(XBN_NAME, XBN_VOLLERNAME, XBN_IPADRESSE, XBN_KON_KEY, XBN_USER, XBN_USERDAT)';
			$SQL .= ' VALUES( ';
			$SQL .= ' \'' . $_POST['txtXBN_NAME'] . '\'';
			$SQL .= ', \'' . $_POST['txtXBN_VOLLERNAME'] . '\'';
			$SQL .= ($_POST['txtXBN_IPADRESSE']==''?', null':', \'' . $_POST['txtXBN_IPADRESSE'] . '\'');
			$SQL .= ', ' . $_POST['txtXBN_KON_KEY'] . '';
			$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ', SYSDATE';
			$SQL .= ')';

			if(awisExecute($con,$SQL)==false)
			{
				awisErrorMailLink("15090852-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
				awis_Debug(1,$SQL);
			}
			else
			{
				$rsXBN = awisOpenRecordset($con,'SELECT * FROM Benutzer WHERE UPPER(XBN_NAME)=\'' . strtoupper($_POST['txtXBN_NAME']) . '\'');
				if($awisRSZeilen>0)
				{
						// Protokoll
					$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
					$SQL .= ' VALUES(';
					$SQL .= "".$rsXBN['XBN_KEY'][0];
					$SQL .= ",'" . "Anwender ". $rsXBN['XBN_VOLLERNAME'][0] ." wurde angelegt.'";
					$SQL .= ", '" . str_replace("'","",$Felder) . "'";
					$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
					$SQL .= ", SYSDATE";
					$SQL .= ")";

					if(awisExecute($con,$SQL)==false)
					{
						awisErrorMailLink("15090852-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
						awis_Debug(1,$SQL);
					}


						//***************************************
						// Login speichern und Kennwort setzen
						//***************************************
					$SQL = 'INSERT INTO BenutzerLogins';
					$SQL .= '(XBL_XBN_KEY, XBL_LOGIN)';
					$SQL .= ' VALUES( ';
					$SQL .= "".$rsXBN['XBN_KEY'][0];
					$SQL .= ", '".strtoupper($rsXBN['XBN_NAME'][0]) . "'";
					$SQL .= ')';

					if(awisExecute($con,$SQL)==false)
					{
						awisErrorMailLink("15090957-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
						awis_Debug(1,$SQL);
					}

					$NeuesKennwort = date('dmHis');
					// Meldung unterdr�cken
					echo '<span class=HinweisText>';
					@system("/daten/zugang/newuser.sh " . strtolower($rsXBN['XBN_NAME'][0]) . " \"" . $NeuesKennwort . "\"",$erg);
					echo '</span>';

					awis_BenutzerParameterSpeichern($con,"Passwortaendern",$rsXBN['XBN_NAME'][0],1);

					echo "<br><br><a href=mailto:" . $rsXBN['XBN_NAME'][0] . "?Subject=AWIS:%20Helpdesk";
					echo "&Body=";
					echo "Sie%20wurden%20fuer%20AWIS%20freigeschaltet.%20Ihr%20Benutzername%20lautet%20";
					echo "".strtolower($rsXBN['XBN_NAME'][0]).'.%20';
					echo "Bitte%20verwenden%20Sie%20das%20Kennwort%20";
					echo $NeuesKennwort;
					echo "%20um%20sich%20anzumelden.";
					echo "%20%20%20";
					echo "Ihr%20AWIS%20Team.";
					echo ">";
					echo "Benutzer per E-Mail benachrichtigen</a><hr>";

						// F�r die Maske den neuen Key setzen
					$_POST['txtXBN_KEY'] = $rsXBN['XBN_KEY'][0];

				}
			}
		}

	}
	else 		// �ndern
	{
		$rsXBN = awisOpenRecordset($con,'SELECT * FROM Benutzer WHERE XBN_Key=0' . $_POST['txtXBN_KEY']);
		$Felder = '';
		$Fehler='';
		$FeldListe = array('sXBN_NAME','sXBN_VOLLERNAME','sXBN_IPADRESSE','nXBN_KON_KEY');

		foreach($FeldListe as $Feld)
		{
			$FeldName = substr($Feld,1);
			$FeldTyp = substr($Feld,0,1);

			if($_POST['txt'.$FeldName] != $_POST['txt'.$FeldName.'_old'])
			{
				if($_POST['txt'.$FeldName.'_old'] != $rsXBN[''.$FeldName.''][0])
				{
					$Fehler  .= '<br>' . substr($FeldName,4) . ' von \'<i>' . $_POST['txt'.$FeldName.'_old'] . '</i>\' auf \'<i>' . $rsXBN[''.$FeldName.''][0] . '</i>\'';
				}
				else
				{
					$Felder .= ", ".$FeldName."=" . ($FeldTyp=='s'?"'":"") . $_POST['txt'.$FeldName.''] . ($FeldTyp=='s'?"'":"");
				}
			}
		}
		if($Felder!='' and $Fehler=='')
		{
			$SQL = 'UPDATE Benutzer SET ' . substr($Felder,1);
			$SQL .= ', XBN_User=\'' . $AWISBenutzer->BenutzerName() . '\', XBN_USERDAT=SYSDATE';
			$SQL .= ' WHERE XBN_KEY = 0'.$_POST['txtXBN_KEY'];

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('17081200-helpdesk_Benutzer.php', 2, $awisDBFehler['message']);
			}

				// Protokoll
			$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
			$SQL .= ' VALUES(';
			$SQL .= "".$_POST['txtXBN_KEY'];
			$SQL .= ",'" . "Anwender ". $rsXBN['XBN_VOLLERNAME'][0] ." wurde ge�ndet.'";
			$SQL .= ", '" . str_replace("'","",$Felder) . "'";
			$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
			$SQL .= ", SYSDATE";
			$SQL .= ")";

			if(awisExecute($con,$SQL)==false)
			{
				awisErrorMailLink("17081533-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
				awis_Debug(1,$SQL);
			}

		}
		elseif($Fehler!='')
		{
			echo '<span class=HinweisText>';

			echo 'Datensatz kann nicht gespeichert werden. Folgende Felder wurden in der Zwischenzeit von ' . $rsXBN['XBN_USER'][0] . ' ver�ndert:<br><br>';
			echo $Fehler;
			echo '<br><br>Ihre �nderungen werden nicht gespeichert.';

			echo '</span>';
		}
	}

	if((isset($_POST['txtXBG_XBB_Key_0']) AND $_POST['txtXBG_XBB_Key_0']!=0) AND $_POST['txtXBN_KEY']>0)
	{

	    $SQL = "INSERT INTO AWIS.BENUTZERGRUPPEN(XBG_XBN_Key,XBG_XBB_Key) VALUES(" . $_POST['txtXBN_KEY'] . ", " . $_POST['txtXBG_XBB_Key_0'] .")";
		if(!awisExecute($con, $SQL ))
		{
			awisErrorMailLink("17081211-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
		}

			// Protokoll
		$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
		$SQL .= ' VALUES(';
		$SQL .= "".$_POST['txtXBN_KEY'];
		$SQL .= ",'" . "Anwender ". $rsXBN['XBN_VOLLERNAME'][0] ." wurde in die Gruppe " . $_POST['txtXBG_XBB_Key_0'] . " hinzugef�gt.'";
		$SQL .= ", ''";
		$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ", SYSDATE";
		$SQL .= ")";

		if(awisExecute($con,$SQL)==false)
		{
			awisErrorMailLink("17081533-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
			awis_Debug(1,$SQL);
		}

	}

}	// Ende Speichern



//************************************
// Gruppenangaben l�schen
//************************************

if(awis_NameInArray($_POST,"cmdDELGRP")!='')
{
    $nr = intval(substr(awis_NameInArray($_POST,"cmdDELGRP"),10));
    $SQL = "DELETE FROM AWIS.BENUTZERGRUPPEN WHERE XBG_XBN_Key=0" . $_POST['txtXBN_KEY'] . " AND XBG_XBB_KEY=0" . $nr ."";
	awis_Debug(1,$SQL);
	$Erg = awisExecute($con, $SQL);
	awis_Debug($Erg);
	//awisLogoff($con);
	if($Erg==FALSE)
	{
		awisErrorMailLink("17081212-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
		die();
	}

	$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
	$SQL .= ' VALUES(';
	$SQL .= "".$_POST['txtXBN_KEY'];
	$SQL .= ",'" . "Anwender ". $_POST['txtXBN_KEY'] ." wurde aus der Gruppe " . $nr . " gel�scht.'";
	$SQL .= ", ''";
	$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
	$SQL .= ", SYSDATE";
	$SQL .= ")";

	if(awisExecute($con,$SQL)==false)
	{
		awisErrorMailLink("17081533-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
		awis_Debug(1,$SQL);
	}

}

    //************************************
    // Erster Aufruf
    //************************************

if(!isset($_POST['txtXBN_KEY']) OR (isset($_POST['txtXBN_KEY']) AND $_POST['txtXBN_KEY']==-1))
{
	echo '<br><span class=Ueberschrift>Benutzer ausw�hlen</span><br><br>';
    echo "<form name=frmBenutzer method=post action=./helpdesk_Main.php?cmdAktion=Benutzer>";

    echo "<table boder=1 cellspacing=5 ><tr><td><table border=0 ><tr><td>Benutzer:</td>";
    echo "<td><select name=txtXBN_KEY size=1>";
    echo '<option value=-1>::Benutzer ausw�hlen::</option>';
    echo '<option value=0>::Neuer Benutzer::</option>';

    $rsBenutzer = awisOpenRecordset($con, "SELECT XBN_Key, XBN_VOLLERNAME FROM AWIS.Benutzer, AWIS.BenutzerLogins WHERE XBN_Key=XBL_XBN_Key AND UPPER(SUBSTR(XBN_VOLLERNAME,1,8)) <> 'FILIALE ' ORDER BY XBN_VOLLERNAME");
    $rsBenutzerZeilen = $awisRSZeilen;

    for($i = 0; $i<$rsBenutzerZeilen; $i++)
    {
        echo "<option value=" . $rsBenutzer["XBN_KEY"][$i] . ">" . $rsBenutzer["XBN_VOLLERNAME"][$i] . "</option>";
    }

    echo "</select></td></tr>";

    echo '<tr><td colspan=2>';
	echo "<br>&nbsp;<input tabindex=99 type=image src=/bilder/eingabe_ok.png title='Benutzerdetails zeigen' name=cmdSuche value=\"Aktualisieren\">";
    echo "</td></tr></table>";

    echo "</form>";

}
    //***************************************
    // Benutzer �ndern oder neu anlegen
    //***************************************
else
{
    $rsBenutzer = awisOpenRecordset($con, "SELECT * FROM AWIS.Benutzer, AWIS.BenutzerLogins WHERE XBN_Key=XBL_XBN_Key(+) AND XBN_Key=0" . $_POST['txtXBN_KEY'] . " ORDER BY XBN_NAME");
    if($awisRSZeilen>=0)
    {
        echo "<form name=frmBenutzer method=post action=./helpdesk_Main.php?cmdAktion=Benutzer>";

        echo "<br><span class=Ueberschrift>Benutzer �ndern</span><br><br>";

        	// Felder speichern
		echo "<input type=hidden name=txtXBN_NAME_old value='" .$rsBenutzer['XBN_NAME'][0] ."'>";
        echo "<input type=hidden name=txtXBN_VOLLERNAME_old value='" . $rsBenutzer['XBN_VOLLERNAME'][0] . "'>";
        echo "<input type=hidden name=txtXBN_IPADRESSE_old value='" . $rsBenutzer['XBN_IPADRESSE'][0] ."'>";
        echo "<input type=hidden name=txtKennwortAendern_old value=1 " . (awis_BenutzerParameter($con,"Passwortaendern",$rsBenutzer['XBN_KEY'][0])==-1?" checked ":"") . ">";
	    echo "<input type=hidden name=txtXBN_KON_KEY_old value='" . $rsBenutzer['XBN_KON_KEY'][0] ."'>";

        	// Formular aufbauen
        echo "<table border=0 id=Maske>";

        echo "<tr><td>Benutzer-ID:</td><td><input style=background-color:#DCDCDC; size=8 type=text readonly name=txtXBN_KEY value=" . $rsBenutzer['XBN_KEY'][0] . "></td><td></td></tr>";

		// Abteilungen anzeigen
		$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || (SELECT KAB_Abteilung FROM KontakteAbteilungen INNER JOIN KontakteAbteilungenZuordnungen ON KAB_KEY=KZA_KAB_KEY WHERE KZA_KON_KEY=KONTAKTE.KON_KEY AND ROWNUM<=1) AS KONTAKT FROM AWIS.KONTAKTE WHERE KON_KKA_KEY=1 ORDER BY KON_NAME1, KON_NAME2");
		$rsKontaktZeilen = $awisRSZeilen;

		echo "<tr><td>Benutzername:</td><td><input type=text name=txtXBN_NAME value=" . chr(34) . $rsBenutzer['XBN_NAME'][0] . chr(34) ."></td><td></td></tr>";
        echo "<tr><td>Voller Name:</td><td><input type=text name=txtXBN_VOLLERNAME value=" . chr(34) . $rsBenutzer['XBN_VOLLERNAME'][0] . chr(34) . "></td><td></td></tr>";
        echo "<tr><td>IP Adresse:</td><td><input type=text name=txtXBN_IPADRESSE value=" . chr(34) . $rsBenutzer['XBN_IPADRESSE'][0] . chr(34) ."></td><td></td></tr>";
        echo "<tr><td>Kennwort�nderung:</td><td><input type=checkbox name=txtKennwortAendern value=1 " . (awis_BenutzerParameter($con,"Passwortaendern",$rsBenutzer['XBN_KEY'][0])==-1?" checked ":"") . "></td><td></td></tr>";

	    echo "<tr><td>Kontakt:</td><td><select size=1 name=txtXBN_KON_KEY><option value=0>Kontakt ausw�hlen...</option>";
        for($i=0; $i<$rsKontaktZeilen;$i++)
        {
            echo "<option ";
			if($rsKontakt["KON_KEY"][$i] == $rsBenutzer["XBN_KON_KEY"][0])
			{
				echo " selected ";
			}
			echo "value=" . $rsKontakt["KON_KEY"][$i] . ">". $rsKontakt["KONTAKT"][$i] . "</option>";
        }
        echo "</select></td></tr>";

		unset($rsKontakt);

        echo "</td></tr></table>";

            //*****************************
            // Benutzergruppen
            //*****************************

        $rsBenutzerGruppen = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez WHERE XBB_SYSTEMGRUPPE IS NULL AND XBB_KEY Not In (SELECT XBG_XBB_KEY FROM AWIS.BenutzerGruppen WHERE XBG_XBN_Key=0" . $_POST['txtXBN_KEY'] . ") ORDER BY XBB_BEZ");
        $rsBenutzerGruppenZeilen = $awisRSZeilen;

        echo "<table border=0><tr><td colspan=5><hr></td></tr>";
        echo "<tr><td colspan=2 id=FeldBez></b>Gruppenmitgliedschaften</td></tr>";

		if(($RechteStufe&8)==8)		// In Gruppe hinzuf�gen
		{
            echo "<tr><td><select size=1 name=txtXBG_XBB_Key_0>";
            echo "<option selected value=0>Gruppe hinzuf�gen...</option>";
            for($BGZeilen=0; $BGZeilen<$rsBenutzerGruppenZeilen;$BGZeilen++)
            {
                echo "<option ";
                if($_POST['txtXBN_KEY']==0 AND $rsBenutzerGruppen['XBB_BEZ'][$BGZeilen]=='AWIS-User')	// Neuer Benutzer
                {
                	echo ' selected ';
                }
                echo " value=" . $rsBenutzerGruppen['XBB_KEY'][$BGZeilen] . ">". $rsBenutzerGruppen['XBB_BEZ'][$BGZeilen] . "</option>";
            }

//            echo "</select></td><td><input type=image alt=Gruppe_hinzuf�gen src=/bilder/diskette.png name=cmdSAVGRP_" . $rsBenutzer['XBN_KEY'][0] . " ></td></tr>";
		}
        $rsGMitglieder = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez WHERE XBB_KEY In (SELECT XBG_XBB_KEY FROM AWIS.BenutzerGruppen WHERE XBG_XBN_Key=0" . $_POST['txtXBN_KEY'] . ")");
        $rsGMitgliederZeilen = $awisRSZeilen;

        echo "<tr><td class=FeldBez>Gruppe</td><td class=FeldBez>Bemerkung</td></tr>";
        for($i=0;$i<$rsGMitgliederZeilen;$i++)
        {
   	        echo "<tr><td style=\"border-bottom-width:1px;border-bottom-style:solid;\">";
			echo $rsGMitglieder['XBB_BEZ'][$i] .  '</td>';
   	        echo "<td style=\"border-bottom-width:1px;border-bottom-style:solid;\">";
			echo $rsGMitglieder['XBB_BEMERKUNG'][$i] .  '</td>';

			if(($RechteStufe&16)==16)		// Aus Gruppe l�schen
			{
                echo "</td><td><input type=image alt=Gruppe_l�schen src=/bilder/muelleimer.png name=cmdDELGRP_" . $rsGMitglieder['XBB_KEY'][$i] . " onclick=document.frmBenutzer.submit();><td></tr>";
			}
        }

        echo "</table>";

		echo "<hr>&nbsp;<input type=image alt=Benutzer_speichern src=/bilder/diskette.png name=cmdSave>";
		echo "&nbsp;<input type=image title='Zur �bersicht' src=/bilder/NeueListe27.png name=cmdUebersicht>";

        echo "</form>";
    }
}
?>