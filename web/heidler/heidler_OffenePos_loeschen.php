<?php
global $AWISCursorPosition;
global $AWISBenutzer;

try {
    require_once 'heidler_funktionen.php';
    $functions = new heidler_funktionen();

    if (isset($_POST['txtALTDATUM']) and strlen($_POST['txtALTDATUM']) == 6) {
        $_POST['txtALTDATUM'] = substr($_POST['txtALTDATUM'], 0, 2) . '.' . substr($_POST['txtALTDATUM'], 2, 2) . '.' . substr($_POST['txtALTDATUM'], 4, 2);
    }

    if (isset($_POST['txtALTDATUM']) and strtotime($_POST['txtALTDATUM']) === false) {
        $functions->_Form->Hinweistext($functions->AWISSprachKonserven['HEIDLER']['DATUMSFORMAT'], awisFormular::HINWEISTEXT_FEHLER);
    } else {
        $SQL = 'SELECT ';
        $SQL .= ' *';
        $SQL .= ' FROM NVS_HEIDLER@NVSWEN';
        $SQL .= ' WHERE MENGE <> VERBUCHTE_MENGE';
        $SQL .= ' AND DATUM_KOMMISSIONIERUNG = ' . $functions->_DB->WertSetzen('NVS', 'T', date('dmy', strtotime($_POST['txtALTDATUM'])));

        $rsDaten = $functions->_DB->RecordSetOeffnen($SQL, $functions->_DB->Bindevariablen('NVS', true));

        while (!$rsDaten->EOF()) {
            $functions->_DB->TransaktionBegin();
            $SQLInsert = 'INSERT INTO NVS_HEIDLER_ARCHIV@NVSWEN ';
            $SQLInsert .= 'SELECT * FROM NVS_HEIDLER@NVSWEN ';
            $SQLInsert .= ' WHERE AUFTRAG_ID = ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('AUFTRAG_ID'));
            $SQLInsert .= ' AND LAGERKZ = ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('LAGERKZ'));
            $SQLInsert .= ' AND TRACKING_NR ' . ($rsDaten->FeldInhalt('TRACKING_NR') == 'null'?'= ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('TRACKING_NR')):'is null');
            $functions->_DB->Ausfuehren($SQLInsert, 'NVS', true, $functions->_DB->Bindevariablen('NVS', true));

            $SQLDelete = 'DELETE FROM NVS_HEIDLER@NVSWEN ';
            $SQLDelete .= ' WHERE AUFTRAG_ID = ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('AUFTRAG_ID'));
            $SQLDelete .= ' AND LAGERKZ = ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('LAGERKZ'));
            $SQLDelete .= ' AND TRACKING_NR ' . ($rsDaten->FeldInhalt('TRACKING_NR') == 'null'?'= ' . $functions->_DB->WertSetzen('NVS', 'T', $rsDaten->FeldInhalt('TRACKING_NR')):'is null');
            $functions->_DB->Ausfuehren($SQLDelete, 'NVS', true, $functions->_DB->Bindevariablen('NVS', true));
            $functions->_DB->TransaktionCommit();

            $rsDaten->DSWeiter();
        }

        if ($rsDaten->AnzahlDatensaetze() == 0) {
            $functions->_Form->Hinweistext($functions->AWISSprachKonserven['HEIDLER']['KEINEDATENGELOESCHT'], awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
        } else {
            $functions->_Form->Hinweistext($functions->AWISSprachKonserven['HEIDLER']['DATENGELOESCHT'], awisFormular::HINWEISTEXT_OK);
        }
    }
} catch (Exception $ex) {
    if ($functions->_DB->TransaktionAktiv()) {
        $functions->_DB->TransaktionRollback();
    }

    if ($functions->_Form instanceof awisFormular) {
        $functions->_Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}