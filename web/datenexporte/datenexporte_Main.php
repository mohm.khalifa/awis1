<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) .">";

}
catch (Exception $ex)
{
	die($ex->getMessage());
}

ini_set('max_execution_time', 6000);
// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Datenexporte');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Datenexporte'].'</title>';
?>
</head>
<body>
<?php

include ("awisHeader3.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	$Recht4001 = $AWISBenutzer->HatDasRecht(4001);
	if($Recht4001==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"201405250920");
		die();
	}

	$Register = new awisRegister(4001);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));
	
	$Form->SetzeCursor($AWISCursorPosition);
	
/*	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmdZurueck','../index.php','/bilder/zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image','cmdXReset','','/bilder/radierer.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'R');
	$Form->SchaltflaechenEnde();
*/
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202090937");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}
?>
</body>
</html>