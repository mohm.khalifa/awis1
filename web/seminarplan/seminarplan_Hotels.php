<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('SEH', '%');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_AktivInaktiv');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht4209 = $AWISBenutzer->HatDasRecht(4209);
    if($Recht4209==0)
    {
        $Form->Fehler_KeineRechte();
    }

    $EditRecht = (($Recht4209 & 2) == 2) ? true : false ;

    if (isset($_POST['cmdDSNeu_x']) and ($Recht4209 & 4) == 4) {
        $EditRecht = true;
    }

    $Start = strpos($_SERVER['HTTP_REFERER'], '/seminarplan');
    $UrlStart = substr($_SERVER['HTTP_REFERER'], $Start);

    $End = strpos($UrlStart, '&HOTEL_SET_KEY=');
    $UrlBack = substr($UrlStart, 0, $End);

    $BackLink = '/seminarplan/seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer';

    echo "<form name=frmSuche method=post action=./seminarplan_Main.php?cmdAktion=Hotels>";

    $Form->Erstelle_HiddenFeld('Backlink',$UrlStart);

    if ($UrlBack == $BackLink) {
        $LinkClose = $UrlStart;
    } else {
        $LinkClose = './seminarplan_Main.php?cmdAktion=Hotels';
    }

    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=$LinkClose accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");

    $LabelBreite = 300;
    $FeldBreite = 200;

    $Form->Formular_Start();

    //********************************************************
    // AWIS_KEY1 setzen
    //********************************************************
    if (isset($_POST['cmdDSNeu_x']) or $UrlBack == $BackLink) {
        $AWIS_KEY1 = -1;
    } elseif (isset($_GET['SEH_KEY'])) {
        $AWIS_KEY1 = $_GET['SEH_KEY'];
    } elseif (isset($_POST['txtSEH_KEY'])) {
        $AWIS_KEY1 = $_POST['txtSEH_KEY'];
    }
    $Param['KEY'] = $AWIS_KEY1;

    //********************************************************
    // Parameter setzen und Seiten inkludieren
    //********************************************************
    if (isset($_POST['cmdWeiter_x'])) {
        $Param['SEH_NAME'] = $Form->Format('T', $_POST['txtSEH_NAME'], true);
        $Param['SEH_LAN_CODE'] = $Form->Format('T', $_POST['txtSEH_LAN_CODE'], true);
        $Param['SEH_STATUS'] = $Form->Format('T', $_POST['txtSEH_STATUS'], true);
        $Param['SEH_PLZ'] = $Form->Format('T', $_POST['txtSEH_PLZ'], true);
        $Param['SEH_ORT'] = $Form->Format('T', $_POST['txtSEH_ORT'], true);
        $Param['SEH_STRASSE'] = $Form->Format('T', $_POST['txtSEH_STRASSE'], true);
        $Param['SEH_HAUSNR'] = $Form->Format('T', $_POST['txtSEH_HAUSNR'], true);

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['BLOCK'] = 1;

        $AWISBenutzer->ParameterSchreiben("SEHSuche",serialize($Param));
    } elseif (isset($_POST['cmdLoeschen_x'])) {
        include('./seminarplan_loeschen.php');
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include './seminarplan_speichern.php';
    } else {
        $AWIS_KEY1 = $Param['KEY'];
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen("SEHSuche"));

    //*********************************************************
    //* SQL Vorbereiten: Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '') {
            $ORDERBY = $Param['ORDER'];
        } else {
            $Param['ORDER'] = 'SEH_NAME ASC nulls last';
            $ORDERBY =  ' ORDER BY ' . $Param['ORDER'];
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Bedingung f�r SQL zusammenstellen
    //********************************************************
    $SQL = 'SELECT seminarhotel.*, laender.*';
    if($AWIS_KEY1<=0)
    {
        $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
    }
    $SQL .= ' FROM SEMINARHOTEL';
    $SQL .= ' INNER JOIN LAENDER';
    $SQL .= ' ON LAENDER.LAN_CODE = SEMINARHOTEL.SEH_LAN_CODE';

    $Bedingung = '';

    if (isset($Param['SEH_NAME']) and $Param['SEH_NAME'] != '') {
        $Bedingung .= ' AND LOWER(SEH_NAME) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_NAME'])?$Param['SEH_NAME']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_LAN_CODE']) and $Param['SEH_LAN_CODE'] != '') {
        $Bedingung .= ' AND LOWER(SEH_LAN_CODE) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_LAN_CODE'])?$Param['SEH_LAN_CODE']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_STATUS']) and $Param['SEH_STATUS'] != '') {
        $Bedingung .= ' AND LOWER(SEH_STATUS) ' . $DB->LikeOderIst(isset($Param['SEH_STATUS'])?$Param['SEH_STATUS']:'', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_PLZ']) and $Param['SEH_PLZ'] != '') {
        $Bedingung .= ' AND LOWER(SEH_PLZ) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_PLZ'])?$Param['SEH_PLZ']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_ORT']) and $Param['SEH_ORT'] != '') {
        $Bedingung .= ' AND LOWER(SEH_ORT) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_ORT'])?$Param['SEH_ORT']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_STRASSE']) and $Param['SEH_STRASSE'] != '') {
        $Bedingung .= ' AND LOWER(SEH_STRASSE) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_STRASSE'])?$Param['SEH_STRASSE']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if (isset($Param['SEH_HAUSNR']) and $Param['SEH_HAUSNR'] != '') {
        $Bedingung .= ' AND LOWER(SEH_HAUSNR) ' . $DB->LikeOderIst('*'.(isset($Param['SEH_HAUSNR'])?$Param['SEH_HAUSNR']:'').'*', awisDatenbank::AWIS_LIKE_LOWER, 'SEH');
    }
    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4) . ' ';
    }
    if ($AWIS_KEY1) {
        $SQL .= ' AND SEH_KEY = ' .$DB->WertSetzen('SEH', 'N0', $AWIS_KEY1);
    }
    if (isset($ORDERBY) and $ORDERBY != '') {
        $SQL .= $ORDERBY;
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen("SEHSuche"));

    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if(isset($_REQUEST['Block']))
    {
        $Block=$Form->Format('N0',$_REQUEST['Block'],false);
    }
    elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL , $DB->Bindevariablen('SEH', false));

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if($AWIS_KEY1<=0)
    {
        $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
    }

    $rsSEH = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SEH', false));

    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $Param['ORDER']=$ORDERBY;
    $Param['KEY']=$AWIS_KEY1;
    $Param['BLOCK']=$Block;
    $AWISBenutzer->ParameterSchreiben("SEHSuche",serialize($Param));

    //***************************************
    // Suchergebnisse
    //***************************************
    if (isset($_POST['cmdWeiter_x']) or isset($_POST['cmdDSNeu_x']) or isset($_GET['SEH_KEY']) or isset($_GET['Sort']) or isset($_REQUEST['Block']) or $UrlBack == $BackLink) {
        
        if ($rsSEH->AnzahlDatensaetze() == 1 or isset($_POST['cmdDSNeu_x']) or $UrlBack == $BackLink) {

            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($rsSEH->FeldInhalt('SEH_USER')));
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($rsSEH->FeldInhalt('SEH_USERDAT')));

            //***************************************
            // Detailansicht
            //***************************************
            $Form->InfoZeile($Felder, '');

            if (!isset($_POST['cmdDSNeu_x']) and $UrlBack != $BackLink) {
                $Form->Erstelle_HiddenFeld('SEH_KEY', $rsSEH->FeldInhalt('SEH_KEY'));
            }

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELNAME'], 100);
            $Form->Erstelle_TextFeld('!SEH_NAME', $rsSEH->FeldInhalt('SEH_NAME'), 60, 60, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_LAND'], 100);
            $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            $SQL = 'SELECT LAN_CODE, LAN_CODE FROM LAENDER';
            $Form->Erstelle_SelectFeld('!SEH_LAN_CODE', $rsSEH->FeldInhalt('SEH_LAN_CODE'), 471, $EditRecht, $SQL, $OptionBitteWaehlen);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_STATUS'], 100);
            $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            $Daten = explode("|", $AWISSprachKonserven['SEH']['SEH_AKTIVINAKTIV']);
            $SQL = 'SELECT SEH_STATUS, SEH_STATUS FROM SEMINARHOTEL';
            $Form->Erstelle_SelectFeld('!SEH_STATUS', $rsSEH->FeldInhalt('SEH_STATUS'), 471, $EditRecht, '', $OptionBitteWaehlen, 'A', '', '', $Daten);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_PLZ'], 100);
            $Form->Erstelle_TextFeld('!SEH_PLZ', $rsSEH->FeldInhalt('SEH_PLZ'), '', 220, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_ORT'], 100);
            $Form->Erstelle_TextFeld('!SEH_ORT', $rsSEH->FeldInhalt('SEH_ORT'), 60, 60, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_STRASSE'], 100);
            $Form->Erstelle_TextFeld('!SEH_STRASSE', $rsSEH->FeldInhalt('SEH_STRASSE'), 60, 60, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HAUSNUMMER'], 100);
            $Form->Erstelle_TextFeld('!SEH_HAUSNR', $rsSEH->FeldInhalt('SEH_HAUSNR'), '', 220, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOMEPAGE'], 100);
            $Form->Erstelle_TextFeld('SEH_HOMEPAGE', $rsSEH->FeldInhalt('SEH_HOMEPAGE'), 100, 100, $EditRecht);
            $Form->ZeileEnde();

        } elseif ($rsSEH->AnzahlDatensaetze() > 1) {

            $FeldBreiten = array();
            $FeldBreiten['SEH_HOTELNAME'] = 500;
            $FeldBreiten['SEH_LAND'] = 200;
            $FeldBreiten['SEH_PLZ'] = 100;
            $FeldBreiten['SEH_ORT'] = 300;
            $FeldBreiten['SEH_STRASSE'] = 300;
            $FeldBreiten['SEH_HAUSNUMMER'] = 100;

            //***************************************
            // Listenansicht
            //***************************************
            $Form->ZeileStart();
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_NAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_NAME'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_HOTELNAME'], $FeldBreiten['SEH_HOTELNAME'], '', $Link);
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_LAN_CODE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_LAN_CODE'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_LAND'], $FeldBreiten['SEH_LAND'], '', $Link);
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_PLZ'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_PLZ'], $FeldBreiten['SEH_PLZ'], '', $Link);
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_ORT'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_ORT'], $FeldBreiten['SEH_ORT'], '', $Link);
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_STRASSE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_STRASSE'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_STRASSE'], $FeldBreiten['SEH_STRASSE'], '', $Link);
            $Link = './seminarplan_Main.php?cmdAktion=Hotels' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&Sort=SEH_HAUSNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SEH_HAUSNR'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEH']['SEH_HAUSNUMMER'], $FeldBreiten['SEH_HAUSNUMMER'], '', $Link);
            $Form->ZeileEnde();

            $DS = 0;

            while (!$rsSEH->EOF()) {
                $HG = ($DS % 2);

                $Form->ZeileStart();
                $Link = './seminarplan_Main.php?cmdAktion=Hotels&SEH_KEY=' . $rsSEH->FeldInhalt('SEH_KEY');
                $Form->Erstelle_ListenFeld('SEH_NAME', $rsSEH->FeldInhalt('SEH_NAME'), 0, $FeldBreiten['SEH_HOTELNAME'], false, $HG, '', $Link, 'T', 'L', $rsSEH->FeldInhalt('SEH_NAME'));
                $Form->Erstelle_ListenFeld('SEH_LAN_CODE', $rsSEH->FeldInhalt('LAN_LAND'), 0, $FeldBreiten['SEH_LAND'], false, $HG, '', '', 'T', 'L', $rsSEH->FeldInhalt('LAN_LAND'));
                $Form->Erstelle_ListenFeld('SEH_PLZ', $rsSEH->FeldInhalt('SEH_PLZ'), 0, $FeldBreiten['SEH_PLZ'], false, $HG, '', '', 'T', 'L', $rsSEH->FeldInhalt('SEH_PLZ'));
                $Form->Erstelle_ListenFeld('SEH_ORT', $rsSEH->FeldInhalt('SEH_ORT'), 0, $FeldBreiten['SEH_ORT'], false, $HG, '', '', 'T', 'L', $rsSEH->FeldInhalt('SEH_ORT'));
                $Form->Erstelle_ListenFeld('SEH_STRASSE', $rsSEH->FeldInhalt('SEH_STRASSE'), 0, $FeldBreiten['SEH_STRASSE'], false, $HG, '', '', 'T', 'L', $rsSEH->FeldInhalt('SEH_STRASSE'));
                $Form->Erstelle_ListenFeld('SEH_HAUSNR', $rsSEH->FeldInhalt('SEH_HAUSNR'), 0, $FeldBreiten['SEH_HAUSNUMMER'], false, $HG, '', '', 'T', 'L', $rsSEH->FeldInhalt('SEH_HAUSNR'));
                $Form->ZeileEnde();

                $rsSEH->DSWeiter();
                $DS++;
            }

            $Link = './seminarplan_Main.php?cmdAktion=Hotels'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
            $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
        } else {
            echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
        }

    } else {

        //***************************************
        // Suchmaske bauen
        //***************************************
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELNAME'], 100);
        $Form->Erstelle_TextFeld('SEH_NAME', '', 60, 60, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_LAND'], 100);
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $SQL = 'SELECT DISTINCT SEH_LAN_CODE, SEH_LAN_CODE FROM SEMINARHOTEL WHERE SEH_STATUS = \'A\'';
        $Form->Erstelle_SelectFeld('SEH_LAN_CODE', '', 471, true, $SQL, $OptionBitteWaehlen);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_STATUS'], 100);
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $Daten = explode("|", $AWISSprachKonserven['SEH']['SEH_AKTIVINAKTIV']);
        $Form->Erstelle_SelectFeld('SEH_STATUS', '', 471, true, '', $OptionBitteWaehlen, 'A', '', '', $Daten);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_PLZ'], 100);
        $Form->Erstelle_TextFeld('SEH_PLZ', '', '', 220, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_ORT'], 100);
        $Form->Erstelle_TextFeld('SEH_ORT', '', 60, 60, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_STRASSE'], 100);
        $Form->Erstelle_TextFeld('SEH_STRASSE', '', 60, 60, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HAUSNUMMER'], 100);
        $Form->Erstelle_TextFeld('SEH_HAUSNR', '', '', 220, true);
        $Form->ZeileEnde();
    }

    $Form->Formular_Ende();

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (!isset($_POST['cmdWeiter_x']) and !isset($_POST['cmdDSNeu_x']) and !isset($_GET['SEH_KEY']) and !isset($_GET['Sort']) and !isset($_REQUEST['Block']) and $UrlBack != $BackLink ) {
        $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    }
    if (($Recht4209&4)==4) {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    if (((isset($_POST['cmdWeiter_x']) and $rsSEH->AnzahlDatensaetze() == 1) or isset($_GET['SEH_KEY']))) {
        if (($Recht4209&8)==8) {
            $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
        }
        if (($Recht4209&2)==2) {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
    } elseif (isset($_POST['cmdDSNeu_x']) or $UrlBack == $BackLink) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form>')

?>
