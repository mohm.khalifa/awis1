<?php
require_once('awisDatenbank.inc');

class awisArtikelstammBild
{
    /**
     * Datenbankverbindung
     *
     * @var awisDatenbank
     */
    private $_DB = null;

    /**
     * Recordset mit dem Bild
     *
     * @var awisRecordset
     */
    private $_Bild = null;

    private $_BildVorhanden = false;

    private $_BildHoehe = 0;

    private $_BildBreite = 0;

    /**
     * Initialisiert das Bild
     *
     * @param string $ATU_NR
     */
    public function __construct($ATU_NR)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = $ATU_NR;

        $SQL = 'SELECT bild FROM bilder';
        $SQL .= ' WHERE bildname=(SELECT bildname FROM bilder_info WHERE atu_nr= :var_T_ast_atunr)';

        $rsBild = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
        $this->_BildVorhanden = !$rsBild->EOF();
        if (!$this->_BildVorhanden) {
            $SQL = 'SELECT bild FROM bilder';
            $SQL .= ' WHERE bildname=:var_T_ast_atunr';

            $rsBild = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $this->_BildVorhanden = !$rsBild->EOF();
        }

        if ($this->_BildVorhanden) {
            $this->_Bild = $rsBild->FeldInhalt('BILD');
        }

        $SQL = "SELECT LAENGE, HOEHE FROM BILDER_INFO WHERE ATU_NR=:var_T_ast_atunr";
        $rsDaten = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
        if (!$rsDaten->EOF()) {
            $this->_BildHoehe = $rsDaten->FeldInhalt('HOEHE');
            $this->_BildBreite = $rsDaten->FeldInhalt('LAENGE');
        } else {
            $this->_BildHoehe = 300;
            $this->_BildBreite = 400;
        }
    }

    /**
     * Gibt an, ob ein Bild vorhanden ist
     *
     * @return boolean
     */
    public function BildVorhanden()
    {
        return $this->_BildVorhanden;
    }

    /**
     * Zeigt das Bild als PNG Datei an
     *
     */
    public function BildAnzeigen()
    {
        ob_clean();
        header("Content-type: image/png");

        $img = imagecreatefromstring($this->_Bild);
        imagepng($img);
        imagedestroy($img);
    }
}

?>