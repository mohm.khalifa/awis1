<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $comcon;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Helpdesk','OCH_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Helpdesk');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Helpdesk');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Helpdesk'].'</title>';
?>
<style type="text/css" title="text/css">
pre {
white-space: pre-wrap; /* CSS3 */
white-space: -moz-pre-wrap; /* Mozilla */
white-space: -pre-wrap; /* Opera 4-6 */
white-space: -o-pre-wrap; /* Opera 4-6 */
word-wrap: break-word; /* IE 5.5+ */
font-family: verdana;
font-weight: normal;
}
</style>

</head>
<body>
<?php

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$Recht3115 = awisBenutzerRecht($con,3115);
if($Recht3115==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$comcon = awisLogonComCluster('DE',false);

echo '<form name=frmOperating_Helpdesk action=./operating_COM_Main.php?cmdAktion=Helpdesk method=POST>';

echo "<p><center><h1><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Helpdesk']."</b></h1></center><p>";

awis_Debug(1,$_REQUEST);

if(isset($_REQUEST["HDF_ALLE"]))
{
	$HDF_ALLE=1;
}else {
	$HDF_ALLE=0;
}

if(isset($_REQUEST["HDK_KEY"]))
{
	$HDK_KEY=$_REQUEST["HDK_KEY"];
}else {
	$HDK_KEY=0;
}

if(isset($_REQUEST["HDF_KEY"]))
{
	$HDF_KEY=$_REQUEST["HDF_KEY"];
}else {
	$HDF_KEY=0;
}

if(isset($_POST['cmdLoeschen_x']))
{
	//include('./operating_COM_Helpdesk_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./operating_COM_Helpdesk_speichern.php');
}

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

awis_FORM_FormularStart();

$SQL = 'SELECT DISTINCT HDK_KEY, HDK_BEZEICHNUNG FROM COMOP.HELPDESK_FELDER_KATEGORIE ';
$SQL.='ORDER BY HDK_BEZEICHNUNG';

awis_Debug(1,$SQL);

$rsHDK=awisOpenRecordset($comcon,$SQL);
$rsHDKZeilen = $awisRSZeilen;

if($rsHDKZeilen>0)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('',20);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_KATEGORIE'],200,'');
	awis_FORM_ZeileEnde();
	
	for($HDKZeile=0;$HDKZeile<$rsHDKZeilen;$HDKZeile++)
	{
		$Icons = array();
		$Icons[] = array('lupe','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$rsHDK['HDK_KEY'][$HDKZeile]);
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListeIcons($Icons,20,($HDKZeile%2),$AWISSprachKonserven['Wort']['lbl_anzeigen']);
		awis_FORM_Erstelle_ListenFeld('OCH_KATEGORIE',$rsHDK['HDK_BEZEICHNUNG'][$HDKZeile],0,200,false,($HDKZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}

}
else 
{
	awis_FORM_Hinweistext('Keine Inormation zum Anzeigen vorhanden.',1);
}

if(!empty($HDK_KEY))
{
	$SQL = 'SELECT * FROM COMOP.V_HELPDESK_FELDER ';
	$SQL.=' where HDK_KEY='.$HDK_KEY;
	$SQL.=' order by HFZ_BEZEICHNUNG';
	
	awis_FORM_Trennzeile();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('<img src=/bilder/icon_lupe.png class=EingabeUeberschrift border=0>',20,'','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$HDK_KEY.'&HDF_ALLE=1',$AWISSprachKonserven['Wort']['lbl_anzeigen']);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_KATEGORIE'],200,'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_ZUORDNUNG'],200,'');
	awis_FORM_ZeileEnde();
	
	//awis_Debug(1,$SQL);

	$rsHDF=awisOpenRecordset($comcon,$SQL);
	$rsHDFZeilen = $awisRSZeilen;
	
	if($rsHDFZeilen>0)
	{
		for($HDFZeile=0;$HDFZeile<$rsHDFZeilen;$HDFZeile++)
		{
			if((!empty($HDF_KEY)&&$HDF_KEY==$rsHDF['HDF_KEY'][$HDFZeile])||!empty($HDF_ALLE))
			{
				awis_FORM_Trennzeile();
				
				
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_KATEGORIE'],200,'font-size: 80%;');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_ZUORDNUNG'],200,'font-size: 80%;');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_USERNAME'],150,'font-size: 80%;');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_USERDAT'],200,'font-size: 80%;');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('OCH_KATEGORIE',$rsHDF['HDK_BEZEICHNUNG'][$HDFZeile],0,200,false,($HDFZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('OCH_ZUORDNUNG',$rsHDF['HFZ_BEZEICHNUNG'][$HDFZeile],0,200,false,($HDFZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('OCH_USERNAME',$rsHDF['HDF_USER'][$HDFZeile],0,150,false,($HDFZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('OCH_USERDAT',$rsHDF['HDF_USERDAT'][$HDFZeile],0,200,false,($HDFZeile%2),'','','DU');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_BEMERKUNG1'],100,'font-size: 80%;');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();				
				if(!empty($HDF_ALLE)||($Recht3115&2)!=2)
				{
					echo '<div class=FeldText style="width:800;"><pre>'.$rsHDF['HDF_BEMERKUNG1'][$HDFZeile].'</pre></div>';
					//echo '<div class=EingabeFeldText style="width:800;word-wrap:break-word;white-space:pre;">'.$rsHDF['HDF_BEMERKUNG1'][$HDFZeile].'</div>';
					//awis_FORM_Erstelle_Textarea('HDF_BEMERKUNG1',$rsHDF['HDF_BEMERKUNG1'][$HDFZeile],600,150,10,false,'white-space:pre;');
				}
				else 
				{	
					awis_FORM_Erstelle_Textarea('HDF_BEMERKUNG1',$rsHDF['HDF_BEMERKUNG1'][$HDFZeile],600,150,10,true,'');
				}
				awis_FORM_ZeileEnde();
				
				if(($Recht3115&2)==2)
				{
					awis_FORM_ZeileStart();				
					echo '<input type=hidden name=HDK_KEY value=0'.$HDK_KEY.'>';
					echo '<input type=hidden name=HDF_KEY value=0'.$rsHDF['HDF_KEY'][$HDFZeile].'>';
					if(!empty($HDF_ALLE))
					{
						awis_FORM_SchaltflaechenStart();
						awis_FORM_Schaltflaeche('href','cmdAendern','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$HDK_KEY.'&HDF_KEY='.$rsHDF['HDF_KEY'][$HDFZeile],'/bilder/icon_edit.png',$AWISSprachKonserven['Wort']['lbl_aendern'],'','',20,20);
						awis_FORM_SchaltflaechenEnde();
					}
					else
					{	
						awis_FORM_SchaltflaechenStart();
//						echo "&nbsp;<input accesskey=s type=image src=/bilder/diskette.png title='".$AWISSprachKonserven['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
						awis_FORM_Schaltflaeche('image','cmdSpeichern','','/bilder/icon_save.png',$AWISSprachKonserven['Wort']['lbl_speichern'],'','',20,20);
						awis_FORM_Schaltflaeche('href','cmdAnzeigen','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$HDK_KEY.'&HDF_ALLE=1','/bilder/icon_lupe.png',$AWISSprachKonserven['Wort']['lbl_anzeigen'],'','',20,20);
						awis_FORM_SchaltflaechenEnde();
					}
					awis_FORM_ZeileEnde();
				}
		
/*				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Helpdesk']['OCH_BEMERKUNG2'],100,'font-size: 80%;');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();				
				awis_FORM_Erstelle_Textarea('OCH_BEMERKUNG2',$rsHDF['HDF_BEMERKUNG2'][$HDFZeile],600,150,10,true,'');
				awis_FORM_ZeileEnde();
*/				
				awis_FORM_Trennzeile();
			}
			else 
			{
				$Icons = array();
				if(($Recht3115&2)==2)
				{
					$Icons[] = array('edit','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$rsHDF['HDK_KEY'][$HDFZeile].'&HDF_KEY='.$rsHDF['HDF_KEY'][$HDFZeile]);
					$IconToolTip=$AWISSprachKonserven['Wort']['lbl_aendern'];
				}
				else
				{	
					$Icons[] = array('lupe','./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$rsHDF['HDK_KEY'][$HDFZeile].'&HDF_KEY='.$rsHDF['HDF_KEY'][$HDFZeile]);
					$IconToolTip=$AWISSprachKonserven['Wort']['lbl_anzeigen'];
				}
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListeIcons($Icons,20,($HDFZeile%2),$IconToolTip);
				awis_FORM_Erstelle_ListenFeld('OCH_KATEGORIE',$rsHDF['HDK_BEZEICHNUNG'][$HDFZeile],0,200,false,($HDFZeile%2),'','','T');
				awis_FORM_Erstelle_ListenFeld('OCH_ZUORDNUNG',$rsHDF['HFZ_BEZEICHNUNG'][$HDFZeile],0,200,false,($HDFZeile%2),'','','T');
				awis_FORM_ZeileEnde();
			}
		}	
	}
	else
	{
		awis_FORM_Hinweistext('Keine Inormation zum Anzeigen vorhanden.',1);
	}
}

awis_FORM_FormularEnde();

echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

