<?php

/**
 * Maske f�r die AFG-Kassendaten
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * @author ??.08.2009 Beierl Christian
 * @version 1.0
 * �nderungen:
 * 30.09.2011 OP : Selektiert werden nur noch Kassendaten mit dem Wert 0
 *                 im Feld FKA_STATUS --> Kassendatensatz ist nicht
 *                 gel�scht.
 * ***********************************************************************
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FGP', 'FGP*');
    $TextKonserven[] = array('FPH', 'FPH_*');
    $TextKonserven[] = array('FKA', 'FKA_*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if ($Recht9100 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if ($AWIS_KEY1 != 0 && isset($_GET['FKA_NR']) != -1)
    {
        $AWIS_KEY2 = $AWIS_KEY1;
        $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY2, false);
        $rsKopf = $DB->RecordSetOeffnen($SQL);
        $VorgangNr = $rsKopf->FeldInhalt('FGK_VORGANGNR');

        // 30.09.2011 OP : SQL um die Einschr�nkung FKA_STATUS <> 1 erweitert
        // Selektiert alle nicht gel�schten Kassendaten zu Vorgang
        $SQL = 'Select FKASSENDATEN.* from FKASSENDATEN WHERE FKA_AEMNR=' . $DB->FeldInhaltFormat('T', $VorgangNr, false);
        $SQL .= ' and FKA_STATUS <> 1';
        $SQL .= ' or FKA_AEMNR=' . $DB->FeldInhaltFormat('T', $VorgangNr, false);
        $SQL .= ' and FKA_STATUS is null';
        $rsKassendaten = $DB->RecordSetOeffnen($SQL);

        $Form->Formular_Start();

        $Icons = array();

        $Form->ZeileStart();
        if ((intval($Recht9100) & 8) != 0)
        {
            $Icons[] = array('new', './autoglas_Main.php?cmdAktion=Details&Seite=Kassendaten&FKA_NR=-1');
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KEY'], 60, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KENNUNG'], 66, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_FILID'], 50, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_DATUM'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_UHRZEIT'], 90, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BSA'], 50, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_WANR'], 80, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_AEMNR'], 130, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_ATUNR'], 80, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_MENGE'], 60, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BETRAG'], 70, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_VERSNR'], 80, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BEMERKUNG'], 300, '');
        $Form->ZeileEnde();

        $FGKZeile = 0;
        $DS = 0;

        while (!$rsKassendaten->EOF())
        {
            $Form->ZeileStart();
            //$Form->Erstelle_ListenFeld('','',0,38,false,($FGKZeile%2),'','');
            if (intval($Recht9100 & 4) > 0) // �ndernrecht
            {
                $Icons[0] = array('delete', './autoglas_Main.php?cmdAktion=Details&Seite=Kassendaten&Del=' . $rsKassendaten->FeldInhalt('FKA_KEY') . '&txtFGK_KEY=' . $AWIS_KEY1);
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));
            $Link = './autoglas_Main.php?cmdAktion=Details&FKA_KEY=' . $rsKassendaten->FeldInhalt('FKA_KEY') . '';
            $Form->Erstelle_ListenFeld('FKA_KEY', $rsKassendaten->FeldInhalt('FKA_KEY'), 0, 60, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_KENNUNG', $rsKassendaten->FeldInhalt('FKA_KENNUNG'), 0, 66, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_FILID', $rsKassendaten->FeldInhalt('FKA_FILID'), 0, 50, false, ($FGKZeile % 2), '', '');
            $DATUM = str_replace("00:00:00", "", $rsKassendaten->FeldInhalt('FKA_DATUM'));
            $Form->Erstelle_ListenFeld('FKA_DATUM', $DATUM, 0, 100, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_UHRZEIT', $rsKassendaten->FeldInhalt('FKA_UHRZEIT'), 0, 90, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BSA', $rsKassendaten->FeldInhalt('FKA_BSA'), 0, 50, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_WANR', $rsKassendaten->FeldInhalt('FKA_WANR'), 0, 80, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_AEMNR', $rsKassendaten->FeldInhalt('FKA_AEMNR'), 13, 130, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_ATUNR', $rsKassendaten->FeldInhalt('FKA_ATUNR'), 0, 80, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_MENGE', $rsKassendaten->FeldInhalt('FKA_MENGE'), 0, 60, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BETRAG', $rsKassendaten->FeldInhalt('FKA_BETRAG'), 0, 70, false, ($FGKZeile % 2), 'font-weight:bold', '', 'N2');
            $Form->Erstelle_ListenFeld('FKA_KFZKENNZ', $rsKassendaten->FeldInhalt('FKA_KFZKENNZ'), 0, 100, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_VERSNR', $rsKassendaten->FeldInhalt('FKA_VERS_NR'), 0, 80, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BEMERKUNG', $rsKassendaten->FeldInhalt('FKA_BEMERKUNG'), 15, 300, false, ($FGKZeile % 2), '', '', '', '', $rsKassendaten->FeldInhalt('FKA_BEMERKUNG'));
            $Form->ZeileEnde();

            $FGKZeile++;
            $rsKassendaten->DSWeiter();
        }
    }
    elseif (isset($_GET['FKA_NR']) == -1)  //Wenn einer neuer Kassendatensatz angelegt wird
    {
        $Form->Formular_Start();

        $AWIS_KEY2 = $_GET['FKA_NR'];

        $Form->Erstelle_HiddenFeld('FKA_NR', $AWIS_KEY2);

        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href='./autoglas_Main.php?cmdAktion=Details&Seite=Kassendaten' ><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht9100 & 2) != 0);

        $Form->ZeileStart();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KENNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('FKA_KENNUNG', 'D', 10, 200, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_FILID'] . ':', 150, '', '', 'Filialnummer im Format 90 eingeben');
        $Form->Erstelle_TextFeld('FKA_FILID', '', 5, 100, True);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_DATUM'] . ':', 150, '', '', 'Datum im Format 12.08.2009 eingeben');
        $Form->Erstelle_TextFeld('FKA_DATUM', '', 10, 200, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_UHRZEIT'] . ':', 150, '', '', 'Uhrzeit im Format 00151012 eingegeben');
        $Form->Erstelle_TextFeld('FKA_UHRZEIT', '', 7, 100, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BSA'] . ':', 150, '', '', 'Bildschirmarbeitsplatz Beispiel: 02');
        $Form->Erstelle_TextFeld('FKA_BSA', '', 5, 100, True);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_WANR'] . ':', 150, '', '', 'Werkstattauftragsnr Beispiel: 135916');
        $Form->Erstelle_TextFeld('FKA_WANR', '', 8, 600, True);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_AEMNR'] . ':', 150, '', '', 'FirstglassNr Beispiel: 0003480135002');
        $Form->Erstelle_TextFeld('FKA_AEMNR', '', 13, 600, True);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_ATUNR'] . ':', 150);
        $Form->Erstelle_TextFeld('FKA_ATUNR', 'GLAS01', 10, 200, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_MENGE'] . ':', 150, '', '', 'Menge 1 oder -1');
        $Form->Erstelle_TextFeld('FKA_MENGE', '', 7, 100, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BETRAG'] . ':', 150, '', '', 'Betrag im Format 100,00 eingeben');
        $Form->Erstelle_TextFeld('FKA_BETRAG', '', 30, 200, True);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'] . ':', 150, '', '', 'KFZ - Kennzeichen im Format NEWCB400 eingeben');
        $Form->Erstelle_TextFeld('FKA_KFZKENNZ', '', 20, 200, True);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BEMERKUNG'] . ':', 150, '', '', 'Grund,Sachbearbeiter,Datum');
        $Form->Erstelle_TextFeld('FKA_BEMERKUNG', '', 150, 200, True);
        $Form->ZeileEnde();
    }
    $Form->Formular_Ende();
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>