<?php
require_once('../heidler/heidler_funktionen.php');

$pal = new heidler_funktionen();

// da jede Position eine laufende Nummer bekommt muss Key ermittelt werden
foreach($_GET as $key => $value) {
    if(substr($key,0,16) == 'sucNDP_AST_ATUNR') {
        $nr = substr($key,17);
        $pal->erstelleArtBezAjax($value,$nr);
        break;
    }
}

