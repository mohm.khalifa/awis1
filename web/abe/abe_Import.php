<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;        // Zum Cursor setzen
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('ABF', '%');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'Dateiname');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Fehler', 'err_keingueltigesDatum');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht51000 = $AWISBenutzer->HatDasRecht(51000);
if ($Recht51000 == 0) {
    $Form->Fehler_KeineRechte();
}

$Form->SchreibeHTMLCode('<form name=frmImport method=post action=./abe_Main.php?cmdAktion=Import enctype="multipart/form-data">');

$Form->Formular_Start();

if (isset($_POST['cmd_Import_x'])) {
    if (isset($_FILES['Importdatei'])) {
        //Dateiformat pr�fen
        if (strtolower(substr($_FILES['Importdatei']["name"], -4)) != '.csv') {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['ABF']['ABF_IMPORT_ERR_FORMAT']);
            $Form->ZeileEnde();
        } elseif (($fd = fopen($_FILES['Importdatei']['tmp_name'], 'r')) !== false) {
            //Importieren
            $Erg = importVIN($fd);

            //Fehlerausgabe
            if (count($Erg['Fehler']) > 0) {
                $Form->ZeileStart();
                $Form->Hinweistext(implode('<br />', $Erg["Fehler"]));
                $Form->ZeileEnde();
            } else {
                $Form->ZeileStart();
                $Form->Hinweistext($AWISSprachKonserven['ABF']['ABF_IMPORT_ERFOLGREICH']);
                $Form->ZeileEnde();
            }
        }
    }
}

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ':', 150);
$Form->Erstelle_DateiUpload('Importdatei', 310, 30, 20000000);
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
$Form->Schaltflaeche('image', 'cmd_Import', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form');

/**
 * @param $fd resource          Die zu Importierende Datei
 * @return array
 */
function importVIN($fd)
{
    global $AWISBenutzer;
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $TextKonserven = array();
    $TextKonserven[] = array('ABF', '%');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Erg = [];
    $Fehler = [];
    $Commit = true;

    $DB->TransaktionBegin();

    try{
        $Zeile = fgets($fd);
        $count = substr_count($Zeile, ';');
        if ($count == 3) {
            while (!feof($fd)) {
                $Zeile = fgets($fd);
                $Zeile = explode(';', trim($Zeile));

                if (count($Zeile) == 4) {
                    $SQL = 'INSERT INTO ABEFAHRZEUGE (ABF_FAHRGESTELLNR, ABF_MOTORNR, ABF_ATU_NR, ABF_FARBE) VALUES (';
                    $SQL .= $DB->WertSetzen('ABF', 'T', $Zeile[1]) . ', ';
                    $SQL .= $DB->WertSetzen('ABF', 'T', $Zeile[2]) . ', ';
                    $SQL .= $DB->WertSetzen('ABF', 'T', $Zeile[0]) . ', ';
                    $SQL .= $DB->WertSetzen('ABF', 'T', $Zeile[3]) . ')';

                    $DB->Ausfuehren($SQL, 'ABF', true, $DB->Bindevariablen('ABF', true));
                } elseif (count($Zeile) > 0 and $Zeile[0] != '') {
                    $Commit = false;
                    $Fehler[] = $AWISSprachKonserven['ABF']['ABF_IMPORT_ERR_ZEILEN'] . fgets($fd);
                }
            }
        } else {
            $Commit = false;
            $Fehler[] = $AWISSprachKonserven['ABF']['ABF_IMPORT_ERR_SPALTEN'];
        }
    }catch (awisException $e){
        //Im Fehlerfall beim ausf�hren des SQLs darf nicht commitet werden.
        //Hier wird bewusst nich returnt, damit die restlichen Zeilen noch �berpr�ft werden k�nnen.
        $Fehler[] = $AWISSprachKonserven['ABF']['ABF_IMPORT_ERR_SQL'];
        $Commit = false;
    }

    //Soll commitet werden
    if($Commit){
        $DB->TransaktionCommit();
    }else{
        $DB->TransaktionRollback();
    }

    $Erg["Fehler"] = $Fehler;

    return $Erg;
}
