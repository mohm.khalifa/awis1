<?php

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

if(!isset($_GET['BNR']))
{
	die();
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('SGD','%');
$TextKonserven[]=array('SGG','%');
$TextKonserven[]=array('SGA','%');
$TextKonserven[]=array('SGS','%');
$TextKonserven[]=array('SGK','%');
$TextKonserven[]=array('SKS','%');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 20;

$countEintrag = 0;
$abglBetrag = 0.0;

$Ausdruck = new awisAusdruck('P','A4',array('BriefpapierATU_DE_Seite_2.pdf'));

$Ausdruck->NeueSeite(0,1);

if(isset($_GET['BNR']))
{
    $BELEGNR = $_GET['BNR'];

    $SQL = " Select bb.SGS_RNRSGH,bb.SGS_RNRDAT,count(aa.SGA_TYP) AS ANZAHL,SUM(bb.sgs_betrag) AS Summe,SUM(bb.sgs_betraggekuerzt) As DIFFERENZ, ";
    $SQL .= " aa.SGA_TYP from SGHABGLEICH aa INNER JOIN SGHDIENSTLEISTERABGL bb ON aa.SGA_SGS_KEY = bb.SGS_KEY ";
    $SQL .= " INNER JOIN SGHABGLEICHSGRUENDE cc ON aa.SGA_TYP = cc.SGG_KEY WHERE SGS_RNRSGH=".$DB->FeldInhaltFormat('T',$BELEGNR);
    $SQL .= " GROUP BY bb.SGS_RNRSGH,bb.SGS_RNRDAT, aa.sga_typ ORDER BY SGA_TYP ";

    $rsFGK = $DB->RecordSetOeffnen($SQL);

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','B',16);
    $Ausdruck->_pdf->cell(65,6,'Pr�fprotokoll',0,0,'L',0);

    $Zeile = 30;
    $Montagedatum = date('d.m.Y');

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(65,6,'Rechnungsnr: '.$BELEGNR,'',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(250,6,'Erstellt am '.$Montagedatum,'',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','B',20);

    $Ausdruck->_pdf->SetFont('Arial','',12);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'Rechnungsdatum: '.substr($rsFGK->FeldInhalt('SGS_RNRDAT'),0,10),'',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'Automatischer Abgleich','',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(40,6,'Position','B',0,'L',0);
    $Ausdruck->_pdf->cell(100,6,'','B',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,'Betrag','B',0,'R',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);

    while(!$rsFGK->EOF())
    {
      if($rsFGK->FeldInhalt('SGA_TYP') == 1)
      {
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch automatischen Abgleich','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
           
      }
      $rsFGK->DSWeiter();
    }

    $rsFGK->DSErster();

    $Zeile+=20;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'Manueller Abgleich','',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(40,6,'Menge','B',0,'L',0);
    $Ausdruck->_pdf->cell(100,6,'Position','B',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,'Betrag','B',0,'R',0);
    $Zeile+=5;

    while(!$rsFGK->EOF())
    {
      if($rsFGK->FeldInhalt('SGA_TYP') == 2)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / Betrag falsch','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
      }
   
      if($rsFGK->FeldInhalt('SGA_TYP') == 3)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / WANR falsch','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
      }
   
      if($rsFGK->FeldInhalt('SGA_TYP') == 4)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / AUTNR falsch','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
      }
      
      if($rsFGK->FeldInhalt('SGA_TYP') == 5)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / KFZKennz falsch','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
      }

      if($rsFGK->FeldInhalt('SGA_TYP') == 6)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / Sonstiges','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
	  }

     $rsFGK->DSWeiter();
    }

    $rsFGK->DSErster();

    $Zeile+=20;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'Rechnungsk�rzung','',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(40,6,'Menge','B',0,'L',0);
    $Ausdruck->_pdf->cell(100,6,'Position','B',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,'Betrag','B',0,'R',0);
    $Zeile+=5;

    $Differenzbetrag = 0.0;

    while(!$rsFGK->EOF())
    {
      if($rsFGK->FeldInhalt('SGA_TYP') == 7)
      {
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / Kompl. Positionsk�rzung','',0,'L',0);
        
        $Differenzbetrag = (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2','-'.$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
      }

      if($rsFGK->FeldInhalt('SGA_TYP') == 8)
      {
        $Zeile+=5;
        $Differenz = 0.0;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,$rsFGK->FeldInhalt('ANZAHL'),'',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'Positionen durch manuellen Abgleich / Betragsk�rzung','',0,'L',0);

        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$rsFGK->FeldInhalt('SUMME')).' �','',0,'R',0);
        $abglBetrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SUMME'));
        $Differenz = (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('DIFFERENZ'));
        $abglBetrag = (float)$abglBetrag + (float)$Differenz;
        $Zeile+=5;
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(40,6,'','',0,'L',0);
        $Ausdruck->_pdf->cell(100,6,'','',0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',"-".$Differenz).' �','',0,'R',0);
      }

      $rsFGK->DSWeiter();
    }

    $Zeile+=20;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(180,6,'','T',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(30,6,'Gesamtbetrag (Netto):','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$abglBetrag).' �','',0,'R',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Mwst = 0.0;
    $Mwst = $abglBetrag / 100 * 19;
    $Ausdruck->_pdf->cell(30,6,'Mwst(19%):','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$Mwst).' �','',0,'R',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Mwst = 0.0;
    $Mwst = $abglBetrag / 100 * 19;
    $Ausdruck->_pdf->cell(30,6,'Gesamtbetrag (Brutto):','',0,'L',0);
    $abglBetrag = $Mwst + $abglBetrag;
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$abglBetrag).' �','',0,'R',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(30,6,'Gesamtbetrag lt. Papierinkasso:','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$_GET['Rechnungsbetrag'].' �','',0,'R',0);
    $Zeile+=5;
    $Diff = 0.0;
    $Diff = (float)$Differenz + (float)$Differenzbetrag;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(30,6,'Rechnungsk�rzungen:','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$Diff).' �','',0,'R',0);

    $MwstDiff = $Diff / 100 * 19;

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(30,6,'Mwst(19%) Rechnungsk�rz:','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$MwstDiff).' �','',0,'R',0);

    $Betrag = $abglBetrag - ($Diff + $MwstDiff);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(110,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(30,6,'�berweisungsbetrag:','',0,'L',0);
    $Ausdruck->_pdf->cell(40,6,$Form->Format('N2',$Betrag).' �','',0,'R',0);


}

if(isset($_GET['BNR']))
{
    $BELEGNR = $_GET['BNR'];

    $SQL = "Select * from SGHABGLEICH INNER JOIN SGHDIENSTLEISTERABGL ON SGA_SGS_KEY = SGS_KEY WHERE (SGA_TYP = 7 OR SGA_TYP=8) AND SGS_RNRSGH=".$BELEGNR;

    $rsFGK = $DB->RecordSetOeffnen($SQL);
    
    $count = 0;	
    
    while(!$rsFGK->EOF())
    {

    $Spalte = 15;
    $Zeile = 20;

    $Ausdruck->NeueSeite(0,1);
    
    $count++;    
    $SQLAnschrift = "Select * from fachbetriebefilialen";
    $SQLAnschrift.=" inner join fachbetriebe on FFA_FAB_KEY = FAB_KEY AND fab_bereich = ".$DB->FeldInhaltFormat('T','trim-line');
    $SQLAnschrift.= " WHERE FFA_FIL_ID=".$rsFGK->FeldInhalt('SGS_FILNR');

    $rsAnschrift = $DB->RecordSetOeffnen($SQLAnschrift);
    
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','B',16);
    $Ausdruck->_pdf->cell(65,6,$AWISSprachKonserven['SGS']['txt_BelastungsAnzeige'],0,0,'L',0);
    $Ausdruck->_pdf->cell(5,6,'',0,0,'L',0);
    
    if(strlen($count) == 1)
    {
	$Ausdruck->_pdf->cell(35,6,"B".$BELEGNR." - 0".$count,0,0,'L',0);
    }
    else
    {
       $Ausdruck->_pdf->cell(35,6,"B".$BELEGNR." - ".$count,0,0,'L',0);
    }
    $Zeile = 30;
    $Montagedatum = date('d.m.Y');

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(65,6,'Rechnungsnr: '.$BELEGNR,'',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(250,6,'Erstellt am '.$Montagedatum,'',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','B',20);
   
    $Ausdruck->_pdf->SetFont('Arial','',10);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'Rechnungsdatum: '.substr($rsFGK->FeldInhalt('SGS_RNRDAT'),0,10),'',0,'L',0);
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(65,6,'Buchungsvermerk:','',0,'L',0);

    $Zeile+=5;

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',16);
    $Ausdruck->_pdf->cell(180,6,'','',0,'L',0);

    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(65,6, $rsAnschrift->FeldInhalt('FAB_NAME1'),'LTR',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(18,6,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);


    $Ausdruck->_pdf->SetFont('Arial','B',20);

    $Ausdruck->_pdf->SetFont('Arial','',10);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,$rsAnschrift->FeldInhalt('FAB_STRASSE'),'LR',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Firma (3)','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Kreditor','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Steuer','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'gepr�ft','LR',0,'C',0);
    $Ausdruck->_pdf->cell(18,5,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);


    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','LR',0,'L',0);
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->cell(18,6,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);
    
    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,$rsAnschrift->FeldInhalt('FAB_PLZ').' '.$rsAnschrift->FeldInhalt('FAB_ORT'),'LR',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Konto (4)','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Kosten (4)','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Filiale (4)','LR',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'IC (4)','LR',0,'C',0);
    $Ausdruck->_pdf->cell(18,5,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','LR',0,'L',0);
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(15,6,'','LTRB',0,'L',0);
    $Ausdruck->_pdf->cell(18,6,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);

    $Zeile+=5;
    $Ausdruck->_pdf->setXY($Spalte, $Zeile);
    $Ausdruck->_pdf->cell(65,6,'','LRB',0,'L',0);
    $Ausdruck->_pdf->cell(35,6,'','',0,'L',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Kostenart ','LRB',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Projekt (8)','LRB',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'leer','LRB',0,'C',0);
    $Ausdruck->_pdf->SetFont('Arial','',5);
    $Ausdruck->_pdf->cell(15,5,'Monat','LRB',0,'C',0);
    $Ausdruck->_pdf->cell(18,5,'','',0,'L','0');
    $Ausdruck->_pdf->SetFont('Arial','',10);



$Zeile+=20;

$Ausdruck->_pdf->SetFillColor(200,200,200);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_FILNR'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_WANR'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_EINBAUDATUM'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_ATUNR'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_BETRAG'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['SGS']['SGS_KFZKENNZ'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell(35,6,$AWISSprachKonserven['SGS']['SGS_RECHNUNGSNR'],'LTRB',0,'L',1);
$Zeile+=5;

$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->SetFillColor(255,255,255);

$Betrag = 0.0;
$MWSTSATZ = 19;


  $Ausdruck->_pdf->setXY($Spalte, $Zeile);
  $Ausdruck->_pdf->cell(25,6,$rsFGK->FeldInhalt('SGS_FILNR'),'LTRB',0,'L',1);
  $Ausdruck->_pdf->cell(25,6,$rsFGK->FeldInhalt('SGS_WANR'),'LTRB',0,'L',1);
  $Ausdruck->_pdf->cell(25,6,substr($rsFGK->FeldInhalt('SGS_EINBAUDATUM'),0,10),'LTRB',0,'L',1);
  $Ausdruck->_pdf->cell(25,6,$rsFGK->FeldInhalt('SGS_ATUNR'),'LTRB',0,'L',1);

  if($rsFGK->FeldInhalt('SGA_TYP') == 7)
  {
    $Ausdruck->_pdf->cell(25,6,$Form->Format('N2',$rsFGK->FeldInhalt('SGS_BETRAG')),'LTRB',0,'R',1);
    $Betrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SGS_BETRAG'),false);
  }
  elseif($rsFGK->FeldInhalt('SGA_TYP') == 8)
  {
    $Ausdruck->_pdf->cell(25,6,$Form->Format('N2',$rsFGK->FeldInhalt('SGS_BETRAGGEKUERZT')),'LTRB',0,'R',1);
    $Betrag += (float)$DB->FeldInhaltFormat('N2',$rsFGK->FeldInhalt('SGS_BETRAGGEKUERZT'),false);
  }
  else
  {
    $Ausdruck->_pdf->cell(25,6,'','LTRB',0,'R',1);
  }
  $Ausdruck->_pdf->cell(25,6,$rsFGK->FeldInhalt('SGS_KFZKENNZ'),'LTRB',0,'L',1);
  $Ausdruck->_pdf->cell(35,6,$rsFGK->FeldInhalt('SGS_RNRTRM'),'LTRB',0,'L',1);
  
  //Begruendungszeile:
  $SQL = "Select * from SGHBEGRUENDUNG WHERE SBG_SGS_KEY=".$rsFGK->FeldInhalt('SGS_KEY');

  $rsBegruendung = $DB->RecordSetOeffnen($SQL);

  if($rsBegruendung->AnzahlDatensaetze() != 0)
  {
  $Zeile+=6;
  $Ausdruck->_pdf->setXY($Spalte, $Zeile);
  $Ausdruck->_pdf->SetFont('Arial','',10);
  
  $Laenge = strlen($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'));
  
  if($Laenge < 106)
  {
  $Ausdruck->_pdf->cell(185,6,$rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),'LRTB',0,'L',1);
  }
  else
  {
    $Zeilenanzahl = $Laenge / 105;
    
    $y = 0; 
    $lastSpaceZeile = 0; 
    
    if(strlen(substr($Zeilenanzahl,0,2)) == 2)
    {
      $len = substr($Zeilenanzahl,0,1);
      $len = $len +1;
      for($i=0;$i<$len;$i++)
      {
	if($i==0)
	{
	  $lastSpaceZeile = strripos(substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),0,105),' ');
	  $Ausdruck->_pdf->cell(185,6,substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),0,$lastSpaceZeile),'LRTB',0,'L',1);
	  //$y = $lastSpace;	
	}
	else
	{
	 $Zeile+=6;
	 $Ausdruck->_pdf->setXY($Spalte, $Zeile);
         $Ausdruck->_pdf->SetFont('Arial','',10);
	 $y = $y + $lastZeile;
	 $lastSpace = strripos(substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),$y,105),' ');
	 $y = 105;
	 $Differenz = $y - $lastSpace;
	 $y = $y - ($Differenz - 1);
	 $Ausdruck->_pdf->cell(185,6,substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),$y,$lastSpace),'LRTB',0,'L',1);
	}
      
      }
     }
     else
     {
       $len = substr($Zeilenanzahl,0,1);
       $len = $len;
       for($i=0;$i<=$len;$i++)
       {
	 if($i==0)
	 {
	   $lastSpaceZeile = strripos(substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),0,105),' ');
	   $Ausdruck->_pdf->cell(185,6,substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),0,$lastSpaceZeile),'LRTB',0,'L',1);
	 }
	 else
	 {
	  $Zeile+=6;
	  $Ausdruck->_pdf->setXY($Spalte, $Zeile);
          $Ausdruck->_pdf->SetFont('Arial','',10);
	  $y = $y + $lastZeile;
	  $lastSpace = strripos(substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),$y,105),' ');
	  $y = 105;
	  $Differenz = $y - $lastSpace;
	  $Differenz = $Differenz - 2;
	  $y = $y - $Differenz;
	  $Ausdruck->_pdf->cell(185,6,substr($rsBegruendung->FeldInhalt('SBG_BEGRUENDUNG'),$y,$lastSpace),'LRTB',0,'L',1);
	}
      } 
     }

  }
  }

$Zeile+=5;

$Zeile+=5;

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(18,6,'','',0,'L','0');
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(14,6,'Betrag: ','',0,'L',0);
$Ausdruck->_pdf->cell(36,6,$Form->Format('N2',$Betrag).' �','',0,'R',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',5);
$Ausdruck->_pdf->cell(15,5,'','',0,'C',0);
$Ausdruck->_pdf->SetFont('Arial','',5);
$Ausdruck->_pdf->cell(15,5,'','',0,'C',0);
$Ausdruck->_pdf->SetFont('Arial','',5);
$Ausdruck->_pdf->cell(15,5,'','',0,'C',0);
$Ausdruck->_pdf->SetFont('Arial','',5);
$Ausdruck->_pdf->cell(15,5,'','',0,'C',0);
$Ausdruck->_pdf->cell(18,5,'','',0,'L','0');
$Ausdruck->_pdf->SetFont('Arial','',10);

$Mwst = 0.0;
$Ausdruck->_pdf->SetFont('Arial','',10);
$Mwst = (($Betrag * $MWSTSATZ) / 100);
$Ausdruck->_pdf->cell(14,5,'Mwst(19%):','',0,'L',0);
$Ausdruck->_pdf->cell(36,5,$Form->Format('N2',$Mwst).' �','',0,'R',0);

$Zeile +=4;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(15,6,'','',0,'L',0);
$Ausdruck->_pdf->cell(18,6,'','',0,'L','0');
$Ausdruck->_pdf->SetFont('Arial','',10);
$BetragBrutto = 0.0;
$BetragBrutto = $Betrag + $Mwst;
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(14,6,'Betrag brutto:','',0,'L',0);
$Ausdruck->_pdf->cell(36,6,$Form->Format('N2',$BetragBrutto).' �','',0,'R',0);

$Zeile+=20;

$Zeile+=5;

//Footer:

$Ausdruck->_pdf->SetY(-30);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'ATU Auto-Teile-Unger GmbH & Co. KG','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'Pers�nlich haftene Gesellschafterin','',0,'L',0);
$Ausdruck->_pdf->cell(65,10,'Bankverbindung:','',0,'L',0);

$Ausdruck->_pdf->SetY(-26);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'Dr.-Killian-Stra�e 11, 92637 Weiden i.d Opf.','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'ATU Auto-Teile-Unger GmbH, Weiden i.d Opf.','',0,'L',0);
$Ausdruck->_pdf->cell(65,10,'HypoVereinsbank Weiden','',0,'L',0);

$Ausdruck->_pdf->SetY(-22);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'Postfach 2254, 92615 Weiden i.d. Opf.','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'Sitz: Weiden i.d Opf., HRB 745','',0,'L',0);
$Ausdruck->_pdf->cell(65,10,'BLZ 753 200 75, Kto. 348 651 110','',0,'L',0);

$Ausdruck->_pdf->SetY(-18);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'Tel +49 (0)961 306-0, Fax +49 (0)961 306-5672','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'Gesch�ftsf�hrer:','',0,'L',0);
$Ausdruck->_pdf->cell(65,10,'BIC: HYVEDEMM454','',0,'L',0);

$Ausdruck->_pdf->SetY(-14);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'Sitz Weiden i.d Opf., HRA 1312','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'Norbert Scheuch, Manfred Koller,','',0,'L',0);
$Ausdruck->_pdf->cell(65,10,'IBAN: DE81 7532 00750348651110','',0,'L',0);

$Ausdruck->_pdf->SetY(-10);
$Ausdruck->_pdf->SetFont('Arial','',7);
$Ausdruck->_pdf->cell(70,10,'UST-Id Nr.: DE134043104','',0,'L',0);
$Ausdruck->_pdf->cell(70,10,'','',0,'L',0); // weiterer GF

  $rsFGK->DSWeiter();
}

$Ausdruck->Anzeigen();


}

?>