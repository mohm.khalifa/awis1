<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('OEN', '%');
$TextKonserven[] = array('LIE', 'LIE_NAME1');
$TextKonserven[] = array('HER', 'HER_BEZEICHNUNG');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $SchnellanlagePos = $AWISBenutzer->ParameterLesen('Artikelstamm_SchnellAnlagePosition');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht454 = $AWISBenutzer->HatDasRecht(454);            // Recht f�r die Lieferantenartikel
    if ($Recht454 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (($Recht454 & 16) and isset($_GET['TEI'])) {
        $SQL = 'UPDATE teileinfos';
        $SQL .= ' SET TEI_USER = ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
        $SQL .= ' , TEI_USERDAT=SYSDATE';
        $SQL .= ' WHERE TEI_KEY = 0' . $DB->FeldInhaltFormat('N0', $_GET['TEI']);

        $DB->Ausfuehren($SQL);
    }

    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $FeldBreiten['OEN_NUMMER'] = 200;
    $FeldBreiten['HER_BEZEICHNUNG'] = 190;
    $FeldBreiten['HER_BEZEICHNUNG_LAENGE'] = 15;
    $FeldBreiten['OEN_BEMERKUNG'] = 350;
    $FeldBreiten['OEN_BEMERKUNG_LAENGE'] = 60;

    $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);

    $Bedingung = '';
    $BindeVariablen = array();
    $BindeVariablen['var_N0_tei_key1'] = $AWIS_KEY1;

    $SQL = 'SELECT OENummern.*, TEI_USER, TEI_USERDAT, TEI_KEY, HER_BEZEICHNUNG';
    $SQL .= ', (SELECT tii_wert FROM TeileInfosInfos WHERE TII_TEI_KEY = TEI_KEY AND TII_ITY_KEY = 5 AND ROWNUM = 1) AS TII_5';
    $SQL .= ' FROM OENummern';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY2 = OEN_KEY and TEI_KEY1 = :var_N0_tei_key1';
    //PG: 16.07.15 - Aus INNER JOIN einen LEFT JOIN gemacht,  weil zugeordnete OE-Nummern ohne Hersteller nicht 
    //angezeigt werden.  
    $SQL .= ' LEFT JOIN Hersteller ON OEN_HER_ID = HER_ID';

    if (isset($_GET['OEN_KEY'])) {
        $BindeVariablen['var_N0_oen_key'] = $_GET['OEN_KEY'];
        $Bedingung .= ' AND OEN_KEY = :var_N0_oen_key';
    }
    if ($AWIS_KEY2 != 0) {
        $BindeVariablen['var_N0_oen_key'] = $AWIS_KEY2;
        $Bedingung .= ' AND OEN_KEY = :var_N0_oen_key';
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['OENSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['OENSort']);
    } else {
        $SQL .= ' ORDER BY HER_BEZEICHNUNG, OEN_NUMMER';
    }

    $Form->DebugAusgabe(1, $SQL, $BindeVariablen);
    $rsOEN = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    if(isset($_GET['kopieren'])){

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('ATU Nr. ', 130);
        $Form->AuswahlBox('ATUNrOENr','oenrkopieren','','artikelstamm_oenummern_kopieren','',100,8,'','T',true);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->AuswahlBoxHuelle('oenrkopieren');
        $Form->ZeileEnde();



    }
    elseif (!isset($_GET['OEN_KEY']))                    // Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        if (($Recht454 & 4)) {
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&OEN_KEY=0', 'g');
            $Icons[] = array('einkaufswagen', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&kopieren=1', 'k');

            $Schaltflaechen =array(array('/bilder/cmd_dsloeschen.png','','close',''),array('/bilder/cmd_speichern.png','cmdSpeichern','post',''));

            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=OEN_NUMMER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_NUMMER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_NUMMER'], $FeldBreiten['OEN_NUMMER'], '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=HER_BEZEICHNUNG' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'HER_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HER']['HER_BEZEICHNUNG'], $FeldBreiten['HER_BEZEICHNUNG'], '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=OEN_MUSTER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_MUSTER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_MUSTER'], 75, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=OEN_BEMERKUNG' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_BEMERKUNG'], $FeldBreiten['OEN_BEMERKUNG'], '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=TEI_USER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'TEI_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USER'], 100, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=TEI_USERDAT' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'TEI_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USERDAT'], 112, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern';
        $Link .= '&OENSort=OEN_AKTNUMMER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_AKTNUMMER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_AKTNUMMER'], 68, '', $Link);

        $Form->ZeileEnde();
        if($SchnellanlagePos==1){
            _Schnellanlage($Form,$Recht454,$FeldBreiten, $AWISSprachKonserven);
        }
        $Text = '';
        $DS = 0;
        while (!$rsOEN->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (($Recht454 & 2))    // �ndernrecht
            {
                $Icons[] = array('edit', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&OEN_KEY=' . $rsOEN->FeldInhalt('OEN_KEY'));
            }

            $Form->Erstelle_ListeIcons($Icons, 19, ($DS % 2));

            if (($Recht454 & 4))    // L�schenrecht
            {
                $Form->Erstelle_ListenLoeschPopUp('./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&OENNummerPopUpDel=' . $rsOEN->FeldInhalt('TEI_KEY') . '&LAR_KEY=' . $rsOEN->FeldInhalt('TEI_KEY') . '&Key=' . $AWIS_KEY1,
                    $rsOEN->FeldInhalt('TEI_KEY'), 19, '', '', ($DS % 2));
            } else {
                $Form->Erstelle_ListenFeld('', '', 12, 19, false, ($DS % 2), '', '', 'T', 'L');
            }
            $Form->Erstelle_ListenFeld('*OEN_NUMMER', $rsOEN->FeldInhalt('OEN_NUMMER'), 10, $FeldBreiten['OEN_NUMMER'], false, ($DS % 2), '', '', 'T', 'L',
                $rsOEN->FeldInhalt('OEN_NUMMER'));
            $Text = (strlen($rsOEN->FeldInhalt('HER_BEZEICHNUNG')) > $FeldBreiten['HER_BEZEICHNUNG_LAENGE'] - 2?substr($rsOEN->FeldInhalt('HER_BEZEICHNUNG'), 0,
                    $FeldBreiten['HER_BEZEICHNUNG_LAENGE'] - 2) . '...':$rsOEN->FeldInhalt('HER_BEZEICHNUNG'));
            $Form->Erstelle_ListenFeld('*OEN_HER_ID', $Text, 10, $FeldBreiten['HER_BEZEICHNUNG'], false, ($DS % 2), '', '', 'T', 'L', $rsOEN->FeldInhalt('HER_BEZEICHNUNG'));
            $Muster = '--';
            foreach ($JaNeinFeld AS $JaNeinAnzeige) {
                if (substr($JaNeinAnzeige, 0, 1) == $rsOEN->FeldInhalt('OEN_MUSTER')) {
                    $Muster = substr($JaNeinAnzeige, 2);
                    break;
                }
            }

            $Form->Erstelle_ListenFeld('*OEN_MUSTER', $Muster, 10, 75, false, ($DS % 2), '', '', 'T', 'L', $AWISSprachKonserven['OEN']['ttt_OEN_MUSTER']);
            $Text = (strlen($rsOEN->FeldInhalt('OEN_BEMERKUNG')) > $FeldBreiten['OEN_BEMERKUNG_LAENGE'] - 2?substr($rsOEN->FeldInhalt('OEN_BEMERKUNG'), 0,
                    $FeldBreiten['OEN_BEMERKUNG_LAENGE'] - 2) . '...':$rsOEN->FeldInhalt('OEN_BEMERKUNG'));
            $Form->Erstelle_ListenFeld('*OEN_BEMERKUNG', $Text, 10, $FeldBreiten['OEN_BEMERKUNG'], false, ($DS % 2), '', '', 'T', 'L', $rsOEN->FeldInhalt('OEN_BEMERKUNG'));

            $Link = '';
            if (($Recht454 & 16) == 16 AND $rsOEN->FeldInhalt('TEI_USER') == 'Import') {
                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&TEI=' . $rsOEN->FeldInhalt('TEI_KEY');
            }
            $Form->Erstelle_ListenFeld('*OEN_USER', $rsOEN->FeldInhalt('TEI_USER'), 10, 100, false, ($DS % 2), '', $Link, 'T', 'L', $rsOEN->FeldInhalt('OEN_USER'));
            $Form->Erstelle_ListenFeld('*OEN_USERDAT', $rsOEN->FeldInhalt('TEI_USERDAT'), 10, 92, false, ($DS % 2), '', '', 'D', 'L',
                $Form->Format('D', $rsOEN->FeldInhalt('OEN_USERDAT')));
            if ($rsOEN->FeldInhalt('TII_5') != '') {
                $Form->Erstelle_HinweisIcon('icon_info', 16, ($DS % 2), $rsOEN->FeldInhalt('TII_5'));
            } else {
                $Form->Erstelle_ListenFeld('*FILLER', '', 0, 16, false, ($DS % 2));
            }
            $Form->Erstelle_ListenFeld('*FILLER', '', 0, 20, false, ($DS % 2));
            $Form->Erstelle_ListenCheckbox('OEN_AKTNUMMER_' . $rsOEN->FeldInhalt('OEN_KEY'), $rsOEN->FeldInhalt('OEN_AKTNUMMER'), 20, (($Recht454 & 2) == 2), 1, '', '', '', '',
                ($DS % 2));
            $Form->Erstelle_ListenFeld('*FILLER', '', 0, 20, false, ($DS % 2));

            $Form->ZeileEnde();
            $DS++;

            $rsOEN->DSWeiter();
        }

        if($SchnellanlagePos==0){
            _Schnellanlage($Form,$Recht454,$FeldBreiten, $AWISSprachKonserven);
        }

        $Form->Formular_Ende();
    }else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtOEN_KEY type=hidden value=0' . $rsOEN->FeldInhalt('OEN_KEY') . '>';
        echo '<input name=txtOEN_IMQ_ID type=hidden value=' . ($rsOEN->FeldInhalt('OEN_IMQ_ID') == ''?$rsOEN->FeldInhalt('OEN_IMQ_ID'):'4') . '>';
        $Form->Erstelle_HiddenFeld('TEI_KEY', $rsOEN->FeldInhalt('TEI_KEY'));

        $AWIS_KEY2 = $rsOEN->FeldInhalt('OEN_KEY');
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=OENummern&OENListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsOEN->FeldInhalt('OEN_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsOEN->FeldInhalt('OEN_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $EditModus = ($rsOEN->FeldInhalt('OEN_USER') != ''?($rsOEN->FeldInhalt('OEN_USER') != 'WWS'?($Recht454 & 6):false):($Recht454 & 6));

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_NUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('OEN_NUMMER', $rsOEN->FeldInhalt('OEN_NUMMER'), 20, 260, $EditModus);

        $AenderRecht = (($Recht454 & 32) >= 32);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_AKTNUMMER'] . ':', 50);
        $Form->Erstelle_Checkbox('OEN_AKTNUMMER', $rsOEN->FeldInhalt('OEN_AKTNUMMER'), 20, $AenderRecht, '1');

        $AWISCursorPosition = ($EditModus?'txtOEN_NUMMER':$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_HER_ID'] . ':', 150);
        $SQL = 'SELECT HER_ID, HER_BEZEICHNUNG FROM Hersteller WHERE HER_TYP = 1 ORDER BY HER_BEZEICHNUNG';
        $Form->Erstelle_SelectFeld('OEN_HER_ID', $rsOEN->FeldInhalt('OEN_HER_ID'), 200, $EditModus, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_MUSTER'] . ':', 150);
        $Form->Erstelle_SelectFeld('OEN_MUSTER', $rsOEN->FeldInhalt('OEN_MUSTER'), 70, $EditModus, '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_BEMERKUNG'] . ':', 150);
        $Form->Erstelle_Textarea('OEN_BEMERKUNG', $rsOEN->FeldInhalt('OEN_BEMERKUNG'), 200, 50, 3, $EditModus);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Trennzeile('L');
        $Form->ZeileEnde();

        $BindeVariablen = array();
        $BindeVariablen['var_N0_tei_key'] = $rsOEN->FeldInhalt('TEI_KEY');

        $SQL = 'SELECT *';
        $SQL .= ' FROM TeileInfosInfos';
        $SQL .= ' WHERE TII_TEI_KEY = :var_N0_tei_key';
        $SQL .= ' AND TII_ITY_KEY = 5';
        $rsTII = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

        $Form->DebugAusgabe(1, $SQL, $BindeVariablen);
        // Eine Zeile mit der Info schreiben (falls Rechte vorhanden sind)
        $Form->Erstelle_InfoFeld('TII', $rsTII->FeldInhalt('TII_WERT'), 5, $rsTII->FeldInhalt('TII_KEY'), $rsOEN->FeldInhalt('TEI_KEY'), 150, $EditModus, '');

        $SQL = '';

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151355");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151356");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function _Schnellanlage($Form, $Recht454, $FeldBreiten, $AWISSprachKonserven){
     if (($Recht454 & 4) == 4) {
         $Form->ZeileStart();
         $Icons = array();
         $Icons[] = array('add', 'POST~OEN');
         $Form->Erstelle_HiddenFeld('OEN_KEY', 0);
         $Form->Erstelle_ListeIcons($Icons, 38, 1);
         $Form->Erstelle_ListenFeld('OEN_NUMMER', '', 25, $FeldBreiten['OEN_NUMMER'], true,1, '', '', 'T', 'L', '');
         $AWISCursorPosition = 'txtOEN_NUMMER';
         $SQL = 'SELECT HER_ID, HER_BEZEICHNUNG FROM Hersteller WHERE HER_TYP = 1 ORDER BY HER_BEZEICHNUNG';
         $Form->Erstelle_SelectFeld('~OEN_HER_ID', '', $FeldBreiten['HER_BEZEICHNUNG'] + 20, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
         $Form->Erstelle_ListenFeld('*OEN_MUSTER', '', 10, 61, false, 1, '', '', 'T', 'L', $AWISSprachKonserven['OEN']['ttt_OEN_MUSTER']);
         $Form->Erstelle_ListenFeld('OEN_BEMERKUNG', '', 60, $FeldBreiten['OEN_BEMERKUNG'], true, 1, '', '', 'T', 'L', '');
         $Form->Erstelle_ListenFeld('*OEN_USER', '', 10, 100, false, 1, '', '', 'T', 'L','');
         $Form->Erstelle_ListenFeld('*OEN_USERDAT', '', 10, 92, false, 1, '', '', 'D', 'L',
             $Form->Format('D', ''));
         $Form->Erstelle_ListenFeld('*FILLER', '', 0, 40, false, 1);
         $Form->Erstelle_ListenCheckbox('OEN_AKTNUMMER', '', 16, true, 1, '', '', '', '', 1);
         $Form->Erstelle_ListenFeld('*FILLER', '', 0, 20, false, 1);

         $Form->ZeileEnde();
     }
}
?>