<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['txtRAV_RAHMENVERTR']))
{
    var_dump($_GET);
    die();
}


try
{
    $DS=0;

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('RAV','%');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Fehler = '';

        $SQL = 'select *';
        $SQL .= ' FROM GSVRAHMENVERTRAEGE V';
        $SQL .= ' inner join GSVLIEFERANTEN L ON GSR_LIEFERANT = GSL_KEY';
        $SQL .= ' WHERE gsr_vertragsart = '.$DB->WertSetzen('GSR','T',$_GET['txtGSR_VERTRAGSART']).'';
        $SQL .= ' AND gsr_key = '.$DB->WertSetzen('GSR','N0',$_GET['txtRAV_RAHMENVERTR']).'';

         $rsGSR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSR', true));

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIELIEFERANT'],560, 'font-weight:bolder;color:blue');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEITEN'],100, 'font-weight:bolder;color:blue');
        $Form->Trennzeile('O');
        $Form->ZeileEnde();


        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'] . ':', 160, '', '');
        $Form->Erstelle_TextFeld('!GSL_FIRMA', $rsGSR->FeldInhalt('GSL_FIRMA'), 30, 400, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEIT'].':',250);
        $Form->Erstelle_TextFeld('!GSL_VERTRAGSLAUFZEIT',$Form->Format('D',$rsGSR->FeldInhalt('GSR_VERTRAGSLAUFZEIT')),7,400,false,'','','','D');
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_VORNAME', $rsGSR->FeldInhalt('GSL_VORNAME'), 30, 400, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGZUM'].':',250);
        $Form->Erstelle_TextFeld('!GSL_KUENDIGUNGZUM',$Form->Format('D',$rsGSR->FeldInhalt('GSR_KUENDIGUNG')),7,400,false,'','','','D');
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_ANSPRECHPARTNER', $rsGSR->FeldInhalt('GSL_ANSPRECHPARTNER'), 30, 400, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGSFRIST'].':',250);
        $Form->Erstelle_TextFeld('!GSL_KUENDIGUNGSFRIST',$rsGSR->FeldInhalt('GSR_KUENDIGUNGSFRIST'),20,400,false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 400, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSSTATUS'].':',250);
        $Form->Erstelle_Checkbox('GSL_VETRAGSSTATUS',$rsGSR->FeldInhalt('GSR_VERTRAGSSTATUS'),50,false,true);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GEKUENDIGT'],180);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_PLZ', $rsGSR->FeldInhalt('GSL_PLZ'), 30, 60, false);
        $Form->Erstelle_TextFeld('!GSL_ORT', $rsGSR->FeldInhalt('GSL_ORT'), 30, 340, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PREISANPASSUNG'].':',250);
        $Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_KUNDE',$rsGSR->FeldInhalt('GSR_PREISANPASSUNG_KUNDE'),50,false,true);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EVU'].':',180);
        $Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_EVU',$rsGSR->FeldInhalt('GSR_PREISANPASSUNG_LIEFERANT'),50,false,true);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_TELNR_FIRMA', $rsGSR->FeldInhalt('GSL_TELNR_FIRMA'), 30, 250, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_FAXNR', $rsGSR->FeldInhalt('GSL_FAXNR'), 30, 250, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'] . ':', 160);
        $Form->Erstelle_TextFeld('!GSL_MAIL', $rsGSR->FeldInhalt('GSL_MAIL'), 30, 300, false);
        $Form->ZeileEnde();

}
catch (Exception $ex)
{
    $Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>