/**
 * Created by gebhardt_p on 13.09.2016.
 **/

$(document).ready(function () {
    $('#txtERP_MWST_KZ').on('change', function () {
        MwSTBerechnen();
    });

    $('#txtERP_BETRAG').on('change', function () {
        MwSTBerechnen();
    });

    $('#txtPOS_TYP').on('change', function () {
        if ($('#txtPOS_TYP option:selected').val() > 1) {
            $('#sucERP_AST_ATUNR').val($('#txtPOS_TYP option:selected').text());
            $('#sucERP_AST_ATUNR').attr('readonly', 'readonly');
        }
        else {
            $('#sucERP_AST_ATUNR').removeAttr('readonly');
            $('#sucERP_AST_ATUNR').val('');
        }
        key_ERP_AST_ATUNR(event);
    });

    $('#txtERP_EST_KEY').on('change', function () {
        MwSTBerechnen();
    });

    $('#txtERP_MWST').ready(function () {
        MwSTBerechnen();
    });

    function MwSTBerechnen() {
        $("#txtERP_BETRAG").val(function(i, v) {return v.replace(",",".");}).val();
        var Betrag = $('#txtERP_BETRAG').val();
        if ($('#txtERP_EST_KEY').is('select')) {
            var MwSt = $('#txtERP_EST_KEY option:selected').text();
        } else {
            var MwSt = $('#txtERP_MWST').val();
        }
        if ($.isNumeric(MwSt)) {
            var Gesamtprozent = 100 + parseFloat(MwSt);
            var Steuer = (Betrag / Gesamtprozent ) * 100;

            Steuer = Steuer.toFixed(2);
            $('#txtERP_MWST_WERT').val(Betrag - Steuer);
            $('#txtERP_BETRAG_NETTO').val(Steuer);
        }
    }
});