<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_GET['button']))
	{
		$SQL = 'UPDATE GSVRAHMENVERTRAEGE SET';
		$SQL .= '  GSR_AKTIV='.$DB->WertSetzen('GSR', 'N0',$_GET['Aktiv']==1?0:1,false);
		$SQL .= ', GSR_user='.$DB->WertSetzen('GSR', 'T',$AWISBenutzer->BenutzerName());
		$SQL .= ', GSR_userdat=sysdate';
		$SQL .= ' WHERE GSR_KEY='.$DB->WertSetzen('GSR', 'N0',$_GET['button']);
		$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSR', true));
	}

	if(isset($_POST['txtGSR_KEY']))
	{
		if(floatval($_POST['txtGSR_KEY'])=='-1')
		{
			$SQL = 'INSERT INTO GSVRAHMENVERTRAEGE(';
			$SQL .= 'GSR_RAHMENVERTR,GSR_VERTRAGSART,GSR_LIEFERANT,GSR_VERTRAGSLAUFZEIT,GSR_KUENDIGUNG,GSR_KUENDIGUNGSFRIST,GSR_HTPREIS_A,GSR_VERTRAGSSTATUS,GSR_PREISANPASSUNG_KUNDE,GSR_PREISANPASSUNG_LIEFERANT';
			$SQL .= ',GSR_USER,GSR_USERDAT)';
			$SQL .= 'VALUES (';
			$SQL .= ' '.$DB->WertSetzen('GSR', 'T',$_POST['txtRAV_RAHMENVERTR']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'N0',$_POST['txtRAV_VERTRAGSART']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'N0',$_POST['txtRAV_LIEFERANT']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'D',$_POST['txtGSL_VERTRAGSLAUFZEIT']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'D',$_POST['txtGSL_KUENDIGUNGZUM']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'T',$_POST['txtGSL_KUENDIGUNGSFRIST']);
			$SQL .= ','.$DB->WertSetzen('GSR', 'N2',$_POST['txtGSL_HTPREIS']);
			if(isset($_POST['txtGSL_VETRAGSSTATUS']))
			{
				$SQL .= ',-1';
			}
			else
			{
				$SQL .= ',0';
			}
			if(isset($_POST['txtGSL_PREISANPASSUNG_KUNDE']))
			{
				$SQL .= ',-1';
			}
			else
			{
				$SQL .= ',0';
			}
			if(isset($_POST['txtGSL_PREISANPASSUNG_EVU']))
			{
				$SQL .= ',-1';
			}
			else
			{
				$SQL .= ',0';
			}
			$SQL .= ','.$DB->WertSetzen('GSR', 'T',$AWISBenutzer->BenutzerName(),true);
			$SQL .= ', SYSDATE)';

			$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSR', true));
		}
		else
		{
			$SQL = 'UPDATE GSVRAHMENVERTRAEGE SET';
			$SQL .= ' GSR_RAHMENVERTR ='.$DB->WertSetzen('GSR', 'T',$_POST['txtRAV_RAHMENVERTR'],false);
			$SQL .= ', GSR_VERTRAGSART='.$DB->WertSetzen('GSR', 'N0',$_POST['txtRAV_VERTRAGSART'],false);
			$SQL .= ', GSR_LIEFERANT='.$DB->WertSetzen('GSR', 'N0',$_POST['txtRAV_LIEFERANT'],false);
			$SQL .= ', GSR_VERTRAGSLAUFZEIT='.$DB->WertSetzen('GSR', 'D',$_POST['txtGSL_VERTRAGSLAUFZEIT'],false);
			$SQL .= ', GSR_KUENDIGUNG='.$DB->WertSetzen('GSR', 'D',$_POST['txtGSL_KUENDIGUNGZUM'],true,null);
			$SQL .= ', GSR_KUENDIGUNGSFRIST='.$DB->WertSetzen('GSR', 'T',$_POST['txtGSL_KUENDIGUNGSFRIST'],false);
			$SQL .= ', GSR_HTPREIS_A='.$DB->WertSetzen('GSR', 'N2',$_POST['txtGSL_HTPREIS'],false);
			if(isset($_POST['txtGSL_AKTIV'])) {
				$SQL .= ', GSR_AKTIV=' .$DB->WertSetzen('GSR', 'N0', $_POST['txtGSL_AKTIV'], false);
			}
			if(isset($_POST['txtGSL_VETRAGSSTATUS']))
			{
				$SQL .= ', GSR_VERTRAGSSTATUS=-1';
			}
			else
			{
				$SQL .= ', GSR_VERTRAGSSTATUS=0';
			}
			if(isset($_POST['txtGSL_PREISANPASSUNG_KUNDE']))
			{
				$SQL .= ', GSR_PREISANPASSUNG_KUNDE=-1';
			}
			else
			{
				$SQL .= ', GSR_PREISANPASSUNG_KUNDE=0';
			}
			if(isset($_POST['txtGSL_PREISANPASSUNG_EVU']))
			{
				$SQL .= ', GSR_PREISANPASSUNG_LIEFERANT=-1';
			}
			else
			{
				$SQL .= ', GSR_PREISANPASSUNG_LIEFERANT=0';
			}
			$SQL .= ', GSR_user='.$DB->WertSetzen('GSR', 'T',$AWISBenutzer->BenutzerName());
			$SQL .= ', GSR_userdat=sysdate';
			$SQL .= ' WHERE GSR_KEY='.$DB->WertSetzen('GSR','N0',$_POST['txtGSR_KEY']);
			$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSR', true));
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>