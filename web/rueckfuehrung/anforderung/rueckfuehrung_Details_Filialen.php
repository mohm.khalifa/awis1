<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $RUECKFUEHRUNGSTYP;
global $AWISCursorPosition;
global $LAND;

try
{

$TextKonserven = array();
$TextKonserven[]=array('RFL','%');
$TextKonserven[]=array('FIL','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_FehlerRueckfuehrungstyp');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('Wort','Filialen');
$TextKonserven[]=array('Wort','Filiale');
$TextKonserven[]=array('Wort','Lager');
$TextKonserven[]=array('Wort','Filialebene');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$FilialebeneObj = new awisFilialEbenen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3900 = $AWISBenutzer->HatDasRecht(3900);
if($Recht3900==0)
{
	$Form->Fehler_KeineRechte();
}

if ($RUECKFUEHRUNGSTYP == 2)
{
	$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_FehlerRueckfuehrungstyp']);
	die();
}

$EditRecht=(($Recht3900&6)!=0);

$AWIS_KEY2_NEW=1;
$Form->DebugAusgabe(1,'LAND:'.$LAND);
//***********************************
//Rückführungsländer anzeigen
//***********************************

$BindeVariablen=array();
$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);

$SQL = 'SELECT DISTINCT FIL_LAN_WWSKENN, count(*) as Anzahl';
$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGEN';
$SQL .= ' LEFT JOIN RUECKFUEHRUNGSANFFILIALEN ON RFL_RFA_KEY = RFA_KEY';
$SQL .= ' LEFT JOIN FILIALEN ON FIL_ID = RFL_FIL_ID';
$SQL .= ' WHERE RFA_KEY=:var_N0_rfa_key';

if(isset($_GET['LAND']))
{
	//$AWIS_KEY2 = floatval($_GET['RFL_KEY']);
	$LAND = $_GET['LAND'];
	if ($_GET['LAND']=='0')
	{
		$AWIS_KEY2='-1';
	}
}

if(isset($LAND) AND $LAND != '0' AND !isset($_GET['RFLListe']))
{
	$BindeVariablen['var_T_land']=$LAND;
	$SQL .= ' AND FIL_LAN_WWSKENN = :var_T_land';
}

$SQL .= ' GROUP BY FIL_LAN_WWSKENN HAVING COUNT(FIL_LAN_WWSKENN) >= 1';

if(!isset($_GET['LandSort']))
{
	$SQL .= ' ORDER BY FIL_LAN_WWSKENN';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['LandSort']);
}
$Form->DebugAusgabe(1,$SQL);
$rsRFL = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

if($AWIS_KEY2=='-1')		//Neues Land hinzufügen
{
	$Form->Formular_Start();
	//echo "haaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	$Form->Erstelle_HiddenFeld('FIL_LAN_WWSKENN', '0');
	$Form->Erstelle_HiddenFeld('RFL_KEY', '0');
	$Form->Erstelle_HiddenFeld('RFL_RFA_KEY', '0'.$AWIS_KEY1);

	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&RFLListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Form->InfoZeile($Felder,'');		
	
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
	$Form->ZeileStart();
	$FilialEbenePfad=$FilialebeneObj->FilialEbenenListe('',awisFilialEbenen::FILIALEBENELISTE_FELD_PFAD,awisFilialEbenen::FILIALEBENELISTE_ERGTYP_SELECT);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filialebene'].':',200);
	$Form->Erstelle_SelectFeld('RFL_FILIALEBENE','',200,$EditRecht,'',$OptionBitteWaehlen,'-1','','',$FilialEbenePfad);	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$FilialEbeneFiliale=$FilialebeneObj->FilialenEinerEbene(0,'',awisFilialEbenen::FILIALEBENELISTE_ERGTYP_SELECT);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filiale'].':',200);
	$Form->Erstelle_SelectFeld('RFL_FILIALE','',200,$EditRecht,'',$OptionBitteWaehlen,'-1','','',$FilialEbeneFiliale);	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Lager'].':',200);
	$Form->Erstelle_SelectFeld('RFL_LAGER','',200,$EditRecht,'SELECT DISTINCT FIL_LAGERKZ, DECODE(FIL_LAGERKZ,\'N\',\'Weiden\',\'L\',\'Werl\',\'Unbekannt\') FROM FILIALEN WHERE FIL_LAGERKZ IS NOT NULL',$OptionBitteWaehlen,'-1','','','');	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFL']['RFL_FILIALLISTE'].':',200);
	$Form->Erstelle_TextFeld('RFL_FILIALLISTE','',300,300,$EditRecht,'','','','T');
	$Form->ZeileEnde();
	
	//-->PG: 10.02.2015 - Checkbox ob Franchiser hinzugefügt werden soll. 
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFL']['RFL_FRANCHISE'].':',200);
	$Form->Erstelle_Checkbox('RFL_FRANCHISE', -1, 15, true);
	$Form->ZeileEnde();
	//<--PG: 10.02.2015 - Checkbox ob Franchiser hinzugefügt werden soll.
	$Form->Formular_Ende();
	
}
elseif($LAND=='' OR $LAND=='0' OR isset($_GET['RFLListe']))					// Liste anzeigen
{
	$Form->Formular_Start();
	$Form->ZeileStart();
	//$LAND='0';

	if(($Recht3900&6)>0)
	{
		$Icons[] = array('new','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&RFL_KEY=0&LAND=0');
		$Form->Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen';
	$Link .= '&LandSort=FIL_LAN_WWSKENN'.((isset($_GET['LandSort']) AND ($_GET['LandSort']=='FIL_LAN_WWSKENN'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_LAN_WWSKENN'],100,'',$Link);		
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Filialen'],100,'',$Link);		
	$Form->ZeileEnde();
	$DS = 1;
	
	while(!$rsRFL->EOF())
	{
		$Form->ZeileStart();
		$Icons = array();
		
		if(($Recht3900&2)>0)	// Ändernrecht
		{				
			$Icons[] = array('edit','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$rsRFL->FeldInhalt('FIL_LAN_WWSKENN'));
		}
	
		if(($Recht3900&8)>0)	// Löschen
		{
			$Icons[] = array('delete','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$rsRFL->FeldInhalt('FIL_LAN_WWSKENN').'&Del='.$AWIS_KEY1);
		}
		$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

		$Form->Erstelle_ListenFeld('RFL_FIL_LAN_WWSKENN',$rsRFL->FeldInhalt('FIL_LAN_WWSKENN'),20,100,false,($DS%2),'','','T');	
		$Form->Erstelle_ListenFeld('ANZAHL',$rsRFL->FeldInhalt('ANZAHL'),20,100,false,($DS%2),'','','T');	

		$Form->ZeileEnde();
		$rsRFL->DSWeiter();
		$DS++;
	}

	$Form->Formular_Ende();
}
else 		// Ein oder kein Land anzeigen und die dazugehörigen Filialen
{
	$Form->Formular_Start();	

	$LAND = $rsRFL->FeldInhalt('FIL_LAN_WWSKENN');
	$Form->DebugAusgabe(1,$LAND);
	
	$EditRecht=false;
	
	
	// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&RFLListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Form->InfoZeile($Felder,'');

	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_LAN_WWSKENN'].':',200);
	$SQL = "SELECT DISTINCT LAN_WWSKENN, LAN_LAND";
	$SQL.= " FROM FILIALEN";
	$SQL.= " LEFT JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN";
	$SQL.= " LEFT JOIN RUECKFUEHRUNGSANFFILIALEN ON RFL_FIL_ID = FIL_ID AND RFL_RFA_KEY = 0".intval($AWIS_KEY1);
	$SQL.= " ORDER BY LAN_WWSKENN";
	$Form->Erstelle_SelectFeld('FIL_LAN_WWSKENN',$rsRFL->FeldInhalt('FIL_LAN_WWSKENN'),200,$EditRecht,$SQL,$OptionBitteWaehlen);	
	$AWISCursorPosition='txtFIL_LAN_WWSKENN';
	$Form->ZeileEnde();
	$Form->Formular_Ende();	
		
	$Form->ZeileStart();
	$Form->Trennzeile();
	$Form->ZeileEnde();
	
	
	//Alle Filialen zu dem Land obigen anzeigen
	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);
	$BindeVariablen['var_T_land']=$LAND;
	
	$SQL = 'SELECT *';
	$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGEN';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSANFFILIALEN ON RFL_RFA_KEY = RFA_KEY';
	$SQL .= ' LEFT JOIN FILIALEN ON FIL_ID = RFL_FIL_ID';
	$SQL .= ' WHERE RFA_KEY=:var_N0_rfa_key';
	$SQL .= ' AND FIL_LAN_WWSKENN = :var_T_land';
	
	if(!isset($_GET['RFLSort']))
	{
		$SQL .= ' ORDER BY FIL_ID';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['RFLSort']);
	}
	
	$rsRFL = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	
	if(!isset($_GET['RFL_KEY']))					// Filialen als Liste anzeigen
	{
		$Form->Formular_Start();
		$Form->ZeileStart();
	
		if(($Recht3900&6)>0)
		{
			$Icons[] = array('new','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$rsRFL->FeldInhalt('FIL_LAN_WWSKENN').'&RFL_KEY=0');
			$Form->Erstelle_ListeIcons($Icons,54,-1);
		}
	
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$LAND;
		$Link .= '&RFLSort=RFL_FIL_ID'.((isset($_GET['RFLSort']) AND ($_GET['RFLSort']=='RFL_FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFL']['RFL_FIL_ID'],100,'',$Link);		
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$LAND;
		$Link .= '&RFLSort=FIL_BEZ'.((isset($_GET['RFLSort']) AND ($_GET['RFLSort']=='FIL_BEZ'))?'~':'');	
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFL']['RFL_FIL_BEZ'],200,'',$Link);		
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$LAND;
		$Link .= '&RFLSort=FIL_LAGERKZ'.((isset($_GET['RFLSort']) AND ($_GET['RFLSort']=='FIL_LAGERKZ'))?'~':'');	
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_LAGERKZ'],75,'',$Link);		
		$Form->ZeileEnde();	
		$DS = 1;
		
		while(!$rsRFL->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();		
			
			if(($Recht3900&8)>0)	// Löschen
			{
				$Icons[] = array('delete','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND='.$LAND.'&Del='.$rsRFL->FeldInhalt('RFL_KEY').'&RFL_KEY='.$rsRFL->FeldInhalt('RFL_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,54,($DS%2));
	
			$Form->Erstelle_ListenFeld('RFL_FIL_ID',$rsRFL->FeldInhalt('RFL_FIL_ID'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('FIL_BEZ',$rsRFL->FeldInhalt('FIL_BEZ'),20,200,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('FIL_LAGERKZ',$rsRFL->FeldInhalt('FIL_LAGERKZ'),20,75,false,($DS%2),'','','T');
	
			$Form->ZeileEnde();
			$rsRFL->DSWeiter();
			$DS++;
		}
	
		$Form->Formular_Ende();
	}
	else 		// Ein neue Filiale zu dem Land hinzufügen
	{
		$Form->Formular_Start();	
	
		$Form->Erstelle_HiddenFeld('LAND', $rsRFL->FeldInhalt('FIL_LAN_WWSKENN'));
		$Form->Erstelle_HiddenFeld('RFL_KEY', '0');
		$Form->Erstelle_HiddenFeld('RFL_RFA_KEY', '0'.$AWIS_KEY1);
		$AWIS_KEY2=0;
		
		$EditRecht=true;
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Filialen&LAND=".$rsRFL->FeldInhalt('FIL_LAN_WWSKENN') ." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Form->InfoZeile($Felder,'');
	
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFL']['RFL_FIL_ID'].':',200);	
		$SQL = "SELECT DISTINCT FIL_ID, FIL_ID || ' - ' || FIL_BEZ || ' - ' || FIL_LAGERKZ";
		$SQL.= " FROM FILIALEN";
		$SQL.= " LEFT JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN";	
		$SQL.= " LEFT JOIN RUECKFUEHRUNGSANFFILIALEN ON RFL_FIL_ID = FIL_ID AND RFL_RFA_KEY =0".intval($AWIS_KEY1);
		$SQL.= " WHERE FIL_GRUPPE IS NOT NULL";
		$SQL.= " AND LAN_WWSKENN = ". $DB->FeldInhaltFormat('T',$rsRFL->FeldInhalt('FIL_LAN_WWSKENN'),true);
		//var_dump ($SQL);
		if($_GET['RFL_KEY']=='0')					// Liste anzeigen
		{
			$SQL.= " AND RFL_FIL_ID is null";
		}
		$SQL.= " ORDER BY FIL_ID";		
		$Form->Erstelle_SelectFeld('RFL_FIL_ID',($AWIS_KEY2==0?'':$rsRFL->FeldInhalt('RFL_FIL_ID')),200,$EditRecht,$SQL,$OptionBitteWaehlen);
		
		$Form->ZeileEnde();
		$Form->Formular_Ende();
	}	
}
}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>