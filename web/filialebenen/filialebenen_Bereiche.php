<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FRB','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3801 = awisBenutzerRecht($con,3801,$AWISBenutzer->BenutzerName());
if($Recht3801==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT FRB_KEY FROM (SELECT FRB_KEY ';
	$SQL .= ' FROM Filialebenenrollenbereiche';
	$SQL .= ' LEFT JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY';
	if(($Recht3801&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' WHERE (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}
	$SQL .= ' ORDER BY FRB_BEZEICHNUNG DESC';
	$SQL .= ') WHERE ROWNUM = 1';

	$rsFRB = awisOpenRecordset($con, $SQL);
	if(isset($rsFRB['FRB_KEY'][0]))
	{
		$AWIS_KEY1=$rsFRB['FRB_KEY'][0];
	}
	else
	{
		$AWIS_KEY1 = $_POST['txtFRB_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT FRB_KEY FROM (SELECT FRB_KEY ';
	$SQL .= ' FROM Filialebenenrollenbereiche';
	$SQL .= ' LEFT JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY';
	if(($Recht3801&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' WHERE (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}
	$SQL .= ' ORDER BY FRB_BEZEICHNUNG ASC';
	$SQL .= ') WHERE ROWNUM = 1';
	$rsFRB = awisOpenRecordset($con, $SQL);
	if(isset($rsFRB['FRB_KEY'][0]))
	{
		$AWIS_KEY1=$rsFRB['FRB_KEY'][0];
	}
	else
	{
		$AWIS_KEY1 = $_POST['txtFRB_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./filialebenen_Bereiche_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./filialebenen_Bereiche_speichern.php');
awis_Debug(1,$AWIS_KEY1);
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$AWIS_KEY1=-1;
}
elseif(isset($_GET['FRB_KEY']))
{
	$AWIS_KEY1 = floatval($_GET['FRB_KEY']);
}
elseif(isset($_POST['txtFRB_KEY']))
{
	$AWIS_KEY1 = floatval($_POST['txtFRB_KEY']);
}
else
{
	if(!isset($_GET['FRBListe']))
	{
		$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerFilialBereich',$AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$SQL = 'SELECT DISTINCT Filialebenenrollenbereiche.*';
$SQL .= ' FROM Filialebenenrollenbereiche';
$SQL .= ' LEFT JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY';

$Bedingung='';

if($AWIS_KEY1!=0)
{
	$Bedingung.= ' AND FRB_KEY = 0'.$AWIS_KEY1;
}

if(($Recht3801&16)==0)		// Nur den eigenen Bereich anzeigen
{
	$Bedingung .= ' AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
}


if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY FRB_Bezeichnung';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}
awis_Debug(1,$SQL);
// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
$rsFRB = awisOpenRecordset($con, $SQL);
$rsFRBZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
/*if($rsFRBZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Filialebenenrollenbereiche gefunden.</span>';
}
*/
if($rsFRBZeilen>1)						// Liste anzeigen
{
	echo '<form name=frmFilialebenenrollenbereiche action=./filialebenen_Main.php?cmdAktion=Bereiche method=POST  enctype="multipart/form-data">';

	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './filialebenen_Main.php?cmdAktion=Bereiche'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FRB_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FRB_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRB']['FRB_BEZEICHNUNG'],350,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Bereiche'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FRB_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FRB_BEMERKUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRB']['FRB_BEMERKUNG'],550,'',$Link);
	awis_FORM_ZeileEnde();

	if($rsFRBZeilen>$MaxDSAnzahl)
	{
//		awis_FORM_Hinweistext('Ausgabe begrenzt auf '.$MaxDSAnzahl.'. Insgesamt '.$rsFRBZeilen.' Zeilen gefunden.');
//		$rsFRBZeilen=$MaxDSAnzahl;
	}

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsFRBZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsFRBZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./filialebenen_Main.php?cmdAktion=Bereiche&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($FRBZeile=$StartZeile;$FRBZeile<$rsFRBZeilen and $FRBZeile<$StartZeile+$MaxDSAnzahl;$FRBZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './filialebenen_Main.php?cmdAktion=Bereiche&FRB_KEY='.$rsFRB['FRB_KEY'][$FRBZeile].'';
		awis_FORM_Erstelle_ListenFeld('FRB_BEZEICHNUNG',$rsFRB['FRB_BEZEICHNUNG'][$FRBZeile],0,350,false,($FRBZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('FRB_BEMERKUNG',$rsFRB['FRB_BEMERKUNG'][$FRBZeile],0,550,false,($FRBZeile%2),'',$Link);
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3801&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	awis_FORM_FormularStart();
	echo '<form name=frmFilialebenenrollenbereiche action=./filialebenen_Main.php?cmdAktion=Bereiche method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsFRB['FRB_KEY'][0])?$rsFRB['FRB_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerFilialBereich" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);
	echo '<input type=hidden name=txtFRB_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Bereiche&FRBListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFRB['FRB_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFRB['FRB_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht3801&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRB']['FRB_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FRB_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsFRB['FRB_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtFRB_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRB']['FRB_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FRB_BEMERKUNG',($AWIS_KEY1===0?'':$rsFRB['FRB_BEMERKUNG'][0]),50,350,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0)
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Mitglieder'));
//		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3801, $con, $RegisterSeite);
	}


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Recht3802 = awisBenutzerRecht($con,3802,$AWISBenutzer->BenutzerName());	// Rechte auf Rollen
	awis_FORM_SchaltflaechenStart();
	if(($Recht3801&6)!=0 OR ($Recht3802&6)!=0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht3801&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht3801&8)==8 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}

	if(!isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
		awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
	}

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsFRB, $_POST, $rsFRB, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>