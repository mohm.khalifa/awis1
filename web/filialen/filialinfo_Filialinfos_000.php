<?php
// Code gepr?ft, Sacha Kerres, 22.07.2005
global $con;
global $awisRSZeilen;
global $SpeichernButton;		// Anzeige eines Speichern-Knopfs
global $ZeigeNachbarn;
global $AWISBenutzer;


	if(awisBenutzerRecht($con,100)==0)
	{
	     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
	     die("Keine ausreichenden Rechte!");
	}

	$LinkRecht = awisBenutzerRecht($con,112);
	$PersRecht = awisBenutzerRecht($con, 1300);			// Personaleinsatzplanung

	$DatenBearbeitung = awisBenutzerRecht($con,118);	// Filialdaten-Bearbeitung
	$RechteStufeTUEV = awisBenutzerRecht($con,111);	// TUEV-Infos
	$RechteSchluessel = awisBenutzerRecht($con,1503);	// Schluessel-Vergaben


		// Waehrungskennzeichen f�r diese Filiale
	$recLAN = awisOpenRecordset($con, "SELECT LAN_WAE_CODE FROM Laender WHERE LAN_WWSKENN='" . $rsFiliale['FIL_LAN_WWSKENN'][0] . "'");

	$FILWaehrung = ($recLAN['LAN_WAE_CODE'][0]==''?'???':$recLAN['LAN_WAE_CODE'][0]);
	unset($recLAN);

	/************************************************
	*
	* Daten anzeigen
	*
	* **********************************************/

	// Aktuelle Filiale speichern
	awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=0". $rsFiliale["FIL_ID"][0]);

	 // Daten f?r die Infoseite sammeln
	 $SQL = "SELECT filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, filialinfostypen.fit_sortierung,
       			filialinfos.fif_wert, fit_id, FIF_KEY, FIF_IMQ_ID, FIF_FIL_ID, FIF_USER,  FIF_USERDAT
				FROM filialinfos, filialinfostypen, filialinfostypengruppen, importquellen
				 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
				 	AND importquellen.IMQ_ID = filialinfos.FIF_IMQ_ID
        			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
					AND FIF_FIL_ID=0" . $rsFiliale["FIL_ID"][0] . ")
				ORDER BY IMQ_PRIORITAET";

	$rsFilialInfo = awisOpenRecordset($con, $SQL);
	if($rsFiliale["FIL_ID"][0]=="")
	{
		die("<span class=HinweisText>Mit den angegebenen Parametern konnte keine Filiale gefunden werden</span>");
	}

	// ?berschrift schreiben
	echo "<input type=hidden name=FILID value=" . $rsFiliale["FIL_ID"][0] . ">";	// F?r Submit
	echo "<table width=100% border=0><tr><td align=left>";
	echo "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
	if(($RechteSchluessel&1)==1)
	{
		echo "</td><td backcolor=#000000 align=right><img src=/bilder/sc_svergabe.png title='Schl&uuml;sselvergabe-Detail anzeigen' onclick=location.href='../schluessel/schluessel_Main.php?cmdAktion=Vergaben&txtFIL_ID=".$rsFiliale["FIL_ID"][0]."&Anzeigen=true';>&nbsp;<img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos&Liste=True';></td></tr></table>";
	}
	else
	{
		echo "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos&Liste=True';></td></tr></table>";
	}
	echo "<hr>";

	echo "<table width=100% id=DatenTabelle><tr>";		// Gro?er Rahmen mit drei Spalten

	echo "<td valign=top width=24%><table border=0>";
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_BEZ"][0] . "</span></td></tr>";
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_STRASSE"][0] . "</span></td></tr>";
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . ($rsFiliale["LAN_CODE"][0]==''?'':$rsFiliale["LAN_CODE"][0].'-') . $rsFiliale["FIL_PLZ"][0] . " " . $rsFiliale["FIL_ORT"][0] . " " . $rsFiliale["FIL_ORTSTEIL"][0] . "</span></td></tr>";

		// Bundesland anzeigen
	$Erg = SucheFilialInfo($rsFilialInfo, 65, "FIT_ID");		// Neue Strasse
	if($Erg>=0)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_bul_id']=intval('0'.number_format($rsFilialInfo["FIF_WERT"][$Erg],0));
		$rsBl = awisOpenRecordset($con, "SELECT BUL_BUNDESLAND, BUL_ID FROM Bundeslaender WHERE BUL_ID=:var_N0_bul_id", true, false, $BindeVariablen);
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>BL: " . $rsBl["BUL_BUNDESLAND"][0] . "</span></td></tr>";
		unset($rsBl);
	}
	
	// Anzeigen, ob die Filiale eine Standardfiliale ist. TR 03.05.2007
	if ($rsFiliale["FIL_FILTYP"][0] == 'S')
	{
		echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>Standardfiliale</span></td></tr>";
	}
	
	// Anzeigen, ob die Filiale eine Lightfiliale ist. TR 03.05.2007
	if ($rsFiliale["FIL_FILTYP"][0] == 'L')
	{
		echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>Lightfiliale</span></td></tr>";
	}	

	
		// Neue Adresse pr�fen!

	$Erg = SucheFilialInfo($rsFilialInfo, 104, "FIT_ID");		// Neue Strasse

	If($Erg <> -1 AND awis_format($rsFilialInfo["FIF_WERT"][$Erg],'SortDatum') > date("Ymt"))
	{
		echo "<tr><td>&nbsp;</td></tr>";
		echo "<tr><td><span Class=HinweisText>ACHTUNG</span></td></tr>";
		echo "<tr><td><span Class=HinweisText>Neue Adresse ab " . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
		echo "<tr><td>&nbsp;</td></tr>";

		$Erg = SucheFilialInfo($rsFilialInfo, 101, "FIT_ID");		// Neue Strasse
 		echo "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
		$Erg = SucheFilialInfo($rsFilialInfo, 102, "FIT_ID");		// Neue Plz
 		echo "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
		$Erg = SucheFilialInfo($rsFilialInfo, 103, "FIT_ID");		// Neuer Ort
 		echo "" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

		echo "<tr><td>&nbsp;</td></tr>";

	}

	echo "</table></td>";
		// Ende Filialadresse


		// Zweite Spalte
 	echo "<td valign=top width=33%><table border=0>";


	$Erg = SucheFilialInfo($rsFilialInfo, 3, "FIT_ID");		// Kundentelefon
	echo "<tr><td><table border=0>";
 	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

	$Erg = SucheFilialInfo($rsFilialInfo, 2, "FIT_ID");		// Faxnummer
 	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

	echo "</table>";

		// ?ffnungszeiten
	echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>�ffnungszeiten</span></td></tr>";

	$Erg = SucheFilialInfo($rsFilialInfo, 121, "FIT_ID");		// ?ffnung Mo-Fr
 	echo "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>Mo-Fr : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	$Erg = SucheFilialInfo($rsFilialInfo, 122, "FIT_ID");
 	echo "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";

	$Erg = SucheFilialInfo($rsFilialInfo, 123, "FIT_ID");		// �ffnung Sa
 	echo "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>Sa : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	$Erg = SucheFilialInfo($rsFilialInfo, 124, "FIT_ID");
 	echo "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";

	echo "<tr><td>&nbsp;</td></tr>";		// Leerzeile

	$Erg = SucheFilialInfo($rsFilialInfo, 34, "FIT_ID", 32);		// Er?ffnungsdatum aus ACCESS
	$AktDatum = awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert');

	if(mktime(0,0,0,substr($rsFilialInfo["FIF_WERT"][$Erg],3,2),substr($rsFilialInfo["FIF_WERT"][$Erg],0,2),substr($rsFilialInfo["FIF_WERT"][$Erg],6,4))>time())
	{
	 	echo "<tr><td id=DatenFeld><span class=HinweisText>Er&ouml;ffnung am : </span><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
	}
	else
	{
	 	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Er&ouml;ffnet am : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 104, "FIT_ID");		// Umzugsdatum
	$AktDatum = ($AktDatum<awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert')?awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert'):$AktDatum);
	if($Erg>=0)
	{
		$Datum = mktime(0,0,0,substr($rsFilialInfo["FIF_WERT"][$Erg],3,2),substr($rsFilialInfo["FIF_WERT"][$Erg],0,2), substr($rsFilialInfo["FIF_WERT"][$Erg],6));

		if($Datum>time())
		{
	 		echo "<tr><td id=DatenFeld><span class=HinweisText>Umzug am : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
		}
		else
		{
	 		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Umgezogen am : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
		}
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 36, "FIT_ID");		// Ladenkonzept 2011

	if(($DatenBearbeitung & 128)==128)		// Ladenkonzept 2011
	{
		$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
		echo '<tr><td id=DatenFeld>';
		echo "<input type=hidden name=txtLadenkonzept_old value=" . $Ausgabe . '>';
		echo '<span class=DatenFeldNormal>Ladenkonzept 2011 : </span>';

		$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
		If($Ausgabe=='' AND $AktDatum > strtotime('01/01/2006'))
		{
			$Ausgabe = date('d.m.Y',$AktDatum);
			echo "<span class=DatenFeldNormalFett>" . $Ausgabe . "</span>";
		}
		else
		{
			echo "<span class=DatenFeldNormalFett><input accesskey=z type=text title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=10 name=txtLadenkonzept value='" . $Ausgabe . "'></span>";
			echo "<input align=right type=hidden name=txtFIF_KEY_Ladenkonzept value='" .  $rsFilialInfo["FIF_KEY"][$Erg] . "'>";
			$SpeichernButton = True;
		}
		echo "</td></tr>";
	}
	else
	{
		$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
		If($Ausgabe=='' AND $AktDatum > strtotime('08/01/2005'))
		{
			$Ausgabe = date('d.m.Y',$AktDatum);
		}
		elseIf($Ausgabe=='')		// Keine Infos
		{
			$Ausgabe = 'kein Termin';
		}
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Ladenkonzept 2011: </span><span class=DatenFeldNormalFett>" . $Ausgabe . "</span></td></tr>";
	}

			//********************************
			// Stundensatz der Filialen und
			// Spezialaktion Radwechsel ohne Wuchten (seit 19.09.05, SP)
			//********************************
	$RechteStufe = awisBenutzerRecht($con, 110);
	if($RechteStufe>0)
	{
		echo "<tr><td>&nbsp;</td></tr>";		// Leerzeile
		$Erg = SucheFilialInfo($rsFilialInfo, 200, "FIT_ID");		// Stundensatz
		if(($RechteStufe & 2)==2)
		{
			$Ausgabe = number_format(str_replace(",",".",$rsFilialInfo["FIF_WERT"][$Erg]),2,",","");
 			echo '<tr><td id=DatenFeld>';
			echo '<input type=hidden name=txtStundenSatz_old value=' . $Ausgabe . '>';
			echo '<span class=DatenFeldNormal>Stundensat<u>z</u> : </span>';
			echo "<span class=DatenFeldNormalFett><input accesskey=z  title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' type=text size='" . strlen($Ausgabe) . "' name=txtStundenSatz value='" . $Ausgabe . "'></span>" . $FILWaehrung;
			echo "<input align=right type=hidden name=txtFIF_KEY_StundenSatz value='" .  $rsFilialInfo["FIF_KEY"][$Erg] . "'>";
			$SpeichernButton = True;
			echo "</td></tr>";
		}
		else
		{
 			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Stundensatz : </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",$rsFilialInfo["FIF_WERT"][$Erg]),2,",","") . $FILWaehrung . "</span></td></tr>";
		}
		if (substr($rsFiliale["FIL_PLZ"][0], 0, 1) == 'A')
		{
			$DPRNummer = 'OEIN03';
		}
		else
		{
			$DPRNummer = '1EIN03';
		}
		$DPFPreisSQL = "Select DPF_VK from dienstleistungspreisefilialen where dpf_dpr_nummer = '" . $DPRNummer . "' and dpf_fil_id = " . $rsFiliale["FIL_ID"][0];
		$rsDPFPreisInfo = awisOpenRecordset($con, $DPFPreisSQL);
		if ($rsDPFPreisInfo["DPF_VK"][0] != 0 && count($rsDPFPreisInfo) > 0)
		{
			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Radwechsel ohne Wuchten (je Rad): </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",$rsDPFPreisInfo["DPF_VK"][0]),2,",","") . $FILWaehrung . "</span></td></tr>";
		}
		else
		{
			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Radwechsel ohne Wuchten (je Rad): </span><span class=DatenFeldNormalFett>::unbekannt::" . "</span></td></tr>";
		}
	}
			//********************************
			// Aktion AU/HU in den Filialen
			//********************************
	$Erg = SucheFilialInfo($rsFilialInfo, 203, "FIT_ID");		// Aktion AU/HU
	if(($DatenBearbeitung & 64)==64)
	{
		echo '<td id=DatenFeld><span class=DatenFeldNormal>Aktion AU/HU : </span>';
		echo '<input type=hidden name=txtAUHU_old value=\'' . $Ausgabe . '\'>';
		echo "<input size=20 title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' name=txtAUHU value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";
	}
	else	// Keine Bearbeitung
	{
		if(substr_count($rsFilialInfo["FIF_WERT"][$Erg], 'EURO') > 0)		// EURO in ? umbauen!
		{
			$Wert = str_replace('EURO','',$rsFilialInfo['FIF_WERT'][$Erg]);
			if(is_numeric($Wert))
			{
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Aktion AU/HU : </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",$rsFilialInfo["FIF_WERT"][$Erg]),2,",","") . " ". $FILWaehrung . "</span></td></tr>";
			}
			else
			{
				$Wert = str_replace('EURO','&euro;',$rsFilialInfo['FIF_WERT'][$Erg]);
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Aktion AU/HU : </span><span class=DatenFeldNormalFett>" . $Wert . "</span></td></tr>";
			}
		}
		else
		{
			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Aktion AU/HU : </span><span class=DatenFeldNormalFett>" . ($rsFilialInfo["FIF_WERT"][$Erg]!=''?$rsFilialInfo["FIF_WERT"][$Erg]:'::unbekannt::') . " </span></td></tr>";
		}
	}

	echo "</table></td>";	// Ende zweite Spalte

	 	// Dritte Spalte
	echo "<td valign=top width=43%><table border=0>";

		// 1. Gesch�ftsleiter-ID
	SchreibePersonenFeld("1. Gesch�ftsleiter",150,74,$rsFilialInfo,$con,($LinkRecht>0));

		// 2. Gesch�ftsleiter-ID
	SchreibePersonenFeld("2. Gesch�ftsleiter",150,75,$rsFilialInfo,$con,($LinkRecht>0));

		// 1. Werkstattleiter
	SchreibePersonenFeld("1. Werkstattleiter",150,76,$rsFilialInfo,$con,($LinkRecht>0));

		// Werkstattleiter2
	SchreibePersonenFeld("2. Werkstattleiter",150,77,$rsFilialInfo,$con,($LinkRecht>0));


	$Erg = SucheFilialInfo($rsFilialInfo, 26, "FIT_ID");		// Regionalzentrum
	$RegZentrum = $rsFilialInfo["FIF_WERT"][$Erg];
	$Erg = SucheFilialInfo($rsFilialInfo, 21, "FIT_ID");		// Gebiet
	$Gebiet = $rsFilialInfo["FIF_WERT"][$Erg];

	//**********************************
	// Int. Vertrieb
	//**********************************

	if($rsFiliale['LAN_CODE'][0]!='DE')
	{
		$rsMIT = awisOpenRecordset($con, "SELECT MIT_PER_NR,MTA_BEZEICHNUNG,MIT_BEZEICHNUNG FROM Mitarbeiter INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID WHERE MIT_STATUS='A' AND MIT_MTA_ID IN (130)");
		$rsMITZeilen = $awisRSZeilen;
		for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
		{
		 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
			echo $rsMIT['MTA_BEZEICHNUNG'][$MITZeile] . ':</span></td><td><span class=DatenFeldNormalFett>';
			echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
			echo '</span></td></tr></table></td></tr>';
		}
	}

	/**********************************
	* VKL
	**********************************/
	$rsMIT = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID WHERE MIT_STATUS='A' AND MIT_REZ_ID=0" . $RegZentrum . " AND MIT_MTA_ID IN (1,110)");
	$rsMITZeilen = $awisRSZeilen;
	for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
	{
		if($PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
		{
			$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
		}
	 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
		echo $Ausgabe . $rsMIT['MTA_BEZEICHNUNG'][$MITZeile] . ':'.($Ausgabe==''?'':'</a>') .' </span></td><td><span class=DatenFeldNormalFett>';
		echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
		echo '</span></td></tr></table></td></tr>';
	}
	unset($rsMIT);
	/**********************************
	* TKDL
	**********************************/

	$Erg = SucheFilialInfo($rsFilialInfo, 28, "FIT_ID");		// manueller TKDL

	if($Erg > 0 AND $rsFilialInfo["FIF_WERT"][$Erg]!='')
	{
	 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
		echo "<input type=hidden name=txtTKDL_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo 'TKDL:</a> </span></td><td><span class=DatenFeldNormalFett>';
		if(($DatenBearbeitung & 16)==16)
		{
			echo "<td><input size=20 name=txtTKDL value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";
		}
		else	// Keine Bearbeitung
		{
			echo $rsFilialInfo["FIF_WERT"][$Erg];
		}
		echo '</span></td></tr></table></td></tr>';
	}
	else
	{
		$rsMIT = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter INNER JOIN VerkaufsGebiete ON MIT_KEY = VKG_TKDL_MIT_KEY WHERE MIT_REZ_ID=0" . $RegZentrum . " AND VKG_ID='". $Gebiet . "'");
		$rsMITZeilen = $awisRSZeilen;

		for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
		{
			if($rsMIT['VKG_ID'][0]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
			{
				$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
				//$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
			}

		 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
			echo $Ausgabe . 'TKDL:</a> </span></td><td><span class=DatenFeldNormalFett>';


			if(($DatenBearbeitung & 16)==16)		// Bearbeitung m�glich
			{
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
				echo "<input type=hidden name=txtTKDL_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
				echo "&nbsp;(<input size=20 name=txtTKDL value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>)";
			}
			else		// Nur Anzeige
			{

				if ($rsFilialInfo["FIF_WERT"][$Erg]!='')	// Ist eine Alternative vorhanden? -> Ja -> Anzeigen
				{
					echo "" . $rsFilialInfo["FIF_WERT"][$Erg] . "";
				}
				else		// Nein -> Standard anzeigen
				{
					echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
				}
			}

			echo '</span></td></tr></table></td></tr>';
		}
		unset($rsMIT);
	}





		// Gebietskennung / Gebietsleiter
	$Ausgabe='';
	if(($DatenBearbeitung & 16)==16)
	{
		$SQL = "SELECT VKG_ID, MIT_Bezeichnung, PER_NR";
		$SQL .= ' FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY';
		$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
		$SQL .= " ORDER BY MIT_BEZEICHNUNG";

		$rsPersListe = awisOpenRecordset($con, $SQL);
		$rsPersListeZeilen = $awisRSZeilen;

		// TODO: Aktuellen Suchen
		for($j=0;$j<$rsPersListeZeilen;$j++)
		{
			if($rsPersListe['VKG_ID'][$j]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
			{
				$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][$j]) . ">";
//				$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][$j]) . ">";
				break;
			}
		}
		echo "<tr><td id=DatenFeld>";
		echo '<input type=hidden name=txtGebLeiter_old value=' . $Gebiet . '>';
		echo "<table border=0><tr><td width=150><span class=DatenFeldNormal>$Ausgabe Gebietsleiter" . ($Ausgabe!=''?'</a>':'') . " :</span></td><td><span class=DatenFeldNormalFett>";
		echo "<select name=txtGebLeiter >";
		echo '<option value=0>::Nicht zugeordnet::</option>';
		for($j=0;$j<$rsPersListeZeilen;$j++)
		{
			echo "<option ";
			if($rsPersListe['VKG_ID'][$j] == $Gebiet)
			{
				echo " selected ";
			}
			echo " value='" . $rsPersListe["VKG_ID"][$j] . "'>" . $rsPersListe["MIT_BEZEICHNUNG"][$j] . "</option>";
		}
		echo "</select>";
		echo "</span></td></tr>";

		$SpeichernButton = True;
	}
	else
	{
		// Gebietsleiter
		$SQL = "SELECT VKG_ID, MIT_Bezeichnung, PER_NR";
		$SQL .= ' FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY';
		$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
		$SQL .= " WHERE VKG_ID='" . $Gebiet . "'";
		$rsPersListe = awisOpenRecordset($con, $SQL);
		if($rsPersListe['VKG_ID'][0]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
		{
			$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][$j]) . ">";
		//	$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][0]) . ">";
		}

		echo "<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>$Ausgabe Gebietsleiter" . ($Ausgabe!=''?'</a>':'') . " :</span></td><td><span class=DatenFeldNormalFett>";
		echo $rsPersListe['MIT_BEZEICHNUNG'][0];
		echo "</span></td></tr>";

	}


		//**************************************
		// Regionalzentrum
		//**************************************

	if(($DatenBearbeitung & 32)==32)
	{
		$rsREZ = awisOpenRecordset($con, "SELECT * FROM RegionalZentren ORDER BY REZ_BEZEICHNUNG");
		$rsREZZeilen = $awisRSZeilen;

		echo "<tr><td id=DatenFeldNormal>";
		echo '<input type=hidden name=txtVGK_ID_old value=' . $rsREZ["REZ_ID"][$j] . '>';
		echo "<span class=DatenFeldNormal>Regionalzentrum:</span></td><td>";
		echo "<select name=txtVGK_ID >";
		echo '<option value=0>::Nicht zugeordnet::</option>';
		for($j=0;$j<$rsREZZeilen;$j++)
		{
			echo "<option ";
			if($rsREZ['REZ_ID'][$j] == $RegZentrum)
			{
				echo " selected ";
			}
			echo " value='" . $rsREZ["REZ_ID"][$j] . "'>" . $rsREZ["REZ_BEZEICHNUNG"][$j] . "</option>";
		}
		echo "</select>";
		echo "</span></td></tr>";

		$SpeichernButton = True;

	}
	else
	{
		if($RegZentrum!="")
		{
			$rsRegionalZentrum = awisOpenRecordset($con, "SELECT * FROM RegionalZentren WHERE REZ_ID=0". $RegZentrum ."");
			If($rsRegionalZentrum["REZ_BEZEICHNUNG"][0]!="")
			{
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span></td><td>";
				echo "<span class=DatenFeldNormalFett>" . $rsRegionalZentrum["REZ_BEZEICHNUNG"][0]  . "</span></td></tr>";
			}
			else
			{
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span></td><td>";
				echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
			}
		}
	}


	echo "<tr><td>&nbsp</td></tr>";		// Leerzeile
	echo '</table></td></tr>';

		// Tabelle f?r die Anlieferung, Kommissioniertag und Regionalzentrum
	echo "<tr><td><table border=0>";

	$Erg = SucheFilialInfo($rsFilialInfo, 49, "FIT_ID");		// Anlieferung
 	echo '<tr><td id=DatenFeld>';
	echo "<input type=hidden name=txtAnlieferungstag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
	echo '<span class=DatenFeldNormal>Anlieferungstag : </span></td><td>';
	if(($DatenBearbeitung & 4)==4)
	{
		echo "<select name=txtAnlieferungstag size=1>";
		echo "<option value=''>::Bitte w�hlen::</option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Montag'?"Selected":"") . ">Montag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Dienstag'?"Selected":"") . ">Dienstag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Mittwoch'?"Selected":"") . ">Mittwoch</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Donnerstag'?"Selected":"") . ">Donnerstag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Freitag'?"Selected":"") . ">Freitag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Samstag'?"Selected":"") . ">Samstag</Option>";
		echo "</Select>";
		echo "<span class=DatenFeldNormalFett> aus " . awis_LagerText($rsFiliale["FIL_LAGERKZ"][0]);	// Bezeichnung laden
		echo "</span></td></tr>";

		$SpeichernButton = True;
	}
	else
	{
		echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
		echo " aus " . awis_LagerText($rsFiliale["FIL_LAGERKZ"][0]);	// Bezeichnung laden
		echo "</span></td></tr>";
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 50, "FIT_ID");		// Lieferh�ufigkeit
	if (!empty($rsFilialInfo["FIF_WERT"][$Erg]))
 	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>&nbsp;</span></td><td>";
		echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 48, "FIT_ID");		// Kommissioniertag
 	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Kommissioniertag : </span></td><td>";
	if(($DatenBearbeitung & 8)==8)
	{
		echo "<input type=hidden name=txtKommissioniertag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<select name=txtKommissioniertag size=1>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Montag'?"Selected":"") . ">Montag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Dienstag'?"Selected":"") . ">Dienstag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Mittwoch'?"Selected":"") . ">Mittwoch</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Donnerstag'?"Selected":"") . ">Donnerstag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Freitag'?"Selected":"") . ">Freitag</Option>";
		echo "<option ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Samstag'?"Selected":"") . ">Samstag</Option>";
		echo "</Select>";
		echo "</td></tr>";

		$SpeichernButton = True;
	}
	else
	{
		echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
	}

	echo "</table></td></tr>";

	echo "</table></td>";	// Ende dritte Spalte

	echo "</tr></table>";	// Ende Datentabelle

		//*******************************************
		// T?V-Termine
		//*******************************************

	echo "<span class=DatenFeldNormalFett>T�V-Termine</span></br>";
	if(($RechteStufeTUEV & 8)!=8)
	{
		$Ausgabe = "";
		$Erg = SucheFilialInfo($rsFilialInfo, 41, "FIT_ID");		// T?V-Termin Montag
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe = ", <span class=DatenFeldNormalFett>Montag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}

		$Erg = SucheFilialInfo($rsFilialInfo, 42, "FIT_ID");		// T?V-Termin Dienstag
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe .= ", <span class=DatenFeldNormalFett>Dienstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}
		$Erg = SucheFilialInfo($rsFilialInfo, 43, "FIT_ID");		// T?V-Termin Mittwoch
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe .= ", <span class=DatenFeldNormalFett>Mittwoch</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}
		$Erg = SucheFilialInfo($rsFilialInfo, 44, "FIT_ID");		// T?V-Termin Donnerstag
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe .= ", <span class=DatenFeldNormalFett>Donnerstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}
		$Erg = SucheFilialInfo($rsFilialInfo, 45, "FIT_ID");		// T?V-Termin Freitag
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe .= ", <span class=DatenFeldNormalFett>Freitag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}
		$Erg = SucheFilialInfo($rsFilialInfo, 46, "FIT_ID");		// T?V-Termin Samstag
		if($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
			$Ausgabe .= ", <span class=DatenFeldNormalFett>Samstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
		}

		If($Ausgabe)
		{
 			echo "<span class=DatenFeldNormal>" . substr($Ausgabe,2) . "</span><br>";
		}
		else
		{
			echo "<span class=DatenFeldNormal>keine.</span><br>";
		}
	}
	else		// Daten bearbeiten
	{

		echo "<table border=0><tr>";
		$Erg = SucheFilialInfo($rsFilialInfo, 41, "FIT_ID");		// T?V-Termin Montag
		echo "<td><input type=hidden name=txtTUEVMontag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Montag</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVMontag value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		$Erg = SucheFilialInfo($rsFilialInfo, 42, "FIT_ID");		// T?V-Termin Dienstag
		echo "<td><input type=hidden name=txtTUEVDienstag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Dienstag</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVDienstag value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		$Erg = SucheFilialInfo($rsFilialInfo, 43, "FIT_ID");		// T?V-Termin Mittwoch
		echo "<td><input type=hidden name=txtTUEVMittwoch_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Mittwoch</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVMittwoch value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		echo '</tr><tr>';
		$Erg = SucheFilialInfo($rsFilialInfo, 44, "FIT_ID");		// T?V-Termin Mittwoch
		echo "<td><input type=hidden name=txtTUEVDonnerstag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Donnerstag</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVDonnerstag value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		$Erg = SucheFilialInfo($rsFilialInfo, 45, "FIT_ID");		// T?V-Termin Mittwoch
		echo "<td><input type=hidden name=txtTUEVFreitag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Freitag</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVFreitag value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		$Erg = SucheFilialInfo($rsFilialInfo, 46, "FIT_ID");		// T?V-Termin Mittwoch
		echo "<td><input type=hidden name=txtTUEVSamstag_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<span class=DatenFeldNormalFett>Samstag</span></td><td><input title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' size=16 name=txtTUEVSamstag value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";

		echo "<tr></table>";

		$SpeichernButton = True;
	}

		// T?V - Preise

	if(($RechteStufeTUEV & 2)==2)			// Mehr Informationen
	{
		$SQL = "SELECT * FROM PruefGesellschaften, PruefArten, PruefFilialen, PruefBemerkungen";
		$SQL .= " WHERE PRF_PRG_ID = PRG_ID AND PRF_PRA_ID = PRA_ID";
		$SQL .= " AND (PRF_FIL_ID=PRB_FIL_ID(+) AND PRF_PRG_ID=PRB_PRG_ID(+))";
		$SQL .= " AND PRF_FIL_ID = 0" . $rsFiliale["FIL_ID"][0];
		$SQL .= " AND PRA_ID = 1";

		$rsTUEVInfos = awisOpenRecordset($con, $SQL);
		$rsTUEVInfosZeilen = $awisRSZeilen;
		if($rsTUEVInfosZeilen==0)
		{
			echo '<br>Es sind keine weiteren Informationen &uuml;ber Pr&uuml;fgesellschaften vorhanden.<br>';
		}
		else
		{
			echo "<br><table width=100% border=0><tr>";
			echo "<td id=FeldBez>ID</td>";
			echo "<td id=FeldBez>Organisation</td>";
	//		echo "<td id=FeldBez>Untersuchung</td>";
			echo "<td id=FeldBez>Preis</td>";
	//		echo "<td id=FeldBez>Nachlass</td><td id=FeldBez>Sonderpreis</td>";
			echo "<td id=FeldBez>Bemerkung</td></tr>";
			for($j=0;$j<$rsTUEVInfosZeilen;$j++)
			{
				echo "<tr>";
				echo "<td valign=top>" . $rsTUEVInfos["PRG_ID"][$j] . "</td>";
				echo "<td valign=top>" . $rsTUEVInfos["PRG_BEZEICHNUNG"][$j] . "</td>";
	//			echo "<td valign=top>" . $rsTUEVInfos["TUA_BEZ"][$j] . "</td>";
				echo "<td valign=top>" . awis_format($rsTUEVInfos["PRF_PREIS"][$j],"Currency") . "</td>";
	//			echo "<td valign=top>" . $rsTUEVInfos["FTU_NACHLASS"][$j] . "</td>";
	//			echo "<td valign=top>" . $rsTUEVInfos["FTU_SONDERPREIS"][$j] . "</td>";
				if(($RechteStufeTUEV & 4)==4)
				{
					$ZeichenAnz = count_chars($rsTUEVInfos["PRB_BEMERKUNG"][$j]);
					$ZeichenAnz = $ZeichenAnz[10]+1;

					echo "<td><textarea title='Letzte �nderung von: " . $rsTUEVInfos["PRB_USER"][$j] .  " am " . $rsTUEVInfos["PRB_USERDAT"][$j] .  "' name=txtPRB_BEMERKUNG_$j cols=60 rows=$ZeichenAnz>" . $rsTUEVInfos["PRB_BEMERKUNG"][$j] . "</textarea>";
					echo "<input type=hidden name=txtPRA_ID_$j value='" . $rsTUEVInfos["PRF_FIL_ID"][$j] . "~" . $rsTUEVInfos["PRF_PRG_ID"][$j]. "~" . $rsTUEVInfos["PRF_PRA_ID"][$j] . "'>";
					echo "</td>";
				}
				else
				{
					echo "<td valign=top>" . $rsTUEVInfos["PRB_BEMERKUNG"][$j] . "</td>";
				}

				echo "</tr>";
			}
			echo "</table><hr>";
		}	// Daten vorhanden?
		unset($rsTUEVInfos);
		unset($rsTUEVInfosZeilen);

		$SpeichernButton = True;
	}
	elseif(($RechteStufeTUEV & 1)==1)		// Nur Zusammenfassung
	{
		echo "<table border=0>";

		$SQL = "SELECT MIN(PRF_Preis) AS Preis_MIN, MAX(PRF_Preis) AS Preis_MAX FROM PruefFilialen";
		$SQL .= " WHERE PRF_FIL_ID = 0" . $rsFiliale["FIL_ID"][0];
		$SQL .= " AND PRF_PRA_ID = 1";
		$rsPreise = awisOpenRecordset($con,$SQL );
		echo "<tr><td width=100><span class=DatenFeldNormal>Preis f&uuml;r HU:</td><td>";
		$Ausgabe = $rsPreise["PREIS_MIN"][0];
		if($Ausgabe <> $rsPreise["PREIS_MAX"][0] and $rsPreise["PREIS_MAX"][0]!=0)
		{
			echo awis_format($rsPreise["PREIS_MIN"][0],'Currency') . " - " . awis_format($rsPreise["PREIS_MAX"][0],'Currency',',');
		}
		else
		{
			echo awis_format($rsPreise["PREIS_MIN"][0],'Currency');
		}
		/*
		$Ausgabe="";
		$Erg = SucheFilialInfo($rsFilialInfo, 201, "FIT_ID");		// Min T?V Preis
		$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
		echo "<tr><td width=100><span class=DatenFeldNormal>Preis f&uuml;r HU:</td><td>" . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
		$Erg = SucheFilialInfo($rsFilialInfo, 202, "FIT_ID");		// Max T?V Preis

		if($Ausgabe <> $rsFilialInfo["FIF_WERT"][$Erg])
		{
			echo " - " . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
		}
		*/
		echo "</span></td></tr></table>";
	}


		//*******************************************
		// Wegbeschreibung
		//*******************************************

	echo "<br><span class=DatenFeldNormalFett>Wegbeschreibung</span></br>";
	$Erg = SucheFilialInfo($rsFilialInfo, 47, "FIT_ID");		// Wegbeschreibung
	if(($DatenBearbeitung & 1)!=1)
	{
		If($rsFilialInfo["FIF_WERT"][$Erg]!="")
		{
 			echo "<span class=DatenFeldNormal>" . nl2br($rsFilialInfo["FIF_WERT"][$Erg]) . "</span>";
		}
		else
		{
			echo "<span class=DatenFeldNormal>keine gefunden.</span>";
		}
	}
	else
	{
		echo "<input type=hidden name=txtWegBeschreibung_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo "<textarea title='Letzte �nderung von: " . $rsFilialInfo["FIF_USER"][$Erg] .  " am " . $rsFilialInfo["FIF_USERDAT"][$Erg] .  "' name=txtWegBeschreibung rows=5 cols=80>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</textarea>";
		$SpeichernButton = True;
	}


	//*******************************************
	// Nachbarfilialen
	//*******************************************

	$Params = explode(';',awis_BenutzerParameter($con, 'FilialSuche', $AWISBenutzer->BenutzerName));
	if($Params[8]=='on')
	{
		echo "<br><span class=DatenFeldNormalFett>Nachbarfilialen</span></br>";
		$Antwort = file_get_contents('http://filialsuche.server.atu.de/filialsuche?plz=' . $rsFiliale['FIL_PLZ'][0] . '&lkz=D&anzahl=' . (awis_BenutzerParameter($con, 'FilialNachbarn', $AWISBenutzer->BenutzerName)+1));
		if($Antwort!==false)
		{
			$p = xml_parser_create();
			xml_parse_into_struct($p, $Antwort, $vals, $index);
			xml_parser_free($p);
			$Nr = 0;
			foreach($vals as $Daten)
			{
				if($Daten['tag']=='NR')
				{
					$Filialen['NR'][++$Nr]=$Daten['value'];
					$FilListe .= ',' . $Daten['value'];
				}
				elseif($Daten['tag']=='NAME')
				{
					$Filialen['NAME'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='PLZ')
				{
					$Filialen['PLZ'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='STRASSE')
				{
					$Filialen['STRASSE'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='ORT')
				{
					$Filialen['ORT'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='NEUEROEFFNUNG')
				{
					$Filialen['NEUEROEFFNUNG'][$Nr]=$Daten['value'];
				}
				elseif($Daten['tag']=='UMZUG')
				{
					$Filialen['UMZUG'][$Nr]=$Daten['value'];
				}
			}

			echo '<table border=1>';
			echo '<tr><td id=FeldBez>Fil-ID</td><td id=FeldBez>Filiale</td><td id=FeldBez>Strasse</td>
					<td id=FeldBez>Plz</td><td id=FeldBez>Ort</td>
					<td id=FeldBez>Er�ffnung am</td>
					<td id=FeldBez>Umzug</td></tr>';

			//if(strtotime($ErOeffDat)
			for($nbFil=1;$nbFil<=$Nr;$nbFil++)
			{
				if($Filialen['NEUEROEFFNUNG'][$nbFil]=='')
				{
					$ErOeffDat='';
				}
				else
				{
					$ErOeffDat = explode('.',$Filialen['NEUEROEFFNUNG'][$nbFil]);
					$ErOeffDat = strtotime($ErOeffDat[1].'/'.$ErOeffDat[0].'/'.$ErOeffDat[2]);
					if($ErOeffDat < time())
					{
						$ErOeffDat='';
					}
					else
					{
						$ErOeffDat = strftime('%d.%m.%Y');
					}
				}


				if($Filialen['NR'][$nbFil]!=$rsFiliale['FIL_ID'][0])
				{
					echo '<tr><td><a href=./filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $Filialen['NR'][$nbFil] . '>' . $Filialen['NR'][$nbFil] . '</a></td>';
					echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['NAME'][$nbFil] . ($ErOeffDat==''?'':'</i>'). '</td>';
					echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['STRASSE'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
					echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['PLZ'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
					echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['ORT'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
					echo '<td ' . ($Filialen['NEUEROEFFNUNG'][$nbFil]==''?'':"title='Seit " . $Filialen['NEUEROEFFNUNG'][$nbFil] . "'") . '>' . ($ErOeffDat==''?'offen':'ab <b>'.$Filialen['NEUEROEFFNUNG'][$nbFil].'</b>') . '</td>';
					echo '<td>' . ($Filialen['UMZUG'][$nbFil]==''?'nein':'<b>'.$Filialen['UMZUG'][$nbFil].'</b>') . '</td>';
					echo '<tr>';
				}
			}
			echo '</table>';
		}// Ende Nachbarn
	}

/************************************************************************************
*
* Hilfsfunktionen
*
************************************************************************************/
Function SucheFilialInfo($rsFiliaInfo, $InfoID, $Spalte, $ImpQuelle = 0)
{

	$i=0;
	while($rsFiliaInfo[$Spalte][$i]!="")
	{
		if($ImpQuelle>0)
		{
			 if($rsFiliaInfo['FIF_IMQ_ID'][$i]==$ImpQuelle AND $rsFiliaInfo[$Spalte][$i]==$InfoID)
			 {
				return $i;
			 }
		}
		elseif($rsFiliaInfo[$Spalte][$i]==$InfoID)
		{
			return $i;
		}
		$i++;
	}
	return -1;
}

/************************************************************
*
* Schreibt eine Person aus der Filiale mit einem Link
*
*************************************************************/
Function SchreibePersonenFeld($Beschreibung, $Breite, $WertID, $rsFilialInfo, $con, $Link)
{
	global $awisRSZeilen;

	$Ausgabe="";

 	echo "<tr><td id=DatenFeld><table border=0><tr><td valign=top width=$Breite><span class=DatenFeldNormal>";

	$Erg = SucheFilialInfo($rsFilialInfo, $WertID, "FIT_ID");

	if($rsFilialInfo["FIF_WERT"][$Erg]!='')		// Ist ein Filial-Mitarbeiter vorhanden?
	{
		if($Link)
		{
			$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsFilialInfo["FIF_WERT"][$Erg]) . ">";
		}
		echo "$Ausgabe $Beschreibung" . ($Ausgabe!=''?'</a>':'') . ": </span></td><td><span class=DatenFeldNormalFett>";
		$Ausgabe=''	;

	//	if($rsFilialInfo["FIF_WERT"][$Erg]!="" AND $Link AND awisHatDasRecht(112)>0)
		{
	//		$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsFilialInfo["FIF_WERT"][$Erg]) . ">";
		}
		$rsPerson = awisOpenRecordset($con,"SELECT * FROM PERSONAL WHERE PER_NR='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'");

		If($Ausgabe!="")
		{
			$Ausgabe .= $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0] . "</a>";
		}
		else
		{
			if($rsPerson["PER_NACHNAME"][0]!="")
			{
				$Ausgabe = $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0];
			}
		}
	}
	else
	{
		$Ausgabe .= $Beschreibung . ':</td><td><span class=DatenFeldNormal> ::keine Angabe::';
	}

			// Weitere Mitarbeiter aus der Personaleinsatzpl�ne

	$SQL = "SELECT PER_Nr, PER_Nachname, Per_Vorname
				FROM PERSONAL, PERSONALEINSATZPLANUNG, PERSONALTAETIGKEITEN
				 WHERE PER_Nr = PEP_PER_Nr AND PET_ID = PER_PET_ID
				  AND PEP_FIL_ID = " . ($rsFilialInfo["FIF_FIL_ID"][0]==''?'0':$rsFilialInfo["FIF_FIL_ID"][0]) . "
				  AND PER_AUSTRITT IS NULL
				  AND PET_TAETIGKEIT = '";			// Wieder auf die Zahl->Stefan Import
	$AusgabeAlternativ=0;
	switch($WertID)
	{
		case 74:				// 1. Gesch?ftsleiter
			$SQL.="1. Gesch�ftsleiter/in'";
			$AusgabeAlternativ=1;
			break;
		case 75:				// 2. Gesch?ftsleiter
			$SQL.="2. Gesch�ftsleiter/in'";
			$AusgabeAlternativ=1;
			break;
		case 76:				// 1. Werkstattleiter
			$SQL.="1. Werkstattleiter/in'";
			$AusgabeAlternativ=1;
			break;
		case 77:				// 2. Werkstattleiter
			$SQL.="2. Werkstattleiter/in'";
			$AusgabeAlternativ=1;
			break;
	}

	if($AusgabeAlternativ==1)
	{
		$SQL .= " AND PEP_JAHR = " . date("Y",time()) . " AND PEP_KALENDERWOCHE = " . date("W",time());

		$rsPEP = awisOpenRecordset($con,$SQL);

		for($pep=0;$pep<$awisRSZeilen;$pep++)
		{

			if($rsPEP['PER_NR'][$pep]!=$rsPerson['PER_NR'][0])
			{
				$Ausgabe .= '<br><a ' . ($Link?'href=./filialinfo_Main.php?cmdAktion=Personal':'') . ' name=Alternativ title=\''.$Beschreibung.' laut Personaleinsatzplan\'>*</a><i>' . $rsPEP['PER_NACHNAME'][$pep] . ', ' . $rsPEP['PER_VORNAME'][$pep] . '</i>';
			}
		}
	}

	echo "" . $Ausgabe . "</span></td></tr></table></td></tr>";
}

?>