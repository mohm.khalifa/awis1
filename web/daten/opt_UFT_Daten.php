<?php
//**************************************************************
// Unfallursache --> Spezifische Ursache!
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
/*
if(!isset($_GET['UFT_GEHOERTZUUFTKEY']))
{
	die('##PARAM##');
}*/
if(!isset($_GET['BEREICHID']))
{
	die('##PARAM##');
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

if(isset($_GET['UFT_GEHOERTZUUFTKEY']))
{
    if(! empty($_GET['UFT_GEHOERTZUUFTKEY']))
    {
    $SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' . 
            'FROM unfallmeldunginfotypen ' .
            'WHERE uft_bereichid  = ' . $DB->FeldInhaltFormat('N0', $_GET['BEREICHID']) . ' ' . 
            'AND uft_ueberschrift = 0 ' .		
            'AND uft_status       = 1 ' .  		
            'AND uft_gehoertzuuftkey = ' . $DB->FeldInhaltFormat('N0', $_GET['UFT_GEHOERTZUUFTKEY']) . ' ' .
            'ORDER BY uft_kennung, uft_information';    	
    }
    else
    {
        $SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' . 
                'FROM unfallmeldunginfotypen ' .
                'WHERE uft_bereichid  = ' . $DB->FeldInhaltFormat('N0', $_GET['BEREICHID']) . ' ' . 
                'AND uft_ueberschrift = 0 ' .		
                'AND uft_status       = 1 ' .  		
                'ORDER BY uft_kennung, uft_information';    	    
    }
}

$rsPEI = $DB->RecordSetOeffnen($SQL);


if (isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
//echo $SQL.'#~#';		// Trennzeichen

echo '#~#';		// Trennzeichen

$DS=0;
if(isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ'] != '')
{
	$Zusatz = explode('~', urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}
/*
if ($rs->EOF())
{
	if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false)
	{
		echo '<option value="">::--::</option>';
	}
	else
	{
		echo '#+##+#::--::';
	}
}
*/

while(!$rsPEI->EOF())
{
	if(++$DS>200 AND $rsPEI->FeldInhalt('UFT_KEY') == $_GET['WERT'])
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPEI->FeldInhalt('UFT_KEY').'">'.$rsPEI->FeldInhalt('UFT_INFORMATION').'</option>';
		}
		else
		{
			echo '#+#'.$rsPEI->FeldInhalt('UFT_KEY').'#+#'.$rsPEI->FeldInhalt('UFT_INFORMATION').'';
		}
	}
	elseif($DS<200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPEI->FeldInhalt('UFT_KEY').'">'.$rsPEI->FeldInhalt('UFT_INFORMATION').'</option>';
		}
		else
		{
			echo '#+#'.$rsPEI->FeldInhalt('UFT_KEY').'#+#'.$rsPEI->FeldInhalt('UFT_INFORMATION').'';
		}
	}

	$rsPEI->DSWeiter();
}
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else
	{
		echo '#+##+#...';
	}
}
?>