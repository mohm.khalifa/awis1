<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

$POSTbenutzen = true;

try
{
    require_once 'heidler_funktionen.php';

    $Functions = new heidler_funktionen();
    $UserFilialen = $Functions->_AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
    $FilialNummer = is_numeric($UserFilialen)?$UserFilialen:1;

    $Recht45000 = $Functions->_AWISBenutzer->HatDasRecht(45000);

    //********************************************************
    // Recht pr�fen
    //********************************************************
    if ($Recht45000 == 0) {
        $Functions->_Form->Formular_Start();
        $Functions->_Form->Fehler_KeineRechte();
        $Functions->_Form->Formular_Ende();
        die();
    }
    
    $OptionBitteWaehlen = '~'.$Functions->AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    $POSTbenutzen = false;

    $Functions->_Form->DebugAusgabe(1, $_POST);
    $Functions->_Form->DebugAusgabe(1, $_GET);

    if(isset($_POST['cmdMailSenden'])) { // Mail senden?
        $Functions->versendeMail($_POST['txtNDK_KEY']);
        $POSTbenutzen = false;
        $AWIS_KEY1 = -1;
    }

    // die letzte Differenzmeldung der Filiale laden
    $SQL = '  SELECT *';
    $SQL .= ' FROM nvsdifferenzkopf';
    $SQL .= ' WHERE ndk_anzmails = 0';
    $SQL .= ' AND ndk_key = ';
    if(isset($_POST['cmdDSNeu_x']) or isset($_POST['cmdSpeichern_x'])) {
        $AWIS_KEY1 = -1;
        $SQL .= $Functions->_DB->WertSetzen('NDK', 'N0', $AWIS_KEY1);
    } else {
        $SQL .= ' (';
        $SQL .= '   SELECT max(ndk_key) as ndk_key';
        $SQL .= '   FROM nvsdifferenzkopf';
        $SQL .= '   WHERE ndk_fil_id = ' . $Functions->_DB->WertSetzen('NDK', 'N0', $FilialNummer);
        $SQL .= ' )';
    }

    $rsNDK = $Functions->_DB->RecordSetOeffnen($SQL,$Functions->_DB->Bindevariablen('NDK', true));
    $Functions->_Form->DebugAusgabe(1, $Functions->_DB->LetzterSQL());

    if($rsNDK->AnzahlDatensaetze() > 0) {
        $AWIS_KEY1 = $rsNDK->FeldInhalt('NDK_KEY');
    } else {
        $AWIS_KEY1 = -1;
    }

    $Functions->_Form->DebugAusgabe(1,$AWIS_KEY1);

    if (isset($_GET['NDP_KEY'])) {
        $Functions->loeschePosition($_GET['NDP_KEY']);
    }

    if(isset($_POST['ico_new__x'])) {
        if($_POST['ico_new__x'] != 0) {
            $POSTbenutzen = true;
            $anzahlPos = $Functions->getMaximalePosNummer() + 1;
            $anzahlPos++;
        } else {
            $anzahlPos = $Functions->getMaximalePosNummer();
            $anzahlPos = $anzahlPos<1?1:$anzahlPos;
        }
    } else {
        $anzahlPos = $Functions->getMaximalePosNummer();
        $anzahlPos = $anzahlPos<1?1:$anzahlPos;
    }

    if (isset($_POST['cmdSpeichern_x']) or isset($_POST['ico_new__x'])) { //Speichern?

        include('./heidler_speichern.php');

        $icoAdd_x = (isset($_POST['ico_new__x'])?$_POST['ico_new__x']:0);

        if($icoAdd_x == 0) {

            echo '<form name=frmHeidlerMail action=./heidler_Main.php?cmdAktion=DifferenzNVS method=POST>';

            // Abfragen ob Mail an Warenbestands-Abteilung gesendet werden soll
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Hinweistext('Datensatz gespeichert. Mail an Warenbest�nde senden?',awisFormular::HINWEISTEXT_OK);
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->Erstelle_HiddenFeld('NDK_KEY', $AWIS_KEY1);

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Schaltflaeche('submit','cmdMailSenden','','',$Functions->AWISSprachKonserven['Wort']['Ja'],'');
            $Functions->_Form->Schaltflaeche('submit','cmdKeineMailSenden','','',$Functions->AWISSprachKonserven['Wort']['Nein'],'');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->SchreibeHTMLCode('</form>');

            $Functions->_Form->Trennzeile('L');
        }

        //nach dem Speichern Daten frisch aus der Datenbank laden
        $rsNDK = $Functions->ladeDaten($FilialNummer, $AWIS_KEY1);
    }

    $LabelBreite = 200;

    $Functions->_Form->Formular_Start();
    echo '<form name=frmHeidler action=./heidler_Main.php?cmdAktion=DifferenzNVS method=POST>';

    $Functions->_Form->Erstelle_HiddenFeld('NDK_KEY', $rsNDK->FeldInhalt('NDK_KEY'));

    $Functions->_Form->ZeileStart();

    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILNR'] . ':',100);
    $Functions->_Form->Erstelle_TextFeld('FIL_ID',$FilialNummer,10,120,false);
    $Functions->_Form->Erstelle_HiddenFeld('NDK_FIL_ID', $FilialNummer);

    $FilBez = '';
    $SQL = 'SELECT * FROM FILIALEN WHERE FIL_ID = ' . $Functions->_DB->WertSetzen('FIL', 'N0', $FilialNummer);
    $rsFil = $Functions->_DB->RecordSetOeffnen($SQL,$Functions->_DB->Bindevariablen('FIL',true));
    $Functions->_Form->DebugAusgabe(1, $Functions->_DB->LetzterSQL());
    if($rsFil->AnzahlDatensaetze() > 0) {
        $FilBez = $rsFil->FeldInhalt('FIL_BEZ');
    }

    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILBEZ'] . ':',120);
    $Functions->_Form->Erstelle_TextFeld('FILIALE',$FilBez,80,170,false);
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Dienstleister'].':',180);
    $Daten = explode('|',$Functions->AWISSprachKonserven['NDK']['lst_NDK_DIENSTLEISTER']);
    $Functions->_Form->Erstelle_SelectFeld('!NDK_DIENSTLEISTER',$rsNDK->FeldOderPOST('NDK_DIENSTLEISTER', 'T', $POSTbenutzen), 120, true, '',$OptionBitteWaehlen,'','','',$Daten);
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    if($rsNDK->FeldInhalt('NDK_ANSPRECHPARTNER') != '') {
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Name'] . ':', 180);
        $Functions->_Form->Erstelle_TextFeld('NDK_ANSPRECHPARTNER', $rsNDK->FeldOderPOST('NDK_ANSPRECHPARTNER', 'T', $POSTbenutzen),180, 200, true);
        $Functions->_Form->ZeileEnde();
    } else {
        $Functions->_Form->ZeileStart('width:1024px;');
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['PER']['PER_NR'].':',180);
        $Block = 'NDK_ANSPRECHPARTNER';
        $Functions->_Form->AuswahlBox('!'.$Block,'box_ansprechpartner','','NDK_Ansprechpartner','', 80, 8, '','T',true,'','','',0,'','','',0,'','autocomplete=off');

        $Functions->_Form->AuswahlBoxHuelle('box_ansprechpartner','','');
        $Functions->_Form->ZeileEnde();
    }

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Meldegrund'].':',180);
    $Daten = explode('|',$Functions->AWISSprachKonserven['NDK']['lst_NDK_GRUND']);
    $Functions->_Form->Erstelle_SelectFeld('!NDK_MELDEGRUND',$rsNDK->FeldOderPOST('NDK_GRUND', 'N0', $POSTbenutzen), 180, true, '',$OptionBitteWaehlen,'','','',$Daten);
    $Functions->_Form->ZeileEnde();


    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['NachtraeglichZugestellt'] . ':',180);
    $Functions->_Form->Erstelle_Checkbox('NDK_ZUGESTELLT', $rsNDK->FeldOderPOST('NDK_ZUGESTELLT', 'N0', $POSTbenutzen) == 1?'on':'', 80, true);
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['LIEFERSCHEINDATUM'] . ':',180);
    $Functions->_Form->Erstelle_TextFeld('NDK_LIEFERSCHEINDATUM',$rsNDK->FeldOderPOST('NDK_LIEFERSCHEINDATUM', 'D', $POSTbenutzen),15,200,true,'','','','D');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $FeldBreiten['NDP_AST_ATUNR'] = 80;
    $FeldBreiten['NDP_AST_BEZEICHNUNGWW'] = 320;
    $FeldBreiten['NDP_MENGE'] = 100;
    $FeldBreiten['NDP_BEMERKUNG'] = 300;

    $IconsAdd[] = array('new','POST~');

    $Functions->_Form->Erstelle_ListeIcons($IconsAdd, 25);
    $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_AST_ATUNR'], $FeldBreiten['NDP_AST_ATUNR']);
    $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_AST_BEZEICHNUNGWW'], $FeldBreiten['NDP_AST_BEZEICHNUNGWW']);
    $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_MENGE'], $FeldBreiten['NDP_MENGE']);
    $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_BEMERKUNG'], $FeldBreiten['NDP_BEMERKUNG']);

    $SQL = '  SELECT *';
    $SQL .= ' FROM nvsdifferenzpos LEFT JOIN ';
    $SQL .= ' (';
    $SQL .= '   SELECT ast_atunr, ast_bezeichnungww FROM artikelstamm ';
    $SQL .= '   UNION ALL ';
    $SQL .= '   SELECT hba_artnr as ast_atunr, hba_artbezl as ast_bezeichnungww FROM hubstamm ';
    $SQL .= ' )';
    $SQL .= ' ON ndp_ast_atunr = ast_atunr';
    $SQL .= ' WHERE ndp_ndk_key = ' . $Functions->_DB->WertSetzen('NDP', 'N0', $AWIS_KEY1);
    $SQL .= ' ORDER BY ndp_key';

    $rsNDP = $Functions->_DB->RecordSetOeffnen($SQL, $Functions->_DB->Bindevariablen('NDP', true));
    $Functions->_Form->DebugAusgabe($Functions->_DB->LetzterSQL());

    $i = 0;

    while(!$rsNDP->EOF()) {
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_HiddenFeld('NDP_KEY_'.$i, $rsNDP->FeldInhalt('NDP_KEY'));
        $IconsDel[] = array('delete','/heidler/heidler_Main.php?cmdAktion=DifferenzNVS&NDP_KEY='.$rsNDP->FeldInhalt('NDP_KEY'));
        $Functions->_Form->Erstelle_ListeIcons($IconsDel, 25);
        unset($IconsDel);
        $Functions->_Form->Erstelle_ListenFeld('NDP_AST_ATUNR_'.$i,$rsNDP->FeldInhalt('NDP_AST_ATUNR'),8,80,true);
        $Functions->_Form->Erstelle_ListenFeld('NDP_AST_BEZEICHNUNGWW_'.$i,$rsNDP->FeldInhalt('AST_BEZEICHNUNGWW'),50,320,true);
        $Functions->_Form->Erstelle_ListenFeld('!NDP_MENGE_'.$i,$rsNDP->FeldInhalt('NDP_MENGE'),10,100,true);
        $Functions->_Form->Erstelle_ListenFeld('NDP_BEMERKUNG_'.$i,$rsNDP->FeldInhalt('NDP_BEMERKUNG'),80,300,true);
        $Functions->_Form->ZeileEnde();

        $i++;
        $rsNDP->DSWeiter();
    }

    while($i < $anzahlPos) {
        $Functions->_Form->ZeileStart('width:1024px;');
        $Functions->_Form->Erstelle_ListeIcons('', 25);
        $Block = 'NDP_AST_ATUNR';
        $Functions->_Form->AuswahlBox($Block.'_'.$i,'box_'.$i,'','NDP_Artbez','', 80, 8, $rsNDP->FeldOderPOST('NDP_AST_ATUNR_'.$i, 'T', $POSTbenutzen),'T',true,'','','height:19px;font-size:12px;',0,'','','',0,'','autocomplete=off');

        ob_start();
        $rsArtbez = $Functions->getArtikelbezeichnung($rsNDP->FeldInhalt('NDP_AST_ATUNR_'.$i));
        $Functions->_Form->Erstelle_ListenFeld('NDP_AST_BEZEICHNUNGWW_'.$i,$rsArtbez->FeldInhalt('AST_BEZEICHNUNGWW'),50,320, true);
        $Listenfeld = ob_get_clean();

        $Functions->_Form->AuswahlBoxHuelle('box_'.$i,'','',$Listenfeld);
        $Functions->_Form->Erstelle_ListenFeld('!NDP_MENGE_'.$i,$rsNDP->FeldInhalt('NDP_MENGE_'.$i, 'N0', $POSTbenutzen),10,100,true);
        $Functions->_Form->Erstelle_ListenFeld('NDP_BEMERKUNG_'.$i,$rsNDP->FeldInhalt('NDP_BEMERKUNG_'.$i, 'T', $POSTbenutzen),80,300,true);
        $Functions->_Form->ZeileEnde();

        $i++;
    }

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->Formular_Ende();

    $Functions->_Form->SchaltflaechenStart();
    $Functions->_Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Functions->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    $Functions->_Form->SchaltflaechenEnde();


    $Functions->_Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
    if($Functions->_Form instanceof awisFormular)
    {
        $Functions->_Form->DebugAusgabe(1, $ex->getSQL());
        $Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else
    {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex)
{

    if($Functions->_Form instanceof awisFormular)
    {

        $Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}