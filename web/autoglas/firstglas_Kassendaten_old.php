<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FGP','FGP*');
        $TextKonserven[]=array('FPH','FPH_*');
        $TextKonserven[]=array('FKA','FKA_*');
        $TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

        $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

        if($Recht9100==0)
        {
            $Form->Formular_Start();
            $Form->Fehler_KeineRechte();
            $Form->Formular_Ende();
            die();
        }

    if($AWIS_KEY1!=0)
    {

        if($AWIS_HISTKEY == 0 && $AWIS_KEY1 !=0)
        {
               $AWIS_KEY2 = $AWIS_KEY1;
               $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY='.$DB->FeldInhaltFormat('NO', $AWIS_KEY1, false);
               $rsKopf = $DB->RecordSetOeffnen($SQL);
               $VorgangNr = $rsKopf->FeldInhalt('FGK_VORGANGNR');
               $SQL = 'Select FKASSENDATEN.* from FKASSENDATEN WHERE FKA_AEMNR='.$DB->FeldInhaltFormat('T', $VorgangNr, false);
               $rsKassendaten = $DB->RecordSetOeffnen($SQL);
        }
        else
        {
            if($AWIS_HISTKEY==0 && $AWIS_KEY1 !=0)
            {
                $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
                $AWIS_KEY2 = $Param['FGK_KEY'];
                $AWIS_KEY2 = $AWIS_KEY1;
                $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY='.$DB->FeldInhaltFormat('NO', $AWIS_KEY2, false);
                $rsKopf = $DB->RecordSetOeffnen($SQL);
                $VorgangNr = $rsKopf->FeldInhalt('FGK_VORGANGNR');
                $SQL = 'Select FKASSENDATEN.* from FKASSENDATEN WHERE FKA_AEMNR='.$DB->FeldInhaltFormat('T', $VorgangNr, false);
                $rsKassendaten = $DB->RecordSetOeffnen($SQL);

            }
        }


    if ($AWIS_KEY1!=0 || $AWIS_KEY2!=0)
    {
    
        $EditRecht=(($Recht9100&2)!=0);
        $Form->Formular_Start();

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KEY'],60,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KENNUNG'],66,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_FILID'],66,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_DATUM'],100,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_UHRZEIT'],100,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BSA'],50,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_WANR'],80,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_AEMNR'],120,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_ATUNR'],125,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_MENGE'],60,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BETRAG'],100,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'],100,'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BEMERKUNG'],100,'');
        $Form->ZeileEnde();

        $FGKZeile=0;

    while(!$rsKassendaten->EOF())
    {
        $Form->ZeileStart();
	$Link = './firstglas_Main.php?cmdAktion=Details&FKA_KEY='.$rsKassendaten->FeldInhalt('FKA_KEY').'';
	$Form->Erstelle_ListenFeld('FKA_KEY',$rsKassendaten->FeldInhalt('FKA_KEY'),0,60,false,($FGKZeile%2),'','');
	$Form->Erstelle_ListenFeld('FKA_KENNUNG',$rsKassendaten->FeldInhalt('FKA_KENNUNG'),0,66,false,($FGKZeile%2),'','');
	$Form->Erstelle_ListenFeld('FKA_FILID',$rsKassendaten->FeldInhalt('FKA_FILID'),0,66,false,($FGKZeile%2),'','');
        $DATUM = str_replace("00:00:00", "", $rsKassendaten->FeldInhalt('FKA_DATUM'));
        $Form->Erstelle_ListenFeld('FKA_DATUM',$DATUM,0,100,false,($FGKZeile%2),'','');
	$Form->Erstelle_ListenFeld('FKA_UHRZEIT',$rsKassendaten->FeldInhalt('FKA_UHRZEIT'),0,100,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_BSA',$rsKassendaten->FeldInhalt('FKA_BSA'),0,50,false,($FGKZeile%2),'','');
	$Form->Erstelle_ListenFeld('FKA_WANR',$rsKassendaten->FeldInhalt('FKA_WANR'),0,80,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_AEMNR',$rsKassendaten->FeldInhalt('FKA_AEMNR'),13,120,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_ATUNR',$rsKassendaten->FeldInhalt('FKA_ATUNR'),0,125,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_MENGE',$rsKassendaten->FeldInhalt('FKA_MENGE'),0,60,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_BETRAG',$rsKassendaten->FeldInhalt('FKA_BETRAG'),0,100,false,($FGKZeile%2),'font-weight:bold','','N2');
        $Form->Erstelle_ListenFeld('FKA_KFZKENNZ',$rsKassendaten->FeldInhalt('FKA_KFZKENNZ'),0,100,false,($FGKZeile%2),'','');
        $Form->Erstelle_ListenFeld('FKA_BEMERKUNG',$rsKassendaten->FeldInhalt('FKA_BEMERKUNG'),15,100,false,($FGKZeile%2),'','');
        $Form->ZeileEnde();

        $FGKZeile++;

        $rsKassendaten->DSWeiter();
    }
    }
        }
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
            echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>