<?php
//**************************************************************
// Liefert die Daten f�r das Testformular
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('/daten/web/gdvService/felder/felder_funktionen.php');

if (!isset($_GET['WERT'])) {
     die('##PARAM##');
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Felder = new felder_funktionen();

//Versicherungsnummer ist immer eine Bedingung
$Bedingung[$Felder::Bedingung_VVE_VERSNR] = $_GET['VVE_VERSNR'];

if (isset($_GET['TYP'])) {
    switch ($_GET['TYP']) {
        case 'Satzarten':
            $Bedingung[$Felder::Bedingung_GDN_NUMMER] = $_GET['txtGDN_NACHRICHTENTYPEN']; //Satzarten hängen am Nachrichtentypen
            $SQL = $Felder->holeFelderDetailDaten($Felder::TYP_SATZARTEN, $Felder::RETURNTYP_SQL, $Bedingung);
            break;
        case 'Felder':
            $Bedingung[$Felder::Bedingung_GDS_NUMMER] = $_GET['txtGDN_SATZARTEN'];
            $Bedingung[$Felder::Bedingung_GDN_NUMMER] = $_GET['txtGDN_NACHRICHTENTYPEN'];
            $SQL = $Felder->holeFelderDetailDaten($Felder::TYP_FELDER, $Felder::RETURNTYP_SQL, $Bedingung);
            break;
    }
}

$Datei = fopen('debug.log', 'w');
fwrite($Datei, $SQL);
foreach ($_GET as $Key => $Val) {
    fwrite($Datei, PHP_EOL . $Key . ' = ' . $Val);
}
$rsGDV = $DB->RecordSetOeffnen($SQL);

if (isset($_GET['WERT'])) {
    echo $_GET['WERT'];
}
echo '#~#';        // Trennzeichen

$DS = 0;

if (isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ'] != '') {
    $Zusatz = explode('~', urldecode($_GET['ZUSATZ']));
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
        echo '<option selected="selected" disabled="disabled" value="' . $Zusatz[0] . '">' . $Zusatz[1] . '</option>';
    } else {
        echo '#+#' . $Zusatz[0] . '#+#' . $Zusatz[1] . '';
    }
}

while (!$rsGDV->EOF()) {
    if (++$DS > 200) {
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
            echo '<option value="' . $rsGDV->FeldInhalt(1) . '">' . $rsGDV->FeldInhalt(1) . '~' . $rsGDV->FeldInhalt(2) . '</option>';
        } else {
            echo '#+#' . $rsGDV->FeldInhalt('GDS_NUMMER') . '#+#' . $rsGDV->FeldInhalt('GDS_NUMMER') . '~' . $rsGDV->FeldInhalt('GDS_BEZEICHNUNG') . '';
        }
    } elseif ($DS < 200) {
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
            echo '<option value="' . $rsGDV->FeldInhalt(1) . '">' . $rsGDV->FeldInhalt(1) . '~' . $rsGDV->FeldInhalt(2) . '</option>';
        } else {
            echo '#+#' . $rsGDV->FeldInhalt(1) . '#+#' . $rsGDV->FeldInhalt(1) . '~' . $rsGDV->FeldInhalt(2) . '';
        }
    }

    $rsGDV->DSWeiter();
}
if (++$DS > 200) {
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
        echo '<option value="">...</option>';
    } else {
        echo '#+##+#...';
    }
}

?>