<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIE', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'PDFErzeugen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'AktuellesSortiment');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'KeineZuordnungGefunden');
    $TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('AST', 'AST_VK');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht605 = $AWISBenutzer->HatDasRecht(605);
    $Recht608 = $AWISBenutzer->HatDasRecht(608);
    $Recht609 = $AWISBenutzer->HatDasRecht(609);
    $Recht611 = $AWISBenutzer->HatDasRecht(611);
    $Recht612 = $AWISBenutzer->HatDasRecht(612);
    $Recht613 = $AWISBenutzer->HatDasRecht(613);
    $Recht614 = $AWISBenutzer->HatDasRecht(614);
    $Recht615 = $AWISBenutzer->HatDasRecht(615);

    if ($Recht605 == 0) {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
        die();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));
    $DetailAnsicht = false;
    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['LIE_NR'] = $_POST['sucLIE_NR'];
        $Param['LIE_NAME1'] = $_POST['sucLIE_NAME1'];

        // Suchfelder aus den Infotypen
        // z.B. sucITY_KEY_17
        $Param['Suchfelder'] = array();
        $Felder = $Form->NameInArray($_POST, 'sucITY_KEY_', 2, 0);
        foreach ($Felder as $Feld) {
            $FeldTeile = explode('_', $Feld);
            $Param['Suchfelder'][$FeldTeile[2]] = $_POST[$Feld];
        }

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./lieferanten_loeschen.php');
        if ($AWIS_KEY1 != 0) {
            $Param['KEY'] = $AWIS_KEY1;
        }
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./lieferanten_speichern.php');
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));
    } elseif (isset($_GET['LIE_NR'])) {
        $AWIS_KEY1 = $Form->Format('T', $_GET['LIE_NR']);
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));
        $Param['KEY'] = $AWIS_KEY1;
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));

        if (!isset($Param['KEY'])) {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['KEY'] = 0;
        }

        $AWIS_KEY1 = $Param['KEY'];
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY LIE_NR';
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    /*
    $SQL = 'SELECT lieferanten.*';
    $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
    $SQL .= ' FROM lieferanten';
    */
    $SQL = 'SELECT lie.*, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 48) AS lin_strasse2, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 49) AS lin_telefon3, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 50) AS lin_telex, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 51) AS lin_mail, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 52) AS lin_reklland, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 53) AS lin_rekltelefon, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 54) AS lin_rekltelex, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 55) AS lin_reklansprechp, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 56) AS lin_reklmail, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 57) AS lin_reklsperre, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 58) AS lin_versand, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 59) AS lin_reklnotiz1, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 60) AS lin_liefer_n, ';
    $SQL .= '(SELECT lin_wert FROM lieferanteninfos WHERE lin_lie_nr = lie.lie_nr AND lin_ity_key = 61) AS lin_liefer_l, ';
    $SQL .= 'row_number() OVER (ORDER BY lie_nr) AS ZeilenNr ';
    $SQL .= 'FROM lieferanten lie';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('LIE', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $rsLIE = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('LIE'));
    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmZukaufartikel action=./lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'') . ' method=POST enctype="multipart/form-data">';

    if ($rsLIE->EOF() AND !isset($_POST['cmdDSNeu_x']))        // Keine Meldung bei neuen Datens�tzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsLIE->AnzahlDatensaetze() > 1)                        // Liste anzeigen
    {
        $DetailAnsicht = false;
        $Form->Formular_Start();

        $Form->ZeileStart();
        $Link = './lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=LIE_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LIE_NR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NR'], 100, '', $Link);
        $Link .= '&Sort=LIE_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LIE_NAME1'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'], 400, '', $Link);
        $Link .= '&Sort=LIE_NAME2' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LIE_NAME2'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME2'], 400, '', $Link);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsLIE->EOF()) {
            $Form->ZeileStart();
            $Link = './lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $rsLIE->FeldInhalt('LIE_NR') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Form->Erstelle_ListenFeld('LIE_NR', $rsLIE->FeldInhalt('LIE_NR'), 0, 100, false, ($DS % 2), '', $Link);
            $Link = '';    // TODO: Sp�ter auf die Hersteller
            $Form->Erstelle_ListenFeld('LIE_NAME1', $rsLIE->FeldInhalt('LIE_NAME1'), 0, 400, false, ($DS % 2), '', $Link, 'T', 'L', $rsLIE->FeldInhalt('ZLH_BEZEICHNUNG'));
            $Form->Erstelle_ListenFeld('LIE_NAME2', $rsLIE->FeldInhalt('LIE_NAME2'), 0, 400, false, ($DS % 2), '', $Link);
            $Form->ZeileEnde();

            $rsLIE->DSWeiter();
            $DS++;
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $DetailAnsicht = true;
        $AWIS_KEY1 = $rsLIE->FeldInhalt('LIE_NR');

        $Param['KEY'] = $AWIS_KEY1;

        echo '<input type=hidden name=txtLIE_NR value=' . $AWIS_KEY1 . '>';

        $Form->Formular_Start();
        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./lieferanten_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIE->FeldInhalt('LIE_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIE->FeldInhalt('LIE_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = ((($Recht605 & 2) != 0) AND ($rsLIE->FeldInhalt('LIE_BEKANNTWW') == 0));

        if ((($Recht612 & 16) != 0) AND ($AWIS_KEY1 > 0)) {
            $LinkPDF = '/berichte/drucken.php?XRE=15&ID=' . base64_encode('LIE_NR=' . urlencode('=~') . $AWIS_KEY1);

            /* $Form->ZeileStart();
            $Form->Erstelle_TextFeld('lnkPDF', $AWISSprachKonserven['LIE']['PDF_DruckenLink'], 0, 800, false, '','', $LinkPDF);
            $Form->ZeileEnde(); */
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['PDF_DruckenLink'], 800, '', $LinkPDF);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->ZeileEnde();
        }

        //�berschrift Lieferantenanschrift
        $Form->ZeileStart();
        $Form->Erstelle_TextFeld('PDF_LieferantenAnschrift', $AWISSprachKonserven['LIE']['PDF_LieferantenAnschrift'] . ':', 20, 200, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->ZeileEnde();

        // Nummer und Hersteller
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NR'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_NR', $rsLIE->FeldInhalt('LIE_NR'), 5, 370, $EditRecht);
        if (!$EditRecht) {
            $Form->Erstelle_HiddenFeld('LIE_NR', $rsLIE->FeldInhalt('LIE_NR'));
        }
        $AWISCursorPosition = 'txtLIE_NR';
        $Form->ZeileEnde();

        // Bezeichnung
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NAME1'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_NAME1', $rsLIE->FeldInhalt('LIE_NAME1'), 55, 370, $EditRecht);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NAME2'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_NAME2', $rsLIE->FeldInhalt('LIE_NAME2'), 55, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_STRASSE'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_STRASSE', $rsLIE->FeldInhalt('LIE_STRASSE'), 55, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_STRASSE2'] . ':', 200);
        $Form->Erstelle_TextFeld('LIN_STRASSE2', $rsLIE->FeldInhalt('LIN_STRASSE2'), 55, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_LAND'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_LAND', $rsLIE->FeldInhalt('LIE_LAND'), 5, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_PLZ'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_PLZ', $rsLIE->FeldInhalt('LIE_PLZ'), 5, 100, $EditRecht);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_ORT'] . ':', 90);
        $Form->Erstelle_TextFeld('LIE_ORT', $rsLIE->FeldInhalt('LIE_ORT'), 55, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_TELEFON'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_TELEFON', $rsLIE->FeldInhalt('LIE_TELEFON'), 55, 250, $EditRecht);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_TELEX'] . ':', 140);
        $Form->Erstelle_TextFeld('LIN_TELEX', $rsLIE->FeldInhalt('LIN_TELEX'), 55, 150, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_TELEFAX'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_TELEFAX', $rsLIE->FeldInhalt('LIE_TELEFAX'), 55, 250, $EditRecht);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_TELEFON3'] . ':', 140);
        $Form->Erstelle_TextFeld('LIN_TELEFAX2', $rsLIE->FeldInhalt('LIN_TELEFON3'), 55, 150, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_ANSPRECHP1'] . ':', 200);
        $Form->Erstelle_TextFeld('LIE_ANSPRECHP1', $rsLIE->FeldInhalt('LIE_ANSPRECHP1'), 55, 300, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_MAIL'] . ':', 200);
        $Form->Erstelle_TextFeld('LIN_MAIL', $rsLIE->FeldInhalt('LIN_MAIL'), 5, 370, $EditRecht);
        $Form->ZeileEnde();

        if (($Recht605 & 64) == 64) {
            $Form->Trennzeile();

            //�berschrift Lieferantenanschrift	
            $Form->ZeileStart();
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('PDF_Reklamationsanschift', $AWISSprachKonserven['LIE']['PDF_Reklamationsanschift'] . ':', 33, 300, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_REKLNAME'] . ':', 200);
            $Form->Erstelle_TextFeld('LIE_REKLNAME', $rsLIE->FeldInhalt('LIE_REKLNAME'), 55, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_REKLSTRASSE'] . ':', 200);
            $Form->Erstelle_TextFeld('LIE_REKLSTRASSE', $rsLIE->FeldInhalt('LIE_REKLSTRASSE'), 55, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLLAND'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_REKLLAND', $rsLIE->FeldInhalt('LIN_REKLLAND'), 5, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_REKLPLZ'] . ':', 200);
            $Form->Erstelle_TextFeld('LIE_REKLPLZ', $rsLIE->FeldInhalt('LIE_REKLPLZ'), 5, 100, $EditRecht);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_REKLORT'] . ':', 90);
            $Form->Erstelle_TextFeld('LIE_REKLORT', $rsLIE->FeldInhalt('LIE_REKLORT'), 55, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLTELEFON'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_REKLTELEFON', $rsLIE->FeldInhalt('LIN_REKLTELEFON'), 55, 250, $EditRecht);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLTELEX'] . ':', 140);
            $Form->Erstelle_TextFeld('LIN_REKLTELEX', $rsLIE->FeldInhalt('LIN_REKLTELEX'), 55, 150, $EditRecht);
            $Form->ZeileEnde();
            /*
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_TELEFAX'].':',200);
            $Form->Erstelle_TextFeld('LIE_TELEFAX',$rsLIE->FeldInhalt('LIE_TELEFAX'),55,250,$EditRecht);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_TELEFON3'].':',140);
            $Form->Erstelle_TextFeld('LIN_TELEFAX2',$rsLIE->FeldInhalt('LIN_TELEFON3'),55,150,$EditRecht);
            $Form->ZeileEnde();			
            */
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLANSPRECHP'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_REKLANSPRECHP', $rsLIE->FeldInhalt('LIN_REKLANSPRECHP'), 55, 300, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_BEKANNTWW'] . ':', 200);
            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
            $Form->Erstelle_SelectFeld('LIE_BEKANNTWW', $rsLIE->FeldInhalt('LIE_BEKANNTWW'), 250, $EditRecht, '', '', '', '', '', $Daten);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_VERSAND'] . ':', 140);
            $Form->Erstelle_TextFeld('LIN_VERSAND', $rsLIE->FeldInhalt('LIN_VERSAND'), 55, 300, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_DUMMYKZ'] . ':', 200);
            $Form->Erstelle_TextFeld('LIE_DUMMYKZ', $rsLIE->FeldInhalt('LIE_DUMMYKZ'), 5, 250, $EditRecht);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLSPERRE'] . ':', 140);
            $Form->Erstelle_TextFeld('LIN_REKLSPERRE', $rsLIE->FeldInhalt('LIN_REKLSPERRE'), 5, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLMAIL'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_REKLMAIL', $rsLIE->FeldInhalt('LIN_REKLMAIL'), 5, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_LIEFER_N'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_LIEFER_N', $rsLIE->FeldInhalt('LIN_LIEFER_N'), 5, 250, $EditRecht);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_LIEFER_L'] . ':', 140);
            $Form->Erstelle_TextFeld('LIN_LIEFER_L', $rsLIE->FeldInhalt('LIN_LIEFER_L'), 5, 370, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIN_REKLNOTIZ1'] . ':', 200);
            $Form->Erstelle_TextFeld('LIN_REKLNOTIZ1', $rsLIE->FeldInhalt('LIN_REKLNOTIZ1'), 5, 450, $EditRecht);
            $Form->ZeileEnde();
        }

        if (!isset($_POST['cmdDSNeu_x'])) {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:'');

            $Reg = new awisRegister(410);
            $Reg->ZeichneRegister($RegisterSeite);
        }

        $Form->Formular_Ende();
    }


    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../stammdaten_Main.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ((($Recht605 & 6) != 0 AND $DetailAnsicht) OR ($Recht608 & 6) != 0 OR ($Recht609 & 6) != 0 OR ($Recht611 & 2) != 0 OR ($Recht612 & 6) != 0 OR ($Recht613 & 6) != 0 OR ($Recht614 & 6) != 0 OR ($Recht615 & 6) != 0) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if ((($Recht605 & 4) != 0)) {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    if ((($Recht612 & 16) != 0) AND ($AWIS_KEY1 > 0)) {
        $LinkPDF = '/berichte/drucken.php?XRE=15&ID=' . base64_encode('LIE_NR=' . urlencode('=~') . $AWIS_KEY1);
        $Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
    }

    $Form->SchaltflaechenEnde();
    $AWISBenutzer->ParameterSchreiben("Formular_LIE", serialize($Param));

    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if ($AWIS_KEY1 != 0) {
        $Bedingung .= ' AND LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);

        return $Bedingung;
    }

    if (isset($Param['LIE_NR']) AND $Param['LIE_NR'] != '') {
        $Bedingung .= ' AND LIE_NR = ' . $DB->FeldInhaltFormat('T', $Param['LIE_NR'], false) . '';
    }
    if (isset($Param['LIE_NAME1']) AND $Param['LIE_NAME1'] != '') {
        $Bedingung .= ' AND (UPPER(LIE_NAME1) ' . $DB->LIKEoderIST($Param['LIE_NAME1'], awisDatenbank::AWIS_LIKE_UPPER) . ')';
    }

    if (isset($Param['Suchfelder'])) {
        foreach ($Param['Suchfelder'] AS $ITY_KEY => $Wert) {
            if ($Wert != '') {
                $Bedingung .= ' AND EXISTS(SELECT * FROM LIEFERANTENINFOS WHERE lin_lie_nr = lie_nr AND lin_ity_key = ' . $DB->WertSetzen('LIE', 'N0',
                        $ITY_KEY) . ' AND lin_wert = ' . $DB->WertSetzen('LIE', 'T', $Wert) . ')';
            }
        }
    }

    return $Bedingung;
}

?>