set lines 500;
set pagesize 0;
set heading off;
set feedback off;
set verify off;
set term off;
set trimspool on;
spool experian_kundenkfz_&1..csv
select
CAST(FAHRZEUG_ID as varchar2(50)) || ';' ||
KENNZEICHEN
from
skFLEET_EXPERIAN
WHERE OpalVertragsNr = &1
AND KENNZEICHEN IS NOT NULL
ORDER BY KENNZEICHEN;
exit;

