<?php

class BestandsauskunftResponse
{

    /**
     * @var ArrayOfBestandV3 $BestandsauskunftResult
     */
    protected $BestandsauskunftResult = null;

    /**
     * @param ArrayOfBestandV3 $BestandsauskunftResult
     */
    public function __construct($BestandsauskunftResult)
    {
      $this->BestandsauskunftResult = $BestandsauskunftResult;
    }

    /**
     * @return ArrayOfBestandV3
     */
    public function getBestandsauskunftResult()
    {
      return $this->BestandsauskunftResult;
    }

    /**
     * @param ArrayOfBestandV3 $BestandsauskunftResult
     * @return BestandsauskunftResponse
     */
    public function setBestandsauskunftResult($BestandsauskunftResult)
    {
      $this->BestandsauskunftResult = $BestandsauskunftResult;
      return $this;
    }

}
