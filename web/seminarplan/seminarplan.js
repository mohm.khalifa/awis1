$(document).ready(function () {

    $('#txtSEP_SRG_KEY').change(function () {
        if($('#txtSEP_SRG_KEY').val() == '-99'){
            $('#txtSEP_SER_KEY').val('-99');
            $('#txtSEP_SER_KEY').change();
            $('#txtSONST_SEP_SRG_KEY').show();
            $('#txtSONST_SEP_SPA_PLZ').show();
            $('#txtSONST_SEP_SPA_ORT').show();
            $('#txtSONST_SEP_SPA_STRASSE').show();
        }else{
            $('#txtSONST_SEP_SRG_KEY').hide();
            $('#txtSONST_SEP_SPA_PLZ').hide();
            $('#txtSONST_SEP_SPA_ORT').hide();
            $('#txtSONST_SEP_SPA_STRASSE').hide();
            $('#txtSONST_SEP_SRG_KEY').val(null);
            $('#txtSONST_SEP_SPA_PLZ').val('92637');
            $('#txtSONST_SEP_SPA_ORT').val('Weiden');
            $('#txtSONST_SEP_SPA_STRASSE').val('Dr. Kilian Strasse 11');
        }
    });

    $('#txtSEP_SER_KEY').change(function () {
        if($('#txtSEP_SER_KEY').val() == '-99'){
            $('#txtSONST_SEP_SER_KEY').show();
        }else{
            $('#txtSONST_SEP_SER_KEY').hide();
            $('#txtSONST_SEP_SER_KEY').val(null);
        }
    });
});