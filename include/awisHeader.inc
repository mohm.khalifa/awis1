<?php
/**
 * awisHeader.inc
 *
 * Kopfzeile f�r die Formulare
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200807
 *
 */

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');
global $AWISVersion;
$AWISVersion = 2;
try {
    // Objekte initialisieren
    $AWISBenutzer = awisBenutzer::Init();
    $AWISDB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISDB->Oeffnen();
    $Form = new awisFormular();
} catch (Exception $ex) {
    echo $ex->getMessage();
    die();
}
// Session starten f�r die Benutzerparameter
if (session_id() == '') {
    session_start();
}

echo '<table border="0" width="100%" cellspacing=0 cellpadding=0 style="border-spacing:0; margin: 0 0 0 0; background-image:url(/bilder/headerbild.png);">';
//echo '<table border="0" width="100%" cellspacing=0 cellpadding=0 style="border-spacing:0; margin: 0 0 0 0;">';

// Parameter Lesen
$MinAnzeige = $AWISBenutzer->ParameterLesen('MinimaleKopfzeile');
$ShortCutLeiste = $AWISBenutzer->ParameterLesen('ShortCutLeiste', true);

$pfad = dirname($_SERVER['SCRIPT_FILENAME']) . "/version.txt";

if (file_exists($pfad)) {
    $fd = fopen($pfad, "r");
    $Zeile = trim(fgets($fd));
    while ((substr($Zeile, 0, 8) != "[Header]") AND !feof($fd))        // Bis zum Header lesen
    {
        $Zeile = trim(fgets($fd));
    }

    $Zeile = trim(fgets($fd));
    $i = 0;

    while ((substr($Zeile, 0, 1) != "[") AND !feof($fd))        // Bis zum n�chsten Header lesen
    {                                                    // Alle Parameter auslesen
        if (substr($Zeile, 0, 1) != "#" AND substr($Zeile, 0, 1) != "") {
            $Werte = explode("=", $Zeile);
            $VInfo[strtoupper($Werte[0])] = $Werte[1];
        }

        $Zeile = trim(fgets($fd));
    }

    // Header schreiben

    echo "<tr><td width=20%><p align=left style='margin: 0 0 0 0'>";

    if (!$MinAnzeige) {
        echo "<a href=/index.php target=_self style='background-color: transparent;'><img alt=\"X\" src='";
        echo $VInfo["LOGO"];
        echo "' ></a></p>";
    } else {
        print "<font size=1pt ><b>" . $AWISBenutzer->BenutzerName() . "</b></font>";
    }
    echo "</td>";

    //**********************************************
    // Shortcut - Leiste
    //**********************************************
    $ShortCutLeiste = explode(';', $ShortCutLeiste);

    echo '<td width=40 valign=middle>';

    echo '<table border=0>';
    echo '<tr>';

    if (isset($ShortCutLeiste[1]) AND $ShortCutLeiste[1] != '') {
        echo '<td><a target=' . $ShortCutLeiste[0] . ' href=' . $ShortCutLeiste[1] . " onMouseover=\"window.status='" . $ShortCutLeiste[3] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[2] . ' title=\'' . $ShortCutLeiste[3] . '\' alt=\'' . $ShortCutLeiste[3] . '\'></a></td>';
    }
    if (isset($ShortCutLeiste[5]) AND $ShortCutLeiste[5] != '') {
        echo '<td><a target=' . $ShortCutLeiste[4] . ' href=' . $ShortCutLeiste[5] . " onMouseover=\"window.status='" . $ShortCutLeiste[7] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[6] . ' title=\'' . $ShortCutLeiste[7] . '\' alt=\'' . $ShortCutLeiste[7] . '\'></a></td>';
    }
    if (isset($ShortCutLeiste[9]) AND $ShortCutLeiste[9] != '') {
        echo '<td><a target=' . $ShortCutLeiste[8] . ' href=' . $ShortCutLeiste[9] . " onMouseover=\"window.status='" . $ShortCutLeiste[11] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[10] . ' title=\'' . $ShortCutLeiste[11] . '\' alt=\'' . $ShortCutLeiste[11] . '\'></a></td>';
    }

    $TRBeginn = '';
    if (!$MinAnzeige) {
        echo '</tr>';
        $TRBeginn = '<tr>';
    }

    $TREnde = '';
    if (isset($ShortCutLeiste[13]) AND $ShortCutLeiste[13] != '') {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[12] . ' href=' . $ShortCutLeiste[13] . " onMouseover=\"window.status='" . $ShortCutLeiste[15] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[14] . ' title=\'' . $ShortCutLeiste[15] . '\' alt=\'' . $ShortCutLeiste[15] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }
    if (isset($ShortCutLeiste[17]) AND $ShortCutLeiste[17] != '') {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[16] . ' href=' . $ShortCutLeiste[17] . " onMouseover=\"window.status='" . $ShortCutLeiste[19] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[18] . ' title=\'' . $ShortCutLeiste[19] . '\' alt=\'' . $ShortCutLeiste[19] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }
    if (isset($ShortCutLeiste[21]) AND $ShortCutLeiste[21] != '') {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[20] . ' href=' . $ShortCutLeiste[21] . " onMouseover=\"window.status='" . $ShortCutLeiste[23] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[22] . ' title=\'' . $ShortCutLeiste[23] . '\' alt=\'' . $ShortCutLeiste[23] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }

    echo $TREnde;
    echo '</table>';

    echo '</td>';

    //***********************************
    // Mittelblock mit dem Namen
    //***********************************
    echo "<td width=35%><p align=center style='margin:0 0 0 0'><b><font color=#AD1021>";

    // Alle Versionsnummern auslesen
    $Zeile = trim(fgets($fd));
    while ((substr($Zeile, 0, 1) == "#" OR substr($Zeile, 0, 1) == "") AND !feof($fd)) {
        $Zeile = trim(fgets($fd));
    }
    $VersionsInfos = explode(";", $Zeile);
    if (!$MinAnzeige) {
        echo $VInfo["PRODUKTNAME"] . "<br>";
    }
    echo "<font size=2>" . $VInfo["MODULNAME"] . ", Version " . $VersionsInfos[0] . "</font>";

    print "<img src=\"/bilder/info_klein.png\" alt=\"Version\" onclick=\"window.open('/hilfe/versionsinfo.php?Verzeichnis=$pfad','Versionsinformation','toolbar=no,menubar=no,dependent=yes,status=no');\" onMouseover=\"window.status='Versionshistorie anzeigen';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
    fclose($fd);

    print "</font>";
    if (!$MinAnzeige) {
        echo "<br><font size=1 color=#AD1021>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";

        //*********************************************************
        // Sprach-/ Landesauswahl
        //*********************************************************
        echo "<td width=5%>";
        if (isset($VInfo["SPRACHEN"])) {
            $AltLink = $_SERVER['REQUEST_URI'];
            if (strpos($AltLink, 'AWISSprache') !== false) {
                global $AWISSprache;
                $AWISSprache = substr($AltLink, strpos($AltLink, 'AWISSprache') + 12, 2);
                $AWISBenutzer->BenutzerSprache($AWISSprache);
                $AltLink = substr($AltLink, 0, strpos($AltLink, 'AWISSprache') - 1);
            }
            echo '<table border=0 width=110><tr><td width=100>';
            $Sprachen = explode(',', $VInfo["SPRACHEN"]);
            foreach ($Sprachen AS $Sprache) {
                echo '<a id=BilderLink href="' . $AltLink . (strpos($AltLink,
                        '?') === false?'?':'&') . 'AWISSprache=' . $Sprache . '"><img border=0px style="background-color:#D0D0D0;border-width:0px;padding:2px;margin:1px;width:20px;height:12px" height=12 width=20 src="/bilder/Flagge_' . strtoupper($Sprache) . '.gif"></a>';
            }
            echo "</td></tr></table>";
        }
        echo "</td>";
    }

    print "<td width=30% align=right style='margin: 0 0 0 0'>";
    if ($VInfo["STARTSEITE"] != '') {
        print "<a href='/hilfe/hilfe_Main.php?HilfeThema=allgemein&Aktion=' border=0><img border=0 src=/bilder/awis_hilfe.png alt=Hilfe></a>";
        print "<a href=" . $VInfo["STARTSEITE"] . " " . (isset($VInfo["TARGET"])?($VInfo["TARGET"] == ''?"target=_self":'target=' . $VInfo["TARGET"]):'') . "><img border=0 src=/bilder/start.png alt=Startseite></a>";
    } elseif ($VInfo["MARQUEE"] != '') {
        print "<marquee scrollamount=1 scrolldelay=50><font size=1>" . $VInfo["MARQUEE"] . "</font></marquee>";
    }
} else        // Falls keine Versionsdatei gefunden wurde -> Allg. Infos anzeigen
{
    echo "<td width=33% align=left style=margin: 0 0 0 0;>";
    echo "<img src=/bilder/atulogo_neu_gross.png>";
    echo '</td>';

    echo "<td width=33% align=center style=margin: 0 0 0 0;><b><font color=#AD1021>";
    echo "Awis, Version 2.00.00";
    echo "</font><br><font size=1 color=#AD1021>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";

    echo "<td width=34% align=right style=margin: 0 0 0 0;>";
    echo '<a href=/index.php target=_self><img border=0 src=/bilder/start.png alt=Startseite></a>';
}

if (!$MinAnzeige) {
    $Recht70 = $AWISBenutzer->HatDasRecht(70);
    if ($Recht70 & 1 == 1) {
        //require_once('kint/Kint.class.php');

        print "<br><font size=1pt >Angemeldet als: <b>";
        if (isset($_GET['Debug'])) {
            $SQL = 'UPDATE BENUTZEROPTIONEN';
            $SQL .= " SET XBO_WERT = DECODE(XBO_WERT,'1','0','0','1','0')";
            $SQL .= ' WHERE XBO_XBT_ID=1';
            $SQL .= ' AND XBO_XBN_KEY=0' . $AWISBenutzer->BenutzerID();

            //var_dump($SQL);
            $AWISDB->Ausfuehren($SQL, '', true);
        }
        $AltLink = $_SERVER['REQUEST_URI'];
        echo '<a id=DebugSwitchLink href="' . $AltLink . (strpos($AltLink, '?') === false?'?':'&') . 'Debug' . '">' . $AWISBenutzer->BenutzerName() . '</a>';
    } else {
        print "<br><font size=1>Angemeldet als <b>" . $AWISBenutzer->BenutzerName() . "</b></font>";
    }
    //if($AWISBenutzer->BenutzerName()=='entwick')
    {
        //print "<br><font size=1pt>Server: " . $_SERVER['HTTP_HOST'] . "</font>";
        $hostname = gethostbyaddr($_SERVER['SERVER_ADDR']);
        print "<br><font size=1>Server: " . $hostname . "</font>";
    }
}
setlocale(LC_TIME, "de_DE");
setlocale(LC_NUMERIC, ",");

echo '</td></tr></table><hr>';

if (isset($VInfo["WARNUNG"]) AND $VInfo["WARNUNG"] != '') {
    echo '<span class=Hinweistext>';
    echo $VInfo['WARNUNG'];
    echo '</span>';
}

$WerkZeug = new awisWerkzeuge();
if ($WerkZeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK) {
    echo '<img width="1280px" height="5px" src="/bilder/lineal_leiste.png" border=0>';
}
//PG:15.09.2017 - Die Popups auch im alten Header laden.
//Leeres PopUp f�r die Seite erzeugen
$Form->PopupDialog('', '', array(array('',
    $Form->LadeTextBaustein('TITEL','pop_zurueck'),'','')), 1, 'MasterPopUp');
?>