<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FIL_LAN_CODE;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FOZ', '%');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5001 = $AWISBenutzer->HatDasRecht(5001);
    $Recht5021 = $AWISBenutzer->HatDasRecht(5021);        // Inhalte der Seite
    $Edit = $Recht5021 & 2 == 2;
    if (($Recht5001 & 8) == 0) {
        $Form->Fehler_KeineRechte();
        die();
    }
    $AWIS_KEY2 = 0;
    if (isset($_GET['FZP_KEY'])) {
        $AWIS_KEY2 = $_GET['FZP_KEY'];
    }

    $SQL = 'select *';
    $SQL .= ' from FILIALENOEFFNUNGSZEITENPFLEGE';
    $SQL .= ' left join FILIALENOEFFNUNGSZEITENMODELLE';
    $SQL .= ' on FZP_FOZ_KEY = FOZ_KEY';
    $SQL .= ' where FZP_FIL_ID = ' . $DB->WertSetzen('FIK', 'N0', $AWIS_KEY1);
    $SQL .= ' order by FZP_GUELTIG_AB desc';

    //Falls irgendwann mal ein Bearbeiten kommen sollte..
    if ($AWIS_KEY2 > 0) {
        $SQL .= ' and FZP_KEY = ' . $DB->WertSetzen('FIK', 'N0', $AWIS_KEY2);
    }

    $rsFIK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIK'));

    if ($AWIS_KEY2 == -1) { //Neuanlage
        $Felder = array();
        $Felder[] =
            array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./filialinfos_Main.php?cmdAktion=Details&Seite=Oeffnungszeiten accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart('', 'div_foz_bezeichnung');
        //Murks um Murks zu beheben... Im Header wird jeder Zeile die Klasse EingabeZeileScrollKlasse gegeben, mitdem kann aber das Mehrfachselectfeld nicht umgehen.
        echo '<script> ';
        echo '$("document").ready(function(){ ';
        echo '  $("#div_foz_bezeichnung").removeClass("EingabeZeileScrollKlasse") ';
        echo ' } );';
        echo '</script>';

        $Form->Erstelle_TextLabel($AWISSprachKonserven['FOZ']['FOZ_BEZEICHNUNG'] . ':', 150);
        $SQL = 'select FOZ_KEY, FOZ_BEZEICHNUNG from Filialenoeffnungszeitenmodelle where FOZ_LAN_CODE = ' . $DB->WertSetzen('FOZ', 'T', $FIL_LAN_CODE);
        $Form->Erstelle_MehrfachSelectFeld('!FZP_FOZ_KEY', [], 500, $Edit, $SQL, '', '', '', '', null, '', '', $DB->Bindevariablen('FOZ'), '', 'AWIS', '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], 1);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FOZ']['FOZ_GUELTIGAB'] . ':', 150);
        $Form->Erstelle_TextFeld("!FZP_GUELTIG_AB", $rsFIK->FeldOderPOST('FZP_GUELTIG_AB'), 12, 150, $Edit, '', '', '', 'D');
        $Form->ZeileEnde();


        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');
        $Form->Trennzeile('O');


    } else { //Liste

        $Form->ZeileStart();
        $Icons = array();
        if (($Recht5021 & 2) == 2)    // Ändernrecht
        {
            $Icons[] = array('new', './filialinfos_Main.php?cmdAktion=Details&Seite=Oeffnungszeiten&FZP_KEY=-1');
        }
        $Form->Erstelle_ListeIcons($Icons, 20, -2);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FOZ']['FOZ_BEZEICHNUNG'], 350);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FOZ']['FOZ_GUELTIGAB'], 100);
        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsFIK->EOF()) {
            $HG = ($DS % 2);

            $Form->ZeileStart();
            $Icons = array();
            $Diff = $Form->DatumsDifferenz($rsFIK->FeldInhalt('FZP_GUELTIG_AB'), date('d.m.Y'));

            if (($Recht5021 & 2) == 2 and $Diff < 0)    // Ändernrecht
            {
                $Icons[] = array('delete', './filialinfos_Main.php?cmdAktion=Details&Seite=Oeffnungszeiten&Del=' . $rsFIK->FeldInhalt('FZP_KEY'));
            }
            $Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));
            $Form->Erstelle_ListenFeld('FOZ_BEZEICHNUNG', $rsFIK->FeldInhalt('FOZ_BEZEICHNUNG'), '', 350, false, $HG);

            $Form->Erstelle_ListenFeld('FZP_GUELTIG_AB', $rsFIK->FeldInhalt('FZP_GUELTIG_AB'), '', 100, false, $HG);

            $Form->ZeileEnde();

            $rsFIK->DSWeiter();
            $DS++;
        }
    }
} catch (Exception $ex) {

}
?>
