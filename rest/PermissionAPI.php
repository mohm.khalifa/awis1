<?php
/**
 * User: diepold_j
 * Date: 11.09.18
 * Time: 09:56
 */

try{
    require_once 'awisDatenbank.inc';
    require_once 'awisBenutzer.inc';
    require_once 'awisRest.inc';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Benutzer = awisBenutzer::Init('awis_jobs');

    $awisREST = new awisRest('PermissionAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();
    $bitStufe = -1;

    //Gueltig von:
    if(isset($Request['USERNAME']) AND isset($Request['XRC_ID'])) {
       $pruefUser = awisBenutzer::Init($Request['USERNAME']['WERT']);
       $bitStufe = $pruefUser->HatDasRecht($Request['XRC_ID']['WERT']);
    }

    $awisREST->sendeResponse([$bitStufe],awisRest::AntwortTyp_JSON);
}

catch (Exception $e){
   if($e->getCode() == 201502031915){
       $awisREST->sendeResponse(['User not found!'],awisRest::AntwortTyp_JSON,404);
   }
}

