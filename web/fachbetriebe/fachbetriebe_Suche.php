<?php
/**
 * Suchmaske f�r die Auswahl Fachbetriebe
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130624
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('FAB','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht27000=$AWISBenutzer->HatDasRecht(27000);		
	if ($Recht27000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    $Form->PruefeDatum('01.10.2015');
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./fachbetriebe_Main.php?cmdAktion=Details>");	

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FAB'));

	$Form->DebugAusgabe(1,$Param);
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();
	
	$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	$OptionAlle = '~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'];

	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_BEREICH'].':',150);
	$SQL = 'SELECT DISTINCT FAB_BEREICH AS KEY, FAB_BEREICH FROM fachbetriebe ORDER BY FAB_BEREICH';
	$Form->Erstelle_SelectFeld('FAB_BEREICH',($Param['SPEICHERN']=='on'?$Param['FAB_BEREICH']:''), 150, true,$SQL,$OptionAlle);
	$Form->ZeileEnde();
    
	//TODO: AJAX Feld in Abh�ngigkeit vom gew�hlten Bereich
/*
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_NAME1'].':',150);
	$SQL = 'SELECT FAB_KEY, FAB_NAME1 FROM fachbetriebe ORDER BY FAB_NAME1';
	$Form->Erstelle_SelectFeld('FAB_KEY',($Param['SPEICHERN']=='on'?$Param['FAB_KEY']:'0'),'280:260',true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->ZeileEnde();
*/
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_NAME1'].':',130);
	unset($rsFAB);
	if ($Param['SPEICHERN']=='on' AND $Param['FAB_BEREICH']!='')
	{
		$SQL = 'SELECT DISTINCT FAB_KEY AS KEY, FAB_NAME1 AS ANZEIGE FROM fachbetriebe';
		$SQL .= ' WHERE FAB_BEREICH = '.$DB->FeldInhaltFormat('T',$Param['FAB_BEREICH']);
		$rsFAB=$DB->RecordSetOeffnen($SQL);
		if ($rsFAB->EOF())
		{
			unset($rsFAB);
		}
	}
	$Form->Erstelle_SelectFeld('FAB_KEY',(isset($rsFAB)?$rsFAB->FeldInhalt('FAB_KEY'):''),'240:200',true,'***FAB_Daten;txtFAB_KEY;FAB_BEREICH=*txtFAB_BEREICH&Zusatz='.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],(isset($rsFAB)?$rsFAB->FeldInhalt('KEY').'~'.$rsFAB->FeldInhalt('ANZEIGE'):$OptionAlle));
	$Form->ZeileEnde();
	
	
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_PLZ'].':',150);
	//$SQL = 'SELECT DISTINCT FAB_PLZ AS KEY, FAB_PLZ || \' - \' || FAB_ORT FROM fachbetriebe ORDER BY FAB_PLZ';
	//$Form->Erstelle_SelectFeld('*FAB_PLZ',($Param['SPEICHERN']=='on'?$Param['FAB_PLZ']:''), '280:260', true,$SQL,'~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_TextFeld('*FAB_PLZ',($Param['SPEICHERN']=='on'?$Param['FAB_PLZ']:''),10,200,true,'','','','','','','',5);
	$Form->ZeileEnde();
	
    $Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FAB']['FAB_ORT'].':',150);
	//$SQL = 'SELECT DISTINCT FAB_ORT AS KEY, FAB_ORT FROM fachbetriebe ORDER BY FAB_ORT';
	//$Form->Erstelle_SelectFeld('*FAB_ORT',($Param['SPEICHERN']=='on'?$Param['FAB_ORT']:''), '280:260', true,$SQL,'~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_TextFeld('*FAB_ORT',($Param['SPEICHERN']=='on'?$Param['FAB_ORT']:''),50,200,true,'','','','','','','',50);
	$Form->ZeileEnde();
    
    $Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',150);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht27000&4) == 4)		
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201307151448");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201307151448");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>