<?php
require_once 'awisBerichte.inc';

class ber_ServiceLeistungenInfo
	extends awisBerichte
{
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{

		//*****************************************
		// Daten f�r den Bericht laden
		//*****************************************
		$SQL = 'SELECT *';
		$SQL .= ' FROM SVCTEILNBENACHRICHTUNGEN';
		$SQL .= ' INNER JOIN SVCTEILNEHMER ON STB_SVT_KEY = SVT_KEY';
		
		// Alle Parameter auswerten
		// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
		//			Beispiel: $Parameter['KEY_STB_KEY']="=~27"
		$Bedingung = '';
		if(isset($Parameter['STB_KEY']))
		{
			$Param = explode('~',$Parameter['STB_KEY']);
			$Bedingung .= " and STB_KEY ".self::_VergleichsOperatorPruefen($Param[0])." ".$this->_DB->FeldInhaltFormat('N0',$Param[1],false);
		}

		// Alle offenen, schriftlichen Ausdrucken
		$Bedingung = ' AND STB_STATUS = 1 AND BITAND(STB_BENACHRICHTIGUNG,4) = 4';
		
		
		if($Bedingung!='')
		{
			$SQL .= ' WHERE '.substr($Bedingung,4);
		}
		$SQL .= ' ORDER BY STB_KEY';

		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
		
		//*****************************************
		// Alle Textbausteine laden
		//*****************************************
		$TextKonserven[]=array('STB','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eiegntliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins(0,0,0);
		$this->AliasNbPages();
		
		// Barcode initalisieren
		$this->_Barcode = new awisBarcode($this);

		
		while(!$this->_rec->EOF())
		{
			// Neue Seite mit neuen Parametern
			$this->_Spalte = $this->_Parameter['LinkerRand'];
			$this->_Zeile = $this->_Parameter['Adresse-Oben'];
			$this->SetMargins($this->_Spalte , $this->_Zeile ,$this->_Parameter['RechterRand']);
			
			$this->SetAutoPageBreak(true,$this->_Parameter['UntererRand']); 
			$this->NeueSeite();
			
			//*********************************************
			// Adresse drucken
			//*********************************************
			if($this->_rec->FeldInhalt('SVT_ANREDE')!='')
			{
				$this->SetXY($this->_Spalte,$this->_Zeile);
				$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
				$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('SVT_ANREDE')),0,0,'L');
				$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			}
			
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
			$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('SVT_NAME').' '.$this->_rec->FeldInhalt('SVT_VORNAME')),0,0,'L');
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			
			if($this->_rec->FeldInhalt('SVT_ADRESSZUSATZ')!='')
			{
				$this->SetXY($this->_Spalte,$this->_Zeile);
				$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
				$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('SVT_ADRESSZUSATZ')),0,0,'L');
				$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			}
					
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
			$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('SVT_STRASSE').' '.$this->_rec->FeldInhalt('SVT_HAUSNUMMER')),0,0,'L');
			// Etwas Abstand nach der Strasse
			$this->_Zeile += $this->_Parameter['Zeilenhoehe']*1.2;
			
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
			$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',($this->_rec->FeldInhalt('SVT_LAN_CODE')!='DE'?$this->_rec->FeldInhalt('SVT_LAN_CODE').'-':'').$this->_rec->FeldInhalt('SVT_PLZ').' '.$this->_rec->FeldInhalt('SVT_ORT')),0,0,'L');
			
			
			//*********************************************
			// Zus�tze drucken
			//*********************************************
			
			$DatumsPos = explode(',',$this->_Parameter['DatumPos']);
			
			$this->SetXY($DatumsPos[0],$DatumsPos[1]);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$this->_rec->FeldInhalt('STB_DATUM')),0,0,'L');
				
			
			//*********************************************
			// TEXT drucken
			//*********************************************
			
			$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$Text = $this->_rec->FeldInhalt('STB_BRIEFTEXT');
		
			$this->SchreibeSonderText($Text,$this->_Parameter['Zeilenhoehe'],$this->_Parameter['Schriftgroesse'],$this->_Parameter['Schriftart']);
			
			
			//*********************************************
			// Kennzeichnen, dass es in Arbeit ist			
			//*********************************************
			
			$SQL = 'UPDATE SVCTEILNBENACHRICHTUNGEN';
			$SQL .= ' SET STB_STATUS = 5';		// IN Arbeit
			$SQL .= ' WHERE STB_KEY = '.$this->_rec->FeldInhalt('STB_KEY');
			$this->_DB->Ausfuehren($SQL);
			
			$this->_rec->DSWeiter();
		}		
		
		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
		
		// Jetzt alle auf abgeschlossen
		$SQL = 'UPDATE SVCTEILNBENACHRICHTUNGEN';
		$SQL .= ' SET STB_STATUS = 9';		// Fertig
		$SQL .= ' , STB_USER = \'AWIS\'';		
		$SQL .= ' , STB_USERDAT = SYSDATE';		
		$SQL .= ' WHERE STB_STATUS = 5';
		$this->_DB->Ausfuehren($SQL);
	}
	
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(($this->PageNo()==1?1:2));		// Erste oder zweite Seite importieren
			$this->useTemplate($pdf_vorlage);
		}
	}
}
?>