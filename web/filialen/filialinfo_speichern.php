<?php
global $con;
global $AWISBenutzer;

$frmSeite = '';
if(isset($_GET['Seite']))
{
	$frmSeite = $_GET['Seite'];	
}
//awis_Debug(1,$_SERVER['QUERY_STRING']);
//awis_Debug(1,$_GET);

$frmUnterSeite = '';
if(isset($_GET['Unterseite']))
{
	$frmUnterSeite = $_GET['Unterseite'];
}


foreach(array(400,401) AS $InfoFeld)
if(isset($_POST['txtFIF_FIT_ID_'.$InfoFeld]))
{
	if($_POST['txtFIF_FIT_ID_'.$InfoFeld]!=$_POST['oldFIF_FIT_ID_'.$InfoFeld])
	{
		$SQL = 'BEGIN AWIS.AENDERN_FILIALINFOS(';
		$SQL .= $_POST['FILID'].','.$InfoFeld.',\''.$_POST['txtFIF_FIT_ID_'.$InfoFeld].'\'';
		$SQL .= ',\''.$AWISBenutzer->BenutzerName().'\'); END;';
		awisExecute($con,$SQL);
	}
}


//**********************************************************************
// Mitbewerber
//**********************************************************************
if($frmSeite=='Mitbewerb')
{
	
	if(isset($_POST['txtAAE_STATUS']) AND $_POST['txtAAE_STATUS']==1)			// �nderung
	{
		$Felder = explode(';',awis_NameInArray($_POST,'txtADR_',1,1));
//awis_Debug(1,$Felder);				
		foreach($Felder AS $Feld)
		{
//echo '<br>'.$Feld;			
			if($_POST[$Feld]!='')		// Ge�ndertes Feld
			{
				$SQL = 'INSERT INTO ADRESSENAENDERUNGEN';
				$SQL .= '(AAE_ADR_KEY,AAE_STATUS,AAE_FELD,AAE_NEUERWERT,AAE_MELDER';
				$SQL .= ',AAE_USER,AAE_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ''.$_POST['txtAAE_ADR_KEY'];
				$SQL .= ','.$_POST['txtAAE_STATUS'];
				$SQL .= ',\''.substr($Feld,3).'\'';
				$SQL .= ',\''.$_POST[$Feld].'\'';
				$SQL .= ',\''.$_POST['txtAAE_MELDER'].'\'';
				$SQL .= ',\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('Filialen',1,'Fehler beim Speichern',$SQL);
				}
			}
		}
	}
}


//**********************************************************************
// Lieferkontrolle
//**********************************************************************
if($frmSeite=='Lieferkontrolle')
{
	//awis_Debug(1,$_POST);
	
	// Scanmenge �ndern
	$Felder = explode(';',awis_NameInArray($_POST,'txtLIK_MENGEIST',1,1));
	//awis_Debug(1,$Felder);
	foreach($Felder AS $Feld)
	{			
		$FeldTeile = explode('_',$Feld);
		//awis_Debug(1,$FeldTeile);
		//awis_Debug(1,$_POST[$Feld]);
		
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,16));
				$SQL = 'UPDATE Lieferkontrollen';
				//$SQL .= ' SET LIK_MENGEIST='.intval($_POST[$Feld]);			
				$SQL .= ' SET LIK_MENGEIST='.intval($_POST[$Feld]);			
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' ,LIK_STATUS = CASE WHEN LIK_MENGESOLL=('.intval($_POST[$Feld]).') THEN 11 ';						
				$SQL .= ' ELSE CASE WHEN LIK_STATUS<10 AND LIK_MENGESOLL<>('.intval($_POST[$Feld]).') THEN 2 ';			
				$SQL .= ' ELSE 12';
				$SQL .= ' END';
				$SQL .= ' END';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);		
	}
	
	if (isset($FeldTeile[3]))
	{
		//Wenn keine Differenzen mehr bestehen, LSNR automatisch abschliessen
		$SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_MENGEIST<>LIK_MENGESOLL AND LIK_LKK_KEY = '.intval($FeldTeile[3]);
		$rsLIK=awisOpenRecordset($con,$SQL);
		$rsLIKZeilen = $awisRSZeilen;
		//awis_debug(1,$rsLIK['ANZAHLDIFF'][0]);
		if ($rsLIK['ANZAHLDIFF'][0]==0)
		{
			$SQL = 'UPDATE LIEFERKONTROLLENKOPF SET LKK_STATUS = \'A\' WHERE LKK_KEY = '.intval($FeldTeile[3]);
			
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
	
	
	/*
	// Mengenkorrekturen
	$Felder = explode(';',awis_NameInArray($_POST,'txtLIK_MENGEKORREKTUR',1,1));
	foreach($Felder AS $Feld)
	{	
		if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
		{
			$Key = intval(substr($Feld,22));
			$SQL = 'UPDATE Lieferkontrollen';
			$SQL .= ' SET LIK_MENGEKORREKTUR='.intval($_POST[$Feld]);
			//$SQL .= ' ,LIK_STATUS = CASE WHEN LIK_MENGESOLL=(LIK_MENGEIST+'.intval($_POST[$Feld]).') THEN 11 ';
			$SQL .= ' ,LIK_STATUS = CASE WHEN LIK_MENGESOLL=('.intval($_POST[$Feld]).') THEN 11 ';
			$SQL .= ' ELSE 2';
			$SQL .= ' END';
			$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
			}
	
		}
	}
	*/
	
	// Grund f�r die Korrektur
	/*$Felder = explode(';',awis_NameInArray($_POST,'txtLIK_KORREKTURGRUND',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{	
		if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
		{
			$Key = intval(substr($Feld,22));
			
			$SQL = 'UPDATE Lieferkontrollen';
			$SQL .= ' SET LIK_KORREKTURGRUND='.awis_FeldInhaltFormat('T',$_POST[$Feld],true);
			$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
			
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
			}
			
		}
	}
	*/
	// Sollbestand
	$Felder = explode(';',awis_NameInArray($_POST,'txtLIK_SOLLBESTAND',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,19));
				
				$SQL = 'UPDATE Lieferkontrollen';
				$SQL .= ' SET LIK_SOLLBESTAND='.intval($_POST[$Feld]);
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
				}
			}	
		}
	}
	
	// Istbestand
	$Felder = explode(';',awis_NameInArray($_POST,'txtLIK_ISTBESTAND',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,18));
				
				$SQL = 'UPDATE Lieferkontrollen';
				$SQL .= ' SET LIK_ISTBESTAND='.intval($_POST[$Feld]);
				$SQL .= ' ,LIK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,LIK_UserDat=SYSDATE';
				$SQL .= ' WHERE LIK_KEY=0'.$Key.'';
				
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Lieferkontrolle',1,'Fehler beim Speichern',$SQL);
				}
			}
		}
	}
}

//**********************************************************************
// Raederauslagerung
//**********************************************************************
if($frmSeite=='MDE' and $frmUnterSeite=='Raederauslagerung')
{
	$Felder = explode(';',awis_NameInArray($_POST,'txtRAL_MENGE',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,13));
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_MENGE='.intval($_POST[$Feld]);			
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
	
	awis_Debug(1,$_POST);
	$Felder = awis_NameInArray($_POST, 'oldRAL_BESCHAEDIGT_VR_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_BESCHAEDIGT_VR='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				//$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldRAL_BESCHAEDIGT_VL_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_BESCHAEDIGT_VL='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				//$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldRAL_BESCHAEDIGT_HR_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_BESCHAEDIGT_HR='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				//$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldRAL_BESCHAEDIGT_HL_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_BESCHAEDIGT_HL='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				//$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = explode(';',awis_NameInArray($_POST,'txtRAL_BEMERKUNG',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE Raederauslagerungen';
				$SQL .= ' SET RAL_BEMERKUNG='.awis_FeldInhaltFormat('T',$_POST[$Feld],true);	
				$SQL .= ' ,RAL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,RAL_UserDat=SYSDATE';
				//$SQL .= ' ,RAL_STATUS = 2';
				$SQL .= ' WHERE RAL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raederauslagerung',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
}
	

//**********************************************************************
// Raedereinlagerung
//**********************************************************************
if($frmSeite=='MDE' and $frmUnterSeite=='Raedereinlagerung')
{
	$Felder = explode(';',awis_NameInArray($_POST,'txtREL_MENGE',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,13));
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_MENGE='.intval($_POST[$Feld]);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
	
	awis_Debug(1,$_POST);
	$Felder = awis_NameInArray($_POST, 'oldREL_BESCHAEDIGT_VR_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_VR='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				//$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldREL_BESCHAEDIGT_VL_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_VL='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				//$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldREL_BESCHAEDIGT_HR_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_HR='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				//$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = awis_NameInArray($_POST, 'oldREL_BESCHAEDIGT_HL_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
	if($Felder!='')
	{
		//awis_Debug(1,$Felder);
		$Felder = explode(';',$Felder);
		$FeldListe='';
		
		$SQL='';
		foreach($Felder as $Feld)
		{
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
			//awis_Debug(1,$AktWert, $FeldName);
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
						
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_HL='.awis_FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				//$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}
				
			}
		}		
	}
	
	$Felder = explode(';',awis_NameInArray($_POST,'txtREL_BEMERKUNG',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BEMERKUNG='.awis_FeldInhaltFormat('T',$_POST[$Feld],true);	
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				//$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Raedereinlagerung',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
}

//**********************************************************************
// ATU Karten
//**********************************************************************
if($frmSeite=='Karten')
{
	$Felder = explode(';',awis_NameInArray($_POST,'txtCGK_KONTAKT',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	awis_Debug(1, $_POST);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,15));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_KONTAKT='.($_POST[$Feld]=='::bitte w�hlen::'?'null':intval($_POST[$Feld]));			
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('ATU-Karten',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
	}
	
	$Felder = explode(';',awis_NameInArray($_POST,'txtCGK_BEMERKUNG',1,1));
	$SQL = '';
	awis_Debug(1, $Felder);
	awis_Debug(1, $_POST);
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_BEMERKUNG=\''.$_POST[$Feld].'\'';			
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('ATU-Karten',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
}

//**********************************************************************
// Lieferkontrolle
//**********************************************************************
if($frmSeite=='MDE' and $frmUnterSeite=='Inventur')
{
	// Scanmenge �ndern
	$Felder = explode(';',awis_NameInArray($_POST,'txtINV_MENGEIST',1,1));
	foreach($Felder AS $Feld)
	{	
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,16));
				$SQL = 'UPDATE Inventuren';
				$SQL .= ' SET INV_MENGEIST='.intval($_POST[$Feld]);			
				$SQL .= ' ,INV_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,INV_UserDat=SYSDATE';
				$SQL .= ' ,INV_STATUS = CASE WHEN INV_MENGESOLL=('.intval($_POST[$Feld]).') THEN 11 ';						
				$SQL .= ' ELSE CASE WHEN INV_STATUS<10 AND INV_MENGESOLL<>('.intval($_POST[$Feld]).') THEN 2 ';			
				$SQL .= ' ELSE 12';
				$SQL .= ' END';
				$SQL .= ' END';
				$SQL .= ' WHERE INV_KEY=0'.$Key.'';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('MDE-Inventur',1,'Fehler beim Speichern',$SQL);
				}	
			}
		}
		//awis_Debug(1,$SQL);
	}
}
?>