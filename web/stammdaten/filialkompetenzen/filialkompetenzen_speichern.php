<?php

global $Funktionen;

if ($Funktionen->AWIS_KEY1 == "-1") {
    //Erstellung

    $SQL = "INSERT INTO filialkompetenzentypen "; //Kompetenz erstellen
    $SQL .= "(";
    $SQL .= "FIK_BEZEICHNUNG";
    $SQL .= ",FIK_BESCHREIBUNG";
    $SQL .= ",FIK_USER";
    $SQL .= ",FIK_USERDAT";
    $SQL .= ")";
    $SQL .= " VALUES ";
    $SQL .= "(";
    $SQL .= $Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BEZEICHNUNG']);
    $SQL .= ",". $Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BESCHREIBUNG']);
    $SQL .= "," . $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWISBenutzer->BenutzerName());
    $SQL .= ", sysdate";
    $SQL .= ")";
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("FIK"));

    $SQL = "SELECT FIK_KEY FROM filialkompetenzentypen WHERE FIK_BEZEICHNUNG = " . $Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BEZEICHNUNG']) . " AND FIK_BESCHREIBUNG = ".$Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BESCHREIBUNG']);
    $rsFIK = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen("FIK"));

    $Funktionen->AWIS_KEY1 = $rsFIK->FeldInhalt("FIK_KEY");


} elseif ($Funktionen->AWIS_KEY1 <> "0") { //Änderungen
    //Alle Updates zur Kompetenz

    $UpdateSQL = "";

    if (isset($_POST['txtFIK_BEZEICHNUNG']) and $_POST['txtFIK_BEZEICHNUNG'] <> $_POST['oldFIK_BEZEICHNUNG']) {
        $UpdateSQL .= ", FIK_BEZEICHNUNG = " . $Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BEZEICHNUNG']);
    }
    if (isset($_POST['txtFIK_BESCHREIBUNG']) and $_POST['txtFIK_BESCHREIBUNG'] <> $_POST['oldFIK_BESCHREIBUNG']) {
        $UpdateSQL .= ", FIK_BESCHREIBUNG = " . $Funktionen->DB->WertSetzen("FIK", "T", $_POST['txtFIK_BESCHREIBUNG']);
    }
    if (strlen($UpdateSQL) > 0) {
        $UpdateSQL = substr($UpdateSQL, 1);
        $SQL = "UPDATE filialkompetenzentypen SET ";
        $SQL .= $UpdateSQL;
        $SQL .= ", FIK_USER = ". $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWISBenutzer->BenutzerName());
        $SQL .= ", FIK_USERDAT = sysdate ";
        $SQL .= " where FIK_KEY = " . $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWIS_KEY1);
        $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("FIK"));

    }
}
//Filiale zu einer Kompetenz hinzufügen ist bei Änderung/Erstellen gleich

if (isset($_POST['txtFIK_KOMPETENZFILIALEN'])) { //Filiale hinzufügen
    foreach ($_POST['txtFIK_KOMPETENZFILIALEN'] as $FIL_ID) {
        $SQL = "INSERT INTO FILIALINFOS ";
        $SQL .= "(";
        $SQL .= "FIF_FIL_ID";
        $SQL .= ",FIF_FIT_ID";
        $SQL .= ",FIF_WERT";
        $SQL .= ",FIF_READONLY";
        $SQL .= ",FIF_IMQ_ID";
        $SQL .= ",FIF_USER";
        $SQL .= ",FIF_USERDAT";
        $SQL .= ")";
        $SQL .= " VALUES ";
        $SQL .= "(";
        $SQL .= $Funktionen->DB->WertSetzen("FIK", "N0", $FIL_ID);
        $SQL .= ",920";
        $SQL .= "," . $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWIS_KEY1);
        $SQL .= ",1";
        $SQL .= ",4";
        $SQL .= "," . $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWISBenutzer->BenutzerName());
        $SQL .= ", sysdate";
        $SQL .= ")";
        $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("FIK"));
    }
}

