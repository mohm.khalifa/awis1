<html>
<head>

<title>Awis 1.0 - ATU webbasierendes Informationssystem - Gruppenverwaltung</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?
global $cmdAktion;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

$RechteStufe = awisBenutzerRecht($con, 1);

if($RechteStufe == 0)
{
    awisEreignis(3,1000,'Benutzerverwaltung',$_SERVER['PHP_AUTH_USER'],'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}


    //************************************
    // Gruppenangaben speichern
    //************************************
if(strpos($QUERY_STRING,"cmdSave."))
{
    if($txtXBB_Key=='')
    {
        $SQL = "INSERT INTO AWIS.BenutzerGruppenBez(XBB_BEZ, XBB_BEMERKUNG) VALUES('" . $txtXBB_BEZ . "', '" . $txtXBB_BEMERKUNG . "')";
        awisexecute($con, $SQL);

        $SQL = "SELECT XBB_KEY FROM AWIS.BenutzerGruppenBez WHERE XBB_BEZ='" . $txtXBB_BEZ . "'";

        $rsBenutzer = awisOpenRecordset($con, $SQL);
        $txtXBB_Key=$rsBenutzer["XBB_KEY"][0];

    }
    else
    {
        $SQL = "UPDATE BenutzerGruppenBez SET XBB_Bez='" . $txtXBB_BEZ . "', XBB_BEMERKUNG='" . $txtXBB_BEMERKUNG . "'";
        $SQL .= " WHERE XBB_KEY=0" . $txtXBB_Key;

        awisexecute($con, $SQL);
    }

    $cmdAktion="Weiter";
}

    //************************************
    //  Benutzer hinzuf�gen
    //************************************

if('0' . $txtXBG_XBN_Key_0 <> '0')
{
    $SQL = "INSERT INTO BENUTZERGRUPPEN(XBG_XBN_KEY, XBG_XBB_KEY) VALUES(" . $txtXBG_XBN_Key_0 . ", " . $txtXBB_Key . " )";

    awisexecute($con, $SQL);

    $cmdAktion = "Weiter";
}

if(strpos($QUERY_STRING,"cmdDELUSR_"))
{

    $nr = strpos($QUERY_STRING,"cmdDELUSR_");
    $nr += 10;

    $nr = substr($QUERY_STRING,$nr,strpos(substr($QUERY_STRING,$nr,5),"."));

    $SQL = "DELETE FROM BENUTZERGRUPPEN WHERE XBG_XBN_KEY=0" . $nr . " AND XBG_XBB_KEY=0" . $txtXBB_Key ."";

    awisexecute($con, $SQL);



   $cmdAktion = "Weiter";

}

    //************************************
    // Neues Recht hinzuf�gen
    //************************************
if("0" . $txtXRC_Key_0 <> "0")
{
    $SQL = "INSERT INTO BENUTZERACLS(XBA_XRC_ID, XBA_TYP, XBA_XXX_KEY, XBA_Stufe) VALUES(" . $txtXRC_Key_0 . ", 'G', " . $txtXBB_Key . ", " . $txtStufe . " )";

    awisexecute($con, $SQL);

    $cmdAktion = "Weiter";
}

    //************************************
    // Recht wegnehmen
    //************************************
    
if(strpos($QUERY_STRING,"cmdDELXRC"))
{
    $nr = strpos($QUERY_STRING,"cmdDELXRC_");
    $nr += 10;

    $nr = substr($QUERY_STRING,$nr,strpos(substr($QUERY_STRING,$nr,5),"."));

    $SQL = "DELETE FROM BENUTZERACLS WHERE XBA_XXX_KEY=0" . $txtXBB_Key . " AND XBA_XRC_ID=0" . $nr ."";

    awisexecute($con, $SQL);

    $cmdAktion = "Weiter";

}
    
    
if(strpos($QUERY_STRING,"cmdLoeschen"))
{
    $SQL = "DELETE FROM BenutzerGruppenBez WHERE XBB_Key=0" . $txtXBB_Key;
    
    awisexecute($con, $SQL);
    
    $cmdAktion = "";        // Zur Auswahl zur�ck
}


    //************************************
    // Erster Aufruf
    //************************************

if(isset($cmdAktion_x))		// Durch Umstellung auf Bild-Submit notwendig -> Wenn Zeit ist, alles umbauen
{
	$cmdAktion="Weiter";
}

if($cmdAktion=="")
{
    print "<form name=frmGruppen action=./gruppen.php>";

    print "<table boder=1 cellspacing=5 bgcolor=#C0C0C0 style=border-color=#000000><tr><td><table border=0 bgcolor=#C0C0C0><tr><td>Gruppe:</td>";
    print "<td><select name=txtXBB_Key size=1 tabindex=1>";

	if(($RechteStufe&128) == 128)
	{
	    print "<option value=0 selected>Neue Gruppe...</option>";
	}


    $rsGruppen = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez ORDER BY XBB_BEZ");
    $rsGruppenZeilen = $awisRSZeilen;

    for($i = 0; $i<$rsGruppenZeilen; $i++)
    {
        print "<option value=" . $rsGruppen["XBB_KEY"][$i] . ">" . $rsGruppen["XBB_BEZ"][$i] . "</option>";
    }

    print "</select></td></tr>";

    print "<tr><td colspan=2><hr></td></tr>";
	print "<tr><td><input tabindex=2 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdAktion value=Weiter></td></tr>";
    //print "<tr><td colsapn=2><input type=submit name=cmdAktion Value=Weiter src=/bilder/weiter.png></form></td></tr>";
	print "</form></td></tr>";
    print "</table></td></tr></table>";

    print "<br><img src=/bilder/zurueck.png name=cmdZurueck value=Zur�ck onclick=self.location.href='./benutzerverwaltung.php'>";

}
    //***************************************
    // Benutzer �ndern oder neu anlegen
    //***************************************
elseif($cmdAktion=="Weiter")
{
    if($txtXBB_Key==0)           // Neuer Benutzer
    {
        print "<h2>Gruppe anlegen</h2>";
        print "<form name=frmGruppen action=./gruppen.php>";
        print "<table border=0 id=Maske>";

        print "<tr><td>Gruppen-ID:</td><td><input style=background-color:#DCDCDC; size=8 type=text readonly name=txtXBB_Key value=" . $rsGruppen[XBB_KEY][0] . "></td><td></td></tr>";

        print "<tr><td>Gruppen-Bez:</td><td><input type=text name=txtXBB_BEZ value=" . chr(34) . $rsGruppen[XBB_BEZ][0] . chr(34) ."></td><td></td></tr>";
        print "<tr><td>Bemerkung:</td><td><input type=text name=txtXBB_BEMERKUNG value=" . chr(34) . $rsGruppen[XBB_BEMERKUNG][0] . chr(34) ."></td><td></td></tr>";

            // Schaltfl�chen f�r die Hauptmaske

        print "</td><td colspan=4><input type=image alt=Gruppe_anlegen src=/bilder/diskette.png name=cmdSave onclick=document.frmBenutzer.submit();>";
   		print "<input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./gruppen.php'>";
        print "</table></form>";
    }
    else                        // Gruppe �ndern
    {
        $rsGruppen = awisOpenRecordset($con, "SELECT * FROM AWIS.BenutzerGruppenBez WHERE XBB_KEY=" . $txtXBB_Key);
        $rsGruppenZeilen = $awisRSZeilen;
        if($rsGruppenZeilen>0)
        {
            print "<h2>Gruppenverwaltung</h2>";
            print "<form name=frmGruppen action=./gruppen.php>";
            print "<table border=0 id=Maske>";

            print "<tr><td>Gruppen-ID:</td><td><input style=background-color:#DCDCDC; size=8 type=text readonly name=txtXBB_Key value=" . $rsGruppen[XBB_KEY][0] . "></td><td></td></tr>";
            print "<tr><td>Gruppen-Bez:</td><td><input " . (($RechteStufe&2)==2?'':'readonly') . " type=text name=txtXBB_BEZ value=" . chr(34) . $rsGruppen[XBB_BEZ][0] . chr(34) ."></td><td></td></tr>";
            print "<tr><td>Bemerkung:</td><td><input " . (($RechteStufe&2)==2?'':'readonly') . " type=text name=txtXBB_BEMERKUNG value=" . chr(34) . $rsGruppen[XBB_BEMERKUNG][0] . chr(34) ."></td><td></td></tr>";

                // Schaltfl�chen f�r die Hauptmaske

			if(($RechteStufe&256) == 256)
			{
	            print "</td><td colspan=4><input type=image alt=Gruppe_speichern src=/bilder/diskette.png name=cmdSave onclick=document.frmGruppen.submit();>";
			}
			if(($RechteStufe&512) == 512)
			{
    	        print "<input type=image alt=Gruppe_l�schen src=/bilder/muelleimer.png name=cmdLoeschen onclick=document.frmGruppen.submit();>";
			}
            print "</td></tr></table>";
            print "</form>";


		    print "<input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./gruppen.php'>";
            
                //************************************
                // Benutzer anzeigen
                //************************************
			print "<table border=0><tr><td valign=top>";		// Zweispaltige Darstellung

            print "<form name=frmMitglieder action=./gruppen.php>";
            $rsBenutzer = awisOpenRecordset($con, "select * from AWIS.benutzer where XBN_Key not in (select XBG_XBN_Key from AWIS.BenutzerGruppen WHERE XBG_XBB_Key=0" . $txtXBB_Key . ") ORDER BY XBN_NAME");
            $rsBenutzerZeilen = $awisRSZeilen;

            print "<input type=hidden name=txtXBB_Key value=" . $txtXBB_Key . "><br>";
            print "<table border=0><tr><td colspan=5><hr></td></tr>";
            print "<tr><td height=35 colspan=4 id=FeldBez></b>Gruppenmitgliedschaften</td></tr>";

			if(($RechteStufe&32) == 32)
			{
				echo '<tr>';
				echo '<td><input type=image alt=Benutzer_hinzuf�gen src=/bilder/diskette.png name=cmdSAVUSR_' . $rsBenutzer[XBN_KEY][$i] . ' onclick=document.frmMitglieder.submit();></td>';
				echo '<td><select size=1 name=txtXBG_XBN_Key_0>';
            	echo '<option selected value=0>Benutzer hinzuf�gen...</option>';
            	for($i=0; $i<$rsBenutzerZeilen;$i++)
            	{
                	print "<option value=" . $rsBenutzer[XBN_KEY][$i] . ">". $rsBenutzer[XBN_NAME][$i] . "</option>";
            	}
	            echo '</select></td>';
			}

			echo '<td align=center class=FeldBez	>PWD &Auml;nderung</td>';
			echo '<td align=center class=FeldBez	>bisherige Gruppen</td>';

					// Passwortpr�fung
			$SQL = "select b.XBN_KEY, b.XBN_NAME, b.XBN_VOLLERNAME, b1.XBP_WERT PWAend, NVL(b2.XBP_WERT, 'NULL') PWDatum, b.XBN_UserDat ";
			$SQL .= "from AWIS.benutzer b, (select * from benutzerparameter where XBP_XPP_ID = 1) b1, (select * from benutzerparameter where XBP_XPP_ID = 2) b2 ";
			$SQL .= "where XBN_Key in (select XBG_XBN_Key from AWIS.BenutzerGruppen WHERE XBG_XBB_Key=0" . $txtXBB_Key . ") and b.XBN_KEY = b1.XBP_XBN_KEY(+) ";
			$SQL .= "and b.XBN_KEY = b2.XBP_XBN_KEY(+) ORDER BY XBN_NAME ";
	
			$rsBenutzer = awisOpenRecordset($con, $SQL);
			$rsBenutzerZeilen = $awisRSZeilen;
	
	        for($i=0; $i<$rsBenutzerZeilen;$i++)
	   	    {
				echo '<tr>'	;
				if(($RechteStufe&64) == 64)
				{
		           		print "<td><input type=image alt=Benutzer_entfernen src=/bilder/muelleimer.png name=cmdDELUSR_" . $rsBenutzer[XBN_KEY][$i] . " onclick=document.frmGruppen.submit();></td>";
				}
	
	   	        print "<td style=\"border-bottom-width:2px;border-bottom-style:solid;\"><input type=hidden name=txtBBKey_" . $rsBenutzer[XBN_KEY][$i] . ">";
				echo '<a href=./benutzer.php?txtXBN_Key=' . $rsBenutzer[XBN_KEY][$i] . '&cmdAktion=Weiter title="' . $rsBenutzer[XBN_VOLLERNAME][$i] . '">' . $rsBenutzer[XBN_NAME][$i] .  '</a></td>';
	
	
				if ($rsBenutzer[PWAEND][$i] == -1)
				{
					print "<td align=center>Sofort</td>";
				}
				else
				{
					if ($rsBenutzer[PWDATUM][$i] <> 'NULL')
					{
						if(awis_BenutzerParameter($con,"PasswortaendernDatum",$rsBenutzer[XBN_NAME][$i]) <= time())
						{
							print "<td style=\"border-bottom-width:2px;border-bottom-style:solid;\" align=center><font color=#FF0000>" . date('d.m.Y',$rsBenutzer['PWDATUM'][$i]) . "</font></td>";
						}
						else
						{
							print "<td style=\"border-bottom-width:2px;border-bottom-style:solid;\" align=center>" . date('d.m.Y',$rsBenutzer['PWDATUM'][$i]) . "</td>";
						}
					}
					else
					{
						print "<td style=\"border-bottom-width:2px;border-bottom-style:solid;\" align=center><font color=#FF0000>Nie (seit " . $rsBenutzer['XBN_USERDAT'][$i] . ")</font></td>";
					}
				}


				// Andere Gruppen
				
			$SQL = "SELECT XBB_BEZ, XBB_KEY FROM BENUTZERGRUPPEN INNER JOIN BenutzerGruppenBez ON XBG_XBB_KEY = XBB_KEY WHERE XBG_XBN_KEY=0" . $rsBenutzer['XBN_KEY'][$i];
			$rsGrp = awisOpenRecordset($con, $SQL);
			$rsGrpZeilen = $awisRSZeilen;
			$Gruppen='';
			for($GrpZeile=0;$GrpZeile<$rsGrpZeilen;$GrpZeile++)
			{	
				$Gruppen .= ', ' . '<a href=./gruppen.php?txtXBB_Key='.$rsGrp['XBB_KEY'][$GrpZeile] . '&cmdAktion=Weiter>' . $rsGrp['XBB_BEZ'][$GrpZeile]. '</a>';
			}

			echo "<td style=\"border-bottom-width:2px;border-bottom-style:solid;\">" . substr($Gruppen, 2) . '</td>';


			echo "</tr>";
	       }

            print "</table></form>";

			print "</td><td valign=top>";	// Zweite Spalte mit Rechten

                //**************************************
                // Rechte anzeigen
                //**************************************

            print "<form name=frmRechte action=./gruppen.php>";

            $rsRechte = awisOpenRecordset($con, "select * from AWIS.Rechte where XRC_ID not in (select XBA_XRC_ID from AWIS.BenutzerACLS WHERE XBA_XXX_Key=0" . $txtXBB_Key . ") ORDER BY XRC_RECHT");
            $rsRechteZeilen = $awisRSZeilen;

            echo "<input type=hidden name=txtXBB_Key value=" . $txtXBB_Key . "><br>";
            echo "<table border=0><tr><td colspan=5><hr></td></tr>";
            echo "<tr><td colspan=1 height=35 id=FeldBez></b>Gruppenrechte</td>";
			echo "<td align=center id=FeldBez><img alt='Info (Alt+i)' src=/bilder/info.png name=cmdInfo accesskey=i onclick=window.open('/admin/rechte_Main.php?cmdAktion=Rechtestufen','Info','toolbar=no,menubar=no,dependent=yes,status=no');></td>";
			echo "<td align=center id=FeldBez><img alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rechte','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');></td></tr>";

			if(($RechteStufe&1024) == 1024)
			{
	
	            print "<tr><td width=300><select size=1 name=txtXRC_Key_0>";
	            print "<option selected value=0>Recht hinzuf�gen...</option>";
	            for($i=0; $i<$rsRechteZeilen;$i++)
	            {
	                print "<option value=" . $rsRechte[XRC_ID][$i] . ">". $rsRechte[XRC_RECHT][$i] . ' (' . $rsRechte[XRC_ID][$i] . ")</option>";
	            }
	
	            print "</select>";
	
	            print "</td><td><input name=txtStufe value=1 size=4></td><td><input type=image alt=Recht_hinzuf�gen src=/bilder/diskette.png name=cmdNEURecht onclick=document.frmRechte.submit();></td></tr>";
			}
			
            $rsRechte = awisOpenRecordset($con, "select DISTINCT * from AWIS.Rechte, AWIS.BenutzerACLS where XBA_XRC_ID = XRC_ID AND XBA_XXX_Key=0" . $txtXBB_Key . " ORDER BY XRC_RECHT");
            $rsRechteZeilen = $awisRSZeilen;

            for($i=0; $i<$rsRechteZeilen;$i++)
            {
                print "<tr><td style=\"border-bottom-width:2px;border-bottom-style:solid;\"><input type=hidden name=txtRCID_" . $rsRechte[XRC_ID][$i] . ">" . $rsRechte["XRC_RECHT"][$i] . " (" . $rsRechte[XRC_ID][$i] . ")" . "</td>";
				print "<td style=\"border-left-width:2px;border-left-style:solid;border-bottom-width:2px;border-bottom-style:solid;\">" . $rsRechte[XBA_STUFE][$i] .  "</td>";
				if(($RechteStufe&1024) == 1024)
				{
					print "<td><input type=image alt=Recht_entziehen src=/bilder/muelleimer.png name=cmdDELXRC_" . $rsRechte[XRC_ID][$i] . " onclick=document.frmRechte.submit();><td></tr>";
	            }
			}
            print "</table></form>";
        }
    }
	print "</td><tr></table>";		// Ende zweispaltige Darstellung
}

awislogoff($con);
?>
</body>
</html>
