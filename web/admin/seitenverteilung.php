<?php
/*
 * Seitenverteilung
 * 
 * @author Sacha Kerres
 * @version 20080212
 * 
 */
include ("ATU_Header.php");	// Kopfzeile
global $AWISBenutzer;
$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

if(($RechteStufe&8)!=8)
{
    awisEreignis(3,1000,'Seitenverteilung',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

echo "Es werden die Webseiten vom DB-Server zu den Webservern abgeglichen <b>und</b> umgekehrt!<br><br>";
    //************************************
    // LoginName l�schen
    //************************************
//var_dump($HTTP_POST_VARS);
if(isset($_POST['cmdVerteilen_x']))
{
	$Erg = system("/bin/rm /daten/web/logs/unison.log");
//	echo 'Verteilung wurde beauftragt.';
}

if(file_exists('/daten/web/logs/unison.log'))
{
	echo 'Der letzte Abgleich wurde durchgef�hrt. Kontrollieren Sie Ihr Ergebnis.';
	echo '<br>Verwenden Sie die Schaltfl�che um eine neue Verteilung zu starten!';
	print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./index.php'>";

	echo '<hr><form name=frmVerteilen method=post action=./seitenverteilung.php>';
	echo "<input type=image alt='Neue Verteilung' src=/bilder/verteilen.png name=cmdVerteilen onclick=location.href='./seitenverteilung.php';>";

	echo '<table border=0 width=100%>';
	echo '<tr><td><b>Zeile</b></td><td><b>Protokoll</b></td></tr>';
	
	$LogDatei = file('/daten/web/logs/unison.log');
	foreach($LogDatei as $ZeileNr => $ZeilenInhalt)
	{
		echo '<tr>';
		echo '<td width=50 align=right>' . $ZeileNr . ':</td>';
		
		If(strstr($ZeilenInhalt,'CONFLICT') OR strstr($ZeilenInhalt,'<-?->'))
		{
			echo '<td ><font color=#FF0000><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,"local") AND strstr($ZeilenInhalt,"ap-"))
		{
			echo '<td ><font size=4 color=#0000FF><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,'new file --') OR strstr($ZeilenInhalt,'changed  --'))
		{
			echo '<td ><font size=3 color=#00CC00><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,'deleted ')!='')
		{
			echo '<td ><font size=3 color=#FFFF00><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		else
		{
			echo '<td >' . $ZeilenInhalt . '</td>';
		}
		echo '</tr>';	
	}
	echo '</table></form>';
}
elseif(file_exists('/daten/web/logs/unison.tmp'))
{
	echo 'Der Abgleich l�uft gerade. Versuchen Sie es sp�ter wieder.';
	echo '<br>Verwenden Sie die Schaltfl�che um zu aktualisieren!';
	echo "<br><input type=image alt='Aktualisieren' src=/bilder/verteilen.png name=cmdVerteilen onclick=location.href='./seitenverteilung.php'>";
}
else
{
	echo 'Der Abgleich wurde um ' . date("H:i:s") . ' Uhr in Auftrag gegeben. Wird in den n�chsten Minuten ausgef�hrt.';
	echo '<br>Verwenden Sie die Schaltfl�che um zu aktualisieren!';
	echo "<br><input type=image alt='Aktualisieren' src=/bilder/verteilen.png name=cmdVerteilen onclick=location.href='./seitenverteilung.php'>";
}

print "<hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./index.php'>";

awislogoff($con);
?>