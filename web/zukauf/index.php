<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular


$Version='3';

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($Version) .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Zukauf');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new awisFormular();
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Zukauf'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader".$Version.".inc");	// Kopfzeile

try
{
    echo '<div class="RegisterInhalt" id="reg_Inhalt" style="background-color:white;margin-top:20px;">';
	$Form = new awisFormular();

	$Menue = new awisMenue(3);
	$Menue->ErstelleMenueSeite($Version);
	
    echo '<div class="HMMenueContainerRight">';
	if($Version>=3)
	{
    	$Form->awisInfotafel(3,'Statistik '.date('Y'),3);
    	$Form->awisInfotafel(2,'Zukaufinformation',3);
	}
	echo '</div>';
	$Menue->ErstelleZurueckLink();
    
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

echo '</div>';
?>
</body>
</html>