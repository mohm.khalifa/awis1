<?php
require_once('awis_HeidlerImport.inc');

try {
    $Benutzer = '';
    $DebugLevel = 50;
    $StartTag = '';
    $EndTag = '';
    $Mount = '';
    $Paketdaten = '';

    $DB = awisDatenbank::NeueVerbindung('AWIS');

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--versandart':
                $Versandart = $argv[++$i];
                break;
            case '--paketdaten':
                $Paketdaten = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-100).' . PHP_EOL;
                echo '  --versandart <Art>                 einzelne Versandart fuer Nachholung (DPD/TNT, ohne Zusatz f�r beide)' . PHP_EOL;
                echo '  --paketdaten <Art>                 Importiert neue Paketstatus f�r NOX oder DPD' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $HeidlerImport = new awis_HeidlerImport($Benutzer);
    $HeidlerImport->DebugLevel($DebugLevel);
    if (isset($Versandart)) {
        switch ($Versandart) {
            case 'DPD':
                $HeidlerImport->ImportDPD();
                break;
            case 'NOX':
                $HeidlerImport->ImportTNT();
                break;
            default:
                $HeidlerImport->ImportTNT();
                $HeidlerImport->ImportDPD();
                break;
        }
    }

    if (isset($Paketdaten)) {
        switch ($Paketdaten){
            case 'NOX':
                $HeidlerImport->ImportPaketdatenNOX();
                break;
            case 'DPD':
                $HeidlerImport->ImportPaketdatenDPD();
                break;
            default:
                break;
        }
    }

} catch (Exception $ex) {
    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}
