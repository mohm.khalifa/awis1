UPDATE "AWIS"."TEXTKONSERVEN"
SET
XTX_TEXT_DE = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).' ,
XTX_TEXT_CZ = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).',
XTX_TEXT_NL = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).',
XTX_TEXT_IT = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).',
XTX_TEXT_FR = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).',
XTX_TEXT_CH = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).',
XTX_TEXT_AT = 'Bitte geben Sie eine g�ltige Kostenstellennummer zwischen 4 und 8 Zahlen ein (g�ltige Kostenstellen in der Kostenstellendatei einsehbar).'
WHERE XTX_KENNUNG = 'OFO_TTT_FORMAT_KOSTENSTELLE'
commit;
