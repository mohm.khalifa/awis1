<?php
/**
 * Speichern Stammdatenpflege GDV-Versandweg
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */

require_once 'awisMailer.inc';
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;


$TextKonserven=array();
$TextKonserven[]=array('GDV','MSG_NEUANLAGE');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if ($_POST['txtKey'] == '-1') //Neuanlage
	{
		$SQL =' insert into versversandweg ';
	    $SQL .=' (VVW_VERSNR,VVW_RELEASE,VVW_ZUSATZWEG,VVW_USER,VVW_USERDAT)';
	    $SQL .=' values';
	    $SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtVVW_VERSICHERUNG']);
		$SQL .=','.$DB->FeldInhaltFormat('N0',($_POST['txtVVW_RELEASE'] == 0?'':$_POST['txtVVW_RELEASE']));
		$SQL .=','.$DB->FeldInhaltFormat('T',($_POST['txtVVW_ZUSATZWEG']));
		$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
	    $SQL .=',sysdate ';
	    $SQL .= ' )';

	    $DB->Ausfuehren($SQL,'',true);
	    $Form->Hinweistext($AWISSprachKonserven['GDV']['MSG_NEUANLAGE']);
	}
}
catch (awisException $ex)
{
        $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
        $Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>
