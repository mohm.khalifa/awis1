<?php
global $con;
global $AWISBenutzer;
global $awisRSZeilen;
global $AWISSprache;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $CursorFeld;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','NeueKontaktart');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CAP','CAP_%');
$TextKonserven[]=array('CAK','CAK_%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('KOT','KOT_TYP');
$TextKonserven[]=array('CAP','lst_CAP_ANREDE');
$TextKonserven[]=array('CAP','lst_CAP_RANKING');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3700 = awisBenutzerRecht($con,3700,$AWISBenutzer->BenutzerName());
if($Recht3700==0)
{
    awisEreignis(3,1000,'CRM-AP',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$SQL = 'SELECT *';
$SQL .= ' FROM CRMAnsprechpartner';
$SQL .= ' WHERE CAP_CAD_KEY=0'.$AWIS_KEY1;

if(isset($_GET['CAPKEY']))
{
	$SQL .= ' AND CAP_KEY=0'.intval($_GET['CAPKEY']);
}
if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
{
	$SQL .= ' AND CAP_KEY=0'.intval($AWIS_KEY2);
}
if(!isset($_GET['CAPSort']))
{
	$SQL .= ' ORDER BY CAP_Nachname, CAP_Vorname';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CAPSort']);
}

$rsCAP = awisOpenRecordset($con,$SQL);
$rsCAPZeilen = $awisRSZeilen;


if($rsCAPZeilen > 1 or isset($_GET['APListe']))					// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();

	if(($Recht3700&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&CAPKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
	$Link .= '&CAPSort=CAP_NACHNAME'.((isset($_GET['CAPSort']) AND ($_GET['CAPSort']=='CAP_NACHNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAP']['CAP_NACHNAME'],200,'',$Link);

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
	$Link .= '&CAPSort=CAP_FUNKTION'.((isset($_GET['CAPSort']) AND ($_GET['CAPSort']=='CAP_FUNKTION'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAP']['CAP_FUNKTION'],180,'',$Link);

	$KOTKEYS = awis_BenutzerParameter($con,'Standardkontakttypen',$AWISBenutzer->BenutzerName());
	$SucheKotKey='';
	$KOTKEYListe=explode(',',$KOTKEYS);
	foreach($KOTKEYListe AS $KOTKEYEintrag)
	{
		$KOTKEY = explode('-',$KOTKEYEintrag);		// 0 = Feld, 1= Breite des Feldes
		$SucheKotKey .= ','.$KOTKEY[0];
	}
	$SQL = 'SELECT KOT_TYP, KOT_KEY FROM Kommunikationstypen WHERE KOT_KEY IN('.substr($SucheKotKey,1).')';
	$rsKOT = awisOpenRecordset($con,$SQL);
	$KOTKEYListe = explode(',',$KOTKEYS);
	foreach($KOTKEYListe AS $KOTKEYEintrag)
	{
		$KOTKEY = explode('-',$KOTKEYEintrag);		// 0 = Feld, 1= Breite des Feldes
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			if($rsKOT['KOT_KEY'][$i]==$KOTKEY[0])
			{
				awis_FORM_Erstelle_Liste_Ueberschrift($rsKOT['KOT_TYP'][$i],$KOTKEY[1],'','');
				break;
			}
		}
	}

	awis_FORM_ZeileEnde();

	for($CAPZeile=0;$CAPZeile<$rsCAPZeilen;$CAPZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3700&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&CAPKEY='.$rsCAP['CAP_KEY'][$CAPZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Del='.$rsCAP['CAP_KEY'][$CAPZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CAPZeile%2));

		awis_FORM_Erstelle_ListenFeld('CAP_NACHNAME',$rsCAP['CAP_NACHNAME'][$CAPZeile].' '.$rsCAP['CAP_VORNAME'][$CAPZeile],20,200,false,($CAPZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('CAP_FUNKTION',$rsCAP['CAP_FUNKTION'][$CAPZeile],20,180,false,($CAPZeile%2),'','','T');
		$SQL = 'SELECT KOT_KEY, CAK_WERT, CAK_BEMERKUNG FROM Kommunikationstypen ';
		$SQL .= ' LEFT OUTER JOIN CRMAnsprechpartnerKontakte ON KOT_KEY = CAK_KOT_KEY';
		$SQL .= ' WHERE KOT_KEY IN('.substr($SucheKotKey,1).')';
		$SQL .= ' AND CAK_CAP_KEY=0'.$rsCAP['CAP_KEY'][$CAPZeile];
		$rsKOT = awisOpenRecordset($con,$SQL);
		foreach($KOTKEYListe AS $KOTKEYEintrag)
		{
			$KOTKEY = explode('-',$KOTKEYEintrag);		// 0 = Feld, 1= Breite des Feldes
			for($i=0;$i<$awisRSZeilen;$i++)
			{
				if($rsKOT['KOT_KEY'][$i]==$KOTKEY[0])
				{
					$Link = '';
					if(strpos($rsKOT['CAK_WERT'][$i],'@')!==false)
					{
						$Link = 'mailto:'.$rsKOT['CAK_WERT'][$i];
					}
					awis_FORM_Erstelle_ListenFeld('*KOT'.$rsKOT['KOT_KEY'][$i],$rsKOT['CAK_WERT'][$i],20,$KOTKEY[1],false,($CAPZeile%2),'',$Link);
					break;
				}
			}
			if($i>=$awisRSZeilen)
			{
				awis_FORM_Erstelle_ListenFeld('*LEER'.$i,'--',20,$KOTKEY[1],false,($CAPZeile%2));
			}
		}
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCAP_KEY type=hidden value=0'.(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_KEY'][0]:'').'>';
	$AWIS_KEY2 = (isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_KEY'][0]:'');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&APListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_ANREDE'].':',150);
	$Liste=explode('|',$AWISSprachKonserven['CAP']['lst_CAP_ANREDE']);
	awis_FORM_Erstelle_SelectFeld('CAP_ANREDE',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_ANREDE'][0]:''),200,($Recht3700&6),$con,'','','',1,'',$Liste);
	if(($Recht3700&6)>0)
	{
		$CursorFeld='txtCAP_ANREDE';
	}
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_NACHNAME'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_NACHNAME',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_NACHNAME'][0]:''),20,200,($Recht3700&6));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_VORNAME'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_VORNAME',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_VORNAME'][0]:''),20,200,($Recht3700&6));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_FUNKTION'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_FUNKTION',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_FUNKTION'][0]:''),20,200,($Recht3700&6));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_RANKING'].':',150);
	$StatusText = explode("|",$AWISSprachKonserven['CAP']['lst_CAP_RANKING']);
	$OptionBitteWaehlen = '0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	awis_FORM_Erstelle_SelectFeld('CAP_RANKING',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_RANKING'][0]:''),200,($Recht3700&6),$con,'',$OptionBitteWaehlen,'','','',$StatusText,'');
//	awis_FORM_Erstelle_TextFeld('CAP_RANKING',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_RANKING'][0]:'1'),5,200,($Recht3700&6));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_AKTIONEN'].':',150);
	$Liste = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('CAP_AKTIONEN',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_AKTIONEN'][0]:''),100,($Recht3700&6),$con,'','','0','','',$Liste);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_GEBURTSTAG'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_GEBURTSTAG',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_GEBURTSTAG'][0]:''),10,200,($Recht3700&6),'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_MITARBEITERSEIT'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_MITARBEITERSEIT',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_MITARBEITERSEIT'][0]:''),10,200,($Recht3700&6),'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_MITARBEITERBIS'].':',150);
	awis_FORM_Erstelle_TextFeld('CAP_MITARBEITERBIS',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_MITARBEITERBIS'][0]:''),10,200,($Recht3700&6),'','','','D');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAP']['CAP_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('CAP_BEMERKUNG',(isset($rsCAP['CAP_KEY'][0])?$rsCAP['CAP_BEMERKUNG'][0]:''),300,150,2,($Recht3700&6));
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	if(isset($rsCAP['CAP_KEY'][0]))		// Nicht bei neuen Datensätzen
	{
		$Unterseite = (isset($_GET['Unterseite'])?$_GET['Unterseite']:'Kontaktdaten');
		awis_RegisterErstellen(3702,$con,$Unterseite);
	}

}
?>