<?php
/**
 * Details Stammdatenpflege GDV-Versandweg
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$script = "<script type='text/javascript'>
	function blendeEinZusatzfeld(obj) {
		var checkWert = obj.options[obj.selectedIndex].value;

		if(checkWert == 'Mailweg' || checkWert == '')
		{
			document.getElementById('ZUSATZWEG').style.display = 'none';
			document.getElementsByName('txtVVW_ZUSATZWEG')[0].value = ''  // Ablehnungsgrund leeren                                                                         0
		}
		else // wenn Storno Stornogrund mit �berschrift einbleden
		{
			document.getElementById('ZUSATZWEG').style.display = 'block';
			document.getElementsByName('txtVVW_ZUSATZWEG')[0].value = ''  // Ablehnungsgrund leeren
		}
	}
	</script>";

    echo $script;
	
	
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GDV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht44001 = $AWISBenutzer->HatDasRecht(44001);
	$EditRecht = ($Recht44001&2) == 2;
	$HinzuRecht = ($Recht44001&4) == 4;
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_VA'));

    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'VVE_BEZEICHNUNG';
 	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	if($Recht44001==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(isset($_GET['VVW_KEY']))
	{
		$AWIS_KEY1 = $_GET['VVW_KEY'];
	}
	else 
	{
	    $AWIS_KEY1='';
	}
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	if(isset($_POST['cmdSuche_x']))
	{	
		$Param['VVW_VERSICHERUNG'] = 	$Form->Format('N0',$_POST['sucVVW_VERSICHERUNG'],true);
		$Param['VVW_RELEASE']= $Form->Format('N0',$_POST['sucVVW_RELEASE'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	//Speichernbutton
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./versandweg_speichern.php');
	}
	//X neben Datensatz oder Löschbutten auf Datensatz
	if((isset($_GET['Del']) or isset($_POST['cmdLoeschen_x'])) or (isset($_POST['cmdLoeschenOK'])))
	{
		include('./versandweg_loeschen.php');
	}
	/* Daten beschaffen */
    $SQL  ='SELECT';
    $SQL .='  VVW_VERSNR,VVW_KEY,VVE_BEZEICHNUNG,VVW_RELEASE,VVW_USER,VVW_USERDAT,VVW_ZUSATZWEG, ';
    $SQL .='   row_number() OVER (';
    $SQL .= ' order by ' . $Param['ORDER'] .' )  AS ZeilenNr';
    $SQL .=' FROM';
    $SQL .='   (';
    $SQL .='     SELECT';
	$SQL .='       VVW_KEY,';
	$SQL .='       VVW_VERSNR,';
	$SQL .='       VVE_BEZEICHNUNG,';
    $SQL .='       VVW_RELEASE,';
	$SQL .='       VVW_ZUSATZWEG,';
	$SQL .='       VVW_USER,';
    $SQL .='       VVW_USERDAT';
    $SQL .='     FROM';
    $SQL .='       versversandweg';
	$SQL .='    inner join versversicherungen on versversandweg.VVW_VERSNR = VVE_VERSNR';
	$SQL .='   )daten';

	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '')
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}
	if($AWIS_KEY1 != '')
	{
		if(!isset($_POST['cmdSuche_x'])){
			$SQL .= ' WHERE VVW_KEY='.$AWIS_KEY1;
		}
		else{
			$SQL .= ' AND VVW_KEY='.$AWIS_KEY1;
		}
	}

	$MaxDS = 1;
	$ZeilenProSeite=1;
    $Block = 1;
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	
	//AWIS_KEY1 ist leer, wenn man über die Suche kommt. Wenn gespeichert wird(AWIS_KEY1 ist dann nicht leer), kommt man wieder in die Liste, deswegen auch die Blockparameter hernehmen.
	if($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x'])))
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsVVW = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten für Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BES_AUFTRAGSART'] = 500;
	$FeldBreiten['BES_VERALTET'] = 20;
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 4);
	}
	$Gesamtbreite += 20;

	$Form->SchreibeHTMLCode('<form name="frmVVWDetails" action="./versandweg_Main.php?cmdAktion=Details'.' " method=POST  enctype="multipart/form-data">');
	$Form->Formular_Start();
	
	if (($rsVVW->AnzahlDatensaetze() > 1))
	{
		$FeldBreiten = array();
		$FeldBreiten['VERSICHERUNG'] = 350;
		$FeldBreiten['RELEASE'] = 200;
		$Param['BLOCK']=1;

		$count = 0;
		$Gesamtbreite = '';
		foreach ($FeldBreiten as $value)
		{
			$Gesamtbreite += $value;
			$count++;
		}

		$Gesamtbreite += 38;

		$Form->ZeileStart();
		$Icons = array();
		$Icons[] = array('new','./versandweg_Main.php?cmdAktion=Details&VVW_KEY=-1','g');
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link= '';

		$Link = './versandweg_Main.php?cmdAktion=Details&Sort=VVW_VERSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VVW_VERSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'], $FeldBreiten['VERSICHERUNG'], '', $Link);
		$Link = './versandweg_Main.php?cmdAktion=Details&Sort=VVW_RELEASE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VVW_RELEASE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VVW_RELEASE'], $FeldBreiten['RELEASE'], '', $Link);
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsVVW->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			$Icons[] = array('delete','./versandweg_Main.php?cmdAktion=Details&Del='.$rsVVW->FeldInhalt('VVW_KEY'));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$TTT = $rsVVW->FeldInhalt('VVE_BEZEICHNUNG');
			$Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVVW->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VERSICHERUNG'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT = $rsVVW->FeldInhalt('VVW_RELEASE');
			$Form->Erstelle_ListenFeld('VVW_RELEASE', $rsVVW->FeldInhalt('VVW_RELEASE') == ''? 'Mailweg':($rsVVW->FeldInhalt('VVW_ZUSATZWEG') != '' ?$rsVVW->FeldInhalt('VVW_RELEASE').'/'.'Mailweg' :$rsVVW->FeldInhalt('VVW_RELEASE')), 0, $FeldBreiten['RELEASE'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsVVW->DSWeiter();
		}

		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['GDV']['SummeGesamtDS'].': '.$Form->Format('N0',$rsVVW->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './versandweg_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
	}
	elseif($rsVVW->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1')
	{
		$edit = true;
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./versandweg_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVW->FeldInhalt('VVW_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVW->FeldInhalt('VVW_USERDAT'));
		$Form->InfoZeile($Felder,'');

		if($rsVVW->AnzahlDatensaetze() == 1 and $AWIS_KEY1 != '-1') {
			$SQL  ='SELECT ';
			$SQL .='VVE_VERSNR,VVE_BEZEICHNUNG ';
			$SQL .=' FROM ';
			$SQL .='versversandweg ';
			$SQL .=' inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';
			$SQL .=' order by VVE_BEZEICHNUNG ';
			$edit = false;
		}
		else
		{
			$SQL  ='SELECT ';
			$SQL .='VVE_VERSNR,VVE_BEZEICHNUNG ';
			$SQL .=' FROM ';
			$SQL .='versversicherungen ';
			$SQL .=' where vve_versnr not in (select vvw_versnr from versversandweg)';
			$SQL .=' order by VVE_BEZEICHNUNG ';
			$edit = true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
		$Form->Erstelle_SelectFeld('!VVW_VERSICHERUNG',$rsVVW->FeldInhalt('VVW_VERSNR'),150,$edit,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
		$Form->ZeileEnde();
		/**
		 *  Release
		 */

		/* Daten beschaffen */
		$SQL  ='SELECT ';
		$SQL .='GDR_RELEASE as KEY,GDR_RELEASE ';
		$SQL .=' FROM ';
		$SQL .='gdvrelease order by GDR_RELEASE';

		if($rsVVW->FeldInhalt('VVW_RELEASE') == '') {
			if($AWIS_KEY1 != '-1'){
				$Wert = 0;
			}
			else{
				$Wert = '';
			}
		}
		else{
			$Wert = $rsVVW->FeldInhalt('VVW_RELEASE');
		}
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_RELEASE'] . ':',200);
		$Form->Erstelle_SelectFeld('!VVW_RELEASE',$Wert,150,$edit,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','onchange="blendeEinZusatzfeld(this);"','',array(),'');
		$Form->ZeileEnde();
		$Form->Erstelle_HiddenFeld('Key',$AWIS_KEY1 == '-1'?$AWIS_KEY1:$rsVVW->FeldInhalt('VVW_KEY'));

		$Form->ZeileStart('display:none', 'GS"ID="ZUSATZWEG');
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_ZUSATZWEG'] . ':',200);
		$Form->Erstelle_TextFeld('VVW_ZUSATZWEG','', 30, 250,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileEnde();

	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
    $Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zurück zum Menü
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/gdvService/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if($AWIS_KEY1 == '-1')//Speichern wenn und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
	{
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if ($rsVVW->AnzahlDatensaetze() == 1 and $AWIS_KEY1 != '-1')
		{
			$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
		}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_GDV_VA',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['VVW_VERSICHERUNG']) AND ($Param['VVW_VERSICHERUNG']!='0' and $Param['VVW_VERSICHERUNG']!=''))
	{
		$Bedingung .= ' AND VVW_VERSNR ' . $DB->LikeOderIst($Param['VVW_VERSICHERUNG']);
	}


	if(isset($Param['VVW_RELEASE']) AND $Param['VVW_RELEASE']!='')
	{
	    if($Param['VVW_RELEASE'] == '0'){
			$Bedingung .= ' AND VVW_RELEASE is null';
		}
		else{
			$Bedingung .= ' AND VVW_RELEASE ' . $DB->LikeOderIst($Param['VVW_RELEASE']) . ' ';
		}

	}
	return $Bedingung;
}

?>