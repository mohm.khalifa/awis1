<?php
require_once('awisFilialEbenen.inc');

global $con;
global $AWISSprache;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('FRZ','%');
$TextKonserven[]=array('FER','%');
$TextKonserven[]=array('Wort','Filialebene');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3802 = awisBenutzerRecht($con,3802,$AWISBenutzer->BenutzerName());
if($Recht3802==0)
{
    awisEreignis(3,1000,'Filialebenenrollen',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


$EditRecht=(($Recht3802&6)!=0);

//***********************************
//Kontakte anzeigen
//***********************************

$SQL = 'SELECT FILIALEBENENROLLENZUORDNUNGEN.*, FER_BEZEICHNUNG, KON_NAME1 || COALESCE(\', \' || KON_NAME2,\'\') AS KON_NAME';
$SQL .= ' FROM FILIALEBENENROLLENZUORDNUNGEN';
$SQL .= ' INNER JOIN Filialebenenrollen ON FRZ_FER_KEY = FER_KEY';
//$SQL .= ' INNER JOIN Filialebenenrollenbereiche on frb_key = fbr_'
$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe on fbr_frb_key = fer_frb_key and fbr_xbn_key = '.$AWISBenutzer->BenutzerID();
$SQL .= ' INNER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY';
$SQL .= ' WHERE FRZ_XXX_KEY=0'.$AWIS_KEY1;
$SQL .= ' AND FRZ_XTN_KUERZEL=\'FEB\'';

if(isset($_GET['FRZ_KEY']))
{
	$SQL .= ' AND FRZ_KEY = '.floatval($_GET['FRZ_KEY']);
}

if(!isset($_GET['FRZSort']))
{
	$SQL .= ' ORDER BY FER_BEZEICHNUNG DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['FRZSort']);
}
$rsFRZ = awisOpenRecordset($con,$SQL);
$rsFRZZeilen = $awisRSZeilen;
awis_Debug(1,$SQL);

if(!isset($_GET['FRZ_KEY']) OR isset($_GET['FRZListe']))					// Liste anzeigen
{
    awis_FORM_FormularStart();
    awis_FORM_ZeileStart();

	if(($Recht3802&6)>0)
	{
		$Icons[] = array('new','./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen';
	$Link .= '&FRZSort=FER_BEZEICHNUNG'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FER_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'],300,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen';
	$Link .= '&FRZSort=KON_NAME'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='KON_NAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],200,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen';
	$Link .= '&FRZSort=FRZ_GUELTIGAB'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FRZ_GUELTIGAB'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'],100,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen';
	$Link .= '&FRZSort=FRZ_GUELTIGBIS'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FRZ_GUELTIGBIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'],100,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Filialebene'],300,'',$Link);

	awis_FORM_ZeileEnde();

	for($FRZZeile=0;$FRZZeile<$rsFRZZeilen;$FRZZeile++)
	{
		if($rsFRZ['FER_BEZEICHNUNG'][$FRZZeile]=='')
		{
			continue;
		}
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3802&2)>0)	// �ndernrecht
		{
			$Icons[] = array('edit','./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY='.$rsFRZ['FRZ_KEY'][$FRZZeile]);
		}
		if(($Recht3802&4)>0)	// L�schen
		{
			$Icons[] = array('delete','./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&Del='.$rsFRZ['FRZ_KEY'][$FRZZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($FRZZeile%2));

		$Style='';

		if(awis_PruefeDatum($rsFRZ['FRZ_GUELTIGAB'][$FRZZeile],1,1) > strtotime(date('d.m.Y'),time()))
		{
			$Style='font-style:italic;color:#FF2020';
		}
		if(awis_PruefeDatum($rsFRZ['FRZ_GUELTIGBIS'][$FRZZeile],1,1) < strtotime(date('d.m.Y'),time()))
		{
			$Style='font-style:italic;color:#FF2020';
		}
		awis_FORM_Erstelle_ListenFeld('#FER_BEZEICHNUNG',$rsFRZ['FER_BEZEICHNUNG'][$FRZZeile],20,300,false,($FRZZeile%2),$Style,'','T');
		awis_FORM_Erstelle_ListenFeld('#KON_NAME',$rsFRZ['KON_NAME'][$FRZZeile],20,200,false,($FRZZeile%2),$Style,'','T');
		awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGAB',$rsFRZ['FRZ_GUELTIGAB'][$FRZZeile],20,100,false,($FRZZeile%2),$Style,'','D');
		awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGBIS',$rsFRZ['FRZ_GUELTIGBIS'][$FRZZeile],20,100,false,($FRZZeile%2),$Style,'','D');
		awis_FORM_ZeileEnde();
	}


	//*******************************************************************
	// Vererbte Rollen
	//*******************************************************************

	try
	{
		$FilialEbene = new awisFilialEbenen($AWIS_KEY1);

		$Rollen = $FilialEbene->ZeigeVererbteRollen();
		foreach($Rollen AS $Rolle)
		{
			if($Rolle['FER_BEZEICHNUNG']=='')
			{
				continue;
			}
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListeIcons(array(),38,($FRZZeile%2));
			awis_FORM_Erstelle_ListenFeld('#FER_BEZEICHNUNG',$Rolle['FER_BEZEICHNUNG'],20,300,false,($FRZZeile%2),'font-style:italic;','','T');
			awis_FORM_Erstelle_ListenFeld('#KON_NAME',$Rolle['KON_NAME'],20,200,false,($FRZZeile%2),'font-style:italic;','','T');
			awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGAB',$Rolle['FRZ_GUELTIGAB'],20,100,false,($FRZZeile%2),'font-style:italic;','','D');
			awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGBIS',$Rolle['FRZ_GUELTIGBIS'],20,100,false,($FRZZeile%2),'font-style:italic;','','D');
			$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FEB_KEY=0'.$Rolle['FEB_KEY'];
			awis_FORM_Erstelle_ListenFeld('#FEB_BEZEICHNUNG',$Rolle['FEB_BEZEICHNUNG'],20,300,false,($FRZZeile%2),'font-style:italic;',$Link,'T');

			awis_FORM_ZeileEnde();
			$FRZZeile++;
		}
	}
	catch (Exception $ex)
	{
		if($ex->getCode()!==awisFilialEbenen::ERR_KEIN_FEB_KEY)
		{
			awis_FORM_Hinweistext('Fehler:'.$ex->getMessage(),1);
		}
	}

	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtFRZ_KEY type=hidden value=0'.(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_KEY'][0]:'').'>';
	echo '<input name=txtFRZ_XXX_KEY type=hidden value=0'.$AWIS_KEY1.'>';
	echo '<input name=txtFRZ_XTN_KUERZEL type=hidden value="FEB">';

	$AWIS_KEY2 = (isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_FER_KEY'].':',150);
	$SQL = "select FER_KEY, FER_BEZEICHNUNG ";
	$SQL .= ' FROM FilialebenenRollen ';
	$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
	$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';
	$SQL .= ' ORDER BY FER_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('FRZ_FER_KEY',($AWIS_KEY2==0?'':$rsFRZ['FRZ_FER_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_KON_KEY'].':',150);
	$SQL = 'select KON_KEY, KON_NAME1 || COALESCE(\', \'||KON_NAME2,\'\') AS KON_NAME ';
	$SQL .= ' FROM Kontakte ';
	$SQL .= ' WHERE (KON_STATUS=\'A\' OR KON_KEY = '.($AWIS_KEY2==0?'0':$rsFRZ['FRZ_KON_KEY'][0]).')';
	$SQL .= ' AND KON_PER_NR IS NOT NULL';
	$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
	awis_FORM_Erstelle_SelectFeld('FRZ_KON_KEY',($AWIS_KEY2==0?'':$rsFRZ['FRZ_KON_KEY'][0]),300,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('FRZ_BEMERKUNG',(isset($rsFRZ['FRZ_BEMERKUNG'][0])?$rsFRZ['FRZ_BEMERKUNG'][0]:''),600,120,5,($Recht3802&6),'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'].':',150);
	awis_FORM_Erstelle_TextFeld('FRZ_GUELTIGAB',(isset($rsFRZ['FRZ_GUELTIGAB'][0])?$rsFRZ['FRZ_GUELTIGAB'][0]:''),10,200,($Recht3802&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'].':',150);
	awis_FORM_Erstelle_TextFeld('FRZ_GUELTIGBIS',(isset($rsFRZ['FRZ_GUELTIGBIS'][0])?$rsFRZ['FRZ_GUELTIGBIS'][0]:''),10,200,($Recht3802&6),'','','','D','','',awis_PruefeDatum('31.12.2030'));
	awis_FORM_ZeileEnde();
	

	awis_FORM_FormularEnde();
	
	$Recht3806 = awisBenutzerRecht($con,3806,$AWISBenutzer->BenutzerName());
	
	if(isset($rsFRZ['FRZ_KEY'][0]) and ($Recht3806&1)>0)
	{
    	$cmdAktion='Vertreter';
    	
    	awis_RegisterErstellen(3804, $con, $cmdAktion);
	}	
}
?>