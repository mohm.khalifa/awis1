<?php
/**
 * Suchmaske f�r die Preisauskunft
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20081113
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZPA','%');
	$TextKonserven[]=array('ZLH','ZLH_BEZEICHNUNG');
	$TextKonserven[]=array('ZAP','ZAP_EK');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('LIE','LIE_NR');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','Summe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','txt_OhneZuordnung');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_WarenkorbBestellen');
	$TextKonserven[]=array('Fehler','err_KeinWert');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Infofeld','ity_lbl_4');
	$TextKonserven[]=array('Infofeld','ity_lbl_31%');
	$TextKonserven[]=array('ZLB','ttt_Hinweis%');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10010 = $AWISBenutzer->HatDasRecht(10010);	// Rechte f�r die Preisanfrage
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);	// Recht f�r den Artikelstamm			
	$Recht10012 = $AWISBenutzer->HatDasRecht(10012);	// Recht f�r Hersteller					
	if($Recht10010==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$AWISWerkzeug = new awisWerkzeuge();
	if($AWISWerkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV or $AWISWerkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING){
		$ATU_SOAP_SERVER = $AWISWerkzeug->LeseAWISParameter('zukauf.conf','ATU_SOAP_SERVER');
	}else{
		$ATU_SOAP_SERVER = 'localhost';
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');


	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./preisauskunft_Main.php?cmdAktion=Suche>");

	if(isset($_POST['cmdBestellen_x']) or isset($_POST['cmdBestellenOK_x']))
	{
		include './preisauskunft_bestellen.php';
	}

	if(isset($_POST['cmdLoeschen_x']))
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']=$AWISBenutzer->BenutzerID();
		
		$SQL = 'DELETE FROM ZukaufPreisauskunft';
		$SQL .= ' WHERE ZPA_XBN_KEY = :var_N0_xbn_key';
		$DB->Ausfuehren($SQL,'',false,$BindeVariablen);
	}

	// Parameter speichern
	$Param['FIL_ID'] = 995;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZPA'));
	if(isset($_POST['sucFIL_ID']))
	{
		$Param['FIL_ID']=$_POST['sucFIL_ID'];
		$Param['LIE_NR']=(isset($_POST['sucLIE_NR'])?$_POST['sucLIE_NR']:'');
		$Param['ZPA_MENGE']=$_POST['sucZPA_MENGE'];
		$Felder = $Form->NameInArray($_POST,'txtZUSTELLUNG_',awisFormular::NAMEINARRAY_LISTE_GESAMT,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
		if($Felder!='')
		{
			$Zustellungen = explode(';',$Felder);
			foreach($Zustellungen AS $Zustellung)
			{
				$Param[$Zustellung]=$_POST[$Zustellung];
			}
		}

		$AWISBenutzer->ParameterSchreiben('Formular_ZPA',serialize($Param));
	}

	/**********************************************
	* Eingabemaske
	***********************************************/
	$Form->Formular_Start();

    // TODO: Allg. Hinweistext auslesen und anzeigen
	$WerkZeug = new awisWerkzeuge();
	if(($Recht10010&128)==128 AND isset($_POST['txtHINWEISTEXT']) AND $_POST['txtHINWEISTEXT']!=$_POST['oldHINWEISTEXT'])
	{
		$HinweisText = $WerkZeug->Hilfswert('ZUB', 'ZENTR-MELDUNG',awisWerkzeuge::HILFSWERT_TYP_TEXT,$_POST['txtHINWEISTEXT']);
	}
	else 
	{
		$HinweisText = $WerkZeug->Hilfswert('ZUB', 'ZENTR-MELDUNG',awisWerkzeuge::HILFSWERT_TYP_TEXT);
	}
	
	if($HinweisText!='' AND ($Recht10010&128)==0)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($HinweisText,($BildschirmBreite<1024?748:956),'Hinweis');
		$Form->ZeileEnde();
	}	
	elseif(($Recht10010&128)==128)
	{
		if(!isset($_POST['cmdBestellen_x']) and !isset($_POST['cmdBestellenOK_x']))
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Hinweistext:',130);
			$Form->Erstelle_Textarea('HINWEISTEXT',$HinweisText,800,100,4,true);
			$Form->ZeileEnde();
			$Form->Trennzeile();
		}
	}
	
    $BindeVariablen=array();
    $BindeVariablen['var_N0_fil_id']=$Param['FIL_ID'];

    //pr�fen wegen Datumsformatierung
    $SQL = 'SELECT lie_nr, lie_name1, FLI_SORTIERUNG';
    $SQL .= ', COALESCE((SELECT LIN_WERT FROM LIEFERANTENINFOS WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY=19),\'\') AS hinweis';
    $SQL .= ' FROM FilialenLieferanten ';
    $SQL .= ' INNER JOIN Lieferanten ON FLI_LIE_NR = LIE_NR';
    $SQL .= ' WHERE FLI_FIL_ID = :var_N0_fil_id';
    $SQL .= ' AND FLI_GUELTIGAB <= SYSDATE AND FLI_GUELTIGBIS >= SYSDATE';
    $SQL .= ' AND COALESCE((SELECT TO_NUMBER(LIN_WERT) FROM LIEFERANTENINFOS WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY=7),0) = 1';
    $rsHinweis = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    
    if(!$rsHinweis->EOF())
    {
        while(!$rsHinweis->EOF())
        {
            if($rsHinweis->FeldInhalt('HINWEIS')!='')
            {
                $HinweisText = '<b>'.$rsHinweis->FeldInhalt('LIE_NAME1').'</b>: '.$rsHinweis->FeldInhalt('HINWEIS');
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($HinweisText,($BildschirmBreite<1024?748:956),'Hinweis');
                $Form->ZeileEnde();

                $Form->Trennzeile();
            }
            $rsHinweis->DSweiter();
        }
    }
    
    
    
	$WarenKorbFelder = $Form->NameInArray($_POST,'txtAuswahl_',1);
	if($WarenKorbFelder!='')
	{
		$WarenKorbFelder = explode(';',$WarenKorbFelder);

		foreach($WarenKorbFelder AS $WarenKorbFeld)
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_zpa_key']=substr($WarenKorbFeld,11);
			
			$SQL = 'UPDATE ZUKAUFPREISAUSKUNFT';
			$SQL .= ' SET ZPA_WARENKORB = 1';
			$SQL .= ' WHERE ZPA_KEY = :var_N0_zpa_key';
			if($DB->Ausfuehren($SQL,'',false,$BindeVariablen)===false)
			{
				$Form->Hinweistext('FEHLER beim Speichern',1);
			}
			
			// Hinweis, wenn ein Artikel mit Bestand bestellt wird
			$SQL = 'SELECT ZPA_BESTANDEIGEN, ZPA_BESTELLNUMMER, ZLA_AST_ATUNR';
			$SQL .= ' FROM ZUKAUFPREISAUSKUNFT';
			$SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZPA_ZLA_KEY = ZLA_KEY ';
			$SQL .= ' WHERE ZPA_KEY = '.$DB->WertSetzen('ZPA', 'N0', substr($WarenKorbFeld,11));
			$rsZPA = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZPA'));
			if($rsZPA->FeldInhalt('ZPA_BESTANDEIGEN')>0)
			{
				$Form->ZeileStart();
				$Hinweis = str_replace('$1', $rsZPA->FeldInhalt('ZPA_BESTANDEIGEN'),$AWISSprachKonserven['ZPA']['txtHinweisBestandEigen']);
				$Hinweis = str_replace('$2', $rsZPA->FeldInhalt('ZLA_AST_ATUNR'),$Hinweis);
				$Form->Hinweistext($Hinweis);
				$Form->ZeileEnde();
			}
		}

		$_POST['sucZLA_ARTIKELNUMMER']='';
		$_POST['sucZPA_MENGE']='1';
		$Param['ZPA_MENGE']=1;
		$_POST['sucZLA_ZLH_KEY']='';
		$_POST['sucLIE_NR']='';
	}

	// Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('*FIL_ID',$Param['FIL_ID'],150,true,$SQL,'~'.utf8_encode($AWISSprachKonserven['Wort']['txt_BitteWaehlen']));
	}
	else
	{
		$Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,180,true,'','','','T','L','','',10);
	}
	$AWISCursorPosition=($AWISCursorPosition==''?'sucFIL_ID':$AWISCursorPosition);

	// Anfrage nur auf einen Lieferanten beschr�nken
	if(($Recht10010&64)==64)
	{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NR'].':',100);
		$Form->Erstelle_TextFeld('*LIE_NR',$Param['LIE_NR'],10,180,true,'','','','T','L','','',10);
	}
	$Form->ZeileEnde();


	// Artikelnummer
	$Form->ZeileStart();
	$Suchfelder = explode("|",$AWISSprachKonserven['ZPA']['Suchtypen']);
	$Form->Erstelle_SelectFeld('*SUCHTYP',(isset($_POST['sucSUCHTYP'])?$_POST['sucSUCHTYP']:''),240,true,null,'','4','','',$Suchfelder);


	$Form->Erstelle_TextFeld('*ZLA_ARTIKELNUMMER',(isset($_POST['sucZLA_ARTIKELNUMMER'])?$_POST['sucZLA_ARTIKELNUMMER']:''),30,150,true,'','','','T','L','','',30);
	$Form->ZeileEnde();

	// Angefragte Menge
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['ZPA_MENGE'].':',240);
	$Form->Erstelle_TextFeld('!*ZPA_MENGE',(isset($Param['ZPA_MENGE'])?$Param['ZPA_MENGE']:1),5,150,true,'','','','T','L','',1,3);
	$Form->ZeileEnde();

	// Hersteller
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':',240);
	$SQL = 'SELECT ZLH_KEY, CONVERT(ZLH_BEZEICHNUNG,\'WE8MSWIN1252\',\'UTF8\') AS ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*ZLA_ZLH_KEY',(isset($_POST['sucZLA_ZLH_KEY'])?$_POST['sucZLA_ZLH_KEY']:''),200,true,$SQL,'~'.utf8_encode($AWISSprachKonserven['Wort']['txt_BitteWaehlen']));
	$Form->ZeileEnde();

	$Form->Trennzeile();

	// Wurde die Suchauswahl best�tigt?
	if(isset($_POST['sucZLA_ARTIKELNUMMER']) AND $_POST['sucZLA_ARTIKELNUMMER']!=''
		AND !(isset($_POST['cmdBestellen_x']) or isset($_POST['cmdBestellenOK_x'])))
	{
		// Warenkorb f�llen?

		// Neue Anfrage starten
		$BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']=$AWISBenutzer->BenutzerID();
		
		$SQL = ' DELETE FROM ZUKAUFPREISAUSKUNFT';
		$SQL .= ' WHERE ZPA_XBN_KEY = :var_N0_xbn_key';
		$SQL .= ' AND ZPA_WARENKORB = 0';
		$DB->Ausfuehren($SQL,'',false,$BindeVariablen);

		$Fehler = '';
		$Pflichtfelder = array('FIL_ID','ZLA_ARTIKELNUMMER');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['suc'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $AWISSprachKonserven['Fehler']['err_KeinWert'].' '.$AWISSprachKonserven[substr($Pflichtfeld,0,3)][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			$Form->Hinweistext($Fehler);
		}
		else
		{
			// Suchparameter zusammen
			$Artikel['zlh_key']=$Form->Format('N0',$_POST['sucZLA_ZLH_KEY'],false);
			$Artikel['menge']=$Form->Format('N0',$_POST['sucZPA_MENGE'],false);
			$AW = new awisWerkzeuge();
				
			switch ($_POST['sucSUCHTYP'])
			{
				case 1:		// Tec-Doc Nummer
					$Artikel['artikelnummer']=$AW->ArtNrKomprimiert(strtoupper($_POST['sucZLA_ARTIKELNUMMER']));
					break;
				case 2:		// OE-Nummer
					$Artikel['oenummer']=$AW->ArtNrKomprimiert(strtoupper($_POST['sucZLA_ARTIKELNUMMER']));
					break;
				case 3:		// EAN-Nummer
					$Artikel['eannummer']=strtoupper($_POST['sucZLA_ARTIKELNUMMER']);
					break;
				case 4:		// ATU Nummer
					$Artikel['atunr']=strtoupper($_POST['sucZLA_ARTIKELNUMMER']);
					break;
			}

			$AnmeldeDaten['user']=$AWISBenutzer->BenutzerName();
			$AnmeldeDaten['userpwd']='dfh358df34jkdf';
			$PEIKey = ($Form->Format('N0',$_POST['sucFIL_ID'])==''?90:$Form->Format('N0',$_POST['sucFIL_ID']));
			$AnmeldeDaten['filiale']=$PEIKey;

			// TODO: Eingabefeld bauen
			$AnmeldeDaten['lienr']=$Param['LIE_NR'];

			$Params = array('trace'=>1
					, 'soap_version'=>SOAP_1_2
					, 'exceptions'=> 1		// Keine Exceptions ausl�sen, wenn der Server nicht reagiert
                    , 'connection_timeout' => 90
					, 'location'=>'http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php'
					);
            try{
			    $SC = new SoapClient('http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php?wsdl',$Params);

			    // Anfrage starten
                $Erg = ($SC->Zukauf_PreisAnfrage($AnmeldeDaten, $Artikel));
            } catch(Exception $e){
                $Form->Fehler_Anzeigen('SoapServer',(isset($SC->__last_response)?$SC->__last_response:''),'SpaeterWiederholen',-4,11122107);
                $Form->DebugAusgabe(1,$SC,$Erg);
                $Mailtext = 'Fehler bei der Preisabfrage:'.(isset($SC->__last_response)?$SC->__getLastResponse():'')."\nAnfrage lautet:".serialize($Artikel) .'<br><br>';
                $Mailtext .= 'Meldung: '.$e->getMessage().'<br>';
                $Mailtext .= 'Datei: '.$e->getFile().'('.$e->getLine().')<br>';
                $Mailtext .= 'DebugDetails: <br>';
                $Mailtext .= var_export($SC,true);
                $Mailtext .= var_export($Erg,true);
                $Mailtext .= '<br>';
                $Mailtext .= 'Stacktrace: ';
                $Mailtext .= $e->getTraceAsString();

                $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'),'SOAP-Client Fehler AWIS (Client) - Zukauf - Filiale '.$Param['FIL_ID'],$Mailtext,3,'shuttle@de.atu.eu','awis@de.awis.eu');
                die;
            }

            $XMLDaten = simplexml_load_string($Erg);
            if($XMLDaten===false)			// genereller Fehler bei der Anfrage
            {
                $Form->ZeileStart();
                $Form->Hinweistext('FEHLER beim Zugriff auf den AWIS-SOAP-Dienst.',0,100);
                $Form->ZeileEnde();
                $Form->DebugAusgabe(1,$Erg,$Artikel);
            }
            elseif(isset($XMLDaten->Fehler->Nummer))			// genereller Fehler bei der Anfrage
            {
                $Form->ZeileStart();
                $Form->Hinweistext(substr($XMLDaten->Fehler->Meldung,0,100),1);
                $Form->ZeileEnde();
            }
            else
            {
                //*************************************************************
                //* BEST-Preis Ermitllung �ber alle Lieferanten und im Lieferanten
                //* 15.05.2013
                //*************************************************************
                $BestPreis = array();
                // Schritt 1: alles sammeln
                foreach($XMLDaten->Lieferanten->Lieferant AS $xmlLieferant)
                {
                    if(!isset($xmlLieferant->Artikelliste->ArtikelTreffer))
                    {
                        continue;
                    }
                    foreach($xmlLieferant->Artikelliste->ArtikelTreffer as $xmlArtikel)
                    {
                        // Nur die anschauen, die eine ATU Nummer mit Preisbindung haben
                        if((string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR != '')
                        {
                            $ATU_VK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Zukaufartikel->ATU_VK,false);
                            $KundenVK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Finanzen->VK,false);
                            $EK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Finanzen->EK,false);

                            if($ATU_VK > 0 AND $ATU_VK >= $KundenVK)
                            {
                                $Differenz = $KundenVK-$EK;
                                $BestPreis[(string)$xmlLieferant->LIE_NR][(string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR][(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]['Wertung']=$Differenz;
                                $BestPreis[(string)$xmlLieferant->LIE_NR][(string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR][(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]['VK']=$KundenVK;
                                $BestPreis[(string)$xmlLieferant->LIE_NR][(string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR][(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]['EK']=$EK;
                            }
                        }
                    }
                }
                // Schritt 2: Bewerten
                $BestPreisATUNR = array();
                $BestPreisLIE = array();

                // Zun�chst den besten Preis pro ATU Nummer suchen (wird gr�n markiert)
                foreach($BestPreis AS $LIE_NR=>$LIEDaten)
                {
                    foreach($LIEDaten AS $ATUNR=>$ATUDaten)
                    {
                        foreach($ATUDaten AS $ZLA=>$ZLADaten)
                        {
                            $Wertung = $ZLADaten['Wertung'];
                            if(!isset($BestPreisATUNR[$ATUNR]) OR $BestPreisATUNR[$ATUNR]['Wertung']<$Wertung)
                            {
                                $BestPreisATUNR[$ATUNR]['ZLA']=$ZLA;
                                $BestPreisATUNR[$ATUNR]['Wertung']=$Wertung;
                                $BestPreisATUNR[$ATUNR]['LIE']=$LIE_NR;
                            }
                        }
                    }
                }

                // Jetzt den zweitbesten Preis pro Lieferant und ATU Nummer suchen (wird orange markiert)
                foreach($BestPreis AS $LIE_NR=>$LIEDaten)
                {
                    foreach($LIEDaten AS $ATUNR=>$ATUDaten)
                    {
                        foreach($ATUDaten AS $ZLA=>$ZLADaten)
                        {
                            $Wertung = $ZLADaten['Wertung'];
                            if((!isset($BestPreisLIE[$LIE_NR][$ATUNR]) OR $BestPreisLIE[$LIE_NR][$ATUNR]['Wertung']<$Wertung)
                                AND ($BestPreisATUNR[$ATUNR]['ZLA']!=$ZLA OR $BestPreisATUNR[$ATUNR]['LIE']!=$LIE_NR))
                            {
                                $BestPreisLIE[$LIE_NR][$ATUNR]['ZLA']=$ZLA;
                                $BestPreisLIE[$LIE_NR][$ATUNR]['Wertung']=$Wertung;
                            }
                        }
                    }
                }


                // Schritt 3: Farbcodes zuordnen
                $FarbCodesGruen = array();
                    // Gr�n: Bester Preis �ber alle
                foreach($BestPreisATUNR AS $ATUNR=>$Daten)
                {
                    $FarbCodesGruen[$Daten['ZLA']]=$Daten['LIE'];
                }
                $FarbCodesOrange = array();
                    // Orange: Bester Preis im Lieferant
                foreach($BestPreisLIE AS $LIE_NR=>$LIEDaten)
                {
                    foreach($LIEDaten AS $ATUNR=>$ATUDaten)
                    {
                        if(!isset($FarbCodesGruen[$ATUDaten['ZLA']]) OR $FarbCodesGruen[$ATUDaten['ZLA']]!=$LIE_NR)				// Nur, wenn nicht Bestpreis vorliegt
                        {
                            $FarbCodesOrange[$LIE_NR][$ATUDaten['ZLA']]=1;
                        }
                    }
                }


                //*************************************************************
                //* Daten anzeigen
                //*************************************************************
                $LetzteLIENR = '';
                foreach($XMLDaten->Lieferanten->Lieferant AS $xmlLieferant)
                {
                    // Neue Darstellung der Verf�gbarkeit ausw�hlen
                    $TROST_SAP = false;
                    $IconsAnzeigenVerfuegbarkeit = false;

                    $SQL = 'SELECT LKD_ZUSATZ1';
                    $SQL .= ' FROM LIEFERANTENKUNDENNUMMERN ';
                    $SQL .= ' WHERE LKD_LIE_NR = '.$DB->WertSetzen('LKD', 'T', (string)$xmlLieferant->LIE_NR);
                    $SQL .= ' AND LKD_FIL_ID = '.$DB->WertSetzen('LKD', 'T', $PEIKey);
                    $rsLKD = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('LKD'));
                    if(strtoupper($rsLKD->FeldInhalt('LKD_ZUSATZ1'))=='SAP')
                    {
                        $TROST_SAP=true;
                        $IconsAnzeigenVerfuegbarkeit=true;
                    }


                    $LagerBestandPruefen = false;					// Legt fest, ob der Lagerbestand in Weiden/Werl gepr�ft werden soll
                    $SonderBestellungPruefen = false;				// Pr�fen, ob ein Artikel als Sonderbestellartikel verf�gbar ist

                    if($LetzteLIENR != (string)$xmlLieferant->LIE_NR)
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($xmlLieferant->Name,($BildschirmBreite<1024?742:954),'font-weight:bold;background-color:#FF8242');
                        $Form->ZeileEnde();
                        if((int)$xmlLieferant->OnlineBestellung == 0)
                        {
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($AWISSprachKonserven['Infofeld']['ity_lbl_4'].': ',250,'background-color:#FF8242');
                            $Form->Erstelle_TextLabel((string)$xmlLieferant->Bestelltelefon,($BildschirmBreite<1024?541:704),'background-color:#FF8242');
                            $Form->ZeileEnde();
                        }

                        $Form->ZeileStart();
                        $AktuellerTag = date('N');
                        $BestellZeitpunkt = '';
                        if($AktuellerTag >= 1 AND $AktuellerTag <= 4)		// Mo-Do
                        {
                            $Form->Erstelle_TextLabel($AWISSprachKonserven['Infofeld']['ity_lbl_314'].': ',250,'background-color:#FF8242');
                            $Form->Erstelle_TextLabel((string)$xmlLieferant->BestellzeitpunktMoDo,($BildschirmBreite<1024?541:704),'font-weight:bold;background-color:#FF8242');
                            if((string)$xmlLieferant->BestellzeitpunktMoDo != '')
                            {
                                if(($BestellZeitpunkt = strtotime((string)$xmlLieferant->BestellzeitpunktMoDo))===false)
                                {
                                    $BestellZeitpunkt = '~'.(string)$xmlLieferant->BestellzeitpunktMoDo;
                                }
                            }
                        }
                        elseif($AktuellerTag == 5)		// Fr
                        {
                            $Form->Erstelle_TextLabel($AWISSprachKonserven['Infofeld']['ity_lbl_315'].': ',250,'background-color:#FF8242');
                            $Form->Erstelle_TextLabel((string)$xmlLieferant->BestellzeitpunktFr,($BildschirmBreite<1024?541:754),'background-color:#FF8242');
                            if((string)$xmlLieferant->BestellzeitpunktFr != '')
                            {
                                if(($BestellZeitpunkt = strtotime((string)$xmlLieferant->BestellzeitpunktFr))===false)
                                {
                                    $BestellZeitpunkt = '~'.(string)$xmlLieferant->BestellzeitpunktFr;
                                }
                            }
                        }
                        elseif($AktuellerTag == 6)		// Sa
                        {
                            $Form->Erstelle_TextLabel($AWISSprachKonserven['Infofeld']['ity_lbl_316'].': ',250,'background-color:#FF8242');
                            $Form->Erstelle_TextLabel((string)$xmlLieferant->BestellzeitpunktSa,($BildschirmBreite<1024?541:754),'background-color:#FF8242');
                            if((string)$xmlLieferant->BestellzeitpunktSa != '')
                            {
                                if(($BestellZeitpunkt = strtotime((string)$xmlLieferant->BestellzeitpunktSa))===false)
                                {
                                    $BestellZeitpunkt = '~'.(string)$xmlLieferant->BestellzeitpunktSa;
                                }
                            }
                        }
                        $Form->ZeileEnde();


                        if(substr($BestellZeitpunkt,0,1)=='~')			// Freitext ist angegeben!
                        {
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel(utf8_encode('HINWEIS: '.substr($BestellZeitpunkt,1)), 0,awisFormular::FORMAT_HINWEIS,'','');
                            $Form->ZeileEnde();
                        }
                        elseif($BestellZeitpunkt!='')
                        {
                            if($AktuellerTag>=6)
                            {
                                $LagerBestandPruefen = false;
                                $SonderBestellungPruefen = false;

                                if($BestellZeitpunkt!='' AND $BestellZeitpunkt<time())			// Bestellzeitpunkt vom Lieferanten ist �berschritten
                                {
                                    $Form->ZeileStart();
                                    $Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['ZEIT_3']), ($BildschirmBreite<1024?541:704),awisFormular::FORMAT_HINWEIS,'','');
                                    $Form->ZeileEnde();
                                }
                            }
                            elseif($BestellZeitpunkt<time())			// Bestellzeitpunkt vom Lieferanten ist �berschritten
                            {
                                if(time()<strtotime('16:45'))		// Letzter Zeitpunkt der Bestellung aus einem Lager
                                {
                                    $LagerBestandPruefen = true;
                                    $SonderBestellungPruefen = true;
                                    $Form->ZeileStart();
                                    $Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['ZEIT_1']), ($BildschirmBreite<1024?541:704),awisFormular::FORMAT_HINWEIS,'','');
                                    $Form->ZeileEnde();
                                }
                                else
                                {
                                    $Form->ZeileStart();
                                    $Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['ZEIT_2']), ($BildschirmBreite<1024?541:704),awisFormular::FORMAT_HINWEIS,'','');
                                    $Form->ZeileEnde();

                                    $SonderBestellungPruefen = false;
                                    $LagerBestandPruefen = false;
                                }
                            }
                        }

                        $Form->Trennzeile();

                        $LetzteLIENR = (string)$xmlLieferant->LIE_NR;
                        $DS=0;
                    }

                    $Form->ZeileStart();
                    $Form->Erstelle_Liste_Ueberschrift(utf8_encode($AWISSprachKonserven['ZPA']['txt_Uebernehmen']),50,'','',utf8_encode($AWISSprachKonserven['ZPA']['ttt_Uebernehmen']));
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'],150);
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'],350);
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_BEZEICHNUNG'],142);

                    if($BildschirmBreite<1024)
                    {
                        $Form->ZeileEnde();
                        $Form->ZeileStart();
                        $Form->Erstelle_Liste_Ueberschrift('',80);
                    }
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['ATW_JN'],35,'','',$AWISSprachKonserven['ZPA']['ttt_ATW_JN']);
                    if(($Recht10010&256)==256)	// EK-Berechtigung!
                    {
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_EK'],75,'text-align:right;');
                    }
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['txt_VKKunde'],105,'text-align:right;');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['txt_Bestand'],80,'text-align:right;');
                    $Form->ZeileEnde();

                    if((int)$xmlLieferant->Artikelliste->Treffer > 0)
                    {
                        $Anzeige=array();
                        foreach($xmlLieferant->Artikelliste->ArtikelTreffer as $xmlArtikel)
                        {
                            if((string)$xmlArtikel->Zukaufartikel->ZLA_KEY!=''
                                AND isset($Anzeige[(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]))
                            {
                                continue;
                            }
                            $Anzeige[(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]=1;
                            $DS++;


                            $SQL = 'INSERT INTO ZUKAUFPREISAUSKUNFT(';
                            $SQL .= ' ZPA_XBN_KEY,  ZPA_ZLA_KEY,  ZPA_MENGE,  ZPA_ANFRAGEZEITPUNKT,  ZPA_BESTANDEIGEN';
                            $SQL .= ', ZPA_BESTANDNACHBAR, ZPA_FIL_ID, ZPA_PREIS, ZPA_LIE_NR, ZPA_BESTELLNUMMER';
                            $SQL .= ', ZPA_ZAL_KEY, ZPA_LAGERMENGE, ZPA_WARENKORB, ZPA_ARTIKELBEZEICHNUNG';
                            $SQL .= ', ZPA_HERSTELLERCODE, ZPA_HERSTELLER, ZPA_BESTELLIDLIEFERANT,ZPA_LIEFERART';
                            $SQL .= ', ZPA_USER, ZPA_USERDAT';
                            $SQL .= ') VALUES (';
                            $SQL .= ' '.$AWISBenutzer->BenutzerID();
                            $SQL .= ', '.$DB->FeldInhaltFormat('N0',(string)$xmlArtikel->Zukaufartikel->ZLA_KEY,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST['sucZPA_MENGE'],true);
                            $SQL .= ', SYSDATE';
                            $SQL .= ', 0';
                            $SQL .= ', 0';
                            $SQL .= ', 0';
                            $SQL .= ', '.$DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Finanzen->VK,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(string)$xmlLieferant->LIE_NR,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(string)$xmlArtikel->Bestellnummer,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('N0',(string)$xmlArtikel->Zukaufartikel->ZAL_KEY,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('N0',(string)$xmlArtikel->Verfuegbarkeit->Liefermenge,false);
                            $SQL .= ', 0';		// NICHT BESTELLEN => Nicht im Warenkorb!
                            //$SQL .= ', '.$DB->FeldInhaltFormat('T',urlencode((string)$xmlArtikel->Artikelbezeichnung),true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',((string)$xmlArtikel->Artikelbezeichnung),true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(string)$xmlArtikel->Herstellercode,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(string)$xmlArtikel->Hersteller,true);
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(string)$xmlArtikel->BestellIdLieferant,true);       							// Nicht bei allen (v.a. Wertenbach)
                            $SQL .= ', '.$DB->FeldInhaltFormat('T',(isset($xmlArtikel->Verfuegbarkeit->Lieferarten)?(string)$xmlArtikel->Verfuegbarkeit->Lieferarten:''),true);       	// Lieferaten, z.B. bei TROST
                            $SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
                            $SQL .= ', SYSDATE)';

                            if($DB->Ausfuehren($SQL)===false)
                            {
                                $Form->DebugAusgabe(1,$SQL);
                            }
                            else
                            {
                                $SQL = 'SELECT seq_ZPA_KEY.CurrVal AS KEY FROM DUAL';
                                $rsKey = $DB->RecordSetOeffnen($SQL);
                                $ZPA_KEY=$rsKey->FeldInhalt('KEY');
                            }

                            //***********************************
                            // Sperrliste beachten
                            //***********************************

                            $SperrlistenSQL  ='select * from V_ZUKAUFSORTIMENTESPERREN';
                            $SperrlistenSQL .=' where ';
                            $SperrlistenSQL .=' (';
                            $SperrlistenSQL .='   ZSS_ZLH_KEY is null or ZSS_ZLH_KEY = '.$DB->WertSetzen('ZSS','N0',(string)$xmlArtikel->Hersteller->ZLH_KEY);;
                            $SperrlistenSQL .=' )';
                            $SperrlistenSQL .=' and ';
                            $SperrlistenSQL .=' (';
                            $SperrlistenSQL .='   ZSS_SORTIMENT is null or ZSS_SORTIMENT = (SELECT ZLA_SORTIMENT FROM ZUKAUFARTIKEL WHERE ZLA_KEY = '.$DB->WertSetzen('ZSS','N0', (string)$xmlArtikel->Zukaufartikel->ZLA_KEY, false);
                            $SperrlistenSQL .=' ))';
                            $SperrlistenSQL .=' and';
                            $SperrlistenSQL .=' (';
                            $SperrlistenSQL .='   ZSS_LIE_NR is null or ZSS_LIE_NR = '.$DB->WertSetzen('ZSS','N0',(string)$xmlLieferant->LIE_NR);
                            $SperrlistenSQL .=' )';
                            $SperrlistenSQL .=' and';
                            $SperrlistenSQL .=' (';
                            $SperrlistenSQL .='   ZSS_ZAL_BESTELLNUMMER is null or ZSS_ZAL_BESTELLNUMMER = ' .$DB->WertSetzen('ZSS','T',(string)$xmlArtikel->Bestellnummer);
                            $SperrlistenSQL .=' )';
                            $SperrlistenSQL .=' and (FIL_ID is null or FIL_ID = ' .$DB->WertSetzen('ZSS','N0',$Param['FIL_ID']) . ')';


                            $rsZSS = $DB->RecordSetOeffnen($SperrlistenSQL, $DB->Bindevariablen('ZSS'));

                            $Form->DebugAusgabe(1, 'SperrListenSQL: ' . $DB->LetzterSQL());

                            $SPVSperre = false;
                            if(!$rsZSS->EOF())
                            {
                                $Form->ZeileStart();
                                $Form->Erstelle_HinweisIcon('icon_warnung', 20, -1);
                                $Form->Erstelle_TextLabel('Artikel steht auf der Sperrliste. Dieser kann nicht extern beschafft werden.',0,'Hinweis');
                                $Form->ZeileEnde();
                                $SPVSperre=true;
                            }


                            //************************
                            // Artikel anzeigen
                            //************************
                            $Form->ZeileStart();

                            // Hier die Auswahl zwischen Abholung und Lieferung
                            if((string)$xmlArtikel->Verfuegbarkeit->Liefermenge>0 AND $SPVSperre==false)
                            {
                                $Form->Erstelle_Checkbox('Auswahl_'.$ZPA_KEY,'off',52,true,'on','',$AWISSprachKonserven['ZPA']['ttt_Uebernehmen']);

                            }
                            else
                            {
                                $Form->Erstelle_ListenFeld('#LEER','--',0,(3*16),false,($DS%2));
                                $AnzBilder=3;
                            }

                            $Link = '';
                            if($Recht10001 > 0 AND (string)$xmlArtikel->Zukaufartikel->ZLA_KEY!='')
                            {
                                $Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.(string)$xmlArtikel->Zukaufartikel->ZLA_KEY;
                            }
                            $Form->Erstelle_ListenFeld('Artikelnummer',$xmlArtikel->Artikelnummer,0,150,false,($DS%2),'',$Link);
                            //$Form->Erstelle_ListenFeld('Artikelbezeichnung',(urldecode((string)$xmlArtikel->Artikelbezeichnung)),0,350,false,($DS%2));
                            $Form->Erstelle_ListenFeld('Artikelbezeichnung',(((string)$xmlArtikel->Artikelbezeichnung)),0,350,false,($DS%2));

                            $Text = substr((string)$xmlArtikel->HerstellerInfos->ZLH_BEZEICHNUNG,0,10);
                            if($Text=='')
                            {
                                $Text=(string)$xmlArtikel->Herstellername;
                                if($Text=='')
                                {
                                    $Text=(string)$xmlArtikel->Herstellercode;
                                }
                            }
                            $Link = '';
                            if($Recht10001 > 0 AND (string)$xmlArtikel->Zukaufartikel->ZLA_KEY!='')
                            {
                                $Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.(string)$xmlArtikel->Zukaufartikel->ZLA_KEY;
                            }

                            $Link = '';
                            if($Recht10012 > 0 AND (string)$xmlArtikel->HerstellerInfos->ZLH_KEY!='')
                            {
                                $Link = '/zukauf/zukaufhersteller/zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY='.(string)$xmlArtikel->HerstellerInfos->ZLH_KEY;
                            }
                            $Form->Erstelle_ListenFeld('Hersteller',$AWISWerkzeug->awisGrossKlein($Text),0,142,false,($DS%2),'',$Link);
                            if($BildschirmBreite<1024)
                            {
                                $Form->ZeileEnde();
                                $Form->ZeileStart();
                                $Form->Erstelle_TextLabel('',82);
                            }

                            $Wert = $Form->WerteListe($AWISSprachKonserven['Liste']['lst_JaNein'],$xmlArtikel->Tauschteil);
                            $Form->Erstelle_ListenFeld('Tauschteil',$Wert,0,35,false,($DS%2),'','','T','L',(isset($xmlArtikel->Tauschteilwert)?'Altteilwert: '.$xmlArtikel->Tauschteilwert:'Kein Altteilwert'));

                            // 13.01.2014, Ber�cksichtigung VPE aus
                            //$ATUVP = isset($xmlArtikel->Finanzen->ATUVP)?(int)$xmlArtikel->Finanzen->ATUVP:0;

                            // 17.02.2014, Bere�cksichtigung Preiseinheit Zukauf (ASI 300), Umstellung notwending wegen fehlendem Feld in der WAWI
                            $ATUVP = isset($xmlArtikel->Finanzen->ATUPreiseinheit)?(int)$xmlArtikel->Finanzen->ATUPreiseinheit:0;

                            if($ATUVP==0)
                            {
                                // 13.01.2014, Ber�cksichtigung VPE aus
                                $ATUVP = isset($xmlArtikel->Finanzen->ATUVP)?(int)$xmlArtikel->Finanzen->ATUVP:0;
                            }


                            if(isset($xmlArtikel->Finanzen->ATUPreiseinheit) AND (int)$xmlArtikel->Finanzen->ATUPreiseinheit>0)
                            {
                                $ATUVP = (int)$xmlArtikel->Finanzen->ATUPreiseinheit;
                            }
                            elseif(isset($xmlArtikel->Finanzen->ATUVP) AND (int)$xmlArtikel->Finanzen->ATUVP>0)
                            {
                                $ATU_VK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Zukaufartikel->ATU_VK,true)/$ATUVP;
                            }
                            else
                            {
                                $ATU_VK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Zukaufartikel->ATU_VK,true);
                            }

                            $KundenVK = $DB->FeldInhaltFormat('N2',(string)$xmlArtikel->Finanzen->VK,true);
                            //$Form->DebugAusgabe(1,$ATU_VK,$KundenVK,(string)$xmlArtikel->Finanzen->ATUVP);
                            $Style = '';
                            $ToolTippText = '';
                            if($ATU_VK > 0 AND $ATU_VK < $KundenVK)
                            {
                                $Style = 'background-color:yellow;color:#FF0000';
                                $ToolTippText = utf8_encode($AWISSprachKonserven['ZLB']['ttt_HinweisATUPreisvergleich']);
                            }
                            elseif(isset($FarbCodesGruen[(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]) AND $FarbCodesGruen[(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]==(string)$xmlLieferant->LIE_NR)
                            {
                                $Style = 'background-color:#00AA00;color:#FFFFFF;font-weight:bold;';
                                $ToolTippText = utf8_encode($AWISSprachKonserven['ZLB']['ttt_HinweisATUPreisvergleichGruen']);
                            }

                            if(isset($FarbCodesOrange[(string)$xmlLieferant->LIE_NR][(string)$xmlArtikel->Zukaufartikel->ZLA_KEY]))
                            {
                                $Style = 'background-color:#FF8000;color:#000000';
                                $ToolTippText = $AWISSprachKonserven['ZLB']['ttt_HinweisATUPreisvergleichOrange'];
                            }
                            if(($Recht10010&256)==256)	// EK-Berechtigung!
                            {
                                $Form->Erstelle_ListenFeld('EK',$xmlArtikel->Finanzen->EK,0,75,false,($DS%2),'','','N2','R');
                            }

                            $Form->Erstelle_ListenFeld('VK',$xmlArtikel->Finanzen->VK,0,75,false,($DS%2),$Style,'','N2','R',$ToolTippText);
                            $Form->Erstelle_ListenFeld('VPE','/'.($xmlArtikel->Finanzen->Preiseinheit=='null'?'?':$xmlArtikel->Finanzen->Preiseinheit),0,28,false,($DS%2),'','','T','R',$AWISSprachKonserven['AST']['AST_VPE']);
                            $Form->Erstelle_ListenFeld('Liefermenge',(string)$xmlArtikel->Verfuegbarkeit->Liefermenge,0,80,false,($DS%2),((string)$xmlArtikel->Verfuegbarkeit->Liefermenge=='0'?'color:red;font-weight:bold':''),'','N0','R');
                            $Form->ZeileEnde();

                            // Spezialfall TROST: Sonderlieferwege
                            if((string)$xmlLieferant->LIE_NR=='0255')
                            {
                                $Lieferarten = explode(',',(isset($xmlArtikel->Verfuegbarkeit->Lieferarten)?(string)$xmlArtikel->Verfuegbarkeit->Lieferarten:''));

                                foreach($Lieferarten AS $Lieferart)
                                {
                                    switch ($Lieferart)
                                    {
                                        case 502:			// Abholung nach Lieferung
                                            $Form->ZeileStart();
                                            $Form->Erstelle_ListenFeld('Anzeige','ACHTUNG: Lieferart beachten',0,($BildschirmBreite<1024?450:650),false,($DS%2));
                                            $Form->ZeileEnde();
                                            break;
                                    }
                                }
                            }


                            foreach($xmlArtikel->Verfuegbarkeit->Verkaufshaus as $Verkaufshaus)
                            {
                                $Form->ZeileStart();
                                $Form->Erstelle_TextLabel('',52);

                                if($IconsAnzeigenVerfuegbarkeit AND $TROST_SAP)
                                {
                                    if(substr((string)$Verkaufshaus->Lieferid,0,3)=='AB-')     // Abholung m�glich
                                    {
                                        $Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_mensch_gruen.png',(string)$Verkaufshaus->Lieferid,($DS%2));
                                    }
                                    elseif(substr((string)$Verkaufshaus->Lieferid,0,3)=='AD-')  // Abholung unter ber�cksichtung des Datums
                                    {
                                        $Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_mensch.png',(string)$Verkaufshaus->Lieferid,($DS%2));
                                    }
                                    elseif((int)$Verkaufshaus->Liefermenge>0)       // Sonstige Lieferarten als LKW darstellen
                                    {
                                        $Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_lkw.png',(string)$Verkaufshaus->Lieferid,($DS%2));
                                    }
                                    else
                                    {
                                        $Form->Erstelle_TextLabel('',20);
                                    }
                                    $LieferText = trim((string)$Verkaufshaus->Lieferort . ' ' . $Verkaufshaus->Lieferanzeige . ' ' .($Verkaufshaus->Liefermenge!=''?'('.$Verkaufshaus->Liefermenge.')':''));
                                    $Form->Erstelle_ListenFeld('Lieferung',$LieferText,0,700,false,($DS%2),(string)$Verkaufshaus->LieferanzeigeStyle,'','','',(string)$Verkaufshaus->Lieferanzeigetooltipp);
                                }
                                else
                                {
                                     $Form->Erstelle_ListenFeld('Ort',utf8_decode((string)$Verkaufshaus->Lieferort),0,110,false,($DS%2));
                                     $Form->Erstelle_ListenFeld('Anzeige',($Verkaufshaus->Lieferanzeige),0,($BildschirmBreite<1024?550:550),false,($DS%2),(string)$Verkaufshaus->LieferanzeigeStyle,'','','',(string)$Verkaufshaus->Lieferanzeigetooltipp);
                                     $Form->Erstelle_ListenFeld('Lagermenge',$Verkaufshaus->Liefermenge,0,($BildschirmBreite<1024?50:150),false,($DS%2));
                                }
                                $Form->ZeileEnde();
                            }

                            // Bekannter Artikel
                            if((int)$xmlArtikel->Zukaufartikel->ZLA_KEY!=0)
                            {
                                $Style = '';
                                $ATUNR = '';
                                $ToolTippText = '';
                                        // ATU Nummer mit Preiseinfluss kommt direkt
                                if((string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR != '')
                                {
                                    $AST_ATUNR = $ATUNR=(string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR;
                                }
                                else
                                {
                                        // ATU Nummer ohne Preiseinfluss
                                    $SQL = 'SELECT ZAI_ID, AST_BEZEICHNUNGWW FROM ZUKAUFLIEFERANTENARTIKELIDS INNER JOIN ARTIKELSTAMM ON AST_ATUNR = ZAI_ID WHERE ZAI_ZLA_KEY = 0'.((int)$xmlArtikel->Zukaufartikel->ZLA_KEY).' AND ZAI_AID_NR = 4 AND ROWNUM=1';
                                    $rsZAI = $DB->RecordSetOeffnen($SQL);
                                    if(!$rsZAI->EOF())
                                    {
                                        $AST_ATUNR = $rsZAI->FeldInhalt('ZAI_ID');
                                        $ATUNR = '('.$rsZAI->FeldInhalt('ZAI_ID').')';
                                        $Style = 'font-style:italic;';
                                        $ToolTippText = $rsZAI->FeldInhalt('AST_BEZEICHNUNGWW').' / '.$AWISSprachKonserven['ZLB']['ttt_HinweisATUNROhnePreiseinfluss'];
                                    }
                                    else
                                    {
                                        // ATU Nummer eines �hnlichen Artikels
                                        $SQL = 'SELECT ZAI_ID, AST_BEZEICHNUNGWW FROM ZUKAUFLIEFERANTENARTIKELIDS INNER JOIN ARTIKELSTAMM ON AST_ATUNR = ZAI_ID WHERE ZAI_ZLA_KEY = 0'.((int)$xmlArtikel->Zukaufartikel->ZLA_KEY).' AND ZAI_AID_NR = 7 AND ROWNUM=1';
                                        $rsZAI = $DB->RecordSetOeffnen($SQL);
                                        if(!$rsZAI->EOF())
                                        {
                                            $AST_ATUNR = $rsZAI->FeldInhalt('ZAI_ID');
                                            $ATUNR = '(~'.$rsZAI->FeldInhalt('ZAI_ID').')';
                                            $Style = 'font-style:italic;color:darkgray';
                                            $ToolTippText = $rsZAI->FeldInhalt('AST_BEZEICHNUNGWW').' / '.$AWISSprachKonserven['ZLB']['ttt_HinweisATUNRAehnlicherArtikel'];
                                        }
                                    }
                                }

                                if($ATUNR != '')
                                {
                                    $Form->ZeileStart();
                                    $Form->Erstelle_TextLabel('',82);
                                    $Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$AST_ATUNR;
                                    $Form->Erstelle_ListenFeld('ATUNR',$ATUNR,0,80,false,($DS%2),$Style,$Link,'T','L',$ToolTippText);

                                    //*************************************************
                                    //Filialbestand ermitteln
                                    //*************************************************
                                    $BindeVariablen=array();
                                    $BindeVariablen['var_T_ast_atunr']=(string)$xmlArtikel->Zukaufartikel->ZLA_AST_ATUNR;
                                    $BindeVariablen['var_N0_fil_id']=$PEIKey;

                                    $SQL = 'SELECT FIB_BESTAND';
                                    $SQL .= ' FROM FilialBestand';
                                    $SQL .= ' WHERE FIB_AST_ATUNR = :var_T_ast_atunr';
                                    $SQL .= ' AND FIB_FIL_ID = :var_N0_fil_id';

                                    $rsFIB = $DB->RecordsetOeffnen($SQL,$BindeVariablen);
                                    $BestandEigen = 0;
                                    $ZPA_BestandEigen=0;
                                    if(!$rsFIB->EOF())
                                    {
                                        $ZPA_BestandEigen = $rsFIB->FeldInhalt('FIB_BESTAND');
                                        $BestandEigen = $rsFIB->FeldInhalt('FIB_BESTAND');
                                    }

                                    //***********************************************
                                    //
                                    // Bestand Nachbarfiliale
                                    //
                                    //***********************************************
                                    $BindeVariablen=array();
                                    $BindeVariablen['var_T_ast_atunr']=$ATUNR;
                                    $BindeVariablen['var_N0_fil_id']=$PEIKey;

                                    $SQL = 'SELECT DATEN.* ';
                                    $SQL .=' FROM(';
                                    $SQL .= 'SELECT DISTINCT CASE WHEN FEG_FIL_ID_VON = :var_N0_fil_id THEN FEG_FIL_ID_NACH';
                                    $SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
                                    $SQL .= ',FIB_BESTAND,FIL_LAGERKZ';
                                    $SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos ';
                                    $SQL .= '   INNER JOIN ZUKAUFLIEFERANTENARTIKELIDS ON asi_ast_atunr = zai_id AND zai_aid_nr = 4';
                                    $SQL .= '   WHERE ROWNUM<=1 AND asi_ast_atunr = :var_T_ast_atunr AND asi_ait_id=10) AS AST_LAGER_WEIDEN';
                                    $SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos ';
                                    $SQL .= '   WHERE ROWNUM<=1 AND asi_ast_atunr = :var_T_ast_atunr AND asi_ait_id=11) AS AST_LAGER_WERL';
                                    $SQL .= ' FROM FILIALENENTFERNUNGEN';
                                    $SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = :var_N0_fil_id THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID';
                                    $SQL .= ' INNER JOIN FilialebenenZuordnungen ON FIL_ID = FEZ_FIL_ID';
                                    $SQL .= ' INNER JOIN FilialBestand ON FIB_FIL_ID = FIL_ID AND FIB_AST_ATUNR = :var_T_ast_atunr';
                                    $SQL .= ' WHERE (FEG_FIL_ID_VON = :var_N0_fil_id';
                                    $SQL .= ' OR FEG_FIL_ID_NACH = :var_N0_fil_id)';
                                    $SQL .= ' AND FIB_BESTAND > 0';
                                    $SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
                                    $SQL .= ') DATEN ';
                                    $SQL .= ' WHERE ROWNUM <= 3';
                                    $rsFIB = $DB->RecordsetOeffnen($SQL,$BindeVariablen);

                                    $SummeBestandNachbar = 0;
                                    $BestandNachbar='';
                                    $ZPA_Nachbar = $rsFIB->FeldInhalt('FEG_FIL_ID');
                                    $ZPA_NachbarBestand = $rsFIB->FeldInhalt('FIB_BESTAND');
                                    $BestandsText = $AWISSprachKonserven['ZPA']['txtBestandWeidenWerl'].': '.$Form->Format('N0',$rsFIB->FeldInhalt('AST_LAGER_WEIDEN'),false).'/'.$Form->Format('N0',$rsFIB->FeldInhalt('AST_LAGER_WERL'),false);
                                    switch($rsFIB->FeldInhalt('FIL_LAGERKZ'))
                                    {
                                        case 'N':
                                            $LagerBestand = (int)$rsFIB->FeldInhalt('AST_LAGER_WEIDEN');
                                            break;
                                        case 'L':
                                            $LagerBestand = (int)$rsFIB->FeldInhalt('AST_LAGER_WERL');
                                            break;
                                        default:
                                            $LagerBestand = 0;
                                            break;
                                    }


                                    while(!$rsFIB->EOF())
                                    {
                                        $SummeBestandNachbar += $rsFIB->FeldInhalt('FIB_BESTAND');
                                        $BestandNachbar .= ' / '.$rsFIB->FeldInhalt('FEG_FIL_ID').': '.$rsFIB->FeldInhalt('FIB_BESTAND');
                                        $rsFIB->DSWeiter();
                                    }

                                    $Form->Erstelle_ListenFeld('BestandEigen',$AWISSprachKonserven['ZPA']['txtBestandEigen'].':'.$Form->Format('N0',$BestandEigen),10,150,false,($DS%2),'','','T','L',utf8_encode($AWISSprachKonserven['ZPA']['tttBestandEigen']));

                                    $Style='';
                                    $LieferantenInfos = array();
                                    if($LagerBestandPruefen AND ($LagerBestand>0))
                                    {
                                        $Style = 'color:#ff0000;background-color:yellow';
                                    }
                                    elseif($SonderBestellungPruefen)
                                    {
                                        $SQL = 'SELECT LIN_LIE_NR, LIE_NAME1, LIN_ITY_KEY, LIN_WERT';
                                        $SQL .= ' FROM artikelstamm ';
                                        $SQL .= ' INNER JOIN artikelstamminfos ON ast_atunr = asi_ast_atunr AND asi_ait_ID = 40';
                                        $SQL .= ' INNER JOIN lieferanten ON asi_wert = lie_nr';
                                        //$SQL .= ' INNER JOIN ZUKAUFHERSTELLERZUORDNUNGEN ON ZHZ_LIE_NR = lie_nr AND ZHZ_ZLH_KEY = '.$DB->WertSetzen('LIE', 'N0', (string)$xmlArtikel->Hersteller->ZLH_KEY);
                                        $SQL .= ' INNER JOIN lieferanteninfos ON LIN_LIE_NR = LIE_NR';
                                        $SQL .= ' LEFT OUTER JOIN informationstypen ON ITY_KEY = LIN_ITY_KEY AND ITY_Bereich = '.$DB->WertSetzen('LIE', 'T', 'Sonderbestellung');
                                        $SQL .= ' WHERE AST_ATUNR = '.$DB->WertSetzen('LIE', 'T', $ATUNR);
                                        $SQL .= ' AND AST_KENNUNG = '.$DB->WertSetzen('LIE', 'T', 'S');


                                        $rsLIE = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('LIE'));
                                        while(!$rsLIE->EOF())
                                        {
                                            $LieferantenInfos[$rsLIE->FeldInhalt('LIN_LIE_NR')][$rsLIE->FeldInhalt('LIN_ITY_KEY')]=$rsLIE->FeldInhalt('LIN_WERT');
                                            $rsLIE->DSWeiter();
                                        }
                                    }

                                    $Form->Erstelle_ListenFeld('BestandLaeger',$BestandsText,10,195,false,($DS%2),$Style,'','T','L',utf8_encode($AWISSprachKonserven['ZPA']['tttBestandWeidenWerl']));
                                    if($BildschirmBreite<1024)
                                    {
                                        $Form->ZeileEnde();
                                        $Form->ZeileStart();
                                        $Form->Erstelle_TextLabel('',82);
                                    }
                                    $Form->Erstelle_ListenFeld('BestandNachbar',$AWISSprachKonserven['ZPA']['txtBestandNachbarfilialen'].': '.substr($BestandNachbar,3),10,432,false,($DS%2),'','','T','L',utf8_encode($AWISSprachKonserven['ZPA']['tttBestandNachbarfilialen']));

                                    unset($rsFIB);

                                    $Form->ZeileEnde();

                                    if(!empty($LieferantenInfos) AND $DB->FeldInhaltFormat('N0',$xmlArtikel->Finanzen->EK,false)>=35)
                                    {
                                        foreach($LieferantenInfos AS $LIE_NR=>$Infos)
                                        {
                                            $Form->ZeileStart();
                                            $Form->Erstelle_TextLabel('',82);
                                            $Form->Erstelle_TextLabel('Sonderbestellung bei '.$LIE_NR.' pr&uuml;fen!'/*.' bis '.$Infos['314']*/, 0, 'color:#ff0000;background-color:yellow');
                                            $Form->ZeileEnde();
                                        }
                                    }

                                    $SQL = 'UPDATE ZUKAUFPREISAUSKUNFT';
                                    $SQL .= ' SET ZPA_BESTANDEIGEN = '.$DB->FeldInhaltFormat('N0',$ZPA_BestandEigen,false);
                                    $SQL .= ' ,ZPA_BESTANDNACHBAR = '.$DB->FeldInhaltFormat('N0',$ZPA_NachbarBestand,false);
                                    $SQL .= ' ,ZPA_FIL_ID = '.$DB->FeldInhaltFormat('N0',$ZPA_Nachbar,false);
                                    $SQL .= ' WHERE ZPA_KEY = 0'.$ZPA_KEY;

                                    if($DB->Ausfuehren($SQL)===false)
                                    {
                                        $Form->Hinweistext('FEHLER beim Speichern (1)',1);
                                    }

                                    // Hinweis, wenn bei der Anfrage schon eine abweichende VPE kommt
                                    if($ATUVP>0 AND $ATUVP!=((int)$xmlArtikel->Finanzen->Preiseinheit))
                                    {
                                        $Form->ZeileStart();
                                        $Form->Erstelle_TextLabel('',85);
                                        $Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['HinweisVPE']),0,'Hinweis');
                                        $Form->ZeileEnde();
                                    }
                                }
                            }
                            $Form->Trennzeile('');
                        }
                        $DS++;
                    }
                    else
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['txt_KeineArtikelGefunden'],($BildschirmBreite<1024?748:956),'Hinweis');
                        $Text = (strlen($xmlLieferant->Fehler->Text)>100?substr($xmlLieferant->Fehler->Text,0,97).'...':($xmlLieferant->Fehler->Text));
                        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['txt_FehlerMeldung'].': '.($Text),($BildschirmBreite<1024?748:956),'Hinweis');
                        $Form->ZeileEnde();
                    }
                }
            }

		}
	}
	elseif(isset($_GET['Add']))
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_zla_key']=$_GET['Add'];
		
		$SQL = 'SELECT ZLA_KEY';
		$SQL .= ' FROM Zukaufartikel';
		$SQL .= ' WHERE ZLA_KEY = :var_N0_zla_key';

		$rsZLA = $DB->RecordSetOeffnen($SQL,$BindeVariablen);


		$ID = md5($AWISBenutzer->BenutzerID().'#'.$rsZLA->FeldInhalt('ZLA_KEY'));
		if($ID==$_GET['ID'])
		{
			$Anfrage = new awisPreisauskunft();

			$SQL = 'INSERT INTO ZukaufPreisauskunft';
			$SQL .= '(ZPA_XBN_KEY, ZPA_ZLA_KEY, ZPA_LIE_Nr, ZPA_MENGE';
			$SQL .= ', ZPA_PREIS, ZPA_BESTANDEIGEN, ZPA_BESTANDNACHBAR, ZPA_FIL_ID';
			$SQL .= ' , ZPA_USER, ZPA_USERDAT)';
			$SQL .= ' VALUES (';
			$SQL .= $AWISBenutzer->BenutzerID();
			$SQL .= ', '.$rsZLA->FeldInhalt('ZLA_KEY');
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$Anfrage->LieferantenNummer());
			$SQL .= ', '.$DB->FeldInhaltFormat('N2',$_GET['Menge']);
			$SQL .= ' ,'.$DB->FeldInhaltFormat('N2',$Anfrage->Preis(),false);
			$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$Anfrage->BestandEigen(),false);
			$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$Anfrage->BestandNachbar('FIB_BESTAND'),false);
			$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$Anfrage->BestandNachbar('FIL_ID'),false);
			$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', SYSDATE)';

			$DB->Ausfuehren($SQL);

		}
	}
	elseif(isset($_GET['Del']))
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_zpa_key']=$_GET['Del'];
		
		$SQL = 'SELECT ZPA_KEY';
		$SQL .= ' FROM ZukaufPreisauskunft';
		$SQL .= ' WHERE ZPA_KEY = :var_N0_zpa_key';

		$rsZLA = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

		$ID = md5($AWISBenutzer->BenutzerID().'#'.$rsZLA->FeldInhalt('ZPA_KEY'));
		if($ID==$_GET['ID'])
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_xbn_key']=$AWISBenutzer->BenutzerID();
			$BindeVariablen['var_N0_zpa_key']=$_GET['Del'];
			
			$SQL = 'DELETE FROM zukaufpreisauskunft';
			$SQL .= ' WHERE ZPA_XBN_KEY = :var_N0_xbn_key';
			$SQL .= ' AND ZPA_KEY = :var_N0_zpa_key';
			$DB->Ausfuehren($SQL,'',false,$BindeVariablen);
		}
	}


	//***************************************************
	//* Mengen�nderung
	//***************************************************
	$Felder = explode(';',$Form->NameInArray($_POST,'txtZPA_MENGE_',1,1));
	foreach ($Felder as $Feld)
	{
		if(isset($_POST[$Feld]) AND $_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
		{
			$SQL = 'UPDATE ZukaufPreisauskunft';
			$SQL .= ' SET ZPA_Menge = '.$DB->FeldInhaltFormat('N2',$_POST[$Feld]);
			$SQL .= ' WHERE ZPA_XBN_KEY = '.$AWISBenutzer->BenutzerID();
			$SQL .= ' AND ZPA_KEY = 0'.substr($Feld,strlen('txtZPA_MENGE_'));

			$DB->Ausfuehren($SQL);
		}
	}

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['txtAktuellerWarenkorb'],0,'font-size:large;font-weight:bold');
	$Form->ZeileEnde();
	//******************************************************************************
	//* Vorhandenen Warenkorb anzeigen
	//******************************************************************************

	$BindeVariablen=array();
	$BindeVariablen['var_N0_xbn_key']=$AWISBenutzer->BenutzerID();
	
	$SQL = 'SELECT DISTINCT ZukaufPreisauskunft.*';
	$SQL .= ', COALESCE(ZLA_BEZEICHNUNG,ZPA_ARTIKELBEZEICHNUNG) AS ZLA_BEZEICHNUNG';
	$SQL .= ', COALESCE(ZLA_ARTIKELNUMMER, ZPA_BESTELLNUMMER) AS ZLA_ARTIKELNUMMER';
	$SQL .= ', COALESCE(zlh.ZLH_BEZEICHNUNG, zlh2.ZLH_BEZEICHNUNG, ZPA_HERSTELLER, ZPA_HERSTELLERCODE) AS HERSTELLERNAME';
	$SQL .= ', LIE_NR, LIE_NAME1, LIE_NAME2, ZLA_AST_ATUNR';
	$SQL .= ',(SELECT ZAI_ID FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4 AND ROWNUM=1) AS AST_ATUNR';
	$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos ';
	$SQL .= '   INNER JOIN ZUKAUFLIEFERANTENARTIKELIDS ON asi_ast_atunr = zai_id AND zai_aid_nr = 4';
	$SQL .= '   WHERE ROWNUM<=1 AND zai_zla_key = zla_key AND asi_ait_id=191) AS AST_VPE';
	$SQL .= ' FROM ZukaufPreisauskunft';
	$SQL .= ' LEFT OUTER JOIN Zukaufartikel ON ZPA_ZLA_KEY = ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN ZukaufLieferantenHersteller zlh ON zlh.ZLH_KEY = ZLA_ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN lieferanten ON ZPA_LIE_Nr = LIE_NR';
	$SQL .= ' LEFT OUTER JOIN zukaufherstellercodes ON ZPA_HERSTELLERCODE = ZHK_CODE AND ZHK_LIE_NR = ZPA_LIE_NR';
	$SQL .= ' LEFT OUTER JOIN zukauflieferantenhersteller zlh2 ON ZHK_ZLH_KEY = zlh2.ZLH_KEY';
	$SQL .= ' WHERE ZPA_XBN_KEY = :var_N0_xbn_key';
	$SQL .= ' AND ZPA_WARENKORB=1';
	$SQL .= ' ORDER BY ZPA_LIE_NR, ZLA_ARTIKELNUMMER, ZPA_AnfrageZeitpunkt DESC';
//$Form->DebugAusgabe(1,$SQL);

	$ATUArtikelAnzahl = array();
	
	$rsZPA = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	if($rsZPA->EOF())
	{
		$Form->Hinweistext($AWISSprachKonserven['ZPA']['txtKeineArtikelImWarenkorb']);
	}
	else
	{
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['ZPA_MENGE'],78);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'],150);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'],300);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_BEZEICHNUNG'],150);
		if($BildschirmBreite<1024)
		{
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('',78);
		}
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['txt_Bestand'],150,'text-align:right;');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'],100,'text-align:center;');
		$Form->ZeileEnde();
		$GesamtSumme = 0;
		$DS=0;
		$LetzterLieferant = 0;

		while(!$rsZPA->EOF())
		{
			if($LetzterLieferant != $rsZPA->FeldInhalt('ZPA_LIE_NR'))
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($rsZPA->FeldInhalt('LIE_NAME1'),550,'Ueberschrift');
				if($BildschirmBreite<1024)
				{
					$Form->ZeileEnde();
					$Form->ZeileStart();
				}
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['Versandart'].':',150,'');
				$Daten = $Form->WerteListe($AWISSprachKonserven['ZPA']['ZUSTELLUNGSARTEN']);
				$StandardZustellung = 2;
				$Form->Erstelle_SelectFeld('ZUSTELLUNG_'.$rsZPA->FeldInhalt('LIE_NR').'',(isset($Param['txtZUSTELLUNG_'.$rsZPA->FeldInhalt('LIE_NR')])?$Param['txtZUSTELLUNG_'.$rsZPA->FeldInhalt('LIE_NR')]:$StandardZustellung),200,true,null,'','','','',$Daten);
				$Form->ZeileEnde();
				$LetzterLieferant = $rsZPA->FeldInhalt('ZPA_LIE_NR');
			}

			// Identische ATU Artikel z�hlen
			if($rsZPA->FeldInhalt('AST_ATUNR')!='')
			{
				if(!isset($ATUArtikelAnzahl[$rsZPA->FeldInhalt('AST_ATUNR')]))
				{
					$ATUArtikelAnzahl[$rsZPA->FeldInhalt('AST_ATUNR')]=0;
				}
				$ATUArtikelAnzahl[$rsZPA->FeldInhalt('AST_ATUNR')]++;
			}
						
			$VPEWarnung = ($rsZPA->FeldInhalt('AST_VPE')==0?0:($rsZPA->FeldInhalt('ZPA_MENGE')%$rsZPA->FeldInhalt('AST_VPE'))!=0);
			$Hintergrund='';
			if($VPEWarnung)
			{
				$Hintergrund = 'background-color:yellow;text-decoration:blink;';
			}
			$Form->ZeileStart();

			$Icons = array();
			$ID = md5($AWISBenutzer->BenutzerID().'#'.$rsZPA->FeldInhalt('ZPA_KEY'));
			$Icons[] = array('delete','./preisauskunft_Main.php?cmdAktion=Suche&ID='.$ID.'&Del='.$rsZPA->FeldInhalt('ZPA_KEY'));
			$Form->Erstelle_ListeIcons($Icons,18,($DS%2));


			$Form->Erstelle_ListenFeld('ZPA_MENGE_'.$rsZPA->FeldInhalt('ZPA_KEY'),$rsZPA->FeldInhalt('ZPA_MENGE'),3,56,true,($DS%2),$Hintergrund,'','N0');
			$Link = '';
			if($AWISBenutzer->HatDasRecht(10001)>0)
			{
				$Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZPA->FeldInhalt('ZPA_ZLA_KEY');
			}
			$Form->Erstelle_ListenFeld('#ZLA_ARTIKELNUMMER',utf8_decode($rsZPA->FeldInhalt('ZLA_ARTIKELNUMMER')),3,150,false,($DS%2),$Hintergrund,$Link,'T');
			//$Form->Erstelle_ListenFeld('#ZLA_BEZEICHNUNG',$AWISWerkzeug->awisGrossKlein($rsZPA->FeldInhalt('ZLA_BEZEICHNUNG')),3,300,false,($DS%2),'','','T');
			// wg. CZ eingef�hrt.
			$Form->Erstelle_ListenFeld('#ZLA_BEZEICHNUNG',iconv('UTF-8','ISO-8859-1//TRANSLIT',$rsZPA->FeldInhalt('ZPA_ARTIKELBEZEICHNUNG')),3,300,false,($DS%2),$Hintergrund,'','T','L',$rsZPA->FeldInhalt('ZLA_BEZEICHNUNG'));
			//$Form->Erstelle_ListenFeld('#ZLA_BEZEICHNUNG',urldecode($rsZPA->FeldInhalt('ZPA_ARTIKELBEZEICHNUNG')),3,300,false,($DS%2),$Hintergrund,'','T','L',$rsZPA->FeldInhalt('ZLA_BEZEICHNUNG'));
			$Form->Erstelle_ListenFeld('#HERSTELLERNAME',$AWISWerkzeug->awisGrossKlein($rsZPA->FeldInhalt('HERSTELLERNAME')),3,150,false,($DS%2),$Hintergrund,'','T');

			if($BildschirmBreite<1024)
			{
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('*Filler','',0,78);
			}
			
			$Bestaende = '';
			if($rsZPA->FeldInhalt('ZPA_BESTANDEIGEN')>0)
			{
				$Bestaende = '<span style="color:red;background:yellow">'.$rsZPA->FeldInhalt('ZPA_BESTANDEIGEN').'</span>';
			}
			
			$Bestaende .= ' / '.$rsZPA->FeldInhalt('ZPA_BESTANDNACHBAR').'('.$rsZPA->FeldInhalt('ZPA_FIL_ID').')';
			$Form->Erstelle_ListenFeld('#BESTAENDE',$Bestaende,3,150,false,($DS%2),$Hintergrund,'','T','R',$AWISSprachKonserven['ZPA']['ttt_Bestand']);

			$Link = '';
			if($AWISBenutzer->HatDasRecht(450)>0)
			{
				$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$rsZPA->FeldInhalt('AST_ATUNR');
			}
			$Form->Erstelle_ListenFeld('#AST_ATUNR',($rsZPA->FeldInhalt('AST_ATUNR')==''?'':'('.$rsZPA->FeldInhalt('AST_ATUNR').')'),3,100,false,($DS%2),$Hintergrund,$Link,'T','Z',$AWISSprachKonserven['AST']['AST_VPE'].':'.$rsZPA->FeldInhalt('AST_VPE'));

			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('#NIX','',0,78,false,($DS%2),'');

			$TextBreite = 454;
			if($BildschirmBreite<1024)
			{
				$TextBreite = 196;
			}

			$Form->Erstelle_ListenFeld('#ZPA_ANFRAGEZEITPUNKT',$rsZPA->FeldInhalt('ZPA_ANFRAGEZEITPUNKT'),3,$TextBreite,false,($DS%2),'','','DU','',$AWISSprachKonserven['ZPA']['ZPA_ANFRAGEZEITPUNKT']);
			//$Form->Erstelle_ListenFeld('#LIE_NAME',$rsZPA->FeldInhalt('LIE_NAME1'),3,300,false,($DS%2),'','','T','');
			if($rsZPA->FeldInhalt('ZPA_PREIS')==0)
			{
				$Form->Erstelle_ListenFeld('#ZPA_PREIS',$AWISSprachKonserven['ZPA']['KeinPreisErmittelbar'],3,408,false,($DS%2),'font-weight:bold;color:#A51021','','T','R');
			}
			else
			{
				$Form->Erstelle_ListenFeld('#ZPA_PREIS',$rsZPA->FeldInhalt('ZPA_PREIS'),3,304,false,($DS%2),'font-weight:bold;color:#A51021','','N2','');
				$GesamtSumme += $DB->FeldInhaltFormat('N2',$rsZPA->FeldInhalt('ZPA_PREIS'))*$DB->FeldInhaltFormat('N2',$rsZPA->FeldInhalt('ZPA_MENGE'));
				$Form->Erstelle_ListenFeld('#ZPA_GESAMT',$DB->FeldInhaltFormat('N2',$rsZPA->FeldInhalt('ZPA_PREIS'))*$DB->FeldInhaltFormat('N2',$rsZPA->FeldInhalt('ZPA_MENGE')),3,100,false,($DS%2),'font-weight:bold;color:#A51021','','N2','');
			}
			$Form->ZeileEnde();

			if($VPEWarnung)
			{
				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('#Hinweis',utf8_encode($AWISSprachKonserven['ZPA']['HinweisVPE']),0,0,false,0,$Hintergrund.';color:#FF0000;font-weight:bold;');
				$Form->ZeileEnde();
			}

			$rsZPA->DSWeiter();
			$DS++;
		}
		$Form->Trennzeile();
		$Form->ZeileStart();
		$TextBreite = 845;
		if($BildschirmBreite<1024)
		{
			$TextBreite = 586;
		}
		$Form->Erstelle_ListenFeld('#ZPA_GESAMT',$AWISSprachKonserven['Wort']['Summe'].':',3,$TextBreite,false,($DS%2),'font-weight:bold;color:#A51021','','T','R');
		$Form->Erstelle_ListenFeld('#ZPA_GESAMT',$GesamtSumme,3,100,false,($DS%2),'font-weight:bold;color:#A51021','','N2','R');
		$Form->ZeileEnde();
		
		foreach($ATUArtikelAnzahl AS $ATUNR=>$Anzahl)
		{
			if($Anzahl>1)
			{
				$Form->Trennzeile();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['DoppelteATUNr']), ($BildschirmBreite<1024?541:704),awisFormular::FORMAT_HINWEIS,'','');
				$Form->ZeileEnde();
				$Form->Trennzeile();
				
				break;		// Ein Hinweis reicht
			}
		}
	}

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	if($AWISBenutzer->ParameterLesen('Startseiten-Version')==1)
	{
		$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	}
	else
	{
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	}
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', utf8_encode($AWISSprachKonserven['Wort']['lbl_suche']), 'W');
	$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', utf8_encode($AWISSprachKonserven['Wort']['lbl_loeschen']), 'D');
	$Form->Schaltflaeche('image', 'cmdBestellen', '', '/bilder/cmd_einkaufswagen.png', utf8_encode($AWISSprachKonserven['Wort']['lbl_WarenkorbBestellen']), '');
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=preisauskunft&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', utf8_encode($AWISSprachKonserven['Wort']['lbl_hilfe']), 'H');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200921041807");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200921041808");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

if(isset($Erg))
{
	$Form->Formular_Ende();
//	$Form->DebugAusgabe(1,htmlentities($Erg));
}

if(($Recht10010&16)!=0)
{
	$Form->Formular_Start();
	if(isset($Erg))
	{
		echo htmlentities($Erg);
	}
	$Form->Formular_Ende();
}
?>