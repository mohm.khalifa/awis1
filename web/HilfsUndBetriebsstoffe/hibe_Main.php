<html>
<head>
<title>Hilfs- und Betriebsstoffe - ATU webbasierendes Informationssystem</title>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
awis_ZeitMessung(0);		// Zeit setzen

include ("ATU_Header.php");	// Kopfzeile

awis_Debug(1,$_REQUEST);

/*if ($AWISBenutzer->BenutzerName()=='entwick')
{
	$con = ocilogon('awis','iloulua','awisrac.atu.de');	
}
else
{*/
	$con = awislogon();
//}

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1100);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffe',$AWISBenutzer->BenutzerName());
    die("Keine ausreichenden Rechte!");
}

$cmdAktion = '';

if(isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}

awis_RegisterErstellen(1100, $con, $cmdAktion);

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=hbst_" . $cmdAktion . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

//print "<input type=hidden name=cmdAktion value=Suche>";

//var_dump($HTTP_POST_VARS);
awislogoff($con);


echo '<br><font size=1>Diese Seite wurde in ' . sprintf('%.4f', awis_ZeitMessung(1)) . ' Sekunden erstellt.';

echo "<p><b>Verbunden mit Datenbank: ".ociserverversion($con)."</b>";

?>
</body>
</html>

