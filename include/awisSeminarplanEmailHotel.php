<?php
require_once 'awisSeminarplanEmail.php';

class awisSeminarplanEmailHotel extends awisSeminarplanEmail
{
    /**
     * awisSeminarplanEinladungsemail constructor.
     * @param $SET_KEY
     */
    function __construct($SET_KEY)
    {
        parent::__construct($SET_KEY);
    }


    /**
     * Vearbeitet die Daten
     */
    function Verarbeitung($Empfaenger = '')
    {
        try {
            $SQL = "SELECT MVT_TEXT, MVT_BETREFF from MAILVERSANDTEXTE WHERE MVT_BEREICH = 'SET_HOTEL_BESTAETIGUNG'";
            $rsMailText = $this->_DB->RecordSetOeffnen($SQL);

            $Text = $rsMailText->FeldInhalt('MVT_TEXT');
            $this->Text($Text);

            $this->Betreff($rsMailText->FeldInhalt('MVT_BETREFF'));

            $this->Absender($this->_Benutzer->ParameterLesen('SEHAbsender'));

            $EmpfaengerCC = serialize(array('ACADEMY_HOTEL'));

            foreach ($this->EmpfaengerAufloesen($EmpfaengerCC) as $EmpCC){
                $this->Adressliste($EmpCC,awisMailer::TYP_CC);
            }

            if($Empfaenger==''){
                $Empfaenger = serialize(array('TEILNEHMER', 'WL', 'GL'));
                foreach ($this->EmpfaengerAufloesen($Empfaenger) as $Emp) {
                    $this->AdressListe($Emp, awisMailer::TYP_TO);
                }
            }else{
                $this->Adressliste($Empfaenger,awisMailer::TYP_TO);
            }

            $this->_Mailer->DokumentenAnhang('SEH',$this->_SET_KEY);

            if(count($this->getAdressliste('TO')) > 0){
                $this->Senden();
                return true;
            }else{
                return false;
            }


        } catch (awisException $e){
            return false;
        }
    }

    /**
     * versendet die Email
     *
     * @return bool
     */
    public function Senden()
    {
        $this->_Mailer->Text($this->_Text, awisMailer::FORMAT_HTML);


        if(count($this->getAdressliste('TO'))>0){
            $this->_Mailer->Betreff($this->_Betreff);
        }else{
            $this->_Mailer->Betreff('ERROR: Hotelreservierungsmail bei SET_KEY ' . $this->_SET_KEY);
            $this->Adressliste('shuttle@de.atu.eu','TO');
        }

        return $this->_Mailer->MailInWarteschlange();
    }

}
