<?php
global $AWISCursorPosition;
global $AWISBenutzer;


$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('SEP','%');
$TextKonserven[]=array('SER','%');
$TextKonserven[]=array('SRG','%');
$TextKonserven[]=array('SET','%');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','txt_OhneZuordnung');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('Liste','lst_ALLE_0');
$TextKonserven[]=array('RFS','lst_RFS_STATUS');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht4200=$AWISBenutzer->HatDasRecht(4200);

echo "<form name=frmSuche method=post action=./seminarplan_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('SEPSuche'));

$Form->Formular_Start();

if (($Recht4200&1024)!=0)
{
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_SCHULUNGSART'].':',180);
	$KateogrieAlt = explode("|",$AWISSprachKonserven['SEP']['lst_SEP_SCHULUNGSART']);
	$Form->Erstelle_SelectFeld('*SEP_SCHULUNGSART',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_SCHULUNGSART']:''),200,true,'',$AWISSprachKonserven['Liste']['lst_ALLE_0'],'2','','',$KateogrieAlt);
	$Form->ZeileEnde();
}
else 
{
	$Form->ZeileStart();
	$Form->SchreibeHTMLCode('<input type=hidden name=sucSEP_SCHULUNGSART value=1>');
	$Form->ZeileEnde();
}
	
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_SEMINAR'].':',180);
$Form->Erstelle_TextFeld('*SEP_SEMINAR',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_SEMINAR']:''),50,300,true);
$AWISCursorPosition='sucSEP_SEMINAR';
$Form->ZeileEnde();	

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_BESCHREIBUNG'].':',180);
$Form->Erstelle_TextFeld('*SEP_BESCHREIBUNG',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_BESCHREIBUNG']:''),50,300,true);
$Form->ZeileEnde();	

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_VON'].':',180);
$Form->Erstelle_TextFeld('*SEP_VON',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_VON']:''),20,200,true,'','','','D');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_BIS'].':',180);
$Form->Erstelle_TextFeld('*SEP_BIS',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_BIS']:''),20,200,true,'','','','D');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SRG']['SRG_REGION'].':',180);
$SQL = 'SELECT SRG_KEY, SRG_REGION FROM SEMINARREGIONEN WHERE SRG_STATUS in (1,10) ORDER BY SRG_REGION';
$Form->Erstelle_SelectFeld('*SEP_SRG_KEY',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_SRG_KEY']:''),200,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SER']['SER_RAUM'].':',180);
$SQL = 'SELECT SER_KEY, SER_RAUM FROM SEMINARRAEUME WHERE SER_STATUS in (1,10) ORDER BY SER_RAUM';
$Form->Erstelle_SelectFeld('*SEP_SER_KEY',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_SER_KEY']:''),200,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_ZIELGRUPPE'].':',180);
$Form->Erstelle_TextFeld('*SEP_ZIELGRUPPE',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEP_ZIELGRUPPE']:''),50,300,true);
$Form->ZeileEnde();

//Suche nach Teilnehmer
if (($Recht4200&32) == 32)
{
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_PER_NR'].':',180);
	$Form->Erstelle_TextFeld('*SET_PER_NR',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SET_PER_NR']:''),20,100,true);
	$Form->ZeileEnde();
}


$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?'on':''),50,true,'on');
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

if(($Recht4200&4) == 4)		// Hinzufügen erlaubt?
{
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Form->Schaltflaeche('href','cmd_Seminarplan','/dokumentanzeigen.php?dateiname=Seminarprogramm_neu&erweiterung=pdf&bereich=seminarplan','/bilder/cmd_pdf.png');
//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=seminarplan&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

$Form->SchaltflaechenEnde();

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>
