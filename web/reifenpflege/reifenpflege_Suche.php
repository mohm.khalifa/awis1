<?php
/**
 * Suchmaske f�r die Auswahl Reifenpflege
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130502
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('RET','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','gelistet');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_KENN1');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15000=$AWISBenutzer->HatDasRecht(15000);		
	if ($Recht15000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./reifenpflege_Main.php?cmdAktion=Details>");	

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RRP'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();
	
	$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['gelistet'].':',200);
	$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_gelistet']);
	$Form->Erstelle_SelectFeld('*GELISTET',$Param['SPEICHERN']=='on'?$Param['GELISTET']:'','200:180', true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();
	$AWISCursorPosition='sucGELISTET';
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ATUNR'].':',200);
	$Form->Erstelle_TextFeld('*RRP_ATUNR',($Param['SPEICHERN']=='on'?$Param['RRP_ATUNR']:''),6,75,true,'','','','','','','',6);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'].':',200);
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*RST_REH_KEY',($Param['SPEICHERN']=='on'?$Param['RST_REH_KEY']:'0'),'200:180',true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNGWW'].':',200);
	$Form->Erstelle_TextFeld('*RRP_BEZEICHNUNGWW',($Param['SPEICHERN']=='on'?$Param['RRP_BEZEICHNUNGWW']:''),50,200,true,'','','','','','','',50);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN1_kurz'].':',200);
//	$Form->Erstelle_TextFeld('*RRP_KENN1',($Param['SPEICHERN']=='on'?$Param['RRP_KENN1']:''),1,50,true);
	$Daten = explode("|", $AWISSprachKonserven['Liste']['lst_KENN1']);
	$Form->Erstelle_SelectFeld('*RRP_KENN1',($Param['SPEICHERN']=='on'?$Param['RRP_KENN1']:''), '200:180', true, '', '0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'], '', '', '', $Daten);
	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN2'].':',200);
	$Form->Erstelle_TextFeld('*RRP_KENN2',($Param['SPEICHERN']=='on'?$Param['RRP_KENN2']:''),1,50,true,'','','','','','','',1);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN_L2'].':',200);
	$Form->Erstelle_TextFeld('*RRP_KENN_L2',($Param['SPEICHERN']=='on'?$Param['RRP_KENN_L2']:''),1,50,true,'','','','','','','',1);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_SUCH1'].':',200);
	$Form->Erstelle_TextFeld('*RRP_SUCH1',($Param['SPEICHERN']=='on'?$Param['RRP_SUCH1']:''),12,50,true,'','','','','','','',10);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_GUELTIGAB'].':',200);
	$Form->Erstelle_TextFeld('*RRP_GUELTIGAB',($Param['SPEICHERN']=='on'?$Param['RRP_GUELTIGAB']:''),20, 200,true,'','','','D','','','',10);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_GUELTIGBIS'].':',200);
	$Form->Erstelle_TextFeld('*RRP_GUELTIGBIS',($Param['SPEICHERN']=='on'?$Param['RRP_GUELTIGBIS']:''),20,200,true,'','','','D','','','',10);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_MEHRFACHKENNUNG'].':',200);
	$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_RRP_MEHRFACHKENNUNG_suche']);
    $Form->Erstelle_SelectFeld('*RRP_MEHRFACHKENNUNG',$Param['SPEICHERN']=='on'?$Param['RRP_MEHRFACHKENNUNG']:'0', '200:180', true, '','0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht15000&4) == 4)		
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201305021528");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201305021529");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>