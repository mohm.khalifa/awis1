<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;
global $con;
global $awisDBFehler;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

if(!isset($_GET['Datei']) or $_GET['Datei']=='')
//if($_GET['Datei']=='')
{
	echo "<meta http-equiv=refresh content=\"30; URL=./absatzSQL_Main.php\">";
}
?>
</head>

<body>
<?php

include ("ATU_Header.php");	// Kopfzeile

clearstatcache();

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung moeglich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,502);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'AbsatzSQL',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

echo "<p><h2>Absatz - SQL</h2><p>";

	// Pfad in den Programmparameter
$SQLPfad = awis_BenutzerParameter($con, 'PfadAbsatzSQLs', $AWISBenutzer->BenutzerName());

	//***********************************************************
	// Datei sofort ausfhren?
	//***********************************************************

if(is_file($SQLPfad . '/schedule'))
{
	$Schedule = @file($SQLPfad . '/schedule');
}
else
{
	$Schedule = array();
}


if(isset($_POST['cmdAusfuehren_x']) AND $_POST['cmdAusfuehren_x']!='')
{
	$Vorhanden = False;

	foreach($Schedule as $Zeile)
	{
		if(trim($Zeile) == $_POST['txtDateiName'])
		{
			$Vorhanden = True;
			break;
		}
	}

	if(!$Vorhanden)
	{
		$fd = fopen($SQLPfad . '/schedule','a+');
		fputs($fd, trim($_POST['txtDateiName']) . "\n");
		fclose($fd);

		echo '<span class=HinweisText>Datei ' . $_POST['txtDateiName'] . ' wird in den naechsten 60 Sekunden ausgefuehrt.</span>';
	}
	else
	{
		echo '<span class=HinweisText>Datei ' . $_POST['txtDateiName'] . ' ist bereits in der Warteschlange.</span>';
	}

	$Schedule = file($SQLPfad . '/schedule');
}

	//***********************************************************
	// Ge�derte Datei speichern
	//***********************************************************
if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='')
{
		// Parsen

	$SQL = str_replace("\'","'", $_POST['txtSQL']);
	$Zeilen = explode("\n", $SQL);
	$SQL = '';
	for($i = 0;$i<count($Zeilen);$i++)
	{
		if(substr($Zeilen[$i],0,3)!='--#')
		{
			$SQL .= $Zeilen[$i] . "\n";
		}
	}
	$SQL = "--# OSUMME, Script\n--# Letzte �nderung: " . date('d.M.Y, H:i') . "\n--# Autor: " . $AWISBenutzer->BenutzerName() . "\n" . $SQL;

	If(strstr(strtoupper($SQL), 'UPDATE') OR strstr(strtoupper($SQL), 'DELETE') OR strstr(strtoupper($SQL), 'MERGE')
		OR strstr(strtoupper($SQL), 'ALTER') OR strstr(strtoupper($SQL), 'CREATE') OR strstr(strtoupper($SQL), 'DROP')
		OR strstr(strtoupper($SQL), 'TRUNCATE') OR strstr(strtoupper($SQL), 'GRANT') OR strstr(strtoupper($SQL), 'DENY')
		OR strstr(strtoupper($SQL), 'REVOKE') OR strstr(strtoupper($SQL), 'RENAME'))
	{
		echo '<span class=HinweisText>Die Anweisungen UPDATE oder DELETE, sowie DDL und DCL Befehle werden nicht unterstuetzt.</span>';
		awisLogoff($con);
		print "<br><hr><input type=image alt=Zurck src=/bilder/zurueck.png name=cmdZurueck onclick=history.back();>";
		die();
	}

		// Speichern
	if($_POST['txtNeuerDateiName']!='')
	{
		$SQLDatei = $SQLPfad . '/osumme3_' . $_POST['txtNeuerDateiName'] . '.sql';
	}
	else		// alter Dateiname
	{
		$SQLDatei = $SQLPfad . '/' . $_POST['txtDateiName'];
		if(!is_writeable($SQLDatei))
		{
			echo 'Kann Datei nicht speichern';
		}
	}

	$fd = fopen($SQLDatei, 'w+');
	fputs($fd, str_replace("\'","'", $SQL));
	fclose($fd);

}
elseif(isset($_POST['cmdLoeschen_x']) AND $_POST['cmdLoeschen_x']!='')
{
	if(isset($_POST['cmdJa']) AND $_POST['cmdJa']=='Ja')
	{
		rename($SQLPfad . '/' . $_POST['txtDateiName'], $SQLPfad . '/' . $_POST['txtDateiName'] . '_' . $_SERVER['PHP_AUTH_USER'] . '_' . filectime($SQLPfad . '~~' . $_POST['txtDateiName']));
	}
	//elseif($_POST['cmdNein']=='')
	elseif(!isset($_POST['cmdNein']) OR $_POST['cmdNein']=='')
	{
		echo '<form name=frmAbsatz method=post action=./absatzSQL_Main.php>';
		echo 'Sind Sie sicher, dass Sie die Datei ' . $_POST['txtDateiName'] . ' loeschen moechten?';
		echo '<input type=hidden name=txtDateiName value=' . $_POST['txtDateiName'] . '>';
		echo '<input type=hidden name=cmdLoeschen_x value=0>';

		echo '<br><input type=submit name=cmdJa value=Ja> <input type=submit name=cmdNein value=Nein>';

		echo '</form>';
		awisLogoff($con);
		die();
	}
}


//****************************************
// Keine Datei gewaehlt, Uebersicht zeigen
//****************************************
if(!isset($_GET['Datei']) or $_GET['Datei']=='')
{
		// Verzeichnis �fnen
	$Verz = dir($SQLPfad);
	echo '<form name=frmAbsatz method=post action=./absatzSQL_Main.php>';
	echo '<table width=600 border=1>';
	echo '<tr><td id=FeldBez>Dateiname</td><td id=FeldBez>Letzte �nderung</td>';
	echo '<td id=FeldBez>durch</td>';
	echo '<td id=FeldBez>Status</td>';
	echo '</tr>';
	while($SQLDatei=$Verz->read())
	{
		if(substr($SQLDatei,0,7)=='osumme3' AND substr($SQLDatei, -4)=='.sql')		// Namen pruefen
		{
			$Zeilen = file($SQLPfad . '/' . $SQLDatei);
			echo '<tr><td><a href=./absatzSQL_Main.php?Datei=' . $SQLDatei . '>' . $SQLDatei . '</a></td>';
			echo '<td>';
			if(substr($Zeilen[1],0,3)=='--#')
			{
				echo substr($Zeilen[1],20);		// Zeit
			}
			else
			{
				echo '- -';
			}
			echo '</td>';

			echo '<input type=hidden name=txtDateiName value=' . $SQLDatei . '>';
			echo '<td>';
			if(substr($Zeilen[2],0,3)=='--#')
			{
				echo substr($Zeilen[2],11);		// Autor
			}
			else
			{
				echo '- -';
			}
			echo '</td>';
			echo '<td>';
			if(count($Schedule)==0)
			{
				echo 'Jede Nacht.';
			}
			else
			{
				for($i=0;$i<count($Schedule);$i++)
				{
					If(trim($Schedule[$i])==$SQLDatei)
					{
						echo 'Laeuft...';
						break;
					}
				}
				if($i>=count($Schedule))
				{
					echo 'Jede Nacht.';
				}
			}
			echo '</td></tr>';
		}
	}
	$Verz->close();	// Verzeichnis schlie�n
	echo '</table>';
	echo '</form>';

}
else		// Datei angegeben
{
	$SQLDatei = $_GET['Datei'];

	if(substr($SQLDatei,0,7)=='osumme3' AND substr($SQLDatei, -4)=='.sql')		// Namen pruefen
	{
		echo '<form name=frmSQL action=./absatzSQL_Main.php method=post>';

		echo '<table border=0 width=100%>';
		echo '<tr><td id=FeldBez>SQL Anweisung</td><td>';
		echo '<tr><td><input type=hidden name=txtDateiName value=' . $SQLDatei . '>';
		echo '<textarea name=txtSQL cols=100 rows=20>';
		$Zeilen = file($SQLPfad . '/' . $SQLDatei);
		for($i=0;$i<count($Zeilen);$i++)
		{
			if(substr($Zeilen[$i],0,3)!='--#' AND substr($Zeilen[$i],0,1)!="\n")
			{
				echo str_replace("\'","'", $Zeilen[$i]) . "";
			}
		}
		echo '</textarea></td><td>';
		echo '</table>';

		echo '<b>Neuer Dateiname:</b>&nbsp;osumme3_<input type=text name=txtNeuerDateiName value=>.sql';

		echo "<br><input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./absatzSQL_Main.php?Speichern=True'>";
		echo "&nbsp;<input type=image alt=\"Loeschen (Alt+X)\" src=/bilder/Muelleimer_gross.png name=cmdLoeschen  accesskey=x onclick=location.href='./absatzSQL_Main.php?Loeschen=True'>";
		echo "&nbsp;<input type=image alt=\"Ausfuehren (Alt+E)\" src=/bilder/ausfuehren.png name=cmdAusfuehren accesskey=E onclick=location.href='./absatzSQL_Main.php?Ausfuehren=True'>";

		echo '</form>';
	}
	else
	{
		echo '<span class=Hinweistext>Dies wird als Einbruchsversuch gewertet. Ihre Daten wurden protokolliert.';
		echo 'Schreiben Sie ein E-Mail an den Benutzer <k>entwick</k> um Ihre Aktion zu begruenden.';
		echo 'Wenn nicht in den naechsten 60 Minuten eine Begruendung vorliegt, wird Ihr Account automatisch';
		echo 'gesperrt.</span>';
	}
}

if(!isset($_GET['Datei']) or $_GET['Datei']=='')
{
	echo '<span class=HinweisText>Diese Seite wird alle 30 Sekunden aktualisiert.</span>';
}


if(!isset($_GET['ZurueckLink']) or $_GET['ZurueckLink']=='')
{
	print "<br><hr><input type=image alt=Zurck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";
}
else
{
	print "<br><hr><input type=image alt=Zurck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $_GET['ZurueckLink'] . "';>";
}
echo "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=absatzSQL&HilfeBereich=','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);

?>
</body>
</html>

