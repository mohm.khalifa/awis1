<?php
require_once 'awisBerichte.inc';
require_once('/daten/web/tuevabgleich/abgleich_funktionen.inc');

class ber_Abgleich
	extends awisBerichte
{
    private $KompletteBreite = 0;
    private $debug = '';
    
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
        $this->KompletteBreite = self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
        
        $TAG = new awis_abgleich_funktionen();
		// Daten f�r den Bericht laden
        $SQL =  'SELECT  tag.*, tgd.*, tkd.*, taa.*, ' .
                        'tue_tgd.tue_bez AS tue_tgd_bez, fil_tgd.fil_bez AS fil_tgd_bez, tua_tgd.tua_bez AS tua_tgd_bez, ' .
                        'tue_tkd.tue_bez AS tue_tkd_bez, fil_tkd.fil_bez AS fil_tkd_bez, tua_tkd.tua_bez AS tua_tkd_bez ' .
                'FROM tuevabgleich tag ' .
                'LEFT JOIN tuevgesellschaftendaten tgd ON tgd.tgd_key = tag.tag_tgd_key ' .
                'LEFT JOIN tuevkassendaten tkd ON tkd.tkd_key = tag.tag_tkd_key ' .
                'LEFT JOIN tuevabgleichart taa ON taa.taa_key = tag.tag_taa_key ' .
                'LEFT JOIN tuevorganisationen tue_tgd ON tue_tgd.tue_id = tgd.tgd_tuevnr ' .
                'LEFT JOIN filialen fil_tgd ON fil_tgd.fil_id = tgd.tgd_fil_id ' .
                'LEFT JOIN tuevarten tua_tgd ON tua_tgd.tua_id = tgd.tgd_tuevart ' .
                'LEFT JOIN tuevorganisationen tue_tkd ON tue_tkd.tue_id = tkd.tkd_tuevnr ' .
                'LEFT JOIN filialen fil_tkd ON fil_tkd.fil_id = tkd.tkd_fil_id ' .
                'LEFT JOIN tuevarten tua_tkd ON tua_tkd.tua_id = tkd.tkd_tuevart';
		
		// Alle Parameter auswerten
		// Format $Parameter[<Name>]=<Wert>
		//			Beispiel: $Parameter['TGD_FIL_ID']="60"
        // Dieses Format kann direkt von der Funktion "ErstelleTAGBedingung()" verarbeitet werden,
        //      solange die Bezeichnungen des assoziatifen Arrays passen (siehe Parameternamen in Klasse oder php-File die Suchparameter)
        $Bedingung = ' and tag_userdat >= (sysdate - 366)';         // nur Datens�tze der letzten 366 Tage anzeigen
        $Bedingung .= $TAG->ErstelleTAGBedingung($Parameter);

		if ($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
        $SQL .= ' order by tgd.tgd_belegnr, tgd.tgd_belegdatum';
//var_dump($SQL);
$this->debug = $SQL;
		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
 
		// Alle Textbausteine laden
		$TextKonserven[]=array('TAG','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eiegntliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
        static $BelegNrMerker = 'zum vergleichen auf eine andere Belegnummer';
        static $BelegDatumMerker = 'zum vergleichen auf ein anderes Belegdatum';
        
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetFillColor(192, 192, 192);
		
        $this->NeueSeite();
        $this->_Zeile = $this->_Parameter['LinkerRand'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        
        $this->ErstelleKopf();

        $DS = 1;                        // Z�hler der Datens�tze
        $Seitenzahl = 1;                // Z�hler der Seiten insgesamt
        while (!$this->_rec->EOF())
        {
            if (($this->_rec->FeldInhalt('TGD_BELEGNR') != $BelegNrMerker)          // wenn neue Belegnummer
            or ($this->_rec->FeldInhalt('TGD_BELEGDATUM') != $BelegDatumMerker))    // ODER neues Belegdatum
            {
                $BelegNrMerker = $this->_rec->FeldInhalt('TGD_BELEGNR');        // im Merker f�r erneuten Vergleich merken
                $BelegDatumMerker = $this->_rec->FeldInhalt('TGD_BELEGDATUM');  // im Merker f�r erneuten Vergleich merken
                $this->ErstelleBelegKopf();                                         // Kopf f�r neue Belegnummer erstellen
            }
            
            $this->ErstelleDS();                    // Bereich f�r einen Datensatz hinpinseln
            
            if (($DS%$this->_Parameter['DSproSeite']) == 0)         // wenn Anzahl der Datens�tze MODULO Datens�tze pro Seite
            {
                $this->ErstelleFuss($Seitenzahl);                   // Fusszeile
                $Seitenzahl++;                                      // Seitenzahl inkrementieren
                
                $this->NeueSeite();                                 // neue Seite und X/Y-Cursor-Positionen wieder auf Anfang
                $this->_Zeile = $this->_Parameter['Seite1-Oben'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];
            }            
            
            $this->_rec->DSWeiter();
            $DS++;
        }
        $this->ErstelleFuss($Seitenzahl);                   // Fusszeile (weil f�r letzte, bzw. wenn nur 1 Seite das if in der oberen Schliefe nicht anschl�gt)
        
//$this->Write($this->_Parameter['Zeilenhoehe'], $this->debug);        
        $this->FertigStellen($Ausgabeart);        
	}
	
    private function ErstelleKopf()
    {
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseUeberschrift']);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 2, $this->_AWISSprachkonserven['TAG']['PDF_AbgleichUeberschrift'], 0, 0, 'C', false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Parameter['Spalte1'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_FilNr'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte1'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte2'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Datum'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte2'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte3'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_KfzKennz'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte3'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte4'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Name'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte4'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte5'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Betrag'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte5'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Tuev'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte6'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_TuevArt'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2']+$this->_Parameter['Spalte3']+$this->_Parameter['Spalte4'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_AbgleichArt'], 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2']+$this->_Parameter['Spalte3']+$this->_Parameter['Spalte4']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte5']+$this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_AbgleichBearbeiter'], 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte5']+$this->_Parameter['Spalte6']);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_AbgleichDatum'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Bemerkung'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/2;
    }
    
    private function ErstelleBelegKopf()
    {
        $this->SetLineWidth(0.5);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_BelegNr'], 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte3']+$this->_Parameter['Spalte4']+$this->_Parameter['Spalte5'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TGD_BELEGNR'), 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte3']+$this->_Parameter['Spalte4']+$this->_Parameter['Spalte5']);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_BelegDatum'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte6'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('TGD_BELEGDATUM')), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];

        $this->SetLineWidth(0);
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
    }
    
    private function ErstelleDS()
    {
        $this->SetLineWidth(0);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte1'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N0', $this->_rec->FeldInhalt('TGD_FIL_ID')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte1'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte2'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('TGD_PRUEFDATUM')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte2'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte3'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TGD_KFZKENNZ')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte3'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte4'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TGD_NACHNAME')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte4'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte5'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TGD_PREIS')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte5'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TUE_TGD_BEZ')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte6'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TUA_TGD_BEZ')), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];

        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte1'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N0', $this->_rec->FeldInhalt('TKD_FIL_ID')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte1'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte2'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('TKD_DATUMZEIT')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte2'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte3'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TKD_KFZKENNZ')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte3'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte4'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TKD_NAME')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte4'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte5'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TKD_BETRAG')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte5'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TUE_TKD_BEZ')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte6'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TUA_TKD_BEZ')), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];        
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2']+$this->_Parameter['Spalte3']+$this->_Parameter['Spalte4'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TAA_BESCHREIBUNG')), 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte1']+$this->_Parameter['Spalte2']+$this->_Parameter['Spalte3']+$this->_Parameter['Spalte4']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte5']+$this->_Parameter['Spalte6'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TAG_USER')), 0, 0, 'L', false);
        $this->_Spalte += ($this->_Parameter['Spalte5']+$this->_Parameter['Spalte6']);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte7'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('DU', $this->_rec->FeldInhalt('TAG_USERDAT')), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];        
        
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T', $this->_rec->FeldInhalt('TAG_BEMERKUNG')), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];        
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/2;
    }
    
    private function ErstelleFuss($Seite)
    {
        $rs = $this->_DB->RecordSetOeffnen('select to_char(sysdate, \'DAY DD. MONTH YYYY\') as datum from dual');
        $Datum = $rs->FeldInhalt('DATUM');
        
        $this->SetLineWidth(0.5);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        
        $this->_Zeile = $this->_Parameter['Seite-Unten'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);        
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/2;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $Datum);
        
        $this->_Spalte = $this->_Parameter['RechteSeite'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Seite'] . $Seite);
    }
    
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
		
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
                /*
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,4,$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
                */
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{              
	    //$this->pdf_vorlage = $this->ImportPage(1);
	    //$this->useTemplate($pdf_vorlage);
		/*
		$Zeile = $this->_Parameter['Kopfzeile1-Oben'];
		$Spalte = $this->_Spalte;

		// Berichtsparameter anzeigen
		$this->SetXY($Spalte,$Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Ueberschrift-Groesse']);
		$this->Cell(30, 4, $this->_AWISSprachkonserven['PVS']['pdf_BvbKopf'] ,0 ,0 ,'L' ,'');

		$Zeile+=5;

		// Trennlinie
		$this->SetXY($Spalte,$Zeile);
		$this->Line($this->_Spalte,$Zeile,self::SeitenBreite()-self::RECHTERRAND,$Zeile);
		$Zeile+=2;
                */
	}	

    public function FertigStellen($Ausgabeart)        
    {
        //*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
    }
    
}

?>