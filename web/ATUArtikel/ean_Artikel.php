<?
/****************************************************************************************************
*
* 	Artikel-Liste f�r Lieferantenartikel
*
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt. 2003
*
****************************************************************************************************/
// Variablen
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $con;
global $Zeile;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 404);
if(($RechteStufe & 2)!=2)
{
	print "<span class=HinweisText>Keine ausreichenden Rechte</span>";
	awisLogoff($con);
	die();
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

If(isset($_GET['EANKEY']))
{
			//*********************************************************************
			// EANKEY wurde als Parameter �bergeben
			//*********************************************************************
	$SQL = "SELECT EANNummern.*, 'EAN' AS FUNDSTELLE ";
	$SQL .= " FROM EANNummern "; //, TeileInfos ";
//	$SQL .= " WHERE TEI_ITY_ID2 = 'EAN' AND TEI_KEY2=EAN_KEY ";
	$SQL .= " WHERE EAN_KEY=0" . $_GET['EANKEY'];
}
else
{
			//********************************************************************
			// Aktualisieren gew�hlt?
			// Block muss identisch sein zu artikel_Artikel.php !!!!
			//********************************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = $_POST['txtAuswahlSpeichern'];
		$Param .= ";" . $_POST['txtAST_ATUNR'];
		$Param .= ";" . $_POST['txtAST_BEZEICHNUNG'];
		$Param .= ";" . $_POST['txtTEI_SUCH'];
		$Param .= ";" . $_POST['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
		$Param .= ";" . $_POST['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
		$Param .= ";" . $_POST['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
		$Param .= ";" . $_POST['txtTEI_SUCH_Exakt'];		// Exakte Suche
		$Param .= ";" . $_POST['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
		$Param .= ";" . $_POST['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
		$Param .= ";" . $_POST['txtKommentar'];				// Kommentar im Artikel
		$Param .= ";" . $_POST['txtTEI_SUCH_XXX'];			// Suche nach beliebiger Nummer
		$Param .= ";" . $_POST['txtWUG_KEY'];				// Suche nach Warenuntergruppe
		$Param .= ";" . $_POST['txtWGR_KEY'];				// Suche nach Warengruppe
		$Param .= ";" . $_POST['txtOEPreise'];				// Immer OE-Preise zeigen

		awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $AWISBenutzer->BenutzerName() , $Param );
		awis_BenutzerParameterSpeichern($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName(), "");

		$Param = explode(";", $Param);
		$Artikel = array('','');
	}
	else		// Gespeicherte Parameter verwenden
	{
		$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));
	    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	}



	//**********************************************************
	// Daten anzeigen
	//**********************************************************

	if($Param[4]!='' AND $Param[1]!='')		// EAN-Nummern UND ATU-Artikelnummer
	{
		$SQL = "SELECT EANNummern.*, 'EAN' AS FUNDSTELLE ";
		$SQL .= " FROM EANNummern, TeileInfos ";

		$SQL .= " WHERE TEI_ITY_ID2 = 'EAN' AND TEI_KEY2=EAN_KEY ";

		if($Param[3]!='')		// Suchbegriff bei EAN Nummer
		{
			if($Param[7]=='on')
			{
				$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE) . ")";
			}
			else
			{
				$SQL .= " AND TEI_SUCH2 " . awisLIKEoderIST($Param[3], True, False, TRUE) . ")";
			}
		}
		$SQL .= " AND TEI_KEY1 IN (SELECT AST_KEY FROM ArtikelStamm WHERE AST_ATUNR " . awisLIKEoderIST($Param[1],True, False, False) . ") ORDER BY EAN_Nummer";
	}
	elseif($Param[4]!='')				// NUR EAN-Nummer
	{
		$SQL = "SELECT EANNummern.*, 'EAN' AS FUNDSTELLE ";
		$SQL .= " FROM EANNummern";

		$SQL .= " WHERE (";
		if($Param[7]=='on')
		{
			$SQL .= " EAN_Nummer " . awisLIKEoderIST($Param[3], True, False, FALSE) . "";
		}
		else
		{
			$SQL .= " SUCHWORT(EAN_Nummer) " . awisLIKEoderIST($Param[3], True, False, TRUE) . "";
		}
		$SQL .= " )";
	}
	elseif($Param[1]!='')				// NUR Artikel-Nummer
	{
		$SQL = "SELECT EANNummern.*, 'EAN' AS FUNDSTELLE ";
		$SQL .= " FROM EANNummern, TeileInfos ";

		$SQL .= " WHERE (TEI_ITY_ID2 = 'EAN' AND TEI_KEY2=EAN_KEY) ";
		$SQL .= " AND TEI_KEY1 IN (SELECT AST_KEY FROM ArtikelStamm WHERE AST_ATUNR " . awisLIKEoderIST($Param[1],True, False, False) . ") ORDER BY EAN_Nummer";
	}
}


if($SQL == '')
{
	print "<span class=HinweisText>Mit den angegebenen Kriterien konnte leider kein Artikel gefunden werden.</span>";
}
elseif(connection_status()==0)		//Verbindung steht noch
{

	$rsArtikel = awisOpenRecordset($con, $SQL);
	if($rsArtikel==FALSE)
	{
		awisErrorMailLink("artikel_EANArtikel.php", 2, $awisDBFehler);
		awisLogoff($con);
		die();
	}
	$rsArtikelZeilen = $awisRSZeilen;

	if($rsArtikelZeilen > 1)
	{
		print "<table width=300 id=DatenTabelle border=1>";
		print "<tr>";	// �berschrift
		print "<td id=FeldBez>EAN-Nummer</td>";
		print "<td id=FeldBez>Typ</td>";
		print "</tr>";

		for($EANNr=0;$EANNr<$rsArtikelZeilen;$EANNr++)
		{
			print "<tr>";
			print "<td " . (($EANNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
			print "<a href=./artikel_Main.php?cmdAktion=EANArtikel&EANKEY=" . $rsArtikel["EAN_KEY"][$EANNr];
			if($Zeile==0)
			{
				echo ' name=ErsterEintrag ';
			}

			echo  ">" . $rsArtikel["EAN_NUMMER"][$EANNr] . "</a></td>";
			print "<td " . (($EANNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsArtikel["EAN_TYP"][$EANNr] . "</td>";
			print "</tr>";
		}

		print "</table>";
		print "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $EANNr Artikel gefunden.</font>";
	}
	elseif($rsArtikelZeilen==1)
	{
		echo '<table border=1 id=DatenTabelle width=100%>';
		echo '<colspec><col width=120><col width=*></colspec>';
		echo '<tr><td id=FeldBez>EAN-Nummer</td>'	;
		echo '<td>' . $rsArtikel['EAN_NUMMER'][0] . '</td></tr>';

		echo '<tr><td id=FeldBez>Typ</td>'	;
		echo '<td>' . $rsArtikel['EAN_TYP'][0] . '</td></tr>';
		echo '</table>';

		$SQL = "SELECT AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW FROM ArtikelStamm, TEILEINFOS WHERE AST_KEY=TEI_KEY1 AND TEI_KEY2=" . $rsArtikel['EAN_KEY'][0];
		$rsATUArtikel = awisOpenRecordset($con, $SQL);
		$rsATUArtikelZeilen = $awisRSZeilen;

		if($rsATUArtikelZeilen > 0)
		{
			echo '<h4>ATU Artikel</h4>';
			echo '<table border=1 id=DatenTabelle width=100%>';
			echo '<colspec><col width=100><col width=*></colspec>';
			echo "<tr><td id=FeldBez>ATU Nummer</td>";
			echo "<td id=FeldBez>Bezeichnung</td></tr>";
			for($Zeile=0;$Zeile<$rsATUArtikelZeilen;$Zeile++)
			{
				echo '<tr>';
				echo '<td><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&KEY=' . $rsATUArtikel['AST_KEY'][$Zeile] . '>';
				echo '' . $rsATUArtikel['AST_ATUNR'][$Zeile] . '</a></td>';
				echo '<td>' . $rsATUArtikel['AST_BEZEICHNUNGWW'][$Zeile] . '</td>';
				echo '</tr>';
			}
			echo '</table>';
		}
	}
	else
	{
		echo '<span class=HinweisText>Es konnten keine EAN-Nummern mit den Suchbegriffen gefunden werden.</span>';
	}
}

print "</form>";
?>
<SCRIPT type="text/javascript">
	document.getElementsByName('ErsterEintrag')[0].focus();
</SCRIPT>
