<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

$TextKonserven = array();
$TextKonserven[]=array('XBG','%');
$TextKonserven[]=array('XBB','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht1 = $AWISBenutzer->HatDasRecht(1);
	if($Recht1==0)
	{
	    awisEreignis(3,1000,'Benutzergruppen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}


	$Form->Formular_Start();

	$EditRecht=(($Recht1&4)!=0);

	//***********************************
	//Kontakte anzeigen
	//***********************************

	$SQL = 'SELECT Benutzergruppen.*, XBB_BEZ, XBB_BEMERKUNG';
	$SQL .= ' FROM Benutzergruppen ';
	$SQL .= ' INNER JOIN BenutzergruppenBez ON XBG_XBB_KEY = XBB_KEY';
	$SQL .= ' WHERE XBG_XBN_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['XBG_XBB_KEY']))
	{
		$SQL .= ' AND XBG_XBB_KEY=0'.intval($_GET['XBG_XBB_KEY']);
	}
	if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
	{
		$SQL .= ' AND XBG_XBB_KEY=0'.intval($AWIS_KEY2);
	}


	if(!isset($_GET['XBGSort']))
	{
		$SQL .= ' ORDER BY XBG_XBB_KEY';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['XBGSort']);
	}
	$rsXBG = $DB->RecordSetOeffnen($SQL);

	if(!isset($_GET['XBG_XBB_KEY']))					// Liste anzeigen
	{
		$Form->ZeileStart();

		if(($Recht1&6)>0)
		{
			$Icons[] = array('new','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Gruppen&XBG_XBB_KEY=0');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}

		$Link = './benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Gruppen';
		$Link .= '&XBGSort=XBB_BEZ'.((isset($_GET['XBGSort']) AND ($_GET['XBGSort']=='XBB_BEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBB']['XBB_BEZ'],250,'',$Link);

		$Form->ZeileEnde();
		$DS = 1;
		while(!$rsXBG->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht1&6)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Gruppen&XBG_XBB_KEY='.$rsXBG->FeldInhalt('XBG_XBB_KEY'));
				$Icons[] = array('delete','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Gruppen&Del='.$rsXBG->FeldInhalt('XBG_XBB_KEY').'~'.$rsXBG->FeldInhalt('XBG_XBN_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Link = '';
			if($AWISBenutzer->HatDasRecht(25)>0)
			{
				$Link = './gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY=0'.$rsXBG->FeldInhalt('XBG_XBB_KEY');
			}
			$Form->Erstelle_ListenFeld('#XBB_BEZ',$rsXBG->FeldInhalt('XBB_BEZ'),20,250,false,($DS%2),'',$Link,'T');
			$Form->ZeileEnde();

			$rsXBG->DSWeiter();
			$DS++;
		}
		$Form->Formular_Ende();
	}
	else 		// Einer oder keiner
	{
		echo '<input name=txtXBG_XBN_KEY type=hidden value=0'.$AWIS_KEY1.'>';

		$AWIS_KEY2 = $rsXBG->FeldInhalt('XBG_XBB_KEY');
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Gruppen&XBGListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBG->FeldInhalt('XBG_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBG->FeldInhalt('XBG_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBG']['XBG_XBB_KEY'].':',150);
		$SQL = 'SELECT XBB_KEY, XBB_BEZ';
		$SQL .= ' FROM BenutzerGruppenBez';
		$SQL .= ' WHERE ((XBB_SYSTEMGRUPPE IS NULL OR XBB_SYSTEMGRUPPE=\'0\')';
		$SQL .= ' AND (NOT EXISTS(SELECT * FROM Benutzergruppen WHERE XBG_XBB_KEY = XBB_KEY AND XBG_XBN_KEY = 0'.$AWIS_KEY1.'))';
		$SQL .= ' OR XBB_KEY = 0'.$rsXBG->FeldInhalt('XBG_XBB_KEY').')';
		$SQL .= ' ORDER BY XBB_BEZ';

		$Form->Erstelle_SelectFeld('XBG_XBB_KEY',$rsXBG->FeldInhalt('XBG_XBB_KEY'),100,$EditRecht,$SQL);
		$AWISCursorPosition='txtXBG_XBB_KEY';
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (Exception $ex)
{
	if(is_object($Form))
	{
		$Form->Formular_Start();
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'WIEDERHOLEN',0);
		$Form->Formular_Ende();
	}
}
?>