<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('VVS', '*');
    $TextKonserven[] = array('VVE', '*');
    $TextKonserven[] = array('FIL', '*');
    $TextKonserven[] = array('SONDERAKTION', '*');
    $TextKonserven[] = array('V_VERS_FORM_DETAIL', '*');
    $TextKonserven[] = array('V_VERS_FORM_RECHNUNGSDATEN', '*');
    $TextKonserven[] = array('VCB', '*');
    $TextKonserven[] = array('VEB', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if(isset($_POST['cmdSuche_x']))
    {
        $Param['VVS_FILID'] = $_POST['sucVVS_FILID'];
        $Param['VVS_WANR'] = $_POST['sucVVS_WANR'];
        $Param['VVS_KFZKENNZ'] = $_POST['sucVVS_KFZKENNZ'];
        $Param['VVS_VORGANGNR'] = $_POST['sucVVS_VORGANGNR'];
        $Param['VVS_SCHADENKZ'] = $_POST['sucVVS_SCHADENKZ'];

        $Param['FEHLERSTATUS'] = $_POST['sucFEHLERSTATUS'];

        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY'] = '';
        $Param['BLOCK'] = '';
        $Param['KEY'] = 0;

        $AWISBenutzer->ParameterSchreiben("VVSSuche", serialize($Param));
        $AWISBenutzer->ParameterSchreiben("AktuellerVVS", '');
    }
    if(isset($_POST['cmdDSZurueck_x'])) // vorheriger DS
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('VVSSuche'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND VVS_KEY < ' . floatval($_POST['txtVVS_KEY']);

        $SQL = 'SELECT * FROM (SELECT VVS_KEY FROM versvorgangsstatus vvs  ';
        $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
        $SQL .= 'ORDER BY VVS_KEY DESC) WHERE ROWNUM = 1';

        $rsVVS = $DB->RecordSetOeffnen($SQL);
        if($rsVVS->FeldInhalt('VVS_KEY') != '')
        {
            $AWIS_KEY1 = $rsVVS->FeldInhalt('VVS_KEY');
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtVVS_KEY'];
        }
    }
    elseif(isset($_POST['cmdDSWeiter_x'])) // n�chster DS
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('VVSSuche'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND VVS_KEY > ' . floatval($_POST['txtVVS_KEY']);

        $SQL = 'SELECT * FROM (SELECT VVS_KEY FROM versvorgangsstatus vvs ';
        $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
        $SQL .= 'ORDER BY VVS_KEY) WHERE ROWNUM = 1';

        $rsVVS = $DB->RecordSetOeffnen($SQL);

        if($rsVVS->FeldInhalt('VVS_KEY') != '')
        {
            $AWIS_KEY1 = $rsVVS->FeldInhalt('VVS_KEY');
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtVVS_KEY'];
        }
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('versicherungen_Speichern.php');
    }
    elseif(isset($_GET['VVS_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['VVS_KEY']);
        $AWISBenutzer->ParameterSchreiben("AktuellerVVS", $_GET['VVS_KEY']);
    }
    else
    {
        if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']))
        {
            $AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerVVS");
        }
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen("VVSSuche"));

    $Form->DebugAusgabe(1, $_POST);

    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if(!isset($_GET['Sort']))
    {
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY'] != '')
        {
            $ORDERBY = $Param['ORDERBY'];
        }
        else
        {
            $ORDERBY = ' VVS.VVS_KEY';
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    $Bedingung .= ' AND vek.quelle  = \'D\'';

    $SQL = 'SELECT vvs.vvs_key,';
    $SQL .= '     vvs.vvs_filid,';
    $SQL .= '     vvs.vvs_wanr,';
    $SQL .= '     vvs.vvs_versnr,';
    $SQL .= '     vvs.vvs_schadenkz,';
    $SQL .= '     vvs.vvs_laenderschl,';
    $SQL .= '     vvs.vvs_ust_schl,';
    $SQL .= '     vvs.vvs_freigabegrund,';
    $SQL .= '     vvs.vvs_datumexport,';
    $SQL .= '     vvs.vvs_kfzkennz,';
    $SQL .= '     vvs.vvs_vorgangnr,';
    $SQL .= '     vvs.vvs_freigabe,';
    $SQL .= '     vvs.vvs_user,';
    $SQL .= '     vvs.vvs_userdat,';
    $SQL .= '     vek.vers_bez,';
    $SQL .= '     vek.vers_strasse,';
    $SQL .= '     vek.vers_hnr,';
    $SQL .= '     vek.vers_plz,';
    $SQL .= '     vek.vers_ort,';
    $SQL .= '     vek.KFZ_HERSTELLER,';
    $SQL .= '     vek.KFZ_MODELL,';
    $SQL .= '     vek.KFZ_TYP,';
    $SQL .= '     vek.fahrgestellnr,';
    $SQL .= '     vek.KTYPNR,';
    $SQL .= '     f.fil_id,';
    $SQL .= '     f.fil_bez,';
    $SQL .= '     f.fil_strasse,';
    $SQL .= '     f.fil_plz,';
    $SQL .= '     f.fil_ort,';
    $SQL .= '     vve_kuerzel,';
    $SQL .= '     vek.umsatz_vers,';
    $SQL .= '     vvs.vvs_datumerhalten,';
    $SQL .= '     vvs.vvs_datumgdvabgelegt,';
    $SQL .= '     vvs.vvs_ven_key,';
    $SQL .= '     vvs.vvs_vcn_key,';
    $SQL .= '     vvs.VVS_DUBCHECK,';
    $SQL .= '     vcn.vcn_bemerkung,';
    $SQL .= '     ven.ven_bemerkung,';
    $SQL .= '     vcn_kategorie';
    if($AWIS_KEY1 <= 0)
    {
        $SQL .= ',row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr ';
    }
    $SQL .= '   FROM versvorgangsstatus vvs';
    $SQL .= '   INNER JOIN verscheckstatusnummern vcn';
    $SQL .= '   ON vcn.VCN_KEY = vvs.VVS_VCN_KEY';
    $SQL .= '   INNER JOIN vers_exp_kopf vek';
    $SQL .= '   ON vvs.vvs_filid = vek.filid';
    $SQL .= '   AND vvs.vvs_wanr = vek.wanr';
    $SQL .= '   INNER JOIN versversicherungen vvv';
    $SQL .= '   ON vvs_versnr = vvv.vve_versnr';
    $SQL .= '   INNER JOIN filialen f';
    $SQL .= '   ON vvs_filid = f.fil_id';
    $SQL .= '   INNER JOIN versstatusnummern ven';
    $SQL .= '   ON ven.ven_key  = vvs.vvs_ven_key';
    if($Bedingung != '')
    {
        $SQL .= ' WHERE' . substr($Bedingung, 4);
    }
    $SQL .= ' ORDER BY ' . $ORDERBY;

    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if(isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }
    elseif(isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('VVS', false));

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $DB->WertSetzen('VVS', 'N0', $StartZeile) . ' AND  ZeilenNr<' . $DB->WertSetzen('VVS', 'N0', ($StartZeile + $ZeilenProSeite));
    }

    $rsVVS = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS'));

    // Aktuelle Parameter sichern
    $Param['ORDERBY'] = $ORDERBY;
    $Param['KEY'] = $AWIS_KEY1;
    $Param['BLOCK'] = $Block;
    $AWISBenutzer->ParameterSchreiben("VVSSuche", serialize($Param));

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenDetails action=./versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $FeldBreiten = array();
    $FeldBreiten['FILID'] = 60;
    $FeldBreiten['WANR'] = 70;
    $FeldBreiten['VORGANGNR'] = 150;
    $FeldBreiten['VERSICHERUNG'] = 80;
    $FeldBreiten['SCHADENKZ'] = 70;
    $FeldBreiten['BEARBEITER'] = 120;
    $FeldBreiten['USERDAT'] = 160;
    $FeldBreiten['VERARBSTATUS'] = 250;
    $FeldBreiten['FEHLERSTATUS'] = 250;
    $FeldBreiten['UMSATZ_VERS'] = 100;
    // Daten anzeigen
    if($rsVVS->EOF())
    {
        echo '<span class=HinweisText>Es wurden keine Datensaetze gefunden.</span>';
    }
    elseif(($rsVVS->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_FILID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_FILID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_FILIALE'], $FeldBreiten['FILID'], '', $Link);
        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_WANR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_WANR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_WA'], $FeldBreiten['WANR'], '', $Link);
        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_VORGANGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_VORGANGNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_VORGANGNR'], $FeldBreiten['VORGANGNR'], '', $Link);

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVE_Kuerzel' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVE_Kuerzel'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_VERSICHERUNG'], $FeldBreiten['VERSICHERUNG'], '', $Link);
        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_SCHADENKZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_SCHADENKZ'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['TXT_SCHADENKZ'], $FeldBreiten['SCHADENKZ'], '', $Link);

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_USER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_BEARBEITER'], $FeldBreiten['BEARBEITER'], '', $Link);
        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_USERDAT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_USERDAT'], $FeldBreiten['USERDAT'], '', $Link);

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VERARBSTATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VERARBSTATUS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift('Verarb_Status', $FeldBreiten['VERARBSTATUS'], '', $Link);

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=VVS_VEN_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVS_VEN_KEY'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift('Fehlerstatus', $FeldBreiten['FEHLERSTATUS'], '', $Link);

        $Link = './versicherungen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=UMSATZ_VERS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'UMSATZ_VERS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['UMSATZ_VERS'], $FeldBreiten['UMSATZ_VERS'], '', $Link);

        $Form->ZeileEnde();

        $VVSZeile = 0;

        while(!$rsVVS->EOF())
        {
            $Form->ZeileStart();
            $Link = './versicherungen_Main.php?cmdAktion=Details&VVS_KEY=' . $rsVVS->FeldInhalt('VVS_KEY');
            $Form->Erstelle_ListenFeld('VVS_FILID', $rsVVS->FeldInhalt('VVS_FILID'), 0, $FeldBreiten['FILID'], false, ($VVSZeile % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('VVS_WANR', $rsVVS->FeldInhalt('VVS_WANR'), 0, $FeldBreiten['WANR'], false, ($VVSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_VORGANGNR', $rsVVS->FeldInhalt('VVS_VORGANGNR'), 0, $FeldBreiten['VORGANGNR'], false, ($VVSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VVE_KUERZEL', $rsVVS->FeldInhalt('VVE_KUERZEL'), 0, $FeldBreiten['VERSICHERUNG'], false, ($VVSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_SCHADENKZ', $rsVVS->FeldInhalt('VVS_SCHADENKZ'), 0, $FeldBreiten['SCHADENKZ'], false, ($VVSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_USER', ($rsVVS->FeldInhalt('VVS_USER') == ''?'':($rsVVS->FeldInhalt('VVS_USER'))), 0, $FeldBreiten['BEARBEITER'], false, ($VVSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VVS_USERDAT', ($rsVVS->FeldInhalt('VVS_USER') == ''?'':($rsVVS->FeldInhalt('VVS_USERDAT'))), 0, $FeldBreiten['USERDAT'], false, ($VVSZeile % 2), '', '', 'D');

            $TTT = $rsVVS->FeldInhalt('VCN_BEMERKUNG');
            $StatusBeschreibung = $rsVVS->FeldInhalt('VCN_BEMERKUNG');
            $Form->Erstelle_ListenFeld('VERARBSTATUS', strlen($StatusBeschreibung) > 30?substr($StatusBeschreibung, 0, 27) . '...':$StatusBeschreibung, 0, $FeldBreiten['VERARBSTATUS'], false, ($VVSZeile % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsVVS->FeldInhalt('VEN_BEMERKUNG');
            $Fehlerkey = $rsVVS->FeldInhalt('VVS_VEN_KEY');
            $FehlerBeschreibung = $rsVVS->FeldInhalt('VEN_BEMERKUNG');
            $Form->Erstelle_ListenFeld('VVS_VEN_KEY', $Fehlerkey > 0?(strlen($FehlerBeschreibung) > 30?substr($FehlerBeschreibung, 0, 27) . '...':$FehlerBeschreibung):'---', 0, $FeldBreiten['FEHLERSTATUS'], false, ($VVSZeile % 2), '', '', 'T', 'L', $TTT);

            $Form->Erstelle_ListenFeld('UMSATZ_VERS', ($rsVVS->FeldInhalt('UMSATZ_VERS') == ''?'':($rsVVS->FeldInhalt('UMSATZ_VERS')."&euro;")), 0, $FeldBreiten['UMSATZ_VERS'], false, ($VVSZeile % 2), '', '', 'T');

            $Form->ZeileEnde();

            $rsVVS->DSWeiter();
            $VVSZeile++;
        }

        $Link = './versicherungen_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    }
    else
    {
        //Nur ein Datensatz gefunden

        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./versicherungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");

        $Form->Formular_Start();

        // Infozeile zusammenbauen
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVVS->FeldInhalt('VVS_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVVS->FeldInhalt('VVS_USERDAT'));

        $Form->InfoZeile($Felder, '');

        $AWIS_KEY1 = $rsVVS->FeldInhalt('VVS_KEY');

        $AWISBenutzer->ParameterSchreiben("AktuellerVVS", $AWIS_KEY1);

        $EditRecht = $Recht9500 & 2;

        $EditDuplikat = 'Select * from versicherungen_dubletten where vdb_status = 555 and (';
        $EditDuplikat .= ' vdb_vvs_key = ' . $AWIS_KEY1 . ' or VDB_REF = ' . $AWIS_KEY1 . ')';
        $EditDuplikat .= '  and vdb_user <>  ' . $DB->WertSetzen('VDB', 'T', $AWISBenutzer->BenutzerName());

        $FeldBreiten = array();
        $FeldBreiten['Standard'] = 180;
        $FeldBreiten['SelectFeld'] = 180;

        if($DB->ErmittleZeilenAnzahl($EditDuplikat, $DB->Bindevariablen('VDB')) > 0)
        {
            $Form->Hinweistext('<b>ACHTUNG: </b><br> Dieser oder ein anderer aehnlicher Datensatz <br>
            wird gerade von einem anderen Mitarbeiter bearbeitet.
             <br>Bitte versuchen Sie es spaeter erneut.');
            $EditRecht = false;
        }

        $Form->Erstelle_HiddenFeld('VVS_KEY', $AWIS_KEY1);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_VORGANGNR'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VVS_VORGANGNR', $rsVVS->FeldInhalt('VVS_VORGANGNR'), 25, 360, $EditRecht);
        $Form->ZeileEnde();

        $Form->Trennzeile();

        // $Form->ZeileStart();
        // $Form->Erstelle_TextLabel('Filialdaten', $FeldBreiten['Standard'], 'font-weight:bold');
        // $Form->Erstelle_TextFeld('DUMMY', '', 360, 360, false);
        // $Form->Erstelle_TextLabel('Versicherungsdaten', $FeldBreiten['Standard'], 'font-weight:bold');
        // $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_FILID'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VVS_FILID', $rsVVS->FeldInhalt('VVS_FILID'), 360, 360, false);

        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_VERSNR'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VVS_VERSNR', $rsVVS->FeldInhalt('VVS_VERSNR'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_WANR'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VVS_WANR', $rsVVS->FeldInhalt('VVS_WANR'), 360, 360, false);

        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['VERS_BEZ'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VERS_BEZ', $rsVVS->FeldInhalt('VERS_BEZ'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->Trennzeile();

        // $Form->ZeileStart();
        // $Form->Erstelle_TextLabel('Fahrzeugdaten', $FeldBreiten['Standard'], 'font-weight:bold');
        // $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_LAENDERSCHL'] . ':', $FeldBreiten['Standard']);
        $KateogrieAlt = explode("|", $AWISSprachKonserven['VVS']['VVS_LST_LAENDERSCHL']);
        $Form->Erstelle_SelectFeld('~VVS_LAENDERSCHL', $rsVVS->FeldInhalt('VVS_LAENDERSCHL'), 360, $EditRecht, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $KateogrieAlt, '');

        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['KTYP_BEZ'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('KTYP_BEZ', $rsVVS->FeldInhalt('KFZ_TYP'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_KFZKENNZ'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VVS_KFZKENNZ', $rsVVS->FeldInhalt('VVS_KFZKENNZ'), 25, 360, $EditRecht);

        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['FAHR_GESTELL_NR'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('FAHR_GESTELL_NR', $rsVVS->FeldInhalt('FAHRGESTELLNR'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['KFZ_HERST_BEZ'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('KFZ_HERST_BEZ', $rsVVS->FeldInhalt('KFZ_HERSTELLER'), 360, 360, false);

        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['KTYPNR'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('KTYPNR', $rsVVS->FeldInhalt('KTYPNR'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_DETAIL']['KMOD_BEZ'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('KMOD_BEZ', $rsVVS->FeldInhalt('KFZ_MODELL'), 360, 360, false);
        $Form->ZeileEnde();

        $Form->Trennzeile();
        //
        // $Form->ZeileStart();
        // $Form->Erstelle_TextLabel('Infos/Bearbeitung', $FeldBreiten['Standard'], 'font-weight:bold');
        // $Form->ZeileEnde();

        $SperrStatus = $rsVVS->FeldInhalt('VVS_VEN_KEY');
        $Verarbstatus = $rsVVS->FeldInhalt('VVS_VCN_KEY');
        $StatusKategorie = $rsVVS->FeldInhalt('VCN_KATEGORIE');
        $DubCheckKey = $rsVVS->FeldInhalt('VVS_DUBCHECK');

        if($SperrStatus == 29 and $StatusKategorie == 'BEARBEITUNG' and $DubCheckKey == 1)
        {
            $Form->Hinweistext('Bitte diesen Vorgang ueber den Reiter Duplikate bearbeiten');
        }
        else
        {
            if($StatusKategorie != 'VERARBEITET' and ($DubCheckKey == 0 or $DubCheckKey == 3))
            {

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_FREIGABEGRUND'] . ':', $FeldBreiten['Standard']);
                $Form->Erstelle_TextFeld('VVS_FREIGABEGRUND', $rsVVS->FeldInhalt('VVS_FREIGABEGRUND'), 80, 360, $EditRecht);
                $Form->ZeileEnde();

                if($SperrStatus != 0 || ($SperrStatus == 29 && $Verarbstatus <> 124 && $DubCheckKey == 0))
                {
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_FREIGABE'] . ':', $FeldBreiten['Standard']);
                    $KateogrieAlt = explode("|", $AWISSprachKonserven['VVS']['VVS_LST_FREIGABE']);
                    $Form->Erstelle_SelectFeld('VVS_FREIGABE', $rsVVS->FeldInhalt('VVS_FREIGABE'), 100, $EditRecht, '', '', '', '', '', $KateogrieAlt, '');
                    $Form->ZeileEnde();
                }

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['SONDERAKTION']['SON_ZURUECKSETZEN'] . ':', $FeldBreiten['Standard']);
                $KateogrieAlt = explode("|", $AWISSprachKonserven['SONDERAKTION']['SON_LST_ZURUECKSETZEN']);
                $Form->Erstelle_SelectFeld('SON_LST_ZURUECKSETZEN', $rsVVS->FeldInhalt('SON_LST_ZURUECKSETZEN'), 360, $EditRecht, '', '', '', '', '', $KateogrieAlt, '');
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['SONDERAKTION']['SON_NACHGEDRUCKT'] . ':', $FeldBreiten['Standard']);
                $KateogrieAlt = explode("|", $AWISSprachKonserven['SONDERAKTION']['SON_LST_NACHGEDRUCKT']);
                $Form->Erstelle_SelectFeld('SON_LST_NACHGEDRUCKT', $rsVVS->FeldInhalt('SON_LST_NACHGEDRUCKT'), 360, $EditRecht, '', '', '', '', '', $KateogrieAlt, '');
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['SONDERAKTION']['SON_UMGEBUCHT'] . ':', $FeldBreiten['Standard']);
                $KateogrieAlt = explode("|", $AWISSprachKonserven['SONDERAKTION']['SON_LST_UMGEBUCHT']);
                $Form->Erstelle_SelectFeld('SON_LST_UMGEBUCHT', $rsVVS->FeldInhalt('SON_LST_UMGEBUCHT'), 360, $EditRecht, '', '', '', '', '', $KateogrieAlt, '');
                $Form->ZeileEnde();

                $Form->Trennzeile('O');
            }
        }
        $Form->ZeileStart();

        $Form->Erstelle_TextLabel($AWISSprachKonserven['VCB']['VCB_BEZEICHNUNG'] . ':', $FeldBreiten['Standard']);
        $Form->Erstelle_TextFeld('VCN_BEMERKUNG', $rsVVS->FeldInhalt('VCN_BEMERKUNG'), 360, 360, false);

        if($rsVVS->FeldInhalt('VVS_VCN_KEY') == 112)
        {
            $Form->Erstelle_TextFeld('VVS_DATUMEXPORT', $rsVVS->FeldInhalt('VVS_DATUMEXPORT'), 160, 160, false);
        }
        elseif($rsVVS->FeldInhalt('VVS_VCN_KEY') == 125)
        {
            $Form->Erstelle_TextFeld('VVS_DATUMEXPORT', $rsVVS->FeldInhalt('VVS_DATUMEXPORT'), 160, 160, false);
        }
        elseif($rsVVS->FeldInhalt('VVS_VCN_KEY') == 126)
        {
            $Form->Erstelle_TextFeld('VVS_DATUMERHALTEN', $rsVVS->FeldInhalt('VVS_DATUMERHALTEN'), 160, 160, false);
        }
        elseif($rsVVS->FeldInhalt('VVS_DATUMGDVABGELEGT') != '')
        {
            $Form->Erstelle_TextFeld('VVS_DATUMGDVABGELEGT', "Datei auf GDV-Server abgelegt am: " . $rsVVS->FeldInhalt('VVS_DATUMGDVABGELEGT'), 200, 250, false);
        }
        $Form->ZeileEnde();

        if($rsVVS->FeldInhalt('VVS_VEN_KEY') != 0)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['VEB']['VEB_BEZEICHNUNG'] . ':', $FeldBreiten['Standard']);
            $Form->Erstelle_TextFeld('VEN_BEMERKUNG', $rsVVS->FeldInhalt('VEN_BEMERKUNG'), 460, 460, false);
            $Form->ZeileEnde();
        }

        $Form->Trennzeile('O');

        if($AWIS_KEY1 != 0)
        {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));

            if(!$EditRecht)
            {
                $SubReg = new awisRegister(9503);
            }
            else
            {
                $SubReg = new awisRegister(9501);
            }

            if($rsVVS->FeldInhalt('VVS_VORGANGNR') != '')
            {
                $SubReg->ZeichneRegister($RegisterSeite);
            }
            else
            {
                //Wenn die 13-stellige Autoglasnummer nicht gefuellt ist (kein Scheibentausch)
                //blende das Subregister aus
                $SubReg->ZeichneRegister($RegisterSeite, 'AutoglasVorgang');
            }
        }
    }

    $Form->Formular_Ende();
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if(($rsVVS->AnzahlDatensaetze() == 1) and isset($EditRecht))
    {
        if(($Recht9500 & 2) == 2 and $RegisterSeite != 'Duplikate')
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
    }

    if(($rsVVS->AnzahlDatensaetze() == 1))
    {
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
        $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
    }
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        //$Form->Fehler _Anzeigen('INTERN',$ex->getMessage('MELDEN'),6,"200905131735");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function _BedingungErstellen($ArrParam)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if(floatval($AWIS_KEY1) != 0)
    {
        $Bedingung .= ' AND vvs.VVS_KEY = ' . $DB->WertSetzen('VVS', 'N0', $AWIS_KEY1);

        return $Bedingung;
    }

    if(isset($ArrParam['VVS_FILID']) AND $ArrParam['VVS_FILID'] != '')
    {
        $Bedingung = " AND vvs.vvs_filid = " . $DB->WertSetzen('VVS', 'N0', $ArrParam['VVS_FILID']);
    }
    if(isset($ArrParam['VVS_WANR']) AND $ArrParam['VVS_WANR'] != '')
    {
        $Bedingung .= " AND vvs.vvs_wanr = " . $DB->WertSetzen('VVS', 'N0', $ArrParam['VVS_WANR']);
    }
    if(isset($ArrParam['VVS_KFZKENNZ']) AND $ArrParam['VVS_KFZKENNZ'] != '')
    {
        $Bedingung .= " AND vvs.vvs_kfzkennz = " . $DB->WertSetzen('VVS', 'T', $ArrParam['VVS_KFZKENNZ']) . " ";
    }
    if(isset($ArrParam['VVS_VORGANGNR']) AND $ArrParam['VVS_VORGANGNR'] != '')
    {
        $Bedingung .= " AND vvs.vvs_vorgangnr =  " . $DB->WertSetzen('VVS', 'T', $ArrParam['VVS_VORGANGNR']) . " ";
    }
    if(isset($ArrParam['VVS_SCHADENKZ']) AND $ArrParam['VVS_SCHADENKZ'] != '')
    {
        $Bedingung .= " AND vvs.vvs_schadenkz =  " . $DB->WertSetzen('VVS', 'T', $ArrParam['VVS_SCHADENKZ']) . " ";
    }

    if(isset($ArrParam['FEHLERSTATUS']) AND $ArrParam['FEHLERSTATUS'] != '')
    {
        //Option fuer die Anzeige, aller fehlerhaften Vorgaenge
        if($ArrParam['FEHLERSTATUS'] == '-1')
        {
            $Bedingung .= " AND vvs.vvs_ven_key <> 0 AND vvs.vvs_freigabe = 0";
            $Bedingung .= " AND vcn.vcn_kategorie = 'BEARBEITUNG'";
        }
        //Option fuer die Anzeige, keiner fehlerhaften Vorgaenge
        elseif($ArrParam['FEHLERSTATUS'] == '-99')
        {
            $Bedingung .= " AND vvs.vvs_ven_key = 0";
            $Bedingung .= " AND vcn.vcn_kategorie = 'BEARBEITUNG'";
        }
        else
        {
            $Bedingung .= " AND vvs.vvs_ven_key = " . $DB->WertSetzen('VVS', 'N0', $ArrParam['FEHLERSTATUS']);
            $Bedingung .= " AND vcn.vcn_kategorie = 'BEARBEITUNG'";
        }
    }

    return $Bedingung;
}

?>