<?php
require_once 'awisWerkzeuge.inc';
require_once 'awisRest.inc';
require_once 'awisFormular.inc';
class awisPlanningReceiverAPI
{
    const URL_AWIS_SYNC = 'https://dsp.atu.de/s088-planning-receiver/v1/sync/awis';
    const URL_AWIS_SYNC_TEST = 'https://dst.atu.de/s088-planning-receiver/v1/sync/awis';

    private $_URL_AWIS_SYNC = '';
    private $_awisRest;
    private $_awisWerkzeuge;
    private $_Form;

    function __construct()
    {
        $this->_awisRest = new awisRest('awisPlaningReceiverAPI');
        $this->_awisWerkzeuge = new awisWerkzeuge();
        $this->_Form = new awisFormular();
        $this->initUrls();
    }

    private function initUrls(){
        $Level = $this->_awisWerkzeuge->awisLevel();

        //Wenn nicht Prod
        if($Level != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV and $Level != awisWerkzeuge::AWIS_LEVEL_SHUTTLE){
            $this->_URL_AWIS_SYNC = self::URL_AWIS_SYNC_TEST;
        }else{
            $this->_URL_AWIS_SYNC = self::URL_AWIS_SYNC;
        }
    }

    public function SyncRequest($PersNrn, $ClientNo, $DatumVon, $DatumBis){
        $Parameter = array();
        $Parameter['employeeIds'] = $PersNrn;
        $Parameter['dateFrom'] = $this->DatumsFormat($DatumVon);
        $Parameter['dateTo'] = $this->DatumsFormat($DatumBis);
        $Parameter['clientNo'] = $ClientNo;

        $Erg = $this->_awisRest->erstelleRequest($this->_URL_AWIS_SYNC, 'JSONPOST', 'JSON', $Parameter);

        if(isset($Erg['success']) and $Erg['success'] == true){
            return true;
        }

        return false;
    }

    private function DatumsFormat($Datum){
        $Formatiert = $this->_Form->Format('DI',$Datum);

        if($Formatiert and strpos($Formatiert,'T')){
            return explode('T',$Formatiert)[0];
        }

        return $Datum;
    }

}