<?php

// Variablen
global $_POST;
global $_GET;
global $awisDBFehler;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 1301);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$_SERVER['PHP_AUTH_USER'],'','','');
   die("Keine ausreichenden Rechte!");
}

$Params = explode(";",awis_BenutzerParameter($con, "HBST_Suche", $_SERVER['PHP_AUTH_USER']));


/*************************************************
* Daten speichern
*************************************************/

if(isset($_POST['cmdSpeichern_x']))
{
	if($_POST['txtMIT_KEY']>0)		// �ndern
	{
		$SQL = '';
		if($_POST['txtMIT_BEZEICHNUNG'] != $_POST['txtMIT_BEZEICHNUNG_old'])
		{
			$SQL .= ", MIT_BEZEICHNUNG='" . $_POST['txtMIT_BEZEICHNUNG'] . "'";
		}
		if($_POST['txtMIT_PER_NR'] != $_POST['txtMIT_PER_NR_old'])
		{
			$SQL .= ", MIT_PER_NR='" . $_POST['txtMIT_PER_NR'] . "'";
		}
		if($_POST['txtMIT_KON_KEY'] != $_POST['txtMIT_KON_KEY_old'])
		{
			$SQL .= ", MIT_KON_KEY='" . $_POST['txtMIT_KON_KEY'] . "'";
		}
		if($_POST['txtMIT_REZ_ID'] != $_POST['txtMIT_REZ_ID_old'])
		{
			$SQL .= ", MIT_REZ_ID='" . $_POST['txtMIT_REZ_ID'] . "'";
		}
		if($_POST['txtMIT_STATUS'] != $_POST['txtMIT_STATUS_old'])
		{
			$SQL .= ", MIT_STATUS='" . $_POST['txtMIT_STATUS'] . "'";
		}
	
		if($SQL!='')
		{
			$SQL .= ", MIT_USER='" . $_SERVER['PHP_AUTH_USER'] . "', MIT_USERDAT=SYSDATE";
			$SQL = 'UPDATE Mitarbeiter SET ' . substr($SQL,2);	
			$SQL .= ' WHERE MIT_KEY=0' . $_POST['txtMIT_KEY'];

			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Mitarbeiter.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
		}		
	}
	elseif($_POST['txtMIT_KEY']==0)		// �ndern
	{
		$SQL = 'INSERT INTO AWIS.MITARBEITER (MIT_BEZEICHNUNG, MIT_PER_NR, MIT_KON_KEY, MIT_STATUS, MIT_REZ_ID, MIT_USER, MIT_USERDAT)';
		$SQL .= 'VALUES (';
		$SQL .= "'" . $_POST['txtMIT_BEZEICHNUNG'] . "'";
		$SQL .= ",'" . $_POST['txtMIT_PER_NR'] . "'";
		$SQL .= "," . $_POST['txtMIT_KON_KEY'] . "";
		$SQL .= ",'A'";		
		$SQL .= ", '" . $_POST['txtMIT_REZ_ID'] . "'";
		$SQL .= ",'" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE)";				

		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("perseins_Mitarbeiter.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
		
	}

}

/*************************************************
* Daten anzeigen
*************************************************/

echo "<form name=frmMitarbeiter method=post action=./perseins_Main.php?cmdAktion=Mitarbeiter>";

								//****************************************
if(isset($_GET['MIT_KEY']))		// Bearbeitung eines Datensatzes
								//****************************************
{
	$rsMIT = awisOpenRecordset($con, 'SELECT * FROM Mitarbeiter WHERE MIT_KEY=0' . $_GET['MIT_KEY']);

	echo "<input type=hidden name=txtMIT_KEY value='" . $rsMIT['MIT_KEY'][0] . "'>";

	echo '<table Class=DatenTabelle width=100% border=1>';

	echo '<tr>';
	echo '<td Class=FeldBez>Bezeichnung</td>';
	echo '<td>';
	echo "<input type=hidden name=txtMIT_BEZEICHNUNG_old value='" . $rsMIT['MIT_BEZEICHNUNG'][0] . "'>";
	echo "<input type=text size=30 name=txtMIT_BEZEICHNUNG value='" . $rsMIT['MIT_BEZEICHNUNG'][0] . "'>";
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td Class=FeldBez>Pers-Nr.</td>';
	echo '<td>';
	echo "<input type=hidden name=txtMIT_PER_NR_old value='" . $rsMIT['MIT_PER_NR'][0] . "'>";
	echo "<input type=text size=30 name=txtMIT_PER_NR value='" . $rsMIT['MIT_PER_NR'][0] . "'>";
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td Class=FeldBez>Kontakte</td>';
	echo '<td>';
	echo "<input type=hidden name=txtMIT_KON_KEY_old value='" . $rsMIT['MIT_KON_KEY'][0] . "'>";
	$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KAB_ABTEILUNG AS KONTAKT FROM AWIS.KONTAKTE INNER JOIN  KONTAKTEABTEILUNGEN ON KON_KAB_KEY = KAB_KEY WHERE KON_KKA_KEY=1 AND KON_KAB_KEY IN (" . awis_BenutzerParameter($con, 'Mitarbeiter_KontaktAbteilungen', $_SERVER['PHP_AUTH_USER']) . ") ORDER BY KON_NAME1, KON_NAME2");
	$rsKontaktZeilen = $awisRSZeilen;
    echo "<select name=txtMIT_KON_KEY><option value=0>Kontakt ausw�hlen...</option>";
    for($i=0; $i<$rsKontaktZeilen;$i++)
	{
    	print "<option ";
		if($rsKontakt["KON_KEY"][$i] == $rsMIT['MIT_KON_KEY'][0])
		{
			print " selected ";
		}
		print "value=" . $rsKontakt["KON_KEY"][$i] . ">". $rsKontakt["KONTAKT"][$i] . "</option>";
    }
    echo "</select></td></tr>";

	echo '<tr>';
	echo '<td Class=FeldBez>Status</td>';
	echo '<td>';
	echo '<input type=hidden name=txtMIT_STATUS_old value=' . $rsMIT['MIT_STATUS'][0] . '>';
    echo "<select name=txtMIT_STATUS>";
	echo '<option value=A ' . ($rsMIT['MIT_STATUS'][0]=='A'?' selected ':'') . '>Aktiv</option>';
	echo '<option value=S ' . ($rsMIT['MIT_STATUS'][0]=='S'?' selected ':'') . '>Stillgelegt</option>';	
    echo "</select></td></tr>";

	echo '<tr>';
	echo '<td Class=FeldBez>Regionalzentrum</td>';
	echo '<td>';
	echo "<input type=hidden name=txtMIT_REZ_ID_old value='" . $rsMIT['MIT__REZ_ID'][0] . "'>";
	$rsREZ = awisOpenRecordset($con,"SELECT * FROM Regionalzentren ORDER BY REZ_BEZEICHNUNG");
	$rsREZZeilen = $awisRSZeilen;
    echo "<select name=txtMIT_REZ_ID><option value=0>Regionalzentrum ausw�hlen...</option>";
    for($i=0; $i<$rsREZZeilen;$i++)
	{
    	print "<option ";
		if($rsREZ["REZ_ID"][$i] == $rsMIT['MIT_REZ_ID'][0])
		{
			print " selected ";
		}
		print "value=" . $rsREZ["REZ_ID"][$i] . ">". $rsREZ["REZ_BEZEICHNUNG"][$i] . "</option>";
    }
    echo "</select></td></tr>";
	
	echo '<tr><td colspan=2>';
	echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '</td></tr>';
	
}
			//****************************
else		// Liste anzeigen
			//****************************
{
	echo '<table Class=DatenTabelle width=100% border=1>';
	echo '<tr>';
	if(($RechteStufe&2)==2 OR ($RechteStufe&4)==4)	// �ndern oder Hinzuf�gen?
	{
		echo '<td Class=FeldBez>&nbsp;</td>';
	}
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Mitarbeiter&Sort=MIT_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Mitarbeiter&Sort=MIT_PER_NR>Pers-Nr</a></td>';
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Mitarbeiter&Sort=REZ_BEZEICHNUNG,MIT_BEZEICHNUNG>Regionalzentrum</a></td>';
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Mitarbeiter&Sort=KON_NAME1,KON_NAME2>Kontaktname</a></td>';
	echo '</tr>';

		// Neue Datens�tze anf�gen?
	echo '<tr><td>';
	echo "<input type=hidden name=txtMIT_KEY value=0>";
	echo "<input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '</td>';

	echo "<td><input type=text size=30 name=txtMIT_BEZEICHNUNG value=''></td>";
	echo "<td><input type=text size=30 name=txtMIT_PER_NR value=''></td>";

	$rsREZ = awisOpenRecordset($con,"SELECT * FROM Regionalzentren ORDER BY REZ_BEZEICHNUNG");
	$rsREZZeilen = $awisRSZeilen;
    echo "<td><select name=txtMIT_REZ_ID><option value=0>Regionalzentrum ausw�hlen...</option>";
    for($i=0; $i<$rsREZZeilen;$i++)
	{
    	print "<option ";
		if($rsREZ["REZ_ID"][$i] == $rsMIT['MIT_REZ_ID'][0])
		{
			print " selected ";
		}
		print "value=" . $rsREZ["REZ_ID"][$i] . ">". $rsREZ["REZ_BEZEICHNUNG"][$i] . "</option>";
    }
    echo "</select></td>";


	echo "<td><input type=hidden name=txtMIT_KON_KEY_old value='" . $rsMIT['MIT_KON_KEY'][0] . "'>";
	$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KAB_ABTEILUNG AS KONTAKT FROM AWIS.KONTAKTE INNER JOIN  KONTAKTEABTEILUNGEN ON KON_KAB_KEY = KAB_KEY WHERE KON_KKA_KEY=1 AND KON_KAB_KEY IN (" . awis_BenutzerParameter($con, 'Mitarbeiter_KontaktAbteilungen', $_SERVER['PHP_AUTH_USER']) . ") ORDER BY KON_NAME1, KON_NAME2");
	$rsKontaktZeilen = $awisRSZeilen;
    echo "<select name=txtMIT_KON_KEY><option value=0>Kontakt ausw�hlen...</option>";
    for($i=0; $i<$rsKontaktZeilen;$i++)
	{
    	print "<option ";
		if($rsKontakt["KON_KEY"][$i] == $rsMIT['MIT_KON_KEY'][0])
		{
			print " selected ";
		}
		print "value=" . $rsKontakt["KON_KEY"][$i] . ">". $rsKontakt["KONTAKT"][$i] . "</option>";
    }
    echo "</select></td>";
	
	echo '</tr>';
	
	
		// Ende neuer Datensatz
		// TODO: Felder ausw�hlen
	$SQL = 'SELECT * ';
	$SQL .= ' FROM Mitarbeiter LEFT OUTER JOIN Kontakte ON MIT_KON_KEY = KON_KEY INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';
	if(isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ' . $_GET['Sort'];
	}
	else
	{
		$SQL .= ' ORDER BY MIT_BEZEICHNUNG';
	}
	$rsMIT = awisOpenRecordset($con, $SQL);
	$rsMITZeilen = $awisRSZeilen;
	
	for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
	{
		echo '<tr>';
	
		if(($RechteStufe&2)==2)
		{
			echo '<td><a href=./perseins_Main.php?cmdAktion=Mitarbeiter&MIT_KEY=' . $rsMIT['MIT_KEY'][$MITZeile] . '><img src=/bilder/aendern.png border=0></a>';
		}
		echo '<td>' . $rsMIT['MIT_BEZEICHNUNG'][$MITZeile] . '</td>';
		echo '<td>' . $rsMIT['MIT_PER_NR'][$MITZeile] . '</td>';
		echo '<td>' . $rsMIT['REZ_BEZEICHNUNG'][$MITZeile] . '</td>';
		echo '<td>' . $rsMIT['KON_NAME1'][$MITZeile] . ', ' . $rsMIT['KON_NAME2'][$MITZeile] . '</td>';
		
		echo '</tr>';
	
	}
	
	echo '</table>';
}

echo '</form>';


/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
