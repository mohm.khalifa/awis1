<?php
require_once('awisReifenAX.php');

try {

    $Benutzer = '';
    $DebugLevel = 0;
    $zeit = true;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-99).' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $rei = new awisReifenAX($Benutzer, $DebugLevel);
    $rei->exportReifenDaten();

} catch (Exception $ex) {
    echo PHP_EOL . 'Reifen Export: Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'ReifenExportAX-Fehler', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>