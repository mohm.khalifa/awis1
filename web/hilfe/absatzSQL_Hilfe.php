<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>

<title>OSUMME - Hilfe</title>
</head>

<body>

<h2>OSUMMEN Scripte</h2>

<h3>Allgemeines</h3>

Mit Hilfe des Moduls k�nnen SQL Abfragen gespeichert werden, die jede Nacht automatisch ausgef�hrt
werden. In den SQL Abfragen k�nnen SELECT Anweisungen gespeichert werden. Eine UPDATE oder DELETE
Anweisung ist nicht zul�ssig. Ausserdem k�nnen keine DDL und DCL Befehle verwendet werden.

<h3>Bearbeiten von Dateien</h3>

Um eine Datei zu bearbeiten, kann der Link in der �bersicht angeklickt werden. Anschlie�end kann in das
Editorfenster eine SQL Anweisung kopiert werden oder direkt eine Eingabe erfolgen.<br><br>
Es stehen drei Funktionen zur Verf�gung:<br>
<table>
<tr>
<td>Symbol</td><td>Aktion</td>
</tr>

<tr><td><img src="../bilder/diskette.png"> </td>
<td>Datei abspeichern.</td></tr>

<tr><td><img src="../bilder/muelleimer.png"> </td>
<td>Datei l�schen.</td></tr>

<tr><td><img src="../bilder/ausfuehren.png"> </td>
<td>Datei sofort ausf�hren.</td></tr>

</tr>
</table>

<h3>Start der Dateien</h3>

Um eine SQL Datei zu starten, muss sie lediglich erzeugt werden. Jede Nacht werden die Dateien ausgef�hrt
und die Ergebnisse in Reportdateien zur Verf�gung gestellt: <a href=\\ATWS01NT40\PCORGA\EDV\Listen>\\ATWS01NT40\PCORGA\EDV\Listen</a>.<br>
<br>
In der �bersichtsseite wird der Status der Dateien gekennzeichnet. Es wird dabei zwischen zwei m�glichen Werten
unterschieden: <k>L�uft</k> (die Datei wird demn�chst ausgef�hrt) und <k>Jede Nacht</k> (wird jede Nacht ausgef�hrt).

</body>

</html>
