<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisCSVSpooler.inc';

class awisTecdocExport
{
    private $DB;

    private $AWISBenutzer;

    private $DebugLevel;

    public function __construct($Benutzer)
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
    }

    public function ExportArtikeldaten($ExportDateiPfad, $Trennzeichen = ';', $LineFeed = PHP_EOL, $Ueberschriften = true, $NurSKArtikel = false)
    {
        $this->Log('Hole Artikeldaten aus Datenbank.', 1);

        $SQL  ='SELECT DISTINCT';
        $SQL .='     ast.ast_atunr     AS ast_atunr,';
        $SQL .='     lar_lartnr        AS lar_lartnr,';
        $SQL .='     lar_lie_nr        AS lie_nr,';
        $SQL .='     tecdoc.lin_wert   AS tecdoc_einspeiser,';
        $SQL .='     CASE';
        $SQL .='         WHEN lar_lie_nr = to_number(hl.asi_wert) THEN';
        $SQL .='             1';
        $SQL .='         ELSE';
        $SQL .='             0';
        $SQL .='     END AS artikel_hauptlieferant';
        $SQL .=' FROM';
        $SQL .='     lieferantenartikel   lar';
        $SQL .='     INNER JOIN lieferanteninfos     tecdoc ON lin_ity_key = 317';
        $SQL .='                                           AND lin_lie_nr = lar.lar_lie_nr';
        $SQL .='     INNER JOIN teileinfos           tei ON tei.tei_key2 = lar_key';
        $SQL .='                                  AND tei.tei_ity_id2 = \'LAR\'';
        $SQL .='     INNER JOIN artikelstamm         ast ON ast.ast_key = tei.TEI_KEY1';
        $SQL .='                                    AND tei.tei_ity_id1 = \'AST\'';
        $SQL .='     INNER JOIN artikelstamminfos    hl ';
        $SQL .='                 on hl.asi_ait_id = 40';
        $SQL .='                                        AND asi_ast_atunr = ast_atunr';
        if(!$NurSKArtikel){
            $SQL .= ' WHERE';
            $SQL .= '     COALESCE((';
            $SQL .= '         SELECT';
            $SQL .= '             SKEXPORT.TII_WERT';
            $SQL .= '         FROM';
            $SQL .= '             TEILEINFOSINFOS SKEXPORT WHERE';
            $SQL .= '             SKEXPORT.TII_TEI_KEY = TEI_KEY';
            $SQL .= '             AND SKEXPORT.TII_ITY_KEY = 318';
            $SQL .= '             AND ROWNUM = 1 ), \'0\') = \'1\' ';
        }
        $SQL .=' ORDER by';
        $SQL .='     ast_atunr';
        $CSVSpooler = new awisCSVSpooler($this->DB);

        $TmpDatei = $ExportDateiPfad . '.tmp';
        if (file_exists($TmpDatei)) {
            unlink($TmpDatei);
        }

        $CSVSpooler->Trennzeichen($Trennzeichen);
        $CSVSpooler->LineFeed($LineFeed);
        $CSVSpooler->StarteExport($SQL, $TmpDatei, [], $Ueberschriften, false);

        $this->Log('Stelle Datei bereit..', 1);
        if (is_file($ExportDateiPfad)) {
            $this->Log('Alte Datei vorhanden, l�sche..', 1);
            unlink($ExportDateiPfad);
        }

        $this->Log('Bennene Tempdatei um.', 1);
        rename($TmpDatei, $ExportDateiPfad);
        $this->Log('Habe alles erledigt.', 1);
    }

    public function ExportMapping($ExportDateiPfad, $Trennzeichen = ';', $LineFeed = PHP_EOL, $Ueberschriften = true)
    {
        $this->Log('Hole Tecdocdaten aus Datenbank.', 1);

        $SQL = 'select';
        $SQL .= '     LIE_NR,';
        $SQL .= '     LIE_NAME1,';
        $SQL .= '     LIN_WERT as TEC_DOC_ZUORDNUNG';
        $SQL .= ' from';
        $SQL .= '     LIEFERANTEN left';
        $SQL .= '     join LIEFERANTENINFOS on LIE_NR = LIEFERANTENINFOS.LIN_LIE_NR';
        $SQL .= '                              and LIEFERANTENINFOS.LIN_ITY_KEY = 317';
        $SQL .= ' where LIN_WERT is not null';
        $SQL .= ' order by';
        $SQL .= '     1 asc';

        $rsLIE = $this->DB->RecordSetOeffnen($SQL);

        $this->Log('Tecdocdaten geladen. Beginne Export.', 1);

        $TmpDatei = $ExportDateiPfad . '.tmp';
        if (file_exists($TmpDatei)) {
            unlink($TmpDatei);
        }

        $ExpDatei = fopen($TmpDatei, 'w');

        if ($Ueberschriften) {
            $this->Log('Schreibe Ueberschriften', 1);
            $Ueberschriften = array();
            $Ueberschriften[] = 'LIE_NR';
            $Ueberschriften[] = 'LIE_NAME1';
            $Ueberschriften[] = 'TEC_DOC_ZUORDNUNG';

            fwrite($ExpDatei, implode($Trennzeichen, $Ueberschriften) . $LineFeed);
        }

        while (!$rsLIE->EOF()) {

            $Zeile = array();
            $Zeile[] = $rsLIE->FeldInhalt('LIE_NR');
            $Zeile[] = $rsLIE->FeldInhalt('LIE_NAME1');
            $Zeile[] = $rsLIE->FeldInhalt('TEC_DOC_ZUORDNUNG');

            fwrite($ExpDatei, implode($Trennzeichen, $Zeile) . $LineFeed);
            $rsLIE->DSWeiter();
        }

        $this->Log('Stelle Datei bereit..', 1);
        if (is_file($ExportDateiPfad)) {
            $this->Log('Alte Datei vorhanden, l�sche..', 1);
            unlink($ExportDateiPfad);
        }
        $this->Log('Bennene Tempdatei um.', 1);
        rename($TmpDatei, $ExportDateiPfad);
        $this->Log('Habe alles erledigt.', 1);
    }

    public function DebugLevel($Level)
    {
        $this->DebugLevel = $Level;
    }

    public function Log($Text, $Level)
    {
        if ($this->DebugLevel >= $Level) {
            echo date('c') . ' ' . $Text . PHP_EOL;
        }
    }

}