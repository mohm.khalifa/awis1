<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FilialeGeschlossen;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');
require_once('filialinfos_funktionen.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('DPR','%');
	$TextKonserven[]=array('DPF','%');
	$TextKonserven[]=array('Filialinfo','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Stundensatz');
	$TextKonserven[]=array('Wort','Einbaupreisliste');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);
	if(($Recht5001&128)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILDPR'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	$Form->Formular_Start();

	$PEIKey = new awisFilialen($AWIS_KEY1);
	FilialInfos('PREISE', $PEIKey,$FilialeGeschlossen);
	
	//********************************************************
	//* Preise f�r Dienstleistungen
	//********************************************************
	switch ($PEIKey->FilialInfo('FIL_LAN_WWSKENN'))
	{
		case 'BRD':
			$Aktiv = 'DPR_AKTIV1';
			break;
		case 'CZE':
			$Aktiv = 'DPR_AKTIV3';
			break;
		case 'ITA':
			$Aktiv = 'DPR_AKTIV5';
			break;
		case 'NED':
			$Aktiv = 'DPR_AKTIV4';
			break;
		case 'OES':
			$Aktiv = 'DPR_AKTIV2';
			break;
		case 'SUI':
			$Aktiv = 'DPR_AKTIV6';
			break;
		default:
			$Aktiv = '';
	}
	
	//Stundensatz
	$SQL = 'SELECT FIF_WERT, STS_SATZ ';
	$SQL .= ' FROM FILIALINFOS';
	//$SQL .= ' LEFT JOIN STUNDENSAETZE ON to_number(STS_STDVERKZ) = to_number(FIF_WERT) AND STS_LAN_WWSKENN = '.$DB->FeldInhaltFormat('T',$Filiale->FilialInfo('FIL_LAN_WWSKENN'));
	$SQL .= ' LEFT JOIN STUNDENSAETZE ON STS_STDVERKZ = FIF_WERT AND STS_LAN_WWSKENN = '.$DB->FeldInhaltFormat('T',$PEIKey->FilialInfo('FIL_LAN_WWSKENN'));
	$SQL .= ' WHERE FIF_FIL_ID = '.$AWIS_KEY1.' AND FIF_FIT_ID = 205';
$Form->DebugAusgabe(1,$SQL);
	$rsSTSatz = $DB->RecordsetOeffnen($SQL);

	if ($PEIKey->FilialInfo('FIL_LAN_WWSKENN') != 'CZE')
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Stundensatz'].':',250);
		$Form->Erstelle_TextFeld('#STDSATZ',$rsSTSatz->FeldInhalt('STS_SATZ'),0,200,false,'','','','N2');
		$Form->ZeileEnde();
	}
	
	//bei CZE Einbaupreisliste anzeigen
	if ($PEIKey->FilialInfo('FIL_LAN_WWSKENN') == 'CZE')
	{
		$Land = '';
		switch ($PEIKey->FilialInfo('FIL_LAN_WWSKENN'))
		{
			case 'CZE':
					$Land = '40_CZ';
					break;
			default:
					$Land = '';
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Einbaupreisliste'].':',250);		
		$Link='/dokumentanzeigen.php?bereich=einbaupreislisten&land='.$Land.'&dateiname=einbaupreislisten&erweiterung=pdf';
		$Form->Erstelle_Bild('href','*Einbaupreisliste',$Link,'/bilder/icon_pdf.png');		
		$Form->ZeileEnde();
	}
	
	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
		
	$FilUser=false;
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FilUser = true;
		$FIL_ID=$FilZugriff;		
	}

	$Recht5009 = $AWISBenutzer->HatDasRecht(5009);
	if((($Recht5009&32)==0 AND $FilUser == false) OR 
	   (($Recht5009&64)==64) OR 
	   (($Recht5009&32)==0 AND $FilUser == true AND $AWIS_KEY1 == $FIL_ID))
	{
			$STSatz = $DB->FeldInhaltFormat('N2',$rsSTSatz->FeldInhalt('STS_SATZ'));
	
			//*********************************************************
			//* Sortierung
			//*********************************************************
			if(!isset($_GET['SSort']))
			{
				if(isset($Param['ORDER']) AND $Param['ORDER']!='')
				{
					$ORDERBY = $Param['ORDER'];
				}
				else
				{
					$ORDERBY = ' ORDER BY DPR_NUMMER';
				}
			}
			else
			{
				$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
				$Param['ORDER']=$ORDERBY;
			}
	
			$SQL = 'SELECT DPR_NUMMER, COALESCE(ASP_BEZEICHNUNG,DPR_BEZEICHNUNG) AS DPR_BEZEICHNUNG, DPR_VK, DPR_KENN, DPR_PREISGRUPPE, ';
			$SQL.= 'DPF_FIL_ID, DPF_VK, PSG_PREISGRUPPE, PSG_WERT, ';
			$SQL.= 'case when DPR_PREISGRUPPE = 0 then DPR_VK else round(round(PSG_WERT * '. $STSatz .',2),1) end as STSATZ ';
			$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
			$SQL .= ' FROM Dienstleistungspreise';
			$SQL .= ' INNER JOIN DienstleistungspreiseFilialen ON DPF_DPR_NUMMER = DPR_NUMMER AND DPF_FIL_ID ='.$AWIS_KEY1;
			$SQL .= ' LEFT JOIN PreisGruppen ON PSG_PREISGRUPPE = DPR_PREISGRUPPE AND PSG_LAN_WWSKENN =\''.$PEIKey->FilialInfo('FIL_LAN_WWSKENN').'\'';
			$SQL .= ' LEFT OUTER JOIN Artikelsprachen ON DPR_NUMMER = ASP_AST_ATUNR AND ASP_LAN_CODE =\''.$PEIKey->FilialInfo('FIL_LAN_WWSKENN').'\'';
			$SQL .= ' WHERE DPR_PREISGRUPPE <> \'0\' and DPR_KENN <> \'S\'';
			if (isset($Aktiv) and $Aktiv!='')
			{
				$SQL .= ' AND '.$Aktiv.' = \'J\'';
			}
			//$Form->DebugAusgabe(1,$SQL);	
			if($AWIS_KEY2<=0)
			{
				// Zum Bl�ttern in den Daten
				$Block = 1;
				if(isset($_REQUEST['Block']))
				{
					$Block=$Form->Format('N0',$_REQUEST['Block'],false);
					$Param['BLOCK']=$Block;
					$AWISBenutzer->ParameterSchreiben('Formular_FILDPR',serialize($Param));
				}
				elseif(isset($Param['BLOCK']) AND $Param['FIL_ID']==$AWIS_KEY1)
				{
				    //PG: Fehlerbehebung: Wenn im Benutzerparameter ein block Stand
				    //    Der nicht mehr existend ist, wurde nichts angezeigt. Nun
				    //    wird pauschal beim Maskeladen die Seite/Block 1 geladen
				    //    wenn man aber per POST oder GET kommt, dann funktioniert das 
				    //    Seitenwechseln nat�rlich. 
				    
					//$Block=$Param['BLOCK'];
					$Block=1;
				}
				
			
				$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
				if($Block==0)
				{
					$Block=1;
				}
				$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
				//var_dump($SQL);
				$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
				$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
			}
			else
			{
				$MaxDS = 1;
				$ZeilenProSeite=1;
				$Block = 1;
			}
	
			$SQL .= $ORDERBY;
	
			$Form->DebugAusgabe(1,$SQL, $Param);
			$rsDPR = $DB->RecordSetOeffnen($SQL);
	
			$Param['FIL_ID']=$AWIS_KEY1;
			$AWISBenutzer->ParameterSchreiben('Formular_FILDPR',serialize($Param));
				
			if($rsDPR->AnzahlDatensaetze()>=1 OR isset($_GET['ListeDPR']))						// Liste anzeigen
			{
				$DetailAnsicht = false;
	
				$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
				if($BildschirmBreite<1024)
				{
					$Breiten['DPR_NUMMER']=100;
					$Breiten['DPR_BEZEICHNUNG']=400;
					$Breiten['DPR_PREISGRUPPE']=100;
					$Breiten['DPF_VK']=100;					
				}
				elseif($BildschirmBreite<1280)
				{
					$Breiten['DPR_NUMMER']=100;
					$Breiten['DPR_BEZEICHNUNG']=500;
					$Breiten['DPR_PREISGRUPPE']=100;
					$Breiten['DPF_VK']=100;					
				}
				else
				{
					$Breiten['DPR_NUMMER']=100;
					$Breiten['DPR_BEZEICHNUNG']=500;
					$Breiten['DPR_PREISGRUPPE']=100;
					$Breiten['DPF_VK']=100;					
				}
	
				$Form->Formular_Start();
	
				$Form->ZeileStart();
				$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Link .= '&SSort=DPR_NUMMER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='DPR_NUMMER'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPR']['DPR_NUMMER'],$Breiten['DPR_NUMMER'],'',$Link);
				$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Link .= '&SSort=DPR_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='DPR_BEZEICHNUNG'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPR']['DPR_BEZEICHNUNG'],$Breiten['DPR_BEZEICHNUNG'],'',$Link);
				$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Link .= '&SSort=DPR_PREISGRUPPE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='DPR_PREISGRUPPE'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPR']['DPR_PREISGRUPPE'],$Breiten['DPR_PREISGRUPPE'],'',$Link);				
				$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Link .= '&SSort=DPF_VK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='DPF_VK'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPF']['DPF_VK'],$Breiten['DPF_VK'],'',$Link);				
				$Form->ZeileEnde();
	
				$DS=0;
				while(!$rsDPR->EOF())
				{					
					//Stundenverrechnungssatz f�r Preisgruppe berechnen					
					$DPPreis = $rsDPR->FeldInhalt('STSATZ');
						
					$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	
					$Form->Erstelle_ListenFeld('DPR_NUMMER',$rsDPR->FeldInhalt('DPR_NUMMER'),0,$Breiten['DPR_NUMMER'],false,($DS%2),($rsDPR->FeldInhalt('DPR_KENN')=='S'?'color:#FF0000':''),'','T','L',($rsDPR->FeldInhalt('DPR_KENN')=='S'?$AWISSprachKonserven['DPR']['txt_DienstleistungGesperrt']:''));
					$Form->Erstelle_ListenFeld('DPR_BEZEICHNUNG',$rsDPR->FeldInhalt('DPR_BEZEICHNUNG'),0,$Breiten['DPR_BEZEICHNUNG'],false,($DS%2),($rsDPR->FeldInhalt('DPR_KENN')=='S'?'color:#FF0000':''),'','T','L',($rsDPR->FeldInhalt('DPR_KENN')=='S'?$AWISSprachKonserven['DPR']['txt_DienstleistungGesperrt']:''));
					$Form->Erstelle_ListenFeld('DPR_PREISGRUPPE','PG '.$rsDPR->FeldInhalt('DPR_PREISGRUPPE'),0,$Breiten['DPR_PREISGRUPPE'],false,($DS%2),'','','T');
					$Form->Erstelle_ListenFeld('DPF_VK',$DPPreis,0,$Breiten['DPF_VK'],false,($DS%2),($rsDPR->FeldInhalt('DPR_KENN')=='S'?'color:#FF0000':''),'','N2','L',$rsDPR->FeldInhalt('DPR_KENN')=='S'?$AWISSprachKonserven['DPR']['txt_DienstleistungGesperrt']:'');					
					
					$Form->ZeileEnde();
	
					$rsDPR->DSWeiter();
					$DS++;
				}
	
				$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
				$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	
				$Form->Formular_Ende();
			}
	}
	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908041634");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908041633");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>