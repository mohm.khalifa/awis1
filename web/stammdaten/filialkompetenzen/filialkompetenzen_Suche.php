<?php
global $Funktionen;

$Funktionen->RechteMeldung(0);

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./filialkompetenzen_Main.php?cmdAktion=Details>");
$Funktionen->Form->Formular_Start();

$LabelBreite = 200;
$FeldBreite = 200;

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_BEZEICHNUNG'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*FIK_BEZEICHNUNG', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['FIK_BEZEICHNUNG']:''), 40, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();
$Funktionen->AWISCursorPosition = "sucFIK_BEZEICHNUNG";

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIL']['FIL_ID'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*FIL_ID', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['FIL_ID']:''), 40, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIL']['FIL_BEZ'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*FIL_BEZ', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['FIL_BEZ']:''), 40, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Funktionen->Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?'on':''),50,true,'on');
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->Formular_Ende();

$Funktionen->Form->SchaltflaechenStart();

$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Funktionen->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4) {
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Funktionen->Form->SchaltflaechenEnde();

$Funktionen->Form->SchreibeHTMLCode("</form>");