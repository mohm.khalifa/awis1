<?php
/**
 * Pr�fmodul: 	3
 * Zweck:		Dieses Modul pr�ft VKPreis �nderungen zur aktuellen Liste
 *
 * Parameter:	MAX_ABWEICHUNG
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}


//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;	// Erfolg als Standard

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


//*****************************************************************************
// TEST 1
//*****************************************************************************


$SQL = ' SELECT AKT.PLT_ARTNR,  AKT.PLT_Preis AS AKTPreis, NEU.PLT_PREIS AS NeuPreis, TO_CHAR((NEU.PLT_Preis-AKT.PLT_Preis),\'09999999D9999\') AS DIFF ';
$SQL .= ' FROM ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB, PLT_PREIS ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . ') NEU';
$SQL .= ' RIGHT OUTER JOIN ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB, PLT_PREIS ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=2 AND PLT_ID_FPLB=' . $PreisListe . ') AKT';
$SQL .= ' ON NEU.PLT_ARTNR = AKT.PLT_ARTNR';
$SQL .= ' WHERE (AKT.PLT_Preis-NEU.PLT_PREIS) <> 0';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: ' . $rsTSTZeilen . ' Historische Preis�nderungen werden gepr�ft. Max. Preis�nderung betr�gt ' . $Params['MAX_ABWEICHUNG'] . '%</b><br>';
flush();
$AnzAuff=0;

// Abweichung als float!
$Params['MAX_ABWEICHUNG'] = floatval(str_replace(',','.',$Params['MAX_ABWEICHUNG']));
$Params['MAX_PREIS'] = floatval(str_replace(',','.',$Params['MAX_PREIS']));

/*for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	$Diff = floatval(str_replace(',','.',$rsTST['DIFF'][$TSTZeile]));
	$AktPreis = floatval(str_replace(',','.',$rsTST['AKTPREIS'][$TSTZeile]));

	if($Diff!=0)
	{
		if($AktPreis!=0)
		{
			$Abweichung = ($Diff/$AktPreis)*100.0;
		}
	}
	elseif($rsTST['AKTPREIS'][$TSTZeile]==0 and $rsTST['NEUPREIS'][$TSTZeile]==0)
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ' hat Preis 0,00. Wird ignoriert.';
		$Abweichung = 0;
		flush();
	}
	else
	{
		echo '<br>PR�FEN: '.$rsTST['AKTPREIS'][$TSTZeile].' '.$rsTST['NEUPREIS'][$TSTZeile].' '.$rsTST['DIFF'][$TSTZeile];
	}
//echo '<br>'.$Abweichung.' AKT:'.$rsTST['AKTPREIS'][$TSTZeile].' NEU:'.$rsTST['NEUPREIS'][$TSTZeile].' DIFF:'.$rsTST['DIFF'][$TSTZeile];
	If(abs($Abweichung)>$Params['MAX_ABWEICHUNG'])
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ', Abweichung ' . awis_format($Abweichung,'Prozent,2',',') . ' -> <font color=red>Fehler</font>';

		$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN,PAU_FELD)';
		$SQL .= ' VALUES(' . $PPHKey;
		$SQL .= ', \'Preisabweichung: ' . awis_format($Abweichung,'Prozent,2',',') . ' bei ' . $rsTST['PLT_ARTNR'][$TSTZeile] . ' aktueller Preis :'.$rsTST['AKTPREIS'][$TSTZeile].' neuer Preis:'.$rsTST['NEUPREIS'][$TSTZeile].'\'' ;
		$SQL .= ', 1';
		$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\'' ;
		$SQL .= ', \'NEUPREIS\')' ;
		
		if(!awisExecute($comcon,$SQL))
		{
			echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
		}
		$TestErgebnis = 2;
		$AnzAuff++;
	}
	else
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ', Abweichung ' . awis_format($Abweichung,'Prozent,2',',') . ' -> <font color=green>I.O</font>';
		flush();
	}

	//*****************************************************************
	// SCHRITT 2
	// Max. Preis
	//*****************************************************************
	If($rsTST['NEUPREIS'][$TSTZeile]>$Params['MAX_PREIS'])
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ', Preis zu hoch ' . awis_format($rsTST['NEUPREIS'][$TSTZeile],'Currency',',') . ' -> <font color=red>Fehler</font>';

		$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN,PAU_FELD)';
		$SQL .= ' VALUES(' . $PPHKey;
		$SQL .= ', \'Preis: ' . awis_format($rsTST['NEUPREIS'][$TSTZeile],'US-Zahl') . ' bei ' . $rsTST['PLT_ARTNR'][$TSTZeile] . ' zu hoch (�ber '.$Params['MAX_PREIS'].')\'' ;
		$SQL .= ', 2';
		$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\'' ;
		$SQL .= ', \'NEUPREIS\')' ;
		
		if(!awisExecute($comcon,$SQL))
		{
			echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
		}
		$TestErgebnis = 2;
		$AnzAuff++;
	}
	else
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ', Abweichung ' . awis_format($Abweichung,'Prozent,2',',') . ' -> <font color=green>I.O</font>';
		flush();
	}

}*/

flush();
unset($rsTST);

$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_3_T_1('.$PreisListe.','.$PPHKey.','.$Params['MAX_ABWEICHUNG'].','.$Params['MAX_PREIS'].'); END;';

	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}

$SQL='SELECT COUNT(*)AS CNT_AUF FROM EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT WHERE PAU_PPH_KEY='.$PPHKey;
$rsAuf = awisOpenRecordset($comcon,$SQL);

if ($rsAuf['CNT_AUF'][0]>0)
{
	$AnzAuff=$rsAuf['CNT_AUF'][0];
	$TestErgebnis=2;
}	

if($TestErgebnis==1)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Keine Fehler gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $AnzAuff . ' Fehler gefunden. Bewertung: <font color=red>Fehler.</font></div>';
}

flush();


//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = '.$TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
}


unset($rsTST);

echo '<hr>';

?>