<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $RUECKFUEHRUNGSTYP;
global $AWISCursorPosition;

try
{

$TextKonserven = array();
$TextKonserven[]=array('RFA','%');
$TextKonserven[]=array('RFP','%');
$TextKonserven[]=array('RFM','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Liste','lst_Rueckfuehrungstyp');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Rueckfuehrungstyp');
$TextKonserven[]=array('Wort','Status');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3900 = $AWISBenutzer->HatDasRecht(3900);
if($Recht3900==0)
{
	$Form->Fehler_KeineRechte();
}

$Recht3910= $AWISBenutzer->HatDasRecht(3910);
if($Recht3910==0)
{
	$Form->Fehler_KeineRechte();
}

$EditRecht=(($Recht3900&6)!=0);

$Form->DebugAusgabe(1,$_REQUEST);

//***********************************
//Rückführungspositionen anzeigen
//***********************************

if(!isset($_GET['RFPSort']))
{
	$SQLSort = 'ORDER BY RFP_AST_ATUNR';
}
else
{
	$SQLSort = 'ORDER BY '.str_replace('~',' DESC ',$_GET['RFPSort']);
}

$BindeVariablen=array();
$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);

$SQL = 'SELECT RFP_KEY, RFP_AST_ATUNR, RFP_GESAMTMENGE, RFP_MINDESTBESTAND, RFP_BEMERKUNG, RFP_RUECKLIEFKZ, RFP_USER, RFP_USERDAT,';
$SQL .= ' RFS_BEZEICHNUNG, RFA_TYP, RUECKLIEFKZ, RFT_BEZEICHNUNG, NVL(RFM_RUECK_GES_MENGE, 0) AS RFM_RUECK_GES_MENGE,';
$SQL .= ' ROW_NUMBER() OVER ('.$SQLSort.') AS ZeilenNr';
$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGEN';
$SQL .= ' LEFT JOIN RUECKFUEHRUNGSANFORDERUNGENPOS ON RFP_RFA_KEY = RFA_KEY';
$SQL .= ' LEFT JOIN RUECKFUEHRUNGSSTATUS ON RFP_RFS_KEY = RFS_KEY';
$SQL .= ' LEFT JOIN (SELECT * FROM V_ARTIKEL_RUECKLIEFKZ WHERE GUELTIG=0) ON (RFP_AST_ATUNR=AST_ATUNR)';
$SQL .= ' LEFT JOIN RUECKFUEHRUNGSTYP ON (RFT_KENNUNG=RUECKLIEFKZ)';
$SQL .= ' LEFT JOIN (SELECT RFM_RFP_KEY, SUM(RFM_MENGE) AS RFM_RUECK_GES_MENGE FROM RUECKFUEHRUNGSANFPOSFILMENGEN GROUP BY RFM_RFP_KEY) ON (RFM_RFP_KEY = RFP_KEY)';
$SQL .= ' WHERE RFA_KEY=:var_N0_rfa_key';


if(isset($_GET['RFP_KEY']))
{
	$AWIS_KEY2 = floatval($_GET['RFP_KEY']);
	if ($AWIS_KEY2==0)
	{
		$AWIS_KEY2=-1;
	}
}
else
{
	$AWIS_KEY2 = unserialize($AWISBenutzer->ParameterLesen('RFA_POS_Blaettern'));
}
if($AWIS_KEY2!=0 AND !isset($_GET['RFPListe']))
{
	$BindeVariablen['var_N0_rfp_key']=intval($AWIS_KEY2);
	
	$SQL .= ' AND RFP_KEY = :var_N0_rfp_key';
}
elseif(isset($_GET['RFPListe']))
{
	$AWISBenutzer->ParameterSchreiben('RFA_POS_Blaettern',serialize(0));
}

$SQL= $SQL . ' ' . $SQLSort;

$Block = 1;
if(isset($_REQUEST['Block']) AND (isset($_GET['RFPListe']) OR isset($_POST['txtRFPListe'])))
{
	$Block=$Form->Format('N0',$_REQUEST['Block'],false);
}
$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

$Form->DebugAusgabe(1,$SQL);

$rsRFP = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
$rsRFPZeilen = $rsRFP->AnzahlDatensaetze();

//Status der Anforderung ermitteln
if ($rsRFPZeilen > 0)
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);
	
	$SQL = 'SELECT RFS_STATUS ';
	$SQL.= ' FROM RUECKFUEHRUNGSANFORDERUNGEN ';
	$SQL.= ' INNER JOIN RUECKFUEHRUNGSSTATUS ON RFA_RFS_KEY = RFS_KEY';
	$SQL .= ' WHERE RFA_KEY=:var_N0_rfa_key';	
	
	$rsRFS = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
}

if ($AWIS_KEY2==-1)
{
	$AWIS_KEY2=0;
	
	$Form->Formular_Start();

	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFPListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");

	$Form->InfoZeile($Felder,'');
	$Form->Erstelle_HiddenFeld('RFP_KEY', $AWIS_KEY2);
	$Form->Erstelle_HiddenFeld('RFP_RFA_KEY', $AWIS_KEY1);
	
	if($RUECKFUEHRUNGSTYP==1)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_AST_ATUNR'].':',150);
		$Form->Erstelle_TextFeld('RFP_AST_ATUNR','',20,200,($Recht3900&6),'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_GESAMTMENGE'].':',150);
		$Form->Erstelle_TextFeld('RFP_GESAMTMENGE','',20,200,($Recht3900&6),'','','','N');
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_MINDESTBESTAND'].':',150);
		$Form->Erstelle_TextFeld('RFP_MINDESTBESTAND','',20,200,($Recht3900&6),'','','','N');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('RFP_BEMERKUNG',$rsRFP->FeldInhalt('RFP_BEMERKUNG'),20,200,($Recht3900&6),'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'].':',150);
		$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);
		$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),175,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
		//$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),50,true,'***RFT_Daten;txtRUECKLIEFKZ;AST_ATUNR=*txtRFP_AST_ATUNR','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
		$Form->ZeileEnde();
	}
	else 
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_AST_ATUNR'].':',150);
		$Form->Erstelle_TextFeld('RFP_AST_ATUNR','',20,200,($Recht3900&6),'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('RFP_BEMERKUNG',$rsRFP->FeldInhalt('RFP_BEMERKUNG'),20,200,($Recht3900&6),'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'].':',150);
		$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);
		$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),175,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
		//$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),50,true,'***RFT_Daten;txtRUECKLIEFKZ;AST_ATUNR=*txtRFP_AST_ATUNR','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();
}
elseif($AWIS_KEY2==0 OR isset($_GET['RFPListe']))					// Liste anzeigen
{
	$Form->Formular_Start();
	
	$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFPListe=1';
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	$Form->Erstelle_HiddenFeld('RFPListe',1);
	
	$Form->ZeileStart();
	
	if(($Recht3900&6)>0)
	{
		if ($RUECKFUEHRUNGSTYP == "1")
		{
			$Icons[] = array('new','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY=0');
			$Icons[] = array('blank','./rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$AWIS_KEY1);
			$Icons[] = array('pdf','./rueckfuehrung_PDF.php?RFA_KEY='.$AWIS_KEY1.'&PDF_LISTE=1');
			$Form->Erstelle_ListeIcons($Icons,54,-1);
		}
		else{
				$Icons[] = array('new','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY=0');
				$Icons[] = array('pdf','./rueckfuehrung_PDF.php?RFA_KEY='.$AWIS_KEY1.'&PDF_LISTE=1');
				$Form->Erstelle_ListeIcons($Icons,36,-1);
		}
	}

	$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
	$Link .= '&RFPSort=RFP_AST_ATUNR'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFP_AST_ATUNR'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFP']['RFP_AST_ATUNR'],120,'',$Link);
	
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	if ($RUECKFUEHRUNGSTYP == "1")
	{
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFP_GESAMTMENGE'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFP_GESAMTMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFP']['RFP_GESAMTMENGE'],120,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFP_MINDESTBESTAND'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFP_MINDESTBESTAND'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFP']['RFP_MINDESTBESTAND'],120,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFM_RUECK_GES_MENGE'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFM_RUECK_GES_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_RUECK_GES_MENGE'],120,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFS_BEZEICHNUNG'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFS_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],350,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFT_BEZEICHNUNG'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFT_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'],200,'',$Link);
	}

	else
	{
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFP_GESAMTMENGE'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFP_GESAMTMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFP']['RFP_GESAMTMENGE'],120,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFM_RUECK_GES_MENGE'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFM_RUECK_GES_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_RUECK_GES_MENGE'],120,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFS_BEZEICHNUNG'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFS_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],350,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFPSort=RFT_BEZEICHNUNG'.((isset($_GET['RFPSort']) AND ($_GET['RFPSort']=='RFT_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'],200,'',$Link);
	}

	$Form->ZeileEnde();

	for($RFPZeile=0;$RFPZeile<$rsRFPZeilen;$RFPZeile++)
	{
		$Form->ZeileStart();
		$Icons = array();

		
		if ($RUECKFUEHRUNGSTYP == "1")
		{
			$Icons[] = array('zoom','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$rsRFP->FeldInhalt('RFP_KEY').'&RFMListe=1');
		}
		
		if(($Recht3900&2)>0)	// Ändernrecht
		{
			if($RUECKFUEHRUNGSTYP==1)
			{
				$Icons[] = array('edit','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$rsRFP->FeldInhalt('RFP_KEY'));
			}
			else{
				$Icons[] = array('edit','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$rsRFP->FeldInhalt('RFP_KEY').'&RFMListe=1');
			}
		}
		if(($Recht3900&8)>0)	// Löschen
		{
			$Icons[] = array('delete','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&Del='.$rsRFP->FeldInhalt('RFP_KEY').'&RF_TYP='.$RUECKFUEHRUNGSTYP);
		}

		if ($RUECKFUEHRUNGSTYP == "1")
		{
			$Form->Erstelle_ListeIcons($Icons,54,($RFPZeile%2));
			$Form->Erstelle_ListenFeld('RFP_AST_ATUNR',$rsRFP->FeldInhalt('RFP_AST_ATUNR'),20,120,false,($RFPZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('RFP_GESAMTMENGE',$rsRFP->FeldInhalt('RFP_GESAMTMENGE'),20,120,false,($RFPZeile%2),'','','Z');
			$Form->Erstelle_ListenFeld('RFP_MINDESTBESTAND',$rsRFP->FeldInhalt('RFP_MINDESTBESTAND'),20,120,false,($RFPZeile%2),'','','Z');
			$Form->Erstelle_ListenFeld('RFM_RUECK_GES_MENGE',$rsRFP->FeldInhalt('RFM_RUECK_GES_MENGE'),20,120,false,($RFPZeile%2),'','','Z');
			$Form->Erstelle_ListenFeld('RFS_BEZEICHNUNG',$rsRFP->FeldInhalt('RFS_BEZEICHNUNG'),'',350,false,($RFPZeile%2),'','','T');
			
			if ($rsRFS->FeldInhalt('RFS_STATUS') == '22' AND $rsRFP->FeldInhalt('RFP_RUECKLIEFKZ') != '')
			{
				$RueckLiefKZ='';
				$RueckLiefText = explode("|",$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);			
				foreach ($RueckLiefText as $Text)
				{
					$RFKZ = explode("~",$Text);				
					if (($RFKZ[0])==$rsRFP->FeldInhalt('RFP_RUECKLIEFKZ'))
					{
						$RueckLiefKZ = $RFKZ[1];					
					}				
				}
				
				$Form->Erstelle_ListenFeld('RFP_RUECKLIEFKZ',$RueckLiefKZ,50,200,false,($RFPZeile%2),'','','T');				
			}		
			else 
			{
				
				if($rsRFP->FeldInhalt('RFT_BEZEICHNUNG')!='')
				{
					$Form->Erstelle_ListenFeld('RFT_BEZEICHNUNG',$rsRFP->FeldInhalt('RFT_BEZEICHNUNG'),50,200,false,($RFPZeile%2),'','','T');
				}else{
					$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);				
					
				$Form->Erstelle_ListenFeld('RFT_BEZEICHNUNG',$Form->Erstelle_SelectFeld('RUECKLIEFKZ'.'_'.$rsRFP->FeldInhalt('RFP_KEY'),$rsRFP->FeldInhalt('RUECKLIEFKZ'),0,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ),'',30,false,($RFPZeile%2),'','','T');
				}
			}
			
			$Form->Erstelle_HiddenFeld('RFT_LISTE',0);
		}
		else
		{
			$Form->Erstelle_ListeIcons($Icons,36,($RFPZeile%2));
			$Form->Erstelle_ListenFeld('RFP_AST_ATUNR',$rsRFP->FeldInhalt('RFP_AST_ATUNR'),20,120,false,($RFPZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('RFP_GESAMTMENGE',$rsRFP->FeldInhalt('RFP_GESAMTMENGE'),20,120,false,($RFPZeile%2),'','','Z');
			$Form->Erstelle_ListenFeld('RFM_RUECK_GES_MENGE',$rsRFP->FeldInhalt('RFM_RUECK_GES_MENGE'),20,120,false,($RFPZeile%2),'','','Z');
			$Form->Erstelle_ListenFeld('RFS_BEZEICHNUNG',$rsRFP->FeldInhalt('RFS_BEZEICHNUNG'),'',350,false,($RFPZeile%2),'','','T');
			
			if ($rsRFS->FeldInhalt('RFS_STATUS') == '22' AND $rsRFP->FeldInhalt('RFP_RUECKLIEFKZ') != '')
			{
				$RueckLiefKZ='';
				$RueckLiefText = explode("|",$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);			
				foreach ($RueckLiefText as $Text)
				{
					$RFKZ = explode("~",$Text);				
					if (($RFKZ[0])==$rsRFP->FeldInhalt('RFP_RUECKLIEFKZ'))
					{
						$RueckLiefKZ = $RFKZ[1];					
					}				
				}
				$Form->Erstelle_ListenFeld('RFP_RUECKLIEFKZ',$RueckLiefKZ,50,200,false,($RFPZeile%2),'','','T');				
			}		
			else 
			{				
				if($rsRFP->FeldInhalt('RFT_BEZEICHNUNG')!='')
				{
					$Form->Erstelle_ListenFeld('RFT_BEZEICHNUNG',$rsRFP->FeldInhalt('RFT_BEZEICHNUNG'),50,200,false,($RFPZeile%2),'','','T');
				}else{
					$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);		
	 				$Form->Erstelle_ListenFeld('RFT_BEZEICHNUNG',$Form->Erstelle_SelectFeld('RUECKLIEFKZ'.'_'.$rsRFP->FeldInhalt('RFP_KEY'),$rsRFP->FeldInhalt('RUECKLIEFKZ'),170,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ),'',30,false,($RFPZeile%2),'','','T');
				}
			}
			
			$Form->Erstelle_HiddenFeld('RFT_LISTE',0);
		}
		$Form->ZeileEnde();
		$rsRFP->DSWeiter();
	}

	$Form->Formular_Ende();
}
else 		// Einer oder keiner
{
	$Form->Formular_Start();

	$Form->Erstelle_HiddenFeld('RFP_KEY', '0'.$rsRFP->FeldInhalt('RFP_KEY'));
	$Form->Erstelle_HiddenFeld('RFP_RFA_KEY', '0'.$AWIS_KEY1);

	if ($rsRFP->FeldInhalt('RFA_TYP')!='')
	{
		$Form->Erstelle_HiddenFeld('RFA_TYP','0'.$rsRFP->FeldInhalt('RFA_TYP'));
	}

	if($rsRFP->FeldInhalt('RFP_KEY')!='')
	{
		$AWIS_KEY2 = $rsRFP->FeldInhalt('RFP_KEY');
	}
	else
	{
		if(isset($_GET['RFP_KEY']))
		{
			$AWIS_KEY2 = $_GET['RFP_KEY'];
		}
	}
	
	$AWISBenutzer->ParameterSchreiben('RFA_POS_Blaettern',serialize($AWIS_KEY2));
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFPListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':$rsRFP->FeldInhalt('RFP_USER')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':$rsRFP->FeldInhalt('RFP_USERDAT')));
	$Form->InfoZeile($Felder,'');

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_AST_ATUNR'].':',150);
	$Form->Erstelle_TextFeld('RFP_AST_ATUNR',$rsRFP->FeldInhalt('RFP_AST_ATUNR'),20,200,'','','','','T');
	$Form->ZeileEnde();

	if (intval($RUECKFUEHRUNGSTYP) == 1)
	{
		
		//$Form->Erstelle_HiddenFeld('RFM_KEY', '0'.$DB->FeldInhaltFormat('N0',isset($_GET['RFM_KEY'])?$_GET['RFM_KEY']:'0'));
		
		if(isset($_GET['RFMListe']) OR isset($_REQUEST['Block']))
		{

			if(!isset($_GET['RFMSort']))
			{
				$SQLSort = 'ORDER BY RFM_MENGE DESC, RFM_FIL_ID ASC';
			}
			else
			{
				$SQLSort = 'ORDER BY '.str_replace('~',' DESC ',$_GET['RFMSort']);
			}
			
			$BindeVariablen=array();
			$BindeVariablen['var_N0_rfp_key']=intval($AWIS_KEY2);
			
			$SQL='SELECT RFM_KEY, RFM_FIL_ID, RFM_MENGE, RFS_BEZEICHNUNG, RFM_FILBESTD, RFM_USERDAT, RFM_USER,';
			$SQL .= ' ROW_NUMBER() OVER ('.$SQLSort.') AS ZeilenNr';
			$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN LEFT JOIN RUECKFUEHRUNGSSTATUS ON RFM_RFS_KEY=RFS_KEY';
			$SQL .= ' WHERE RFM_RFP_KEY=:var_N0_rfp_key';
	
			if(isset($_GET['RFM_KEY']))
			{
				$BindeVariablen['var_N0_rfm_key']=intval($_GET['RFM_KEY']);
				
				$SQL .= ' AND RFM_KEY = :var_N0_rfm_key';
			}
			
			$SQL = $SQL . ' ' . $SQLSort;
	
			$Block = 1;
			if(isset($_REQUEST['Block']) AND (isset($_GET['RFMListe']) OR isset($_POST['txtRFMListe'])))
			{
				$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			}
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
			$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
			
			$Form->DebugAusgabe(1,'XXX: '.$SQL);
			
			$rsRFM = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
			$rsRFMZeilen = $rsRFM->AnzahlDatensaetze();
	
			if ($rsRFMZeilen>0)
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_rfp_key']=intval($AWIS_KEY2);
				
				$SQL='SELECT NVL(SUM(RFM_MENGE),0) AS RFM_GESAMTMENGE FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_RFP_KEY=:var_N0_rfp_key';
				$rsRFMGesMenge = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
				$rsRFMGesMengeZeilen = $rsRFMGesMenge->AnzahlDatensaetze();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_GESAMTMENGE'].':',150);
				$Form->Erstelle_TextFeld('RFM_GESAMTMENGE',$rsRFMGesMenge->FeldInhalt('RFM_GESAMTMENGE'),20,200,'','','','','Z');
				$Form->ZeileEnde();
				
				$Form->Trennzeile();
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.unserialize($AWISBenutzer->ParameterLesen('RFA_POS_Blaettern')).'&RFMListe=1';
				$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
				$Form->Erstelle_HiddenFeld('RFMListe',1);
	
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
				$Link .= '&RFMListe=1&RFMSort=RFM_FIL_ID'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFM_FIL_ID'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_FIL_ID'],75,'',$Link);
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
				$Link .= '&RFMListe=1&RFMSort=RFM_MENGE'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFM_MENGE'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_MENGE'],75,'',$Link);
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
				$Link .= '&RFMListe=1&RFMSort=RFS_BEZEICHNUNG'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFS_BEZEICHNUNG'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],350,'',$Link);
	
				for($RFMZeile=0;$RFMZeile<$rsRFMZeilen;$RFMZeile++)
				{
					$Form->ZeileStart();
					$Form->Erstelle_ListenFeld('RFM_FIL_ID',$rsRFM->FeldInhalt('RFM_FIL_ID'),'',75,false,($RFMZeile%2),'','','Z');
					$Form->Erstelle_ListenFeld('RFM_MENGE',$rsRFM->FeldInhalt('RFM_MENGE'),'',75,false,($RFMZeile%2),'','','Z');
					$Form->Erstelle_ListenFeld('RFS_BEZEICHNUNG',$rsRFM->FeldInhalt('RFS_BEZEICHNUNG'),'',350,false,($RFMZeile%2),'','','T');
					$Form->ZeileEnde();
					$rsRFM->DSWeiter();
				}
	
			}
		}	
		else 
		{
			if(!isset($_GET['RFMSort']))
			{
				$SQLSort = 'ORDER BY RFM_MENGE DESC, RFM_FIL_ID ASC';
			}
			else
			{
				$SQLSort = 'ORDER BY '.str_replace('~',' DESC ',$_GET['RFMSort']);
			}
			
			$BindeVariablen=array();
			$BindeVariablen['var_N0_rfp_key']=intval($AWIS_KEY2);
			
			$SQL='SELECT RFM_KEY, RFM_FIL_ID, RFM_MENGE, RFS_BEZEICHNUNG, RFM_USERDAT, RFM_USER,';
			$SQL .= ' ROW_NUMBER() OVER ('.$SQLSort.') AS ZeilenNr ';
			$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN LEFT JOIN RUECKFUEHRUNGSSTATUS ON RFM_RFS_KEY=RFS_KEY';
			$SQL .= ' WHERE RFM_RFP_KEY=:var_N0_rfp_key';
	
			if(isset($_GET['RFM_KEY']))
			{
				$BindeVariablen['var_N0_rfm_key']=intval($_GET['RFM_KEY']);
				
				$SQL .= ' AND RFM_KEY = :var_N0_rfm_key';
			}
			$SQL=$SQL . ' ' . $SQLSort;
	
			$rsRFM = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
			$rsRFMZeilen = $rsRFM->AnzahlDatensaetze();
		
			$Form->Trennzeile();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_GESAMTMENGE'].':',150);
			$Form->Erstelle_TextFeld('RFP_GESAMTMENGE',$rsRFP->FeldInhalt('RFP_GESAMTMENGE'),20,200,($Recht3900&6),'','','','T');
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_MINDESTBESTAND'].':',150);
			$Form->Erstelle_TextFeld('RFP_MINDESTBESTAND',$rsRFP->FeldInhalt('RFP_MINDESTBESTAND'),20,200,($Recht3900&6),'','','','T');
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_BEMERKUNG'].':',150);
			$Form->Erstelle_TextFeld('RFP_BEMERKUNG',$rsRFP->FeldInhalt('RFP_BEMERKUNG'),20,200,($Recht3900&6),'','','','T');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'].':',150);
			
			$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);
			
			if ($rsRFS->FeldInhalt('RFS_STATUS') == '22' AND $rsRFP->FeldInhalt('RFP_RUECKLIEFKZ') != '')
			{
				$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RFP_RUECKLIEFKZ'),175,false,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
			}
			else 		
			{
				$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),175,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
				//$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),50,true,'***RFT_Daten;txtRUECKLIEFKZ;AST_ATUNR=*txtRFP_AST_ATUNR','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
			}
			
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',150);
			$Form->Erstelle_TextFeld('RFS_BEZEICHNUNG',$rsRFP->FeldInhalt('RFS_BEZEICHNUNG'),'',350,'','','','','T');
			$Form->ZeileEnde();	
		}
		
	}
	else
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('RFP_BEMERKUNG',$rsRFP->FeldInhalt('RFP_BEMERKUNG'),30,40,($Recht3900&6),'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'].':',150);
		
		$RueckfuehrungsKZ=explode('|',$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);
		
		if ($rsRFS->FeldInhalt('RFS_STATUS') == '22' AND $rsRFP->FeldInhalt('RFP_RUECKLIEFKZ') != '')
		{
			$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RFP_RUECKLIEFKZ'),200,false,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
		}
		else 
		{
			$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),50,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$RueckfuehrungsKZ);
			//$Form->Erstelle_SelectFeld('RUECKLIEFKZ',$rsRFP->FeldInhalt('RUECKLIEFKZ'),50,true,'***RFT_Daten;txtRUECKLIEFKZ;AST_ATUNR=*txtRFP_AST_ATUNR','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
		}
		
		$Form->ZeileEnde();

		$Form->Erstelle_HiddenFeld('RFM_KEY', '0'.$DB->FeldInhaltFormat('N0',isset($_GET['RFM_KEY'])?$_GET['RFM_KEY']:'0'));

		$BindeVariablen=array();
		$BindeVariablen['var_N0_rfp_key']=intval($AWIS_KEY2);
		
		$SQL='SELECT RFM_KEY, RFM_FIL_ID, RFM_MENGE, RFS_BEZEICHNUNG, RFM_USER, RFM_USERDAT, RFM_FILBESTD,';
		$SQL .= ' ROW_NUMBER() OVER (ORDER BY 1) AS ZeilenNr';
		$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN LEFT JOIN RUECKFUEHRUNGSSTATUS ON RFM_RFS_KEY=RFS_KEY';
		$SQL .= ' WHERE RFM_RFP_KEY=:var_N0_rfp_key';

		if(isset($_GET['RFM_KEY']))
		{
			$BindeVariablen['var_N0_rfm_key']=intval($_GET['RFM_KEY']);
			
			$SQL .= ' AND RFM_KEY = :var_N0_rfm_key';
		}
		if(!isset($_GET['RFMSort']))
		{
			$SQL .= ' ORDER BY RFM_MENGE DESC, RFM_FIL_ID ASC';
		}
		else
		{
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['RFMSort']);
		}

		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		}
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

		$Form->DebugAusgabe(1,'YYY: '.$SQL);
		
		$rsRFM = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
		$rsRFMZeilen = $rsRFM->AnzahlDatensaetze();

		if ($rsRFMZeilen>1 OR isset($_REQUEST['RFMListe']))
		{
			$Form->Trennzeile();
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.unserialize($AWISBenutzer->ParameterLesen('RFA_POS_Blaettern'));
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

			if(($Recht3900&6)>0)
			{
				$Icons[] = array('new','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY=0'.$AWIS_KEY2.'&RFM_KEY=0');
				$Form->Erstelle_ListeIcons($Icons,38,-1);
			}
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
			$Link .= '&RFMSort=RFM_FIL_ID'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFM_FIL_ID'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_FIL_ID'],75,'',$Link);
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
			$Link .= '&RFMSort=RFM_MENGE'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFM_MENGE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_MENGE'],75,'',$Link);
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2;
			$Link .= '&RFMSort=RFS_BEZEICHNUNG'.((isset($_GET['RFMSort']) AND ($_GET['RFMSort']=='RFS_BEZEICHNUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],350,'',$Link);

			for($RFMZeile=0;$RFMZeile<$rsRFMZeilen;$RFMZeile++)
			{
				$Form->ZeileStart();

				$Icons = array();
				if(($Recht3900&2)>0)	// Ändernrecht
				{
					$Icons[] = array('edit','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFP_KEY='.$AWIS_KEY2.'&RFM_KEY='.$rsRFM->FeldInhalt('RFM_KEY'));
				}
				if(($Recht3900&8)>0)	// Löschen
				{
					$Icons[] = array('delete','./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&Del='.$AWIS_KEY2.'&RFM_KEY='.$rsRFM->FeldInhalt('RFM_KEY').'&RF_TYP='.$RUECKFUEHRUNGSTYP);
				}
				$Form->Erstelle_ListeIcons($Icons,38,($RFMZeile%2));

				$Form->Erstelle_ListenFeld('RFM_FIL_ID',$rsRFM->FeldInhalt('RFM_FIL_ID'),'',75,false,($RFMZeile%2),'','','Z');
				$Form->Erstelle_ListenFeld('RFM_MENGE',$rsRFM->FeldInhalt('RFM_MENGE'),'',75,false,($RFMZeile%2),'','','Z','','Filialbestand: '.$rsRFM->FeldInhalt('RFM_FILBESTD'));
				$Form->Erstelle_ListenFeld('RFS_BEZEICHNUNG',$rsRFM->FeldInhalt('RFS_BEZEICHNUNG'),'',350,false,($RFMZeile%2),'','','T');
				$Form->ZeileEnde();
				$rsRFM->DSWeiter();
			}
		}
		else			// Bearbeiten
		{
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Seite=Positionen&RFMListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsRFM->FeldInhalt('RFM_USER')));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsRFM->FeldInhalt('RFM_USERDAT')));

			$Form->Trennzeile();
			
			$Form->InfoZeile($Felder,'');

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFM']['RFM_FIL_ID'].':',150);
			if($rsRFM->FeldInhalt('RFM_FIL_ID')!='')
			{
				$Form->Erstelle_TextFeld('RFM_FIL_ID',$rsRFM->FeldInhalt('RFM_FIL_ID'),20,30,false,'','','','T');
			}
			else{
				$Form->Erstelle_TextFeld('RFM_FIL_ID',$rsRFM->FeldInhalt('RFM_FIL_ID'),20,30,($Recht3900&6),'','','','T');
			}
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFM']['RFM_MENGE'].':',150);
			$Form->Erstelle_TextFeld('RFM_MENGE',$rsRFM->FeldInhalt('RFM_MENGE'),20,30,($Recht3900&6),'','','','T');
			$Form->ZeileEnde();
			
			$Form->SchreibeHTMLCode('<input type="hidden" name="RFMListe" value="1">');
		}
	}

	$Form->Formular_Ende();
}

}
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>