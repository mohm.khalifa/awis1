<?php
global $AWIS_KEY1;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());


//***********************************************
// filialebenenrollen
//***********************************************
awis_Debug(1,$_POST);

$AWIS_KEY1=$_POST['txtFER_KEY'];

$Felder = awis_NameInArray($_POST, 'txtFER_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FER','FER_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFER_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenrollen';
			$SQL .= '(FER_BEZEICHNUNG,FER_BEMERKUNG,FER_GUELTIGAB,FER_GUELTIGBIS,FER_FRB_KEY';
			$SQL .= ',FER_USER,FER_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtFER_BEZEICHNUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFER_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFER_GUELTIGAB'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFER_GUELTIGBIS'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFER_FRB_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('filialebenenrollen',1,$awisDBError['message'],'200804011001');
				die();
			}
			$SQL = 'SELECT seq_FER_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY1=$rsKey['KEY'][0];
		}
	}
	else 					// gešnderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFER = awisOpenRecordset($con,'SELECT * FROM filialebenenrollen WHERE FER_key=' . $_POST['txtFER_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFER[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFER['FER_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenenrollen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FER_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FER_userdat=sysdate';
			$SQL .= ' WHERE FER_key=0' . $_POST['txtFER_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('filialebenenrollenBereiche',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
}
?>