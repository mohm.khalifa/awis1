<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

	if(awisBenutzerRecht($con,100)==0)
	{
	     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
	     die("Keine ausreichenden Rechte!");
	}

	// Lokale Variablen
	$Bedingung = '';		// Bedingung f�r SQL
	$SQL = '';				// SQL Abfragestring

	 // Daten f�r die Infoseite sammeln
	 $SQL = "SELECT DISTINCT FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, MAX(FIF_Wert) AS FIF_WERT
				FROM filialinfos, filialinfostypen, filialinfostypengruppen, filialen
				 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
				 	AND FILIALEN.FIL_ID = filialinfos.FIF_FIL_ID
        			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
					AND (FIF_FIT_ID=34)
					AND (FIL_GRUPPE IS NULL))
			GROUP BY FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT
		";

//					AND (FIF_FIT_ID=21 AND FIF_WERT IS NOT NULL)
	if(isset($_GET["Sortierung"]) AND $_GET["Sortierung"]=="FilialNr")
	{
		$SQL .= "ORDER BY FIL_ID ";
	}
	elseif(isset($_GET["Sortierung"]) AND $_GET["Sortierung"]=="Datum")
	{
		$SQL .= "ORDER BY TO_DATE(FIF_WERT,'dd.mm.RRRR'), FIL_BEZ";
	}
	else
	{
		$SQL .= "ORDER BY FIL_BEZ";
	}

	$rsFiliale = awisOpenRecordset($con, $SQL);
	$rsFilialeAnz = $awisRSZeilen;

	If($rsFilialeAnz>1)		// Liste gefunden
	{
		print "<table border=0 width=100%>";
		if(isset($_GET["Sortierung"]) AND $_GET["Sortierung"]=="FilialNr" )
		{
			print "<tr><td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Neueroeffnungen&Sortierung=Filiale>Filiale</a></td>";
		}
		else
		{
			print "<tr><td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Neueroeffnungen&Sortierung=FilialNr>Filiale</a></td>";
		}
		print "<td id=FeldBez>Anschrift</td>";
		print "<td id=FeldBez>Ort</td>";
		print "<td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Neueroeffnungen&Sortierung=Datum>Datum</a></td>";

		for($i=0;$i<$rsFilialeAnz;$i++)
		{
			$SQL = "SELECT filialinfos.fif_wert
				FROM filialinfos, importquellen
				 WHERE (importquellen.IMQ_ID = filialinfos.FIF_IMQ_ID
					AND (FIF_FIT_ID = 900 AND FIF_WERT IS NOT NULL)
					AND FIF_FIL_ID=0" . $rsFiliale["FIL_ID"][$i] . ")
				ORDER BY IMQ_PRIORITAET";
			$rsFilialInfo = awisOpenRecordset($con, $SQL);

			if($awisRSZeilen>0)		// Nur anzeigen, wenn eine Gebietskennung existiert
			{
				print "<tr><td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?NeuEroef=ja&cmdAktion=Filialinfos&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
				print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
				print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">". $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . (isset($rsFiliale["FIL_ORTSTEIL"][$i])?$rsFiliale["FIL_ORTSTEIL"][$i]:'') . "</td>";

// 		    if($rsFilialInfo["FIF_WERT"][0]!="")
				{
					print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsFiliale["FIF_WERT"][$i],'Datum') . "</td>";
				}
/*			else
			{
				print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">&nbsp;</td>";
			}
			*/
				print "</tr>";
			}
		}
		print "</table>";
	}
	else
	{
		print "<span class=HinweisText>Es stehen keine Neuer�ffnungen aus</span>";
	}

/************************************************************************************
*
* Hilfsfunktionen
*
* **********************************************************************************/
Function SucheFilialInfo($rsFiliale, $InfoID, $Spalte)
{
	//for($i=0;$i<count($rsFiliaInfo);$i++)
	$i=0;
	while($rsFiliale[$Spalte][$i]!="")
	{
		if($rsFiliale[$Spalte][$i]==$InfoID)
		{
			return $i;
		}
		$i++;
//	print "/". $rsFiliale[$Spalte][$i];
	}
	return -1;
}

?>