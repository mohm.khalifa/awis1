<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Preislistenpr�fungen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;
global $awisRSZeilen;

@apache_setenv('no-gzip', 1);

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body bgcolor="#eeeeeF">
<?php
$con = awislogon();

$RechteStufe = awisBenutzerRecht($con,2000);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'Preislistenpr�fungen',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

$comcon = awisLogonComCluster('DE',false);
awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");

echo '<h2>Preislistenpr�fung</h2>';


// �bersicht zeigen und Best�tigen lassen
if(!isset($_POST['cmdStart_x']))
{
	$SQL = 'SELECT BEZEICHNUNG, PST_ID, PST_STATUS, PTS_USERDAT, PTS_TRANSAKTIONSID, PTS_ID_FPLB';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ';
	$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB ON ID_FPLB = PTS_ID_FPLB';
	$SQL .= ' WHERE PTS_TRANSAKTIONSID='.$_GET['TID'].'';
	$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';

	$rsPLI = awisOpenRecordset($comcon,$SQL);
	//awis_Debug(1,$rsPLI);

	echo '<form name=frmBestaetigung action=./preislisten_PruefungDurchfuehren.php method=POST>';

	echo '<input type=hidden name=txtTID value=' . $_GET['TID'] . '>';
	echo '<input type=hidden name=txtPREISLISTE value=' . $rsPLI['PTS_ID_FPLB'][0] . '>';

	echo '<table border=1 width=100% ID=DatenTabelle>';
	echo '<tr>';
	echo '<td class=FeldBez width=200>Preisliste:</td><td>' . $rsPLI['BEZEICHNUNG'][0] . '(' . $rsPLI['PTS_ID_FPLB'][0] . ')</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td class=FeldBez>Transaktion:</td><td>' . $rsPLI['PTS_TRANSAKTIONSID'][0] . '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td class=FeldBez>Status:</td><td>' . $rsPLI['PST_STATUS'][0] . '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td class=FeldBez>Letzte Status�nderung:</td><td>' . $rsPLI['PTS_USERDAT'][0] . '</td>';
	echo '</tr>';


	echo '</table>';

	echo '<br><b>Folgende Pr�fungen werden durchgef�hrt:</b>';
	// Alle Pr�fungen anzeigen

	echo '<table border=1 width=100% id=DatenTabelle>';

	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN ON PLP_PTA_ID=PTA_ID';
	$SQL .= ' WHERE PLP_ID_FPLB=' . $rsPLI['PTS_ID_FPLB'][0];
	$SQL .= ' ORDER BY PLP_ReihenFolge';

	$rsPTA = awisOpenRecordset($comcon,$SQL);
	$rsPTAZeilen = $awisRSZeilen;

	echo '<tr><td class=FeldBez width=200>Pr�fung</td><td class=FeldBez>Bemerkung</td></tr>';

	for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
	{
		echo '<tr>';
		echo '<td><b>' . $rsPTA['PTA_BEZEICHNUNG'][$PTAZeile] . '</b></td>';
		echo '<td>' . $rsPTA['PTA_BEMERKUNG'][$PTAZeile] . '</td>';
		echo '</tr>';

		$SQL = 'SELECT *';
		$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
		$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
		$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
		$SQL .= ' WHERE PLP_ID_FPLB=' . $rsPTA['PLP_ID_FPLB'][$PTAZeile];
		$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

		$rsPAR = awisOpenRecordset($comcon,$SQL);
		$rsPARZeilen = $awisRSZeilen;
		if($rsPARZeilen>0)
		{
			echo '<tr><td>&nbsp;</td>';
			echo '<td><table border=1>';
			echo '<colgroup>';
			echo '<col width=170>';
			echo '<col width=400>';
			echo '</colgroup>';
			echo '<tr><td class=FeldBez>Parameter</td>';
			echo '<td class=FeldBez>Aktueller Wert</td></tr>';

			for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
			{
				echo '<td title=\'' . $rsPAR['PTP_BESCHREIBUNG'][$PARZeile] . '\'>'.$rsPAR['PTP_PARAMETER'][$PARZeile].'</td>';

				if(($RechteStufe&16)==16)
				{
					echo "<td><input type=hidden name=oldParam_".$rsPAR['PTP_KEY'][$PARZeile]."#".$rsPAR['PLP_KEY'][$PARZeile]." value='".($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]).'\'>';
					echo "<input size='100%' name=txtParam_".$rsPAR['PTP_KEY'][$PARZeile]."#".$rsPAR['PLP_KEY'][$PARZeile]." value='".($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]).'\'></td>';
				}
				else
				{
					echo '<td>'.($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]).'</td>';
				}
				echo '<tr>';
			}

			echo '</table></td>';
			echo '</tr>';
		}
	}


	echo '</table>';

	echo "<br><hr><input type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=window.close();>";
	echo "&nbsp;<input type=image title='Pr�fung starten' src=/bilder/schraubenschluessel.png name=cmdStart accesskey=p >";

	echo '</form>';
}
else		// Pr�fungen durchf�hren..
{
		echo str_repeat(" ",4096)."<pre>";
		
		echo '<br><center><b>Pr�fung wird gestartet</b>...</center>';
		echo "<br><center><input type=image title='Pr�fung' src=/bilder/loading_bar.gif name=cmdLoading></center>";
		
		ob_flush();
		flush();
		usleep(50000);
		
		//***********************************************
		//Parameter speichern, falls ge�ndert
		//***********************************************
	$ParamListe = awis_NameInArray($_POST,'txtParam_',1);
	if($ParamListe!=='')
	{
		$ParamListe = explode(';',$ParamListe);
		foreach ($ParamListe AS $Param)
		{


			$PTPKey = intval(substr($Param,9));
			$PLPKey = intval(substr($Param,10+strlen($PTPKey)));

			if($_POST[$Param] != $_POST['old'.substr($Param,3)])
			{
				$SQL = 'DELETE FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM WHERE PPP_PTP_KEY=0' . $PTPKey . ' AND PPP_PLP_KEY=0' . $PLPKey;
				awisExecute($comcon,$SQL);
				$SQL = 'INSERT INTO  EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM';
				$SQL .= ' VALUES(';
				$SQL .= '' . $PLPKey . '';
				$SQL .= ',' . $PTPKey . '';
				$SQL .= ',\'' . $_POST[$Param] . '\'';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				awisExecute($comcon,$SQL);
			}
		}
	}


	ini_set('max_execution_time',60*60);		// Maximal 1 Stunde Laufzeit
	echo '<br>Setze <b>Pr�fungsstatus</b>...';
	flush();

	// Pr�fungsstatus: Pr�fung l�uft
	$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
	$SQL .= ' SET PTS_PST_ID=10';
	$SQL .= ' , PTS_GUELTIG_VON=(SELECT PLT_GUELTIG_VON FROM (SELECT PLT_GUELTIG_VON FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID='.$_POST['txtTID'].' GROUP BY PLT_GUELTIG_VON ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
	$SQL .= ' , PTS_GUELTIG_BIS=(SELECT PLT_GUELTIG_BIS FROM (SELECT PLT_GUELTIG_BIS FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID='.$_POST['txtTID'].' GROUP BY PLT_GUELTIG_BIS ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
	$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];
	awisExecute($comcon,$SQL);

	$SQL = 'DELETE FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $_POST['txtTID'];
	awisExecute($comcon,$SQL);

		// Pr�fungen suchen
	echo '<br>Suche <b>Pr�fungen</b>...';
	flush();
	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN ON PLP_PTA_ID=PTA_ID';
	$SQL .= ' WHERE PLP_ID_FPLB=' . $_POST['txtPREISLISTE'];
	$SQL .= ' ORDER BY PLP_ReihenFolge';

	echo $SQL;

	$rsPTA = awisOpenRecordset($comcon,$SQL);
	$rsPTAZeilen = $awisRSZeilen;
	echo '<br><b>'.$rsPTAZeilen.'</b> Pr�fungen gefunden...';



	// Variabeln f�r die Module
	$PreisListe = $_POST['txtPREISLISTE'];
	$TransaktionsID = $_POST['txtTID'];

	for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
	{
		echo str_repeat(" ",4096);
		echo '<br><br><div class=UeberschriftBalken>Starte Pr�fung <b>' . $rsPTA['PTA_BEZEICHNUNG'][$PTAZeile] . '</b>...</div>';
		ob_flush();
		flush();
		usleep(50000);

		include $rsPTA['PTA_SKRIPTNAME'][$PTAZeile];

		// Pr�fungsstatus: Pr�fung l�uft
		$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
		$SQL .= ' SET PTS_PST_ID=11';
		$SQL .= ' , PTS_GUELTIG_VON=(SELECT PLT_GUELTIG_VON FROM (SELECT PLT_GUELTIG_VON FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID='.$_POST['txtTID'].' GROUP BY PLT_GUELTIG_VON ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
		$SQL .= ' , PTS_GUELTIG_BIS=(SELECT PLT_GUELTIG_BIS FROM (SELECT PLT_GUELTIG_BIS FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID='.$_POST['txtTID'].' GROUP BY PLT_GUELTIG_BIS ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
		$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];
		awisExecute($comcon,$SQL);

	}


	echo '<br>Setze <b>Pr�fungsstatus</b>...';
	flush();

	$SQL = 'SELECT MIN(PPH_STATUS) AS STATUS';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
	$SQL .= ' WHERE PPH_Key=0'.$PPHKey;
	$rsTST  = awisOpenRecordset($comcon,$SQL);


	if($rsTST['STATUS'][0]=='0')
	{
		$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
		$SQL .= ' SET PTS_PST_ID=11, PTS_USER=\'' . $AWISBenutzer->BenutzerName() . '\', PTS_USERDAT=SYSDATE';
		$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];
	}
	else
	{
		$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
		$SQL .= ' SET PTS_PST_ID=15, PTS_USER=\'' . $AWISBenutzer->BenutzerName() . '\', PTS_USERDAT=SYSDATE';
		$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];
	}
	awisExecute($comcon,$SQL);

	echo "<br><hr><input type=image title='Zur�ck und aktualisieren' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=opener.location.reload();window.close();>";
}

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);


awisLogoff($con);
awisLogoff($comcon);

?>