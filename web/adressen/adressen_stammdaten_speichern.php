<?php
global $AWIS_KEY1;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISBenutzer;

try 
{
    $TextKonserven=array();
    $TextKonserven[]=array('Fehler','err_KeinWert');
    $TextKonserven[]=array('Fehler','err_FelderVeraendert');
    $TextKonserven[]=array('Wort','geaendert_von');
    $TextKonserven[]=array('Wort','geaendert_auf');
    $TextKonserven[]=array('Meldung','DSVeraendert');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprache = $AWISBenutzer->ParameterLesen('AnzeigeSprache');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    //***********************************************
    // Adressinfos
    //***********************************************
    $Fehler='';
    $Felder = $Form->NameInArray($_POST, 'txtADT_',1,1);
    if($Felder!='')
    {
        $AWIS_KEY1=$_POST['txtADT_KEY'];
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('ADT','ADT_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven, $AWISSprache);
        
        $Pflichtfelder = array('ADT_INFORMATION','ADT_FORMAT','ADT_ANZAHL','ADT_AZG_KEY','ADT_STATUS','ADT_SORTIERUNG','ADT_BREITE');
            
        foreach($Pflichtfelder AS $Pflichtfeld)
        {
            if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['GSK'][$Pflichtfeld].'<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if($Fehler!='')
        {
            die('<span class=HinweisText>'.$Fehler.'</span>');
        }
        $FeldListe='';
        $SQL = '';
    
        if($_POST['txtADT_KEY']=='0')
        {
            $Speichern = true;
    
            if($Speichern)
            {
                $Fehler = '';
                $SQL = 'INSERT INTO AdressenInfoTypen';
                $SQL .= '(ADT_INFORMATION,ADT_FORMAT,ADT_ANZAHL,ADT_DATENQUELLE,ADT_VERWENDUNG,ADT_AZG_KEY';
                $SQL .= ',ADT_STATUS,ADT_SORTIERUNG,ADT_ZEICHEN,ADT_BREITE';
                $SQL .= ',ADT_USER, ADT_USERDAT';
                $SQL .= ')VALUES (';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtADT_INFORMATION'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADT_FORMAT'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtADT_ANZAHL'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtADT_DATENQUELLE'])?$_POST['txtADT_DATENQUELLE']:''),true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtADT_VERWENDUNG'])?$_POST['txtADT_VERWENDUNG']:0),true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtADT_AZG_KEY'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADT_STATUS'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtADT_SORTIERUNG'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtADT_ZEICHEN'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtADT_BREITE'],true);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403241011,$SQL,awisException::AWIS_ERR_SYSTEM);
                    die();
                }
                $SQL = 'SELECT seq_ADT_KEY.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY1=$rsKey->FeldInhalt('KEY');
            }
        }
        else 					// gešnderte Zuordnung
        {
            $FehlerListe = array();
            $UpdateFelder = '';
            $rsADT = $DB->RecordSetOeffnen('SELECT * FROM AdressenInfoTypen WHERE ADT_key=' . $_POST['txtADT_KEY'] . '');
            $FeldListe = '';
            
            foreach($Felder AS $Feld)
            {
                $FeldName = substr($Feld,3);
                if(isset($_POST['old'.$FeldName]))
                {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu=$DB->FeldInhaltFormat($rsADT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
                    $WertAlt=$DB->FeldInhaltFormat($rsADT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
                    $WertDB=$DB->FeldInhaltFormat($rsADT->FeldInfo($FeldName,'TypKZ'),$rsADT->FeldInhalt($FeldName),true);
                    //echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
                    if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
                    {
                        if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
                        {
                            $FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
                        }
                        else
                        {
                            $FeldListe .= ', '.$FeldName.'=';
    
                            if($_POST[$Feld]=='')	// Leere Felder immer als NULL
                            {
                                $FeldListe.=' null';
                            }
                            else
                            {
                                $FeldListe.=$WertNeu;
                            }
                        }
                    }
                }
            }
            if(count($FehlerListe)>0)
            {
                $Meldung = str_replace('%1',$rsADT->FeldInhalt('ADT_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
                foreach($FehlerListe AS $Fehler)
                {
                    $Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
                }
                $Form-> Hinweistext($Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
            }
            elseif($FeldListe!='')
            {
                $SQL = 'UPDATE AdressenInfoTypen SET';
                $SQL .= substr($FeldListe,1);
                $SQL .= ', ADT_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', ADT_userdat=sysdate';
                $SQL .= ' WHERE ADT_key=0' . $_POST['txtADT_KEY'] . '';
                
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403241021,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }
        }
    }
    
    //***********************************************
    // Adressaktionen
    //***********************************************
    $Felder = $Form->NameInArray($_POST, 'txtAAT_',1,1);
    if($Felder!='')
    {
        $AWIS_KEY1=$_POST['txtAAT_KEY'];
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('AAT','AAT_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven, $AWISSprache);
        $FeldListe='';
        $SQL = '';
    
        if($_POST['txtAAT_KEY']=='0')
        {
            $Speichern = true;
    
            if($Speichern)
            {
                $Fehler = '';
                $SQL = 'INSERT INTO AdressenAktionenTypen';
                $SQL .= '(AAT_BEZEICHNUNG,AAT_BEMERKUNG,AAT_AZG_KEY';
                $SQL .= ',AAT_STATUS';
                $SQL .= ',AAT_USER, AAT_USERDAT';
                $SQL .= ')VALUES (';
                $SQL .= ' ' . $Form->Format('T',$_POST['txtAAT_BEZEICHNUNG'],true);
                $SQL .= ',' . $Form->Format('T',$_POST['txtAAT_BEMERKUNG'],true);
                $SQL .= ',' . $Form->Format('Z',$_POST['txtAAT_AZG_KEY'],true);
                $SQL .= ',' . $Form->Format('T',$_POST['txtAAT_STATUS'],true);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if($DB->Ausfuehren($SQL)=='')
                {
                    throw new awisException('Fehler beim Speichern',201403241022,$SQL,awisException::AWIS_ERR_SYSTEM);
                    die();
                }
                $SQL = 'SELECT seq_AAT_KEY.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY1=$rsKey['KEY'][0];
            }
        }
        else 					// gešnderte Zuordnung
        {
            $FehlerListe = array();
            $UpdateFelder = '';
    
            $rsAAT = $DB->RecordSetOeffnen('SELECT * FROM AdressenAktionenTypen WHERE AAT_key=' . $_POST['txtAAT_KEY'] . '');
            $FeldListe = '';
            foreach($Felder AS $Feld)
            {
                $FeldName = substr($Feld,3);
                if(isset($_POST['old'.$FeldName]))
                {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu=$Form->Format($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
                    $WertAlt=$Form->Format($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
                    $WertDB=$Form->Format($awisRSInfoName[$FeldName]['TypKZ'],$rsAAT[$FeldName][0],true);
                    //echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
                    if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
                    {
                        if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
                        {
                            $FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
                        }
                        else
                        {
                            $FeldListe .= ', '.$FeldName.'=';
    
                            if($_POST[$Feld]=='')	// Leere Felder immer als NULL
                            {
                                $FeldListe.=' null';
                            }
                            else
                            {
                                $FeldListe.=$WertNeu;
                            }
                        }
                    }
                }
            }
            if(count($FehlerListe)>0)
            {
                $Meldung = str_replace('%1',$rsAAT->FeldInhalt('AAT_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
                foreach($FehlerListe AS $Fehler)
                {
                    $Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
                }
                $Form-> Hinweistext($Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
            }
            elseif($FeldListe!='')
            {
                $SQL = 'UPDATE AdressenAktionenTypen SET';
                $SQL .= substr($FeldListe,1);
                $SQL .= ', AAT_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', AAT_userdat=sysdate';
                $SQL .= ' WHERE AAT_key=0' . $_POST['txtAAT_KEY'] . '';
                
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403241024,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }
        }
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>