<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	
	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtGUT_KEY']))
		{
			$Tabelle = 'GUT';
			$Key=$_POST['txtGUT_KEY'];

			$SQL = 'SELECT *';
			$SQL .= ' FROM GUTSCHEINAKTION ';
			$SQL .= ' WHERE GUT_KEY=0'.$Key;

			$rsDaten = $GUT->DB->RecordsetOeffnen($SQL);

			$GUTKey = $rsDaten->FeldInhalt('GUT_KEY');

			$Felder=array();
			$Felder[]=array($GUT->Form->LadeTextBaustein('GUT','GUT_GUTSCHEINNR'),$rsDaten->FeldInhalt('GUT_GUTSCHEINNR'));
			$Felder[]=array($GUT->Form->LadeTextBaustein('GUT','GUT_VORGANGSNUMMER'),$rsDaten->FeldInhalt('GUT_VORGANGSNUMMER'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'GUT':
				$SQL = 'UPDATE gutscheinaktion set gut_status = 5 WHERE GUT_key=0'.$_POST['txtKey'];
				$AWIS_KEY1='';
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			$GUT->DB->Ausfuehren($SQL);
		}
	}

	if($Tabelle!='')
	{
		$GUT->AWISSprachKonserven = $GUT->Form->LadeTexte($TextKonserven);

		$GUT->Form->SchreibeHTMLCode('<form name=frmLoeschen action=./gutscheinaktion_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$GUT->Form->Formular_Start();
		$GUT->Form->ZeileStart();
		$GUT->Form->Hinweistext($GUT->AWISSprachKonserven['Wort']['WirklichLoeschen']);
		$GUT->Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$GUT->Form->ZeileStart();
			$GUT->Form->Erstelle_TextLabel($Feld[0].':',150);
			$GUT->Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$GUT->Form->ZeileEnde();
		}

		$GUT->Form->Erstelle_HiddenFeld('GUTKey',$GUTKey);
		$GUT->Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$GUT->Form->Erstelle_HiddenFeld('Key',$Key);

		$GUT->Form->Trennzeile();

		$GUT->Form->ZeileStart();
		$GUT->Form->Schaltflaeche('submit','cmdLoeschenOK','','',$GUT->AWISSprachKonserven['Wort']['Ja'],'');
		$GUT->Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$GUT->AWISSprachKonserven['Wort']['Nein'],'');
		$GUT->Form->ZeileEnde();

		$GUT->Form->SchreibeHTMLCode('</form>');

		$GUT->Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$GUT->Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$GUT->Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$GUT->Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>