<?php
/**
 * awisHeader.inc
 *
 * Kopfzeile f�r die Formulare
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200807
 *
 */

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');
global $AWISVersion;
try
{
    $AWISVersion = 3;
    // Objekte initialisieren
    $AWISBenutzer = awisBenutzer::Init();
    $AWISDB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISDB->Oeffnen();
}
catch(Exception $ex)
{
    echo $ex->getMessage();
    die();
}
// Session starten f�r die Benutzerparameter
if(session_id() == '')
{
    session_start();
}

$Level = new awisWerkzeuge();

// Parameter Lesen
$MinAnzeige = $AWISBenutzer->ParameterLesen('MinimaleKopfzeile');
$ShortCutLeiste = $AWISBenutzer->ParameterLesen('ShortCutLeiste', true);

if($MinAnzeige)
{
    $Zusatzklasse = 'MinHeader';
}
else
{
    $Zusatzklasse = '';
}

echo "<div id='awisHeader" . $Level->awisLevel() . "' class='$Zusatzklasse'>";
echo "<div id='awisHeaderScroll' class=''>";

/*background-image:url(/bilder/headerbild.png);*/
echo '<table border="0" width="98%" cellspacing=0 cellpadding=0 style="border-spacing:0; margin: 0 0 0 0; ">';
//echo '<table border="0" width="100%" cellspacing=0 cellpadding=0 style="border-spacing:0; margin: 0 0 0 0;">';

$pfad = dirname($_SERVER['SCRIPT_FILENAME']) . "/version.txt";

if(file_exists($pfad))
{
    $fd = fopen($pfad, "r");
    $Zeile = trim(fgets($fd));
    while((substr($Zeile, 0, 8) != "[Header]") AND !feof($fd))        // Bis zum Header lesen
    {
        $Zeile = trim(fgets($fd));
    }

    $Zeile = trim(fgets($fd));
    $i = 0;

    while((substr($Zeile, 0, 1) != "[") AND !feof($fd))        // Bis zum n�chsten Header lesen
    {                                                    // Alle Parameter auslesen
        if(substr($Zeile, 0, 1) != "#" AND substr($Zeile, 0, 1) != "")
        {
            $Werte = explode("=", $Zeile);
            $VInfo[strtoupper($Werte[0])] = $Werte[1];
        }

        $Zeile = trim(fgets($fd));
    }

    // Header schreiben

    echo "<tr><td width=20%><p align=left style='margin: 0 0 0 0'>";

    if(!$MinAnzeige)
    {
        echo "<a href=/index.php target=_self><img width='116' height='53' alt=\"X\" src='";
        echo $VInfo["LOGO"];
        echo "' ></a></p>";
    }
    else
    {
        print "<font size=1pt ><b>" . $AWISBenutzer->BenutzerName() . "</b></font>";
    }
    echo "</td>";

    //**********************************************
    // Shortcut - Leiste
    //**********************************************
    $ShortCutLeiste = explode(';', $ShortCutLeiste);

    echo '<td width=40 valign=middle>';

    echo '<table border=0>';
    echo '<tr>';

    if(isset($ShortCutLeiste[1]) AND $ShortCutLeiste[1] != '')
    {
        echo '<td><a target=' . $ShortCutLeiste[0] . ' href=' . $ShortCutLeiste[1] . " onMouseover=\"window.status='" . $ShortCutLeiste[3] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[2] . ' title=\'' . $ShortCutLeiste[3] . '\' alt=\'' . $ShortCutLeiste[3] . '\'></a></td>';
    }
    if(isset($ShortCutLeiste[5]) AND $ShortCutLeiste[5] != '')
    {
        echo '<td><a target=' . $ShortCutLeiste[4] . ' href=' . $ShortCutLeiste[5] . " onMouseover=\"window.status='" . $ShortCutLeiste[7] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[6] . ' title=\'' . $ShortCutLeiste[7] . '\' alt=\'' . $ShortCutLeiste[7] . '\'></a></td>';
    }
    if(isset($ShortCutLeiste[9]) AND $ShortCutLeiste[9] != '')
    {
        echo '<td><a target=' . $ShortCutLeiste[8] . ' href=' . $ShortCutLeiste[9] . " onMouseover=\"window.status='" . $ShortCutLeiste[11] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[10] . ' title=\'' . $ShortCutLeiste[11] . '\' alt=\'' . $ShortCutLeiste[11] . '\'></a></td>';
    }

    $TRBeginn = '';
    if(!$MinAnzeige)
    {
        echo '</tr>';
        $TRBeginn = '<tr>';
    }

    $TREnde = '';
    if(isset($ShortCutLeiste[13]) AND $ShortCutLeiste[13] != '')
    {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[12] . ' href=' . $ShortCutLeiste[13] . " onMouseover=\"window.status='" . $ShortCutLeiste[15] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[14] . ' title=\'' . $ShortCutLeiste[15] . '\' alt=\'' . $ShortCutLeiste[15] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }
    if(isset($ShortCutLeiste[17]) AND $ShortCutLeiste[17] != '')
    {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[16] . ' href=' . $ShortCutLeiste[17] . " onMouseover=\"window.status='" . $ShortCutLeiste[19] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[18] . ' title=\'' . $ShortCutLeiste[19] . '\' alt=\'' . $ShortCutLeiste[19] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }
    if(isset($ShortCutLeiste[21]) AND $ShortCutLeiste[21] != '')
    {
        echo $TRBeginn . '<td><a target=' . $ShortCutLeiste[20] . ' href=' . $ShortCutLeiste[21] . " onMouseover=\"window.status='" . $ShortCutLeiste[23] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[22] . ' title=\'' . $ShortCutLeiste[23] . '\' alt=\'' . $ShortCutLeiste[23] . '\'></a></td>';
        $TRBeginn = '';
        $TREnde = '</tr>';
    }

    echo $TREnde;
    echo '</table>';

    echo '</td>';

    //***********************************
    // Mittelblock mit dem Namen
    //***********************************
    echo "<td width=35%><p align=center style='margin:0 0 0 0'><b><font color=white>";

    // Alle Versionsnummern auslesen
    $Zeile = trim(fgets($fd));
    while((substr($Zeile, 0, 1) == "#" OR substr($Zeile, 0, 1) == "") AND !feof($fd))
    {
        $Zeile = trim(fgets($fd));
    }
    $VersionsInfos = explode(";", $Zeile);
    if(!$MinAnzeige)
    {
        echo $VInfo["PRODUKTNAME"] . "<br>";
    }
    echo "<font size=2>" . $VInfo["MODULNAME"] . ", Version " . $VersionsInfos[0] . "</font>";

    print "<img src=\"/bilder/info_klein.png\" alt=\"Version\" onclick=\"window.open('/hilfe/versionsinfo.php?Verzeichnis=$pfad','Versionsinformation','toolbar=no,menubar=no,dependent=yes,status=no');\" onMouseover=\"window.status='Versionshistorie anzeigen';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
    fclose($fd);

    print "</font>";
    if(!$MinAnzeige)
    {
        echo "<br><font size=1 color=white>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";

        //*********************************************************
        // Sprach-/ Landesauswahl
        //*********************************************************
        echo "<td width=5%>";
        if(isset($VInfo["SPRACHEN"]))
        {
            $AltLink = $_SERVER['REQUEST_URI'];
            if(strpos($AltLink, 'AWISSprache') !== false)
            {
                global $AWISSprache;
                $AWISSprache = substr($AltLink, strpos($AltLink, 'AWISSprache') + 12, 2);
                $AWISBenutzer->BenutzerSprache($AWISSprache);
                $AltLink = substr($AltLink, 0, strpos($AltLink, 'AWISSprache') - 1);
            }
            echo '<table border=0 width=110><tr><td width=100>';
            $Sprachen = explode(',', $VInfo["SPRACHEN"]);
            foreach($Sprachen AS $Sprache)
            {
                echo '<a id=BilderLink href="' . $AltLink . (strpos($AltLink, '?') === false?'?':'&') . 'AWISSprache=' . $Sprache . '"><img border=0px style="background-color:#D0D0D0;border-width:0px;padding:2px;margin:1px;width:20px;height:12px" height=12 width=20 src="/bilder/Flagge_' . strtoupper($Sprache) . '.gif"></a>';
            }

            // TODO: Andere Position, muss auch bei MIN gehen
            //PG 01.03.2016: Div geschlossen
            echo "</td><td><div id=\"DSStatus\"></div><div id=\"DSAENDERN\"></div></td></tr></table>";
        }
        echo "</td>";
    }

    print "<td width=30% align=right style='margin: 0 0 0 0'>";

    //Telefon HEADER Teil START
    echo '<a class="tel_klasse" id="" ><img border=0 width=32 height=32 src=/bilder/tel_icon.png alt=Suchen></a><div id="telBereich" name="telBereich">';
    $Form = new awisFormular();
    $Form->AjaxSuchfeld('telBereich', 'telBox', 'BeitragsKey', 'TELEFON_Liste', '', 600, 32, 350, '', '', '', '', true, 2, 'Name,Telefon,E-Mail oder Abteilung eingeben...');
    echo '<div class="telBereichClose">Schlie&szlig;en</div>';
    echo '</div>';
    //Telefon HEADER Teil ENDE

    if($VInfo["STARTSEITE"] != '')
    {
        print "<a href='/hilfe/hilfe_Main.php?HilfeThema=allgemein&Aktion=' border=0><img border=0 src=/bilder/awis_hilfe.png alt=Hilfe></a>";
        print "<a href=" . $VInfo["STARTSEITE"] . " " . (isset($VInfo["TARGET"])?($VInfo["TARGET"] == ''?"target=_self":'target=' . $VInfo["TARGET"]):'') . "><img border=0 src=/bilder/start.png alt=Startseite></a>";
    }
    elseif($VInfo["MARQUEE"] != '')
    {
        print "<marquee scrollamount=1 scrolldelay=50><font size=1>" . $VInfo["MARQUEE"] . "</font></marquee>";
    }
}
else        // Falls keine Versionsdatei gefunden wurde -> Allg. Infos anzeigen
{
    echo "<td width=33% align=left style=margin: 0 0 0 0;>";
    echo "<img src=/bilder/atulogo_neu_gross.png>";
    echo '</td>';

    echo "<td width=33% align=center style=margin: 0 0 0 0;><b><font color=white>";
    echo "Awis, Version 2.00.00";
    echo "</font><br><font size=1 color=white>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";

    echo "<td width=34% align=right style=margin: 0 0 0 0;>";

    echo '<a href=/index.php target=_self><img border=0 src=/bilder/start.png alt=Startseite></a>';
}

if(!$MinAnzeige)
{
    $Recht70 = $AWISBenutzer->HatDasRecht(70);
    if($Recht70 & 1 == 1)
    {
        //require_once('kint/Kint.class.php');

        print "<br><font color=white size=1pt >Angemeldet als: <b color=white>";
        if(isset($_GET['Debug']))
        {
            $SQL = 'UPDATE BENUTZEROPTIONEN';
            $SQL .= " SET XBO_WERT = DECODE(XBO_WERT,'1','0','0','1','0')";
            $SQL .= ' WHERE XBO_XBT_ID=1';
            $SQL .= ' AND XBO_XBN_KEY=0' . $AWISBenutzer->BenutzerID();

            //var_dump($SQL);
            $AWISDB->Ausfuehren($SQL, '', true);
        }
        $AltLink = $_SERVER['REQUEST_URI'];
        echo '<a id=DebugSwitchLink color=white href="' . $AltLink . (strpos($AltLink, '?') === false?'?':'&') . 'Debug' . '">' . $AWISBenutzer->BenutzerName() . '</a>';
    }
    else
    {
        print "<br><font color=white size=1>Angemeldet als <b>" . $AWISBenutzer->BenutzerName() . "</b></font>";
    }
    //if($AWISBenutzer->BenutzerName()=='entwick')
    {
        //print "<br><font size=1pt>Server: " . $_SERVER['HTTP_HOST'] . "</font>";
        $hostname = gethostbyaddr($_SERVER['SERVER_ADDR']);
        print "<br><font size=1>Server: " . $hostname . "</font>";
    }
}
setlocale(LC_TIME, "de_DE");
setlocale(LC_NUMERIC, ",");

echo '</td></tr></table>';

if(isset($VInfo["WARNUNG"]) AND $VInfo["WARNUNG"] != '')
{
    echo '<span class=Hinweistext>';
    echo $VInfo['WARNUNG'];
    echo '</span>';
}

$WerkZeug = new awisWerkzeuge();
if($WerkZeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK)
{
    echo '<img width="1280px" height="5px" style="position:absolute;left:0;bottom:0;" src="/bilder/lineal_leiste.png" border=0>';
}
echo '</div>';
echo '</div>';

?>
<script src="/jquery_ie11.js"></script>
<script src="/jquery.scrollUp.min.js"></script>
<script src="/popup.js"></script>
<script src="/formularBereich.js"></script>
<script src="/jquery.tooltipster.js"></script>
<script src="/jquery-ui.js"></script>


<?php
echo '<script type=text/javascript>';
echo '$(function(){';
echo '	var HeaderTop = $("#awisHeader' . $Level->awisLevel() . '").offset().top; ';
echo '	var browser_hoehe = $(document).height(); ';
echo '	var bleibenbe_seitegroesse; ';
echo '	bleibenbe_seitegroesse = document.body.scrollHeight - document.body.clientHeight; ';
echo '	if(document.body.scrollHeight > document.body.clientHeight & bleibenbe_seitegroesse>150)';
echo '{';
echo '	$(window).scroll(function(){';
echo '		if( $(window).scrollTop() > HeaderTop ) 
			{';
echo '			$("#awisHeader' . $Level->awisLevel() . '").css({"position": "fixed","z-index":"9999","width":"100%"});';
echo '			$(".RegisterZeile:first .RegisterReiterScroll").css({"position": "fixed","left":"0", "background-color":"#727c8f","border-bottom":"5px solid #c9c9c9","top":"40px", "box-shadow":"0 0 11px 0 black","width":"100%"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksInaktiv .RegisterInaktivBildDiv > a > .RegisterInaktivBild").css({"display":"none"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksAktiv .RegisterAktivBildDiv  > .RegisterAktivBild").css({"display":"none"});';
echo '           if($(".RegisterLinksInaktiv").contents().length == 0){';
echo '			$(".RegisterZeile:first .RegisterReiterScroll").css({"margin-top": "20px", "min-height":"26px"});';
echo '			}else{';
echo '			$(".RegisterZeile:first .RegisterReiterScroll").css({"margin-top": "5px", "min-height":"47px"});';
echo '			}';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksInaktiv .RegisterInaktivBildDiv").css({"height": "19px", "min-width":"19px"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksAktiv .RegisterAktivBildDiv").css({"height": "19px", "min-width":"19px"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteInaktiv > a").css({"color":"white"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteAktiv").css({"color":"black","background-color":"transparent","box-shadow":"0 0px 0px rgba(0, 0, 0, 0.55)","text-shadow":"0 0px 0 #f3f3f3"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteInaktiv").css({"color":"white","background-color":"transparent","box-shadow":"0 0px 0px rgba(0, 0, 0, 0.55)","text-shadow":"0 0px 0 #f3f3f3"});';

echo '		}
			 else 
			{';
echo '			$("#awisHeader' . $Level->awisLevel() . '").css({"position": "relative","width":"100%"});';
echo '			$(".RegisterZeile:first .RegisterReiterScroll").css({"position": "relative", "background-color":"transparent","border-bottom":"0px solid #c9c9c9","top":"0%", "box-shadow":"0 0 0px 0 black"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksInaktiv .RegisterInaktivBildDiv > a > .RegisterInaktivBild").css({"display":"inline"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksAktiv .RegisterAktivBildDiv  > .RegisterAktivBild").css({"display":"inline"});';
echo '			$(".RegisterZeile:first .RegisterReiterScroll").css({"margin-top": "0px"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksInaktiv .RegisterInaktivBildDiv").css({"height": "50px", "min-width":"19px"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterLinksAktiv .RegisterAktivBildDiv").css({"height": "50px", "min-width":"19px"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteInaktiv > a").css({"color":"#006a7d"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteAktiv").css({"color":"black","background-color":"#d0d0d0","box-shadow":"0 1px 3px rgba(0, 0, 0, 0.55)","text-shadow":"0 1px 0 #f3f3f3","margin-top":"0"});';
echo '			$(".RegisterZeile:first .RegisterReiter > .RegisterMitteInaktiv").css({"color":"black","background-color":"#f6f6f6","box-shadow":"0 1px 3px rgba(0, 0, 0, 0.55)","text-shadow":"0 1px 0 #f3f3f3","margin-top":"0"});';

echo '		}';
echo '	});';
echo '}';
echo '});';
echo '</script>';
?>
<script type="text/javascript">

    function setzeFarbe(feldName) {
        document.getElementById("DSStatus").innerHTML = "<font color= size=1></font>";
        if ($(".ds_veraendert").length == 0) {
            $("#DSAENDERN").append("<div class='ds_veraendert'><img border='0' class='ToolImage' src='/bilder/ico_speichern.png' alt='TEXT fur Aenderung' /></div>");
        }


    }


    $(document).ready(function () {

        $(".tel_klasse").click(function () {

            $("#telBereich").fadeIn();
            $('#telBereich').find('input[type=text]').filter(':visible:first').focus();

        });
        $(".telBereichClose").click(function () {

            $("#telBereich").fadeOut();

        });


        $('.MenuePunkte').tooltipster({
            animation: 'grow',
            contentAsHTML: true,
            theme: 'tooltipster-punk'
        });

        // Neue Tooltipps verwenden bei den Eingabefeldern
        // TODO: Pr�fen, ob man das in einem Select machen kann!
        $(document).ready(function () {
            jQuery(".EingabeFeld,.EingabeFeldPflicht,.EingabeLabel,.EingabeFeldTextarea,.awisInfotafelUeberschrift,.EingabeBild").tooltipster({

                animation: 'grow',
                contentAsHTML: true,
            });
        });

        $.scrollUp({
            animation: 'fade',
            scrollImg: {
                active: true,
                type: 'background',
                src: '/bilder/top.png'
            }
        });

        //Wenn wir scrollen,dann geben wir zu Scroll div eine andere Klasse
        $(window).scroll(function () {
            if ($("#scrollUp")) {
                $("#scrollUp").addClass('scrollUp');
            }
        });


        //Fixed Schaltflaechen, wenn Bildschirm gr��er wie 800px
        var istScroll = document.body.scrollHeight > document.body.clientHeight;
        if (istScroll === true) {
            if ($(document).height() > <?php
                echo $AWISBenutzer->ParameterLesen('SchaltflaecheFixierenAb');

                ?> ) {
                if ($('.SchaltflaechenBereich:last').is(':not(:empty)')) {

                    var $document = $(document),
                        $element = $('.SchaltflaechenBereich'),
                        className = 'SchaltflaechenBereichFixed';
                    $element.addClass(className);
                    $("#scrollUp").addClass("scrollUpFixed");
                    $(window).scroll(function () {

                        if ($(window).scrollTop() + window.innerHeight - 20 == $(document).height() - 20) {
                            $element.removeClass(className);
                            $("#scrollUp").removeClass("scrollUpFixed");
                        }
                        else {
                            $element.addClass(className);
                            $("#scrollUp").addClass("scrollUpFixed");
                        }

                    });
                }
            }
        }


//Wenn Bildschirm kleiner ist, wie das Inhalt, dann kommt scroll
        var EingabeZeileMaxWidth = 0;
        var reg_Inhalt_breite = 0;
        var itemWidth = 0;
        $(".EingabeZeile").each(function (index) {
            itemWidth = $(this).outerWidth(true);
            EingabeZeileMaxWidth = Math.max(EingabeZeileMaxWidth, itemWidth);
            reg_Inhalt_breite = $(this).parent().width();


            if ($(this).is(':last-child') && $(this).is(":visible")) {
                reg_Inhalt_breite = $(this).parent().width();
                if (reg_Inhalt_breite == EingabeZeileMaxWidth) {
                    $(this).addClass("EingabeZeileScrollKlasse");
                    $(this).parent().find(".EingabeZeile").addClass("EingabeZeileScrollKlasse");
                    $(this).parent().addClass("EingabeformularScrollKlasse");

                }

                if (reg_Inhalt_breite < EingabeZeileMaxWidth) {
                    $(this).addClass("EingabeZeileScrollKlasse");
                    $(this).parent().find(".EingabeZeile").addClass("EingabeZeileScrollKlasse");
                    $(this).parent().addClass("EingabeformularScrollKlasse");
                }


            }
            else {

                reg_Inhalt_breite = $(this).parent().width();
                if (reg_Inhalt_breite == EingabeZeileMaxWidth) {
                    $(this).addClass("EingabeZeileScrollKlasse");
                    $(this).parent().find(".EingabeZeile").addClass("EingabeZeileScrollKlasse");
                    $(this).parent().addClass("EingabeformularScrollKlasse");
                }
                if (reg_Inhalt_breite < EingabeZeileMaxWidth) {
                    $(this).addClass("EingabeZeileScrollKlasse");
                    $(this).parent().find(".EingabeZeile").addClass("EingabeZeileScrollKlasse");
                    $(this).parent().addClass("EingabeformularScrollKlasse");
                }
            }

        });
    });
</script>
<?php
// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('TITEL', 'pop_uebersicht');
$TextKonserven[] = array('TITEL', 'pop_inhalt');
$TextKonserven[] = array('TITEL', 'pop_zurueck');
$Form = new awisFormular();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

if($AWISBenutzer->ParameterLesen('Seiteverlassen') == 1)
{
    ?>
    <script>
        $(document).ready(function () {
//Wenn wir die Webseite verlassen will

            //Vibartion fuer Speichern
            function blink(time, interval) {
                var timer = window.setInterval(function () {
                    $("input[name='cmdSpeichern']").css("opacity", "0.1");
                    $("input[name='cmdSpeichern']").css("background-color", "#f76c6c");
                    $("input[name='cmdSpeichern']").css("box-shadow", "0 0px 18px 10px #f76c6c");
                    $("input[name='cmdSpeichern']").css("border-radius", "20px");
                    window.setTimeout(function () {
                        $("input[name='cmdSpeichern']").css("opacity", "1");

                    }, 200);
                }, interval);
                window.setTimeout(function () {
                    clearInterval(timer);
                }, time);
            }

            var akt_link;
            var neu_url;
            var base_url;
            var link_location;
            var visible = false;


            neu_url = (location.pathname + location.search).substr(1);


            //Wenn wir auf <a>-tag klikken will
            $("a").click(function (event) {

                var link_location = $(this).attr('href'); //Nehmen wir den Link von <a>-tag Link, wohin wir geklickt haben

                //PG: Javascript hinzugef�gt
                if (typeof link_location !== typeof undefined && link_location !== false && link_location !=='javascript:;') //Wenn der Link nicht Javascript/jQuery Funkcion ist
                {
                    if (link_location != neu_url) {
                        if (document.getElementById("DSStatus").innerHTML !== '') {
                            event.preventDefault();
                            blink(10000, 1000); //Vibration
                            DialogPopup(1); //Popup rufen

                            if (visible) //Weiter Button erzeugen
                            {
                                $(".fensterSchaltflaecheErzeugt").remove();
                                $("#fenster_1 .fensterFooter ").append('<div class="fensterSchaltflaecheErzeugt"><a href="' + link_location + '" class="seiteVerlassen" >Weiter</a></div>');
                            }
                            else {
                                $("#fenster_1 .fensterFooter ").append('<div class="fensterSchaltflaecheErzeugt"><a href="' + link_location + '" class="seiteVerlassen" >Weiter</a></div>');
                                visible = true;
                            }
                        }
                    }
                }

            });

            //Wenn wir auf Schaltflaeche klicken will
            $(".Schaltflaeche:input").not("#cmdAssistent #cmdSpeichern").click(function (event) {

                var ButtonId = $(this).attr("id");
                var ButtonSrc = $(this).attr("src");
                var $form = $(this).closest('form');
                var FormName = $form.attr('name');
                var FormAction = $form.attr('action');

                if (document.getElementById("DSStatus").innerHTML !== '') {
                    event.preventDefault();

                    blink(10000, 1000); //Vibration
                    DialogPopup(1); //Popup rufen

                    if (visible) //Weiter Button erzeugen
                    {
                        $(".fensterSchaltflaecheErzeugt").remove();
                        $("#fenster_1 .fensterFooter ").append('<div class="fensterSchaltflaecheErzeugt"><form enctype="multipart/form-data" name="' + FormName + '" method="POST" action="' + FormAction + '" ><input type="image" style="margin:0px;padding:0px;" src="' + ButtonSrc + '" name="' + ButtonId + '" class="seiteVerlassenSchaltflaeche" /></form></div>');

                    }
                    else {
                        $("#fenster_1 .fensterFooter ").append('<div class="fensterSchaltflaecheErzeugt"><form enctype="multipart/form-data" name="' + FormName + '" method="POST" action="' + FormAction + '" ><input type="image" style="margin:0px;padding:0px;" src="' + ButtonSrc + '" name="' + ButtonId + '" class="seiteVerlassenSchaltflaeche" /></form></div>');
                        visible = true;
                    }
                }
            });
        });


    </script>

    <?php
    //Wir erzeugen das Fenster, fuer die Webseite wenn wir die ohne Speichern verlassen will
    $Form->PopupDialog($AWISSprachKonserven['TITEL']['pop_uebersicht'], $AWISSprachKonserven['TITEL']['pop_inhalt'], array(array('',
        $AWISSprachKonserven['TITEL']['pop_zurueck'],
        '',
        '')), 2, 1);
}

//Leeres PopUp f�r die Seite erzeugen
$Form->PopupDialog('', '', array(array('',
    $AWISSprachKonserven['TITEL']['pop_zurueck'],'','')), 1, 'MasterPopUp');
?>
