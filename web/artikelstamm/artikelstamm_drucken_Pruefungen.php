<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');

global $AWIS_KEY1;
global $AWIS_KEY2;
try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISBenutzer = awisBenutzer::Init();

    $Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
    $Form = new awisFormular();

    $RechteStufe = $AWISBenutzer->HatDasRecht(407);
    if ($RechteStufe == 0) {
        $Form->Fehler_KeineRechte();
    }

    $TextKonserven = array();
    $TextKonserven[] = array('APR', '%');
    $TextKonserven[] = array('Wort', 'wrd_Stand');
    $TextKonserven[] = array('Wort', 'wrd_Artikelpruefungen');
    $TextKonserven[] = array('Ausdruck', 'txt_HinweisAusdruckIntern');
    $TextKonserven[] = array('Ausdruck', 'txt_KeineDatenGefunden');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Ausdruck = new awisAusdruck('L', 'A4', $Vorlagen, $AWISSprachKonserven['Wort']['wrd_Artikelpruefungen']);
    $Param = $AWISBenutzer->ParameterLesen('AktuellerAPR');
    /***************************************
     *
     * Daten laden
     *
     ***************************************/

    $SQL = 'SELECT *';
    $SQL .= ' FROM ARTIKELPRUEFUNGEN LEFT OUTER JOIN Lieferanten ON APR_LIE_NR = LIE_NR';
    $SQL .= ' INNER JOIN Artikelstamm ON APR_AST_ATUNR=AST_ATUNR';
    $SQL .= ' LEFT OUTER JOIN ARTIKELPRUEFUNGENMELDUNGEN ON APR_KEY = APM_APR_KEY';
    $SQL .= " WHERE APR_KEY=0" . $Param . "";

    $rsAPR = $DB->RecordSetOeffnen($SQL);
    if ($rsAPR->EOF()) {
        $Ausdruck->NeueSeite(0, 1);
        $Ausdruck->_pdf->SetFont('Arial', '', 10);
        $Ausdruck->_pdf->cell(270, 6, $AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'], 0, 0, 'C', 0);
    } else        // Daten gefunden
    {
        $Ausdruck->NeueSeite(0, 1);
        $Ausdruck->_pdf->SetFont('Arial', '', 6);
        $Ausdruck->_pdf->setXY(10, 200);
        $Ausdruck->_pdf->cell(270, 6, $AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'], 0, 0, 'C', 0);
        $Zeile = 15;
    }    // Ende Datenaufbereitung

    $LinkerRand = 10;

    // �berschrift setzen

    $Ausdruck->_pdf->SetFont('Arial', '', 10);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(60, 6, "ATU-Nummer:", 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 60, $Zeile);
    $Ausdruck->_pdf->cell(60, 6, "Lieferantenartikelnummer:", 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 120, $Zeile);
    $Ausdruck->_pdf->cell(160, 6, "Artikelbezeichnung", 0, 0, 'L', 0);

    // Zweite Zeile

    $Zeile += 5;
    $Ausdruck->_pdf->SetFont('Arial', 'b', 14);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_AST_ATUNR'), 1, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 60, $Zeile);
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_LAR_LARTNR'), 1, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 120, $Zeile);
    $Ausdruck->_pdf->cell(160, 6, $rsAPR->FeldInhalt('AST_BEZEICHNUNGWW'), 1, 0, 'L', 0);

    // Dritte Zeile
    $Zeile += 7;
    $Ausdruck->_pdf->SetFont('Arial', '', 10);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand + 80, $Zeile);
    $Ausdruck->_pdf->cell(40, 6, "LF-Nr:", 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 120, $Zeile);
    $Ausdruck->_pdf->cell(160, 6, "Lieferantenname", 0, 0, 'L', 0);

    // Vierte Zeile

    $Zeile += 5;
    $Ausdruck->_pdf->SetFont('Arial', '', 14);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand + 80, $Zeile);
    $Ausdruck->_pdf->cell(40, 6, $rsAPR->FeldInhalt('LIE_NR'), 1, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 120, $Zeile);
    $Ausdruck->_pdf->cell(160, 6, $rsAPR->FeldInhalt('LIE_NAME1'), 1, 0, 'L', 0);

    // F�nfte Zeile

    $Zeile += 7;
    $Ausdruck->_pdf->SetFont('Arial', '', 12);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(55, 6, "Fehlerbeschreibung:", 0, 0, 'R', 0);

    $Ausdruck->_pdf->SetFont('Arial', '', 14);                // Schrift setzen
    $Ausdruck->_pdf->setXY($LinkerRand + 60, $Zeile);
    $Ausdruck->_pdf->cell(220, 30, '', 1);
    $Ausdruck->_pdf->setXY($LinkerRand + 60, $Zeile);
    $Ausdruck->_pdf->MultiCell(220, 6, $rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG'), 0, 'L', 0);

    // Sechste Zeile

    $Zeile = 80;
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(90, 6, "Logistikzentrum Weiden", 0, 0, 'C', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile);
    $Ausdruck->_pdf->cell(90, 6, "Logistikzentrum Werl", 0, 0, 'C', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 180, $Zeile);
    $Ausdruck->_pdf->cell(90, 6, "Filialbest�nde", 0, 0, 'C', 0);

    $Zeile += 5;

    //******************************
    // Weiden
    //******************************
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(90, 60, '', 1);

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(25, 6, "Menge:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_MENGE_N'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile + 5);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(25, 6, "Pr�fer:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_PRUEFER_N'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile + 10);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(25, 6, "Datum:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(60, 6, $Form->Format('D', $rsAPR->FeldInhalt('APR_DATUM_N')), 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile + 15);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(25, 6, "Ergebnis:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen
    $Ausdruck->_pdf->setXY($LinkerRand + 25, $Zeile + 16);
    $Ausdruck->_pdf->multicell(60, 4, $rsAPR->FeldInhalt('APR_ERGEBNIS_N'), 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile + 35);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);                // Schrift setzen
    $Ausdruck->_pdf->cell(25, 6, "Ma�nahme:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);                // Schrift setzen
    $Ausdruck->_pdf->setXY($LinkerRand + 25, $Zeile + 36);
    $Ausdruck->_pdf->multicell(60, 4, $rsAPR->FeldInhalt('APR_MASSNAHME_N'), 0, 'L', 0);

    //******************************
    // Werl
    //******************************
    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile);
    $Ausdruck->_pdf->cell(90, 60, '', 1);

    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Menge:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_MENGE_L'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile + 5);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Pr�fer:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_PRUEFER_L'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile + 10);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Datum:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(60, 6, $Form->Format('D', $rsAPR->FeldInhalt('APR_DATUM_L')), 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile + 15);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Ergebnis:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->setXY($LinkerRand + 115, $Zeile + 16);
    $Ausdruck->_pdf->multicell(60, 4, $rsAPR->FeldInhalt('APR_ERGEBNIS_L'), 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 90, $Zeile + 35);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Ma�nahme:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->setXY($LinkerRand + 115, $Zeile + 36);
    $Ausdruck->_pdf->multicell(60, 4, $rsAPR->FeldInhalt('APR_MASSNAHME_L'), 0, 'L', 0);

    //******************************
    // Filialbest�nde
    //******************************

    $Ausdruck->_pdf->setXY($LinkerRand + 180, $Zeile);
    $Ausdruck->_pdf->cell(100, 60, '', 1);

    $Ausdruck->_pdf->setXY($LinkerRand + 180, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Menge:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(60, 6, $rsAPR->FeldInhalt('APR_FILIALBESTAND'), 0, 0, 'L', 0);

    $Ausdruck->_pdf->setXY($LinkerRand + 180, $Zeile + 5);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Ma�nahme:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->setXY($LinkerRand + 205, $Zeile + 6);
    $Ausdruck->_pdf->multicell(65, 4, $rsAPR->FeldInhalt('APR_MASSNAHME_FILIALEN'), 0, 'L', 0);

    // Ma�nahme QS
    $Zeile += 60;
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->multicell(280, 6, $rsAPR->FeldInhalt('APR_MASSNAHME'), 0, 'L', 0);
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->cell(280, 20, '', 1);

    // Bearbeiter
    $Zeile += 21;
    $Ausdruck->_pdf->setXY($LinkerRand, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Bearbeiter:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(40, 6, $rsAPR->FeldInhalt('APR_BEARBEITER'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Datum:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(40, 6, $Form->Format('D', $rsAPR->FeldInhalt('APR_BEARBEITUNGSTAG')), 0, 0, 'L', 0);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Meldung durch:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(40, 6, $rsAPR->FeldInhalt('APR_MELDUNGDURCH'), 0, 0, 'L', 0);

    // Lieferant informiert
    $Zeile += 5;
    $Ausdruck->_pdf->setXY($LinkerRand + 65, $Zeile);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "Lieferant informiert am:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(40, 6, $Form->Format('D', $rsAPR->FeldInhalt('APR_LIEFINFORMIERTAM')), 0, 0, 'L', 0);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 11);
    $Ausdruck->_pdf->cell(25, 6, "durch:", 0, 0, 'R', 0);
    $Ausdruck->_pdf->SetFont('Arial', '', 11);
    $Ausdruck->_pdf->cell(40, 6, $rsAPR->FeldInhalt('APR_LIEFINFORMIERT'), 0, 0, 'L', 0);

    // Trennlinie ziehen
    $Ausdruck->_pdf->Line($LinkerRand, $Zeile + 5, 290, $Zeile + 5);
    // Kontrollk�stchen

    $Ausdruck->_pdf->SetFont('Arial', '', 10);                // Schrift setzen

    $SQL = 'SELECT * ';
    $SQL .= ' FROM ARTIKELPRUEFUNGENINFOSTELLEN';
    $SQL .= ' WHERE AIS_STATUS = \'A\'';
    $SQL .= ' ORDER BY AIS_SORTIERUNG';

    $rsAIS = $DB->RecordSetOeffnen($SQL);

    $Zeile = 180;
    $Spalte = 0;

    $i = 0;
    while (!$rsAIS->EOF()) {
        $Ausdruck->_pdf->setXY($LinkerRand + $Spalte, $Zeile);
        $Ausdruck->_pdf->cell(5, 6, (strchr(';' . $rsAPR->FeldInhalt('APM_BENACHRICHTIGUNGEN') . ';', ';' . $rsAIS->FeldInhalt('AIS_KEY') . ';') == ''?'':'X'), 1, 0, 'R', 0);
        $Ausdruck->_pdf->cell(50, 6, $rsAIS->FeldInhalt('AIS_BEZEICHNUNG'), 0, 0, 'L', 0);

        $Spalte += 56;
        if ($Spalte > 240) {
            $Zeile += 6;
            $Spalte = 0;
        }
        $i++;
        $rsAIS->DSWeiter();
    }
    $Ausdruck->_pdf->setXY($LinkerRand + $Spalte, $Zeile + 1);
    $Ausdruck->_pdf->SetFont('Arial', 'b', 10);
    $Ausdruck->_pdf->cell(100, 5, 'Weitergeleitet am: ' . $Form->Format($rsAPR->FeldInhalt('APR_WEITERGELEITETAM')), 0, 0, 'l', 0);

    $Ausdruck->Anzeigen();
} catch (Exception $ex) {
}
?>