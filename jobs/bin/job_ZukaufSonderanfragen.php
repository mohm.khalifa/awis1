<?php

require_once('/daten/include/awisZukaufSonderanfragen.php');

try {
    $Benutzer = '';
    $DebugLevel = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--wanummer':
                $WANummer = $argv[++$i];
                break;
            case '--starttag':
                $StartTag = $argv[++$i];
                break;
            case '--endtag':
                $EndTag = $argv[++$i];
                break;
            case '--abfrage':
                $Abfrage = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $ZUK = new awisZukaufSonderanfragen($Benutzer,$DebugLevel);
    $ZUK->VerarbeiteSonderanfragen();


} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
}
?>
