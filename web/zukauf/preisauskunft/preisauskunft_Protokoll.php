<?php
/**
 * Protokoll aller Bestellungen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090623
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLB','%');
	$TextKonserven[]=array('ZLH','ZLH_BEZEICHNUNG');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('ZPA','ZUSTELLUNGSARTEN');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Fehler','err_KeinWert');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10010=$AWISBenutzer->HatDasRecht(10010);		// Rechte des Mitarbeiters
	if($Recht10010==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./preisauskunft_Main.php?cmdAktion=Protokoll>");

	$Form->Formular_Start();


	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XLB'));

	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY ZLB_USERDAT DESC';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$Param['FIL_ID']=(isset($_POST['sucFIL_ID'])?$_POST['sucFIL_ID']:$Param['FIL_ID']);
	$Param['ORDERBY']=$ORDERBY;
	$Param['BLOCK']=(isset($_REQUEST['Block'])?$_REQUEST['Block']:1);

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

	if($FilZugriff!='')
	{
		if(strpos($FilZugriff,',')!==false)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$Form->Erstelle_SelectFeld('*FIL_ID',$Param['FIL_ID'],150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			$Form->ZeileEnde();
			$AWISCursorPosition='sucFIL_ID';
		}
		elseif($Param['FIL_ID']=='')
		{
			$Param['FIL_ID']=$FilZugriff;
		}
	}
	else
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
		$Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,180,true,'','','','T','L','','',10);
		$Form->ZeileEnde();
		$AWISCursorPosition='sucFIL_ID';
	}

	//***********************************************************
	//* Filiale ausgew�hlt
	//***********************************************************
	if($Param['FIL_ID']!='')
	{
		$AWISBenutzer->ParameterSchreiben('Formular_XLB',serialize($Param));

		$Form->Trennzeile();

		$SQL = 'SELECT DISTINCT ZUKAUFLIEFERANTENBESTELLUNGEN.*, ZUB_KEY';
		$SQL .= ', LVK_EMAIL, LVK_NAME1';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		$SQL .= ' FROM ZUKAUFLIEFERANTENBESTELLUNGEN';
		$SQL .=' LEFT OUTER JOIN FilialenLieferanten ON FLI_LIE_NR = ZLB_LIE_NR AND FLI_FIL_ID = ZLB_FIL_ID';
		$SQL .=' LEFT OUTER JOIN LieferantenVerkaufshaeuser ON FLI_LVK_KEY = LVK_KEY';
//		$SQL .=' LEFT OUTER JOIN Zukaufbestellungen ON ZUB_ZLA_KEY = ZLB_ZLA_KEY AND ZUB_WANR = ZLB_WANR AND ZUB_BESTELLMENGE = ZLB_BESTELLMENGE';
		$SQL .=' LEFT OUTER JOIN Zukaufbestellungen ON ZUB_KEY = ZLB_ZUB_KEY';
//		$SQL .= ' WHERE FLI_GUELTIGAB <= TRUNC(SYSDATE) AND FLI_GUELTIGBIS >= TRUNC(SYSDATE)';
		if($Param['FIL_ID']==='*')
		{
			$SQL .= ' WHERE ZLB_USERDAT >= '.$DB->FeldInhaltFormat('D',date('d.m.Y',strtotime('today -14 days')));
		}
		else
		{
			$SQL .= ' WHERE ZLB_FIL_ID = 0'.$DB->FeldInhaltFormat('N0',$Param['FIL_ID']);
		}
		$SQL .= ' '.$ORDERBY;


//$Form->DebugAusgabe(1,$SQL);
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUB',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$ZUBLink = false;
		if($AWISBenutzer->HatDasRecht(10003)>0)
		{
			$ZUBLink = true;
		}

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
//$Form->DebugAusgabe(1,$SQL);
		$rsZLB = $DB->RecordSetOeffnen($SQL);
		if($rsZLB->EOF())
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['ZLB']['err_KeineBestellungen']);
			$Form->ZeileEnde();
		}
		else
		{
			$Form->ZeileStart();
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_WANR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_WANR'],130,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_USERDAT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_USERDAT'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_USERDAT'],100,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_BESTELLNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_BESTELLNUMMER'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_BESTELLNUMMER'],150,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_ARTIKELBEZEICHNUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_ARTIKELBEZEICHNUNG'],250,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_BESTELLMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_BESTELLMENGE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_BESTELLMENGE_KURZ'],100,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_LIEFERMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_LIEFERMENGE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_LIEFERMENGE_KURZ'],100,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_RUECKSTAND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_RUECKSTAND'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_RUECKSTAND_KURZ'],100,'',$Link);
			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ZLB_LIE_NR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLB_LIE_NR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLB']['ZLB_LIE_NR_KURZ'],140,'',$Link);
			$Form->ZeileEnde();

			$DS=0;
			while(!$rsZLB->EOF())
			{
				$Form->ZeileStart();
 				$Link = './preisauskunft_Drucken.php?ID='.base64_encode($rsZLB->FeldInhalt('ZLB_LIE_NR').'~'.$rsZLB->FeldInhalt('ZLB_WANR'));
				$Form->Erstelle_ListenFeld('*ZLB_WANR',$rsZLB->FeldInhalt('ZLB_WANR'),0,130,false,($DS%2),'',$Link,'T');
				$Form->Erstelle_ListenFeld('*ZLB_USERDAT',$rsZLB->FeldInhalt('ZLB_USERDAT'),0,100,false,($DS%2),'','','D','Z',$Form->Format('DU',$rsZLB->FeldInhalt('ZLB_USERDAT')));
				$Text = (strlen($rsZLB->FeldInhalt('ZLB_BESTELLNUMMER'))>18?substr($rsZLB->FeldInhalt('ZLB_BESTELLNUMMER'),0,16).'...':$rsZLB->FeldInhalt('ZLB_BESTELLNUMMER'));
				$Form->Erstelle_ListenFeld('*ZLB_BESTELLNUMMER',$Text,0,150,false,($DS%2),'','','T');
				$Text = awisWerkzeuge::awisGrossKlein(strtolower(strlen($rsZLB->FeldInhalt('ZLB_ARTIKELBEZEICHNUNG'))>28?substr($rsZLB->FeldInhalt('ZLB_ARTIKELBEZEICHNUNG'),0,26).'...':$rsZLB->FeldInhalt('ZLB_ARTIKELBEZEICHNUNG')));
				$Form->Erstelle_ListenFeld('*ZLB_ARTIKELBEZEICHNUNG',$Text,0,250,false,($DS%2),'','','T','',$rsZLB->FeldInhalt('ZLB_ARTIKELBEZEICHNUNG'));
				$Form->Erstelle_ListenFeld('*ZLB_BESTELLMENGE',$rsZLB->FeldInhalt('ZLB_BESTELLMENGE'),0,100,false,($DS%2),'','','N0');
				$Form->Erstelle_ListenFeld('*ZLB_LIEFERMENGE',$rsZLB->FeldInhalt('ZLB_LIEFERMENGE'),0,100,false,($DS%2),'','','N0');
				$Form->Erstelle_ListenFeld('*ZLB_RUECKSTAND',$rsZLB->FeldInhalt('ZLB_RUECKSTAND'),0,100,false,($DS%2),'','','N0');
				$Link = ($rsZLB->FeldInhalt('LVK_EMAIL')==''?'':'mailto:'.$rsZLB->FeldInhalt('LVK_EMAIL'));
				$ToolTipp = $rsZLB->FeldInhalt('LVK_NAME1');
				$Form->Erstelle_ListenFeld('*ZLB_LIE_NR',$rsZLB->FeldInhalt('ZLB_LIE_NR'),0,($ZUBLink?80:100),false,($DS%2),'',$Link,'T','Z',$ToolTipp);

				if($ZUBLink)
				{
					if($rsZLB->FeldInhalt('ZUB_KEY')!='')
					{
						$Link = '/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$rsZLB->FeldInhalt('ZUB_KEY');
						$Form->Erstelle_ListenBild('href','bestellung',$Link,'/bilder/icon_ok.png',$AWISSprachKonserven['ZLB']['txt_BestellungDa'],($DS%2));
					}
					elseif($rsZLB->FeldInhalt('ZLB_ZLA_KEY')=='')
					{
						$Form->Erstelle_ListenBild('href','nix','/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_WANR='.$rsZLB->FeldInhalt('ZLB_WANR'),'/bilder/icon_minus.png',$AWISSprachKonserven['ZLB']['txt_BestellungKeinKEY'],($DS%2));
					}
					else
					{
						$Form->Erstelle_ListenBild('href','nix','','/bilder/icon_warnung.png',$AWISSprachKonserven['ZLB']['txt_BestellungNochNichtDa'],($DS%2));
					}

				}
				$Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_info.png',utf8_decode($rsZLB->FeldInhalt('ZLB_STATUSTEXT')),($DS%2));

				// Versandart anzeigen
				$VersandartText = $Form->WerteListe($AWISSprachKonserven['ZPA']['ZUSTELLUNGSARTEN'],$rsZLB->FeldInhalt('ZLB_VERSANDART'));
				switch($rsZLB->FeldInhalt('ZLB_VERSANDART'))
				{
					case 2:
						$Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_lkw.png',$VersandartText ,($DS%2));
						break;
					case 1:
						$Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'info','','/bilder/icon_mensch.png',$VersandartText ,($DS%2));
						break;
				}

				// Best�tigungs-URL anzeigen (TROST,?)
				if((($Recht10010&16) == 16)  AND $rsZLB->FeldInhalt('ZLB_AUFTRAGSURL')!='')
				{
					$Form->Erstelle_ListenBild('href','nix',$rsZLB->FeldInhalt('ZLB_AUFTRAGSURL'),'/bilder/icon_pdf.png','',($DS%2));
				}
				$Form->ZeileEnde();

				$rsZLB->DSWeiter();
				$DS++;
			}

			$Link = './preisauskunft_Main.php?cmdAktion=Protokoll';
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		}

	}
	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	if($AWISBenutzer->ParameterLesen('Startseiten-Version')==1)
	{
		$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	}
	else
	{
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	}
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();


	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200923051807");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200923051808");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

$Form->Formular_Ende();
?>