<?php
	global $HTTP_POST_VARS;
	global $HTTP_GET_VARS;
	global $PHP_AUTH_USER;

	global $RechteStufe;

	$ListeZeigen=FALSE;
/***************************************************************************************************
* 
* Daten L�SCHEN
* 
****************************************************************************************************/	

	foreach($HTTP_POST_VARS as $Eintrag => $EintragWert)		
	{
		if(strpos($Eintrag,"cmdDelXSA_")!==FALSE)
		{
			$SQL = (substr($Eintrag,10));
			$SQL = substr($SQL,0,strlen($SQL)-2);
			$SQL = "DELETE FROM DatenAenderungen WHERE XSA_KEY=0" . $SQL;
			
			awisExecute($con, $SQL);

			$ListeZeigen = True;
			break;	// Nicht weiter suchen
		}


	   if(strpos($Eintrag,"cmdSpeichern")!==FALSE)
	   {

			If($HTTP_POST_VARS["txtXSA_KEY"]==0)
			{
				$SQL = "INSERT INTO DatenAenderungen(";
				$SQL .= "XSA_BEZEICHNUNG,XSA_XXX_KUERZEL,XSA_XFN_FELD,XSA_AUSWAHLSQL,XSA_KEYFELD,XSA_KEYVERGLEICH,XSA_AKTIV,XSA_USER,XSA_USERDAT)";
				$SQL .= "VALUES( '" . $HTTP_POST_VARS["txtXSA_BEZEICHNUNG"] . "'";
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSA_XXX_KUERZEL"] . "'";
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSA_XFN_FELD"] . "'";
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSA_AUSWAHLSQL"] . "'";
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSA_KEYFELD"] . "'";				
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSA_KEYVERGLEICH"] . "'";				
				$SQL .= ", " . $HTTP_POST_VARS["txtXSA_AKTIV"] . "";				
				$SQL .= ", '" . $PHP_AUTH_USER . "'";				
				$SQL .= ", SYSDATE)";				

				awisExecute($con, $SQL);
				$ListeZeigen=True;
			}
			else
			{
				$SQL = "UPDATE DatenAenderungen SET";
				$SQL .= " XSA_BEZEICHNUNG='" . $HTTP_POST_VARS["txtXSA_BEZEICHNUNG"] . "'";
				$SQL .= ", XSA_XXX_KUERZEL='" . $HTTP_POST_VARS["txtXSA_XXX_KUERZEL"] . "'";
				$SQL .= ", XSA_XFN_FELD='" . $HTTP_POST_VARS["txtXSA_XFN_FELD"] . "'";
				$SQL .= ", XSA_AUSWAHLSQL='" . $HTTP_POST_VARS["txtXSA_AUSWAHLSQL"] . "'";
				$SQL .= ", XSA_KEYFELD='" . $HTTP_POST_VARS["txtXSA_KEYFELD"] . "'";				
				$SQL .= ", XSA_KEYVERGLEICH='" . $HTTP_POST_VARS["txtXSA_KEYVERGLEICH"] . "'";				
				$SQL .= ", XSA_AKTIV=" . $HTTP_POST_VARS["txtXSA_AKTIV"] . "";				
				$SQL .= ", XSA_USER='" . $PHP_AUTH_USER . "'";				
				$SQL .= ", XSA_USERDAT=SYSDATE";
				$SQL .= " WHERE XSA_KEY=" . intval($HTTP_POST_VARS["txtXSA_KEY"]) . "";

				awisExecute($con, $SQL);
				$ListeZeigen=True;
			}

			break;
	   }

	}	
	
/***************************************************************************************************
* 
* Daten anzeigen
* 
****************************************************************************************************/	

	$SQL = "SELECT * ";
	$SQL .= " FROM DATENAENDERUNGEN, FELDNAMEN, TABELLENKUERZEL";
	$SQL .= " WHERE XSA_XFN_FELD = XFN_FELD AND XSA_XXX_KUERZEL = XXX_KUERZEL ";
	if($HTTP_GET_VARS["txtXSA_KEY"] != '')
	{
		$SQL .= " AND XSA_KEY=" . $HTTP_GET_VARS["txtXSA_KEY"];
	}
	$SQL .= " ORDER BY XSA_Aktiv";
	
	$rsDatenAenderung = awisOpenRecordset($con, $SQL);
	$rsDatenAenderungZeilen=$awisRSZeilen;

		// Feldnamen holen
	$Felder = array_Keys($rsDatenAenderung);
	for($i=0;$i<sizeof($Felder);$i++)
	{
		$FeldListe[$i] = $Felder[$i];
	}
	$FeldListe = awis_FeldNamenListe($con, $FeldListe, $SatzNr = 1);
	

	If(($HTTP_GET_VARS["txtXSA_KEY"]=='' AND $HTTP_POST_VARS["txtXSA_KEY"]=='') OR $ListeZeigen)
	{
		// �berschrift aufbauen
	
		print "<form method=post name=frmDatenAenderungen action=./aenderungen_Main.php?CmdAktion=DatenAenderungen>";
		
		print "<table  width=100% id=DatenTabelle border=1><tr>";
		
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSA_BEZEICHNUNG"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XFN_BEZEICHNUNG"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XFN_BEMERKUNG"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XXX_TABELLENNAME"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSA_AUSWAHLSQL"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSA_KEYFELD"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSA_KEYVERGLEICH"][0] . "</span></td>";	
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSA_AKTIV"][0] . "</span></td>";	
		print "<td></td>";
		
		print "</tr>";
		

		for($i=0;$i<$rsDatenAenderungZeilen;$i++)
		{
			print "<tr>";
	
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
			print "<input type=hidden name=txtXSA_KEY value=" . $rsDatenAenderung["XSA_KEY"][$i] . ">";
			print $rsDatenAenderung["XSA_BEZEICHNUNG"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XFN_BEZEICHNUNG"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XFN_BEMERKUNG"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XXX_TABELLENNAME"][$i] . "</td>";			
			if(($RechteStufe & 16)==16)
			{
				print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a target=NeuesFenster href=./SQL_Test.php?SQL=" . str_replace(" ","%20",$rsDatenAenderung["XSA_AUSWAHLSQL"][$i]) . ">SELECT...</a></td>";			
			}
			else
			{
				print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">SELECT...</td>";			
			}
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XSA_KEYFELD"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XSA_KEYVERGLEICH"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . ($rsDatenAenderung["XSA_AKTIV"][$i]==0?'Nein':'Ja') . "</td>";			
	
			if($RechteStufe > 1)
			{
				print "<td>";
			}

			if(($RechteStufe & 2)==2)
			{
	        	print "<input type=image alt='Feld �ndern' src=/bilder/aendern.png name=cmdAendern_" . $rsDatenAenderung["XSA_KEY"][$i] . " onclick=submit();>";
			}
	
			if(($RechteStufe & 8)==8)
			{
	        	print "<input type=image alt='Feld l�schen' src=/bilder/muelleimer.png name=cmdDelXSA_" . $rsDatenAenderung["XSA_KEY"][$i] . " onclick=submit();>";
			}

			if($RechteStufe > 1)
			{
				print "</td>";
			}

			print "</tr>";
		}
		
		print "</table>";
	}
	else			// Daten bearbeiten
	{
	
		print "<form method=post name=frmDatenAenderungen action=./aenderungen_Main.php?CmdAktion=DatenAenderungen>";
		
		print "<table  width=100% id=DatenTabelle border=1>";

			// Bezeichnung		
		print "\n\r<tr>";
		print "<td><input name=txtXSA_KEY type=HIDDEN Value=0" . $rsDatenAenderung["XSA_KEY"][0] . "<span class=FeldBez>" . $FeldListe["XSA_BEZEICHNUNG"][0] . "</span></td>";
		print "<td><input name=txtXSA_BEZEICHNUNG type=Text size=60 Value=" . $rsDatenAenderung["XSA_BEZEICHNUNG"][0] . "></td>";
		print "</tr>";

			// Tabelle
		print "\n\r<tr>";
		print "<td><span class=FeldBez>" . $FeldListe["XSA_XXX_KUERZEL"][0] . "</span></td>";
		print "<td><select name=txtXSA_XXX_KUERZEL>";
		$rsHilfsTab = awisOpenRecordset($con, "SELECT * FROM TABELLENKUERZEL WHERE XXX_AENDERBAR=1");
		for($j=0;$j<$awisRSZeilen;$j++)
		{
			print "<option ";
			if($rsDatenAenderung["XSA_BEZEICHNUNG"][0]==$rsHilfsTab["XXX_KUERZEL"][$j])
			{
				print " selected ";
			}
			print " value=" . $rsHilfsTab["XXX_KUERZEL"][$j] . ">" . $rsHilfsTab["XXX_TABELLENNAME"][$j] . "</option>";
		}
		print "</select>";
		print "</tr>";
		unset($rsHilfsTab);

			// Zu �nderndes Feld
		print "\n\r<tr>";
		print "<td>" . $FeldListe["XSA_XFN_FELD"][0] . "</span></td>";

		print "<td><select name=txtXSA_XFN_FELD>";
		$rsHilfsTab = awisOpenRecordset($con, "SELECT XFN_FELD, XFN_BEZEICHNUNG FROM FELDNAMEN, TABELLENKUERZEL WHERE SUBSTR(XFN_FELD,1,3) = XXX_KUERZEL AND XXX_AENDERBAR=1");
		for($j=0;$j<$awisRSZeilen;$j++)
		{
			print "<option ";
			if($rsDatenAenderung["XSA_XFN_FELD"][0]==$rsHilfsTab["XSA_XFN_FELD"][$j])
			{
				print " selected ";
			}
			print " value=" . $rsHilfsTab["XFN_FELD"][$j] . ">" . $rsHilfsTab["XFN_FELD"][$j] . "</option>";
		}
		print "</select>";
		print "</tr>";
		unset($rsHilfsTab);	

			// SQL f�r die Auswahlliste der Daten
			
		print "\n\r<tr>";
		print "<td>" . $FeldListe["XSA_AUSWAHLSQL"][0] . "</span></td>";
		print "<td><input name=txtXSA_AUSWAHLSQL type=Text size=60 Value='" . $rsDatenAenderung["XSA_AUSWAHLSQL"][0] . "'>";

		print "</tr>";

			// Prim�rschl�ssel-Feld
		print "\n\r<tr>";
		print "<td>" . $FeldListe["XSA_KEYFELD"][0] . "</span></td>";
		print "<td><input name=txtXSA_KEYFELD type=Text size=20 Value=" . $rsDatenAenderung["XSA_KEYFELD"][0] . "></td>";
		print "</tr>";


				// Operator
		print "\n\r<tr>";
		print "<td>" . $FeldListe["XSA_KEYVERGLEICH"][0] . "</span></td>";
		print "<td><select name=txtXSA_KEYVERGLEICH type=Text size=1>";
		print "<option " . ($rsDatenAenderung["XSA_KEYVERGLEICH"][0]=="="?'Selected':'') . " value='='>=</option>";
		print "<option " . ($rsDatenAenderung["XSA_KEYVERGLEICH"][0]==">="?'Selected':'') . " value='>='>>=</option>";
		print "<option " . ($rsDatenAenderung["XSA_KEYVERGLEICH"][0]=="<="?'Selected':'') . " value='<='><=</option>";
		print "<option " . ($rsDatenAenderung["XSA_KEYVERGLEICH"][0]=="<>"?'Selected':'') . " value='<>'><></option>";
		print "</select></td>";
		print "</tr>";

		
					// XSA_AKTIV
		print "\n\r<tr>";
		print "<td>" . $FeldListe["XSA_AKTIV"][0] . "</span></td>";
		print "<td><select name=txtXSA_AKTIV type=Text size=1>";
		print "<option " . ($rsDatenAenderung["XSA_AKTIV"][0]=="0"?'Selected':'') . " value=0>Nein</option>";
		print "<option " . ($rsDatenAenderung["XSA_AKTIV"][0]!="0"?'Selected':'') . " value=1>Ja</option>";
		print "</select></td>";
		print "</tr>";
	
		print "</table>";

        print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}	
	
	print "</form>";
	unset($rsDatenAenderung);
	
?>