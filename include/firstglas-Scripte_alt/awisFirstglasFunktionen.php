<?php

require_once('awisDatenbank.inc');
require_once('awisFirstglasFunktionen.php');
require_once('awisBenutzer.inc');

class FirstglasFunktionen
{

private $_DB;
protected $KFZKennzeichen;
protected $SB;
protected $bol_VorgangNr = false;

function __construct()
{
    $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
    $this->_DB->Oeffnen();
}

public function checkNumber($Number)
{
    $bol_checkNumber = false;

    //$Number = str_replace('.', ',', $Number);

    if ($Number !='')
    {
       if (is_numeric($Number) == true)
       //if(preg_match('/^[0-9]*[.][0-9]*$/',$Number) || preg_match('/^[0-9]*/',$Number))
        {
            /*
            echo "<br>";
            echo "Nummer:".$Number;
            echo "<br>";
            */
            $bol_checkNumber = true;
            return $bol_checkNumber;

        }
        else
        {
            //echo "Keine Nummer";

            $bol_checkNumber = false;
            return $bol_checkNumber;
        }
    }
    else
    {
        //echo "Kein R�ckgabewert";
    }
}





public function pruefeSB($SB)
{
    if ($SB !='' || $SB != 0)
    {
        //echo "SB vor ver�nderung".$SB;
        $SB = str_replace('.', ',', $SB);
        
        if(preg_match('/^[0-9]*[,][0-9]*$/',$SB) || preg_match('/^[0-9]*/',$SB))
        {
           $this->SB = $SB;
        }
        else
        {
           $this->SB = '-1';
        }

    }
    else
    {
        $this->SB = '-1';
    }

    return $this->SB;

}

public function pruefeVorgangNr($vorgangNr)
{
    if (strlen($vorgangNr) == 13)
    {
        $this->bol_VorgangNr = true;
        return $this->bol_VorgangNr;
    }
    else
    {
        $this->bol_VorgangNr = false;
        return $this->bol_VorgangNr;
    }
}



public function wandleKFZKennzeichen($KFZKennzeichen)
{
  $dummy = $KFZKennzeichen;
  //$dummy = 'HA-OH\2435';
  $pos = '';
  $anz = substr_count($dummy, ' ');
  
  for($element=0;$element<$anz;$element++)
  {
      $pos = '';
      $pos = strpos($dummy, ' ');
      $dummy = substr($dummy,0,$pos).substr($dummy,$pos+1);
      //echo "GEWANDELTESKFZKENNZEICHEN:".$dummy;
      //echo "FUNDSTELLE: ".$pos;
  }

  $anz='';
  $anz = substr_count($dummy, '-');

  for($element=0;$element<$anz;$element++)
  {
      $pos = '';
      $pos = strpos($dummy, '-');
      $dummy = substr($dummy,0,$pos).substr($dummy,$pos+1);
      //echo "GEWANDELTESKFZKENNZEICHEN:".$dummy;
      //echo "FUNDSTELLE: ".$pos;
  }

  $anz='';
  $anz = substr_count($dummy, '_');

  for($element=0;$element<$anz;$element++)
  {
      $pos = '';
      $pos = strpos($dummy, '_');
      $dummy = substr($dummy,0,$pos).substr($dummy,$pos+1);
      //echo "GEWANDELTESKFZKENNZEICHEN:".$dummy;
      //echo "FUNDSTELLE: ".$pos;
  }

  $anz='';
  $anz = substr_count($dummy, '/');

  for($element=0;$element<$anz;$element++)
  {
      $pos = '';
      $pos = strpos($dummy, '/');
      $dummy = substr($dummy,0,$pos).substr($dummy,$pos+1);
      //echo "GEWANDELTESKFZKENNZEICHEN:".$dummy;
      //echo "FUNDSTELLE: ".$pos;
  }

  $anz='';
  $anz = substr_count($dummy, '\\');

  for($element=0;$element<$anz;$element++)
  {
      $pos = '';
      $pos = strpos($dummy, '\\');
      $dummy = substr($dummy,0,$pos).substr($dummy,$pos+1);
      //echo "GEWANDELTESKFZKENNZEICHEN:".$dummy;
      //echo "FUNDSTELLE: ".$pos;
  }

  $this->KFZKennzeichen = $dummy;
  
  return $this->KFZKennzeichen;

}

//-----------------------------
//-Export Tabzugang - Firstglas
//-----------------------------

//Dateiname muss �bergeben werden
//-----------------------------------------------------

public function csv_ExportKassendatenStorniert($Dateiname)
{
   $AWISBenutzer = awisBenutzer::Init();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $DB->Oeffnen();

   //$Pf = '';
   //$Pfad = '';
   $Datein = $Dateiname;

   //$Datein = "KassendatenStorniert.csv";


   $SQL = 'Select * from FKASSENDATEN_STORNIERT WHERE FKS_EXPORTIERT IS NULL OR FKS_EXPORTIERT = 0 ORDER BY FKS_KEY ASC';
   
    if($Pfad!='')
   {
      $Pf = $Pfad;
   }
   else
   {
      //Default - Pfad
      $Pf = '/daten/daten/versicherungen/firstglas/Export/Storno/';
      //$Dateiname = 'TEST.csv';
   }


    if($SQL!='')
   {

       //---EXPORT DER TABELLE TABZUGANG
       $rsStorno = $DB->RecordSetOeffnen($SQL);

       if(($fd = fopen($Pf.$Datein,'w')) === false)
	   {
	      echo $this->error_msg = "Fehler beim Erstellen der Datei";
	   }
	   else
	   {
           while(!$rsStorno->EOF())
           {
             $i = 0;
             $List = array();

             $List[0] = $rsStorno->FeldInhalt('FKS_DATUMSTORNIERT');
             $List[1] = $rsStorno->FeldInhalt('FKS_FIRSTGLASSNR');
             

             $Line = array($List[0].';'.$List[1].chr(13).chr(10));

             foreach($Line as $Test)
             {
                fwrite($fd,$Test);

                $BindeVariablen=array();
                $BindeVariablen['var_N0_fks_exportiert']=intval(1);
                $BindeVariablen['var_N0_fks_key']=$rsStorno->FeldInhalt('FKS_KEY');
                
                $SQL = 'UPDATE FKASSENDATEN_STORNIERT SET ';
                $SQL .= ' FKS_EXPORTIERT=:var_N0_fks_exportiert';
                $SQL .= ' WHERE FKS_KEY=:var_N0_fks_key';

                if($DB->Ausfuehren($SQL,'',false,$BindeVariablen)===false)
                {
                }
             }

             $rsStorno->DSWeiter();
           }
           fclose($fd);
       }
   }

}



public function csv_ExportZugang($Dateiname)
{
   $AWISBenutzer = awisBenutzer::Init();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $DB->Oeffnen();

   $Pf = '';
   $Pfad = '';
   
   $Tag = '';
   $Monat = '';
   $Jahr = '';
   $wandleDatum = '';
   $gewDatum = '';
   $len = 0;
   //-------------------------------------------------------------------------------------
   //Nur die Daten an Zugang schreiben - die noch nicht Exportiert wurden
   //-------------------------------------------------------------------------------------

   $SQL = 'Select * from FGZUGANG WHERE FGZ_NAMEEXPORTDATEI IS NULL ORDER BY FGZ_KEY ASC';

   if($Pfad!='')
   {
      $Pf = $Pfad;
   }
   else
   {
      //Default - Pfad
      $Pf = '/daten/daten/versicherungen/firstglas/Zugang/';
      $PfNeu = '/daten/daten/pccommon/glas/';
      //$Dateiname = 'TEST.csv';
   }

   if($SQL!='')
   {

       //---EXPORT DER TABELLE TABZUGANG
       $rsZugang = $DB->RecordSetOeffnen($SQL);

       if(($fd = fopen($Pf.$Dateiname,'w')) === false)
	   {
	      echo $this->error_msg = "Fehler beim Erstellen der Datei";
	   }
	   else
	   {
           while(!$rsZugang->EOF())
           {
             $i = 0;
             $List = array();

             $List[0] = $rsZugang->FeldInhalt('FGZ_EXPFELD1');
             $List[1] = $rsZugang->FeldInhalt('FGZ_EXPFELD2');
             $List[2] = $rsZugang->FeldInhalt('FGZ_EXPFELD3');
             $List[3] = $rsZugang->FeldInhalt('FGZ_EXPFELD4');
             $List[4] = $rsZugang->FeldInhalt('FGZ_EXPFELD5');
             $List[5] = $rsZugang->FeldInhalt('FGZ_EXPFELD6');
             $List[6] = $rsZugang->FeldInhalt('FGZ_EXPFELD7');
             $List[7] = $rsZugang->FeldInhalt('FGZ_EXPFELD8');
             $List[8] = $rsZugang->FeldInhalt('FGZ_EXPFELD9');
             $List[9] = $rsZugang->FeldInhalt('FGZ_EXPFELD10');
             $List[10] = $rsZugang->FeldInhalt('FGZ_EXPFELD11');
             $List[11] = $rsZugang->FeldInhalt('FGZ_EXPFELD12');

             //Wandle Datum Zugang
             //-------------------

             $wandleDatum = substr($List[10], 0, 10);
             $Jahr = substr($wandleDatum,6,10);
             $Monat = substr($wandleDatum,3,2);
             $Tag = substr($wandleDatum,0,2);
             $gewDatum = $Jahr."-".$Monat."-".$Tag;
             $len = 0;
             $len = strlen($gewDatum);
             if ($len == 10)
             {
                $List[10] = $gewDatum;
             }
             else
             {
                 $List[10] = '';
             }
             $List[12] = $rsZugang->FeldInhalt('FGZ_EXPFELD13');

             $Line = array($List[0].';'.$List[1].';'.$List[2].';'.$List[3].';'.$List[4].';'.$List[5]
             .';'.$List[6].';'.$List[7].';'.$List[8].';'.$List[9].';'.$List[10].';'.$List[11].';'.$List[12].chr(13).chr(10));

             foreach($Line as $Test)
             {

                fwrite($fd,$Test);


                //F�hre Update aus
                //--------------------------------------------------------------

                $SQL = 'UPDATE FGZUGANG SET ';
                $SQL .= ' FGZ_EXPFELD1=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD1"),false);
                $SQL .= ',FGZ_EXPFELD2=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD2"),false);
                $SQL .= ',FGZ_EXPFELD3=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD3"),false);
                $SQL .= ',FGZ_EXPFELD4=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD4"),false);
                $SQL .= ',FGZ_EXPFELD5=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD5"),false);
                $SQL .= ',FGZ_EXPFELD6=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD6"),false);
                $SQL .= ',FGZ_EXPFELD7=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD7"),false);
                $SQL .= ',FGZ_EXPFELD8=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD8"),false);
                $SQL .= ',FGZ_EXPFELD9=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD9"),false);
                $SQL .= ',FGZ_EXPFELD10=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD10"),false);
                $SQL .= ',FGZ_EXPFELD11=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD11"),false);
                $SQL .= ',FGZ_EXPFELD12=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD12"),false);
                $SQL .= ',FGZ_EXPFELD13=' . $DB->FeldInhaltFormat('T',$rsZugang->FeldInhalt("FGZ_EXPFELD13"),false);
                $SQL .= ',FGZ_NAMEEXPORTDATEI=' . $DB->FeldInhaltFormat('T',$Dateiname,false);
                $SQL .= ',FGZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',FGZ_USERDAT=sysdate';
                $SQL .= ' WHERE FGZ_KEY='. $DB->FeldInhaltFormat('NO',$rsZugang->FeldInhalt('FGZ_KEY'),false);

                if($DB->Ausfuehren($SQL)===false)
                {
                }
             }

             $rsZugang->DSWeiter();
           }
           fclose($fd);
           copy($Pf.$Dateiname, $PfNeu.$Dateiname);
       }
   }
   else
   {
        echo 'Fehler: SQL f�r DS-Select fehlt';
   }
}


public function csv_FirtglasExport($Dateiname)
{
   $AWISBenutzer = awisBenutzer::Init();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $DB->Oeffnen();

   $Pf = '';
   $Pfad = '';
   $Datein = $Dateiname;

   $SQL = "Select * from FGFREIGABERECHNUNGEN WHERE FGR_NAMEEXPORTDATEI IS NULL";



   if($Pfad!='')
   {
      $Pf = $Pfad;
   }
   else
   {
      //Default - Pfad
      $Pf = '/daten/daten/versicherungen/firstglas/FirstglasExport/';
      //$Dateiname = 'TEST.csv';
   }

   if($SQL!='')
   {

       //---EXPORT DER TABELLE TABZUGANG
       $rsZugang = $DB->RecordSetOeffnen($SQL);

       //var_dump($rsZugang);

       //die();

       if(($fd = fopen($Pf.$Datein,'w')) === false)
       {
	      echo $this->error_msg = "Fehler beim Erstellen der Datei";
       }
       else
       {
           while(!$rsZugang->EOF())
           {
             $i = 0;
             $List = array();

             $List[0] = $rsZugang->FeldInhalt('FGR_KASSIERDATUM');
             $List[1] = $rsZugang->FeldInhalt('FGR_VORGANGNR');
             $List[2] = $rsZugang->FeldInhalt('FGR_SELBSTZAHLER');
            
             
                 $Line = array($List[0].';'.$List[1].';'.$List[2].chr(13).chr(10));

                 //echo "Start FirstglassExport\n";
                 //var_dump($Line);

                 
                     foreach($Line as $Test)
                     {

                        fwrite($fd,$Test);

                        $BindeVariablen=array();
                        $BindeVariablen['var_T_fgr_nameexportdatei']=$Datein;
                        $BindeVariablen['var_N0_fgr_key']=$rsZugang->FeldInhalt('FGR_KEY');
                        
                        $SQL = 'UPDATE FGFREIGABERECHNUNGEN SET ';
                        $SQL .= " FGR_NAMEEXPORTDATEI=:var_T_fgr_nameexportdatei";
                        $SQL .= " WHERE FGR_KEY=:var_N0_fgr_key";

                        //echo $SQL;

                        if($DB->Ausfuehren($SQL,'',false,$BindeVariablen)===false)
                        {
                        }

                        //die();
                 }
            

             $rsZugang->DSWeiter();
           }
           fclose($fd);
       }
   }
}


public function SchreibeModulStatus($Modulname,$Statustext)
{
        $BindeVariablen=array();
        $BindeVariablen['var_T_vjs_modulstatus']=$Statustext;
        $BindeVariablen['var_T_vjs_modul']=$Modulname;
        
		$SQL = 'UPDATE versjobsteuerung ';
        $SQL .= 'SET vjs_modulstatus = :var_T_vjs_modulstatus ';
        $SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';

        if($this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen)===false)
        {
            
            //throw new awisException('',,$SQL,2);
        }
}

    public function SchreibeStartZeit($Modulname)

    {
        $BindeVariablen=array();
        $BindeVariablen['var_T_vjs_modul']=$Modulname;
        
    	$this->_SQL = 'UPDATE versjobsteuerung ';

        $this->_SQL .= 'SET vjs_startzeit = sysdate, ';

        $this->_SQL .= 'vjs_endezeit = null, ';

        $this->_SQL .= 'vjs_modulstatus = null ';

        $this->_SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';

        if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)

        {

            //throw new awisException('',,$SQL,2);

        }

    }
    /*
    public function SchreibeEndeZeit($Modulname)
    {

        $this->_SQL = 'UPDATE versjobsteuerung ';
        //$this->_SQL .= 'SET vjs_endezeit = sysdate, ';
        $this->_SQL .= 'SET vjs_endezeit = sysdate ';
        //$this->_SQL .= 'vjs_jobid = '.$this->_JobID.' ';
        $this->_SQL .= 'WHERE vjs_modul = \''.$Modulname.'\'';

        if($this->_DB->Ausfuehren($this->_SQL)===false)
        {
            //throw new awisException('',,$SQL,2);
        }

    }
    */

    public function SchreibeEndeZeit($Modulname)
    {

        $BindeVariablen=array();
        $BindeVariablen['var_T_vjs_modul']=$Modulname;
        
    	$this->_SQL = 'UPDATE versjobsteuerung ';
        $this->_SQL .= 'SET vjs_endezeit = sysdate ';
        $this->_SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';

        if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
        {
            //throw new awisException('',,$SQL,2);
        }

        $this->ErmittleScriptLaufzeit($Modulname);
    }



    public function ErmittleScriptLaufzeit($Modulname)
    {

        $BindeVariablen=array();
        $BindeVariablen['var_T_vjs_modul']=$Modulname;
        
    	$this->_SQL = 'SELECT vjs_startzeit, vjs_endezeit ';

        $this->_SQL .= 'FROM versjobsteuerung ';

        $this->_SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';



        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,'',false,$BindeVariablen) > 0)

        {

            $rsZeit = $this->_DB->RecordSetOeffnen($this->_SQL);

            $timeStartzeit = strtotime($rsZeit->FeldInhalt('VJS_STARTZEIT'));

            $timeEndezeit = strtotime($rsZeit->FeldInhalt('VJS_ENDEZEIT'));

            $timeDifferenz = $timeEndezeit - $timeStartzeit;



            $this->_SQL = 'UPDATE versjobsteuerung ';

            $this->_SQL .= 'SET vjs_laufzeit = \''.$timeDifferenz.'\' ';

            $this->_SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';



            if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)

            {

                //throw new awisException('',,$SQL,2);

            }

        }



    }














}//Ende der Klasse
?>
