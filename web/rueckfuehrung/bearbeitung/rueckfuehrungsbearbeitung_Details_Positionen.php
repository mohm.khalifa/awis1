<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RFF','%');
	$TextKonserven[]=array('ASP','ASP_BEZEICHNUNG');
	$TextKonserven[]=array('FIB','FIB_BESTAND');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Rueckfuehrungstyp');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$AWISSprache = $AWISBenutzer->BenutzerSprache();

	if($AWISSprache=='')
	{
		$AWISSprache='DE';
	}

	$Recht3920 = $AWISBenutzer->HatDasRecht(3920);
	if($Recht3920==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfk_key']='0'.$AWIS_KEY1;
		
	$SQL = 'SELECT RFS_STATUS FROM RUECKFUEHRUNGSFILIALMENGENKOPF ';
	$SQL.= ' LEFT JOIN RUECKFUEHRUNGSSTATUS ON (RFK_RFS_KEY = RFS_KEY)';
	$SQL.= ' WHERE RFK_KEY = :var_N0_rfk_key';
	
	$rsRFKStatus = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	$rsRFKStatusZeilen = $rsRFKStatus->AnzahlDatensaetze();
	$Form->DebugAusgabe(1,$_POST,$_GET);
	//********************************************************
	// Daten suchen
	//********************************************************

	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfk_key']='0'.$AWIS_KEY1;
	$BindeVariablen['var_T_AWISSprache']=$AWISSprache;
	
//	$SQL = 'SELECT distinct RueckFuehrungsFilialMengenKopf.*, RueckFuehrungsFilialMengenPos.*, RFS_STATUS, ASP_BEZEICHNUNG, RUECKLIEFKZ';
	$SQL = 'SELECT distinct RueckFuehrungsFilialMengenKopf.*, RueckFuehrungsFilialMengenPos.*, RFS_STATUS, FUNC_ASP_BEZEICHNUNG(RFF_AST_ATUNR,:var_T_AWISSprache) AS ASP_BEZEICHNUNG, RUECKLIEFKZ';
	$SQL .= ", (SELECT RFT_BEZEICHNUNG FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG=NVL(RFF_RUECKLIEFKZ,' ')) AS RUECKLIEFKZBEZ";
	$SQL .= ", (SELECT RFT_WWS_BUCHUNGSKENNZIFFER FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG=NVL(RFF_RUECKLIEFKZ,' ')) AS RFT_WWS_BUCHUNGSKENNZIFFER";
	$SQL .= ', row_number() over (order by 1) AS ZeilenNr';
	$SQL .= ' FROM RueckFuehrungsFilialMengenKopf';
	$SQL .= ' LEFT JOIN RueckFuehrungsFilialMengenPos ON RFK_KEY = RFF_RFK_KEY';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSSTATUS RFS ON RFK_RFS_KEY = RFS_KEY';
//	$SQL .= ' LEFT JOIN ARTIKELSPRACHEN ON ASP_AST_ATUNR = RFF_AST_ATUNR AND ASP_LAN_CODE = \''.$AWISSprache.'\'';
	$SQL .= ' LEFT JOIN V_ARTIKEL_RUECKLIEFKZ ON RFF_AST_ATUNR = AST_ATUNR AND GUELTIG=0';
	$SQL .= ' WHERE RFK_KEY=:var_N0_rfk_key';

	
	if(isset($_GET['RFF_KEY']))
	{
		$BindeVariablen['var_N0_rff_key']=intval($_GET['RFF_KEY']);
		
		$SQL .= ' AND RFF_KEY = :var_N0_rff_key';		
	}
	
	if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
	{
		$BindeVariablen['var_N0_rff_key']='0'.intval($AWIS_KEY2);
		
		$SQL .= ' AND RFF_KEY= :var_N0_rff_key';
	}
	
	if((($Recht3920&32)!=0 AND $DB->FeldInhaltFormat('Z',$rsRFKStatus->FeldInhalt('RFS_STATUS'))>=31)
		and !isset($_POST['txtAlleAnzeigen']) and !isset($_GET['RFF_KEY']))
	{
			if ($DB->FeldInhaltFormat('Z',$rsRFKStatus->FeldInhalt('RFS_STATUS'))==31)
			{
				$SQL.= ' AND ((RFF_MENGEFILIALE <> RFF_MENGEZENTRALE)';			
				$SQL.= ' OR (RFF_MENGEZENTRALE > ';
				//$SQL.= '(SELECT NVL((NVL(FIB_BESTAND,0)-(NVL(FBB_RESERVIERT,0))),0) as BESTAND';
				$SQL.= '(SELECT (NVL(FIB_BESTAND,0)) as BESTAND';
				$SQL.= ' FROM ARTIKELSTAMM';
				$SQL.= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENKOPF ON RFK_KEY = :var_N0_rfk_key';
				$SQL.= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = AST_ATUNR AND FIB_FIL_ID = RFK_FIL_ID';
				//$SQL.= ' LEFT JOIN FILIALBESTANDBEWEGUNG ON FIB_AST_ATUNR = FBB_AST_ATUNR AND FIB_FIL_ID = FBB_FIL_ID';
				$SQL.= ' WHERE AST_ATUNR = RFF_AST_ATUNR)))';										
			}
			
			elseif ($DB->FeldInhaltFormat('Z',$rsRFKStatus->FeldInhalt('RFS_STATUS'))>=32)
			{
				$SQL.= ' AND (RFF_MENGEFILIALE <> RFF_MENGEZENTRALE)';			
			}
	}	
		
	if(!isset($_GET['RFFSort']))
	{
		$SQL .= ' ORDER BY RFF_AST_ATUNR';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['RFFSort']);
	}
	//echo $SQL;
	$Form->DebugAusgabe(1,$SQL);
	$rsRFF = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	//PG: W Artikel m�ssen immer einzeln auf der R�ckf�hrung stehen 
	$SQLWPruefung = 'Select distinct upper(rueckliefkz) as rueckliefkz from (' . $SQL . ')'; //Mit am SQL h�ngen.
    
	//echo $SQLWPruefung;
	
	//echo $SQLWPruefung;
	$rsWPruefung = $DB->RecordSetOeffnen($SQLWPruefung,$BindeVariablen);

	
	//Wenn mehr als ein RF Typ, muss gepr�ft werden, ob ein W (Altwarenverwertung) Artikel dabei ist.
	if(($rsWPruefung->AnzahlDatensaetze())>1)
	{
	   
	    $WArtikel = false; 
	    while (!$rsWPruefung->EOF())
	    {
	       //echo $rsWPruefung->FeldInhalt('RUECKLIEFKZ');
	        if($rsWPruefung->FeldInhalt('RUECKLIEFKZ') == 'W')
	       {
	           $WArtikel = True;
	       }
	       $rsWPruefung->DSWeiter();	       
	    }
	    if ($WArtikel === true)
	    {
	        $SQLWArtikel = 'Select RFF_AST_ATUNR from ( ' . $SQL. ') where NVL(upper(RUECKLIEFKZ),\'bla}\') NOT Like \'W\' ';
            //echo $$SQLWArtikel;
	        $rsWArtikel = $DB->RecordSetOeffnen($SQLWArtikel,$BindeVariablen);
	        //echo $SQLWArtikel;
	        
	        $Artikel='';
	        while (!$rsWArtikel->EOF())
	        {
	            $Artikel .= $rsWArtikel->FeldInhalt('RFF_AST_ATUNR') . ','; 
	            $rsWArtikel->DSWeiter();
	        }
	        
	        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_RueckfuehrungWArtikel']. ' ' . $Artikel) ;
	    }
	    
	}

	if(!isset($_GET['RFF_KEY']))//$rsRFF->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$Form->Formular_Start('font-size:90%;');

		$Form->ZeileStart();
				
		$EditRecht = false;	
		$EditRechtFiliale=false;
		$EditRechtZentrale=false;
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale) hat und der Status 30 (R�ckf�hrung erstellt) ist
		if (($Recht3920&16)!=0 AND $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 30)
		{
			$EditRechtFiliale=true;		
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Zentrale) hat und der Status 31 (R�ckf�hrung - abgeschlossen - Filiale) ist
		if (($Recht3920&32)!=0 AND $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 31)
		{
			$EditRechtZentrale=true;
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale und Zentrale) 
		if (($Recht3920&64)!=0)
		{
			if ($DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) < 32)
			{
				$EditRechtFiliale=true;
				$EditRechtZentrale=true;
			}
			else 
			{
				$EditRechtFiliale=false;
				$EditRechtZentrale=false;
			}
		}							
		
		$Icons = array();
		if(($Recht3920&6)>0 AND ($EditRechtFiliale==true or $EditRechtZentrale==true))
		{
			$Icons[] = array('new','./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen&RFF_KEY=0');			
		}
		$Form->Erstelle_ListeIcons($Icons,38,-1,$AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);
				
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFFSort=RFF_AST_ATUNR'.((isset($_GET['RFFSort']) AND ($_GET['RFFSort']=='RFF_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'],70,'',$Link);
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFFSort=ASP_BEZEICHNUNG'.((isset($_GET['RFFSort']) AND ($_GET['RFFSort']=='ASP_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASP']['ASP_BEZEICHNUNG'],325,'',$Link);		
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen';
		$Link .= '&RFFSort=RFF_MENGEFILIALE'.((isset($_GET['RFFSort']) AND ($_GET['RFFSort']=='RFF_MENGEFILIALE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'],80,'',$Link);
		
		if ($EditRechtZentrale == true or $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) >= 32)
		{
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen';
			$Link .= '&RFFSort=RFF_MENGEZENTRALE'.((isset($_GET['RFFSort']) AND ($_GET['RFFSort']=='RFF_MENGEZENTRALE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_MENGEZENTRALE'],100,'',$Link);
		}
		
		//PG: 20150415 - Kennzeichen soll nun immer angezeigt werden. 
		//if ($EditRechtZentrale == true or $rsRFF->FeldInhalt('RFS_STATUS') >= 31)
		//{
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen';
			$Link .= '&RFFSort=RFF_RUECKLIEFKZ'.((isset($_GET['RFFSort']) AND ($_GET['RFFSort']=='RFF_RUECKLIEFKZ'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_RUECKLIEFKZ'],260,'',$Link);
		//}
		
		$Form->ZeileEnde();

		$RFFZeile=0;
		while(!$rsRFF->EOF())
		{
			$Form->ZeileStart();
			
			$Icons = array();
			
			if(($Recht3920&2)>0 or ($Recht3920&256)>0) //AND ($EditRechtFiliale==true or $EditRechtZentrale==true))	// �ndernrecht
			{				
				$Icons[] = array('edit','./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen&RFF_KEY='.$rsRFF->FeldInhalt('RFF_KEY'));				
			}
			if(($Recht3920&8)>0  AND ($EditRechtFiliale==true or $EditRechtZentrale==true))	// L�schen
			{
				$Icons[] = array('delete','./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Seite=Positionen&Del='.$rsRFF->FeldInhalt('RFF_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($RFFZeile%2));						
																			
			if ($EditRechtZentrale == true and $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 31)
			{					
				
				$BindeVariablen=array();
				$BindeVariablen['var_T_ast_atunr']=$rsRFF->FeldInhalt('RFF_AST_ATUNR');
				$BindeVariablen['var_N0_fil_id']=$rsRFF->FeldInhalt('RFK_FIL_ID');
				
				//$SQL = 'SELECT NVL((NVL(FIB_BESTAND,0)-(NVL(FBB_RESERVIERT,0))),0) as BESTAND';
				$SQL = 'SELECT (NVL(FIB_BESTAND,0)) as BESTAND';
				$SQL.= ' FROM FILIALBESTAND';
				//$SQL.= ' LEFT JOIN FILIALBESTANDBEWEGUNG ON FIB_AST_ATUNR = FBB_AST_ATUNR AND FIB_FIL_ID = FBB_FIL_ID';
				$SQL.= ' WHERE FIB_AST_ATUNR = :var_T_ast_atunr';
				$SQL.= ' AND FIB_FIL_ID = :var_N0_fil_id';
				
				$rsFIB = $DB->RecordSetOeffnen($SQL,$BindeVariablen);						
				//$Form->DebugAusgabe(1,$rsFIB->AnzahlDatensaetze());
				if (($rsFIB->AnzahlDatensaetze()==0 and $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))>0) or
				 	($rsFIB->AnzahlDatensaetze()>0 and $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))))
				{
					$Form->Erstelle_ListenFeld('RFF_AST_ATUNR',$rsRFF->FeldInhalt('RFF_AST_ATUNR'),0,70,false,2,'','','T','','Negativbestand! (aktueller Filialbestand: '.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').')');
					$Form->Erstelle_ListenFeld('ASP_BEZEICHNUNG',$rsRFF->FeldInhalt('ASP_BEZEICHNUNG'),0,325,false,2,'','','T','','Negativbestand! (aktueller Filialbestand: '.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').')');
					$Form->Erstelle_ListenFeld('RFF_MENGEFILIALE',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'),0,80,false,2,'','','','','Negativbestand! (aktueller Filialbestand: '.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').')');
					$Form->Erstelle_ListenFeld('RFF_MENGEZENTRALE',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'),0,100,false,2,'','','','','Negativbestand! (aktueller Filialbestand: '.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').')');					
					//$Form->Erstelle_ListenFeld('RFF_MENGEZENTRALE','Negativbestand',0,150,false,2,'','');
				}
				else 
				{
					$Form->Erstelle_ListenFeld('RFF_AST_ATUNR',$rsRFF->FeldInhalt('RFF_AST_ATUNR'),0,70,false,($RFFZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('ASP_BEZEICHNUNG',$rsRFF->FeldInhalt('ASP_BEZEICHNUNG'),0,325,false,($RFFZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('RFF_MENGEFILIALE',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'),0,80,false,($RFFZeile%2),'','');
					$Form->Erstelle_ListenFeld('RFF_MENGEZENTRALE',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'),0,100,false,($RFFZeile%2),'','');					
				}
					$Form->Erstelle_ListenFeld('RFF_RUECKLIEFKZ',$rsRFF->FeldInhalt('RUECKLIEFKZBEZ').' ('.$rsRFF->FeldInhalt('RFF_RUECKLIEFKZ').' - '.$rsRFF->FeldInhalt('RFT_WWS_BUCHUNGSKENNZIFFER').')',0,260,false,($RFFZeile%2),'','');					
			}
			else 
			{
				$Form->Erstelle_ListenFeld('RFF_AST_ATUNR',$rsRFF->FeldInhalt('RFF_AST_ATUNR'),0,70,false,($RFFZeile%2),'','','T');
				$Form->Erstelle_ListenFeld('ASP_BEZEICHNUNG',$rsRFF->FeldInhalt('ASP_BEZEICHNUNG'),0,325,false,($RFFZeile%2),'','','T');
				$Form->Erstelle_ListenFeld('RFF_MENGEFILIALE',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'),0,80,false,($RFFZeile%2),'','');
				
				if ($EditRechtZentrale == true or $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) >= 32)
				{
					$Form->Erstelle_ListenFeld('RFF_MENGEZENTRALE',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'),0,100,false,($RFFZeile%2),'','');										
				}
				
				//PG: 20150414
				if ($rsRFF->FeldInhalt('RFS_STATUS') >= 31)
				{
					$Form->Erstelle_ListenFeld('RFF_RUECKLIEFKZ',$rsRFF->FeldInhalt('RUECKLIEFKZBEZ').' ('.$rsRFF->FeldInhalt('RFF_RUECKLIEFKZ').' - '.$rsRFF->FeldInhalt('RFT_WWS_BUCHUNGSKENNZIFFER').')',0,260,false,($RFFZeile%2),'','');										
				}
				else
				{
					//Wenn noch offen, dann Zeig das R�ckf�hrungskennzeichen des Artikels an. Laut Hr. Glei�ner.
				    $SQLKZ = ' select RFT_KENNUNG, RFT_BEZEICHNUNG, RFT_WWS_BUCHUNGSKENNZIFFER from   V_ARTIKEL_RUECKLIEFKZ  inner join   RUECKFUEHRUNGSTYP   on RUECKLIEFKZ = RFT_KENNUNG where AST_ATUNR = \'' .  $rsRFF->FeldInhalt('RFF_AST_ATUNR') . '\' and GUELTIG = 0';
				    $RSKZ = $DB->RecordSetOeffnen($SQLKZ);
				    //var_dump ($SQLKZ);
				    $Form->Erstelle_ListenFeld('RFF_RUECKLIEFKZ',$RSKZ->FeldInhalt('RFT_BEZEICHNUNG').' ('.$RSKZ->FeldInhalt('RFT_KENNUNG').' - '.$RSKZ->FeldInhalt('RFT_WWS_BUCHUNGSKENNZIFFER').')',0,260,false,($RFFZeile%2),'','');
				}
			}
			
			$Form->ZeileEnde();

			$rsRFF->DSWeiter();
			$RFFZeile++;
		}

		$Form->Formular_Ende();

	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$Form->Erstelle_HiddenFeld('RFF_KEY',$rsRFF->FeldInhalt('RFF_KEY'));
		$Form->Erstelle_HiddenFeld('RFF_RFK_KEY',$AWIS_KEY1);
		
		$AWIS_KEY2 = $rsRFF->FeldInhalt('RFF_KEY');

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFFListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRFF->FeldInhalt('RFF_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRFF->FeldInhalt('RFF_USERDAT'));
		$Form->InfoZeile($Felder,'');

		//$EditRecht=(($Recht3920&2)!=0);
		
		$EditRecht = false;	
		$EditRechtFiliale=false;
		$EditRechtZentrale=false;
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale) hat und der Status 30 (R�ckf�hrung erstellt) ist
		if (($Recht3920&16)!=0 AND ($AWIS_KEY2==0 or $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 30))
		{
			$EditRecht=true;
			$EditRechtFiliale=true;		
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Zentrale) hat und der Status 31 (R�ckf�hrung - abgeschlossen - Filiale) ist
		if (($Recht3920&32)!=0 AND ($AWIS_KEY2==0 or $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 31))
		{
			$EditRecht=true;
			$EditRechtZentrale=true;
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale und Zentrale) 
		if (($Recht3920&64)!=0)
		{
			if ($DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) < 32)
			{
				$EditRecht=true;
				$EditRechtFiliale=true;
				$EditRechtZentrale=true;
			}
			else 
			{
				$EditRecht = false;	
				$EditRechtFiliale=false;
				$EditRechtZentrale=false;
			}
		}							

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'].':',150);
		if ($AWIS_KEY2!=0)
		{
			$Form->Erstelle_TextFeld('RFF_AST_ATUNR',$rsRFF->FeldInhalt('RFF_AST_ATUNR'),50,100,$EditRechtFiliale);
		}
		else 
		{
			$Form->Erstelle_TextFeld('RFF_AST_ATUNR',$rsRFF->FeldInhalt('RFF_AST_ATUNR'),50,100,$EditRecht);
		}
		$AWISCursorPosition='txtRFF_AST_ATUNR';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ASP']['ASP_BEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('ASP_BEZEICHNUNG',$rsRFF->FeldInhalt('ASP_BEZEICHNUNG'),50,360,false);		
		$Form->ZeileEnde();
		
		if (($DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) == 31) and (($Recht3920&128) == 128))
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'].':',150);
			$Form->Erstelle_TextFeld('RFF_MENGEFILIALE',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'),50,100,true);
			$Form->ZeileEnde();
		}
		else 
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'].':',150);
			$Form->Erstelle_TextFeld('RFF_MENGEFILIALE',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'),50,100,$EditRechtFiliale);
			$Form->ZeileEnde();
		}
		/*	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_BEMERKUNGFILIALE'].':',150);
		$Form->Erstelle_TextFeld('RFF_BEMERKUNGFILIALE',$rsRFF->FeldInhalt('RFF_BEMERKUNGFILIALE'),100,350,$EditRechtFiliale);
		$Form->ZeileEnde();
		*/		
		if ($EditRechtZentrale == true or $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFS_STATUS')) >= 32)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_MENGEZENTRALE'].':',150);
			$Form->Erstelle_TextFeld('RFF_MENGEZENTRALE',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'),50,100,$EditRechtZentrale);
			$Form->ZeileEnde();
							
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_BEMERKUNGZENTRALE'].':',150);
			$Form->Erstelle_TextFeld('RFF_BEMERKUNGZENTRALE',$rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE'),50,500,$EditRechtZentrale,'','','','T','L','','',255);
			//$Form->Erstelle_Textarea('RFF_BEMERKUNGZENTRALE',$rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE'),100,38,2,$EditRechtZentrale,'','','','maxlength="255"');
			$Form->ZeileEnde();			

			/*
			if ($rsRFF->FeldInhalt('RUECKLIEFKZ')=='L')
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_LIE_NR'].':',150);																
				
				$SQL = "select distinct LIE_NR, LIE_NR || ' - ' || LIE_NAME1 ";
				$SQL.= "from artikelstamm ";
				$SQL.= "inner join teileinfos on tei_ity_id1 = 'AST' and tei_ity_id2 = 'LAR' and tei_key1 = ast_key ";
				$SQL.= "inner join lieferantenartikel on lar_key = tei_key2 ";
				$SQL.= "inner join lieferanten on lar_lie_nr = lie_nr ";
				$SQL.= "where ast_atunr = ".$DB->FeldInhaltFormat('T',$rsRFF->FeldInhalt('RFF_AST_ATUNR'))." ";
				$SQL .= "and tei_user='WWS'";
				$SQL.= "order by 1 ";				
				
				//$Form->DebugAusgabe(1,$SQL);
				
				//$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				$Form->Erstelle_SelectFeld('RFF_LIE_NR',($AWIS_KEY2==0?'':$rsRFF->FeldInhalt('RFF_LIE_NR')),300,$EditRechtZentrale,$SQL,$OptionBitteWaehlen);
				//$Form->Erstelle_SelectFeld('RFF_LIE_NR','',200,true,$SQL,$AWISSprachKonserven['RFF']['RFF_LIE_NR'],'-1','','','');	
				$Form->ZeileEnde();	
			}
			*/	
					
		}

		if($EditRechtZentrale == true or $rsRFF->FeldInhalt('RFS_STATUS') >= 31)
		{
			$SQL="SELECT RFT_KENNUNG, RFT_BEZEICHNUNG ||' ('||RFT_KENNUNG||' - '||RFT_WWS_BUCHUNGSKENNZIFFER||')' FROM AWIS.RUECKFUEHRUNGSTYP";
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_RUECKLIEFKZ'].':',150);
			$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
			$Form->Erstelle_SelectFeld('RFF_RUECKLIEFKZ',($rsRFF->FeldInhalt('RFF_RUECKLIEFKZ')==''?' ':$rsRFF->FeldInhalt('RFF_RUECKLIEFKZ')),260,$EditRechtZentrale,$SQL,$OptionBitteWaehlen);
			$Form->ZeileEnde();			
		}
			
		
		$Form->Formular_Ende();
	}
}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>