<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $Key;


$TextKonserven=array();
$TextKonserven[]=array('FLB','*');
$TextKonserven[]=array('PEI','*');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_Hilfe');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');


try
{
   $Form = new awisFormular();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
   $AWISBenutzer = awisBenutzer::Init();
   

   $Form->DebugAusgabe(1, $_POST);
   $Felder = explode(';',$Form->NameInArray($_POST, 'txtPEZ_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')// AND $ATUNR!='')
	{
		$Form->DebugAusgabe(1, $Felder);
		
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$SQL = '';

			if($FeldTeile[3]>0) //Key > 0 --> aktualisieren
			{
				// Nur schreiben, wenn sich die Felder ge�ndert haben sollten!
				if($DB->FeldInhaltFormat('T',$_POST[$Feld],true)!=$DB->FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)],true))
				{
					$SQL .= 'UPDATE Personaleinsberichtszuord';
					if ($FeldTeile[1]=='BEMERKUNG')
					{
						$SQL .= ' SET PEZ_BEMERKUNG='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
					}
					else
					{
						$SQL .= ' SET PEZ_WERT='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
					}
					$SQL .= ', PEZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', PEZ_USERDAT=SYSDATE';
					$SQL .= ' WHERE PEZ_KEY=0'.$FeldTeile[3];				
					//echo $SQL;
					//die();
				}
			}

			if($SQL!='')
			{
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',102031526,$SQL,2);
				}			
			}
		}
	}

   
   if(!isset($_POST['txtEdit']))
   {
	if(isset($_POST['txtFLB_KEY']) && $_POST['txtFLB_KEY'] == '-1')
	{
		
	  $Felder = $Form->NameInArray($_POST, 'txtFLB_',1,1);
	  
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('FLB','FLB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('FLB_DATUM','FLB_PRUEFER');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FLB'][$Pflichtfeld].'<br>';
				}
			}

			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
		}
		
			$SQL = "INSERT INTO FILIALBERICHTE (FLB_FIL_ID,FLB_DATUM,FLB_BEMERKUNG,FLB_PRUEFER,FLB_PBE_KEY,FLB_USER,FLB_USERDAT) VALUES(";
			$SQL .= ' ' . $DB->FeldInhaltFormat('NO',$_POST['txtFLB_FIL_ID'],false);
			$SQL .= ', ' . $DB->FeldInhaltFormat('D',$_POST['txtFLB_DATUM'],false);
			$SQL .= ', ' . $DB->FeldInhaltFormat('T',$_POST['txtFLB_BEMERKUNG'],false);
			$SQL .= ', ' . $DB->FeldInhaltFormat('T',$_POST['txtFLB_PRUEFER'],false);
			$SQL .= ', ' . $DB->FeldInhaltFormat('NO',2,false);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			
			if($DB->Ausfuehren($SQL)===false)
			{
				die();
			}
			
			$SQL = 'SELECT seq_FLB_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
			
				$Form->DebugAusgabe('1',$AWIS_KEY1);	
					
				$SQL = 'INSERT INTO personaleinsberichtszuord';
				$SQL .= '(PEZ_XXX_KEY,PEZ_XTN_KUERZEL,PEZ_PBF_ID,PEZ_USER,PEZ_USERDAT)';
				$SQL .= ' select '.$AWIS_KEY1. ', \'FLB\', PBF_ID ';
				$SQL .= ' ,\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ' ,SYSDATE';
				$SQL .= ' FROM Personaleinsberichtsfelder';
				$SQL .= ' WHERE PBF_PBE_KEY = 2';
				$SQL .= ' AND PBF_SPALTEN > 0';

				if($DB->Ausfuehren($SQL)===false)
				{
					die();
				}
			
			//$AWIS_KEY1 = 0;
			
	 }
    }
	elseif(isset($_POST['txtFLB_KEY']) && isset($_POST['txtFLB_KEY']) != '') 
	{
		$Felder = $Form->NameInArray($_POST, 'txtFLB_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('FLB','FLB_%');
			$TextKonserven[]=array('FLB','FLB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('FLB_DATUM','FLB_PRUEFER');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FLB'][$Pflichtfeld].'<br>';
				}
			}

			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
		}
		
		
		if(isset($_POST['txtFLB_PEZ_STATUS']))
		{		        	
			if($_POST['txtFLB_PEZ_STATUS'] =='1')
			{
				$SQL = 'SELECT * FROM PERSONALEINSBERICHTSZUORD ';
				$SQL.= ' INNER JOIN PERSONALEINSBERICHTSFELDER ON PBF_ID = PEZ_PBF_ID AND PBF_SPALTEN=3';
				$SQL.= ' WHERE PEZ_WERT = \'0\' AND (PEZ_BEMERKUNG IS NULL OR PEZ_BEMERKUNG = \'\') ';
				$SQL.= ' AND PEZ_XXX_KEY = 0'.$_POST['txtEdit'] .' AND PEZ_XTN_KUERZEL=\'FLB\'';
//					echo $SQL;
//die();					
				$rsPruef = $DB->RecordSetOeffnen($SQL);
				if ($rsPruef->AnzahlDatensaetze() > 0)
				{
					$Fehler.= $TXT_Speichern['PEI']['err_Bemerkung'].'<br>';
				}
									
				if ($Fehler != '')
				{
				$Form->Hinweistext($Fehler);
										
				$Link='./filialberichte_Main.php?cmdAktion=Details&FLB_KEY='.$_POST['txtEdit'];
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();												
				}									
									
			}
			
			$SQL = "UPDATE FILIALBERICHTE SET FLB_FIL_ID=".$DB->FeldInhaltFormat('NO',$_POST['txtFLB_FIL_ID']); 
			$SQL .= " ,FLB_DATUM=".$DB->FeldInhaltFormat('D',$_POST['txtFLB_DATUM']);
			$SQL .= " ,FLB_BEMERKUNG=".$DB->FeldInhaltFormat('T',$_POST['txtFLB_BEMERKUNG']);
			$SQL .= " ,FLB_PRUEFER=".$DB->FeldInhaltFormat('T',$_POST['txtFLB_PRUEFER']);
			$SQL .= " ,FLB_PEZ_STATUS=".$DB->FeldInhaltFormat('NO',$_POST['txtFLB_PEZ_STATUS']);
			$SQL .= ',FLB_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ',FLB_USERDAT=sysdate';
			$SQL .= " WHERE FLB_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtEdit']);
        	
			if($DB->Ausfuehren($SQL)===false)
			{
				die();
			}
        	
		}
		else
		{
			$SQL = "UPDATE FILIALBERICHTE SET FLB_FIL_ID=".$DB->FeldInhaltFormat('NO',$_POST['txtFLB_FIL_ID']); 
			$SQL .= " ,FLB_DATUM=".$DB->FeldInhaltFormat('D',$_POST['txtFLB_DATUM']);
			$SQL .= " ,FLB_BEMERKUNG=".$DB->FeldInhaltFormat('T',$_POST['txtFLB_BEMERKUNG']);
			$SQL .= " ,FLB_PRUEFER=".$DB->FeldInhaltFormat('T',$_POST['txtFLB_PRUEFER']);
			$SQL .= ',FLB_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ',FLB_USERDAT=sysdate';
			$SQL .= " WHERE FLB_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtEdit']);
			
			if($DB->Ausfuehren($SQL)===false)
			{
				die();
			}
		}        
	}
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>