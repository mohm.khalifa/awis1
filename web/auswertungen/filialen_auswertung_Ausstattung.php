<?php
// Variablen
global $_POST;
//global $HTTP_GET_VARS;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);


print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=Ausstattung>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	//$SQL = 'SELECT FIL_ID, FIL_BEZ, SUM(FIF_WERT) AS Summe FROM Filialen, FilialInfos ';
	//$SQL .= ' WHERE FIL_ID = FIF_FIL_ID AND FIF_FIT_ID IN (111,112,113,114,116)';
	//$SQL .= ' GROUP BY FIL_ID, FIL_BEZ';
	
	//alternativ
	//$SQL = 'select fil_id , fil_bez,sum(a.fif_wert) as Summe ';
	//$SQL .= ' from filialinfos a,';
	//$SQL .= ' (';
	//$SQL .= ' SELECT FIL_ID, fil_bez, fif_fit_id, max(fif_imq_id) as imq_id';
	//$SQL .= ' FROM Filialen, FilialInfos';
	//$SQL .= ' WHERE FIL_ID = FIF_FIL_ID AND FIF_FIT_ID IN (111,112,113,114,116)';
	//$SQL .= ' group by FIL_ID, FIL_BEZ, fif_fit_id';
	//$SQL .= ' ) b';
	//$SQL .= ' where a.FIF_FIL_ID=b.fil_id';
	//$SQL .= ' and a.FIF_FIT_ID=b.fif_fit_id';
	//$SQL .= ' and a.FIF_IMQ_ID=b.imq_id';
	//$SQL .= ' group by FIL_ID, FIL_BEZ';
	//$SQL .= ' order by FIL_ID';
	
	//aus Hilfs- und Betriebsstoffe
	$SQL = "select fil_id,fil_bez,count(hbe_fil_id) as Summe from HILFSUNDBETRIEBSSTOFFEINSAETZE, ";
	$SQL .= "(select hbs_key, hbs_hbt_key from ";
	$SQL .= "HILFSUNDBETRIEBSSTOFFE ";
	$SQL .= "where hbs_hbt_key in(select hbt_key from HILFSUNDBETRIEBSSTOFFTYPEN ";
	$SQL .= "where HBT_HBG_KEY in (12,13,14)) ";
	$SQL .= "and hbs_ausgemustert is null), ";
	$SQL .= "filialen ";
	$SQL .= "where hbe_hbs_key=hbs_key ";
	$SQL .= "and hbe_fil_id=fil_id ";
	$SQL .= "and to_date(hbe_datumbis,'DD.MM.YYYY')>=to_date(sysdate,'DD.MM.YYYY') ";
	$SQL .= "group by fil_id,fil_bez ";
	$SQL .= "order by fil_id ";
	
	
	$rsAusstattung = awisOpenRecordset($con, $SQL);
	$rsAusstattungZeilen = $awisRSZeilen;
	$DateiName = awis_UserExportDateiName('.' . $_POST['txtFormat']);
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];

	if($_POST['txtFormat']=='pdf')
	{
		require 'PDF.php';
		
	 	$pdf = &PDF::factory('p','a4');
		$pdf->open();
		$pdf->addpage();

		$pdf->SetFont('Arial','',30);
		$RasterZeile = 50;
		$pdf->text(50,$RasterZeile,"Filialausstattungen");
		$RasterZeile += 40;

		$pdf->SetFont('Arial','',12);
		$pdf->text(50,$RasterZeile,"Filiale");
		$pdf->text(100,$RasterZeile,"Bezeichnung");
		$pdf->text(300,$RasterZeile,"Summe");
		$RasterZeile += 2;

		$AusgabeBeginn = $RasterZeile;
		$pdf->line(50,$RasterZeile,500,$RasterZeile);
		$pdf->line(50,$RasterZeile+2,500,$RasterZeile+2);
		$RasterZeile +=14;
		
		for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
		{
			$pdf->text(51,$RasterZeile,$rsAusstattung['FIL_ID'][$DSNr]);
			$pdf->text(101,$RasterZeile,$rsAusstattung['FIL_BEZ'][$DSNr]);
			$pdf->text(301,$RasterZeile,$rsAusstattung['SUMME'][$DSNr]);
			$pdf->line(50,$RasterZeile+2,500,$RasterZeile+2);
			$RasterZeile += 15;
			
			if($RasterZeile > 800)
			{
				$pdf->image('../bilder/atulogo_neu.jpg',400,20);
				$pdf->line(50,$AusgabeBeginn,50,$RasterZeile-13);
				$pdf->line(100,$AusgabeBeginn,100,$RasterZeile-13);
				$pdf->line(300,$AusgabeBeginn,300,$RasterZeile-13);			
				$pdf->line(500,$AusgabeBeginn,500,$RasterZeile-13);				
				
				$pdf->addpage();					// Neue Seite
				$pdf->SetFont('Arial','',30);
				$RasterZeile = 50;
				$pdf->text(50,$RasterZeile,"Filialausstattungen");
				$RasterZeile += 40;
		
				$pdf->SetFont('Arial','',12);
				$pdf->text(50,$RasterZeile,"Filiale");
				$pdf->text(100,$RasterZeile,"Bezeichnung");
				$pdf->text(300,$RasterZeile,"Summe");
				$RasterZeile += 2;
		
				$AusgabeBeginn = $RasterZeile;
				$pdf->line(50,$RasterZeile,500,$RasterZeile);
				$pdf->line(50,$RasterZeile+2,500,$RasterZeile+2);
				$RasterZeile +=14;
			}
		}
		$pdf->image('../bilder/atulogo_neu.jpg',400,20);
		$pdf->line(50,$AusgabeBeginn,50,$RasterZeile-13);
		$pdf->line(100,$AusgabeBeginn,100,$RasterZeile-13);
		$pdf->line(300,$AusgabeBeginn,300,$RasterZeile-13);			
		$pdf->line(500,$AusgabeBeginn,500,$RasterZeile-13);				

		$pdf->saveas($DateiName);
    	echo "<br><a href=$DateiNameLink>Datei �ffnen</a>";
	}
	else
	{
//		$DateiName = $_SERVER['DOCUMENT_ROOT'] . $DateiName . '.csv';
//		$DateiNameLink .= '.csv';
		
		$fd = fopen($DateiName,'w');
		if($fd==FALSE)
		{
			die($DateiName);
		}
		
		fputs($fd, "Filiale;Bezeichnung;Summe\n");
		for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
		{
			fputs($fd, $rsAusstattung['FIL_ID'][$DSNr] . ';' . $rsAusstattung['FIL_BEZ'][$DSNr] . ';' . $rsAusstattung['SUMME'][$DSNr] . "\n");
		}
		fclose($fd);
		
		echo '<br><a href='.$DateiNameLink.'>CSV Datei �ffnen</a>';
	}
}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Summe der Austattungen pro Filiale.</li></ul>';
	echo 'Ausgabeformat: <select name=txtFormat>';
	echo '<option value=pdf>PDF</option>';
	echo '<option value=csv>EXCEL</option>';
	echo '</select><hr>';
	
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

