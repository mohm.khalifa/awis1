<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;
global $MDI;

echo "<form name=frmSuche method=post action=./operating_MobiData_Main.php?cmdAktion=Allgemein>";

$Param = unserialize($MDI->AWISBenutzer->ParameterLesen('MDISuche'));

//***************************************
// Suchmaske bauen
//***************************************
$MDI->Form->Formular_Start();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_NAME'], 200);
$SQL = 'SELECT DISTINCT MDJ_KEY, MDJ_BEZEICHNUNG FROM MOBIDATAJOBS';
$MDI->Form->Erstelle_MehrfachSelectFeld('*MDI_NAME', array(), '800:500', true, $SQL, '', '', '', '', array(), '', '', array(), '', '', '', '', 100);
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_STATUS'], 200);
$OptionBitteWaehlen = '~' . $MDI->AWISSprachkonserven['Wort']['txt_BitteWaehlen'];
$Status = explode("|",$MDI->AWISSprachkonserven['MDI']['lst_MDI_STATUS']);
$MDI->Form->Erstelle_SelectFeld('*MDI_STATUS', '', 471, true, '', $OptionBitteWaehlen, '', '', '', $Status);
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_DATUM'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_DATUM', '', 20, 200, true, '', '', '', 'D');
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_START'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_START', '', 15, 60, true, '', '', '', '', '', '', '', '', '', '', 'placeholder="hh:mm:ss"');
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_ENDE'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_ENDE', '', 15, 60, true, '', '', '', '', '', '', '', '', '', '', 'placeholder="hh:mm:ss"');
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_DATEI_DE'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_DATEI_DE', '', 60, 60, true);
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_DATEI_AT'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_DATEI_AT', '', 60, 60, true);
$MDI->Form->ZeileEnde();

$MDI->Form->ZeileStart();
$MDI->Form->Erstelle_TextLabel($MDI->AWISSprachkonserven['MDI']['MDI_DATEI_CH'], 200);
$MDI->Form->Erstelle_TextFeld('*MDI_DATEI_CH', '', 60, 60, true);
$MDI->Form->ZeileEnde();

$MDI->Form->Formular_Ende();

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$MDI->Form->SchaltflaechenStart();
$MDI->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $MDI->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');
$MDI->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $MDI->AWISSprachkonserven['Wort']['lbl_suche'], 'W');
$MDI->Form->SchaltflaechenEnde();
