ALTER TABLE AZUBIPROJEKTEBEREICHE
ADD AZB_STATUS VARCHAR2(20 BYTE);

UPDATE AZUBIPROJEKTEBEREICHE SET AZB_STATUS = 'I';


INSERT INTO azubiprojektebereiche (
    azb_stufe,
    azb_bereichname,
    azb_infotext,
    azb_user,
    azb_status
) VALUES (
    3,
    'Motor (26.02.2018 - 28.02.2018)',
    'Hier erwartet Sie die Teilzerlegung des Motor, sowie die Bestandsaufnahme der Motorkomponenten.<br>
Zudem erfolgt die komplette Abdichtung des Motors und die anschließende Zusammenbau des Motors.<br>
Sie befinden sich im 3. oder 4. Lehrjahr und möchten ein Teil dieses Projekts werden, dann nutzen Sie Ihre Chance.<br>
Die Teilnehmer werden ca. 1 Woche vor Beginn ausgelost und entsprechend per E-Mail informiert. <br>
',
    'streit_d',
    'A',
);
UPDATE AZUBIPROJEKTEBEREICHE SET AZB_STATUS = 'A' WHERE AZB_STUFE = 3;
COMMIT;