<?php
global $AWISCursorPosition;
global $AWISBenutzer;


$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('LIK','%');
$TextKonserven[]=array('LKK','%');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3600=$AWISBenutzer->HatDasRecht(3600);

echo "<form name=frmSuche method=post action=./lieferkontrolle_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('LKKSuche'));

$Form->Formular_Start();

$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
$FilZugriffListe=explode(',',$FilZugriff);

$AWISCursorPosition='sucLKK_STATUS';
$Form->ZeileStart();

if(count($FilZugriffListe)>1)
{
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_FIL_ID'].':',180);
	$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
	$SQL .= ' FROM Filialen ';
	$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
	$SQL .= ' ORDER BY FIL_BEZ';
	$Form->Erstelle_SelectFeld('*LKK_FIL_ID','',200,true,$SQL);
	$AWISCursorPosition='sucLKKFIL_ID';
}
elseif($FilZugriff!='')
{
	$FIL_ID=$FilZugriff;	
	//$Form->Erstelle_HiddenFeld('*LKK_FIL_ID',$FIL_ID);
	//$Form->Erstelle_TextLabel($FIL_ID,200);
	echo '<input type=hidden name="sucLKK_FIL_ID" value="'.$FIL_ID.'">';
}
else
{
	$Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_FIL_ID'].':',180);
	$Form->Erstelle_TextFeld('*LKK_FIL_ID',($Param['SPEICHERN']=='on'?$Param['LKK_FIL_ID']:''),20,200,true);		
	$AWISCursorPosition='sucLKK_FIL_ID';
}
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',180);
$StatusText = explode("|",$AWISSprachKonserven['LIK']['lst_Lieferkontrollenstatus']);
$Form->Erstelle_SelectFeld('*LKK_STATUS',($Param['SPEICHERN']=='on'?$Param['LKK_STATUS']:''),200,true,'','0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'O','','',$StatusText,'');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_LIEFERSCHEINNR'].':',180);
$Form->Erstelle_TextFeld('*LKK_LIEFERSCHEINNR',($Param['SPEICHERN']=='on'?$Param['LKK_LIEFERSCHEINNR']:''),20,200,true);
$Form->ZeileEnde();	

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_DATUMKOMM'].':',180);
$Form->Erstelle_TextFeld('*LKK_DATUMKOMM',($Param['SPEICHERN']=='on'?$Param['LKK_DATUMKOMM']:''),20,200,true,'','','','D');
$Form->ZeileEnde();	

//Auswahl speichern
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
$Form->ZeileEnde();

/*
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['LIK']['LIK_AST_ATUNR'].':',180);
$Form->Erstelle_TextFeld('*LIK_AST_ATUNR','',20,200,true);
$Form->ZeileEnde();	
*/
$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

$Form->SchaltflaechenEnde();

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>
