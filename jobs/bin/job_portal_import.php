<?php
/**
 * Job_import.php
 * 
 * Mit diesem Job werden die aktuellen St�nde aus dem Portal in AWIS eingelesen
 * 
 * @author Sacha Kerres
 * @copyright ATU Auto-Teile-Unger
 * @version 20091216
 * @package PORTAL
 */
require_once 'awis_portalzugaenge.inc';

try
{

	$Portal = new awis_Portalzugaenge('awis_jobs');
	$Portal->DebugLevel(10);
	
	// Aktuellen Stand aus dem Portal einlesen
	$Portal->BenutzerDatenEinlesen();
}
catch(awisException $ex)
{
	awisWerkzeuge::EMail('shuttle@de.atu.eu','Personalabgleich-FEHLER','Fehlermeldung: '.$ex->getMessage().'. SQL: '.$ex->getSQL(),'shuttle@de.atu.eu');
}
catch(Exception $ex)
{
	awisWerkzeuge::EMail('shuttle@de.atu.eu','Personalabgleich-FEHLER','Fehlermeldung: '.$ex->getMessage(),'shuttle@de.atu.eu');
}
?>