<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
//require_once('awisVersicherungenFunktionen.php');
require_once 'awisBenutzer.inc';
require_once('/daten/web/berichte/ber_sonstKasko.inc');
//require_once('/daten/web/berichte/ber_WWK_Monatsreport.inc');

class awisRepkostenvers
{
	private $_DebugLevel;
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_FORM;
    private $_Functions;
    private $_PfadExportPDF = '/daten/daten/versicherungen/export_repkostenvers/pdf_rechnungen/';
    private $_PfadRueckmeldeDateien = '/daten/daten/teams/AWIS-Transfer/02-RepkostenVers/01-OUT/01-Cargarantie/';
    private $_PfadExportRechDateien = '/daten/daten/versicherungen/export_repkostenvers/';
    private $_PfadArchivPDF = '/daten/daten/versicherungen/archiv/repkostenvers/pdf_rechnungen/';
    private $_PfadArchivCSV = '/daten/daten/versicherungen/archiv/repkostenvers/csv_dateien/';
    private $_PfadDatenTransfer = '/daten/daten/teams/AWIS-Transfer/02-RepkostenVers/01-OUT/01-Cargarantie/';	
    private $_PfadDatenTransfertest = '/daten/daten/teams/AWIS-Transfer/02-RepkostenVers/01-OUT/01-Cargarantie/bla/';
    private $_Werkzeuge;
    private $_EMAIL_Infos =  array('shuttle@de.atu.eu');
    
    function __construct($DebugLevel = 0, $Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_DebugLevel = $DebugLevel;
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_FORM = new awisFormular();
        //$this->_Functions = new VersicherungenFunktionen();
        
    }
    public function LadeNeueDatenReparaturkostenvers()
    {
    	$this->debugAusgabe(" START: Lade neue Daten in Tabelle versreparaturkosten..", 500);
        try {

            $this->_DB->TransaktionBegin();

            $this->_SQL  ='MERGE INTO versreparaturkosten vrk USING';
            $this->_SQL .=' (SELECT DISTINCT vl.filid ,';
            $this->_SQL .='   vl.wanr,';
            $this->_SQL .='   vl.vers_nr,';
            $this->_SQL .='   vl.schaden_kz,';
            $this->_SQL .='   vl.kfz_kz,';
            $this->_SQL .='   vl.versscheinnr,';
            $this->_SQL .='   vl.schaden_nr,';
            $this->_SQL .='   vve.vve_bezeichnung';
            $this->_SQL .=' FROM vers_exp_kopf vl';
            $this->_SQL .=' INNER JOIN versversicherungen vve';
            $this->_SQL .=' ON vve.vve_versnr = vl.vers_nr';
            $this->_SQL .=' WHERE vl.schaden_kz IN (\'XX\', \'R\')';
            $this->_SQL .=' AND vve.vve_aktiv = 1';
            $this->_SQL .=' AND vl.insdate    >= vve.vve_verarbeitenab) vr ';
            $this->_SQL .=' ON (vr.filid = vrk.vrk_filid';
            $this->_SQL .=' AND vr.wanr = vrk.vrk_wanr)';
            $this->_SQL .=' WHEN NOT MATCHED THEN INSERT';
            $this->_SQL .=' (';
            $this->_SQL .='      vrk_filid, ';
            $this->_SQL .='      vrk_wanr, ';
            $this->_SQL .='      vrk_kfzkz, ';
            $this->_SQL .='      vrk_versnr, ';
            $this->_SQL .='      vrk_versbez, ';
            $this->_SQL .='      vrk_versscheinnr, ';
            $this->_SQL .='      vrk_schadennr, ';
            $this->_SQL .='      vrk_schadenkz, ';
            $this->_SQL .='      vrk_vrs_key, ';
            $this->_SQL .='      vrk_datumimport, ';
            $this->_SQL .='      vrk_datumexport, ';
            $this->_SQL .='      vrk_user, ';
            $this->_SQL .='      vrk_userdat ';
            $this->_SQL .=' ) ';
            $this->_SQL .=' VALUES';
            $this->_SQL .=' (';
            $this->_SQL .='     vr.filid,';
            $this->_SQL .='     vr.wanr,';
            $this->_SQL .='     vr.kfz_kz,';
            $this->_SQL .='     vr.vers_nr,';
            $this->_SQL .='     vr.vve_bezeichnung,';
            $this->_SQL .='     vr.versscheinnr,';
            $this->_SQL .='     vr.schaden_nr,';
            $this->_SQL .='     vr.schaden_kz,';
            $this->_SQL .='     0,';
            $this->_SQL .='     sysdate,';
            $this->_SQL .='     NULL,';
            $this->_SQL .='\'' . $this->_AWISBenutzer->BenutzerName() . '\',';
            $this->_SQL .='     sysdate';
            $this->_SQL .=' )';

            $this->_DB->Ausfuehren($this->_SQL, '',true);

            $this->_DB->TransaktionCommit();

            $this->debugAusgabe(" Import abgeschlossen.", 500);

        }
        catch(Exception $ex)
        {
            $this->_DB->TransaktionRollback();

            echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
            echo 'CODE:  '.$ex->getCode().PHP_EOL;
            echo 'Zeile: '.$ex->getLine().PHP_EOL;
            echo 'Datei: '.$ex->getFile().PHP_EOL;

            $this->_Werkzeuge->EMail($this->_EMAIL_Infos,'ERROR - REPARATURKOSTENVERS',$ex->getMessage(),2,'','shuttle@de.atu.eu');

            die();
        }


        $this->debugAusgabe(" ENDE: Laden neuer Daten in Tabelle versreparaturkosten abgeschlossen.. ", 500);
    }
    public function ErstelleSchnittstellenDatei()
    {
    	try 
    	{
    		$this->debugAusgabe(" START: Erstelle CSV-Rechnungsdateien..", 500);
    		 
    		$this->_SQL = 'SELECT vrk_filid, vrk_wanr ';
    		$this->_SQL .= 'FROM versreparaturkosten ';
    		$this->_SQL .= 'WHERE vrk_vrs_key = 0 ';
    		$this->_SQL .= 'AND vrk_schadenkz = \'R\' ';
    		 
    		if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
    		{
    			$rsExport = $this->_DB->RecordsetOeffnen($this->_SQL);
    			 
    			while(!$rsExport->EOF())
    			{
    				$this->_DB->SetzeBindevariable('VRK', 'var_N0_vrk_filid',$rsExport->FeldInhalt('VRK_FILID') , awisDatenbank::VAR_TYP_GANZEZAHL);
    				$this->_DB->SetzeBindevariable('VRK', 'var_N0_vrk_wanr',$rsExport->FeldInhalt('VRK_WANR') , awisDatenbank::VAR_TYP_GANZEZAHL);
    				 
    				$FILID = $this->_DB->FeldInhaltFormat('N0',$rsExport->FeldInhalt('VRK_FILID'),false);
    				$WANR = $this->_DB->FeldInhaltFormat('N0',$rsExport->FeldInhalt('VRK_WANR'),false);
    		
    				$this->debugAusgabe(" Erstelle Datei: " . $FILID . "_" . $WANR . ".csv", 500);
    		
    				$this->_SQL  ='SELECT ve.kennung,';
    				$this->_SQL .='   ve.filid,';
    				$this->_SQL .='   ve.wanr,';
    				$this->_SQL .='   ve.vorst_abzug_kz,';
    				$this->_SQL .='   ve.vorst_betrag,';
    				$this->_SQL .='   ve.selbst_kz,';
    				$this->_SQL .='   ve.selbst_betrag,';
    				$this->_SQL .='   ve.vers_bez,';
    				$this->_SQL .='   ve.vers_strasse,';
    				$this->_SQL .='   ve.vers_hnr,';
    				$this->_SQL .='   ve.vers_plz,';
    				$this->_SQL .='   ve.vers_ort,';
    				$this->_SQL .='   ve.vers_telefon,';
    				$this->_SQL .='   ve.versscheinnr,';
    				$this->_SQL .='   ve.schaden_nr,';
    				$this->_SQL .='   ve.schaden_datum,';
    				$this->_SQL .='   ve.schaden_bez,';
    				$this->_SQL .='   ve.schaden_art,';
    				$this->_SQL .='   ve.umsatz_gesamt,';
    				$this->_SQL .='   ve.umsatz_kunde,';
    				$this->_SQL .='   ve.umsatz_vers,';
    				$this->_SQL .='   ve.anrede,';
    				$this->_SQL .='   ve.titel,';
    				$this->_SQL .='   ve.name1,';
    				$this->_SQL .='   ve.name2,';
    				$this->_SQL .='   ve.name3,';
    				$this->_SQL .='   ve.strasse,';
    				$this->_SQL .='   ve.hnr,';
    				$this->_SQL .='   ve.plz,';
    				$this->_SQL .='   ve.ort,';
    				$this->_SQL .='   ve.rufnr1,';
    				$this->_SQL .='   ve.rufnr2,';
    				$this->_SQL .='   ve.staat_nr,';
    				$this->_SQL .='   vv.vrk_kfzkz,';
    				$this->_SQL .='   ve.kfz_hersteller,';
    				$this->_SQL .='   ve.kfz_modell,';
    				$this->_SQL .='   ve.kfz_typ,';
    				$this->_SQL .='   ve.fahrgestellnr,';
    				$this->_SQL .='   ve.kfz_herstnr,';
    				$this->_SQL .='   ve.ktypnr,';
    				$this->_SQL .='   ve.kfz_km,';
    				$this->_SQL .='   vv.vrk_versnr';
    				$this->_SQL .=' FROM V_VERS_EXP_KOPF ve';
    				$this->_SQL .=' INNER JOIN versreparaturkosten vv';
    				$this->_SQL .=' ON ve.filid = vv.vrk_filid';
    				$this->_SQL .=' AND ve.wanr = vv.vrk_wanr';
    				$this->_SQL .=' INNER JOIN versversicherungen v';
    				$this->_SQL .=' ON v.vve_versnr = vv.vrk_versnr';
    				$this->_SQL .=' WHERE ve.filid = :var_N0_vrk_filid';
    				$this->_SQL .=' AND ve.wanr = :var_N0_vrk_wanr';
    				 
    				$rsExpKopf = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VRK',false));
    				 
    				$ExpStringKopf = '';
    				 
    				// Felder in Exportstring schreiben (Semikolon getrennt)
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KENNUNG').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('FILID').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('WANR').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_ABZUG_KZ').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_BETRAG').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('SELBST_KZ').';';
    				$ExpStringKopf .= intval($rsExpKopf->FeldInhalt('SELBST_BETRAG')).';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_BEZ').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_STRASSE').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_HNR').';';
    				$ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('VERS_PLZ'),5,'0',STR_PAD_LEFT).';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_ORT').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_TELEFON').';';
    				$ExpStringKopf .= substr($rsExpKopf->FeldInhalt('VERSSCHEINNR'),0,17).';';
    		
    				if($rsExpKopf->FeldInhalt('SCHADEN_NR') != '' && is_null($rsExpKopf->FeldInhalt('SCHADEN_NR')) != true)
    				{
    					$ExpStringKopf .= substr($rsExpKopf->FeldInhalt('SCHADEN_NR'),0,20).';';
    				}
    				else
    				{
    					$ExpStringKopf .= ';';
    				}
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_DATUM').';';
    		
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_BEZ').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_ART').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_GESAMT').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_KUNDE').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_VERS').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('ANREDE').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('TITEL').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME1').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME2').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME3').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('STRASSE').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('HNR').';';
    				if($rsExpKopf->FeldInhalt('PLZ') != '' && is_null($rsExpKopf->FeldInhalt('PLZ')) != true)
    				{
    					$ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('PLZ'),5,'0',STR_PAD_LEFT).';';
    				}
    				else
    				{
    					$ExpStringKopf .= $rsExpKopf->FeldInhalt('PLZ').';';
    				}
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('ORT').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR1').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR2').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('STAAT_NR').';';
    				 
    				 
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('VRK_KFZKZ').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTELLER').';';
    				$ExpStringKopf .= substr($rsExpKopf->FeldInhalt('KFZ_MODELL'),0,55).';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_TYP').';';
    				$ExpStringKopf .= substr($rsExpKopf->FeldInhalt('FAHRGESTELLNR'),0,17).';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTNR').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KTYPNR').';';
    				$ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_KM');
    				 
    				$ExpStringKopf = str_replace("'", "�", $ExpStringKopf);
    				 
    				if(!$fd = fopen($this->_PfadExportRechDateien.$FILID.'_'.$WANR.'.csv','w'))
    				{
    					throw new Exception('Fehler beim Erstellen der Datei '.$FILID.'_'.$WANR.'.csv');
    				}
    		
    				fputs($fd,$ExpStringKopf.chr(13).chr(10));
    		
    				 
    				$this->_SQL  ='SELECT \'P\' AS kennung,';
    				$this->_SQL .='   vp.filnr,';
    				$this->_SQL .='   vp.wanr,';
    				$this->_SQL .='   vp.position,';
    				$this->_SQL .='   vp.artnr,';
    				$this->_SQL .='   vp.artkz,';
    				$this->_SQL .='   vp.bez,';
    				$this->_SQL .='   vp.menge,';
    				$this->_SQL .='   vp.vk_preis,';
    				$this->_SQL .='   vp.umsatz';
    				$this->_SQL .=' FROM v_vers_form_posdaten vp';
    				$this->_SQL .=' WHERE vp.filnr = :var_N0_vrk_filid';
    				$this->_SQL .=' AND vp.wanr = :var_N0_vrk_wanr';
    				 
    				$rsExpPos = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VRK',false));
    				 
    				while(!$rsExpPos->EOF())
    				{
    					$ExpStringPos = '';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('KENNUNG').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('FILNR').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('WANR').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('POSITION').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('ARTNR').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('ARTKZ').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('BEZ').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('MENGE').';';
    					$ExpStringPos .= $rsExpPos->FeldInhalt('VK_PREIS').';';
    					if(strpos($rsExpPos->FeldInhalt('UMSATZ'),',') !== false)
    					{
    						$ExpStringPos .= substr($rsExpPos->FeldInhalt('UMSATZ'),0,strpos($rsExpPos->FeldInhalt('UMSATZ'),',')).';';
    					}
    					else
    					{
    						$ExpStringPos .= $rsExpPos->FeldInhalt('UMSATZ').';';
    					}
    		
    					//$ExpStringPos .= 'N';
    					$ExpStringPos = str_replace("'", "�", $ExpStringPos);
    		
    		
    					fputs($fd,$ExpStringPos.chr(13).chr(10));
    		
    					$rsExpPos->DSWeiter();
    						
    				}
    		
    				fclose($fd);
    				 
    				$this->SetzeStatus(1, $FILID, $WANR);
    				 
    				$rsExport->DSWeiter();
    			}
    		}
    		else
    		{
    			$this->debugAusgabe(" Keine Daten zum Erstellen der CSV-Datei vorhanden.", 500);
    		}
    		$this->debugAusgabe(" ENDE: Erstellen der CSV-Rechnungsdateien abgeschlossen..", 500);
    		
    	}
    	catch(Exception $ex)
    	{
    		echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    		echo 'CODE:  '.$ex->getCode().PHP_EOL;
    		echo 'Zeile: '.$ex->getLine().PHP_EOL;
    		echo 'Datei: '.$ex->getFile().PHP_EOL;
    			
    		$this->_Werkzeuge->EMail($this->_EMAIL_Infos,'ERROR - REPARATURKOSTENVERS',$ex->getMessage(),2,'','shuttle@de.atu.eu');
    			
    		die();
    	}
    	
    }

    
    public function ErstelleRechnungRepkostenvers()
    {
    	try
    	{
    		echo "\n";
    		$this->debugAusgabe(" START: Erstellen der PDF-Rechnngen..", 500);
    		 
    		$Verzeichnis = '/tmp/';                                         // tempor�res Speicherverzeichnis f�r PDF (=Mail-Anhang)
    		$DateiRumpf = '';                      			// Dateirumpf der zu speichernden Datei                               // Handle des Verzeichnisses holen
    		 
    		$this->_SQL  ='SELECT *';
    		$this->_SQL .=' FROM versreparaturkosten';
    		$this->_SQL .=' WHERE vrk_vrs_key = 1';
    		 
    		$AnzahlImpDS = $this->_DB->ErmittleZeilenAnzahl($this->_SQL);
    		 
    		$RsFinaleVorgaenge = $this->_DB->RecordSetOeffnen($this->_SQL);
    		if ($AnzahlImpDS >= 1)
    		{
    			while(!$RsFinaleVorgaenge->EOF())
    			{
    				 
    				$FILID = $this->_DB->FeldInhaltFormat('N0',$RsFinaleVorgaenge->FeldInhalt('VRK_FILID'),false);
    				$WANR = $this->_DB->FeldInhaltFormat('N0',$RsFinaleVorgaenge->FeldInhalt('VRK_WANR'),false);
    		
    				$this->debugAusgabe(" Erstelle Datei: " . $FILID . "_" . $WANR . ".pdf", 500);
    		
    		
    				$PDFAusgabename = $this->_PfadExportRechDateien . $FILID. '_' . $WANR . '.pdf';
    				$Bericht = new ber_sonstKasko($this->_AWISBenutzer, $this->_DB, 52);
    				$SuchParam['FILID'] = $RsFinaleVorgaenge->FeldInhalt('VRK_FILID');
    				$SuchParam['WANR'] = $RsFinaleVorgaenge->FeldInhalt('VRK_WANR');
    		
    				$Bericht->init($SuchParam);
    				$Bericht->SetzeDateipfad($PDFAusgabename);
    				$Bericht->ErzeugePDF(4);
    		
    				$this->SetzeStatus(2, $FILID, $WANR);
    				 
    				if(!file_exists($this->_PfadExportRechDateien.$FILID. '_'.$WANR.'.pdf'))
    				{
    					//L�schen der  zugeh�rigen CSV-Datei
    					$delfiles = '';
    					$directory = opendir($this->_PfadExportRechDateien);
    					
    					while($delfiles = readdir($directory))
    					{
    						if($delfiles != "." && $delfiles != "..")
    						{
    							unlink($this->_PfadExportRechDateien.$FILID. '_'.$WANR.'.csv');
    						}
    					}
    					closedir($directory);
    					
    					//Zur�cksetzen vom Status auf 0
    					$this->SetzeStatus(0, $FILID, $WANR);
    					
    					throw new Exception('Folgende PDF-Rechnung wurde nicht erstellt: '. $FILID. '_'.$WANR.'.pdf' . '. Zugeh�rige CSV-Datei wird gel�scht und Verarbeitungsstatus auf 0 zur�ck gesetzt.');
    					
    				}
    				$RsFinaleVorgaenge->DSWeiter();
    			}
    			$this->debugAusgabe(" ENDE: Erstellen der PDF-Rechnngen abgeschlossen..", 500);
    		}
    		else
    		{
    			$this->debugAusgabe(" Keine Daten zur Rechnungserstellung vorhanden. ", 500);
    			$this->debugAusgabe(" ENDE: Rechnungserstellung abgeschlossen.", 500);
    		}
    		 
    	}
    	catch(Exception $ex)
    	{
    		echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    		echo 'CODE:  '.$ex->getCode().PHP_EOL;
    		echo 'Zeile: '.$ex->getLine().PHP_EOL;
    		echo 'Datei: '.$ex->getFile().PHP_EOL;
    		 
    		$this->_Werkzeuge->EMail($this->_EMAIL_Infos,'ERROR - REPARATURKOSTENVERS',$ex->getMessage(),2,'','shuttle@de.atu.eu');
    		 
    		die();
    	}
    }
    
    public function VerschiebeDateien()
    {
    	try 
    	{
    		$Datei = '';
    		$directory = opendir($this->_PfadExportRechDateien);
    		echo "\n";
    		$this->debugAusgabe(" START: Verschiebe und archiviere die Rechnungsdateien..", 500);
    		 
    		$zaehler = 0;
    		 
    		while($Datei = readdir($directory))
    		{
    			$zaehler ++;
    			if(count(scandir($this->_PfadExportRechDateien)) <= 2)
    			{
    				if($zaehler == 1)
    				{
    					$this->debugAusgabe(" Keine Daten zum Verschieben oder Archivieren vorhanden", 500);
    				}
    			}
    		
    			if ($Datei != '.' && $Datei != '..')
    			{
    				$FILID = 0;
    				$WANR = 0;
    				 
    				$Dateiendung = substr($Datei, -4);
    				$FILID = substr($Datei,0,strpos($Datei,"_"));
    				$WANR = substr($Datei,strpos($Datei,"_") + 1,(strpos($Datei,"."))-(strpos($Datei,"_") + 1));
    				$Dateiname = $FILID.'_'.$WANR;
    				 
    				if(is_file($this->_PfadExportRechDateien.$Dateiname.'.csv') and is_file($this->_PfadExportRechDateien.$Dateiname .'.pdf'))
    				{
    					switch($Dateiendung)
    					{
    						case('.csv'):
    							echo "\n";
    							$this->debugAusgabe(' START: Verschiebe CSV-Datei: ' . $Datei .' in Zielordner f�r FTP-Versand', 500);
    							 
    							if(copy($this->_PfadExportRechDateien.$Datei, $this->_PfadDatenTransfer.$Datei))
    							{
    								$this->debugAusgabe(' ENDE: CSV-Datei: ' . $Datei . ' in Zielordner f�r FTP-Versand verschoben.',500);
    								$this->debugAusgabe(' START: Verschiebe CSV-Datei: ' . $Datei .' ins Archiv',500);
    		
    								if(copy($this->_PfadExportRechDateien.$Datei, $this->_PfadArchivCSV.$Datei))
    								{
    									$this->debugAusgabe(' ENDE: CSV-Datei: ' . $Datei . ' ins Archiv verschoben.', 500);
    									echo "\n";
    								}
    								else
    								{
    									//wenn zugeh�rige PDF bereits im Zielverzeichnis und Archiv vorhanden, dann dort wieder l�schen
    									//plus die CSV l�schen, die bereits im Zielverzeichnis abgelegt worden ist
    									if(file_exists($this->_PfadDatenTransfer.$Dateiname.'.pdf') and file_exists($this->_PfadArchivPDF.$Dateiname .'.pdf'))
    									{
    										unlink($this->_PfadDatenTransfer.$Dateiname.'.pdf');
    										unlink($this->_PfadArchivPDF.$Dateiname.'.pdf');
    											
    										unlink($this->_PfadDatenTransfer.$Dateiname.'.csv');
    									}
    									throw new Exception('Fehler bei der Archivierung der CSV-Datei: '. $Datei);
    								}
    							}
    							else
    							{
    								//wenn zugeh�rige PDF bereits im Zielverzeichnis und Archiv vorhanden, dann dort wieder l�schen
    								if(file_exists($this->_PfadDatenTransfer.$Dateiname.'.pdf') and file_exists($this->_PfadArchivPDF.$Dateiname .'.pdf'))
    								{
    									unlink($this->_PfadDatenTransfer.$Dateiname.'.pdf');
    									unlink($this->_PfadArchivPDF.$Dateiname.'.pdf');
    								}
    								throw new Exception('Fehler beim Kopieren der CSV-Datei: '. $Datei . ' in das Transferverzeichnis');
    							}
    		
    							break;
    		
    						case('.pdf'):
    		
    							$this->debugAusgabe(' START: Verschiebe PDF-Rechnung: ' . $Datei . ' in Zielordner f�r FTP-Versand',500);
    		
    							if(copy($this->_PfadExportRechDateien.$Datei, $this->_PfadDatenTransfer.$Datei))
    							{
    								$this->debugAusgabe(' ENDE: PDF-Rechnung: ' . $Datei . ' in Zielordner f�r FTP-Versand verschoben.',500);
    								$this->debugAusgabe(' START: Verschiebe PDF-Rechnung: ' . $Dateiname . '.pdf ins Archiv',500);
    		
    								if(copy($this->_PfadExportRechDateien.$Dateiname.'.pdf', $this->_PfadArchivPDF.$Dateiname.'.pdf'))
    								{
    									$this->debugAusgabe(' ENDE: PDF-Rechnung: ' . $Dateiname . '.pdf ins Archiv verschoben.',500);
    								}
    								else
    								{
    									//wenn zugeh�rige CSV bereits im Zielverzeichnis vorhanden, dann dort wieder l�schen
    									//plus die PDF l�schen, die bereits im Zielverzeichnis abgelegt worden ist
    									if(file_exists($this->_PfadDatenTransfer.$Dateiname.'.csv') and file_exists($this->_PfadArchivCSV.$Dateiname .'.pdf'))
    									{
    										unlink($this->_PfadDatenTransfer.$Dateiname.'.csv');
    										unlink($this->_PfadArchivCSV.$Dateiname.'.csv');
    		
    										unlink($this->_PfadDatenTransfer.$Dateiname.'.pdf');
    									}
    									throw new Exception('Fehler bei der Archivierung der PDF-Rechnung: '. $Datei);
    								}
    							}
    							else
    							{
    								//wenn zugeh�rige CSV-Datei bereits im Zielverzeichnis und Archiv vorhanden, dann wieder aus FTP-Verzeichnis u. Archiv l�schen
    								if(file_exists($this->_PfadDatenTransfer.$Dateiname.'.csv') and file_exists($this->_PfadArchivCSV.$Dateiname .'.csv'))
    								{
    									unlink($this->_PfadDatenTransfer.$Dateiname.'.csv');
    									unlink($this->_PfadArchivCSV.$Dateiname.'.csv');
    								}
    								throw new Exception('Fehler beim Kopieren der PDF-Rechnung: '. $Datei . ' in das Transferverzeichnis');
    							}
    		
    							break;
    					}
    				}
    				else
    				{
    					$delfiles = '';
    					$directory = opendir($this->_PfadDatenTransfer);
    		
    					while($delfiles = readdir($directory))
    					{
    						if($delfiles != "." && $delfiles != "..")
    						{
    							unlink($this->_PfadDatenTransfer.$delfiles);
    						}
    					}
    					closedir($directory);
    					 
    					throw new Exception('Zu der Datei: '. $Datei . ' fehlt die zugeh�rige PDF-Rechnung oder CSV-Datei');
    				}
    		
    		
    				if(file_exists($this->_PfadDatenTransfer.$Dateiname.'.csv') and file_exists($this->_PfadDatenTransfer.$Dateiname .'.pdf')
    						and file_exists($this->_PfadArchivCSV.$Dateiname.'.csv') and file_exists($this->_PfadArchivPDF.$Dateiname.'.pdf'))
    				{
    					$this->SetzeStatus(3, $FILID, $WANR);
    					unlink($this->_PfadExportRechDateien.$Dateiname.'.csv');
    					unlink($this->_PfadExportRechDateien.$Dateiname.'.pdf');
    				}
    			}
    			 
    		}
    		closedir($directory);
    		
    		$this->debugAusgabe(' ENDE: Verschieben und archivieren der Rechnungsdateien abgeschlossen.',500);
    	}
    	catch(Exception $ex)
    	{
    		echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    		echo 'CODE:  '.$ex->getCode().PHP_EOL;
    		echo 'Zeile: '.$ex->getLine().PHP_EOL;
    		echo 'Datei: '.$ex->getFile().PHP_EOL;
    		 
    		$this->_Werkzeuge->EMail($this->_EMAIL_Infos,'ERROR - REPARATURKOSTENVERS',$ex->getMessage(),2,'','shuttle@de.atu.eu');
    		 
    		die();
    	}
    }
    
    public function SetzeStatus($StatusNr,$FILID,$WANR)
    {
    	try
    	{
    		$BindeVariablen=array();
    		$BindeVariablen['var_N0_vrk_vrs_key']=$StatusNr;
    		$BindeVariablen['var_N0_vrk_filid']=$FILID;
    		$BindeVariablen['var_N0_vrk_wanr']=$WANR;
    
    		$this->_SQL = 'UPDATE versreparaturkosten ';
    		$this->_SQL .= ' SET vrk_vrs_key = :var_N0_vrk_vrs_key ';
    		$this->_SQL .= ' WHERE vrk_filid = :var_N0_vrk_filid ';
    		$this->_SQL .= ' AND vrk_wanr = :var_N0_vrk_wanr ';
    		 
    		$this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
    	}
    	catch(Exception $e)
    	{
    		echo ('Exception:'.$e->getMessage());
    		die();
    	}
    	catch(awisException $awis)
    	{
    		echo ('Exception:'.$awis->getMessage().$awis->getSQL());
    		die();
    	}
    
    }
    
    public function DebugLevel($NeuerLevel = null)
    {
    	if(!is_null($NeuerLevel))
    	{
    		$this->_DebugLevel = intval($NeuerLevel);
    	}
    	return $this->_DebugLevel;
    }
    /*
    * Legt eine INFO f�r die DebugAusgabe() fest
    * @var int
    */
    const PROTOKOLL_INFO = 1;
    /**
     * Legt eine WARNUNG f�r die DebugAusgabe() fest
     * @var int
     */
    const PROTOKOLL_WARNUNG = 2;
    /**
     * Legt einen FEHLER f�r die DebugAusgabe() fest
     * @var int
     */
    const PROTOKOLL_FEHLER = 3;
    /**
     * Schreibt einen Eintrag in die Protokolldatei
     * @param string $Text
     * @param string $Bereich
     * @param int $DebugLevel
     */
//     public function DebugAusgabe($Text, $Bereich, $Typ=self::PROTOKOLL_INFO,$DebugLevel=0)
//     {
//     	if($this->_DebugLevel >= $DebugLevel)
//     	{
//     		$Ziel = $Typ<self::PROTOKOLL_FEHLER?STDOUT:STDERR;
    		 
//     		$Daten[] = date('c');
//     		$Daten[] = ''; //gethostname();
//     		$Daten[] = $this->_DB->Datenbank();
//     		$Daten[] = ($Typ==self::PROTOKOLL_INFO?'I':($Typ==self::PROTOKOLL_WARNUNG?'W':'F'));
//     		$Daten[] = $Bereich;
//     		$Daten[] = str_replace(';', '_*_', $Text);
    		 
//     		fputcsv($Ziel, $Daten, ';');
//     	}
//     }


		/**
		 * Echo't einen Text wenn das Level passt
		 * @param string $Text der Ausgegeben werden soll
		 * @param number $Level Level welches Ben�tigt wird
		 */
	public function debugAusgabe($Text='', $Level=500)
	{
		if ($this->_DebugLevel >= $Level)
		{
			echo date('d.m.y H:i:s ').$Text . PHP_EOL;
		}
	}

}
?>