<?php
global $AWIS_KEY1;
global $awisRSInfo;
global $awisRSInfoName;

$TextKonserven=array();
$TextKonserven[]=array('Adressen','ADR_NAME1');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');

$TXT_AdrSpeichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if($_POST['txtADR_KEY']=='')		// Neue Adresse
{
	if($_POST['txtAdressTypen']==1)		// Firmenadresse
	{
		if($_POST['txtADR_NAME1']=='')	// Name muss angegeben werden
		{
			die('<span class=HinweisText>'.$TXT_AdrSpeichern['Fehler']['err_KeinWert'].' '.$TXT_AdrSpeichern['Adressen']['ADR_NAME1'].'</span>');
		}

		$SQL = 'INSERT INTO Adressen';
		$SQL .= '(ADR_ANREDE,ADR_KONZERN, ADR_NAME1, ADR_NAME2';
		$SQL .= ', ADR_POSTFACH, ADR_POSTFACHPLZ,ADR_STRASSE, ADR_HAUSNUMMER';
		$SQL .= ',ADR_LAN_CODE, ADR_PLZ, ADR_ORT, ADR_BEMERKUNG, ADR_AZG_KEY, ADR_STATUS, ADR_USER, ADR_USERDAT';
		$SQL .= ')VALUES (1';
		$SQL .= ',' . ($_POST['txtADR_KONZERN']==''?'null':'\'' . $_POST['txtADR_KONZERN'] . '\'') . '';
		$SQL .= ',\'' . $_POST['txtADR_NAME1'] . '\'';
		$SQL .= ',' . ($_POST['txtADR_NAME2']==''?'null':'\'' . $_POST['txtADR_NAME2'] . '\'') . '';
		$SQL .= ',' . ($_POST['txtADR_POSTFACH']==''?'null':'\'' . $_POST['txtADR_POSTFACH'] . '\'') . '';
		$SQL .= ',' . ($_POST['txtADR_POSTFACHPLZ']==''?'null':'\'' . $_POST['txtADR_POSTFACHPLZ'] . '\'') . '';
		$SQL .= ',\'' . $_POST['txtADR_STRASSE'] . '\'';
		$SQL .= ',\'' . $_POST['txtADR_HAUSNUMMER'] . '\'';
		$SQL .= ',\'' . $_POST['txtADR_LAN_CODE'] . '\'';
		$SQL .= ',\'' . $_POST['txtADR_PLZ'] . '\'';
		$SQL .= ',\'' . $_POST['txtADR_ORT'] . '\'';
		$SQL .= ',\'' . awisDBString($_POST['txtADR_BEMERKUNG']) . '\'';
		$SQL .= ',' .$_POST['txtADR_AZG_KEY'] . '';
		$SQL .= ',\'A\'';
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';

		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('adressen_speichern.php',1,$awisDBError['message'],'NEU/1');
			die();
		}
		$SQL = 'SELECT seq_ADR_KEY.CurrVal AS KEY FROM DUAL';
		$rsKey = awisOpenRecordset($con,$SQL);
		$Param = $rsKey['KEY'][0].';;';
		$AWIS_KEY1=$rsKey['KEY'][0];
	}
}
else 					// ge�nderte Adresse
{
//awis_Debug(1,$_POST);
	$AWIS_KEY1=$_POST['txtADR_KEY'];
	$SQL = 'SELECT * FROM Adressen WHERE ADR_KEY=0'.$_POST['txtADR_KEY'];

	$rsFelder = awisOpenRecordset($con, $SQL);
	$rsADRFeldListe = $awisRSInfo;
	$Felder = explode(';',awis_NameInArray($_POST,'txtADR',1));
	$Fehler=array();
	$FeldListe='';


	foreach($Felder AS $Feld)
	{

		if(isset($_POST[str_replace('txtADR','oldADR',$Feld)]) AND ($_POST[$Feld].'') != ($_POST[str_replace('txtADR','oldADR',$Feld)].''))
		{
			if($rsFelder[substr($Feld,3)][0] != $_POST[str_replace('txtADR','oldADR',$Feld)])
			{
				$Fehler[] = array(substr($Feld,3),$rsFelder[substr($Feld,3)][0]);
			}
			else
			{
				$Typ = awisFeldInfo($rsADRFeldListe,substr($Feld,3),'Typ');
				$FeldListe .= ', '.substr($Feld,3).'=';
				if($Typ=='VARCHAR')
				{
					$FeldListe .= '\''.$_POST[$Feld].'\'';
				}
				else
				{
					$FeldListe .= ''.$_POST[$Feld].'';
				}
			}


		}
	}

	if($FeldListe!='')			//  Update notwendig?
	{
		$SQL = 'UPDATE ADRESSEN SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ' WHERE ADR_KEY = 0'.$_POST['txtADR_KEY'];

		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('adressen_speichern.php',1,$awisDBFehler['message']);
			die();
		}

	}
}

/*********************************************************************************************************
*
* Ansprechpartner
*
**********************************************************************************************************/

if(isset($_POST['txtAAP_KEY']))
{
	if($_POST['txtAAP_KEY']!=0)
	{
		$SQL = 'SELECT * From AdressenAnsprechpartner WHERE AAP_KEY=0'.$_POST['txtAAP_Key'];
		$rsAAP = awisOpenRecordset($con, $SQL);
		$rsAAPFelder = $awisRSInfo;

		$Felder = explode(';',awis_NameInArray($_POST,'txtAAP_',1));
	//awis_Debug(1,$_POST,$Felder);

		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			if(isset($_POST['old'.substr($Feld,3)]))
			{
				if(isset($rsAAP[substr($Feld,3)][0]) AND $rsAAP[substr($Feld,3)][0]!=$_POST['old'.substr($Feld,3)])
				{


				}
				if($_POST[$Feld] != $_POST['old'.substr($Feld,3)])
				{
					$Typ = awisFeldInfo($rsAAPFelder,substr($Feld,3),'Typ');
					$FeldListe .= ', '.substr($Feld,3).'='.($Typ=='VARCHAR'?'\'':'').$_POST[$Feld].($Typ=='VARCHAR'?'\'':'');

				}
			}
		}

		if($FeldListe != '')
		{
			$SQL = 'UPDATE AdressenAnsprechpartner';
			$SQL .= ' SET '. substr($FeldListe,1);
			$SQL .= ', AAP_USER=\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
			$SQL .= ', AAP_USERDAT=SYSDATE';

			awisExecute($con, $SQL);
		}
	}
	else 		// Neu hinzufügen
	{

		$SQL = 'INSERT INTO AdressenAnsprechpartner';
		$SQL .= '(';
		$SQL .= 'AAP_ADR_Key, AAP_Anrede, AAP_Titel, AAP_Nachname, AAP_Vorname, AAP_Abteilung';
		$SQL .= ',AAP_POSITION, AAP_TELEFON, AAP_EMAIL';
		$SQL .= ',AAP_USER, AAP_USERDAT)VALUES(';
		$SQL .= '\''.$_POST['txtADR_KEY'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_ANREDE'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_TITEL'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_NACHNAME'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_VORNAME'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_ABTEILUNG'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_POSITION'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_TELEFON'].'\'';
		$SQL .= ',\''.$_POST['txtAAP_EMAIL'].'\'';

		$SQL .= ',\''.$_SERVER['PHP_AUTH_USER'].'\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';

		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('adresen_speichern.php',1,$SQL);
		}
	}
}

// Adressinfos speichern
$Felder = awis_NameInArray($_POST,'txtADI_',1,1);
if($Felder!==false)
{
	if($_POST['txtADI_KEY']=='0')  // neu
	{
		$SQL = 'INSERT INTO AdressenInformationen';
		$SQL .= '(adi_xxx_key, adi_adt_key, adi_wert, adi_bemerkung';
		$SQL .= ',adi_user, adi_userdat)';
		$SQL .= 'VALUES(';
		$SQL .= ' '.$_POST['txtADI_XXX_KEY'].'';
		$SQL .= ','.$_POST['txtADI_ADT_KEY'].'';
		$SQL .= ',\''.$_POST['txtADI_WERT'].'\'';
		$SQL .= ',\''.$_POST['txtADI_BEMERKUNG'].'\'';
		$SQL .= ',\''.$_SERVER['PHP_AUTH_USER'].'\'';
		$SQL .= ', SYSDATE )';
	}
	else 	// �nderung
	{
		$SQL = '';
		$rsADI = awisOpenRecordset($con,'SELECT * FROM AdressenInformationen WHERE adi_key=0'.$_POST['txtADI_KEY']);

		$FelderAlt = explode(';',awis_NameInArray($_POST,'oldADI_',1,1));
		$FehlerListe=array();
		$UpdateListe='';
		foreach($FelderAlt AS $Feld)
		{
			if(isset($_POST['old'.substr($Feld,3)]))
			{
				$WertAlt = $_POST[$Feld];
				$WertNeu = $_POST['txt'.substr($Feld,3)];
				$WertDB = $rsADI[substr($Feld,3)][0];
				if($WertDB != $WertAlt)
				{
					$FehlerListe[] = array(substr($Feld,3),$WertAlt,$WertDB);
				}
				elseif($WertAlt != $WertNeu)
				{

					$UpdateListe .= ', '.substr($Feld,3).'=';
					if($awisRSInfoName[substr($Feld,3)]['TypKZ']=='T')
					{
						$UpdateListe.='\''.$WertNeu.'\'';
					}
					else
					{
						$UpdateListe.=''.$WertNeu.'';
					}
				}
			}
		}
		if(sizeof($FehlerListe)!==0)
		{
			echo '<span class=HinweisText>';

			echo $TXT_AdrSpeichern['Fehler']['err_FelderVeraendert'].'';
			echo '<br>'.$rsADI['ADI_USER'][0].', '.$rsADI['ADI_USERDAT'][0];
			echo '<br>';

			foreach($FehlerListe AS $Fehler)
			{
				echo '<br>'.$Fehler[0].': '.$TXT_AdrSpeichern['Wort']['geaendert_von'].' "'.$Fehler[1].'" '.$TXT_AdrSpeichern['Wort']['geaendert_auf'].' "'.$Fehler[2].'"';

			}

			echo '</span>';
		}
		else
		{
			$SQL = 'UPDATE AdressenInformationen';
			$SQL .= ' SET ADI_User=\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
			$SQL .= ',ADI_UserDat=SYSDATE';
			$SQL .= $UpdateListe;
			$SQL .= ' WHERE ADI_KEY=0'.$_POST['txtADI_KEY'];
		}

	}
	if($SQL!='')
	{
		if(awisExecute($con,$SQL,true,false,0)==false)
		{
			global $awisDBError;
			awisErrorMailLink('adressen_infos_speichern',1,$awisDBError['message'],'');
		}
	}
}



?>