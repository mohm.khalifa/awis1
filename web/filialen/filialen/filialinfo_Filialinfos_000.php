<?php
	global $SpeichernButton;		// Anzeige eines Speichern-Knopfs

	/************************************************
	* 
	* Daten anzeigen
	* 
	* **********************************************/

	 // Daten f�r die Infoseite sammeln
	 $SQL = "SELECT filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, filialinfostypen.fit_sortierung,
       			filialinfos.fif_wert, fit_id, FIF_KEY
				FROM filialinfos, filialinfostypen, filialinfostypengruppen
				 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
        			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
					AND FIF_FIL_ID=0" . $rsFiliale["FIL_ID"][0] . ")";
					
	 $rsFilialInfo = awisOpenRecordset($con, $SQL);	 

	// �berschrift schreiben
	print "<input type=hidden name=FILID value=" . $rsFiliale["FIL_ID"][0] . ">";	// F�r Submit
	print "<table width=100% border=0><tr><td align=left>";
	print "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
//	print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos';></td></tr></table>";
	print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos';></td></tr></table>";
	print "<hr>";
	
	print "<table width=100% id=DatenTabelle><tr>";		// Gro�er Rahmen mit drei Spalten

	print "<td valign=top width=24%><table border=0>";
	print "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_BEZ"][0] . "</span></td></tr>";
	print "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_STRASSE"][0] . "</span></td></tr>";
	print "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_PLZ"][0] . " " . $rsFiliale["FIL_ORT"][0] . " " . $rsFiliale["FIL_ORTSTEIL"][0] . "</span></td></tr>";

		// Neue Adresse pr�fen!
	
	$Erg = SucheFilialInfo($rsFilialInfo, 104, "FIT_ID");		// Neue Strasse

	If($Erg <> -1 AND awis_format($rsFilialInfo["FIF_WERT"][$Erg],'SortDatum') > date("Ymt"))
	{
		print "<tr><td>&nbsp;</td></tr>";
		print "<tr><td><span Class=HinweisText>ACHTUNG</span></td></tr>";
		print "<tr><td><span Class=HinweisText>Neue Adresse ab " . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
		print "<tr><td>&nbsp;</td></tr>";

		$Erg = SucheFilialInfo($rsFilialInfo, 101, "FIT_ID");		// Neue Strasse
 		print "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
		$Erg = SucheFilialInfo($rsFilialInfo, 102, "FIT_ID");		// Neue Plz
 		print "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
		$Erg = SucheFilialInfo($rsFilialInfo, 103, "FIT_ID");		// Neuer Ort
 		print "" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

		print "<tr><td>&nbsp;</td></tr>";

	}
	
	print "</table></td>";
		// Ende Filialadresse
		 
	 
		// Zweite Spalte	 
 	print "<td valign=top width=33%><table border=0>";
	 
	 
	$Erg = SucheFilialInfo($rsFilialInfo, 3, "FIT_ID");		// Kundentelefon
	print "<tr><td><table border=0>";
 	print "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

	$Erg = SucheFilialInfo($rsFilialInfo, 2, "FIT_ID");		// Faxnummer
 	print "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

	print "</table>";

		// �ffnungszeiten
	print "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>�ffnungszeiten</span></td></tr>";
	
	$Erg = SucheFilialInfo($rsFilialInfo, 121, "FIT_ID");		// �ffnung Mo-Fr
 	print "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>Mo-Fr : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	$Erg = SucheFilialInfo($rsFilialInfo, 122, "FIT_ID");
 	print "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";
	
	$Erg = SucheFilialInfo($rsFilialInfo, 123, "FIT_ID");		// �ffnung Sa
 	print "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>Sa : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	$Erg = SucheFilialInfo($rsFilialInfo, 124, "FIT_ID");
 	print "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";
	
	print "<tr><td>&nbsp;</td></tr>";		// Leerzeile

	$Erg = SucheFilialInfo($rsFilialInfo, 34, "FIT_ID");		// Er�ffnungsdatum
 	print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Er�ffnet am : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];

			//********************************
			// Stundensatz der Filialen
			//********************************
	$RechteStufe = awisBenutzerRecht($con, 110);
	if($RechteStufe>0)
	{
		print "<tr><td>&nbsp;</td></tr>";		// Leerzeile
		$Erg = SucheFilialInfo($rsFilialInfo, 200, "FIT_ID");		// Stundensatz
		if(($RechteStufe & 2)==2)
		{
			$Ausgabe = number_format(str_replace(",",".",$rsFilialInfo["FIF_WERT"][$Erg]),2,",","");
 			print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Stundensat<u>z</u> : </span>";
			print "<span class=DatenFeldNormalFett><input accesskey=z type=text size='" . strlen($Ausgabe) . "' name=txtStundenSatz value='" . $Ausgabe . "'></span>�";
			print "<input align=right type=hidden name=txtFIF_KEY_StundenSatz value='" .  $rsFilialInfo["FIF_KEY"][$Erg] . "'>";
			$SpeichernButton = True;
			print "</td></tr>";
		}
		else
		{
 			print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Stundensatz : </span><span class=DatenFeldNormalFett>" . number_format($rsFilialInfo["FIF_WERT"][$Erg],2,",","") . "�</span></td></tr>";
		}
	}

	print "</table></td>";	// Ende zweite Spalte

	 	// Dritte Spalte
	print "<td valign=top width=43%><table border=0>";

				// 1. Gesch�ftsleiter-ID
	SchreibePersonenFeld("1. Gesch�ftsleiter",150,74,$rsFilialInfo,$con,true);

		// 2. Gesch�ftsleiter-ID
	SchreibePersonenFeld("2. Gesch�ftsleiter",150,75,$rsFilialInfo,$con,true);

		// 1. Werkstattleiter
	SchreibePersonenFeld("1. Werkstattleiter",150,76,$rsFilialInfo,$con,true);
	
		// Werkstattleiter2
	SchreibePersonenFeld("2. Werkstattleiter",150,77,$rsFilialInfo,$con,true);

		// Verkaufsleiter
	SchreibePersonenFeld("Verkaufsleiter",150,71,$rsFilialInfo,$con,false);
	
		// Gebietsleiter
	SchreibePersonenFeld("Gebietsleiter",150,72,$rsFilialInfo,$con,false);

		// TKDL Kundenbeschwerden

	$Ausgabe="";
 	print "<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>TKDL Kunden- beschwerden : </span></td><td valign=bottom><span class=DatenFeldNormalFett>";

	$Erg = SucheFilialInfo($rsFilialInfo, 26, "FIT_ID");		// Regionalzentrum
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		
		$SQL = "SELECT RIF_WERT FROM RegionalZentrumInfos";
		$SQL = $SQL . " WHERE RIF_RIT_ID=10 AND RIF_REZ_ID=0" . $rsFilialInfo["FIF_WERT"][$Erg];
		$rsPersonenListe = awisOpenRecordset($con, $SQL);

		if($awisRSZeilen>0)				
		{
			$rsPerson = awisOpenRecordset($con,"SELECT * FROM PERSONAL WHERE PER_NR IN(" . $rsPersonenListe["RIF_WERT"][0] . ")");
		}
		$Ausgabe = "";
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			if($i>0)
			{
				$Ausgabe.="<br>";
			}
				// Schichtplan - Infos?
			if(awisHatDasRecht(112)>0)
			{
				$Ausgabe .= "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsPerson["PER_NR"][$i]) . ">";
				$Ausgabe .= $rsPerson["PER_NACHNAME"][$i] . ", " . $rsPerson["PER_VORNAME"][$i];
				$Ausgabe .= "</a>";
			}
			else
			{
				$Ausgabe .= $rsPerson["PER_NACHNAME"][$i] . ", " . $rsPerson["PER_VORNAME"][$i];
			}
		}
		print "" . $Ausgabe . "</span></td></tr></table></td></tr>";

	}	// Ende Regionalzentrum-Infos
 	 
	print "<tr><td>&nbsp;</td></tr>";		// Leerzeile
	
	$Erg = SucheFilialInfo($rsFilialInfo, 49, "FIT_ID");		// Anlieferung
 	print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Anlieferungstag : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	if($rsFiliale["FIL_LAGERKZ"][0]=="N")
	{
		print " aus Weiden";
	}
	elseif($rsFiliale["FIL_LAGERKZ"][0]=="L")
	{
		print " aus Werl";
	}
	else
	{
		print " aus " . $rsFiliale["FIL_LAGERKZ"][0] . "";
	}
	print "</span></td></tr>";
	 	 
	$Erg = SucheFilialInfo($rsFilialInfo, 48, "FIT_ID");		// Kommissioniertag
 	print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Kommissioniertag : </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

		// Regionalzentrum
		
	$Erg = SucheFilialInfo($rsFilialInfo, 26, "FIT_ID");		// Regionalzentrum
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$rsRegionalZentrum = awisOpenRecordset($con, "SELECT * FROM RegionalZentren WHERE REZ_ID='". $rsFilialInfo["FIF_WERT"][$Erg] ."'");
		If($rsRegionalZentrum["REZ_BEZEICHNUNG"][0]!="")
		{
			print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span><span class=DatenFeldNormalFett>" . $rsRegionalZentrum["REZ_BEZEICHNUNG"][0]  . "</span></td></tr>";
		}
		else
		{
			print "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
		}
	}

	

	print "</table></td>";	// Ende dritte Spalte

	print "</tr></table>";	// Ende Datentabelle

		//*******************************************
		// T�V-Termine
		//*******************************************

	$Ausgabe = "";
	print "<span class=DatenFeldNormalFett>T�V-Termine</span></br>";
	$Erg = SucheFilialInfo($rsFilialInfo, 41, "FIT_ID");		// T�V-Termin Montag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe = ", <span class=DatenFeldNormalFett>Montag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 42, "FIT_ID");		// T�V-Termin Dienstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>Dienstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 43, "FIT_ID");		// T�V-Termin Mittwoch
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>Mittwoch</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 44, "FIT_ID");		// T�V-Termin Donnerstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>Donnerstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 45, "FIT_ID");		// T�V-Termin Freitag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>Freitag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 46, "FIT_ID");		// T�V-Termin Samstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>Samstag</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}

	If($Ausgabe)	
	{
 		print "<span class=DatenFeldNormal>" . substr($Ausgabe,2) . "</span><br>";
	}
	else
	{
		print "<span class=DatenFeldNormal>keine.</span><br>";
	}

		// T�V - Preise
	
	$RechteStufe = awisBenutzerRecht($con, 111);
	if($RechteStufe>0)
	{
		print "<table border=0>";

		$Ausgabe="";
		$Erg = SucheFilialInfo($rsFilialInfo, 201, "FIT_ID");		// Min T�V Preis
		$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
		print "<tr><td width=100><span class=DatenFeldNormal>Preis f�r HU:</td><td>" . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
		$Erg = SucheFilialInfo($rsFilialInfo, 202, "FIT_ID");		// Max T�V Preis
		
		if($Ausgabe <> $rsFilialInfo["FIF_WERT"][$Erg])
		{
			print " - " . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
		}
			
		print "</span></td></tr></table>";
	}
	
		//*******************************************
		// Wegbeschreibung
		//*******************************************
		
	print "<br><span class=DatenFeldNormalFett>Wegbeschreibung</span></br>";
	$Erg = SucheFilialInfo($rsFilialInfo, 47, "FIT_ID");		// Wegbeschreibung
	If($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
 		print "<span class=DatenFeldNormal>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span>";
	}
	else
	{
		print "<span class=DatenFeldNormal>keine gefunden.</span>";
	}

//	print "<input type=hidden name=cmdAktion value=FilialInfo>"	;


/************************************************************************************
* 
* Hilfsfunktionen
* 
* **********************************************************************************/
Function SucheFilialInfo($rsFiliaInfo, $InfoID, $Spalte)
{
	//for($i=0;$i<count($rsFiliaInfo);$i++)
	$i=0;
	while($rsFiliaInfo[$Spalte][$i]!="")
	{
	//if($InfoID==41){ print "X " . count($rsFiliaInfo) . "X:" . $rsFiliaInfo[$Spalte][$i]. "-" . $InfoID;}
		if($rsFiliaInfo[$Spalte][$i]==$InfoID)
		{
			return $i;
		}
		$i++;
	}
	return -1;
}

/************************************************************
* 
* Schreibt eine Person aus der Filiale mit einem Link
* 
*************************************************************/
Function SchreibePersonenFeld($Beschreibung, $Breite, $WertID, $rsFilialInfo, $con, $Link)
{

	$Ausgabe="";
 	print "<tr><td id=DatenFeld><table border=0><tr><td width=$Breite><span class=DatenFeldNormal>$Beschreibung : </span></td><td><span class=DatenFeldNormalFett>";
	$Erg = SucheFilialInfo($rsFilialInfo, $WertID, "FIT_ID");

	if($rsFilialInfo["FIF_WERT"][$Erg]!="" AND $Link AND awisHatDasRecht(112)>0)
	{
		$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsFilialInfo["FIF_WERT"][$Erg]) . ">";
	}
	$rsPerson = awisOpenRecordset($con,"SELECT * FROM PERSONAL WHERE PER_NR='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'");

	If($Ausgabe!="")
	{
		$Ausgabe .= $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0] . "</a>";
	}
	else
	{
		if($rsPerson["PER_NACHNAME"][0]!="")
		{
			$Ausgabe = $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0];
		}
	}
	print "" . $Ausgabe . "</span></td></tr></table></td></tr>";

}


?>