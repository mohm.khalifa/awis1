<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class verskonditionen_funktionen
{
    public $AWIS_KEY1;
    public $AWIS_KEY2;
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht9400;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;
    public $AenderungsHistorie;
    private $_EditRecht;

    function __construct($Benutzer = '')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht9400 = $this->AWISBenutzer->HatDasRecht(9400);
        $this->_EditRecht = (($this->Recht9400 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_VKD'));
        $this->AWISWerkzeug = new awisWerkzeuge();
        $this->AenderungsHistorie = new awisAenderungshistorie($this->DB, $this->AWISBenutzer);

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('VKD', '%');
        $TextKonserven[] = array('VVE', '%');
        $TextKonserven[] = array('VVS', '%');
        $TextKonserven[] = array('VKR', '%');
        $TextKonserven[] = array('XAH', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Dateiname');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'Ja');
        $TextKonserven[] = array('Wort', 'Nein');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_verskonditionen');
        $TextKonserven[] = array('Liste', 'lst_SCHADKZ');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_VKD', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht9400 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(),
                'VKD'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function ErstellePosUeberschrift($schadenKZ, $FeldBreiten, $EditRecht, $konditionVorsteuerabzug)
    {
        $this->Form->Trennzeile('O');

        $this->Form->ZeileStart();
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_POSITION'], $FeldBreiten['POSITION']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_ARTNR'], $FeldBreiten['ARTNR']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_STEUERSATZ'], $FeldBreiten['STEUERSATZ']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_PREIS_NETTO'], $FeldBreiten['PREIS_NETTO']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_PREIS_BRUTTO'], $FeldBreiten['PREIS_BRUTTO']);

        if ($schadenKZ == 'S') {
            $konditionVorsteuerabzug ? $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_VSTABZUG'], $FeldBreiten['VSTABZUG']) : '';
        } elseif ($schadenKZ == 'P') {
            $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_HOECHSTGRENZE'], $FeldBreiten['HOECHSTGRENZE']);
            $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_MIN_AUSBUCHUNG'], $FeldBreiten['MIN_AUSBUCHUNG']);
            $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_MAX_AUSBUCHUNG'], $FeldBreiten['MAX_AUSBUCHUNG']);
            $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['VKD']['VKD_SB'], $FeldBreiten['SB']);
        }
        $this->Form->ZeileEnde();
    }


    public function CheckDatum($DatumGueltigAb, $DatumGueltigBis, $AWIS_KEY, $schadenKZ = '', $VVE_VERSNR = 0)
    {
        $Fehler = '';
        if (strtotime($DatumGueltigAb) > strtotime($DatumGueltigBis)) {
            $Fehler .= 'Das Datum f�r "G�ltig ab" ist gr��er als das Datum f�r "G�ltig Bis". <br>';
        }
        $TextBaustein = 'Das Datum f�r #Datum# liegt in der Vergangenheit. <br>';

        if (strtotime($DatumGueltigBis) < strtotime(date('d.m.Y'))) {
            $Fehler .= str_replace('#Datum#', '"G�ltig bis": ' . $DatumGueltigBis, $TextBaustein);
        }
        //if ($AWIS_KEY == -1) {
        if (strtotime($DatumGueltigAb) < strtotime(date('d.m.Y'))) {
            $Fehler .= str_replace('#Datum#', '"G�ltig ab": ' . $DatumGueltigAb, $TextBaustein);
        }

        $SQL = 'SELECT DISTINCT TRUNC(VKR_GUELTIG_BIS) as VKR_GUELTIG_BIS, VKR_KEY';
        $SQL .= ' FROM VERSKONDITIONEN VKD';
        $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
        $SQL .= ' ON VKD.VKD_VVE_VERSNR = VVE.VVE_VERSNR';
        $SQL .= ' INNER JOIN VERSKONDREF VKR';
        $SQL .= ' ON VKR.VKR_KEY = VKD.VKD_VKR_KEY';
        $SQL .= ' WHERE VKD_SKZ = ' . $this->DB->WertSetzen('VKD', 'T', $schadenKZ);
        $SQL .= ' AND VVE.VVE_VERSNR = ' . $this->DB->WertSetzen('VKD', 'N0', $VVE_VERSNR);
        $SQL .= ' AND TRUNC(VKR.VKR_GUELTIG_BIS) >= ' . $this->DB->WertSetzen('VKD', 'D', $DatumGueltigAb);
        $SQL .= ' AND VKR_KEY <> ' . $this->DB->WertSetzen('VKD', 'N0', $AWIS_KEY);

        if ($this->DB->ErmittleZeilenAnzahl($SQL, $this->DB->Bindevariablen('VKD', false)) > 0) {
            $rsGueltigBis = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('VKD'));

            $Fehler .= 'Zu den angegebenen Daten existiert bereits eine Kondition, die bis zum: ' . $this->Form->Format('D', $rsGueltigBis->FeldInhalt('VKR_GUELTIG_BIS')) . ' g�ltig ist. <br> ';
            $Fehler .= ' Bitte entweder die bisherige Kondition bis zum ' . $DatumGueltigAb . ' auslaufen lassen, <br>';
            $Fehler .= ' oder ein anderes Datum f�r "G�ltig ab" verwenden.';

        }
        $this->DB->Bindevariablen('VKD');

        //}

        return $Fehler;
    }

    public function KonditionsDaten () {
        $SQL = 'SELECT * ';
        $SQL .= 'FROM VERSKONDITIONEN ';
        $SQL .= 'WHERE VKD_VKR_KEY = ' . $this->DB->WertSetzen('VKD', 'Z', $this->AWIS_KEY1);
        $SQL .= ' ORDER BY VKD_POSITION';

        $rsVKD = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('VKD'));

        return $rsVKD;
    }

    public function CheckRequestDaten ($Post, $SKZ) {
        $Fehler = '';
        if($Post['txtVVE_VERSNR'] == '' OR is_null($Post['txtVVE_VERSNR'])) {
            $Fehler .= 'Keine Versicherung ausgew�hlt ';
        }
        if($Post['txtVKR_GUELTIG_AB'] == '' OR is_null($Post['txtVKR_GUELTIG_AB'])) {
            $Fehler .= '/ Kein G�ltig ab Datum vorhanden ';
        }
        if($Post['txtVKR_GUELTIG_BIS'] == '' OR is_null($Post['txtVKR_GUELTIG_BIS'])) {
            $Fehler .= '/ Kein G�ltig bis Datum vorhanden ';
        }

        return $Fehler;
    }

    public function BedingungErstellen()
    {
        $Bedingung = '';

        if ($this->AWIS_KEY1 != 0) {
            $Bedingung .= ' AND VKR_KEY = ' . $this->DB->WertSetzen('VKD', 'N0', $this->AWIS_KEY1);

            return $Bedingung;
        }
        if (isset($this->Param['VERS_KONZERN']) and $this->Param['VERS_KONZERN'] != '') {
            $VersKonzern = mb_strtoupper(implode("','", $this->Param['VERS_KONZERN']));
            $Bedingung .= " and upper(VVE_KUERZEL) in ('$VersKonzern')";
        }
        if (isset($this->Param['VKD_SKZ']) and $this->Param['VKD_SKZ'] != '') {
            $Bedingung .= " AND VKR_SKZ = " . $this->DB->WertSetzen('VKD', 'T', $this->Param['VKD_SKZ']);
        }
        if (isset($this->Param['VKD_GUELTIG_AB']) and $this->Param['VKD_GUELTIG_AB'] != '') {
            $Bedingung .= " AND VKR_GUELTIG_AB >= " . $this->DB->WertSetzen('VKD', 'DU', $this->Param['VKD_GUELTIG_AB'].' 00:00:00');
        }
        if (isset($this->Param['VKD_GUELTIG_BIS']) and $this->Param['VKD_GUELTIG_BIS'] != '') {
            $Bedingung .= " AND VKR_GUELTIG_BIS <= " . $this->DB->WertSetzen('VKD', 'DU', $this->Param['VKD_GUELTIG_BIS'].' 23:59:59');
        }
        return $Bedingung;
    }
}