<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Rechtestufe = awisBenutzerRecht($con, 1502);
		//  1=Einsehen
		//	2=Hinzuf�gen
		//	4=Bearbeiten
		//  8=L�schen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schluessel_Nummern',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug('entwick',0,false,$_REQUEST);

$txtSLNKEY=(isset($_REQUEST['txtSLN_KEY'])?$_REQUEST['txtSLN_KEY']:''); //Kein Unterschied ob POST oder GET

if(isset($_POST['cmdTrefferliste_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLNKEY=0;
}

if(isset($_REQUEST['txtFIL_ID']) && $_REQUEST['txtFIL_ID']!='')			
{
	$Params[0] = $_REQUEST['txtFIL_ID'];
	
	awis_BenutzerParameterSpeichern($con, "SchluesselNummern", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}

$anzeigen=false;

if(isset($_REQUEST['txtSLN_KEY_loeschen']) && $_REQUEST['txtSLN_KEY_loeschen']!='')
{
		$SQL='SELECT * FROM SCHLUESSELVERGABEN WHERE SLV_SLN_KEY=0'.$_REQUEST['txtSLN_KEY_loeschen'];
		$rsSLN_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLN_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		if($rsSLN_KEY_LOESCHENZeilen>0)
		{
				echo "<span class=HinweisText>Datensatz kann nicht gel&ouml;scht werden. L&ouml;sen Sie erst die Zuordnung auf.</span>";
				
				$anzeigen=true;
		}
		else
		{
				$SQL='DELETE FROM SCHLUESSELNUMMERN WHERE SLN_KEY=0'.$_REQUEST['txtSLN_KEY_loeschen'];
				
				$anzeigen=true;
				
				if(awisExecute($con, $SQL)===FALSE)
				{
				awisErrorMailLink("schluessel_Nummern.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
				}
		}
}

if(isset($_POST['cmdSpeichern_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
		
		if($txtSLNKEY>0) // UPDATE-Anweisung
		{		
			$Params=explode(';',awis_BenutzerParameter($con, "SchluesselNummern", $AWISBenutzer->BenutzerName()));
		
				//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
				//--->
		
				$SQL='SELECT * FROM SCHLUESSELNUMMERN WHERE SLN_KEY=0'.$txtSLNKEY;
				$rsSchluesselnummern = awisOpenRecordset($con,$SQL);
				$rsSchluesselnummernZeilen = $awisRSZeilen;
				
				$SQL='';
				$txtHinweis='';
				
				if($rsSchluesselnummernZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
				}
				
				if($_POST['txtSLN_SchluesselNr'] != $_POST['txtSLN_SchluesselNr_old'])
				{
						if($rsSchluesselnummern['SLN_SCHLUESSELNUMMER'][0] != $_POST['txtSLN_SchluesselNr_old'])
						{
								$txtHinweis.=',Schl&uuml;sselnummer von '.$_POST['txtSLN_SchluesselNr_old']. ' auf '.$rsSchluesselnummern['SLN_SCHLUESSELNUMMER'][0];
						}
						else
						{
								$SQL.=',SLN_SCHLUESSELNUMMER=\''.$_POST['txtSLN_SchluesselNr'].'\'';
						}
				}
				
				if($_POST['txtSLN_SicherheitskartenNr'] != $_POST['txtSLN_SicherheitskartenNr_old'])
				{
						if($rsSchluesselnummern['SLN_SICHERKARTENNUMMER'][0] != $_POST['txtSLN_SicherheitskartenNr_old'])
						{
								$txtHinweis.=',Sicherheitskartennummer von '.$_POST['txtSLN_SicherheitskartenNr_old']. ' auf '.$rsSchluesselnummern['SLN_SICHERKARTENNUMMER'][0];
						}
						else
						{
								$SQL.=',SLN_SICHERKARTENNUMMER=\''.$_POST['txtSLN_SicherheitskartenNr'].'\'';
						}
				}
				
				if($_POST['txtSLN_SLL_KEY'] != $_POST['txtSLN_SLL_KEY_old'])
				{
						if($rsSchluesselnummern['SLN_SLL_KEY'][0] != $_POST['txtSLN_SLL_KEY_old'])
						{
								$txtHinweis.=',Lieferant von '.$_POST['txtSLN_SLL_KEY_old']. ' auf '.$rsSchluesselnummern['SLN_SLL_KEY'][0];
						}
						else
						{
								$HilfsSQL="SELECT NVL2(MAX(SLN_SCHLUESSELLFDNR),MAX(SLN_SCHLUESSELLFDNR),0) AS LFDNR FROM SCHLUESSELNUMMERN ";
								$HilfsSQL.="WHERE SLN_FIL_ID=" . $Params[0] . " AND SLN_SLL_KEY=" . $_POST['txtSLN_SLL_KEY'] . " ";
								$HilfsSQL.="AND SLN_SLG_KEY=" . $_POST['txtSLN_SLG_KEY'] . " AND SLN_SLU_KEY=" . $_POST['txtSLN_SLU_KEY'];
								
								$rsSLN_LFDNR = awisOpenRecordset($con,$HilfsSQL);
								$rsSLN_LFDNRZeilen = $awisRSZeilen;
								
								$txtSLN_LFDNR=0;
								$txtSLN_LFDNR=$rsSLN_LFDNR['LFDNR'][0];
								$txtSLN_LFDNR=$txtSLN_LFDNR+1;
							
							
								$SQL.=',SLN_SCHLUESSELLFDNR='.$txtSLN_LFDNR.',SLN_SLL_KEY=\''.$_POST['txtSLN_SLL_KEY'].'\'';
						}
				}
				
				if($_POST['txtSLN_SLG_KEY'] != $_POST['txtSLN_SLG_KEY_old'])
				{
						if($rsSchluesselnummern['SLN_SLG_KEY'][0] != $_POST['txtSLN_SLG_KEY_old'])
						{
								$txtHinweis.=',Schl&uuml;sselgruppe von '.$_POST['txtSLN_SLG_KEY_old']. ' auf '.$rsSchluesselnummern['SLN_SLG_KEY'][0];
						}
						else
						{
								$SQL.=',SLN_SLG_KEY=\''.$_POST['txtSLN_SLG_KEY'].'\'';
						}
				}
				
				if($_POST['txtSLN_SLU_KEY'] != $_POST['txtSLN_SLU_KEY_old'])
				{
						if($rsSchluesselnummern['SLN_SLU_KEY'][0] != $_POST['txtSLN_SLU_KEY_old'])
						{
								$txtHinweis.=',Schl&uuml;sseluntergruppe von '.$_POST['txtSLN_SLU_KEY_old']. ' auf '.$rsSchluesselnummern['SLN_SLU_KEY'][0];
						}
						else
						{
								$SQL.=',SLN_SLU_KEY=\''.$_POST['txtSLN_SLU_KEY'].'\'';
						}
				}
				
				if($_POST['txtSLN_BEMERKUNG'] != $_POST['txtSLN_BEMERKUNG_old'])
				{
						if($rsSchluesselnummern['SLN_BEMERKUNG'][0] != $_POST['txtSLN_BEMERKUNG_old'])
						{
								$txtHinweis.=',Bermerkung von '.$_POST['txtSLN_BEMERKUNG_old']. ' auf '.$rsSchluesselnummern['SLN_BEMERKUNG'][0];
						}
						else
						{
								$SQL.=',SLN_BEMERKUNG=\''.$_POST['txtSLN_BEMERKUNG'].'\'';
						}
				}
				
				if($_POST['txtSLN_RechnungsNr'] != $_POST['txtSLN_RechnungsNr_old'])
				{
						if($rsSchluesselnummern['SLN_RECHNUNGSNUMMER'][0] != $_POST['txtSLN_RechnungsNr_old'])
						{
								$txtHinweis.=',Rechnungsnummer von '.$_POST['txtSLN_RechnungsNr_old']. ' auf '.$rsSchluesselnummern['SLN_RECHNUNGSNUMMER'][0];
						}
						else
						{
								$SQL.=',SLN_RECHNUNGSNUMMER=\''.$_POST['txtSLN_RechnungsNr'].'\'';
						}
				}
				
				//<---
				
				// Update- Befehl
				
				if($txtHinweis=='' && $SQL!='')
				{
						
						$SQL.=',SLN_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						
						$SQL.=',SLN_USERDAT=SYSDATE';
				
						$SQL='UPDATE SCHLUESSELNUMMERN SET ' .substr($SQL,1).' WHERE SLN_KEY=0'.$txtSLNKEY;
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Nummern.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
				}
				elseif($txtHinweis!='')
				{
						echo $txtHinweis;
						awislogoff($con);
						
						die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSchluesselnummern['SLN_USER'][0] ." ge�ndert !</span>");
				}
		}
		else // INSERT-Anweisung
		{
				if(is_numeric($_POST['txtSLN_Anzahl'])==True && $_POST['txtSLN_Anzahl']>1)
				{
						$Params=explode(';',awis_BenutzerParameter($con, "SchluesselNummern", $AWISBenutzer->BenutzerName()));
						
						$Anzahl=$_POST['txtSLN_Anzahl'];
						
						if($_POST['txtSLN_SLG_KEY']<0)
						{
								awislogoff($con);
								die("<span class=HinweisText>Sie m�ssen eine Schl&uuml;ssel-Gruppe angeben!</span>");
						}
					
						for($i=1;$i<=$Anzahl;$i++)
						{		
								$HilfsSQL="SELECT NVL2(MAX(SLN_SCHLUESSELLFDNR),MAX(SLN_SCHLUESSELLFDNR),0) AS LFDNR FROM SCHLUESSELNUMMERN ";
								$HilfsSQL.="WHERE SLN_FIL_ID=" . $Params[0] . " AND SLN_SLL_KEY=" . $_POST['txtSLN_SLL_KEY'] . " ";
								$HilfsSQL.="AND SLN_SLG_KEY=" . $_POST['txtSLN_SLG_KEY'] . " AND SLN_SLU_KEY=" . $_POST['txtSLN_SLU_KEY'];
								
								$rsSLN_LFDNR = awisOpenRecordset($con,$HilfsSQL);
								$rsSLN_LFDNRZeilen = $awisRSZeilen;
								
								$txtSLN_LFDNR=0;
								$txtSLN_LFDNR=$rsSLN_LFDNR['LFDNR'][0];
								$txtSLN_LFDNR=$txtSLN_LFDNR+1;
								
								if(empty($_POST['txtSLN_SchluesselNr']))
								{
										awislogoff($con);
										die("<span class=HinweisText>Sie m�ssen eine Schl&uuml;ssel-Nummer angeben!</span>");
								}
								else{
										$SQL='INSERT INTO SCHLUESSELNUMMERN (SLN_SCHLUESSELNUMMER,SLN_SCHLUESSELLFDNR,SLN_SICHERKARTENNUMMER,SLN_FIL_ID,SLN_SLL_KEY,SLN_SLG_KEY,SLN_SLU_KEY,SLN_BEMERKUNG,SLN_USER,SLN_USERDAT,SLN_RECHNUNGSNUMMER)';
										$SQL.='VALUES(\''.$_POST['txtSLN_SchluesselNr'].'\',' . $txtSLN_LFDNR . ' ,\''.$_POST['txtSLN_SicherheitskartenNr'].'\',' . $Params[0] . ','.$_POST['txtSLN_SLL_KEY'].','.$_POST['txtSLN_SLG_KEY'].','.$_POST['txtSLN_SLU_KEY'].',\''.substr($_POST['txtSLN_BEMERKUNG'],0,255).'\',\''.$AWISBenutzer->BenutzerName().'\',SYSDATE,\''.$_POST['txtSLN_RechnungsNr'].'\')';
								}
								
								if(awisExecute($con, $SQL)===FALSE)
								{
									awisErrorMailLink("schluessel_Nummern.php", 2, $awisDBFehler);
									awisLogoff($con);
									die();
								}
								
								$rsSLN_KEY = awisOpenRecordset($con,'SELECT SEQ_SLN_KEY.CURRVAL AS KEY FROM DUAL');
								
								$txtSLNKEY=$rsSLN_KEY['KEY'][0];
								$anzeigen=true;
						}
				}
				elseif(is_numeric($_POST['txtSLN_Anzahl'])==True && $_POST['txtSLN_Anzahl']==1)
				{				
						$Params=explode(';',awis_BenutzerParameter($con, "SchluesselNummern", $AWISBenutzer->BenutzerName()));
										
						$HilfsSQL="SELECT NVL2(MAX(SLN_SCHLUESSELLFDNR),MAX(SLN_SCHLUESSELLFDNR),0) AS LFDNR FROM SCHLUESSELNUMMERN ";
						$HilfsSQL.="WHERE SLN_FIL_ID=" . $Params[0] . " AND SLN_SLL_KEY=" . $_POST['txtSLN_SLL_KEY'] . " ";
						$HilfsSQL.="AND SLN_SLG_KEY=" . $_POST['txtSLN_SLG_KEY'] . " AND SLN_SLU_KEY=" . $_POST['txtSLN_SLU_KEY'];
						
						
						$rsSLN_LFDNR = awisOpenRecordset($con,$HilfsSQL);
						$rsSLN_LFDNRZeilen = $awisRSZeilen;
						
						$txtSLN_LFDNR=0;
						$txtSLN_LFDNR=$rsSLN_LFDNR['LFDNR'][0];
						$txtSLN_LFDNR=$txtSLN_LFDNR+1;
						
						if($_POST['txtSLN_SLG_KEY']<0)
						{
								awislogoff($con);
								die("<span class=HinweisText>Sie m�ssen eine Schl&uuml;ssel-Gruppe angeben!</span>");
						}
						
						if(empty($_POST['txtSLN_SchluesselNr']))
						{
								awislogoff($con);
								die("<span class=HinweisText>Sie m�ssen eine Schl&uuml;ssel-Nummer angeben!</span>");
						}
						else{
								$SQL='INSERT INTO SCHLUESSELNUMMERN (SLN_SCHLUESSELNUMMER,SLN_SCHLUESSELLFDNR,SLN_SICHERKARTENNUMMER,SLN_FIL_ID,SLN_SLL_KEY,SLN_SLG_KEY,SLN_SLU_KEY,SLN_BEMERKUNG,SLN_USER,SLN_USERDAT,SLN_RECHNUNGSNUMMER)';
								$SQL.='VALUES(\''.$_POST['txtSLN_SchluesselNr'].'\',' . $txtSLN_LFDNR . ' ,\''.$_POST['txtSLN_SicherheitskartenNr'].'\',' . $Params[0] . ','.$_POST['txtSLN_SLL_KEY'].','.$_POST['txtSLN_SLG_KEY'].','.$_POST['txtSLN_SLU_KEY'].',\''.substr($_POST['txtSLN_BEMERKUNG'],0,255).'\',\''.$AWISBenutzer->BenutzerName().'\',SYSDATE,\''.$_POST['txtSLN_RechnungsNr'].'\')';
						}
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Nummern.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
						
						$rsSLN_KEY = awisOpenRecordset($con,'SELECT SEQ_SLN_KEY.CURRVAL AS KEY FROM DUAL');
						
						$txtSLNKEY=$rsSLN_KEY['KEY'][0];
						$anzeigen=true;
				}
		}
}
if((isset($_POST['cmdAnzeigen_x']) && $_POST['cmdAnzeigen_x']==TRUE) || (isset($_GET['cmdAnzeigen_x']) && $_GET['cmdAnzeigen_x']==TRUE) || $anzeigen || (isset($_REQUEST['Anzeigen']) && $_REQUEST['Anzeigen']==TRUE)) // Formular f�r das Hinzuf�gen von Datens�tzen
{
		$Params=explode(';',awis_BenutzerParameter($con, "SchluesselNummern", $AWISBenutzer->BenutzerName()));
		
		echo '<form name=frmNummern method=post action=./schluessel_Main.php?cmdAktion=Nummern>';

		echo '<table>';
		echo '<tr><td width=50><a title=\'Filial-Details anzeigen\' href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$Params[0].'&Zurueck=../schluessel/schluessel_Main.php?cmdAktion=Nummern~~1~~Anzeigen=TRUE>Filiale</a></td>';
		echo '<td><select name=txtFIL_ID>';
		
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ORDER BY FIL_ID");
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8 ORDER BY FIL_ID");
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32 ORDER BY FIL_ID");
		
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('A','B','C') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('D','E','F') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//ORDER BY FIL_ID");
		
		$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='BRD' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='OES' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='CZE' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='NED' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='SUI' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='ITA' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_LAN_WWSKENN='BRD' AND FIL_ID=597
		ORDER BY FIL_ID");
		
		$rsFilialenZeilen = $awisRSZeilen;
		
		echo "<option value=0>::Auswahl::</option>";
		for($FilialenZeile=0;$FilialenZeile<$rsFilialenZeilen;$FilialenZeile++)
		{
				if($Params[0]==$rsFilialen['FIL_ID'][$FilialenZeile])
				{
						echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . " selected='selected'>" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
				}
				else
				{
						echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . ">" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
				}
		}
		echo "</select></td>";
		echo "<td>&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png accesskey=a name=cmdAnzeigen title='Anzeigen (Alt+A)'></td>";
		echo "</tr>";
		echo '</table>';
		
		echo '<hr>';
		
		unset($rsFilialen);
		
		echo '</form>';

		echo '<form name=frmNummern method=post action=./schluessel_Main.php?cmdAktion=Nummern>';
		
		if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
		{
				echo "<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf&uuml;gen (Alt+N)' onclick=location.href='./schluessel_Main.php?cmdAktion=Nummern'>";
				echo '<hr>';
		}
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
		
		$SQL='SELECT SLN_KEY,SLN_SCHLUESSELNUMMER, SLN_SCHLUESSELLFDNR, SLN_SICHERKARTENNUMMER, FIL_BEZ, SLL_BEZEICHNUNG, SLG_BEZEICHNUNG, SLU_BEZEICHNUNG, SLN_BEMERKUNG,SLN_RECHNUNGSNUMMER';
		$SQL.=' FROM SCHLUESSELNUMMERN, FILIALEN, SCHLUESSELLIEFERANTEN, SCHLUESSELGRUPPEN, SCHLUESSELUNTERGRUPPEN';
		$SQL.=' WHERE ((SLN_FIL_ID = FIL_ID(+)) AND (SLN_SLL_KEY = SLL_KEY(+)) AND (SLN_SLG_KEY = SLG_KEY(+)) AND (SLN_SLU_KEY = SLU_KEY(+)) AND SLN_FIL_ID='.$Params[0].')';
		$SQL.=' ORDER BY SLG_BEZEICHNUNG, SLU_BEZEICHNUNG DESC, SLN_SCHLUESSELLFDNR';
		
		$rsSchluesselnummern = awisOpenRecordset($con,$SQL);
		$rsSchluesselnummernZeilen = $awisRSZeilen;
		
		if($rsSchluesselnummernZeilen==0)		// Keine Daten
		{
				echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
				// �berschrift aufbauen
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
				{
					echo "<td id=FeldBez></td>";
				}					
				echo "<td id=FeldBez>Schl&uuml;ssel-Nr</td>";
				echo "<td id=FeldBez>Schl&uuml;ssel-LfdNr</td>";
				echo "<td id=FeldBez>Sicherungskarten-Nr</td>";
				//echo "<td id=FeldBez>Filiale</td>";
				echo "<td id=FeldBez>Lieferant</td>";
				echo "<td id=FeldBez>Gruppe</td>";
				echo "<td id=FeldBez>Untergruppe</td>";
				echo "<td id=FeldBez>Rechnungs-Nr</td>";
				if(($Rechtestufe&8)==8){
						echo "<td id=FeldBez></td>";
				}
				//echo "<td id=FeldBez>Bemerkung</td>";
				echo "</tr>";
				
				for($i=0;$i<$rsSchluesselnummernZeilen;$i++)
				{
					if($i<$AwisBenuPara)
					{
						echo '<tr>';
						
						if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
						{
								echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . "><a href=./schluessel_Main.php?cmdAktion=Nummern&txtSLN_KEY=" . $rsSchluesselnummern['SLN_KEY'][$i] . "><img border=0 src=/bilder/aendern.png name=cmdBearbeiten title='Bearbeiten'></a></td>";
						}
					
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLN_SCHLUESSELNUMMER'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLN_SCHLUESSELLFDNR'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLN_SICHERKARTENNUMMER'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLL_BEZEICHNUNG'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLG_BEZEICHNUNG'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLU_BEZEICHNUNG'][$i] . '</td>';
						echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselnummern['SLN_RECHNUNGSNUMMER'][$i] . '</td>';
						if(($Rechtestufe&8)==8){
								echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./schluessel_Main.php?cmdAktion=Nummern&txtSLN_KEY_loeschen=". $rsSchluesselnummern['SLN_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
						}
						echo '</tr>';
					}
				}
				print "</table>";
				
				if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
				{
					echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
				}
				else
				{
					echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
				}
		}
		echo '</form>';
}
elseif($txtSLNKEY!=0 && ($Rechtestufe&2)==2) // Formular f�r die Bearbeitung der Datens�tze
{
		$SQL='SELECT * FROM SCHLUESSELNUMMERN WHERE SLN_KEY='.$txtSLNKEY;
		
		$rsSchluesselnummern = awisOpenRecordset($con,$SQL);
		$rsSchluesselnummernZeilen = $awisRSZeilen;
		
		if($rsSchluesselnummernZeilen==0)		// Keine Daten
		{
			echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
				echo '<form name=frmNummern method=post action=./schluessel_Main.php?cmdAktion=Nummern>';
				
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez width=20%>Schl&uuml;ssel-Nr</td>";
				echo "<td id=TabellenZeileGrau width=80%><input name=txtSLN_SchluesselNr size=68 value='" . $rsSchluesselnummern['SLN_SCHLUESSELNUMMER'][0] . "'><input type=hidden	name=txtSLN_SchluesselNr_old  value='" . $rsSchluesselnummern['SLN_SCHLUESSELNUMMER'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Sicherungskarten-Nr</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLN_SicherheitskartenNr size=50 value='" . $rsSchluesselnummern['SLN_SICHERKARTENNUMMER'][0] . "'><input type=hidden name=txtSLN_SicherheitskartenNr_old  value='" . $rsSchluesselnummern['SLN_SICHERKARTENNUMMER'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Lieferant</td>";
				echo "<td><select name=txtSLN_SLL_KEY>";
						echo '<option value=-1>Bitte w�hlen...</option>';
						$rsSLN_SLL_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELLIEFERANTEN ORDER BY SLL_BEZEICHNUNG');
						$rsSLN_SLL_KEYZeilen = $awisRSZeilen;
						
						$rsSLNSLL_KEY = awisOpenRecordset($con, 'SELECT SLN_SLL_KEY FROM SCHLUESSELNUMMERN WHERE SLN_KEY='.$txtSLNKEY);
						
						for($SLN_SLL_KEYZeile=0;$SLN_SLL_KEYZeile<$rsSLN_SLL_KEYZeilen;$SLN_SLL_KEYZeile++)
						{
								if($rsSLNSLL_KEY['SLN_SLL_KEY'][0]==$rsSLN_SLL_KEY['SLL_KEY'][$SLN_SLL_KEYZeile])
								{
										echo '<option value=' . $rsSLN_SLL_KEY['SLL_KEY'][$SLN_SLL_KEYZeile] . ' selected="selected">' . $rsSLN_SLL_KEY['SLL_BEZEICHNUNG'][$SLN_SLL_KEYZeile] . '</option>';
								}
								else								
								{
										echo '<option value=' . $rsSLN_SLL_KEY['SLL_KEY'][$SLN_SLL_KEYZeile] . '>' . $rsSLN_SLL_KEY['SLL_BEZEICHNUNG'][$SLN_SLL_KEYZeile] . '</option>';
								}
						}
						echo '</select>';
						echo "<input type=hidden name=txtSLN_SLL_KEY_old value=" . $rsSLNSLL_KEY['SLN_SLL_KEY'][0] . ">";
						unset($rsSLN_SLL_KEY);
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Gruppe</td>";
				echo "<td><select name=txtSLN_SLG_KEY>";
						echo '<option value=-1>Bitte w�hlen...</option>';
						$rsSLN_SLG_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELGRUPPEN ORDER BY SLG_BEZEICHNUNG');
						$rsSLN_SLG_KEYZeilen = $awisRSZeilen;
						
						$rsSLNSLG_KEY = awisOpenRecordset($con, 'SELECT SLN_SLG_KEY FROM SCHLUESSELNUMMERN WHERE SLN_KEY='.$txtSLNKEY);
						
						for($SLN_SLG_KEYZeile=0;$SLN_SLG_KEYZeile<$rsSLN_SLG_KEYZeilen;$SLN_SLG_KEYZeile++)
						{
								if($rsSLNSLG_KEY['SLN_SLG_KEY'][0]==$rsSLN_SLG_KEY['SLG_KEY'][$SLN_SLG_KEYZeile])
								{
										echo '<option value=' . $rsSLN_SLG_KEY['SLG_KEY'][$SLN_SLG_KEYZeile] . ' selected="selected">' . $rsSLN_SLG_KEY['SLG_BEZEICHNUNG'][$SLN_SLG_KEYZeile] . '</option>';
								}
								else
								{
										echo '<option value=' . $rsSLN_SLG_KEY['SLG_KEY'][$SLN_SLG_KEYZeile] . '>' . $rsSLN_SLG_KEY['SLG_BEZEICHNUNG'][$SLN_SLG_KEYZeile] . '</option>';
								}
						}
						echo '</select>';
						echo "<input type=hidden name=txtSLN_SLG_KEY_old value=" . $rsSLNSLG_KEY['SLN_SLG_KEY'][0] . ">";
						unset($rsSLN_SLG_KEY);
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Unterguppe</td>";
				echo "<td><select name=txtSLN_SLU_KEY>";
						echo '<option value=0>Bitte w�hlen...</option>';
						$rsSLN_SLU_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELGRUPPEN INNER JOIN SCHLUESSELUNTERGRUPPEN ON SLG_KEY = SLU_SLG_KEY ORDER BY SLG_BEZEICHNUNG, SLU_BEZEICHNUNG');
						$rsSLN_SLU_KEYZeilen = $awisRSZeilen;
						
						$rsSLNSLU_KEY = awisOpenRecordset($con, 'SELECT SLN_SLU_KEY FROM SCHLUESSELNUMMERN WHERE SLN_KEY='.$txtSLNKEY);
						
						$LetzteGrp = '';
						for($SLN_SLU_KEYZeile=0;$SLN_SLU_KEYZeile<$rsSLN_SLU_KEYZeilen;$SLN_SLU_KEYZeile++)
						{
							if($LetzteGrp!=$rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile])
							{
								if($LetzteGrp=='')
								{
									echo '</optgroup>';
								}
								echo "<optgroup label='" . $rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile]. "'>";
								$LetzteGrp=$rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile];
							}
								if($rsSLNSLU_KEY['SLN_SLU_KEY'][0]==$rsSLN_SLU_KEY['SLU_KEY'][$SLN_SLU_KEYZeile])
								{
										echo '<option value=' . $rsSLN_SLU_KEY['SLU_KEY'][$SLN_SLU_KEYZeile] . ' selected="selected">' . $rsSLN_SLU_KEY['SLU_BEZEICHNUNG'][$SLN_SLU_KEYZeile] . '</option>';
								}
								else
								{
										echo '<option value=' . $rsSLN_SLU_KEY['SLU_KEY'][$SLN_SLU_KEYZeile] . '>' . $rsSLN_SLU_KEY['SLU_BEZEICHNUNG'][$SLN_SLU_KEYZeile] . '</option>';
								}
						}
						echo '</optgroup>';
						echo '</select>';
						echo "<input type=hidden name=txtSLN_SLU_KEY_old value=" . $rsSLNSLU_KEY['SLN_SLU_KEY'][0] . ">";
						unset($rsSLN_SLU_KEY);
				echo '</tr>';
				echo "<tr>";
				echo "<td id=FeldBez>Rechnungs-Nr</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLN_RechnungsNr size=50 value='" . $rsSchluesselnummern['SLN_RECHNUNGSNUMMER'][0] . "'><input type=hidden name=txtSLN_RechnungsNr_old  value='" . $rsSchluesselnummern['SLN_RECHNUNGSNUMMER'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez valign=top>Bemerkung</td>";
				echo "<td id=TabellenZeileGrau><textarea name=txtSLN_BEMERKUNG cols=50 rows=5>" . $rsSchluesselnummern['SLN_BEMERKUNG'][0] . "</textarea></td><input type=hidden name=txtSLN_BEMERKUNG_old  value='" . $rsSchluesselnummern['SLN_BEMERKUNG'][0] . "'></td>";
				echo "</tr>";
				echo "</table>";
				
				echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
				echo "<input type=hidden name=txtSLN_KEY value=".$rsSchluesselnummern['SLN_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
				echo "</form>";
		
		}
}
elseif((isset($_POST['cmdHinzufuegen_x']) && $_POST['cmdHinzufuegen_x'])  && ($Rechtestufe&4)==4) // Formular f�r das Hinzuf�gen von Datens�tzen
{
		echo '<form name=frmNummern method=post action=./schluessel_Main.php?cmdAktion=Nummern>';
		
		echo "<table  width=100% id=DatenTabelle border=1><tr>";
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Anzahl</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLN_Anzahl size=10 value=1></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Schl&uuml;ssel-Nr</td>";
		echo "<td id=TabellenZeileGrau width=80%><input name=txtSLN_SchluesselNr size=68></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Sicherungskarten-Nr</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLN_SicherheitskartenNr size=50></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Lieferant</td>";
		echo "<td><select name=txtSLN_SLL_KEY>";
				echo '<option value=-1>Bitte w�hlen...</option>';
				$rsSLN_SLL_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELLIEFERANTEN ORDER BY SLL_BEZEICHNUNG');
				$rsSLN_SLL_KEYZeilen = $awisRSZeilen;
				for($SLN_SLL_KEYZeile=0;$SLN_SLL_KEYZeile<$rsSLN_SLL_KEYZeilen;$SLN_SLL_KEYZeile++)
				{
						echo '<option value=' . $rsSLN_SLL_KEY['SLL_KEY'][$SLN_SLL_KEYZeile] . '>' . $rsSLN_SLL_KEY['SLL_BEZEICHNUNG'][$SLN_SLL_KEYZeile] . '</option>';
				}
				echo '</select>';
				unset($rsSLN_SLL_KEY);
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Gruppe</td>";
		echo "<td><select name=txtSLN_SLG_KEY>";
				echo '<option value=-1>Bitte w�hlen...</option>';
				$rsSLN_SLG_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELGRUPPEN ORDER BY SLG_BEZEICHNUNG');
				$rsSLN_SLG_KEYZeilen = $awisRSZeilen;
				for($SLN_SLG_KEYZeile=0;$SLN_SLG_KEYZeile<$rsSLN_SLG_KEYZeilen;$SLN_SLG_KEYZeile++)
				{
						echo '<option value=' . $rsSLN_SLG_KEY['SLG_KEY'][$SLN_SLG_KEYZeile] . '>' . $rsSLN_SLG_KEY['SLG_BEZEICHNUNG'][$SLN_SLG_KEYZeile] . '</option>';
				}
				echo '</select>';
				unset($rsSLN_SLG_KEY);
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Unterguppe</td>";
		echo "<td><select name=txtSLN_SLU_KEY>";
				echo '<option value=0>Bitte w�hlen...</option>';
				$rsSLN_SLU_KEY = awisOpenRecordset($con, 'SELECT * FROM SCHLUESSELGRUPPEN INNER JOIN SCHLUESSELUNTERGRUPPEN ON SLG_KEY = SLU_SLG_KEY ORDER BY SLG_BEZEICHNUNG, SLU_BEZEICHNUNG');
				$rsSLN_SLU_KEYZeilen = $awisRSZeilen;
				$LetzteGrp = '';
				for($SLN_SLU_KEYZeile=0;$SLN_SLU_KEYZeile<$rsSLN_SLU_KEYZeilen;$SLN_SLU_KEYZeile++)
				{
					if($LetzteGrp!=$rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile])
					{
						if($LetzteGrp=='')
						{
							echo '</optgroup>';
						}
						echo "<optgroup label='" . $rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile]. "'>";
						$LetzteGrp=$rsSLN_SLU_KEY['SLG_BEZEICHNUNG'][$SLN_SLU_KEYZeile];
					}
					echo '<option value=' . $rsSLN_SLU_KEY['SLU_KEY'][$SLN_SLU_KEYZeile] . '>' . $rsSLN_SLU_KEY['SLU_BEZEICHNUNG'][$SLN_SLU_KEYZeile] . '</option>';
				}
				echo '</optgroup>';
				echo '</select>';
				unset($rsSLN_SLU_KEY);
		echo '</tr>';
		echo "<tr>";
		echo "<td id=FeldBez>Rechnungs-Nr</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLN_RechnungsNr size=50></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez valign=top>Bemerkung</td>";
		echo "<td id=TabellenZeileGrau><textarea name=txtSLN_BEMERKUNG cols=50 rows=5></textarea></td>";
		echo "</tr>";
		echo "</table>";
					
		echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		echo "<input type=hidden name=txtSLN_KEY value=0>";
		echo "</form>";
}
else
{
		echo '<form name=frmNummern method=post action=./schluessel_Main.php?cmdAktion=Nummern>';

		echo '<table>';
		echo '<tr><td width=50>Filiale</td>';
		echo '<td><select name=txtFIL_ID>';
		
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ORDER BY FIL_ID");
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8 ORDER BY FIL_ID");
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32 ORDER BY FIL_ID");
		
		//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('A','B','C') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//union
		//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('D','E','F') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		//ORDER BY FIL_ID");
		
		$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='BRD' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='OES' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='CZE' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='NED' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='SUI' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='ITA' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
		union
		SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_LAN_WWSKENN='BRD' AND FIL_ID=597
		ORDER BY FIL_ID");
		
		$rsFilialenZeilen = $awisRSZeilen;
		
		echo "<option value=0>::Auswahl::</option>";
		for($FilialenZeile=0;$FilialenZeile<$rsFilialenZeilen;$FilialenZeile++)
		{
			echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . ">" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
		}
		echo "</select></td>";
		echo "<td>&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png accesskey=a name=cmdAnzeigen title='Anzeigen (Alt+A)'></td>";
		echo "</tr>";
		echo '</table>';
		echo '<hr>';
		
		unset($rsFilialen);
		
		echo '</form>';
}

?>
</body>
</html>
