<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisFormular.inc';
require_once 'awisSeminarplanEinladungsemail.php';

/**
 * Created by PhpStorm.
 * User: patrickgebhardt
 * Date: 10.07.17
 * Time: 17:10
 */
class awisSeminarplanEmailJob
{
    /**
     * Aktueller SET_KEY
     *
     * @var int
     */
    protected $_SET_KEY;

    /**
     * Aktuelle FIlialnummer des Teilnehmers
     *
     * @var string
     */
    protected $_FIL_ID;

    /**
     * Betreff der Email
     *
     * @var string
     */
    protected $_Betreff;

    /**
     * Text der Email
     *
     * @var string
     */
    protected $_Text;

    /**
     * Debuglevel des Jobs
     *
     * @var string
     */
    private $_DebugLevel;

    /**
     * @var awisRecordset
     */
    protected $_rsSET;

    /**
     * @var awisDatenbank
     */
    protected $_DB;


    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisFormular
     */
    protected $_Form;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;


    /**
     * awisSeminarplanEmailJob constructor.
     * @param $Benutzer
     */
    function __construct($Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_Form = new awisFormular();
        $this->_Werkzeuge = new awisWerkzeuge();
    }


    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level >= $this->DebugLevel()) {
            echo date('d.m.Y');
            echo ': ' . $Typ;
            echo ': ' . $Text;
            echo PHP_EOL;
        }
    }


    /**
     * Versendet alle ausstehenden Einladungen f�r Schulungsart 1
     */
    public function versendeFilialeinladungen()
    {
        //Alle Seminare holen, bei dene ich einen Einladung verschicken muss
        $SQL = 'select';
        $SQL .= '     LPAD(SET_FIL_NR,4,\'0\') as SET_FIL_NR,';
        $SQL .= '     SEP_KEY,';
        $SQL .= '     SEP_SEMINAR,';
        $SQL .= '     SEP_BESCHREIBUNG,';
        $SQL .= '     SEP_TRAINING,';
        $SQL .= '     SET_KEY,';
        $SQL .= '     SET_NACHNAME,';
        $SQL .= '     SET_VORNAME,';
        $SQL .= '     SEP_SEMINARSTATUS,';
        $SQL .= '     SET_STATUSEMAIL,';
        $SQL .= '     SET_TEILNAHME,';
        $SQL .= '     SEP_VON,';
        $SQL .= '     SEP_BIS, ';
        $SQL .= '     SET_PER_NR,';
        $SQL .= '     SET_STATUS ';
        $SQL .= '   from';
        $SQL .= '     AWIS.SEMINARPLAN';
        $SQL .= '   inner join SEMINARTEILNEHMER';
        $SQL .= '   on';
        $SQL .= '     SEP_KEY = SET_SEP_KEY';
        $SQL .= '   where ';
        $SQL .= '     SEP_SCHULUNGSART           = 1';
        $SQL .= '   and upper(SEP_SEMINARSTATUS) = upper(\'Anmeldephase\')';
        $SQL .= '   and upper(SET_STATUS) = upper(\'Gebucht\')';
        $SQL .= '   and SET_TEILNAHME = -1 ';
        $SQL .= '   and (SET_STATUSEMAIL = 0 or SET_STATUSEMAIL is null)';
        $SQL .= '   and SET_STATUSTEILNEHMER = 1';
        $rsZuVersenden = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsZuVersenden->EOF()) {

            $Einladung = new awisSeminarplanEinladungsemail($rsZuVersenden->FeldInhalt('SET_KEY'));
            $Einladung->Senden();
            $Einladung->ProtokolliereEmailversand(awisSeminarplanEinladungsemail::EMAIL_STATUS_JOB);

            $rsZuVersenden->DSWeiter();
        }

    }


}