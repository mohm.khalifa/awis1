<?php
require_once('awisWerkstattauftraege.php');
try 
{
	echo 'INIT.'.PHP_EOL;
	$WA = new awisWerkstattAuftraege();	
	$WA->ImportiereWAAuftrage();
	$WA->BestellungenZuordnen();
}
catch (Exception $ex)
{
	echo 'FEHLER: '.$ex->getMessage();
}
?>
