<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisFormular.inc';
require_once 'awisOT.inc';

class awis_ticket_funktionen
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * AWIS Werkzeugobjekt
	 *
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeug = null;

	/**
	 * AWIS Benutzer
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Onmitracker - Objekt
	 * @var awisOT
	 */
	private $_OTObj = null;

	public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
	{
	    $this->_DB = $DB;
	    $this->_AWISBenutzer = $AWISBenutzer;
	    $this->_AWISWerkzeug = new awisWerkzeuge();

        $this->_OTObj = new awisOT($AWISBenutzer);
	}

    public function Funktion($FunktionsName, $ParameterListe)
    {
        switch($FunktionsName)
        {
            case 'USER':
                return $this->_AWISBenutzer->BenutzerName();
            case 'PERSNR':
                return $this->_AWISBenutzer->PersonalNummer();
            case 'KOSTENSTELLE':
                return $this->_AWISBenutzer->Kostenstelle();
                // OmniTracker Funktionen
            case 'OTPERSONEN':
                return $this->_OTObj->ObjektListe('Stammdaten\Personen','Mitarbeiter-Zentrale',array('Nachname','Vorname'),9);
            case 'KATEGORIEN':
                return $this->_OTObj->ObjektListe('Category','Aktive Kategorien',array('Title'),50);
        }
    }
}