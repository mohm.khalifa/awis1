<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $con;
global $awisRSZeilen;
global $AWISBenutzer;
					
	if(awisBenutzerRecht($con,100)==0)
	{
	     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
	     die("Keine ausreichenden Rechte!");
	}
	// Lokale Variablen
	$Bedingung = '';		// Bedingung f�r SQL
	$SQL = '';				// SQL Abfragestring

	 // Daten f�r die Infoseite sammeln
	 $SQL = "SELECT filialen.*
				FROM filialinfos, filialinfostypen, filialinfostypengruppen, filialen
				 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
				 	AND FILIALEN.FIL_ID = filialinfos.FIF_FIL_ID
        			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
					AND (FIF_FIT_ID=104 AND TO_DATE(FIF_WERT,'dd.mm.yy') >= SYSDATE-1))
				ORDER BY FIL_Bez";

	$rsFiliale = awisOpenRecordset($con, $SQL);
	$rsFilialeAnz = $awisRSZeilen;

	If($rsFilialeAnz>=1)		// Liste gefunden
	{
		print "<table border=0 width=100%>";
		for($i=0;$i<$rsFilialeAnz;$i++)
		{
			print "<tr><td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
			print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
//			print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>". $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td>";

			$SQL = "SELECT filialinfos.fif_wert
				FROM filialinfos, importquellen
				 WHERE (importquellen.IMQ_ID = filialinfos.FIF_IMQ_ID
        			AND FIF_FIT_ID in (101, 102, 103, 104)
					AND FIF_FIL_ID=0" . $rsFiliale["FIL_ID"][$i] . ")
				ORDER BY IMQ_PRIORITAET, FIF_FIT_ID";
			$rsFilialInfo = awisOpenRecordset($con, $SQL);	 
 		    if($rsFilialInfo["FIF_WERT"][0]!="")
			{
				print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialInfo["FIF_WERT"][0] . "</td><td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">". $rsFilialInfo["FIF_WERT"][1] . " " . $rsFilialInfo["FIF_WERT"][2] . "</td>";
				print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialInfo["FIF_WERT"][3] . "</td>";
			}
			else
			{
				print "<td>kein Datum.</td>";
			}
			print "</tr>";
		}
		print "</table>";
	}
	else
	{
		print "<span class=HinweisText>Es stehen keine Umz�ge an</span>";
	}

/************************************************************************************
* 
* Hilfsfunktionen
* 
* **********************************************************************************/
Function SucheFilialInfo($rsFiliale, $InfoID, $Spalte)
{
	//for($i=0;$i<count($rsFiliaInfo);$i++)
	$i=0;
	while($rsFiliale[$Spalte][$i]!="")
	{
		if($rsFiliale[$Spalte][$i]==$InfoID)
		{
			return $i;
		}
		$i++;
//	print "/". $rsFiliale[$Spalte][$i];
	}
	return -1;
}

?>