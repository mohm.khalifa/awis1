<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $GUT;

$DetailAnsicht = false;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['GUT_KEY'])) {
    $AWIS_KEY1 = $GUT->DB->FeldInhaltFormat('N0', $_GET['GUT_KEY']);
} elseif (isset($_POST['txtGUT_KEY'])) {
    $AWIS_KEY1 = $GUT->DB->FeldInhaltFormat('N0', $_POST['txtGUT_KEY']);
}

if (isset($_POST['cmdDSNeu_x'])) {
    $AWIS_KEY1 = -1;
    $_POST = array();
}
$GUT->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
$POSTVerwenden = true;
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $POSTVerwenden = false;
    $GUT->Param = array();
    $GUT->Param['GUT_VORGANGSNUMMER'] = $_POST['sucGUT_VORGANGSNUMMER'];
    $GUT->Param['GUT_GUTSCHEINNR'] = $_POST['sucGUT_GUTSCHEINNR'];
    $GUT->Param['GUT_VORNAME'] = $_POST['sucGUT_VORNAME'];
    $GUT->Param['GUT_NACHNAME'] = $_POST['sucGUT_NACHNAME'];
    $GUT->Param['GUT_NACHNAME'] = $_POST['sucGUT_NACHNAME'];
    $GUT->Param['GUT_FIL_ID'] = $_POST['sucGUT_FIL_ID'];
    $GUT->Param['GUT_STATUS'] = $_POST['sucGUT_STATUS'];

    $GUT->Param['KEY'] = '';
    $GUT->Param['WHERE'] = '';
    $GUT->Param['ORDER'] = '';
    $GUT->Param['BLOCK'] = 1;
    $GUT->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./gutscheinaktion_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./gutscheinaktion_speichern.php');
}elseif (isset($_POST['cmdFormulareingang_x'])){
    include('./gutscheinaktion_formulareingang.php');
}elseif (isset($_POST['cmdZahlungseingang_x'])){
    include('./gutscheinaktion_zahlungseingang.php');
}
else
{ //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $GUT->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($GUT->Param['ORDER']) AND $GUT->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $GUT->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY GUT_VORGANGSNUMMER DESC';
        $GUT->Param['ORDER'] = ' GUT_VORGANGSNUMMER DESC ';
    }
} else {
    $GUT->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $GUT->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $GUT->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL  ='SELECT GUT_KEY,';
$SQL .='   GUT_VORGANGSNUMMER,';
$SQL .='   GUT_BETRAG,';
$SQL .='   GUT_NACHNAME,';
$SQL .='   GUT_VORNAME,';
$SQL .='   GUT_STRASSE,';
$SQL .='   GUT_KREDITINSITUT,';
$SQL .='   GUT_PLZ,';
$SQL .='   GUT_BIC,';
$SQL .='   GUT_ORT,';
$SQL .='   GUT_IBAN,';
$SQL .='   GUT_USER,';
$SQL .='   GUT_USERDAT,';
$SQL .='   GUT_STATUS,';
$SQL .='   GUT_GUTSCHEINNR,';
$SQL .='   GUT_IBAN_LAND,';
$SQL .='   GUT_FIL_ID';
$SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .=' FROM GUTSCHEINAKTION ';


if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $GUT->Form->Format('N0', $_REQUEST['Block'], false);
        $GUT->Param['BLOCK'] = $Block;
    } elseif (isset($GUT->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $GUT->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $GUT->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $GUT->DB->ErmittleZeilenAnzahl($SQL, $GUT->DB->Bindevariablen('GUT', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $GUT->DB->WertSetzen('GUT', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $GUT->DB->WertSetzen('GUT', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsGUT = $GUT->DB->RecordsetOeffnen($SQL, $GUT->DB->Bindevariablen('GUT', true));
$GUT->Form->DebugAusgabe(1, $GUT->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$GUT->Form->Formular_Start();
echo '<form name=frmgutscheinaktion action=./gutscheinaktion_Main.php?cmdAktion=Details method=POST>';

if ($rsGUT->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $GUT->Form->Hinweistext($GUT->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsGUT->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['GUT_FIL_ID'] = 50;
    $FeldBreiten['GUT_VORGANGSNUMMER'] = 150;
    $FeldBreiten['GUT_GUTSCHEINNR'] = 160;
    $FeldBreiten['GUT_BETRAG'] = 100;
    $FeldBreiten['GUT_NACHNAME'] = 130;
    $FeldBreiten['GUT_VORNAME'] = 130;
    $FeldBreiten['GUT_STATUS'] = 30;

    $GUT->Form->ZeileStart();


    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_GUTSCHEINNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_GUTSCHEINNR'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_GUTSCHEINNR'], $FeldBreiten['GUT_GUTSCHEINNR'], '', $Link);

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_FIL_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_FIL_ID'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_FIL'], $FeldBreiten['GUT_FIL_ID'], '', $Link);

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_VORGANGSNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_VORGANGSNUMMER'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_VORGANGSNUMMER'], $FeldBreiten['GUT_VORGANGSNUMMER'], '', $Link);


    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_BETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_BETRAG'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_BETRAG'], $FeldBreiten['GUT_BETRAG'], '', $Link);

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_NACHNAME'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_NACHNAME'], $FeldBreiten['GUT_NACHNAME'], '', $Link);

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_VORNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_VORNAME'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_VORNAME'], $FeldBreiten['GUT_VORNAME'], '', $Link);

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=GUT_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GUT_STATUS'))?'~':'');
    $GUT->Form->Erstelle_Liste_Ueberschrift($GUT->AWISSprachKonserven['GUT']['GUT_S'], $FeldBreiten['GUT_STATUS'], '', $Link,$GUT->AWISSprachKonserven['GUT']['GUT_STATUS'],'L');


    $GUT->Form->ZeileEnde();

    $DS = 0;
    while (!$rsGUT->EOF()) {
        $HG = ($DS % 2);
        $GUT->Form->ZeileStart();
        $Link = './gutscheinaktion_Main.php?cmdAktion=Details&GUT_KEY=' . $rsGUT->FeldInhalt('GUT_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $GUT->Form->Erstelle_ListenFeld('GUT_GUTSCHEINNR', $rsGUT->FeldInhalt('GUT_GUTSCHEINNR'), 0, $FeldBreiten['GUT_GUTSCHEINNR'], false, $HG, '', $Link, 'T', 'L');
        $GUT->Form->Erstelle_ListenFeld('GUT_FIL_ID', $rsGUT->FeldInhalt('GUT_FIL_ID'), 0, $FeldBreiten['GUT_FIL_ID'], false, $HG, '', '', 'T');
        $GUT->Form->Erstelle_ListenFeld('GUT_VORGANGSNUMMER', $rsGUT->FeldInhalt('GUT_VORGANGSNUMMER'), 0, $FeldBreiten['GUT_VORGANGSNUMMER'], false, $HG, '', '', 'T', 'L');
        $GUT->Form->Erstelle_ListenFeld('GUT_BETRAG', $rsGUT->FeldInhalt('GUT_BETRAG'), 0, $FeldBreiten['GUT_BETRAG'], false, $HG, '', '', 'T', 'L');
        $GUT->Form->Erstelle_ListenFeld('GUT_NACHNAME', $rsGUT->FeldInhalt('GUT_NACHNAME'), 0, $FeldBreiten['GUT_NACHNAME'], false, $HG, '', '', 'T', 'L');
        $GUT->Form->Erstelle_ListenFeld('GUT_VORNAME', $rsGUT->FeldInhalt('GUT_VORNAME'), 0, $FeldBreiten['GUT_VORNAME'], false, $HG, '', '', 'T', 'L');


        $Icons = array();
        if($rsGUT->FeldInhalt('GUT_STATUS')<=2){
            $Icons = array("edit", '', "", $GUT->AWISSprachKonserven['GUT']['GUT_ERFASST']);
        }elseif($rsGUT->FeldInhalt('GUT_STATUS')==10){
            $Icons = array("geldkasten_rot", '', "", $GUT->AWISSprachKonserven['GUT']['GUT_FORMULAR_EINGEGANGEN']);
        }elseif($rsGUT->FeldInhalt('GUT_STATUS')==1000){
            $Icons = array("geldkasten_gruen", '', "", $GUT->AWISSprachKonserven['GUT']['GUT_ZAHLUNG_EINGEGANGEN']);
        }
        $GUT->Form->Erstelle_ListeIcons(array($Icons), $FeldBreiten['GUT_STATUS'], $HG, '', 'align: center');

        $GUT->Form->ZeileEnde();

        $rsGUT->DSWeiter();
        $DS++;
    }

    $Link = './gutscheinaktion_Main.php?cmdAktion=Details';
    $GUT->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    $DetailAnsicht = true;

    if($AWIS_KEY1!=-1){
        $AWIS_KEY1 = $rsGUT->FeldInhalt('GUT_KEY');
    }

    $GUT->Form->Erstelle_HiddenFeld('GUT_KEY', $AWIS_KEY1);
    $GUT->Param['KEY'] = $AWIS_KEY1;
    $GUT->EditSperren();

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./gutscheinaktion_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $GUT->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsGUT->FeldInhalt('GUT_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsGUT->FeldInhalt('GUT_USERDAT'));
    $GUT->Form->InfoZeile($Felder, '');



    $GUT->Form->FormularBereichStart();
    $GUT->Form->FormularBereichInhaltStart($GUT->AWISSprachKonserven['GUT']['GUT_GUTSCHEIN'],true);
    $Vorgangsnr = '400';
    if($GUT->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING )!= ''){
        $Filialen = $GUT->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_ARRAY);
        $Daten = array();
        foreach ($Filialen as $Fil){
            $Daten[] = $Fil . '~' .$Fil;
        }
        //Wenn nur eine Filiale gefunden wird, braucht mans nicht anzeigen
        if(count($Filialen)==1){
            $Style='display:none';
        }else{
            $Style ='';
        }
        $GUT->Form->ZeileStart($Style);
        $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_FIL_ID'].':',170);
        $GUT->Form->Erstelle_SelectFeld('!GUT_FIL_ID', $rsGUT->FeldOderPOST('GUT_FIL_ID','',$POSTVerwenden),'105:110', $GUT->EditRecht,'','','','','',$Daten);
        $GUT->Form->ZeileEnde();
        $Vorgangsnr .= str_pad(intval($Filialen[0]),3,'0',STR_PAD_LEFT) ;
    }else{
        $GUT->Form->ZeileStart();
        $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_FIL_ID'].':',170);
        $GUT->Form->Erstelle_TextFeld('!GUT_FIL_ID', $rsGUT->FeldOderPOST('GUT_FIL_ID','',$POSTVerwenden),16,150, $GUT->EditRecht,'','','','T','L','','',4);
        $GUT->Form->ZeileEnde();

    }

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_GUTSCHEINNR'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_GUTSCHEINNR', $rsGUT->FeldOderPOST('GUT_GUTSCHEINNR','',$POSTVerwenden), 16, 200, $GUT->EditRecht, '', '', '', 'T', '', $GUT->AWISSprachKonserven['GUT']['GUT_TTT_GUTSCHEINNR'],'',16,'','','pattern="[0-9]{16}"');
    $GUT->Form->ZeileEnde();

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_VORGANGSNUMMER'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_VORGANGSNUMMER', $rsGUT->FeldOderPOST('GUT_VORGANGSNUMMER','',$POSTVerwenden)!=''?$rsGUT->FeldOderPOST('GUT_VORGANGSNUMMER','',$POSTVerwenden):$Vorgangsnr, 16, 150,  $GUT->EditRecht, '', '', '','T', '', $GUT->AWISSprachKonserven['GUT']['GUT_TTT_VORGANGSNUMMER'],'',13,'','','pattern="[0-9]{13}"');
    $GUT->Form->ZeileEnde();

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_BETRAG'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_BETRAG', $rsGUT->FeldOderPOST('GUT_BETRAG','',$POSTVerwenden),16, 150,  $GUT->EditRecht, '', '', '', 'T','L',$GUT->AWISSprachKonserven['GUT']['GUT_TTT_BETRAG'],'','','','','pattern="[0-9]+(\\,[0-9][0-9]?)?"');
    $GUT->Form->ZeileEnde();


    $GUT->Form->FormularBereichInhaltEnde();
    $GUT->Form->FormularBereichEnde();


    $GUT->Form->FormularBereichStart();
    $GUT->Form->FormularBereichInhaltStart($GUT->AWISSprachKonserven['GUT']['GUT_KUNDENDATEN'],true);
    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_VORNAME'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_VORNAME', $rsGUT->FeldOderPOST('GUT_VORNAME','',$POSTVerwenden),12, 150,  $GUT->EditRecht, '', '', '', 'T');
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_NACHNAME'] . ':', 97);
    $GUT->Form->Erstelle_TextFeld('!GUT_NACHNAME', $rsGUT->FeldOderPOST('GUT_NACHNAME','',$POSTVerwenden),22, 220,  $GUT->EditRecht, '', '', '', 'T');
    $GUT->Form->ZeileEnde();


    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_STRASSE'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_STRASSE', $rsGUT->FeldOderPOST('GUT_STRASSE','',$POSTVerwenden),62, 435,  $GUT->EditRecht, '', '', '', 'T');
    $GUT->Form->ZeileEnde();

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_PLZ'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_PLZ', $rsGUT->FeldOderPOST('GUT_PLZ','',$POSTVerwenden),6, 150,  $GUT->EditRecht, '', '', '', 'T');
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_ORT'] . ':', 97);
    $GUT->Form->Erstelle_TextFeld('!GUT_ORT', $rsGUT->FeldOderPOST('GUT_ORT','',$POSTVerwenden),22, 220,  $GUT->EditRecht, '', '', '', 'T');
    $GUT->Form->ZeileEnde();

    echo '<script>'.PHP_EOL;
    echo '$(document).ready(function(){ '.PHP_EOL;
    echo ' key_GUT_IBAN(event); '.PHP_EOL;
    echo '});'.PHP_EOL;
    echo '</script>'.PHP_EOL;

    if(isset($_POST['txtBLZ_BEZEICHNUNG'])){ //Wurde gepostet? In Hiddenfelder schreiben, um die Werte in die Auswahlbox zu geben
        $GUT->Form->Erstelle_HiddenFeld('hiddenBLZ_BEZEICHNUNG',$_POST['txtBLZ_BEZEICHNUNG']);
        $GUT->Form->Erstelle_HiddenFeld('hiddenBLZ_BIC',$_POST['txtBLZ_BIC']);
    }else{ //Nicht gepostet. Dummyfelder schreiben
        $GUT->Form->Erstelle_HiddenFeld('hiddenBLZ_BEZEICHNUNG',$rsGUT->FeldOderPOST('GUT_KREDITINSITUT','',$POSTVerwenden));
        $GUT->Form->Erstelle_HiddenFeld('hiddenBLZ_BIC',$rsGUT->FeldOderPOST('GUT_BIC','',$POSTVerwenden));
    }

    if($GUT->EditRecht===false){
        $GUT->erstelleIBANInfos($rsGUT->FeldInhalt('GUT_IBAN'),$rsGUT->FeldInhalt('GUT_IBAN_LAND'),$AWIS_KEY1);
    }
    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_IBAN'] . ':', 170);
    $GUT->Form->Erstelle_TextFeld('!GUT_IBAN_LAND', isset($_POST['txtGUT_IBAN_LAND'])?$_POST['txtGUT_IBAN_LAND']:'DE',2, 30,  $GUT->EditRecht, '', '', '', 'T','L',$GUT->AWISSprachKonserven['GUT']['GUT_TTT_IBAN_LAN'],'',2,'onFocusOut="key_GUT_IBAN(event);"');
    $GUT->Form->AuswahlBox('!GUT_IBAN', 'box1', '', 'GUT_IBAN', 'txtGUT_IBAN_LAND,txthiddenBLZ_BIC,txthiddenBLZ_BEZEICHNUNG,txtGUT_KEY', 405, 57, $rsGUT->FeldOderPOST('GUT_IBAN','',$POSTVerwenden), 'T',  $GUT->EditRecht, '', '', '', '', '', $GUT->AWISSprachKonserven['GUT']['GUT_TTT_IBAN'], '', 20,'','pattern="[0-9]{20}{34}"');
    $GUT->Form->ZeileEnde();

    $GUT->Form->AuswahlBoxHuelle('box1');


    $GUT->Form->FormularBereichInhaltEnde();
    $GUT->Form->FormularBereichEnde();


}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$GUT->Form->Formular_Ende();
$GUT->Form->SchaltflaechenStart();

$GUT->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $GUT->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if ($GUT->EditRecht AND $DetailAnsicht) {
    $GUT->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $GUT->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}elseif (($GUT->Recht55000 & 2) == 2) {       // Hinzuf�gen erlaubt?
    $GUT->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $GUT->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

if (($GUT->Recht55000 & 64) == 64 AND $AWIS_KEY1 > 0) { //Formulareingang
    $GUT->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $GUT->AWISSprachKonserven['Wort']['lbl_drucken'], '');
}


if (($GUT->Recht55000 & 16) == 16 AND $AWIS_KEY1 > 0 and $rsGUT->FeldInhalt('GUT_STATUS') <= 4) { //Formulareingang
    $GUT->Form->Schaltflaeche('image', 'cmdFormulareingang', '', '/bilder/cmd_Formular_OK.png', $GUT->AWISSprachKonserven['GUT']['GUT_TTT_FORM_EINGANG'], '');
}

if (($GUT->Recht55000 & 32) == 32 AND $AWIS_KEY1 > 0 and ($rsGUT->FeldInhalt('GUT_STATUS') >= 10 and $rsGUT->FeldInhalt('GUT_STATUS') <1000  ) ) { //Zahlungseingang
    $GUT->Form->Schaltflaeche('image', 'cmdZahlungseingang', '', '/bilder/cmd_euro.png', $GUT->AWISSprachKonserven['GUT']['GUT_TTT_ZAHL_EINGANG'], '');
}

if (($GUT->Recht55000 & 8) == 8 AND $AWIS_KEY1 > 0) {
    $GUT->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $GUT->AWISSprachKonserven['Wort']['lbl_loeschen'], '');
}



$GUT->Form->SchaltflaechenEnde();

$GUT->Form->SchreibeHTMLCode('</form>');

?>