<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 11.04.2017
 * Time: 14:47
 */

require_once '/daten/soap/goehrum/BestellArchiv/goehrum_autoload.php';

$Service = new BestellArchiv(array(), 'http://webdienste01.goehrum.de/BestellArchiv.asmx?wsdl');

$Datum = new DateTime('25.04.2017');
$AuftraegeProTag = new AuftraegeProTag('40665','r50T35AM',$Datum);

$AuftraegeProTagResponse = $Service->AuftraegeProTag($AuftraegeProTag);

$AuftraegeProTagResult = $AuftraegeProTagResponse->getAuftraegeProTagResult();

$Auftraege = $AuftraegeProTagResponse->getAuftraegeProTagResult();

$Auftragsarray = $Auftraege->getAuftraege();
if($Auftragsarray->count() == 0){
    echo 'Keine auftr�ge';
    die;
}
foreach ($Auftragsarray as $A){
    echo $A->getVerarbeitungsnummer() . '<br>';
    $Positionsarray = $A->getPositionen()->getBestaetigungPosition();
    foreach ($Positionsarray as $Pos){
        echo $Pos->getEinspeiserNr() . '<br>';
    }
}
