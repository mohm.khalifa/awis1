<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtKEW_KEY']))
		{
			$Tabelle = 'KEW';
			$Key=$_POST['txtKEW_KEY'];

			$SQL = 'SELECT KEW_KEY, KEW_BEZEICHNUNG';
			$SQL .= ' FROM KENNWOERTER ';
			$SQL .= ' WHERE KEW_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$KEWKey = $rsDaten->FeldInhalt('KEW_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('KEW','KEW_BEZEICHNUNG'),$rsDaten->FeldInhalt('KEW_BEZEICHNUNG'));
		}
		elseif(isset($_POST['txtKZG_KEY']))		// Zugriffsgruupe AP
		{
			$Tabelle = 'KZG';
			$Key=$_POST['txtKZG_KEY'];

			$SQL = 'SELECT *';
			$SQL .= ' FROM KENNWOERTERGRUPPEN ';
			$SQL .= ' WHERE KZG_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$KEWKey = $rsDaten->FeldInhalt('KEW_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('KZG','KZG_BEZEICHNUNG'),$rsDaten->FeldInhalt('KZG_BEZEICHNUNG'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Mitglieder':
					$Tabelle = 'KZM';
					$Key=$_GET['Del'];

					$SQL = 'SELECT KZM_KEY, XBN_NAME';
					$SQL .= ' FROM KENNWOERTERGRUPPENMITGLIEDER ';
					$SQL .= ' LEFT OUTER JOIN Benutzer ON kzm_xbn_key = xbn_key';
					$SQL .= ' WHERE KZM_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$KEWKey=$rsDaten->FeldInhalt('KZM_KZG_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_NAME'),$rsDaten->FeldInhalt('XBN_NAME'));
					break;
				case 'Zugriffe':
					$Tabelle = 'KEZ';
					$Key=$_GET['Del'];

					$SQL = 'SELECT KEZ_KEY, KEZ_KEW_KEY, COALESCE(XBN_NAME,KZG_BEZEICHNUNG) AS XBN_NAME';
					$SQL .= ' FROM KENNWOERTERZUGRIFFE ';
					$SQL .= ' LEFT OUTER JOIN Benutzer ON kez_xxx_key = xbn_key';
					$SQL .= ' LEFT OUTER JOIN KENNWOERTERGRUPPEN ON kez_xxx_key = kzg_key';
					$SQL .= ' WHERE KEZ_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);
					$KEWKey=$rsDaten->FeldInhalt('KEZ_KEW_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_NAME'),$rsDaten->FeldInhalt('XBN_NAME'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'KEW':
				$SQL = 'DELETE FROM KENNWOERTER WHERE KEW_KEY=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'KZM':
				$SQL = 'DELETE FROM KENNWOERTERGRUPPENMITGLIEDER WHERE KZM_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtKEWKey'];
				break;
			case 'KEZ':
				$SQL = 'DELETE FROM KENNWOERTERZUGRIFFE WHERE KEZ_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtKEWKey'];
				break;
			case 'KZG':
				$SQL = 'DELETE FROM KENNWOERTERGRUPPEN WHERE KZG_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',200910201246,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./kennwoerter_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('KEWKey',$KEWKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>