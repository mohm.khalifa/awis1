<?php
//***********************************************************************************************
// Handliste
//
// �nderung:
// 20.04.2010 - skerres - Kurzwahlen mit eingebaut und Speichernaufruf uaf Standard ge�ndert.
//
//***********************************************************************************************
global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con,153);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Listen Telefonverzeichnis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

require_once('fpdf.php');
require_once 'fpdi.php';

/***************************************
* Neue PDF Datei erstellen
***************************************/
define('FPDF_FONTPATH','font/');
$pdf = new fpdi('p','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);

/***************************************
*
* Daten laden
*
***************************************/

$Bedingung='';
		// Daten suchen
if($_POST['txtKategorie']!='-1')
{
	$Bedingung .= ' AND KON_KKA_KEY=0' . $_POST['txtKategorie'];
}
if($_POST['txtBereich']!='-1')
{
	$Bedingung .= ' AND KON_KBE_KEY=0' . $_POST['txtBereich'];
}
if($_POST['txtGebaeude']!='-1')
{
	$Bedingung .= ' AND KON_GEB_ID=0' . $_POST['txtGebaeude'];
}

if($_POST['txtAbteilung']!='-1')
{
	$Bedingung .= ' AND KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen WHERE KZA_KAB_KEY=0' . $_POST['txtAbteilung'] . ')';
}

	// Filialliste
if(isset($_POST['chkFilialen']))
{
	$Bedingung .= "AND BITAND(KON_LISTEN,2)=2";
}
	// �ffentliche Daten
if(isset($_POST['chkOeffentliche']))
{
	$Bedingung .= "AND BITAND(KON_LISTEN,1)=1";
}

$Bedingung .= ' AND KON_STATUS = \'A\'';

/*$SQL = "SELECT DISTINCT GEB_ID, GEB_Bezeichnung";
$SQL .= " FROM Kontakte, GEBAEUDE ";
$SQL .= " WHERE ((KON_GEB_ID = GEB_ID(+)) ";
$SQL .= " " . $Bedingung . ")";
$SQL .= " ORDER BY GEB_ID";
		// Osrtliste aufbauen
$rsGebaeude=awisOpenRecordset($con,$SQL);
$rsGebaeudeZeilen = $awisRSZeilen;
$OrtsListe = 'Orte: ';
for($i=0;$i<$rsGebaeudeZeilen;$i++)
{
	$OrtsListe .= $rsGebaeude["GEB_ID"][$i] . "=" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . " // ";
}*/
$OrtsListe='';

$SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, GEB_ID";
$SQL .= " FROM Kontakte, GEBAEUDE ";
$SQL .= " WHERE ((KON_GEB_ID = GEB_ID(+)) ";
$SQL .= " " . $Bedingung . ")";
$SQL .= " AND KON_KEY IN (SELECT KKO_KON_KEY FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=6)";
$SQL .= " ORDER BY KON_NAME1,KON_NAME2";

$rsKON = awisOpenRecordset($con,$SQL);
$rsKONZeilen = $awisRSZeilen;

if($rsKONZeilen==0)			// Keine Daten gefunden
{
	$LinkerRand = 15;
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,150,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(170,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(180,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
}
else 						// Daten gefunden
{
	//$NeueSeite = true;
	$Zeile = 0;
	$LinkerRand = 15;
	$LetzterBlock = '';

	NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);

	for($KONZeile=0;$KONZeile<$rsKONZeilen;$KONZeile++)
	{
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);				// Schrift setzen

		$ZeilenHoehe = 1;	// Eine Zeile hoch

			// Daten sammeln und H�hen berechnen
		$AusgabeAbt = '';
		$rsKZA = awisOpenRecordset($con,"SELECT KAB_ABTEILUNG, KZA_LISTENHINWEIS FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY=0" . $rsKON["KON_KEY"][$KONZeile]);
		for($ZeilenHoeheAbt=0;$ZeilenHoeheAbt<$awisRSZeilen;$ZeilenHoeheAbt++)
		{
//			$AusgabeAbt .= ", " . $rsKZA['KAB_ABTEILUNG'][$ZeilenHoeheAbt];
			$AusgabeAbt .= ", " . $rsKZA['KAB_ABTEILUNG'][$ZeilenHoeheAbt] . ' ' . $rsKZA['KZA_LISTENHINWEIS'][$ZeilenHoeheAbt];
		}
		$foo = $pdf->GetStringWidth(substr($AusgabeAbt,2));
		$ZeilenHoeheAbt = round(($foo/58)+0.5,0);			// H�he nach der Textausgabe steuern
		$ZeilenHoeheAbt=($ZeilenHoeheAbt==0?1:$ZeilenHoeheAbt);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheAbt)?$ZeilenHoeheAbt:$ZeilenHoehe;

		$AusgabeHandy = '';
		$KurzwahlHandy = '';
		$rsKonHandy = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT, KKO_KURZWAHL FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=6 AND KKO_KON_KEY=0" . $rsKON['KON_KEY'][$KONZeile]);
		for($ZeilenHoeheHandy=0;$ZeilenHoeheHandy<$awisRSZeilen;$ZeilenHoeheHandy++)
		{
			$AusgabeHandy .= "\n " . $rsKonHandy['KKO_WERT'][$ZeilenHoeheHandy];
			$KurzwahlHandy .= "\n " . $rsKonHandy['KKO_KURZWAHL'][$ZeilenHoeheHandy];
		}
		$ZeilenHoeheHandy=($ZeilenHoeheHandy==0?1:$ZeilenHoeheHandy);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheHandy)?$ZeilenHoeheHandy:$ZeilenHoehe;

		if($LetzterBlock!=substr($rsKON['KON_NAME1'][$KONZeile],0,1))
		{
			if($Zeile > (275-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
			{
				NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',8);				// Schrift setzen
			}
			$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
			$pdf->SetFont('Arial','B',12);
			$pdf->MultiCell(190,5,strtoupper(substr($rsKON['KON_NAME1'][$KONZeile],0,1)),1,'L',1);
			$Zeile+=5;
			$LetzterBlock=substr($rsKON['KON_NAME1'][$KONZeile],0,1);
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}

		if($Zeile > (280-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
		{
			//$NeueSeite=true;
			NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}

		// Daten ausgeben
		$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
		$foo = $rsKON['KON_NAME1'][$KONZeile] . ($rsKON['KON_NAME2'][$KONZeile]==''?'':', '.$rsKON['KON_NAME2'][$KONZeile]);
		$pdf->MultiCell(70,$ZeilenHoehe*4,$foo,1,'L',1);

		$pdf->setXY($LinkerRand+70,$Zeile);							// Abteilungen
		$pdf->MultiCell(55,($ZeilenHoehe-$ZeilenHoeheAbt+1)*4,substr($AusgabeAbt,2),1,'L',1);

		$pdf->setXY($LinkerRand+125,$Zeile);						// E-Handy
		$pdf->MultiCell(45,($ZeilenHoehe-$ZeilenHoeheHandy+1)*4,substr($AusgabeHandy,2),1,'L',1);

		$pdf->setXY($LinkerRand+170,$Zeile);						// Kurzwahl
		$pdf->MultiCell(20,($ZeilenHoehe-$ZeilenHoeheHandy+1)*4,substr($KurzwahlHandy,2),1,'L',1);

		$Zeile += $ZeilenHoehe*4;
	}	// Datens�tze

	$pdf->setXY($LinkerRand,285);					// Cursor setzen
	$pdf->SetFont('Arial','',6);					// Schrift setzen
			// �berschrift
	$pdf->MultiCell(160,3,$OrtsListe,0,'L',0);
	$pdf->setXY(190,285);					// Cursor setzen
	$pdf->Cell(170,3,'Seite ' . $pdf->PageNo(),0,0,'L',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(180,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);


}	// Ende Datenaufbereitung


/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
$pdf->Output($DateiName);
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";
echo "<br><br><a href=./telefon_Main.php?cmdAktion=Ausdrucke>Neue Suche</a>";

/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  string OrtsListe
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$OrtsListe)
{
	static $Seite=0;


	if($Seite > 0)		// Fu�zeile?
	{
		$pdf->setXY($LinkerRand,285);					// Cursor setzen
		$pdf->SetFont('Arial','',6);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(160,3,$OrtsListe,0,'L',0);
		$pdf->setXY(190,285);					// Cursor setzen
		$pdf->Cell(170,3,'Seite ' . $Seite,0,0,'L',0);

		$pdf->setXY($LinkerRand,290);					// Cursor setzen
		$pdf->SetFont('Arial','',5);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(180,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
	}
	$Seite++;

	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,185,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(190,6,"Telefonliste ATU - Zentrale Weiden",0,0,'C',0);

	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',8);				// Schrift setzen
	$pdf->cell(190,6,"Stand per " . date('d.m.Y'),0,0,'C',0);


	// �berschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->setXY($LinkerRand,18);				// Cursor setzen
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	$pdf->cell(70,6,"Mitarbeiter/-in",1,0,'C',1);

	$pdf->setXY($LinkerRand+70,18);							// Cursor setzen
	$pdf->cell(55,6,"Abteilung",1,0,'C',1);

	$pdf->setXY($LinkerRand+125,18);						// Cursor setzen
	$pdf->cell(45,6,"Handy",1,0,'C',1);

	$pdf->setXY($LinkerRand+170,18);						// Cursor setzen
	$pdf->cell(20,6,"Kurzwahl",1,0,'C',1);

	$Zeile = 24;
}
?>