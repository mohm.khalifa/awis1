/***************************************************************************
* Tabellendefinitionen : GewinnspielNuerburgring
* Erstellt am : 20.03.2017 16:25:00
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE TABLE GewinnspielNuerburgring(
GWN_KEY NUMBER(10,0) CONSTRAINT CNN_GWN_KEY NOT NULL,
GWN_ANTWORT VARCHAR2(250),
GWN_NAME VARCHAR2(250),
GWN_VORNAME VARCHAR2(250),
GWN_PERSNR NUMBER(10,0) DEFAULT '0',
GWN_HERKUNFT VARCHAR2(250),
GWN_SHIRTGROESSE VARCHAR2(250),
GWN_TELNR VARCHAR2(250),
GWN_MAIL VARCHAR2(250),
GWN_USER VARCHAR2(250) DEFAULT USER,
GWN_USERDAT DATE,
CONSTRAINT PK_GWN_KEY PRIMARY KEY(GWN_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : GewinnspielNuerburgring
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

COMMENT ON COLUMN GewinnspielNuerburgring.GWN_KEY IS 'KEY';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_ANTWORT IS '1: VW K�fer; 2: Opel Astra OPC; 3: Tesla Model S';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_NAME IS 'Name';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_VORNAME IS 'Vorname';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_PERSNR IS 'Personalnummer';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_HERKUNFT IS 'Filiale/Abteilung';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_SHIRTGROESSE IS 'T-Shirt-Gr��e';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_TELNR IS 'Telefonnummer';
COMMENT ON COLUMN GewinnspielNuerburgring.GWN_MAIL IS 'Email';


/***************************************************************************
* Indexdefinitionen : GewinnspielNuerburgring
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

/*DROP INDEX IDX_GWN_ID;*/
CREATE INDEX IDX_GWN_ID ON GewinnspielNuerburgring( GWN_KEY) TABLESPACE INDX
;

/***************************************************************************
* Sequenz erstellen : GewinnspielNuerburgring
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE SEQUENCE SEQ_GWN_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

/*DROP TRIGGER trg_seq_GWN_KEY;*/

CREATE OR REPLACE TRIGGER trg_seq_GWN_KEY
     BEFORE INSERT ON GewinnspielNuerburgring
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.GWN_KEY := seq_GWN_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 20.03.2017 16:25:00
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='GWN';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('GWN', 'GewinnspielNuerburgring', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='GWN' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('GWN','Tabelle', 'GewinnspielNuerburgring', 'GewinnspielNuerburgring')
;


/***************************************************************************
* Ende des Skripts GewinnspielNuerburgring
****************************************************************************
* Fertig am : 20.03.2017 16:25:00
***************************************************************************/

COMMIT;
