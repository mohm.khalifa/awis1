<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $AWISBenutzer;

	$Rechte_OEN = awisBenutzerRecht($con,402);	
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $AWISBenutzer->BenutzerName()));
	
	$SQL = "SELECT * FROM LieferantenArtikel, Lieferanten, ArtikelPruefPersonal, TeileInfos WHERE APP_ID(+) = LAR_APP_ID AND LIE_NR(+) = LAR_LIE_NR AND TEI_KEY1 = LAR_KEY AND TEI_KEY2=0" . $Artikel[0] ;
	if(!isset($HTTP_GET_VARS['LARSort']))
	{
		$SQL .= " ORDER BY LAR_LARTNR";
	}
	else
	{
		$SQL .= " ORDER BY " . $HTTP_GET_VARS['LARSort'] . ", LAR_LARTNR";
	}
	$rsLiefArt = awisOpenRecordset($con, $SQL);
	if($rsLiefArt==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsLiefartZeilen=$awisRSZeilen;

	$MitReklInfo = awis_BenutzerParameter($con,"ReklamationsInfoLieferanten" , $AWISBenutzer->BenutzerName() );
	
	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr><td id=FeldBez></td>";
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LARTNR&Key=" . $Artikel[0] . "><u>L</u>iefArtNr</a></td>";
	if($MitReklInfo)
	{
		print "<td id=FeldBez>Info</td>";
	}
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LIE_NR&Key=" . $Artikel[0] . ">LiefNr</a></td>";	
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LIE_NAME1&Key=" . $Artikel[0] . ">Lieferant</a></td>";	
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=APP_NAME&Key=" . $Artikel[0] . ">Pr�fer</a></td>";	
	print "<td id=FeldBez>Zuord.</td>";
	print "<td id=FeldBez>Rekl.</td>";
	print "</tr>";
	
	for($LiefArt=0;$LiefArt<$rsLiefartZeilen;$LiefArt++)
	{
		print "<tr>";
	
		if(($Rechte_OEN & 2)==2 AND strtoupper($rsLiefArt["TEI_USER"][$LiefArt])!='WWS')
		{
			print "<td><input name=cmdLARLoeschen_" . $rsLiefArt["TEI_KEY"][$LiefArt] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\" onmouseover=\"window.status='Zuordnung l�schen';return true;\"></td>";
		}
		else
		{
			print "<td></td>";
		}
		
		print "<td><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt] . " onmouseover=\"window.status='Lieferantenartikel';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >" . $rsLiefArt["LAR_LARTNR"][$LiefArt] . "</a></td>";

		if($MitReklInfo)
		{
			print "<td align=center valign=center>";
			$DateiName = realpath('../dokumente/lieferanten/reklamationen') . '/lief_rekl_' . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . '.pdf';
			if(file_exists($DateiName))
			{
				print "<a href=../dokumente/lieferanten/reklamationen/lief_rekl_" . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . ".pdf><img border=0 src=/bilder/pdf.png alt=Anleitung onmouseover=\"window.status='Reklamation';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Reklamation';return true;\"></a>";
			}
			print "</td>";
		}
		
		print "<td>" . $rsLiefArt["LAR_LIE_NR"][$LiefArt] . "</td>";
		print "<td>" . $rsLiefArt["LIE_NAME1"][$LiefArt] . "</td>";
		print "<td>" . $rsLiefArt["APP_NAME"][$LiefArt] . "</td>";
		print "<td>" . $rsLiefArt["TEI_USER"][$LiefArt] . "</td>";

		print "<td>" . ($rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]==''?'- -':"<font color=#FF0000>" . $rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]) . "</font></td>";
		
		print "</tr>";
		
		// Gebrauchsnummern anzeigen
		if($HTTP_GET_VARS["GNr"]=='Ja')		
		{
			$rsGNr = awisOpenRecordset($con, "SELECT * FROM Gebrauchsnummern, TEILEINFOS WHERE GNR_KEY = TEI_KEY2 AND TEI_KEY1 = " . $rsLiefArt["LAR_KEY"][$LiefArt]);
			$rsGNRZeilen = $awisRSZeilen;
			
			if($rsGNRZeilen>0)
			{
 				print "<tr><td></td><td colspan=4>";
				print "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
				for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
				{
					print "<td>Gebrauchsnummer:</td>";
					print "<td><input name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
					print "</td>";
					print "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
					print "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
					print "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
				}
				print "</tr></table>";
				print "</td></tr>";
			}
			else
			{
				print "\n<!--Keine Gebrauchsnummern gefunden-->\n";
			}
					
			unset($rsGNr);
		}
	}

		// Hinzuf�gen von Datens�tzen
	if(($Rechte_OEN & 2)==2)
	{
		print "<tr>";
		
		// Lieferantenartikel
		print "<td></td>";
		print "<td><input accesskey=l name=txtLAR_LARTNR_0></td>";
		if($MitReklInfo)
		{
			print "<td>&nbsp;</td>";
		}

		print "<td><input name=txtLAR_LIE_NR size=6 onchange=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value=''> oder </td>";
		print "<td><select name=txtLAR_LIE_NR2 onchange=document.getElementsByName(\"txtLAR_LIE_NR\")[0].value=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value;>";
		
		$rsLieferanten = awisOpenRecordset($con, "SELECT * FROM Lieferanten ORDER BY LIE_Name1");
		$rsLieferantenZeilen = $awisRSZeilen;
		print "<option value=0>Bitte w�hlen...</option>";
		
		for($LiefNr=0;$LiefNr<$rsLieferantenZeilen;$LiefNr++)
		{
			print "<option value=" . $rsLieferanten["LIE_NR"][$LiefNr] . ">" . $rsLieferanten["LIE_NAME1"][$LiefNr] . "</option>";
		}
		unset($rsLieferanten);
		
		print "</select></td>";


		print "<td>/</td>";			// Pr�fer
		print "<td>/</td>";
		print "<td>/</td>";			// Reklamation
		
		print "</tr>";
	
	}

	print "</table>";
	
	unset($rsLiefArt);
?>