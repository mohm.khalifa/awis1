<?php
/**
 * awis_HeidlerImport.inc
 *
 * Importiert die Heidler-DPD-Dateien
 *
 * @author Pascal Pirschel
 * @copyright ATU - Auto-Teile-Unger
 * @version 20160908
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');
require_once('awisRest.inc');

class awis_HeidlerImport
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 100=Alles)
	 */
	private $_DebugLevel = 0;

    /**
     * Verbindungsinfos
     * @var array
     */
    private $_FTPDaten = [
        'host'             => '62.159.142.203',
        'user'             => 'atu04700',
        'passwort'         => 'Atu00011',
        'ordner'           => 'abhol',
        'pattern'          => 'rep_atu.',
        'downloadpfad'     => '/daten/daten/teams/AWIS-Transfer/06-NVS/08-NOX/in/',
        'archivpfad'       => '/daten/daten/teams/AWIS-Transfer/06-NVS/08-NOX/in/Archiv/',
        'errorpfad'        => '/daten/daten/teams/AWIS-Transfer/06-NVS/08-NOX/in/Error/',
    ];

    private $DPDRestHeader = [
        'Version'                     => '100',
        'Language'                    => 'de_DE',
        'PartnerCredentials-Name'     => 'DPD Cloud Service Alpha2',
        'PartnerCredentials-Token'    => '33879594E70436D58685',
        'UserCredentials-cloudUserID' => '639134',
        'UserCredentials-Token'       => '338746249733968',
    ];

    /**
     * Array f�r Fehler
     *
     * @var array
     */
    private $error = [
        'fatal' => false,
    ];

	/**
	 * Importverzeichnisse DPD
	 * @var array
	 */
	private $_DPDImportDateiPfad = array(
		'/daten/daten/teams/AWIS-Transfer/06-NVS/04-Exportdatei/in/EXPORTDATEI_K12_3/' => '/daten/daten/teams/AWIS-Transfer/06-NVS/04-Exportdatei/in/EXPORTDATEI_K12_3/Archiv_sonst/',
		'/daten/daten/teams/AWIS-Transfer/06-NVS/04-Exportdatei/in/EXPORTDATEI_ACADEMY/' => '/daten/daten/teams/AWIS-Transfer/06-NVS/04-Exportdatei/in/EXPORTDATEI_ACADEMY/Archiv_sonst/'
    );

	/**
	 * Importverzeichnisse TNT
	 * @var array
	 */
	private $_TNTImportDateiPfad = '/daten/daten/teams/AWIS-Transfer/06-NVS/01-Heidler-WaWiRueck/in/prod/';

	/**
	 * Emailbenachrichtigung
	 * @var string
	 */
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');

    public $REST;

	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->REST = new awisRest('DPD-Paketstatus');
    }

	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel)) {
            $this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0', $NeuerLevel);
		}

		return $this->_DebugLevel;
	}

	public function ImportDPD() {
        try {
        	$Werkzeug = new awisWerkzeuge();

			if($this->_DebugLevel>50)
            {
                $Werkzeug->EMail($this->_EMAIL_Infos,'OK Heidler-DPD-Import beginnt.','',1,'','awis@de.atu.eu');
            }

            $this->debugAusgabe(date('d.m.Y') . 'Heidler-DPD-Import BEGINNT', 5);

			foreach ($this->_DPDImportDateiPfad as $pfad => $newPfad) {
                foreach (scandir($pfad) as $file) {
                    if (!is_dir($pfad . $file)) {
                        //Auskommentierte Zeilen und leere IF aufgrund von Kl�rungsbedarf ob Applikation komplett abgel�st werden soll
                        if (substr($file, 0, 10) == 'MPSEXPDATA') {
                            rename($pfad . $file, $newPfad . $file);
                        } elseif (substr($file, 20, 3) == 'dat') {
                            rename($pfad . $file, $pfad . 'Archiv_exp/imp_' . $file);
                        } elseif (substr($file, 0, 3) == 'imp') {
                            rename($pfad . $file, $pfad . 'Archiv_exp/' . $file);
                        } else {
                            $currentFile = fopen($pfad . $file, 'r');
                            $firstLine = true;
                            $doppeltGecheckt = false;
                            while (!feof($currentFile)) {
                                $line = stream_get_line($currentFile , 0, "\r\n");
                                if ($firstLine or strlen($line) == 0) {
                                    $firstLine = false;
                                    continue;
                                }
                                $values = explode(';', $line);

								//Schauen, ob diese Daten schonmal Importiert wurden
								if (!$doppeltGecheckt) {
                                    $doppeltGecheckt = true;
									$SQL = 'SELECT * FROM DPD_EXPORT@NVSWEN WHERE';
									$SQL .= ' DATUM = ' . $this->_DB->WertSetzen('DPD', 'DT.M.J', $values[10]);
									$SQL .= ' AND PAKETSCHEINNR = ' . $this->_DB->WertSetzen('DPD', 'T', $values[22]);

									$anzahlZeilen = $this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->Bindevariablen('DPD', true));

									if ($anzahlZeilen > 0) {
                                        $this->debugAusgabe( $pfad . $file . 'wurde bereits importiert!', 5);
										break;
									}
                                }

                                for ($i = 1; $i <= $values[8]; $i++) {
                                    $SQL  = 'INSERT INTO DPD_EXPORT@NVSWEN (DATUM, PAKETSCHEINNR, GEWICHT, PACKSTKNR, NAME_1, NAME_2, NAME_3, EMPF_STRASSE, EMPF_LKZ, EMPF_PLZ, EMPF_ORT, KNDNR, MANDANT) VALUES(';
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'DT.M.J', $values[10]) . ', '; //DATUM
                                    if ($i == 1) {
                                        $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[22]) . ', '; //PAKETSCHEINNR
                                        $SQL .= $this->_DB->WertSetzen('DPD', 'N4', $values[9]) . ', '; //GEWICHT
                                    } else {
                                        $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[99+$i]) . ', '; //PAKETSCHEINNR
                                        $SQL .= $this->_DB->WertSetzen('DPD', 'N4', $values[21+$i]) . ', '; //GEWICHT
                                    }
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $i . '/' . $values[8]) . ', '; //PACKSTKNR
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[1]) . ', '; //NAME_1
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[2]) . ', '; //NAME_2
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[3]) . ', '; //NAME_3
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[4]) . ', '; //Str
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[5]) . ', '; //Land
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[6]) . ', '; //PLZ
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[7]) . ', '; //Ort
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[0], true, '0') . ', '; //KNDNR
                                    $SQL .= $this->_DB->WertSetzen('DPD', 'T', $values[160], true, '0') . ')'; //MANDANT

                                    $this->_DB->Ausfuehren($SQL,'DPD',true,$this->_DB->Bindevariablen('DPD',true));
                                }
                            }
                            fclose($currentFile);
                            rename($pfad . $file, $pfad . 'old/imp_' . $file);
                        }
                    }
                }
            }

            //MERGE in DPD-Status; alle neuen DPD-Pakete in DPD_Status eintragen mit Delivered_Status offen bzw. false
            $SQL  ='MERGE INTO DPD_STATUS@NVSWEN TAB';
            $SQL .=' USING (SELECT MV.* FROM MV_HEIDLER MV';
            $SQL .='       WHERE MV.TRACKING_NR NOT LIKE \'Fehlereti%\' ';
            $SQL .='       AND TRUNC(MV.DATUM_KOMMISSIONIERUNG) >= SYSDATE - 30 ';
            $SQL .='       AND MV.VERSANDART = \'2\') SRC';
            $SQL .=' ON (TAB.TRACKING_NR = SRC.TRACKING_NR AND TRUNC(TAB.DATUM_KOMMISSIONIERUNG) = TRUNC(SRC.DATUM_KOMMISSIONIERUNG) AND TAB.KNDNR = SRC.KNDNR)';
            $SQL .=' WHEN NOT MATCHED THEN';
            $SQL .=' INSERT';
            $SQL .='     (';
            $SQL .='       TRACKING_NR,';
            $SQL .='       STATUS_ID,';
            $SQL .='       HEADLINE,';
            $SQL .='       LAST_DESCRIPTION,';
            $SQL .='       STATUS_DATE,';
            $SQL .='       DEPOT_NR,';
            $SQL .='       DEPOT_ZIPCODE,';
            $SQL .='       DEPOT_CITY,';
            $SQL .='       DELIVERED_STATUS,';
            $SQL .='       IMPORT_DATE,';
            $SQL .='       DATUM_KOMMISSIONIERUNG,';
            $SQL .='       KNDNR,';
            $SQL .='       LAST_CHECK';
            $SQL .='     )';
            $SQL .='     VALUES';
            $SQL .='     (';
            $SQL .='       SRC.TRACKING_NR,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       null,';
            $SQL .='       0,';
            $SQL .='       SYSDATE,';
            $SQL .='       TRUNC(SRC.DATUM_KOMMISSIONIERUNG),';
            $SQL .='       SRC.KNDNR,';
            $SQL .='       to_date(\'01.01.1970\')';
            $SQL .=' )';

            $this->debugAusgabe(date('d.m.Y') . ' Fuehre MERGE aus', 50);
            $this->_DB->Ausfuehren($SQL);
            $this->debugAusgabe($this->_DB->LetzterSQL(), 100);

            if($this->_DebugLevel>=50) {
                $Werkzeug->EMail($this->_EMAIL_Infos,'OK Heidler-DPD-Import erfolgreich.','',1,'','awis@de.atu.eu');
            }

            $this->debugAusgabe(date('d.m.Y') . ' Heidler-DPD-Import erfolgreich', 5);


		} catch (awisException $ex) {
			echo $this->_DB->LetzterSQL().PHP_EOL;
			echo 'AWIS-Fehler:'.$ex->getMessage().PHP_EOL;
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR Heidler-DPD-Import fehlgeschlagen.','AWIS-Fehler:'.$ex->getMessage().PHP_EOL,1,'','awis@de.atu.eu');
		} catch (Exception $ex) {
			echo 'allg. Fehler:'.$ex->getMessage().PHP_EOL;
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR Heidler-DPD-Import fehlgeschlagen.','allg. Fehler:'.$ex->getMessage().PHP_EOL,1,'','awis@de.atu.eu');
		}
	}

	public function ImportTNT() {
		try {
			$Werkzeug = new awisWerkzeuge();

			if($this->_DebugLevel>=50)
			{
				$Werkzeug->EMail($this->_EMAIL_Infos,'OK Heidler-Tagesabschluss-Import beginnt.','',1,'','awis@de.atu.eu');
            }

			$this->debugAusgabe(date('d.m.Y') . ' Heidler-Tagesabschluss-Import BEGINNT', 5);

			$folder = scandir($this->_TNTImportDateiPfad);
            foreach ($folder as $key => $file) {
                if (is_dir($this->_TNTImportDateiPfad . $file)) {
                    unset($folder[$key]);
                }
            }

            if (count($folder) > 0) {
                $this->debugAusgabe(date('d.m.Y') . ' Daten wurden gefunden, Leere TAGESABSCHLUSS_IMPORT', 50);
                $SQL = 'DELETE FROM TAGESABSCHLUSS_IMPORT@NVSWEN';
                $this->_DB->Ausfuehren($SQL);
            } else {
                die('Keine Daten vorhanden' . PHP_EOL);
            }

            $this->debugAusgabe(date('d.m.Y') . ' Daten werden importiert', 50);
            foreach ($folder as $file) {
                $currentFile = NULL;
                $currentFile = fopen($this->_TNTImportDateiPfad . $file, 'r');
                $firstLine = true;
                while (!feof($currentFile)) {
                    $line = stream_get_line($currentFile, 0, "\r\n");
                    if ($firstLine or strlen($line) == 0) {
                        $firstLine = false;
                        continue;
                    }

                    $values = explode(';', $line);

                    $SQL  = 'INSERT INTO TAGESABSCHLUSS_IMPORT@NVSWEN VALUES (';
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[0]) . ', '; // SATZKENNUNG
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[1]) . ', '; // LIEFERSCHEINNR
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', (strpos($values[18],'C')!==false?'1':'2')) . ', '; // VERSANDART
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[3]) . ', '; // KNDNR
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[4]) . ', '; // EMPF_NAME1
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[5]) . ', '; // EMPF_NAME2
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[6]) . ', '; // EMPF_NAME3
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[8]) . ', '; // EMPF_STRASSE
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[9]) . ', '; // EMPF_LKZ
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[10]) . ', '; // EMPF_PLZ
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[11]) . ', '; // EMPF_ORT
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[12]) . ', '; // MANDANT
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[13]) . ', '; // AVIS_HINWEIS1
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[14]) . ', '; // AVIS_HINWEIS2
					$SQL .= $this->_DB->WertSetzen('TAG', 'T', str_replace('.', ',', $values[15])) . ', '; // GEWICHT_NETTO
					$SQL .= $this->_DB->WertSetzen('TAG', 'N0', $values[16]) . ', '; // PACKSTUECK_ANZAHL
					$SQL .= $this->_DB->WertSetzen('TAG', 'N0', $values[17]) . ', '; // PACKSTUECK_NR
					$SQL .= $this->_DB->WertSetzen('TAG', 'T', $values[18]) . ', '; // TRACKING_NR
					$SQL .= $this->_DB->WertSetzen('TAG', 'DU', $values[19]) . ', '; // LABEL_GENDATE
                    $SQL .= $this->_DB->WertSetzen('TAG', 'D', $values[20]) . ', '; // DATUM_KOMMISSIONIERUNG
                    $SQL .= $this->_DB->WertSetzen('TAG', 'T', str_replace('.', ',', $values[21])) . ')'; // FRACHTGEBUEHR

                    $this->_DB->Ausfuehren($SQL, 'TAG', true, $this->_DB->Bindevariablen('TAG', true));
                    $this->debugAusgabe($this->_DB->LetzterSQL(), 100);
                }
                fclose($currentFile);
                rename($this->_TNTImportDateiPfad . $file, $this->_TNTImportDateiPfad . 'Backup/' . $file);
            }


            $SQL  ='Update TAGESABSCHLUSS_IMPORT@NVSWEN';
            $SQL .=' set KNDNR = \'0\'';
            $SQL .=' Where KNDNR is null';

            $this->debugAusgabe(date('d.m.Y') . ' Update KNDNR', 50);
            $this->_DB->Ausfuehren($SQL);
            $this->debugAusgabe($this->_DB->LetzterSQL(), 100);


            $SQL  ='MERGE INTO TAGESABSCHLUSS@NVSWEN TAB';
            $SQL .=' USING (SELECT IMP.* FROM TAGESABSCHLUSS_IMPORT@NVSWEN IMP LEFT JOIN MV_HEIDLER MV';
            $SQL .='       ON MV.TRACKING_NR like \'%\' || IMP.TRACKING_NR || \'%\'';
            $SQL .='       AND TRUNC(IMP.DATUM_KOMMISSIONIERUNG) = TRUNC(MV.DATUM_KOMMISSIONIERUNG)';
            $SQL .='       WHERE MV.TRACKING_NR IS NULL ';
            $SQL .='       AND IMP.LIEFERSCHEINNR <> \'+\') SRC';
            $SQL .=' ON (TAB.TRACKING_NR = SRC.TRACKING_NR AND TRUNC(TAB.DATUM_KOMMISSIONIERUNG) = TRUNC(SRC.DATUM_KOMMISSIONIERUNG) AND TAB.KNDNR = SRC.KNDNR)';
            $SQL .=' WHEN NOT MATCHED THEN';
            $SQL .=' INSERT';
            $SQL .='     (';
            $SQL .='       SATZKENNUNG,';
            $SQL .='       LIEFERSCHEINNR,';
            $SQL .='       VERSANDART,';
            $SQL .='       KNDNR,';
            $SQL .='       EMPF_NAME1,';
            $SQL .='       EMPF_NAME2,';
            $SQL .='       EMPF_NAME3,';
            $SQL .='       EMPF_STRASSE,';
            $SQL .='       EMPF_LKZ,';
            $SQL .='       EMPF_PLZ,';
            $SQL .='       EMPF_ORT,';
            $SQL .='       MANDANT,';
            $SQL .='       AVIS_HINWEIS1,';
            $SQL .='       AVIS_HINWEIS2,';
            $SQL .='       GEWICHT_NETTO,';
            $SQL .='       PACKSTUECK_ANZAHL,';
            $SQL .='       PACKSTUECK_NR,';
            $SQL .='       TRACKING_NR,';
            $SQL .='       LABEL_GENDATE,';
            $SQL .='       DATUM_KOMMISSIONIERUNG,';
            $SQL .='       FRACHTGEBUEHR,';
            $SQL .='       IMPORTDAT';
            $SQL .='     )';
            $SQL .='     VALUES';
            $SQL .='     (';
            $SQL .='       SRC.SATZKENNUNG,';
            $SQL .='       SRC.LIEFERSCHEINNR,';
            $SQL .='       SRC.VERSANDART,';
            $SQL .='       SRC.KNDNR,';
            $SQL .='       SRC.EMPF_NAME1,';
            $SQL .='       SRC.EMPF_NAME2,';
            $SQL .='       SRC.EMPF_NAME3,';
            $SQL .='       SRC.EMPF_STRASSE,';
            $SQL .='       SRC.EMPF_LKZ,';
            $SQL .='       SRC.EMPF_PLZ,';
            $SQL .='       SRC.EMPF_ORT,';
            $SQL .='       SRC.MANDANT,';
            $SQL .='       SRC.AVIS_HINWEIS1,';
            $SQL .='       SRC.AVIS_HINWEIS2,';
            $SQL .='       SRC.GEWICHT_NETTO,';
            $SQL .='       SRC.PACKSTUECK_ANZAHL,';
            $SQL .='       SRC.PACKSTUECK_NR,';
            $SQL .='       SRC.TRACKING_NR,';
            $SQL .='       SRC.LABEL_GENDATE,';
            $SQL .='       SRC.DATUM_KOMMISSIONIERUNG,';
            $SQL .='       SRC.FRACHTGEBUEHR,';
            $SQL .='       SYSDATE';
            $SQL .=' )';

            $this->debugAusgabe(date('d.m.Y') . ' Fuehre MERGE aus', 50);
            $this->_DB->Ausfuehren($SQL);
            $this->debugAusgabe($this->_DB->LetzterSQL(), 100);


            if($this->_DebugLevel>=50) {
                $Werkzeug->EMail($this->_EMAIL_Infos, 'OK Heidler-Tagesabschluss-Import erfolgreich.', '', 1, '', 'awis@de.atu.eu');
            }

            $this->debugAusgabe(date('d.m.Y') . ' Tagesabschluss-Import erfolgreich', 5);

        } catch (awisException $ex) {
			echo $this->_DB->LetzterSQL().PHP_EOL;
			echo 'AWIS-Fehler:'.$ex->getMessage().PHP_EOL;
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR Heidler-Tagesabschluss-Import fehlgeschlagen.','AWIS-Fehler:'.$ex->getMessage().PHP_EOL,1,'','awis@de.atu.eu');
            if ($currentFile !== NULL) {
                fclose($currentFile);
                rename($this->_TNTImportDateiPfad . $file, $this->_TNTImportDateiPfad . 'Backup/' . $file . '.err');
            }
		} catch (Exception $ex) {
			echo 'allg. Fehler:'.$ex->getMessage().PHP_EOL;
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR Heidler-Tagesabschluss-Import fehlgeschlagen.','allg. Fehler:'.$ex->getMessage().PHP_EOL,1,'','awis@de.atu.eu');
            if ($currentFile !== NULL) {
                fclose($currentFile);
                rename($this->_TNTImportDateiPfad . $file, $this->_TNTImportDateiPfad . 'Backup/' . $file . '.err');
            }
		}
	}


    /**
     * Paketstatus von DPD abholen und importieren
     */
	public function ImportPaketdatenDPD(){
	    try{
            $this->_DB->TransaktionBegin();

            //Select alle TrackingNr, Delivered_Status: false and Import_Date -60 Tage
            $SQL = 'Select * ';
            $SQL .= 'FROM( ';
            $SQL .= 'SELECT dpd.TRACKING_NR, dpd.DATUM_KOMMISSIONIERUNG, dpd.KNDNR ';
            $SQL .= 'FROM DPD_STATUS@NVSWEN dpd ';
            $SQL .= 'INNER JOIN MV_HEIDLER mv on mv.TRACKING_NR = dpd.TRACKING_NR ';
            $SQL .= 'and TRUNC(mv.DATUM_KOMMISSIONIERUNG) = TRUNC(dpd.DATUM_KOMMISSIONIERUNG) ';
            $SQL .= 'and mv.KNDNR = dpd.KNDNR ';
            $SQL .= 'WHERE (TRUNC(dpd.DATUM_KOMMISSIONIERUNG) > TRUNC(SYSDATE -30) and (dpd.Delivered_Status=0 or dpd.Delivered_Status is null)) ';
            $SQL .= 'order by dpd.LAST_CHECK asc) ';
            $SQL .= 'WHERE ROWNUM <= 1800 ';

            $rec = $this->_DB->RecordSetOeffnen($SQL);

            while(! $rec->EOF()) {
                $PaketInfos = $this->REST->erstelleRequest('https://cloud.dpd.com/api/v1/getOrderStatus/' . $rec->FeldInhalt('TRACKING_NR') . '/null', 'GET', 'JSON', [], $this->DPDRestHeader, true);

                if ($PaketInfos["ErrorDataList"] != null) {
                    //Update DPD_STATUS: Description = $PaketInfos["ErrorDataList"][0]["ErrorMsgShort"];
                         $UPDATE_ERR ='UPDATE DPD_STATUS@NVSWEN dpd SET';
                         $UPDATE_ERR .=' dpd.LAST_DESCRIPTION = \'' . $PaketInfos["ErrorDataList"][0]["ErrorMsgLong"] . '\',';
                         $UPDATE_ERR .=' dpd.LAST_CHECK = sysdate';
                         $UPDATE_ERR .=' WHERE dpd.TRACKING_NR = ' . $this->_DB->WertSetzen('ERR', 'T', $rec->FeldInhalt('TRACKING_NR', 'T')) . '';
                         $UPDATE_ERR .=' AND dpd.KNDNR = ' . $this->_DB->WertSetzen('ERR', 'T', $rec->FeldInhalt('KNDNR', 'T')) .'';
                         $UPDATE_ERR .=' AND dpd.DATUM_KOMMISSIONIERUNG = ' . $this->_DB->WertSetzen('ERR', 'D', $rec->FeldInhalt('DATUM_KOMMISSIONIERUNG', 'D')) .'';

                         $this->_DB->Ausfuehren($UPDATE_ERR,'',true,$this->_DB->Bindevariablen('ERR',true));
                         $this->_DB->TransaktionCommit();

                } else {
                //Update DPD_STATUS mit
                //Headline | Description | StatusDate | Delivered_StatusReached | Zipcode | City | StatusID | DepotNr
                        $LetzterStatus = $PaketInfos["OrderStatus"]["LastStatusInfo"];

                        $UPDATE ='UPDATE DPD_STATUS@NVSWEN dpd SET';
                        $UPDATE .=' dpd.STATUS_ID = \'' . $LetzterStatus["StatusID"] . '\',';
                        $UPDATE .=' dpd.HEADLINE = \'' . $LetzterStatus["Headline"] . '\',';
                        $UPDATE .=' dpd.LAST_DESCRIPTION = \'' . $LetzterStatus["Description"] . '\',';
                        $UPDATE .=' dpd.STATUS_DATE = \'' . $LetzterStatus["StatusDate"] . '\',';
                        $UPDATE .=' dpd.DEPOT_NR = \'' . $LetzterStatus["DepotData"]["Depot"] . '\',';
                        $UPDATE .=' dpd.DEPOT_ZIPCODE = \'' . $LetzterStatus["DepotData"]["Address"]["ZipCode"] . '\',';
                        $UPDATE .=' dpd.DEPOT_CITY = \'' . $LetzterStatus["DepotData"]["Address"]["City"] . '\',';
                        $UPDATE .=' dpd.DELIVERED_STATUS = \'' . $PaketInfos["OrderStatus"]["StatusInfoContainer"]["Delivered"]["StatusReached"] . '\',';
                        $UPDATE .=' dpd.LAST_CHECK = sysdate';
                        $UPDATE .=' WHERE dpd.TRACKING_NR = ' . $this->_DB->WertSetzen('DPD', 'T',$rec->FeldInhalt('TRACKING_NR', 'T') ) . '';
                        $UPDATE .=' AND dpd.KNDNR = ' . $this->_DB->WertSetzen('DPD', 'T', $rec->FeldInhalt('KNDNR', 'T')) .'';
                        $UPDATE .=' AND dpd.DATUM_KOMMISSIONIERUNG = ' . $this->_DB->WertSetzen('DPD', 'D', $rec->FeldInhalt('DATUM_KOMMISSIONIERUNG', 'D')) .'';

                        $this->_DB->Ausfuehren($UPDATE,'',true,$this->_DB->Bindevariablen('DPD',true));
	                    $this->_DB->TransaktionCommit();
                }

                $rec->DSWeiter();
            }

        }catch (awisException $e) {
            if ($this->_DB->TransaktionAktiv()) {
                $this->_DB->TransaktionRollback();
            }

        }

    }


    /**
     * Paketstatus-Dateien vom NOX-FTP importieren
     */
    public function ImportPaketdatenNOX()
    {
        //Erst mal schauen, ob noch Dateien im Zielpfad vorhanden
        $localfiles = array_diff(scandir($this->_FTPDaten["downloadpfad"]), ['..', '.']);
        $count = 0;
        foreach ($localfiles as $file) {
            if (file_exists($this->_FTPDaten["downloadpfad"] . $file)) {
                $count++;
            }
        }
        if ($count > 0) {
            $this->debugAusgabe('Lokale Dateien zum Verarbeiten gefunden...', 0);
            foreach ($localfiles as $file) {
                $this->NOXFileWeiterreichen($file);
            }
        }

        //Jetzt auf dem FTP schauen, ob noch Dateien vorhanden
        $FTP = $this->FTPConnect();
        if ($FTP !== false) {
            $files = ftp_nlist($FTP, '.');
            if (count($files) == 0) {
                $this->debugAusgabe("Keine Dateien zur Abholung vorhanden", 0);
            } else {
                $this->debugAusgabe('Verarbeite Dateien vom FTP...', 0);
                foreach ($files as $file) {
                    if (strpos($file, $this->_FTPDaten["pattern"]) !== false) {
                        if (!@ftp_get($FTP, $this->_FTPDaten["downloadpfad"] . $file, $file, FTP_TEXT)) {
                            $this->error["msg"] = 'Konnte File ' . $file . ' nicht downloaden. Ueberspringe...' . PHP_EOL;
                            $this->debugAusgabe($this->error["msg"], 0);
                        } else {
                            $this->NOXFileWeiterreichen($file, true, $FTP);
                        }
                    }
                }
            }
        }

        $Werkzeug = new awisWerkzeuge();
        if (isset($this->error["msg"])) {
            if ($this->error["fatal"] == true) {
                $Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR NOX-Paketstatus Import.', $this->error["msg"], 1, '', 'awis@de.atu.eu');
            } else {
                $Werkzeug->EMail($this->_EMAIL_Infos, 'WARNING NOX-Paketstatus Import', $this->error["msg"], 1, '', 'awis@de.atu.eu');
            }
        } else {
            $Werkzeug->EMail($this->_EMAIL_Infos, 'OK NOX-Paketstatus Import', '', 1, '', 'awis@de.atu.eu');
        }
    }

    /**
     * Verbindet mit dem FTP und gibt die FTP resource (bereits auf den korrekten Ordner gewechselt) zur�ck.
     *
     * @return mixed (resource bei erfolg, ansonsten false)
     */
    private function FTPConnect()
    {
        $FTP = ftp_connect($this->_FTPDaten["host"]);
        if ($FTP === false) {
            $this->error["msg"] = 'Verbindung mit FTP nicht moeglich';
            $this->error["fatal"] = true;
            $this->debugAusgabe($this->error["msg"], 0);

            return false;
        }

        if (!@ftp_login($FTP, $this->_FTPDaten["user"], $this->_FTPDaten["passwort"])) {
            $this->error["msg"] = 'Login auf FTP nicht moeglich';
            $this->error["fatal"] = true;
            $this->debugAusgabe($this->error["msg"], 0);

            return false;
        }

        ftp_pasv($FTP, true);

        if (!@ftp_chdir($FTP, $this->_FTPDaten["ordner"])) {
            $this->error["msg"] = 'Ordner ' . $this->_FTPDaten["ordner"] . ' nicht verfuegbar.';
            $this->error["fatal"] = true;
            $this->debugAusgabe($this->error["msg"], 0);

            return false;
        }

        $dir = ftp_nlist($FTP, '.');
        if ($dir === false) {
            $this->debugAusgabe('Konnte Verzeichnis nicht auslesen. Versuche erneut im Aktiven Modus...', 10);
            ftp_pasv($FTP, false);
            $dir = ftp_nlist($FTP, '.');
            if ($dir === false) {
                $this->error["msg"] = 'Verzeichnis auslesen nicht moeglich';
                $this->error["fatal"] = true;
                $this->debugAusgabe($this->error["msg"], 0);

                return false;
            }
        }

        return $FTP;
    }

    private function NOXFileWeiterreichen($file, $FTPNutzen = false, $FTP = null) {
        if (!is_file($this->_FTPDaten["downloadpfad"] . $file)) {
            return;
        }
        if (!copy($this->_FTPDaten["downloadpfad"] . $file, $this->_FTPDaten["archivpfad"] . $file)) {
            $this->error["msg"] = (isset($this->error["msg"])?$this->error["msg"]:'') . 'Konnte File ' . $file . ' nicht ins Archiv kopieren. Ueberspringe...' . PHP_EOL;
            $this->debugAusgabe($this->error["msg"], 0);
        } elseif ($this->VerarbeiteNOXFile($this->_FTPDaten["downloadpfad"] . $file)) {
            unlink($this->_FTPDaten["downloadpfad"] . $file);
            if ($FTPNutzen and !ftp_delete($FTP, $file)) {
                $this->error["msg"] = (isset($this->error["msg"])?$this->error["msg"]:'') . $file . ' wurde zwar erfolgreich verarbeitet, konnte aber nich vom FTP geloescht werden.' . PHP_EOL;
                $this->error["msg"] .= 'Sollte bei naechstem Lauf keine Probleme verursachen. Falls es haeufiger passiert, mal haendisch probieren.' . PHP_EOL;
                $this->debugAusgabe($this->error["msg"], 0);
            }
        } else {
            $this->error["msg"] = (isset($this->error["msg"])?$this->error["msg"]:'') . $file . ' konnte nicht verarbeitet werden. Verschiebe ins Error-Verzeichnis...' . PHP_EOL;
            if (!rename($this->_FTPDaten["downloadpfad"] . $file, $this->_FTPDaten["errorpfad"] . $file)) {
                $this->error["msg"] .= $file . ' konnte nicht verschoben werden.' . PHP_EOL;
            }
            $this->debugAusgabe($this->error["msg"], 0);
        }
    }

    /**
     * @param $dateipfad string
     * @return boolean true wenn datei erfolgreich verarbeitet, false wenn nicht
     */
    private function VerarbeiteNOXFile($dateipfad)
    {
        $startzeit = time();

        $file = new SplFileObject($dateipfad);
        if ($file->getSize() == 0) {
            $this->debugAusgabe($file->getFilename() . " ist leer. Ueberspringe...", 0);

            return false;
        }

        //Neuste Paketinfos raussuchen
        $Daten = [];
        $count = 0;
        while (!$file->eof()) {
            $line = $file->current();
            if ($line != "") {
                if (substr($line, 0, 3) == 'PID') {
                    //Aufbau: 0: Paketnummer, 1: Datum (Format: YYYYMMDDHHMISS), 2: Scan-Ort, 3: Scan-Feld, 4: Statuscode, (Unn�tig: 5: Leer, 6: Zeilenumbruch)
                    $PID = explode('|', substr($line, 4));

                    //Wenn noch keine Daten zu diesem Paket oder neuere Daten
                    //oder wenn Daten von gleichem Zeitpunkt, aber neue mit Status STA77/STA99 kommen (manuell gescannt), dann vorherigen belassen, da diese genauer
                    if ((!isset($Daten[$PID[0]])) or $Daten[$PID[0]][1] < $PID[1] or ($Daten[$PID[0]][1] == $PID[1] and ($PID[4] != 'STA77' or $PID[4] != 'STA99'))) {
                        $Daten[$PID[0]] = $PID;
                    }
                }
            }

            $file->next();
            $count++;
        }

        $InsertCount = 0;
        foreach ($Daten as $DS) {
            try {
                //Datum umformatieren
                $DS[1] = date('d.m.Y H:i:s', strtotime($DS[1]));

                $this->_DB->TransaktionBegin();

                $Select = 'SELECT * FROM NOX_STATUS@NVSWEN WHERE TRACKING_NR = ' . $this->_DB->WertSetzen('NOX', 'T', $DS[0]);
                $rec = $this->_DB->RecordSetOeffnen($Select, $this->_DB->Bindevariablen('NOX'));

                $InsertNew = false;
                if ($rec->AnzahlDatensaetze() > 0) {
                    if (date_create_from_format("d.m.Y H:i:s",$rec->FeldInhalt('DATUM', 'DU').':00') < date_create_from_format("d.m.Y H:i:s",$DS[1])) {
                        $Delete = 'DELETE FROM NOX_STATUS@NVSWEN WHERE TRACKING_NR = ' . $this->_DB->WertSetzen('NOX', 'T', $DS[0]);
                        $this->_DB->Ausfuehren($Delete, 'NOX', true, $this->_DB->Bindevariablen('NOX'));
                        $InsertNew = true;
                    }
                } else {
                    $InsertNew = true;
                }

                if ($InsertNew) {
                    $insert = 'INSERT INTO NOX_STATUS@NVSWEN (TRACKING_NR, DATUM, SCAN_ORT, SCAN_FELD, STATUSCODE, IMPORTDAT) VALUES (';
                    $insert .= $this->_DB->WertSetzen('NOX', 'T', $DS[0]);
                    $insert .= ', to_date(' . $this->_DB->WertSetzen('NOX', 'T', $DS[1]) . ', \'dd.mm.yyyy hh24:mi:ss\')';
                    $insert .= ', ' . $this->_DB->WertSetzen('NOX', 'T', $DS[2]);
                    $insert .= ', ' . $this->_DB->WertSetzen('NOX', 'T', $DS[3]);
                    $insert .= ', ' . $this->_DB->WertSetzen('NOX', 'T', $DS[4]);
                    $insert .= ', sysdate)';

                    $this->_DB->Ausfuehren($insert, 'NOX', true, $this->_DB->Bindevariablen('NOX'));
                    $InsertCount++;
                }
                $this->_DB->TransaktionCommit();
            } catch (awisException $e) {
                if ($this->_DB->TransaktionAktiv()) {
                    $this->_DB->TransaktionRollback();
                }
                $this->error["msg"] = 'Fehler beim Import von ' . $file->getFilename() . ':' . PHP_EOL;
                $this->error["msg"] .= $e->getCode() . ': ' . $e->getMessage() . PHP_EOL;
                $this->error["msg"] .= "Letzter SQL:" . PHP_EOL;
                $this->error["msg"] .= $this->_DB->LetzterSQL() . PHP_EOL;
                $this->debugAusgabe($this->error["msg"], 0);

                return false;
            } catch (Exception $e) {
                $this->error["msg"] = 'Fehler beim Import von ' . $file->getFilename() . ':' . PHP_EOL;
                $this->error["msg"] .= $e->getCode() . ': ' . $e->getMessage() . PHP_EOL;
                $this->debugAusgabe($this->error["msg"], 0);

                return false;
            }
        }

        $this->debugAusgabe($file->getFilename() . ' wurde importiert.', 0);
        $this->debugAusgabe($count . ' Zeilen bzw. ' . count($Daten) . ' Datensaetze in ' . date('i:s', time() - $startzeit) . ' min verarbeitet.', 10);
        $this->debugAusgabe('Dadurch wurden ' . $InsertCount . ' neue Datensaetze importiert.', 10);

        return true;
    }

    private function debugAusgabe($text, $debugLevel)
    {
        if ($this->_DebugLevel >= $debugLevel) {
            echo $text . ((substr($text, -(strlen(PHP_EOL))) == PHP_EOL)?'':PHP_EOL);
        }
    }
}