<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$RechteStufe = awisBenutzerRecht($con, 1500);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Schluessel',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

awis_Debug('entwick',0,false,$_REQUEST);

if(isset($_POST['cmdSuche_x']))
{	
	$Params[0]=$_REQUEST['txtSuche_SchluesselNr'];
	$Params[1]=$_REQUEST['txtSuche_Mitarbeiter'];
	$Params[2]=$_REQUEST['txtSuche_Filiale'];
	$Params[3]=$_REQUEST['txtSuche_RechnungsNr'];
	$Params[4]=$_REQUEST['txtSuche_SLN_SLG_KEY'];
	$Params[5]=$_REQUEST['txtSuche_SLV_SLS_KEY'];
	$Params[6]=$_REQUEST['txtSuche_SLN_SLL_KEY'];

	awis_BenutzerParameterSpeichern($con, "Schluessel_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}
else
{
	$Params=explode(';',awis_BenutzerParameter($con, "Schluessel_Suche", $AWISBenutzer->BenutzerName()));	
}

$SQL="SELECT schluesselnummern.sln_schluesselnummer, schluesselnummern.sln_schluessellfdnr, schluesselnummern.sln_fil_id, ";  
$SQL.="schluesselnummern.sln_key, schluesselvergaben.slv_key,DECODE(schluesselvergaben.slv_per_nr,'-1','Nicht zugeordnet',schluesselvergaben.slv_per_nr) AS PER_NR, v_personal_komplett_map.per_nachname, v_personal_komplett_map.per_vorname, ";
$SQL.="schluesselnummern.sln_sicherkartennummer, schluesselgruppen.slg_bezeichnung ";
$SQL.="FROM schluesselnummern LEFT OUTER JOIN schluesselvergaben on (schluesselnummern.sln_key = schluesselvergaben.slv_sln_key) ";
$SQL.="LEFT OUTER JOIN v_personal_komplett_map on (v_personal_komplett_map.per_nr = schluesselvergaben.slv_per_nr) ";
$SQL.="LEFT OUTER JOIN schluesselgruppen on (schluesselgruppen.slg_key = schluesselnummern.sln_slg_key) ";
$SQL.="LEFT OUTER JOIN filialen on (filialen.fil_id=schluesselnummern.sln_fil_id) ";
	
	$Zusatz=FALSE;
	
if($Params[0]!='')
{
	$SQL.="WHERE (upper(schluesselnummern.sln_schluesselnummer) ".awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0).")";
	$SQL.=" OR (upper(schluesselnummern.sln_sicherkartennummer) ".awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0).")";
	$Zusatz=TRUE;	
}

If($Params[1]!='')
{	
	if($Zusatz)
	{
		$SQL.=" AND ( upper(v_personal_komplett_map.per_nachname) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) . " OR upper(v_personal_komplett_map.per_vorname) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) ." OR upper(schluesselvergaben.slv_per_nr) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) .")";
	}
	else
	{
		$SQL.="WHERE ( upper(v_personal_komplett_map.per_nachname) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) . " OR upper(v_personal_komplett_map.per_vorname) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) ." OR upper(schluesselvergaben.slv_per_nr) ". awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) .")";
		$Zusatz=TRUE;
	}
}

If($Params[2]!='')
{	
	if(is_numeric($Params[2]))
	{
		if($Zusatz)
		{
			$SQL.=" AND (schluesselnummern.sln_fil_id ". awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0) .")";
		}
		else
		{
			$SQL.="WHERE (schluesselnummern.sln_fil_id ". awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0) .")";
			$Zusatz=TRUE;
		}
	}
	else
	{
		if($Zusatz)
		{
			$SQL.=" AND ((upper(filialen.fil_ort) ". awisLIKEoderIST(strtoupper($Params[2]),TRUE,FALSE,FALSE,0) .") OR (upper(filialen.fil_ortsteil) ". awisLIKEoderIST(strtoupper($Params[2]),TRUE,FALSE,FALSE,0) ."))";
		}
		else
		{
			$SQL.="WHERE ((upper(filialen.fil_ort) ". awisLIKEoderIST(strtoupper($Params[2]),TRUE,FALSE,FALSE,0) .") OR (upper(filialen.fil_ortsteil) ". awisLIKEoderIST(strtoupper($Params[2]),TRUE,FALSE,FALSE,0) ."))";
			$Zusatz=TRUE;
		}
	}
}

If($Params[3]!='')
{	
	if($Zusatz)
	{
		$SQL.=" AND (schluesselnummern.sln_rechnungsnummer ". awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) .")";
	}
	else
	{
		$SQL.="WHERE (schluesselnummern.sln_rechnungsnummer ". awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) .")";
		$Zusatz=TRUE;
	}
}

If($Params[4]!=-1)
{	
	$ParaSLG=explode(',',$Params[4]);
	
	if($ParaSLG[1]=='')
	{
		if($Zusatz)
		{
			$SQL.=" AND (schluesselnummern.sln_slg_key ". awisLIKEoderIST($ParaSLG[0],TRUE,FALSE,FALSE,0) .")";
		}
		else
		{
			$SQL.="WHERE (schluesselnummern.sln_slg_key ". awisLIKEoderIST($ParaSLG[0],TRUE,FALSE,FALSE,0) .")";
			$Zusatz=TRUE;
		}
	}
	else
	{
		if($Zusatz)
		{
			$SQL.=" AND (schluesselnummern.sln_slg_key ". awisLIKEoderIST($ParaSLG[0],TRUE,FALSE,FALSE,0) .") AND (schluesselnummern.sln_slu_key ". awisLIKEoderIST($ParaSLG[1],TRUE,FALSE,FALSE,0) .")";
		}
		else
		{
			$SQL.="WHERE (schluesselnummern.sln_slg_key ". awisLIKEoderIST($ParaSLG[0],TRUE,FALSE,FALSE,0) .") AND (schluesselnummern.sln_slu_key ". awisLIKEoderIST($ParaSLG[1],TRUE,FALSE,FALSE,0) .")";
			$Zusatz=TRUE;
		}
	}
}

If($Params[5]!=-1)
{	
	if($Zusatz)
	{
		$SQL.=" AND (schluesselvergaben.slV_sls_key ". awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0) .")";
	}
	else
	{
		$SQL.="WHERE (schluesselvergaben.slv_sls_key ". awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0) .")";
		$Zusatz=TRUE;
	}
}

If($Params[6]!=-1)
{	
	if($Zusatz)
	{
		$SQL.=" AND (schluesselnummern.sln_sll_key ". awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0) .")";
	}
	else
	{
		$SQL.="WHERE (schluesselnummern.sln_sll_key ". awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0) .")";
		$Zusatz=TRUE;
	}
}


$SQL.=" ORDER BY schluesselnummern.sln_fil_id ASC, schluesselgruppen.slg_bezeichnung ASC, schluesselnummern.sln_schluesselnummer ASC, schluesselnummern.sln_schluessellfdnr ASC";

if($Params[0]!='' && $Params[1]=='' && $Params[2]=='')
{	
	$SQL="SELECT schluesselnummern.sln_schluesselnummer, schluesselnummern.sln_schluessellfdnr, schluesselnummern.sln_fil_id, ";  
	$SQL.="schluesselnummern.sln_key, schluesselvergaben.slv_key,DECODE(schluesselvergaben.slv_per_nr,'-1','Nicht zugeordnet',schluesselvergaben.slv_per_nr) AS PER_NR, v_personal_komplett_map.per_nachname, v_personal_komplett_map.per_vorname, ";
	$SQL.="schluesselnummern.sln_sicherkartennummer, schluesselgruppen.slg_bezeichnung ";
	$SQL.="FROM schluesselnummern LEFT OUTER JOIN schluesselvergaben on (schluesselnummern.sln_key = schluesselvergaben.slv_sln_key) ";
	$SQL.="LEFT OUTER JOIN v_personal_komplett_map on (v_personal_komplett_map.per_nr = schluesselvergaben.slv_per_nr) ";
	$SQL.="LEFT OUTER JOIN schluesselgruppen on (schluesselgruppen.slg_key = schluesselnummern.sln_slg_key) ";
	$SQL.="WHERE (upper(schluesselnummern.sln_schluesselnummer) ".awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0).")";
	$SQL.=" OR (upper(schluesselnummern.sln_sicherkartennummer) ".awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0).")";
	$SQL.=" ORDER BY schluesselnummern.sln_fil_id ASC, schluesselgruppen.slg_bezeichnung ASC, schluesselnummern.sln_schluesselnummer ASC, schluesselnummern.sln_schluessellfdnr ASC";
}

awis_Debug('entwick',0,false,$SQL);

$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
			
$rsSchluesselsuche = awisOpenRecordset($con,$SQL);
$rsSchluesselsucheZeilen = $awisRSZeilen;

if($rsSchluesselsucheZeilen==0)
{
	echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
	awisLogoff($con);
	die();
}

// Überschrift aufbauen
echo "<table  width=100% id=DatenTabelle border=1><tr>";					
echo "<td id=FeldBez>Schl&uuml;ssel-Gruppe</td>";
echo "<td id=FeldBez>Schl&uuml;ssel-Nr</td>";
echo "<td id=FeldBez>Schl&uuml;ssel-LfdNr</td>";
echo "<td id=FeldBez>Sicherungskarten-Nr</td>";
echo "<td id=FeldBez>Filiale</td>";
echo "<td id=FeldBez>Personal-Nr</td>";
echo "<td id=FeldBez>Personal-Name</td>";
echo '</tr>';
	
for($i=0;$i<$rsSchluesselsucheZeilen;$i++)
{	
	if($i<$AwisBenuPara)
	{
		echo '<tr>';
		
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselsuche['SLG_BEZEICHNUNG'][$i] . '</td>';
		if($rsSchluesselsuche['SLN_KEY'][$i]!='')
		{
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title="Schl&uuml;sselnummern-Detail anzeigen" href=./schluessel_Main.php?cmdAktion=Nummern&txtSLN_KEY=' . $rsSchluesselsuche['SLN_KEY'][$i] . '&txtFIL_ID='.$rsSchluesselsuche['SLN_FIL_ID'][$i].'>' . $rsSchluesselsuche['SLN_SCHLUESSELNUMMER'][$i] . '</a></td>';
		}
		else
		{
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
		}
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselsuche['SLN_SCHLUESSELLFDNR'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselsuche['SLN_SICHERKARTENNUMMER'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselsuche['SLN_FIL_ID'][$i] . '</td>';
	
		if($rsSchluesselsuche['SLV_KEY'][$i]!='')
		{
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title="Schl&uuml;sselvergabe-Detail anzeigen" href=./schluessel_Main.php?cmdAktion=Vergaben&txtSLV_KEY=' . $rsSchluesselsuche['SLV_KEY'][$i] . '&txtFIL_ID='.$rsSchluesselsuche['SLN_FIL_ID'][$i].'>' . $rsSchluesselsuche['PER_NR'][$i] . '</a></td>';
		}
		else
		{
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>::&nbsp;Zuordnen&nbsp;::</td>';
		}
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselsuche['PER_NACHNAME'][$i] . ' ' . $rsSchluesselsuche['PER_VORNAME'][$i] . '</td>';
		echo '</tr>';
	}
}
print "</table>";

if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
{
	echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
}
else
{
	echo '<font size=2>Es wurden ' . $i . ' Datens&auml;tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datensätze eingeschränkt!';
}
	
?>
</body>
</html>
