<?php
global $CRM;
global $AWIS_KEY1;
require_once 'awisFileReader.php';
try {

    $Datei = new awisFileReader($_FILES['CRM_UPLOAD']['tmp_name']);

    $Gebiete = [];
    $Kontakte = [];
    $FalscheVertragsnummern = [];
    $AnzGeaenderteDS = 0;

    while (!$Datei->EOF()) {
        $CAD_KEY = '';

        if ($Datei->FeldInhalt('CAD_KEY') != '') {
            $CAD_KEY = $Datei->FeldInhalt('CAD_KEY');
        } else {
            $sql = "select CAQ_CAD_KEY ";
            $sql .= "from CRMAKQUISESTAND ";
            $sql .= "where CAQ_VERTRAGSNUMMER = " . $CRM->DB->WertSetzen('CAQ', 'N0', $Datei->FeldInhalt('CAQ_VERTRAGSNUMMER'));

            $rsCAQ = $CRM->DB->RecordSetOeffnen($sql, $CRM->DB->Bindevariablen('CAQ', true));

            if ($rsCAQ->AnzahlDatensaetze() == 1) {
                $CAD_KEY = $rsCAQ->FeldInhalt('CAQ_CAD_KEY');
            } else {
                $FalscheVertragsnummern[] = $Datei->FeldInhalt('CAQ_VERTRAGSNUMMER');
            }
        }

        if ($Datei->FeldInhalt('CGB_KEY') != '' AND $CAD_KEY != '' AND $Datei->FeldInhalt('KON_KEY') != '') {
            $AnzGeaenderteDS++;
            $sql = "update CRMADRESSEN ";
            $sql .= "set ";
            $sql .= "  CAD_CGB_KEY = " . $CRM->DB->WertSetzen('CAD', 'N0', $Datei->FeldInhalt('CGB_KEY'));
            $sql .= ", CAD_KON_KEY = " . $CRM->DB->WertSetzen('CAD', 'N0', $Datei->FeldInhalt('KON_KEY'));
            $sql .= " where CAD_KEY = " . $CRM->DB->WertSetzen('CAD', 'N0', $CAD_KEY);

            $CRM->DB->Ausfuehren($sql, '', true, $CRM->DB->Bindevariablen('CAD', true));
        }

        $Datei->DSWeiter();
    }

    if ($AnzGeaenderteDS == 0) {
        $Meldung = $CRM->AWISSprachKonserven['CRM']['CRM_SPEICHERN_NOK'];
        $HinweisFarbe = awisFormular::HINWEISTEXT_WARNUNG;
    } elseif (count($FalscheVertragsnummern) > 0) {
        $Meldung = $CRM->AWISSprachKonserven['CRM']['CRM_VERTRAGSNUMMERN_NOK'] . ' ' . implode(', ', $FalscheVertragsnummern);
        $HinweisFarbe = awisFormular::HINWEISTEXT_WARNUNG;
    } else {
        $Meldung = $CRM->AWISSprachKonserven['CRM']['CRM_SPEICHERN_OK'];
        $HinweisFarbe = awisFormular::HINWEISTEXT_OK;
    }

    $CRM->Form->Hinweistext($Meldung, $HinweisFarbe);
} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    $Information .= var_export($_REQUEST);
    $CRM->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $CRM->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    $Information .= var_export($_REQUEST);
    $CRM->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>