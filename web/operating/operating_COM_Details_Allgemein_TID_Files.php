<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $comcon;
global $AWISBenutzer;
global $awisDBFehler;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Allgemein');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Allgemein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Allgemein'].'</title>';
?>
</head>
<body>
<?php

$Recht3111 = awisBenutzerRecht($con,3111);
if($Recht3111==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$TID='';

if (isset($_GET['TID']))
{
	$TID=$_GET['TID'];
}

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Allgemein_TID_Files action=./operating_COM_Details_Allgemein_TID_Files.php method=POST>';

awis_FORM_FormularStart();

echo "<p><center><h2><b>COM - TRANSAKTIONSID - Dateien</b></h2></center><p>";

If(($Recht3111&2 )==2 ){
	
	if ($TID!=='')
	{
		echo "<h3><b>TRANSAKTIONSID: ".$TID."</b><h3><p>";
		
		$BindeVariablen=array();
		$BindeVariablen['var_transaktionsid']=$TID;
		
		$SQL='SELECT * FROM EXPERIAN_ATU.V_IMPORTDATEIEN_INFO WHERE TRANSAKTIONSID=:var_transaktionsid';	
			
		if(!isset($_GET['Sort']))
		{
			$SQL .= ' ORDER BY IMD_FIL_ID';
			$SORT_LINK='';
		}
		else
		{
			$SORT_LINK='&Sort='.$_GET['Sort'];
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
		}
				
		$rsIMI=awisOpenRecordset($comcon, $SQL, true, false, $BindeVariablen);
		$rsIMIZeilen=$awisRSZeilen;
		
		if ($rsIMIZeilen>0)
		{
			echo "<h4 style='margin: 0px;'><b>Datei�bersicht:</b></h4>";
			awis_FORM_FormularStart('font-size: 80%;');
			
			
			$WEB_LINK='./operating_COM_Details_Allgemein_TID_Files.php?TID='.$TID;
			
			awis_FORM_ZeileStart();
				$Link = $WEB_LINK.'&Sort=IMD_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='IMD_NAME'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei',260,'',$Link);
				$Link = $WEB_LINK.'&Sort=IMD_DATEI_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='IMD_DATEI_DATUM'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei-Datum',120,'',$Link);
				$Link = $WEB_LINK.'&Sort=IMD_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='IMD_FIL_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Filiale',41,'',$Link);
				$Link = $WEB_LINK.'&Sort=IMD_FILESIZE_BYTES'.((isset($_GET['Sort']) AND ($_GET['Sort']=='IMD_FILESIZE_BYTES'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Gr��e (Bytes)',92,'',$Link);
				$Link = $WEB_LINK.'&Sort=BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEZEICHNUNG'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei-Typ',140,'',$Link);
				$Link = $WEB_LINK.'&Sort=DATEI_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='DATEI_STATUS'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei-Status',90,'',$Link);
				$Link = $WEB_LINK.'&Sort=TSCREATE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TSCREATE'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei-Verarbeitet',120,'',$Link);
				$Link = $WEB_LINK.'&Sort=TSLOAD'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TSLOAD'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Datei-Abgeholt',120,'',$Link);
				$Link = $WEB_LINK.'&Sort=ARBEITSSTATUS_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ARBEITSSTATUS_BEZ'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift('Arbeitsstatus',160,'',$Link);
			awis_FORM_ZeileEnde();
			
			for($IMIZeile=0;$IMIZeile<$rsIMIZeilen;$IMIZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['IMD_NAME'][$IMIZeile],'',260,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['IMD_DATEI_DATUM'][$IMIZeile],'',120,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['IMD_FIL_ID'][$IMIZeile],'',41,'',($IMIZeile%2),'','','Z','R');
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['IMD_FILESIZE_BYTES'][$IMIZeile],'',92,'',($IMIZeile%2),'','','Z','R');
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['BEZEICHNUNG'][$IMIZeile],'',140,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['DATEI_STATUS'][$IMIZeile],'',90,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['TSCREATE'][$IMIZeile],'',120,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['TSLOAD'][$IMIZeile],'',120,'',($IMIZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsIMI['ARBEITSSTATUS_BEZ'][$IMIZeile],'',160,'',($IMIZeile%2));				
				awis_FORM_ZeileEnde();
			}
			
			awis_FORM_FormularEnde();
			
		}else 
		{
			awis_FORM_Hinweistext('Es wurden keinen Dateien mit dieser TRANSAKTIONSID verarbeitet.');
		}

		awis_FORM_Trennzeile('P');
		
		echo "<input type=button value='Schlie�en' style='margin: 20px 20px;' onclick=\"javascript:self.close();\">";
		echo "<input type=hidden name='TID' value='".$TID."'>";
		
	}	
}
else {
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
}

awis_FORM_FormularEnde();

echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

