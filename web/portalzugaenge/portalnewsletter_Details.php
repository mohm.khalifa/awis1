<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awis_portalzugaenge.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PNL','%');
	$TextKonserven[]=array('PNT','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_Vorschau');
	$TextKonserven[]=array('Wort','lbl_NewsLetterImport');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','PNL_STATUS');
	$TextKonserven[]=array('Wort','lbl_Versand%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PortalNewsletter'));
	$AWIS_KEY1=0;

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht2102 = $AWISBenutzer->HatDasRecht(2102);
	if($Recht2102==0)
	{
	    awisEreignis(3,1000,'PNL',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$Portal = new awis_Portalzugaenge();

	$DetailAnsicht=false;
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['PNL_DATUM'] = $_POST['sucPNL_DATUM'];

		$Param['ORDERBY']='PNL_DESC';
		$Param['KEY']='';
		$Param['BLOCK']=1;

	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./portalnewsletter_speichern.php');
	}
	elseif(isset($_POST['cmdMail_x']))
	{
		//include('./portalnewsletter_senden.php');
        $SQL = 'UPDATE portalnewsletter';
        $SQL .= ' SET pnl_status = 5';
        $SQL .= ' WHERE pnl_key = '.$DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
        $DB->Ausfuehren($SQL);
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
	}
	elseif(isset($_POST['cmdMailStoppen_x']))
	{
        $SQL = 'UPDATE portalnewsletter';
        $SQL .= ' SET pnl_status = 1';
        $SQL .= ' WHERE pnl_key = '.$DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
        $DB->Ausfuehren($SQL);
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
	}
	elseif(isset($_POST['cmdMailStoppen_x']))
	{
        $SQL = 'UPDATE portalnewsletter';
        $SQL .= ' SET pnl_status = 1';
        $SQL .= ' WHERE pnl_key = '.$DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
        $DB->Ausfuehren($SQL);
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
	}
	elseif(isset($_POST['cmdImport_x']))
	{
        require_once 'awis_portalnewsletter.inc';
		$Newsletter = new awis_PortalNewsletter($AWISBenutzer->BenutzerName());
        $Newsletter->ImportiereAktuelleInfos($Param['KEY']);
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
	}
	elseif(isset($_POST['cmdTestMail_x']))
	{
        $SQL = 'UPDATE portalnewsletter';
        $SQL .= ' SET pnl_status = 3';
        $SQL .= ' WHERE pnl_key = '.$DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
        $DB->Ausfuehren($SQL);
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPNL_KEY'],false);
	}
	elseif(isset($_GET['PNL_KEY']))
	{
		$AWIS_KEY1=$DB->FeldInhaltFormat('N0',$_GET['PNL_KEY']);
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}

	//********************************************************
	// Daten suchen
	//********************************************************

	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY PNL_DATUM DESC';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	$Param['ORDER']=$ORDERBY;
    
	$Bedingung = _BedingungErstellen($Param,$Recht2102);

	$SQL = 'SELECT portalnewsletter.*, PLT_BEREICHE';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM portalnewsletter';
	$SQL .= ' INNER JOIN portalnewsletterTypen ON PLT_KEY = PNL_PLT_KEY';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$Param, $SQL);

	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

    $SQL .= $ORDERBY;

	$rsPNL = $DB->RecordsetOeffnen($SQL);
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmportalnewsletter action=./portalnewsletter_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST>';

	if($AWIS_KEY1==0 AND (($rsPNL->EOF() AND !isset($_POST['cmdDSNeu_x']))))
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
    elseif($rsPNL->AnzahlDatensaetze()>1 OR isset($_GET['Liste']))
    {
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './portalnewsletter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PNL_BETREFF'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PNL_BETREFF'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PNL']['PNL_BETREFF'],250,'',$Link);
		$Link = './portalnewsletter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PNL_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PNL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PNL']['PNL_DATUM'],120,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPNL->EOF())
		{
			$Form->ZeileStart();
			$Link = './portalnewsletter_Main.php?cmdAktion=Details&PNL_KEY=0'.$rsPNL->FeldInhalt('PNL_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

			$Text = (strlen($rsPNL->FeldInhalt('PNL_BETREFF'))>(int)(250/$ListenSchriftFaktor)?substr($rsPNL->FeldInhalt('PNL_BETREFF'),0,(int)(250/$ListenSchriftFaktor)-2).'..':$rsPNL->FeldInhalt('PNL_BETREFF'));
			$Form->Erstelle_ListenFeld('PNL_BETREFF',$Text,0,250,false,($DS%2),'',$Link,'T','L',$rsPNL->FeldInhalt('PNL_BETREFF'));

			$Text = (strlen($rsPNL->FeldInhalt('PNL_DATUM'))>(int)(120/$ListenSchriftFaktor)?substr($rsPNL->FeldInhalt('PNL_DATUM'),0,(int)(250/$ListenSchriftFaktor)-2).'..':$rsPNL->FeldInhalt('PNL_DATUM'));
			$Form->Erstelle_ListenFeld('PNL_DATUM',$Text,0,120,false,($DS%2),'','','','',$rsPNL->FeldInhalt('PNL_DATUM'));

			$Form->ZeileEnde();

			$rsPNL->DSWeiter();
			$DS++;
		}

		$Link = './portalnewsletter_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
    }
	else
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsPNL->FeldInhalt('PNL_KEY');


        $EditRecht = false;
        if($rsPNL->FeldInhalt('PNL_STATUS')<5)
        {
            $EditRecht = (($Recht2102&6)>0);
        }

		$Param['KEY']=$AWIS_KEY1;

		echo '<input type=hidden name=txtPNL_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./portalnewsletter_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPNL->FeldInhalt('PNL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPNL->FeldInhalt('PNL_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_DATUM'].':',150);
		$Form->Erstelle_TextFeld('PNL_DATUM',$rsPNL->FeldInhalt('PNL_DATUM'),10,300,$EditRecht,'','','','D');
		$AWISCursorPosition = 'txtPNL_DATUM';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_BETREFF'].':',150);
		$Form->Erstelle_TextFeld('PNL_BETREFF',$rsPNL->FeldInhalt('PNL_BETREFF'),50,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_PLT_KEY'].':',150);
        $SQL = 'SELECT PLT_KEY, PLT_TYP';
        $SQL .= ' FROM PortalNewsletterTypen';
        $SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('PNL_PLT_KEY', $rsPNL->FeldInhalt('PNL_PLT_KEY'), 200, $EditRecht, $SQL);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_EINLEITUNGSTEXT'].':',150);
		$Form->Erstelle_TextArea('PNL_EINLEITUNGSTEXT',$rsPNL->FeldInhalt('PNL_EINLEITUNGSTEXT'),800,100,5,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_EINLEITUNGBILD'].':',150);
		$Form->Erstelle_TextFeld('PNL_EINLEITUNGBILD',$rsPNL->FeldInhalt('PNL_EINLEITUNGBILD'),100,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_BEMERKUNGINTERN'].':',150);
		$Form->Erstelle_TextArea('PNL_BEMERKUNGINTERN',$rsPNL->FeldInhalt('PNL_BEMERKUNGINTERN'),800,100,5,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PNL']['PNL_STATUS'].':',150);
        $Daten = explode('|',$AWISSprachKonserven['Liste']['PNL_STATUS']);
		$Form->Erstelle_SelectFeld('PNL_STATUS', $rsPNL->FeldInhalt('PNL_STATUS'), 200, false,'','','','','',$Daten);
		$Form->ZeileEnde();




        if($rsPNL->FeldInhalt('PNL_KEY')!='')
        {
            $Bereiche = explode(',',$rsPNL->FeldInhalt('PLT_BEREICHE'));
            foreach($Bereiche as $Bereich)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($Bereich, 1000, 'Ueberschrift');
                $Form->ZeileEnde();

                $SQL = 'SELECT *';
                $SQL .= ' FROM PortalNewsletterTexte';
                $SQL .= ' WHERE PNT_BEREICH = '.$DB->FeldInhaltFormat('T',$Bereich);
                $SQL .= ' AND PNT_PNL_KEY = '.$AWIS_KEY1;
                $SQL .= ' ORDER BY PNT_BEREICH, PNT_KEY';

                $rsPNT = $DB->RecordSetOeffnen($SQL);

                while(!$rsPNT->EOF())
                {
                    $Form->Erstelle_HiddenFeld('PNT_KEY_'.$rsPNT->FeldInhalt('PNT_KEY'), $rsPNT->FeldInhalt('PNT_KEY'));
                    $Form->Erstelle_HiddenFeld('PNT_BEREICH_'.$rsPNT->FeldInhalt('PNT_KEY'), $rsPNT->FeldInhalt('PNT_BEREICH'));

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['PNT']['PNT_VERSENDEN'].':',150);
                    $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                    $Form->Erstelle_SelectFeld('PNT_VERSENDEN_'.$rsPNT->FeldInhalt('PNT_KEY'),$rsPNT->FeldInhalt('PNT_VERSENDEN'),200,$EditRecht,'','','','','',$Daten);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['PNT']['PNT_IMPORTID'].':',150);
                    $Link = 'https://atu-portal.de/index.php/login.html';
                    switch($Bereich)
                    {
                        case 'Beitrag':
                            $ZielLink = 'index.php?news/'.$rsPNT->FeldInhalt('PNT_IMPORTID').'.html';
                            break;
                        case 'Forum':
                            $ZielLink = 'index.php?forum/topic.html?id='.$rsPNT->FeldInhalt('PNT_IMPORTID');
                            break;
                        case 'Frage':
                            $ZielLink = 'index.php?component/dfcontact/';
                            break;
                        default:
                            $ZielLink = 'index.php';
                            break;
                    }
                    $Link .= '?return='.base64_encode($ZielLink);
                    $Form->Erstelle_TextLabel($rsPNT->FeldInhalt('PNT_IMPORTID').' / '.$rsPNT->FeldInhalt('PNT_KEY'),100,'',$Link);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['PNT']['PNT_UEBERSCHRIFT'].':',150);
                    $Form->Erstelle_TextFeld('PNT_UEBERSCHRIFT_'.$rsPNT->FeldInhalt('PNT_KEY'),$rsPNT->FeldInhalt('PNT_UEBERSCHRIFT'),100,700,$EditRecht,'','','','T');
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['PNT']['PNT_TEXT'].':',150);
                    $Form->Erstelle_TextArea('PNT_TEXT_'.$rsPNT->FeldInhalt('PNT_KEY'),$rsPNT->FeldInhalt('PNT_TEXT'),800,100,5,$EditRecht,'','','','T');
                    $Form->ZeileEnde();

                    $XML = simplexml_load_string($rsPNT->FeldInhalt('PNT_INFOS'));
                    foreach($XML->Children() AS $Eintrag=>$Wert)
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($Eintrag.':',150);
                        $Form->Erstelle_TextFeld('INFO',$Wert,0,500,false);
                        $Form->ZeileEnde();
                    }



                    $Form->Trennzeile();
                    
                    $rsPNT->DSWeiter();
                }
            }
        }


        $Form->Formular_Ende();
	}

    $AWISBenutzer->ParameterSchreiben('Formular_PortalNewsletter', serialize($Param));
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht2102&6)!=0  AND $DetailAnsicht))
	{
        if($rsPNL->FeldInhalt('PNL_STATUS')<5)
        {
    		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
	}
	if(($Recht2102&36)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if((($Recht2102&6)!=0  AND $DetailAnsicht) AND $AWIS_KEY1 > 0)
	{
        if($rsPNL->FeldInhalt('PNL_STATUS')<5)
        {
            $Form->Schaltflaeche('image', 'cmdImport', '', '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['Wort']['lbl_NewsLetterImport'], 'I');
        }
	}

	if((($Recht2102&6)!=0  AND $DetailAnsicht) AND $AWIS_KEY1 > 0)
	{
		$Form->Schaltflaeche('href', 'cmdVorschau', './portalnewsletter_vorschau.php?PNL_KEY='.$AWIS_KEY1, '/bilder/cmd_lupe.png', $AWISSprachKonserven['Wort']['lbl_Vorschau'], 'V','',27,27,'Vorschau');
	}

	if(($Recht2102&36)!=0 AND $DetailAnsicht)
	{
        switch($rsPNL->FeldInhalt('PNL_STATUS'))
        {
            case 1:     // Bearbeitung
            case 3:     // Testmail aktiv
                $Form->Schaltflaeche('image', 'cmdMail', '', '/bilder/cmd_mail.png', $AWISSprachKonserven['Wort']['lbl_VersandStarten'], '');
                $Form->Schaltflaeche('image', 'cmdTestMail', '', '/bilder/cmd_stern.png', $AWISSprachKonserven['Wort']['lbl_VersandTesten'], '');
                break;
            case 5:
                $Form->Schaltflaeche('image', 'cmdMailStoppen', '', '/bilder/cmd_player_stop.png', $AWISSprachKonserven['Wort']['lbl_VersandStoppen'], '');
                break;
        }
		
	}

	/*
	if((($Recht2102&8)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}
	*/

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151350");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151351");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param,$Recht2102)
{
	global $AWIS_KEY1;
	global $DB;
	global $AWISBenutzer;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PNL_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['PNL_DATUM']) AND $Param['PNL_DATUM']!='')
	{
		$Bedingung .= ' AND (PNL_DATUM =' . $DB->FeldInhaltFormat('D',$Param['PNL_DATUM']).')';
	}

	return $Bedingung;
}
?>