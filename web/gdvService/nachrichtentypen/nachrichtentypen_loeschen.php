<?php
/**
 * Löschen Stammdatenpflege GDV-Nachrichtentypen
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Tabelle= '';

if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedr�ckt
{
		$Tabelle = 'VNT';
		$Felder = explode(';',$FelderDel);
		$Feld = explode('_',$Felder[1]);
		$Key=$Feld[2];

		$SQL =' select GDN_GDR_KEY,VNT_KEY,GDN_KEY,VNT_GDN_NUMMER,GDN_BEZEICHNUNG,VVE_BEZEICHNUNG,VVE_VERSNR from versnachrichtentypen ';
		$SQL .=' inner join VERSVERSANDWEG on vnt_versnr = vvw_versnr';
		$SQL .=' inner join VERSVERSICHERUNGEN on VNT_VERSNR = VVE_VERSNR';
		$SQL .=' inner join gdvrelease on vvw_release = gdr_release';
		$SQL .=' inner join gdvnachrichtentypen on VNT_GDN_NUMMER = GDN_NUMMER and GDR_KEY = GDN_GDR_KEY';
	    $SQL .= ' where VNT_KEY =' .$Key;

		$rsDaten = $DB->RecordSetOeffnen($SQL);

		/**
		 * Werte f�r Frage 'Wirklich l�schen?' holen
		**/
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','GDN_NNUMMER'),$rsDaten->FeldInhalt('VNT_GDN_NUMMER'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','GDN_BEZEICHNUNG'),$rsDaten->FeldInhalt('GDN_BEZEICHNUNG'));

}
if(isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'VNT2';
	$Key=$_POST['txtAWIS_KEY1'];

	$SQL =' select * from versversicherungen ';
	$SQL .= ' where VVE_VERSNR =' .$Key;

	$rsDaten = $DB->RecordSetOeffnen($SQL);

	/**
	 * Werte f�r Frage 'Wirklich l�schen?' holen
	 **/
	$Felder=array();
	$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'VNT':

			$SQL = 'DELETE FROM VERSNACHRICHTENTYPEN WHERE VNT_KEY='.$_POST['txtKEY']; // Sachbearbeiter Zuord l�schen
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		$AWIS_KEY1 = $_POST['txtVERSNR'];
		break;
		case 'VNT2':

			$SQL = 'DELETE FROM VERSNACHRICHTENTYPEN WHERE VNT_VERSNR='.$_POST['txtKEY']; // Sachbearbeiter Zuord l�schen


			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
			break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':

			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./nachrichtentypen_Main.php?cmdAktion=Details  method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext('M&oumlchten sie die Daten f&uumlr folgende Versicherung wirklich l&oumlschen?');	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/

			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('NT',$Key);
			$Form->Erstelle_HiddenFeld('VERSNR',$rsDaten->FeldInhalt('VVE_VERSNR'));
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);

			$Form->Trennzeile();
				
			/**
			 * L�schen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
				
			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>