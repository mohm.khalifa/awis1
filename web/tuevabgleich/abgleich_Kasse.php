<?php
/**
 * Abgleich Kassendaten Tuev-Anwendung
 *
 * @author    Tobias Schaeffler
 * @copyright ATU Auto Teile Unger
 * @version   201610061450
 *
 */
require_once('abgleich_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TAG','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_zurueck');    
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht23010 = $AWISBenutzer->HatDasRecht(23010);
	if($Recht23010 == 0) {
	    $Form->Fehler_KeineRechte();
	}
    
    $AWIS_KEYs1 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TKD_KEY)
    $AWIS_KEYs2 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TGD_KEY)    
    $AWIS_KEYs1[0] = (isset($_GET['TKD_KEY'])? $_GET['TKD_KEY']: 0);    // wenn �ber GET (direkter Klick vom Benutzer auf href)
    if ($AWIS_KEYs1[0] <= 0)       {                                      // wenn 0-Index leer,
        foreach ($_POST as $key => $value)                              // dann alle POST-�bergaben durchlaufen
        {
            if (strpos($key, 'txtTKD_CHK_') === 0)    {                  // wenn an 1. Position (0. Index), 'txtTKD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
                if ($AWIS_KEYs1[0] <= 0)           {                       // wenn 0. Index vom Array noch unbelegt, dann hier hin schreibe
                    $AWIS_KEYs1[0] = $value;
                } else         {                                           // ansonsten hinzuf�gen
                    $AWIS_KEYs1[] = $value;
                }
            }
        }
    }    
    
    foreach ($_POST as $key => $value)                              // alle POST-�bergaben durchlaufen
    {
        if (strpos($key, 'txtTGD_CHK_') === 0)    {                   // wenn an 1. Position (0. Index), 'txtTGD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merker
            $AWIS_KEYs2[] = $value;
        }
    }
    $TAG = new awis_abgleich_funktionen();
    $AnsichtSuche = false;
    $AnsichtAuswahl = false;
    $AnsichtZuordnung = false;
    
    $Gespeichert = false;
    
    if (isset($_POST['cmdSuche_x'])) {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdDSWeiter_x']) and $AWIS_KEYs1[0] > 0) {
        $AnsichtZuordnung = true;
    }
    elseif (isset($_POST['cmdDSZurueck_x'])) {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdSpeichern_P'])) {
        foreach ($AWIS_KEYs1 as $key => $TKDvalue)
        {
            $TAG->AbgleichSpeichernP($TKDvalue, $_POST['txtTAG_BEMERKUNG_UNTEN'], $AWISBenutzer->BenutzerName(1));
        }
        $Form->Hinweistext($AWISSprachKonserven['TAG']['DatenGespeichert'], 1);
        $AnsichtAuswahl = true;
        $Gespeichert = true;
    } else {
        if (isset($_REQUEST['txtANSICHT']))     {
            switch ($_REQUEST['txtANSICHT'])
            {
                case 'Auswahl':     $AnsichtAuswahl = true;     break;
                case 'Zuordnung':   $AnsichtZuordnung = true;   break;
                default: break;
            }
        }
        elseif ($AWIS_KEYs1[0] > 0) {
            $AnsichtZuordnung = true;
        }
        else {
            $AnsichtSuche = true;
        }
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAuswahl: ' . $AnsichtAuswahl, 'AnsichtZuordnung: ' . $AnsichtZuordnung);        
$Form->DebugAusgabe(1, $AWIS_KEYs1, $AWIS_KEYs2);
$Form->DebugAusgabe(1, $_REQUEST);    

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_TKD')) < 8)) {
        $SuchParam['DatumVon'] = '';
        $SuchParam['DatumBis'] = '';
        $SuchParam['FilId'] = '';
        $SuchParam['TuevNr'] = '';
        $SuchParam['TuevArtKey'] = 0;
        $SuchParam['Kfz'] = '';
        $SuchParam['Name'] = '';
        $SuchParam['PreisVon'] = '';
        $SuchParam['PreisBis'] = '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_TKD", serialize($SuchParam));
	}
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_TKD'));
	if ($AnsichtAuswahl) {
        $SuchParam['DatumVon'] = (isset($_POST['sucTAG_DATUMVON'])? $_POST['sucTAG_DATUMVON']: $SuchParam['DatumVon']);
        $SuchParam['DatumBis'] = (isset($_POST['sucTAG_DATUMBIS'])? $_POST['sucTAG_DATUMBIS']: $SuchParam['DatumBis']);
        $SuchParam['FilId'] = (isset($_POST['sucTAG_FILNR'])? $_POST['sucTAG_FILNR']: $SuchParam['FilId']);
        $SuchParam['TuevNr'] = (isset($_POST['sucTAG_TUEVNR'])? $_POST['sucTAG_TUEVNR']: $SuchParam['TuevNr']);
        $SuchParam['TuevArtKey'] = (isset($_POST['sucTAG_TUEVARTKEY'])? $_POST['sucTAG_TUEVARTKEY']: $SuchParam['TuevArtKey']);
        $SuchParam['Kfz'] = (isset($_POST['sucTAG_KFZKENNZ'])? $_POST['sucTAG_KFZKENNZ']: $SuchParam['Kfz']);
        $SuchParam['Name'] = (isset($_POST['sucTAG_NAME'])? $_POST['sucTAG_NAME']: $SuchParam['Name']);
        $SuchParam['PreisVon'] = (isset($_POST['sucTAG_PREISVON'])? $_POST['sucTAG_PREISVON']: $SuchParam['PreisVon']);
        $SuchParam['PreisBis'] = (isset($_POST['sucTAG_PREISBIS'])? $_POST['sucTAG_PREISBIS']: $SuchParam['PreisBis']);
        $SuchParam['SPEICHERN'] = ((isset($_POST['sucTAG_AUSWAHLSPEICHERN']) or ($Gespeichert == true)  or (isset($_POST['cmdDSZurueck_x'])))? 'on': 'off');
		$AWISBenutzer->ParameterSchreiben("Formular_TKD", serialize($SuchParam));	        
	}    
    else {
       	if ($SuchParam['SPEICHERN'] == 'off') {
            $SuchParam['DatumVon'] = '';
            $SuchParam['DatumBis'] = '';
            $SuchParam['FilId'] = '';
            $SuchParam['TuevNr'] = '';
            $SuchParam['TuevArtKey'] = 0;
            $SuchParam['Kfz'] = '';
            $SuchParam['Name'] = '';
            $SuchParam['PreisVon'] = '';
            $SuchParam['PreisBis'] = '';
            $SuchParam['SPEICHERN']= 'off';
    	    $AWISBenutzer->ParameterSchreiben("Formular_TKD", serialize($SuchParam));    		
    	}
	}        

    /**********************************************
	* Formular
	***********************************************/
    $Form->SchreibeHTMLCode('<form name=frmKasse action=./abgleich_Main.php?cmdAktion=Kasse method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
    $Form->DebugAusgabe(1, $SuchParam);

    if ($AnsichtSuche or $AnsichtAuswahl) {
        $TAG->Label_Filter($AnsichtSuche);
        $TAG->Feld_DatumVonBis($SuchParam, $AnsichtSuche);
        $TAG->Feld_SelectFilId($SuchParam, $AnsichtSuche, 2);
        $TAG->Feld_SelectTuevNr($SuchParam, $AnsichtSuche);
        $TAG->Feld_SelectTuevArt($SuchParam, $AnsichtSuche);
        $TAG->Feld_KfzKennz($SuchParam, $AnsichtSuche);
        $TAG->Feld_Name($SuchParam, $AnsichtSuche);
        $TAG->Feld_PreisVonBis($SuchParam, $AnsichtSuche);
        $TAG->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    }
    
    if ($AnsichtAuswahl or $AnsichtZuordnung) {
    	$Sort= '';
    	if(isset($_GET['TKDSort'])) {
    		$Sort = $_GET['TKDSort'];
    	}
    	elseif(isset($_REQUEST['txtTKDSort'])) {
    		$Sort = $_REQUEST['txtTKDSort'];
    	}
    	
    	$Form->Trennzeile('L');
        
		// Infozeile zusammenbauen
		$Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['TAG']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        if ($AnsichtAuswahl) {
            $SQL = 'select tkd.*, tue.tue_bez, fil.fil_bez, tua.tua_bez, ' .
              'case when tkd.tkd_duplikat = 1 ' .
              'then \'X\' else null end as duplikat, ' .
                   'row_number() OVER (';            
            if ($Sort != '') {		// wenn GET-Sort, dann nach diesen Feld sortieren
                $SQL .= ' order by ' . str_replace('~',' DESC ', $Sort);
            }
            else {		// sonst pauschal nach Datum
                $SQL .= ' order by tkd_datumzeit';
            }
            $SQL .= ') AS ZeilenNr ' . 
                    'from tuevkassendaten tkd ' .
                    'left join tuevorganisationen tue on tkd.tkd_tuevnr = tue.tue_id ' .
                    'left join filialen fil on tkd.tkd_fil_id = fil.fil_id ' .
                    'left join tuevarten tua on tua.tua_id = tkd.tkd_tuevart';                  
            $Bedingung = ' and tkd_abgeglichen = 0';
            $Bedingung .= ' and tkd_datumzeit >= add_months(sysdate,-15)';
            $Bedingung .= $TAG->ErstelleTKDBedingung($SuchParam);
            if ($Bedingung != '') {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }                       
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
            // Zum Bl�ttern in den Daten
            if (isset($_REQUEST['Block'])) {
                if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x'])) {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
            }
            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);

            $rsTKD = $DB->RecordSetOeffnen($SQL);
            $Form->DebugAusgabe(1, $SQL);
            // Spaltenbreiten f�r Listenansicht
            $FeldBreiten = array();
            $FeldBreiten['ChkBox'] = 20;
            $FeldBreiten['Betrag'] = 70;
            $FeldBreiten['TuevNr'] = 60;
            $FeldBreiten['FilNr'] = 70;
            $FeldBreiten['DatumZeit'] = 130;
            $FeldBreiten['Kfz'] = 140;
            $FeldBreiten['Name'] = 250;
            $FeldBreiten['TuevArt'] = 230;
            $FeldBreiten['Nachverfolgung']=20;
            $Gesamtbreite = 0;
            if($rsTKD->AnzahlDatensaetze() > 12) {
                $FeldBreiten['Balken'] = 17;
            } else {
                $Gesamtbreite += 2;
            }
            foreach ($FeldBreiten as $value)
            {
                $Gesamtbreite += $value + 2.5;
            }
            
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-5);
            // �berschrift der Listenansicht mit Sortierungslink: Kassendatum
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_DATUMZEIT'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_DATUMZEIT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Datum'], $FeldBreiten['DatumZeit'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_KFZKENNZ'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_KFZKENNZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['KfzKennz'], $FeldBreiten['Kfz'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Name
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_NAME'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_NAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Name'], $FeldBreiten['Name'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_BETRAG'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Preis'], $FeldBreiten['Betrag'], '', $Link);            
            // �berschrift der Listenansicht mit Sortierungslink: T�V-Art
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_TUEVART'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_TUEVART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevArt'], $FeldBreiten['TuevArt'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: FilNr
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_FIL_ID'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['FilId'], $FeldBreiten['FilNr'], '', $Link);
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_DUPLIKAT'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_DUPLIKAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift('D', $FeldBreiten['Nachverfolgung'], '', $Link);
            if($rsTKD->AnzahlDatensaetze() > 12) {
                $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Balken']);
            }
            $Form->ZeileEnde();

            if($rsTKD->AnzahlDatensaetze() > 12) {
                $Form->ZeileStart();
                $Form->ScrollBereichStart('scrKasse',250,943,true,false);
            }
            $DS = 0;	// f�r Hintergrundfarbumschaltung
            while(! $rsTKD->EOF())
            {
                $Form->ZeileStart();
                
                $Link = ((($Recht23010 & 2) == 2)? './abgleich_Main.php?cmdAktion=Kasse&TKD_KEY=0'.$rsTKD->FeldInhalt('TKD_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:''): '');
                $Form->Erstelle_Checkbox('TKD_CHK_' . $rsTKD->FeldInhalt('TKD_KEY'),'',$FeldBreiten['ChkBox'], true,$rsTKD->FeldInhalt('TKD_KEY'));                
                $TTT = $Form->Format('DU', $rsTKD->FeldInhalt('TKD_DATUMZEIT'));
                $Form->Erstelle_ListenFeld('TKD_DATUMZEIT', $Form->Format('D', $rsTKD->FeldInhalt('TKD_DATUMZEIT')), 0, $FeldBreiten['DatumZeit'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTKD->FeldInhalt('TKD_KFZKENNZ') . ' (' . $rsTKD->FeldInhalt('TKD_NAME') . ')';
                $Form->Erstelle_ListenFeld('TKD_KFZKENNZ', $rsTKD->FeldInhalt('TKD_KFZKENNZ'), 0, $FeldBreiten['Kfz'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTKD->FeldInhalt('TKD_NAME') . ' (' . $rsTKD->FeldInhalt('TKD_KFZKENNZ') . ')';
                $Form->Erstelle_ListenFeld('TKD_NAME', $rsTKD->FeldInhalt('TKD_NAME'), 0, $FeldBreiten['Name'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $Form->Format('N2', $rsTKD->FeldInhalt('TKD_BETRAG'));
                $Form->Erstelle_ListenFeld('TKD_BETRAG', $Form->Format('N2', $rsTKD->FeldInhalt('TKD_BETRAG')), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTKD->FeldInhalt('TUA_BEZ') . ' (' . $rsTKD->FeldInhalt('TUA_BEZ') . ')';
                $Form->Erstelle_ListenFeld('TKD_TUEVART', $rsTKD->FeldInhalt('TUA_BEZ'), 0, $FeldBreiten['TuevArt']-5, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTKD->FeldInhalt('TKD_FIL_ID') . ' - ' . $rsTKD->FeldInhalt('FIL_BEZ');
                $Form->Erstelle_ListenFeld('TKD_FIL_ID', $rsTKD->FeldInhalt('TKD_FIL_ID'), 0, $FeldBreiten['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTKD->FeldInhalt('DUPLIKAT');
                $Form->Erstelle_ListenFeld('TKD_DUPLIKAT',$rsTKD->FeldInhalt('DUPLIKAT'), 0, $FeldBreiten['Nachverfolgung'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                $Form->ZeileEnde();
                $DS++;
                $rsTKD->DSWeiter();                
            }
            $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            if($rsTKD->AnzahlDatensaetze() > 12) {
                $Form->ScrollBereichEnde();
                $Form->ZeileEnde();
            }
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['TAG']['SummeGesamtDS'].':', $Gesamtbreite-289, 'font-weight:bolder;');
            $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsTKD->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0', $DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), 200, 'font-weight:bolder;','','','R');
            $Form->ZeileEnde();            
                      
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl'.'&TKDSort='.$Sort;        // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
            $Form->Erstelle_HiddenFeld('ANSICHT', 'Auswahl');   
            $Form->Erstelle_HiddenFeld('TKDSort', $Sort);                    // f�r direkte Auswahl der Seite
        }
        elseif ($AnsichtZuordnung) {
            $SQL =  'SELECT tkd.*, tue.tue_bez, fil.fil_bez, tua.tua_bez ' .
                    'FROM tuevkassendaten tkd ' .
                    'LEFT JOIN tuevorganisationen tue ON tkd.tkd_tuevnr = tue.tue_id ' .
                    'LEFT JOIN filialen fil ON tkd.tkd_fil_id = fil.fil_id ' .
                    'LEFT JOIN tuevarten tua ON tua.tua_id = tkd.tkd_tuevart ' .
                    'WHERE tkd_abgeglichen = 0 ';
            
            if 	(count($AWIS_KEYs1)==1) {
				$DB->SetzeBindevariable('TKD', 'var_N0_tkd_key', $AWIS_KEYs1[0], awisDatenbank::VAR_TYP_GANZEZAHL );
				$SQL.='and tkd.tkd_key = :var_N0_tkd_key';
			}
			else {
				$SQL.='and tkd.tkd_key in (';
	            $i=0;
				foreach ($AWIS_KEYs1 as $key => $value)
	            {
	            	++$i;
	            	$DB->SetzeBindevariable('TKD', 'var_N0_tkd_key_'.$i, $value, awisDatenbank::VAR_TYP_GANZEZAHL );
	            	$SQL .=':var_N0_tkd_key_'.$i.', ';                
	            }           
	            
	            $SQL = substr($SQL, 0, -2);
	            $SQL .= ')';
			}
            $Form->DebugAusgabe(1, $SQL);        
            
            $rsTKD = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TKD'));
            
            $TAG->Label_Abgleich($rsTKD->AnzahlDatensaetze());
            
            while(! $rsTKD->EOF())
            {
                $TAG->Felder_AbgleichTKD($rsTKD);
                $rsTKD->DSWeiter();
                if (! $rsTKD->EOF()) {
                    $Form->Trennzeile('O');
                }
            }
            foreach ($AWIS_KEYs1 as $key => $value)
            {
                $Form->Erstelle_HiddenFeld('TKD_CHK_' . $value, $value);    // AWIS_KEYs1 f�r n�chste Seite merken
            }            
            
            $Form->Trennzeile('L');
            $TAG->Feld_Bemerkung();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);
            $Form->Schaltflaeche('submit', 'cmdSpeichern_P', '', '', $AWISSprachKonserven['TAG']['MAPapierrechnung'], '');
            $Form->ZeileEnde();
        }
    }
    
	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht23010&1) == 1)
    and ($AnsichtSuche)) {
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}    
    
	if ((($Recht23010&2) == 2)
    and ($AnsichtAuswahl)) {
        $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'N');
    }

    if ((($Recht23010&2) == 2)
    and ($AnsichtZuordnung)) {
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'N');
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular) {
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	} else {
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular) {
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	} else {
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>