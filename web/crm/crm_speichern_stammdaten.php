<?php
//*****************************************************************************************
//*****************************************************************************************
//* Stammdaten speichern
//*
//* Aufgeteilt, damit die Dateien nicht so gross sind!
//*****************************************************************************************
//*****************************************************************************************
require_once('awisDokumente.php');
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('CAD','CAD_%');


$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


$Felder = awis_NameInArray($_POST, 'txtCKA_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCKA_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CKA','CKA_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCKA_KEY']=='0')
	{
		if($_POST['txtCKA_BEZEICHNUNG']!='')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMKontaktArten';
			$SQL .= '(CKA_BEZEICHNUNG';
			$SQL .= ',CKA_USER, CKA_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCKA_BEZEICHNUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMKontaktArten WHERE CKA_key=' . $_POST['txtCKA_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CKA_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMKontaktArten SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CKA_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CKA_userdat=sysdate';
			$SQL .= ' WHERE CKA_key=0' . $_POST['txtCKA_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}



$Felder = awis_NameInArray($_POST, 'txtCKE_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCKE_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CKE','CKE_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCKE_KEY']=='0')
	{
		if($_POST['txtCKE_BEZEICHNUNG']!='')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMKontaktErgebnisse';
			$SQL .= '(CKE_BEZEICHNUNG';
			$SQL .= ',CKE_USER, CKE_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCKE_BEZEICHNUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMKontaktErgebnisse WHERE CKE_key=' . $_POST['txtCKE_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CKE_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMKontaktErgebnisse SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CKE_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CKE_userdat=sysdate';
			$SQL .= ' WHERE CKE_key=0' . $_POST['txtCKE_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}



$Felder = awis_NameInArray($_POST, 'txtCGB_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCGB_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CGB','CGB_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCGB_KEY']=='0')
	{
		if($_POST['txtCGB_GEBIET']!='')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMGebiete';
			$SQL .= '(CGB_GEBIET, CGB_KENNUNG';
			$SQL .= ',CGB_USER, CGB_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCGB_GEBIET'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGB_KENNUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMGebiete WHERE CGB_key=' . $_POST['txtCGB_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CGB_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMGebiete SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CGB_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CGB_userdat=sysdate';
			$SQL .= ' WHERE CGB_key=0' . $_POST['txtCGB_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}





$Felder = awis_NameInArray($_POST, 'txtCLV_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCLV_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CLV','CLV_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	$FehlerListe = array();
	$UpdateFelder = '';

	$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMLeistungsbereichvorgaben WHERE CLV_key=' . $_POST['txtCLV_KEY'] . '');
//awis_Debug(1,$_POST,$Felder);
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
	// Alten und neuen Wert umformatieren!!
			$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
	//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		$Meldung = str_replace('%1',$rsCKT['CLV_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE CRMLeistungsbereichvorgaben SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', CLV_user=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', CLV_userdat=sysdate';
		$SQL .= ' WHERE CLV_key=0' . $_POST['txtCLV_KEY'] . '';
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
		}
	}

}

//************************************************
// Aktionentypen
//************************************************

$Felder = awis_NameInArray($_POST, 'txtCAT_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCAT_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CAT','CAT_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCAT_KEY']=='0')
	{
		if($_POST['txtCAT_BEZEICHNUNG']!='')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMAktionenTypen';
			$SQL .= '(CAT_BEZEICHNUNG, CAT_BEMERKUNG';
			$SQL .= ',CAT_USER, CAT_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCAT_BEZEICHNUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAT_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMAktionenTypen WHERE CAT_key=' . $_POST['txtCAT_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CAT_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMAktionenTypen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CAT_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CAT_userdat=sysdate';
			$SQL .= ' WHERE CAT_key=0' . $_POST['txtCAT_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}

//********************************************************************************
//* Hilfswerte zum Drucken
//********************************************************************************
awis_Debug(1,$_POST);
$Felder = awis_NameInArray($_POST,'txtXHW_',1,1);
if($Felder!='')
{
	$SQL = '';
	$Felder = explode(';',$Felder);

	$TXTFelder = array(1,2);
	$DATFelder = array(3,4);

	foreach($TXTFelder AS $Feld)
	{
		$SQL = 'UPDATE Hilfswerte';
		$SQL .= ' SET XHW_WERTTXT = '.awisFeldFormat('T',$_POST['txtXHW_'.$Feld]);
		$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = '.$Feld;

		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
		}
	}


	foreach($DATFelder AS $Feld)
	{
		$SQL = 'UPDATE Hilfswerte';
		$SQL .= ' SET XHW_WERTDAT = '.awisFeldFormat('D',$_POST['txtXHW_'.$Feld]);
		$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = '.$Feld;

		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
		}
	}

}

$Felder = awis_NameInArray($_POST, 'txtCPF_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtCPF_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CPF','CPF_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	
	if($_POST['txtCPF_KEY']=='0')
	{
		if($_POST['txtCPF_PFLICHTFELD']!='' && $_POST['txtCPF_PFLICHTFELD'] != -1)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMPflichtfelder ';
			$SQL .= '(CPF_PFLICHTFELD, CPF_RANKING, CPF_KON_KEY ';
			$SQL .= ',CPF_USER, CPF_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCPF_PFLICHTFELD'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCPF_RANKING'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCPF_KON_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}

		}
		else
		{
			$Meldung = 'Es wurde kein Pflichtfeld ausgew�hlt. Datensatz wurde nicht gespeichert!';
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
	}
	else
	{
		$FehlerListe = array();
		$UpdateFelder = '';
		
		$rsCPF = awisOpenRecordset($con,'SELECT * FROM CRMPflichtfelder WHERE CPF_key=' . $_POST['txtCPF_KEY'] . '');
		$FeldListe = '';
		
		foreach ($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old' . $FeldName]))
			{
				// Alten und neuen Wert umformatieren
				$WertNeu = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old' . $FeldName],true);
				$WertDB = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCPF[$FeldName][0],true);
				
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';
	
						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}
		
		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCPF['CPF_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMPflichtfelder SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CPF_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CPF_userdat=sysdate';
			$SQL .= ' WHERE CPF_key=0' . $_POST['txtCPF_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
}

?>