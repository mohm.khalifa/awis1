<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $BMV;

try {

    //********************************************************
    // SQL der Unterdetailseite
    //********************************************************

    $SQL = 'select BMR.* ';
    $SQL .=' ,row_number() OVER (';
    $SQL .='  order by BMR_REDATUM) as ZEILENNR';
    $SQL .= ' from BMARECHNUNGEN BMR ';

    //********************************************************
    // SQL Vorbereiten: Bedingung
    //********************************************************
    $Bedingung = '';
    $Bedingung .= ' WHERE BMR_BMV_KEY = ' . $BMV->DB->WertSetzen('BMR','N0', $AWIS_KEY1);

    if($AWIS_KEY2 != '') {
        $Bedingung .= ' and BMR_KEY = ' . $BMV->DB->WertSetzen('BMR','N0', $AWIS_KEY2);
    }

    $SQL .= $Bedingung;

    $SQL .= ' order by BMR_REDATUM';

    //********************************************************
    // SQL Nachbereiten: Blätternfunktion
    //********************************************************
    if ($AWIS_KEY2 == '') { //Liste?
        if (isset($_REQUEST['Block'])) { //Wurde geblättert?
            $Block = $BMV->Form->Format('N0', $_REQUEST['Block'], false);
            $BMV->Param['Block2'] = $Block;
        } elseif (isset($BMV->Param['Block2'])) { //Zurück zur Liste, Tab gewechselt..
            $Block = $BMV->Param['Block2'];
        }else{
            $Block = 1;
        }

        $ZeilenProSeite = $BMV->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $BMV->DB->ErmittleZeilenAnzahl($SQL, $BMV->DB->Bindevariablen('BMR', false));
        
    }

    //********************************************************
    // Fertigen SQL ausführen
    //********************************************************
    $rsBMR = $BMV->DB->RecordSetOeffnen($SQL, $BMV->DB->Bindevariablen('BMR'));

    //********************************************************
    // Anzeige Start
    //********************************************************
    $DS = 0; // fuer Hintergrundfarbumschaltung
    if($AWIS_KEY2 == '') { // Wenn Vertrag ausgewaehlt ist aber keine spezielle Rechnung dann Liste anzeigen

        $BMV->Form->ZeileStart();
        if(($BMV->Recht50000&8)  == 8) {
            $Icons = array();
            $Icons[] = array('blank', '');
            $Icons[] = array('save', './sicherheitsanlagen_Main.php?cmdAktion=Details&BMV_KEY=' . $AWIS_KEY1 . '&BMR_KEY=-2'); // Re-Datensatz aus Benuzerparam einfügen
            $Icons[] = array('new', './sicherheitsanlagen_Main.php?cmdAktion=Details&BMV_KEY=' . $AWIS_KEY1 . '&BMR_KEY=-1');
            $BMV->Form->Erstelle_ListeIcons($Icons, 60,-2);
        }
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMR']['BMR_REDATUM'],150);
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMR']['BMR_RENUMMER'],150);
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMR']['BMR_TEXTFELD'],380);
        $BMV->Form->Erstelle_Liste_Ueberschrift($BMV->AWISSprachKonserven['BMR']['BMR_NETTO'],150,'','','','R');
        $BMV->Form->ZeileEnde();

        while(!$rsBMR->EOF()) {
            $HG = ($DS % 2);
            $BMV->Form->ZeileStart();

            $Icons = array();
            if (($BMV->Recht50000 & 4) == 4) {
                $Icons[] = array('delete', './sicherheitsanlagen_Main.php?cmdAktion=Details&DEL&BMV_KEY=' . $AWIS_KEY1 . '&BMR_KEY=' . $rsBMR->FeldInhalt('BMR_KEY'));
            }
            if (($BMV->Recht50000 & 2) == 2) {
                $Icons[] = array('edit', './sicherheitsanlagen_Main.php?cmdAktion=Details&EDIT&BMV_KEY=' . $AWIS_KEY1 . '&BMR_KEY=' . $rsBMR->FeldInhalt('BMR_KEY'));
            }
            $BMV->Form->Erstelle_ListeIcons($Icons, 60, $HG);
            $BMV->Form->Erstelle_ListenFeld('BMR_REDATUM', $rsBMR->FeldInhalt('BMR_REDATUM'), 150, 150, false, $HG,'','','D');
            $BMV->Form->Erstelle_ListenFeld('BMR_RENUMMER', $rsBMR->FeldInhalt('BMR_RENUMMER'), 150, 150, false, $HG);
            $BMV->Form->Erstelle_ListenFeld('BMR_TEXTFELD', $rsBMR->FeldInhalt('BMR_TEXTFELD'), 150, 380, false, $HG);
            $BMV->Form->Erstelle_ListenFeld('BMR_NETTO', $rsBMR->FeldInhalt('BMR_NETTO','N2') . ' &euro;', 150, 150, false, $HG,'','','','R');
            $BMV->Form->ZeileEnde();
            $DS++;
            $rsBMR->DSWeiter();
        }
    }
    else { // neue oder einzelne bestehende Rechnung

        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./sicherheitsanlagen_Main.php?cmdAktion=Details&BMV_KEY=".$AWIS_KEY1." accesskey=T title='" . $BMV->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBMR->FeldInhalt('BMR_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBMR->FeldInhalt('BMR_USERDAT'));
        $BMV->Form->InfoZeile($Felder, '');

        //Key fuers speichern in hiddenfeld stecken
        $BMV->Form->Erstelle_HiddenFeld('BMR_KEY',($rsBMR->AnzahlDatensaetze()==1?$rsBMR->FeldInhalt('BMR_KEY'):-1));

        //Eine neue Rechnung soll angelegt werden aber bereits mit Werten vorbelegt sein aus der letzten Rechnungsanlage
        if($AWIS_KEY2 == -2) {
            $reDat = @unserialize($BMV->AWISBenutzer->ParameterLesen('Rechnung_Sicherheitsanlagen'));
        }



        if(($BMV->Recht50000&2)==2) {
            $Aendernrecht = true;
        }
        else {
            $Aendernrecht = false;
        }

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMR']['BMR_REDATUM'] . ':',170);
        $BMV->Form->Erstelle_TextFeld('!BMR_REDATUM',(isset($reDat)?$reDat['BMR_REDATUM']:$rsBMR->FeldInhalt('BMR_REDATUM')),10,150,$Aendernrecht,'','','','D');
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMR']['BMR_RENUMMER'] . ':',170);
        $BMV->Form->Erstelle_TextFeld('BMR_RENUMMER',(isset($reDat)?$reDat['BMR_RENUMMER']:$rsBMR->FeldInhalt('BMR_RENUMMER')),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMR']['BMR_TEXTFELD'] . ':',170);
        $BMV->Form->Erstelle_TextFeld('BMR_TEXTFELD',(isset($reDat)?$reDat['BMR_TEXTFELD']:$rsBMR->FeldInhalt('BMR_TEXTFELD')),65,380,$Aendernrecht);
        $BMV->Form->ZeileEnde();

        $BMV->Form->ZeileStart();
        $BMV->Form->Erstelle_TextLabel($BMV->AWISSprachKonserven['BMR']['BMR_NETTO'] . ':',170);
        $BMV->Form->Erstelle_TextFeld('BMR_NETTO',(isset($reDat)?$BMV->DB->FeldInhaltFormat('N2',$reDat['BMR_NETTO']):$rsBMR->FeldInhalt('BMR_NETTO','N2')),65,170,$Aendernrecht);
        $BMV->Form->ZeileEnde();
    }
    
    
}
catch (awisException $ex) {
    if($BMV->Form instanceof awisFormular) {
        $BMV->Form->DebugAusgabe(1, $ex->getSQL());
        $BMV->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex) {
    if($BMV->Form instanceof awisFormular) {
        $BMV->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}