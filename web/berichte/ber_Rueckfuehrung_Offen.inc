<?php
require_once 'awisBerichte.inc';

class ber_Rueckfuehrung_Offen
	extends awisBerichte
{
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		//var_dump($Parameter);
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_RueckfuehrungOffen'));
		
		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = $this->PDFBedingungErstellen($Param,$BindeVariablen);
		
		//********************************************************
		// Order erstellen
		//********************************************************
		$ORDERBY = ' rfm_fil_id, rfp_ast_atunr, rfa_nr DESC, rfa_lfdnr DESC';
		
		//*****************************************
		// Daten f�r den Bericht laden
		//*****************************************
		$SQL  = 'SELECT';
		$SQL .= ' ANFDAT.*,';
		$SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILENNR';
		$SQL .= ' FROM ';
		$SQL .= ' (';
		$SQL .= ' SELECT ';
		$SQL .= ' RFM_FIL_ID, ';
		$SQL .= ' (SELECT FIL_BEZ';
		$SQL .= '  FROM FILIALEN';
		$SQL .= '  WHERE FIL_ID = RFM_FIL_ID) AS FIL_BEZ,';
		$SQL .= ' (SELECT XX1_EBENE';
		$SQL .= '  FROM V_FILIALPFAD';
		$SQL .= '  WHERE XX1_FIL_ID = RFM_FIL_ID) AS FEB_BEZEICHNUNG,';
		$SQL .= ' RFP_AST_ATUNR,';
		$SQL .= ' (SELECT AST_BEZEICHNUNGWW';
		$SQL .= '  FROM ARTIKELSTAMM';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR) AS AST_BEZEICHNUNGWW,';
		$SQL .= ' RFA_KEY,';
		$SQL .= ' RFA_NR,';
		$SQL .= ' RFA_LFDNR,';
		$SQL .= ' RFA_DATUM,';
		$SQL .= ' NVL (RFM_MENGE, 0) AS RFM_MENGE,';
		$SQL .= ' NVL (RFP_MINDESTBESTAND, 0) AS RFP_MINDESTBESTAND,';
		$SQL .= ' (SELECT NVL (BESTAND, 0)';
		$SQL .= '  FROM V_FILIALBESTAND_AKTUELL';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR';
		$SQL .= '  AND FIL_ID = RFM.RFM_FIL_ID) AS FIL_BEST';
		$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN RFM ';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGENPOS RFP';
		$SQL .= ' ON RFM.RFM_RFP_KEY = RFP.RFP_KEY';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGEN RFA';
		$SQL .= ' ON RFA.RFA_KEY = RFP.RFP_RFA_KEY';
		$SQL .= ' WHERE RFA.RFA_RFS_KEY					= 10';
		$SQL .= ' AND RFM.RFM_MENGE						> 0';
		$SQL .= ' AND NVL (RFP.RFP_MINDESTBESTAND, 0)	= 0';
		if($Bedingung!='')
		{
			$SQL .= ' AND ' . substr($Bedingung,4);
		}		
		$SQL .= ' ) ANFDAT';
		$SQL .= ' WHERE FIL_BEST > 0';
		$SQL .= ' ORDER BY ' .$ORDERBY;

		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		
		//*****************************************
		// Alle Textbausteine laden
		//*****************************************
	    $TextKonserven = array();
	    $TextKonserven[]=array('RFM','RFM_AST_ATUNR');
	    $TextKonserven[]=array('RFM','RFM_FIL_ID');
	    $TextKonserven[]=array('RFA','RFA_DATUM');
	    $TextKonserven[]=array('RFA','RFA_LFDNR');
	    $TextKonserven[]=array('RFA','ber_Rueckfuehrung_Offen');
	    $TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	    $TextKonserven[]=array('RFP','RFP_MINDESTBESTAND');
	    $TextKonserven[]=array('FIB','FIB_BESTAND');
	    
	    $TextKonserven[]=array('FIL','FIL_ID');
	    $TextKonserven[]=array('FIL','FIL_GEBIET');
	    $TextKonserven[]=array('FIL','FIL_BEZ');
	    $TextKonserven[]=array('FIB','FIB_BESTAND');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','DatumVom');
		$TextKonserven[]=array('Wort','DatumBis');
		$TextKonserven[]=array('Liste','lst_ALLE_0');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('Wort','Soll');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eigentliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetFillColor(210,210,210);
		
		// Seite erstellen
		$this->NeueSeite();
		
		while (!$this->_rec->EOF())
		{
			$this->SetXY($this->_Spalte,$this->_Zeile);

			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(12,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('RFM_FIL_ID')),1,0,'L');
			$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('FIL_BEZ')),1,0,'L');
			$this->Cell(12,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('FEB_BEZEICHNUNG')),1,0,'L');
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('RFP_AST_ATUNR')),1,0,'L');
			$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('AST_BEZEICHNUNGWW')),1,0,'L');
			$ANFNR = $this->_Form->Format('D',$this->_rec->FeldInhalt('RFA_DATUM')).' - '.str_pad($this->_Form->Format('T',$this->_rec->FeldInhalt('RFA_LFDNR')), 3, "0", STR_PAD_LEFT);			
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$ANFNR,1,0,'L');
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('Z',$this->_rec->FeldInhalt('RFP_MINDESTBESTAND')),1,0,'R');
			$this->Cell(25,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('Z',$this->_rec->FeldInhalt('FIL_BEST')),1,0,'R');
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];

			$this->_rec->DSWeiter();
		}

		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
	}
	

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,4,$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(($this->PageNo()==1?1:2));		// Erste oder zweite Seite importieren
			$this->useTemplate($pdf_vorlage);
		}
		
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Kopfzeile'.($this->PageNo()==1?'1':'2').'-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// Berichtsparameter anzeigen
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['KopfzeileSchriftgroesse']);
		$this->Cell(30,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFA']['ber_Rueckfuehrung_Offen']),0,0,'L','');
		
		$this->_Zeile+=10;

		if ($this->PageNo()==1)
		{
			$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_RueckfuehrungOffen'));

			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['DatumVom']).':',0,0,'L','');
			$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$Param['DATUM_VOM']),0,0,'L','');
			
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
			$this->Cell(20,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['DatumBis']).':',0,0,'L','');
			$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
			$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$Param['DATUM_BIS']),0,0,'L','');

			$this->_Zeile+=5;
		}
		
		// Trennlinie
/*
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->Line($this->_Spalte,$this->_Zeile,$this->SeitenBreite()-self::RECHTERRAND,$this->_Zeile);
		$this->_Zeile+=2;
*/		
		$this->Ueberschrift();
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
	}
	
	private function Ueberschrift()
	{
		//�berschrift
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['TabelleUeberschrift-Groesse']*1);
		$this->Cell(12,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFM']['RFM_FIL_ID']),1,0,'L',1);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFM']['RFM_FIL_ID']),1,0,'L',1);
		$this->Cell(12,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['FIL']['FIL_GEBIET']),1,0,'L',1);
		$this->Cell(20,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFM']['RFM_AST_ATUNR']),1,0,'L',1);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['AST']['AST_BEZEICHNUNGWW']),1,0,'L',1);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFA']['RFA_DATUM']),1,0,'L',1);
		//$this->Cell(30,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['RFP']['RFP_MINDESTBESTAND']),1,0,'R',1);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Soll']),1,0,'R',1);
		$this->Cell(25,$this->_Parameter['Zeilenhoehe']*1.4,$this->_Form->Format('T',$this->_AWISSprachkonserven['FIB']['FIB_BESTAND']),1,0,'R',1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*1.4;		
	}
	
	private function PDFBedingungErstellen($Param, &$BindeVariablen)
	{
		$Bedingung = '';
		if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
		{
			$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) >= :var_D_DATUM_VOM';
			$BindeVariablen['var_D_DATUM_VOM'] = $this->_Form->Format('D',$Param['DATUM_VOM'],false);
		}
		
		if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
		{
			$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) <= :var_D_DATUM_BIS';
			$BindeVariablen['var_D_DATUM_BIS'] = $this->_Form->Format('D',$Param['DATUM_BIS'],false);
		}
		
		if(isset($Param['RFP_AST_ATUNR']) AND $Param['RFP_AST_ATUNR']!='')
		{
			$Bedingung .= ' AND RFP.RFP_AST_ATUNR = :var_T_RFP_AST_ATUNR';
			$BindeVariablen['var_T_RFP_AST_ATUNR'] = $this->_Form->Format('T',$Param['RFP_AST_ATUNR'],true);
		}
		
		if(isset($Param['RFM_FIL_ID']) AND $Param['RFM_FIL_ID']!='')
		{
			$Bedingung .= ' AND rfm.rfm_fil_id = :var_N_RFM_FIL_ID';
			$BindeVariablen['var_N_RFM_FIL_ID'] = $this->_Form->Format('N0',$Param['RFM_FIL_ID'],false);
		}
	
		if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET']!=0)
		{
			$Bedingung .= ' AND EXISTS (';
			$Bedingung .= '  SELECT *';
			$Bedingung .= '  FROM V_FILIALPFAD';
			$Bedingung .= '  WHERE REGEXP_LIKE (XX1_PFADKEYS, :var_N_FIL_GEBIET)';
			$Bedingung .= '  AND XX1_FIL_ID = RFM.RFM_FIL_ID)';
			$BindeVariablen['var_N_FIL_GEBIET'] = $this->_Form->Format('N0',$Param['FIL_GEBIET'],false);
		}
	
		return $Bedingung;
	}
	
}
?>