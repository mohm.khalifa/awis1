<?php
global $con;
global $Recht10006;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZSA','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10006 = awisBenutzerRecht($con,10006,$AWISBenutzer->BenutzerName());
if($Recht10006==0)
{
    awisEreignis(3,1000,'ZSA',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./zukaufbestellungen_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./zukaufbestellungen_speichern.php');
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM ZukaufSonderanfragen ';

if(isset($_POST['cmdDSNeu_x']))
{
	$SQL .= ' WHERE ZSA_WANR IS NULL';
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY ZSA_USERDAT ASC';
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_Debug(1,$SQL);
if(awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'")===false)
{
	echo '####';
}

$rsZSA = awisOpenRecordset($con, $SQL);
$rsZSAZeilen = $awisRSZeilen;

echo '<form name=frmZukaufSonderanfragen action=./zukaufbestellungen_Main.php?cmdAktion=Sonderanfragen method=POST>';

//********************************************************
// Daten anzeigen
//********************************************************
if($rsZSAZeilen>=1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = '';
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_WANR'],240,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_FIRMA'],130,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_USERDAT'],200,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_USER'],200,'',$Link);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZSAZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZSAZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufbestellungen_Main.php?cmdAktion=Sonderanfragen&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}
 
	for($ZSAZeile=$StartZeile;$ZSAZeile<$rsZSAZeilen and $ZSAZeile<$StartZeile+$MaxDSAnzahl;$ZSAZeile++)
	{
		$Status = '';
		awis_FORM_ZeileStart();

		$Link = '';
		awis_FORM_Erstelle_ListenFeld('ZSA_WANR',$rsZSA['ZSA_WANR'][$ZSAZeile],0,240,false,($ZSAZeile%2),'',$Link,'T');
	
		$Link = '';
		awis_FORM_Erstelle_ListenFeld('ZSA_FIRMA',$rsZSA['ZSA_FIRMA'][$ZSAZeile],0,130,false,($ZSAZeile%2),'',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('ZSA_USERDAT',$rsZSA['ZSA_USERDAT'][$ZSAZeile],0,200,false,($ZSAZeile%2),'',$Link,'DU');
		awis_FORM_Erstelle_ListenFeld('ZSA_USERDAT',$rsZSA['ZSA_USER'][$ZSAZeile],0,200,false,($ZSAZeile%2),'',$Link,'T');
	
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
elseif(($Recht10006&2)!==0)											// Eine einzelne oder neue Bestellung
{
	//echo '<table>';
	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufbestellungen_Main.php?cmdAktion=Sonderanfragen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>(isset($_POST['cmdDSNeu_x'])?'':$rsZSA['ZSA_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>(isset($_POST['cmdDSNeu_x'])?'':$rsZSA['ZSA_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht10006&2)!=0);

	awis_FORM_ZeileStart();
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_WANR'].':',250);
 	awis_FORM_Erstelle_TextFeld('ZSA_WANR',(isset($_POST['cmdDSNeu_x'])?'':$rsZSA['ZSA_WANR'][0]),20,200,$EditRecht,'','','','T','','','',11);
 	$CursorFeld='txtZSA_WANR';
	awis_FORM_ZeileEnde();

		// Lieferant
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_FIRMA'].':',250);
	$Daten = array('PV~PV','TROST~TROST');
	awis_FORM_Erstelle_SelectFeld('ZSA_FIRMA',(isset($_POST['cmdDSNeu_x'])?'':$rsZSA['ZSA_FIRMA'][0]),200,$EditRecht,$con,'','','','','',$Daten);
	awis_FORM_ZeileEnde();
	
	awis_FORM_FormularEnde();

}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************

awis_FORM_SchaltflaechenStart();
if(($Recht10006&2)!==0)		//
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	if(isset($_POST['cmdDSNeu_x']) OR $rsZSAZeilen==0)
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
}	
awis_FORM_SchaltflaechenEnde();

echo '</form>';

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}