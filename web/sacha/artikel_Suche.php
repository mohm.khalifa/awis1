<?
// Konstanten

$sk_BreiteErsteSpalte = 250;		// Breite f�r die erste Splate in der Suchmaske

// Variablen
global $HTTP_POST_VARS;
global $HTTP_GET_VARS;

$RechteStufe = awisBenutzerRecht($con, 400);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'ATU-Artikel: Suchen',$_SERVER['PHP_AUTH_USER'],'','','');
	awisLogoff($con);   
	die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

// lokale Variablen

// Ende Variablen

// Auswahl zur�cksetzen

if($HTTP_GET_VARS["Reset"]=='True')
{
//TODO: Haken speichern
	$Param =explode(";",awis_BenutzerParameter($con, "ArtikelSuche", $_SERVER['PHP_AUTH_USER']));
	$Param[1]='';
	$Param[2]='';	
	$Param[3]='';	
	$Param[10]='';	
	awis_BenutzerParameterSpeichern($con,"ArtikelSuche",$_SERVER['PHP_AUTH_USER'],implode(";",$Param));
}

print "<form name=frmSuche method=post action=./artikel_Main.php>";//?cmdAktion=ArtikelInfos>";

print "<table id=SuchMaske>";

$ArtSuche = explode(";",awis_BenutzerParameter($con, "ArtikelSuche", $_SERVER['PHP_AUTH_USER']));

// ATU Nummer
print "<tr><td width=$sk_BreiteErsteSpalte>ATU Artikel-<u>N</u>ummer</td><td><input name=txtAST_ATUNR accesskey=n tabindex=1 size=10 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[1]:'') . "\"></td></tr>";
// Artikelbezeichnung
print "<tr><td width=$sk_BreiteErsteSpalte>Artikel-Bezei<u>c</u>hnung</td><td><input name=txtAST_BEZEICHNUNG accesskey=c tabindex=2 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[2]:'') . "\"></td></tr>";
print "<tr><td width=$sk_BreiteErsteSpalte>Kommentar</td><td><input name=txtKommentar tabindex=3 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[10]:'') . "\"></td></tr>";

// Auswahl Speichern
print "<tr><td colspan=2><hr></td></tr>";

print "<tr><td width=$sk_BreiteErsteSpalte>Zu suchende N<u>u</u>mmer</td><td><input name=txtTEI_SUCH accesskey=u tabindex=10 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[3]:'') . "\">";
print "<input type=checkbox name=txtTEI_SUCH_Exakt accesskey=� tabindex=20 value='on'  " . ($ArtSuche[0]=='on'?($ArtSuche[7]=='on'?'checked':''):'') . "> exakte <u>�</u>bereinstimmung";	

print "</td></tr>";
print "<tr><td colspan=2>Nummer ist eine</td></tr>";

print "<tr><td width=$sk_BreiteErsteSpalte>�<u>E</u>AN-Nummer</td><td><input type=checkbox name=txtTEI_SUCH_EAN accesskey=e tabindex=20 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[4]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
print "<tr><td width=$sk_BreiteErsteSpalte>�<u>G</u>ebrauchsnummer</td><td><input type=checkbox name=txtTEI_SUCH_GNR accesskey=g tabindex=21 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[5]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
print "<tr><td width=$sk_BreiteErsteSpalte>�<u>L</u>ieferanten-Artikel-Nummer</td><td><input type=checkbox name=txtTEI_SUCH_LAR accesskey=l tabindex=22 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[6]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
print "<tr><td width=$sk_BreiteErsteSpalte>�Lieferan<u>t</u>en-Set</td><td><input type=checkbox name=txtTEI_SUCH_LAS accesskey=t tabindex=23 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[8]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
print "<tr><td width=$sk_BreiteErsteSpalte>�OE Nu<u>m</u>mer</td><td><input type=checkbox name=txtTEI_SUCH_OEN accesskey=m tabindex=24 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[9]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
// Sondersuche: Sucht nach einer beliebigen Nummer
print "<tr><td width=$sk_BreiteErsteSpalte>�Bel<u>i</u>ebige Nummer</td><td><input type=checkbox name=txtTEI_SUCH_XXX accesskey=m tabindex=25 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[11]=='on'?'checked':''):'') . " onclick=\"SetzeBeliebigeNummer();\"></td></tr>";

print "<tr><td colspan=2><hr></td></tr>";

print "<tr><td width=$sk_BreiteErsteSpalte>Auswahl <u>s</u>peichern:</td>";
print "<td><input type=checkbox value=on " . ($ArtSuche[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=55></td></tr>";

print "<tr><td>Anzuzeigendes Reg<u>i</u>ster:</td>";

print "<td><select tabindex=56 accesskey=i name=cmdAktion>";
print "<option selected Value=ArtikelInfos>Artikelinformationen</option>";
print "<option Value=EANArtikel>EAN Artikel</option>";
print "<option Value=OENummern>OE Nummern</option>";
print "<option Value=Lieferantenartikel>Lieferantenartikel</option>";

print "</select></td>";
print "</tr>";

print "</table>";

//print "<input type=hidden name=cmdAktion value=ArtikelInfos>";
print "<hr><br>&nbsp;<input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren>";
print "&nbsp;<img accesskey=r tabindex=99 src=/bilder/radierer.png alt='Formularinhalt l�schen (Alt+r)' name=cmdReset onclick=location.href='./artikel_Main.php?Reset=True';>";

print "</form>";

// Cursor in das erste Feld setzen
print "<Script Language=JavaScript>";
print "document.getElementsByName(\"txtAST_ATUNR\")[0].focus();";

echo 'function SetzeBeliebigeNummer()';
echo '{';
echo "  if(document.getElementsByName(\"txtTEI_SUCH_XXX\")[0].checked==true)";
echo '{';
echo "    document.getElementsByName(\"txtTEI_SUCH_EAN\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_GNR\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_LAR\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_LAS\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_OEN\")[0].checked=false;";
echo "    document.getElementsByName(\"cmdAktion\")[0].value=\"ArtikelInfos\";";
echo '}';
echo "  return(0)";
echo '}';

echo 'function BeliebigeNummerAus()';
echo '{';
echo "  document.getElementsByName(\"txtTEI_SUCH_XXX\")[0].checked=false;";
echo "  return(0)";
echo '}';



print "</Script>";

?>