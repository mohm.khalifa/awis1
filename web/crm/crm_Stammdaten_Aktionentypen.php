<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CAT','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT CAT_KEY FROM (SELECT CAT_KEY FROM CRMAktionenTypen';
	$SQL .= ' WHERE  UPPER(CAT_BEZEICHNUNG) < '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCAT_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CAT_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsCAT = awisOpenRecordset($con, $SQL);
	if(isset($rsCAT['CAT_KEY'][0]))
	{
		$AWIS_KEY2=$rsCAT['CAT_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCAT_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT CAT_KEY FROM (SELECT CAT_KEY FROM CRMAktionenTypen';
	$SQL .= ' WHERE  UPPER(CAT_BEZEICHNUNG) > '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCAT_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CAT_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsCAT = awisOpenRecordset($con, $SQL);

	if(isset($rsCAT['CAT_KEY'][0]))
	{
		$AWIS_KEY2=$rsCAT['CAT_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCAT_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./crm_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}
elseif(isset($_GET['CAT_KEY']))
{
	$Bedingung .= ' AND CAT_KEY='.awis_FeldInhaltFormat('Z',$_GET['CAT_KEY']).'';
}
elseif(isset($_POST['txtCAT_KEY']))
{
	$Bedingung .= ' AND CAT_KEY='.awis_FeldInhaltFormat('Z',$_POST['txtCAT_KEY']).'';
}

if($AWIS_KEY2>0)
{
	$Bedingung .= ' AND CAT_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY2).'';
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMAktionenTypen';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY CAT_BEZEICHNUNG';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

$rsCAT = awisOpenRecordset($con, $SQL);
$rsCATZeilen = $awisRSZeilen;
awis_Debug(1,$_POST,$_GET, $SQL);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsCATZeilen==0 AND !isset($_GET['CAT_KEY']))		// Keine Meldung bei neuen Datensätzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden CRMAktionenTypen gefunden.</span>';
}
elseif($rsCATZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen&CAT_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CAT_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CAT_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAT']['CAT_BEZEICHNUNG'],450,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAT']['CAT_BEMERKUNG'],450,'',$Link);
	awis_FORM_ZeileEnde();

	for($CATZeile=0;$CATZeile<$rsCATZeilen;$CATZeile++)
	{
		awis_FORM_ZeileStart();

		$Icons = array();
		if(($Recht3708&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen&CAT_KEY='.$rsCAT['CAT_KEY'][$CATZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen&Del='.$rsCAT['CAT_KEY'][$CATZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CATZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('CAT_BEZEICHNUNG',$rsCAT['CAT_BEZEICHNUNG'][$CATZeile],0,450,false,($CATZeile%2));
		awis_FORM_Erstelle_ListenFeld('CAT_BEMERKUNG',$rsCAT['CAT_BEMERKUNG'][$CATZeile],0,450,false,($CATZeile%2));
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmCRMAktionenTypen action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsCAT['CAT_KEY'][0])?$rsCAT['CAT_KEY'][0]:0);

	echo '<input type=hidden name=txtCAT_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Stammdaten&Seite=Aktionentypen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCAT['CAT_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCAT['CAT_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$TabellenKuerzel='CAT';
	$EditRecht=(($Recht3708&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAT']['CAT_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CAT_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsCAT['CAT_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtCAT_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAT']['CAT_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CAT_BEMERKUNG',($AWIS_KEY1===0?'':$rsCAT['CAT_BEMERKUNG'][0]),50,350,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	//***************************************
	// Schaltflächen für dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
/*	if(($Recht3708&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzufügen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht3708&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}	
	*/

	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>