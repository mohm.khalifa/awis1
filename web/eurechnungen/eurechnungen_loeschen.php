<?php
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');

try {
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISSprachKonservenLoeschen = $Form->LadeTexte($TextKonserven);

    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);
    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    //Position l�schen

    if (isset($_GET['ERP_KEY']) and isset($_GET['DEL'])) {

        $Delete = 'DELETE';
        $Delete .= ' FROM EURECHNUNGSPOSITIONEN';
        $Delete .= ' WHERE ERP_KEY        = ' . $DB->WertSetzen('ERP', 'N0', $_GET['ERP_KEY']);

        $DB->Ausfuehren($Delete, '', '', $DB->Bindevariablen('ERP'));
    }

    if (isset($_POST['cmdLoeschenOK']))    // Loeschen durchf�hren
    {
        $SQL = 'DELETE FROM eurechnungskopf where ERK_ID = ' . $DB->WertSetzen('ERP', 'N0',$_POST['txtERK_ID']);

        $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('ERP'));
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>