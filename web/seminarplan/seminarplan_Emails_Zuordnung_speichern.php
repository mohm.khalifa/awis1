<?php
global $SEP;
try {
    $SQL = 'merge into SEMINAREMAILS DEST using';
    $SQL .= '     ( select';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', $_POST['txtSEM_SEP_SEMINAR']) . '          as SEM_SEP_SEMINAR,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', $_POST['txtSEM_MVT_BEREICH']) . '          as SEM_MVT_BEREICH,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', serialize(@$_POST['txtSEM_ANHAENGE'])) . '          as SEM_ANHAENGE,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', $_POST['txtSEM_ABSENDER']) . '          as SEM_ABSENDER,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', serialize(@$_POST['txtSEM_EMPFAENGER'])) . '          as SEM_EMPFAENGER,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', serialize(@$_POST['txtSEM_EMPFAENGERCC'])) . '          as SEM_EMPFAENGERCC,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'T', serialize(@$_POST['txtSEM_EMPFAENGERBCC'])) . '          as SEM_EMPFAENGERBCC,';
    $SQL .= $SEP->DB->WertSetzen('MVT', 'TU', $SEP->AWISBenutzer->BenutzerName()) . '          as SEM_USER';
    $SQL .= '     from';
    $SQL .= '         DUAL';
    $SQL .= '     )';
    $SQL .= ' SRC on (';
    $SQL .= '     SRC.SEM_SEP_SEMINAR = DEST.SEM_SEP_SEMINAR';
    $SQL .= ' ) when matched then';
    $SQL .= '     update';
    $SQL .= ' set DEST.SEM_MVT_BEREICH = SRC.SEM_MVT_BEREICH,';
    $SQL .= '     DEST.SEM_ANHAENGE = SRC.SEM_ANHAENGE,';
    $SQL .= '     DEST.SEM_ABSENDER = SRC.SEM_ABSENDER,';
    $SQL .= '     DEST.SEM_EMPFAENGER = SRC.SEM_EMPFAENGER,';
    $SQL .= '     DEST.SEM_EMPFAENGERCC = SRC.SEM_EMPFAENGERCC,';
    $SQL .= '     DEST.SEM_EMPFAENGERBCC = SRC.SEM_EMPFAENGERBCC,';
    $SQL .= '     DEST.SEM_USER = SRC.SEM_USER,';
    $SQL .= '     DEST.SEM_USERDAT = SYSDATE';
    $SQL .= ' when not matched then ';
    $SQL .= ' insert ( DEST.SEM_SEP_SEMINAR,DEST.SEM_MVT_BEREICH,DEST.SEM_ANHAENGE,DEST.SEM_ABSENDER,DEST.SEM_EMPFAENGER,DEST.SEM_EMPFAENGERCC,DEST.SEM_EMPFAENGERBCC,DEST.SEM_USER,DEST.SEM_USERDAT ) ';
    $SQL .= ' values ';
    $SQL .= ' ( SRC.SEM_SEP_SEMINAR,SRC.SEM_MVT_BEREICH,SRC.SEM_ANHAENGE,SRC.SEM_ABSENDER';
    $SQL .= ' ,SRC.SEM_EMPFAENGER,SRC.SEM_EMPFAENGERCC,SRC.SEM_EMPFAENGERBCC,SRC.SEM_USER,SYSDATE )';


    $SEP->DB->Ausfuehren($SQL, '', true, $SEP->DB->Bindevariablen('MVT'));

    if ($_GET['SEM_KEY'] == -1) { //Wars eine Neuanlage? Dann brauch ich die Sequenz
        $_GET['SEM_KEY'] = $SEP->DB->RecordSetOeffnen('Select SEQ_SEM_KEY.currval from dual')->FeldInhalt(1);
    }
    $SEP->Form->Hinweistext($SEP->AWISSprachKonserven['SEP']['SEP_SPEICHERNOK'], awisFormular::HINWEISTEXT_OK);

} catch (awisException $ex) {
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>