<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWISBenutzer;

try 
{
    require_once('awisDokumente.php');
    
    $TextKonserven=array();
    $TextKonserven[]=array('Wort','WirklichLoeschen');
    $TextKonserven[]=array('Wort','Ja');
    $TextKonserven[]=array('Wort','Nein');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();    
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);    
    $Tabelle= '';
    
    if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Adresse
    {
        $Tabelle = 'ADR';
        $Key=$_POST['txtADR_KEY'];
    
        $Felder=array();
        $Felder[]=array($Form->LadeTextBaustein('ADR_NAME1','Adressen',$AWISSprache),$_POST['txtADR_NAME1']);
        $Felder[]=array($Form->LadeTextBaustein('ADR_NAME2','Adressen',$AWISSprache),$_POST['txtADR_NAME2']);
    }
    elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
    {
        switch($_GET['Seite'])
        {
        	case 'Infos':
        	    $Tabelle = 'ADT';
        	    $Key=$_GET['Del'];
    
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT ADT_KEY,ADT_INFORMATION FROM AdressenInfoTypen WHERE ADT_KEY=0'.$Key);
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('ADT_INFORMATION','ADT',$AWISSprache),$rsDaten->FeldInhalt('ADT_INFORMATION'));
        	    break;
        	case 'Aktionen':
        	    $Tabelle = 'AAT';
        	    $Key=$_GET['Del'];
    
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT AAT_KEY,AAT_BEZEICHNUNG FROM AdressenAktionenTypen WHERE AAT_KEY=0'.$Key);
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('AAT_BEZEICHNUNG','AAT',$AWISSprache),$rsDaten->FeldInhalt('AAT_BEZEICHNUNG'));
        	    break;
        }
    }
    elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) {
        	case 'ADT':
        	    $SQL = 'DELETE FROM AdressenInfoTypen WHERE adt_key=0'.$_POST['txtKey'];
        	    break;
        	default:
        	    break;
        	case 'AAT':
        	    $SQL = 'DELETE FROM AdressenAktionenTypen WHERE aat_key=0'.$_POST['txtKey'];
        	    break;
        	default:
        	    break;
        }
    
        if($SQL !='')
        {
            if($DB->Ausfuehren($SQL)===false)
            {
                throw new awisException('Fehler beim Loeschen',201403240959,$SQL,awisException::AWIS_ERR_SYSTEM);
            }
        }
    }
    
    if($Tabelle!='')
    {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven, $AWISSprache);
        echo '<form name=frmLoeschen action=./adressen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
        echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';
    
        foreach($Felder AS $Feld)
        {
            echo '<br>'.$Feld[0].': '.$Feld[1];
        }
        echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
        echo '<input type=hidden name=txtKey value="'.$Key.'">';
        echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
        echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';
        echo '</form>';
        $DB->Schliessen();
        die();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>