<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	if(isset($_POST['txtID']))
	{
			if($_POST['txtID'] == '-1')
			{	
					$SQL ='insert into VORSCHLAG_BEM ';
					$SQL .=' (BEM,AKTIV,BEMUSER,BEMUSERDAT)';
					$SQL .=' values';
					$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
					if(isset($_POST['txtCHK_AKTIV']))
					{
						$SQL .= ',1';
					}
					else
					{
						$SQL .= ',0';
					}
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=',sysdate)';
					
					$DBSCHAD->Ausfuehren($SQL,'',true);
					$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
			}
			else 
			{
				$SQL ='update VORSCHLAG_BEM ';
				$SQL .= 'set BEM='.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
				if(isset($_POST['txtCHK_AKTIV']))
				{
					$SQL .= ', AKTIV=1';
				}
				else
				{
					$SQL .= ', AKTIV=0';
				}
				$SQL .= ', BEMUSER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', BEMUSERDAT=SYSDATE';
				$SQL .=' where ID='.$_POST['txtID'];
				$DBSCHAD->Ausfuehren($SQL,'',true);		
				$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			}
	}
}
catch (awisException $ex)
{

	if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.UID_BEM) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = 'Der Bemerkungstext befindet sich bereits in der Datenbank.';
		 
		$Form->Hinweistext($Hinweis);
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}



}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
