<?php
/**
 * Suchmaske f�r eurechnungen
 *
 * @author    Patrick Gebhardt
 * @copyright ATU
 * @version   201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('ERE', '%');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'DatumVom');
    $TextKonserven[] = array('Wort', 'DatumBis');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_JaNein');

    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);
    if ($Recht47000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./eurechnungen_Main.php?cmdAktion=Details>");

    /**********************************************
     * * Eingabemaske
     ***********************************************/

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_EURechnungen'));

    if (!isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
    }

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['ERE_RECHNUNGSNUMMER'] . ':', 190);
    $Form->Erstelle_TextFeld('*ERE_RECHNUNGSNUMMER', (isset($Param['SPEICHERN']) && $Param['SPEICHERN'] == 'on'?$Param['ERE_RECHNUNGSNUMMER']:''), 4, 130, true, '', '', '', 'T');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 190);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

    if (($Recht47000 & 2) == 2)        // Hinzuf�gen erlaubt?
    {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();

    if ($AWISCursorPosition != '') {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";
        echo '</Script>';
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>