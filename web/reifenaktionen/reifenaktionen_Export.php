<?php
/**
 * Suchmaske f�r die Auswahl Reifenpflege Exporte
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130502
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('RRA','%');
	$TextKonserven[]=array('RST','%');
	$TextKonserven[]=array('RPP','%');
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','gelistet');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15200=$AWISBenutzer->HatDasRecht(15200);		
	if ($Recht15200 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
    $Form->DebugAusgabe(1,$_POST);
    
    $Form->SchreibeHTMLCode('<form name=frmExport action=./reifenaktionen_Main.php?cmdAktion=Export method=post enctype="multipart/form-data">');
    
	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RRA_Export'));

	$Form->DebugAusgabe(1,$Param);
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();
	
	$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['lbl_export'].':',180);
	$Daten = explode("|",$AWISSprachKonserven['RRA']['lst_EXPORT']);
	$Form->Erstelle_SelectFeld('*EXPORTART',$Param['SPEICHERN']=='on'?$Param['EXPORTART']:'',"220:200", true, '','','','','',$Daten);
	$Form->ZeileEnde();
	$AWISCursorPosition='sucEXPORTART';
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_AKTIONSNAME'].':',180);
	$Form->Erstelle_TextFeld('*RRA_AKTIONSNAME',($Param['SPEICHERN']=='on'?$Param['RRA_AKTIONSNAME']:''),60,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_GUELTIGAB'].':',180);
	$Form->Erstelle_TextFeld('*RRA_GUELTIGAB',($Param['SPEICHERN']=='on'?$Param['RRA_GUELTIGAB']:''),10,120,true,'','','','D','L','','',10);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_GUELTIGBIS'].':',70);
	$Form->Erstelle_TextFeld('*RRA_GUELTIGBIS',($Param['SPEICHERN']=='on'?$Param['RRA_GUELTIGBIS']:''),10,200,true,'','','','D','L','','',10);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_LAN_CODE'] . ':', 180, '', '', '');
	$SQL = "SELECT LAN_CODE, LAN_LAND FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL";
	$Form->Erstelle_SelectFeld('*RRA_LAN_CODE',($Param['SPEICHERN']=='on'?$Param['RRA_LAN_CODE']:''), "220:200", true, $SQL, '-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'].':',180);
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*RST_REH_KEY',($Param['SPEICHERN']=='on'?$Param['RST_REH_KEY']:'0'),"220:200",true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_WGR_ID'] . ':', 180, '', '', '');
	$SQL = "SELECT WGR_ID, WGR_ID || ' - ' || WGR_BEZEICHNUNG FROM WARENGRUPPEN WHERE WGR_ID IN ('01','02','23','29') ORDER BY WGR_ID";
	$Form->Erstelle_SelectFeld('RRA_WGR_ID', ($Param['SPEICHERN']=='on'?$Param['RRA_WGR_ID']:''), "220:200", true, $SQL, $OptionBitteWaehlen);
	 
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_WUG_ID'] . ':', 70, '', '', '');
	if ($Param['SPEICHERN']=='on')
	{
		$SQL = "SELECT DISTINCT WUG_ID AS KEY , WUG_ID || ' - ' || WUG_BEZEICHNUNG AS ANZEIGE ";
		$SQL .= ' FROM WARENUNTERGRUPPEN';
		$SQL .= ' WHERE WUG_WGR_ID='.$DB->FeldInhaltFormat('T',$Param['RRA_WGR_ID'],true);
		$SQL .= ' AND WUG_ID='.$DB->FeldInhaltFormat('T',$Param['RRA_WUG_ID'],true);
		$SQL .= ' ORDER BY WUG_ID';
		$rsWUG=$DB->RecordSetOeffnen($SQL);
		$Form->Erstelle_SelectFeld('RRA_WUG_ID',$Param['RRA_WUG_ID'],'240:200',true,'***WUG_Daten;txtRRA_WUG_ID;WGR=*txtRRA_WGR_ID&Zusatz='.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],($rsWUG->EOF())?$OptionBitteWaehlen:$rsWUG->FeldInhalt('KEY').'~'.$rsWUG->FeldInhalt('ANZEIGE'));
	}
	else
	{
		$Form->Erstelle_SelectFeld('RRA_WUG_ID','',"220:200",true,'***WUG_Daten;txtRRA_WUG_ID;WGR=*txtRRA_WGR_ID&Zusatz='.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],$OptionBitteWaehlen);
	}
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_KENN1'].':',180);
	$Form->Erstelle_TextFeld('*RRA_KENN1',($Param['SPEICHERN']=='on'?$Param['RRA_KENN1']:''),1,50,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_ATUNR'].':',180);
	$Form->Erstelle_TextFeld('*RRA_ATUNR',($Param['SPEICHERN']=='on'?$Param['RRA_ATUNR']:''),6,75,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_AKTIONSTEXT'].':',180);
	$Form->Erstelle_TextFeld('*RRA_AKTIONSTEXT',($Param['SPEICHERN']=='on'?$Param['RRA_AKTIONSTEXT']:''),60,200,true);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if(($Recht15200&32)==32)
	{
		$Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
	}

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}

catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306190936");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306190937");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>