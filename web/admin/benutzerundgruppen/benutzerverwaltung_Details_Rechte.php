<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;
global $AWISCursorPosition;

$TextKonserven = array();
$TextKonserven[]=array('XBA','%');
$TextKonserven[]=array('XRC','%');
$TextKonserven[]=array('XRS','XRS_BESCHREIBUNG');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht1 = $AWISBenutzer->HatDasRecht(1);	
	if($Recht1&(2^9)==0)		
	{		
	    awisEreignis(3,1000,'BenutzerACLs',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$EditRecht=(($Recht1&(2^9))!=0);

	//***********************************
	// Rechte anzeigen
	//***********************************

	$SQL = 'SELECT BenutzerACLs.*, XRC_RECHT';
	$SQL .= ' FROM BenutzerACLs ';
	$SQL .= ' INNER JOIN Rechte ON XBA_XRC_ID = XRC_ID';
	$SQL .= ' WHERE XBA_XXX_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['XBA_KEY']))
	{
		$SQL .= ' AND XBA_KEY=0'.intval($_GET['XBA_KEY']);
	}
	if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
	{
		$SQL .= ' AND XBA_KEY=0'.intval($AWIS_KEY2);
	}

	if(!isset($_GET['XBLSort']))
	{
		$SQL .= ' ORDER BY XRC_RECHT';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['XBLSort']);
	}
	$rsXBA = $DB->RecordSetOeffnen($SQL);

	if(!isset($_GET['XBA_KEY']) AND $AWIS_KEY2<=0)					// Liste anzeigen
	{
		$Form->Formular_Start();
		$Form->ZeileStart();

		//if(($Recht1&6)>0)
		if(($Recht1&512)>0)
		{
			$Icons[] = array('new','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Rechte&XBA_KEY=0');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}

		$Link = './benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Rechte';
		$Link .= '&XBLSort=XRC_RECHT'.((isset($_GET['XBLSort']) AND ($_GET['XBLSort']=='CAP_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XRC']['XRC_RECHT'],350,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XRS']['XRS_BESCHREIBUNG'],650);

		$Form->ZeileEnde();
		$DS=1;
		while(!$rsXBA->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			//if(($Recht1&6)>0)	// Ändernrecht
			if(($Recht1&512)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Rechte&XBA_KEY='.$rsXBA->FeldInhalt('XBA_KEY'));
				$Icons[] = array('delete','./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Rechte&Del='.$rsXBA->FeldInhalt('XBA_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));


			$SQL = 'SELECT *';
			$SQL .= ' FROM RechteStufen';
			$SQL .= ' WHERE xrs_xrc_id = 0'.$rsXBA->FeldInhalt('XBA_XRC_ID');
			$SQL .= ' ORDER BY XRS_BIT';

			$rsXRS = $DB->RecordSetOeffnen($SQL);
			$Wert = array();
			while(!$rsXRS->EOF())
			{
				if((pow(2,floatval(($rsXRS->FeldInhalt('XRS_BIT'))))&floatval($rsXBA->FeldInhalt('XBA_STUFE')))!=0)
				{
					$Wert[] = $rsXRS->FeldInhalt('XRS_BESCHREIBUNG');
				}
				$rsXRS->DSWeiter();
			}

			$Link = '';
			if($AWISBenutzer->HatDasRecht(2)>0)
			{
				$Link = '/admin/rechte_Main.php?cmdAktion=Rechtestufen&XRC_ID='.$rsXBA->FeldInhalt('XBA_XRC_ID');
			}

			$Form->Erstelle_ListenFeld('#XRC_RECHT',$rsXBA->FeldInhalt('XRC_RECHT'),20,350,false,($DS%2),'',$Link,'T','',implode(',',$Wert));
			$Text = implode(',',$Wert);
			$Form->Erstelle_ListenFeld('#STUFEN',(strlen($Text)>80?substr($Text,0,78).'...':$Text),20,650,false,($DS%2),'','','T','',$Text);

			$Form->ZeileEnde();
			$DS++;
			$rsXBA->DSWeiter();
		}
		$Form->Formular_Ende();
	}
	else 		// Einer oder keiner
	{
		$Form->Formular_Start();

		echo '<input name=txtXBA_KEY type=hidden value=0'.$rsXBA->FeldInhalt('XBA_KEY').'>';
		echo '<input name=txtXBA_XBN_KEY type=hidden value=0'.$AWIS_KEY1.'>';

		$AWIS_KEY2 = $rsXBA->FeldInhalt('XBA_KEY');
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./benutzerverwaltung_Main.php?cmdAktion=Details&Seite=Rechte&XBLListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBA->FeldInhalt('XBA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBA->FeldInhalt('XBA_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XRC']['XRC_RECHT'].':',150);
		$SQL = 'SELECT XRC_ID, XRC_RECHT || \' - \' || XRC_ID';
		$SQL .= ' FROM Rechte';
		$SQL .= ' WHERE NOT EXISTS(SELECT XBA_XRC_ID FROM Benutzeracls WHERE xba_xxx_key=0'.$AWIS_KEY1.' AND xba_xrc_id = XRC_ID)';
		$SQL .= ' OR XRC_ID = 0'.$rsXBA->FeldInhalt('XBA_XRC_ID');
		$SQL .= ' ORDER BY XRC_RECHT';
		$AWISCursorPosition='txtXBA_XRC_ID';
		$Form->Erstelle_SelectFeld('XBA_XRC_ID',$rsXBA->FeldInhalt('XBA_XRC_ID'),300,$EditRecht,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		// Überschriftszeile
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBA']['txtRechteStufenVergabe'].':',700,awisFormular::FORMAT_UEBERSCHRIFT);
		$Form->ZeileEnde();

		// Einzelne Rechtestufen
		if(floatval($rsXBA->FeldInhalt('XBA_XRC_ID'))!=0)
		{
			$SQL = 'SELECT *';
			$SQL .= ' FROM RechteStufen';
			$SQL .= ' WHERE xrs_xrc_id = 0'.$rsXBA->FeldInhalt('XBA_XRC_ID');
			$SQL .= ' ORDER BY XRS_BIT';

			$rsXRS = $DB->RecordSetOeffnen($SQL);
			while(!$rsXRS->EOF())
			{
				$Form->ZeileStart();
				$Wert = ((pow(2,floatval(($rsXRS->FeldInhalt('XRS_BIT'))))&floatval($rsXBA->FeldInhalt('XBA_STUFE')))!=0?'on':'');
				$Form->Erstelle_Checkbox('XRC_BIT_'.$rsXRS->FeldInhalt('XRS_BIT'),$Wert,30,$EditRecht,'on');
				$Form->Erstelle_TextLabel($rsXRS->FeldInhalt('XRS_BESCHREIBUNG').'('.$rsXRS->FeldInhalt('XRS_BIT').')',450);
				$Form->ZeileEnde();

				$rsXRS->DSWeiter();
			}
		}

		$Form->Formular_Ende();
	}
}
catch (Exception $ex)
{
	if(is_object($Form))
	{
		$Form->Formular_Start();
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'WIEDERHOLEN',0);
		$Form->Formular_Ende();
	}
}
?>