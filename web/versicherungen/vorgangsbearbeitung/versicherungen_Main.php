<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');

    global $AWISCursorPosition;        // Aus AWISFormular

    try
    {
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        $AWISBenutzer = awisBenutzer::Init();
        echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) . ">";
    }
    catch(Exception $ex)
    {
        die($ex->getMessage());
    }

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('TITEL', 'tit_Versicherungen');

    $Form = new AWISFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    echo '<title>' . $AWISSprachKonserven['TITEL']['tit_Versicherungen'] . '</title>';

    ?>
</head>
<body>
<?php
echo '<script src="/jquery_ie11.js"></script>';
echo ' <script src="/jquery.scrollUp.min.js"></script>
<script src="/popup.js"></script>
<script src="/formularBereich.js"></script>
<script src="/jquery.tooltipster.js"></script>
<script src="/jquery-ui.js"></script>';

include("awisHeader3.inc");    // Kopfzeile

try
{
    $Form = new awisFormular();

    if($AWISBenutzer->HatDasRecht(9500) == 0)
    {
        $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200905061445");
        die();
    }

    $Register = new awisRegister(9500);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $Form->SetzeCursor($AWISCursorPosition);
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905061446");
    }
    else
    {
        echo 'AWIS: ' . $ex->getMessage();
    }
}
?>
</body>
</html>