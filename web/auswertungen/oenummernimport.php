<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>
<link rel="SHORTCUT ICON" href="/bilder/favicon.ico" />

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("phpCheckTime.class.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $_POST;
global $_GET;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $OEIZeile;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con, 506);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Oenummernimport',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

//$SQL = "Select AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW, AST_KENNUNG, WUG_WGR_ID, WUG_BEZEICHNUNG ";
$SQL = "Select * ";
$SQL .= "From OENUMMERNPREISIMPORT, OENUMMERNPREISIMPORTQUELLEN ";
$SQL .= "WHERE OIQ_NR = OEI_OIQ_NR ";
$SQL .= "Order by OEI_USERDAT DESC";
$rsOEI = awisOpenRecordset($con, $SQL);

if($rsOEI==FALSE)
{
	awisErrorMailLink("oenummernimport.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsOEIZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>Quelle</td>";
print "<td id=FeldBez>Dateiname</td>";
print "<td id=FeldBez>Stand</td>";
print "<td id=FeldBez>Import</td>";
print '<td id=FeldBez title="Importierte">Datens&auml;tze</td>';
print '<td id=FeldBez>Gesamt</td>';
print '<td id=FeldBez>Fehler-Code</td>';
print "</tr>";
	
for($OEIZeile=0;$OEIZeile<$rsOEIZeilen;$OEIZeile++)
{
	echo '<tr>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OIQ_QUELLE'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OEI_DATEINAME_ORIG'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OIQ_STAND'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OEI_USERDAT'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OEI_ANZAHL'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEI['OEI_GESAMTANZAHL'][$OEIZeile] . '</td>';
	echo '<td ' . (($OEIZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' title="'. htmlspecialchars($rsOEI['OEI_FEHLERTEXT'][$OEIZeile]) . '">' . $rsOEI['OEI_FEHLERCODE'][$OEIZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
unset($rsOEI);

if($_GET['ZurueckLink']=='')
{
	print "<br><hr><input type=image title=Zur&uuml;ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";
}
else
{
	print "<br><hr><input type=image title=Zur&uuml;ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $_GET['ZurueckLink'] . "';>";
}


?>

