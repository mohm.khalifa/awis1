<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10012 = $AWISBenutzer->HatDasRecht(10012);
	if($Recht10012==0)
	{
	    awisEreignis(3,1000,'Zukaufhersteller',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZLH_BEZEICHNUNG'] = $_POST['sucZLH_BEZEICHNUNG'];
		$Param['ZLH_KUERZEL'] = $_POST['sucZLH_KUERZEL'];
		$Param['ZLH_TECDOCID'] = $_POST['sucZLH_TECDOCID'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("FormularZLH",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{		
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
		include('./zukaufhersteller_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufhersteller_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
	}
	elseif(isset($_POST['cmdDSZurueck_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
		$SQL = 'SELECT ZLH_KEY';
		$SQL .= ' FROM Zukauflieferantenhersteller';
		$SQL .= ' WHERE ZLH_BEZEICHNUNG < ' .$DB->WertSetzen('ZLH','T',$_POST['txtZLH_BEZEICHNUNG']);
		$SQL .= ' ORDER BY nlssort(ZLH_BEZEICHNUNG,\'NLS_SORT=BINARY\') DESC';
		$rsZLH = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ZLH'));
		if(!$rsZLH->EOF())
		{
			$AWIS_KEY1 = $rsZLH->FeldInhalt('ZLH_KEY');
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
		$SQL = 'SELECT ZLH_KEY';
		$SQL .= ' FROM Zukauflieferantenhersteller';
		$SQL .= ' WHERE ZLH_BEZEICHNUNG > ' .$DB->WertSetzen('ZLH','T',$_POST['txtZLH_BEZEICHNUNG']);
		$SQL .= ' ORDER BY nlssort(ZLH_BEZEICHNUNG,\'NLS_SORT=BINARY\') ASC';
		$rsZLH = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ZLH'));
		if(!$rsZLH->EOF())
		{
			$AWIS_KEY1 = $rsZLH->FeldInhalt('ZLH_KEY');
		}
	}
	elseif(isset($_POST['cmdNeu_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['ZLH_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZLH_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('FormularZLH'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('FormularZLH',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY=$Param['ORDER'];		
		}
		else
		{
			$ORDERBY = ' ORDER BY ZLH_BEZEICHNUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	$Param['ORDER']=$ORDERBY;
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT ZukaufLieferantenHersteller.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ', (SELECT COUNT(*) FROM ZukaufArtikel WHERE zla_zlh_key = zlh_key) AS Anzahl';
	$SQL .= ' FROM ZukaufLieferantenHersteller';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
//$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('FormularZLH',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('ZLH',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

//$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZLH = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('ZLH'));
	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmzukaufhersteller action=./zukaufhersteller_Main.php?cmdAktion=Details method=POST>';

	if($rsZLH->EOF() AND !isset($_POST['cmdNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZLH->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$AWISBenutzer->ParameterSchreiben('FormularZLH',serialize($Param));
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

		$Link .= '&Sort=ZLH_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_BEZEICHNUNG'],400,'',$Link);
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],100,'',$Link);
		$Link .= '&Sort=ZLH_TECDOCID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_TECDOCID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_TECDOCID'],200,'',$Link);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ANZAHL_ARTIKEL'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZLH->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$Link = './zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY=0'.$rsZLH->FeldInhalt('ZLH_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#ZLH_BEZEICHNUNG',$rsZLH->FeldInhalt('ZLH_BEZEICHNUNG'),0,400,false,($DS%2),'',$Link);
			$Link='/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsZLH->FeldInhalt('ZLH_LIE_NR');
			$Form->Erstelle_ListenFeld('#ZLH_KUERZEL',$rsZLH->FeldInhalt('ZLH_KUERZEL'),0,100,false,($DS%2));
			$Form->Erstelle_ListenFeld('#ZLH_TECDOCID',$rsZLH->FeldInhalt('ZLH_TECDOCID'),0,200,false,($DS%2));
			$Form->Erstelle_ListenFeld('#ANZAHL',$rsZLH->FeldInhalt('ANZAHL'),0,100,false,($DS%2),'','','N0T');
			$Form->ZeileEnde();

			$rsZLH->DSWeiter();
			$DS++;
		}

		$Link = './zukaufhersteller_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZLH->FeldInhalt('ZLH_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('FormularZLH',serialize($Param));

		$Form->Erstelle_HiddenFeld('ZLH_KEY',$AWIS_KEY1);
		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufhersteller_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZLH->FeldInhalt('ZLH_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZLH->FeldInhalt('ZLH_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10012&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10012&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLH']['ZLH_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('!ZLH_BEZEICHNUNG',$rsZLH->FeldInhalt('ZLH_BEZEICHNUNG'),50,370,$EditRecht);
		$AWISCursorPosition='txtZLH_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'].':',200);
		$Form->Erstelle_TextFeld('!ZLH_KUERZEL',$rsZLH->FeldInhalt('ZLH_KUERZEL'),4,370,$EditRecht,'','','','T','L','','',3);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLH']['ZLH_TECDOCID'].':',200);
		$Form->Erstelle_TextFeld('ZLH_TECDOCID',$rsZLH->FeldInhalt('ZLH_TECDOCID'),6,370,$EditRecht,'','','','N0','L','','',5);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLH']['ZLH_BEMERKUNG'].':',200);
		$Form->Erstelle_Textarea('ZLH_BEMERKUNG',$rsZLH->FeldInhalt('ZLH_BEMERKUNG'),600,100,3,$EditRecht);
		$Form->ZeileEnde();

		if(!isset($_POST['cmdDSNeu_x']))
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Lieferanten'));
			echo '<input type=hidden name=Seite value='.$DB->FeldInhaltFormat('T',$RegisterSeite,'DB',false).'>';

			$Reg = new awisRegister(10013);
			$Reg->ZeichneRegister($RegisterSeite);
		}
		
		$Form->ZeileEnde();
		
		
		
		
		$Form->Formular_Ende();
	}
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10012&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10012&4)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	
	if(($Recht10012&8)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}

	if(!isset($_POST['cmdDSNeu_x'])  AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
		$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZLH_KEY = '.$DB->WertSetzen('ZLH','N0',floatval($AWIS_KEY1));
		return $Bedingung;
	}

	if(isset($Param['ZLH_BEZEICHNUNG']) AND $Param['ZLH_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND (UPPER(ZLH_BEZEICHNUNG) ' . $DB->LIKEoderIST($Param['ZLH_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZLH_KUERZEL']) AND $Param['ZLH_KUERZEL']!='')
	{
		$Bedingung .= ' AND (UPPER(ZLH_KUERZEL) ' . $DB->LIKEoderIST($Param['ZLH_KUERZEL'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZLH_TECDOCID']) AND $Param['ZLH_TECDOCID']!='')
	{
		$Bedingung .= ' AND (UPPER(ZLH_TECDOCID) ' . $DB->LIKEoderIST($Param['ZLH_TECDOCID'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>