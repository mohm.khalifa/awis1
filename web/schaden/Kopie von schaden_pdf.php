<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?php
require_once('register.inc.php');
require_once('db.inc.php');		// DB-Befehle
require_once('sicherheit.inc.php');
require_once('fpdi.php');

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3300);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Schaden-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

var_dump($_REQUEST);

if ($_REQUEST['BEARB_NR']!='' && ($RechteStufe&16)==16)
{
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE";
	$SQL.=" LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE AA ON AA.ID = BEARBEITER";
	$SQL.=" LEFT JOIN EINGANGPER@SCHAD.ATU.DE BB ON BB.ID = EINGANGPER";
	//$SQL.=" WHERE BEARBEITUNGSNR='080900001'";//".$_REQUEST['BEARB_NR'];
	$SQL.=" WHERE BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";
	
	var_dump($SQL);	
	//die();
	
	$rsResult = awisOpenRecordset($con,$SQL);
	$rsResultZeilen = $awisRSZeilen;		
	
	var_dump($rsResultZeilen);
	die();
	
	if($rsResultZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten f�r Bearbeitungs-Nr.: ".$_REQUEST['BEARB_NR']." gefunden!</span></center>");
	}
	else
	{
		$BEARBEITER=$rsResult['SBNAME'][0];		
		$BEARBEITUNGSNR=$rsResult['BEARBEITUNGSNR'][0];
		$EINGABEAM=$rsResult['EINGABEAM'][0];
		$EINGANGPER=$rsResult['MITTEL'][0];
//		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',chr(13).chr(10),$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',' ',$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FILNR=$rsResult['FILNR'][0];
		$GESCHLECHT=$rsResult['GESCHLECHT'][0];
		$KFZ_KENNZ=$rsResult['KFZ_KENNZ'][0];
		$KUNDENNAME=$rsResult['KUNDENNAME'][0];
		$PLZ=$rsResult['PLZ'][0];
		$ORT=$rsResult['ORT'][0];
		$STRASSE=$rsResult['STRASSE'][0];
		$TELEFON=$rsResult['TELEFON'][0];
		$TELEFON2=$rsResult['TELEFON2'][0];
		$FAX=$rsResult['FAX'][0];
		$WANR=$rsResult['WANR'][0];
		$PAN=$rsResult['PAN'][0];
		$EMAIL=$rsResult['EMAIL'][0];
		$VORNAME=$rsResult['VORNAME'][0];		
	}
	
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
		
		define('FPDF_FONTPATH','font/');
		$pdf = new fpdi('p','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		$ATULogo = $pdf->ImportPage(1);					
	
		$Protokolltext1='Sehr geehrte Gesch�ftsleiterin / sehr geehrter Gesch�ftlsleiter,';
	//	$Protokolltext2='von unserem gemeinsamen Kunden erhielten wir folgende Beschwerde / Nachfrage.';
		$Protokolltext3='Bitte setzen Sie sich sofort zur Kl�rung des Falles mit dem Kunden in Verbindung!';
		$Protokolltext4='Sehr geehrte(r) Gesch�ftsleiter(in),';
		$Protokolltext5='wir halten was wir versprechen. Dies gilt f�r Preisw�rdigkeit, Produkt- und Servicequalit�t';
		$Protokolltext6='genauso wie f�r Termintreue. Jede Beschwerde ist f�r ATU - f�r Ihre Filiale vor Ort - eine';
		$Protokolltext7='gute Chance, einen neuen Stammkunden zu gewinnen und langfristig zu binden.';
		$Protokolltext8='Bitte setzen Sie sich zur Kl�rung der oben genannten Angelegenheit / Beschwerde';
		$Protokolltext9='sp�testens bis zu Ihrem Gesch�ftsschluss mit dem Kunden in Verbindung!';
		$Protokolltext10='NEU:';
		$Protokolltext11='R�ckmeldungen bzw. den aktuellen Bearbeitungsstatus geben Sie per EDV im Programm';
		$Protokolltext17='AWIS unter dem Men�punkt Beschwerdeverwaltung ein.';
		$Protokolltext18='(Anleitung siehe Filialinfo KW 50/2007 - Leitfaden Beschwerder�ckmeldung per EDV)';
		$Protokolltext12='Sollten Sie zur Bearbeitung der Kundenbeschwerde Unterst�tzung ben�tigen, setzen Sie';
		$Protokolltext13='sich mit Ihrem Gebietsleiter oder der Abteilung Qualit�tsmanagement Beschwerden';
		$Protokolltext14='in Verbindung.';
		$Protokolltext15='Freundliche Gr��e';
		$Protokolltext16='Qualit�tsmanagement';
		
		$Zeile=0;
		$Seitenrand=15;
		
		NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true);
		
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;		
		
		$Y_Wert=$Y_Wert+5;
					
		$Y_Wert=$Y_Wert+18;
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(130,6,"Kundendaten",1,0,'L',1);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(50,6,"Bearbeitungsdaten",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+6;
		$pdf->SetFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Name:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$KUNDENNAME,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"WA-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$WANR,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Bearb-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$BEARBEITUNGSNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Vorname :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$VORNAME,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"Fz.-Kennz.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$KFZ_KENNZ,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Sachbearb.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$BEARBEITER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Stra�e :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$STRASSE,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"ATU-Card:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Filial-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$FILNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"PLZ / Ort :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$PLZ." ".$ORT,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,$PAN,0,0,'L',0);
	
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Erfasst am:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$EINGABEAM,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Tel. 1:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$TELEFON,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"FAX:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$FAX,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Eingang per:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$EINGANGPER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Tel. 2:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$TELEFON2,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"E-Mail:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$EMAIL,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+6;
		
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,"Beschwerde / Kommentar",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+7;
		$pdf->SetFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->MultiCell(180,4,$FEHLERBESCHREIBUNG,0,'N',0);
		
		$bla=$pdf->getStringWidth($FEHLERBESCHREIBUNG);
		$Hoehe=round($bla/46+0.4,0);
		
		$Y_Wert=$Y_Wert+$Hoehe+6;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,"",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+12;
		
		$pdf->SetFont('Arial','',12);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext4,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+10;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext5,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext6,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext7,0,0,'L',0);
		
		$pdf->SetFont('Arial','B',12);
		$Y_Wert=$Y_Wert+15;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext8,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext9,0,0,'L',0);
				
		$Y_Wert=$Y_Wert+15;
		$pdf->Line($LinkerRand,$Y_Wert,$LinkerRand+180,$Y_Wert);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext10,0,0,'L',0);
		
		$pdf->SetFont('Arial','',12);
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext11,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext17,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext18,0,0,'L',0);
		
		$pdf->Line($LinkerRand,$Y_Wert+5,$LinkerRand+180,$Y_Wert+5);
		$pdf->Line($LinkerRand,$Y_Wert+5,$LinkerRand,$Y_Wert-15);
		$pdf->Line($LinkerRand+180,$Y_Wert+5,$LinkerRand+180,$Y_Wert-15);
		
		$Y_Wert=$Y_Wert+15;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext12,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext13,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext14,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+15;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext15,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+10;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,$Protokolltext16,0,0,'L',0);
								
		
		$DateiName = awis_UserExportDateiName('.pdf');
		$DateiNameLink = pathinfo($DateiName);
		$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		$pdf->saveas($DateiName);
		
		echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
		echo "<hr><a href=./schaden_Main.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
		
}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Christian Argauer
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool �berschrift
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+160,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
	
	
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"ATU Qualit�tsmanagement - Beschwerdeprotokoll -",0,0,'C',0);
	
	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	
	$Zeile = 22;
}	
	
	
?>
</body>
</html>
