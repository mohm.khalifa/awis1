<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
	$TextKonserven[]=array('BES','*');
	//$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	//$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	//$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht39003 = $AWISBenutzer->HatDasRecht(39003);

	if($Recht39003==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

		$Form->SchreibeHTMLCode('<form name=frmSchaden method=post action=./schaden_Main.php?cmdAktion=Details>');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES'));

		$Form->Formular_Start();


		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='') // Wenn GL
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VON'] . ':', 200);
			$Form->Erstelle_TextFeld('*BES_DATUMVON',$Param['SPEICHERN'] == 'on' ? $Param['BES_DATUMVON']: '', 10, 120, true, '', '', '', 'D', 'L');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BIS'], 30);
			$Form->Erstelle_TextFeld('*BES_DATUMBIS', $Param['SPEICHERN'] == 'on' ? $Param['BES_DATUMBIS']: '', 10, 120, true, '', '', '', 'D', 'L');
			$Form->ZeileEnde();
			$AWISCursorPosition = 'sucBES_DATUMVON';
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
			$Form->Erstelle_TextFeld('*BES_FIL_ID',($Param['SPEICHERN']=='on'?$Param['BES_FIL_ID']:''), 1,150, true, '', '', '', 'T', 'L','');
			$Form->ZeileEnde();
		}
		else 
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
			$Form->Erstelle_TextFeld('BES_FIL_ID', $FilZugriff, 1,150, false, '', '', '', 'T', 'L','');
			$Form->Erstelle_HiddenFeld('BES_FIL_ID',$FilZugriff);
			$Form->ZeileEnde();
		}
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARBEITUNG'].':',200);
		$Form->Erstelle_TextFeld('*BES_BEARBEITUNG',($Param['SPEICHERN']=='on'?$Param['BES_BEARBEITUNG']:''),10,180,true,'','','','T','L','','',10);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='') // Wenn GL
		{
			$AWISCursorPosition = 'sucBES_BEARBEITUNG';
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORGANG'].':',200);
		$Daten = explode('|',$AWISSprachKonserven['BES']['lst_VORGANG']);
		$Form->Erstelle_SelectFeld('*BES_VORGANG',($Param['SPEICHERN']=='on'?$Param['BES_VORGANG']:''),150,true,'','','','','',$Daten);
		$Form->ZeileEnde();
		
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='') // Wenn GL
		{
			$SQL ='select ID,WERT from Antragart where id in (10,3,18,17) order by WERT';
		}
		else 
		{
			$SQL ='select ID,WERT from Antragart order by WERT';
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AART'] . ':',200);
		$Form->Erstelle_SelectFeld('*BES_AART',$Param['SPEICHERN']=='on'?$Param['BES_AART']:'',120,true,$SQL,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='') // Wenn GL
		{
			$Form->ZeileStart();
			$Form->SchreibeHTMLCode('<hr noshade width="600" size="1" align="left" color="black">');
			$Form->ZeileEnde();		
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KUNDE'].':',200);
			$Form->Erstelle_TextFeld('*BES_KUNDE',($Param['SPEICHERN']=='on'?$Param['BES_KUNDE']:''),30,180,true,'','','','T','L','','',30);
			$Form->ZeileEnde();	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STRASSE'].':',200);
			$Form->Erstelle_TextFeld('*BES_STRASSE',($Param['SPEICHERN']=='on'?$Param['BES_STRASSE']:''),30,180,true,'','','','T','L','','',30);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PLZ'].':',200);
			$Form->Erstelle_TextFeld('*BES_PLZ',($Param['SPEICHERN']=='on'?$Param['BES_PLZ']:''),6,180,true,'','','','T','L','','',6);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ORT'].':',200);
			$Form->Erstelle_TextFeld('*BES_ORT',($Param['SPEICHERN']=='on'?$Param['BES_ORT']:''),10,180,true,'','','','T','L','','',10);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON'].':',200);
			$Form->Erstelle_TextFeld('*BES_TELEFON',($Param['SPEICHERN']=='on'?$Param['BES_TELEFON']:''),10,180,true,'','','','T','L','','',10);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EMAIL'].':',200);
			$Form->Erstelle_TextFeld('*BES_EMAIL',($Param['SPEICHERN']=='on'?$Param['BES_EMAIL']:''),30,180,true,'','','','T','L','','',30);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KFZKZ'].':',200);
			$Form->Erstelle_TextFeld('*BES_KFZ','',15,0,true,'','','','T','L','','',15,'','f');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ATUCARD'].':',200);
			$Form->Erstelle_TextFeld('*BES_ATUCARD',($Param['SPEICHERN']=='on'?$Param['BES_ATUCARD']:''),30,180,true,'','','','T','L','','',30);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SACHBEARBEITER'].':',200);
			$Form->Erstelle_TextFeld('*BES_SACHBEARBEITER',($Param['SPEICHERN']=='on'?$Param['BES_SACHBEARBEITER']:''),30,180,true,'','','','T','L','','',30);
			$Form->ZeileEnde();	
		}
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/schaden_neu/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		if(($Recht39003&4)==4)
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
		$Form->SchreibeHTMLCode('</form>');
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>