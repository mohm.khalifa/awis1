<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awis_portalzugaenge.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PZU','%');
	$TextKonserven[]=array('PER','%');
	$TextKonserven[]=array('KKO','KKO_WERT_9');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Liste','lst_JaNein');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Param = array();
	$AWIS_KEY1=0;

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht2100 = $AWISBenutzer->HatDasRecht(2100);
	if($Recht2100==0)
	{
	    awisEreignis(3,1000,'PZU',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$Portal = new awis_Portalzugaenge();

	$DetailAnsicht=false;
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['PZU_BENUTZERNAME'] = $_POST['sucPZU_BENUTZERNAME'];
		$Param['PZU_KENNWORT'] = (isset($_POST['sucPZU_KENNWORT'])?$_POST['sucPZU_KENNWORT']:'');
		$Param['PZU_PERSNR'] = (isset($_POST['sucPZU_PERSNR'])?$_POST['sucPZU_PERSNR']:'');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./portalzugaenge_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param,$Recht2100);

	$SQL = 'SELECT portalzugaenge.*';
	$SQL .= ' FROM portalzugaenge';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	$rsPZU = $DB->RecordsetOeffnen($SQL);
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmportalzugaenge action=./portalzugaenge_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST>';
	$KennwortOK = (($Recht2100&48)?true:($Portal->KennwortPruefen($rsPZU->FeldInhalt('PZU_KENNWORT'),(isset($Param['PZU_KENNWORT'])?$Param['PZU_KENNWORT']:''))));

	if($AWIS_KEY1==0 AND (($rsPZU->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	 OR ($KennwortOK==false)))
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	else
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsPZU->FeldInhalt('PZU_KEY');

		$EditRecht = (($Recht2100&6)>0);

		$Param['KEY']=$AWIS_KEY1;

		echo '<input type=hidden name=txtPZU_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPZU->FeldInhalt('PZU_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPZU->FeldInhalt('PZU_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_BENUTZERNAME'].':',150);
		$Form->Erstelle_TextFeld('PZU_BENUTZERNAME',$rsPZU->FeldInhalt('PZU_BENUTZERNAME'),50,300,($rsPZU->FeldInhalt('PZU_BENUTZERNAME')==''?$EditRecht:false),'','','','T');
		$AWISCursorPosition = 'txtPZU_BENUTZERNAME';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_KENNWORT'].':',150);
		$Form->Erstelle_TextFeld('PZU_BENUTZERKENNWORT1','********',20,200,$EditRecht,'','','','P');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['KennwortWiederholen'].':',200);
		$Form->Erstelle_TextFeld('PZU_BENUTZERKENNWORT2','********',20,200,$EditRecht,'','','','P');
		if($rsPZU->FeldInhalt('PZU_KENNWORTABLAUF')!='')
		{
			$Form->Erstelle_TextLabel(str_replace('$1',$Form->Format('D',$rsPZU->FeldInhalt('PZU_KENNWORTABLAUF')),$AWISSprachKonserven['PZU']['KennwortAblaufHinweis']),400);
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_PERSNR'].':',150);
		$Form->Erstelle_TextFeld('PZU_PERSNR',$rsPZU->FeldInhalt('PZU_PERSNR'),10,200,(($Recht2100&32)==0?false:$EditRecht),'','','','T');
		$Form->ZeileEnde();

		if(($Recht2100&48)!=0 AND $rsPZU->FeldInhalt('PZU_TYP')>=5)
		{
			$SQL = 'SELECT * FROM PERSONAL_KOMPLETT WHERE PERSNR = '.$DB->FeldInhaltFormat('T',$rsPZU->FeldInhalt('PZU_PERSNR'));
			$rsPER = $DB->RecordSetOeffnen($SQL);
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PER']['NAME'].':',150);
			$Form->Erstelle_TextFeld('#PER_NAME',$rsPER->FeldInhalt('NAME'),10,200,false,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PER']['VORNAME'].':',150);
			$Form->Erstelle_TextFeld('#PER_VORNAME',$rsPER->FeldInhalt('VORNAME'),10,200,false,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PER']['STRASSE'].':',150);
			$Form->Erstelle_TextFeld('#PER_STRASSE',$rsPER->FeldInhalt('STRASSE'),10,200,false,'','','','T');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PER']['PLZ'].':',150);
			$Form->Erstelle_TextFeld('#PER_PLZ',$rsPER->FeldInhalt('PLZ'),10,150,false,'','','','T');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PER']['ORT'].':',100);
			$Form->Erstelle_TextFeld('#PER_ORT',$rsPER->FeldInhalt('ORT'),10,200,false,'','','','T');
			$Form->ZeileEnde();
			
			// Durchwahl suchen
			$SQL = 'SELECT kon_key, kko_wert';
			$SQL .= ' FROM kontakte';
			$SQL .= ' INNER JOIN kontaktekommunikation ON kko_kon_key = kon_key AND kko_kot_key = 9';
			$SQL .= ' WHERE kon_per_nr = '.$DB->FeldInhaltFormat('T',$rsPZU->FeldInhalt('PZU_PERSNR'));
		
			$rsKKO = $DB->RecordSetOeffnen($SQL);
			if(!$rsKKO->EOF())
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KKO']['KKO_WERT_9'].':',150);
				$Form->Erstelle_TextFeld('#KKO_WERT',$rsKKO->FeldInhalt('KKO_WERT'),10,200,false,'','','','T');
				$Form->ZeileEnde();
			}
		}

		if($rsPZU->FeldInhalt('PZU_TYP')>=10 OR $rsPZU->EOF())		// Standarduser mit Kennwort
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_GEBDATUM'].':',150);
			$Form->Erstelle_TextFeld('PZU_GEBDATUM',$rsPZU->FeldInhalt('PZU_GEBDATUM'),10,200,(($Recht2100&32)==0?false:$EditRecht),'','','','D');
			$Form->ZeileEnde();
		}
		
		if($rsPZU->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_TYP'].':',150);
			$Daten = explode('|',$AWISSprachKonserven['PZU']['lst_PZU_TYP']);
			$Form->Erstelle_SelectFeld('PZU_TYP',$rsPZU->FeldInhalt('PZU_TYP'),100,$EditRecht,'','','','','',$Daten);
			$Form->ZeileEnde();
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_EMAIL'].':',150);
		$Form->Erstelle_TextFeld('PZU_EMAIL',$rsPZU->FeldInhalt('PZU_EMAIL'),60,400,(($Recht2100&32)==0?false:$EditRecht),'','','','T');
		if($rsPZU->FeldInhalt('PZU_EMAIL')!='')
		{
			$Link = 'mailto:'.$rsPZU->FeldInhalt('PZU_EMAIL');
			$Form->Erstelle_Bild(awisFormular::BILD_TYP_HREF,'maillink',$Link,'/bilder/icon_mail.png','','',12,12,16,'NeuesFenster');
		}
		$Form->ZeileEnde();

		if(($Recht2100&32)!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_LETZTERZUGRIFF'].':',150);
			$Form->Erstelle_TextFeld('PZU_EMAIL',$rsPZU->FeldInhalt('PZU_LETZTERZUGRIFF'),60,400,false,'','','','DU');
			$Form->ZeileEnde();
		}

		if(($Recht2100&48)!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_SPERRE'].':',150);
			$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
			$Form->Erstelle_SelectFeld('PZU_SPERRE',$rsPZU->FeldInhalt('PZU_SPERRE'),100,$EditRecht,'','','','','',$Daten);
			$Form->ZeileEnde();
		}

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['PZU']['PZU_NEWSLETTER'].':',150);
        $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
        $Form->Erstelle_SelectFeld('PZU_NEWSLETTER',$rsPZU->FeldInhalt('PZU_NEWSLETTER'),100,$EditRecht,'','','','','',$Daten);
        $Form->ZeileEnde();

		$Form->Formular_Ende();

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht2100&6)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht2100&36)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	/*
	if((($Recht2100&8)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}
	*/

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		@file_put_contents('/tmp/portalzugaenge_Details.err',$AWISBenutzer->BenutzerName()."\n".serialize($SQL)."\n\n",FILE_APPEND);
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151350");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151351");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param,$Recht2100)
{
	global $AWIS_KEY1;
	global $DB;
	global $AWISBenutzer;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PZU_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(($Recht2100&16)!=0 AND isset($Param['PZU_PERSNR']) AND $Param['PZU_PERSNR']!='')	// Admins alle
	{
		$Bedingung .= ' AND (PZU_PERSNR) =' . $DB->FeldInhaltFormat('T',$Param['PZU_PERSNR']) . '';
	}
	if(($Recht2100&32)==0)		// Nur SuperAdmins d�rfen interne Accounts verwalten
	{
		$Bedingung .= ' AND PZU_TYP = 10';
	}
	
	if(isset($Param['PZU_BENUTZERNAME']) AND $Param['PZU_BENUTZERNAME']!='')
	{
		$Bedingung .= ' AND (UPPER(PZU_BENUTZERNAME) =' . $DB->FeldInhaltFormat('TU',$Param['PZU_BENUTZERNAME']).')';
	}
	elseif(($Recht2100&48)==0 OR $Bedingung=='')
	{
		$Bedingung .= ' AND PZU_BENUTZERNAME = \'##++##++##\'';
	}

	return $Bedingung;
}



?>