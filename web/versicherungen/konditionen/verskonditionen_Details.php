<?php
global $VKD;

$DetailAnsicht = false;

$VKD->Form->DebugAusgabe(1, $_POST);
$VKD->Form->DebugAusgabe(1, $_GET);

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$VKD->AWIS_KEY1 = '';

if (isset($_GET['VKR_KEY'])) {
    $VKD->AWIS_KEY1 = $_GET['VKR_KEY'];
} elseif (isset($_POST['txtVKR_KEY'])) {
    $VKD->AWIS_KEY1 = $_POST['txtVKR_KEY'];
} elseif  (isset($_GET['VKD_KEY'])) {
    $VKD->AWIS_KEY2 = $_GET['VKD_KEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $VKD->AWIS_KEY1 = -1;
}
if ($VKD->AWIS_KEY1 != '') {
    $VKD->Param['KEY'] = $VKD->AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
}

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $VKD->Param = array();
    $VKD->Param['VERS_KONZERN'] = isset($_POST['txtVERS_KONZERN'])?$_POST['txtVERS_KONZERN']:'';
    $VKD->Param['VKD_GUELTIG_AB'] = $_POST['txtVKD_GUELTIG_AB'];
    $VKD->Param['VKD_GUELTIG_BIS'] = $_POST['txtVKD_GUELTIG_BIS'];
    $VKD->Param['VKD_SKZ'] = $_POST['txtVKR_SKZ'];

    $VKD->Param['KEY'] = '';
    $VKD->Param['WHERE'] = '';
    $VKD->Param['ORDER'] = '';
    $VKD->Param['BLOCK'] = 1;
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./verskonditionen_speichern.php');
}
// //*********************************************************
// //* SQL Vorbereiten: Sortierung
// //*********************************************************


//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $VKD->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL = 'SELECT VKR.*, VVE.*';
$SQL .= ' FROM VERSKONDREF VKR';
$SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
$SQL .= ' ON VKR.VKR_VVE_KEY = VVE.VVE_KEY';
if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}
$SQL .= ' ORDER BY VVE_KUERZEL,VVE_VERSNR,VKR_SKZ,VKR_GUELTIG_AB';

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************

$rsVKD = $VKD->DB->RecordsetOeffnen($SQL, $VKD->DB->Bindevariablen('VKD', true));
$VKD->Form->DebugAusgabe(1, $VKD->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$VKD->Form->Formular_Start();
echo '<form name=frmverskonditionen action=./verskonditionen_Main.php?cmdAktion=Details method=POST>';

if ($rsVKD->FeldInhalt('VKR_KEY') != '') {
    $VKD->AWIS_KEY1 = $rsVKD->FeldInhalt('VKR_KEY');
}

$VKD->Form->Erstelle_HiddenFeld('VKR_KEY', $VKD->AWIS_KEY1);

$schadenKZ = $rsVKD->FeldOderPOST('VKR_SKZ');

$EditRecht = (($VKD->Recht9400 & 2) != 0);

$FeldBreiten = array();
$FeldBreiten['POSITION'] = 70;
$FeldBreiten['ARTNR'] = 100;
$FeldBreiten['PREIS_NETTO'] = 100;
$FeldBreiten['PREIS_BRUTTO'] = 100;
$FeldBreiten['GUELTIG_AB'] = 100;
$FeldBreiten['GUELTIG_BIS'] = 100;
$FeldBreiten['VSTABZUG'] = 300;
$FeldBreiten['STEUERSATZ'] = 80;
$FeldBreiten['HOECHSTGRENZE'] = 150;
$FeldBreiten['MIN_AUSBUCHUNG'] = 100;
$FeldBreiten['MAX_AUSBUCHUNG'] = 100;
$FeldBreiten['SB'] = 100;
$FeldBreiten['KOSTENLOS'] = 280;
$FeldBreiten['ICONS'] = 60;

if ($rsVKD->EOF() and $VKD->AWIS_KEY1 == '') { // Nichts gefunden
    $VKD->Form->Hinweistext($VKD->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsVKD->AnzahlDatensaetze() > 1) { // mehrere Datens�tze -> Liste
    $FeldBreiten = array();
    $FeldBreiten['VVE_KUERZEL'] = 100;
    $FeldBreiten['VVE_VERSNR'] = 100;
    $FeldBreiten['VVE_BEZEICHNUNG'] = 350;
    $FeldBreiten['VKR_SKZ'] = 200;
    $FeldBreiten['VKR_GUELTIG_AB'] = 100;
    $FeldBreiten['VKR_GUELTIG_BIS'] = 100;

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVE']['VVE_KUERZEL'], $FeldBreiten['VVE_KUERZEL']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVE']['VVE_VERSNR'], $FeldBreiten['VVE_VERSNR']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVE']['VVE_VERSBEZ'], $FeldBreiten['VVE_BEZEICHNUNG']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VKR']['VKR_SKZ'], $FeldBreiten['VKR_SKZ']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VKR']['VKR_GUELTIG_AB'], $FeldBreiten['VKR_GUELTIG_AB']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VKR']['VKR_GUELTIG_BIS'], $FeldBreiten['VKR_GUELTIG_BIS']);
    $VKD->Form->ZeileEnde();

    $DS = 0;
    while(!$rsVKD->EOF()) {
        $HG = ($DS % 2);
        $VKD->Form->ZeileStart();
        $Link = './verskonditionen_Main.php?cmdAktion=Details&VKR_KEY=0' . $rsVKD->FeldInhalt('VKR_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $VKD->Form->Erstelle_ListenFeld('VVE_KUERZEL', $rsVKD->FeldInhalt('VVE_KUERZEL'), 0, $FeldBreiten['VVE_KUERZEL'], false, $HG, '', $Link, 'T', 'L',$rsVKD->FeldInhalt('VVE_KUERZEL'));
        $VKD->Form->Erstelle_ListenFeld('VVE_VERSNR', $rsVKD->FeldInhalt('VVE_VERSNR'), 0, $FeldBreiten['VVE_VERSNR'], false, $HG, '', '', 'T', 'L',$rsVKD->FeldInhalt('VVE_VERSNR'));
        $VKD->Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVKD->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VVE_BEZEICHNUNG'], false, $HG, '', '', 'T', 'L',$rsVKD->FeldInhalt('VVE_BEZEICHNUNG'));
        $VKD->Form->Erstelle_ListenFeld('VKR_SKZ', $rsVKD->FeldInhalt('VKR_SKZ'), 0, $FeldBreiten['VKR_SKZ'], false, $HG, '', '', 'T', 'L',$rsVKD->FeldInhalt('VKR_SKZ'));
        $VKD->Form->Erstelle_ListenFeld('VKR_GUELTIG_AB', $rsVKD->FeldInhalt('VKR_GUELTIG_AB'), 0, $FeldBreiten['VKR_GUELTIG_AB'], false, $HG, '', '', 'T', 'L',$rsVKD->FeldInhalt('VKR_GUELTIG_AB'));
        $VKD->Form->Erstelle_ListenFeld('VKR_GUELTIG_BIS', $rsVKD->FeldInhalt('VKR_GUELTIG_BIS'), 0, $FeldBreiten['VKR_GUELTIG_BIS'], false, $HG, '', '', 'T', 'L',$rsVKD->FeldInhalt('VKR_GUELTIG_BIS'));
        $VKD->Form->ZeileEnde();

        $rsVKD->DSWeiter();
        $DS++;
    }

} else { // Details
    if($rsVKD->FeldInhalt('VKR_KEY')!=''){
        $VKD->AWIS_KEY1 = $rsVKD->FeldInhalt('VKR_KEY');
    }
    $VKD->Form->Erstelle_HiddenFeld('VKR_KEY', $VKD->AWIS_KEY1);

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./verskonditionen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $VKD->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVKD->FeldInhalt('VKD_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVKD->FeldInhalt('VKD_USERDAT'));
    $VKD->Form->InfoZeile($Felder, '');

    $VKD->Form->FormularBereichStart();
    $VKD->Form->FormularBereichInhaltStart('Versicherungsdaten', true);

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVS']['VVS_VERSICHERUNG']. ':', 170);
    $SQL = 'SELECT VVE_VERSNR, VVE_KUERZEL || \' - \' || VVE_VERSNR || \' - \' || VVE_BEZEICHNUNG FROM VERSVERSICHERUNGEN ORDER BY VVE_KUERZEL, VVE_VERSNR';
    $VKD->Form->Erstelle_SelectFeld('VVE_VERSNR', $rsVKD->FeldOderPOST('VVE_VERSNR'), '600', $EditRecht, $SQL, '~' . $VKD->AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', '', [], 'height:24px');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVE']['VVE_VERSNR']. ':', 170);
    $VKD->Form->Erstelle_TextFeld('VVE_VERSNR', $rsVKD->FeldOderPOST('VVE_VERSNR'), 30, 150, false, '', '', '', 'T');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVE']['VVE_KUERZEL'] . ':', 170);
    $VKD->Form->Erstelle_TextFeld('VVE_KUERZEL', $rsVKD->FeldOderPOST('VVE_KUERZEL'), 30, 150, false, '', '', '', 'T', '', '');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVS']['VVS_VERSICHERUNG'] . ':', 170);
    $VKD->Form->Erstelle_TextFeld('VVE_BEZEICHNUNG', $rsVKD->FeldOderPOST('VVE_BEZEICHNUNG'), 100, 300, false, '', '', '', 'T', '', '');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVE']['VVE_AKTIV'] . ':', 170);
    $VKD->Form->Erstelle_TextFeld('VVE_AKTIV', $rsVKD->FeldOderPOST('VVE_AKTIV') == ('ja' || '1') ? 'ja' : 'nein', 30, 150, false, '', '', '', 'T', '', '');
    $VKD->Form->ZeileEnde();

    if ($rsVKD->FeldOderPOST('VVE_VERARBWEG') != '') {
        $VKD->Form->ZeileStart();
        $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVE']['VVE_VERARBWEG'] . ':', 170);
        $VKD->Form->Erstelle_TextFeld('VVE_VERARBWEG', $rsVKD->FeldOderPOST('VVE_VERARBWEG'), 30, 150, false, '', '', '', 'T', '', '');
        $VKD->Form->ZeileEnde();
    }
    $VKD->Form->FormularBereichInhaltEnde();
    $VKD->Form->FormularBereichEnde();


    $VKD->Form->FormularBereichStart();
    switch ($schadenKZ) {
        case 'S':
            $UeberschriftFormularBereich = 'Steinschlagkondition(en)';
            break;
        case 'P':
            $UeberschriftFormularBereich = 'Parkschadenkondition(en)';
            break;
        default:
            $UeberschriftFormularBereich = 'Kondition(en)';
    }
    $VKD->Form->FormularBereichInhaltStart($UeberschriftFormularBereich, true);

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_AB'] . ':', 170);
    $VKD->Form->Erstelle_TextFeld('VKR_GUELTIG_AB', $rsVKD->FeldOderPOST('VKR_GUELTIG_AB'), 10, 150, $EditRecht, '', '', '', 'D', '', 'dd.mm.yyyy z. B. 01.01.2017', '', 0, '', '', 'pattern=^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)[0-9]{2}$');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_BIS'] . ':', 170);
    $VKD->Form->Erstelle_TextFeld('VKR_GUELTIG_BIS', $rsVKD->FeldOderPOST('VKR_GUELTIG_BIS'), 10, 150, $EditRecht, '', '', '', 'D', '', 'dd.mm.yyyy z. B. 01.01.2017', '', 0, '', '', 'pattern=^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)[0-9]{2}$');
    $VKD->Form->ZeileEnde();

    $konditionVorsteuerabzug = false;

    $SQL = 'SELECT DISTINCT VKD_VSTABZUG ';
    $SQL .= ' FROM VERSKONDITIONEN ';
    $SQL .= ' WHERE VKD_VKR_KEY = ' . $VKD->DB->WertSetzen('VKD', 'Z', $VKD->AWIS_KEY1);
    $SQL .= ' AND VKD_VSTABZUG = 1';

    if ($VKD->DB->ErmittleZeilenAnzahl($SQL, $VKD->DB->Bindevariablen('VKD')) > 0) {
        $konditionVorsteuerabzug = true;
    }
    $VKD->ErstellePosUeberschrift($schadenKZ, $FeldBreiten, $EditRecht, $konditionVorsteuerabzug);

    $SQL = '';

    $POS = 1;

    if($schadenKZ == 'S') {
        $maxPos = 3;
    } else {
        $maxPos = 1;
    }

    $rsKondDaten = $VKD->KonditionsDaten();

    $VKD->Form->Erstelle_HiddenFeld('VKR_SKZ', $schadenKZ);

    while($POS <= $maxPos) {

        $VKD->Form->ZeileStart();
        $VKD->Form->Erstelle_HiddenFeld('VKD_POSITION_' . $POS, $POS);
        $VKD->Form->Erstelle_TextFeld('VKD_POSITION_' . $POS, $POS, 12, $FeldBreiten['POSITION'], false, '', 'height:24px', '', 'T','Z');
        $VKD->Form->Erstelle_TextFeld('VKD_ARTNR_' . $POS, $rsKondDaten->FeldOderPOST('VKD_ARTNR'), 12, $FeldBreiten['ARTNR'], $EditRecht, '', 'height:24px', '', 'T');
        $DatenSteuersatz = explode("|", $VKD->AWISSprachKonserven['VKD']['lst_VKD_STEUERSATZ']);
        $VKD->Form->Erstelle_SelectFeld('VKD_STEUERSATZ_' . $POS, $rsKondDaten->FeldOderPost('VKD_STEUERSATZ'), $FeldBreiten['STEUERSATZ'], true, '', '', '', '', '', $DatenSteuersatz, '', '', [], 'height:24px');
        $VKD->Form->Erstelle_TextFeld('VKD_PREIS_NETTO_' . $POS, $rsKondDaten->FeldOderPOST('VKD_PREIS_NETTO'), 12, $FeldBreiten['PREIS_NETTO'], $EditRecht, '', 'height:24px', '', 'T');
        $VKD->Form->Erstelle_TextFeld('VKD_PREIS_BRUTTO_' . $POS, $rsKondDaten->FeldOderPOST('VKD_PREIS_BRUTTO'), 12, $FeldBreiten['PREIS_BRUTTO'], $EditRecht, '', 'height:24px', '', 'T');
        if($schadenKZ == 'P') {
            $VKD->Form->Erstelle_TextFeld('VKD_HOECHSTGRENZE_' . $POS, $rsKondDaten->FeldOderPOST('VKD_HOECHSTGRENZE'), 12, $FeldBreiten['HOECHSTGRENZE'], $EditRecht, '', 'height:24px', '', 'T');
            $VKD->Form->Erstelle_TextFeld('VKD_MIN_AUSBUCHUNG_' . $POS, $rsKondDaten->FeldOderPOST('VKD_MIN_AUSBUCHUNG'), 12, $FeldBreiten['MIN_AUSBUCHUNG'], $EditRecht, '', 'height:24px', '', 'T');
            $VKD->Form->Erstelle_TextFeld('VKD_MAX_AUSBUCHUNG_' . $POS, $rsKondDaten->FeldOderPOST('VKD_MAX_AUSBUCHUNG'), 12, $FeldBreiten['MAX_AUSBUCHUNG'], $EditRecht, '', 'height:24px', '', 'T');
            $VKD->Form->Erstelle_TextFeld('VKD_SB_' . $POS, $rsKondDaten->FeldOderPOST('VKD_SB'), 12, $FeldBreiten['SB'], $EditRecht, '', 'height:24px', '', 'T');
        } else { // Scheibenreparatur (S)
            $Daten = explode("|", '0~nein|1~ja');
            $VKD->Form->Erstelle_SelectFeld('VKD_VSTABZUG_' . $POS, $rsKondDaten->FeldOderPOST('VKD_VSTABZUG'), '100', $EditRecht, '', '', '', '', '', $Daten, '', '', [], 'height:24px');
        }

        $VKD->Form->ZeileEnde();

        $rsKondDaten->DSWeiter();
        $POS++;
    }
    $VKD->Form->FormularBereichInhaltEnde();
    $VKD->Form->FormularBereichEnde();

    $VKD->Form->FormularBereichStart();
    $VKD->Form->FormularBereichInhaltStart('�nderungsprotokoll', false);

    $FeldBreiten['XAH_FELD'] = 300;
    $FeldBreiten['XAH_WERTALT'] = 150;
    $FeldBreiten['XAH_WERTNEU'] = 150;
    $FeldBreiten['XAH_USER'] = 150;
    $FeldBreiten['XAH_USERDAT'] = 150;

    $SQL = 'SELECT DISTINCT XAH_WERTALT, XAH_WERTNEU, XAH_FELD, XAH_USER, XAH_USERDAT ';
    $SQL .= 'FROM VERSKONDREF INNER JOIN AENDERUNGSHISTORIE ';
    $SQL .= 'ON VKR_KEY = XAH_XXX_KEY ';
    $SQL .= 'AND XAH_XTN_KUERZEL IN (\'VKD\',\'VKR\') ';
    $SQL .= 'WHERE VKR_KEY = ' . $VKD->DB->WertSetzen('XAH', 'N0', $VKD->AWIS_KEY1);
    $SQL .= ' ORDER BY XAH_USERDAT DESC';

    $rsXAH = $VKD->DB->RecordSetOeffnen($SQL, $VKD->DB->Bindevariablen('XAH'));

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['XAH']['XAH_FELD'], $FeldBreiten['XAH_FELD']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['XAH']['XAH_WERTALT'], $FeldBreiten['XAH_WERTALT']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['XAH']['XAH_WERTNEU'], $FeldBreiten['XAH_WERTNEU']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['XAH']['XAH_USER'], $FeldBreiten['XAH_USER']);
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['XAH']['XAH_USERDAT'], $FeldBreiten['XAH_USERDAT']);
    $VKD->Form->ZeileEnde();

    $DS = 0;

    while(!$rsXAH->EOF()) {
        $HG = ($DS % 2);
        $VKD->Form->ZeileStart();
        $VKD->Form->Erstelle_ListenFeld('XAH_FELD', $rsXAH->FeldInhalt('XAH_FELD'), '', $FeldBreiten['XAH_FELD'], false, $HG);
        $VKD->Form->Erstelle_ListenFeld('XAH_WERTALT', $rsXAH->FeldInhalt('XAH_WERTALT'), '', $FeldBreiten['XAH_WERTALT'], false, $HG);
        $VKD->Form->Erstelle_ListenFeld('XAH_WERTNEU', $rsXAH->FeldInhalt('XAH_WERTNEU'), '', $FeldBreiten['XAH_WERTNEU'], false, $HG);
        $VKD->Form->Erstelle_ListenFeld('XAH_USER', $rsXAH->FeldInhalt('XAH_USER'), '', $FeldBreiten['XAH_USER'], false, $HG);
        $VKD->Form->Erstelle_ListenFeld('XAH_USERDAT', $rsXAH->FeldInhalt('XAH_USERDAT'), '', $FeldBreiten['XAH_USERDAT'], false, $HG);
        $VKD->Form->ZeileEnde();

        $DS++;
        $rsXAH->DSWeiter();
    }

    $VKD->Form->FormularBereichInhaltEnde();
    $VKD->Form->FormularBereichEnde();
}
//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$VKD->Form->Formular_Ende();
$VKD->Form->SchaltflaechenStart();

$VKD->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $VKD->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($VKD->Recht9400 & 2) == 2 AND $VKD->AWIS_KEY1 != '') { //Speichern erlaubt + in einen Datensatz?
    $VKD->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $VKD->AWISSprachKonserven['Wort']['lbl_speichern'], 'S', '', 27, 27, '', true);
}

if (($VKD->Recht9400 & 2) == 2 AND $VKD->AWIS_KEY1 != -1 AND $rsVKD->AnzahlDatensaetze() <= 1) {// Hinzuf�gen erlaubt + nicht gerade in einer Neuanlage?
    $VKD->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $VKD->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$VKD->Form->SchaltflaechenEnde();

$VKD->Form->SchreibeHTMLCode('</form>');

?>