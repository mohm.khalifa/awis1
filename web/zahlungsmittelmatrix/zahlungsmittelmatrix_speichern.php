<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	//var_dump($_POST);
	$AWIS_KEY1 = isset($_POST['txtZMM_KEY'])?$_POST['txtZMM_KEY']:$SEQ;
	$Form->DebugAusgabe(1,'AWIS_KEY1: ' . $AWIS_KEY1);
	
	$TextKonserven=array();
	$TextKonserven[]=array('ZMM','%');
	$TextKonserven[]=array('Fehler','err_KeinWert');
	$TextKonserven[]=array('Fehler','err_FelderVeraendert');
	$TextKonserven[]=array('Fehler','err_DatumEndeVorAnfang');
	$TextKonserven[]=array('Fehler','err_DatumAbstandTage');
	$TextKonserven[]=array('Wort','geaendert_von');
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TextKonserven[]=array('Meldung','EingabeWiederholen');
	

	//var_dump($_REQUEST);
    $FehlerListe=array();

    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
    
    //Alle Felder die geupdatet oder geinsertet werden sollen
    $UpdateInsertFelder[0]['Feldname'] = 'ZMM_WERTIGKEIT';
    $UpdateInsertFelder[0]['Format'] = 'N0';
    $UpdateInsertFelder[0]['Pflicht'] = '1';
    
    
    $UpdateInsertFelder[1]['Feldname'] = 'ZMM_ZAMINR';
    $UpdateInsertFelder[1]['Format'] = 'N0';   
    $UpdateInsertFelder[1]['Pflicht'] = '1';
        
    $UpdateInsertFelder[2]['Feldname'] = 'ZMM_ZAMIBEZEICHNUNG';
    $UpdateInsertFelder[2]['Format'] = 'T';
    $UpdateInsertFelder[2]['Pflicht'] = '1';
    
    
    $UpdateInsertFelder[3]['Feldname'] = 'ZMM_GUELTIG_VON';
    $UpdateInsertFelder[3]['Format'] = 'D';
    $UpdateInsertFelder[3]['Pflicht'] = '1';
    
    $UpdateInsertFelder[4]['Feldname'] = 'ZMM_GUELTIG_BIS';
    $UpdateInsertFelder[4]['Format'] = 'D';
    $UpdateInsertFelder[4]['Pflicht'] = '1';

    $UpdateInsertFelder[5]['Feldname'] = 'ZMM_SKONTO';
    $UpdateInsertFelder[5]['Format'] = 'N0';
    $UpdateInsertFelder[5]['Pflicht'] = '1';
    
    $UpdateInsertFelder[6]['Feldname'] = 'ZMM_RECHNUNGSTEXT';
    $UpdateInsertFelder[6]['Format'] = 'T';
    $UpdateInsertFelder[6]['Pflicht'] = '0';
    
    $UpdateInsertFelder[7]['Feldname'] = 'ZMM_ZAHLUNGSZIEL';
    $UpdateInsertFelder[7]['Format'] = 'N0';
    $UpdateInsertFelder[7]['Pflicht'] = '1';
    
    $KeinWertFehler = '';
    $Formatfehler ='';
    $ZamiNrFehler ='';
    $ZamiWertigkeitFehler = '';
    $SpeicherFehler =false;
    

    //Formate Pr�fen
    foreach($UpdateInsertFelder AS $Element => $Inhalt)
    {
        $Feld = $Inhalt['Feldname'];
        $Format = $Inhalt['Format'];
        $Pflicht = $Inhalt['Pflicht'];
        
        $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
        
        //Pflichtfelder pr�fen
        if ($Pflicht == 1)
        {
            if(isset($_POST[$Praefix.$Feld]) AND ($_POST[$Praefix.$Feld]=='' or $_POST[$Praefix.$Feld] == '::bitte w�hlen::'))
            {
                $KeinWertFehler .= $TXT_Speichern['ZMM'][$Feld].', ';
                continue; //Formatpr�fung macht kein Sinn.. gleich n�chstes Element anschauen.
            }
        }
        
        //Formate Pr�fen
        if (isset($_POST[$Praefix.$Feld]) AND $_POST[$Praefix.$Feld] != '' and $_POST[$Praefix.$Feld] != '::bitte w�hlen::' )
        {
            if ($Format == 'N0')
            {
                 //Numerisch
                 if(!ctype_digit($_POST[$Praefix.$Feld]))
                 {
                     echo $Feld;
                     $Formatfehler .= $TXT_Speichern['ZMM']['ZMM_ERR_NUMMER'] . $TXT_Speichern['ZMM'][$Feld] . '<br>'; 
                 }
            }
            elseif (substr($Format,0,1) == 'D')
            {
                //Datum
                if($Form->Format('D',$_POST[$Praefix.$Feld],true) == '') 
                {
                    $Formatfehler .= $TXT_Speichern['ZMM']['ZMM_ERR_DATUM'] . $TXT_Speichern['ZMM'][$Feld] . '<br>';
                }
                else
                {                
                    //Datum heute bis Zukunft
                    if ($Format == 'DHZ') //Datum heute bis Zukunft
                    {                  
                        if ((strtotime($_POST[$Praefix.$Feld]) < strtotime(date('d.m.Y',time()))))
                        {       
                            $Formatfehler .= $TXT_Speichern['ZMM']['ZMM_ERR_DATUM_VERGANGENHEIT'] .' ' . $TXT_Speichern['ZMM'][$Feld] . '<br>';
                        }
                    }     
                }
            }
        } 
    }
    
    //ZamiNRPruefung: nachschauen, ob der Datensatz kolidiert, aufgrund der G�ltigkeit
    $SQLPruef  ='SELECT';
    $SQLPruef .='   *';
    $SQLPruef .=' FROM';
    $SQLPruef .='   zahlungsmittelmatrix';
    //var_dump($_POST['txtZMM_GUELTIG_VON']);
    $SQLPruef .= ' WHERE ZMM_GUELTIG_VON  >='.$DB->WertSetzen('ZMM','D',$_POST['txtZMM_GUELTIG_VON']);
    $SQLPruef .= ' AND ZMM_GUELTIG_BIS  >='.$DB->WertSetzen('ZMM','D',$_POST['txtZMM_GUELTIG_BIS']);
    $SQLPruef .= ' AND ZMM_ZAMINR  ='.$DB->WertSetzen('ZMM','T',$_POST['txtZMM_ZAMINR']);
    $SQLPruef .= ' AND ZMM_KEY  !='.$DB->WertSetzen('ZMM','T',$AWIS_KEY1);
    

    $rsPruef = $DB->RecordSetOeffnen($SQLPruef,$DB->Bindevariablen('ZMM',true));
    
    if($rsPruef->AnzahlDatensaetze()>0)
    {
        $ZamiNrFehler = true;
    }
    
    //ZamiWertigkeitspruefung: nachschauen, ob der Datensatz kolidiert, aufgrund der G�ltigkeit
    $SQLPruef  ='SELECT';
    $SQLPruef .='   *';
    $SQLPruef .=' FROM';
    $SQLPruef .='   zahlungsmittelmatrix';
    //var_dump($_POST['txtZMM_GUELTIG_VON']);
    $SQLPruef .= ' WHERE ZMM_GUELTIG_VON  >='.$DB->WertSetzen('ZMM','D',$_POST['txtZMM_GUELTIG_VON']);
    $SQLPruef .= ' AND ZMM_GUELTIG_BIS  >='.$DB->WertSetzen('ZMM','D',$_POST['txtZMM_GUELTIG_BIS']);
    $SQLPruef .= ' AND ZMM_WERTIGKEIT  ='.$DB->WertSetzen('ZMM','T',$_POST['txtZMM_WERTIGKEIT']);
    $SQLPruef .= ' AND ZMM_KEY  !='.$DB->WertSetzen('ZMM','T',$AWIS_KEY1);
    
    
    $rsPruef = $DB->RecordSetOeffnen($SQLPruef,$DB->Bindevariablen('ZMM',true));
    
    if($rsPruef->AnzahlDatensaetze()>0)
    {
        $ZamiWertigkeitFehler = true;
    }
    
    
    // Wurden Fehler entdeckt? => Speichern abbrechen
    if($KeinWertFehler!='')
    {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_Speichern['Fehler']['err_KeinWert'].' '.substr($KeinWertFehler,0, strlen($KeinWertFehler)-2),1,'');
        $Form->ZeileEnde();
    }
    if($Formatfehler != '')
    {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($Formatfehler,1,'');
        $Form->ZeileEnde();
    }
    
    if($ZamiNrFehler == true)
    {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_Speichern['ZMM']['ZMM_ERR_ZAMINR'],1,'');
        $Form->ZeileEnde();
    }
    if($ZamiWertigkeitFehler == true)
    {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_Speichern['ZMM']['ZMM_ERR_WERTIGKEIT'],1,'');
        $Form->ZeileEnde();
    }
    
    //AWIS_KEY1 ist in jedem Fall gesetzt. 
    $SEQ = $AWIS_KEY1;
    
    if ($KeinWertFehler=='' and $Formatfehler=='' and $ZamiNrFehler == '' )
    {
       $SQL = '';
       if($AWIS_KEY1=='0') //Neuer DS
        {            
            $SQL = 'INSERT INTO ZAHLUNGSMITTELMATRIX';
            $SQL .= '(zmm_user, zmm_userdat';
            $SQLFelder = '';
            $SQLWerte = '';
            foreach($UpdateInsertFelder AS $Element => $Inhalt)
            {
                $Feld = $Inhalt['Feldname'];
                $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
                 
                if (isset($_POST[$Praefix.$Feld]) and $_POST[$Praefix.$Feld] != '' and $_POST[$Praefix.$Feld] != '::bitte w�hlen::')
                {   
                    $SQLFelder .= ', '.$Feld;
                    $SQLWerte  .= ', \''.$_POST[$Praefix.$Feld].'\'';
                }
            }
            $SQL .= $SQLFelder; //Relevante Datenfelder dazuf�gen
            $SQL .= ')VALUES (';
            $SQL .= '\'' .$AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= $SQLWerte; //Relevante Datenwerte dazuf�gen.
            $SQL .= ')';
            
            $DB->Ausfuehren($SQL);
            
            $SEQSQL = 'select seq_zmm_key.CURRVAL AS Key from dual';
            $rsSEQ = $DB->RecordSetOeffnen($SEQSQL);
            $SEQ = $rsSEQ->FeldInhalt('KEY');
               
            $Form->Hinweistext($TXT_Speichern['ZMM']['ZMM_INSERTOK']);
        }
        else // ge�nderterDS
        {       
            //var_dump($_POST);
            $FeldListe = '';
            foreach($UpdateInsertFelder AS $Element => $Inhalt)
            {
                $Feld = $Inhalt['Feldname'];
                $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
                //echo $Praefix.$Feld.'<br>';
                if((isset($_POST['old'.$Feld]) or $Praefix == 'suc')and isset($_POST[$Praefix.$Feld])) //Sucfelder haben kein old
                {                  
                    $WertNeu=$_POST[$Praefix.$Feld]!='::bitte w�hlen::'?$_POST[$Praefix.$Feld]:'';
                    $WertAlt=$Praefix=='txt'?$_POST['old'.$Feld]:'';
                    
                    if((isset($_POST['old'.$Feld])or $Praefix=='suc')) 
                    {                       
                        //echo $Feld . "<br>";
                        $FeldListe .= ', '.$Feld.'=';            
                        if($WertNeu=='')	// Leere Felder immer als NULL
                        {
                            $FeldListe.=' null';
                        }
                        else
                        {
                            $FeldListe.= '\''.$WertNeu . '\'';
                        }
                         
                    }
                }
            }  
            if($FeldListe!='')
            {
                $SQL = 'UPDATE ZAHLUNGSMITTELMATRIX SET';
                $SQL .= substr($FeldListe,1);
                $SQL .= ', zmm_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', zmm_userdat=sysdate';
                $SQL .= ' WHERE zmm_key=0' . $AWIS_KEY1 . '';
                //var_dump($SQL);
                $DB->Ausfuehren($SQL);
                $Form->Hinweistext($TXT_Speichern['ZMM']['ZMM_UPDATEOK']);
                $SEQ = $AWIS_KEY1;
            }
        }        
    }		
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>