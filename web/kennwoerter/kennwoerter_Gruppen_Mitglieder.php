<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KZM','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_JaNein');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht9200 = $AWISBenutzer->HatDasRecht(9200);
	if(($Recht9200&16)==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Form->DebugAusgabe(1,$Recht9200);
	// Daten ermitteln
	$SQL = 'SELECT *';
	$SQL .= ' FROM KENNWOERTERGRUPPENMITGLIEDER ';
	$SQL .= ' LEFT OUTER JOIN Benutzer ON kzm_xbn_key = xbn_key';
	$SQL .= ' WHERE kzm_kzg_key = 0'.$AWIS_KEY1;
	if(isset($_GET['KZM_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['KZM_KEY']);
		$SQL .= ' AND KZM_KEY = '.$AWIS_KEY2;
	}
	$SQL .= ' ORDER BY xbn_name, xbn_vorname';

	$rsKZM = $DB->RecordSetOeffnen($SQL);

	if($rsKZM->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht9200)&16)!=0)
		{
			$Icons[] = array('new','./kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder&KZG_KEY='.$AWIS_KEY1.'&KZM_KEY=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=KZM_XBN_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KZM_XBN_KEY'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KZM']['KZM_XBN_KEY'],350,'',$Link);
		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=KZM_MITGLIEDVON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KZM_MITGLIEDVON'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KZM']['KZM_MITGLIEDVON'],150,'',$Link);
		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=KZM_MITGLIEDBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KZM_MITGLIEDBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KZM']['KZM_MITGLIEDBIS'],150,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKZM->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht9200&16)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder&KZG_KEY='.$AWIS_KEY1.'&KZM_KEY='.$rsKZM->FeldInhalt('KZM_KEY'));
				$Icons[] = array('delete','./kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder&KZG_KEY='.$AWIS_KEY1.'&Del='.$rsKZM->FeldInhalt('KZM_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$Form->Erstelle_ListenFeld('XBN_NAME',$rsKZM->FeldInhalt('XBN_NAME').' '.$rsKZM->FeldInhalt('XBN_VORNAME'),0,350,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('KZM_MITGLIEDVON',$rsKZM->FeldInhalt('KZM_MITGLIEDVON'),0,150,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('KZM_MITGLIEDBIS',$rsKZM->FeldInhalt('KZM_MITGLIEDBIS'),0,150,false,($DS%2),'','','D');
			$Form->ZeileEnde();

			$rsKZM->DSWeiter();
			$DS++;
		}

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsKZM->FeldInhalt('KZM_KEY');

		$Form->Erstelle_HiddenFeld('KZM_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('KZM_KZG_KEY',$AWIS_KEY1);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./kennwoerter_Main.php?cmdAktion=Gruppen&Seite=Mitglieder&KZMListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKZM->FeldInhalt('KZM_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKZM->FeldInhalt('KZM_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht9200&16)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KZM']['KZM_XBN_KEY'].':',150);
		$SQL = 'SELECT xbn_key, xbn_name || \' \' || xbn_vorname AS xbnname ';
		$SQL .= ' FROM Benutzer';
		$SQL .= ' WHERE LOWER(xbn_name) not like \'fil%\'';
		$SQL .= ' AND xbn_status = \'A\'';
		$SQL .= ' ORDER BY xbn_name, xbn_vorname';

		$Form->Erstelle_SelectFeld('KZM_XBN_KEY',$rsKZM->FeldInhalt('KZM_XBN_KEY'),50,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','');
		$AWISCursorPosition='txtKZM_XBN_KEY';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KZM']['KZM_MITGLIEDVON'].':',150);
		$Form->Erstelle_TextFeld('KZM_MITGLIEDVON',$rsKZM->FeldInhalt('KZM_MITGLIEDVON'),10,200,$EditRecht,'','','','D','','',date('d.m.Y'));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KZM']['KZM_MITGLIEDBIS'].':',150);
		$Form->Erstelle_TextFeld('KZM_MITGLIEDBIS',$rsKZM->FeldInhalt('KZM_MITGLIEDBIS'),10,200,$EditRecht,'','','','D','','','31.12.2030');
		$Form->ZeileEnde();


		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200809111043);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>