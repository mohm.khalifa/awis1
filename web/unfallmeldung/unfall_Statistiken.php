<?php
require_once('unfall_funktionen.inc');
require_once 'awisDiagramm.inc';

echo '<script type="application/javascript" src="unfall_Statistiken.js" href="unfall_Statistiken.js"></script>';

global $AWISCursorPosition;
global $AWISBenutzer;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISDiagramm = new awisDiagramm();
    $UNF = new awis_unfall_funktionen();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('UNF', '%');
    $TextKonserven[] = array('UFT', '%');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht21030 = $AWISBenutzer->HatDasRecht(21030);
    if ($Recht21030 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $AWIS_KEY1 = '';
    if (isset($_GET['UFA_KEY'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['UFA_KEY']);
    } elseif (isset($_POST['txtUFA_KEY'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtUFA_KEY']);
    }

    if (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
    } elseif (isset($_POST['cmdSpeichern_x'])) {

        if ($AWIS_KEY1 == -1) {
            $SQL  ='insert into UNFALLMELDUNGAUSWERTUNG (';
            $SQL .='     UFA_BEZEICHNUNG,';
            $SQL .='     UFA_FIL_OPTION,';
            $SQL .='     UFA_FIL_ID,';
            $SQL .='     UFA_FEB_KEY,';
            $SQL .='     UFA_BUL_ID,';
            $SQL .='     UFA_DATUM_OPTION,';
            $SQL .='     UFA_DATUM_VON,';
            $SQL .='     UFA_DATUM_BIS,';
            $SQL .='     UFA_DATUM_RELATIV_EINHEIT,';
            $SQL .='     UFA_DIAGRAMMTYP,';
            $SQL .='     UFA_GROUP_BY,';
            $SQL .='     UFA_DATUM_RELATIV_ANZ, ';
            $SQL .='     UFA_USER,';
            $SQL .='     UFA_USERDAT';
            $SQL .=' ) values (';
            $SQL .=  $DB->WertSetzen('UFA','T',$_POST['txtUFA_BEZEICHNUNG']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_FIL_OPTION']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_FIL_ID']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_FEB_KEY']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_BUL_ID']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_OPTION']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_VON']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_BIS']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_RELATIV_EINHEIT']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DIAGRAMMTYP']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_GROUP_BY']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_RELATIV_ANZ']);
            $SQL .= ' , ' . $DB->WertSetzen('UFA','T',$AWISBenutzer->BenutzerID());
            $SQL .=', sysdate';
            $SQL .=' )';

            $DB->Ausfuehren($SQL,'',false, $DB->Bindevariablen('UFA'));

            $AWIS_KEY1 = $DB->RecordSetOeffnen('select seq_ufa_key.currval from dual ')->FeldInhalt(1);

        } else {
            $SQL = 'update UNFALLMELDUNGAUSWERTUNG ';
            $SQL .= 'set ';
            $SQL .='     UFA_BEZEICHNUNG = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_BEZEICHNUNG']);
            $SQL .=',     UFA_FIL_OPTION = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_FIL_OPTION']);
            $SQL .=',     UFA_FIL_ID = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_FIL_ID']);
            $SQL .=',     UFA_FEB_KEY = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_FEB_KEY']);
            $SQL .=',     UFA_BUL_ID = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_BUL_ID']);
            $SQL .=',     UFA_DATUM_OPTION = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_OPTION']);
            $SQL .=',     UFA_DATUM_VON = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_VON']);
            $SQL .=',     UFA_DATUM_BIS = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_BIS']);
            $SQL .=',     UFA_DATUM_RELATIV_EINHEIT = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_RELATIV_EINHEIT']);
            $SQL .=',     UFA_DIAGRAMMTYP = '. $DB->WertSetzen('UFA','T',$_POST['txtUFA_DIAGRAMMTYP']);
            $SQL .=',     UFA_GROUP_BY = ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_GROUP_BY']);
            $SQL .=',     UFA_DATUM_RELATIV_ANZ = ' . $DB->WertSetzen('UFA','T',$_POST['txtUFA_DATUM_RELATIV_ANZ']);
            $SQL .=',     UFA_USER = ' . $DB->WertSetzen('UFA','T',$AWISBenutzer->BenutzerID());
            $SQL .=',     UFA_USERDAT = sysdate';
            $SQL .= ' WHERE UFA_KEY = ' . $DB->WertSetzen('UFA','Z',$AWIS_KEY1);

            $DB->Ausfuehren($SQL,'',false, $DB->Bindevariablen('UFA'));
        }

        $Form->Hinweistext('Erfolgreich gespeichert',awisFormular::HINWEISTEXT_OK);


    }

    $SQL = 'select * from UNFALLMELDUNGAUSWERTUNG ';
    if($AWIS_KEY1 != ''){
        $SQL .= 'WHERE UFA_KEY = ' . $DB->WertSetzen('UFA','Z',$AWIS_KEY1);
    }
    $SQL .= ' order by UFA_BEZEICHNUNG';
    $rsUFA = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('UFA'));

    $Form->DebugAusgabe(1, $DB->LetzterSQL(), $rsUFA->AnzahlDatensaetze());

    // Seite-GET-Parameter abholen, um im <form>-Tag zu verwenden, dass bei "Weiter"-Klick immer der richtige Sub-Reiter ge�ffnet wird
    $Seite = (isset($_GET['Seite']) ? $_GET['Seite'] : 'Bereich');
    $Form->SchreibeHTMLCode('<form name=frmUnfallDetails action=./unfall_Main.php?cmdAktion=Statistiken&Seite=' . $Seite . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '') . ' method=POST enctype="multipart/form-data">');
    $Form->Formular_Start();

    $Form->Erstelle_HiddenFeld('UFA_KEY',$AWIS_KEY1);

    if ($AWIS_KEY1 != '') {

        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart('Allgemeines', true);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Bezeichnung: ', 300);
        $Form->Erstelle_TextFeld('UFA_BEZEICHNUNG', $rsUFA->FeldOderPOST('UFA_BEZEICHNUNG'), 300, 300, true);
        $Form->ZeileEnde();

        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();

        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart('Einschr�nkungen', true);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Filialen einschr�nken: ', 300, 'font-weight: bold;');
        $Optionen = 'FIL_ID~Filialnummer(n)|FEB_KEY_BEZIRK~Bezirk|BUL_ID~Bundesland';
        $Optionen = explode('|', $Optionen);
        $Form->Erstelle_SelectFeld('UFA_FIL_OPTION',  $rsUFA->FeldOderPOST('UFA_FIL_OPTION'), 300, true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Optionen);
        $Form->ZeileEnde();

        $Form->ZeileStart('', 'DIV_FIL_ID', 'OPTION_FIL');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'] . ':', 300);
        $Form->Erstelle_TextFeld('UFA_FIL_ID',  $rsUFA->FeldOderPOST('UFA_FIL_ID') , 4, 300, true, '', '', '', 'T', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart('', 'DIV_FEB_KEY_BEZIRK', 'OPTION_FIL');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_VERTRIEBSBEZIRK'] . ':', 300);
        $SQL = 'SELECT';
        $SQL .= '     FEB_KEY,';
        $SQL .= '     FEB_BEZEICHNUNG';
        $SQL .= ' FROM';
        $SQL .= '     FILIALEBENEN';
        $SQL .= ' WHERE';
        $SQL .= '     FEB_GUELTIGAB <= SYSDATE';
        $SQL .= '     AND FEB_GUELTIGBIS >= SYSDATE';
        $SQL .= '     AND FEB_EBENE = 4';
        $SQL .= ' ORDER BY FEB_BEZEICHNUNG ASC';
        $Form->Erstelle_SelectFeld('UFA_FEB_KEY', $rsUFA->FeldOderPOST('UFA_FEB_KEY') , 300, true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart('', 'DIV_BUL_ID', 'OPTION_FIL');
        $Form->Erstelle_TextLabel('Bundesland' . ':', 300);
        $SQL = 'SELECT';
        $SQL .= '     BUL_ID,';
        $SQL .= '     BUL_LAN_CODE || \': \' || BUL_BUNDESLAND';
        $SQL .= ' FROM';
        $SQL .= '     BUNDESLAENDER';
        $SQL .= ' WHERE BUL_LAN_CODE in (\'DE\',\'AT\',\'CH\')    ';
        $SQL .= ' ORDER BY BUL_LAN_CODE, BUL_BUNDESLAND ASC';
        $Form->Erstelle_SelectFeld('UFA_BUL_ID',  $rsUFA->FeldOderPOST('UFA_BUL_ID') , 300, true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Datum einschr�nken: ', 300, 'font-weight: bold;');
        $Optionen = 'ABSOLUT~Absolut';//|RELATIV~Relativ';
        $Optionen = explode('|', $Optionen);
        $Form->Erstelle_SelectFeld('UFA_DATUM_OPTION', $rsUFA->FeldOderPOST('UFA_DATUM_OPTION') , 300, true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Optionen);
        $Form->ZeileEnde();

        echo '<div id="DIV_ABSOLUT" class="OPTION_DATUM" >';
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 300);
        $Form->Erstelle_TextFeld('UFA_DATUM_VON',  $rsUFA->FeldOderPOST('UFA_DATUM_VON')  , 8, 300, true, '', '', '', 'D', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 300);
        $Form->Erstelle_TextFeld('UFA_DATUM_BIS',  $rsUFA->FeldOderPOST('UFA_DATUM_BIS')  , 8, 300, true, '', '', '', 'D', 'L');
        $Form->ZeileEnde();
        echo '</div>';

        echo '<div id="DIV_RELATIV" class="OPTION_DATUM" >';

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Relative Angabe: ', 210);
        $Form->Erstelle_TextLabel('Die letzten  ', 90);
        $Form->Erstelle_TextFeld('UFA_DATUM_RELATIV_ANZ',  $rsUFA->FeldOderPOST('UFA_DATUM_RELATIV_ANZ') , 8, 80, true, '', '', '', 'T', 'L');
        $Optionen = 'DAYS~Tage|MONTHS~Monate';
        $Optionen = explode('|', $Optionen);
        $Form->Erstelle_SelectFeld('UFA_DATUM_RELATIV_EINHEIT', $rsUFA->FeldOderPOST('UFA_DATUM_RELATIV_EINHEIT'), 120, true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Optionen);
        $Form->ZeileEnde();

        echo '</div>';

        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();

        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart('Darstellungsoptionen', true);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Diagrammtyp: ', 300);
        $Optionen = 'tab~Tabelle|line~Liniendiagramm|bar~Balkendiagramm|pie~Kuchendiagramm';
        $Optionen = explode('|', $Optionen);
        $Form->Erstelle_SelectFeld('UFA_DIAGRAMMTYP',  $rsUFA->FeldOderPOST('UFA_DIAGRAMMTYP') , 120, true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Optionen);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Gruppierung: ', 300);
        $Zusatzoptionen = [];
        foreach ($UNF->Gruppierfelder() as $Key =>  $Feld) {
            $Zusatzoptionen[] = $Key . '~' . $Feld;
        }

        $Form->Erstelle_SelectFeld('UFA_GROUP_BY',  $rsUFA->FeldOderPOST('UFA_GROUP_BY')  , 120, true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Zusatzoptionen);
        $Form->ZeileEnde();

        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();

        $Form->FormularBereichStart();
        $Form->FormularBereichInhaltStart('Ergebnis', true);

        if (isset($_POST)) {
            $XTX_Bereich = explode('_', $rsUFA->FeldOderPOST('UFA_GROUP_BY'))[0];
            $Label = isset($AWISSprachKonserven[$XTX_Bereich][$rsUFA->FeldOderPOST('UFA_GROUP_BY')]) ? $AWISSprachKonserven[$XTX_Bereich][$rsUFA->FeldOderPOST('UFA_GROUP_BY')] : 'Wert';

            $Params = array();
            $Params['UFA_DATUM_VON'] = $rsUFA->FeldInhalt('UFA_DATUM_VON');
            $Params['UFA_DATUM_BIS'] = $rsUFA->FeldInhalt('UFA_DATUM_BIS');
            $Params['UFA_BEZEICHNUNG'] = $rsUFA->FeldInhalt('UFA_BEZEICHNUNG');
            $Params['UFA_FIL_OPTION'] = $rsUFA->FeldInhalt('UFA_FIL_OPTION');
            $Params['UFA_FIL_ID'] = $rsUFA->FeldInhalt('UFA_FIL_ID');
            $Params['UFA_BUL_ID'] = $rsUFA->FeldInhalt('UFA_BUL_ID');
            $Params['UFA_FEB_KEY'] = $rsUFA->FeldInhalt('UFA_FEB_KEY');
            $Params['UFA_DATUM_OPTION'] = $rsUFA->FeldInhalt('UFA_DATUM_OPTION');
            $Params['UFA_DATUM_RELATIV_ANZ'] = $rsUFA->FeldInhalt('UFA_DATUM_RELATIV_ANZ');
            $Params['UFA_DATUM_RELATIV_EINHEIT'] = $rsUFA->FeldInhalt('UFA_DATUM_RELATIV_EINHEIT');
            $Params['UFA_DIAGRAMMTYP'] = $rsUFA->FeldInhalt('UFA_DIAGRAMMTYP');
            $Params['UFA_GROUP_BY'] = $rsUFA->FeldInhalt('UFA_GROUP_BY');


            switch ($rsUFA->FeldOderPOST('UFA_DIAGRAMMTYP')) {

                case 'tab':
                    $rsAuswertung = $UNF->rsAuswertung($Params);
                    $Form->ZeileStart();
                    $Form->Erstelle_Liste_Ueberschrift($Label, 300);
                    $Form->Erstelle_Liste_Ueberschrift('Anzahl', 300);
                    $Form->ZeileEnde();

                    $Gesamt = 0;
                    while (!$rsAuswertung->EOF()) {
                        $HG = $rsAuswertung->DSNummer() % 2;

                        $Form->ZeileStart();
                        $Form->Erstelle_ListenFeld('Feldname', $rsAuswertung->FeldInhalt(1), 30, 300, false, $HG);
                        $Form->Erstelle_ListenFeld('Anzahl', $rsAuswertung->FeldInhalt(2), 30, 300, false, $HG);
                        $Gesamt += $rsAuswertung->FeldInhalt(2);
                        $Form->ZeileEnde();

                        $rsAuswertung->DSWeiter();
                    }

                    $Form->ZeileStart();
                    $Form->Erstelle_Liste_Ueberschrift('Gesamt: ', 300);
                    $Form->Erstelle_Liste_Ueberschrift($Gesamt, 300);
                    $Form->ZeileEnde();
                    break;

                case 'pie':

                    $Form->ZeileStart();
                    $AWISDiagramm->ErstelleKreisDiagramm('diagramm', $UNF->AuswertungSQL($Params), $DB->Bindevariablen('UFA'), 300, 300, $Params['UFA_BEZEICHNUNG'], '', 'AWIS_PROD');
                    $Form->ZeileEnde();
                    break;
            }
        }

        $Form->FormularBereichInhaltEnde();
        $Form->FormularBereichEnde();
    }else{
        //Listenansicht

        if($rsUFA->AnzahlDatensaetze() == 0){
            $Form->ZeileStart();
            $Form->Hinweistext('Keine Datens�tze gefunden.');
            $Form->ZeileEnde();
        }else{
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift('Bezeichnung',300);
            $Form->Erstelle_Liste_Ueberschrift('Typ',100);
            $Form->ZeileEnde();

            while (!$rsUFA->EOF()){
                $Form->ZeileStart();
                $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite=Bereich&UFA_KEY='.$rsUFA->FeldInhalt('UFA_KEY');
                $Form->Erstelle_ListenFeld('UFA_BEZEICHNUNG',$rsUFA->FeldInhalt('UFA_BEZEICHNUNG'),30,300,false, $rsUFA->DSNummer()%2,'',$Link);
                $Form->Erstelle_ListenFeld('UFA_DIAGRAMMTYP',$rsUFA->FeldInhalt('UFA_DIAGRAMMTYP'),10,100,false, $rsUFA->DSNummer()%2);
                $Form->ZeileEnde();

                $rsUFA->DSWeiter();
            }

        }


    }

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (($Recht21030 & 1) == 1 and $AWIS_KEY1 != '') {
        $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'F');
    }elseif(($Recht21030 & 1) == 1 and $AWIS_KEY1 == ''){
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'F');
    }

    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>