<link rel=stylesheet type=text/css href=/css/awis.css>
<?php


/*
 * 
select *
from 
(
select LAI_KEY
,(select L.LAR_KEY FROM lieferantenartikel L WHERE L.LAR_SUCH_LARTNR = LAR.LAR_SUCH_LARTNR AND L.LAR_LIE_NR = '2746' AND L.lar_key <> LAR.lar_key) AS LAI_LAR_KEY
,LAI_LIT_ID,LAI_WERT,LAI_IMQ_ID,LAI_USER,LAI_USERDAT
from AWIS.LIEFERANTENARTIKELINFOS
inner join lieferantenartikel LAR on lai_lar_key = LAR.lar_key
) DATEN
WHERE LAI_LAR_KEY IS NOT NULL

 */
require_once('awisDatenbank.inc');

/*
 * A-008032
 * Spiegeln der Lieferantendaten/Zuordnungen von 0070 auf 2746
 * 
 * Festlegungen in Absprache mit dem Anforderer:
 * Wenn alter und neuer Lieferantenartikeldatensatz vorhanden:
 * 	Alter DS auf Auslauf
 * 	Neuer DS um Daten alter DS erg�nzen
 * 	vorhandene alte Teileinfos AST/LAR pr�fen ob bei neuem LIEF auch vorhanden und ggf neu anlegen
 * Wenn alter Lieferantenartikeldatensatz vorhanden, aber kein neuer:
 * 	alten DS LIEF auf neuen LIEF drehen wenn nicht (mehr) im WWS vorhanden, sonst Auslauf
 * 
 * 
*/

function Protokoll($fp, $ProtArray)
{
	$zeile  = $ProtArray['LAR_LARTNR'] .';';
	$zeile .= $ProtArray['KATEGORIE'] .';';
	$zeile .= $ProtArray['AKTION'] .';';
	$zeile .= $ProtArray['SQL'] .";\n";
	fputs($fp,$zeile);
	
	echo $zeile;
}

try
{
	$Anforderung = 'A008032';
	$Anforderer = 'Berutausch';
	$LieferantAlt = '0070';
	$LieferantNeu = '2746';
	$Bemerkung = "Ersetzt durch Lieferant 2746, Anforderung: ".$Anforderung;

	//$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130403_A008082_Staging_PROTOKOLL.csv';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130403_A008082_Produktiv_PROTOKOLL.csv';
	$fp = fopen($ProtokollDatei,'w+');
	if ($fp===false)
	{
	    die();
	}
	$zeile = "LAR_LARTNR;KATEGORIE;AKTION;SQL;\n";
	fputs($fp,$zeile);
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	// alle DS des alten LIEF laden
	$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
	$SQL .= ' WHERE LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$LieferantAlt, false);
	
	$rsQuelle = $DB->RecordSetOeffnen($SQL);
	
	$DB->TransaktionBegin();
	
	while (!$rsQuelle->EOF())
	{
		$ProtokollArray = array();
		$ProtokollArray['LAR_LARTNR'] = $rsQuelle->Feldinhalt('LAR_LARTNR');
		$ProtokollArray['KATEGORIE'] = '';
		$ProtokollArray['AKTION'] = '';
		$ProtokollArray['SQL'] = '';
		
		//Gibt es Eintr�ge im Lieferantenartikel f�r LAR_LARTNR und Lieferant NEU
		$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
		$SQL .= ' WHERE LAR_SUCH_LARTNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_SUCH_LARTNR'),false);
		$SQL .= ' AND LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$LieferantNeu, false);

		$ProtokollArray['AKTION'] = 'Laden Lieferantenartikel fuer neuen Lieferant';
		$ProtokollArray['SQL'] = $SQL; 		
		$ProtokollArray['KATEGORIE'] = 'INFO'; 		
		Protokoll($fp, $ProtokollArray);
		
		$rsZiel = $DB->RecordSetOeffnen($SQL);
		
		if (!$rsZiel->EOF())
		{
			while (!$rsZiel->EOF())
			{
			    //Neuen Lieferantenartikel mit Altdaten erg�nzen
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				if (($rsZiel->Feldinhalt('LAR_BEMERKUNGEN') == '') or is_null($rsZiel->Feldinhalt('LAR_BEMERKUNGEN')))
				{
					$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_BEMERKUNGEN'), false).',';
				}
				else if (stripos($rsZiel->Feldinhalt('LAR_BEMERKUNGEN'),$rsQuelle->Feldinhalt('LAR_BEMERKUNGEN')) === false)
				{
					$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_BEMERKUNGEN'), false) .'|| \' \' || LAR_BEMERKUNGEN'.',';
				}
				$SQL .= '  LAR_IMQ_ID='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_IMQ_ID'),false);
				$SQL .= ' ,LAR_ALTERNATIVARTIKEL='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_ALTERNATIVARTIKEL'),false);
				$SQL .= ' ,LAR_MUSTER='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_MUSTER'),false);
				$SQL .= ' ,LAR_AUSLAUFARTIKEL='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_AUSLAUFARTIKEL'),false);
				if ($DB->FeldInhaltFormat('D',$rsQuelle->Feldinhalt('LAR_GEPRUEFT'),false)!= "'01.01.1970'")
				{
					$SQL .= ' ,LAR_GEPRUEFT='.$DB->FeldInhaltFormat('D',$rsQuelle->Feldinhalt('LAR_GEPRUEFT'),false);
				}
				$SQL .= ' ,LAR_APP_ID='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_APP_ID'),false);
				$SQL .= ' ,LAR_PROBLEMARTIKEL='.$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_PROBLEMARTIKEL'),false);
				if (($rsZiel->Feldinhalt('LAR_PROBLEMBESCHREIBUNG') == '') or is_null($rsZiel->Feldinhalt('LAR_PROBLEMBESCHREIBUNG')))
				{
					$SQL .= ' ,LAR_PROBLEMBESCHREIBUNG = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_PROBLEMBESCHREIBUNG'), false);
				}
				else if (stripos($rsZiel->Feldinhalt('LAR_PROBLEMBESCHREIBUNG'),$rsQuelle->Feldinhalt('LAR_PROBLEMBESCHREIBUNG')) === false)
				{
					$SQL .= ' ,LAR_PROBLEMBESCHREIBUNG = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_PROBLEMBESCHREIBUNG'), false) .'|| \' \' || LAR_PROBLEMBESCHREIBUNG';
				}
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_KEY'));
				
				$ProtokollArray['AKTION'] = 'Neuen Datensatz gefunden, Daten aus alten Lieferantenartikel �bernehmen';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UPDATE'; 		
				Protokoll($fp, $ProtokollArray);
				
				$DB->Ausfuehren($SQL,'',true);
				
				//undo Datensatz protokollieren
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsZiel->Feldinhalt('LAR_BEMERKUNG'), false);
				$SQL .= ' ,LAR_ALTERNATIVARTIKEL='.$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_ALTERNATIVARTIKEL'),false);
				$SQL .= ' ,LAR_MUSTER='.$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_MUSTER'),false);
				$SQL .= ' ,LAR_AUSLAUFARTIKEL='.$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_AUSLAUFARTIKEL'),false);
				if ($DB->FeldInhaltFormat('D',$rsZiel->Feldinhalt('LAR_GEPRUEFT'),false)!= "'01.01.1970'")
				{
					$SQL .= ' ,LAR_GEPRUEFT='.$DB->FeldInhaltFormat('D',$rsZiel->Feldinhalt('LAR_GEPRUEFT'),false);
				}
				$SQL .= ' ,LAR_APP_ID='.$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_APP_ID'),false);
				$SQL .= ' ,LAR_PROBLEMARTIKEL='.$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_PROBLEMARTIKEL'),false);
				$SQL .= ' ,LAR_PROBLEMBESCHREIBUNG = ' .$DB->FeldInhaltFormat('T',$rsZiel->Feldinhalt('LAR_PROBLEMBESCHREIBUNG'), false);
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_KEY'));

				$ProtokollArray['AKTION'] = 'Undo Update neuen Datensatz aus alten Lieferantenartikel';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
				Protokoll($fp, $ProtokollArray);
			    
                //alten DS auf Auslauf setzen
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				if (($rsQuelle->Feldinhalt('LAR_BEMERKUNGEN') == '') or is_null($rsQuelle->Feldinhalt('LAR_BEMERKUNGEN')))
				{
					$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$Bemerkung, false).',';
				}
				else if (stripos($rsQuelle->Feldinhalt('LAR_BEMERKUNGEN'),$Bemerkung) === false)
				{
					$SQL .= ' LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || \' \' || '.$DB->FeldInhaltFormat('T',$Bemerkung, false) .',';
				}
				$SQL .= ' LAR_AUSLAUFARTIKEL=1';
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_KEY'));
				
				$ProtokollArray['AKTION'] = 'Neuen Datensatz gefunden, alten Lieferantenartikel auf Auslauf und Bemerkung';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UPDATE'; 		
				Protokoll($fp, $ProtokollArray);
				
				$DB->Ausfuehren($SQL,'',true);
				
				//undo Datensatz protokollieren
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_BEMERKUNG'), false);
				$SQL .= ' ,LAR_AUSLAUFARTIKEL = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_AUSLAUFARTIKEL'), false);
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_KEY'));

				$ProtokollArray['AKTION'] = 'Undo Update alter Lieferantenartikel Auslauf und Bemerkung';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
				Protokoll($fp, $ProtokollArray);
				
				//Gibt es Eintr�ge in Teileinfos f�r AST/LAR ALTE LAR_LARTNR
				$SQL = 'SELECT * FROM TEILEINFOS';
				$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\'';
				$SQL .= ' AND TEI_ITY_ID2 = \'LAR\'';
				$SQL .= ' AND TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_KEY'));
				$SQL .= " AND TEI_USER NOT IN ('WWS','XXX')";

				$ProtokollArray['AKTION'] = 'Laden Teileinfos Kombination AST/LAR alte Nummer';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'INFO'; 		
				Protokoll($fp, $ProtokollArray);
				
				$rsTEI_ALT = $DB->RecordSetOeffnen($SQL);
				
				if (!$rsTEI_ALT->EOF()) //Mind. ein alter Teileinfosatz vorhanden
				{
					while (!$rsTEI_ALT->EOF())
					{
						$ProtokollArray['AKTION'] = 'Gefunden Teileinfos Kombination AST/LAR alte Nummer';
						$ProtokollArray['AKTION'] .= ' TEI_KEY=' .$rsTEI_ALT->FeldInhalt('TEI_KEY');
						$ProtokollArray['SQL'] = '';
						$ProtokollArray['KATEGORIE'] = 'INFO'; 		
						Protokoll($fp, $ProtokollArray);
						
						//Gibt es diese Verkn�pfung auch f�r den neuen Lieferantenartikel?
							
        				$SQL = 'SELECT * FROM TEILEINFOS';
        				$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\'';
        				$SQL .= ' AND TEI_ITY_ID2 = \'LAR\'';
        				$SQL .= ' AND TEI_KEY1 = ' .$DB->FeldInhaltFormat('Z',$rsTEI_ALT->Feldinhalt('TEI_KEY1'));
        				$SQL .= ' AND TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_KEY'));
        				$SQL .= " AND TEI_USER NOT IN ('WWS','XXX')";
        				
						$ProtokollArray['AKTION'] = 'Pr�fung Teileinfos Kombination AST/LAR neue Nummer';
						$ProtokollArray['SQL'] = $SQL; 		
						$ProtokollArray['KATEGORIE'] = 'INFO'; 		
						Protokoll($fp, $ProtokollArray);
						
						$rsTEI_NEU = $DB->RecordSetOeffnen($SQL);
						
                        if($rsTEI_NEU->EOF()) //Neuen DS anlegen
                        {                           
							$SQL = 'INSERT INTO TEILEINFOS (';
							$SQL .= 'TEI_ITY_ID1,';
							$SQL .= 'TEI_KEY1,';
							$SQL .= 'TEI_SUCH1,';
							$SQL .= 'TEI_WERT1,';
							$SQL .= 'TEI_ITY_ID2,';
							$SQL .= 'TEI_KEY2,';
							$SQL .= 'TEI_SUCH2,';
							$SQL .= 'TEI_WERT2,';
							$SQL .= 'TEI_USER,';
							$SQL .= 'TEI_USERDAT';
							$SQL .= ') VALUES (';
							// AST Teil = alte Werte �bernehmen
							$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_ITY_ID1')).',';
							$SQL .= $DB->FeldInhaltFormat('Z',$rsTEI_ALT->Feldinhalt('TEI_KEY1')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_SUCH1')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_WERT1')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_ITY_ID2')).',';
							// LAR Teil = neue Werte bef�llen
							$SQL .= $DB->FeldInhaltFormat('Z',$rsZiel->Feldinhalt('LAR_KEY')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsZiel->Feldinhalt('LAR_SUCH_LARTNR')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsZiel->Feldinhalt('LAR_LARTNR')).',';
							$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_USER'),false) .',';
							$SQL .= 'SYSDATE';
							$SQL .= ')';

							$ProtokollArray['AKTION'] = 'Datensatz nicht gefunden, Insert Teileinfos Kombination AST/LAR neue Nummer';
							$ProtokollArray['SQL'] = $SQL; 		
							$ProtokollArray['KATEGORIE'] = 'INSERT'; 		
							Protokoll($fp, $ProtokollArray);
							
							$DB->Ausfuehren($SQL,'',true);
							
							$SQL = "SELECT seq_TEI_KEY.CurrVal AS KEY FROM DUAL";
							$rsSeqTEIKey = $DB->RecordSetOeffnen ($SQL);

							$SQL  = 'DELETE FROM TEILEINFOS';
							$SQL .= ' WHERE TEI_KEY = ' .$DB->FeldInhaltFormat('Z',$rsSeqTEIKey->FeldInhalt('KEY'));
							$ProtokollArray['AKTION'] = 'Undo Insert Teileinfos Kombination AST/LAR neue Nummer';
							$ProtokollArray['SQL'] = $SQL; 		
							$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
							Protokoll($fp, $ProtokollArray);
                        }	
						else
						{
							//nichts machen
							$ProtokollArray['AKTION'] = 'Gefunden Teileinfos Kombination AST/LAR neue Nummer, nichts machen';
							$ProtokollArray['SQL'] = '';
							$ProtokollArray['KATEGORIE'] = 'INFO'; 		
							Protokoll($fp, $ProtokollArray);
						} //endif
						
						$rsTEI_ALT->DSWeiter();

					} //end while
				}
				
				$rsZiel->DSWeiter();
			}
		}
		else
		{
			$ProtokollArray['AKTION'] = 'Lieferantenartikel fuer neuen Lieferant nicht gefunden';
			$ProtokollArray['SQL'] = ''; 		
			$ProtokollArray['KATEGORIE'] = 'INFO'; 		
			Protokoll($fp, $ProtokollArray);
			
			$SQL = 'SELECT * FROM TEILEINFOS';
			$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\'';
			$SQL .= ' AND TEI_ITY_ID2 = \'LAR\'';
			$SQL .= ' AND TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->Feldinhalt('LAR_KEY'));
			
			$ProtokollArray['AKTION'] = 'Pr�fung auf Teileinfos';
			$ProtokollArray['SQL'] = ''; 		
			$ProtokollArray['KATEGORIE'] = 'INFO'; 		
			Protokoll($fp, $ProtokollArray);
			
			$rsTEI = $DB->RecordSetOeffnen($SQL);
			$Geloescht = false;
			while(!$rsTEI->EOF())
			{
				if($rsTEI->FeldInhalt('TEI_USER')=='XXX')
				{
					$Geloescht = true;
				}
				$rsTEI->DSWeiter();
			}
				
			if (($rsQuelle->FeldInhalt('LAR_IMQ_ID')== 4) or
			    ($rsQuelle->FeldInhalt('LAR_IMQ_ID')== 6 AND $Geloescht))
		    {
		    	// Bemerkung
		    	// Drehen
   
    			$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				if (stripos($rsQuelle->Feldinhalt('LAR_BEMERKUNGEN'),$Bemerkung) === false)
				{
					$SQL .= " LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || ' ' || " .$DB->FeldInhaltFormat('T',$Bemerkung, false) .',';
				}
    			$SQL .= ' LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$LieferantNeu,false);
    			$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->FeldInhalt('LAR_KEY'),false);
    			
    			$ProtokollArray['AKTION'] = 'Lieferantenartikel ist IMQ 4/6 und WWS gel�scht -> LIEF drehen';
    			$ProtokollArray['SQL'] = $SQL;
    			$ProtokollArray['KATEGORIE'] = 'UPDATE';
    			Protokoll($fp, $ProtokollArray);
    			 
    			$DB->Ausfuehren($SQL,'',true);
    
    			$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
    			$SQL .= ' LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LIE_NR'),false);
    			$SQL .= ' ,LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_BEMERKUNGEN'),false);
    			$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->FeldInhalt('LAR_KEY'),false);
    
    			$ProtokollArray['AKTION'] = 'Undo LIEF drehen';
    			$ProtokollArray['SQL'] = $SQL; 		
    			$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
    			Protokoll($fp, $ProtokollArray);
		    }
		    elseif ((($rsQuelle->FeldInhalt('LAR_IMQ_ID')== 2) or ($rsQuelle->FeldInhalt('LAR_IMQ_ID')== 6))
		             AND !$Geloescht)
		    {
	    		// Auslauf-KZ setzen
	    		// Bemerkung
    			$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				if (stripos($rsQuelle->Feldinhalt('LAR_BEMERKUNGEN'),$Bemerkung) === false)
				{
					$SQL .= " LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || ' ' || " .$DB->FeldInhaltFormat('T',$Bemerkung, false).',';
				}
    			$SQL .= ' LAR_AUSLAUFARTIKEL = 1';
    			$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->FeldInhalt('LAR_KEY'),false);
    			
    			$ProtokollArray['AKTION'] = 'Lieferantenartikel ist IMQ 2/6 und WWS nicht gel�scht -> LIEF Auslauf setzen';
    			$ProtokollArray['SQL'] = $SQL;
    			$ProtokollArray['KATEGORIE'] = 'UPDATE';
    			Protokoll($fp, $ProtokollArray);
    			 
    			$DB->Ausfuehren($SQL,'',true);
    
    			$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
    			$SQL .= ' LAR_AUSLAUFARTIKEL = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_AUSLAUFARTIKEL'),false);
    			$SQL .= ' ,LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_BEMERKUNGEN'),false);
    			$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsQuelle->FeldInhalt('LAR_KEY'),false);
    
    			$ProtokollArray['AKTION'] = 'Undo Auslauf setzen';
    			$ProtokollArray['SQL'] = $SQL; 		
    			$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
    			Protokoll($fp, $ProtokollArray);
		    }
		}

		$rsQuelle->DSWeiter();
		
	} //end while

	//$DB->TransaktionRollback();
	$DB->TransaktionCommit();
	$DB->Schliessen();
	fclose($fp);
	echo "fertig";
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}

?>
