<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">
<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
$SK_KEY=0;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";
$con = awisLogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Zukauf');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Zukauf'].'</title>';
?>
</head>
<body>
<?
include ("ATU_Header.php");	// Kopfzeile
$con = awisLogon();

awis_ZeitMessung(0);

$Recht10001 = awisBenutzerRecht($con,10001);
if($Recht10001==0)
{
    awisEreignis(3,1000,'Zukauf-Artikelstamm',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$cmdAktion='';

if(isset($_GET['cmdAktion']))
{
	$cmdAktion = $_GET['cmdAktion'];
}
if(isset($_POST['cmdSuche_x']))
{
	$cmdAktion='Details';
}

if(isset($_POST['cmdXReset_x']))
{
	awis_BenutzerParameterSpeichern($con, "ZukaufArtikelstamm", $AWISBenutzer->BenutzerName(),'');
	$cmdAktion='Suche';
}

/************************************************
* Daten anzeigen
************************************************/

awis_RegisterErstellen(10001, $con, $cmdAktion);


awis_FORM_SchaltflaechenStart();
awis_FORM_Schaltflaeche('href','cmdZurueck','../index.php','/bilder/zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
awis_FORM_Schaltflaeche('image','cmdXReset','','/bilder/radierer.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'R');
awis_FORM_Schaltflaeche('script','cmdHilfe',"onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=Zukaufartikel','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",'/bilder/hilfe.png',$AWISSprachKonserven['Wort']['lbl_hilfe'],'H');
awis_FORM_SchaltflaechenEnde();

awislogoff($con);
echo '</form>';
?>
</body>
</html>

