<?php
//**************************************************************
// Liefert die Daten f�r das Testformular
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('/daten/web/gdvService/felder/felder_funktionen.php');

if (!isset($_GET['WERT'])) {
     die('##PARAM##');
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Felder = new felder_funktionen();

$SQL = 'SELECT CNAME, CNAME FROM SYS.COL ';
$SQL .= ' WHERE TNAME = ' . $DB->WertSetzen('SYS', 'T', $_GET['txtTABELLE']);
$SQL .= ' ORDER BY 1';
$rsGDV = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SYS'));

$Datei = fopen('debug.log', 'w');
fwrite($Datei, $DB->LetzterSQL());
foreach ($_GET as $Key => $Val) {
    fwrite($Datei, PHP_EOL . $Key . ' = ' . $Val);
}

if (isset($_GET['WERT'])) {
    echo $_GET['WERT'];
}
echo '#~#';        // Trennzeichen

$DS = 0;

if (isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ'] != '') {
    $Zusatz = explode('~', urldecode($_GET['ZUSATZ']));
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
        echo '<option selected="selected" value="' . $Zusatz[0] . '">' . $Zusatz[1] . '</option>';
    } else {
        echo '#+#' . $Zusatz[0] . '#+#' . $Zusatz[1] . '';
    }
}

while (!$rsGDV->EOF()) {
    if (++$DS > 200) {
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
            echo '<option value="' . $rsGDV->FeldInhalt(1) . '">' . $rsGDV->FeldInhalt(2) . '</option>';
        } else {
            echo '#+#' . $rsGDV->FeldInhalt(1) . '#+#' . $rsGDV->FeldInhalt(2) . '';
        }
    } elseif ($DS < 200) {
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
            echo '<option value="' . $rsGDV->FeldInhalt(1) . '">' . $rsGDV->FeldInhalt(2) . '</option>';
        } else {
            echo '#+#' . $rsGDV->FeldInhalt(1) . '#+#' .  $rsGDV->FeldInhalt(2) . '';
        }
    }

    $rsGDV->DSWeiter();
}
if (++$DS > 200) {
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') === false) {
        echo '<option value="">...</option>';
    } else {
        echo '#+##+#...';
    }
}

?>