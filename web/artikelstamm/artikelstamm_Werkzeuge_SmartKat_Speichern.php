<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $SK;

$SK->ladebalkenStart();

$SpeichernOK = false;

if (isset($SK->Param['ZuVieleDS']) and $SK->Param['ZuVieleDS'] == true) {

    if (isset($SK->Param['txtchkAlle'])) {
        $Wert = 1;
    } else {
        $Wert = 0;
    }
    $SpeichernOK = $SK->MergeAlle($Wert);
} else {
    $TEIs = $SK->Form->NameInArray($_POST, 'txtTII_SK_SPERRE_', 2, 1);
    $TEI_KEYs = array();
    foreach ($TEIs as $TEI) {
        $TEI_KEYs[] = explode('_', $TEI)[3];
        unset($_POST['old' . substr($TEI, 3)]); //unsetten, f�r sp�ter
        unset($_POST['txt' . substr($TEI, 3)]);
    }

    $SpeichernOK = $SK->MergeAuserwaehlte($TEI_KEYs, 1);

    if ($SpeichernOK) { //1er gespeichert OK
        $TEI_KEYs = array();
        //Wenn es jetzt noch Olds gibt, dann ist kein Haken im new gewesen (und kommt deswegen auch nicht im POST)
        $OldTEIs = $SK->Form->NameInArray($_POST, 'oldTII_SK_SPERRE_', 2, 1);
        foreach ($OldTEIs as $TEI) {
            $TEI_KEYs[] = explode('_', $TEI)[3];
        }
        $SpeichernOK = $SK->MergeAuserwaehlte($TEI_KEYs, 0);
    }
}

if ($SpeichernOK) {
    $SK->Form->ZeileStart();
    $SK->Form->Hinweistext($SK->Form->LadeTextBaustein('AST', 'AST_SK_SPEICHERN_OK', $SK->AWISBenutzer->BenutzerSprache()), awisFormular::HINWEISTEXT_OK);
    $SK->Form->ZeileEnde();
    $SK->Param = array(); //Alle Parameter vernichten
} else {
    $SK->Form->ZeileStart();
    $SK->Form->Hinweistext($SK->Form->LadeTextBaustein('AST', 'AST_SK_SPEICHERN_NOK', $SK->AWISBenutzer->BenutzerSprache()), awisFormular::HINWEISTEXT_FEHLER);
    $SK->Form->ZeileEnde();
}
$SK->ladebalkenEnde();
?>