<?php
require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $StatParam;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort', 'AlleAnzeigen');    
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21034 = $AWISBenutzer->HatDasRecht(21034);
	if($Recht21034 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
	
    $Seite = 'Monat';       // Subreiter => wird z.B. bei Sortierungen verwendet
    
    $FeldBreiten = array();
    $FeldBreiten['Monat'] = 380;
    //$FeldBreiten['Info'] = 320;
    $FeldBreiten['Zahl'] = 120;    
	
    //************************************************************
	//* Hier nochmal Parameter anzeigen, die als Filter benutzt werden
	//************************************************************
    $Form->Trennzeile('O');
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'].':', 200);
    $Form->Erstelle_TextFeld('UNF_FIL_ID', $StatParam['STAT_UNF_FilNr'], 5, 200, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERVON', $StatParam['STAT_UNF_DatumVon'], 6, 86, false, '', '', '', 'D');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERBIS', $StatParam['STAT_UNF_DatumBis'], 6, 70, false, '', '', '', 'D');
    $Form->ZeileEnde();        
    
    $StatParam['STAT_UNF_Bereich'] = $AWISSprachKonserven['Wort']['AlleAnzeigen'];      // pauschal mit "Alle Anzeigen" vorbelegen, da hier Auswahl aus �berReiterSeite noch keinen Sinn macht
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEREICH'].':', 200);
    $Form->Erstelle_TextFeld('UNF_BEREICHID', $StatParam['STAT_UNF_Bereich'], 100, 200, false);
    $Form->ZeileEnde();    
    
    $Form->Trennzeile('O');
    
    //************************************************************
	//* �berschriftenzeile der Listenansicht
	//************************************************************
    $Form->ZeileStart();    
    // �berschrift der Listenansicht mit Sortierungslink: Rang
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=monat'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='monat'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['MONAT'], $FeldBreiten['Monat'], '', $Link);
    // �berschrift der Listenansicht mit Sortierungslink: Zahl
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=anzahl'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='anzahl'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ZAHL'], $FeldBreiten['Zahl'], '', $Link);
    $Form->ZeileEnde();        
    
    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Start
	//************************************************************
    $Bedingung = '';        // Var zum Zusammenbauen der Filter
    // immer wenn Parameter da, mit and an Bedingungsstring anh�ngen
    if (! empty($StatParam['STAT_UNF_FilNr']))
    {
        $Bedingung .= ' and unf.unf_fil_id = ' . $StatParam['STAT_UNF_FilNr'];
    }
    if (! empty($StatParam['STAT_UNF_DatumVon']))
    {
        $Bedingung .= ' and unf.unf_datum >= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumVon']);    
    }
    if (! empty($StatParam['STAT_UNF_DatumBis']))
    {
        $Bedingung .= ' and unf.unf_datum <= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumBis']);    
    }
	if ($Bedingung != '')
	{
		$Bedingung = $Bedingung . ' ';
	}    
    
    $SQL =  '';
    $SQL .= 'select * from (';
    // Schleife �ber alle Monate
    for ($Idx = 1; $Idx <= 12; $Idx++)
    {
        $SQL .= 'select \'' . str_pad($Idx, 2, '0', STR_PAD_LEFT) . '\' as monat, count(*) as anzahl from v_unfallmeldung unf where to_number(to_char(unf_datum, \'mm\')) = ' . $Idx . ' ' . $Bedingung;
        if ($Idx != 12)         // nachm Dezember kein 'union' mehr anf�gen
        {
            $SQL .= 'union ';
        }
    }
    $SQL .= ') ';
    if (isset($_GET['STATSort']))
    {
        $SQL .= 'order by ' . str_replace('~',' DESC ', $_GET['STATSort']);
    }
    else
    {
        $SQL .= 'order by monat';
    }    
    
    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Ende
	//************************************************************    
    $rs = $DB->RecordSetOeffnen($SQL);    
    
    //************************************************************
	//* Listen in Schleife aufbauen
	//************************************************************        
    $DS = 0;	// f�r Hintergrundfarbumschaltung
    $AnzahlGesamt = 0;
    while(! $rs->EOF())
    {
        // Array mit allen Monatsnamen, damit in Maske nicht die Monatsnummern stehen
        // Index 0 = dummy, damit Zahl von "$rs->FeldInhalt('MONAT')" direkt dem Array-Index entspricht
        $Monate = array('dummy', 'Januar', 'Februar', 'M�rz', 'April', 'Mai', 'Juni', 
                        'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'); 
        
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('*MONAT', $Monate[intval($rs->FeldInhalt('MONAT'))], 50, $FeldBreiten['Monat'], false, ($DS%2), '', '', 'T', 'L');
        $Form->Erstelle_ListenFeld('*ZAHL', $rs->FeldInhalt('ANZAHL'), 10, $FeldBreiten['Zahl'], false, ($DS%2), '', '', 'N0', 'L');		
        $AnzahlGesamt += $rs->FeldInhalt('ANZAHL');     // Gesamtzahl hochaddieren
        $Form->ZeileEnde();
        $DS++;
        $rs->DSWeiter();
    }
    // Gesamtzahl noch hinpinseln
    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ANZAHLGESAMT'].': '.$AnzahlGesamt, $FeldBreiten['Monat']+$FeldBreiten['Zahl']+4, 'font-weight:bolder');
    $Form->ZeileEnde();
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>