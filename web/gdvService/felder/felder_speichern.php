<?php
/**
 * Speichern Stammdatenpflege GDV-Felder
 *
 * @author Sch�ffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */

require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $Fehler;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('GDV','HINWEIS');

try
{

	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if (isset($FelderNew) and $FelderNew != '') //Neuanlage
	{
		$SQL =' insert into versfelder ';
		$SQL .=' (VSF_VERSNR,VSF_VERSWELTFELD,VSF_GDVPFAD,VSF_KONSTANT,VSF_FORMAT,VSF_INDEX,VSF_USER,VSF_USERDAT)';
		$SQL .=' values';
		$SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtVVW_VERSNR']);
		$SQL .=', '.$DB->FeldInhaltFormat('T',$_POST['txtFELDTYPWERT']);
		$SQL .=', '.$DB->FeldInhaltFormat('T',$_POST['txtGDVPFAD']);
		if($_POST['txtGDV_FELDTYP'] == 2) {
			$SQL .=', '.$DB->FeldInhaltFormat('T','WERT:'.$_POST['txtGDV_FELDTYPWERT'.$_POST['txtGDV_FELDTYP']]);
		}
		elseif($_POST['txtGDV_FELDTYP'] == 3){
			$SQL .=', '.$DB->FeldInhaltFormat('T','FUNC:'.$_POST['txtGDV_FELDTYPWERT'.$_POST['txtGDV_FELDTYP']]);
		}
		elseif($_POST['txtGDV_FELDTYP'] == 4){
			$SQL .=', '.$DB->FeldInhaltFormat('T','INVOICE:'.$_POST['txtGDV_FELDTYPWERT'.$_POST['txtGDV_FELDTYP']]);
		}
		else {
			$SQL .=', '.$DB->FeldInhaltFormat('T','');
		}
		$SQL .=', '.$DB->FeldInhaltFormat('T',$_POST['txtGDV_FORMAT']);
		$SQL .=', '.$DB->FeldInhaltFormat('N0',$_POST['txtGDV_INDEX']);
		$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .=',sysdate ';
		$SQL .= ' )';


		$DB->Ausfuehren($SQL,'',true);
		$Fehler = false;
	}
}
catch (awisException $ex)
{
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_VERSFELDER) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = $AWISSprachKonserven['GDV']['HINWEIS'];

		$Form->Hinweistext($Hinweis);
	}
	else {
		$Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
		$Form->DebugAusgabe(1, $ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
