<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class sicherheitsanlagen_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht50000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht50000 = $this->AWISBenutzer->HatDasRecht(50000);
        $this->_EditRecht = (($this->Recht50000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_Sicherheitsanlagen'));

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[]=array('BMK','%');
        $TextKonserven[]=array('BMV','%');
        $TextKonserven[]=array('BMR','%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
        $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'WirklichLoeschen');
        $TextKonserven[] = array('Wort', 'Ja');
        $TextKonserven[] = array('Wort', 'Nein');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_Sicherheitsanlagen');
        $TextKonserven[] = array('FIL', 'FIL_ID');
        $TextKonserven[] = array('FIL', 'FIL_BEZ');
        $TextKonserven[] = array('FIL', 'FIL_STRASSE');
        $TextKonserven[] = array('FIL', 'FIL_PLZ');
        $TextKonserven[] = array('FIL', 'FIL_ORT');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_Sicherheitsanlagen', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function DSButton_AWISKEY($LetzterAWIS_KEY1,$Operator){
        //FilID des letzten DS Ermitteln
        $SQL = 'select bmv_fil_id from BMAVERTRAEGE where bmv_key = ' . $this->DB->WertSetzen('DSW','N0',$LetzterAWIS_KEY1);
        $rsDSW = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('DSW'));

        $SQLFil = 'SELECT * FROM (';
        $SQLFil .= ' select distinct FIL_ID ' ;
        $SQLFil .= ' from v_filialen_aktuell a ' ;
        $SQLFil .= ' inner join bmavertraege b ' ;
        $SQLFil .= ' on a.fil_id = b.bmv_fil_id ' ;
        $SQLFil .= ' WHERE FIL_ID '.$Operator. ' '. $this->DB->WertSetzen('FIL','N0',$rsDSW->FeldInhalt(1));
        $SQLFil .= ' order by 1 '.($Operator=='>'?'asc':($Operator=='<'?'desc':'')).' )';
        $SQLFil .= ' WHERE ROWNUM = 1';

        $rsFil = $this->DB->RecordSetOeffnen($SQLFil,$this->DB->Bindevariablen('FIL'));

        return $rsFil->FeldInhalt('FIL_ID');
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht50000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'BMV'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function erstelleATUNrInfos($ATU_NR, $Hersteller = '')
    {
        $SQL = 'SELECT AST.AST_ATUNR, AST.AST_BEZEICHNUNGWW, RST.RST_LIE_NR, REH.REH_BEZEICHNUNG';
        $SQL .= ' FROM REIFENSTAMM RST';
        $SQL .= ' INNER JOIN V_REIFENHERSTELLER REH';
        $SQL .= ' ON REH.REH_KEY = RST.RST_REH_KEY';
        $SQL .= ' INNER JOIN REIFENPFLEGE RRP';
        $SQL .= ' ON RRP.RRP_RST_KEY = RST.RST_KEY';
        $SQL .= ' INNER JOIN ARTIKELSTAMM AST';
        $SQL .= ' ON RRP.RRP_ATUNR = AST.AST_ATUNR ';

        $SQL .= ' WHERE AST_ATUNR =' . $this->DB->WertSetzen('AST', 'TU', $ATU_NR);

        $rsAst = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('AST'));

        $this->Form->ZeileStart();
        $Style = '';
        $Text = $rsAst->FeldInhalt('AST_BEZEICHNUNGWW');
        if ($rsAst->EOF()) {
            $Text = $this->Form->LadeTextBaustein('BMV', 'BMV_ERR_UNBEKANNTE_ATUNR', $this->AWISBenutzer->BenutzerSprache());
            $Style = 'font-weight: bold; color: #FF0000';
        }
        $this->Form->Erstelle_TextLabel($this->Form->LadeTextBaustein('AST', 'AST_BEZEICHNUNGWW') . ':', 170, $Style);
        $this->Form->Erstelle_TextLabel($Text, 500, $Style);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();

        if($Hersteller==''){
            $Hersteller = $rsAst->FeldInhalt('REH_BEZEICHNUNG');
        }
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BMV']['BMV_HERSTELLER'] . ':', 170);
        $SQL = 'select distinct reh_bezeichnung, reh_bezeichnung from V_REIFENHERSTELLER order by 1 asc';
        $this->Form->Erstelle_SelectFeld('~!BMV_HERSTELLER', $Hersteller, '', $this->_EditRecht, $SQL);
        $this->Form->ZeileEnde();
    }

    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if($AWIS_KEY1!=0) {
            $Bedingung.= ' AND BMV_KEY = ' . $this->DB->WertSetzen('BMV','N0',$AWIS_KEY1) ;
            return $Bedingung;
        }

        if(isset($this->Param['BMK_KATEGORIE']) AND $this->Param['BMK_KATEGORIE']!=0) {
            $Bedingung .= 'AND BMV_BMK_KEY =' . $this->DB->WertSetzen('BMV','N0',$this->Param['BMK_KATEGORIE']) . ' ';
        }
        if(isset($this->Param['BMV_FIL_ID']) AND $this->Param['BMV_FIL_ID']!=0) {
            $Bedingung .= 'AND BMV_FIL_ID =' . $this->DB->WertSetzen('BMV','N0',$this->Param['BMV_FIL_ID']);
        }
        if(isset($this->Param['BMV_VERTRAGSNR']) AND $this->Param['BMV_VERTRAGSNR']!='') {
            $Bedingung .= 'AND BMV_VERTRAGSNR ' . $this->DB->LikeOderIst($this->Param['BMV_VERTRAGSNR']) . ' ';
        }
        if(isset($this->Param['BMV_TNNL']) AND $this->Param['BMV_TNNL']!='') {
            $Bedingung .= 'AND upper(BMV_TNNL) ' . $this->DB->LikeOderIst($this->Param['BMV_TNNL']) . ' ';
        }

        return $Bedingung;
    }

    public function erstelleKopfAjax($FIL) {

        $SQL = 'select * from filialen ';
        $SQL .= ' where ';

        if (intval($FIL) or $FIL == '') {
            $SQL .= ' FIL_ID = ' . $this->DB->WertSetzen('FIL','N0',$FIL);
        }else{
            $SQL .= ' lower(FIL_BEZ) like ' . $this->DB->WertSetzen('FIL','T','%'.mb_strtolower($FIL).'%');
        }

        $rsFil = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('FIL',true));

        $FeldBreiten['Labels'] = 200;
        $FeldBreiten['Werte'] = 200;
        $FeldBreiten['FIL_PLZ'] = 100;
        $FeldBreiten['FIL_ORT'] = 150;

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['FIL']['FIL_ID'] . ':', $FeldBreiten['Labels']);
        $this->Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ID'),$FeldBreiten['Werte']);
        $this->Form->Erstelle_HiddenFeld('BMV_FIL_ID',$rsFil->FeldInhalt('FIL_ID'));
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['FIL']['FIL_BEZ'] . ':', $FeldBreiten['Labels']);
        $this->Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_BEZ'),$FeldBreiten['Werte']);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['FIL']['FIL_STRASSE'] . ':', $FeldBreiten['Labels']);
        $this->Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_STRASSE'),$FeldBreiten['Werte']);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['FIL']['FIL_PLZ'].'/'.$this->AWISSprachKonserven['FIL']['FIL_ORT'].':',$FeldBreiten['Labels']);
        $this->Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_PLZ'),$FeldBreiten['FIL_PLZ']);
        $this->Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ORT'),$FeldBreiten['FIL_ORT']);
        $this->Form->ZeileEnde();
    }


}