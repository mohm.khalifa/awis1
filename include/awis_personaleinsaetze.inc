<?php
/**
 * awis_personaleinsaetze.inc
 *
 *
 * @author Thomas Riedl
 * @copyright ATU - Auto-Teile-Unger
 * @version 20110124
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_Personaleinsaetze
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Debuglevel für Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;

	/**
	 * Exportdateiname
	 * @var string
	 */
	//private $_ExportDateiname = '/daten/daten/portal/portalzugang.dat';

	/**
	 * Importdateiname aus dem Portal
	 * @var string
	 */
	private $_ExportDateiname = '/daten/daten/pccommon/Riedl/personaleinsaetze.csv';

	/**
	 * Formularobjekt
	 * @var awisFormular
	 */
	private $_FORM = null;
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);

		$this->_FORM = new awisFormular();
	}


	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);
		}

		return $this->_DebugLevel;
	}

	/**
	 * Exportiert die Personaleinsätze des aktuellen Tages in eine Datei
	 */
	public function PersonalEinsatzExport()
	{
		$Werkzeug = new awisWerkzeuge();
		
		$SQL = 'SELECT DISTINCT pei_user, pei_xbn_key, peb_key, peb_bezeichnung';
        $SQL.= ' FROM Personaleinsaetze';
        $SQL.= ' inner join perseinsbereichemitglieder on pbm_xbn_key = pei_xbn_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >=sysdate';
        $SQL.= ' inner join perseinsbereiche on peb_key = pbm_peb_key';
        $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
        $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
        $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
        $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
        $SQL.= ' INNER JOIN filialebenenrollenzuordnungen on frz_kon_key = kon_key and frz_fer_key = 23 and frz_gueltigab <= sysdate and frz_gueltigbis >= sysdate';
        $SQL.= ' WHERE TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        $SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        //$SQL.= ' WHERE PEI_DATUM >= TO_DATE (\'02.01.2008\', \'DD.MM.RRRR\')';
        //$SQL.= ' AND PEI_DATUM <= TO_DATE (\'02.01.2008\', \'DD.MM.RRRR\')';        
        $SQL.= ' order by peb_bezeichnung';
        
        $rsPEBRL = $this->_DB->RecordSetOeffnen($SQL);
        //echo $SQL.PHP_EOL;
		if($this->_DebugLevel> 4)
		{
			echo 'Exportiere Personaleinsätze...'.PHP_EOL;
		}
		
		$fp = fopen($this->_ExportDateiname,'w');		// Daten werden angehängt, falls Übertragung nicht geht

		$DS = 0;
		$Fehler = 0;

		if(!$rsPEBRL->EOF())
		{
			fputs($fp,"#\n# Exportdatei ".date('d.m.Y')."\n#\n");
		}
		
		while(!$rsPEBRL->EOF())
		{
			//Alle Personaleinsätze des RL ausgeben			
			$SQL = 'SELECT DISTINCT Personaleinsaetze.*, ';
			$SQL.= ' xbn_name AS Mitarbeiter,  XBN_KON_KEY, ';
			$SQL.= ' XTZVON.XTZ_TAGESZEIT AS ZEITVON, XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, ';
            $SQL.= ' PEA_BEZEICHNUNG, FIL_BEZ';
            $SQL.= ' FROM Personaleinsaetze';
            $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
            $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
            $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
            $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
            $SQL.= ' WHERE PEI_XBN_KEY = '.$rsPEBRL->FeldInhalt('PEI_XBN_KEY');
            $SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        	$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
            $SQL.= ' AND EXISTS (SELECT * FROM V_FILIALEBENENROLLEN WHERE xx1_fer_key = 023 AND xx1_kon_key = KON_KEY)';
        	$SQL.= ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC';
			
			$rsPEIRL = $this->_DB->RecordSetOeffnen($SQL);
        	//echo $SQL.PHP_EOL;
			while (!$rsPEIRL->EOF())
			{
				echo '1 - '.$rsPEIRL->FeldInhalt('MITARBEITER').PHP_EOL;
				
				fputs($fp,'RL;');
				fputs($fp,$rsPEIRL->FeldInhalt('MITARBEITER').';');
				fputs($fp,$rsPEIRL->FeldInhalt('PEI_DATUM').';');
				fputs($fp,$rsPEIRL->FeldInhalt('ZEITVON').';');
				fputs($fp,$rsPEIRL->FeldInhalt('ZEITBIS').';');
				fputs($fp,$rsPEIRL->FeldInhalt('PEA_BEZEICHNUNG').';');
				fputs($fp,$rsPEIRL->FeldInhalt('PEA_FIL_ID').' - '.$rsPEIRL->FeldInhalt('FIL_BEZ'));
				fputs($fp,"\n");
				
				$rsPEIRL->DSWeiter();	
			}
        	
			//Alle Personaleinsätze der GBL ausgeben
			$SQL = 'SELECT DISTINCT pei_user, pei_xbn_key, peb_key, peb_bezeichnung';
	        $SQL.= ' FROM Personaleinsaetze';
	        $SQL.= ' inner join perseinsbereichemitglieder on pbm_xbn_key = pei_xbn_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >=sysdate';
	        $SQL.= ' inner join perseinsbereiche on peb_key = pbm_peb_key and peb_key ='.$rsPEBRL->FeldInhalt('PEB_KEY');
	        $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	        $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	        $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	        $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
	        $SQL.= ' INNER JOIN filialebenenrollenzuordnungen on frz_kon_key = kon_key and frz_fer_key = 25 and frz_gueltigab <= sysdate and frz_gueltigbis >= sysdate';
	        $SQL.= ' WHERE TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        	$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
	        //$SQL.= ' WHERE  PEI_DATUM >= sysdate AND PEI_DATUM <= sysdate';
	        $SQL.= ' order by peb_bezeichnung';
	        
	        $rsPEBGBL = $this->_DB->RecordSetOeffnen($SQL);
	        echo $SQL.PHP_EOL;
	        while(!$rsPEBGBL->EOF())
			{
				//Alle Personaleinsätze des RL ausgeben			
				$SQL = 'SELECT DISTINCT Personaleinsaetze.*, ';
				$SQL.= ' xbn_name AS Mitarbeiter,  XBN_KON_KEY, ';
				$SQL.= ' XTZVON.XTZ_TAGESZEIT AS ZEITVON, XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, ';
	            $SQL.= ' PEA_BEZEICHNUNG, FIL_BEZ';
	            $SQL.= ' FROM Personaleinsaetze';
	            $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	            $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	            $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	            $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
	            $SQL.= ' WHERE PEI_XBN_KEY = '.$rsPEBGBL->FeldInhalt('PEI_XBN_KEY');
	        	$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        		$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
	            $SQL.= ' AND EXISTS (SELECT * FROM V_FILIALEBENENROLLEN WHERE xx1_fer_key = 025 AND xx1_kon_key = KON_KEY)';
	        	$SQL.= ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC';
				echo $SQL;
				$rsPEIGBL = $this->_DB->RecordSetOeffnen($SQL);
				
				while (!$rsPEIGBL->EOF())
				{	
					echo '2 - '.$rsPEIGBL->FeldInhalt('MITARBEITER').PHP_EOL;
					
					fputs($fp,'GBL;');
					fputs($fp,$rsPEIGBL->FeldInhalt('MITARBEITER').';');
					fputs($fp,$rsPEIGBL->FeldInhalt('PEI_DATUM').';');
					fputs($fp,$rsPEIGBL->FeldInhalt('ZEITVON').';');
					fputs($fp,$rsPEIGBL->FeldInhalt('ZEITBIS').';');
					fputs($fp,$rsPEIGBL->FeldInhalt('PEA_BEZEICHNUNG').';');
					fputs($fp,$rsPEIGBL->FeldInhalt('PEA_FIL_ID').' - '.$rsPEIGBL->FeldInhalt('FIL_BEZ'));
					fputs($fp,"\n");
					
					$rsPEIGBL->DSWeiter();
				}
				$rsPEBGBL->DSWeiter();
			}
			
			//Alle Personaleinsätze der GBL ausgeben
			$SQL = 'SELECT DISTINCT pei_user, pei_xbn_key, peb_key, peb_bezeichnung';
	        $SQL.= ' FROM Personaleinsaetze';
	        $SQL.= ' inner join perseinsbereichemitglieder on pbm_xbn_key = pei_xbn_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >=sysdate';
	        $SQL.= ' inner join perseinsbereiche on peb_key = pbm_peb_key and peb_key ='.$rsPEBRL->FeldInhalt('PEB_KEY');
	        $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	        $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	        $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	        $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	        $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
	        $SQL.= ' INNER JOIN filialebenenrollenzuordnungen on frz_kon_key = kon_key and frz_fer_key = 26 and frz_gueltigab <= sysdate and frz_gueltigbis >= sysdate';
	        $SQL.= ' WHERE TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        	$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
	        //$SQL.= ' WHERE  PEI_DATUM >= sysdate AND PEI_DATUM <= sysdate';
	        $SQL.= ' order by peb_bezeichnung';
	        
	        $rsPEBTKDL = $this->_DB->RecordSetOeffnen($SQL);
	        
	        while(!$rsPEBTKDL->EOF())
			{							
				//Alle Personaleinsätze des RL ausgeben			
				$SQL = 'SELECT DISTINCT Personaleinsaetze.*, ';
				$SQL.= ' xbn_name AS Mitarbeiter,  XBN_KON_KEY, ';
				$SQL.= ' XTZVON.XTZ_TAGESZEIT AS ZEITVON, XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, ';
	            $SQL.= ' PEA_BEZEICHNUNG, FIL_BEZ';
	            $SQL.= ' FROM Personaleinsaetze';
	            $SQL.= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	            $SQL.= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	            $SQL.= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	            $SQL.= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	            $SQL.= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';
	            $SQL.= ' WHERE PEI_XBN_KEY = '.$rsPEBTKDL->FeldInhalt('PEI_XBN_KEY');
	        	$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
        		$SQL.= ' AND TO_CHAR(PEI_DATUM, \'DD.MM.YYYY\') = TO_CHAR(sysdate,\'DD.MM.YYYY\')';
	            $SQL.= ' AND EXISTS (SELECT * FROM V_FILIALEBENENROLLEN WHERE xx1_fer_key = 026 AND xx1_kon_key = KON_KEY)';
	        	$SQL.= ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC';
				
				$rsPEITKDL = $this->_DB->RecordSetOeffnen($SQL);
				
				while (!$rsPEITKDL->EOF())
				{
					echo '3 - '.$rsPEITKDL->FeldInhalt('MITARBEITER').PHP_EOL;
					
					fputs($fp,'TKDL;');
					fputs($fp,$rsPEITKDL->FeldInhalt('MITARBEITER').';');
					fputs($fp,$rsPEITKDL->FeldInhalt('PEI_DATUM').';');
					fputs($fp,$rsPEITKDL->FeldInhalt('ZEITVON').';');
					fputs($fp,$rsPEITKDL->FeldInhalt('ZEITBIS').';');
					fputs($fp,$rsPEITKDL->FeldInhalt('PEA_BEZEICHNUNG').';');
					fputs($fp,$rsPEITKDL->FeldInhalt('PEA_FIL_ID').' - '.$rsPEITKDL->FeldInhalt('FIL_BEZ'));
					fputs($fp,"\n");
				
					$rsPEITKDL->DSWeiter();
				}
				$rsPEBTKDL->DSWeiter();
			}
			$rsPEBRL->DSWeiter();
			
			//$DS++;
		}
		fclose($fp);

		/*
		if($this->_DebugLevel> 4)
		{
			echo ' Es wurden '.$DS.' Zugaenge exportiert. '.$Fehler.' Fehler.'.PHP_EOL;
		}
		*/

		//return $DS;
	}
}
?>
