<?php

global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;


try {

if(isset($_GET['ExportWEB_KEY']) && !isset($_GET['ExportWBE_KEY']))
{

       $SQL = ' Select Distinct To_Number(Wbe_Filialen) AS FILNR From Werbemittelzuordnung Where Wbe_Web_Key='.$DB->FeldInhaltFormat('NO',$_GET['ExportWEB_KEY']);
       $SQL .= ' MINUS';	
       $SQL .= ' Select DISTINCT PER_FIL_ID From Werbemittelzuordnung Inner Join Werbeeintraege';	
       $SQL .= ' ON WBE_WEB_KEY = WER_WEB_KEY inner join Personal on WER_PERSNR = PER_NR';

       $rsUnbearbeitet = $DB->RecordSetOeffnen($SQL);

	$SQL = 'Select DISTINCT Wer_Persnr, Per_Fil_Id,Per_Nachname,Per_Vorname, Per_Strasse,Per_Plz,Per_Ort,';
	$SQL .= ' BUL_BUNDESLAND,FILIALROLLE(PER_FIL_ID,23) AS Regionalleiter,FILIALROLLE(PER_FIL_ID,25) AS GBL, XX1_EBENE AS REGION,';
	$SQL .= ' Wer_Selwert1 AS Status,Wer_Selwert2 AS Werbeverweigerer';
	$SQL .= ' From Werbeeintraege Inner Join Werbemittelzuordnung On Wer_Web_Key = Wbe_Web_Key'; 
	$SQL .= ' Inner Join Personal On Per_Nr = Wer_Persnr Inner Join Filialen On Fil_Id = Per_Fil_Id';
	$SQL .= ' Left Join Filialinfos On Per_Fil_Id = Fif_Fil_Id And Fif_Fit_Id = 65';
	$SQL .= ' Left Join Bundeslaender On Bul_Id = Fif_Wert';
	$SQL .= ' inner join V_FILIALPFAD ON PER_FIL_ID = XX1_FIL_ID';
	$SQL .= ' WHERE WER_WEB_KEY='.$DB->FeldInhaltFormat('NO',$_GET['ExportWEB_KEY']);
	$SQL .= ' AND PER_AUSTRITT IS NULL';
	$SQL .= ' ORDER BY PER_FIL_ID'; 	
	
	$rsExportKomplett = $DB->RecordSetOeffnen($SQL);

	@ob_clean();
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="'.'VerteilqualitaetExportKomplett'.'.csv"');

	$Zeile='';
	$Spalten = $rsExportKomplett->FeldListe();
	
	if($rsUnbearbeitet->AnzahlDatensaetze() > 0)
	{
	   $Zeile = '';
	   
	   while(!$rsUnbearbeitet->EOF())
	   {
		
	        $Zeile .= " Filiale:.".$rsUnbearbeitet->FeldInhalt('FILNR')." wurde nicht bearbeitet";
	        
		$rsUnbearbeitet->DSWeiter();
	   }
	   $Zeile .= "\n\n";
	   echo substr($Zeile,1);
	}
	
	$Zeile = '';
	
	for($Spalte=1;$Spalte<=$rsExportKomplett->AnzahlSpalten() - 1;$Spalte++)
	{
		
		$Zeile .= ";".$Spalten[$Spalte];
		
	}
	$Zeile .= "\n";

	echo substr($Zeile,1);

	for($CSVZeile=0;$CSVZeile<$rsExportKomplett->AnzahlDatensaetze();$CSVZeile++)
	{
		$Zeile='';
		for($Spalte=1;$Spalte<=$rsExportKomplett->AnzahlSpalten() - 1;$Spalte++)
		{
			if($Spalte == 11)
			{
			  $StatusWert = $rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			
			  switch($StatusWert)	
			  {
			    case 0: {
			        $Zeile .= ";".'bitte waehlen / Keine Angabe';
			    }break;
			    case 1: {
			        $Zeile .= ";".'Werbung erhalten';
			    }break;
			    case 2: {
			        $Zeile .= ";".'Werbung nicht erhalten';
			    }break;
			  }
			
			}
			elseif($Spalte == 12)
			{
			  $StatusWert = $rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			
			  switch($StatusWert)	
			  {
			     case 0: {
			        $Zeile .= ";".'bitte waehlen / Keine Angabe';
			    }break;
			    case 1: {
			        $Zeile .= ";".'ja';
			    }break;
			    case 2: {
			        $Zeile .= ";".'nein';
			    }break;
			  }
			}
			
			if($Spalte != 11 && $Spalte !=12)
			{
				$Zeile .= ";".$rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			}
		}
		$rsExportKomplett->DSWeiter();
		$Zeile .= "\n";

		echo substr($Zeile,1);
	}
	die();
}
else
{
	$SQL = ' Select Distinct To_Number(Wbe_Filialen) AS FILNR From Werbemittelzuordnung Where Wbe_Web_Key='.$DB->FeldInhaltFormat('NO',$_GET['ExportWEB_KEY']);
        $SQL .= ' MINUS';	
        $SQL .= ' Select DISTINCT PER_FIL_ID From Werbemittelzuordnung Inner Join Werbeeintraege';	
        $SQL .= ' ON WBE_WEB_KEY = WER_WEB_KEY inner join Personal on WER_PERSNR = PER_NR';

        $rsUnbearbeitet = $DB->RecordSetOeffnen($SQL);

	$SQL = 'Select WBE_FILIALEN from WERBEMITTELZUORDNUNG WHERE WBE_KEY='.$DB->FeldInhaltFormat('T',$_GET['ExportWBE_KEY']);
	
	$rsFiliale = $DB->RecordSetOeffnen($SQL);
	
	$SQL = 'Select DISTINCT Wer_Persnr, Per_Fil_Id,Per_Nachname,Per_Vorname, Per_Strasse,Per_Plz,Per_Ort,';
	$SQL .= ' BUL_BUNDESLAND,FILIALROLLE(PER_FIL_ID,23) AS Regionalleiter,FILIALROLLE(PER_FIL_ID,25) AS GBL, XX1_EBENE AS REGION,';
	$SQL .= ' Wer_Selwert1 AS Status,Wer_Selwert2 AS Werbeverweigerer';
	$SQL .= ' From Werbeeintraege Inner Join Werbemittelzuordnung On Wer_Web_Key = Wbe_Web_Key'; 
	$SQL .= ' Inner Join Personal On Per_Nr = Wer_Persnr Inner Join Filialen On Fil_Id = Per_Fil_Id';
	$SQL .= ' Left Join Filialinfos On Per_Fil_Id = Fif_Fil_Id And Fif_Fit_Id = 65';
	$SQL .= ' Left Join Bundeslaender On Bul_Id = Fif_Wert';
	$SQL .= ' inner join V_FILIALPFAD ON PER_FIL_ID = XX1_FIL_ID';
	$SQL .= ' WHERE WER_WEB_KEY='.$DB->FeldInhaltFormat('NO',$_GET['ExportWEB_KEY']);
	$SQL .= ' AND PER_FIL_ID='.$DB->FeldInhaltFormat('NO',$rsFiliale->FeldInhalt('WBE_FILIALEN'));
	$SQL .= ' AND PER_AUSTRITT IS NULL';
	$SQL .= ' ORDER BY PER_FIL_ID'; 	
	
	$rsExportKomplett = $DB->RecordSetOeffnen($SQL);

	@ob_clean();
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="'.'VerteilqualitaetExportFiliale'.'.csv"');

	$Zeile='';
	$Spalten = $rsExportKomplett->FeldListe();
	
	if($rsUnbearbeitet->AnzahlDatensaetze() > 0)
	{
	   $Zeile = '';
	   
	   while(!$rsUnbearbeitet->EOF())
	   {
		
	        $Zeile .= " Filiale:.".$rsUnbearbeitet->FeldInhalt('FILNR')." wurde nicht bearbeitet";
	        
		$rsUnbearbeitet->DSWeiter();
	   }
	   $Zeile .= "\n\n";
	   echo substr($Zeile,1);
	}
	
	$Zeile='';
	$Spalten = $rsExportKomplett->FeldListe();
	
	
	for($Spalte=1;$Spalte<=$rsExportKomplett->AnzahlSpalten() - 1;$Spalte++)
	{
		
		$Zeile .= ";".$Spalten[$Spalte];
		
	}
	$Zeile .= "\n";

	echo substr($Zeile,1);

	for($CSVZeile=0;$CSVZeile<$rsExportKomplett->AnzahlDatensaetze();$CSVZeile++)
	{
		$Zeile='';
		for($Spalte=1;$Spalte<=$rsExportKomplett->AnzahlSpalten() - 1;$Spalte++)
		{
			if($Spalte == 11)
			{
			  $StatusWert = $rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			
			  switch($StatusWert)	
			  {
			    case 0: {
			        $Zeile .= ";".'bitte waehlen / Keine Angabe';
			    }break;
			    case 1: {
			        $Zeile .= ";".'Werbung erhalten';
			    }break;
			    case 2: {
			        $Zeile .= ";".'Werbung nicht erhalten';
			    }break;
			  }
			
			}
			elseif($Spalte == 12)
			{
			  $StatusWert = $rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			
			  switch($StatusWert)	
			  {
			     case 0: {
			        $Zeile .= ";".'bitte waehlen / Keine Angabe';
			    }break;
			    case 1: {
			        $Zeile .= ";".'ja';
			    }break;
			    case 2: {
			        $Zeile .= ";".'nein';
			    }break;
			  }
			}
			
			if($Spalte != 11 && $Spalte !=12)
			{
				$Zeile .= ";".$rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
			}
		}
		$rsExportKomplett->DSWeiter();
		$Zeile .= "\n";

		echo substr($Zeile,1);
	}
	die();
}

}
catch (Exception $ex)
{
if($Form instanceof awisFormular)
{

        //$Form->Fehler _Anzeigen('INTERN',$ex->getMessage('MELDEN'),6,"200905131735");
}
else
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
}

?>