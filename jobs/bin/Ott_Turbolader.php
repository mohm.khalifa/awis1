<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once 'awisVerschiedenes.php';

/*
 * Lieferantenartikel durch neue Lieferantenartikel ersetzen
 * 
*/

/*
try
{
	$Lief='0815';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/'.$Lief.'.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$Zeile = "LAR_LARTNRALT;LAR_LARTNRNEU;STATUS;\n";
	fputs($fp,$Zeile);

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// alle zu ersetzenden LARTNR aus temp. Tabelle laden
	$SQL = 'SELECT * FROM XXX_'.$Lief.'_ALT';
	$rsALT = $DB->RecordSetOeffnen($SQL);
	//$Form->DebugAusgabe(1,$SQL);
	
	$DB->TransaktionBegin(); //TEST
	
	while (!$rsALT->EOF())
	{
		$Zeile = $rsALT->Feldinhalt('LAR_LARTNRALT') .';';

		// Daten der neuen, ersetzenden LARTNR aus temp. Tabelle laden
		$SQL = 'SELECT * FROM XXX_'.$Lief.'_NEU';
		$SQL.= ' WHERE LAR_LARTNRNEU = :var_T_LAR_LARTNRNEU';
		
		$BindeVariablen = array();
		$BindeVariablen['var_T_LAR_LARTNRNEU'] = $rsALT->Feldinhalt('LAR_LARTNRNEU');
		
		//$Form->DebugAusgabe(1,$SQL);
		$rsNEU = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

		$Zeile .= $rsNEU->Feldinhalt('LAR_LARTNRNEU') .';';

		//Gibt es einen Eintrag im Lieferantenartikel f�r ALTE LAR_LARTNR und Lieferant X
		$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
		//$SQL .= ' WHERE LAR_SUCH_LARTNR = :var_T_LAR_SUCH_LARTNR';
		$SQL .= ' WHERE LAR_SUCH_LARTNR ' .$DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRALT'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
		$SQL .= ' AND LAR_LIE_NR = :var_T_LAR_LIE_NR';

		$BindeVariablen = array();
		//$BindeVariablen['var_T_LAR_SUCH_LARTNR'] = $DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRALT'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
		$BindeVariablen['var_T_LAR_LIE_NR'] = $Lief;
		
		//$Form->DebugAusgabe(1,$SQL);
		$rsLAR_ALT = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

		if (!$rsNEU->EOF()) //neue LARTNR in Steuertabelle vorhanden
		{
			if (!$rsLAR_ALT->EOF()) //mind. ein DS in LIEFERANTENARTIKEL vorhanden
			{
				$i = 1;
				while (!$rsLAR_ALT->EOF())
				{
					if ($i == 1)
					{
						$Zeile .= 'OK;';
						$i = 2;
					}
					else
					{
						$Zeile = ';;OK;';
					}

					$Zeile .= 'Alter Lieferantenartikel vorhanden LAR_KEY=' .$rsLAR_ALT->Feldinhalt('LAR_KEY') .";\n";
					fputs($fp,$Zeile);
										
					$Zeile = ';;;01 Update alter Lieferantenartikel;';
					
					$DB->TransaktionBegin(); //PRODUKTIV
						
					// Update alter Lieferantendatensatz
					$BindeVariablen = array();
					$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
					if (($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN') == '') or is_null($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN')))
					{
						$SQL .= ' LAR_BEMERKUNGEN = :var_T_LAR_BEMERKUNGEN,';
						$BindeVariablen['var_T_LAR_BEMERKUNGEN'] = $rsALT->Feldinhalt('LAR_BEMERKUNGALT');
					}
					else if (stripos($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN'),$rsALT->Feldinhalt('LAR_BEMERKUNGALT')) === false)
					{
						$SQL .= ' LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || \' \' || :var_T_LAR_BEMERKUNGEN,';
						$BindeVariablen['var_T_LAR_BEMERKUNGEN'] = $rsALT->Feldinhalt('LAR_BEMERKUNGALT');
					}
					$SQL .= ' LAR_AUSLAUFARTIKEL=1';
					$SQL .= ' WHERE LAR_KEY = :var_N0_LAR_KEY';
					
					$BindeVariablen['var_N0_LAR_KEY'] = $rsLAR_ALT->Feldinhalt('LAR_KEY');
						
					$Zeile .= $SQL ;
					$Zeile .= ';LAR_KEY=' .$rsLAR_ALT->Feldinhalt('LAR_KEY');
					$Zeile .= ';' .serialize($BindeVariablen) .";\n";
					fputs($fp,$Zeile);
					
					//undo Befehl speichern
					$Zeile = ';;;01 Undo Update Lieferantenartikel SQL;';
					$Zeile .= 'UPDATE LIEFERANTENARTIKEL SET ';
					$Zeile .= ' LAR_BEMERKUNGEN=\'' .$rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN') .'\',';
					$Zeile .= ' LAR_AUSLAUFARTIKEL=' .$rsLAR_ALT->Feldinhalt('LAR_AUSLAUFARTIKEL');
					$Zeile .= ' WHERE LAR_KEY=' .$rsLAR_ALT->Feldinhalt('LAR_KEY') .";\n";
					fputs($fp,$Zeile);
					
					$DB->Ausfuehren($SQL,'',true,$BindeVariablen);
					
					//Gibt es Eintr�ge in Teileinfos f�r ALTE LAR_LARTNR
					$SQL = 'SELECT * FROM TEILEINFOS';
					$SQL .= ' WHERE TEI_ITY_ID2 = \'LAR\' AND';
					$SQL .= ' TEI_KEY2 = :var_N0_TEI_KEY2';

					$BindeVariablen = array();
					$BindeVariablen['var_N0_TEI_KEY2'] = $rsLAR_ALT->Feldinhalt('LAR_KEY');
		
					$Zeile = ';;;Suche Teileinfo TEI_KEY2=' .$BindeVariablen['var_N0_TEI_KEY2'] ."\n";
					fputs($fp,$Zeile);
					//$Form->DebugAusgabe(1,$SQL);
					$rsTEI_ALT = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

					if (!$rsTEI_ALT->EOF()) //Mind. ein alter Teileinfosatz vorhanden
					{
						$Zeile = ";;;mindestens ein alter Teileinfosatz gefunden\n";
						fputs($fp,$Zeile);
						
						while (!$rsTEI_ALT->EOF())
						{
							$Zeile = ';;;Alter Teileinfosatz gefunden TEI_KEY=';
							$Zeile .= $rsTEI_ALT->Feldinhalt('TEI_KEY');
							$Zeile .= ' TEI_KEY1=';
							$Zeile .= $rsTEI_ALT->Feldinhalt('TEI_KEY1');
							$Zeile .= ' TEI_KEY2=';
							$Zeile .= $rsTEI_ALT->Feldinhalt('TEI_KEY2') .";\n";
							fputs($fp,$Zeile);
							
							//Gibt es einen Eintrag im Lieferantenartikel f�r NEUE LAR_LARTNR und Lieferant X
							
							$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
							//$SQL .= ' WHERE LAR_SUCH_LARTNR :var_T_LAR_SUCH_LARTNR';
							$SQL .= ' WHERE LAR_SUCH_LARTNR ' .$DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRNEU'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
							$SQL .= ' AND LAR_LIE_NR = :var_T_LAR_LIE_NR';

							$BindeVariablen = array();
							//$BindeVariablen['var_T_LAR_SUCH_LARTNR'] = $DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRNEU'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER); 
							$BindeVariablen['var_T_LAR_LIE_NR'] = $Lief;
							
							$Zeile = ';;;Suche neuen Lieferantenartikel LAR_LIE_NR=\'';
							$Zeile .= $BindeVariablen['var_T_LAR_LIE_NR'] .'\' AND ';
							$Zeile .= 'LAR_SUCH_LARTNR';
							$Zeile .= $DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRNEU'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);  
							$Zeile .= ";\n";
							fputs($fp,$Zeile);
							//$Form->DebugAusgabe(1,$SQL);
							$rsLAR_NEU = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

                            if($rsLAR_NEU->EOF())
                            {                           
                            	$Zeile = ";;;Neuer Lieferantenartikel nicht vorhanden\n";
                            	fputs($fp,$Zeile);
                                // Neuen Lieferantenartikelsatz anlegen
								$Zeile = ';;;01 Insert neuer Lieferantenartikel;';

								$SQLINS = 'INSERT INTO LIEFERANTENARTIKEL (';
								$SQLINS .= 'LAR_LARTNR,';
								$SQLINS .= 'LAR_LIE_NR,';
								$SQLINS .= 'LAR_BEMERKUNGEN,';
								$SQLINS .= 'LAR_BEKANNTWW,';
								$SQLINS .= 'LAR_IMQ_ID,';
								$SQLINS .= 'LAR_SUCH_LARTNR,';
								$SQLINS .= 'LAR_USER,';
								$SQLINS .= 'LAR_USERDAT';
								$SQLINS .= ') VALUES (';
								$SQLINS .= ':var_T_LAR_LARTNR,';
								$SQLINS .= ':var_T_LAR_LIE_NR,';
								$SQLINS .= ':var_T_LAR_BEMERKUNGEN,';
								$SQLINS .= '0,';
								$SQLINS .= '4,';
								$SQLINS .= 'asciiwort(' .$DB->FeldInhaltFormat('TU',$rsALT->Feldinhalt('LAR_LARTNRNEU'),false) .'),';
								$SQLINS .= '\'OTT_H\'' .',';
								$SQLINS .= 'SYSDATE';
								$SQLINS .= ')';
								
								$BindeVariablenIns = array();
								$BindeVariablenIns['var_T_LAR_LARTNR'] = $rsNEU->Feldinhalt('LAR_LARTNRNEU'); 
								$BindeVariablenIns['var_T_LAR_LIE_NR'] = $Lief;
								$BindeVariablenIns['var_T_LAR_BEMERKUNGEN'] = $rsNEU->Feldinhalt('LAR_BEMERKUNGNEU'); 
								//$BindeVariablenIns['var_T_LAR_SUCH_LARTNR'] = $DB->FeldInhaltFormat('TU',$rsALT->Feldinhalt('LAR_LARTNRNEU'),false);

								//$Form->DebugAusgabe(1,$SQL);
								$Zeile .= $SQLINS .';' .serialize($BindeVariablenIns) .";\n";
								fputs($fp,$Zeile);
								$DB->Ausfuehren($SQLINS,'',true,$BindeVariablenIns);

								//neu angelegten DS �ffnen und Daten laden
								$Zeile = ';;;Suche nochmal neuen Lieferantenartikel LAR_LIE_NR=\'';
								$Zeile .= $BindeVariablen['var_T_LAR_LIE_NR'] .'\' AND ';
								$Zeile .= 'LAR_SUCH_LARTNR';
								$Zeile .= $DB->LIKEoderIST($rsALT->Feldinhalt('LAR_LARTNRNEU'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);  
								$Zeile .= ';';
								
								fputs($fp,$Zeile);
								
								$rsLAR_NEU = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
                            
								if (!$rsLAR_NEU->EOF())
								{
									$Zeile = ";;;Neuen Lieferantenartikel wurde angelegt\n";
								}
								else
								{
									$Zeile = $rsALT->Feldinhalt('LAR_LARTNRALT') .';';
									$Zeile .= $rsNEU->Feldinhalt('LAR_LARTNRNEU') .';';
									$Zeile .= "ERROR;Neuen Lieferantenartikel nicht angelegt\n";
								}
								fputs($fp,$Zeile);
                            } //endif

							while (!$rsLAR_NEU->EOF())
							{
                            	$Zeile = ';;;Neuer Lieferantenartikel vorhanden ';
								$Zeile .= 'LAR_KEY=' .$rsLAR_NEU->Feldinhalt('LAR_KEY') ."\n";
                            	fputs($fp,$Zeile);
									
                            	//gibt es einen Teileinfosatz f�r Kombination alter AST/neuer LAR?
								$SQL = 'SELECT * FROM TEILEINFOS ';
								$SQL .= 'WHERE TEI_ITY_ID2 = \'LAR\' AND ';
								$SQL .= 'TEI_KEY1 = :var_N0_TEI_KEY1 AND ';
								$SQL .= 'TEI_KEY2 = :var_N0_TEI_KEY2';
									
								$BindeVariablen = array();
								$BindeVariablen['var_N0_TEI_KEY1'] = $rsTEI_ALT->FeldInhalt('TEI_KEY1');
								$BindeVariablen['var_N0_TEI_KEY2'] = $rsLAR_NEU->Feldinhalt('LAR_KEY');
									
								$Zeile = ';;;Suche Teileinfosatz f�r neue Kombination TEI_KEY1='.$rsTEI_ALT->FeldInhalt('TEI_KEY1');
								$Zeile .= ' AND TEI_KEY2='.$rsLAR_NEU->Feldinhalt('LAR_KEY');
								$Zeile .= "\n";
								fputs($fp,$Zeile);
									
								$rsTEI_NEU = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

								if ($rsTEI_NEU->EOF()) //kein TEI vorhanden -> neuen anlegen
								{
									$Zeile = ';;;01 Teileinfosatz nicht vorhanden, anlegen;';
									$SQL = 'INSERT INTO TEILEINFOS (';
									$SQL .= 'TEI_ITY_ID1,';
									$SQL .= 'TEI_KEY1,';
									$SQL .= 'TEI_SUCH1,';
									$SQL .= 'TEI_WERT1,';
									$SQL .= 'TEI_ITY_ID2,';
									$SQL .= 'TEI_KEY2,';
									$SQL .= 'TEI_SUCH2,';
									$SQL .= 'TEI_WERT2,';
									$SQL .= 'TEI_USER,';
									$SQL .= 'TEI_USERDAT';
									$SQL .= ') VALUES (';
									$SQL .= ':var_T_TEI_ITY_ID1,';
									$SQL .= ':var_N0_TEI_KEY1,';
									$SQL .= ':var_T_TEI_SUCH1,';
									$SQL .= ':var_T_TEI_WERT1,';
									$SQL .= ':var_T_TEI_ITY_ID2,';
									$SQL .= ':var_N0_TEI_KEY2,';
									$SQL .= ':var_T_TEI_SUCH2,';
									$SQL .= ':var_T_TEI_WERT2,';
									$SQL .= '\'OTT_H\'' .',';
									$SQL .= 'SYSDATE';
									$SQL .= ')';
										
									$BindeVariablen = array();
									// AST Teil = alte Werte �bernehmen
									$BindeVariablen['var_T_TEI_ITY_ID1'] = $rsTEI_ALT->Feldinhalt('TEI_ITY_ID1');
									$BindeVariablen['var_N0_TEI_KEY1'] = $rsTEI_ALT->Feldinhalt('TEI_KEY1');
									$BindeVariablen['var_T_TEI_SUCH1'] = $rsTEI_ALT->Feldinhalt('TEI_SUCH1');
									$BindeVariablen['var_T_TEI_WERT1'] = $rsTEI_ALT->Feldinhalt('TEI_WERT1');
									$BindeVariablen['var_T_TEI_ITY_ID2'] = $rsTEI_ALT->Feldinhalt('TEI_ITY_ID2');
									// LAR Teil = neue Werte bef�llen
									$BindeVariablen['var_N0_TEI_KEY2'] = $rsLAR_NEU->Feldinhalt('LAR_KEY');
									$BindeVariablen['var_T_TEI_SUCH2'] = $rsLAR_NEU->Feldinhalt('LAR_SUCH_LARTNR');
									$BindeVariablen['var_T_TEI_WERT2'] = $rsLAR_NEU->Feldinhalt('LAR_LARTNR');
									
									//$Form->DebugAusgabe(1,$SQL);
									$Zeile .= $SQL .';' .serialize($BindeVariablen) .";\n";
									fputs($fp,$Zeile);
									$DB->Ausfuehren($SQL,'',true,$BindeVariablen);
								}
								else
								{
									//nichts machen
									$Zeile = ";;;Teileinfosatz bereits vorhanden, nichts machen;\n";
									fputs($fp,$Zeile);
								} //endif

								$rsLAR_NEU->DSWeiter();
									
							} //end while
							
							$rsTEI_ALT->DSWeiter();
							
						} //end while

					}
					else
					{
						$Zeile = $rsALT->Feldinhalt('LAR_LARTNRALT') .';';
						$Zeile .= $rsNEU->Feldinhalt('LAR_LARTNRNEU') .';';
						$Zeile .= "ERROR;Kein Teileinfodatensatz vorhanden\n";
						fputs($fp,$Zeile);
					} //end if

					$DB->TransaktionCommit(); //PRODUKTIV

					$rsLAR_ALT->DSWeiter();
					
				} //end while
			}
			else
			{
				$Zeile .= "INFO;Kein alter Lieferantendatensatz vorhanden\n";
				fputs($fp,$Zeile);
			}
		}
		else
		{
			$Zeile .= "ERROR;Keine Zuordnung neue LAR_LARTNR\n";
			fputs($fp,$Zeile);
		}

		$rsALT->DSWeiter();
		
	} //end while

	//$DB->TransaktionRollback(); //TEST
	$DB->Schliessen();
	fclose($fp);
}
catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}

*/

/*
 * Artikel neu einspielen
*/
try 
{
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/Artikelneueinspielung.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$Zeile = "AST_ARTNR;VORGANG;SQL;\n";
	fputs($fp,$Zeile);

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// alle DS aus temp. Tabelle laden
	$SQL = 'SELECT * ';
	$SQL .= 'FROM XXX_ARTIKELNEUEINSPIELUNG ';
	$SQL .= 'WHERE erfolgreich IS NULL ';
	$SQL .= 'OR erfolgreich = 0';
	
	$rsQuellDat = $DB->RecordSetOeffnen($SQL);

	$DB->TransaktionBegin();
	
	while (!$rsQuellDat->EOF())
	{
		$SQL = 'SELECT ast_key, ast_atunr';
		$SQL .= ' FROM artikelstamm';
		$SQL .= ' WHERE ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
		//echo $SQL.'<br>';		

		$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
		$Zeile .= ';Artikelstamm laden';
		$Zeile .= ';' .$SQL ."\n";
		fputs($fp,$Zeile);
		
		$rsArtikelstamm = $DB->RecordSetOeffnen($SQL);
		
		if($rsArtikelstamm->EOF())
		{
			//neue ATUNR anlegen
			$SQL = 'INSERT INTO Artikelstamm (';
			$SQL .= 'AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG';
			$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
			$SQL .= ') VALUES (';
			$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
			$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('AST_BEZEICHNUNG'),false) .',';
			$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_KENNUNGVORSCHLAG'),false) .',';
			$SQL .= '1,';
			$SQL .= '4,';
			$SQL .= '\'schmidberger\',';
			$SQL .= 'SYSDATE)';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Artikelstamm anlegen';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$DB->Ausfuehren($SQL,'',true);			

			$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Artikelstamm angelegt AST_KEY=' .$AWIS_KEY1;
			$Zeile .= ";\n";
			fputs($fp,$Zeile);
			
			// Feld Verpackungsmenge AIT_ID = 60 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos ';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 60 ';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			if($DB->ErmittleZeilenAnzahl($SQL) == 0)
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ') values (';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '60,';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_60'),false) .',';
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';					
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos';
				$SQL .= ' SET asi_wert = ' .$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_60'),false) .',';
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 60';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			// Feld Einkaufsmeldung AIT_ID = 100 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos ';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 100 ';
			//echo $SQL.'<br>';					
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			if($DB->ErmittleZeilenAnzahl($SQL) == 0)
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ") values (";
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '100,';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_100'),false) .',';
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';					
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos';
				$SQL .= ' SET asi_wert = ' .$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_100'),false);
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 100';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			// Feld Einkaufsmeldung durch AIT_ID = 101 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 101';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			if($DB->ErmittleZeilenAnzahl($SQL) == 0)
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ') values (';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '101,';
				$SQL .= '\'1\',';
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';

				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);					
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos ';
				$SQL .= ' SET asi_wert = \'1\'';
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 101';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
			$SQL = 'SELECT * ';
			$SQL .= ' FROM LIEFERANTENARTIKEL';
			$SQL .= ' WHERE (LAR_SUCH_LARTNR) ' .$DB->LIKEoderIST($rsQuellDat->Feldinhalt('LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
			$SQL .= ' AND (TO_NUMBER(LAR_LIE_NR)) = ' .$DB->FeldInhaltFormat('Z',$rsQuellDat->Feldinhalt('LAR_LIE_NR'));
			//echo $SQL.'<br>';
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Pr�fung LIEFERANTENARTIKEL';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsLIEFERANTENARTIKEL = $DB->RecordSetOeffnen($SQL);
			
			if ($rsLIEFERANTENARTIKEL->EOF())
			{
				//neue LiefArtNr anlegen
				$SQL = 'INSERT INTO Lieferantenartikel (';
				$SQL .= 'LAR_LARTNR,';
				$SQL .= 'LAR_LIE_NR,';
				$SQL .= 'LAR_BEKANNTWW,';
				$SQL .= 'LAR_IMQ_ID,';
				$SQL .= 'LAR_SUCH_LARTNR,';
				$SQL .= 'LAR_USER,';
				$SQL .= 'LAR_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false) .',';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->Feldinhalt('LAR_LIE_NR'),false) .',';
				$SQL .= '0,';
				$SQL .= '4,';		// Importquelle
				$SQL .= 'asciiwort(' . $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false).'),';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE';
				$SQL .= ')';
				//echo $SQL.'<br>';

				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Insert Lieferantenartikel';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
				
				$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 
			{
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Lieferantenartikel bereits vorhanden';
				$Zeile .= ";\n";
				fputs($fp,$Zeile);

				$AWIS_KEY2=$rsLIEFERANTENARTIKEL->FeldInhalt('LAR_KEY');
			}
			
			//Pr�fen, ob es bereits einen Teileinfo Satz gibt
			
			$SQL = 'SELECT tei_key';
			$SQL .= ' FROM teileinfos ';
			$SQL .= ' WHERE tei_ity_id1 = \'AST\'';
			$SQL .= ' AND tei_key1 = ' .$AWIS_KEY1;
			$SQL .= ' AND tei_ity_id2 = \'LAR\'';
			$SQL .= ' AND tei_key2 = ' .$AWIS_KEY2;
			//echo $SQL.'<br>';
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Pr�fung TEILEINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsTEILEINFOS = $DB->RecordSetOeffnen($SQL);
			
			if ($rsTEILEINFOS->EOF())
			{
				$SQL = 'INSERT INTO TeileInfos (';
				$SQL .= 'TEI_ITY_ID1,';
				$SQL .= 'TEI_KEY1,';
				$SQL .= 'TEI_SUCH1,';
				$SQL .= 'TEI_WERT1,';
				$SQL .= 'TEI_ITY_ID2,';
				$SQL .= 'TEI_KEY2,';
				$SQL .= 'TEI_SUCH2,';
				$SQL .= 'TEI_WERT2,';
				$SQL .= 'TEI_USER,';
				$SQL .= 'TEI_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= '\'AST\',';
				$SQL .= $AWIS_KEY1 .',';
				$SQL .= 'suchwort(\''.$rsQuellDat->FeldInhalt('AST_ATUNR').'\'),';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '\'LAR\',';
				$SQL .= $AWIS_KEY2 .',';
				$SQL .= 'asciiwort(' . $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false).'),';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false) .',';
				$SQL .= '\'schmidberger\',';			
				$SQL .= 'SYSDATE';
				$SQL .= ')';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';Insert TEILEINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';TEILEINFOS bereits vorhanden';
				$Zeile .= ";\n";
				fputs($fp,$Zeile);
			}				
			//Datensatz als erfolgreich markieren
			$SQL = 'UPDATE XXX_ARTIKELNEUEINSPIELUNG';
			$SQL .= ' SET erfolgreich = 1 ';
			$SQL .= ' WHERE AST_ATUNR = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Datensatz als erfolgreich markieren';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			fputs($fp,"\n");
			
			$DB->Ausfuehren($SQL,'',true);
		}
		else 
		{
			// Artikel bereits vorhanden
			$SQL = 'UPDATE XXX_ARTIKELNEUEINSPIELUNG';
			$SQL .= ' SET erfolgreich = 0 ';
			$SQL .= ' WHERE AST_ATUNR = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';Artikelstamm bereits vorhanden';
			$Zeile .= ";\n";
			fputs($fp,$Zeile);
			fputs($fp,"\n");
			
			$DB->Ausfuehren($SQL,'',true);
		}
		
		$rsQuellDat->DSWeiter();

	}// end of while

	$DB->TransaktionCommit();
	//$DB->TransaktionRollback(); //TEST
	$DB->Schliessen();
	fclose($fp);
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}



?>