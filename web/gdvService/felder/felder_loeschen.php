<?php
/**
 * Löschen Stammdatenpflege GDV-Felder
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

$Tabelle= '';


if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedrückt
{
		$Tabelle = 'VSF';
		$Felder = explode(';',$FelderDel);
		$Feld = explode('_',$Felder[1]);
		$Key=$Feld[2];


		$SQL = 'select * from versfelder ';
		$SQL .= ' inner join versversandweg on VVW_VERSNR =  VSF_VERSNR';
		$SQL .= ' inner join gdvrelease on GDR_RELEASE =  VVW_RELEASE';
		$SQL .= ' inner join versversicherungen on vsf_versnr = vve_versnr';
		$SQL .= ' where vsf_key = '.$Key;

		$rsDaten = $DB->RecordSetOeffnen($SQL);
	
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_GDVPFAD'),$rsDaten->FeldInhalt('VSF_GDVPFAD'));
		if($rsDaten->FeldInhalt('VSF_VERSWELTFELD') != '') {
			$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_VERSWELTFELD'),$rsDaten->FeldInhalt('VSF_VERSWELTFELD'));
		}
		else{
			$Konstant = explode(':',$rsDaten->FeldInhalt('VSF_KONSTANT'));

			switch ($Konstant[0]) {
				case 'WERT':
					$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_KONSTANT'),$Konstant[1]);
					break;
				case 'FUNC':
					$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_FUNKTION'),$Konstant[1]);
					break;
				case 'INVOICE':
					$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_INVOICE'),$Konstant[1]);
					break;
			}
		}
	    $Felder[]=array($Form->LadeTextBaustein('GDV','VSF_FORMAT'),$rsDaten->FeldInhalt('VSF_FORMAT'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','VSF_INDEX'),$rsDaten->FeldInhalt('VSF_INDEX'));



}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen wenn auf Frage 'Wirklich löschen?' ok
{
	switch ($_POST['txtTabelle']) {
		case 'VSF':

			$SQL = 'DELETE FROM VERSFELDER WHERE VSF_KEY=' . $_POST['txtKEY']; // Sachbearbeiter Zuord l�schen

			if ($DB->Ausfuehren($SQL) === false) {
				throw new awisException('Fehler beim Loeschen', 201202181536, $SQL, awisException::AWIS_ERR_SYSTEM);
			}
			$AWIS_KEY1 = $_POST['txtVERSNR'];
			break;
	}
}
if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':

			$Form->SchreibeHTMLCode('<form name=frmLoeschen action="./felder_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsDaten->FeldInhalt('VSA_VERSNR').  '" method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext('M&oumlchten sie die Daten f&uumlr folgende Versicherung wirklich l&oumlschen?');	// Fragen ob wirklich gelöscht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder für Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder für Anzeigen und zum löschen des Datensatzes per Hidden übergeben.
			 **/

			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('VERSNR',$rsDaten->FeldInhalt('VSF_VERSNR'));
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);

			$Form->Trennzeile();
				
			/**
			 * Löschen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();

			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>