
SET DEFINE OFF;
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<div style="font-family: Arial; font-size: 12pt">Sehr geehrter GL / WL,<br>
<br>
in der Zeit vom #PVS_VON# bis #PVS_BIS# hat #PVS_ANREDE# #PVS_NAME# ein Praktikum in Ihrer Filiale absolviert.<br>
<br>
Da heute der letzte Tag des Praktikums ist, m�chten wir gern ein Feedback zum Verlauf des Praktikums von Ihnen haben. Insbesondere m�chten wir gern nachhalten, ob der Praktikant f�r eine m�gliche Ausbildung geeignet ist oder nicht.<br>
<br>
<p style= "font-weight: bold"> Bitte geben Sie uns unter folgendem Link ihr Feedback: <a href = "#PVS_LINK#">Hier zur Bewertungsseite</a></p>
<br>
Wir danken Ihnen f�r die Unterst�tzung und w�nschen weiterhin viel Erfolg.<br>
<br>
Mit freundlichen Gr��en<br>
<br>
A.T.U Auto-Teile-Unger GmbH & Co. KG<br>
Tel.:	+49 (0)961 306-5627<br>
Fax:	+49 (0)961 306-5867<br>
Mail:	jenny.froebel@de.atu.eu<br>
<br>
<br>
<br>
i. A. Jenny Fr�bel<br>
Koordinatorin Ausbildung<br>
Personalentwicklung </div>' WHERE XTX_KENNUNG = 'PVS_MAILVERSAND_TEXT' and XTX_BEREICH = 'PVS';