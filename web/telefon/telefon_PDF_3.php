<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once('fpdf.php');
require_once 'fpdi.php';

/***************************************
* Neue PDF Datei erstellen
***************************************/
define('FPDF_FONTPATH','font/');
$pdf = new fpdi('p','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);				

/***************************************
*
* Daten laden
*
***************************************/

$Bedingung='';

		// Daten suchen
if($_POST['txtKategorie']!='-1')
{
	$Bedingung .= ' AND KON_KKA_KEY IN (31,36)';		// Extern und Extern �ffentlich
}
if($_POST['txtBereich']!='-1')
{
	$Bedingung .= ' AND KON_KBE_KEY=11';				// Extern
}

	// �ffentliche Daten
if(isset($_POST['chkOeffentliche']))
{
	$Bedingung .= " AND BITAND(KON_LISTEN,1)=1";
}

$Bedingung .= ' AND KON_STATUS = \'A\'';

$SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KON_PLZ, KON_ORT, KON_ZUSTAENDIGKEIT";
$SQL .= " FROM Kontakte ";

if($Bedingung!='')
{
	$SQL .= " WHERE " . substr($Bedingung,4) . "";
}
$SQL .= " ORDER BY KON_ZUSTAENDIGKEIT, KON_NAME1, KON_NAME2";

$rsKON = awisOpenRecordset($con,$SQL);
$rsKONZeilen = $awisRSZeilen;

if($rsKONZeilen==0)			// Keine Daten gefunden
{
	$LinkerRand='';
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,150,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(170,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
}
else 						// Daten gefunden
{
	$NeueSeite = true;
	$Zeile = 0;
	$LinkerRand = 15;
	$LetzterBlock = '';
	
	NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand);
	
	for($KONZeile=0;$KONZeile<$rsKONZeilen;$KONZeile++)
	{
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);				// Schrift setzen

		$ZeilenHoehe = 1;	// Eine Zeile hoch

		// Ort
		$AusgabeOrt = $rsKON['KON_PLZ'][$KONZeile] . ' ' . $rsKON['KON_ORT'][$KONZeile];
		$foo = $pdf->GetStringWidth(substr($AusgabeOrt,2));
		$ZeilenHoeheOrt = round(($foo/48)+0.5,0);			// H�he nach der Textausgabe steuern
		$ZeilenHoeheOrt=($ZeilenHoeheOrt==0?1:$ZeilenHoeheOrt);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheOrt)?$ZeilenHoeheOrt:$ZeilenHoehe;
		
		// Name		
		$AusgabeName = $rsKON['KON_NAME1'][$KONZeile] . ($rsKON['KON_NAME2'][$KONZeile]==''?'':', '.$rsKON['KON_NAME2'][$KONZeile]);
		$foo = $pdf->GetStringWidth(substr($AusgabeName,2));
		$ZeilenHoeheName = round(($foo/58)+0.5,0);			// H�he nach der Textausgabe steuern
		$ZeilenHoeheName=($ZeilenHoeheName==0?1:$ZeilenHoeheName);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheName)?$ZeilenHoeheName:$ZeilenHoehe;
		
		// Kommunikation
		$AusgabeTyp = '';
		$AusgabeNr = '';
		$AusgabeKW = '';
		$SQL = "SELECT KKO_KON_KEY, KKO_WERT, KKO_KURZWAHL, KOT_TYP ";
		$SQL .= " FROM KONTAKTEKOMMUNIKATION INNER JOIN KOMMUNIKATIONSTYPEN ON KKO_KOT_KEY = KOT_KEY ";
		$SQL .= " WHERE KKO_KON_KEY=0" . $rsKON['KON_KEY'][$KONZeile];
		$SQL .= " AND KOT_KEY IN (2,4,6)";
		$SQL .= " ORDER BY KOT_KEY";
		
		$rsKonTel = awisOpenRecordset($con, $SQL);
		for($ZeilenHoeheNr=0;$ZeilenHoeheNr<$awisRSZeilen;$ZeilenHoeheNr++)
		{
			$AusgabeTyp .= "\n" . $rsKonTel['KOT_TYP'][$ZeilenHoeheNr];
			$AusgabeNr .= "\n" . $rsKonTel['KKO_WERT'][$ZeilenHoeheNr];
			$AusgabeKW .= "\n" . ($rsKonTel['KKO_KURZWAHL'][$ZeilenHoeheNr]==''?'-':$rsKonTel['KKO_KURZWAHL'][$ZeilenHoeheNr]);
		}
		$ZeilenHoeheNr=($ZeilenHoeheNr==0?1:$ZeilenHoeheNr);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheNr)?$ZeilenHoeheNr:$ZeilenHoehe;
		

		
		/*********************
		* Blockwechsel?
		*********************/
		if($LetzterBlock!=$rsKON['KON_ZUSTAENDIGKEIT'][$KONZeile])
		{
			if($Zeile > (275-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
			{
				NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',8);				// Schrift setzen
			}		
			$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
			$pdf->SetFont('Arial','B',12);
			$pdf->MultiCell(190,5,$rsKON['KON_ZUSTAENDIGKEIT'][$KONZeile],1,'L',1);
			$Zeile+=5;
			$LetzterBlock=$rsKON['KON_ZUSTAENDIGKEIT'][$KONZeile];
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}
		
		if($Zeile > (280-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
		{
			//$NeueSeite=true;
			NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}		
		
		// Daten ausgeben
		$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
		
		$pdf->setXY($LinkerRand,$Zeile);							// Name
		$pdf->MultiCell(60,($ZeilenHoehe-$ZeilenHoeheName+1)*4,$AusgabeName,1,'L',1);
	
		$pdf->setXY($LinkerRand+60,$Zeile);							// Ort
		$pdf->MultiCell(50,($ZeilenHoehe-$ZeilenHoeheOrt+1)*4,$AusgabeOrt,1,'L',1);
		
		$pdf->setXY($LinkerRand+110,$Zeile);						// Typ
		$pdf->MultiCell(35,($ZeilenHoehe-$ZeilenHoeheNr+1)*4,substr($AusgabeTyp,1),1,'C',1);
		
		$pdf->setXY($LinkerRand+145,$Zeile);						// Nr
		$pdf->MultiCell(30,($ZeilenHoehe-$ZeilenHoeheNr+1)*4,substr($AusgabeNr,1),1,'C',1);
		
		$pdf->setXY($LinkerRand+175,$Zeile);						// KW
		$pdf->Multicell(15,($ZeilenHoehe-$ZeilenHoeheNr+1)*4,substr($AusgabeKW,1),1,'C',1);

		
		$Zeile += $ZeilenHoehe*4;		
		
		
	}	// Datens�tze
	
	$pdf->setXY($LinkerRand,285);					// Cursor setzen
	$pdf->SetFont('Arial','',6);					// Schrift setzen
			// �berschrift
//	$pdf->MultiCell(160,3,$OrtsListe,0,'L',0);
	$pdf->setXY(190,285);					// Cursor setzen
	$pdf->Cell(40,3,'Seite ' . $pdf->PageNo(),0,0,'L',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
		
		
}	// Ende Datenaufbereitung


/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
//$pdf->saveas($DateiName);
$pdf->Output($DateiName,'F');
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";

echo "<br><br><a href=./telefon_Main.php?cmdAktion=Ausdrucke>Neue Suche</a>";


/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  string OrtsListe
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite = 0;
	
	if($Seite > 0)		// Fu�zeile?
	{
		$pdf->SetFont('Arial','',6);					// Schrift setzen
					// �berschrift
		$pdf->setXY(190,285);					// Cursor setzen
		$pdf->Cell(40,3,'Seite ' . $Seite,0,0,'L',0);

		$pdf->setXY($LinkerRand,290);					// Cursor setzen
		$pdf->SetFont('Arial','',5);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
	
	}
	$Seite++;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,185,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(190,6,"Branchen-Telefonverzeichnis ATU",0,0,'C',0);
	
	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',8);				// Schrift setzen
	$pdf->cell(190,6,"Stand per " . date('d.m.Y'),0,0,'C',0);

	
	// �berschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->setXY($LinkerRand,18);				// Cursor setzen
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	$pdf->cell(60,6,"Name",1,0,'C',1);
	
	$pdf->setXY(75,18);							// Cursor setzen
	$pdf->cell(50,6,"Ort",1,0,'C',1);
	
	$pdf->setXY(125,18);						// Cursor setzen
	$pdf->cell(35,6,"Komm.",1,0,'C',1);
	
	$pdf->setXY(160,18);						// Cursor setzen
	$pdf->cell(30,6,"Tel",1,0,'C',1);
	
	$pdf->setXY(190,18);						// Cursor setzen
	$pdf->cell(15,6,"KW",1,0,'C',1);
	
	$Zeile = 24;
}

?>