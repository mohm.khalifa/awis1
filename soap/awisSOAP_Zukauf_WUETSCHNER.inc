<?php
require_once('/daten/soap/awisSOAP_Zukauf.inc');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

/**
 * Zukauf Verwaltung des Lieferanten Wuetschner
 * @author  Sacha Kerres
 * @version 20110320
 */
class awisZukaufArtikel_WUETSCHNER extends awisZukaufArtikel
{
    /**
     * Lieferantennummer
     *
     */
    const LIE_NR = '8164';

    /**
     * Lieferantennummer in �sterreich
     *
     */
    const LIE_NR_AT = '9169';

    /**
     * WSDL Datei f�r die Abrufe der Daten
     *
     */
    const WSDL_DATEI_ALT = 'http://212.63.68.136/ATU_WEBSERVER/ATU_WEBSERVER.exe/wsdl/IATU_WEBSERVER';

    const WSDL_DATEI = 'http://atuservice1.wuetschner.com/ATU_WEBSERVER_M1/ATU_WEBSERVER.exe/wsdl/IATU_WEBSERVER';
    const WSDL_DATEI_AT = 'http://atuservice6.wuetschner.com/ATU_WEBSERVER_M6/ATU_WEBSERVER.exe/wsdl/IATU_WEBSERVER';

    const KUNDENNUMMER = '1';
    const USERID = '';
    const PASSWORT = 'shopft';
    const ZUSATZ = 'ft';

    /**
     * SOAP Client f�r die Aufrufe
     *
     * @var SoapClient
     */
    private $_SOAPClient;

    /**
     * Ergebnisdaten als XML
     *
     * @var SimpleXMLElement
     */
    private $_Daten;

    private $_WSDL;

    private $_LIE_NR;

    private $_LKZ;

    private $_Verkaufshaeuser = array(
        'SW' => 'Schweinfurt',
        'WUE' => 'Wuerzburg',
        'PBG' => 'Petersberg',
        'BAM' => 'Bamberg',
        'KAUFU' => 'Kaufungen',
        'SHL' => 'Suhl',
        'FFM' => 'Maintal',
        'EA' => 'Eisenach',
        'GZ' => 'Gross-Zimmern',
        'AB' => 'Aschaffenburg',
        'GIE' => 'Giessen',
        'GLA' => 'Gladenbach',
        'NBG' => 'Nuernberg',
        'WI' => 'Wiesbaden',
        'MZ' => 'Mainz',
        'GRF' => 'Gruensfeld'
    );

    // ASI-IDs fuer die Refenzpreise (VK-Preis)
    protected $_ReferenzPreise = array(
        'DE'=>0
        ,'AT'=>50
        ,'CZ'=>51
        ,'NL'=>52
        ,'IT'=>53
        ,'CH'=>54
    );

    /**
     * Anmldedaten
     *
     * @var string
     */
    private $_LoginDaten;

    /**
     * @var integer MWST ID f�r diese Filiale
     */
    private $_MwStId = null;

    /**
     * @var integer MWST ID f�r Altteile f�r diese Filiale
     */
    private $_MwStId_Altteil;

    public function __construct($XML = false, $FIL_ID = 0)
    {
        parent::__construct();

        $this->_FIL_ID = $FIL_ID;
        $this->setzeLandesspezifisches($FIL_ID);
        ini_set('allow_call_time_pass_reference', 'true');

        if ($XML === false) {
            $this->_Daten = new awisSimpleXMLElement('<Daten><Anfragezeit>' . date('c') . '</Anfragezeit></Daten>');
        } else {
            $this->_Daten = $XML;
        }
        file_put_contents('/var/log/wuetschner_bestellung.log', "\nFiliale " . $FIL_ID . ", " . date('c') . "\n", FILE_APPEND);

        $Params = array('trace' => 1                    // Protokoll
                        , 'encoding' => 'utf-8'
                        , 'exceptions' => 0
        );

        $AnmeldeDaten = $this->_LeseKundenNummer($this->_LIE_NR, $this->_FIL_ID);
        file_put_contents('/var/log/wuetschner_bestellung.log', "\nAnmeldedaten " . serialize($AnmeldeDaten) . "\n", FILE_APPEND);
        if ($AnmeldeDaten['ZUSATZ'] == '') {
            $this->_SOAPClient = new SoapClient(self::WSDL_DATEI_ALT, $Params);
            file_put_contents('/var/log/wuetschner_bestellung.log', "\nWSDL:" . self::WSDL_DATEI . "\n", FILE_APPEND);
        } else {
            $this->_SOAPClient = new SoapClient($this->_WSDL, $Params);
            file_put_contents('/var/log/wuetschner_bestellung.log', "\nWSDL:" . $this->_WSDL . "\n", FILE_APPEND);
            file_put_contents('/var/log/wuetschner_bestellung.log', "\nParameter:" . json_encode($Params) . "\n", FILE_APPEND);
        }
        $Erg = $this->_SOAPClient->ATU_Lieferart2Wert('Abholung');

        if ($Erg !== 1) {
            $Daten = $this->_Daten->addChild('Anfrage');
            $Daten->addChild('Meldung', 'Fehler beim Zugriff');
            $Daten->addChild('Nummer', -1);
        }
    }

    /**
     * Pr�fung eines Artikelpreises und eines Bestands bei W�tschner
     *
     * @param array $ArtikelInfos
     *
     * @return SimpleXMLElement
     */
    public function ArtikelVerfuegbarkeit(array $ArtikelInfos, $DebugInfos = false)
    {
        try {
            if ($DebugInfos == true) {
                $DebugXML = $this->_Daten->addChild('Debug');
            }
            file_put_contents('/var/log/wuetschner_bestellung.log', serialize($ArtikelInfos) . "\n", FILE_APPEND);

            // Es kann nur nach Tec-Doc oder nach W�tschner gesucht werden
            if (!isset($ArtikelInfos['atunr'])) {
                $SuchArt = 1;            // TecDoc suchen
                $SerialID = '';
                $ArtikelNummer = $ArtikelInfos['artikelnummer'];
                $Code = isset($ArtikelInfos['tecdocid']) ? $ArtikelInfos['tecdocid'] : '';
            } elseif (isset($ArtikelInfos['artikelnummer']) AND $ArtikelInfos['artikelnummer'] != '')    // TecDoc Nummer
            {
                $SuchArt = 0;            // W�tschner Artikelnummer suchen
                $SerialID = '';
                $ArtikelNummer = $ArtikelInfos['artikelnummer'];
                $Code = '';     // Hier gibt es keine Suche nach Hersteller

                $SQL = 'SELECT zal_bestellnummer';
                $SQL .= ' FROM ZUKAUFARTIKEL';
                $SQL .= ' INNER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_LIE_NR=\'' . $this->_LIE_NR . '\'';
                if (is_array($ArtikelInfos['artikelnummer'])) {
                    $SQL .= ' WHERE (';

                    $Bedingung = '';
                    foreach ($ArtikelInfos['artikelnummer'] as $ArtNr) {
                        $Bedingung .= ' OR ZLA_ARTIKELNUMMER = ' . $this->_DB->FeldInhaltFormat('T', $ArtNr);
                    }
                    $SQL .= substr($Bedingung, 4) . ')';
                } else {
                    $SQL .= ' WHERE ZLA_ARTIKELNUMMER = ' . $this->_DB->FeldInhaltFormat('T', $ArtikelInfos['artikelnummer']);
                }
                if ($ArtikelInfos['zlh_key'] != '0') {
                    $SQL .= ' AND ZLA_ZLH_KEY = ' . $ArtikelInfos['zlh_key'];
                }
                file_put_contents('/var/log/wuetschner_bestellung.log', $SQL . "\n", FILE_APPEND);
                $rsZAL = $this->_DB->RecordSetOeffnen($SQL);
                if (!$rsZAL->EOF()) {
                    $ArtikelNummer = $rsZAL->FeldInhalt('ZAL_BESTELLNUMMER');
                } else {
                    $Warnung = $this->_Daten->addChild('Warnung');
                    $Warnung->addChild('Meldung', 'Artikelnummer konnte nicht in den Stammdaten gefunden werden.');
                    $Warnung->addChild('Nummer', 1);
                    $SuchArt = 2;
                }
            } else {
                $SuchArt = 2;            // TecDoc - Artikel
                $SerialID = '';
                $ArtikelNummer = $ArtikelInfos['artikelnummer'];
                $Code = '';
            }

            //******************************************
            //******************************************
            // Anfrage zusammenbauen
            //******************************************
            //******************************************

            $Artikelliste = $this->_Daten->addChild('Artikelliste');
            $Artikelliste->addChild('Treffer', 0);

            $AnmeldeDaten = $this->_LeseKundenNummer($this->_LIE_NR, $this->_FIL_ID);
            if ($AnmeldeDaten === false) {
                $AnmeldeDaten['KDNR'] = self::KUNDENNUMMER;
                $AnmeldeDaten['KENNWORT'] = self::PASSWORT;
                $AnmeldeDaten['ZUSATZ'] = self::ZUSATZ;
            }

            $WeiterSuchen = true;
            while ($WeiterSuchen) {
                if ($SuchArt == 2 OR $SuchArt == 1) {
                    $WeiterSuchen = false;
                } elseif (isset($rsZAL)) {
                    if ($rsZAL->EOF()) {
                        break;
                    }
                    $ArtikelNummer = $rsZAL->FeldInhalt('ZAL_BESTELLNUMMER');
                    $rsZAL->DSWeiter();
                }

                $Param = array('Kundennummer' => $AnmeldeDaten['KDNR']
                               , 'NaviID' => $AnmeldeDaten['ZUSATZ']
                               , 'Passphrase' => $AnmeldeDaten['KENNWORT']
                               , 'Mode' => $SuchArt        // Suche in W�tschner - Nummer (=0) oder TecDoc (=1)
                               , 'SerialID' => ''
                               , 'Nummer' => $ArtikelNummer
                               , 'Code' => $Code
                );

                $ArtikelTreffer = $Artikelliste->addChild('ArtikelTreffer');
                file_put_contents('/var/log/wuetschner_bestellung.log', "Anfrage ATU_GetArtikelinfo:" . json_encode($Param) . "\n", FILE_APPEND);
                $ArtikelDaten = $this->_SOAPClient->ATU_GetArtikelinfo($Param);
                file_put_contents('/var/log/wuetschner_bestellung.log', "Antwort ATU_GetArtikelinfo: " . json_encode($ArtikelDaten) . "\n", FILE_APPEND);

                if (!isset($ArtikelDaten->ErrorCode)) {
                    $Fehler = $this->_Daten->addChild('Fehler');
                    $Fehler->addChild('Nummer', -1);
                    $Fehler->addChild('Text', 'Fehler beim Zugriff auf den Server.');
                } elseif ($ArtikelDaten->ErrorCode > 0) {
                    switch ($ArtikelDaten->ErrorCode) {
                        case 61:        // Artikel gibt es nicht bei Wuetschner
                            // Eigentlich kein Fehler
                            $Fehler = $this->_Daten->addChild('Fehler');
                            $Fehler->addChild('Nummer', $ArtikelDaten->ErrorCode);
                            if ($Code == '' AND $SuchArt == 1) {
                                $Fehlertext = 'Bei der Suche nach Lieferantennummer muss der Hersteller mit Einspeisernummer angegeben werden.';
                            } else {
                                $Fehlertext =
                                    'Es konnte keine Artikel mit der Nummer ' . $ArtikelNummer . ' (Hersteller ' . ($Code == '' ? '::alle::' : $Code) . ') gefunden werden.';
                            }
                            $Fehler->addChild('Text', $Fehlertext);

                            break;
                        case 60:            // Anmeldefehler
                        default:

                            $Fehler = $this->_Daten->addChild('Fehler');
                            $Fehler->addChild('Nummer', $ArtikelDaten->ErrorCode);
                            $Fehler->addChild('Text', 'Fehler beim Zugriff auf die Daten fuer den Artikel ' . $ArtikelNummer . ' / ' . $Code);
                            $Meldung = 'Meldung: ' . $ArtikelDaten->Nummer . "\n";
                            $Meldung .= "\nZugang:" . $AnmeldeDaten['KDNR'] . '/' . $AnmeldeDaten['ZUSATZ'];
                            $Meldung .= "\nFiliale:" . $this->_FIL_ID . "\nArtikel " . $ArtikelNummer . ' / ' . $Code;
                            $this->_awisWerkzeuge->EMail('shuttle@de.atu.eu', 'Wuetschner-Fehler: ' . (string)$ArtikelDaten->ErrorCode, $Meldung);

                            return $this->_Daten->asXML();
                            break;
                    }
                } else {
                    $Artikelliste->Treffer = ((int)$Artikelliste->Treffer) + 1;

                    $Param = array('Kundennummer' => $AnmeldeDaten['KDNR']
                                   , 'NaviID' => $AnmeldeDaten['ZUSATZ']
                                   , 'Passphrase' => $AnmeldeDaten['KENNWORT']
                                   , 'Mode' => 2
                                   , 'SerialID' => $ArtikelDaten->SerialID
                                   , 'ArtikelNummer' => ''
                    );

                    file_put_contents('/var/log/wuetschner_bestellung.log', "Anfrage ATU_GetBestandsInfo:" . json_encode($Param) . "\n", FILE_APPEND);
                    $BestandsInfo = $this->_SOAPClient->ATU_GetBestandsInfo($Param);
                    file_put_contents('/var/log/wuetschner_bestellung.log', "Antwort ATU_GetBestandsInfo: " . json_encode($BestandsInfo) . "\n", FILE_APPEND);


                    $Param = array('Kundennummer' => $AnmeldeDaten['KDNR']
                                   , 'NaviID' => $AnmeldeDaten['ZUSATZ']
                                   , 'Passphrase' => $AnmeldeDaten['KENNWORT']
                                   , 'Mode' => 2
                                   , 'SerialID' => $ArtikelDaten->SerialID
                                   , 'Menge' => $ArtikelInfos['menge']
                                   , 'ArtikelNummer' => ''
                    );

                    file_put_contents('/var/log/wuetschner_bestellung.log', "Anfrage ATU_GetPreisInfo:" . json_encode($Param) . "\n", FILE_APPEND);
                    $PreisInfo = $this->_SOAPClient->ATU_GetPreisInfo($Param);
                    file_put_contents('/var/log/wuetschner_bestellung.log', "Antwort ATU_GetPreisInfo: " . json_encode($PreisInfo) . "\n", FILE_APPEND);


                    // Ergebnis zusammenbauen

                    $SQL = 'SELECT ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG, ZLA_KEY, ZLA_AST_ATUNR, ZAL_KEY, ZLA_ZLH_KEY';
                    $SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS VP';
                    $SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
                    $SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = '.$this->_DB->WertSetzen('ZUK','T',$this->_LKZ).' AND ATW_KENNUNG = ASI_WERT';
                    $SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
                    $SQL .= ', (SELECT AST_VK FROM Artikelstamm WHERE AST_ATUNR = ZLA_AST_ATUNR ) AS AST_VK';

                    if($this->_LKZ=='DE') {
                        $SQL .= ', (SELECT AST_VK FROM Artikelstamm WHERE AST_ATUNR = ZLA_AST_ATUNR ) AS AST_VK';
                    }
                    else {
                        $SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos ';
                        $SQL .= '     WHERE ASI_AIT_ID='.$this->_DB->WertSetzen('ZUK','N0',$this->_ReferenzPreise[$this->_LKZ]).' AND ASI_AST_ATUNR=ZLA_AST_ATUNR AND ROWNUM = 1) AS AST_VK';
                    }

                    $SQL .= ' FROM Zukaufartikel';
                    $SQL .= ' LEFT OUTER JOIN Zukaufartikellieferanten ON zla_key = zal_zla_key AND zal_lie_nr = ' . $this->_DB->WertSetzen('ZUK','T',$this->_LIE_NR)  ;
                    $SQL .= ' WHERE ZAL_BESTELLNUMMER = ' . $this->_DB->WertSetzen('ZUK','T',$ArtikelDaten->Nummer) ;



                    $rsZLA = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('ZUK'));
                    if (isset($DebugXML)) {
                        $DebugXML->addChild('ArtikelsuchSQL', $this->_DB->LetzterSQL());
                    }

                    $Hilf = $ArtikelTreffer->addChild('Zukaufartikel');
                    $Hilf->addChild('ZLA_KEY', ($rsZLA->EOF() ? '' : $rsZLA->FeldInhalt('ZLA_KEY')));
                    $Hilf->addChild('ZLA_ZLH_KEY', ($rsZLA->EOF() == '' ? '' : $rsZLA->FeldInhalt('ZLA_ZLH_KEY')));
                    $Hilf->addChild('ZAL_KEY', ($rsZLA->FeldInhalt('ZAL_KEY') == '' ? '' : $rsZLA->FeldInhalt('ZAL_KEY')));
                    $Hilf->addChild('ZLA_ARTIKELNUMMER', ($rsZLA->EOF() ? '' : $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER')));
                    $Hilf->addCData('ZLA_BEZEICHNUNG', ($rsZLA->EOF() ? '' : ($rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'))));
                    $Hilf->addChild('ZLA_AST_ATUNR', ($rsZLA->FeldInhalt('ZLA_AST_ATUNR') == '' ? '' : $rsZLA->FeldInhalt('ZLA_AST_ATUNR')));
                    $Hilf->addChild('ATU_VK', $rsZLA->FeldInhalt('AST_VK'));

                    $ArtikelTreffer->addChild('Artikelnummer', $ArtikelDaten->TecDocNr);
                    $ArtikelTreffer->addChild('Katalognummer', $ArtikelDaten->Nummer);
                    $ArtikelTreffer->addCData('Artikelbezeichnung', utf8_decode($ArtikelDaten->Bezeichnung));
                    $ArtikelTreffer->addChild('Herstellercode', $ArtikelDaten->EinspeiserCode);
                    $ArtikelTreffer->addChild('Herstellername', '');
                    $ArtikelTreffer->addChild('EAN-Nummer', $ArtikelDaten->EAN);
                    $ArtikelTreffer->addChild('OE-Nummer', '');
                    $ArtikelTreffer->addChild('Bestellnummer', $ArtikelDaten->Nummer);
                    $ArtikelTreffer->addChild('Mengeneinheit', $ArtikelDaten->PE);
                    $ArtikelTreffer->addChild('Tauschteil', ($ArtikelDaten->Tauschwert == 0 ? 0 : 1));
                    // Gewicht mit speichern als Option
                    $ArtikelTreffer->addChild('Gewicht', $ArtikelDaten->Nettogewicht);

                    // Hersteller ermitteln
                    $SQL = 'SELECT ZHK_KEY, ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
                    $SQL .= ' FROM ZUKAUFHERSTELLERCODES';
                    $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZHK_ZLH_KEY = ZLH_KEY';
                    $SQL .= ' WHERE ZLH_TECDOCID = \'' . $ArtikelDaten->EinspeiserCode . '\'';
                    $SQL .= ' AND ZHK_LIE_NR = \'' . $this->_LIE_NR . '\'';
                    $rsZHK = $this->_DB->RecordSetOeffnen($SQL);

                    $Hilf = $ArtikelTreffer->addChild('Hersteller');

                    $Hilf->addChild('ZLH_KEY', $rsZHK->FeldInhalt('ZLH_KEY'));
                    $Hilf->addChild('ZHK_KEY', $rsZHK->FeldInhalt('ZHK_KEY'));
                    $Hilf->addChild('ZLH_BEZEICHNUNG', $rsZHK->FeldInhalt('ZLH_BEZEICHNUNG'));
                    $Hilf->addChild('ZLH_KUERZEL', $rsZHK->FeldInhalt('ZLH_KUERZEL'));

                    $ArtikelDaten->BuchGr = str_pad($ArtikelDaten->BuchGr, 4, '0', STR_PAD_LEFT);
                    $SQL = 'SELECT ZSZ_KEY, ZSZ_SORTIMENT';
                    $SQL .= ' FROM  ZUKAUFSORTIMENTZUORDNUNGEN';
                    $WG1 = substr($ArtikelDaten->BuchGr, 0, 2);
                    $SQL .= ' WHERE ZSZ_WG1 = \'' . $WG1 . '\'';
                    $WG2 = substr($ArtikelDaten->BuchGr, 2, 2);
                    $SQL .= ' AND ZSZ_WG2 = \'' . $WG2 . '\'';
                    $SQL .= ' AND ZSZ_WG3 IS NULL';
                    $SQL .= ' AND ZSZ_LIE_NR = \'' . $this->_LIE_NR . '\'';

                    if (isset($DebugXML)) {
                        $DebugXML->addChild('WarengruppenSQL', $SQL);
                        $DebugXML->addChild('WG1', $WG1);
                        $DebugXML->addChild('WG2', $WG2);
                    }
                    $rsZSZ = $this->_DB->RecordSetOeffnen($SQL);
                    if ($rsZSZ->EOF()) {
                        $SQL = 'INSERT INTO Zukaufsortimentzuordnungen';
                        $SQL .= '(ZSZ_WG1,ZSZ_WG2,ZSZ_WG3,ZSZ_SORTIMENT,ZSZ_LIE_NR,ZSZ_BEZEICHNUNG,ZSZ_USER,ZSZ_USERDAT)';
                        $SQL .= ' VALUES(';
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $WG1);
                        $SQL .= ', ' . $this->_DB->FeldInhaltFormat('T', $WG2);
                        $SQL .= ', null';
                        $SQL .= ', ' . $this->_DB->FeldInhaltFormat('T', self::DEFAULT_SORTIMENT);
                        $SQL .= ', ' . $this->_DB->FeldInhaltFormat('T', $this->_LIE_NR);
                        $SQL .= ', null';
                        $SQL .= ', \'Preisanfrage\'';
                        $SQL .= ', SYSDATE)';

                        $this->_DB->Ausfuehren($SQL);
                        $this->_awisWerkzeuge->EMail('shuttle@de.atu.eu', 'Neues Sortiment', 'Es wurde fuer ' . $this->_LIE_NR . ' ein neues Sortiment angelegt.');

                        $SQL = 'SELECT ZSZ_KEY, ZSZ_SORTIMENT';
                        $SQL .= ' FROM  ZUKAUFSORTIMENTZUORDNUNGEN';
                        $SQL .= ' WHERE ZSZ_WG1 = \'' . $WG1 . '\'';
                        $SQL .= ' AND ZSZ_WG2 = \'' . $WG2 . '\'';
                        $SQL .= ' AND ZSZ_WG3 IS NULL';
                        $SQL .= ' AND ZSZ_LIE_NR = \'' . $this->_LIE_NR . '\'';
                        $rsZSZ = $this->_DB->RecordSetOeffnen($SQL);
                    }

                    $Hilf = $ArtikelTreffer->addChild('Warengruppe');
                    $Hilf->addChild('ZSZ_KEY', $rsZSZ->FeldInhalt('ZSZ_KEY'));
                    $Hilf->addChild('ZSZ_SORTIMENT', $rsZSZ->FeldInhalt('ZSZ_SORTIMENT'));

                    $Hilf = $ArtikelTreffer->addChild('Finanzen');
                    if ($ArtikelDaten->Altteilsteuer == true) {
                        $Hilf->addChild('MWS_ID', $this->_MwStId_Altteil);
                    } else {
                        $Hilf->addChild('MWS_ID', $this->_MwStId);
                    }

                    // MwSt Satz direkt aus der Datenbank lesen
                    if (!isset($Mehrwertsteuer[(int)$Hilf->MWS_ID])) {
                        $rsMWS = $this->_DB->RecordSetOeffnen('SELECT MWS_SATZ from mehrwertsteuersaetze where mws_id = 0' . $Hilf->MWS_ID);
                        $Mehrwertsteuer[(int)$Hilf->MWS_ID] = ($this->_DB->FeldInhaltFormat('N4', $rsMWS->FeldInhalt('MWS_SATZ'), false) / 100.0);
                    }

                    $Hilf->addChild('VKPR', $this->_DB->FeldInhaltFormat('N2', $PreisInfo->VkPreis));
                    $Hilf->addChild('EKPR', $this->_DB->FeldInhaltFormat('N2', $PreisInfo->Bruttopreis));
                    $Hilf->addChild('Rabatt1', abs($this->_DB->FeldInhaltFormat('N2', (string)$PreisInfo->RabattProzent, false)));
                    $Hilf->addChild('Rabatt2', 0);
                    $Hilf->addChild('Altteilwert', $this->_DB->FeldInhaltFormat('N2', (string)$ArtikelDaten->Tauschwert, false));
                    $Hilf->addChild('EK', $this->_DB->FeldInhaltFormat('N2', $PreisInfo->Nettopreis));
                    $Hilf->addChild('MwSt', $this->_DB->FeldInhaltFormat('N2', $Mehrwertsteuer[(int)$Hilf->MWS_ID]));
                    $Hilf->addChild('VP', 1);
                    $Hilf->addChild('Preiseinheit', $this->_DB->FeldInhaltFormat('N0', $PreisInfo->PE));
                    $Hilf->addChild('Technikeinheit', $this->_DB->FeldInhaltFormat('N0', $ArtikelDaten->TE));

                    $Hilf->addChild(
                        'VK',
                        $this->ErmittleKundenVK(
                            $rsZLA->FeldInhalt('ZLA_KEY'),
                            $this->_DB->FeldInhaltFormat('N2', $PreisInfo->Nettopreis),
                            $this->_DB->FeldInhaltFormat('N2', $PreisInfo->VkPreis),
                            $Mehrwertsteuer[(int)$Hilf->MWS_ID],
                            $this->_LKZ
                        )
                    );

                    //********************************************
                    // Gesamte Lieferbare Menge?
                    //********************************************
                    $Hilf = $ArtikelTreffer->addChild('Verfuegbarkeit');
                    $Hilf->addChild('Liefermenge', 0);
                    $Hilf->addChild('NurTelefonisch', 0);

                    $GesamtBestand = 0;
                    foreach ($BestandsInfo->FilialBestand AS $Verkaufshaeuser) {
                        if ($Verkaufshaeuser->Kunde == true OR $Verkaufshaeuser->Zentrale == true) {
                            if ($Verkaufshaeuser->Verfuegbar > 0) {
                                $Hilf1 = $Hilf->addChild('Verkaufshaus');
                                $Hilf1->addChild('Lieferid', $Verkaufshaeuser->Code);
                                $Hilf1->addChild('Liefermenge', $Verkaufshaeuser->Verfuegbar);
                                $Anzeige = '';
                                $AnzeigeToolTipp = '';
                                $Anzeige .= ($Verkaufshaeuser->Zentrale == true ? '/Z' : '');
                                $Anzeige .= ($Verkaufshaeuser->Kunde == true ? '/K' : '');
                                $Anzeige .= ($Verkaufshaeuser->Tour == true ? '/T' : '');
                                $Anzeige .= ($Verkaufshaeuser->NachtExpress == true ? '/X' : '');
                                $AnzeigeToolTipp .= ($Verkaufshaeuser->Zentrale == true ? '/Zentrale' : '');
                                $AnzeigeToolTipp .= ($Verkaufshaeuser->Kunde == true ? '/Kundenfiliale' : '');
                                $AnzeigeToolTipp .= ($Verkaufshaeuser->Tour == true ? '/Tourenfiliale' : '');
                                $AnzeigeToolTipp .= ($Verkaufshaeuser->NachtExpress == true ? '/Nachtexpress' : '');

                                $Anzeige .= ($Verkaufshaeuser->GeplanteFahrt == true ? ',heute' : '');
                                $Hilf1->addChild('Lieferanzeige', substr($Anzeige, 1));
                                $Hilf1->addChild('Lieferanzeigetooltipp', substr($AnzeigeToolTipp, 1));

                                // Verkaufsh�user umsetzen
                                if (isset($this->_Verkaufshaeuser[$Verkaufshaeuser->Kurzname])) {
                                    $Hilf1->addCData('Lieferort', $this->_Verkaufshaeuser[$Verkaufshaeuser->Kurzname]);
                                } else {
                                    $Hilf1->addCData('Lieferort', $Verkaufshaeuser->Kurzname);
                                }

                                $GesamtBestand += $Verkaufshaeuser->Verfuegbar;
                            }
                        }
                    }

                    $Hilf->Liefermenge = $GesamtBestand;
                }
            }
        } catch (awisException $ex) {
            $Fehler = $this->_Daten->addChild('Fehler');
            $Fehler->addChild('Nummer', $ex->getCode());
            $Fehler->addChild('Text', $ex->getMessage());
            $Fehler->addChild('Zeile', $ex->getLine());
            $Fehler->addChild('SQL', $ex->getSQL());
        } catch (Exception $ex) {
            $Fehler = $this->_Daten->addChild('Fehler');
            $Fehler->addChild('Nummer', $ex->getCode());
            $Fehler->addChild('Text', $ex->getMessage());
            $Fehler->addChild('Zeile', $ex->getLine());
        }

        return $this->_Daten->asXML();
    }

    /**
     * Bestellt die angegegeben Artikel
     *
     * @param array $Artikelliste
     * @param string $WANr
     * @param string $Versandart
     * @param string $Sachbearbeiter
     */
    public function Bestellen(awisSimpleXMLElement $Artikelliste, $WANr, $Versandart, $Sachbearbeiter = 'AWIS')
    {
        $Befehl = new awisSimpleXMLElement('<onlinebestellung></onlinebestellung>');
        switch ($Versandart) {
            case 1:        // Abholung
                $VersandartText = 'Abholung';
                break;
            case 2:        // Zustellung
                $VersandartText = 'Zustellung';
                break;
        }

        $AnmeldeDaten = $this->_LeseKundenNummer($this->_LIE_NR, $this->_FIL_ID);
        if ($AnmeldeDaten === false) {
            $AnmeldeDaten['KDNR'] = self::KUNDENNUMMER;
            $AnmeldeDaten['KENNWORT'] = self::PASSWORT;
            $AnmeldeDaten['ZUSATZ'] = self::ZUSATZ;
        }

        $Param = array('Kundennummer' => $AnmeldeDaten['KDNR']
                       , 'NaviID' => $AnmeldeDaten['ZUSATZ']
                       , 'Passphrase' => $AnmeldeDaten['KENNWORT']
                       , 'ATUBestellnummer' => (string)$WANr
        );

        $Keys = array();
        $AnzPositionen = 0;
        $Positionen = array();
        foreach ($Artikelliste->Artikel AS $Bestellposition) {
            $Positionen[] = array('LieferadressCode' => ''
                                  , 'WuetschnerNummer' => (string)$Bestellposition->Bestellnummer
                                  , 'TecDocNummer' => ''
                                  , 'EinspeiserCode' => ''
                                  , 'Menge' => (string)$Bestellposition->Menge
                                  , 'Lieferart' => $VersandartText
                                  , 'Bestellmeldung' => ''
            );

            $Keys[(string)$Bestellposition->Bestellnummer] = (string)$Bestellposition->Key;

            $AnzPositionen++;
        }

        $Param['NumBestellZeilen'] = $AnzPositionen;
        $Param['BestellZeilen'] = $Positionen;


        file_put_contents('/var/log/wuetschner_bestellung.log', "Anfrage ATU_SetBestellung:" . json_encode($Param) . "\n", FILE_APPEND);
        $Antwort = $this->_SOAPClient->ATU_SetBestellung($Param);
        file_put_contents('/var/log/wuetschner_bestellung.log', "Antwort ATU_SetBestellung:" . json_encode($Antwort) . "\n", FILE_APPEND);
        if (is_a($Antwort, 'SoapFault')) {
            echo 'Fehler.<br>';
            // Fehlermeldung senden und FEHLER zur�ck liefern
            $this->_awisWerkzeuge->EMail(
                'shuttle@de.atu.eu',
                'Wuetschner Bestellfehler',
                'Filiale ' . $this->_FIL_ID . ' hat fuer WA ' . (string)$WANr . ' eine Fehlermeldung erhalten.'
            );
            $this->_Daten->addChild('Bestellstatus', $AllgemeineMeldung);
            $this->_Daten->addChild('Benutzer', $Sachbearbeiter);

            $BestellteArtikel = $this->_Daten->addChild('Positionen');
            $BestellterArtikel->addChild('Status', 0);
            $BestellterArtikel->addCData('Statustext', 'Fehler bei Wuetschner. Bitte wiederholen.');
            $BestellterArtikel->addChild('Liefermenge', 0);
            $BestellterArtikel->addChild('Rueckstand', 0);
            $BestellterArtikel->addChild('Bestellnummer', '0000000');
            $BestellterArtikel->addCData('Artikelbezeichnung', 'FEHLER');
            $BestellterArtikel->addChild('Menge', 0);

            $this->_Daten->Bestellstatus = '##3##';

            return $this->_Daten;
        }
        $this->_Daten->addChild('Benutzer', $Sachbearbeiter);

        $AllgemeineMeldung = '';
        $MeldungenAnzahl = $Antwort->NumError;
        $BestellBestaetigungen = array();

        $i = 0;
        foreach ($Antwort->ErrorZeilen AS $Meldungen) {
            if (substr($Meldungen, 0, 1) == '(')        // Meldungen auswerten
            {
                list($Id) = sscanf($Meldungen, '(%d)');
                $Text = substr($Meldungen, strlen('(' . $Id . ')'));
                $Positionen[$Id - 1]['Bestellmeldung'] = trim($Text);
            } else {
                $AllgemeineMeldung = $Meldungen;
            }
        }
        file_put_contents('/var/log/wuetschner_bestellung.log', "\nBestellung: " . $Sachbearbeiter . " \n", FILE_APPEND);
        file_put_contents('/var/log/wuetschner_bestellung.log', serialize($Antwort) . "\n", FILE_APPEND);

        $this->_Daten->addChild('Bestellstatus', $AllgemeineMeldung);
        $BestellteArtikel = $this->_Daten->addChild('Positionen');
        $GelieferteArtikel = 0;
        $NichtGelieferteArtikel = 0;
        $Rueckstand = 0;

        $i = 0;
        foreach ($Artikelliste->Artikel AS $Bestellposition) {
            $BestellterArtikel = $BestellteArtikel->addChild('Artikel');
            $BestellterArtikel->addChild('Bestellnummer', (string)$Bestellposition->Bestellnummer);
            $BestellterArtikel->addCData('Artikelbezeichnung', awisWerkzeuge::awisGrossKlein((string)$Bestellposition->Artikelbezeichnung));
            $BestellterArtikel->addChild('Menge', (string)$Bestellposition->Menge);

            $BestellterArtikel->addChild('Key', $Keys[(string)$Bestellposition->Bestellnummer]);

            // Mindermenge
            if (substr($Positionen[$i]['Bestellmeldung'], 0, strlen('MinderMenge:')) == 'MinderMenge:') {
                $Details = explode(' ', trim(substr($Positionen[$i]['Bestellmeldung'], strlen('MinderMenge:') + 1)));
                file_put_contents('/var/log/wuetschner_bestellung.log', serialize($Details) . "\n", FILE_APPEND);

                $Mengen['F'] = 0;        // Filiale
                $Mengen['Z'] = 0;        // Zentrale (kommt morgen)
                $Mengen['R'] = 0;        // Nicht lieferbar
                $Mengen['W'] = 0;        // Wunschmenge
                // Aufbau:  W=1,00, F=0,00, Z=0,00, R=1,00
                foreach ($Details AS $Detail) {
                    $Detail = explode('=', $Detail);
                    $Mengen[$Detail[0]] = $this->_DB->FeldInhaltFormat('N0', $Detail[1]);
                }
                $BestellterArtikel->addChild('Status', 0);
                $BestellterArtikel->addCData('Statustext', $Positionen[$i]['Bestellmeldung']);
                $BestellterArtikel->addChild('Liefermenge', $Mengen['F']);
                $BestellterArtikel->addChild('Rueckstand', $Mengen['Z']);
                if ($Mengen['F'] + $Mengen['Z'] == $Mengen['W']) {
                    $GelieferteArtikel++;
                }
                $Rueckstand = $Mengen['Z'];
            } elseif (substr($Positionen[$i]['Bestellmeldung'], 0, strlen('MinderMenge:')) == 'MorgenMenge:') {
                $Details = explode(' ', trim(substr($Positionen[$i]['Bestellmeldung'], strlen('MorgenMenge:') + 1)));
                file_put_contents('/var/log/wuetschner_bestellung.log', serialize($Details) . "\n", FILE_APPEND);

                $Mengen['F'] = 0;        // Filiale
                $Mengen['Z'] = 0;        // Zentrale (kommt morgen)
                $Mengen['R'] = 0;        // Nicht lieferbar
                $Mengen['W'] = 0;        // Wunschmenge
                // Aufbau:  W=1,00, F=0,00, Z=0,00, R=1,00
                foreach ($Details AS $Detail) {
                    $Detail = explode('=', $Detail);
                    $Mengen[$Detail[0]] = $this->_DB->FeldInhaltFormat('N0', $Detail[1]);
                }
                $BestellterArtikel->addChild('Status', 0);
                $BestellterArtikel->addCData('Statustext', $Positionen[$i]['Bestellmeldung']);
                $BestellterArtikel->addChild('Liefermenge', 0);
                $BestellterArtikel->addChild('Rueckstand', $Mengen['F'] + $Mengen['Z']);
                if ($Mengen['F'] + $Mengen['Z'] == $Mengen['W']) {
                    $GelieferteArtikel++;
                }
                $Rueckstand = $Mengen['F'] + $Mengen['Z'];
            } elseif ($Positionen[$i]['Bestellmeldung'] == '')        // Alles wurde bestellt
            {
                $BestellterArtikel->addChild('Status', 1);
                $BestellterArtikel->addCData('Statustext', 'bestellt');
                $BestellterArtikel->addChild('Liefermenge', $Bestellposition->Menge);
                $BestellterArtikel->addChild('Rueckstand', 0);
                $GelieferteArtikel++;
            } else {
                $BestellterArtikel->addChild('Status', 1);
                $BestellterArtikel->addCData('Statustext', $Positionen[$i]['Bestellmeldung']);
                $BestellterArtikel->addChild('Liefermenge', 0);
                $BestellterArtikel->addChild('Rueckstand', $Bestellposition->Menge);
                $Rueckstand += $Bestellposition->Menge;
            }

            if ($Rueckstand == 0 AND $GelieferteArtikel > 0) {
                $this->_Daten->Bestellstatus = '##1##';
            } elseif ($GelieferteArtikel == 0 AND $Rueckstand == 0) {
                $this->_Daten->Bestellstatus = '##3##';
            } else {
                $this->_Daten->Bestellstatus = '##2##';
            }

            $i++;
        }
        file_put_contents('/var/log/wuetschner_bestellung.log', "Daten: " . $this->_Daten->asXML() . " \n", FILE_APPEND);

        return $this->_Daten;
    }

    private function setzeLandesspezifisches($FIL_ID)
    {

        file_put_contents('/var/log/wuetschner_bestellung.log', "Setze die Steuern f�r die Filiale $FIL_ID ", FILE_APPEND);

        $SQL = 'select LAN_CODE from v_filialen_aktuell where FIL_ID =' . $this->_DB->WertSetzen('LAN', 'T', $FIL_ID);
        $rsLAN = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('LAN'));
        $this->_LKZ = $rsLAN->FeldInhalt('LAN_CODE');

        if ($this->_LKZ == 'AT') {
            $this->_MwStId = self::MWST_ID_AT_STANDARD;
            $this->_MwStId_Altteil = self::MWST_ID_AT_STANDARD;
            $this->_LIE_NR = self::LIE_NR_AT;
            $this->_WSDL = self::WSDL_DATEI_AT;
        } else {
            $this->_MwStId = self::MWST_DE_STANDARD;
            $this->_MwStId_Altteil = self::MWST_DE_ALTTEIL;
            $this->_LIE_NR = self::LIE_NR;
            $this->_WSDL = self::WSDL_DATEI;
        }

        file_put_contents('/var/log/wuetschner_bestellung.log', " im Land $this->_LKZ" . " \n", FILE_APPEND);
    }
}

?>
