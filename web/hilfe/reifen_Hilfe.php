<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>

<title></title>
</head>

<body>

<h2>AWIS - Reifensuche</h2>

Mit diesem Fenster haben Sie die M�glichkeit, Reifen nach unterschiedlichen Kriterien zu suchen. 
Werden keine Kriterien angegeben, so werden alle Reifen im System angezeigt. 
<br><br>
Um nach Reifen zu suchen, f�llen Sie alle gew�nschten Suchfelder aus. Je mehr Felder Sie ausf�llen,
desto weniger Reifen werden gefunden, d.h. desto genauer ist die Trefferliste.
<br><br>
<table id=DatenTabelle border=1 width=95%>
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Bedeutung</td></tr>
<tr><td>Breite</td><td>Reifenbreite, z.B. 165.</td></tr>
<tr><td>Querschnitt</td><td>Reifenquerschnitt, z.B. 70.</td></tr>
<tr><td>Felgengr��e</td><td>Gr��e der Felge in Zoll, z.B. 17</td></tr>
<tr><td>LLKW</td><td>Eignung f�r Leicht-LKW. M�gliche Werte sind (bisher): C oder leer.</td></tr>
<tr><td>Load Ixdex</td><td>Tragf�higkeit des Reifens, z.B. 70.</td></tr>
<tr><td>Load Index 2</td><td>Zweite Tragf�higkeit des Reifens, z.B. 80.</td></tr>
<tr><td><a name=SpeedIndex>Speed Index</td><td>Geschwindigkeitsangabe f�r die Reifen als Zeichen. Sie k�nnen hier
							aus der (fest hinterlegten) Liste alle Indizes ausw�hlen.</td></tr>
<tr><td>Max VK</td><td>Maximaler Preis in Hausw�hrung, z.B. 50. Sollte f�r einen Reifen ein
							Aktionspreis gelten, so wird nach dem niedrigeren Aktionspreis gesucht.</td></tr>
<tr><td>ATU Nummer</td><td>Eine g�ltige ATU-Artikelnummer. Es k�nnen auch Jokerzeichen eingesetzt werden, 
							d.h. es k�nnen ein Stern (*) f�r beliebige oder ein Fragezeichen (?) 
							f�r ein beliebiges Zeichen verwendet werden. So wird z.B. bei dem Suchmuster
							FU3* alle Reifen gefunden, die mit der Artikelnummer FU3 beginnen.</td></tr>
<tr><td>Bezeichnung</td><td>Reifenbezeichnung. Hier wird nach der im Artikelstamm hinterlegten Bezeichnung
							im Reifen gesucht. Es k�nnen - wie bei der ATU-Nummer - Jokerzeichen
							f�r die Suche verwendet werden, also z.B. *winter* f�r alle Reifen, deren
							Bezeichnung das Wort winter enth�lt.</td></tr>
<tr><td>Hersteller</td><td>Reifenhersteller. Es k�nnen - wie bei der ATU-Nummer - Jokerzeichen
							f�r die Suche verwendet werden, also z.B. *cont* f�r alle Reifen, deren
							Hersteller das Wort cont enth�lt.</td></tr>
<tr><td>Auswahl speichern</td><td>Dieses Feld beeinflu�t die Suchmaske selbst. Normalerweise wird die
							Suchmaske mit leeren Suchfeldern ge�ffnet. Wird diese Option aktiviert,
							so werden alle ausgef�llten Suchbegriffe gespeichert und alle Werte
							bei der n�chsten Suche automatisch in die Suchfelder eingetragen.</td></tr>
<?php
If(awisHatDasRecht(251) != 0)
{
	print "<tr><td>KBListe</td><td>Zeigt die Reifen als KB-Liste an. Diese Option ist nicht f�r alle Benutzer verf�gbar.";
	print "</td></tr>";
}
?>					
</table>

<br>

Die nachfolgenden Einstellungen beeinflussen die Darstellung der Daten.
<br>
<br>
<table border=1 id=DatenTabelle width=95%>
<tr><td id=FeldBez>Option</td><td id=FeldBez>Bemerkung</td></tr>
<tr><td>Sortierung nach</td><td>Hiermit kann die Sortierung der Daten angegeben werden. Eine Ausnahme
							stellt die Sortierung f�r das Call Center (<a href=#CCSortierung>CC-Sortierung</a>) dar.<br>
							Diese Option wird gespeichert und als Vorgabe bei der n�chsten Suche
							wieder vorgeschlagen.</td></tr>
<tr><td>Reifentyp</td><td>Mit dieser Option k�nnen die Reifentypen eingeschr�nkt werden. Die Optionen
							Sommer- und Winterreifen werden permanent gespeichert und bei der 
							n�chsten Suche wieder als Vorschlag angezeigt. Sollte f�r eine Suche
							ein anderer Reifentyp verwendet werden, so wird diese Option nicht 
							gespeichert. Wird jedoch der Programmparameter <i><b>ReifenSucheAlleWerteMerken</i>
							auf den Wert 1 gesetzt, so werden alle Typen gespeichert.</td></tr>
<tr><td>CC-Sortierung</td><td>Mit dieser Option wird eine spezielle Sortierung f�r das Call Center
							aktiviert (vgl. weiter <a href=#CCSortierung>unten</a>).</td></tr>
<tr><td>Reifen mit h�herem Speedindex</td><td>Ist diese Option aktiviert, so werden auch Reifen mit einem
							h�heren <a href=#Speedindex>Speedindex</a> angezeigt.</td></tr>
</table>


<hr>
<h2><a name=CCSortierung>Hinweise zur Verwendung der CC-Sortierung</h2>


<p>Durch Setzen der CC-Sortierungs-Option ergeben sich folgende Konsequenzen,
die in der nachfolgenden Tabelle beschrieben sind.</p>
<table border=1 width="95%" height="103">
  <tr>
    <td id=FeldBez width="27%" height="47">
      <p style="line-height: 100%; margin: 10"><font size="4">Einschr�nkung</font></td>
    <td id=FeldBez width="73%" height="47">
      <p style="line-height: 100%; margin: 10"><font size="4">Beschreibung</font></td>
  </tr>
  <tr>
    <td width="27%" height="19" valign="top">
      <p style="line-height: 100%; margin: 10"><b>Sortierfolge</b></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Die angezeigten Produkte werden
      nacheinander nach der Kennung des Artikels (Standardartikel, dann
      Auslauftypen A, E, L, P), dem Speed-Index, und zuletzt nach dem in der
      Auswahlmaske angegebenen Kriterium (z.B. ATU-Nummer) sortiert.</p>
      <p style="line-height: 100%; margin: 10">Ohne CC-Sortierung wird nach dem
      in der Auswahlmaske angegebenen Kriterium (z.B. ATU-Nummer) sortiert.</td>
  </tr>
  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%; margin: 10">Keine Eingabe im Feld <b>Querschnitt</b></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Es werden nur Reifen mit dem
      Querschnitt 80 sowie solche ohne Querschnittsangabe angezeigt.</td>
  </tr>
  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%; margin: 10">Keine Eingabe im Feld <b>LLKW</b></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Es werden nur Reifen ohne
      Kennzeichen (<b>C)</b> f�r erh�hte Tragf�higkeit angezeigt.</td>
  </tr>
  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%; margin: 10">Produktstatus <b>Neuanlage</b>
      bzw. Reifen <b>ohne g�ltigen Preis</b></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Es werden nur Produkte angezeigt,
      die nicht als Neuanlage gekennzeichnet sind, und f�r die ein g�ltiger
      VK-Preis eingetragen ist. Es werden auch keine Artikel mit SUCH2 = SONDERARTI
      angezeigt (d.h. Artikel ist noch nicht in die Filialen �bernommen).</td>
  </tr>
</table>

<hr>
<h2><a name=Kennzeichen>Reifenkennzeichen</h2>
<br>
<img src="./Tragfaehigkeitskennziffern_1_0001.jpg" alt=Tragf�higkeitskennziffern>
<img src="./Tragfaehigkeitskennziffern_2_0001.jpg" alt=Tragf�higkeitskennziffern>

</body>
</html>
