<?php
	global $PHP_AUTH_USER;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $BearbeitungsStufe;
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $PHP_AUTH_USER));
	
	$SQL = "SELECT * FROM EANNummern, TeileInfos ";
	$SQL .= " WHERE TEI_KEY2=EAN_KEY AND TEI_ITY_ID2 = 'EAN' AND TEI_KEY1=0" . $Artikel[0];
//print "<hr>$SQL<hr>";

	$rsEANNummer = awisOpenRecordset($con, $SQL);
	if($rsEANNummer==FALSE)
	{
		awisErrorMailLink("artikel_EANNummern.php", 2, $awisDBFehler['message']);
	}
	$rsEANNummerZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=300 border=1>";
	print "<cols><col width=80></col><col width=200></col><col width=100></col></cols>";
	print "<tr>";
	print "<tr><td id=FeldBez></td>";
	print "<td id=FeldBez>EAN-Nummer</td>";
	print "<td id=FeldBez>Typ</td>";	
	print "</tr>";
	
	for($Zeile=0;$Zeile<$rsEANNummerZeilen;$Zeile++)
	{
		print "<tr>";
		
		if(($BearbeitungsStufe & 4)==4)			// Recht 401
		{
			print "<td><input name=cmdEANLoeschen_" . $rsEANNummer["EAN_KEY"][$Zeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
		}
		else
		{
			print "<td>&nbsp;</td>";
		}
		print "<td><a href=./artikel_Main.php?cmdAktion=EANArtikel&EANKEY=" . $rsEANNummer["EAN_KEY"][$Zeile] . ">" . $rsEANNummer["EAN_NUMMER"][$Zeile] . "</a></td>";
		print "<td>" . $rsEANNummer["EAN_TYP"][$Zeile] . "</td>";

		print "</tr>";
	}

		// Neue Zuordnung erlaubt?
	if(($BearbeitungsStufe & 4)==4)			// Recht 401
	{
		print "<tr>";
		print "<td><input name=txtEAN_KEY_0 type=hidden></td>";
		print "<td><input name=txtEAN_Nummer size=30></td>";
		print "<td><input name=txtEAN_TYP size=5></td>";
		print "</tr>";
	}
	
	print "</table>";

	unset($rsEANNummer);
?>