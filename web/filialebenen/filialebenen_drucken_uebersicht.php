<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');

global $awisRSZeilen;

ini_set('max_execution_time','9000');

$AWISBenutzer = new awisUser();

$con = awisLogon();
$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FEB','FEB%');
$TextKonserven[]=array('TITEL','tit_FilialebenenUebersicht');
$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
$TextKonserven[]=array('Wort','Seite');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Filialebenen');
$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund

$Spalte = 20;
$Zeile = 20;

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','B',15);
$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$AWISSprachKonserven['TITEL']['tit_FilialebenenUebersicht']),0,0,'L',0);

$Ausdruck->_pdf->SetXY($Spalte,$Ausdruck->SeitenHoehe()-12);
$Ausdruck->_pdf->SetFont('Arial','B',6);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('TP',$AWISSprachKonserven['Wort']['Seite']).' '.$Ausdruck->_pdf->PageNo(),0,0,'L',0);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('TP',$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich']),0,0,'C',0);
$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-40)/3,5,awisFormat('DU',date('d.m.Y H:i')),0,0,'R',0);
$Ausdruck->_pdf->Line(20,$Ausdruck->SeitenHoehe()-11,$Ausdruck->SeitenBreite()-20,$Ausdruck->SeitenHoehe()-11);

$Zeile = 40;

$Punkte = array();
$Breite = $Ausdruck->SeitenBreite()-40;
$SQL = 'SELECT DISTINCT FEB_EBENE AS Ebenen FROM FilialEbenen';
if(isset($_GET['FEB_KEY']))
{
	$SQL .= ' WHERE FEB_EBENE >= '.awis_FeldInhaltFormat('N0',$_GET['FEB_EBENE'],false);
}
$rsEbenen = awisOpenRecordset($con, $SQL);
$AnzEbenen = $awisRSZeilen;

$EbenenHoehe = (160/floatval($AnzEbenen))-10;
$MaxKaestchen = 25;
$BreiteKaestchen = 6;
$LetzterVater = 0;
$EbeneVersetzt=0;
$VersatzBeiNeuemVater = 20;

$StartEbene = awis_FeldInhaltFormat('N0',$_GET['FEB_EBENE'],false);
for($Ebene=($StartEbene==0?1:$StartEbene);$Ebene<=9;$Ebene++)
{
	// Abfrage mit den Kunden, f�r die etwas gedruckt werden soll
	$SQL = 'SELECT DISTINCT FE2.*, FE1.FEB_SORTIERUNG AS SOR2';
	$SQL .= ' FROM Filialebenen FE2';
	$SQL .= ' LEFT OUTER JOIN FilialEbenen FE1 ON FE2.FEB_FEB_KEY = FE1.FEB_KEY';
	$SQL .= ' WHERE FE2.FEB_EBENE = '.$Ebene;
	$SQL .= ' AND trunc(FE2.FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FE2.FEB_GUELTIGBIS) >= trunc(SYSDATE)';
	if(isset($_GET['FEB_KEY']))
	{
		$SQL .= ' AND ( FE2.FEB_KEY = '.awis_FeldInhaltFormat('N0',$_GET['FEB_KEY'],false);
		$SQL .= ' OR  FE2.FEB_FEB_KEY = '.awis_FeldInhaltFormat('N0',$_GET['FEB_KEY'],false).')';
	}
	$SQL .= ' ORDER BY FE1.FEB_SORTIERUNG, FE2.FEB_SORTIERUNG, FE2.FEB_BEZEICHNUNG';

	$rsFBR = awisOpenRecordset($con, $SQL);
	$rsFBRZeilen = $awisRSZeilen;

	$AnzVaeter = (isset($rsFBR['FEB_FEB_KEY'][0])?sizeof(array_count_values($rsFBR['FEB_FEB_KEY'])):0);
	$EbeneVersetzt=0;

	if($rsFBRZeilen>0)
	{
		$LetzterVater = $rsFBR['FEB_FEB_KEY'][0];
		$Pos = ($Breite/$rsFBRZeilen);//+($AnzVaeter*$VersatzBeiNeuemVater/$rsFBRZeilen);

		$Ausdruck->_pdf->SetFont('Arial','',6);

		for($FBRZeile=0;$FBRZeile<$rsFBRZeilen;$FBRZeile++)
		{
			if($rsFBRZeilen > $MaxKaestchen AND $LetzterVater != $rsFBR['FEB_FEB_KEY'][$FBRZeile])
			{
				$Zeile+=7;
				$Ausdruck->_pdf->SetY($Zeile);
				$EbeneVersetzt++;
				$LetzterVater = $rsFBR['FEB_FEB_KEY'][$FBRZeile];
			}
			$Punkte[$rsFBR['FEB_KEY'][$FBRZeile]]=array(($FBRZeile*$Pos)+($Pos/2)+($BreiteKaestchen/2)-($EbeneVersetzt*$VersatzBeiNeuemVater),$Zeile);

			$Ausdruck->_pdf->SetXY(($FBRZeile*$Pos)+($Pos/2),$Zeile);
			//$Ausdruck->_pdf->SetXY(($FBRZeile*$Pos)+($Pos/2)-($EbeneVersetzt*$VersatzBeiNeuemVater),$Zeile);
			$Ausdruck->_pdf->Cell($BreiteKaestchen,6,$rsFBR['FEB_BEZEICHNUNG'][$FBRZeile],1,0,'C');


			if($rsFBR['FEB_FEB_KEY'][$FBRZeile]!='' AND isset($Punkte[$rsFBR['FEB_FEB_KEY'][$FBRZeile]][0]))
			{
				$Ausdruck->_pdf->Line($Punkte[$rsFBR['FEB_KEY'][$FBRZeile]][0],$Punkte[$rsFBR['FEB_KEY'][$FBRZeile]][1],$Punkte[$rsFBR['FEB_FEB_KEY'][$FBRZeile]][0],$Punkte[$rsFBR['FEB_FEB_KEY'][$FBRZeile]][1]+6);
			}
		}
		$Zeile+=$EbenenHoehe+($EbeneVersetzt*7);
	}

}

// Ende des Massendrucks?
//@ob_clean();
$Ausdruck->Anzeigen();

awisLogoff($con);
?>