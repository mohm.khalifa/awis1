<?php
global $AWISBenutzer;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GSD','%');	
	$TextKonserven[]=array('Wort','Auswahl_ALLE');	
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Kostenstelle');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht28000=$AWISBenutzer->HatDasRecht(28000);

	if($Recht28000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./gutschein_Main.php?cmdAktion=Details>");
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSD'));

	$script = '	$(document).ready(function(){
					showKunde(-1);});
				function showKunde(intVal){
											
											if (intVal == \'E\'){
												$( "#kundeint" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												$( "#kundeext" ).show();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
												$( "#anzeige" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
											} 
											else if (intVal == \'I\'){
												$( "#kundeint" ).show();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
												$( "#kundeext" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												$( "#anzeige" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
											}
											else{
												$( "#kundeint" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												$( "#kundeext" ).hide();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												$( "#anzeige" ).show();
												$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
											}
										}';
	$Form->SchreibeHTMLCode('<script>' . $script . '</script>');

	//Eingabemaske bzw. Suchmaske
	$Form->Formular_Start();

	//Gutscheinnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_GUTSCHEINNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*GSD_GUTSCHEINNUMMER',($Param['SPEICHERN']=='on'?$Param['GSD_GUTSCHEINNUMMER']:''),25,200,true);
	$Form->ZeileEnde();

	//Auftragsnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_AUFTRAGNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*GSD_AUFTRAGNUMMER',($Param['SPEICHERN']=='on'?$Param['GSD_AUFTRAGNUMMER']:''),25,200,true);
	$Form->ZeileEnde();

	//Auftragstatus Offen-Bearbeitung-Erledigt
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_AUFTRAGSTATUS'].':',190);
	$Daten = explode("|",$AWISSprachKonserven['GSD']['lst_GSD_KOPFSTATUS']);
	$Form->Erstelle_SelectFeld('*GSD_AUFTRAGSTATUS',($Param['SPEICHERN']=='on'?$Param['GSD_AUFTRAGSTATUS']:''),200,true,'',$AWISSprachKonserven['Liste']['lst_ALLE_0'],'2','','',$Daten);
	$Form->ZeileEnde();

	//Kostenstelle
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KOSTENSTELLE'].':',190);
	$Form->Erstelle_TextFeld('*GSD_KOSTENSTELLE',($Param['SPEICHERN']=='on'?$Param['GSD_KOSTENSTELLE']:''),25,200,true);
	$Form->ZeileEnde();

	//Gutscheinkunde auswahl
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE'],190);
	$Daten = explode('|', $AWISSprachKonserven['GSD']['lst_GSD_KUNDEAUSWAHL']);
	$Form->Erstelle_SelectFeld('*GSD_KUNDE', (isset($_POST['txtGSD_KUNDE'])?$_POST['txtGSD_KUNDE']:''), 250, true, '','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten, 'onchange="showKunde(this.value)"');
	$Form->ZeileEnde();

	//Vorauswahl anzeigen Kunde intern und extern, Steuerung per Script
	if(isset($_POST['txtGSK_KUNDE']) AND isset($_POST['txtGSK_KUNDE_EXTERN']) AND ($_POST['txtGSK_KUNDE']=='E'))
	{
		$Form->ZeileStart('', 'anzeige');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_GKU_KEY'],190);
		$SQL_lst = 'SELECT GKU_KEY, GKU_KONZERN AS Konzern';
		$SQL_lst .= ' FROM GUTSCHEINKUNDEEXTERN';
		$Form->Erstelle_SelectFeld('*GSD_GKU_KEY',($Param['SPEICHERN']=='on'?$Param['GSD_GKU_KEY']:''), 200,true,$SQL_lst,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','');
		$Form->ZeileEnde();
	}
	if(isset($_POST['txtGSK_KUNDE']) AND isset($_POST['txtGSK_KUNDE_INTERN']) AND ($_POST['txtGSK_KUNDE']=='I'))
	{
		$Form->ZeileStart('', 'anzeige');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE_INTERN'],190);
		$Form->Erstelle_TextFeld('*GSD_KUNDE_INTERN',($Param['SPEICHERN']=='on'?$Param['GSD_KUNDE_INTERN']:''),250,250,true);
		$Form->ZeileEnde();
	}

	$Form->ZeileStart('', 'kundeint');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE_INTERN'],190);
	$Form->Erstelle_TextFeld('*GSD_KUNDE_INTERN',($Param['SPEICHERN']=='on'?$Param['GSD_KUNDE_INTERN']:''),250,250,true);
	$Form->ZeileEnde();

	$Form->ZeileStart('', 'kundeext');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_GKU_KEY'],190);
	$SQL_lst = 'SELECT GKU_KEY, GKU_KONZERN AS Konzern';
	$SQL_lst .= ' FROM GUTSCHEINKUNDEEXTERN';
	$Form->Erstelle_SelectFeld('*GSD_GKU_KEY',($Param['SPEICHERN']=='on'?$Param['GSD_GKU_KEY']:''), 200,true,$SQL_lst,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','');
	$Form->ZeileEnde();

	//Gutscheinprofil
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_PROFIL'].':',190);
	$SQL_lst = 'SELECT GSV_KEY, GSV_BEZEICHNUNG AS Profil';
	$SQL_lst .= ' FROM GUTSCHEINVORLAGE';
	$Form->Erstelle_SelectFeld('*GSD_PROFIL',($Param['SPEICHERN']=='on'?$Param['GSD_PROFIL']:''), 200,true,$SQL_lst,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','');
	$Form->ZeileEnde();

	//Gutscheinstatus vom GiftCard-Server
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_BUCHUNGSART'].':',190);
	$Daten = explode("|",$AWISSprachKonserven['GSD']['lst_GSD_STATUS']);
	$Form->Erstelle_SelectFeld('*GSD_BUCHUNGSART',($Param['SPEICHERN']=='on'?$Param['GSD_BUCHUNGSART']:''),200,true,'',$AWISSprachKonserven['Liste']['lst_ALLE_0'],'2','','',$Daten);
	$Form->ZeileEnde();

	//Auswahl speichern
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	// Schaltfl�chen f�r dieses Register
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht28000&4) == 4)
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1308141114");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1308141115");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>