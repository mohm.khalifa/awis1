<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SAB','%');
	$TextKonserven[]=array('XDI','%');
	$TextKonserven[]=array('SBZ','SBZ_BEZEICHNUNG');
	$TextKonserven[]=array('Liste','SAB_%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Wort','EANPruefung%');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19001 = $AWISBenutzer->HatDasRecht(19001);
	if(($Recht19001)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$SABParam = unserialize($AWISBenutzer->ParameterLesen('Formular_ServiceArtikelBenachrichtigungen'));
	if(!isset($SABParam['BLOCK']))
	{
		$SABParam['BLOCK']=1;
		$SABParam['ORDERBY']='SAB_STUFE';
		$SABParam['KEY']='';
		$SABParam['SAAKEY']=$AWIS_KEY1;
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY SAB_STUFE';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}
	
	$SQL = 'SELECT SVCARTIKELBENACHRICHTIGUNGEN.*, SBZ_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM SVCARTIKELBENACHRICHTIGUNGEN ';
	$SQL .= ' INNER JOIN SVCZEITRAEUME ON SAB_SBZ_KEY = SBZ_KEY';
	$SQL .= ' WHERE SAB_SVA_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['SAB_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['SAB_KEY']);
	}
	elseif($SABParam['KEY']!='' AND !isset($_GET['SListe']))
	{
		$AWIS_KEY2 = ($SABParam['SAAKEY']==$AWIS_KEY1?$SABParam['KEY']:0);
	}
	
	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND SAB_KEY = 0'.$AWIS_KEY2;
		$_GET['SAB_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}


	$SABParam['SAAKEY']=$AWIS_KEY1;
	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsSAB = $DB->RecordsetOeffnen($SQL);

    $Form->Formular_Start();

	if($rsSAB->AnzahlDatensaetze()>1 OR isset($_GET['SListe']) OR !isset($_GET['SAB_KEY']))
	{
		$Form->ZeileStart();
		$Icons=array();
		if((intval($Recht19001)&6)!=0)
		{
			$Icons[] = array('new','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Benachrichtigungen&SVA_KEY='.$AWIS_KEY1.'&SAB_KEY=-1');
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAB_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAB_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAB']['SAB_BEZEICHNUNG'],350,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SBZ_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SBZ_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBZ']['SBZ_BEZEICHNUNG'],250,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAB_MONAT,SAB_TAG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAB_MONAT,SAB_TAG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAB']['SAB_TAG'],150,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAB_STUFE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAB_STUFE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAB']['SAB_STUFE'],150,'',$Link);
		
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$SABParam['KEY']='';
		
		$DS=0;
		while(!$rsSAB->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht19001&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Benachrichtigungen&SVA_KEY='.$AWIS_KEY1.'&SAB_KEY='.$rsSAB->FeldInhalt('SAB_KEY'));
			}
			if(intval($Recht19001&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Benachrichtigungen&SVA_KEY='.$AWIS_KEY1.'&Del='.$rsSAB->FeldInhalt('SAB_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*SAB_BEZEICHNUNG',$rsSAB->FeldInhalt('SAB_BEZEICHNUNG'),20,350,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SBZ_BEZEICHNUNG',$rsSAB->FeldInhalt('SBZ_BEZEICHNUNG'),20,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SAB_TAG',$rsSAB->FeldInhalt('SAB_TAG').'/'.$rsSAB->FeldInhalt('SAB_MONAT'),20,150,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SAB_STUFE',$rsSAB->FeldInhalt('SAB_STUFE'),20,150,false,($DS%2),'','','T');
            if($rsSAB->FeldInhalt('SAB_BEMERKUNG')!='')
            {
                $Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsSAB->FeldInhalt('SAB_BEMERKUNG'));
            }
			$Form->ZeileEnde();

			$rsSAB->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsSAB->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht19001&6);
		$AWIS_KEY2 = $rsSAB->FeldInhalt('SAB_KEY');
		$SABParam['KEY']=$AWIS_KEY2;
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		$Form->Erstelle_HiddenFeld('SAB_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('SAB_SVA_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Benachrichtigungen&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAB->FeldInhalt('SAB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAB->FeldInhalt('SAB_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('SAB_BEZEICHNUNG',$rsSAB->FeldInhalt('SAB_BEZEICHNUNG'),50,500,$EditModus,'','','','T');
		if($EditModus)
		{
			$AWISCursorPosition='txtSAB_BEZEICHNUNG';
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_SBZ_KEY'].':',200);
		$SQL = ' SELECT SBZ_KEY, SBZ_BEZEICHNUNG';
		$SQL .= ' FROM SVCZEITRAEUME';
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('!SAB_SBZ_KEY',$rsSAB->FeldInhalt('SAB_SBZ_KEY'),"200:190",$EditModus,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_TAG'].':',200);
		$ListenDaten=array();
		for($i=1;$i<=31;$i++)
		{
			$ListenDaten[] = $i.'~'.$i;
		}
		$Form->Erstelle_SelectFeld('!SAB_TAG',$rsSAB->FeldInhalt('SAB_TAG'),"200:190",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_MONAT'].':',140);
		$ListenDaten=array();
		for($i=1;$i<=12;$i++)
		{
			$ListenDaten[] = $i.'~'.$i;
		}
		$Form->Erstelle_SelectFeld('!SAB_MONAT',$rsSAB->FeldInhalt('SAB_MONAT'),"200:190",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_STUFE'].':',200);
		$Form->Erstelle_TextFeld('!SAB_STUFE',$rsSAB->FeldInhalt('SAB_STUFE'),3,200,$EditModus,'','','','N0');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_WARTEZEIT'].':',200);
		$Form->Erstelle_TextFeld('!SAB_WARTEZEIT',$rsSAB->FeldInhalt('SAB_WARTEZEIT'),3,100,$EditModus,'','','','N0');
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_STATUS'].':',200);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAB_STATUS']);
		$Form->Erstelle_SelectFeld('!SAB_STATUS',$rsSAB->FeldInhalt('SAB_STATUS'),"200:190",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_EMPFAENGER'].':',200);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAB_EMPFAENGER']);
		$Form->Erstelle_SelectFeld('!SAB_EMPFAENGER',$rsSAB->FeldInhalt('SAB_EMPFAENGER'),"200:190",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_SMSTEXT'].':',200);
		$Form->Erstelle_TextFeld('SAB_SMSTEXT',$rsSAB->FeldInhalt('SAB_SMSTEXT'),80,100,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_MAILBETREFF'].':',200);
		$Form->Erstelle_TextFeld('SAB_MAILBETREFF',$rsSAB->FeldInhalt('SAB_MAILBETREFF'),80,100,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_MAILTEXT'].':',200);
		$Form->Erstelle_TextArea('SAB_MAILTEXT',$rsSAB->FeldInhalt('SAB_MAILTEXT'),800,99,10,$EditModus);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_MAILTEXT2'].':',200);
		$Form->Erstelle_TextArea('SAB_MAILTEXT2',$rsSAB->FeldInhalt('SAB_MAILTEXT2'),800,99,10,$EditModus);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAB']['SAB_BRIEFTEXT'].':',200);
		$Form->Erstelle_TextArea('SAB_BRIEFTEXT',$rsSAB->FeldInhalt('SAB_BRIEFTEXT'),800,99,10,$EditModus);
		$Form->ZeileEnde();
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikelBenachrichtigungen',serialize($SABParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>