<?php
/***********************************************************
* Importskript zum Verarbeiten von Serviceleistungen 
*
* Autor:   Sacha Kerres
* Version: 2012-10-23
*
************************************************************/
require_once('awisServiceleistungen.inc');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try {

    $Benutzer = 'awis_jobs';	// Standardbenutzer, wenn keiner angegeben wurde
    $DebugLevel = 0;		// Standard-Debuglevel

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            echo 'Debug auf '.$DebugLevel.' setzen...'.PHP_EOL;
                            break;
                    case '--help':
		    default:
                            echo $argv[0].PHP_EOL;
			    echo 'Importiert die Serviceleistungen aus den Filialen in AWIS'.PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo PHP_EOL;
                            die(-1);

            }
    }
    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $X = new awisServiceleistungen($Benutzer);

    $X->DebugLevel($DebugLevel);
    $X->ImportiereDaten(1);			// Import fuer Mandant 1
    die(0);
}
catch(awisException $ex)
{
    echo $ex->getSQL().PHP_EOL;
    echo $ex->getMessage().PHP_EOL;
    $Mail = new awisMailer($DB, $AWISBenutzer);
    $Mail->Absender('shuttle@de.atu.eu');
    $Mail->AdressListe(awisMailer::TYP_TO,'shuttle@de.atu.eu',awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);

    $Mail->Betreff('Serviceleistung : Fehler beim Import');
    $Mail->Text($Text,awisMailer::FORMAT_HTML,true);
    $Mail->MailSenden();

    die(-1);
}

?>

