<?php
/**
 * Bearbeitung der R�ckf�hrungen durch die Filiale bzw. Zentrale
 *
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200809291144
 */

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('LIK','%');
    $TextKonserven[]=array('LKK','%');
    $TextKonserven[]=array('LKF','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','lbl_Hilfe');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_Zeitangabe');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Wort','Status');
    $TextKonserven[]=array('Wort','AlleAnzeigen');
    $TextKonserven[]=array('Wort','Abschliessen');
    $TextKonserven[]=array('Wort','Abbrechen');
    $TextKonserven[]=array('Wort','Abgeschlossen');
    $TextKonserven[]=array('Wort','NichtAbgeschlossen');
    $TextKonserven[]=array('Wort','PDFErzeugen');
    $TextKonserven[]=array('Wort','Zwangsabschluss');

    $Form = new awisFormular(3);
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht3600 = $AWISBenutzer->HatDasRecht(3600);
    if($Recht3600==0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Param = array_fill(0,15,'');
    if(isset($_POST['cmdSuche_x']))
    {
        $Param['KEY']=0;			// Key

        $Param['LKK_FIL_ID']=$_POST['sucLKK_FIL_ID'];
        $Param['LKK_LIEFERSCHEINNR']=$_POST['sucLKK_LIEFERSCHEINNR'];
        $Param['LKK_DATUMKOMM']=$_POST['sucLKK_DATUMKOMM'];
        $Param['LKK_STATUS']=$_POST['sucLKK_STATUS'];

        $Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Params['BLOCKPOS']='';
        $Param['KEY']=0;

        $AWISBenutzer->ParameterSchreiben("LKKSuche",serialize($Param));
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params));
        $AWISBenutzer->ParameterSchreiben("AktuellerLKK",0);
    }
    elseif(isset($_POST['cmd_Abschliessen_x']))
    {
        include('./lieferkontrolle_speichern.php');
        include('./lieferkontrolle_abschliessen.php');
        $Params['BLOCKPOS']='';
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params));
    }
    elseif(isset($_POST['cmdAbschliessenOK']))
    {
        include('./lieferkontrolle_abschliessen.php');
        $Params['BLOCKPOS']='';
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params));
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('./lieferkontrolle_speichern.php');
        $Params['BLOCKPOS']='';
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params));
    }elseif (isset($_POST['cmdAdmin_x'])){
        include('./lieferkontrolle_speichern.php');
        $Params['BLOCKPOS']='';
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params));
    }
    elseif(isset($_GET['LKK_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['LKK_KEY']);
    }
    elseif(isset($_POST['txtLKK_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtLKK_KEY']);
    }
    else 		// Letzte Lieferkontrolle suchen
    {
        $Params['BLOCKPOS']='';
        $AWISBenutzer->ParameterSchreiben("LKKPos",serialize($Params['BLOCKPOS']));

        if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']) and !isset($_GET['BlockPos']))
        {
            $AWIS_KEY1 = unserialize($AWISBenutzer->ParameterLesen('AktuellerLKK' , false));

            if (intval($AWIS_KEY1) <= 0)
            {
                $UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
                if ($UserFilialen != '')
                {
                    $Param['LKK_FIL_ID']=$UserFilialen;
                    $AWISBenutzer->ParameterSchreiben("AktuellerLKK",0);
                    $AWISBenutzer->ParameterSchreiben("LKKSuche",serialize($Param));
                }
            }
        }

        if($Param=='' OR $Param=='0')
        {
            $AWISBenutzer->ParameterSchreiben("AktuellerLKK",0);
        }
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen("LKKSuche"));

    $Form->DebugAusgabe(1,$Param);

    //Puefen ob aktuell Feedbacks gesammelt werden
    $FeedbackSammlungAktiv = false;

    if($AWISBenutzer->ParameterLesen('LieferkontrollFeedback') == "1"){
        $FeedbackSammlungAktiv = true;
    }

    $FeedbackAbgegeben=false;
    //Puefen, ob Feedback bereit get�tigt wurde, falls Feedback gesammelt wird
    if($FeedbackSammlungAktiv==true){
        $SQL = "SELECT LKF_KEY FROM LIEFERKONTROLLENFEEDBACK";
        $SQL .= " WHERE LKF_LKK_KEY = ".$DB->WertSetzen('LKF', 'Z', $AWIS_KEY1);

        $rsFEEDBACK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('LKF'));
        if($rsFEEDBACK->AnzahlDatensaetze()>0){
            $FeedbackAbgegeben = true;
        }
    }


    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if(!isset($_GET['Sort']))
    {
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
        {
            $ORDERBY = $Param['ORDERBY'];
        }
        else
        {
            $ORDERBY = ' LKK_DATUMKOMM DESC';
        }
    }
    else
    {
        $ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $SQL = 'select daten.* ' ;
    if($AWIS_KEY1<=0)
    {
        $SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
    }
    $SQL .= ' from (SELECT DISTINCT LKK_KEY, LKK_LieferscheinNr, LKK_DatumKomm, LKK_FIL_ID, LKK_STATUS, LKK_USERDAT, 
			(select lik_datumabschluss from lieferkontrollen where lik_lkk_key = lkk_key and rownum = 1) as LIK_DATUMABSCHLUSS
 
			
			FROM LIEFERKONTROLLENKOPF LKK) daten ';


    if($Bedingung!='')
    {
        $SQL.= ' WHERE ' . substr($Bedingung,4);
    }

    $SQL.= ' ORDER BY '.$ORDERBY;
    $Form->DebugAusgabe(1,$SQL,$Param);

    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (!isset($_GET['Liste']))
    {
        if(isset($_REQUEST['Block']))
        {
            $Block=$Form->Format('N0',$_REQUEST['Block'],false);
        }
        elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
        {
            $Block = intval($Param['BLOCK']);
        }
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if($AWIS_KEY1<=0)
    {
        $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
    }

    $rsLKK = $DB->RecordSetOeffnen($SQL);

    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $Param['ORDERBY']=$ORDERBY;
    $Param['KEY']=$AWIS_KEY1;
    $Param['BLOCK']=$Block;
    $AWISBenutzer->ParameterSchreiben("LKKSuche",serialize($Param));

    $Form->SchreibeHTMLCode('<form name=frmLieferkontrolle action=./lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
    $Form->DebugAusgabe(1,$SQL);

    //Pr�fen ob User eine Filiale ist
    $FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
    $FilZugriffListe=explode(',',$FilZugriff);

    //Wenn eine Fililale angemeldet ist, dann gleich speichern
    if(count($FilZugriffListe)==1 and $FilZugriff != '')
    {
        $FIL_ID=$FilZugriff;
    }

    //********************************************************
    // Daten anzeigen
    //********************************************************
    if($rsLKK->EOF())		// Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
    }
    elseif(($rsLKK->AnzahlDatensaetze()>1) or (isset($_GET['Liste'])))						// Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();
        $Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=LKK_LIEFERSCHEINNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LKK_LIEFERSCHEINNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LKK']['LKK_LIEFERSCHEINNR'],200,'',$Link);

        if(count($FilZugriffListe)>1 or $FilZugriff == '')
        {
            $Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=LKK_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LKK_FIL_ID'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LKK']['LKK_FIL_ID'],100,'',$Link);
        }

        $Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=LKK_DATUMKOMM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LKK_DATUMKOMM'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LKK']['LKK_DATUMKOMM'],200,'',$Link);
        $Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=LKK_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LKK_STATUS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LKK']['LKK_STATUS'],200,'',$Link);

        $Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=LIK_DATUMABSCHLUSS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIK_DATUMABSCHLUSS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LKK']['LIK_DATUMABSCHLUSS'],120,'',$Link);

        $Form->ZeileEnde();

        $LKKZeile=0;

        while(!$rsLKK->EOF())
        {
            $Form->ZeileStart();
            $Link = './lieferkontrolle_Main.php?cmdAktion=Details&LKK_KEY='.$rsLKK->FeldInhalt('LKK_KEY');
            $Form->Erstelle_ListenFeld('LKK_LIEFERSCHEINNR',$rsLKK->FeldInhalt('LKK_LIEFERSCHEINNR'),0,200,false,($LKKZeile%2),'',$Link,'T');

            if(count($FilZugriffListe)>1 or $FilZugriff == '')
            {
                $Form->Erstelle_ListenFeld('LKK_FIL_ID',$rsLKK->FeldInhalt('LKK_FIL_ID'),0,100,false,($LKKZeile%2),'','','T');
            }

            $Form->Erstelle_ListenFeld('LKK_DATUMKOMM',$rsLKK->FeldInhalt('LKK_DATUMKOMM'),0,200,false,($LKKZeile%2),'','','D');

            $LKKStatus='';
            $StatusText = explode("|",$AWISSprachKonserven['LIK']['lst_Lieferkontrollenstatus']);
            foreach ($StatusText as $Status)
            {
                $LKStatus = explode("~",$Status);
                if ($LKStatus[0]==$rsLKK->FeldInhalt('LKK_STATUS'))
                {
                    $LKKStatus = $LKStatus[1];
                }
            }
            $Form->Erstelle_ListenFeld('LKK_STATUS',$LKKStatus,0,200,false,($LKKZeile%2),'','','T','L','');

            $BindeVariablen=array();
            $BindeVariablen['var_N0_lik_lkk_ley']=$rsLKK->FeldInhalt('LKK_KEY');

            $SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_LKK_KEY = :var_N0_lik_lkk_ley AND LIK_MENGEIST<>LIK_MENGESOLL ';
            $rsLIKDiff=$DB->RecordSetOeffnen($SQL,$BindeVariablen);
            $rsLIKDiffZeilen = $rsLIKDiff->AnzahlDatensaetze();
            $AnzahlDifferenzen=$rsLIKDiff->FeldInhalt('ANZAHLDIFF');

            $Abschlussdatum = $rsLKK->FeldInhalt('LIK_DATUMABSCHLUSS');
            if(($rsLKK->FeldInhalt('LKK_STATUS')=='A' or $rsLKK->FeldInhalt('LKK_STATUS') == 'Z') and $Abschlussdatum == ''){
                $Abschlussdatum = $rsLKK->FeldInhalt('LKK_USERDAT');
            }

            $Form->Erstelle_ListenFeld('LIK_DATUMABSCHLUSS',$Abschlussdatum,0,120,false,($LKKZeile%2),'','','D');

            if (($Recht3600 & 32)==32 AND ($rsLKK->FeldInhalt('LKK_STATUS')=='A' or $rsLKK->FeldInhalt('LKK_STATUS') == 'Z') AND $AnzahlDifferenzen>0)
            {
                $Icons[0] = array('pdf','./lieferkontrolle_pdf.php?DruckArt=2&LKK_KEY='.$rsLKK->FeldInhalt('LKK_KEY'));
                $Form->Erstelle_ListeIcons($Icons,20,($LKKZeile%2));
            }
            if (($Recht3600 & 256)==256 AND ($rsLKK->FeldInhalt('LKK_STATUS')=='A' or $rsLKK->FeldInhalt('LKK_STATUS') == 'Z' ) AND $AnzahlDifferenzen>0 )
            {
                $Icons[0] = array('excel','./lieferkontrolle_csv.php?LKK_KEY='.$rsLKK->FeldInhalt('LKK_KEY'));
                $Form->Erstelle_ListeIcons($Icons,20,($LKKZeile%2));
            }

            $Form->ZeileEnde();

            $rsLKK->DSWeiter();
            $LKKZeile++;
        }

        $Link = './lieferkontrolle_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
        $Form->SchaltflaechenEnde();
    }
    else										// Eine einzelne Lieferkontrolle
    {
        $Form->SchreibeHTMLCode('<form name=frmLieferkontrollen action=./lieferkontrolle_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsLKK->FeldInhalt('LKK_KEY');

        $AWISBenutzer->ParameterSchreiben('AktuellerLKK',serialize($AWIS_KEY1));
        $Form->Erstelle_HiddenFeld('LKK_KEY',$AWIS_KEY1);

        $Form->Formular_Start();
        $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./lieferkontrolle_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKK->FeldInhalt('LKK_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKK->FeldInhalt('LKK_USERDAT'));
        $Form->InfoZeile($Felder,'');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_LIEFERSCHEINNR'].':',180);
        $Form->Erstelle_TextFeld('LKK_LIEFERSCHEINNR',$rsLKK->FeldInhalt('LKK_LIEFERSCHEINNR'),50,300,false);
        $Form->ZeileEnde();

        if(count($FilZugriffListe)>1 or $FilZugriff == '')
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_FIL_ID'].':',180);
            $Form->Erstelle_TextFeld('LKK_LIEFERSCHEINNR',$rsLKK->FeldInhalt('LKK_FIL_ID'),50,300,false);
            $Form->ZeileEnde();
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_DATUMKOMM'].':',180);
        $Form->Erstelle_TextFeld('LKK_DATUMKOMM',($rsLKK->FeldInhalt('LKK_DATUMKOMM')),50,300,false,',,','','','D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LKK']['LKK_STATUS'].':',180);
        $StatusText = explode("|",$AWISSprachKonserven['LIK']['lst_Lieferkontrollenstatus']);
        $Form->Erstelle_SelectFeld('LKK_STATUS',$rsLKK->FeldInhalt('LKK_STATUS'),200,false,'','','','','',$StatusText,'');
        $Form->ZeileEnde();

        if($FeedbackSammlungAktiv){
            $Form->ZeileStart('');
            $Form->Hinweistext($AWISSprachKonserven['LKF']['LKF_DISCLAIMER_2'],6,'','LKF_DISCLAIMER_2');
            $Form->ZeileEnde();
        }

        $Form->Trennzeile();

        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Differenzen'));
        $RegDet = new awisRegister(3622);
        $RegDet->ZeichneRegister($RegisterSeite);

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();

        $Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

        if((($Recht3600 & 2)==2 and $rsLKK->FeldInhalt('LKK_STATUS')=='O') or ($Recht3600 & 4)==4)
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
            if($rsLKK->FeldInhalt('LKK_STATUS')=='O' and (!isset($_GET['Seite']) or (isset($_GET['Seite']) and $_GET['Seite']!='Feedback') or $FeedbackAbgegeben==true))
            {
                if($FeedbackSammlungAktiv==false or ($FeedbackSammlungAktiv==true and $FeedbackAbgegeben==true)) {
                    $Form->Schaltflaeche('image', 'cmd_Abschliessen', '', '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['LIK']['LieferscheinAbschliessen'], 'A');
                } else {
                    $Form->Erstelle_Bild('', 'cmd_Abschliessen', '', '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['LIK']['LieferscheinAbschliessen'],'',27,27);

                    $Form->SchreibeHTMLCode('
                    <script>
                    $("#cmd_Abschliessen").click(function() {
                            DisclaimerZeigen("LKF_DISCLAIMER_2");
                        }
                        );
                    </script>
                    ');

                }
            }
        }
        $Form->Schaltflaeche('href', 'cmd_DruckLieferscheinDifferenzen','./lieferkontrolle_pdf.php?DruckArt=2&LKK_KEY='.$AWIS_KEY1,'/bilder/cmd_statistik.png', $AWISSprachKonserven['LIK']['DruckLieferscheinDifferenzen'],'.');

        $Form->Schaltflaeche('href', 'cmd_DruckLieferschein','./lieferkontrolle_ls_pdf.php?LKK_KEY='.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['LIK']['DruckLieferschein'],'.');


        /*
         * Adminfunktionen
         */
        if(($Recht3600&128)==128 ){
            $Form->Schaltflaeche('script','Adminfunktionen',$Form->PopupOeffnen(2),'/bilder/cmd_schluessel.png','Adminfunktionen','','float: right',27,27,'',true);

            ob_start();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('Falsche Daten: ',120,'text-align: right; margin-right: 5px','','Daten l�schen, f�r die es keine Kommissionierung gibt.');
            $Optionen = array('~nicht l�schen','1~Mit Angabe des Scandatums l�schen','2~Ohne Datum l�schen');
            $Form->Erstelle_SelectFeld('admin_LIK_LOESCHTYP','',30,true,'','','','','',$Optionen);
            $Form->ZeileEnde();


            ?>

            <script>
                $("#txtadmin_LIK_LOESCHTYP").change(function (){
                    if($("#txtadmin_LIK_LOESCHTYP").val() == '1'){
                        $("#LOESCHDATUM").show();
                    }else{
                        $("#LOESCHDATUM").hide();
                    }
                });
            </script>
            <?php


            $Form->ZeileStart('display:none','LOESCHDATUM');
            $Form->Erstelle_TextLabel('Scandatum: ',120,'text-align: right; margin-right: 5px');
            $Form->Erstelle_TextFeld('admin_LOESCHDATUM',date('c'),10, 100, true,'','','','D');
            $Form->ZeileEnde();


            $PopUp = ob_get_clean();

            $Form->PopupDialog('Adminfunktionen',$PopUp,array(array('/bilder/cmd_dsloeschen.png','','close',''),array('/bilder/cmd_weiter.png','cmdAdmin','post','')),awisFormular::POPUP_INFO,2);
        }


        $Form->SchaltflaechenEnde();

        $Form->SchreibeHTMLCode('</form>');
    }

    if($AWISCursorPosition!='')
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
        echo '</Script>';
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031126");
    }
    else
    {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031127");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $FIL_ID;
    global $LSNR;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if(floatval($AWIS_KEY1) != 0)
    {
        //Pr�fen, ob der angemeldete Benutzer eine Filiale ist
        $UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

        // Beschr�nkung auf eine Filiale?
        if ($UserFilialen != '')
        {
            $Bedingung.= 'AND LKK_FIL_ID IN ('.$UserFilialen.') ';
        }

        $Bedingung.= 'AND LKK_KEY = '.$AWIS_KEY1;
        return $Bedingung;
    }

    if(isset($Param['LKK_FIL_ID']) AND $Param['LKK_FIL_ID']!='')
    {
        $Bedingung .= 'AND LKK_FIL_ID = ' . $DB->FeldInhaltFormat('N0',$Param['LKK_FIL_ID']) . ' ';
    }

    if(isset($Param['LKK_LIEFERSCHEINNR']) AND $Param['LKK_LIEFERSCHEINNR']!='')
    {
        $Bedingung .= 'AND LKK_LIEFERSCHEINNR = ' . $DB->FeldInhaltFormat('N0',$Param['LKK_LIEFERSCHEINNR']) . ' ';
    }

    if(isset($Param['LKK_DATUMKOMM']) AND $Param['LKK_DATUMKOMM']!='')
    {
        $Bedingung .= 'AND LKK_DATUMKOMM = ' . $DB->FeldInhaltFormat('D',$Param['LKK_DATUMKOMM']) . ' ';
    }

    if(isset($Param['LKK_STATUS']) AND $Param['LKK_STATUS']!='0')
    {
        $Bedingung .= 'AND LKK_STATUS = ' . $DB->FeldInhaltFormat('T',$Param['LKK_STATUS']) . ' ';
    }

    return $Bedingung;
}

?>