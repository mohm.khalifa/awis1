<?php
/**
 * Job_import.php
 * 
 * Mit diesem Job werden die Seminar und Seminarteilnehmer in AWIS eingelesen
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto-Teile-Unger
 * @version 20100420
 */
require_once 'awis_seminarplan.inc';
require_once('awisDatenbank.inc');

try
{
	
	$Seminarplan = new awis_Seminarplan('oppl_s');
	
	$Seminarplan->DebugLevel(10);
	
	$Seminarplan->TransaktionBegin();

	//Seminare importieren
	echo "Beginne Termin-Import" .PHP_EOL;
	$Seminarplan->Import_Termine();
	//Teilnehmer importieren
	echo "Beginne Teilnehmer-Import" .PHP_EOL;
	$Seminarplan->Import_Teilnehmer();
	//Seminarlokationen importieren
	echo "Beginne Lokations-Import" .PHP_EOL;
	$Seminarplan->Import_Lokationen();
	
	$Seminarplan->TransaktionCommit();
	
	
}
catch (Exception $ex)
{
	$Seminarplan->TransaktionRollback();
	
	awisWerkzeuge::EMail(array('shuttle@de.atu.eu'),'ERROR SEMINARPLANIMPORT','',3,'','awis@de.atu.eu');
	$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	
	
}
?>
