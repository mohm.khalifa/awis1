<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucPKS_FIL_ID']))
{
	var_dump($_GET);
	die();
}

try
{
	$DS=0;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PKS','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	
	
		
		$SQL  = ' select FEB_BEZEICHNUNG,(select FIL_BEZ from Filialen where FIL_ID = '.intval($_GET['sucPKS_FIL_ID']).') as FIL_BEZ from filialebenen where feb_key = ';
		$SQL .= ' (select distinct feb_feb_key from v_filialebenenrollen_aktuell  ';
		$SQL .= ' inner join Filialebenen on xx1_feb_key = feb_key ';
		$SQL .= ' where xx1_fil_id ='.intval($_GET['sucPKS_FIL_ID']).')';
		
		$rsFIL = $DB->RecordSetOeffnen($SQL);

		
	$Fehler = '';
	if($rsFIL->AnzahlDatensaetze() == 0)
	{
		
		$Fehler = 'keine gültige Fililale';
	}
	
	if($_GET['sucPKS_FIL_ID'] == '')
	{
		$Fehler =  $AWISSprachKonserven['PKS']['MSG_FILIALEINGABE'];
	}
		
	
	
	
	/**
	 * Standort nur lesen!
	 */
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_FILNAME'].':',200);
	$Form->Erstelle_TextFeld('PKS_FIL_NAME',$Fehler == '' ?$rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	$Form->ZeileEnde();
		
	/**
	 * Region nur lesen!
	*/
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_REGION'].':',200);
	$Form->Erstelle_TextFeld('PKS_REGION',$Fehler == '' ? $rsFIL->FeldInhalt('FEB_BEZEICHNUNG'):$Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_LL'].':',200);
	
	$Zustaendiger='';
	if($_GET['sucPKS_FIL_ID']!= '')
	{
		$SQL  = 'select ';
		$SQL .= '(select KON_NAME1||\',\'||KON_NAME2||\' \'||\'(GBL)\' as KON_NAME from v_filialebenenrollen_aktuell ';
		$SQL .= ' inner join kontakte on xx1_kon_key = kon_key';
		$SQL .= ' where xx1_fil_id ='.intval($_GET['sucPKS_FIL_ID']);
		$SQL .= ' and xx1_fer_key = 25 and rownum = 1) GBL';
		//$SQL .= '(select KON_NAME1||\',\'||KON_NAME2||\' \'||\'(LEITER LACK)\' as KON_NAME from v_filialebenenrollen_aktuell';
		//$SQL .= ' inner join kontakte on xx1_kon_key = kon_key';
		//$SQL .= ' where xx1_fil_id ='.intval($_GET['sucPKS_FIL_ID']);
		//$SQL .= ' and xx1_fer_key = 47';
		//$SQL .= ') LL from dual';
		$SQL .= ' from dual';
		
		$rsZU = $DB->RecordSetOeffnen($SQL);
	
		/**
		 * Zuständiger nur lesen!
		*/
	
		if($rsZU->FeldInhalt('LL') =='' and $rsZU->FeldInhalt('GBL') != '')
		{
			$Zustaendiger = $rsZU->FeldInhalt('GBL');
		}
		else if($rsZU->FeldInhalt('LL') !='')
		{
			$Zustaendiger = $rsZU->FeldInhalt('LL');
		}
		else
		{
				$Zustaendiger = 'Kein Zuständiger gefunden';
		}
	}
	$Form->Erstelle_TextFeld('PKS_ZUSTAENDIG',$Fehler == '' ? $Zustaendiger: $Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	$Form->ZeileEnde();

}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>