<?php

class AuftragsListe
{

    /**
     * @var int $FehlerCode
     */
    protected $FehlerCode = null;

    /**
     * @var string $FehlerText
     */
    protected $FehlerText = null;

    /**
     * @var ArrayOfPartnerBestellBestaetigungV3 $Auftraege
     */
    protected $Auftraege = null;

    /**
     * @param int $FehlerCode
     */
    public function __construct($FehlerCode)
    {
      $this->FehlerCode = $FehlerCode;
    }

    /**
     * @return int
     */
    public function getFehlerCode()
    {
      return $this->FehlerCode;
    }

    /**
     * @param int $FehlerCode
     * @return AuftragsListe
     */
    public function setFehlerCode($FehlerCode)
    {
      $this->FehlerCode = $FehlerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getFehlerText()
    {
      return $this->FehlerText;
    }

    /**
     * @param string $FehlerText
     * @return AuftragsListe
     */
    public function setFehlerText($FehlerText)
    {
      $this->FehlerText = $FehlerText;
      return $this;
    }

    /**
     * @return ArrayOfPartnerBestellBestaetigungV3
     */
    public function getAuftraege()
    {
      return $this->Auftraege;
    }

    /**
     * @param ArrayOfPartnerBestellBestaetigungV3 $Auftraege
     * @return AuftragsListe
     */
    public function setAuftraege($Auftraege)
    {
      $this->Auftraege = $Auftraege;
      return $this;
    }

}
