<?php

class ArrayOfBestandsabfrageV3 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BestandsabfrageV3[] $BestandsabfrageV3
     */
    protected $BestandsabfrageV3 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BestandsabfrageV3[]
     */
    public function getBestandsabfrageV3()
    {
      return $this->BestandsabfrageV3;
    }

    /**
     * @param BestandsabfrageV3[] $BestandsabfrageV3
     * @return ArrayOfBestandsabfrageV3
     */
    public function setBestandsabfrageV3(array $BestandsabfrageV3 = null)
    {
      $this->BestandsabfrageV3 = $BestandsabfrageV3;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BestandsabfrageV3[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BestandsabfrageV3
     */
    public function offsetGet($offset)
    {
      return $this->BestandsabfrageV3[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BestandsabfrageV3 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BestandsabfrageV3[] = $value;
      } else {
        $this->BestandsabfrageV3[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BestandsabfrageV3[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BestandsabfrageV3 Return the current element
     */
    public function current()
    {
      return current($this->BestandsabfrageV3);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BestandsabfrageV3);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BestandsabfrageV3);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BestandsabfrageV3);
    }

    /**
     * Countable implementation
     *
     * @return BestandsabfrageV3 Return count of elements
     */
    public function count()
    {
      return count($this->BestandsabfrageV3);
    }

}
