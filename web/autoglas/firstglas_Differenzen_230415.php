<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
include_once('awisFirstglasImport.inc.php');

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $TextKonserven = array();
    $TextKonserven[] = array('FFR', '%');
    $TextKonserven[] = array('FIL', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsscheinDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsdifferenzenDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungslisteDrucken');
    $TextKonserven[] = array('Wort', 'KeineBerechtigungRueckfuehrung');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[] = array('Wort', 'Abschlussdatum');
    $TextKonserven[] = array('RFS', 'lst_RFS_STATUS');
    $TextKonserven[] = array('Wort', 'Text');
    $TextKonserven[] = array('Wort', 'FFR_UNBEARBEITET');
    $TextKonserven[] = array('Wort', 'FFR_BEARBEITET');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if ($Recht9100 == 0)
    {
        awisEreignis(3, 1000, 'KEW', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FFR'));
    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $ListenSchriftFaktor = (int) ((($ListenSchriftGroesse == 0 ? 12 : $ListenSchriftGroesse) / 12) * 9);

    $DetailAnsicht = false;

    //$Form->DebugAusgabe(1,$_POST);
    $Form->Formular_Start();
    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./autoglas_Main.php?cmdAktion=Differenzen>");

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'] . ':', 240);
    $Form->Erstelle_TextFeld('*FFR_FILID', '', 10, 180, true, '', '', '', 'T', 'L', '', '', 10);
    $AWISCursorPosition = 'sucFFR_FILID';
    $Form->ZeileEnde();
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 240);
    $Form->Erstelle_TextFeld('*FFR_VORGANGNR', '', 13, 180, true, '', '', '', 'T', 'L', '', '', 13);
    $Form->ZeileEnde();

    $Form->Formular_Ende();


    if (isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['FFR_FILID'] = $_POST['sucFFR_FILID'];
        $Param['FFR_VORGANGNR'] = $_POST['sucFFR_VORGANGNR'];

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern']) ? 'on' : '';

        $AWISBenutzer->ParameterSchreiben("Formular_FFR", serialize($Param));
    }
    elseif (isset($_POST['cmdSpeichern_x']))
    {
        include './firstglas_rueckfuehrung_speichern.php';
    }
    elseif (isset($_GET['FFR_KEY']))
    {
        $AWIS_KEY1 = $Form->Format('N0', $_GET['FFR_KEY']);
        $Param['KEY'] = ($AWIS_KEY1 < 0 ? '' : $AWIS_KEY1);
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $AWISBenutzer->ParameterSchreiben('Formular_FFR', serialize($Param));
    }
    else
    {
        if (!isset($Param['KEY']))
        {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
            $AWISBenutzer->ParameterSchreiben('Formular_FFR', serialize($Param));
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
        {
            $Param['KEY'] = 0;
        }
        $AWIS_KEY1 = $Param['KEY'];
    }


    if (!isset($_GET['Sort']))
    {
        if ($Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' ORDER BY FFR_FILID';
        }
    }
    else
    {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    $Param['ORDER'] = $ORDERBY;

    $Bedingung = _BedingungErstellen($Param);
    $Bedingung .= " AND FFR_STATUS < 2";

    $SQL = 'SELECT FGRUECKFUEHRUNG.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM FGRUECKFUEHRUNG';


    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }



    //$Form->DebugAusgabe(1,$SQL,$Param);

    if ($AWIS_KEY1 <= 0)
    {
        $Block = 1;
        if (isset($_REQUEST['Block']))
        {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
            $AWISBenutzer->ParameterSchreiben('Formular_FFR', serialize($Param));
        }
        elseif (isset($Param['BLOCK']))
        {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    else
    {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;

    //echo $SQL;

    $rsFFR = $DB->RecordsetOeffnen($SQL);
    $AWISBenutzer->ParameterSchreiben('Formular_FFR', serialize($Param));

    //$DebugAusgabe(1,$rsFFR);

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./autoglas_Main.php?cmdAktion=Differenzen>");
    //echo '<form name=frmkennwoerter action=./firstglas_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

    if ($rsFFR->EOF() AND $AWIS_KEY1 != -1)  // Keine Meldung bei neuen Datensätzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    }
    elseif ($rsFFR->AnzahlDatensaetze() > 1 or isset($_GET['Liste']))      // Liste anzeigen
    {
        $DetailAnsicht = false;
        $Form->Formular_Start();

        $Form->ZeileStart($ListenSchriftGroesse == 0 ? '' : 'font-size:' . intval($ListenSchriftGroesse) . 'pt');
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_FILID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_FILID')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_FILID'], 120, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_WANR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_WANR')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_WANR'], 120, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_VORGANGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_VORGANGNR')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_VORGANGNR'], 150, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_AEMBETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_AEMBETRAG')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'], 120, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_KASSENBETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_KASSENBETRAG')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'], 130, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_DIFFERENZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_DIFFERENZ')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'], 120, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_DIFFERENZDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_DIFFERENZDATUM')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'], 200, '', $Link);
        $Link = './autoglas_Main.php?cmdAktion=Differenzen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=FFR_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_STATUS')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_STATUS'], 200, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsFFR->EOF())
        {
            $Form->ZeileStart();
            $Link = './autoglas_Main.php?cmdAktion=Differenzen&FFR_KEY=0' . $rsFFR->FeldInhalt('FFR_KEY') . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
            $Form->Erstelle_ListenFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 0, 120, false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 0, 120, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 0, 150, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 0, 120, false, ($DS % 2), '', '', 'N2');
            $Form->Erstelle_ListenFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 0, 130, false, ($DS % 2), '', '', 'N2');
            $Form->Erstelle_ListenFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 0, 120, false, ($DS % 2), '', '', 'N2');
            $Form->Erstelle_ListenFeld('FFR_DIFFERENZDATUM', $rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'), 0, 200, false, ($DS % 2), '', '', 'DU');

            $Status = "";

            if ($rsFFR->FeldInhalt('FFR_STATUS', 'NO') == 0)
            {
                $Status = "unbearbeitet";
                $Form->Erstelle_ListenFeld('FFR_STATUS', $Status, 0, 200, false, ($DS % 2), 'color:FF4500', '', 'T');
            }
            elseif ($rsFFR->FeldInhalt('FFR_STATUS', 'NO') == 1)
            {
                $Status = "bearbeitet";
                $Form->Erstelle_ListenFeld('FFR_STATUS', $Status, 0, 200, false, ($DS % 2), 'color:FFFF00', '', 'T');
            }
            elseif ($rsFFR->FeldInhalt('FFR_STATUS', 'NO') == 2)
            {
                $Status = "Erledigt";
                $Form->Erstelle_ListenFeld('FFR_STATUS', $Status, 0, 200, false, ($DS % 2), 'color:00FF00', '', 'T');
            }
            elseif ($rsFFR->FeldInhalt('FFR_STATUS', 'NO') == 3)
            {
                $Status = "Erledigt";
                $Form->Erstelle_ListenFeld('FFR_STATUS', $Status, 0, 200, false, ($DS % 2), 'color:00FF00', '', 'T');
            }


            $Form->ZeileEnde();

            $rsFFR->DSWeiter();
            $DS++;
        }

        $Link = './autoglas_Main.php?cmdAktion=Differenzen';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    }
    else
    {
        $DetailAnsicht = true;
        $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');

        $Param['KEY'] = $AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben('Formular_FFR', serialize($Param));

        echo '<input type=hidden name=txtFFR_KEY value=' . $AWIS_KEY1 . '>';

        $Form->Formular_Start();
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Differenzen&Liste=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISBenutzer->BenutzerName());
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', date('Ymd H:i:s')));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KEY'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'), 20, 50, false);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 10, 200, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 5, 100, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 50, 200, false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_KFZKENNZ', $rsFFR->FeldInhalt('FFR_KFZKENNZ'), 10, 200, false);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 7, 100, false, '', '', '', 'N2');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 30, 100, false, '', '', '', 'N2');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 30, 100, false, '', '', '', 'N2');
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'] . ':', 150);
        $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'), 100, 200, false, '', '', '', 'DU');
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORDEFTEXT'] . ':', 150);
        $Form->Erstelle_TextFeld("Begruendung", $rsFFR->FeldInhalt('FFR_VORDEFTEXT'), 400, 400);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('Begründung', 250);
        $Form->Erstelle_Textarea('FFR_BEGRUENDUNG', $rsFFR->FeldInhalt('FFR_BEGRUENDUNG'), 1000, 90, 4, True);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $Form->SchaltflaechenStart();

        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht9100 & 4) == 4)
        {
            $Form->Erstelle_HiddenFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'));
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }

        $Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/','','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
        $Form->SchaltflaechenEnde();
        $Form->SchreibeHTMLCode('</form>');
        $Form->Formular_Ende();
    }

    if ($DetailAnsicht == true)
    {
        
    }
    else
    {
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
        $Form->SchaltflaechenEnde();
    }
    $Form->SchreibeHTMLCode('</form>');
}
catch (Exception $e)
{
    
}

function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $DB;
    global $AWISBenutzer;

    $Bedingung = '';

    if ($AWIS_KEY1 != 0)
    {
        $Bedingung.= ' AND FFR_KEY = ' . floatval($AWIS_KEY1);
        return $Bedingung;
    }

    if (isset($Param['FFR_FILID']) AND $Param['FFR_FILID'] != '')
    {
        $Bedingung .= ' AND FFR_FILID =' . $DB->FeldInhaltFormat('NO', $Param['FFR_FILID']);
    }

    if (isset($Param['FFR_VORGANGNR']) AND $Param['FFR_VORGANGNR'] != '')
    {
        $Bedingung .= ' AND FFR_VORGANGNR =' . $DB->FeldInhaltFormat('T', $Param['FFR_VORGANGNR']);
    }


    //$Bedinung .= ' AND FFR_STATUS < 2';


    return $Bedingung;
}

?>
