<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('WVL','%');
	$TextKonserven[]=array('RST','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

$Form->DebugAusgabe(1,$_POST);
$Form->DebugAusgabe(1,$_GET);

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15300 = $AWISBenutzer->HatDasRecht(15300);
	if(($Recht15300)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$WVLParam = unserialize($AWISBenutzer->ParameterLesen('Formular_RST_Wiedervorlagen'));
	
	if(!isset($WVLParam['BLOCK']))
	{
		$WVLParam['BLOCK']=1;
		$WVLParam['KEY']='';
		$WVLParam['RST_KEY']=$AWIS_KEY1;
	}
	
	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY WVL_DATUM ASC';
		$WVLParam['ORDER']=$ORDERBY;
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$ORDERBY = '';
		foreach($SortFelder AS $SortFeld)
		{
			$ORDERBY .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($ORDERBY==''?'':' ORDER BY '.$ORDERBY);
		$WVLParam['ORDER']=$ORDERBY;
	}
	
	$SQL = 'SELECT';
	$SQL .= ' WVL_KEY,';
	$SQL .= ' WVL_DATUM,';
	$SQL .= ' WVL_TEXT,';
	$SQL .= ' WVL_BEARBEITER,';
	$SQL .= ' WVL_TYP,';
	$SQL .= ' WVL_KENNUNG,';
	$SQL .= ' WVL_STATUS,';
	$SQL .= ' WVL_USER,';
	$SQL .= ' WVL_USERDAT,';
	$SQL .= ' row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM WIEDERVORLAGEN ';
	$SQL .= ' WHERE WVL_XXX_KEY=0'.$AWIS_KEY1;
	$SQL .= " AND WVL_XTN_KUERZEL='RST'";
	//$SQL .= ' AND TRUNC(WVL_DATUM)<=TRUNC(SYSDATE)';
	
	if(isset($_GET['WVL_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['WVL_KEY']);
$Form->DebugAusgabe(1,$AWIS_KEY2);
	}
	elseif($AWIS_KEY2=='')
	{
		if($WVLParam['RST_KEY']==$AWIS_KEY1 and !isset($_GET["WVLListe"]))
		{
			$AWIS_KEY2 = $WVLParam['KEY'];
		}
		else
		{
			$RSTParam['BLOCK']=1;
			$RSTParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}
	
	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND WVL_KEY = '.$AWIS_KEY2;
		$_GET['WVL_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}
	
	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$RSTParam['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$RSTParam['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$Form->DebugAusgabe(1,$SQL);
	$rsWVL = $DB->RecordsetOeffnen($SQL);

    $Form->Formular_Start();

	if($rsWVL->AnzahlDatensaetze()>1 OR isset($_GET['WVLListe']) OR !isset($_GET['WVL_KEY']))
	{
		$Form->ZeileStart();

		$Icons=array();
		if((intval($Recht15300)&6)!=0)
		{
			$Icons[] = array('new','./reifenstamm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&RST_KEY='.$AWIS_KEY1.'&WVL_KEY=-1');
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WVL_DATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WVL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_DATUM'],120,'',$Link);
		
  		$Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WVL_TEXT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WVL_TEXT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_TEXT'],550,'',$Link);
		
		$Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WVL_BEARBEITER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WVL_BEARBEITER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_BEARBEITER'],100,'',$Link);
		
		$Link = './reifenstamm_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WVL_TYP'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WVL_TYP'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_TYP'],100,'',$Link);
		
		$Form->ZeileEnde();

		// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$WVLParam['RST_KEY']=$AWIS_KEY1;
		$WVLParam['KEY']='';
		
		$DS=0;
		while(!$rsWVL->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht15300&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./reifenstamm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&RST_KEY='.$AWIS_KEY1.'&WVL_KEY='.$rsWVL->FeldInhalt('WVL_KEY'));
			}
			if(intval($Recht15300&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./reifenstamm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&RST_KEY='.$AWIS_KEY1.'&Del='.$rsWVL->FeldInhalt('WVL_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			
			$Form->Erstelle_ListenFeld('*WVL_DATUM',$rsWVL->FeldInhalt('WVL_DATUM'),10,120,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('*WVL_TEXT',$rsWVL->FeldInhalt('WVL_TEXT'),80,550,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*WVL_BEARBEITER',$rsWVL->FeldInhalt('WVL_BEARBEITER'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*WVL_TYP',$rsWVL->FeldInhalt('WVL_TYP'),20,100,false,($DS%2),'','','T');
			$Form->ZeileEnde();

			$rsWVL->DSWeiter();
			$DS++;
		}

		$Link = './reifenstamm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsWVL->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditRecht = ($Recht15300&6);
		$AWIS_KEY2 = $rsWVL->FeldInhalt('WVL_KEY');
		$WVLParam['KEY']=$AWIS_KEY2;
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		$Form->Erstelle_HiddenFeld('WVL_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('WVL_XXX_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./reifenstamm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&WVLListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsWVL->FeldInhalt('WVL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsWVL->FeldInhalt('WVL_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WVL']['WVL_DATUM'].':',150);
		$Form->Erstelle_TextFeld('!WVL_DATUM',$rsWVL->FeldInhalt('WVL_DATUM'),10,120,$EditRecht,'','','','D','','','',10);
		$Form->ZeileEnde();

	   	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['WVL']['WVL_TEXT'].':',150);
       	$Form->Erstelle_TextArea('WVL_TEXT',$rsWVL->FeldInhalt('WVL_TEXT'),800,103,5,$EditRecht);
       	$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	
	$AWISBenutzer->ParameterSchreiben('Formular_RST_Wiedervorlagen',serialize($WVLParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306120921");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306120922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>