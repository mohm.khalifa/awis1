<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class seminarplan_Emails_funktionen
{
    /**
     * @var awisFormular
     */
    public $Form;

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var awisBenutzer
     */
    public $AWISBenutzer;

    /**
     * Bitte w�hlen f�r Selectfelder
     *
     * @var string
     */
    public $OptionBitteWaehlen;

    /**
     * Recht der Maske
     *
     * @var int
     */
    public $Recht4207;

    /**
     * Edit Recht der Maske
     *
     * @var bool
     */
    public $EditRecht;

    /**
     * Textkonserven
     *
     * @var array
     */
    public $AWISSprachKonserven;

    /**
     * Parameter in der Datenbank
     * @var array
     */
    public $Param;

    /**
     * Cursorposition
     *
     * @var string
     */
    public $AWISCursorPosition;

    /**
     * aktueller Key
     *
     * @var int
     */
    public $AWIS_KEY1;


    /**
     * seminarplan_Emails_funktionen constructor.
     */
    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht4207 = $this->AWISBenutzer->HatDasRecht(4207);
        $this->EditRecht = (($this->Recht4207 & 2) != 0);

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('SEP', '%');
        $TextKonserven[] = array('MVT', '%');
        $TextKonserven[] = array('Wort', 'ttt_PositionHinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('TITEL', 'tit_Gutscheinaktion');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    /**
     * Setzt auch den Cursor
     */
    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }


    /**
     * Gibt eine Meldug aus, wenn der User keine Rechte hat
     *
     * @param int $Bit
     */
    public function RechteMeldung($Bit = 1)
    {
        if (($this->Recht4207 & $Bit) !== $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'GUT'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    /**
     * Wird �ber Ajax aufgerufen, generiert eine Vorschau f�r den Emailtext.
     *
     * @param $Bereich
     */
    public function EmailVorschau($Bereich)
    {

        $SQL = 'Select MVT_TEXT, MVT_BETREFF from Mailversandtexte where MVT_BEREICH = ' . $this->DB->WertSetzen('MVT', 'T', $Bereich);
        $rsMVT = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('MVT'));

        if ($rsMVT->FeldInhalt(1) != '') {
            $this->Form->Hinweistext($rsMVT->FeldInhalt('MVT_TEXT'), awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
        }
    }


}