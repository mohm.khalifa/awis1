<?php
	global $con;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $BearbeitungsStufe;
	global $AWISBenutzer;

	$Recht400 = awisBenutzerRecht($con,400);
	
	// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	
	$SQL = "select * from v_ekat_artikelstamminfos, v_ekat_artikelstamminfotypen  ";
	$SQL .= " WHERE ASI_AIT_ID = AIT_ID AND ASI_AST_ATUNR='" . $Artikel[1] . "'";
	$SQL .= " ORDER BY ASI_SORTIERUNG, ASI_WERT";
//print "<hr>$SQL<hr>";

	$rsEKAT = awisOpenRecordset($con, $SQL);
	if($rsEKAT==FALSE)
	{
		awisErrorMailLink("artikel_EKATn.php", 2, $awisDBFehler['message']);
	}
	$rsEKATZeilen=$awisRSZeilen;

	print "<table id=DatenTabelle width=600 border=1>";
//	print "<cols><col width=400></col><col width=200></col></cols>";
	print "<tr>";
	print "<td id=FeldBez>EKAT-Info</td>";
	print "<td id=FeldBez>Informationswert</td>";
	print "</tr>";

	//**************************************
	// EKAT Infos
	//**************************************

	$AnzInfos=0;
	for($Zeile=0;$Zeile<$rsEKATZeilen;$Zeile++)
	{
		if($rsEKAT['AIT_ID'][$Zeile]<9999990)		//Sonderinfos nicht hier
		{
			echo '<tr>';

			echo '<td>'	. $rsEKAT['AIT_INFORMATION'][$Zeile] . '</td>';
			echo '<td align=right>'	. $rsEKAT['ASI_WERT'][$Zeile] . '</td>';

			echo '</tr>';
			$AnzInfos++;
		}
	}
	if($AnzInfos==0)
	{
		echo '<tr><td colspan=2 align=center><font size=2>Keine EKAT-Infos verf�gbar.</font></td></tr>';
	}

	//**************************************
	// EKAT-Zusatztext
	//**************************************

	$SQL = "select TEXT FROM ARTIKEL_TEXTE ";
	$SQL .= " WHERE ATU_NR='" . $Artikel[1] . "'";
	$SQL .= " ";

	$rsZusatztext = awisOpenRecordset($con, $SQL);
	if($rsZusatztext==FALSE)
	{
		awisErrorMailLink("artikel_EKAT.php", 2, $awisDBFehler['message']);
	}
	$rsZusatztextZeilen=$awisRSZeilen;

	print "<table id=DatenTabelle width=600 border=1>";
	//print "<cols><col width=60><col width=400></cols>";
	print "<tr>";
	print "<td id=FeldBez align=center>EKAT-Zusatztext</td>";
	print "</tr>";

	$AnzInfos=0;
	for($Zeile=0;$Zeile<$rsZusatztextZeilen;$Zeile++)
	{
		echo '<tr>';
		echo '<td><textarea cols=94 rows=5>' . $rsZusatztext['TEXT'][$Zeile] . '</textarea></td>';
		echo '</tr>';
		$AnzInfos++;
	}

	if($AnzInfos==0)
	{
		echo '<tr><td colspan=2 align=center><font size=2>Kein EKAT-Zusatztext verf�gbar.</font></td></tr>';
	}
	
	unset($rsAWS);
	
	//**************************************
	// Anwendungsstellen
	//**************************************

	$SQL = "select DISTINCT AWS_ID, AWS_BEZEICHNUNG from V_EKAT_ANWENDUNGSSTELLEN, V_EKAT_AST_ANWENDUNGSSTELLEN  ";
	$SQL .= " WHERE AWS_ID = AAS_AWS_ID AND AAS_AST_ATUNR='" . $Artikel[1] . "'";
	$SQL .= " ";

	$rsAWS = awisOpenRecordset($con, $SQL);
	if($rsAWS==FALSE)
	{
		awisErrorMailLink("artikel_EKATn.php", 2, $awisDBFehler['message']);
	}
	$rsAWSZeilen=$awisRSZeilen;

	print "<table id=DatenTabelle width=600 border=1>";
	print "<cols><col width=60><col width=400></cols>";
	print "<tr>";
	print "<td id=FeldBez align=center>Nr</td>";
	print "<td id=FeldBez align=center>Anwendungstellen</td>";
	print "</tr>";

	$AnzInfos=0;
	for($Zeile=0;$Zeile<$rsAWSZeilen;$Zeile++)
	{
		echo '<tr>';
		echo '<td>' . $rsAWS['AWS_ID'][$Zeile] . '</td>';
		echo '<td>' . $rsAWS['AWS_BEZEICHNUNG'][$Zeile] . '</td>';
		echo '</tr>';
	}

	unset($rsAWS);

	//**************************************
	// Bilder
	//**************************************
	
	echo '<tr><td colspan=2 align=center id=FeldBez>Abbildung</td></tr>';
	
	echo '<tr><td colspan=2 align=center>';
		$SQL="SELECT LAENGE, HOEHE FROM BILDER_INFO WHERE ATU_NR='".$Artikel[1]."'";
		$rsBildInfo=awisOpenRecordset($con,$SQL);
		$rsBildInfoZeilen=$awisRSZeilen;
		
		If($rsBildInfoZeilen>0){
			$BildHoehe=$rsBildInfo['HOEHE'][0];
			$BildBreite=$rsBildInfo['LAENGE'][0];
		}else{
			$BildHoehe=300;
			$BildBreite=400;
		}
		
		$src="artikel_Bild.php?Bild='". $Artikel[1] ."'&Breite=".$BildBreite."&Hoehe=".$BildHoehe;
		echo "<iframe frameborder='image/png' height='".$BildHoehe."px' width='".$BildBreite."px' src=".$src." marginheight='0' marginwidth='0' scrolling='No'></iframe>";
	echo '</td>';
    echo '</tr>';

	//**************************************
	// St�cklisten-Infos
	//**************************************
	$AnzInfos=0;
	print "<tr>";
	print "<td colspan=2 id=FeldBez align=center>St�ckliste</td>";
	print "</tr>";
	echo '<tr><td colspan=2>';
	for($Zeile=0;$Zeile<$rsEKATZeilen;$Zeile++)
	{
		if($rsEKAT['AIT_ID'][$Zeile]==9999998)		//St�cklistenInfos hier
		{
			echo ($AnzInfos++==0?'':' | ')	. $rsEKAT['ASI_WERT'][$Zeile] . '';
		}
	}
	echo '</td></tr>';

	if($AnzInfos==0)
	{
		echo '<tr><td colspan=2 align=center><font size=2>Artikel ist auf keiner St�ckliste enthalten.</font></td></tr>';
	}

	print "</table>";

	unset($rsEKAT);

	//*******************************************
	// Anwendungen in den Fahrzeugen
	//*******************************************
		
	if(($Recht400&64)==64)
	{
		$SQL = "SELECT DISTINCT KFZ_KBANR.KBANRC AS KBANR, KFZ_HERST.BEZ AS HERSTELLER, KFZ_MODELL.BEZ AS MODELL,
					KFZ_TYP.BEZ AS TYP, KFZ_MODELL.BJVON AS BJVON, KFZ_MODELL.BJBIS AS BJBIS, KFZ_HERST.KHERNR,
					KFZ_SNAP_ARTIKELVKN4.ANZ AS zulassungen, ARTIKEL_KTYPNR_AWST.AWSTNR as  baugruppe,
					KFZ_TYP.KTYPNR AS KTYPNR
				FROM ARTIKEL_KTYPNR_AWST
					LEFT JOIN KFZ_TYP ON ARTIKEL_KTYPNR_AWST.KTYPNR=KFZ_TYP.KTYPNR
					LEFT OUTER JOIN KFZ_KBANR ON KFZ_TYP.KTYPNR = KFZ_KBANR.KTYPNR
					LEFT JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR
					LEFT JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR
					LEFT OUTER JOIN KFZ_SNAP_ARTIKELVKN4 ON (KFZ_SNAP_ARTIKELVKN4.HER || KFZ_SNAP_ARTIKELVKN4.TYP)= KFZ_KBANR.KBANRC
				WHERE ARTIKEL_KTYPNR_AWST.ATU_NR = '" . $Artikel[1] . "'";
		$SQL .= "ORDER BY HERSTELLER, MODELL, TYP";
	
		$rsAWS = awisOpenRecordset($con, $SQL);
		if($rsAWS==FALSE)
		{
			awisErrorMailLink("artikel_EKATn.php", 2, $awisDBFehler['message']);
		}
		$rsAWSZeilen=$awisRSZeilen;
	
		print "<br><table id=DatenTabelle width=100% border=1>";
		print "<tr><td align=center id=FeldBez colspan=8>Zugeordnete Fahrzeuge (".$rsAWSZeilen.")</td></tr>";
		print "<tr>";
		print "<td id=FeldBez align=center>Hersteller</td>";
		print "<td id=FeldBez align=center>Modell</td>";
		print "<td id=FeldBez align=center>Typ</td>";
		print "<td id=FeldBez align=center>BJ von</td>";
		print "<td id=FeldBez align=center>BJ bis</td>";
		print "<td id=FeldBez align=center>Zulassungen</td>";
		print "<td id=FeldBez align=center>BauGrp</td>";
		print "<td id=FeldBez align=center>KTYP-Nr</td>";
		print "</tr>";
	
		$AnzZulassungen=0;
		for($Zeile=0;$Zeile<$rsAWSZeilen;$Zeile++)
		{
			echo '<tr>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['HERSTELLER'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['MODELL'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['TYP'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['BJVON'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['BJBIS'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .' align=right>' . number_format($rsAWS['ZULASSUNGEN'][$Zeile],0,'','.') . '</td>';
			$AnzZulassungen+=$rsAWS['ZULASSUNGEN'][$Zeile];
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['BAUGRUPPE'][$Zeile] . '</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") .'>' . $rsAWS['KTYPNR'][$Zeile] . '</td>';
			echo '</tr>';
		}
		echo '<tr><td id=FeldBez align=right colspan=5>Zulassungen gesamt:</td><td align=right>' . number_format($AnzZulassungen,0,'','.') . '</td><td id=FeldBez colspan=2></td></tr>';
		echo '</table>';
		unset($rsAWS);
	}
	
?> 