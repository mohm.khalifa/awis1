<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SCH', '%');
    $TextKonserven[]=array('Wort', 'lbl_zurueck');
    $TextKonserven[]=array('Wort', 'lbl_suche');
    $TextKonserven[]=array('Wort', 'lbl_trefferliste');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht25040=$AWISBenutzer->HatDasRecht(25040);
    if($Recht25040 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
    if(isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['KEY']=0;			

        $Param['SCH_Key']=$_POST['sucSCH_Key'];	
        $Param['TUR_Key']=$_POST['sucTUR_Key'];	
        $Param['KON_Key']=$_POST['sucKON_Key'];	
        
        $Param['SCH_Historie']=(isset($_POST['sucSCH_Historie'])?'on':'');
        $Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Param['KEY']=0;	
        
        $AWISBenutzer->ParameterSchreiben("Formular_SCH",serialize($Param));
    }
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_SCH"));
    //$MAS_Key = $_GET['MAS_KEY'];
    if(!isset($_GET['Sort']))
    {        
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
        {
                $ORDERBY = $Param['ORDERBY'];
        }
        else
        {
            if (isset($Param['KON_Key']))
            {
                $ORDERBY = ' SCH_Bezeichnung';
            }
            elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
            {
                $ORDERBY = ' KON_Name1';
            }
        }		
    }
    else
    {
        $ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
    }
    
    
    if ($Param['KON_Key']!=0)
    {
        $SQL = 'SELECT DISTINCT sch.SCH_Bezeichnung as BEZ, sgr.SGR_Bezeichnung, tur.TUR_Bezeichnung, ';
        $SQL.= '\'Bauteil \' ||tur.TUR_Bauteil || \', Stockwerk \' || tur.TUR_Stockwerk || \', B�ro \' || tur.TUR_Raum, mas.MAS_Ausgabedatum';
        if ($Param['SCH_Historie']== 'on')
        {
            $SQL.= ', mas.MAS_Rueckgabedatum, mas.MAS_Verlustdatum';
        }
        $SQL.= ', mas.MAS_Key';
        $SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
        $SQL.= ' FROM SchlSchluessel sch';
        $SQL.= ' Join SchlSchluesselgruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key';
        $SQL.= ' Join SchlTueren tur on sch.SCH_TUR_Key = tur.TUR_Key';
        $SQL.= ' Join SchlMitarbeiterSchluessel mas on mas.MAS_SCH_Key = sch.SCH_Key';
        $SQL.= ' Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key';
        $SQL.= ' WHERE MAS_KON_Key = '.($Param['KON_Key']);
        if (!$Param['SCH_Historie']== 'on')
        {
            $SQL.=' AND mas.MAS_Verlustdatum is null ' ;
            $SQL.=' AND mas.MAS_Rueckgabedatum is null ';
        }
    }
    elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
    {
        $SQL = 'SELECT kon.KON_NAME1, kon.KON_NAME2, mas.MAS_Ausgabedatum, kab.KAB_Abteilung, mas.MAS_Bemerkung, kko.KKO_WERT';
        if ($Param['SCH_Historie']== 'on')
        {
            $SQL.= ', mas.MAS_Rueckgabedatum, mas.MAS_Verlustdatum ';
        }
        $SQL.= ', mas.MAS_Key';
        $SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
        $SQL.= ' FROM SchlMitarbeiterSchluessel mas Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key';
        $SQL.= ' Join SchlSchluessel sch on mas.MAS_SCH_Key = sch.SCH_Key';
        $SQL.= ' Join SchlSchluesselGruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key';
        $SQL.= ' Join SchlTueren tur on sch.SCH_TUR_Key = tur.Tur_Key';
        $SQL.= ' Join KontakteAbteilungenZuordnungen kza on kza.KZA_KON_Key = kon.KON_Key';
        $SQL.= ' Join KontakteAbteilungen kab on kza.KZA_KAB_KEY = kab.KAB_Key';
        $SQL.= ' Join Kontaktekommunikation kko on kon.KON_Key = kko.KKO_KON_Key';
        $SQL.= ' WHERE KZA_KAB_Key != 55';
        $SQL.= ' AND kko.KKO_KOT_Key = 9';
        if (!$Param['SCH_Historie']== 'on')
        {
            $SQL.=' AND mas.MAS_Verlustdatum is null ' ;
            $SQL.=' AND mas.MAS_Rueckgabedatum is null ';
        }
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQL.= ' AND mas.MAS_SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQL.= ' AND sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        }
    }
    
    // AWIS-Framework Seitenumschalterei bei Listen - Standardvorgehensweise
    $Block = 1;
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    $SQL = 'SELECT * FROM ('.$SQL.') WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr <'.($StartZeile+$ZeilenProSeite);

    $rsPEI  = $DB->RecordSetOeffnen($SQL);

    $Form->Formular_Start();

    // Zum Bl�ttern in den Daten
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }

    if($Param['KON_Key']!=0)
    {
        $BreiteNr = 120;
        $BreiteSGR = 120;
        $BreiteTuer = 100;
        $BreiteBem = 250;
        $BreiteDatum = 100;
        
        //Listen�berschriften erstellen
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('', 20, '', '');
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=SCH_Bezeichnung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SCH_Bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_SchluesselNr'], $BreiteNr, '', $Link);
        $Link = './schluesselzentrale_Main.php?ccmdAktion=Details&Seite=Listenansicht&Sort=SGR_Bezeichnung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SGR_Bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Schliessgruppe'], $BreiteSGR, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=TUR_Bezeichnung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TUR_Bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Tuer'], $BreiteTuer, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=TUR_Bezeichnung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TUR_Bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'], $BreiteBem, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Ausgabedatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Ausgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Ausgabedatum'], $BreiteDatum, '', $Link);
        if($Param['SCH_Historie']== 'on') 
        {
            $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Rueckgabedatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Rueckgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Rueckgabedatum'], $BreiteDatum, '', $Link);
            $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Verlustdatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Rueckgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Verlustdatum'], $BreiteDatum, '', $Link);
        }
        $Form->ZeileEnde();

        $DS = 0;	// f�r Hintergrundfarbumschaltung
        
        //Listeneintr�ge erstellen
        while(!$rsPEI->EOF())
        {        
            $Form->ZeileStart();
            //Hier den MAS_Key je Zeile wegspeichern!!
            if ($Param['SCH_Historie']== 'on')
            {
                $Param['MAS_Key']= $rsPEI->FeldInhalt('8');
            }
            elseif ($Param['SCH_Historie']!= 'on')
            {
                $Param['MAS_Key']= $rsPEI->FeldInhalt('6');
            }
            if($Recht25040!=0)	// Kein �ndernrecht
            {
                $Icons = array();
                if($Recht25040)
                {
                        $Icons[] = array('edit','./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Bearbeiten&MAS_KEY='.$Param['MAS_Key'], 'g',$AWISSprachKonserven['SCH']['SCH_ttt_Bearbeiten']);
                }
                $Form->Erstelle_ListeIcons($Icons,20,-1);
            }
            $Form->Erstelle_ListenFeld('SchluesselNr', $rsPEI->FeldInhalt('1'), 0, $BreiteNr, false, ($DS%2), '', '', 'T', 'L' );
            $Form->Erstelle_ListenFeld('Schlie�gruppe', $rsPEI->FeldInhalt('2'), 0, $BreiteSGR, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Tuer', $rsPEI->FeldInhalt('3'), 0, $BreiteTuer, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Tuer2', $rsPEI->FeldInhalt('4'), 0, $BreiteBem, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Ausgabedatum', $rsPEI->FeldInhalt('5'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
            if ($Param['SCH_Historie']=='on') 
            {
                $Form->Erstelle_ListenFeld('Rueckgabedatum', $rsPEI->FeldInhalt('6'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
                $Form->Erstelle_ListenFeld('Verlustdatum', $rsPEI->FeldInhalt('7'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
            }
            $Form->ZeileEnde();

            $DS++;
            $rsPEI->DSWeiter();
        }
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');           
            
    }
    elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
    {
        $BreiteNN       = 100;
        $BreiteVN       = 100;
        $BreiteAbt      = 290; 
        $BreiteTel      = 65;
        $BreiteDatum    = 82;
        $BreiteBem      = 250;

        //Listen�berschriften erstellen
        $Form->ZeileStart();
        $Icons = array();
        if($Recht25040!=0)
        {
            $Icons[] = array('new','./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Bearbeiten&SCH_KEY='.$Param['SCH_Key'], 'g',$AWISSprachKonserven['SCH']['SCH_ttt_Bearbeiten']);
        }
        $Form->Erstelle_ListeIcons($Icons,20,-1);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=KON_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KON_NAME1'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Nachname'], $BreiteNN, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=KON_NAME2'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KON_NAME2'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Vorname'], $BreiteVN, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=KAB_Abteilung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KAB_Abteilung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Abteilung'], $BreiteAbt, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=KKO_KURZWAHL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TUR_Bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Telefon'], $BreiteTel, '', $Link);
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Ausgabedatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Ausgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Ausgabedatum'], $BreiteDatum, '', $Link);
        if($Param['SCH_Historie']== 'on') //Funktioniert noch  nicht!!!
        {
            $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Rueckgabedatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Rueckgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Rueckgabedatum'], $BreiteDatum, '', $Link);
            $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Verlustdatum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_Rueckgabedatum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_Verlustdatum'], $BreiteDatum, '', $Link);
        }
        $Link = './schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht&Sort=MAS_Bemerkung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MAS_'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SCH']['SCH_MAS_Bermerkung'], $BreiteBem, '', $Link);
        $Form->ZeileEnde();

        $DS = 0;	// f�r Hintergrundfarbumschaltung
        
        //Listeneintr�ge erstellen
        while(!$rsPEI->EOF())
        {        
            $Form->ZeileStart();
            if ($Param['SCH_Historie']== 'on')
            {
                $Param['MAS_Key']= $rsPEI->FeldInhalt('9');
            }
            elseif ($Param['SCH_Historie']!= 'on')
            {
                $Param['MAS_Key']= $rsPEI->FeldInhalt('7');
            }
            if($Recht25040!=0)	// Kein �ndernrecht
            {
                $Icons = array();
                if($Recht25040)
                {
                        $Icons[] = array('edit','./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Bearbeiten&MAS_KEY='.$Param['MAS_Key'], 'g',$AWISSprachKonserven['SCH']['SCH_ttt_Bearbeiten']);
                }
                $Form->Erstelle_ListeIcons($Icons,20,-1);
            }
            $Form->Erstelle_ListenFeld('Nachname', $rsPEI->FeldInhalt('1'), 0, $BreiteNN, false, ($DS%2), '', '', 'T', 'L' );
            $Form->Erstelle_ListenFeld('Vorname', $rsPEI->FeldInhalt('2'), 0, $BreiteVN, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Abteilung', $rsPEI->FeldInhalt('4'), 0, $BreiteAbt, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Telefon', '- '.$rsPEI->FeldInhalt('6'), 0, $BreiteTel, false, ($DS%2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('Ausgabedatum', $rsPEI->FeldInhalt('3'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
            if ($Param['SCH_Historie']== 'on')
            {
                $Form->Erstelle_ListenFeld('Rueckgabedatum', $rsPEI->FeldInhalt('7'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
                $Form->Erstelle_ListenFeld('Verlustdatum', $rsPEI->FeldInhalt('8'), 0, $BreiteDatum, false, ($DS%2), '', '', 'D', 'L');
            }
            $Form->Erstelle_ListenFeld('Bemerkung', $rsPEI->FeldInhalt('5'), 0, $BreiteBem, false, ($DS%2), '', '', 'T', 'L');
            $Form->ZeileEnde();

            $DS++;
            $rsPEI->DSWeiter();
        }
     
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, ''); 
          
    }
    
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('schluesselzentrale_Speichern.php');
    }
    
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>