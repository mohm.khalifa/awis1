<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	HilfsUndBetriebsstoffGruppen
* 
* 	Pflege der HilfsUndBetriebsstoffGruppen
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt 2004
* 
****************************************************************************************************/
// Variablen
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 604);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffGruppen',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($HTTP_POST_VARS);
echo '<form name=frmHBG method=post action=./HilfsUndBetriebsstoffGruppen.php>';

/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(isset($HTTP_POST_VARS['cmdLoeschen_x']))
{
//	var_dump($HTTP_POST_VARS);
	if(isset($HTTP_POST_VARS["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM HILFSUNDBETRIEBSSTOFFGRUPPEN WHERE HBG_KEY=0" . $HTTP_POST_VARS["txtHGB_KEY_old"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			var_dump($SQL);
			
			awisErrorMailLink("HilfsUndBetriebsstoffGruppen.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Die Gruppe wurde erfolgreich gel�scht<br><br><a href=./HilfsUndBetriebsstoffGruppen.php?Liste=True>Weiter</a>');
	}
	elseif($HTTP_POST_VARS["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmHBG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHGB_KEY_old value=" . $HTTP_POST_VARS["txtHBG_KEY_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Hilfs- und Betriebsstoffgruppe l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}


if(isset($HTTP_POST_VARS['cmdSpeichern_x']))
{
	if($HTTP_POST_VARS['txtHBG_KEY_old']=='')
	{
		$SQL = "INSERT INTO HilfsUndBetriebsstoffGruppen";
		$SQL .= "(HBG_BEZEICHNUNG, HBG_BEMERKUNG, HBG_USER, HBG_USERDAT)";
		$SQL .= "VALUES('" . $HTTP_POST_VARS['txtHBG_BEZEICHNUNG'] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS['txtHBG_BEMERKUNG'] . "'";
		$SQL .= ",'" . $_SERVER['PHP_AUTH_USER'] . "'";
		$SQL .= ",SYSDATE)";
	}
	else
	{
		$Aenderungen = '';
		If($HTTP_POST_VARS['txtHBG_BEZEICHNUNG'] != $HTTP_POST_VARS['txtHBG_BEZEICHNUNG_old'])
		{
			$Aenderungen = ", HBG_BEZEICHNUNG = '" . $HTTP_POST_VARS['txtHBG_BEZEICHNUNG'] . "'";
		}
		If($HTTP_POST_VARS['txtHBG_BEMERKUNG'].'-' != $HTTP_POST_VARS['txtHBG_BEMERKUNG_old'].'-')
		{
			$Aenderungen .= ", HBG_BEMERKUNG = '" . $HTTP_POST_VARS['txtHBG_BEMERKUNG'] . "'";
		}
	
		If($Aenderungen!='')
		{
			$SQL = "UPDATE HilfsUndBetriebsstoffGruppen SET " . substr($Aenderungen,2);
			$SQL .= ", HBG_USER='" . $_SERVER['PHP_AUTH_USER'] . "', HBG_USERDAT=SYSDATE";
			$SQL .= " WHERE HBG_KEY=0" . $HTTP_POST_VARS['txtHBG_KEY_old'];
		}
	}	// Neu oder �ndern
//var_dump($SQL);
	$Erg = awisExecute($con, $SQL);
	if($Erg===FALSE)
	{
		awisErrorMailLink("HilfsUndBetriebsstoffGruppen.php", 2, $awisDBFehler);
		awisLogoff($con);
		die();
	}

}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************
		
if((!isset($HTTP_GET_VARS['HBG_KEY']) and !isset($HTTP_POST_VARS['txtHBG_KEY'])) or isset($HTTP_POST_VARS['cmdTrefferListe_x']))
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffGruppen";
}
else
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffGruppen WHERE HBG_KEY=0". $HTTP_GET_VARS['HBG_KEY'] . $HTTP_POST_VARS['txtHBG_KEY'] ."";
}

if(isset($HTTP_GET_VARS['Sort']))
{
	$SQL .= " ORDER BY " . $HTTP_GET_VARS['Sort'];
}
else
{
	$SQL .= " ORDER BY HBG_BEZEICHNUNG";
}

if(!isset($HTTP_GET_VARS['Hinzufuegen']))
{
	$rsHBG = awisOpenRecordset($con, $SQL);
	$rsHBGZeilen = $awisRSZeilen;
}
else
{
	$rsHBGZeilen=-1;
}


	//**************************************************
	// Daten ausgeben
	//**************************************************
	
if($rsHBGZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td width=100 id=FeldBez><a href=./HilfsUndBetriebsstoffGruppen.php?Sort=HBG_KEY>Nummer</a></td>';
	echo '<td id=FeldBez><a href=./HilfsUndBetriebsstoffGruppen.php?Sort=HBG_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td id=FeldBez><a href=./HilfsUndBetriebsstoffGruppen.php?Sort=HBG_KONZERN>Bemerkung</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHBGZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./HilfsUndBetriebsstoffGruppen.php?HBG_KEY=" . $rsHBG['HBG_KEY'][$Zeile] . ">" . $rsHBG['HBG_KEY'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBG_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBG_BEMERKUNG'][$Zeile] . "</td>";
		
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{

	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Hilfs- und Betriebsstoffgruppen</h3></td>';
	echo '<td><font size=1>' . $rsHBG['HER_USER'][0] . '<br>' . $rsHBG['HER_USERDAT'][0] . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./HilfsUndBetriebsstoffGruppen.php?Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=*></colgroup>';


	echo '<tr>';
	echo '<td colspan=2 align=right><font size=2>' . $rsHBG['HBG_USER'][0] . '</font>';
	echo '<br><font size=2>' . $rsHBG['HBG_USERDAT'][0] . '</font></td>';
	echo '</tr>';


	echo '<tr>';
	echo '<td id=FeldBez>ID</td>';
	echo '<td>' . $rsHBG['HBG_KEY'][0] . '';
	echo '<input type=hidden name=txtHBG_KEY_old value=' . $rsHBG['HBG_KEY'][0] . '></td>';
	echo '</tr>';

			// Zeile 2
	echo '<tr>';
	echo '<td id=FeldBez>Bezeichnung</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBG_BEZEICHNUNG size=40 value='" . $rsHBG['HBG_BEZEICHNUNG'][0] . "'>";
		echo "<input type=hidden name=txtHBG_BEZEICHNUNG_old value='" . $rsHBG['HBG_BEZEICHNUNG'][0] . "'></td>";
	}
	else
	{
		echo '<td>' . $rsHBG['HBG_BEZEICHNUNG'][0] . '</td>';
	}
	echo '</tr>';

			// Zeile 3
	echo '<tr>';
	echo '<td id=FeldBez>Bemerkung</td>';
	echo '<td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<input type=hidden name=txtHBG_BEMERKUNG_old value='" . $rsHBG['HBG_BEMERKUNG'][0] . "'>";

		echo '<textarea name=txtHBG_BEMERKUNG cols=60 rows=5>';
		echo $rsHBG['HBG_BEMERKUNG'][0] . "</textarea>";
	}
	else
	{
		echo $rsHBG['HBG_BEMERKUNG'][0];
	}
	echo '</td>';
	echo '</tr>';

	echo '</table>';

	if($Rechtestufe>1)
	{
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}
//		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}

	echo '</form>';
				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHBG_BEZEICHNUNG\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
* 
* */

if($HTTP_POST_VARS['txtZurueck']!='')
{
	$ZurueckLink = $HTTP_POST_VARS['txtZurueck'];
}
else
{
	if($HTTP_GET_VARS['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $HTTP_GET_VARS['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif($HTTP_GET_VARS['Zurueck']=='')
	{
		$ZurueckLink = '/index.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $HTTP_GET_VARS['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

if(!isset($HTTP_GET_VARS['Hinzufuegen']))
{
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./HilfsUndBetriebsstoffGruppen.php?Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}

echo '</form>';


print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=hbst_gruppen','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>