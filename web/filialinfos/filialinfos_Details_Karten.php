<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('CGK','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');	
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_TelefonischPersoenlich');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht121= $AWISBenutzer->HatDasRecht(121);		// Filialenlieferanten
			
	if(($Recht121&4)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}
	
	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);

	$FilUser=false;
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FilUser = true;
		$FIL_ID=$FilZugriff;		
	}
	
	if ($FilUser == true and intval($AWIS_KEY1) <> intval($FIL_ID))
	{
		echo ("<span class=HinweisText>Sie haben keine Berechtigung die ATU - Karten Daten von dieser Filiale anzusehen.</span>");
		die ();
	}
			
	$AWIS_KEY2 = (isset($_GET['CGK_KEY'])?$_GET['CGK_KEY']:(isset($_POST['txtCGK_KEY'])?$_POST['txtCGK_KEY']:''));
	
	$EditRecht=false;

	if(($Recht121 & 16)==16)
	{	
		$EditRecht=true;
	}	
	
	$SQL = 'SELECT * FROM';
	$SQL.= ' (SELECT CRM.*';
	$SQL.= ', CASE WHEN SUBSTR(CGK_NACHNAME,1,3)=\'***\' THEN CGK_FIRMA1 ';
	$SQL.= '       WHEN CGK_VORNAME IS NULL THEN \'KZ: \' || CGK_NACHNAME ';
	$SQL.= '       ELSE CGK_NACHNAME ';
	$SQL.= '  END AS ANZEIGENAME';
	$SQL.= ', DECODE(CGK_KONTAKTPERSONANREDE_ID, 1, \'Herr\', 2, \'Frau\', \'\') ||';
	$SQL.= ' NVL2(CGK_KONTAKTPERSONTITEL, \' \' || CGK_KONTAKTPERSONTITEL, \'\') ||';
	$SQL.= ' NVL2(CGK_KONTAKTPERSONVN, \' \' || CGK_KONTAKTPERSONVN, \'\') ||';
	$SQL.= ' NVL2(CGK_KONTAKTPERSONNN, \' \' || CGK_KONTAKTPERSONNN, \'\')  AS ANSPRECHPARTNER';
	$SQL.= ' , CGK_HAUSNR || \' \' || CGK_HNR2 as HAUSNUMMER';
	$SQL.= ', CGK_FIRMA1 ||';
	$SQL.= ' NVL2(CGK_FIRMA2, \' \' || CGK_FIRMA2, \'\') ||';
	$SQL.= ' NVL2(CGK_FIRMA3, \' \' || CGK_FIRMA3, \'\')  AS FIRMA';
	$SQL.= ' , (SELECT MAX(CGK_LASTTRANSACTIONDATE) FROM CRMGewerbekunden';
	$SQL.= ' WHERE CGK_ACCOUNT_ID = CRM.CGK_ACCOUNT_ID) AS MAXLASTTRANSACTIONDATE';
	$SQL.= ' , (SELECT MAX(CGK_LASTPRODUCTIONDATE) FROM CRMGewerbekunden';
	$SQL.= ' WHERE CGK_ACCOUNT_ID = CRM.CGK_ACCOUNT_ID) AS MAXLASTPRODUCTIONDATE';
	$SQL.= ' FROM CRMGewerbekunden CRM';
	$SQL.= ' WHERE CGK_ORIG_STORE_ID=0'.$AWIS_KEY1;
	//$SQL.= ' WHERE CGK_ORIG_STORE_ID > 1 and cgk_orig_store_id < 600';	
	$SQL.= ' AND CGK_ACCOUNT_TY = 3';
	$SQL.= ' AND CGK_DBM_KARTENART = \'HK\'';
	//$SQL.= ' AND CGK_ISCARDREPLACED = \'N\'';
	$SQL.= ' AND CGK_PLANTDATE >= (SYSDATE - 100)';
	//$SQL.= ' AND ((NVL(CGK_KONTAKT,0) <> 1) OR (NVL(CGK_KONTAKT,0) = 1 AND CGK_USERDATFILIALE >= (SYSDATE - 5)))';
	$SQL.= ' AND CGK_CUST_TY_ID = 3';
	$SQL.= ' AND CGK_RECSTATE = \'N\')';
	$SQL.= ' WHERE MAXLASTTRANSACTIONDATE IS NULL';
	$SQL.= ' AND MAXLASTPRODUCTIONDATE IS NOT NULL';
	$SQL.= ' AND MAXLASTPRODUCTIONDATE <= (SYSDATE - 5)';
	//$SQL .= ' OR (CGK_AC_DBM_UPDATE_DT > (SYSDATE - 300))';
	//$SQL .= ' AND (CGK_AC_DBM_UPDATE_DT > (SYSDATE - 300)';
	//$SQL .= ' AND CGK_PLANTDATE <= (SYSDATE - 5)';
	//$SQL .= ' AND CGK_OPAL_KARTENART IN (\'HK\',\'ZK\')';
				
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND CGK_KEY = 0'.$DB->FeldInhaltFormat('Z',$AWIS_KEY2);		
	}		
	
	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['SSort']))
	{
		$SQL.= ' ORDER BY CGK_FIRST_CONTACT_DT DESC';		
	}
	else
	{
		$SQL.= ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);		
	}
		
	$Form->DebugAusgabe(1,$SQL);
	$rsCGK = $DB->RecordSetOeffnen($SQL);
	
	if($AWIS_KEY2=='' or isset($_GET['CGKListe']))	// Liste anzeigen
	{
		$DetailAnsicht = false;
	
		$Form->Formular_Start();

		$Form->ZeileStart();						
		
		$Form->Erstelle_Liste_Ueberschrift('',38,'');
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=CGK_FIRST_CONTACT_DT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='CGK_FIRST_CONTACT_DT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRST_CONTACT_DT'],120,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=CGK_FIRMA1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='CGK_FIRMA1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRMA1'],320,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=CGK_VERTRAGSART'.((isset($_GET['SSort']) AND ($_GET['SSort']=='CGK_VERTRAGSART'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_VERTRAGSART'],300,'',$Link);

		$Form->ZeileEnde();
		
		$DS=0;
		
		while(!$rsCGK->EOF())
		{	
			$Form->ZeileStart();		
			
			$Icons = array();
			$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&CGK_KEY='.$rsCGK->FeldInhalt('CGK_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));						
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('#CGK_PLANTDATE',$rsCGK->FeldInhalt('CGK_PLANTDATE'),0,120,false,($DS%2),'','','D','L','');
			$Form->Erstelle_ListenFeld('#CGK_FIRMA1',$rsCGK->FeldInhalt('CGK_FIRMA1'),0,320,false,($DS%2),'','','T','L','');
			$Form->Erstelle_ListenFeld('#CGK_VERTRAGSART',$rsCGK->FeldInhalt('CGK_VERTRAGSART'),0,300,false,($DS%2),'','','T','L','');
						
			$Form->ZeileEnde();

			$rsCGK->DSWeiter();
			$DS++;
		}
		
		$Form->Formular_Ende();
	}
	else //Einer oder keiner
	{
		$AWIS_KEY2 = $rsCGK->FeldInhalt('CGK_KEY');	

		echo '<input type=hidden name=txtCGK_KEY value='.$AWIS_KEY2. '>';
		echo '<input type=hidden name=txtCGK_CGK_KEY value='.$AWIS_KEY1. '>';
		
		$Form->Formular_Start();		
		$EditModus=true;
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./filialinfos_Main.php?cmdAktion=Details&CGKListe=1".(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");		
		//$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFLI->FeldInhalt('FLI_USER'));
		//$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFLI->FeldInhalt('FLI_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ACCOUNT_ID'].':',150);
		$Form->Erstelle_TextFeld('*CGK_ACCOUNT_ID',$rsCGK->FeldInhalt('CGK_ACCOUNT_ID'),10,200,false,'','','','T');
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_KONTAKT'].':',130);
		$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_TelefonischPersoenlich']);		
		$Form->Erstelle_SelectFeld('CGK_KONTAKT_'.$rsCGK->FeldInhalt('CGK_KEY'),$rsCGK->FeldInhalt('CGK_KONTAKT'),200,$EditModus,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','','','');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLANTDATE'].':',150);
		$Form->Erstelle_TextFeld('*CGK_PLANTDATE',$rsCGK->FeldInhalt('CGK_PLANTDATE'),10,200,false,'','','','D');		
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_BEMERKUNG'].':',130);
		$Form->Erstelle_Textarea('CGK_BEMERKUNG_'.$rsCGK->FeldInhalt('CGK_KEY'),$rsCGK->Feldinhalt('CGK_BEMERKUNG'),250,40,2,$EditRecht,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_TITLE'].':',150);
		$Form->Erstelle_TextFeld('*CGK_TITLE_1',$rsCGK->FeldInhalt('CGK_TITLE_1'),10,400,false,'','','','T');		
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRMA'].':',150);
		$Form->Erstelle_TextFeld('*CGK_FIRMA',$rsCGK->FeldInhalt('FIRMA'),10,400,false,'','','','T');	
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_STRASSE'].':',150);
		$Form->Erstelle_TextFeld('*CGK_STRASSE',$rsCGK->FeldInhalt('CGK_STRASSE'),10,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_HAUSNUMMER'].':',130);
		$Form->Erstelle_TextFeld('*CGK_HAUSNUMMER',$rsCGK->FeldInhalt('HAUSNUMMER'),10,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLZ'].':',150);
		$Form->Erstelle_TextFeld('*CGK_PLZ',$rsCGK->FeldInhalt('CGK_PLZ'),10,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ORT'].':',130);
		$Form->Erstelle_TextFeld('*CGK_ORT',$rsCGK->FeldInhalt('CGK_ORT'),10,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PHONE_H'].':',150);
		$Form->Erstelle_TextFeld('*CGK_PHONE_H',$rsCGK->FeldInhalt('CGK_PHONE_H'),10,400,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PHONE_M'].':',150);
		$Form->Erstelle_TextFeld('*CGK_PHONE_M',$rsCGK->FeldInhalt('CGK_PHONE_M'),10,400,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Link = ($rsCGK->FeldInhalt('CGK_EMAIL')!=''?'mailto:'.$rsCGK->FeldInhalt('CGK_EMAIL'):'');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_EMAIL'].':',150);
		$Form->Erstelle_TextFeld('*CGK_EMAIL',$rsCGK->FeldInhalt('CGK_EMAIL'),10,400,false,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ANSPRECHPARTNER'].':',150);
		$Form->Erstelle_TextFeld('*CGK_ANSPRECHPARTNER',$rsCGK->FeldInhalt('ANSPRECHPARTNER'),10,400,false,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_DT'].':',150);
		$Form->Erstelle_TextFeld('*CGK_LOCK_DT',$rsCGK->FeldInhalt('CGK_LOCK_DT'),10,120,false,'','','','D');	
		$Form->ZeileEnde();

		$Form->Formular_Ende();
		
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002101351");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201002101351");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>