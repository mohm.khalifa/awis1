<?php
/**
 * Ausdruck der offenen Preisabfragen
 *
 * Parameter
 *  PFA_KEY
 *  FIL_ID
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();


$PAF_KEY = $_GET['PAF_KEY'];
$Filialen = $_GET['FIL_ID'];

// Ermitteln, welche Gebiete man sehen darf

$Recht8005 = $AWISBenutzer->HatDasRecht(8005);
$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');

$TextKonserven=array();
$TextKonserven[]=array('AST','AST_%');
$TextKonserven[]=array('Wort','wrd_Stand');
$TextKonserven[]=array('Wort','wrd_Paketpreis');
$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

//$AWISSprachKonserven['PFA']['tit_Bericht_1']
$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Preisabfrage');

$Zeile = $Ausdruck->NeueSeite(0,1);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->setXY(10,200);
$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

$Ausdruck->_pdf->setXY(10,12);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);

//
// Daten
//

$SQL = 'SELECT PreisAbfragenPreise.*';
$SQL .= ' ,COALESCE(AST_BEZEICHNUNGWW,DPR_BEZEICHNUNG) AS AST_BEZEICHNUNGWW';
$SQL .= ', MBW_NAME1, MBW_STRASSE, MBW_HAUSNUMMER, MBW_PLZ, MBW_ORT, MBW_TELEFON';
$SQL .= ', (SELECT DISTINCT DPR_BEZEICHNUNG FROM Dienstleistungspreise WHERE DPR_NUMMER = PFA_DLNR ) AS DLBEZ';
$SQL .= ', COALESCE((SELECT DISTINCT AST_BEZEICHNUNGWW FROM Artikelstamm WHERE AST_ATUNR = PFA_KOMPLETTNR ),(SELECT DISTINCT DPR_BEZEICHNUNG FROM Dienstleistungspreise WHERE DPR_NUMMER = PFA_KOMPLETTNR)) AS KOMPLETTBEZ';
$SQL .=', PFI_Baujahr, PFI_kmstand, KFZ_PS, KFZ_HERSTELLER, KFZ_MODELL, KFZ_TYP, KFZ_KRAFTSTOFFART, KFZ_ZYLINDER, KFZ_LIT';
$SQL .= ' FROM PreisAbfragenPreise ';
$SQL .= ' INNER JOIN Mitbewerber ON PFA_MBW_KEY = MBW_KEY';
$SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = PFA_AST_ATUNR ';
$SQL .= ' LEFT OUTER JOIN Preisabfragengruppen ON PFG_KEY = PFA_PFG_KEY';
$SQL .= ' LEFT OUTER JOIN Preisabfragenfahrzeuginfos ON PFG_PFI_KEY = PFI_KEY';
$SQL .= ' LEFT OUTER JOIN V_KFZ_DATEN ON PFI_KTYPNR = KFZ_TYPNR';
$SQL .= ' LEFT OUTER JOIN (SELECT DISTINCT DPR_NUMMER, DPR_BEZEICHNUNG FROM Dienstleistungspreise ) DL ON DPR_NUMMER = PFA_AST_ATUNR ';
$SQL .= ' WHERE PFA_PAF_KEY = 0'.$PAF_KEY ;
$SQL .= ' AND PFA_FIL_ID IN ('.$Filialen.')';
$SQL .= ' AND (PFA_PREIS IS NULL AND PFA_ERMITTLUNGSSTATUS = 1)';
$SQL .= ' ORDER BY MBW_NAME1, PFA_MBW_KEY, PFA_AST_ATUNR';

$rsPFA = $DB->RecordSetOeffnen($SQL);

// �berschrift f�r den Bericht
$Zeile = 30;
$Spalte = 15;

$Ausdruck->_pdf->setXY(15,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','B',16);
$Ausdruck->_pdf->cell(50,6,'Preisabfrage',0,0,'L',0);
$Ausdruck->_pdf->line($Spalte,$Zeile+6,$Ausdruck->SeitenBreite()-20,$Zeile+6);
$Zeile +=8;

$LetzterMBW = 0;
$LetztePFG = 0;
while(!$rsPFA->EOF())
{
    if($LetztePFG != $rsPFA->FeldInhalt('PFA_PFG_KEY'))
    {
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','B',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
        $Ausdruck->_pdf->cell(65,6,'Hersteller: '.$rsPFA->FeldInhalt('KFZ_HERSTELLER'),0,0,'L',0);
        $Ausdruck->_pdf->cell(65,6,'Modell: '.$rsPFA->FeldInhalt('KFZ_MODELL'),0,0,'L',0);
        $Ausdruck->_pdf->cell(65,6,'Typ: '.$rsPFA->FeldInhalt('KFZ_TYP'),0,0,'L',0);
        $Zeile+=5;

		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','B',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
        $Ausdruck->_pdf->cell(36,6,'Baujahr: '.$rsPFA->FeldInhalt('PFI_BAUJAHR'),0,0,'L',0);
        $Ausdruck->_pdf->cell(40,6,'km-Stand: '.$Form->Format('N0T',$rsPFA->FeldInhalt('PFI_KMSTAND')),0,0,'L',0);
        $Ausdruck->_pdf->cell(30,6,'PS: '.$rsPFA->FeldInhalt('KFZ_PS'),0,0,'L',0);
        $Ausdruck->_pdf->cell(40,6,'Hubraum: '.$rsPFA->FeldInhalt('KFZ_LIT'),0,0,'L',0);
        $Ausdruck->_pdf->cell(40,6,'Zylinder: '.$rsPFA->FeldInhalt('KFZ_ZYLINDER'),0,0,'L',0);

        $LetztePFG = $rsPFA->FeldInhalt('PFA_PFG_KEY');
        $Zeile += 5;
	    $Ausdruck->_pdf->line($Spalte,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
        $Zeile += 5;
    }
	
    if($LetzterMBW != $rsPFA->FeldInhalt('PFA_MBW_KEY'))
    {
    	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
    	
        $Ausdruck->_pdf->SetFont('Arial','B',12);
        $Ausdruck->_pdf->cell(80,6,$rsPFA->FeldInhalt('MBW_NAME1'),0,0,'L',0);
        $Ausdruck->_pdf->cell(80,6,$rsPFA->FeldInhalt('MBW_STRASSE').' '.$rsPFA->FeldInhalt('MBW_HAUSNUMMER'),0,0,'L',0);
        $Ausdruck->_pdf->cell(60,6,$rsPFA->FeldInhalt('MBW_PLZ').' '.$rsPFA->FeldInhalt('MBW_ORT'),0,0,'L',0);
        $Ausdruck->_pdf->cell(40,6,$rsPFA->FeldInhalt('MBW_TELEFON'),0,0,'R',0);

        $LetzterMBW = $rsPFA->FeldInhalt('PFA_MBW_KEY');
        $Zeile += 10;
    }
    
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
    
    $Ausdruck->_pdf->SetFont('Arial','',12);
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);

    $Ausdruck->_pdf->cell(20,6,$rsPFA->FeldInhalt('PFA_AST_ATUNR'),0,0,'L',0);
    $Ausdruck->_pdf->cell(100,6,$rsPFA->FeldInhalt('AST_BEZEICHNUNGWW'),0,0,'L',0);
    $Ausdruck->_pdf->Line($Spalte+240, $Zeile+5, $Spalte+260, $Zeile+5);
	$Ausdruck->_pdf->setXY($Spalte+260,$Zeile);
    $Ausdruck->_pdf->cell(5,6,'�',0,0,'L',0);
     if($rsPFA->FeldInhalt('PFA_DLNR')!='')
    {
    	$Zeile+=10;
    	$Ausdruck->_pdf->setXY($Spalte+20,$Zeile);
    	$Ausdruck->_pdf->cell(105,6,$rsPFA->FeldInhalt('DLBEZ'),0,0,'L',0);
    	$Ausdruck->_pdf->cell(40,6,'('.$rsPFA->FeldInhalt('PFA_DLNR').')',0,0,'L',0);
    	$Ausdruck->_pdf->Line($Spalte+240, $Zeile+5, $Spalte+260, $Zeile+5);
    }
    
    // Falls ein Komplettpreis gew�nscht ist
    if($rsPFA->FeldInhalt('PFA_KOMPLETTNR')!='')
    {
    	$Zeile+=10;
    	
	    if($Zeile > $Ausdruck->SeitenHoehe()-30)
	    {
	        $Zeile = $Ausdruck->NeueSeite(0,1);
	
	        $Zeile = 30;
	        $Spalte = 15;
	
	        $Ausdruck->_pdf->setXY(15,$Zeile);
	        $Ausdruck->_pdf->SetFont('Arial','B',16);
	        $Ausdruck->_pdf->cell(50,6,'Preisabfrage',0,0,'L',0);
	        $Ausdruck->_pdf->line($Spalte,$Zeile+6,$Ausdruck->SeitenBreite()-20,$Zeile+6);
	        $Zeile += 10;
	
	        $Ausdruck->_pdf->SetFont('Arial','',6);
	        $Ausdruck->_pdf->setXY(10,200);
	        $Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
	
	        $Ausdruck->_pdf->setXY(10,12);
	        $Ausdruck->_pdf->SetFont('Arial','',6);
	        $Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);
	    }
	    
    	$Ausdruck->_pdf->setXY($Spalte+20,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','B',12);
    	if($rsPFA->FeldInhalt('KOMPLETTBEZ')=='')
    	{
    		$Ausdruck->_pdf->cell(105,6,$AWISSprachKonserven['Wort']['wrd_Paketpreis'],0,0,'L',0);
    	}
    	else
    	{
    		$Ausdruck->_pdf->cell(105,6,$rsPFA->FeldInhalt('KOMPLETTBEZ'),0,0,'L',0);
    	}
    	$Ausdruck->_pdf->cell(40,6,'('.$rsPFA->FeldInhalt('PFA_KOMPLETTNR').')',0,0,'L',0);
    	$Ausdruck->_pdf->Line($Spalte+240, $Zeile+5, $Spalte+260, $Zeile+5);
    }
        
    $Zeile+=10;

    if($Zeile > $Ausdruck->SeitenHoehe()-30)
    {
        $Zeile = $Ausdruck->NeueSeite(0,1);

        $Zeile = 30;
        $Spalte = 15;

        $Ausdruck->_pdf->setXY(15,$Zeile);
        $Ausdruck->_pdf->SetFont('Arial','B',16);
        $Ausdruck->_pdf->cell(50,6,'Preisabfrage',0,0,'L',0);
        $Ausdruck->_pdf->line($Spalte,$Zeile+6,$Ausdruck->SeitenBreite()-20,$Zeile+6);
        $Zeile += 10;

        $Ausdruck->_pdf->SetFont('Arial','',6);
        $Ausdruck->_pdf->setXY(10,200);
        $Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

        $Ausdruck->_pdf->setXY(10,12);
        $Ausdruck->_pdf->SetFont('Arial','',6);
        $Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);
    }

    $rsPFA->DSWeiter();
}

$Ausdruck->Anzeigen();

