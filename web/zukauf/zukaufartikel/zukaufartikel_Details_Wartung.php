<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZUP','%');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);		// Artikelstamm
	if(($Recht10001&1)==0)
	{
		$DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZLA_ARTIKELNUMMER DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZUKAUFANPASSUNGENPROTOKOLL.*, ZLA_ARTIKELNUMMER';
	$SQL .= ',row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZUKAUFANPASSUNGENPROTOKOLL';
	$SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZUP_XXX_KEY = ZLA_KEY';
	$SQL .= ' WHERE ZUP_XTN_KUERZEL ='.$DB->WertSetzen('ZUP', 'T','ZLA');
	$SQL .= ' AND ZUP_XXX_KEY ='.$DB->WertSetzen('ZUP', 'T',$AWIS_KEY1);

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('ZUP',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsZLA = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('ZUP'));
	$Form->Formular_Start();

	if($rsZLA->AnzahlDatensaetze()>0)
	{
		$Form->ZeileStart();
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZUP_USERDAT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUP_USERDAT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUP']['ZUP_USERDAT'],190,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZUP_AKTION'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUP_AKTION'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUP']['ZUP_AKTION'],650,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZUP_DATENALT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUP_DATENALT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUP']['ZUP_DATENALT'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZUP_DATENNEU'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUP_DATENNEU'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUP']['ZUP_DATENNEU'],150,'',$Link);

		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZLA->EOF())
		{
			$Form->ZeileStart();

			$Form->Erstelle_ListenFeld('*ZUP_USERDAT',$rsZLA->FeldInhalt('ZUP_USERDAT'),20,190,false,($DS%2),'','','DU');
			$Form->Erstelle_ListenFeld('*ZUP_AKTION',$rsZLA->FeldInhalt('ZUP_AKTION'),20,650,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZUP_DATENALT',$rsZLA->FeldInhalt('ZUP_DATENALT'),20,150,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZUP_DATENNEU',$rsZLA->FeldInhalt('ZUP_DATENNEU'),20,150,false,($DS%2),'','','T');


			$Form->ZeileEnde();

			$rsZLA->DSWeiter();
			$DS++;
		}


		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Wartung';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>