<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.$AWISBenutzer->BenutzerName().';Export gestartet...'.PHP_EOL,FILE_APPEND);
    
    $Recht10016 = $AWISBenutzer->HatDasRecht(10016);
    if($Recht10016==0)
    {
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
		die();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Zukauf_Preisvergleich'));
    
    
    $Bedingung = _BedingungErstellen($Param);
    
    $SQL = ' SELECT ZLA_KEY,WUG_ID, WUG_BEZEICHNUNG';
    $SQL .= ', ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG, ZLH_BEZEICHNUNG, ZLA_AST_ATUNR';
    $SQL .= ' FROM ZUKAUFARTIKEL';
    $SQL .= ' INNER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
    $SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZAL_ZLA_KEY = ZLA_KEY';
    $SQL .= ' INNER JOIN Lieferanten ON ZAL_LIE_NR = LIE_NR';
    $SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = ZLA_AST_ATUNR';
    $SQL .= ' LEFT OUTER JOIN WARENUNTERGRUPPEN ON WUG_KEY = AST_WUG_KEY ';
    
    if($Bedingung!='')
    {
        $SQL .= ' WHERE ' . substr($Bedingung,4);
    }
    $SQL .= ' ORDER BY 1';

    $rsZLA = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('ZLA'));
    
    file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.$AWISBenutzer->BenutzerName().';'.$rsZLA->AnzahlDatensaetze().' Zeilen gefunden'.PHP_EOL,FILE_APPEND);

    $Verzeichnis = $AWISBenutzer->ParameterLesen('Zukauf: Preisvergleich-Exportpfad');
    if(!file_exists($Verzeichnis))
    {
        file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.'Exportpfad '.$Verzeichnis.' exisitiert nicht'.PHP_EOL,FILE_APPEND);
        echo '<span style="color:red">Exportpfad nicht vorhanden. Bitte an die Entwicklung wenden!</span>';
        die();
    }
    $Dateiname = 'Export_'.$AWISBenutzer->BenutzerName().'_'.date('Ymd_His').'.csv';
    $fp = fopen($Verzeichnis.'/'.$Dateiname,'w');
    if($fp==false)
    {
        echo '<span style="color:red">Kann Datei nicht erzeugen. Bitte an die Entwicklung wenden!</span>';
        die();
    }

    $Felder[]='Artikelnummer';
    $Felder[]='Bezeichnung';
    $Felder[]='Hersteller';
    $Felder[]='WUG';
    $Felder[]='Warengruppe';
    $Felder[]='ATU Nummer';
    foreach($Param['LIE_NR'] AS $LIE_NR)
    {
        $Felder[]='EK ('.$LIE_NR.')';
        $Felder[]='VK ('.$LIE_NR.')';
        $Felder[]='Ab ('.$LIE_NR.')';
    }
    fputcsv($fp, $Felder, "\t");
    
    $DS=0;
    while(!$rsZLA->EOF())
    {
        $Felder = array();
        $Felder[]=$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER');
        $Felder[]=$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG');
        $Felder[]=$rsZLA->FeldInhalt('ZLH_BEZEICHNUNG');
        $Felder[]=$rsZLA->FeldInhalt('WUG_ID');
        $Felder[]=$rsZLA->FeldInhalt('WUG_BEZEICHNUNG');
        $Felder[]=$rsZLA->FeldInhalt('ZLA_AST_ATUNR');

        $SQL = 'SELECT ZAL_LIE_NR, ZAP_VKPR, ZAP_EK, ZAP_GUELTIGAB';
        $SQL .= ' FROM v_ZukaufArtikelPreise';
        $SQL .= ' INNER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAP_ZAL_KEY = ZAL_KEY';
        $SQL .= ' WHERE ZAL_ZLA_KEY = '.$DB->WertSetzen('ZAP', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));

        $TeilBedingung = '';
        foreach($Param['LIE_NR'] AS $LIE_NR)
        {
            $TeilBedingung .= ' OR ZAL_LIE_NR = ' . $DB->WertSetzen('ZAP', 'N0', $LIE_NR);
        }
        $SQL .= ' AND ( '.substr($TeilBedingung,4) . ')';
        $SQL .= ' ORDER BY 1';        
        
        $rsZAP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZAP'));
        $Daten=array();
        while(!$rsZAP->EOF())
        {
            $Daten[$rsZAP->FeldInhalt('ZAL_LIE_NR')]['EK']=$rsZAP->FeldInhalt('ZAP_EK');
            $Daten[$rsZAP->FeldInhalt('ZAL_LIE_NR')]['VK']=$rsZAP->FeldInhalt('ZAP_VKPR');
            $Daten[$rsZAP->FeldInhalt('ZAL_LIE_NR')]['AB']=$rsZAP->FeldInhalt('ZAP_GUELTIGAB');
            
            $rsZAP->DSWeiter();
        }
        foreach($Param['LIE_NR'] AS $LIE_NR)
        {
            if(isset($Daten[$LIE_NR]))
            {
                $Felder[]=$Daten[$LIE_NR]['EK'];
                $Felder[]=$Daten[$LIE_NR]['VK'];
                $Felder[]=$Daten[$LIE_NR]['AB'];
            }
            else
            {
                $Felder[]='';
                $Felder[]='';
                $Felder[]='';
            }
        }
        
        fputcsv($fp, $Felder, "\t");
        $DS++;
        
        $rsZLA->DSWeiter();
    }
    
    echo 'Es wurden '.$DS.' Datenzeilen in die Datei '.$Dateiname.' exportiert';
}
catch(awisException $ex)
{
    file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.$AWISBenutzer->BenutzerName().';Fehler:'.$ex->getMessage().PHP_EOL,FILE_APPEND);
    file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.$AWISBenutzer->BenutzerName().';Fehler:'.$ex->getSQL().PHP_EOL,FILE_APPEND);
    
}
catch(Exception $ex)
{
    file_put_contents('/var/log/awis/zukauf_preisvergleich_export.log', date('c').';'.$AWISBenutzer->BenutzerName().';Fehler:'.$ex->getMessage().PHP_EOL,FILE_APPEND);
    
}
/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if(isset($Param['ZLH_KEY']) AND !empty($Param['ZLH_KEY']))
    {
        $Bedingung .= ' AND (';
        $TeilBedingung = '';
        foreach($Param['ZLH_KEY'] AS $ZLH_KEY)
        {
            $TeilBedingung .= ' OR ZLA_ZLH_KEY = ' . $DB->WertSetzen('ZLA', 'N0', $ZLH_KEY);
        }
        $Bedingung .= substr($TeilBedingung,4) . ')';
    }


    if(isset($Param['LIE_NR']) AND !empty($Param['LIE_NR']))
    {
        $Bedingung .= ' AND (';
        $TeilBedingung = '';
        foreach($Param['LIE_NR'] AS $LIE_NR)
        {
            $TeilBedingung .= ' OR ZAL_LIE_NR = ' . $DB->WertSetzen('ZLA', 'T', $LIE_NR);
        }
        $Bedingung .= substr($TeilBedingung,4) . ')';
    }

    if(isset($Param['WUG_ID']) AND !empty($Param['WUG_ID']))
    {
        $Bedingung .= ' AND (';
        $TeilBedingung = '';
        foreach($Param['WUG_ID'] AS $WUG_ID)
        {
            $TeilBedingung .= ' OR AST_WUG_KEY = ' . $DB->WertSetzen('ZLA', 'T', $WUG_ID);
        }
        $Bedingung .= substr($TeilBedingung,4) . ')';
    }

    if(isset($Param['ABVERKAUF']) AND $Param['ABVERKAUF']!='')
    {
        $Bedingung .= ' AND '.($Param['ABVERKAUF']==0?'NOT':'').' EXISTS(SELECT * FROM zukaufbestellungen WHERE zub_zla_key = zla_key)';
    }

    $Param['WHERE']=$Bedingung;

    return $Bedingung;
}
?>