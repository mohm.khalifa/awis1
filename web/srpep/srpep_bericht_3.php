<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();

$Recht11008 = $AWISBenutzer->HatDasRecht(11008);

$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SRTermine'));
if (!isset($Param['FEB_KEY'])) {
    $Param['FEB_KEY'] = '';
    $Param['KOV_WOCHE'] = date('W');
    $Param['KOV_JAHR'] = date('Y');
}
if (isset($_POST['sucFEB_KEY'])) {
    $Param['FEB_KEY'] = $_POST['sucFEB_KEY'];
    $Param['KOV_WOCHE'] = $_POST['sucKOV_WOCHE'];
    $Param['KOV_JAHR'] = $_POST['sucKOV_JAHR'];
}

$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');

$TextKonserven = array();
$TextKonserven[] = array('WTP', '%');
$TextKonserven[] = array('WTE', 'WTE_TERMINUEBERSICHT');
$TextKonserven[] = array('Wort', 'wrd_Stand');
$TextKonserven[] = array('WTE', 'WTE_FIL_ID');
$TextKonserven[] = array('Wort', 'ATUNR');
$TextKonserven[] = array('Wort', 'Bezeichnung');
$TextKonserven[] = array('WPO', 'WPO_TERMINVON');
$TextKonserven[] = array('WPO', 'WPO_TERMINBIS');
$TextKonserven[] = array('WPO', 'WPOZEITSTD');
$TextKonserven[] = array('Wort', 'Umsatz');
$TextKonserven[] = array('Ausdruck', 'txt_HinweisAusdruckIntern');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$SQL = 'select kon_name1, kon_name2, wpo_ast_atunr, wpo_bezeichnung, wpo_terminvon, wpo_terminbis, wpo_zeitmin, wte_fil_id, wpo_vk';
$SQL .= ' from kontakte ';
$SQL .= ' inner join werkstatttermineplanungen';
$SQL .= ' on werkstatttermineplanungen.wtp_kon_key = kontakte.kon_key';
$SQL .= ' inner join werkstattterminepositionen';
$SQL .= ' on werkstattterminepositionen.wpo_key = werkstatttermineplanungen.wtp_wpo_key';
$SQL .= ' inner join werkstatttermine ';
$SQL .= ' on werkstatttermine.wte_key = werkstattterminepositionen.wpo_wte_key';
$SQL .= ' where wpo_terminvon >= trunc(SYSDATE) ';
$SQL .= ' and kon_key = ' . $DB->WertSetzen('WTE', 'Z', $AWISBenutzer->BenutzerKontaktKEY());
$rsWTE = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('WTE'));

$Ausdruck = new awisAusdruck('L', 'A4', $Vorlagen, $AWISSprachKonserven['WTE']['WTE_TERMINUEBERSICHT'] . ' ' . $rsWTE->FeldInhalt('KON_NAME1') . ' ' . $rsWTE->FeldInhalt('KON_NAME2'));

$Zeile = $Ausdruck->NeueSeite(0, 1);
$Ausdruck->_pdf->setXY(10, 12);
$Ausdruck->_pdf->SetFont('Arial', '', 6);
$Ausdruck->_pdf->cell(10, 6, $AWISSprachKonserven['Wort']['wrd_Stand'] . ': ' . date('d.m.Y H:i'), 0, 0, 'L', 0);

$Zeile = 30;
$Ausdruck->_pdf->setXY(10, $Zeile);
$Ausdruck->_pdf->SetFont('Arial', '', 12);
$Ausdruck->_pdf->cell(50, 6, $AWISSprachKonserven['WTE']['WTE_TERMINUEBERSICHT'] . ' ' . $rsWTE->FeldInhalt('KON_NAME1') . ' ' . $rsWTE->FeldInhalt('KON_NAME2'), 0, 0, 'L', 0);
$Zeile += 6;

$Ausdruck->_pdf->setXY(10, $Zeile);
$Ausdruck->_pdf->SetFont('Arial', '', 10);
$Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['WTE']['WTE_FIL_ID'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['Wort']['ATUNR'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(100, 6, $AWISSprachKonserven['Wort']['Bezeichnung'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(40, 6, $AWISSprachKonserven['WPO']['WPO_TERMINVON'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(40, 6, $AWISSprachKonserven['WPO']['WPO_TERMINBIS'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['WPO']['WPOZEITSTD'], 1, 0, 'L', 0);
$Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['Wort']['Umsatz'], 1, 0, 'L', 0);

while (!$rsWTE->EOF()) {
    $Ausdruck->_pdf->setXY(10, $Zeile + 6);
    $Ausdruck->_pdf->cell(15, 6, $rsWTE->FeldInhalt('WTE_FIL_ID'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(30, 6, $rsWTE->FeldInhalt('WPO_AST_ATUNR'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(100, 6, $rsWTE->FeldInhalt('WPO_BEZEICHNUNG'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(40, 6, $rsWTE->FeldInhalt('WPO_TERMINVON'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(40, 6, $rsWTE->FeldInhalt('WPO_TERMINBIS'), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(20, 6, $Form->Format('N1', ($rsWTE->FeldInhalt('WPO_ZEITMIN') / 60)), 0, 0, 'L', 0);
    $Ausdruck->_pdf->cell(20, 6, $rsWTE->FeldInhalt('WPO_VK'), 0, 0, 'L', 0);

    $rsWTE->DSWeiter();
    $Zeile += 6;
}

$Ausdruck->Anzeigen();
