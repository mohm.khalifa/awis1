<?php

require_once('autoglas_differenzliste.inc');
require_once('awisBenutzer.inc');
require_once('awisJobVerwaltung.inc');

try
{
	$Benutzer = '';
	$DebugLevel = 0;
	$StartTag = '';
	$EndTag = '';
	$Testsystem = false;
	$WANummer = '';
	
	for($i=1;$i<$argc;$i++)
	{
		switch (strtolower($argv[$i]))
		{
		case '--benutzer':
			$Benutzer = $argv[++$i];
			break;
					case '--debuglevel':
					$DebugLevel = $argv[++$i];
					break;
					case '--testsystem':
					$Testsystem = true;
					break;
							case '--wanummer':
							$WANummer = $argv[++$i];
							break;
							case '--starttag':
							$StartTag = $argv[++$i];
									break;
									case '--endtag':
									$EndTag = $argv[++$i];
									break;
									case '--abfrage':
									$Abfrage = $argv[++$i];
									break;
									default:
									echo 'Parameter '.$argv[$i].' ist unbekannt.';
									case '--help':
										echo $argv[0].PHP_EOL;
										echo 'Optionen:'.PHP_EOL;
										echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
										echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
										echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;
										echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
										echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.'.PHP_EOL;
										echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
	                            		echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
										echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.'.PHP_EOL;
	
	                            echo PHP_EOL;
												die();
			}
		}

		$AWISBenutzer = awisBenutzer::Init($Benutzer);
    	$Form = new awisFormular();
    	$DB = awisDatenbank::NeueVerbindung('AWIS');
    	
    	$report = new autoglas_differenzliste();
        $report->erstelleReporting();
    	
}
catch (awisException $ex)
{
	echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
	echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
	echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
}

?>