<?php
require_once('abgleich_funktionen.inc');
/**
 * Seite fuer Abgeglichene Datensaetze Tuev
 *
 * @author    Tobias Schaeffler
 * @copyright ATU Auto Teile Unger
 * @version   201610061335
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TAG','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Wort', 'PDFErzeugen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht23030 = $AWISBenutzer->HatDasRecht(23030);
	if($Recht23030 == 0) {
	    $Form->Fehler_KeineRechte();
	}
    $TAG = new awis_abgleich_funktionen();
    
    $AnsichtSuche = false;
    $AnsichtAnsicht = false;
    $ErzeugePDF = false;
    
    if (isset($_POST['cmdSuche_x']) or isset($_POST['cmdPdf_x'])) {
        $AnsichtAnsicht = true;
        if (isset($_POST['cmdPdf_x'])) {
            $ErzeugePDF = true;
        }
    } else {
        if (isset($_REQUEST['txtANSICHT'])) { //wegen Block
            $AnsichtAnsicht = true;
        } else {
            $AnsichtSuche = true;
        }
    }
    
    if (isset($_GET['Storno'])) {
        $SQL =  'select * from tuevabgleich where tag_key = :var_N0_tag_key';
        $DB->SetzeBindevariable('TAG', 'var_N0_tag_key', $_GET['Storno'], awisDatenbank::VAR_TYP_GANZEZAHL);
        $rsStorno = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TAG'));

        if (! $rsStorno->EOF()) {
            $DB->SetzeBindevariable('TGD', 'var_N0_tgd_key', $rsStorno->FeldInhalt('TAG_TGD_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
            $SQL =  'update tuevgesellschaftendaten set ' .
                    'tgd_abgeglichen = 0 ' .
                    'where tgd_key = :var_N0_tgd_key';
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('TGD'));
            
            $DB->SetzeBindevariable('TKD', 'var_N0_tkd_key', $rsStorno->FeldInhalt('TAG_TKD_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
            $SQL =  'update tuevkassendaten set ' .
                    'tkd_abgeglichen = 0 ' .
                    'where tkd_key = :var_N0_tkd_key';
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('TKD'));
            
            $DB->SetzeBindevariable('TAG', 'var_N0_tag_key', $rsStorno->FeldInhalt('TAG_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
            $SQL =  'delete from tuevabgleich where tag_key = :var_N0_tag_key';
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('TAG'));            
        }
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAnsicht: ' . $AnsichtAnsicht, 'ErzeugePDF: ' . $ErzeugePDF);
$Form->DebugAusgabe(1, $_REQUEST);

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_TAG')) < 8)) {
        $SuchParam['SuchenInKey'] = 1;
        $SuchParam['BelegNr'] = '';
        $SuchParam['BelegNrText'] = '';
        $SuchParam['BelegDatumVon'] = '';
        $SuchParam['BelegDatumBis'] = '';
        $SuchParam['AbgleichArtKey'] = 0;
        $SuchParam['DatumVon'] = '';
        $SuchParam['DatumBis'] = '';
        $SuchParam['FilId'] = '';
        $SuchParam['TuevNr'] = '';
        $SuchParam['TuevArtKey'] = 0;
        $SuchParam['Kfz'] = '';
        $SuchParam['Name'] = '';
        $SuchParam['PreisVon'] = '';
        $SuchParam['PreisBis'] = '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_TAG", serialize($SuchParam));
	}    
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_TAG'));
	if ($AnsichtAnsicht and !$ErzeugePDF) {
        if(isset($_POST['sucTAG_BELEGNR']) and $_POST['sucTAG_BELEGNR'] != '' and $_POST['sucTAG_BELEGNR'] != '::bitte w�hlen::') {
            $Daten = explode('|',$_POST['sucTAG_BELEGNR']);
        } else {
            $Daten[0] = '';
            $Daten[1] = '';
        }
        $SuchParam['SuchenInKey'] = (isset($_POST['sucTAG_SUCHENINKEY'])? $_POST['sucTAG_SUCHENINKEY']: $SuchParam['SuchenInKey']);
        $SuchParam['BelegNr'] = (isset($_POST['sucTAG_BELEGNR'])? $Daten[0]: $SuchParam['BelegNr']);
        $SuchParam['BelegDatum'] = (isset($_POST['sucTAG_BELEGNR'])? $Daten[1]: $SuchParam['BelegDatum']);
        $SuchParam['BelegNrText'] = (isset($_POST['sucTAG_BELEGNRTEXT'])? $_POST['sucTAG_BELEGNRTEXT']: $SuchParam['BelegNrText']);
        $SuchParam['BelegDatumVon'] = (isset($_POST['sucTAG_BELEGDATUMVON'])? $_POST['sucTAG_BELEGDATUMVON']: $SuchParam['BelegDatumVon']);
        $SuchParam['BelegDatumBis'] = (isset($_POST['sucTAG_BELEGDATUMBIS'])? $_POST['sucTAG_BELEGDATUMBIS']: $SuchParam['BelegDatumBis']);
        $SuchParam['AbgleichArtKey'] = (isset($_POST['sucTAG_ABGLEICHARTKEY'])? $_POST['sucTAG_ABGLEICHARTKEY']: $SuchParam['AbgleichArtKey']);
        $SuchParam['DatumVon'] = (isset($_POST['sucTAG_DATUMVON'])? $_POST['sucTAG_DATUMVON']: $SuchParam['DatumVon']);
        $SuchParam['DatumBis'] = (isset($_POST['sucTAG_DATUMBIS'])? $_POST['sucTAG_DATUMBIS']: $SuchParam['DatumBis']);
        $SuchParam['FilId'] = (isset($_POST['sucTAG_FILNR'])? $_POST['sucTAG_FILNR']: $SuchParam['FilId']);
        $SuchParam['TuevNr'] = (isset($_POST['sucTAG_TUEVNR'])? $_POST['sucTAG_TUEVNR']: $SuchParam['TuevNr']);
        $SuchParam['TuevArtKey'] = (isset($_POST['sucTAG_TUEVARTKEY'])? $_POST['sucTAG_TUEVARTKEY']: $SuchParam['TuevArtKey']);
        $SuchParam['Kfz'] = (isset($_POST['sucTAG_KFZKENNZ'])? $_POST['sucTAG_KFZKENNZ']: $SuchParam['Kfz']);
        $SuchParam['Name'] = (isset($_POST['sucTAG_NAME'])? $_POST['sucTAG_NAME']: $SuchParam['Name']);
        $SuchParam['PreisVon'] = (isset($_POST['sucTAG_PREISVON'])? $_POST['sucTAG_PREISVON']: $SuchParam['PreisVon']);
        $SuchParam['PreisBis'] = (isset($_POST['sucTAG_PREISBIS'])? $_POST['sucTAG_PREISBIS']: $SuchParam['PreisBis']);
        $SuchParam['SPEICHERN'] = isset($_POST['sucTAG_AUSWAHLSPEICHERN'])? 'on': 'off';
		$AWISBenutzer->ParameterSchreiben("Formular_TAG", serialize($SuchParam));
	}
    if ($ErzeugePDF) {
        $_GET['XRE'] = '32';        // Berichtskey
        $_GET['ID'] = base64_encode($TAG->ErstellePDFBedingung($SuchParam));
        include('../berichte/drucken.php');
    }    
    
    $Form->SchreibeHTMLCode('<form name=frmAbgeglichen action=./abgleich_Main.php?cmdAktion=Abgeglichen' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
    $Form->DebugAusgabe(1, $SuchParam);

    $TAG->Feld_SelectTGDoderTKD($SuchParam, $AnsichtSuche);
    $Form->Trennzeile('O');
    $TAG->Feld_SelectBelegNr($SuchParam, $AnsichtSuche, true, true);
    $TAG->Feld_BelegDatumVonBis($SuchParam, $AnsichtSuche);
    $TAG->Feld_SelectAbgleichArt($SuchParam, $AnsichtSuche);
    $TAG->Feld_DatumVonBis($SuchParam, $AnsichtSuche);
    $TAG->Feld_SelectFilId($SuchParam, $AnsichtSuche, 3);
    $TAG->Feld_SelectTuevNr($SuchParam, $AnsichtSuche);
    $TAG->Feld_SelectTuevArt($SuchParam, $AnsichtSuche);
    $TAG->Feld_KfzKennz($SuchParam, $AnsichtSuche);
    $TAG->Feld_Name($SuchParam, $AnsichtSuche);
    $TAG->Feld_PreisVonBis($SuchParam, $AnsichtSuche);
    $TAG->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    
    if ($AnsichtAnsicht) {
        $Form->Trennzeile('L');
        
        $Sort= '';
        if(isset($_GET['TAGSort'])) {
        	$Sort = $_GET['TAGSort'];
        }
        elseif(isset($_REQUEST['txtTAGSort'])) {
        	$Sort = $_REQUEST['txtTAGSort'];
        }		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Abgeglichen' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['TAG']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        $SQL =  'select  tag.*, tgd.*, tkd.*, taa.*,tgd_tgk.tgk_bezahlt, ' . 
                        'tue_tgd.tue_bez as tue_tgd_bez, fil_tgd.fil_bez as fil_tgd_bez, tua_tgd.tua_bez as tua_tgd_bez, ' . 
                        'tue_tkd.tue_bez as tue_tkd_bez, fil_tkd.fil_bez as fil_tkd_bez, tua_tkd.tua_bez as tua_tkd_bez, ' .
                        'row_number() over (';
        if ($Sort != '') {		// wenn GET-Sort, dann nach diesen Feld sortieren
            $SQL .= ' order by ' . str_replace('~',' DESC ',$Sort);
        } else {		// sonst pauschal nach Abgleichdatum
            $SQL .= ' order by tgd_fil_id,tgd_pruefdatum';
        }                
        $SQL .= ') as zeilennr ' .
                'from tuevabgleich tag ' . 
                'left join tuevgesellschaftendaten tgd on tgd.tgd_key = tag.tag_tgd_key ' . 
                'left join tuevkassendaten tkd on tkd.tkd_key = tag.tag_tkd_key ' .
                'left join tuevabgleichart taa on taa.taa_key = tag.tag_taa_key ' .
                'left join tuevorganisationen tue_tgd on tue_tgd.tue_id = tgd.tgd_tuevnr ' . 
                'left join filialen fil_tgd on fil_tgd.fil_id = tgd.tgd_fil_id ' . 
                'left join tuevarten tua_tgd on tua_tgd.tua_id = tgd.tgd_tuevart ' . 
                'left join tuevorganisationen tue_tkd on tue_tkd.tue_id = tkd.tkd_tuevnr ' . 
                'left join filialen fil_tkd on fil_tkd.fil_id = tkd.tkd_fil_id ' . 
                'left join tuevarten tua_tkd on tua_tkd.tua_id = tkd.tkd_tuevart '.
      		    'left join tuevgesellschaftenkopf tgd_tgk on tgd_tgk.tgk_belegnr = tgd.tgd_belegnr and tgd_tgk.tgk_belegdatum = tgd.tgd_belegdatum';
            $Bedingung = ' and tag_userdat >= (sysdate - 366)';         // nur Datens�tze der letzten 366 Tage anzeigen
            $Bedingung .= $TAG->ErstelleTAGBedingung($SuchParam);
            if ($Bedingung != '') {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }
        $Form->DebugAusgabe(1, $SQL);
        $MaxDS = 1;
        $ZeilenProSeite=1;
        $Block = 1;
        // Zum Bl�ttern in den Daten
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
        $Form->DebugAusgabe(1, $SQL);
        $rsTAG = $DB->RecordSetOeffnen($SQL);
        // Spaltenbreiten f�r Listenansicht
        $FeldBreiten = array();
        $FeldBreitenO['ChkBox'] = 20;
        $FeldBreitenO['AbgleichArt'] = 350;
        $FeldBreitenO['BelegNr'] = 140;
        $FeldBreitenO['BelegDatum'] = 740;
        $FeldBreitenO['Bemerkung'] = 270;
        $FeldBreitenO['User'] = 100;
        $FeldBreitenO['UserDat'] = 177;
        $FeldBreitenO['Storno'] = 33;
        $FeldBreitenU['Betrag'] = 80;
        $FeldBreitenU['TuevNr'] = 80;
        $FeldBreitenU['FilNr'] = 70;
        $FeldBreitenU['Datum'] = 180;
        $FeldBreitenU['Kfz'] = 150;
        $FeldBreitenU['Name'] = 280;
        $FeldBreitenU['TuevArt'] = 74;

        $Gesamtbreite = 0;
        if($rsTAG->AnzahlDatensaetze() > 4) {
            $FeldBreitenO['Balken'] = 17;
            $FeldBreitenU['Balken'] = 17;
        } else {
            $Gesamtbreite += 2;
        }
        foreach ($FeldBreitenO as $value) {
            $Gesamtbreite += $value + 2.3;
        }

        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreitenO['ChkBox']-4);
        // �berschrift der Listenansicht mit Sortierungslink: FilNr
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_FIL_ID'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['FilId'], $FeldBreitenU['FilNr'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Pr�fdatum
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_PRUEFDATUM'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_PRUEFDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Datum'], $FeldBreitenU['Datum'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_KFZKENNZ'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_KFZKENNZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['KfzKennz'], $FeldBreitenU['Kfz'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Name
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_NACHNAME'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Name'], $FeldBreitenU['Name'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_PREIS'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_PREIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Preis'], $FeldBreitenU['Betrag'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: T�V-Nr
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_TUEVNR'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_TUEVNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevNr'], $FeldBreitenU['TuevNr'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: T�V-Art
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TGD_TUEVART'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_TUEVART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevArt'], $FeldBreitenU['TuevArt'], '', $Link);
        if($rsTAG->AnzahlDatensaetze() > 4) {
            $Form->Erstelle_Liste_Ueberschrift('', $FeldBreitenO['Balken']);
        }
        $Form->ZeileEnde();
        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Abgleichart
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TAA_KENNUNG'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TAA_KENNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['AbgleichArt'], $FeldBreitenO['AbgleichArt'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Bemerkung
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TAG_BEMERKUNG'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TAG_BEMERKUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Bemerkung'], $FeldBreitenO['Bemerkung'], '', $Link);                
        // �berschrift der Listenansicht mit Sortierungslink: User
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TAG_USER'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TAG_USER'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['User'], $FeldBreitenO['User'], '', $Link);                
        // �berschrift der Listenansicht mit Sortierungslink: Userdat
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&TAGSort=TAG_USERDAT'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TAG_USERDAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['UserDat'], $FeldBreitenO['UserDat'], '', $Link);                        
        // �berschrift der Listenansicht mit Sortierungslink: Storno
        $Link = '';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Storno'], $FeldBreitenO['Storno'], 'font-weight:bolder;', $Link, $AWISSprachKonserven['TAG']['TTTStorno'], 'C');
        if($rsTAG->AnzahlDatensaetze() > 4) {
            $Form->Erstelle_Liste_Ueberschrift('', $FeldBreitenO['Balken']);
        }
        $Form->ZeileEnde();
        
        $DS = 0;	// f�r Hintergrundfarbumschaltung
        if($rsTAG->AnzahlDatensaetze() > 4) {
            $Form->ZeileStart();
            $Form->ScrollBereichStart('scrKasse',250,948,true,false);
        }
        while(! $rsTAG->EOF()) {
            $Link = '';
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_BELEGNR') . ' (' . $Form->Format('D', $rsTAG->FeldInhalt('TGD_BELEGDATUM')) . ')';
            $Form->Erstelle_ListenFeld('TAG_BELEGNR',$AWISSprachKonserven['TAG']['BelegNr'].': '. $rsTAG->FeldInhalt('TGD_BELEGNR'), 0, $FeldBreitenO['BelegNr']+50, false, ($DS%2), 'font-weight:bold;', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $Form->Format('D', $rsTAG->FeldInhalt('TGD_BELEGDATUM')) . ' (' . $rsTAG->FeldInhalt('TGD_BELEGNR') . ')';
            $Form->Erstelle_ListenFeld('TGD_BELEGDATUM', $AWISSprachKonserven['TAG']['BelegDatum'].': '.$Form->Format('D', $rsTAG->FeldInhalt('TGD_BELEGDATUM')), 0, $FeldBreitenO['BelegDatum'], false, ($DS%2),'font-weight:bold;', $Link, 'T', 'L', $TTT);
            $Form->ZeileEnde();
            
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'];
            $Form->Erstelle_ListenFeld('TAG_DUMMY', 'I:', 0, $FeldBreitenO['ChkBox']-4, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_FIL_ID') . ' - ' . $rsTAG->FeldInhalt('FIL_TGD_BEZ');
            $Form->Erstelle_ListenFeld('TGD_FIL_ID', $rsTAG->FeldInhalt('TGD_FIL_ID'), 0, $FeldBreitenU['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $Form->Format('D', $rsTAG->FeldInhalt('TGD_PRUEFDATUM'));
            $Form->Erstelle_ListenFeld('TGD_PRUEFDATUM', $Form->Format('D', $rsTAG->FeldInhalt('TGD_PRUEFDATUM')), 0, $FeldBreitenU['Datum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_KFZKENNZ') . ' (' . $rsTAG->FeldInhalt('TGD_NACHNAME') . ')';
            $Form->Erstelle_ListenFeld('TGD_KFZKENNZ', $rsTAG->FeldInhalt('TGD_KFZKENNZ'), 0, $FeldBreitenU['Kfz'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_NACHNAME') . ' (' . $rsTAG->FeldInhalt('TGD_KFZKENNZ') . ')';
            $Form->Erstelle_ListenFeld('TGD_NACHNAME', $rsTAG->FeldInhalt('TGD_NACHNAME'), 0, $FeldBreitenU['Name'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $Form->Format('N2', $rsTAG->FeldInhalt('TAG_PREIS'));
            $Form->Erstelle_ListenFeld('TAG_PREIS', $Form->Format('N2', $rsTAG->FeldInhalt('TGD_PREIS')), 0, $FeldBreitenU['Betrag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_TUEVNR') . ' - ' . $rsTAG->FeldInhalt('TUE_TGD_BEZ');
            $Form->Erstelle_ListenFeld('TGD_TUEVNR', $rsTAG->FeldInhalt('TGD_TUEVNR'), 0, $FeldBreitenU['TuevNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Inkasso'] . ': ' . $rsTAG->FeldInhalt('TGD_TUEVART') . ' (' . $rsTAG->FeldInhalt('TUA_TGD_BEZ') . ')';
            $Form->Erstelle_ListenFeld('TGD_TUEVART', $rsTAG->FeldInhalt('TGD_TUEVART'), 0, $FeldBreitenU['TuevArt'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['TAG']['Kasse'];
            $Form->Erstelle_ListenFeld('TAG_DUMMY', 'K:', 0, $FeldBreitenO['ChkBox']-4, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $rsTAG->FeldInhalt('TKD_FIL_ID') . ' - ' . $rsTAG->FeldInhalt('FIL_TKD_BEZ');
            $Form->Erstelle_ListenFeld('TKD_FIL_ID', $rsTAG->FeldInhalt('TKD_FIL_ID'), 0, $FeldBreitenU['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $Form->Format('DU', $rsTAG->FeldInhalt('TKD_DATUMZEIT'));
            $Form->Erstelle_ListenFeld('TKD_DATUMZEIT', $Form->Format('D', $rsTAG->FeldInhalt('TKD_DATUMZEIT')), 0, $FeldBreitenU['Datum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $rsTAG->FeldInhalt('TKD_KFZKENNZ') . ' (' . $rsTAG->FeldInhalt('TKD_NAME') . ')';
            $Form->Erstelle_ListenFeld('TKD_KFZKENNZ', $rsTAG->FeldInhalt('TKD_KFZKENNZ'), 0, $FeldBreitenU['Kfz'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $rsTAG->FeldInhalt('TKD_NAME') . ' (' . $rsTAG->FeldInhalt('TKD_KFZKENNZ') . ')';
            $Form->Erstelle_ListenFeld('TKD_NAME', $rsTAG->FeldInhalt('TKD_NAME'), 0, $FeldBreitenU['Name'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $Form->Format('N2', $rsTAG->FeldInhalt('TKD_BETRAG'));
            $Form->Erstelle_ListenFeld('TKD_BETRAG', $Form->Format('N2', $rsTAG->FeldInhalt('TKD_BETRAG')), 0, $FeldBreitenU['Betrag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $rsTAG->FeldInhalt('TKD_TUEVNR') . ' - ' . $rsTAG->FeldInhalt('TUE_TKD_BEZ');
            $Form->Erstelle_ListenFeld('TKD_TUEVNR', $rsTAG->FeldInhalt('TKD_TUEVNR'), 0, $FeldBreitenU['TuevNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Kasse'] . ': ' . $rsTAG->FeldInhalt('TKD_TUEVART') . ' (' . $rsTAG->FeldInhalt('TUA_TKD_BEZ') . ')';
            $Form->Erstelle_ListenFeld('TKD_TUEVART', $rsTAG->FeldInhalt('TKD_TUEVART'), 0, $FeldBreitenU['TuevArt'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $Form->ZeileEnde();
            
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['TAG']['AbgleichArt'] . ': ' . $rsTAG->FeldInhalt('TAA_KENNUNG') . ' - ' . $rsTAG->FeldInhalt('TAA_BESCHREIBUNG');
            $Form->Erstelle_ListenFeld('TAG_ABLEICHART', $rsTAG->FeldInhalt('TAA_BESCHREIBUNG'), 0, $FeldBreitenO['AbgleichArt'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Abgleich'] . ': ' . $rsTAG->FeldInhalt('TAG_BEMERKUNG');
            $Form->Erstelle_ListenFeld('TAG_BEMERKUNG', $rsTAG->FeldInhalt('TAG_BEMERKUNG'), 0, $FeldBreitenO['Bemerkung'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Abgleich'] . ': ' . $rsTAG->FeldInhalt('TAG_USER') . ' (' . $Form->Format('DU', $rsTAG->FeldInhalt('TAG_USERDAT')) . ')';
            $Form->Erstelle_ListenFeld('TAG_USER', $rsTAG->FeldInhalt('TAG_USER'), 0, $FeldBreitenO['User'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['Abgleich'] . ': ' . $Form->Format('DU', $rsTAG->FeldInhalt('TAG_USERDAT')) . ' (' . $rsTAG->FeldInhalt('TAG_USER') . ')';
            $Form->Erstelle_ListenFeld('TAG_USERDAT', $rsTAG->FeldInhalt('TAG_USERDAT'), 0, $FeldBreitenO['UserDat'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['TAG']['TTTStornoTTT'];
            if ($rsTAG->FeldInhalt('TGK_BEZAHLT') == 0)
            {
            	$Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Ansicht&Storno='.$rsTAG->FeldInhalt('TAG_KEY').(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            	$Form->Erstelle_ListenFeld('TAG_STORNO', $AWISSprachKonserven['TAG']['Storno'], 0, $FeldBreitenO['Storno'], false, ($DS%2), '', $Link, 'T', 'C', $TTT);
            	$Link = '';
            }
            else
            {
            	$Form->Erstelle_ListenFeld('TAG_STORNO', '', 0, $FeldBreitenO['Storno'], false, ($DS%2), '', $Link, 'T', 'C', $TTT);
            }
            $Form->ZeileEnde();
            
            $DS++;
            $rsTAG->DSWeiter();                
        }
        if($rsTAG->AnzahlDatensaetze() > 4) {
            $Form->ScrollBereichEnde();
            $Form->ZeileEnde();
        }
        $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['TAG']['SummeGesamtDS'].':', $Gesamtbreite-1170, 'font-weight:bolder;');
        $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsTAG->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt+ $DS).' / '.$Form->Format('N0',$MaxDS), 250, 'font-weight:bolder;','','','R');
      
        $Form->ZeileEnde();   
           
        $Link = './abgleich_Main.php?cmdAktion=Abgeglichen&txtANSICHT=Auswahl'.'&TAGSort='.$Sort;;        // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
        $Form->Erstelle_HiddenFeld('ANSICHT', 'Ansicht');                       // f�r direkte Auswahl der Seite
        $Form->Erstelle_HiddenFeld('TAGSort', $Sort);
    }
    
	$Form->Formular_Ende();
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht23030&1) == 1)
    and ($AnsichtSuche)) {
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}
	$SQL = '';
	$SQL =  'select tgd_belegnr ' .
			'from tuevabgleich tag ' .
			'left join tuevgesellschaftendaten tgd on tgd.tgd_key = tag.tag_tgd_key ' .
			'left join tuevkassendaten tkd on tkd.tkd_key = tag.tag_tkd_key ' .
			'left join tuevabgleichart taa on taa.taa_key = tag.tag_taa_key ' .
			'left join tuevorganisationen tue_tgd on tue_tgd.tue_id = tgd.tgd_tuevnr ' .
			'left join filialen fil_tgd on fil_tgd.fil_id = tgd.tgd_fil_id ' .
			'left join tuevarten tua_tgd on tua_tgd.tua_id = tgd.tgd_tuevart ' .
			'left join tuevorganisationen tue_tkd on tue_tkd.tue_id = tkd.tkd_tuevnr ' .
			'left join filialen fil_tkd on fil_tkd.fil_id = tkd.tkd_fil_id ' .
			'left join tuevarten tua_tkd on tua_tkd.tua_id = tkd.tkd_tuevart';
	$Bedingung = ' and tag_userdat >= (sysdate - 366)';         // nur Datens�tze der letzten 366 Tage anzeigen
	$Bedingung .= $TAG->ErstelleTAGBedingung($SuchParam);
	if ($Bedingung != '') {
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$SQL .= 'group by tgd_belegnr';
	$rsAB = $DB->RecordSetOeffnen($SQL);

    $SQL =  'select * ' .
            ' from tuevgesellschaftendaten ';
    $Bedingung  = ' and tgd_belegnr =\'' .$rsAB->FeldInhalt('TGD_BELEGNR').'\'';
    $Bedingung .= ' and tgd_abgeglichen = 0';
     $TAG->ErstelleTAGBedingung($SuchParam);
    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $rsPDF = $DB->RecordSetOeffnen($SQL);
    if ((($Recht23030&1) == 1)
    and ($AnsichtAnsicht) and $MaxDS <= 10000) {
        $LinkPDF = '/berichte/drucken.php?XRE=32&ID='.base64_encode($TAG->ErstellePDFBedingung($SuchParam));
        $Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular) {
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	} else {
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular) {
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	} else {
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>