<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		$Tabelle = 'PEB';
		$Key=$_POST['txtPEB_KEY'];
		$PEBKey=$_POST['txtPEB_KEY'];

		$AWIS_KEY1 = $Key;

		$Felder=array();

		$Felder[]=array($Form->LadeTextBaustein('PEB','PEB_BEZEICHNUNG'),$_POST['txtPEB_BEZEICHNUNG']);
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case 'Mitglieder':
					$Tabelle = 'PBM';
					$Key=$_GET['Del'];

					$SQL = 'SELECT PBM_KEY, PBM_PEB_KEY, XBN_NAME, XBN_VORNAME';
					$SQL .= ' FROM PerseinsBereicheMitglieder ';
					$SQL .= ' LEFT OUTER JOIN Benutzer ON PBM_XBN_KEY = XBN_KEY';
					$SQL .= ' WHERE PBM_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$PEBKey = $rsDaten->FeldInhalt('PBM_PEB_KEY');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_NAME'),$rsDaten->FeldInhalt('XBN_NAME'));
					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_VORNAME'),$rsDaten->FeldInhalt('XBN_VORNAME'));
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'PEB':
				$SQL = 'DELETE FROM PerseinsBereiche WHERE PEB_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'PBM':
				$SQL = 'DELETE FROM PerseinsBereicheMitglieder WHERE PBM_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtPEBKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('personaleinsaetze_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./personaleinsaetze_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('PEBKey',$PEBKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>