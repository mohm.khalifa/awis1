<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVT','SVT_*');
	$TextKonserven[]=array('SVL','SVL_*');
	$TextKonserven[]=array('FIL','FIL_BEZ');
	$TextKonserven[]=array('SVB','SVB_VERTRAGSNUMMER');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Jahr');
	$TextKonserven[]=array('TITEL','tit_SERVICELEISTUNGEN');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19003 = $AWISBenutzer->HatDasRecht(19003);
	if($Recht19003==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	//**********************************************************
	// Export der aktuellen Daten f�r das ausgew�hlte Jahr
	//**********************************************************
	if(isset($_POST['txtjahr']))
	{
		ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
		ini_set('max_execution_time', 600);
		require_once('PHPExcel.php');

		$DateiName = 'Serviceleistungen';
		$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);
		
		@ob_end_clean();
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Expires: 01 Jan 2000");
		header('Pragma: public');
		header('Cache-Control: max-age=0');

		switch ($ExportFormat)
		{
			case 1:                 // Excel 5.0
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
				break;
			case 2:                 // Excel 2007
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
				break;
		}
		$XLSXObj = new PHPExcel();
		$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
		$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
		$XLSXObj->getProperties()->setTitle(utf8_encode($DateiName));
		$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
		$XLSXObj->getProperties()->setDescription(utf8_encode($AWISSprachKonserven['TITEL']['tit_SERVICELEISTUNGEN']));
		
		$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

		// Alle Informationen sammeln
		$SQL = 'SELECT SVB_KEY, SVB_VERTRAGSNUMMER, SVA_BEZEICHNUNG';
		$SQL .= ', SVT_BEZEICHNUNG, SVT_KENNUNG, SVT_KEY';
		$SQL .= ', SVL_DATUM, SVL_FIL_ID, FIL_BEZ';
		$SQL .= ', CASE WHEN SVT_DATUMBIS >= SYSDATE THEN 0 ELSE 1 END AS ABGELAUFEN';
		$SQL .= ' FROM SVCVEREINBARUNGEN';
		$SQL .= ' INNER JOIN SVCARTIKEL ON SVB_SVA_KEY = SVA_KEY';
		$SQL .= ' INNER JOIN SVCTEILNEHMER ON SVT_SVB_KEY = SVB_KEY';
		$SQL .= ' LEFT OUTER JOIN SVCLEISTUNGEN ON SVT_KEY = SVL_SVT_KEY';
		$SQL .= ' 		AND (SVL_DATUM IS NULL OR TO_CHAR(SVL_DATUM,\'RRRR\') = :var_T_JAHR)';
		$SQL .= ' LEFT OUTER JOIN FILIALEN ON SVL_FIL_ID = FIL_ID';
		$SQL .= ' WHERE SVB_KEY = :var_N0_SVB_KEY';
		$SQL .= ' ORDER BY SVT_BEZEICHNUNG, SVL_DATUM';
		
		$DB->SetzeBindevariable('SVB', 'var_N0_SVB_KEY', $_POST['txtSVB_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
		$DB->SetzeBindevariable('SVB', 'var_T_JAHR', $_POST['txtjahr'], awisDatenbank::VAR_TYP_TEXT);
		$rsSVB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SVB'));		
		
		// Ermitteln, wie oft ein Teilnehmer kommen muss
		$SQL = 'SELECT SBZ_KEY,SBZ_BEZEICHNUNG,SBZ_TAGVON,SBZ_MONATVON';
		$SQL .= ', TO_DATE(SBZ_TAGVON || \'.\' || SBZ_MONATVON || \'.\' || :var_T_JAHR) AS DATUMVOM';
		$SQL .= ', TO_DATE(SBZ_TAGBIS || \'.\' || SBZ_MONATBIS || \'.\' || :var_T_JAHR) AS DATUMBIS';
		$SQL .= ', ROW_NUMBER() OVER(ORDER BY SBZ_MONATVON) AS Block';
		$SQL .= ' FROM SVCZEITRAEUME ';
		$SQL .= ' WHERE SBZ_KEY IN (SELECT DISTINCT SAB_SBZ_KEY';
		$SQL .= ' FROM AWIS.SVCVEREINBARUNGEN';
		$SQL .= ' INNER JOIN AWIS.SVCARTIKEL ON SVB_SVA_KEY = SVA_KEY';
		$SQL .= ' INNER JOIN AWIS.SVCARTIKELBENACHRICHTIGUNGEN ON SAB_SVA_KEY = SVA_KEY';
		$SQL .= ' WHERE SVB_KEY = :var_N0_SVB_KEY )';
		$SQL .= ' ORDER BY SBZ_MONATVON';
		$DB->SetzeBindevariable('SVB', 'var_T_JAHR', $_POST['txtjahr'], awisDatenbank::VAR_TYP_TEXT);
		$DB->SetzeBindevariable('SVB', 'var_N0_SVB_KEY', $_POST['txtSVB_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
		$rsSAB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SVB'));
		while(!$rsSAB->EOF())
		{
			$ServiceBloecke[$rsSAB->FeldInhalt('BLOCK')]= $Form->PruefeDatum($rsSAB->FeldInhalt('DATUMBIS'),false,true,true);
			$ServiceBloeckeAnzeige[$rsSAB->FeldInhalt('BLOCK')]= 'Zeitraum vom '.$Form->Format('D',$rsSAB->FeldInhalt('DATUMVOM')).' - '.$Form->Format('D',$rsSAB->FeldInhalt('DATUMBIS'));
			$rsSAB->DSWeiter();
		}

		$XLSXObj->setActiveSheetIndex(0);
		$XLSXObj->getActiveSheet()->setTitle(utf8_encode($DateiName));
		$SpaltenNr = 0;
		$ZeilenNr = 1;		

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($rsSVB->FeldInhalt('SVA_BEZEICHNUNG')));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(14);
		$ZeilenNr++;
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['SVB']['SVB_VERTRAGSNUMMER']));
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr+1,$ZeilenNr, utf8_encode($rsSVB->FeldInhalt('SVB_VERTRAGSNUMMER')));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$ZeilenNr++;
		
		// Daten zeilenweise exportieren
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['SVT']['SVT_BEZEICHNUNG']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['SVT']['SVT_KENNUNG']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Inaktiv');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		for($i=1;$i<=count($ServiceBloecke);$i++)
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr-1, utf8_encode($ServiceBloeckeAnzeige[$i]));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr-1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr-1)->getFill()->getStartColor()->setARGB('FFE0E0E0');
			$XLSXObj->getActiveSheet()->mergeCellsByColumnAndRow($SpaltenNr,$ZeilenNr-1,$SpaltenNr+2,$ZeilenNr-1);

			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['SVL']['SVL_DATUM']));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
			$SpaltenNr++;
			
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['SVL']['SVL_FIL_ID']));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
			$SpaltenNr++;
			
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['FIL']['FIL_BEZ']));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
			$SpaltenNr++;
		}
				
		$SpaltenNr=0;
		$TN=array();
		while(!$rsSVB->EOF())
		{
			
			if(isset($TN[$rsSVB->FeldInhalt('SVT_KEY')]))
			{
				$Zeile=$TN[$rsSVB->FeldInhalt('SVT_KEY')];
			}
			else 
			{
				$TN[$rsSVB->FeldInhalt('SVT_KEY')] = $Zeile = ++$ZeilenNr;
				
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, utf8_encode($rsSVB->FeldInhalt('SVT_BEZEICHNUNG')));
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, utf8_encode($rsSVB->FeldInhalt('SVT_KENNUNG')));
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, $rsSVB->FeldInhalt('ABGELAUFEN'));
			}

			foreach($ServiceBloecke AS $BlockNr=>$BlockDatum)
			{
				if($Form->PruefeDatum($rsSVB->FeldInhalt('SVL_DATUM'),false,true,true) <= $BlockDatum)
				{
					$SpaltenNr = (($BlockNr-1)*3)+3;
					break;
				}
			}
				
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, utf8_encode($Form->Format('D',$rsSVB->FeldInhalt('SVL_DATUM'))));
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, utf8_encode($rsSVB->FeldInhalt('SVL_FIL_ID')));
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$Zeile, utf8_encode($rsSVB->FeldInhalt('FIL_BEZ')));
			
			$SpaltenNr=0;
			
			$rsSVB->DSWeiter();
		}
		for($S='A';$S<='I';$S++)
		{
			$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
		}
		
		// Verschiedene Formate erfordern andere Objekte
		switch ($ExportFormat)
		{
			case 1:                 // Excel 5.0
				$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
				break;
			case 2:                 // Excel 2007
				$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
				break;
		}
		$objWriter->save('php://output');
		$XLSXObj->disconnectWorksheets();
	}
}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}
?>