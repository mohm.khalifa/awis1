<?php
//**************************************************************
// Liefert die Daten f�r das Verbundfeld in CRM Details
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


if(!isset($_GET['FILTER']))
{
	die('##PARAM##');
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


$SQL = 'SELECT KHERNR,BEZ from kfz_herst ';

$Bedingung = '';

if($_GET['FILTER'] != '')
{
	$Bedingung .= ' AND (UPPER(BEZ) '.$DB->LikeOderIst($_GET['FILTER'].'*',awisDatenbank::AWIS_LIKE_UPPER);
	$Bedingung .= ' OR KHERNR in(select KHERNR from kfz_modell where KMODNR in(select kmodnr from kfz_typ where ktypnr in(select ktypnr from kfz_kbanr where UPPER(KBA_NR)'.$DB->LikeOderIst($_GET['FILTER'].'*',awisDatenbank::AWIS_LIKE_UPPER).'))))';
}
if(isset($_GET['PKS_KMODNR']) and $_GET['PKS_KMODNR'] != '::bitte w�hlen::' and $_GET['PKS_KMODNR'] != 'B')
{
	$Bedingung .= ' AND KHERNR in(select KHERNR from kfz_modell where KMODNR ='.$_GET['PKS_KMODNR'].')';
}
if(isset($_GET['PKS_KTYPNR']) and $_GET['PKS_KTYPNR'] != '::bitte w�hlen::' and $_GET['PKS_KTYPNR'] != 'B')
{
	$Bedingung .= ' AND KHERNR in(select KHERNR from kfz_modell where KMODNR in(select kmodnr from kfz_typ where ktypnr ='.$_GET['PKS_KTYPNR'].'))';
}

if($Bedingung != '')
{
	$SQL .= ' WHERE '.substr($Bedingung, 4);
}

$SQL .= ' order by bez';


$rsPKSDaten=$DB->RecordSetOeffnen($SQL);



echo '#~#';		// Trennzeichen
$DS=0;
if(isset($_GET['ZUSATZ'])!='')
{
	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else 
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}
while(!$rsPKSDaten->EOF())
{
	if(++$DS>200 AND $rsPKSDaten->FeldInhalt('BEZ')==$_GET['FILTER'])
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPKSDaten->FeldInhalt('KHERNR').'">'.$rsPKSDaten->FeldInhalt('BEZ').'</option>';
		}
		else 
		{
			echo '#+#'.$rsPKSDaten->FeldInhalt('KHERNR').'#+#'.$rsPKSDaten->FeldInhalt('BEZ').'';
		}
	}
	elseif($DS<200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPKSDaten->FeldInhalt('KHERNR').'">'.$rsPKSDaten->FeldInhalt('BEZ').'</option>';
		}
		else 
		{
			echo '#+#'.$rsPKSDaten->FeldInhalt('KHERNR').'#+#'.$rsPKSDaten->FeldInhalt('BEZ').'';
		}
	}
	
	$rsPKSDaten->DSWeiter();
}
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else 
	{
		echo '#+##+#...';
	}
	//break;
}
?>