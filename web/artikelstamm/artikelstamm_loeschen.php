<?php
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');

try {
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Tabelle = '';

    if (!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x'])) {
        if (isset($_POST['txtAST_KEY'])) {
            $Tabelle = 'AST';
            $Key = $_POST['txtAST_KEY'];

            $SQL = 'SELECT AST_ATUNR,AST_BEZEICHNUNG,AST_KEY';
            $SQL .= ' FROM Artikelstamm ';
            $SQL .= ' WHERE AST_KEY=0' . $Key;

            $rsDaten = $DB->RecordsetOeffnen($SQL);

            $ASTKey = $rsDaten->FeldInhalt('AST_KEY');

            $Felder = array();
            $Felder[] = array($Form->LadeTextBaustein('AST', 'AST_ATUNR'), $rsDaten->FeldInhalt('AST_ATUNR'));
            $Felder[] = array($Form->LadeTextBaustein('AST', 'AST_ASTBEZEICHNUNG'), $rsDaten->FeldInhalt('AST_ASTBEZEICHNUNG'));
        } elseif (isset($_POST['txtAPP_ID'])) {
            $Tabelle = 'APP';
            $Key = $_POST['txtAPP_ID'];

            $SQL = 'SELECT APP_ID, APP_NAME';
            $SQL .= ' FROM Artikelpruefpersonal ';
            $SQL .= ' WHERE APP_ID=0' . $Key;

            $rsDaten = $DB->RecordsetOeffnen($SQL);

            $ASTKey = '';

            $Felder = array();
            $Felder[] = array($Form->LadeTextBaustein('APP', 'APP_ID'), $rsDaten->FeldInhalt('APP_ID'));
            $Felder[] = array($Form->LadeTextBaustein('APP', 'APP_NAME'), $rsDaten->FeldInhalt('APP_NAME'));
        }
    } elseif (isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x'])) {
        if (isset($_GET['Unterseite'])) {
            switch ($_GET['Unterseite']) {
                case 'Lieferantenartikel':
                    $Tabelle = 'LARTEI';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];
                    $LARKey = $_GET['LAR_KEY'];

                    $SQL = 'SELECT LAR_LARTNR, LAR_LIE_NR FROM Lieferantenartikel ';
                    $SQL .= ' INNER JOIN TeileInfos ON TEI_KEY1 = LAR_KEY';
                    $SQL .= ' WHERE TEI_KEY=0' . $Key;
                    $rsDaten = $DB->RecordsetOeffnen($SQL);
                    $Felder = array();
                    $Felder[] = array($Form->LadeTextBaustein('LAR', 'LAR_LARTNR'), $rsDaten->FeldInhalt('LAR_LARTNR'));
                    $Felder[] = array($Form->LadeTextBaustein('LAR', 'LAR_LIE_NR'), $rsDaten->FeldInhalt('LAR_LIE_NR'));
                    break;
                case 'OENummern':
                    $Tabelle = 'OENTEI';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];
                    $LARKey = $_GET['LAR_KEY'];

                    $SQL = 'SELECT OEN_KEY, OEN_NUMMER FROM OENummern';
                    $SQL .= ' INNER JOIN TeileInfos ON TEI_KEY2 = OEN_KEY';
                    $SQL .= ' WHERE TEI_KEY=0' . $Key;

                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();
                    $Felder[] = array($Form->LadeTextBaustein('OEN', 'OEN_NUMMER'), $rsDaten->FeldInhalt('OEN_NUMMER'));
                    break;
                case 'Gebrauchsnummern':
                    $Tabelle = 'GNRTEI';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];
                    $LARKey = $_GET['LAR_KEY'];
                    $GNRKey = $_GET['GNR_KEY'];

                    $SQL = 'SELECT GNR_KEY, GNR_NUMMER FROM Gebrauchsnummern';
                    $SQL .= ' INNER JOIN TeileInfos ON TEI_KEY2 = GNR_KEY';
                    $SQL .= ' WHERE TEI_KEY=0' . $Key;

                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();
                    $Felder[] = array($Form->LadeTextBaustein('GNR', 'GNR_NUMMER'), $rsDaten->FeldInhalt('GNR_NUMMER'));
                    break;
            }
        } else {
            switch ($_GET['Seite']) {
                case 'Kommentare':
                    $Tabelle = 'KOMMENTARE';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];

                    $SQL = 'SELECT ASI_KEY, ASI_WERT, ASI_USER';
                    $SQL .= ' FROM ArtikelstammInfos ';
                    $SQL .= ' WHERE ASI_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('ASI', 'ASI_WERT'), $rsDaten->FeldInhalt('ASI_WERT'));
                    $Felder[] = array($Form->LadeTextBaustein('ASI', 'ASI_USER'), $rsDaten->FeldInhalt('ASI_USER'));
                    break;
                case 'OENummern':
                    $Tabelle = 'OENTEI';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];
                    $LARKey = $_GET['LAR_KEY'];

                    $SQL = 'SELECT OEN_NUMMER, HER_BEZEICHNUNG FROM OENummern INNER JOIN Hersteller ON OEN_HER_ID = HER_ID';
                    $SQL .= ' INNER JOIN TeileInfos ON TEI_KEY2 = OEN_KEY';
                    $SQL .= ' WHERE TEI_KEY=0' . $Key;
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('OEN', 'OEN_NUMMER'), $rsDaten->FeldInhalt('OEN_NUMMER'));
                    $Felder[] = array($Form->LadeTextBaustein('HER', 'HER_BEZEICHNUNG'), $rsDaten->FeldInhalt('HER_BEZEICHNUNG'));
                    break;
                case 'Lieferantenartikel':
                    $Tabelle = 'LARTEI';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];

                    $SQL = 'SELECT LAR_LARTNR, LAR_LIE_NR FROM Lieferantenartikel ';
                    $SQL .= ' INNER JOIN TeileInfos ON TEI_KEY2 = LAR_KEY';
                    $SQL .= ' WHERE TEI_KEY=0' . $Key;

                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('LAR', 'LAR_LARTNR'), $rsDaten->FeldInhalt('LAR_LARTNR'));
                    $Felder[] = array($Form->LadeTextBaustein('LAR', 'LAR_LIE_NR'), $rsDaten->FeldInhalt('LAR_LIE_NR'));
                    break;

                case 'Pruefungen':
                    $Tabelle = 'APR';
                    $Key = $_GET['Del'];
                    $ASTKey = $_GET['Key'];

                    $SQL = 'SELECT APR_KEY, APR_LAR_LARTNR, LAR_LARTNR ';
                    $SQL .= ' FROM Artikelpruefungen ';
                    $SQL .= ' LEFT OUTER JOIN Lieferantenartikel ON APR_LAR_KEY = LAR_KEY';
                    $SQL .= ' WHERE APR_KEY=0' . $Key;

                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $Felder = array();

                    $Felder[] = array(
                        $Form->LadeTextBaustein('LAR', 'LAR_LARTNR'),
                        ($rsDaten->FeldInhalt('LAR_LARTNR') == ''?$rsDaten->FeldInhalt('APR_LAR_LARTNR'):$rsDaten->FeldInhalt('LAR_LARTNR'))
                    );
                    break;
                case 'Artikelalternativen';
                    $Tabelle = 'AAL';
                    $Key = $_GET['AAL_KEY'];
                    $ASTKey = $AWIS_KEY1;

                    $SQL = 'SELECT AAL_KEY, AAL_ALTERNATIV_AST_ATUNR ';
                    $SQL .= ' FROM Artikelalternativen ';
                    $SQL .= ' WHERE AAL_KEY=' . $DB->WertSetzen('AAL', 'N0', $Key);

                    $rsDaten = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('AAL'));

                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('AAL', 'AAL_ALTERNATIV_AST_ATUNR'), ($rsDaten->FeldInhalt('AAL_ALTERNATIV_AST_ATUNR')));
                    break;
                default:
                    break;
            }
        }
    } elseif (isset($_POST['cmdLoeschenOK']))    // Loeschen durchführen
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) {
            case 'AST':
                $SQL = 'DELETE FROM Artikelstamm WHERE AST_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = 0;
                break;
            case 'LARTEI':
                $SQL = 'DELETE FROM Teileinfos WHERE tei_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtASTKey'];
                if (isset($_POST['txtLARKey'])) {
                    $AWIS_KEY2 = $_POST['txtLARKey'];
                }
                break;
            case 'OENTEI':
                $SQL = 'DELETE FROM Teileinfos WHERE tei_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtASTKey'];
                if (isset($_POST['txtLARKey'])) {
                    $AWIS_KEY2 = $_POST['txtLARKey'];
                }
                break;
            case 'GNRTEI':
                $SQL = 'BEGIN ';
                $SQL .= ' DELETE FROM Teileinfos WHERE tei_key=0' . $_POST['txtKey'] . ';';
                $SQL .= ' DELETE FROM Gebrauchsnummern WHERE gnr_key=0' . $_POST['txtGNRKey'] . ';';
                $SQL .= ' END;';
                $AWIS_KEY1 = $_POST['txtASTKey'];
                if (isset($_POST['txtLARKey'])) {
                    $AWIS_KEY2 = $_POST['txtLARKey'];
                }
                break;
            case 'KOMMENTARE':
                $SQL = 'DELETE FROM Artikelstamminfos WHERE asi_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtASTKey'];
                $AWIS_KEY2 = '';
                break;
            case 'APP':
                $SQL = 'DELETE FROM Artikelpruefpersonal WHERE APP_ID=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtAPP_ID'];
                $AWIS_KEY2 = '';
                break;
            case 'APR':
                $SQL = 'DELETE FROM Artikelpruefungen WHERE APR_KEY=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtASTKey'];
                $AWIS_KEY2 = '';
                break;
            case 'AAL':
                $SQL = 'DELETE FROM ARTIKELALTERNATIVEN WHERE AAL_KEY=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtASTKey'];
                $AWIS_KEY2 = '';
                break;
                break;

            default:
                break;
        }

        if ($SQL != '') {
            if ($DB->Ausfuehren($SQL) === false) {
                awisErrorMailLink('artikelstamm_loeschen_1', 1, $awisDBError['messages'], '');
            }
        }
    }

    if ($Tabelle != '') {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

        $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./artikelstamm_Main.php?cmdAktion=' . $_GET['cmdAktion'] . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

        $Form->Formular_Start();
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
        $Form->ZeileEnde();

        foreach ($Felder AS $Feld) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Feld[0] . ':', 150);
            $Form->Erstelle_TextFeld('Feld', $Feld[1], 100, 500, false);
            $Form->ZeileEnde();
        }

        $Form->Erstelle_HiddenFeld('ASTKey', $ASTKey);
        if (isset($LARKey)) {
            $Form->Erstelle_HiddenFeld('LARKey', $LARKey);
        }
        if (isset($GNRKey)) {
            $Form->Erstelle_HiddenFeld('GNRKey', $GNRKey);
        }
        $Form->Erstelle_HiddenFeld('Tabelle', $Tabelle);
        $Form->Erstelle_HiddenFeld('Key', $Key);

        $Form->Trennzeile();

        $Form->ZeileStart();
        $Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
        $Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
        $Form->ZeileEnde();

        $Form->SchreibeHTMLCode('</form>');

        $Form->Formular_Ende();

        die();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>