<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
require_once("operating_COM_config.php");

global $COMWebSrv;

global $comcon;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Errlog','OCE_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Errlog');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Errlog');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Errlog'].'</title>';
?>
</head>
<body>
<?php

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$Recht3114 = awisBenutzerRecht($con,3114);
if($Recht3114==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$comcon = awisLogonComCluster('DE',false);

echo '<form name=frmOperating_Errlog action=./operating_COM_Main.php?cmdAktion=ERROR-Log method=POST>';

echo "<p><center><h1><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Errlog']."</b></h1></center><p>";


if (isset($_REQUEST["showDays"]) && is_numeric($_REQUEST["showDays"])) 
{
	$txt_showDays=$_REQUEST["showDays"];
	echo '<input type="hidden" name="showDays" value="'.$txt_showDays.'">';
	$url_showDays="showDays=".$txt_showDays;
}
else
{
	$txt_showDays=7;
	echo '<input type="hidden" name="showDays" value="'.$txt_showDays.'">';
	$url_showDays="showDays=".$txt_showDays;
}

echo "Zeige Log-Eintr�ge f�r <input type=text name='showDays' size=3 value='".$txt_showDays."'> Tage";
echo '&nbsp;<input type=submit name="cmdShowDays" value="Anzeigen">';

if(isset($_POST['cmdLoeschen_x']))
{
	//include('./operating_COM_Errlog_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./operating_COM_Errlog_speichern.php');
}

$ERR_KEY="";
if (isset($_REQUEST['Edit']))
{
	$ERR_KEY=$_REQUEST['Edit'];
}

if(isset($_POST['txtOCE_ERR_KEY']))
{
	$ERR_KEY=$_POST['txtOCE_ERR_KEY'];
}


$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

$SQL='SELECT DISTINCT HDK_KEY, HDK_BEZEICHNUNG FROM COMOP.V_HELPDESK_FELDER';

$rsHDK=awisOpenRecordset($comcon,$SQL);
$rsHDKZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

$SQL ='SELECT * FROM ERR_LOG JOIN ERR_LOG_STATUS ON (ERR_STATUS=ERR_STAT_KEY(+))';
$SQL.=' WHERE TRUNC(ERR_DATUM)>=TRUNC(SYSDATE)-'.$txt_showDays;
$SQL.=' ORDER BY ERR_KEY DESC';

//awis_Debug(1,$SQL);

$rsERL=awisOpenRecordset($comcon,$SQL);
$rsERLZeilen = $awisRSZeilen;

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift('',20);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_NR'],100,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_KATEGORIE'],100,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_MSG'],400,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_LOG'],80,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_PROGRAMMTYP'],100,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_STATUS'],120,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_DATUM'],100,'font-size: 80%;');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Errlog']['OCE_USERDAT'],100,'font-size: 80%;');
awis_FORM_ZeileEnde();

for($ERLZeile=0;$ERLZeile<$rsERLZeilen;$ERLZeile++)
{
	
	if($ERLZeile==$MaxDSAnzahl)
	{
		awis_FORM_Trennzeile();
		awis_FORM_FormularEnde();
		awis_FORM_Hinweistext('Ausgabe auf '.$MaxDSAnzahl.' Datens�tze beschr�nkt.',0,'font-size: 75%;');
		break;
	}
	
	$Icons = array();
	$Icons[] = array('edit','"./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit='.$rsERL['ERR_KEY'][$ERLZeile].'&'.$url_showDays.'#anc'.$rsERL['ERR_KEY'][$ERLZeile].'"');
	
	
	if (isset($ERR_KEY) && $rsERL['ERR_KEY'][$ERLZeile]==$ERR_KEY)
	{
		
		$Link_HDK='';
		
		if($rsHDKZeilen>0)
		{
			for($HDKZeile=0;$HDKZeile<$rsHDKZeilen;$HDKZeile++)
			{
				if($rsHDK['HDK_BEZEICHNUNG'][$HDKZeile]==$rsERL['ERR_KATEGORIE'][$ERLZeile])
				{
					$Link_HDK='"./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$rsHDK['HDK_KEY'][$HDKZeile].'&HDF_ALLE=1" target="_blank"';
				}
			}
		}
				
		echo '<input type=hidden name=txtOCE_ERR_KEY value=0'.$rsERL['ERR_KEY'][$ERLZeile].'>';
		
		awis_FORM_Trennzeile();
		
		awis_FORM_ZeileStart('text-align: right;');
		
		if(($Recht3114&2)==2)
		{
			echo "&nbsp;<input accesskey=s type=image src=/bilder/icon_save.png title='".$AWISSprachKonserven['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
		}
		if(($Recht3114&8)==8)
		{
			echo "&nbsp;<input accesskey=x type=image src=/bilder/icon_delete.png title='".$AWISSprachKonserven['Wort']['lbl_loeschen']." (ALT+X)' name=cmdLoeschen>";
		}
		if(($Recht3114&16)==16)
		{
			echo "&nbsp;<a title='Historie anzeigen' name='anc".$rsERL['ERR_KEY'][$ERLZeile]."' href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=".$rsERL['ERR_KEY'][$ERLZeile]."&History=True&".$url_showDays."#anc".$rsERL['ERR_KEY'][$ERLZeile]."><img border=0  src=/bilder/icon_report.png></a>";
			//echo "&nbsp;<input accesskey=x type=image src=/bilder/sc_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_anzeigen']." (ALT+A)' name=cmdAnzeigen>";
			
		}
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_NR'].':',120);
		awis_FORM_Erstelle_TextFeld('ERR_NR',$rsERL['ERR_NR'][$ERLZeile],0,90,false,'','','','Z','','');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_KATEGORIE'].':',120);
		awis_FORM_Erstelle_TextFeld('ERR_KATEGORIE',$rsERL['ERR_KATEGORIE'][$ERLZeile],0,180,false,'','',$Link_HDK,'T','','');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_DATUM'].': ',110);
		awis_FORM_Erstelle_TextFeld('ERR_DATUM',$rsERL['ERR_DATUM'][$ERLZeile],0,190,false,'','','','DU','','');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_USERNAME'].' - '.$AWISSprachKonserven['Operating_COM_Errlog']['OCE_USERDAT'].': ',150,'color: red; font-size: 80%;');
		awis_FORM_Erstelle_TextFeld('USERNAME',$rsERL['USERNAME'][$ERLZeile].' - '.$rsERL['USERDAT'][$ERLZeile],0,250,false,'color: red; font-size: 80%;','','','T','','');		
		awis_FORM_ZeileEnde();

		awis_FORM_ZeileStart();	
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_STATUS'].': ',80);
		$SQL='SELECT ERR_STAT_KEY, ERR_STAT_BEZEICHNUNG FROM ERR_LOG_STATUS ORDER BY ERR_STAT_SORT';
		awis_FORM_Erstelle_SelectFeld('ERR_STATUS',(isset($rsERL['ERR_STATUS'][$ERLZeile])?$rsERL['ERR_STATUS'][$ERLZeile]:''),250,true,$comcon,$SQL,'-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],$rsERL['ERR_STATUS'][$ERLZeile],2,'','','');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_PROGRAMMTYP'].':',120);
		awis_FORM_Erstelle_TextFeld('PROGRAMMTYP',$rsERL['PROGRAMMTYP'][$ERLZeile],0,140,false,'','','','T','','');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_PROGRAMMNAME'].': ',150);
		awis_FORM_Erstelle_TextFeld('PROGRAMMNAME',$rsERL['PROGRAMMNAME'][$ERLZeile],0,0,false,'','','','T','','');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_MSG'].': ',150);
		awis_FORM_Erstelle_TextFeld('ERR_MSG',$rsERL['ERR_MSG'][$ERLZeile],0,800,false,'','','','T','','');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_SERVER'].': ',150);
		awis_FORM_Erstelle_TextFeld('SERVER',$rsERL['SERVER'][$ERLZeile],0,0,false,'','','','T','','');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		if($rsERL['ERR_STATUS'][$ERLZeile]!=6)
		{
			$Link_Log='"'.$COMWebSrv.'/schnelldiagnose/showfile_sqlldr.php?search=Satz &file='.$rsERL['LOG'][$ERLZeile].'" target="_blank"';
		}
		else 
		{
			$Link_Log='';
		}
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_LOG'].': ',150);
		awis_FORM_Erstelle_TextFeld('LOG',$rsERL['LOG'][$ERLZeile],0,0,false,'','',$Link_Log,'T','','');
		awis_FORM_ZeileEnde();
		
		if ($rsERL['ILO_REF_ID'][$ERLZeile]!='')
		{
			awis_FORM_ZeileStart();
						
			//$Link_ILO='"./operating_COM_Details_Errlog_ILO.php?ILO_REF_ID='.$rsERL['ILO_REF_ID'][$ERLZeile].'" target="_blanks"';
			$ILO_REF_ID=$rsERL['ILO_REF_ID'][$ERLZeile];
			$Link_ILO="onclick=\"window.open('./operating_COM_Details_Errlog_ILO.php?ILO_REF_ID=".$ILO_REF_ID."','','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');"; 					

			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ILO'].': ',150);		
			awis_FORM_Erstelle_TextFeld('ILO',$rsERL['ILO_REF_ID'][$ERLZeile],0,0,false,'','','','T','','');
			awis_FORM_Erstelle_ListenBild('script','EXEC_'.$ILO_REF_ID,'','/bilder/icon_lupe.png" '.$Link_ILO,'COM - ERROR-Log - ILO','','','16','16',20,'L');
			awis_FORM_ZeileEnde();
		}		 					
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_BEMERKUNG'].': ',150);
		awis_FORM_Erstelle_Textarea('BEMERKUNG',$rsERL['BEMERKUNG'][$ERLZeile],300,150,3,true,'');
		awis_FORM_ZeileEnde();
		
/*		awis_FORM_ZeileStart();
		
		if(($Recht3114&2)==2)
		{
			echo "&nbsp;<input accesskey=s type=image src=/bilder/icon_save.png title='".$AWISSprachKonserven['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
		}
		if(($Recht3114&8)==8)
		{
			echo "&nbsp;<input accesskey=x type=image src=/bilder/icon_delete.png title='".$AWISSprachKonserven['Wort']['lbl_loeschen']." (ALT+X)' name=cmdLoeschen>";
		}
		if(($Recht3114&16)==16)
		{
			echo "&nbsp;<a name='anc".$rsERL['ERR_KEY'][$ERLZeile]."' href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=".$rsERL['ERR_KEY'][$ERLZeile]."&History=True&".$url_showDays."><img border=0  src=/bilder/icon_report.png></a>";
			//echo "&nbsp;<input accesskey=x type=image src=/bilder/sc_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_anzeigen']." (ALT+A)' name=cmdAnzeigen>";
			
		}
		awis_FORM_ZeileEnde();
*/		

		if (isset($_GET['History']) && ($Recht3114&16)==16)
		{
			
			$SQL='SELECT * FROM ERR_LOG_HIST WHERE ERR_KEY=0'.$rsERL['ERR_KEY'][$ERLZeile].' ORDER BY ERR_DATUM DESC';
			$rsERH=awisOpenRecordset($comcon,$SQL);
			$rsERHZeilen = $awisRSZeilen;
			
			if ($rsERHZeilen<>0)
			{
				awis_FORM_Trennzeile();
			}
			
			for($ERHZeile=0;$ERHZeile<$rsERHZeilen;$ERHZeile++)
			{
				if ($ERHZeile<>0 )
				{
					awis_FORM_Trennzeile('D');
				}
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_NR'].':',120,'font-size: 75%;');
				awis_FORM_Erstelle_TextFeld('ERR_NR',$rsERH['ERR_NR'][$ERHZeile],0,80,false,'font-size: 75%;','','','Z','','');
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_PROGRAMMTYP'].':',100,'font-size: 75%;');
				awis_FORM_Erstelle_TextFeld('PROGRAMMTYP',$rsERH['PROGRAMMTYP'][$ERHZeile],0,120,false,'font-size: 75%;','','','T','','');
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_STATUS'].': ',100,'font-size: 75%;');
				$SQL='SELECT ERR_STAT_KEY, ERR_STAT_BEZEICHNUNG FROM ERR_LOG_STATUS ORDER BY ERR_STAT_SORT';
				awis_FORM_Erstelle_SelectFeld('ERR_HIST_STATUS',(isset($rsERH['ERR_STATUS'][$ERHZeile])?$rsERH['ERR_STATUS'][$ERHZeile]:''),250,'',$comcon,$SQL,'-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],$rsERH['ERR_STATUS'][$ERHZeile],'','font-size: 75%;','','');
				awis_FORM_Erstelle_TextFeld('USERNAME',$rsERH['ERR_MODKZ'][$ERHZeile],0,120,false,'font-size: 75%;','','','T','','');
				awis_FORM_Erstelle_TextFeld('USERNAME',$rsERH['USERNAME'][$ERHZeile],0,130,false,'font-size: 75%;','','','T','','');
				awis_FORM_Erstelle_TextFeld('USERDAT',$rsERH['USERDAT'][$ERHZeile],0,150,false,'font-size: 75%;','','','T','','');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_PROGRAMMNAME'].': ',120,'font-size: 75%;');
				awis_FORM_Erstelle_TextFeld('PROGRAMMNAME',$rsERH['PROGRAMMNAME'][$ERHZeile],0,0,false,'font-size: 75%;','','','T','','');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_ERR_MSG'].': ',120,'font-size: 75%');
				awis_FORM_Erstelle_TextFeld('ERR_MSG',$rsERH['ERR_MSG'][$ERHZeile],0,0,false,'font-size: 75%;','','','T','','');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_SERVER'].': ',120,'font-size: 75%;');
				awis_FORM_Erstelle_TextFeld('SERVER',$rsERH['SERVER'][$ERHZeile],0,0,false,'font-size: 75%;','','','T','','');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_LOG'].': ',120,'font-size: 75%;');
				awis_FORM_Erstelle_TextFeld('LOG',$rsERH['LOG'][$ERHZeile],0,0,false,'font-size: 75%;','','','T','','');
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Errlog']['OCE_BEMERKUNG'].': ',120,'font-size: 75%;');
				awis_FORM_Erstelle_Textarea('BEMERKUNG',$rsERH['BEMERKUNG'][$ERHZeile],300,150,3,false,'font-size: 75%;');
				awis_FORM_ZeileEnde();
				
			}
			
		}
		
		awis_FORM_Trennzeile();
		
	}
	else
	{
	
	awis_FORM_ZeileStart();
	$link='';
	
	$log_link="";
	$log_text="";
	$Icon_Log = array();
	
	$LogPath=pathinfo($rsERL['LOG'][$ERLZeile]);
	
	if($LogPath!='')
	{
		$log_text=$rsERL['LOG'][$ERLZeile];
		
		if($rsERL['ERR_STATUS'][$ERLZeile]!=6)
		{
			$log_link='"'.$COMWebSrv.'/schnelldiagnose/showfile_sqlldr.php?search=Satz &file=' .str_replace(' ','',$log_text).'" target="_blank"';
			$Icon_Log[] = array('lupe',$log_link);
		}
		else
		{
			$log_link="";
			$Icon_Log = array();
		}
	}	
	elseif(ereg(' \/.*.log',$rsERL['ERR_MSG'][$ERLZeile],$log_text))
	{
		if($rsERL['ERR_STATUS'][$ERLZeile]!=6)
		{
			$log_link='"'.$COMWebSrv.'/schnelldiagnose/showfile_sqlldr.php?search=Satz &file=' .str_replace(' ','',$log_text[0]).'" target="_blank"';
			$Icon_Log[] = array('lupe',$log_link);
		}
		else
		{
			$log_link="";
			$Icon_Log = array();
		}
	}
	
	if (substr($rsERL['ERR_NR'][$ERLZeile],0,1)=='-') {
	$link='HTTP://ora-'.str_pad(substr($rsERL['ERR_NR'][$ERLZeile],1,(strlen($rsERL['ERR_NR'][$ERLZeile])-1)),5,'0',0).'.ora-code.com target="_blank"'; 
	}
	
	$Link_HDK='';
	
	if($rsHDKZeilen>0)
	{
		for($HDKZeile=0;$HDKZeile<$rsHDKZeilen;$HDKZeile++)
		{
			if($rsHDK['HDK_BEZEICHNUNG'][$HDKZeile]==$rsERL['ERR_KATEGORIE'][$ERLZeile])
			{
				$Link_HDK='"./operating_COM_Main.php?cmdAktion=Helpdesk&HDK_KEY='.$rsHDK['HDK_KEY'][$HDKZeile].'&HDF_ALLE=1" target="_blank"';
			}
		}
	}
	
	awis_FORM_Erstelle_ListeIcons($Icons,20,($ERLZeile%2));
	awis_FORM_Erstelle_ListenFeld('ERR_NR',$rsERL['ERR_NR'][$ERLZeile],0,100,false,($ERLZeile%2),'font-size: 80%;',$link,'Z');
	awis_FORM_Erstelle_ListenFeld('ERR_KATEGORIE',$rsERL['ERR_KATEGORIE'][$ERLZeile],0,100,false,($ERLZeile%2),'font-size: 80%;',$Link_HDK,'T');
	awis_FORM_Erstelle_ListenFeld('ERR_MSG',$rsERL['ERR_MSG'][$ERLZeile],0,400,false,($ERLZeile%2),'font-size: 80%;','','T');
	awis_FORM_Erstelle_ListeIcons($Icon_Log,80,($ERLZeile%2));
	awis_FORM_Erstelle_ListenFeld('PROGRAMMTYP',$rsERL['PROGRAMMTYP'][$ERLZeile],0,100,false,($ERLZeile%2),'font-size: 80%;','','T');
	awis_FORM_Erstelle_ListenFeld('ERR_STAT_BEZEICHNUNG',$rsERL['ERR_STAT_BEZEICHNUNG'][$ERLZeile],0,120,false,($ERLZeile%2),'font-size: 80%;','','T');
	awis_FORM_Erstelle_ListenFeld('ERR_DATUM',$rsERL['ERR_DATUM'][$ERLZeile],0,100,false,($ERLZeile%2),'font-size: 80%;','','DU');
	awis_FORM_Erstelle_ListenFeld('USERDAT',$rsERL['USERDAT'][$ERLZeile],0,100,false,($ERLZeile%2),'font-size: 80%;','','DU');
	awis_FORM_ZeileEnde();
	awis_FORM_Trennzeile();
	}
}

awis_FORM_FormularEnde();

echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

