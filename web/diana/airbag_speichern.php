<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $ABG;

try {

    $Meldung = '';
    if (isset($_POST['txtDruckKey']) and count($_POST['txtDruckKey']) > 0) {
        foreach ($_POST['txtDruckKey'] as $KEY) {
            $SQL = 'UPDATE AIRBAGS ';
            $SQL .= ' SET ABG_MENGE = ' . $ABG->DB->WertSetzen('ABG', 'Z', $_POST['txtABG_MENGE_' . $KEY]);
            $SQL .= ' , ABG_MODELLNR = ' . $ABG->DB->WertSetzen('ABG', 'T', $_POST['txtABG_MODELLNR_' . $KEY]);
            $SQL .= ' , ABG_FILIALE = ' . $ABG->DB->WertSetzen('ABG', 'Z', $_POST['txtABG_FILIALE_' . $KEY]);
            $SQL .= ' , ABG_BESTELLNR = ' . $ABG->DB->WertSetzen('ABG', 'T', $_POST['txtABG_BESTELLNR_' . $KEY]);
            $SQL .= ' , ABG_DATUM = ' . $ABG->DB->WertSetzen('ABG', 'D', $_POST['txtABG_DATUM_' . $KEY]);
            $SQL .= ' , ABG_BEZEICHNUNG = ' . $ABG->DB->WertSetzen('ABG', 'T', $_POST['txtABG_BEZEICHNUNG_' . $KEY]);
            $SQL .= ' , ABG_AUTO = ' . $ABG->DB->WertSetzen('ABG', 'T', $_POST['txtABG_AUTO_' . $KEY]);
            $SQL .= ' WHERE ABG_KEY = ' . $ABG->DB->WertSetzen('ABG', 'Z', $KEY);

            $ABG->DB->Ausfuehren($SQL, 'ABG', false, $ABG->DB->Bindevariablen('ABG', true));
        }
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201202081035");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201202081036");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>