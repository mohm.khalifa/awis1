<?
echo "<html>";
echo "<head>";
echo "<title>Awis - ATU webbasierendes Informationssystem</title>";

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $AWISSprache;
global $AWISBenutzer;
global $awisRSZeilen;
global $awisDBFehler;
$con = awislogon();

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

echo "</head>";
echo "<body>";


if(isset($_GET['Sprache']))
{
	$AWISSprache=$_GET['Sprache'];
}
else
{
	$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
}

include ("ATU_Header.php");	// Kopfzeile
$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3500);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Gewinnspiel',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

//Pr�fen, ob eine Gewinnfrage vorhanden ist
$mydate = date("d.m.Y"); 
//SELECT * FROM GEWINNFRAGEN WHERE '26.06.2007' >= GWF_GUELTIGVON AND '26.06.2007' <= GWF_GUELTIGBIS

$SQL = "SELECT * FROM GEWINNFRAGEN WHERE '". $mydate ."' >= GWF_GUELTIGVON AND '". $mydate ."' <= GWF_GUELTIGBIS";
awis_Debug(1,$_POST);
$rsGWF = awisOpenRecordset($con, $SQL);
$rsGWFZeilen = $awisRSZeilen;

if ($rsGWFZeilen <= 0)
{
	echo "<span class=HinweisText>Momentan ist keine Gewinnfrage vorhanden!</span>";
	echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
	
	if(($RechteStufe&2)==2)
	{
//		echo "<form name=frmSuche method=post action=./gewinnspiel_auswertung.php>";
		echo "&nbsp<input tabindex=90 type=image src='/bilder/diagramm.png' title='Auswertung' name=cmdAuswertung onclick=location.href='./gewinnspiel_auswertung.php';>";
//		echo "</form>";
	}
	
	die();		
}

else 
{
	
	$von = awis_PruefeDatum($rsGWF['GWF_GUELTIGVON'][0],0,1);
	$bis = awis_PruefeDatum($rsGWF['GWF_GUELTIGBIS'][0],0,1);

	if(date("d.m.Y",$von) == date("d.m.Y"))
	{
		if ((date("G") < 14) or (date("G") == 14 and date("i") < 30))
		{
			echo "<span class=HinweisText>Momentan ist keine Gewinnfrage vorhanden!</span>";
			echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

			if(($RechteStufe&2)==2)
			{
		//		echo "<form name=frmSuche method=post action=./gewinnspiel_auswertung.php>";
				echo "&nbsp<input tabindex=90 type=image src='/bilder/diagramm.png' title='Auswertung' name=cmdAuswertung onclick=location.href='./gewinnspiel_auswertung.php';>";
		//		echo "</form>";
			}
			
			die();		
		}
	}
	
	if(date("d.m.Y",$bis) == date("d.m.Y")) 
	{
		if (date("G") >= 20) // or (date("G") == 20 and date("i") > 00))
		{
			echo "<span class=HinweisText>Momentan ist keine Gewinnfrage vorhanden!</span>";
			echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
			
			if(($RechteStufe&2)==2)
			{
		//		echo "<form name=frmSuche method=post action=./gewinnspiel_auswertung.php>";
				echo "&nbsp<input tabindex=90 type=image src='/bilder/diagramm.png' title='Auswertung' name=cmdAuswertung onclick=location.href='./gewinnspiel_auswertung.php';>";
		//		echo "</form>";
			}
			
			die();		
		}
	}
}


$SQL ='';

//Personaldaten zur eingegebenen Personalnummer suchen, Mitarbeiter in ausl�ndischen Filialen sind in der Tabelle Sonderausweise gespeichert
if(isset($_POST['txtPER_NR']) AND !isset($_POST['cmdSpeichern_x']))
{
	$SQL = 'SELECT PER_NACHNAME, PER_VORNAME, PER_FIL_ID, PER_NR FROM PERSONAL WHERE (PER_AUSTRITT > SYSDATE OR PER_AUSTRITT IS NULL) AND PER_NR = \''.$_POST['txtPER_NR'].'\'';
	$rsPER = awisOpenRecordset($con, $SQL);
	$rsPERZeilen = $awisRSZeilen;
	
	if ($rsPERZeilen == 0)
	{
		$SQL = 'SELECT SAW_PER_NR, SAW_NAME, SAW_VORNAME, SAW_FIL_ID FROM SONDERAUSWEISE WHERE (SAW_AUSTRITT > SYSDATE OR SAW_AUSTRITT IS NULL) AND SAW_PER_NR < 900000 AND SAW_SAG_KEY = 1 AND SAW_PER_NR = \''.$_POST['txtPER_NR'].'\'';
		$rsSAW = awisOpenRecordset($con, $SQL);
		$rsSAWZeilen = $awisRSZeilen;
		
		if ($rsSAWZeilen == 0)
		{
			echo "<span class=HinweisText>Mit den angegebenen Parametern konnte kein Personal gefunden werden</span>";
			echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./gewinnspiel_Main.php';>";
			die();		
		}
		
		else
		{
			$NACHNAME = $rsSAW['SAW_NAME'][0];
			$VORNAME = $rsSAW['SAW_VORNAME'][0];
			$FIL_ID = $rsSAW['SAW_FIL_ID'][0];
			$PER_NR = $rsSAW['SAW_PER_NR'][0];
		}
	}
	
	else 
	{
		$NACHNAME = $rsPER['PER_NACHNAME'][0];
		$VORNAME = $rsPER['PER_VORNAME'][0];
		$FIL_ID = $rsPER['PER_FIL_ID'][0];
		$PER_NR = $rsPER['PER_NR'][0];
	}
	
	
	$SQL = 'SELECT GWS_PER_NR FROM GEWINNSPIELE WHERE GWS_PER_NR = '.$_POST['txtPER_NR'].' AND GWS_GWF_KEY = '.$rsGWF['GWF_KEY'][0];
	$rsGWS = awisOpenRecordset($con, $SQL);
	$rsGWSZeilen = $awisRSZeilen;
	
	if ($rsGWSZeilen > 0)
	{		
		echo "<span class=HinweisText>Sie haben bereits am Wettbewerb teilgenommen!</span>";
		echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
		die();		
	}	
}

//Antwort speichern
elseif(isset($_POST['cmdSpeichern_x']))
{	
	if(!isset($_POST['txtAntwort']))
	{
		echo "<span class=HinweisText>Bitte geben Sie eine Antwort an!</span>";
		echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./gewinnspiel_Main.php';>";
		die();
	}

	else 
	{
		$SQL = 'SELECT GWS_PER_NR FROM GEWINNSPIELE WHERE GWS_PER_NR = '.$_POST['txtPER_NR'].' AND GWS_GWF_KEY = '.$rsGWF['GWF_KEY'][0];
		$rsGWS = awisOpenRecordset($con, $SQL);
		$rsGWSZeilen = $awisRSZeilen;
		
		if ($rsGWSZeilen > 0)
		{		
			echo "<span class=HinweisText>Sie haben bereits am Wettbewerb teilgenommen!</span>";
			echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
			die();		
		}	
	
		$SQL = 'INSERT INTO GEWINNSPIELE (GWS_PER_NR, GWS_ANTWORT, GWS_GWF_KEY, GWS_USER, GWS_USERDAT) VALUES ('.$_POST['txtPER_NR'].',\'' . $_POST['txtAntwort'] .'\',' . $rsGWF['GWF_KEY'][0] .',\'' .$AWISBenutzer->BenutzerName().'\',SYSDATE)';
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("gewinnspiel_Main.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='./gewinnspiel_Main.php>Eingabe konnte nicht gespeichert werden. Zur�ck zur Eingabe.</a><p>";
			awisLogoff($con);
			die();
		}	 		
	
		echo "<br>Vielen dank f�r die Teilnahme. Ihre Antwort wurde gespeichert.";
		echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
		die();

	}
}

//awis_Debug(1,$SQL);

//echo "<form name=frmSuche method=post action=./gewinnspiel_Main.php>";

echo "<table width = 80% id=Tabelle border=0 bgcolor=\"lightgrey\" align=center>";
echo "<tr><td><b><font color=\"red\" size=5><center>Gewinnfrage zur Sendung</center></font></b></td></tr>";
echo "<tr><td><b><font color=\"red\" size=5><center>ATU - aktuell</center></font></b></td></tr>";
echo "<tr><td><b><font color=\"red\" size=5><center>am ".$rsGWF['GWF_DATUM_ATU_TV'][0]."</center></font></b></td></tr>";
echo "</table>";
echo "<br><br>";

if(!isset($_POST['txtPER_NR']))
{
	echo "<form name=frmSuche method=post action=./gewinnspiel_Main.php>";
	echo "<table width = 80% id=DatenTabelle border=0 align=center>";
	echo "<tr><td id=FeldBez width=20%>Pers-Nr.:</td>";
	echo "<td id=TabellenZeileGrau width=80%><input tabindex=1 type=text name=txtPER_NR size=10></td></tr>";	
	echo "<tr><td>&nbsp</td></tr>";
	echo "<tr><td>&nbsp<input tabindex=90 type=image src='/bilder/eingabe_ok.png' title='Suche starten' name=cmdSuche></td>";
	echo "</form>";
		
	if(($RechteStufe&2)==2)
	{
		echo "<form name=frmSuche method=post action=./gewinnspiel_auswertung.php>";
		echo "<td colspan = 2 align = right><input tabindex=90 type=image src='/bilder/diagramm.png' title='Auswertung' name=cmdAuswertung>&nbsp</td>";
		echo "</form>";
	}
	
	echo "</tr></table>";
}

else
{
	echo "<form name=frmSuche method=post action=./gewinnspiel_Main.php>";
	echo "<table width = 80% id=DatenTabelle border=0 align=center>";
	echo "<tr><td id=FeldBez width=150>Pers-Nr.:</td>";
	echo "<td id=TabellenZeileGrau width=250>".$PER_NR."</td>";
	echo "<td id=FeldBez width=150>Filial-Nr.:</td>";
	echo "<td id=TabellenZeileGrau width=250>".$FIL_ID."</td>";
	echo "</tr>";
	echo "<tr><td id=FeldBez width=150>Name:</td>";
	echo "<td id=TabellenZeileGrau width=250>".$NACHNAME."</td>";	
	//echo "<td width=300>&nbsp&nbsp<input type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\"></td>";
	echo "<td id=FeldBez width=150>Vorname:</td>";
	echo "<td id=TabellenZeileGrau width=250>".$VORNAME."</td>";
	echo "</tr></table>";
	echo "<br>";
	
	echo "<table width = 80% id=InhaltTabelle border=0 align=center>";
	echo "<tr><td><h3>Gewinnfrage:</h3></td></tr>";
	echo "<tr><td>".$rsGWF['GWF_FRAGE'][0]."</td></tr>";
	echo "</table>";
		
	echo "<br>";
	echo "<input type=hidden name=txtPER_NR value=".$PER_NR.">";
	echo "<table width = 80% id=InhaltTabelle3 border=0 align=center>";
	echo "<tr><td width=20>a)</td>";
	echo "<td><input type=radio name=txtAntwort value=A> ".$rsGWF['GWF_ANTWORT_A'][0]."</td></tr>";
	echo "<tr><td width=20>b)</td>";
	echo "<td><input type=radio name=txtAntwort value=B> ".$rsGWF['GWF_ANTWORT_B'][0]."</td></tr>";
	echo "<tr><td colspan=2><input type=image title='Speichern' src=/bilder/diskette.png name=cmdSpeichern value=\"Speichern\"></td></tr>";
	echo "</table>";
	
	echo "<br>";
	echo "<table width = 80% id=InhaltTabelle border=0 align=center>";
	echo "<tr><td>Die Beantwortung der Fragen ist vom Dienstag, ".$rsGWF['GWF_GUELTIGVON'][0].", 14:30 Uhr bis Dienstag, ". $rsGWF['GWF_GUELTIGBIS'][0].", 20:00 Uhr m�glich.</td></tr>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr><td><b>Die ATU Academy lost <font color=\"red\">(*)</font> <u>f�nf</u> Teilnehmer mit der richtigen Antwort aus.</b></td></tr>";
	echo "<tr><td><b>Die Gewinner erhalten einen ATU Warengutschein in H�he von 50,00 Euro.</tr></td></b>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr><td><b>Wir freuen uns �ber Ihre Teilnahme und w�nschen Ihnen viel Erfolg!</b></tr></td>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr><td>Freundliche Gr��e</td></tr>";
	echo "<tr><td>Ihr ATU - tv Team</td></tr>";
	echo "<tr><td>&nbsp</td></tr>";
	echo "<tr><td>&nbsp</td></tr>";
	echo "<tr><td><font color=\"red\" size=1>(*) der Rechtsweg ist ausgeschlossen</font></td></tr>";
	echo "</table>";
	echo '</form>';
}	

echo "<br><hr><input type=image title='Zur�ck' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";



// Cursor in das erste Feld setzen
echo "<Script Language=JavaScript>";
echo "document.getElementsByName(\"txtPER_NR\")[0].focus();";
echo '</SCRIPT>';

awislogoff($con);

echo "</body>";
echo "</html>";

?>