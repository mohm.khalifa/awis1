<?php
global $AWIS_KEY2;                //Bewerbung
global $Aktionsmeldung;
require_once 'awisMailer.inc';
try {

    //Status Vorstellungsgespr�ch: Hier wird entweder eine Email in Outlook ge�ffnet 
    //oder eine PDF erzeugt. Eome automatisierte Email gibt es hier nicht.

    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $SQL = 'select ';
    $SQL .= ' a.HRF_FIRMIERUNG HRF_FIRMIERUNG, D.HRF_EMAIL_BEZEICHNUNG, d.HRF_KEY,';
    $SQL .= ' a.EINGANGSDATUM EINGANGSDATUM,';
    $SQL .= ' a.EINGANGSART EINGANGSART,';
    $SQL .= ' B.HRK_ANR_ID HRK_ANR_ID,';
    $SQL .= ' B.HRK_TITEL HRK_TITEL,';
    $SQL .= ' B.HRK_VORNAME HRK_VORNAME,';
    $SQL .= ' B.HRK_NACHNAME HRK_NACHNAME,';
    $SQL .= ' B.HRK_STRASSEHNR HRK_STRASSEHNR,';
    $SQL .= ' B.HRK_PLZ HRK_PLZ,';
    $SQL .= ' B.HRK_ORT HRK_ORT,';
    $SQL .= ' C.ANR_ANREDE,';
    $SQL .= ' VSDAT.BIF_WERT VORSTELLDATUM,';
    $SQL .= ' VSUHR.BIF_WERT VORSTELLZEIT,';
    $SQL .= ' VSORT.BIF_WERT VORSTELLORT,';
    $SQL .= ' VSPLZ.BIF_WERT VORSTELLPLZ,';
    $SQL .= ' VSLOKAT.BIF_WERT VORSTELLLOKATION,';
    $SQL .= ' VSSTR.BIF_WERT VORSTELLSTR';
    $SQL .= ' from V_BEWERBERVERWALTUNG a';
    $SQL .= ' inner join hrkopf b';
    $SQL .= ' on a.hrk_key = b.hrk_key';
    $SQL .= ' inner join anreden c';
    $SQL .= ' on b.hrk_anr_id = c.anr_id';
    $SQL .= ' inner join HRFIRMIERUNG d';
    $SQL .= ' on a.HRF_KEY = D.HRF_KEY';
    $SQL .= ' inner join BEWERBERINFOS VSDAT';
    $SQL .= ' on a.BWH_KEY = VSDAT.BIF_BWH_KEY and VSDAT.BIF_BIT_ID = 18';
    $SQL .= ' inner join BEWERBERINFOS VSUHR';
    $SQL .= ' on a.BWH_KEY = VSUHR.BIF_BWH_KEY and VSUHR.BIF_BIT_ID = 19';
    $SQL .= ' inner join BEWERBERINFOS VSORT';
    $SQL .= ' on a.BWH_KEY = VSORT.BIF_BWH_KEY and VSORT.BIF_BIT_ID = 20';
    $SQL .= ' inner join BEWERBERINFOS VSPLZ';
    $SQL .= ' on a.BWH_KEY = VSPLZ.BIF_BWH_KEY and VSPLZ.BIF_BIT_ID = 21';
    $SQL .= ' inner join BEWERBERINFOS VSLOKAT';
    $SQL .= ' on a.BWH_KEY = VSLOKAT.BIF_BWH_KEY and VSLOKAT.BIF_BIT_ID = 22';
    $SQL .= ' inner join BEWERBERINFOS VSSTR';
    $SQL .= ' on a.BWH_KEY = VSSTR.BIF_BWH_KEY and VSSTR.BIF_BIT_ID = 23';

    $SQL .= ' WHERE BWH_KEY' . $DB->LikeOderIst($_GET['BWH_KEY']);

    $rsMail = $DB->RecordSetOeffnen($SQL);

    $Werkzeuge = new awisWerkzeuge();
    if (isset($_POST['cmdStatusWeiterO'])) //In Outlook �ffnen.
    {

        $EmailKonserven = array();
        $EmailKonserven[] = array('BEW', '%');
        $EmailKonserven[] = array('Wort', 'lbl_weiter');
        $EmailKonserven[] = array('Wort', 'lbl_speichern');
        $EmailKonserven[] = array('Wort', 'lbl_trefferliste');
        $EmailKonserven[] = array('Wort', 'lbl_aendern');
        $EmailKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $EmailKonserven[] = array('Wort', 'lbl_loeschen');
        $EmailKonserven[] = array('Fehler', 'err_keineDaten');
        $EmailKonserven[] = array('Fehler', 'err_keineDatenbank');
        $EmailKonserven[] = array('Wort', 'txt_BitteWaehlen');

        $Form = new awisFormular();
        $AWISEmailKonserven = $Form->LadeTexte($EmailKonserven);

        $Firmierung = $rsMail->Feldinhalt('HRF_KEY');

        //Wochentage-Array f�llen
        $wochentag[0] = "Sonntag";
        $wochentag[1] = "Montag";
        $wochentag[2] = "Dienstag";
        $wochentag[3] = "Mittwoch";
        $wochentag[4] = "Donnerstag";
        $wochentag[5] = "Freitag";
        $wochentag[6] = "Samstag";

        $SQL = 'SELECT BVORNAME.BIF_WERT||\' \'||BNACHNAME.BIF_WERT as TEILNEHMER, BART.BIF_WERT as ART FROM V_BEWERBERVERTEILER BVORNAME';
        $SQL .= ' INNER JOIN V_BEWERBERVERTEILER BNACHNAME';
        $SQL .= ' ON BNACHNAME.DS=BVORNAME.DS and BNACHNAME.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BNACHNAME.BIF_BIT_ID=27';
        $SQL .= ' INNER JOIN V_BEWERBERVERTEILER BART';
        $SQL .= ' ON BART.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BART.DS=BVORNAME.DS and BART.BIF_BIT_ID=28';
        $SQL .= ' WHERE BVORNAME.BIF_BIT_ID=26 and BART.BIF_WERT <> \'Bewerber\'';
        $SQL .= ' and BART.BIF_WERT <> \'MANUELL\' and BVORNAME.BIF_BWH_KEY' . $DB->LikeOderIst($_GET['BWH_KEY']);
        $rsTeilnehmer = $DB->RecordSetOeffnen($SQL);

        $Text = $AWISEmailKonserven['BEW']['BEW_PDF_VORST_TEXT'];
        $Text = str_replace('#GEEHRT#', $rsMail->FeldInhalt('ANR_ANREDE') == 'Herr'?'geehrter':'geehrte', $Text);
        $Text = str_replace('#ANREDE#', $rsMail->FeldInhalt('ANR_ANREDE'), $Text);
        $Text = str_replace('#HRK_TITEL#', $rsMail->FeldInhalt('HRK_TITEL'), $Text);

        $Text = str_replace('#HRK_VORNAME#', $rsMail->FeldInhalt('HRK_VORNAME'), $Text);
        $Text = str_replace('#HRK_NACHNAME#', $rsMail->FeldInhalt('HRK_NACHNAME'), $Text);

        $Text = str_replace('#DATUM#', $rsMail->Feldinhalt('VORSTELLDATUM'), $Text);
        $Text = str_replace('#WOCHENTAG#', $wochentag[date('w', strtotime($rsMail->Feldinhalt('VORSTELLDATUM')))],
            $Text);
        $Text = str_replace('#UHRZEIT#', $rsMail->Feldinhalt('VORSTELLZEIT') . ' Uhr', $Text);
        $Text = str_replace('#ORT#', $rsMail->Feldinhalt('VORSTELLORT'), $Text);
        $Text = str_replace('#PLZ#', $rsMail->Feldinhalt('VORSTELLPLZ'), $Text);
        $Text = str_replace('#LOKATION#', $rsMail->Feldinhalt('VORSTELLLOKATION'), $Text);
        $Text = str_replace('#STRA�E#', $rsMail->Feldinhalt('VORSTELLSTR'), $Text);

        $Teilnehmer = '';
        while (!$rsTeilnehmer->EOF()) {
            $Teilnehmer .= ", " . $rsTeilnehmer->Feldinhalt('TEILNEHMER');
            $rsTeilnehmer->DSWeiter();
        }

        $Text = str_replace('#TEILNEHMER#', substr($Teilnehmer, 1), $Text);

        $Text = str_replace('#IHRE#', $rsTeilnehmer->AnzahlDatensaetze() > 1?'Ihre':'Ihr', $Text);
        $Text = str_replace('#ERWARTET#', $rsTeilnehmer->AnzahlDatensaetze() > 1?'erwarten':'erwartet', $Text);

        //Textkonserven haben hinten eine Nummer f�r die Firmierung. Wenn die da ist, nimm die, wenn ned Standard ATU Signatur.
        if (isset($MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_' . $Firmierung])) {
            $Signatur = $AWISEmailKonserven['BEW']['BEW_MAIL_SIGNATUR_' . $Firmierung];
        } else {
            $Signatur = $AWISEmailKonserven['BEW']['BEW_MAIL_SIGNATUR_1'];
        }
        $Text .= $Signatur;

        $Absender = 'karriere@de.atu.eu';                // Absender der Mails
        $Betreff = '';
        $EmpfaengerCC = '';

        if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) {
            //Email nehmen, wenn keine Email hinterlegt, schick an shuttle.

            $SQL = 'SELECT distinct bemail.bif_wert as email FROM V_BEWERBERVERTEILER BVORNAME';
            $SQL .= ' INNER JOIN V_BEWERBERVERTEILER BNACHNAME';
            $SQL .= ' ON BNACHNAME.DS=BVORNAME.DS and BNACHNAME.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BNACHNAME.BIF_BIT_ID=27';
            $SQL .= ' INNER JOIN V_BEWERBERVERTEILER BART';
            $SQL .= ' ON BART.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BART.DS=BVORNAME.DS and BART.BIF_BIT_ID=28
					INNER JOIN V_BEWERBERVERTEILER bemail
					ON bemail.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and bemail.DS=BVORNAME.DS and bemail.BIF_BIT_ID=29	
			';
            $SQL .= ' WHERE BVORNAME.BIF_BWH_KEY' . $DB->LikeOderIst($_GET['BWH_KEY']);
            $rsTeilnehmer = $DB->RecordSetOeffnen($SQL);
            echo $SQL;
            while (!$rsTeilnehmer->EOF()) {
                $Empf .= $rsTeilnehmer->Feldinhalt('EMAIL') . "; ";
                $rsTeilnehmer->DSWeiter();
            }

            $Empfaenger = $Empf == ''?'shuttle@de.atu.eu':$Empf;
        } else {
            $Empfaenger = 'shuttle@de.atu.eu';
            //$EmpfaengerCC = 'tobias.schaeffler@de.atu.eu';
        }

        $EmpfaengerBCC = '';

        $Betreff .= $AWISEmailKonserven['BEW']['BEW_MAIL_BETREFF'];
        $Betreff = str_replace('#FIRMIERUNG#', $rsMail->FeldInhalt('HRF_EMAIL_BEZEICHNUNG'), $Betreff);

        $Text = strip_tags($Text);
        $Text = rawurlencode($Text);
        $Betreff = rawurlencode($Betreff);

        $Aktionsmeldung = '<a href="mailto:' . $Empfaenger . '?subject=' . $Betreff . ($EmpfaengerCC != ''?'&amp;cc=' . $EmpfaengerCC:'') . ($EmpfaengerBCC != ''?'&bcc=' . $EmpfaengerBCC:'') . '&amp;body=' . $Text . '">
             <br>Email �ffnen</a>';
    } elseif (isset($_POST['cmdStatusWeiterS'])) //Serienbrief
    {
        //ob_start();
        $_GET['XRE'] = '56';
        $_GET['ID'] = 'bla';

        $Aktionsmeldung .= '<br><a href="/berichte/drucken.php?XRE=56&ID=1&BWH_KEY=' . $AWIS_KEY2 . '"> Serienbrief �ffnen </a>';
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
}

?>