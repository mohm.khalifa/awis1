<?php
global $SpeichernOK;
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once '../schaden_neu/Aufgabensteuerung/aufgabensteuerung_funktionen.php';


if(!isset($_GET['sucBES_BEARBEITER_AB']))
{
	var_dump($_GET);
	die();
}

$Form = new awisFormular();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('BES','%');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','wrd_Filiale');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Liste','lst_OffenMass');
$TextKonserven[]=array('Wort','PDFErzeugen');
$TextKonserven[]=array('Wort','DatumVom');
$TextKonserven[]=array('Wort','Datum');
$TextKonserven[]=array('FIL','FIL_GEBIET');
$TextKonserven[]=array('Liste','lst_ALLE_0');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');


$AWISBenutzer = awisBenutzer::Init();
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
$Recht40004 = $AWISBenutzer->HatDasRecht(40004);

$SQL ='select * from sachbearbeiter where ID='.$_GET['sucBES_BEARBEITER_AB'];
$rsSach = $DBSCHAD->RecordSetOeffnen($SQL);

if(($Recht40004&2)==2) {

	$aendern = true;

}
else
{
	$aendern = false;
}


$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANZEIGEFIL'] . ':', 130);
$Form->Erstelle_Checkbox('CHK_ANZEIGEFIL',$rsSach->FeldInhalt('SBAKTIVFIL'), 65,$aendern,1);
$Form->Erstelle_HiddenFeld('ANZEIGEFIL',$rsSach->FeldInhalt('SBAKTIVFIL'));


?>