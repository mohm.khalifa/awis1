INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_AKTIVINAKTIV', 'SEH', 'Status des Hotels', 'A~Aktiv|I~Inaktiv   ', TO_DATE('2018-07-27 09:04:11', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_STATUS', 'SEH', 'Textlabel f�r den Status', 'Status:', TO_DATE('2018-07-27 09:11:12', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_DATEN_GESPEICHERT', 'SEH', 'Meldung, dass Daten gespeichert wurden', 'Daten erfolgreich gespeichert.', TO_DATE('2018-07-27 09:58:22', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_HOTEL_INAKTIV', 'SEH', 'Meldung, dass das Hotel auf inaktiv geschalten wurde', 'Das Hotel wurde auf inaktiv gesetzt', TO_DATE('2018-07-27 09:58:18', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_GLEICHES_HOTEL', 'SEH', 'Infotext f�r Teilnehmer mit gleichem Hotel', 'Personen im gleichen Hotel:<br><br>', TO_DATE('2018-07-30 08:41:32', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT)
VALUES ('reg_42009', 'Register', 'Hotels', 'Hotels', 'Hotels', 'Hotels', 'Hotels', 'Hotels', 'Hotels', 'Hotels', TO_DATE('2018-07-23 12:38:47', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('ttt_42009', 'Register', 'Tooltipptext f�r die Pflegemaske der Hotels im Seminarplan', 'Pflege der Hotels', TO_DATE('2018-07-30 09:12:42', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."RECHTE" (XRC_ID, XRC_RECHT) VALUES ('4209', 'Seminarplan Hotels');

INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_SORTIERUNG, XRG_NEUEHILFE)
VALUES ('42009', '4200', 'Hotels', 'Hotels', './seminarplan_Main.php?cmdAktion=Hotels', './seminarplan_Hotels.php', 'Hotels', '4209', '30', '0');

INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('4209', '0', 'Seminarhotel anzeigen');
INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('4209', '1', 'Seminarhotel bearbeiten');
INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('4209', '2', 'Seminarhotel hinzuf�gen');
INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('4209', '3', 'Seminarhotel l�schen');

INSERT INTO "AWIS"."PROGRAMMPARAMETER" (XPP_ID, XPP_BEZEICHNUNG, XPP_DEFAULT, XPP_UEBERSCHREIBBAR, XPP_BEMERKUNG, XPP_KODIERT, XPP_DATENTYP)
VALUES ('200137', 'SEHSuche', 'a:0:{}', '255', 'Suchparameter f�r die Seminarhotels', '0', 'T');
COMMIT;
