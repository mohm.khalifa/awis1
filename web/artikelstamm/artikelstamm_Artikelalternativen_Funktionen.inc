<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class artikelstamm_Artikelalternativen_Funktionen
{
    private $_Form;
    private $_DB;
    private $_Benutzer;

    function __construct()
    {
        $this->_Benutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
    }

    public function erstelleArtikelalternativenAjax($AST_ATUNR)
    {
        if ($AST_ATUNR != '') {
            $SQL = 'SELECT';
            $SQL .= ' AST_ATUNR, ';
            $SQL .= ' AST_BEZEICHNUNG, ';
            $SQL .= ' AST_BEZEICHNUNGWW ';
            $SQL .= ' FROM artikelstamm';
            $SQL .= ' WHERE ast_atunr = ' . $this->_DB->WertSetzen('AST', 'TU', $AST_ATUNR);

            $rsAST = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST'));

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_Form->LadeTextBaustein('AST', 'AST_ATUNR', $this->_Benutzer->BenutzerSprache()) . ': ', 150);
            $this->_Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_ATUNR'), 600, 'font-weight:bold');
            $this->_Form->Erstelle_HiddenFeld('AAL_ALTERNATIV_AST_ATUNR', $rsAST->FeldInhalt('AST_ATUNR'));
            $this->_Form->ZeileEnde();

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_Form->LadeTextBaustein('AST', 'AST_BEZEICHNUNGWW', $this->_Benutzer->BenutzerSprache()) . ': ', 150);
            $this->_Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_BEZEICHNUNGWW'), 600, 'font-weight:bold');
            $this->_Form->ZeileEnde();

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_Form->LadeTextBaustein('AST', 'AST_BEZEICHNUNG', $this->_Benutzer->BenutzerSprache()) . ': ', 150);
            $this->_Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_BEZEICHNUNG'), 600, 'font-weight:bold');
            $this->_Form->ZeileEnde();
        }
    }

}