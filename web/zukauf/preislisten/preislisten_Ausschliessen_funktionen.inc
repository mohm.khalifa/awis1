<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class preislisten_Ausschliessen_funktionen
{
    public $FilterFelder = array();
    public $Form;
    function __construct()
    {
        $this->_FilterFelderInit();
        $this->Form = new awisFormular();
    }


    private function _FilterFelderInit()
    {
        $this->FilterFelder[] = 'txtHSK';
        $this->FilterFelder[] = 'txtTECDOCID';
        $this->FilterFelder[] = 'txtBESTELLNUMMER';
        $this->FilterFelder[] = 'txtATUNR';
        $this->FilterFelder[] = 'txtATUSORTIMENT';
        $this->FilterFelder[] = 'cmbHERSTELLER';
        $this->FilterFelder[] = 'cmbWARENGRUPPE1';
        $this->FilterFelder[] = 'cmbWARENGRUPPE2';
        $this->FilterFelder[] = 'cmbWARENGRUPPE3';
    }


}