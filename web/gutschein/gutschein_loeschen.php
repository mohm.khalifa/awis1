<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

$TextKonserven=array();
$TextKonserven[]=array('GSD','%');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_zurueck');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		//Kopfdaten l�schen
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		
		switch ($_GET['Seite'])
		{
			case 'Positionen':
				$Tabelle = 'GSP';
				$Key=$_GET['Del'];

				$SQL = 'SELECT *';
				$SQL .= ' FROM GUTSCHEINPOSDATEN ';
				$SQL .= ' WHERE GSP_KEY='.$Key . '';
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				
				$HauptKey=$rsDaten->FeldInhalt('GSP_AUFTRAGNR');
				$Felder=array();
                
				$Felder[]=array($Form->LadeTextBaustein('GSD','GSD_AUFTRAGNUMMER'),$rsDaten->FeldInhalt('GSP_AUFTRAGNR'));
				$Felder[]=array($Form->LadeTextBaustein('GSD','GSD_POSITIONEN'),$rsDaten->FeldInhalt('GSP_POSITION'));
				$Felder[]=array($Form->LadeTextBaustein('GSD','GSD_POSANZAHL'),$rsDaten->FeldInhalt('GSP_MENGE'));
				$Felder[]=array($Form->LadeTextBaustein('GSD','GSD_BETRAG'),$rsDaten->FeldInhalt('GSP_BETRAG'));
				$AWIS_KEY1=$rsDaten->FeldInhalt('GSP_AUFTRAGNR');

				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'GSP':
			    //Wenn h�here Position existiert als die gel�scht werden soll, dann
			    //h�here -1. Wird Position 1 gel�scht m�ssen alle nachkommenden 
			    //Positionen -1 gerechnet werden
			    
			    $rsPositionEntf = $DB->RecordSetOeffnen('SELECT GSP_POSITION FROM GUTSCHEINPOSDATEN WHERE GSP_KEY =' . $_POST['txtKey']);
			    $rsPosition = $DB->RecordsetOeffnen('SELECT GSP_POSITION FROM GUTSCHEINPOSDATEN WHERE GSP_AUFTRAGNR='.$_POST['txtHauptKey']);
			    
			    $SQL = 'DELETE FROM GUTSCHEINPOSDATEN WHERE GSP_KEY='.$_POST['txtKey'];
			    $DB->Ausfuehren($SQL);
			    
			    while(!$rsPosition->EOF())
			    {
			        If(intval($rsPosition->FeldInhalt('GSP_POSITION'))> intval($rsPositionEntf->FeldInhalt('GSP_POSITION')))
			        {
			            $PositionNEU = (intval($rsPosition->FeldInhalt('GSP_POSITION')))-1;
			            
			        	$SQL = 'UPDATE GUTSCHEINPOSDATEN SET';
                        $SQL .= ' GSP_POSITION='. $PositionNEU;
                        $SQL .= ' WHERE GSP_AUFTRAGNR=' . $_POST['txtHauptKey'];
                        $SQL .= ' AND GSP_POSITION=' . $rsPosition->FeldInhalt('GSP_POSITION') .'';

			        	$DB->Ausfuehren($SQL);
			        }
			        $rsPosition->DSWeiter();
			    }
			    
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./gutschein_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>