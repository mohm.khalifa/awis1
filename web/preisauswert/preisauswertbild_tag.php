<?php
// Gantt hour example
require_once("jpgraph/jpgraph.php");
require_once("jpgraph/jpgraph_line.php");
require_once("jpgraph/jpgraph_date.php");
require_once("jpgraph/jpgraph_scatter.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

if (!empty($_GET["filnr"]))
{
	$filnr = strtoupper($_GET["filnr"]);
}
else
{
	$filnr = 4;
}

// $con = awisLogon();  <- Geht nicht wegen der Kommentare in der Funktion!
// $con = @OCILogon("preisauswert","IlOuLua",awisDBServer());
$con = @OCILogon("preisauswert","preisauswert",awisDBServer());

if($con)
{

	awisExecute($con, "ALTER SESSION SET NLS_DATE_LANGUAGE = AMERICAN");	// Wird fr den strtotime ben�igt!
	$maxwert = 0;								// Wird fr die Berechnung der Skala gebraucht (-> maximaler Bestand)
	$minwert = 0;								// Wird fr die Berechnung der Skala gebraucht (-> minimaler Bestand)

	// Create the graph. 
	$graph = new Graph(1100, 650,"auto");    

	//*************************************************************************************************
	//	Absaetze holen
	//*************************************************************************************************
	$sql = "select to_char(datum, 'DD MONTH YYYY') dat, nvl(count(*),0) gesanz, count(vorgang_nr) waanz from artvk_bon ";
	$sql .= "where wg = '03' and fil_nr = " . $filnr  . " and datum >= to_date('01.01.2004', 'DD.MM.RRRR') and datum < to_date('31.12.2005', 'DD.MM.RRRR') ";
	$sql .= "group by datum order by datum";
	$rsDaten1 = awisOpenRecordset($con, $sql);
	$rsDatenZeilen = $awisRSZeilen;
	for ($i=0; $i < $rsDatenZeilen; $i++)
	{
		$rsDaten1["DAT"][$i] = strtotime($rsDaten1["DAT"][$i]);
// 		if ($maxwert<$rsDaten1["ENDBESTAND"][$i]) $maxwert=$rsDaten1["ENDBESTAND"][$i];
// 		if ($minwert>$rsDaten1["ENDBESTAND"][$i]) $minwert=$rsDaten1["ENDBESTAND"][$i];
	}

//  var_dump($rsDaten1);
	//*************************************************************************************************
	//	Absaetze zeichnen
	//*************************************************************************************************

	if (count($rsDaten1["GESANZ"]) > 0)
	{
		$lineplot1 =new LinePlot($rsDaten1["GESANZ"], $rsDaten1["DAT"]);
	// 	$lineplot1 =new LinePlot($rsDaten1["ENDBESTAND"], $rsDaten1["DAT"]);
// 		$lineplot1->SetStepStyle();
		$lineplot1 ->SetColor("blue");
		$lineplot1->SetLegend ("Gesamt");
	// 	$lineplot1->SetWeight(2);

		// Add the plot to the graph
		$graph->Add( $lineplot1);
	}

	if (count($rsDaten1["WAANZ"]) > 0)
	{
		$lineplot2 =new LinePlot($rsDaten1["WAANZ"], $rsDaten1["DAT"]);
	// 	$lineplot2 =new LinePlot($rsDaten1["ENDBESTAND"], $rsDaten1["DAT"]);
// 		$lineplot2->SetStepStyle();
		$lineplot2 ->SetColor("orange");
		$lineplot2->SetLegend ("WA");
	// 	$lineplot2->SetWeight(2);

		// Add the plot to the graph
		$graph->Add( $lineplot2);
	}


	awisLogoff($con);			// Wieder abmelden

}

$graph->SetScale("datlin");
$graph->title->Set("Absatzentwicklung WG 03 in Filiale: " . $filnr);
$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->title->SetColor('white');
$graph->xaxis->scale->SetDateFormat('d M');
$graph->xaxis->SetLabelAngle(90);
$graph->SetMargin(50,50,50,120);

// Set X-axis at the minimum value of Y-axis (default will be at 0)
//$graph->xaxis->SetPos("min");    // "min" will position the x-axis at the minimum value of the Y-axis

// Add mark graph with static lines
$line = new PlotLine(HORIZONTAL,0,"black",1);
$graph->AddLine($line);

// Adjust the legend position
$graph->legend->SetLayout(LEGEND_HOR);
$graph->legend->Pos(0.4,0.95,"center","bottom");

$graph->ygrid->SetLineStyle('dotted');
$graph->ygrid->Show(true, true);

// Display the graph
$graph->Stroke();

?>