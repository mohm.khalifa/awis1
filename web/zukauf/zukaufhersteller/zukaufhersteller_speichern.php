<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Hersteller
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZLH_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY1 = $_POST['txtZLH_KEY'];

		$rsLIN = $DB->RecordSetOeffnen('SELECT * FROM Zukauflieferantenhersteller WHERE ZLH_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsLIN->EOF())
		{
			$SQL = 'INSERT INTO Zukauflieferantenhersteller';
			$SQL .= '(ZLH_ZLI_KEY,ZLH_KUERZEL,ZLH_BEZEICHNUNG,ZLH_TECDOCID,ZLH_BEMERKUNG,ZLH_USER,ZLH_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= '1';		// Aus komp.
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLH_KUERZEL'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLH_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLH_TECDOCID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLH_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZLH_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsZLH = $DB->RecordSetOeffnen('SELECT * FROM ZUKAUFLIEFERANTENHERSTELLER WHERE ZLH_key=' . $_POST['txtZLH_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsZLH->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsZLH->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsZLH->FeldInfo($FeldName,'TypKZ'),$rsZLH->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZLH->FeldInhalt('ZLH_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE ZUKAUFLIEFERANTENHERSTELLER SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZLH_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ZLH_userdat=sysdate';
				$SQL .= ' WHERE ZLH_key=0' . $_POST['txtZLH_KEY'] . '';
$Form->DebugAusgabe(1,$SQL);
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}


	//***********************************************************************************
	//** Lieferanten
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZHK_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY2 = $_POST['txtZHK_KEY'];

		$rsLIN = $DB->RecordSetOeffnen('SELECT * FROM ZUKAUFHERSTELLERCODES WHERE ZHK_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY2));

		if($rsLIN->EOF())
		{
			$SQL = 'INSERT INTO ZUKAUFHERSTELLERCODES';
			$SQL .= '(ZHK_ZLH_KEY,ZHK_LIE_NR, ZHK_CODE,ZHK_USER,ZHK_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ''.$AWIS_KEY1;
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZHK_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZHK_CODE'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZHK_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsZHK = $DB->RecordSetOeffnen('SELECT * FROM ZUKAUFHERSTELLERCODES WHERE ZHK_key=' . $_POST['txtZHK_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsZHK->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsZHK->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsZHK->FeldInfo($FeldName,'TypKZ'),$rsZHK->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZHK->FeldInhalt('ZHK_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE ZUKAUFHERSTELLERCODES SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZHK_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ZHK_userdat=sysdate';
				$SQL .= ' WHERE ZHK_key=0' . $_POST['txtZHK_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}

	//***********************************************************************************
	//** Lieferanten
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZHZ_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY2 = $_POST['txtZHZ_KEY'];

		$rsLIN = $DB->RecordSetOeffnen('SELECT * FROM ZUKAUFHERSTELLERZUORDNUNGEN WHERE ZHZ_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY2));

		if($rsLIN->EOF())
		{
			$SQL = 'INSERT INTO ZUKAUFHERSTELLERZUORDNUNGEN';
			$SQL .= '(ZHZ_ZLH_KEY,ZHZ_LIE_NR,ZHZ_SORTIERUNG,ZHZ_BEMERKUNG,ZHZ_USER,ZHZ_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ''.$AWIS_KEY1;
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZHZ_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZHZ_SORTIERUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZHZ_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZHZ_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsZHZ = $DB->RecordSetOeffnen('SELECT * FROM ZUKAUFHERSTELLERZUORDNUNGEN WHERE ZHZ_key=' . $_POST['txtZHZ_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsZHZ->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsZHZ->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsZHZ->FeldInfo($FeldName,'TypKZ'),$rsZHZ->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZHZ->FeldInhalt('ZHZ_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE ZUKAUFHERSTELLERZUORDNUNGEN SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZHZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ZHZ_userdat=sysdate';
				$SQL .= ' WHERE ZHZ_key=0' . $_POST['txtZHZ_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>