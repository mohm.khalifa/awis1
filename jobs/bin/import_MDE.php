<?php

require_once('awisBenutzer.inc');
require_once('awisMDE.php');
require_once('awisINFO.php');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $Methode = 'Scheduler';

    for($i=1;$i<$argc;$i++)
    {
    	switch (strtolower($argv[$i]))
        {
        	case '--benutzer':
            	$Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i]; 
                break;
            case '--jobid':
                $JobId = $argv[++$i];
                break;                           
            case '--methode':
                $Methode = $argv[++$i];
                break;
            default:
				echo 'Parameter '.$argv[$i].' ist unbekannt.';
            case '--help':
                echo $argv[0].PHP_EOL;
                echo 'Optionen:'.PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                echo '  --methode  [Methodetyp]            [--Scheduler = alles | --LeseLieferabgleich = Lieferscheine]'.PHP_EOL;
                echo PHP_EOL;
                die();
            }
    }
    
    $MDE = new awisMDE($DebugLevel,$Benutzer);
    
    if(strtolower($Methode) == 'scheduler')
    {
        $MDE->HoleZetesDaten();
    	sleep(5);
        $MDE->LeseLieferabgleich();
    	$MDE->ImportBestandsabfrage();
    	$MDE->LeseRaedereinlagerung();
    	$MDE->LeseRaederauslagerung();
    	$MDE->LeseRueckfuehrung();
    }
    else
    {
    	switch(strtolower($Methode))
    	{
            case 'zetes':
                $MDE->HoleZetesDaten();
                break;
    		case 'leselieferabgleich':
    			$MDE->LeseLieferabgleich();
    			break;
    		case 'importbestandsabfrage':
    			$MDE->ImportBestandsabfrage();
    			break;
    		case 'leseraedereinlagerung':
    			$MDE->LeseRaedereinlagerung();
    			break;
    		case 'leseraederauslagerung':
    			$MDE->LeseRaederauslagerung();
    			break;
    		case 'leserueckfuehrung':
    			$MDE->LeseRueckfuehrung();
    			break;
    		case 'leserueckfuehrungzentrale':
    			$MDE->LeseRueckfuehrungZentrale();
    			break;
    		default:
    			echo "Unbekannte Methode" . PHP_EOL;
    			break;
    	}
    }

}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{	
	echo PHP_EOL.'awisMDE: Fehler: '.$ex->getMessage().PHP_EOL;
	$AWISInfo = new awisINFO();
	$AWISInfo->EMail(array('shuttle@de.atu.eu','shuttle@de.atu.eu'),'MDE-Fehler','Fehler : '.$ex->getMessage(),3);
}

?>







