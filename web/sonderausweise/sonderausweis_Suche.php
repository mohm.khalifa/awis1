<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SAW','%');	
	$TextKonserven[]=array('SAG','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');	
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht1700=$AWISBenutzer->HatDasRecht(1700);
	$Recht1701=$AWISBenutzer->HatDasRecht(1701);
	
	if($Recht1700==0 || $Recht1701==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./sonderausweis_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske bzw. Suchmaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SAW'));
	
	(!isset($Param['SPEICHERN'])?$Param['SPEICHERN']='off':'');

	$Form->Formular_Start();
	
	//Schnellsuche f�r textindexsuche
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Schnellsuche'].':',180);
	$Form->Erstelle_TextFeld('*Schnellsuche',($Param['SPEICHERN']=='on'?$Param['Schnellsuche']:''),25,200,true);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('P');
	
	//Personalnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_PER_NR'].':',180);
	$Form->Erstelle_TextFeld('*SAW_PER_NR',($Param['SPEICHERN']=='on'?$Param['SAW_PER_NR']:''),25,200,true);
	$AWISCursorPosition='sucSAW_PER_NR';
	$Form->ZeileEnde();
	
	//EAN-Nummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_EAN'].':',180);
	$Form->Erstelle_TextFeld('*SAW_EAN',($Param['SPEICHERN']=='on'?$Param['SAW_EAN']:''),25,200,true);
	$Form->ZeileEnde();
	
	//Filialnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_FIL_ID'].':',180);
	$Form->Erstelle_TextFeld('*SAW_FIL_ID',($Param['SPEICHERN']=='on'?$Param['SAW_FIL_ID']:''),25,200,true);
	$Form->ZeileEnde();
	
	//Bereich
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_BEREICH'].':',180);
	$DB->SetzeBindevariable('SAG', 'var_NO_sag_rechtebit', $Recht1701);
	$SQL = 'SELECT ROWNUM AS RID, SAW_BEREICH FROM (SELECT DISTINCT(SAW_BEREICH) AS SAW_BEREICH FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT, :var_NO_sag_rechtebit)=SAG_RECHTEBIT) ORDER BY 1 NULLS FIRST)';
	$Form->Erstelle_SelectFeld('*SAW_BEREICH',($Param['SPEICHERN']=='on'?$Param['SAW_BEREICH']:''), '25:180',true,$SQL,'','','','',array(array('0',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'])),'','',$DB->Bindevariablen('SAG'));
	$Form->ZeileEnde();
	
	//Ausweisgruppe
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_SAG_KEY'].':',180);
	$DB->SetzeBindevariable('SAG', 'var_NO_sag_rechtebit', $Recht1701);
	$SQL = 'SELECT SAG_KEY, SAG_GRUPPE FROM SONDERAUSWEISGRUPPEN';
	$SQL .= ' WHERE BITAND (SAG_RECHTEBIT, :var_NO_sag_rechtebit) = SAG_RECHTEBIT';
	$Form->Erstelle_SelectFeld('*SAW_SAG_KEY',($Param['SPEICHERN']=='on'?$Param['SAW_SAG_KEY']:''), '25:180',true,$SQL,'','','','',array(array('0',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'])),'','',$DB->Bindevariablen('SAG'));
	$Form->ZeileEnde();
	
	//T�tigkeit
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_TAETIGKEIT'].':',180);
	$DB->SetzeBindevariable('SAG', 'var_NO_sag_rechtebit', $Recht1701);
	$SQL = 'SELECT ROWNUM AS RID, SAW_TAETIGKEIT FROM (SELECT DISTINCT(SAW_TAETIGKEIT) AS SAW_TAETIGKEIT FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT, :var_NO_sag_rechtebit)=SAG_RECHTEBIT) ORDER BY 1 NULLS FIRST)';
	$Form->Erstelle_SelectFeld('*SAW_TAETIGKEIT',($Param['SPEICHERN']=='on'?$Param['SAW_TAETIGKEIT']:''), '25:180',true,$SQL,'','','','',array(array('0',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'])),'','',$DB->Bindevariablen('SAG'));
	$Form->ZeileEnde();	
	
	// Datum - Eintritt / Austritt
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_EINTRITT'].' / ' . $AWISSprachKonserven['SAW']['SAW_AUSTRITT'] .':',180);
	$Form->Erstelle_TextFeld('*SAW_EINTRITT',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SAW_EINTRITT']:''),20,200,true,'','','','D');
	$Form->Erstelle_TextFeld('*SAW_AUSTRITT',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SAW_AUSTRITT']:''),20,200,true,'','','','D');
	$Form->ZeileEnde();

	// Datum - G�ltig ab / G�ltig bis
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SAW']['SAW_GUELTIGAB'].' / ' . $AWISSprachKonserven['SAW']['SAW_GUELTIGBIS'] .':',180);
	$Form->Erstelle_TextFeld('*SAW_GUELTIGAB',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SAW_GUELTIGAB']:''),20,200,true,'','','','D');
	$Form->Erstelle_TextFeld('*SAW_GUELTIGBIS',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SAW_GUELTIGBIS']:''),20,200,true,'','','','D');
	$Form->ZeileEnde();
	
		//Auswahl speichern
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht1700&4) == 4)
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
		
	$Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1308141114");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1308141115");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>