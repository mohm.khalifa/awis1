<?php

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";
$con = awisLogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

$TextKonserven = array();
$TextKonserven[]=array('CRM','%');
$TextKonserven[]=array('CRM','wrd_ZusatzText%');
$TextKonserven[]=array('CRM','wrd_AktionSchreiben');
$TextKonserven[]=array('CAD','txt_SucheBetreuer%');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','txt_Konditionsmodell');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

awis_Debug(1,$_POST);
$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht254 = awisBenutzerRecht($con,254,$AWISBenutzer->BenutzerName());
if($Recht254==0)
{
    awisEreignis(3,1000,'Reifen-Preislisten',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Fehler=false;

if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='')
{
	if ($_POST['sucFIL_ID']=='' or $_POST['sucWGR_ID']=='')
	{		
		$Fehler=true;
	}	
	else
	{
		$Param = array();
		$Param['FIL_ID']=$_POST['sucFIL_ID'];
		$Param['WGR_ID']=$_POST['sucWGR_ID'];
	
		awis_BenutzerParameterSpeichern($con, "Formular_Reifen_Preislisten", $AWISBenutzer->BenutzerName() , $Param);
	}
}
else 
{
	$Param = array();
	$Param['FIL_ID']='';
	$Param['WGR_ID']='';
	
	awis_BenutzerParameterSpeichern($con, "Formular_Reifen_Preislisten", $AWISBenutzer->BenutzerName() , $Param);
}

$Param = awis_BenutzerParameter($con, "Formular_Reifen_Preislisten",$AWISBenutzer->BenutzerName());

echo '<form name=frmMassendruck method=post action=./reifen_Main.php?cmdAktion=Preislisten>';

awis_FORM_FormularStart();

if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='' AND $Fehler==false)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('Preisliste �ffnen',200,'','./reifen_DruckPreisliste.php?Druckart=2');
	awis_FORM_ZeileEnde();	
	awis_FORM_Trennzeile();	
}

//Pr�fen, ob Filiale angemeldet ist
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
if ($UserFilialen!='')
{
	echo '<input type=hidden name=sucFIL_ID value='.$UserFilialen.'>';	
}
else 
{
	awis_FORM_ZeileStart();
	//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',250);
	awis_FORM_Erstelle_TextLabel('Filiale:',250);
	//awis_FORM_Erstelle_TextFeld('*FIL_ID',(isset($Param['FIL_ID'])?$Param['FIL_ID']:''),10,50,true);
	awis_FORM_Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,50,true);
	awis_FORM_ZeileEnde();	
}

awis_FORM_ZeileStart();
//awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',250);
awis_FORM_Erstelle_TextLabel('Warengruppe:',250);
$SQL = "select WGR_ID, WGR_BEZEICHNUNG";
$SQL .= ' FROM Warengruppen';
$SQL .= ' WHERE WGR_ID IN (\'01\', \'02\', \'23\')';
$SQL .= ' ORDER BY WGR_BEZEICHNUNG';
//awis_FORM_Erstelle_SelectFeld('*WGR_ID',(isset($Param['WGR_ID'])?$Param['WGR_ID']:''),250,true,$con,$SQL);
awis_FORM_Erstelle_SelectFeld('*WGR_ID',$Param['WGR_ID'],250,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
awis_FORM_ZeileEnde();	
	
awis_FORM_Trennzeile();

awis_FORM_ZeileStart();
awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();
echo '</form>';

?>