<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	ini_set('max_execution_time',600);

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('VSK','%');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	// Parameter
	$Trenner=";";

	@ob_clean();

	//header("Cache-Control: no-cache, must-revalidate");
	//header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="visitenkarten_export.csv"');
	
	
	$FeldlisteUeberschrift = 'VSK_PERSNR,VSK_VORNAME,VSK_NACHNAME,VSK_TAETIGKEIT,VSK_TELEFON,VSK_FAX,VSK_EMAIL,VSK_FILID,VSK_FILBEZEICHNUNG,VSK_FILSTRASSE,VSK_FILORT,VSK_FILPLZ';
	$FeldListeSelect = 'VSK_PER_NR,VSK_VORNAME,VSK_NACHNAME,VSK_TAETIGKEIT,VSK_TELEFON,VSK_FAX,VSK_EMAIL,VSK_FILID,VSK_FILBEZEICHNUNG,VSK_FILSTRASSE,VSK_FILORT,VSK_FILPLZ';
	
	$FeldWerteUeberschrift='';
	foreach (explode(',', $FeldlisteUeberschrift) as $value)
	{
		$FeldWerteUeberschrift = $FeldWerteUeberschrift.$AWISSprachKonserven['VSK'][$value].';';
	}
	$FeldWerteUeberschrift = rtrim($FeldWerteUeberschrift, ';');

	$SQL = ' select * from (SELECT ' . $FeldListeSelect;
	$SQL .= ' FROM Visitenkarten vsk inner join filialen on vsk.vsk_filid = fil_id where VSK_VSA_ID =  ' . $_GET['ExportVSK_KEY'] . ' AND FIL_GRUPPE = \'1\' AND FIL_LAN_WWSKENN = \'BRD\' )';
 	
	
	// Alle Parameter auswerten
	// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
	//			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
	
	$Abfrage = $SQL;

	// Blockgr��e festlegen
	$Blockgroesse = 5000;

	for($i=1;$i<999;$i++)
	{
		$SQL = 'SELECT ABFRAGE2.* FROM (';
		$SQL .= 'SELECT ABFRAGE1.*, row_number() over(order by 1) AS zeile';
		$SQL .= ' FROM ('.$Abfrage.') ABFRAGE1';
		$SQL .= ') ABFRAGE2 WHERE zeile >= '.(($i-1)*$Blockgroesse).' AND zeile < '.(($i)*$Blockgroesse);

		$rsDaten = $DB->RecordSetOeffnen($SQL);
        $Form->DebugAusgabe(1, $SQL);
		if($rsDaten->AnzahlDatensaetze()==0)
		{
			break;
		}
		if($i==1)		// Beim ersten Mal den Kopf schreiben
		{
			$Felder = $rsDaten->SpaltenNamen();
			$Zeile = '';
			
			$Zeile = $FeldWerteUeberschrift;
			echo $Zeile."\n";
		}

		while(!$rsDaten->EOF())
		{
			$Zeile = '';
			foreach($Felder AS $FeldName)
			{
				if($FeldName!='ZEILE')
				{
					$Text = str_replace("\n",'',$rsDaten->FeldInhalt($FeldName));
					$Text = str_replace("\r",'',$Text);
					$Text = str_replace("\n",'',$Text);
					$Text = str_replace("\t",'',$Text);
					$Zeile .= $Trenner.$Text;
				}
			}
			echo substr($Zeile,1)."\n";

			$rsDaten->DSWeiter();
		}
	}
	
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>