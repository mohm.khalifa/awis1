<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class zukaufsortimentesperren_funktionen
{
    private $_Form;
    private $_DB;
    private $_Benutzer;
    private $_OptionBitteWaehlen;


    function __construct()
    {
        $this->_Benutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
        $this->_OptionBitteWaehlen = '~' . $this->_Form->LadeTextBaustein('Wort','txt_BitteWaehlen',$this->_Benutzer->BenutzerSprache());
    }

    const SperrZuOrdFil = 'FIL';
    const SperrZuOrdLan = 'LAN';
    public function erstelleSperrzuordnungsAjax($Typ, $FeldWert){
        switch ($Typ){
            case self::SperrZuOrdFil:
                $SQL = 'Select distinct FIL_ID, FIL_ID || \' - \' || FIL_BEZ from v_filialen_aktuell order by FIL_ID';
                break;
            case self::SperrZuOrdLan:
                $SQL = 'select LAN_CODE, LAN_LAND from laender where lan_wwskenn is not null order by 2 asc';
                break;
            default:
                $SQL = 'select LAN_CODE, LAN_LAND from laender where lan_wwskenn is not null order by 2 asc';
        }
        if($Typ!=''){
            $Label =  $this->_Form->LadeTextBaustein('ZSS','ZSS_XXX_KEY',$this->_Benutzer->BenutzerSprache());
            $this->_Form->Erstelle_TextLabel($Label.':',170);
            $this->_Form->Erstelle_SelectFeld('~ZSS_XXX_KEY',$FeldWert,'300:300',true,$SQL,$this->_OptionBitteWaehlen);
        }
    }

}