<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN2152">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="Thomas Riedl, ATU">
<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once("awisFilialEbenen.inc");

global $AWISCursorPosition;		// Aus AWISFormular
$AWISVersion = 3;
try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($AWISVersion) .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Rueckfuehrung');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Rueckfuehrung'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader$AWISVersion.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if($AWISBenutzer->HatDasRecht(3900)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',1,"200809161552");
		die();
	}

	$Register = new awisRegister(3900);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

	if($AWISCursorPosition!=='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (Exception $ex)
{
	echo  $ex->getMessage();
}
?>
</body>
</html>