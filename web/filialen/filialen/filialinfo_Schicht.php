<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?

include ("ATU_Header.php");	// Kopfzeile

if(!(awisHatDasRecht(110) || $REMOTE_ADDR=="125.0.3.20"))
{
    awisEreignis(3,1000,'Benutzerverwaltung',"$PHP_AUTH_USER",'','','');
    die("Keine ausreichenden Rechte!");
}

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$rsTaetigkeit = awisOpenRecordset($con,"SELECT * FROM PKUERZEL");
$rsTaetigkeitZeilen = $awisRSZeilen;

$SQL = "SELECT * FROM PERSONALEINSATZPLANUNG, PERSONAL ";
$SQL .= " WHERE PEP_PER_NR='" . $PER_NR . "'";
$SQL .= " AND PEP_PER_NR = PER_NR";
$SQL .= " ORDER BY PEP_JAHR DESC, PEP_KALENDERWOCHE DESC";

$rsSchichtPlan = awisOpenRecordset($con, $SQL);
$rsSchichtPlanZeilen = $awisRSZeilen;

print "<table width=100% border=0><tr><td align=left><span class=DatenFeldGross>Schichtplan f�r " . $rsSchichtPlan["PER_NACHNAME"][0] . ", " . $rsSchichtPlan["PER_VORNAME"][0] . "</span></td>";
print "<td align=right><input align=right type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='$HTTP_REFERER'></td></tr></table>";

print "<br>Mobilfunk: " . $rsSchichtPlan["PER_MOBILTELEFON"][0] . "";
print "<br>E-Mail: <a href=mailto:" . $rsSchichtPlan["PER_EMAIL"][0] . ">" . $rsSchichtPlan["PER_EMAIL"][0] . "</a>";
print "<hr>";


print "<table width=100% border=0>";
print "<tr><td id=Ueberschrift width=100>Woche</td><td width=50 id=Ueberschrift>Mo</td><td width=50 id=Ueberschrift>Di</td><td width=50 id=Ueberschrift>Mi</td><td width=50 id=Ueberschrift>Do</td><td width=50 id=Ueberschrift>Fr</td><td width=50 id=Ueberschrift>Sa</td><td width=50 id=Ueberschrift>So</td><td id=Ueberschrift>Bemerkung</td></tr>";

for($i=0;$i<$rsSchichtPlanZeilen;$i++)
{
	if(strftime("%V%Y",time()) == $rsSchichtPlan["PEP_KALENDERWOCHE"][$i].$rsSchichtPlan["PEP_JAHR"][$i])
	{
		$format = "DatenFeldNormalFett";
		
		for($j=$i-1;$j<=$i+2;$j++)
		{
			if(strftime("%V%Y",time()) == $rsSchichtPlan["PEP_KALENDERWOCHE"][$j].$rsSchichtPlan["PEP_JAHR"][$j])
			{
				$format = "DatenFeldNormalFett";
			}
			else
			{
				$format = "DatenFeldNormal";
			}
	
			print "<tr><td><span class=$format>" . $rsSchichtPlan["PEP_KALENDERWOCHE"][$j] . "/" . $rsSchichtPlan["PEP_JAHR"][$j] . "</span></td>";
				//Wochentage
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_MONTAG"][$j] . "</span></td>";	
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_DIENSTAG"][$j] . "</span></td>";
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_MITTWOCH"][$j] . "</span></td>";
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_DONNERSTAG"][$j] . "</span></td>";
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_FREITAG"][$j] . "</span></td>";
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_SAMSTAG"][$j] . "</span></td>";
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_PEK_ID_SONNTAG"][$j] . "</span></td>";
	
			print "<td><span class=$format>" . $rsSchichtPlan["PEP_BEMERKUNG"][$j] . "</span></td>";

		}
		break;	
	}
	else
	{

	}

	
	print "</tr>";
}

print "</table>";


/*********************************
* Legende ausgeben
* *******************************/

print "<hr><br><span class=DatenFeldGross>Legende</span><br>";

for($j=0;$j<$rsTaetigkeitZeilen;$j++)
{
	print "<br><font size=1>" . $rsTaetigkeit["PEK_ID"][$j] . " = " . $rsTaetigkeit["PEK_TEXT"][$j] . "</font>";
}

awislogoff($con);

?>
</body>
</html>
