<?php
global $AWIS_KEY1;
global $MOB;
require_once 'awisDiagramm.inc';
try {
    
    $MOB->Form->Formular_Start();
    echo '<form name=frmmobilitaetsgarantie action=./mobilitaetsgarantie_Main.php?cmdAktion=Statistik method=POST>';


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel('Statistiken:',170,'font-weight: bold; font-size:15px');
    $MOB->Form->ZeileEnde();


    $AWISDiagramm = new awisDiagramm();
    $MOB->Form->ZeileStart();
    $AWISDiagramm->ErstelleLinienDiagramm('Diagramm1','MOB_Verkaufszahlen_Jahr',array(),200,400,'MOB_DATUM','MOB_ANZ','Monat','Verkaufte Mobis','Jahresstatistik','');
    $AWISDiagramm->ErstelleLinienDiagramm('Diagramm2','MOB_Verkaufszahlen_Monat',array(),200,400,'MOB_DATUM','MOB_ANZ','Tag','Verkaufte Mobis','Monatsstatistik','');
    //$SQL  ='select \'ADAC\' as label, count(*) as value from MOBILITAETSGARANTIEN where MOB_VERS_GUELTIG_AB <= sysdate and MOB_VERS_GUELTIG_BIS >= sysdate and MOB_VERS_NUMMER like \'6%\' ';
    //$SQL .=' union all ';
    $SQL ='select \'AVD\' as label, count(*) as value from MOBILITAETSGARANTIEN where MOB_VERS_GUELTIG_AB <= sysdate and MOB_VERS_GUELTIG_BIS >= sysdate ';//and MOB_VERS_NUMMER like \'7%\' ';

    $AWISDiagramm->ErstelleKreisDiagramm('Aufteilung',$SQL,array(),200,200,'Aktuelle Aufteilung');
    $MOB->Form->Trennzeile('O');
    $MOB->Form->ZeileEnde();

    $Feldbreiten = [];
    $Feldbreiten['Summe'] = 100;
    $Feldbreiten['Anzahl'] = 120;
    $Feldbreiten['Umsatz'] = 200;
    $Feldbreiten['Bereich'] = 130;
    $Feldbreiten['Durchschnitt'] = 200;

    $BereichSQL = 'SELECT DISTINCT SUBSTR(MOB_VERS_NUMMER, 0, 3) AS BEREICH FROM MOBILITAETSGARANTIEN ORDER BY BEREICH ASC';
    $rsBereich = $MOB->DB->RecordSetOeffnen($BereichSQL);

    for ($i = 0; $i <= 2; $i++) {
        if ($i == 0) {
            $Label = $MOB->AWISSprachKonserven['MOB']['MOB_STAT_DE'];
            $GrundZeileSQL = 'SELECT COUNT(MOB_KEY) AS ANZAHL, SUM(MOB_GESAMTUMSATZ) AS UMSATZ, ROUND(AVG(MOB_GESAMTUMSATZ), 2) AS DURCHSCHNITT FROM MOBILITAETSGARANTIEN 
                     WHERE MOB_LAN_ATUSTAATS_NR = 4 AND MOB_VERS_GUELTIG_BIS >= SYSDATE AND MOB_VERS_GUELTIG_AB <= SYSDATE
                     AND SUBSTR(MOB_VERS_NUMMER, 0, 3) = ';
            $SummeSQL = 'SELECT COUNT(MOB_KEY) AS ANZAHL, SUM(MOB_GESAMTUMSATZ) AS UMSATZ, ROUND(AVG(MOB_GESAMTUMSATZ), 2) AS DURCHSCHNITT FROM MOBILITAETSGARANTIEN 
                     WHERE MOB_LAN_ATUSTAATS_NR = 4 AND MOB_VERS_GUELTIG_BIS >= SYSDATE AND MOB_VERS_GUELTIG_AB <= SYSDATE';
        } elseif ($i == 1) {
            $Label = $MOB->AWISSprachKonserven['MOB']['MOB_STAT_NB'];
            $GrundZeileSQL = 'SELECT COUNT(MOB_KEY) AS ANZAHL, SUM(MOB_GESAMTUMSATZ) AS UMSATZ, ROUND(AVG(MOB_GESAMTUMSATZ), 2) AS DURCHSCHNITT FROM MOBILITAETSGARANTIEN 
                     WHERE MOB_LAN_ATUSTAATS_NR <> 4 AND MOB_VERS_GUELTIG_BIS >= SYSDATE AND MOB_VERS_GUELTIG_AB <= SYSDATE
                     AND SUBSTR(MOB_VERS_NUMMER, 0, 3) = ';
            $SummeSQL = 'SELECT COUNT(MOB_KEY) AS ANZAHL, SUM(MOB_GESAMTUMSATZ) AS UMSATZ, ROUND(AVG(MOB_GESAMTUMSATZ), 2) AS DURCHSCHNITT FROM MOBILITAETSGARANTIEN 
                     WHERE MOB_LAN_ATUSTAATS_NR <> 4 AND MOB_VERS_GUELTIG_BIS >= SYSDATE AND MOB_VERS_GUELTIG_AB <= SYSDATE';
        } else {
            $Label = $MOB->AWISSprachKonserven['MOB']['MOB_STAT_GESAMT'];
            $SummeSQL = 'SELECT COUNT(MOB_KEY) AS ANZAHL, SUM(MOB_GESAMTUMSATZ) AS UMSATZ, ROUND(AVG(MOB_GESAMTUMSATZ), 2) AS DURCHSCHNITT FROM MOBILITAETSGARANTIEN 
                     WHERE MOB_VERS_GUELTIG_BIS >= SYSDATE AND MOB_VERS_GUELTIG_AB <= SYSDATE';
        }

        $MOB->Form->ZeileStart();
        $MOB->Form->Erstelle_TextLabel($Label, 200);
        $MOB->Form->ZeileEnde();
        $MOB->Form->ZeileStart();
        $MOB->Form->Erstelle_Liste_Ueberschrift('', $Feldbreiten['Summe']);
        $MOB->Form->Erstelle_Liste_Ueberschrift('Anzahl', $Feldbreiten['Anzahl']);
        $MOB->Form->Erstelle_Liste_Ueberschrift('Umsatz', $Feldbreiten['Umsatz']);
        $MOB->Form->Erstelle_Liste_Ueberschrift('Bereich', $Feldbreiten['Bereich']);
        $MOB->Form->Erstelle_Liste_Ueberschrift('Durchschnitt', $Feldbreiten['Durchschnitt']);
        $MOB->Form->ZeileEnde();

        if ($i != 2) {
            $DS = 0;
            $rsBereich->DSErster();
            while(!$rsBereich->EOF()) {
                $ZeileSQL = $GrundZeileSQL . $MOB->DB->WertSetzen('MOB', 'T', $rsBereich->FeldInhalt('BEREICH'));
                $rsZeile = $MOB->DB->RecordSetOeffnen($ZeileSQL, $MOB->DB->Bindevariablen('MOB', true));
                $MOB->Form->DebugAusgabe(1,$Label . ': <br>' .$MOB->DB->LetzterSQL());

                if ($rsZeile->FeldInhalt('ANZAHL', 'Z') > 0) {
                    $MOB->Form->ZeileStart();
                    $MOB->Form->Erstelle_ListenFeld('platzhalter', '', $Feldbreiten['Summe'], $Feldbreiten['Summe'], false, ($DS % 2), '', '', 'T', 'R');
                    $MOB->Form->Erstelle_ListenFeld('Anzahl', $rsZeile->FeldInhalt('ANZAHL'), $Feldbreiten['Anzahl'], $Feldbreiten['Anzahl'], false, ($DS % 2), '', '', 'T', 'R');
                    $MOB->Form->Erstelle_ListenFeld('Umsatz', $rsZeile->FeldInhalt('UMSATZ'), $Feldbreiten['Umsatz'], $Feldbreiten['Umsatz'], false, ($DS % 2), '', '', 'N2T', 'R');
                    $MOB->Form->Erstelle_ListenFeld('Bereich', $rsBereich->FeldInhalt('BEREICH'), $Feldbreiten['Bereich'], $Feldbreiten['Bereich'], false, ($DS % 2), '', '', 'T', 'R');
                    $MOB->Form->Erstelle_ListenFeld('Durchschnitt', $rsZeile->FeldInhalt('DURCHSCHNITT'), $Feldbreiten['Durchschnitt'], $Feldbreiten['Durchschnitt'], false, ($DS++ % 2), '', '', 'N2T', 'R');
                    $MOB->Form->ZeileEnde();
                }

                $rsBereich->DSWeiter();
            }
        }

        $rsSumme = $MOB->DB->RecordSetOeffnen($SummeSQL);

        $MOB->Form->ZeileStart();
        $MOB->Form->Erstelle_ListenFeld('summe', 'Summe:', $Feldbreiten['Summe'], $Feldbreiten['Summe'], false, 3, '', '', 'T', 'R');
        $MOB->Form->Erstelle_ListenFeld('Anzahl', $rsSumme->FeldInhalt('ANZAHL'), $Feldbreiten['Anzahl'], $Feldbreiten['Anzahl'], false, 3, '', '', 'T', 'R');
        $MOB->Form->Erstelle_ListenFeld('Umsatz', $rsSumme->FeldInhalt('UMSATZ', 'W'), $Feldbreiten['Umsatz'], $Feldbreiten['Umsatz'], false, 3, '', '', 'N2T', 'R');
        $MOB->Form->Erstelle_ListenFeld('Bereich', $Label, $Feldbreiten['Bereich'], $Feldbreiten['Bereich'], false, 3, '', '', 'T', 'R');
        $MOB->Form->Erstelle_ListenFeld('Durchschnitt', $rsSumme->FeldInhalt('DURCHSCHNITT'), $Feldbreiten['Durchschnitt'], $Feldbreiten['Durchschnitt'], false, 3, '', '', 'N2T', 'R');
        $MOB->Form->ZeileEnde();

        $MOB->Form->Trennzeile('O');
    }

    
    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel('Importüberwachung:',170,'font-weight: bold; font-size:15px');
    $MOB->Form->ZeileEnde();


    $SQL = 'Select * from (';
    $SQL .= 'SELECT XDI_KEY ,';
    $SQL .= '   XDI_DATEINAME ,';
    $SQL .= '   XDI_DATUM ,';
    $SQL .= '   XDI_MD5 ,';
    $SQL .= '   XDI_BEMERKUNG ,';
    $SQL .= '   XDI_USER ,';
    $SQL .= '   XDI_USERDAT ';
    $SQL .= ' FROM IMPORTPROTOKOLL';
    $SQL .= ' WHERE xdi_bereich = \'MOB\'';
    $SQL .= ' ORDER BY XDI_DATUM DESC';
    $SQL .= ') where rownum <= 30';

    $rsBSW = $MOB->DB->RecordSetOeffnen($SQL, $MOB->DB->Bindevariablen('BSW'));

    $FeldBreiten = array();
    $FeldBreiten['XDI_KEY'] = 100;
    $FeldBreiten['XDI_DATEINAME'] = 500;
    $FeldBreiten['XDI_DATUM'] = 150;

    $Link = '';
    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_Liste_Ueberschrift('XDI_KEY', $FeldBreiten['XDI_KEY'], '', $Link);
    $MOB->Form->Erstelle_Liste_Ueberschrift('XDI_DATEINAME', $FeldBreiten['XDI_DATEINAME'], '', $Link);
    $MOB->Form->Erstelle_Liste_Ueberschrift('XDI_DATUM', $FeldBreiten['XDI_DATUM'], '', $Link);
    $MOB->Form->ZeileEnde();

    $DS = 0;
    while (!$rsBSW->EOF()) {
        $HG = ($DS % 2);
        $MOB->Form->ZeileStart();
        $MOB->Form->Erstelle_ListenFeld('XDI_KEY', $rsBSW->FeldInhalt('XDI_KEY'), 0, $FeldBreiten['XDI_KEY'], false, $HG, '', '', 'T', 'L');
        $MOB->Form->Erstelle_ListenFeld('XDI_DATEINAME', $rsBSW->FeldInhalt('XDI_DATEINAME'), 0, $FeldBreiten['XDI_DATEINAME'], false, $HG, '', '', 'T', 'L');
        $MOB->Form->Erstelle_ListenFeld('XDI_DATUM', $rsBSW->FeldInhalt('XDI_DATUM'), 0, $FeldBreiten['XDI_DATUM'], false, $HG, '', '', 'T', 'L');
        $MOB->Form->ZeileEnde();

        $rsBSW->DSWeiter();
        $DS++;
    }


    
    $MOB->Form->Formular_Ende();
    //***************************************
    // Schaltflächen für dieses Register
    //***************************************
    $MOB->Form->SchaltflaechenStart();
    $MOB->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $MOB->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $MOB->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $MOB->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $MOB->Form->SchaltflaechenEnde();

    $MOB->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->DebugAusgabe(1, $ex->getSQL());
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $MOB->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>