<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('ZUL','ARUAImportDatei');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10005 = $AWISBenutzer->HatDasRecht(10005);
	if($Recht10005==0)
	{
	    awisEreignis(3,1000,'Zukauflieferscheine',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	echo '<form name=frmZukaufRechnungen action=./zukaufrechnungen_Main.php?cmdAktion=Import method=POST  enctype="multipart/form-data">';
    $Form->Formular_Start();

	if(!empty($_FILES))
	{
        if(($fd = fopen($_FILES['ARUADATEI']['tmp_name'],'r'))!==false)
        {
            $Zeile = fgets($fd);
            if(substr($Zeile,0,4)=='0401')
            {
                // Daten aus der ARUA Datei ziehen
                $awisInfo = new awisWerkzeuge();
                $ZielPfad = $AWISBenutzer->ParameterLesen('ARUA-Datenablage');

                $Datum = substr($Zeile,34,8);
                $LfdNr = str_pad(trim(substr($Zeile,29,5)),10,'0',STR_PAD_LEFT);
                $Lieferant = trim(substr($Zeile,15,9));
                $Jahr = date('Y',$Form->PruefeDatum($Datum,false,false,true));

                $ZielPfad .= '/'.$awisInfo->awisLevel().'/RE/'.$Lieferant.'/'.$Jahr.'/';


                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('Datum:', 150);
                $Form->Erstelle_TextLabel($Form->Format('D',$Datum), 150);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('Lieferant:', 150);
                $Form->Erstelle_TextLabel($Lieferant, 150);
                $Form->ZeileEnde();
                
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('Lfd-Nr:', 150);
                $Form->Erstelle_TextLabel($LfdNr, 150);
                $Form->ZeileEnde();

                // Jetzt einen sauberen Dateinamen aufbauen
                $DateiName = $LfdNr.'_'.$Datum.'.arua';
                //$Form->DebugAusgabe(1,'Dateiname: '.$ZielPfad.'/'.$DateiName);

                $MD5 = md5_file($_FILES['ARUADATEI']['tmp_name']);
				$SQL = 'SELECT XDI_Dateiname';
				$SQL .= ' FROM IMPORTPROTOKOLL';
				$SQL .= ' WHERE XDI_BEREICH = \'ZUR\'';
				$SQL .= ' AND XDI_MD5 = \''.$MD5.'\'';
				$rsXDI = $DB->RecordsetOeffnen($SQL);
				if($rsXDI->EOF())			// Neue Datei
				{
                    $SQL = 'INSERT INTO IMPORTPROTOKOLL';
                    $SQL .= '(XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5, XDI_USER, XDI_USERDAT)';
                    $SQL .= 'VALUES (';
                    $SQL .= '  \'ZUR\'';
                    $SQL .= ', '.$DB->FeldInhaltFormat('T',$DateiName);
                    $SQL .= ', '.$DB->FeldInhaltFormat('D',$Datum);
                    $SQL .= ', '.$DB->FeldInhaltFormat('T',$MD5);
                    $SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
                    $SQL .= ', SYSDATE)';

                    $Erg = true;
                    if(!is_dir($ZielPfad))
                    {
                        $Erg = mkdir($ZielPfad,0770,true);
                    }
                    if($Erg === true)
                    {
                        $Erg = rename($_FILES['ARUADATEI']['tmp_name'],$ZielPfad.'/'.$DateiName);
                    }

                    if($Erg === true)
                    {
                        $Form->ZeileStart();
                        $Form->HinweisText('Datei wurde erfolgreich gespeichert.',awisFormular::HINWEISTEXT_HINWEIS);
                        $Form->ZeileEnde();
                        if($DB->Ausfuehren($SQL)===false)
                        {
                            throw new Exception('Fehler beim Speichern eines Importdateilog.',1);
                        }
                    }
                    else
                    {
                        $Form->ZeileStart();
                        $Form->HinweisText('Fehler beim Hochladen der Datei.',awisFormular::HINWEISTEXT_HINWEIS);
                        $Form->ZeileEnde();

                    }
                }
                else
                {
                    $Form->ZeileStart();
                    $Form->HinweisText('Datei wurde bereits hochgeladen.',awisFormular::HINWEISTEXT_HINWEIS);
                    $Form->ZeileEnde();
                }
            }
            else
            {
                $Form->ZeileStart();
                $Form->HinweisText('Falsche Datei',awisFormular::HINWEISTEXT_HINWEIS);
                $Form->ZeileEnde();
            }
        }
		else
		{
			$Form->ZeileStart();
			$Form->Hinweistext(str_replace('$1','CSV',$AWISSprachKonserven['FEHLER']['err_UngueltigesUploadDateiFormat']),'Warnung','');
			$Form->ZeileEnde();
		}
		
		
	}



    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ARUAImportDatei'].':', 150);
	$Form->Erstelle_DateiUpload('ARUADATEI',300,30,10000000,'');
    $AWISCursorPosition='ARUADATEI';
    $Form->ZeileEnde();

    $Form->Formular_Ende();


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (Exception $ex)
{

}

?>