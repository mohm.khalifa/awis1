<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;


$RechteStufe = awisBenutzerRecht($con, 1900);

if(($RechteStufe & 64)==0)
{
    awisEreignis(3,1900,'Helpdesk-Benutzer',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}


if(isset($_POST['cmdAnlegen_x']))
{
	awis_Debug(1,$_POST);
	
	$SQL = 'SELECT TO_NUMBER(SUBSTR(XBN_NAME,5,4)) AS FIL, TO_NUMBER(SUBSTR(XBN_NAME,10,3)) AS PC';
	//$SQL = 'SELECT TO_NUMBER(SUBSTR(XBN_NAME,5,4)) AS FIL';
	$SQL .= ' FROM benutzer WHERE UPPER(SUBSTR(XBN_NAME,1,4)) = \'FIL-\' AND UPPER(SUBSTR(XBN_NAME,1,8)) <> \'FIL-TEST\'';
	//$SQL .= ' AND TO_NUMBER(SUBSTR(XBN_NAME,5,4)) <0'. $_POST['txtFILNr'];
	$SQL .= ' AND TO_NUMBER(SUBSTR(XBN_NAME,5,4)) =0'. $_POST['txtFILNr'];

	$rsFIL = awisOpenRecordset($con, $SQL);
awis_Debug(1, $SQL, $rsFIL);	
	$rsFILZeilen = $awisRSZeilen;
	
	$PCListe = explode(',',$_POST['txtPCLISTE']);
	$BenutzerName = array();
	$BenutzerName[] = 'fil-'.substr('0000'.$_POST['txtFILNr'],-4,4);
	foreach($PCListe AS $PCNr)
	{
		for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
		{
			if($PCNr == $rsFIL['PC'][$FILZeile])
			{
				break;
			}
		}
		if($FILZeile==$rsFILZeilen)
		{
			$BenutzerName[] = 'fil-'.substr('0000'.$_POST['txtFILNr'],-4,4).'-'.substr('000'.$PCNr,-3,3);
			//$BenutzerName[] = 'fil-'.substr('0000'.$_POST['txtFILNr'],-4,4);
			
		}
	}
		
	if(!isset($_POST['cmdOK']))
	{
		echo '<form name=frmFilialen method=post action=./helpdesk_Main.php?cmdAktion=Filialuser>';
		echo 'Folgende Benutzer werden angelegt:<br><br>';

		foreach ($BenutzerName AS $Name)
		{
			echo '<br><b>'.$Name.'</b>';
		}
		
		echo '<br><br><input title="Benutzerkonten anlegen" name=cmdAnlegen type=image src=/bilder/eingabe_ok.png>';
		echo '<input type=hidden name=cmdOK>';
		echo '<input type=hidden name=txtPCLISTE value="'.$_POST['txtPCLISTE'].'">';
		echo '<input type=hidden name=txtFILNr value="'.$_POST['txtFILNr'].'">';
		echo '</form>';
		die();
	}
	else 
	{
		foreach ($BenutzerName AS $Name)
		{
			echo '<br><b>'.$Name.'</b>';

			$SQL = 'INSERT INTO Benutzer';
			$SQL .= '(XBN_NAME, XBN_VOLLERNAME, XBN_IPADRESSE, XBN_KON_KEY, XBN_USER, XBN_USERDAT)';
			$SQL .= ' VALUES( ';
			$SQL .= ' \'' . $Name . '\'';
			$SQL .= ', \'Filiale ' . substr($Name,4,4) . ', PC '.substr($Name,9,3) .'\'';
			//$SQL .= ', \'Filiale ' . substr($Name,4,4).'\'';
			$SQL .= ', null';
			$SQL .= ', null';
			$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ', SYSDATE';
			$SQL .= ')';

			if(awisExecute($con,$SQL)==false)
			{
				awisErrorMailLink("15090852-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
				awis_Debug(1,$SQL);
			}
			else
			{
				$rsXBN = awisOpenRecordset($con,'SELECT * FROM Benutzer WHERE UPPER(XBN_NAME)=\'' . strtoupper($Name) . '\'');
				if($awisRSZeilen>0)
				{
						// Protokoll
					$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
					$SQL .= ' VALUES(';
					$SQL .= "".$rsXBN['XBN_KEY'][0];
					$SQL .= ",'" . "Anwender ". $rsXBN['XBN_VOLLERNAME'][0] ." wurde angelegt.'";
					$SQL .= ", null";
					$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
					$SQL .= ", SYSDATE";
					$SQL .= ")";

					if(awisExecute($con,$SQL)==false)
					{
						awisErrorMailLink("15090852-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
						awis_Debug(1,$SQL);
					}

						// Gruppenmitgliedschaft
					$SQL = 'INSERT INTO Benutzergruppen';
					$SQL .= '(XBG_XBN_KEY, XBG_XBB_KEY)';
					$SQL .= 'VALUES(';
					$SQL .= ''.$rsXBN['XBN_KEY'][0];
					$SQL .= ', (SELECT XBB_KEY FROM BenutzergruppenBez WHERE XBB_BEZ = \'Filialen\')';
					$SQL .= ')';
					
					if(awisExecute($con,$SQL)==false)
					{
						awisErrorMailLink("15090852-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
						awis_Debug(1,$SQL);
					}
					
						//***************************************
						// Login speichern und Kennwort setzen
						//***************************************
					$SQL = 'INSERT INTO BenutzerLogins';
					$SQL .= '(XBL_XBN_KEY, XBL_LOGIN)';
					$SQL .= ' VALUES( ';
					$SQL .= "".$rsXBN['XBN_KEY'][0];
					$SQL .= ", '".strtoupper($rsXBN['XBN_NAME'][0]) . "'";
					$SQL .= ')';

					if(awisExecute($con,$SQL)==false)
					{
						awisErrorMailLink("15090957-helpdesk_Benutzer.php", 2, $awisDBFehler['message']);
						awis_Debug(1,$SQL);
					}

					//$NeuesKennwort = 'ATU-Weiden';
					$NeuesKennwort = 'atuw';
					// Meldung unterdr�cken
					echo '<span class=HinweisText>';
					@system("/daten/zugang/newuser.sh " . strtolower($rsXBN['XBN_NAME'][0]) . " \"" . $NeuesKennwort . "\"",$erg);
					echo '</span>';

					awis_BenutzerParameterSpeichern($con,"Passwortaendern",$rsXBN['XBN_NAME'][0],0);
					awis_BenutzerParameterSpeichern($con,"PasswortaendernDatum",$rsXBN['XBN_NAME'][0],2000000000);
				}
			}
		}// Einzelne Filialen
	}
}





echo '<form name=frmFilialen method=post action=./helpdesk_Main.php?cmdAktion=Filialuser>';

echo '<table border=0>';

echo '<tr>';
echo '<td colspan=2 class=Feldbez>Neue Filialbenutzer anlegen</td>';
echo '</tr>';

echo '<tr>';
echo '<td>Filialnummer:</td>';

/* Geaendert durch CA am 23.03.07
$SQL = 'SELECT FIL_ID, FIL_BEZ';
$SQL .= ' FROM FILIALEN';
$SQL .= " where FIL_GRUPPE IN ('1','2','3','6','7','8','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O')";
$SQL .= " AND FIL_ID NOT IN (597)";
$SQL .= ' AND FIL_ID NOT IN ';
$SQL .= ' ( ';
$SQL .= 'select TO_NUMBER(SUBSTR(XBN_NAME,5,4)) from benutzer where UPPER(SUBSTR(XBN_NAME,1,4)) = \'FIL-\' AND UPPER(SUBSTR(XBN_NAME,1,8)) <> \'FIL-TEST\' GROUP BY TO_NUMBER(SUBSTR(XBN_NAME,5,4)) HAVING COUNT(*) = 6';
$SQL .= ')';
$SQL .= " ORDER BY FIL_ID";
*/

$SQL = "SELECT FIL_ID, FIL_BEZ";
$SQL .= " FROM FILIALEN";
$SQL .= " WHERE (FIL_GRUPPE IN ('1','2','3','6','7','8','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O')";
$SQL .= " OR FIL_LAN_WWSKENN IS NOT NULL)";
$SQL .= " AND (FIL_BEZ LIKE 'ATU%' or FIL_BEZ in ('Schneidenbach Wolfgang', 'Autoteile Mueller'))"; // 02.06.07 SP, um "externe" Filialen erweitert
$SQL .= " AND FIL_ID<1000";
$SQL .= " AND FIL_ID NOT IN";
$SQL .= " (";
$SQL .= "select TO_NUMBER(SUBSTR(XBN_NAME,5,4)) from benutzer where UPPER(SUBSTR(XBN_NAME,1,4)) = 'FIL-' AND UPPER(SUBSTR(XBN_NAME,1,8)) <> 'FIL-TEST' GROUP BY TO_NUMBER(SUBSTR(XBN_NAME,5,4))";
$SQL .= ")";
//$SQL .= " union select FIL_ID, FIL_BEZ FROM FILIALEN where fil_id=982"; // CA 08.10.08 Temp-Eintrag f�r "Nicht"-Filialen
$SQL .= " ORDER BY FIL_ID";


$rsFIL = awisOpenRecordset($con, $SQL);
$rsFILZeilen = $awisRSZeilen;

echo '<td><select name=txtFILNr>';
for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
{
	echo '<option value='.$rsFIL['FIL_ID'][$FILZeile].'>'.$rsFIL['FIL_ID'][$FILZeile].' ('.$rsFIL['FIL_BEZ'][$FILZeile].')</option>';
}
echo '</select></td>';
echo '</tr>';

echo '<tr>';
echo '<td>PC-Nummern:</td>';
echo '<td><input readonly name=txtPCLISTE value="110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,131,132,133,134" size=80></td>';
echo '</tr>';

echo '<tr>';
echo '<td>Benutzerformat:</td>';
echo '<td>fil-XXXX-YYY</td>';
//echo '<td>fil-XXXX</td>';
echo '</tr>';

echo '</table>';

echo '<input title="Benutzerkonten anlegen" name=cmdAnlegen type=image src=/bilder/eingabe_ok.png>';

echo '</form>';
?>