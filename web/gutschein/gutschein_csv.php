<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisGiftcard.inc');

global $AWIS_KEY1;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	ini_set('max_execution_time',600);

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GSD','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	// Parameter
	$Trenner=";";

	if(isset($_GET['GSK_AUFTRAGNR']))
	{
		@ob_clean();

		header('Pragma: public');
		header('Cache-Control: max-age=0');

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="gutschein.txt"');

		//***********************************************************************
		// Export aller Gutscheinnummern + Pr�fziffer;Betrag
		// ausser GS wird bzw. ist storniert
		//***********************************************************************
		$SQL = 'SELECT gs.GPD_GUTSCHEINNR, gs.GPD_PRUEFZ, pos.GSP_BETRAG';
		$SQL .= ' FROM GUTSCHEINE gs';
		$SQL .= ' LEFT JOIN GUTSCHEINPOSDATEN pos on gs.GPD_AUFTRAGNR = pos.GSP_AUFTRAGNR';
		$SQL .= ' AND gs.GPD_POSITION = pos.GSP_POSITION';
		$SQL .= ' WHERE gs.GPD_AUFTRAGNR = ' . $DB->WertSetzen('GS','N0',($_GET['GSK_AUFTRAGNR']),false) . '';
		$SQL .= ' AND gs.GPD_BUCHART <> \'S\'';
		$rsGPD = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GS',true));

		while(!$rsGPD->EOF())
		{
			$Zeile = '';
			$Text = $Form->Format('T',$rsGPD->FeldInhalt('GPD_GUTSCHEINNR'));
			$Text.= $Form->Format('T',$rsGPD->FeldInhalt('GPD_PRUEFZ'));
			$Text.= $Trenner.$Form->Format('N2',$rsGPD->FeldInhalt('GSP_BETRAG'));
			$Text = str_replace("\n",'',$Text);
			$Text = str_replace("\r",'',$Text);
			$Text = str_replace("\n",'',$Text);
			$Text = str_replace("\t",'',$Text);
			$Zeile .= $Text;

			echo substr($Zeile,0)."\r\n";

			$rsGPD->DSWeiter();
		}
	}
	elseif (isset($_GET['PZGENERATOR']))
	{
		if($_FILES['Importdatei']['tmp_name']!= '')
		{
			//UploadDatei �ffnen, dann Pr�fziffer hinzuf�gen
			if (($fd = fopen($_FILES['Importdatei']['tmp_name'], 'r+')) !== false)
			{
				//Importieren
				$Import = new awisGiftcard();
				$Erg =  $Import->_PruefZifferGenerator($fd);
			}
			else
			{
				echo"Bitte eine neue Datei ausw�hlen! Das sind alte Daten vom letzten Import!!!\r\n";
			}
		}
		else
		{
			echo"Bitte eine neue Datei ausw�hlen! Das sind alte Daten vom letzten Import!!!\r\n";
		}

		if (file_exists('/daten/web/export/Gutschein-PZGenerator/GSPZ.txt'))
		{
			$fe=fopen('/daten/web/export/Gutschein-PZGenerator/GSPZ.txt','r');
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="pzgenerator.txt"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			readfile('/daten/web/export/Gutschein-PZGenerator/GSPZ.txt');
			fclose($fe);
		}
		else
		{
			//Datei wurde noch nicht importiert bzw. nicht richtig importiert
			exit;
		}

	}


}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>