<?php
global $CursorFeld;
global $AWISBenutzer;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FEB','%');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','Filiale');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','NurAktive');
$TextKonserven[]=array('Liste','lst_JaNein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3800=awisBenutzerRecht($con,3800);

echo "<br>";
echo "<form name=frmSuche method=post action=./filialebenen_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_BEZEICHNUNG'].':',190);
awis_FORM_Erstelle_TextFeld('*FEB_BEZEICHNUNG','',20,200,true);
$CursorFeld='sucFEB_BEZEICHNUNG';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_EBENE'].':',190);
awis_FORM_Erstelle_TextFeld('*FEB_EBENE','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filiale'].':',190);
awis_FORM_Erstelle_TextFeld('*FEB_FILIALE','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['KON']['KON_NAME'].':',190);
awis_FORM_Erstelle_TextFeld('*KON_NAME','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['NurAktive'].':',190);
$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
awis_FORM_Erstelle_SelectFeld('*AKTIVE','',200,true,$con,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


awis_FORM_SchaltflaechenStart();
	// Zur�ck zum Men�
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht3800&4) == 4)		// Hinzuf�gen erlaubt?
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

awis_FORM_SchaltflaechenEnde();


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>