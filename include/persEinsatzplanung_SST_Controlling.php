<?php

/**
 * Klasse zum ermitteln der Personaleinsatzdaten (Checkbericht) f�r das Controlling
 * @author : Stefan Oppl
 * @version : 1.0
 *
 */
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

class SST_Controlling
{
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_Form;

    /**
     * Konstruktor
     * �ffnet alle Resourcen (z.B. Datenbank) die im Script gebraucht werden
     */
    public function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_FORM = new awisFormular();
    }
}

?>