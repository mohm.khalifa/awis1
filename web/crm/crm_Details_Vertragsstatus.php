<?php
global $con;
global $Recht3709;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CAQ','%');
$TextKonserven[]=array('CGK','CGK_JAHRESBONUSMODELL');
$TextKonserven[]=array('Liste','lst_CAQ_STATUS');
$TextKonserven[]=array('Liste','lst_CAQ_VERTRAGSNUTZUNG');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','VERTRAGSNUMMER');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_KonditionsblattDrucken');
$TextKonserven[]=array('Wort','lbl_KundenstammblattDrucken');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3709 = awisBenutzerRecht($con,3709,$AWISBenutzer->BenutzerName());
if($Recht3709==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//********************************************************
// Daten suchen
//********************************************************
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$AWIS_KEY2=0;
while($AWIS_KEY2==0)
{
	$SQL = 'SELECT * ';
	$SQL .= ' FROM CRMAkquiseStand';
	$SQL .= ' WHERE CAQ_CAD_KEY=0'.$AWIS_KEY1;

	// Zeilen begrenzen
	$rsCAQ = awisOpenRecordset($con, $SQL);
	if($awisRSZeilen==0)
	{
		$SQL = 'INSERT  INTO CRMAkquiseStand';
		$SQL .= ' (CAQ_CAD_KEY, CAQ_STATUS)';
		$SQL .= ' VALUES('.$AWIS_KEY1.', 0)';
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			awisLogoff($con);
			die();
		}
	}
	else
	{
		$AWIS_KEY2=$rsCAQ['CAQ_KEY'][0];
	}
}

//********************************************************
// Daten anzeigen
//********************************************************
echo '<form name=frmCRMAkquise action=./crm_Main.php?cmdAktion=Details&Seite=Vertragsstatus&Seite=Vertragsstatus method=POST  enctype="multipart/form-data">';

echo '<input type=hidden name=txtCAQ_KEY value='.$AWIS_KEY2. '>';

awis_FORM_FormularStart();
$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

	// Infozeile zusammenbauen
$Felder = array();
$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCAQ['CAQ_USER'][0]));
$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCAQ['CAQ_USERDAT'][0]));
awis_FORM_InfoZeile($Felder,'');

$EditModus=(($Recht3709&2)!=0);

awis_FORM_ZeileStart();
$CursorFeld='txtCAQ_DATUM';
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_DATUM'].':',150);
awis_FORM_Erstelle_TextFeld('CAQ_DATUM',(isset($rsCAQ['CAQ_DATUM'][0])?$rsCAQ['CAQ_DATUM'][0]:''),10,100,$EditModus,'','','','D');
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_STATUS'].':',150);
$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_CAQ_STATUS']);
awis_FORM_Erstelle_SelectFeld('CAQ_STATUS',$rsCAQ['CAQ_STATUS'][0],100,$EditModus,$con,'','','3','','',$Daten,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_VERTRAGSBEGINN'].':',150);
awis_FORM_Erstelle_TextFeld('CAQ_VERTRAGSBEGINN',(isset($rsCAQ['CAQ_VERTRAGSBEGINN'][0])?$rsCAQ['CAQ_VERTRAGSBEGINN'][0]:''),10,100,$EditModus,'','','','D');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_CAP_KEY'].':',150);
$SQL = "select CAP_KEY, CAP_Nachname || ', ' || COALESCE(CAP_VORNAME,'') AS APName ";
$SQL .= ' FROM CRMAnsprechpartner';
$SQL .= ' WHERE CAP_CAD_KEY=0'.$AWIS_KEY1;
$SQL .= ' ORDER BY CAP_NACHNAME, CAP_VORNAME';
awis_FORM_Erstelle_SelectFeld('CAQ_CAP_KEY',($AWIS_KEY2==0?'':$rsCAQ['CAQ_CAP_KEY'][0]),200,$EditModus,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
awis_FORM_ZeileEnde();


awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['VERTRAGSNUMMER'].':',150);
awis_FORM_Erstelle_TextFeld('CAQ_VERTRAGSNUMMER',$rsCAQ['CAQ_VERTRAGSNUMMER'][0],20,150,$EditModus);
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_VERBANDNR'].':',150);
awis_FORM_Erstelle_TextFeld('CAQ_VERBANDNR',$rsCAQ['CAQ_VERBANDNR'][0],20,150,$EditModus);
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_VERTRAGSNUTZUNG'].':',150);
$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_CAQ_VERTRAGSNUTZUNG']);
awis_FORM_Erstelle_SelectFeld('CAQ_VERTRAGSNUTZUNG',$rsCAQ['CAQ_VERTRAGSNUTZUNG'][0],100,$EditModus,$con,'','','','','',$Daten,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAQ']['CAQ_BEMERKUNG'].':',150);
awis_FORM_Erstelle_Textarea('CAQ_BEMERKUNG',($AWIS_KEY1===0?'':$rsCAQ['CAQ_BEMERKUNG'][0]),300,80,4,$EditModus,'border-style=solid;');
awis_FORM_ZeileEnde();

// InfoFeld Versicherungs-ID
$EditModusVersID=(($Recht3709&32)!=0);
$BindeVariablen = array();
$SQL = InfoFeldSQLErstellen(4500,$AWIS_KEY1,'Info',$BindeVariablen);
$rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150);
awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditModusVersID,'','','',$rsCIN['CIT_FORMAT'][0]);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('',150);
awis_FORM_ZeileEnde();

// Die Felder zum Kartenstatus usw. aus OPAL sind Infofelder werden hier
// aber einzeln ausgegeben da diese bei Update durch OPAL farbig dargestellt
// werden muessen

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift('OPAL - Status',0,'','');
awis_FORM_ZeileEnde();

// InfoFeld Vertragsstatus
$BindeVariablen = array();
$SQL = InfoFeldSQLErstellen(4005,$AWIS_KEY1,'OPAL Status',$BindeVariablen);
$rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
awis_FORM_ZeileStart();
$style = (PruefeObOPALUpdate('INFO_VERTRAGSSTATUS')?'color:#FF0000':'');
awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],false,$style,'','',$rsCIN['CIT_FORMAT'][0]);
awis_FORM_ZeileEnde();

// InfoFeld Vertragssperren
$BindeVariablen = array();
$SQL = InfoFeldSQLErstellen(4008,$AWIS_KEY1,'OPAL Status',$BindeVariablen);
$rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
$vertragsSperren = 'Nein';
if($rsCIN['CIN_WERT'][0] <> 'keine Sperre')
{
    $vertragsSperren = 'Ja';
}
awis_FORM_ZeileStart();
$style = (PruefeObOPALUpdate('INFO_VERTRAGSSPERREN')?'color:#FF0000':'');
awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$vertragsSperren,$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],false,$style,'','',$rsCIN['CIT_FORMAT'][0]);
awis_FORM_ZeileEnde();

// InfoFeld Kartensperren
$BindeVariablen = array();
$SQL = InfoFeldSQLErstellen(4006,$AWIS_KEY1,'OPAL Status',$BindeVariablen);
$rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
$kartenSperre  = 'Nein';
if($rsCIN['CIN_WERT'][0] <> 'keine Sperre' && !is_null($rsCIN['CIN_WERT'][0]))
{
    $kartenSperre = 'Ja';
}
awis_FORM_ZeileStart();
$style = (PruefeObOPALUpdate('INFO_KARTENSPERREN')?'color:#FF0000':'');
awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$kartenSperre,$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],false,$style,'','',$rsCIN['CIT_FORMAT'][0]);
awis_FORM_ZeileEnde();

// InfoFeld Poststatus
$BindeVariablen = array();
$SQL = InfoFeldSQLErstellen(4007,$AWIS_KEY1,'OPAL Status',$BindeVariablen);
$rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
$postStatus = 'Nein';
if($rsCIN['CIN_WERT'][0] <> 0)
{
    $postStatus = 'Ja';
}
awis_FORM_ZeileStart();
$style = (PruefeObOPALUpdate('INFO_POSTSTATUS')?'color:#FF0000':'');
awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$postStatus,$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],false,$style,'','',$rsCIN['CIT_FORMAT'][0]);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('',150);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_JAHRESBONUSMODELL'].':',150);
if($rsCAQ['CAQ_VERTRAGSNUMMER'][0]!='')
{
	$SQL = 'SELECT CGK_JAHRESBONUSMODELL_ID,CGK_JAHRESBONUSMODELL';
	$SQL .= ' FROM CRMGewerbekunden';
	$SQL .= ' WHERE CGK_ACCOUNT_ID='.awis_FeldInhaltFormat('N0',$rsCAQ['CAQ_VERTRAGSNUMMER'][0]);
	$SQL .= ' AND ROWNUM=1';
	$rsCGK=awisOpenRecordset($con,$SQL);

	awis_FORM_Erstelle_TextFeld('CGK_JAHRESBONUSMODELL_ID',(isset($rsCGK['CGK_JAHRESBONUSMODELL_ID'][0])?$rsCGK['CGK_JAHRESBONUSMODELL_ID'][0].' - '.$rsCGK['CGK_JAHRESBONUSMODELL'][0]:''),80,300,false,'');
	awis_FORM_ZeileEnde();
}

//***********************************************************
//Vertragsinfos
//***********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMInfoTypen';
$SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY='.$AWIS_KEY1;
$SQL .= ' WHERE CIT_SEITE = \'Vertragsstatus\' AND CIT_STATUS=\'A\'';
$SQL .= ' ORDER BY CIT_BEREICH, CIT_SORTIERUNG';
$rsCIN = awisOpenRecordset($con, $SQL);
$rsCINZeilen = $awisRSZeilen;

$LetzterBereich = '';
for($CINZeile=0;$CINZeile<$rsCINZeilen;$CINZeile++)
{
	if($LetzterBereich!=$rsCIN['CIT_BEREICH'][$CINZeile])
	{
		awis_FORM_Trennzeile();
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($rsCIN['CIT_BEREICH'][$CINZeile],0,'','');
		awis_FORM_ZeileEnde();
		$LetzterBereich=$rsCIN['CIT_BEREICH'][$CINZeile];
	}
	awis_FORM_ZeileStart();
	echo '<input type=hidden name=txtCIN_KEY value=0'.$rsCIN['CIN_KEY'][$CINZeile].'>';
	awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][$CINZeile].':',250);
	if($rsCIN['CIT_DATENQUELLE'][$CINZeile]!='')
	{
		switch(substr($rsCIN['CIT_DATENQUELLE'][$CINZeile],0,3))
		{
			case 'TXT':
				$Felder = explode(':',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
				$Daten = awis_LadeTextKonserven($con, array(array($Felder[1],$Felder[2])), $AWISSprache);
				$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
				awis_FORM_Erstelle_SelectFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,$con,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],$rsCIN['CIT_STANDARDWERT'][$CINZeile],'','',$Daten);
				break;
			case 'SQL':
				$Felder = explode(':',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
				awis_FORM_Erstelle_SelectFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,$con,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],$rsCIN['CIT_STANDARDWERT'][$CINZeile]);
				break;
			default:
				awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,'','','',$rsCIN['CIT_FORMAT'][$CINZeile],'','',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
				break;
		}

	}
	elseif(strpos($rsCIN['CIT_ZEICHEN'][$CINZeile],',')===false)
	{
		awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,'','','',$rsCIN['CIT_FORMAT'][$CINZeile],'','',$rsCIN['CIT_STANDARDWERT'][$CINZeile]);
	}
	else
	{
		$Groesse = explode(',',$rsCIN['CIT_ZEICHEN'][$CINZeile]);
		awis_FORM_Erstelle_Textarea('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$Groesse[0],$Groesse[1],$EditModus);
	}
	awis_FORM_ZeileEnde();
}
awis_FORM_FormularEnde();

if (($Recht3709&16)!=0)
{
	awis_FORM_SchaltflaechenStart();
	awis_FORM_Schaltflaeche('href', 'cmdDruckKonditionsblatt', './crm_DruckKonditionsblatt.php?DruckArt=1&CADKEY=0'.$AWIS_KEY1, '/bilder/pdf_gross.png', $AWISSprachKonserven['Wort']['lbl_KonditionsblattDrucken'], '.');
	awis_FORM_Schaltflaeche('href', 'cmdDruckKundenstammblatt', './crm_DruckKundenstammblatt.php?DruckArt=1&CADKEY=0'.$AWIS_KEY1, '/bilder/bestaetigen.png', $AWISSprachKonserven['Wort']['lbl_KundenstammblattDrucken'], '.');
	awis_FORM_SchaltflaechenEnde();
}
?>