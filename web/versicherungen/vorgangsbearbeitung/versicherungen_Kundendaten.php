<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('V_VERS_FORM_KUNDENDATEN', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    $SQL = 'SELECT v.kunden_art,v.firku_art,v.anrede,v.titel,v.name1,v.name2,v.name3,v.strasse,v.hnr,v.plz,v.ort,v.rufnr1,v.rufnr2 ';
    $SQL .= 'FROM v_vers_form_kundendaten v, versvorgangsstatus vv ';
    $SQL .= 'WHERE v.filid = vv.vvs_filid AND ';
    $SQL .= 'v.wanr = vv.vvs_wanr AND ';
    $SQL .= 'vv.vvs_key = ' . $DB->WertSetzen('VVS', 'N0', $Param);

    $rsKndDat = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS'));

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenKundendaten action=./versicherungen_Details.php?cmdAktion=Details&Seite=Kundendaten' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $EditRecht = $Recht9500 & 2;

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['KUNDEN_ART'] . ':', 150);
    $Form->Erstelle_TextFeld('KUNDEN_ART', $rsKndDat->FeldInhalt('KUNDEN_ART'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['STRASSE'] . ':', 150);
    $Form->Erstelle_TextFeld('STRASSE', $rsKndDat->FeldInhalt('STRASSE'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['FIRKU_ART'] . ':', 150);
    $Form->Erstelle_TextFeld('FIRKU_ART', $rsKndDat->FeldInhalt('FIRKU_ART'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['HNR'] . ':', 150);
    $Form->Erstelle_TextFeld('HNR', $rsKndDat->FeldInhalt('HNR'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['ANREDE'] . ':', 150);
    $Form->Erstelle_TextFeld('ANREDE', $rsKndDat->FeldInhalt('ANREDE'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['PLZ'] . ':', 150);
    $Form->Erstelle_TextFeld('PLZ', $rsKndDat->FeldInhalt('PLZ'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['TITEL'] . ':', 150);
    $Form->Erstelle_TextFeld('TITEL', $rsKndDat->FeldInhalt('TITEL'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['ORT'] . ':', 150);
    $Form->Erstelle_TextFeld('ORT', $rsKndDat->FeldInhalt('ORT'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['NAME1'] . ':', 150);
    $Form->Erstelle_TextFeld('NAME1', $rsKndDat->FeldInhalt('NAME1'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['RUFNR1'] . ':', 150);
    $Form->Erstelle_TextFeld('RUFNR1', $rsKndDat->FeldInhalt('RUFNR1'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['NAME2'] . ':', 150);
    $Form->Erstelle_TextFeld('NAME2', $rsKndDat->FeldInhalt('NAME2'), 360, 360, false);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['RUFNR2'] . ':', 150);
    $Form->Erstelle_TextFeld('RUFNR2', $rsKndDat->FeldInhalt('RUFNR2'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_KUNDENDATEN']['NAME3'] . ':', 150);
    $Form->Erstelle_TextFeld('NAME3', $rsKndDat->FeldInhalt('NAME3'), 360, 360, false);
    $Form->ZeileEnde();
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905131842");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>