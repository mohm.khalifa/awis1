<?
/****************************************************************************************************
*
* 	Artikel-Liste f�r Lieferantenartikel
*
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt. 2003
*
****************************************************************************************************/
// Variablen
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$LARRechtestufe = awisBenutzerRecht($con, 402) ;

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';


		//********************************************************************
		// Aktualisieren gew�hlt?
		// Block muss identisch sein zu artikel_Artikel.php !!!!
		//********************************************************************
if(isset($_POST['cmdSuche_x']))
{
	$Param = $_POST['txtAuswahlSpeichern'];
	$Param .= ";" . $_POST['txtAST_ATUNR'];
	$Param .= ";" . $_POST['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $_POST['txtTEI_SUCH'];
	$Param .= ";" . $_POST['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
	$Param .= ";" . $_POST['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
	$Param .= ";" . $_POST['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
	$Param .= ";" . $_POST['txtTEI_SUCH_Exakt'];		// Exakte Suche
	$Param .= ";" . $_POST['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . $_POST['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
	$Param .= ";" . $_POST['txtKommentar'];				// Kommentar im Artikel
	$Param .= ";" . $_POST['txtTEI_SUCH_XXX'];			// Suche nach beliebiger Nummer
	$Param .= ";" . $_POST['txtWUG_KEY'];				// Suche nach Warenuntergruppe
	$Param .= ";" . $_POST['txtWGR_KEY'];				// Suche nach Warengruppe
	$Param .= ";" . $_POST['txtOEPreise'];				// Immer OE-Preise zeigen

	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $AWISBenutzer->BenutzerName(), "");

	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));
    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerLAR", $AWISBenutzer->BenutzerName()));
}
	// SQL Anweisung erstellen
$SQL = "SELECT DISTINCT LAR_KEY, LAR_LARTNR, LIE_NAME1, LAR_BEZWW, LAR_REKLAMATIONSKENNUNG";
$TabellenListe = " FROM AWIS.Lieferantenartikel, AWIS.Lieferanten ";

$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($_GET["LARKEY"]) OR $Artikel[0]!='')
{
	if(isset($_GET["LARKEY"]))
	{
		$LARKEY = $_GET["LARKEY"];
//		awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $AWISBenutzer->BenutzerName(), $LARKEY);
	}
	else
	{
		$LARKEY=$Artikel[0];
	}
	$Bedingung = " AND LAR_KEY = " . $LARKEY;

//**********************************************************
// Daten speichern
//**********************************************************

	if(awis_NameInArray($_POST, "cmdSpeichern")!='')
	{

		$SQL = "UPDATE LieferantenArtikel SET LAR_BEMERKUNGEN='" . substr($_POST['txtLAR_BEMERKUNGEN'],0,255) . "'";
		$SQL .= ",LAR_ALTERNATIVARTIKEL=" . $_POST['txtLAR_ALTERNATIVARTIKEL'];
		$SQL .= ",LAR_ALTELIEFNR=" . $_POST['txtLAR_ALTELIEFNR'];
//????		$SQL .= ",LAR_AUSLAUFARTIKEL=" . $_POST['txtLAR_AUSLAUFARTIKEL'];
		$SQL .= ",LAR_PROBLEMARTIKEL=" . $_POST['txtLAR_PROBLEMARTIKEL'];
		$SQL .= ",LAR_PROBLEMBESCHREIBUNG='" . $_POST['txtLAR_PROBLEMBESCHREIBUNG'] . "'";
		$SQL .= ",LAR_GEPRUEFT=TO_DATE('" . $_POST['txtLAR_GEPRUEFT'] . "','DD.MM.RRRR')";
		$SQL .= ",LAR_APP_ID=" . $_POST['txtLAR_APP_ID'] . "";
		$SQL .= ",LAR_MUSTER=" . $_POST['txtLAR_MUSTER'] . "";
		$SQL .= ",LAR_USER='" . $AWISBenutzer->BenutzerName() . "', LAR_USERDAT=SYSDATE";
		$SQL .= " WHERE LAR_KEY=0" . $_POST["txtLAR_KEY"];

		$Erg = awisExecute($con, $SQL);
		if($Erg === FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler['message']);
		}
	}  // Ende Speichern von Daten
}
													//***********************************************************
elseif(!isset($_GET["LARKEY"]))			// Artikel suchen (Liste anzeigen, oder einen)
													//***********************************************************
{
	$DSAnz=0;		// Insgesamt gelesene Zeilen

//	var_dump($Param);

	if($Param[6]!='' OR $Param[11]!='')		// LiefNr
	{
		if($Param[7] == 'on')
		{
			$Bedingung .= " AND LAR_LARTNR " . awisLIKEoderIST($Param[3],true) . "";
		}
		else
		{
//			$TabellenListe .= ", TeileInfos ";
//			$Bedingung .= " AND TEI_KEY2 = LAR_KEY AND TEI_SUCH2 " . awisLIKEoderIST($Param[3],True, False, False) . "";
// TODO: Neues Feld in der Maske: LAR_SUCH_LARTNR
//			$Bedingung .= " AND SUCHWORT(LAR_LARTNR)" . awisLIKEoderIST($Param[3],true,false,true,false) . "";
			$Bedingung .= " AND LAR_SUCH_LARTNR " . awisLIKEoderIST($Param[3],true,false,false,true) . "";
		}
	}
	if($Param[1] != '')		// Artikel-Nummer
	{
		$TabellenListe .= ", TeileInfos ";
		$Bedingung .= " AND (TEI_KEY2 = LAR_KEY AND TEI_KEY1 IN (SELECT AST_KEY FROM AWIS.ArtikelStamm WHERE AST_ATUNR " . awisLIKEoderIST($Param[1],True, False, False) . "))";
	}
	if($Param[9] != '')		// OE-Nummer
	{
		if(strstr($TabellenListe,"TeileInfos")=='')
		{
			$TabellenListe .= ", TeileInfos ";
		}
		$Bedingung .= " AND (TEI_KEY1 = LAR_KEY AND TEI_KEY2 IN (SELECT OEN_KEY FROM AWIS.OENUMMERN WHERE OEN_NUMMER " . awisLIKEoderIST($Param[3],True, False, False) . "))";
	}

	if($Bedingung == '')	// Keine vern�nftige Bedingung
	{
		die('<span class=HinweisText>Diese Suchkombination ist nicht m�glich</span>');
	}

	$SQL .= $TabellenListe . " WHERE LAR_LIE_NR = LIE_NR(+) AND (" . substr($Bedingung,5) . ") ";
	$SQL .= " ORDER BY LAR_LARTNR";

	// Max. Datenanzahl einschr�nken
	$SQL = 'SELECT * FROM (' . $SQL . ') WHERE ROWNUM <= 0' . ($MaxDSAnzahl+1);

	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;

	if($rsArtikelZeilen == 1)
	{
		$DSAnz=-1;			// Einzelanzeige
		$LARKEY = $rsArtikel["LAR_KEY"][0];
	}
	else
	{
		echo "<table width=100% border=1>";

		echo "\n<tr>";	// �berschrift
		echo "\n<td id=FeldBez>Artikel-Nummer</td>";
		echo "\n<td id=FeldBez>Lieferant</td>";
		echo "\n<td id=FeldBez>Bezeichnung</td>";
		echo "\n<td id=FeldBez>Rekl.-Kenn.</td>";
		echo "\n<td id=FeldBez>ATU-Nr</td>";
		echo "</tr>\n";

 		for($LARNr=0;$LARNr<$rsArtikelZeilen;$LARNr++)
		{
			echo "<tr>";

			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][$LARNr] . ">";
			echo "" . $rsArtikel["LAR_LARTNR"][$LARNr] . "</a></td>";
			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LIE_NAME1"][$LARNr] . "</td>";
			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LAR_BEZWW"][$LARNr] . "</td>";
			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["LAR_REKLAMATIONSKENNUNG"][$LARNr] . "</td>";

			$SQL = 'SELECT DISTINCT TEI_Wert1, TEI_Key1 FROM TeileInfos WHERE TEI_Key2=' . $rsArtikel['LAR_KEY'][$LARNr] . ' AND TEI_ITY_ID2=\'LAR\' AND TEI_ITY_ID1=\'AST\'';
			$rsART = awisOpenRecordset($con,$SQL);
			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">";
			for($rsArtZeile=0;$rsArtZeile<$awisRSZeilen;$rsArtZeile++)
			{
				echo ($rsArtZeile>0?', ':'') . '<a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=' . $rsArt['TEI_KEY1'][$rsArtZeile] . '>' . $rsART['TEI_WERT1'][$rsArtZeile] . '</a>';
			}
			echo '</td>';

			echo "</tr>";
			if($LARNr==$MaxDSAnzahl)
			{
				echo '<tr><td colspan=99><span Class=HinweisText>Das Maximum von ' . $LARNr . ' Zeilen ist erreicht</span></td></tr>';
				break;
			}
		}

		echo "</table>";
		echo "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $LARNr Artikel gefunden.</font>";
	}
}


/*********************************************************************
*
* Datensatz l�schen oder zur�cksetzen
*
**********************************************************************/

if(awis_NameInArray($_POST, "cmdLARLoeschen")!='' AND !isset($_POST["cmdSpeichernAbbruch"]))
{
	if(isset($_POST["cmdSpeichernBestaetigung"]))
	{
		if($_POST['txtLAR_BEKANNTWW']=='0')
		{
			$SQL = "DELETE FROM AWIS.LieferantenArtikel WHERE LAR_KEY=" . $_POST['txtLAR_KEY'];
		}
		else
		{
			$SQL = "UPDATE LieferantenArtikel SET LAR_USER='LOESCHEN' WHERE LAR_KEY=" . $_POST['txtLAR_KEY'];
		}
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
	else
	{
		echo "<form name=frmArtikel method=post>";

		echo "<input type=hidden name=txtLAR_KEY value='" . $_POST['txtLAR_KEY'] . "'>";
		echo "<input type=hidden name=txtLAR_BEKANNTWW value='" . $_POST['txtLAR_BEKANNTWW'] . "'>";
		echo "<input type=hidden name=cmdLARLoeschen>";

		echo "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Lieferantenartikel " . ($_POST['txtLAR_BEKANNTWW']==0?'l�schen':'zur�cksetzen') . " wollen?</span><br><br>";
		echo "<input accesskey=j type=submit value=\"Ja, Aktion ausf�hren\" name=cmdSpeichernBestaetigung>";
		echo "<input accesskey=n type=submit value=\"Nein, Aktion nicht ausf�hren\" name=cmdSpeichernAbbruch>";

		echo "</form>";
		awisLogoff($con);
		die();
	}
}

//********************************************************************
// Einzelner Datensatz -> Formular anzeigen
//********************************************************************
//die("1");

if($DSAnz==-1)
{
	awis_BenutzerParameterSpeichern($con, "AktuellerLAR", $AWISBenutzer->BenutzerName(), $LARKEY);
	echo "<form name=frmLieferantenArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerLAR", $AWISBenutzer->BenutzerName()));

	$SQL = "SELECT LIEFERANTENARTIKEL.*, LIE_NAME1, ";
	$SQL .= "APP_NAME ";
	$SQL .= " FROM AWIS.LIEFERANTENARTIKEL, AWIS.LIEFERANTEN, AWIS.ARTIKELPRUEFPERSONAL ";
	$SQL .= " WHERE LAR_KEY=0" . $LARKEY;
	$SQL .= " AND LAR_LIE_NR = LIE_NR(+) AND APP_ID(+) = LAR_APP_ID";

//echo "<hr>$SQL<hr>";
	$rsArtikel = awisOpenRecordset($con, $SQL);
	if($rsArtikel==FALSE)
	{
		awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
		awisLogoff($con);
		die();
	}

	if($awisRSZeilen==1)		// Nur ein Datensatz gefunden
	{
		echo "<table border=0 width=100%>";
		echo "<tr><td><font size=5><b>Lieferanten Artikel " . $rsArtikel['LAR_LARTNR'][0] . "</b></font></td>";
		echo "<td width=100><font size=1>" . $rsArtikel["LAR_USER"][0] ."<br>"  . $rsArtikel["LAR_USERDAT"][0] . "</font></td>";
		echo "<td width=40 backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&Liste=True';></td>";
		if($rsArtikel["LAR_USER"][0]=='LOESCHEN')
		{
			echo "<tr><td><span class=HinweisText>Dieser Artikel wird beim n�chsten Datenabgleich zur�ckgesetzt. Um dies zu verhindern, speichern Sie den Artikel erneut ab (ALT+S).</span></font></td>";
		}
		echo "</tr></table>";


			// Zeile 1
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=120><col width=230><col width=120><col width=*></colgroup>";
		echo "<tr>";
		echo "<td id=FeldBez>Nummer</td><td><input type=hidden name=txtLAR_KEY value=". $rsArtikel['LAR_KEY'][0] . ">" . $rsArtikel['LAR_LARTNR'][0] . "</td>";
		echo "<td id=FeldBez>Bezeichnung</td><td>" . $rsArtikel['LAR_BEZWW'][0] . "</td>";
		echo "</tr>";
		echo "</table>";


			// Zeile 1a
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=120><col width=230><col width=120><col width=*><col width=120><col width=*><col width=120><col width=*></colgroup>";
		echo "<td id=FeldBez>Reklamation</td><td>" . ($rsArtikel["LAR_REKLAMATIONSKENNUNG"][0]==''?'- -':$rsArtikel["LAR_REKLAMATIONSKENNUNG"][0]) . "</td>";
		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			echo "<td id=FeldBez>Alternativ</td><td>";
			echo "<select name=txtLAR_ALTERNATIVARTIKEL>";
			echo "<option value=0 " . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==0?'selected':'') . ">Nein</option>";
			echo "<option value=1 " . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==1?'selected':'') . ">Ja</option>";
			echo "</select></td>";


			echo "<td id=FeldBez>Alte Nr</td><td>";
			echo "<select name=txtLAR_ALTELIEFNR>";
			echo "<option value=0 " . ($rsArtikel['LAR_ALTELIEFNR'][0]==0?'selected':'') . ">Nein</option>";
			echo "<option value=1 " . ($rsArtikel['LAR_ALTELIEFNR'][0]==1?'selected':'') . ">Ja</option>";
			echo "</select></td>";
		}
		else
		{
			echo "<td id=FeldBez>Alternativ</td><td>" . ($rsArtikel['LAR_ALTERNATIVARTIKEL'][0]==0?'Nein':'Ja') . "</td>";
			echo "<td id=FeldBez>Alte Nr</td><td>" . ($rsArtikel['LAR_ALTELIEFNR'][0]==0?'Nein':'Ja') . "</td>";
		}
		echo "<td id=FeldBez><input type=hidden name=txtLAR_BEKANNTWW value='" . $rsArtikel['LAR_BEKANNTWW'][0] . "'>WWS-Artikel</td><td>" . ($rsArtikel['LAR_BEKANNTWW'][0]==1?'Ja':'Nein') . "</td>";
		echo "</tr>";
		echo "</table>";


			// Zeile 1b
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=140><col width=60><col width=*></colgroup>";
		echo "<td id=FeldBez>Lieferant</td>";
		echo "<td>" . $rsArtikel['LAR_LIE_NR'][0] . "</td>";
		echo "<td>" . $rsArtikel['LIE_NAME1'][0] . "</td>";
		echo "</table>";

			// Zeile 2
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=140><col width=60><col width=100><col width=*></colgroup>";
		echo "<tr>";

		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			echo "<td id=FeldBez>Problemartikel</td><td><select name=txtLAR_PROBLEMARTIKEL>";
			echo "<option value=0 " . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'selected':'') . ">Nein</option>";
			echo "<option value=1 " . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==1?'selected':'') . ">Ja</option>";
			echo "</select></td>";

			echo "" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'<td id=FeldBez>Problem':'<td bgcolor=#FF0000>Problem');
			echo "<td><input name=txtLAR_PROBLEMBESCHREIBUNG size=70 value='" . $rsArtikel['LAR_PROBLEMBESCHREIBUNG'][0] . "'></td>";
		}
		else
		{
			echo "<td id=FeldBez>Problemartikel</td><td>" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'Nein':'Ja') . "</td>";
			echo "" . ($rsArtikel['LAR_PROBLEMARTIKEL'][0]==0?'<td id=FeldBez>Problem':'<td bgcolor=#FF0000>Problem') . "</td><td>" . $rsArtikel['LAR_PROBLEMBESCHREIBUNG'][0] . "</td>";
		}

		echo "</tr>";
		echo "</table>";

			// Zeile 3
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=140><col width=60><col width=70><col width=*><col width=100><col width=60></colgroup>";
		echo "<tr>";
		If(($LARRechtestufe & 2)==2)		// Bearbeiten
		{
			echo "<td id=FeldBez>Gepr�ft</td><td><input name=txtLAR_GEPRUEFT size=11 value='" . $rsArtikel['LAR_GEPRUEFT'][0] . "'></td>";

			echo '<td id=FeldBez>durch';
			if(awisBenutzerRecht($con, 601)>0)		// Stammdaten: Pr�fpersonal
			{
				echo '<a href=/stammdaten/pruefpersonal.php?APPID='. $rsArtikel['LAR_APP_ID'][0] . '&Zurueck=artikel_LIEF&LARKEY=' . $rsArtikel['LAR_KEY'][0] . '><img border=0 src=/bilder/suche.png alt=Pr�fpersonal></a>';
			}

			echo '</td><td><select name=txtLAR_APP_ID>';
			$rsAPP = awisOpenRecordset($con, "SELECT APP_ID, APP_NAME FROM AWIS.ArtikelPruefpersonal WHERE APP_AKTIVBIS >= SYSDATE ORDER BY APP_NAME");
			for($_i=0;$_i<$awisRSZeilen;$_i++)
			{
				echo "<option value=" . $rsAPP['APP_ID'][$_i] . " " . ($rsArtikel['LAR_APP_ID'][0]==$rsAPP['APP_ID'][$_i]?'selected':'') . ">" . $rsAPP['APP_NAME'][$_i] . "</option>";
			}
			echo "</select></td>";
			unset($rsAPP);

			echo "<td id=FeldBez>Muster</td><td><select name=txtLAR_MUSTER>";
			echo "<option value=0 " . ($rsArtikel['LAR_MUSTER'][0]==0?'selected':'') . ">Nein</option>";
			echo "<option value=1 " . ($rsArtikel['LAR_MUSTER'][0]==1?'selected':'') . ">Ja</option>";
			echo "</select></td>";
		}
		else
		{
			echo "<td id=FeldBez>Gepr�ft</td><td>" . $rsArtikel['LAR_GEPRUEFT'][0] . "</td>";
			echo "<td id=FeldBez>durch</td><td>" . $rsArtikel['APP_NAME'][0] . "</td>";
			echo "<td id=FeldBez>Muster</td><td>" . ($rsArtikel['LAR_MUSTER'][0]==0?'Nein':'Ja') . "</td>";
		}
		echo "</tr>";
		echo "</table>";

				// Zeile 4
		echo "<table border=1 width=100%>";
		echo "<colgroup><col width=140></col><col width=*></col></colgroup>";
		echo "<tr>";
		If(($LARRechtestufe & 2)==2)
		{
			echo "<td id=FeldBez>Bemerkung</td><td><textarea cols=90 rows=3 type=text name=txtLAR_BEMERKUNGEN>" . $rsArtikel['LAR_BEMERKUNGEN'][0] . "</textarea></td>";
		}
		else
		{
			echo "<td id=FeldBez>Bemerkung</td><td>" . $rsArtikel['LAR_BEMERKUNGEN'][0] . "</td>";
		}
		echo "</tr>";

		//	Bei Lieferantenartikelsets

		$SQL = "SELECT * ";
		$SQL .= " FROM TeileInfos T1 ";
		$SQL .= " INNER JOIN Lieferantenartikel ON T1.TEI_SUCH2 = LAR_LARTNR";
		$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
		$SQL .= " AND T1.TEI_SUCH2 " . awisLIKEoderIST($rsArtikel['LAR_LARTNR'][0],false,false,true,false);

		$rsSuche = awisOpenRecordset($con, $SQL);
		if($awisRSZeilen>0)
		{
			echo '<tr><td id=FeldBez>Bestehend aus</td><td>';
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo ($DSNr==0?'':', ') . '<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY='. $rsSuche['TEI_KEY1'][$DSNr] . '>'  . $rsSuche['TEI_WERT1'][$DSNr] . '</a>';
			}
			echo '</td></tr>';
		}
		unset($rsSuche);
			// Teil eines vorhandenen Sets
		$SQL = "SELECT * ";
		$SQL .= " FROM TeileInfos T1 ";
		$SQL .= " INNER JOIN Lieferantenartikel ON T1.TEI_SUCH2 = LAR_LARTNR";
		$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
		$SQL .= " AND T1.TEI_SUCH1 " . awisLIKEoderIST($rsArtikel['LAR_LARTNR'][0],false,false,true,false);

		$rsSuche = awisOpenRecordset($con, $SQL);
		if($awisRSZeilen>0)
		{
			echo '<tr><td id=FeldBez>Enthalten in</td><td>';
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo ($DSNr==0?'':', ') . '<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY='. $rsSuche['TEI_KEY2'][$DSNr] . '>'  . $rsSuche['TEI_WERT2'][$DSNr] . '</a>';
			}
			echo '</td></tr>';
		}
		unset($rsSuche);





		echo "</table>";

		echo "<hr>";

		$SQL = "SELECT * FROM AWIS.Gebrauchsnummern, TEILEINFOS WHERE GNR_KEY = TEI_KEY2 AND TEI_KEY1 =0" . $rsArtikel["LAR_KEY"][0];
		$rsGNr = awisOpenRecordset($con, $SQL);
//awis_Debug(1,$SQL);		
		$rsGNRZeilen = $awisRSZeilen;
		
		if($rsGNRZeilen>0)
		{
			echo "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
			for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
			{
				echo "<td>Gebrauchsnummer:</td>";
				echo "<td><input type=hidden name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
				echo "</td>";
				echo "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
				echo "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
				echo "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
			}
			echo "</tr></table>";
		}
		else
		{
			echo "\n<!--Keine Gebrauchsnummern gefunden-->\n";
		}

		// Unterregister mit abh�ngigen Daten
		$cmdAktion = (!isset($_GET["Seite"]) || $_GET["Seite"]==''?"ATUArtikel":$_GET["Seite"]);
		awis_RegisterErstellen(42, $con, $cmdAktion);


		If(($LARRechtestufe & 2)==2)
		{
			echo "<input type=hidden name=Speichern Value=True>";
			echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . (isset($rsArtikel["AST_KEY"][0])?$rsArtikel["AST_KEY"][0]:'') . "&Speichern=True'>";

			if($rsArtikel['LAR_BEKANNTWW'][0]==0)			// L�schen m�glich, da kein WWS Artikel
			{
				echo "&nbsp;<input type=image alt=\"L�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdLARLoeschen accesskey=x onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][0] . "&Loeschen=True'>";
			}
			else
			{
				echo "&nbsp;<input type=image alt=\"Informationen l�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdLARLoeschen accesskey=x onclick=location.href='./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsArtikel["LAR_KEY"][0] . "&Loeschen=True'>";
			}

						// Cursor in das erste Feld setzen
			echo "<Script Language=JavaScript>";
			echo "document.getElementsByName(\"txtLAR_ALTERNATIVARTIKEL\")[0].focus();";
			echo "</Script>";

		}

	}
	else		// Gar keinen gefunden
	{
		echo "<span class=HinweisText>Keine Lieferantenartikel gefunden</span>";
	}

	unset($rsGNr);

}

echo "</form>";


/********************************************************************************************************************
*
* _ArtikelInfo(ID, Recordset, ArtikelNummer)
*
*********************************************************************************************************************/

function _ArtikelInfo($ID, $rsOEI, $LARKey, $FeldName)
{
	for($_i=0;$rsOEI["OEI_LAR_KEY"][$_i]!='';$_i++)
	{
		if($rsOEI["OEI_LAR_KEY"][$_i]==$LARKey AND $rsOEI["OEI_OIT_ID"][$_i]==$ID)
		{
			return $rsOEI[$FeldName][$_i];
		}
	}
	return('');
}