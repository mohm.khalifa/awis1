<?php

/**
 * ***********************************************************************
 * Enthaelt Methoden zum erledigten verschiedenen kleineren Sachen aus
 * dem AWIS-Tagesgesch�ft. Die Methoden werden in richtiger Reihenfolge
 * je nach Zweck von der Verschiedenes.php aufgerufen. 
 * ***********************************************************************
 * Erstellt:
 * @author 17.11.2011 Stefan Oppl
 * @version 1.0
 * �nderungen:
 * 12.12.2011 OP : Methode 'existiertLiefArtNr' erstellt & getestet
 * 12.12.2011 OP : Methode 'setAuslaufLiefArtNr' erstellt & getestet
 * ***********************************************************************
 */

require_once 'awisDatenbank.inc';

class awisVerschiedenes
{
	
	/**
	 * Datenbankobjekt
	 * @var objekt
	 */
	private $_db;
	
	/**
	 * Enter description here ...
	 */
	public function __construct(){
		$this->_db = awisDatenbank::NeueVerbindung('AWIS');
		$this->_db->Oeffnen();
		$this->_fileCSV = new fileCSV();
	}
	
	/**
	 * Prueft ob der uebergebene Lieferant in der
	 * AWIS-Tabelle Lieferanten vorhanden ist.
	 * @param string $liefNr
	 * @return boolean (false = nicht drin / true = drin)
	 */
	private function existiertLieferant($liefNr) {
		$SQL = 'SELECT * ';
		$SQL .= 'FROM lieferanten ';
		$SQL .= "WHERE lie_nr = '".$liefNr."'";
		
		return ($this->_db->ErmittleZeilenAnzahl($SQL) > 0);
	}
	
	/**
	 * Ersetzt die alte Lieferantennummer durch die neue Lieferantennummer.
	 * Sollte eine der beiden Lieferantennummern in der Tabelle Lieferant nicht
	 * vorhanden sein bzw. zu der alten Lieferantennummer keine LiefArt hinterlegt
	 * sind werden Exceptions ausgeloest. Sollten bereits unter der neuen LiefNr
	 * die gleichen Artikel wie unter der alten LiefNr existieren werden die der
	 * alten LiefNr geloescht. 
	 * @param string $liefNrAlt
	 * @param string $liefNrNeu
	 */
	public function ersetzeLiefNrInLiefArtNr($liefNrAlt,$liefNrNeu) 
	{
		/*pruefen ob alte LieferantenArtikelnummer vorhanden ist*/
		if (!$this->existiertLieferant($liefNrAlt)) {
			throw new Exception('ersetzteLiefNrInLiefArtNr__liefNrAlt_nicht_vorhanden', '20111117');
		}
		else{
			if(!$this->existiertLieferant($liefNrNeu)){
				throw new Exception('ersetzteLiefNrInLiefArtNr__liefNrNeu_nicht_vorhanden', '20111117');
			}
			else{
				$SQL = 'SELECT * ';
				$SQL .= 'FROM lieferantenartikel ';
				$SQL .= "WHERE lar_lie_nr ='".$liefNrAlt."'";
				
				if($this->_db->ErmittleZeilenAnzahl($SQL) == 0){
					throw new Exception('ersetzeLiefNrInLiefArtNr__liefNrAlt_keine_LiefArtNr', '20111117');
				}
				else{
					/*pruefen ob es zur neuen Lieferantennummer schon LiefArtNr gibt
					 * die auch unter der alten existieren. Wenn ja w�rde der Update einen
					 * Constraint-Fehler bringen da LiefArtNr und LiefNr den Primaerschluessel
					 * bilden*/
					$SQL = 'SELECT * ';
					$SQL .= 'FROM lieferantenartikel ';
					$SQL .= 'WHERE lar_lartnr in ';
					$SQL .= '(SELECT lar_lartnr ';
					$SQL .= 'FROM lieferantenartikel ';
					$SQL .= "WHERE lar_lie_nr = '".$liefNrNeu."') ";
					$SQL .= "AND lar_lie_nr = '".$liefNrAlt."'";
					
					if($this->_db->ErmittleZeilenAnzahl($SQL) > 0){
						/*Es gibt unter der neuen LiefNr ArtNr die es auch
						 * unter der alten gibt. Daher die alten loeschen*/
						$SQL = 'DELETE ';
						$SQL .= 'FROM lieferantenartikel ';
						$SQL .= 'WHERE lar_key in ';
						$SQL .= '(SELECT lar_key ';
						$SQL .= 'FROM lieferantenartikel ';
						$SQL .= 'WHERE lar_lartnr in ';
						$SQL .= '(SELECT lar_lartnr ';
						$SQL .= 'FROM lieferantenartikel ';
						$SQL .= "WHERE lar_lie_nr = '".$liefNrNeu."') ";
						$SQL .= "AND lar_lie_nr = '".$liefNrAlt."')";
						
						$this->_db->Ausfuehren($SQL);
					}
					
					$SQL = 'UPDATE lieferantenartikel ';
					$SQL .= "SET lar_lie_nr = '".$liefNrNeu."', ";
					$SQL .= "lar_user = 'awis', ";
					$SQL .= 'lar_userdat = sysdate ';
					$SQL .= "WHERE lar_lie_nr = '".$liefNrAlt."'";
					
					$this->_db->Ausfuehren($SQL);
				}
			}
		}
	}
	
	/**
	 * Prueft ob die uebergebene Kombination aus Lieferantemartikel
	 * und Lieferant AWIS-Tabelle Lieferantenartikel vorhanden ist.
	 * @param string $liefNr (Lieferantennummer)
	 * @param string $liefARtNr (Lieferantenartikelnummer)
	 * @return boolean (false = nicht vorhanden / true = vorhanden)
	 */
	private function existiertLiefArtNr($liefNr,$liefArtNr)
	{
		$SQL = 'SELECT LAR_KEY ';
		$SQL .= 'FROM lieferantenartikel ';
		$SQL .= "WHERE lar_lie_nr = '" . $liefNr . "' ";
		$SQL .= "AND lar_lartnr = '" .$liefArtNr . "'";
		
		return ($this->_db->ErmittleZeilenAnzahl($SQL) == 1);		
	}
	
	/**
	 * Setzt die Auslaufkennung + Bemerkung fuer uebergebenen LiefArt.
	 * Wenn LiefArtNr + LiefNr nicht existieren wird false ansonsten true
	 * zurueckgegeben. 
	 * @param unknown_type $liefNr
	 * @param unknown_type $liefArtNr
	 * @param unknown_type $bemerkung
	 */
	public function setAuslaufLiefArtNr($liefNr,$liefArtNr,$bemerkung)
	{
		if(!$this->existiertLiefArtNr($liefNr, $liefArtNr))
		{
			return false;
		}
		else
		{
			$SQL = 'UPDATE lieferantenartikel ';
			$SQL .= 'set LAR_BEMERKUNGEN = CASE WHEN LAR_BEMERKUNGEN IS NOT NULL THEN LAR_BEMERKUNGEN ';
			$SQL .= "||chr(13)||chr(10)||'" . $bemerkung . "' ELSE '" . $bemerkung . "' END, ";
			$SQL .= "LAR_AUSLAUFARTIKEL = 1, LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 ";
			$SQL .= "THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END, LAR_USER='AWIS', ";
			$SQL .= "LAR_USERDAT=sysdate where LAR_LARTNR = trim('" . $liefArtNr . "') ";
			$SQL .= "AND LAR_LIE_NR ='" . $liefNr . "'";
					
			$this->_db->Ausfuehren($SQL);
			
			return true;
		}
	}
}

?>