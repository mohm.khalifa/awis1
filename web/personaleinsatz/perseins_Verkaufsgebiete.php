<?php

// Variablen
global $awisDBFehler;
global $awisRSZeilen;
global $con;
global $rsVKG;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 1305);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

$Params = explode(";",awis_BenutzerParameter($con, "HBST_Suche", $AWISBenutzer->BenutzerName()));
$EingabeFeld = '';

/*************************************************
* Daten speichern
*************************************************/
if(isset($_GET['cmdLoeschen']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM VERKAUFSGEBIETE WHERE VKG_ID='" . $_POST["txtVKG_ID_old"] . "'";
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			var_dump($SQL);

			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			die();
		}

		die('<br>Das Verkaufsgebiet wurde erfolgreich gel�scht<br><br><a href=./perseins_Main.php?cmdAktion=Gebiete>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmVKG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtVKG_ID_old value=" . $_GET["VKG_ID"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie das Verkaufsgebiet l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

		print "</form>";
		awisLogoff($con);
		die();
	}

}

if(isset($_POST['cmdSpeichern_x']))
{



	if($_POST['txtVKG_ID_old']!='')		// �ndern
	{
		$SQL = '';
		if($_POST['txtVKG_BEMERKUNG'] != $_POST['txtVKG_BEMERKUNG_old'])
		{
			$SQL .= ", VKG_BEMERKUNG='" . $_POST['txtVKG_BEMERKUNG'] . "'";
		}
		if($_POST['txtVKG_MIT_KEY'] != $_POST['txtVKG_MIT_KEY_old'])
		{
			$SQL .= ", VKG_MIT_KEY='" . $_POST['txtVKG_MIT_KEY'] . "'";
		}
		if($_POST['txtVKG_TKDL_MIT_KEY'] != $_POST['txtVKG_TKDL_MIT_KEY_old'])
		{
			$SQL .= ", VKG_TKDL_MIT_KEY='" . $_POST['txtVKG_TKDL_MIT_KEY'] . "'";
		}
		if($_POST['txtVKG_MITBemerkung'] != $_POST['txtVKG_MITBemerkung_old'])
		{
			$SQL .= ", VKG_MITBEMERKUNG='" . $_POST['txtVKG_MITBemerkung'] . "'";
		}
		if($_POST['txtVKG_TKDLBemerkung'] != $_POST['txtVKG_TKDLBemerkung_old'])
		{
			$SQL .= ", VKG_TKDLBEMERKUNG='" . $_POST['txtVKG_TKDLBemerkung'] . "'";
		}

		if($SQL!='')
		{
			$SQL .= ", VKG_USER='" . $AWISBenutzer->BenutzerName() . "', VKG_USERDAT=SYSDATE";
			$SQL = 'UPDATE Verkaufsgebiete SET ' . substr($SQL,2);
			$SQL .= " WHERE VKG_ID='" . $_POST['txtVKG_ID_old'] . "'";

			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Verkaufsgebiete.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
		}
	}
	elseif($_POST['txtVKG_ID_old']==0)		// NEU
	{
		$SQL = 'INSERT INTO AWIS.VERKAUFSGEBIETE (VKG_ID, VKG_MIT_KEY, VKG_BEMERKUNG, VKG_USER, VKG_USERDAT)';
		$SQL .= 'VALUES (';
		$SQL .= "'" . $_POST['txtVKG_ID'] . "'";
		$SQL .= ",'" . $_POST['txtVKG_MIT_KEY'] . "'";
		$SQL .= ",'" . $_POST['txtVKG_BEMERKUNG'] . "'";
		$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "', SYSDATE)";

		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("perseins_Mitarbeiter.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
}

/*************************************************
* Daten anzeigen
*************************************************/

echo "<form name=frmGebiete method=post action=./perseins_Main.php?cmdAktion=Gebiete>";

								//****************************************
if(isset($_GET['VKG_ID']))		// Bearbeitung eines Datensatzes
								//****************************************
{
	$rsVKG = awisOpenRecordset($con, "SELECT * FROM Verkaufsgebiete WHERE VKG_ID='" . $_GET['VKG_ID'] . "'");
	echo '<table Class=DatenTabelle width=100% border=1>';

	echo '<tr>';
	echo '<td Class=FeldBez>Gebiet</td>';
	echo '<td>';
	echo "<input type=hidden name=txtVKG_ID_old value='" . $rsVKG['VKG_ID'][0] . "'>";
	echo "" . $rsVKG['VKG_ID'][0] . "";
	echo '</td></tr>';

		// Gebietsleiter //
	echo '<tr>';
	echo '<td Class=FeldBez>Gebietsleiter</td>';
	echo '<td>';
	echo "<input type=hidden name=txtVKG_MIT_KEY_old value='" . $rsVKG['VKG_MIT_KEY'][0] . "'>";
	$rsGebLeiter = awisOpenRecordset($con,"SELECT MIT_KEY, MIT_BEZEICHNUNG, MIT_STATUS FROM Mitarbeiter WHERE MIT_MTA_ID = 10 AND MIT_STATUS <> 'S' ORDER BY MIT_BEZEICHNUNG");
	$rsGebLeiterZeilen = $awisRSZeilen;
    echo "<select name=txtVKG_MIT_KEY><option value=0>Mitarbeiter ausw�hlen...</option>";
    for($i=0; $i<$rsGebLeiterZeilen;$i++)
	{
    	print "<option ";
		if($rsGebLeiter["MIT_KEY"][$i] == $rsVKG['VKG_MIT_KEY'][0])
		{
			print " selected ";
		}
		print "value=" . $rsGebLeiter["MIT_KEY"][$i] . ">". $rsGebLeiter["MIT_BEZEICHNUNG"][$i] . "</option>";
    }
    echo "</select>&nbsp;";
	echo "<span class=FeldBez>Bemerkung zum GBL</span>&nbsp;<input size=50 type=hidden name=txtVKG_MITBemerkung_old value='" . $rsVKG['VKG_MITBEMERKUNG'][0]  ."'><input size=50 name=txtVKG_MITBemerkung value='" . $rsVKG['VKG_MITBEMERKUNG'][0]  ."'></td>";
	echo "</tr>";


		// TKDL //
	echo '<tr>';
	echo '<td Class=FeldBez>TKDL</td>';
	echo '<td>';
	echo "<input type=hidden name=txtVKG_TKDL_MIT_KEY_old value='" . $rsVKG['VKG_TKDL_MIT_KEY'][0] . "'>";
	$rsGebLeiter = awisOpenRecordset($con,"SELECT MIT_KEY, MIT_BEZEICHNUNG FROM Mitarbeiter WHERE MIT_MTA_ID = 30 AND MIT_STATUS <> 'S' ORDER BY MIT_BEZEICHNUNG");
	$rsGebLeiterZeilen = $awisRSZeilen;
    echo "<select name=txtVKG_TKDL_MIT_KEY><option value=0>Mitarbeiter ausw�hlen...</option>";
    for($i=0; $i<$rsGebLeiterZeilen;$i++)
	{
    	print "<option ";
		if($rsGebLeiter["MIT_KEY"][$i] == $rsVKG['VKG_TKDL_MIT_KEY'][0])
		{
			print " selected ";
		}
		print "value=" . $rsGebLeiter["MIT_KEY"][$i] . ">". $rsGebLeiter["MIT_BEZEICHNUNG"][$i] . "</option>";
    }
    echo "</select>&nbsp;";
	echo "<span class=FeldBez>Bemerkung zum TKDL</span>&nbsp;<input type=hidden name=txtVKG_TKDLBemerkung_old value='" . $rsVKG['VKG_TKDLBEMERKUNG'][0] . "'><input size=50 name=txtVKG_TKDLBemerkung value='" . $rsVKG['VKG_TKDLBEMERKUNG'][0]  ."'></td>";
	echo "</tr>";


	echo '<tr>';
	echo '<td Class=FeldBez>Bemerkung</td>';
	echo '<td>';
	echo "<input type=hidden name=txtVKG_BEMERKUNG_old value='" . $rsVKG['VKG_BEMERKUNG'][0] . "'>";
	echo "<input type=text size=50 name=txtVKG_BEMERKUNG value='" . $rsVKG['VKG_BEMERKUNG'][0] . "'>";
	echo '</td>';
	echo '</tr>';


	echo '<tr><td colspan=2>';
	echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '<a href=./perseins_Main.php?cmdAktion=Gebiete&cmdLoeschen&VKG_ID=' . $rsVKG['VKG_ID'][0] . '><img src=/bilder/Muelleimer_gross.png border=0></a>';
	echo '</td></tr>';

}
			//****************************
else		// Liste anzeigen
			//****************************
{
	echo '<table Class=DatenTabelle width=100% border=1>';
	echo '<tr>';
	if(($RechteStufe&2)==2 OR ($RechteStufe&4)==4)	// �ndern oder Hinzuf�gen?
	{
		echo '<td Class=FeldBez>&nbsp;</td>';
	}
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Gebiete&Sort=VKG_ID>Gebiet</a></td>';
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Gebiete&Sort=MIT_BEZEICHNUNG>Gebietsleiter</a></td>';
	echo '<td Class=FeldBez><a href=./perseins_Main.php?cmdAktion=Gebiete&Sort=MIT_BEZEICHNUNG_TKDL>TKDL</a></td>';
	echo '<td Class=FeldBez>Bemerkung</td>';
	echo '</tr>';

		// Neue Datens�tze anf�gen?

	if(($RechteStufe&4)==4)
	{
		echo '<tr><td>';
		echo "<input type=hidden name=txtVKG_ID_old value=''>";
		echo "<input type=hidden name=txtVKG_MIT_KEY_old value='" . $rsVKG['VKG_MIT_KEY'][0] . "'>";
		echo "<input type=hidden name=txtVKG_TKDL_MIT_KEY_old value='" . $rsVKG['VKG_MIT_KEY'][0] . "'>";
		echo "<input type=hidden name=txtVKG_BEMERKUNG_old value='" . $rsVKG['VKG_BEMERKUNG'][0] . "'>";

		echo "<input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		echo '</td>';

		echo "<td><input type=text size=10 name=txtVKG_ID value=''></td>";

			/********** Gebietsleiter ************/
		$rsMTA = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_MTA_ID = 10 AND MIT_STATUS <> 'S' ORDER BY MIT_BEZEICHNUNG");
		$rsMTAZeilen = $awisRSZeilen;
	    echo "<td><select name=txtVKG_MIT_KEY><option value=0>Mitarbeiter ausw�hlen...</option>";
	    for($i=0; $i<$rsMTAZeilen;$i++)
		{
	    	print "<option ";
			if($rsMTA["MIT_KEY"][$i] == $rsVKG['VKG_MIT_KEY'][0])
			{
				print " selected ";
			}
			print "value=" . $rsMTA["MIT_KEY"][$i] . ">". $rsMTA["MIT_BEZEICHNUNG"][$i] . "</option>";
	    }
	    echo "</select></td>";


			/********** TKDL ************/
		$rsMTA = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_MTA_ID = 30 AND MIT_STATUS <> 'S' ORDER BY MIT_BEZEICHNUNG");
		$rsMTAZeilen = $awisRSZeilen;
	    echo "<td><select name=txtVKG_TKDL_MIT_KEY><option value=0>Mitarbeiter ausw�hlen...</option>";
	    for($i=0; $i<$rsMTAZeilen;$i++)
		{
	    	print "<option ";
			if($rsMTA["MIT_KEY"][$i] == $rsVKG['VKG_TKDL_MIT_KEY'][0])
			{
				print " selected ";
			}
			print "value=" . $rsMTA["MIT_KEY"][$i] . ">". $rsMTA["MIT_BEZEICHNUNG"][$i] . "</option>";
	    }
	    echo "</select></td>";


		echo "<td><input type=text size=50 name=txtVKG_BEMERKUNG value=''></td>";


		echo '</tr>';
	}

		// Ende neuer Datensatz
		// TODO: Felder ausw�hlen
	$SQL = 'SELECT Verkaufsgebiete.*, Mitarbeiter.MIT_BEZEICHNUNG, Mitarbeiter.MIT_STATUS, M2.MIT_STATUS AS MIT_STATUS_TKDL,M2.MIT_BEZEICHNUNG AS MIT_BEZEICHNUNG_TKDL, M2.MIT_KEY AS MIT_KEY_TKDL ';
	$SQL .= ' FROM Verkaufsgebiete LEFT OUTER JOIN Mitarbeiter ON VKG_MIT_KEY = Mitarbeiter.MIT_KEY ';
	$SQL .= ' LEFT OUTER JOIN Mitarbeiter M2 ON VKG_TKDL_MIT_KEY = M2.MIT_KEY ';


	if(isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ' . $_GET['Sort'];
	}
	else
	{
		$SQL .= ' ORDER BY Mitarbeiter.MIT_BEZEICHNUNG';
	}
	$rsVKG = awisOpenRecordset($con, $SQL);
	$rsVKGZeilen = $awisRSZeilen;

	for($VKGZeile=0;$VKGZeile<$rsVKGZeilen;$VKGZeile++)
	{
		echo '<tr>';

		if(($RechteStufe&2)==2)
		{
			echo '<td><a href=./perseins_Main.php?cmdAktion=Gebiete&VKG_ID=' . $rsVKG['VKG_ID'][$VKGZeile] . '><img src=/bilder/aendern.png border=0></a></td>';
		}

		echo '<td>' . $rsVKG['VKG_ID'][$VKGZeile] . '</td>';
		if($rsVKG['MIT_STATUS'][$VKGZeile]=='S')
		{
			echo "<td><span class=Hinweistext title='Mitarbeiter ist stillgelegt!'>" . $rsVKG['MIT_BEZEICHNUNG'][$VKGZeile] . '</span></td>';
		}
		else
		{
			echo '<td>' . $rsVKG['MIT_BEZEICHNUNG'][$VKGZeile] . '</td>';
		}

		if($rsVKG['MIT_STATUS_TKDL'][$VKGZeile]=='S')
		{
			echo "<td><span class=Hinweistext title='Mitarbeiter ist stillgelegt!'>" . $rsVKG['MIT_BEZEICHNUNG_TKDL'][$VKGZeile] . '</span></td>';
		}
		else
		{
			echo '<td>' . $rsVKG['MIT_BEZEICHNUNG_TKDL'][$VKGZeile] . '</td>';
		}
		echo '<td>' . $rsVKG['VKG_BEMERKUNG'][$VKGZeile] . '</td>';

		echo '</tr>';

	}

	echo '</table>';
}

echo '</form>';


/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
