<?php
require_once('awisDokument.inc');

global $Param;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');
$TextKonserven[] = array('Fehler', 'err_UntergeordneterDatensatz');
$TextKonserven[] = array('HRK', '%');
try {
    $Key = '';
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

    $Tabelle = '';
    
    if (!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x'])) {
        $Tabelle = 'HRK';
        $Key = $AWIS_KEY1;
        $SQL = 'select hrk_key, hrk_vorname, hrk_nachname, hrk_ort from HRKOPF ';
        $SQL .= ' WHERE hrk_key' . $DB->LikeOderIst($AWIS_KEY1);

        $rsHRK = $DB->RecordSetOeffnen($SQL);

        $Felder = array();
        $Felder[] = array($TXT_AdrLoeschen['HRK']['HRK_VORNAME'], $rsHRK->FeldInhalt('HRK_VORNAME'));
        $Felder[] = array($TXT_AdrLoeschen['HRK']['HRK_NACHNAME'], $rsHRK->FeldInhalt('HRK_NACHNAME'));
        $Felder[] = array($TXT_AdrLoeschen['HRK']['HRK_ORT'], $rsHRK->FeldInhalt('HRK_ORT'));
    } elseif (isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']) and !isset($_POST['cmdLoeschenOK'])) {
        if (isset($_GET['Dokumente'])) {
            $Tabelle = 'DOC';
            $Key = $_GET['Del'];

            $TextKonserven = array();
            $TextKonserven[] = array($Tabelle, 'DOC_BEZEICHNUNG');
            $TextKonserven[] = array($Tabelle, 'DOC_DATUM');
            $LoeschFelder = $Form->LadeTexte($TextKonserven);

            $SQL = 'SELECT DOC_BEZEICHNUNG, DOC_DATUM, DOZ_XXX_KEY AS CAD_KEY ';
            $SQL .= ' FROM Dokumente';
            $SQL .= ' INNER JOIN DOKUMENTZUORDNUNGEN ON DOZ_DOC_KEY = DOC_KEY AND DOZ_XXX_KUERZEL = \'BEW\'';
            $SQL .= ' WHERE DOC_KEY=' . $DB->WertSetzen('DATEN', 'N0', $Key);

            $rsDaten = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('DATEN'));

            $Felder = array();
            $Felder[] = array($LoeschFelder['DOC']['DOC_DATUM'], $rsDaten->FeldInhalt('DOC_DATUM'));
            $Felder[] = array($LoeschFelder['DOC']['DOC_BEZEICHNUNG'], $rsDaten->FeldInhalt('DOC_BEZEICHNUNG'));
        }
    } elseif (isset($_POST['cmdLoeschenOK']) and isset($_GET['Del']) and isset($_GET['Dokumente']))    // Loeschen durchführen
    {
        //awis_Debug(1,$_POST);
        $SQL = '';
        if (isset($_GET['Dokumente'])) {
            $SQL = '';
            $DOC = new awisDokument($DB, $AWISBenutzer);
            $DOC->DokumentLoeschen($_GET['Del']);
        }
    } elseif (isset($_POST['cmdLoeschenOK']) and isset($_POST['txtTabelle']) and $_POST['txtTabelle'] == 'HRK') {
        $SQL = 'Delete from HRKOPF where hrk_key' . $DB->LikeOderIst($AWIS_KEY1);
        $Form->DebugAusgabe(1, $SQL);
        $DB->Ausfuehren($SQL);
        $AWIS_KEY1 = '';
        $AWIS_KEY2 = '';
    }
    if ($Tabelle != '') {
        echo '<form name=frmLoeschen action=./bewerberverwaltung_Main.php?cmdAktion=' . $_GET['cmdAktion'] . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '&HRK_KEY=' . $AWIS_KEY1 . '&BWH_KEY=' . $AWIS_KEY2 . (isset($_GET['Dokumente'])?'&Dokumente=1':'') . (isset($_GET['Del'])?'&Del=' . $_GET['Del']:'') . '#Dokumente' . ' method=post>';
        $Form->ZeileStart();
        echo '<span class=HinweisText>' . $TXT_AdrLoeschen['Wort']['WirklichLoeschen'] . '</span>';
        $Form->ZeileEnde();

        foreach ($Felder AS $Feld) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Feld[0] . ':', 200);
            $Form->Erstelle_TextLabel($Feld[1], 500, 'text-align:left;font-weight:bold');
            $Form->ZeileEnde();
        }

        $Form->Erstelle_HiddenFeld('Tabelle', $Tabelle);
        $Form->Erstelle_HiddenFeld('Key', $Key);

        //echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
        //echo '<input type=hidden name=txtKey value="'.$Key.'">';

        $Form->Formular_Ende();
        $Form->SchaltflaechenStart();

        //echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
        //echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';
        $Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', 'cmd_ja', $TXT_AdrLoeschen['Wort']['Ja']);
        $Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', 'cmd_nein', $TXT_AdrLoeschen['Wort']['Nein']);
        $Form->SchaltflaechenEnde();

        echo '</form>';
        die();
    }
} catch (awisException $ex) {
    $Form->DebugAusgabe(1, $ex->getMessage(), 'Zeile:' . $ex->getLine(), $ex->getSQL());
} catch (Exception $ex) {
    $Form->DebugAusgabe(1, $ex->getMessage(), 'Zeile:' . $ex->getLine());
}

?>