<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
$Form->DebugAusgabe(1,$_POST);


	if(isset($_POST['txtXFT_KEY']))
	{
		$AWIS_KEY1=$_POST['txtXFT_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtXFT_',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('XFT','XFT_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('XFT_BEZEICHNUNG','XFT_TAG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['XFT'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if($_POST['txtXFT_FILAUSNAHME']!='')
			{
				$Filialen=explode(',',$_POST['txtXFT_FILAUSNAHME']);
				$Daten = array();
				foreach($Filialen AS $Filiale)
				{
					if(intval(trim($Filiale))>0 AND intval(trim($Filiale))<=9999)
					{
						$Daten[] = $Filiale;
					}
				}
				
				$_POST['txtXFT_FILAUSNAHME']=implode(',',$Daten);
			}
			
			
			if(floatval($_POST['txtXFT_KEY'])==0)
			{
				$SQL = 'INSERT INTO FEIERTAGE(';
				$SQL .= 'XFT_TAG,XFT_LAN_CODE,XFT_BUL_ID,XFT_FIL_ID,XFT_BEZEICHNUNG';
//				$SQL .= ',XFT_FILAUSNAHME';
				$SQL .= ',XFT_USER,XFT_USERDAT)';
  				$SQL .= 'VALUES (';
				$SQL .= ' '.$DB->FeldInhaltFormat('D',$_POST['txtXFT_TAG'],false);
				$SQL .= ','.$DB->FeldInhaltFormat('T',$_POST['txtXFT_LAN_CODE'],true);
				$SQL .= ','.$DB->FeldInhaltFormat('N0',$_POST['txtXFT_BUL_ID'],true);
				$SQL .= ','.$DB->FeldInhaltFormat('N0',$_POST['txtXFT_FIL_ID'],true);
				$SQL .= ','.$DB->FeldInhaltFormat('T',$_POST['txtXFT_BEZEICHNUNG'],false);
//				$SQL .= ','.$DB->FeldInhaltFormat('T',$_POST['txtXFT_FILAUSNAHME'],true);
				$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
				$SQL .= ', SYSDATE)';
				
				$DB->Ausfuehren($SQL);
			}
			else 					// gešnderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM feiertage WHERE XFT_Key=' . $_POST['txtXFT_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('XFT_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE feiertage SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', XFT_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', XFT_userdat=sysdate';
					$SQL .= ' WHERE XFT_key=0' . $_POST['txtXFT_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>