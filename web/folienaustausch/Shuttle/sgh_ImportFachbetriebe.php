<?php
/**
 * ***********************************************************************
 * Script zum Importieren von den Trim-Line Fachbetrieben (werden bei
 * den Filialinfos angezeigt). Tabellen werden beim Import kurz geloescht. 
 * ***********************************************************************
 * Erstellt:
 * @author 16.11.2011 Stefan Oppl
 * @version 1.0
 * �nderungen:
 * 14.03.2012 HO : Anpassungen wegen Anforderung A00019
 * ***********************************************************************
 */

require_once 'awisDatenbank.inc';
require_once 'fileCSV.inc.php';

try{
	$SQL = '';
	$csvDatenSatz = array();
	$rsDupli = '';
	$dupliFil = '';
	
	$objDB = awisDatenbank::NeueVerbindung('AWIS');
	$objDB->Oeffnen();
	$objFileCSV = new fileCSV();
	
	/*Werte festlegen*/
	$TempTabelle = 'XXX_A00019';
	$objFileCSV->setDateiPfad('/daten/daten/sgh/Fachbetriebe/');
	$objFileCSV->setDateiName('FachbetriebeTrimLine.csv');
	$objFileCSV->setCSVSeperator(';');
	$gueltigAb = strftime('%d.%m.%Y'); /*ab wann soll die Zuordnung gelten->akt.Datum*/ 
	/*bis wann soll die Zuord. gelten->akt.Dat. + 10 Jahre */
	$gueltigBis = strftime("%d.%m.%Y",mktime(0,0,0,date("m"),date("d"),date("Y")+10));
	
	/*Temporaere Importtabelle bereinigen (Daten bleiben nach Import drin stehen*/
	$SQL = 'delete from ' . $TempTabelle;
	$objDB->Ausfuehren($SQL);
	
	/* Importdatei oeffnen (nur lesend) und Daten in eine temporaere Tabelle importiern */
	$objFileCSV->oeffneDatei('r');
	
	$csvDatenSatz = $objFileCSV->leseCSVDatensatz();
	
	while (!$objFileCSV->dateiEOF()){
		$SQL  = 'insert into ' .$TempTabelle;
		$SQL .= ' (xxx_fil_id,xxx_betrieb,xxx_strasse,xxx_plz,xxx_ort,xxx_telefon,';
		$SQL .= ' xxx_telefax,xxx_mobilnummer,xxx_email) ';
		$SQL .= ' values';
		$SQL .= " (".$csvDatenSatz[0].",'".trim($csvDatenSatz[2])."','".$csvDatenSatz[3]."',";
		$SQL .= "'".$csvDatenSatz[4]."','".$csvDatenSatz[5]."','".$csvDatenSatz[6]."',";
		$SQL .= "'".$csvDatenSatz[7]."','".$csvDatenSatz[8]."','".$csvDatenSatz[9]."')";

		$objDB->Ausfuehren($SQL);		
		$csvDatenSatz = $objFileCSV->leseCSVDatensatz();
	}
	
	$objFileCSV->schliesseDatei();
	
	/*Daten pruefen (Felder FIL_ID & Betriebe) auf Duplikate*/
	$SQL = 'select anz, xxx_fil_id';
	$SQL .= ' from';
	$SQL .= ' (select xxx_fil_id,xxx_betrieb,count(*) as anz';
	$SQL .= ' from ' .$TempTabelle ;
	$SQL .= ' group by xxx_fil_id,xxx_betrieb)';
	$SQL .= ' where anz > 1';
	
	if($objDB->ErmittleZeilenAnzahl($SQL) > 0){
		/*Duplikate gefunden --> Exeption ausloesen und damit weitere Ausfuehrung beenden
		 * au�erdem die doppelten Filialen bei Execption-Message mit ausgeben*/
		$rsDupli = $objDB->RecordSetOeffnen($SQL);
		while(!$rsDupli->EOF()){
			$dupliFil .= $rsDupli->FeldInhalt('XXX_FIL_ID').'/';
			$rsDupli->DSWeiter();
		}
		throw new Exception('sgh_ImportFachbetriebe__Duplikate_gefunden_Fil:'.$dupliFil, '201111161042');
	}
	else{
		/*da Fachbetriebefilialen und Fachbetriebe-Tabelle voneinander abhaengig sind
		 * das loeschen und wiederbefuellen beider Tabellen in einer Transaktion */
		$objDB->TransaktionBegin();		
		/*Fachbetriebe->Filial Zuordnung leeren*/

		$SQL  = 'delete from fachbetriebefilialen';
		$SQL .= ' where ffa_fab_key in ';
		$SQL .= ' (select fab_key';
		$SQL .= ' from fachbetriebe';
		$SQL .= " where fab_bereich = 'trim-line')";
		
		$objDB->Ausfuehren($SQL);
		
		/*Fachbetriebe (TrimLine) leeren*/
		$SQL  = 'delete from fachbetriebe';
		$SQL .= " where fab_bereich = 'trim-line'";
		
		$objDB->Ausfuehren($SQL);
		
				
		/*beide Tabellen neu befuellen*/
		$SQL = 'insert into fachbetriebe ';
		$SQL .= '(FAB_NAME1,FAB_STRASSE, FAB_PLZ, FAB_ORT, FAB_TELEFON, FAB_TELEFAX,'; 
		$SQL .= 'FAB_EMAIL, FAB_BEREICH, FAB_USER, FAB_USERDAT) ';
		$SQL .= 'select distinct xxx_betrieb,xxx_strasse,xxx_plz,xxx_ort,xxx_telefon,';
		$SQL .= "xxx_telefax,xxx_email,'trim-line','AWIS',sysdate ";
		$SQL .= 'from ' .$TempTabelle;
		$SQL .= ' order by 1';
		
		$objDB->Ausfuehren($SQL);
		
		$SQL = 'insert into fachbetriebefilialen ';
		$SQL .= '(FFA_FAB_KEY, FFA_FIL_ID, FFA_GUELTIGAB, FFA_GUELTIGBIS, FFA_USER, FFA_USERDAT) ';
		$SQL .= "select fab_key, x.xxx_fil_id, '".$gueltigAb."', '".$gueltigBis."', 'AWIS', sysdate ";
		$SQL .= 'from ' .$TempTabelle .' x inner join fachbetriebe ';
		$SQL .= 'on trim(x.xxx_betrieb) = trim(fab_name1) ';
		$SQL .= 'order by 2';
		
		$objDB->Ausfuehren($SQL);
		
		$objDB->TransaktionCommit();
	}

}
catch (awisException $e){
	/*awisException bei Fehlern mit SQL-Statements*/
	echo $e->getMessage().'/'.$e->getSQL();
	$objDB->TransaktionRollback();	
}
catch (Exception $e){
	/*sollte eine Transaktion noch offen sein (beim befuellen der AWIS-Tabellen)
	 * dann diese zurueckrollen*/
	echo $e->getCode().$e->getMessage();
	$objDB->TransaktionRollback();	
}
?>