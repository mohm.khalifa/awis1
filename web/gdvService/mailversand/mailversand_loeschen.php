<?php
/**
 * Löschen Stammdatenpflege GDV-Mailversand
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607081616
 * @todo
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

$Tabelle= '';


if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedrückt
{
		
		$Tabelle = 'VMV';
		$Felder = explode(';',$FelderDel);
		$Feld = explode('_',$Felder[1]);
		$Key=$Feld[2];
	
		$SQL =' select * from VERSMAILVERSAND ';
	    $SQL .= ' where VMV_KEY =' .$Key;

		$rsDaten = $DB->RecordSetOeffnen($SQL);
	
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$_POST['txtAWIS_KEY1']);
		$Felder[]=array($Form->LadeTextBaustein('GDV','VMV_BEDINGUNG'),$rsDaten->FeldInhalt('VMV_BEDINGUNG'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','VMV_MAILADRESSE'),$rsDaten->FeldInhalt('VMV_MAILADRESSE'));


}
if(isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'VNT2';
	$Key=$_POST['txtAWIS_KEY1'];

	$SQL =' select * from versversicherungen ';
	$SQL .= ' where VVE_VERSNR =' .$Key;

	$rsDaten = $DB->RecordSetOeffnen($SQL);

	/**
	 * Werte f�r Frage 'Wirklich l�schen?' holen
	 **/
	$Felder=array();
	$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen wenn auf Frage 'Wirklich löschen?' ok
{
	switch ($_POST['txtTabelle']) {
		case 'VMV':

			$SQL = 'DELETE FROM VERSMAILVERSAND WHERE VMV_KEY=' . $_POST['txtKEY']; // Sachbearbeiter Zuord l�schen

			if ($DB->Ausfuehren($SQL) === false) {
				throw new awisException('Fehler beim Loeschen', 201202181536, $SQL, awisException::AWIS_ERR_SYSTEM);
			}
			$AWIS_KEY1 = $_POST['txtVERSNR'];
			break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':

			$Form->SchreibeHTMLCode('<form name=frmLoeschen action="./mailversand_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsDaten->FeldInhalt('VMV_VERSNR').  '" method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext('M&oumlchten sie die Daten f&uumlr folgende Versicherung wirklich l&oumlschen?');	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder für Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder für Anzeigen und zum löschen des Datensatzes per Hidden übergeben.
			 **/

			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('VERSNR',$rsDaten->FeldInhalt('VMV_VERSNR'));
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);

			$Form->Trennzeile();
				
			/**
			 * Löschen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();

			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>