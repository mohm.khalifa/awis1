<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');
require_once('filialinfos_funktionen.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('Filialinfo','fit_lbl_915');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5013 = $AWISBenutzer->HatDasRecht(5013);
	if($Recht5013==0)
	{
		$DB->ProtokollEintrag(awisDatenbank::EREIGNIS_FEHLER, 1000, $AWISBenutzer->BenutzerName(),'AX-Filialen Register in Filialen',$AWISBenutzer->BenutzerName());
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}


	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./filialinfos_speichern.php');
		$AWIS_KEY1='';
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./filialinfos_loeschen.php');
	}

	if(!isset($_GET['Liste']) AND !isset($_REQUEST['Block']) AND !isset($_GET['Sort']))
	{
		$Params = array();
		$Params['ORDER'] = '';
		$Params['BLOCK'] = '';
		
		$AWISBenutzer->ParameterSchreiben("Formular_AX",serialize($Params));
	}	
	
	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		$Params = unserialize($AWISBenutzer->ParameterLesen('Formular_AX'));		
		
		if($Params['ORDER']!='')
		{
			$ORDERBY = $Params['ORDER'];
		}
		else
		{		
			$ORDERBY = ' ORDER BY CASE WHEN FIF_WERT_915 IS NULL OR FIF_WERT_915 IS NOT NULL AND FIF_WERT_914 IS NOT NULL THEN TO_DATE(\'31.12.2020\',\'DD.MM.RRRR\') ELSE FIF_WERT_915 END';
			$ORDERBY .= ', FIF_WERT_914 DESC, FIL_ID';
			//$ORDERBY = ' ORDER BY COALESCE(FIF_WERT_915,FIF_WERT_915), FIL_ID';
		}
	}
	else
	{
		$Sort = str_replace('EROEFFDAT','COALESCE(FIF_WERT_914,FIF_WERT_915)',$_GET['Sort']);
		
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$Sort);
		
		$Params = array();
		$Params['ORDER'] = $ORDERBY;
		
		$AWISBenutzer->ParameterSchreiben("Formular_AX",serialize($Params));	
	}

	$Param = array();

	if(isset($_GET['FIL_ID']))
	{
		$Param['FIL_ID']=$_GET['FIL_ID'];
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$Param['FIL_ID']=-1;
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT filialen.*, LAN_CODE, LAN_LAND';
	$SQL .= ', FIF_WERT_914';
	$SQL .= ', COALESCE(FIF_WERT_914,FIF_WERT_915) AS EROEFFDAT';
	$SQL .=' , Daten.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM filialen';
	$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' LEFT OUTER JOIN (SELECT FIF_FIL_ID AS FIF_FIL_ID_915, FIF_KEY AS FIF_KEY_915, FIF_USER AS FIF_USER_915, FIF_USERDAT AS FIF_USERDAT_915, to_date(FIF_WERT) AS FIF_WERT_915 FROM filialinfos WHERE FIF_FIT_ID = 915';
	$SQL .= '                 ) DATEN ON  fif_fil_id_915 = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN (SELECT to_date(FIF_WERT) AS FIF_WERT_914, FIF_FIL_ID AS FIF_FIL_ID_914 FROM filialinfos WHERE FIF_FIT_ID = 914) ISTDATEN ON FIF_FIL_ID_914 = FIL_ID';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Params['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben("Formular_AX",serialize($Params));
		}
		elseif(isset($Params['Block']))
		{
			$Block=$Params['BLOCK'];
		}
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

//$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1,$SQL);

	$rsFIL = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmFilialinfos action=./filialinfos_Main.php?cmdAktion=AX-Filialen'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

	if($rsFIL->EOF() AND !isset($Param['FIL_ID']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsFIL->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht5013)&4)!=0)
		{
			$Icons[] = array('new','./filialinfos_Main.php?cmdAktion=AX-Filialen&FIL_ID=-1');
			$Form->Erstelle_ListeIcons($Icons,36,-1);
  		}

		$Link = './filialinfos_Main.php?cmdAktion=AX-Filialen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ID'],100,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=AX-Filialen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FIL_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_BEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],300,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=AX-Filialen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FIF_WERT_914'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIF_WERT_914'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['AXISTDATUM'],140,'',$Link);
		$Link = './filialinfos_Main.php?cmdAktion=AX-Filialen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FIF_WERT_915'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIF_WERT_915'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['AXSOLLDATUM'],140,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsFIL->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht5013&2)>0 AND $rsFIL->FeldInhalt('FIF_WERT_914')=='')	// �ndernrecht
			{
				$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=AX-Filialen&FIL_ID='.$rsFIL->FeldInhalt('FIL_ID'));
			}
			if(intval($Recht5013&8)>0 AND $rsFIL->FeldInhalt('FIF_WERT_914')=='')	// L�schen
			{
				$Icons[] = array('delete','./filialinfos_Main.php?cmdAktion=AX-Filialen&Del='.$rsFIL->FeldInhalt('FIF_KEY_915'));
			}

			if(intval($Recht5013&8)>0 or intval($Recht5013&2)>0)
			{
				$Form->Erstelle_ListeIcons($Icons,36,($DS%2));
			}

			$Link = './filialinfos_Main.php?cmdAktion=Details&FIL_ID=0'.$rsFIL->FeldInhalt('FIL_ID').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('FIL_ID',$rsFIL->FeldInhalt('FIL_ID'),0,100,false,($DS%2),'',$Link);
			$Link='';
			$Form->Erstelle_ListenFeld('FIL_BEZ',$rsFIL->FeldInhalt('FIL_BEZ'),0,300,false,($DS%2),'',$Link,'T','L',$rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Wert = ($rsFIL->FeldInhalt('FIF_WERT_914')==''?$rsFIL->FeldInhalt('FIF_WERT_915'):$rsFIL->FeldInhalt('FIF_WERT_914'));
			$Form->Erstelle_ListenFeld('FIF_WERT_914',$rsFIL->FeldInhalt('FIF_WERT_914', 'Dk'),0,140,false,($DS%2),'text-style:italic;',$Link,'D','L');
			$Form->Erstelle_ListenFeld('FIF_WERT_915',($rsFIL->FeldInhalt('FIF_WERT_914')==''?$rsFIL->FeldInhalt('FIF_WERT_915'):''),0,140,false,($DS%2),'text-style:italic;',$Link,'D','L');
			$Form->ZeileEnde();

			$rsFIL->DSWeiter();
			$DS++;
		}

		$Link = './filialinfos_Main.php?cmdAktion=AX-Filialen';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsFIL->FeldInhalt('FIL_ID');

		$Param['KEY']=$AWIS_KEY1;

		echo '<input type=hidden name=txtFIL_ID value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];


			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialinfos_Main.php?cmdAktion=AX-Filialen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFIL->FeldInhalt('FIF_USER_915'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFIL->FeldInhalt('FIF_USERDAT_915'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht5013&2)!=0);

		// �berschrift
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',150);
		$SQL = 'SELECT FIL_ID, FIL_ID || \' - \' || FIL_BEZ AS ANZEIGE FROM FILIALEN';
		$SQL .= ' WHERE NOT EXISTS(SELECT * FROM Filialinfos WHERE fif_fil_id = fil_id AND fif_fit_id in (914,915) AND FIF_WERT IS NOT NULL)';
		$SQL .= ' OR FIL_ID = 0'.$rsFIL->FeldInhalt('FIL_ID');
		$SQL .= ' ORDER BY 1';
		$Form->Erstelle_SelectFeld('FIL_ID',$rsFIL->FeldInhalt('FIL_ID'),200,($EditRecht AND $rsFIL->FeldInhalt('FIL_ID')==''),$SQL);
		$AWISCursorPosition = 'txtFIL_ID';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Filialinfo']['fit_lbl_915'].':',150);
		$Form->Erstelle_TextFeld('FIT_WERT_915_'.$rsFIL->FeldInhalt('FIF_KEY_915').'_D',$rsFIL->FeldInhalt('FIF_WERT_915'),10,150,$EditRecht,'','','','D');
		$Form->ZeileEnde();
	}

	//awis_Debug(1, $Param, $Bedingung, $rsFIL, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht5013&6)!=0 ) AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!=='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND FIL_ID = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['FIL_ID']) AND $Param['FIL_ID']!='')
	{
		$Bedingung .= ' AND (FIL_ID =' .$DB->FeldInhaltFormat('N0',$Param['FIL_ID']).')';
	}
	else
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM Filialinfos WHERE fif_fil_id = fil_id AND fif_fit_id in (914,915) AND FIF_WERT IS NOT NULL)';
	}

	return $Bedingung;
}
?>