<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','wrd_Filiale');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Wort','txt_NaechsteFreieNummer');
$TextKonserven[]=array('ADS','%');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	//$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_453_2'));

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4301 = $AWISBenutzer->HatDasRecht(4301);		// Recht f�r Stammdaten
	if($Recht4301==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Bedingung='';
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./adr_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./adr_speichern.php');
	}
	elseif(isset($_GET['ADS_KEY']))
	{
		$Bedingung .= ' AND ADS_KEY='.$DB->FeldInhaltFormat('Z',$_GET['ADS_KEY']).'';
	}
	elseif(isset($_POST['txtADS_KEY']))
	{
		$Bedingung .= ' AND ADS_KEY='.$DB->FeldInhaltFormat('Z',$_POST['ADS_KEY']).'';
	}
	
	if($AWIS_KEY2>0)
	{
		$Bedingung .= ' AND ADS_KEY='.$DB->FeldInhaltFormat('Z',$AWIS_KEY2).'';
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	
	$SQL = 'SELECT ADRST.*';		
	$SQL .= ' FROM ADRSTAMM ADRST ';		
	$SQL .= ' WHERE ADS_ADRART = 1 ';		
	
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}
	
	if(!isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ADS_ADRNR';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	$rsADS = $DB->RecordsetOeffnen($SQL);
	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmadrstamm action=./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm method=POST>';

	$Form->Formular_Start();
	
	$EditRecht=false;
	
	if($rsADS->EOF() AND  !isset($_GET['ADS_KEY']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsADS->AnzahlDatensaetze()>1)						// Liste anzeigen
	{		
		$Form->ZeileStart();
		if(($Recht4301&4))
		{
			$Icons[] = array('new','./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm&ADS_KEY=0','g',$AWISSprachKonserven['Wort']['lbl_hinzufuegen']);
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADS_ADRNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_ADRNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_ADRNR'],150,'',$Link);	
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADS_EWCCODE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_EWCCODE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCCODE'],150,'',$Link);	
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADS_EWCBEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_EWCBEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCBEZ'],500,'',$Link);	
		$Form->ZeileEnde();
		
		$DS=0;
		while(!$rsADS->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(($Recht4301&6)>0)	// �ndernrecht
			{
				$Icons[] = array('edit','./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm&ADS_KEY='.$rsADS->FeldInhalt('ADS_KEY'));
				$Icons[] = array('delete','./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm&Del='.$rsADS->FeldInhalt('ADS_KEY'));
			}			
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$Form->Erstelle_ListenFeld('ADS_ADRNR',$rsADS->Feldinhalt('ADS_ADRNR'),0,150,false,($DS%2),'','','','','');											
			$Form->Erstelle_ListenFeld('ADS_EWCCODE',$rsADS->Feldinhalt('ADS_EWCCODE'),0,150,false,($DS%2),'','','','','');											
			$Form->Erstelle_ListenFeld('ADS_EWCBEZ',$rsADS->Feldinhalt('ADS_EWCBEZ'),0,500,false,($DS%2),'','','','','');											
			$Form->ZeileEnde();
			
			$DS++;
			$rsADS->DSWeiter();					
		}
	}
	else 
	{
		echo '<form name=frmADRAbfallstamm action=./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm method=POST  enctype="multipart/form-data">';
		
		$AWIS_KEY1 = $rsADS->FeldInhalt('ADS_KEY');		
		echo '<input type=hidden name=txtADS_KEY value='.$AWIS_KEY1. '>';
		$Form->Erstelle_HiddenFeld('ADS_ADRART',1);
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADS->FeldInhalt('ADS_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADS->FeldInhalt('ADS_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$EditRecht=(($Recht4301&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_ADRNR'].':',200);
		$Form->Erstelle_TextFeld('ADS_ADRNR',$rsADS->FeldInhalt('ADS_ADRNR'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_EWCCODE'].':',200);
		$Form->Erstelle_TextFeld('ADS_EWCCODE',$rsADS->FeldInhalt('ADS_EWCCODE'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_EWCBEZ'].':',200);
		$Form->Erstelle_TextFeld('ADS_EWCBEZ',$rsADS->FeldInhalt('ADS_EWCBEZ'),100,300,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_LAGACODE'].':',200);
		$Form->Erstelle_TextFeld('ADS_LAGACODE',$rsADS->FeldInhalt('ADS_LAGACODE'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_LAGABEZ'].':',200);
		$Form->Erstelle_TextFeld('ADS_LAGABEZ',$rsADS->FeldInhalt('ADS_LAGABEZ'),100,300,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_BEHTYP'].':',200);
		$Form->Erstelle_TextFeld('ADS_BEHTYP',$rsADS->FeldInhalt('ADS_BEHTYP'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_BEHGEW'].':',200);
		$Form->Erstelle_TextFeld('ADS_BEHGEW',$rsADS->FeldInhalt('ADS_BEHGEW'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_ZULADGEW'].':',200);
		$Form->Erstelle_TextFeld('ADS_ZULADGEW',$rsADS->FeldInhalt('ADS_ZULADGEW'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_FAKTOR'].':',200);
		$Form->Erstelle_TextFeld('ADS_FAKTOR',$rsADS->FeldInhalt('ADS_FAKTOR'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADS']['ADS_EINHEIT'].':',200);
		$Form->Erstelle_TextFeld('ADS_EINHEIT',$rsADS->FeldInhalt('ADS_EINHEIT'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();				
		
		//$Form->ZeileStart();
		//$Form->Erstelle_TextLabel('Land :',200);
		//$Form->Erstelle_TextFeld('ADS_ZULADGEW',$rsADS->FeldInhalt('ADS_ZULADGEW'),20,50,$EditRecht,'','','','T');		
		//$Form->ZeileEnde();

	}

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if ($EditRecht==true)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=werkzeuge&Seite=".(isset($_GET['Seite'])?$_GET['Seite']:'Geloeschte')."','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200810060006");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"20081��61223");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>