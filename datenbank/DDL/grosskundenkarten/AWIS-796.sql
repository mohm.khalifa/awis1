
UPDATE Berichte
set xre_bezeichnung = 'Grosskundenkarten ALT',
    xre_klassenname = 'ber_Grosskundenkarten_alt',
    xre_bemerkung = 'Alte Grosskundenkarte - Nicht mehr in Benutzung',
    xre_status = 'I'
where XRE_KEY = 71;
commit;

INSERT INTO berichte (
    xre_key,
    xre_bezeichnung,
    xre_klassenname,
    xre_format,
    xre_ausrichtung,
    xre_vorlage,
    xre_xrc_id,
    xre_stufe,
    xre_protokolleintrag,
    xre_status,
    xre_bemerkung,
    xre_user,
    xre_userdat
) VALUES (
    78,
    'Grosskundenkarten_BAR_UNBAR_UNIVERSAL',
    'ber_Grosskundenkarten_BAR_UNBAR',
    'A4',
    'L',
    'Grosskunden.pdf',
    59000,
    1,
    1,
    'A',
    'Grosskundenkarte',
    'Andre Meyer',
    sysdate
);

commit;

update textkonserven
set xtx_kennung =  replace(XTX_KENNUNG,'ERM_PDF_','ERM_PDF_ALT_')
where xtx_kennung like 'ERM_PDF_%';

commit;

SET DEFINE OFF;
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_UEBERSCHRIFT','ERM','','Gro�kunden-Ausweis');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_KD_NR','ERM','','Kd-Nr.:');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_GUELTIG_BIS','ERM','','G�ltig bis:');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_RABATT_1','ERM','','Ihr Rabatt auf das gesamte Produktportfolio');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_RABATT_2','ERM','','und alle Dienstleistungen');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_KARTEN_NR','ERM','','Kartennummer');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_RABATT_EAN','ERM','','9999990035738');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_LOGO_PFAD','ERM','','/daten/web/bilder/atu_logo_#LAND#_150p.png');
commit;


INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandBarcodeBAR1','4' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandBarcodeBAR2','44' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandBarcodeEAN','0.25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandBarcodeUNBAR','25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandDisclaimerLogo','3' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandGueltigBis','73' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandKartennummerBAR','4' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandKartennummerUNBAR','27.5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AbstandKundennummer','48' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AnschriftH','2' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'AnschriftW','25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'BarcodeBARH','11.25' ,'Andre Meyer',sysdate);

commit;

INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'BarcodeBARW','25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'BarcodeKarteH','7.5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'BarcodeKarteW','0.28' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'DisclaimerH','2' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'DisclaimerW','55' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'DisclaimerY','28' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EAN_BereichH','12.5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EAN_BereichW','80' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EAN_BereichY','10' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EAN_NrH','2' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EAN_NrW','25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'EinrueckungKunde','1' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteGueltigBis','1' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteKartennummer','20' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteKdNr','1' ,'Andre Meyer',sysdate);

commit;

INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteName','49' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteRabatt','25' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'FeldBreiteUeberschrift','100' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'LinkerRand','3' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'LogoH','10' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'LogoW','22' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'RabattTextH','3' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'RabattTextW','50' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'RabattTextX','31' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'Schriftart','Arial' ,'Andre Meyer',sysdate);

commit;

INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'SeiteOben','6' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'SeiteObenRueckseite','4' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'TextGroesseDaten','8' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'TextGroesseDisclaimer','5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'TextGroesseInfos','6.5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'TextGroesseUeberschrift','16' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'ZeilenAbstandBarcodes','9.5' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'ZeilenAbstandDaten','4' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'ZeilenAbstandKartennummer','3' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'ZeilenAbstandKundennummer','50' ,'Andre Meyer',sysdate);
INSERT INTO berichteparameter (xrp_xre_key,xrp_parameter,xrp_wert,xrp_user,xrp_userdat) VALUES (78, 'ZeilenAbstandUeberschrift','6' ,'Andre Meyer',sysdate);

commit;

INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_DISCLAIMER','ERM','','Diese Karte ist Eigentum von A.T.U Auto-Teile-Unger GmbH & Co. KG und darf nur von den auf der Vorderseite genannten berechtigten Personen verwendet werden. Der Verlust der Karte ist sofort zu melden. Gefundene Karten bitte an untenstehende Adresse senden.

Dieses Angebot gilt nur f�r Teile, die A.T.U im Sortiment f�hrt. Ausgenommen sind Leistungen externer Partner (z.B. HU), alle von der Versicherung bezahlten Leistungen, Gutscheine, Wertschecks, Alufelgen von Fremdherstellern (au�er Aluett und Europe), Pfand/Altteile, Mietwagen, Sonderbestellungen, Finanzierungsangebote, usw.');
commit;

INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) VALUES ('ERM_PDF_ANSCHRIFT','ERM','','A.T.U-CARD-Service
D-92633 Weiden
Tel.: +49 (0)961/306-5830
infocard@de.atu.eu
www.atu.de/flotte');
commit;



