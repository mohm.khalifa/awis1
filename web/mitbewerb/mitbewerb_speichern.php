<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('MBW','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','MBWDoppeltTelefon');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	$TXT_Speichern = $Form->LadeTexte($TextKonserven);

	$AWIS_KEY1 = $_POST['txtMBW_KEY'];

	//***********************************************************************************
	//** Mitbewerber
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtMBW_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY1 = $_POST['txtMBW_KEY'];

		if($DB->FeldInhaltFormat('T',$_POST['txtMBW_TELEFON'])!='')
		{
			$SQL = 'SELECT *';
			$SQL .= ' FROM Mitbewerber';
			$SQL .= ' WHERE MBW_TELEFON = '.$DB->FeldInhaltFormat('T',$_POST['txtMBW_TELEFON']);
			$SQL .= ' AND MBW_KEY <> 0'.$_POST['txtMBW_KEY'];
			$rsMBW = $DB->RecordSetOeffnen($SQL);
			if(!$rsMBW->EOF())
			{
				$Fehler = $TXT_Speichern['Meldung']['MBWDoppeltTelefon'];

				$Form->Formular_Start();

				$Form->ZeileStart();
				$Form->Hinweistext($Fehler);
				$Form->ZeileEnde();

				$EditRecht = false;

				$Form->ZeileStart();
				$Link = './mitbewerb_Main.php?cmdAktion=Details&MBW_KEY=0'.$rsMBW->FeldInhalt('MBW_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_NAME1'].':',150,'',$Link);
				$Form->Erstelle_TextFeld('MBW_NAME1',$rsMBW->FeldInhalt('MBW_NAME1'),50,500,$EditRecht,'','','','T');
				$Form->ZeileEnde();

				$Form->ZeileStart();

				$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_STRASSE'].'/'.$AWISSprachKonserven['MBW']['MBW_HAUSNUMMER'].':',150);
				$Form->Erstelle_TextFeld('MBW_STRASSE',$rsMBW->FeldInhalt('MBW_STRASSE'),40,0,$EditRecht,'','','','T');
				$Form->Erstelle_TextFeld('MBW_HAUSNUMMER',$rsMBW->FeldInhalt('MBW_HAUSNUMMER'),5,90,$EditRecht,'','','','T');

				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_LAN_CODE'].'/'.$AWISSprachKonserven['MBW']['MBW_PLZ'].'/'.$AWISSprachKonserven['MBW']['MBW_ORT'].':',150);
				$SQL = 'SELECT LAN_CODE, LAN_LAND';
				$SQL .= ' FROM Laender';
				$SQL .= ' ORDER BY LAN_LAND';
				$Form->Erstelle_SelectFeld('MBW_LAN_CODE',$rsMBW->FeldInhalt('MBW_LAN_CODE'),150,$EditRecht,$SQL,'',$AWISBenutzer->BenutzerSprache(),'','','-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
				$Form->Erstelle_TextFeld('MBW_PLZ',$rsMBW->FeldInhalt('MBW_PLZ'),20,60,$EditRecht,'','','','T');
				$Form->Erstelle_TextFeld('MBW_ORT',$rsMBW->FeldInhalt('MBW_ORT'),30,300,$EditRecht,'','','','T');
				$Form->ZeileEnde();

				$Form->Formular_Ende();
				die();
			}
		}


			// Daten auf Vollst�ndigkeit pr�fen
		$Fehler='';
		$Pflichtfelder = array('MBW_NAME1','MBW_STRASSE','MBW_HAUSNUMMER','MBW_PLZ','MBW_ORT','MBW_MBK_KEY','MBW_MUT_KEY');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['MBW'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}


		$rsLIN = $DB->RecordSetOeffnen('SELECT * FROM Mitbewerber WHERE MBW_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsLIN->EOF())
		{
			$SQL = 'INSERT INTO Mitbewerber';
			$SQL .= '(MBW_NAME1,MBW_STRASSE,MBW_HAUSNUMMER,MBW_LAN_CODE,';
			$SQL .= ' MBW_PLZ,MBW_ORT,MBW_MBK_KEY,MBW_MUT_KEY,MBW_TELEFON,';
			$SQL .= ' MBW_ONLINESHOP,MBW_BEMERKUNG,MBW_PRUEFUNGDURCH,MBW_PRUEFUNGAM,';
			$SQL .= ' MBW_USER,MBW_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_NAME1'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_STRASSE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_HAUSNUMMER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_LAN_CODE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_PLZ'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_ORT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtMBW_MBK_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtMBW_MUT_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_TELEFON'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_ONLINESHOP'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_BEMERKUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtMBW_PRUEFUNGDURCH'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtMBW_PRUEFUNGAM'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_MBW_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsMBW = $DB->RecordSetOeffnen('SELECT * FROM Mitbewerber WHERE MBW_key=' . $_POST['txtMBW_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsMBW->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsMBW->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsMBW->FeldInfo($FeldName,'TypKZ'),$rsMBW->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsMBW->FeldInhalt('MBW_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Mitbewerber SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', MBW_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', MBW_userdat=sysdate';
				$SQL .= ' WHERE MBW_key=0' . $_POST['txtMBW_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',808181117,$SQL,2);
				}
			}
		}
	}


//*******************************************************************************************
//* Filialzuordnungen
//*******************************************************************************************

	$Felder = $Form->NameInArray($_POST,'txtMFI',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY2=$_POST['txtMFI_KEY'];
		$rsMFI = $DB->RecordSetOeffnen('SELECT * FROM MitbewerberFilialen WHERE MFI_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtMFI_KEY'],false));

		if($rsMFI->EOF())		// Neue Zuordnung
		{
			$Fehler = '';
				// Daten auf Vollst�ndigkeit pr�fen
			$Pflichtfelder = array('MFI_FIL_ID');

			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['AST'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}

			$SQL = 'INSERT INTO MitbewerberFilialen';
			$SQL .= '(MFI_FIL_ID, MFI_MBW_KEY, MFI_ENTFERNUNG, MFI_SICHTBAR';
			$SQL .= ',MFI_USER, MFI_USERDAT)';
			$SQL .= 'VALUES (';
			$SQL .= '' . $DB->FeldInhaltFormat('N0',$_POST['txtMFI_FIL_ID'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtMFI_MBW_KEY'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N2',$_POST['txtMFI_ENTFERNUNG'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtMFI_SICHTBAR'],false);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('MFI speichern',909031332,$SQL,4);
			}
			$SQL = 'SELECT seq_MFI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->Feldinhalt('KEY');

		}
		else 					// ge�nderter Artikel
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{

					$WertNeu=$DB->FeldInhaltFormat($rsMFI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsMFI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsMFI->FeldInfo($FeldName,'TypKZ'),$rsMFI->FeldInhalt($FeldName),true);
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsMFI->FeldInhalt('MFI_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				die();
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE MitbewerberFilialen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', MFI_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', MFI_userdat=sysdate';
				$SQL .= ' WHERE MFI_KEY=0' . $_POST['txtMFI_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),909131431,$SQL,2);
				}
			}
			$AWIS_KEY2=$_POST['txtMFI_KEY'];
		}
	}


//*******************************************************************************************
//* Filialzuordnungen
//*******************************************************************************************

	$Felder = $Form->NameInArray($_POST,'txtMFR',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$Felder = $Form->NameInArray($_POST,'txtMFR_RANKING',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);

		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$SQL = '';

			if($FeldTeile[2]=='')		// Neu
			{
				if($DB->FeldInhaltFormat('N0',$_POST[$Feld],false)!='0' OR $DB->FeldInhaltFormat('T',$_POST[str_replace('RANKING','BEMERKUNG',$Feld)],true)!='null')
				{
					$SQL = 'INSERT INTO MitbewerberFilialenRanking';
					$SQL .= '(MFR_MFI_KEY, MFR_RANKING, MFR_BEMERKUNG, MFR_MRG_KEY, MFR_USER, MFR_USERDAT)';
					$SQL .= ' VALUES(';
					$SQL .= ' '.$AWIS_KEY2;
					$SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
					$SQL .= ', '.$DB->FeldInhaltFormat('T',$_POST[str_replace('RANKING','BEMERKUNG',$Feld)]);
					$SQL .= ', '.$DB->FeldInhaltFormat('N0',$FeldTeile[3]);
					$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', sysdate)';
				}
			}
			else		// �ndern
			{
				$rsMFR = $DB->RecordSetOeffnen('SELECT * FROM MitbewerberFilialenRanking WHERE MFR_KEY = 0'.$FeldTeile[2]);
				$Speichern = false;

				$FeldName = 'MFR_'.$FeldTeile[1];
				$WertNeu=$DB->FeldInhaltFormat($rsMFR->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsMFR->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr($Feld,3)],true);
				if($WertAlt!=$WertNeu)
				{
					$Speichern = true;
				}
				$FeldName = 'MFR_BEMERKUNG';
				$WertNeu=$DB->FeldInhaltFormat($rsMFR->FeldInfo($FeldName,'TypKZ'),$_POST[str_replace('RANKING','BEMERKUNG',$Feld)],true);
				$WertAlt=$DB->FeldInhaltFormat($rsMFR->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr(str_replace('RANKING','BEMERKUNG',$Feld),3)],true);
				if($WertAlt!=$WertNeu)
				{
					$Speichern = true;
				}

				if($Speichern)
				{
					$SQL = 'UPDATE MitbewerberFilialenRanking SET ';
					$SQL .= ' MFR_RANKING= '.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
					$SQL .= ', MFR_BEMERKUNG = '.$DB->FeldInhaltFormat('T',$_POST[str_replace('RANKING','BEMERKUNG',$Feld)]);
					$SQL .= ', MFR_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', MFR_USERDAT = sysdate';
					$SQL .= ' WHERE MFR_KEY = 0'.$FeldTeile[2];
				}
			}

			if($SQL!='')
			{
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),200901151754,$SQL,2);
				}
			}
		}
	}

//*******************************************************************************************
//* Hersteller => Unternehmenstypen
//*******************************************************************************************


	$Felder = $Form->NameInArray($_POST,'oldMIU_MUT_KEY',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$Form->DebugAusgabe(1,$_POST);
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$SQL = '';

			if($_POST[$Feld]=='' AND isset($_POST['txt'.substr($Feld,3)]))
			{
				$SQL = 'INSERT INTO MITBEWERBUTYPEN';
				$SQL .= '(MIU_MBW_KEY, MIU_MUT_KEY, MIU_USER, MIU_USERDAT)';
				$SQL .= ' VALUES(';
				$SQL .= ' '.$AWIS_KEY1;
				$SQL .= ', '.$FeldTeile[4];
				$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
				$SQL .= ', sysdate';
				$SQL .= ')';
$Form->DebugAusgabe(1,'NEU',$SQL);
			}
			elseif($_POST[$Feld]=='on' AND !isset($_POST['txt'.substr($Feld,3)]))
			{
				$SQL = 'DELETE FROM MITBEWERBUTYPEN';
				$SQL .= ' WHERE MIU_MBW_KEY = '.$AWIS_KEY1;
				$SQL .= ' AND MIU_KEY = '.$FeldTeile[3];
$Form->DebugAusgabe(1,'DEL',$SQL);
			}

			if($SQL!='')
			{
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),200901151754,$SQL,2);
				}
			}

		}




	}

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>