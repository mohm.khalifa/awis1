ALTER TABLE SEMINARTEILNEHMER ADD SET_TEILNEHMERVORSCHLAG VARCHAR2(100);

INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('SET_DROP_TEILNAHME','SET','Dropdown zur Teilnahme im Seminarplan','0~nein|1~ja|2~Teilnehmertausch','29.01.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('SET_TEILNEHMERTAUSCH','SET','','Teilnehmervorschlag (inkl. Personalnummer)','30.01.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('SET_ERR_TAUSCH_AUSWAHL','SET','','Bitte geben Sie eine Person ein.','30.01.2018');


SET DEFINE OFF;
INSERT INTO MAILVERSANDTEXTE (MVT_BEREICH, MVT_SPRACHE, MVT_BETREFF,MVT_TEXT,MVT_GUELTIGAB,MVT_USER,MVT_USERDAT) values ('SET_TEILNEHMER_VORSCHLAG','DE',
'[TEILNEHMERTAUSCH] #Vorname Nachname# f�r Seminar "#Seminartitel#" (#Terminnummer#) (KW#KW#)',
'<div style="font-family: Arial; font-size: 10pt">Sehr geehrte Damen und Herren, <br>
<br>
statt Herrn/Frau #Vorname Nachname# (#Personalnummer#, #Filialnummer#) nimmt nun "#TauschText#" am Seminar "#Seminartitel#" (#Terminnummer#) in/im <br>
<br>
#Lokationsname#<br>
#Schulungsort#<br>
#Strasse#<br>
#PLZ# #Ort#<br>
teil.<br>
<br>
Vielen Dank.
</div>','01.01.1970','ADMIN',SYSDATE);