<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('XBP','%');
$TextKonserven[]=array('XPP','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
$Recht6 = $AWISBenutzer->HatDasRecht(6);
if($Recht6==0)
{
    awisEreignis(3,1000,'Benutzerverwaltung',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/cmd_zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBP'));

//********************************************************
// Parameter ?
//********************************************************
$Param = unserialize($AWISBenutzer->ParameterLesen("Formular_XBP"));

if(isset($_POST['cmdSuche_x']))
{
	$Param['KEY']=0;			// Key
	$Param['XPP_BEZEICHNUNG']=$_POST['sucXPP_BEZEICHNUNG'];

	$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

	$Param['ORDERBY']='';
	$Param['BLOCK']='';
	$Param['KEY']=0;

	$AWISBenutzer->ParameterSchreiben("Formular_XBP",serialize($Param));
}
elseif(isset($_POST['cmdDSZurueck_x']))
{
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBP'));
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND XPP_BEZEICHNUNG < '.$DB->FeldInhaltFormat('T',$_POST['txtXPP_BEZEICHNUNG']);

	$SQL = 'SELECT * FROM (Select XPP_ID FROM Programmparameter ';
	$SQL .= ' WHERE '.substr($Bedingung,4);
	$SQL .= ' ORDER BY XPP_BEZEICHNUNG DESC';
	$SQL .= ') DATEN WHERE ROWNUM = 1';
	$rsXBP = $DB->RecordSetOeffnen($SQL);
	if(!$rsXBP->EOF())
	{
		$AWIS_KEY1=$rsXBP->FeldInhalt('XPP_ID');
	}
	else
	{
		$AWIS_KEY1 = $_POST['txtXPP_ID'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND XPP_BEZEICHNUNG > '.$DB->FeldInhaltFormat('T',(isset($_POST['txtXPP_BEZEICHNUNG'])?$_POST['txtXPP_BEZEICHNUNG']:''),true);

	$SQL = 'SELECT * FROM (Select XPP_ID FROM Programmparameter ';
	$SQL .= ' WHERE '.substr($Bedingung,4);
	$SQL .= ' ORDER BY XPP_BEZEICHNUNG ASC';
	$SQL .= ') Daten WHERE ROWNUM = 1';
	$rsXBP = $DB->RecordSetOeffnen($SQL);
	if(!$rsXBP->EOF())
	{
		$AWIS_KEY1=$rsXBP->FeldInhalt('XPP_ID');
	}
	else
	{
		$AWIS_KEY1 = (isset($_POST['txtXPP_ID'])?$_POST['txtXPP_ID']:0);
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./benutzerparameter_speichern.php');
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$AWIS_KEY1 = -1;
}
elseif(isset($_GET['XPP_ID']))
{
	$AWIS_KEY1 = $Form->Format('N0',$_GET['XPP_ID']);
}
else 		// Letzten Benutzer suchen
{
	if(!isset($_GET['Liste']))
	{
		$AWIS_KEY1 = $Param['KEY'];
	}
	if($Param=='' OR $Param=='0')
	{
		$Param['KEY']=0;
	}
}

//********************************************************
// Bedingung erstellen
//********************************************************
$Bedingung = _BedingungErstellen($Param);

//*****************************************************************
// Sortierung aufbauen
//*****************************************************************

if(!isset($_GET['Sort']))
{
	if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
	{
		$ORDERBY = $Param['ORDERBY'];
	}
	else
	{
		$ORDERBY = ' XPP_BEZEICHNUNG';
	}
}
else
{
	$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
}

//*****************************************************************
// Abfrage erstellen -> Zeilennummer wg. Block
//*****************************************************************
$SQL = 'SELECT Programmparameter.*';

if($AWIS_KEY1<=0)
{
	$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
}
$SQL .= ' FROM Programmparameter';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}
$SQL .= ' ORDER BY '.$ORDERBY;

//************************************************
// Aktuellen Datenblock festlegen
//************************************************
$Block = 0;
if(isset($_REQUEST['Block']))
{
	$Block = $Form->Format('N0',$_REQUEST['Block'],false);
}
elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
{
	$Block = intval($Param['BLOCK']);
}
//$Form->DebugAusgabe(1,$SQL);

//************************************************
// Zeilen begrenzen
//************************************************
$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
$StartZeile = ($Block*$ZeilenProSeite)+1;
$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
//$Form->DebugAusgabe(1,$SQL,$Param,$AWIS_KEY1);
//*****************************************************************
// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
//*****************************************************************
if($AWIS_KEY1<=0)
{
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
}
$rsXBP = $DB->RecordSetOeffnen($SQL);

//*****************************************************************
// Aktuelle Parameter sichern
//*****************************************************************
$Param['ORDERBY']=$ORDERBY;
$Param['KEY']=$AWIS_KEY1;
$Param['BLOCK']=$Block;
$AWISBenutzer->ParameterSchreiben("Formular_XBP",serialize($Param));

//awisFormular::DebugAusgabe(1,$SQL,$Param,$_POST,$Bedingung,$Block);

$BildSchirmBreite=$AWISBenutzer->ParameterLesen('BildschirmBreite');
$Form->SchreibeHTMLCode('<form name=frmBenutzerparameter action=./benutzerparameter_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

//********************************************************
// Daten anzeigen
//********************************************************
if($rsXBP->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Benutzer gefunden.</span>';
}
elseif($rsXBP->AnzahlDatensaetze()>1)						// Liste anzeigen
{
	if($BildSchirmBreite<1024)
	{
		$BSZeichen = 50;
		$BSBreite = 400;
	}
	elseif($BildSchirmBreite<1280)
	{
		$BSZeichen = 90;
		$BSBreite = 650;
	}
	else
	{
		$BSZeichen = 120;
		$BSBreite = 895;
	}

	$Form->Formular_Start();

	$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

	$Link = './benutzerparameter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=XPP_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XPP_BEZEICHNUNG'))?'~':'');

	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XPP']['XPP_BEZEICHNUNG'],300,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XPP']['XPP_BEMERKUNG'],$BSBreite);
	$Form->ZeileEnde();
	$DS=0;

	while(!$rsXBP->EOF())
	{
		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Text = $Form->LadeTextBaustein('XPP','txt_XPP_'.$rsXBP->FeldInhalt('XPP_ID'));

		if($rsXBP->FeldInhalt('XPP_XRC_ID')>0 AND ($AWISBenutzer->HatDasRecht($rsXBP->FeldInhalt('XPP_XRC_ID'))&$rsXBP->FeldInhalt('XPP_STUFE'))==0)
		{
			$Form->Erstelle_ListenFeld('XPP_BEZEICHNUNG',$Text,0,300,false,($DS%2),'font-style:italic;','','','',$AWISSprachKonserven['XPP']['err_KeineRechte']);
		}
		else
		{
			$Link = './benutzerparameter_Main.php?cmdAktion=Details&XPP_ID='.$rsXBP->FeldInhalt('XPP_ID').'';
			$Form->Erstelle_ListenFeld('XPP_BEZEICHNUNG',$Text,0,300,false,($DS%2),'',$Link);
		}

		$Text = $Form->LadeTextBaustein('XPP','lbl_XPP_'.$rsXBP->FeldInhalt('XPP_ID'));
		$Text = (strlen($Text)>$BSZeichen?substr($Text,0,$BSZeichen-2).'...':$Text);
		$Form->Erstelle_ListenFeld('XPP_BEMERKUNG',$Text,0,$BSBreite,false,($DS%2),'','','T','L',$Text);
		$Form->ZeileEnde();

		$rsXBP->DSWeiter();
	}

	$Link = './benutzerparameter_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht6&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=benutzerparameter&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	$Form->Formular_Start();

	{
		//echo '<table>';
		$AWIS_KEY1 = $rsXBP->FeldInhalt('XPP_ID');

		$Param['KEY']=$AWIS_KEY1;
		$Form->Erstelle_HiddenFeld('XPP_ID',$AWIS_KEY1);

		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./benutzerparameter_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Form->InfoZeile($Felder,'');

		if($AWIS_KEY1=='')
		{
			$EditRecht=true;
		}
		else
		{
			//$EditRecht =(($Recht6&2)!=0);
			$EditRecht = (($rsXBP->FeldInhalt('XPP_XRC_ID')>0 AND ($AWISBenutzer->HatDasRecht($rsXBP->FeldInhalt('XPP_XRC_ID'))&$rsXBP->FeldInhalt('XPP_STUFE'))==0)?false:($Recht6&1));
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XPP']['XPP_BEZEICHNUNG'].':',150);
		$Text = $Form->LadeTextBaustein('XPP','txt_XPP_'.$rsXBP->FeldInhalt('XPP_ID'));
		$Form->Erstelle_TextFeld('XPP_BEZEICHNUNG',($Text),50,350,false);
		$Form->Erstelle_HiddenFeld('XPP_BEZEICHNUNG',$rsXBP->FeldInhalt('XPP_BEZEICHNUNG'),50,350,false);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XPP']['XPP_BEMERKUNG'].':',150);
		$Text = $Form->LadeTextBaustein('XPP','lbl_XPP_'.$rsXBP->FeldInhalt('XPP_ID'));
		$Form->Erstelle_TextFeld('XPP_BEMERKUNG',($Text),50,700,false);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XPP']['XPP_DEFAULT'].':',150);

		if($rsXBP->FeldInhalt('XPP_DATENQUELLE')!='')
		{
			switch(substr($rsXBP->FeldInhalt('XPP_DATENQUELLE'),0,3))
			{
				case 'TXT':
					$Felder = explode(':',$rsXBP->FeldInhalt('XPP_DATENQUELLE'));
					$Konserve = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));

					$Daten = explode('|',$Konserve[$Felder[1]][$Felder[2]]);

					$Wert = $rsXBP->FeldInhalt('XPP_DEFAULT');
					$Form->Erstelle_SelectFeld('XBP_DEFAULT',$Wert,500,false,'','','','','',$Daten);
					$Form->ZeileEnde();

					$Wert = $AWISBenutzer->ParameterLesen($rsXBP->FeldInhalt('XPP_BEZEICHNUNG'),true);
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XBP']['XBP_WERT'].':',150);
					$Form->Erstelle_SelectFeld('XBP_WERT',$Wert,500,$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
					break;
				case 'SQL':
					$Felder = explode(':',$rsXBP->FeldInhalt('XPP_DATENQUELLE'));

					$Wert = $rsXBP->FeldInhalt('XPP_DEFAULT');
					$Form->Erstelle_SelectFeld('XPP_DEFAULT',$rsXBP->FeldInhalt('XPP_DEFAULT'),500,false,$Felder[1]);
					$Form->ZeileEnde();

					$Wert = $AWISBenutzer->ParameterLesen($rsXBP->FeldInhalt('XPP_BEZEICHNUNG'),true);
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XBP']['XBP_WERT'].':',150);
					$Form->Erstelle_SelectFeld('XBP_WERT',$Wert,500,$EditRecht,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
					break;
				default:
					$Form->Erstelle_TextFeld('XPP_DEFAULT',($rsXBP->FeldInhalt('XPP_DEFAULT')),50,700,false);
					$Form->ZeileEnde();

					$Wert = $AWISBenutzer->ParameterLesen($rsXBP->FeldInhalt('XPP_BEZEICHNUNG'),true);
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XBP']['XBP_WERT'].':',150);
					$Form->Erstelle_TextFeld('XBP_WERT',$Wert,50,500,$EditRecht,'','','',$rsXBP->FeldInhalt('XPP_DATENTYP'),'','',$rsXBP->FeldInhalt('XPP_DATENQUELLE'));
					break;
			}
		}
		else
		{
			$Form->Erstelle_TextFeld('XPP_DEFAULT',($rsXBP->FeldInhalt('XPP_DEFAULT')),50,700,false);
			$Form->ZeileEnde();
			$Wert = $AWISBenutzer->ParameterLesen($rsXBP->FeldInhalt('XPP_BEZEICHNUNG'),true);
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XBP']['XBP_WERT'].':',150);
			$Form->Erstelle_TextFeld('XBP_WERT',$Wert,50,500,$EditRecht,'','','',$rsXBP->FeldInhalt('XPP_DATENTYP'));
		}

		if($EditRecht==false)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['XPP']['err_KeineRechte'],1);
			$Form->ZeileEnde();
		}
		$AWISCursorPosition='txtXBP_WERT';
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

	if(($Recht6&(1+2+4+256))!==0)		//
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=benutzerparameter&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if(floatval($AWIS_KEY1) <> 0)
	{
		$Bedingung = 'AND XPP_ID = '.$AWIS_KEY1;
		return $Bedingung;
	}

	if(isset($Param['XPP_BEZEICHNUNG']) AND $Param['XPP_BEZEICHNUNG']!='')
	{
		$Bedingung .= 'AND ((UPPER(XPP_BEZEICHNUNG) ' . $DB->LikeOderIst('%'.$Param['XPP_BEZEICHNUNG'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ') ';
		$Bedingung .= 'OR (UPPER(XPP_BEMERKUNG) ' . $DB->LikeOderIst('%'.$Param['XPP_BEZEICHNUNG'].'%',awisDatenbank::AWIS_LIKE_UPPER) . ') ';
		$Bedingung .= ')';
	}
	if(($AWISBenutzer->HatDasRecht(6)&(64))==0)
	{
		$Bedingung .= ' AND XPP_UEBERSCHREIBBAR <= 10';
	}

	return $Bedingung;
}