<?php
require_once('awis_sonderausweiseJob.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $StartTag = '';
    $EndTag = '';
    $Mount = '';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    
    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--mount':
                          	$Mount = $argv[++$i];
                           	break;
                   
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --mount  <bool>                    Mount erfolgreich? 1=Ja 0=Nein'.PHP_EOL;                  
                            echo PHP_EOL;
                            die();
            }
    }


    $SonderausweiseExport = new awis_sonderausweiseJob($Benutzer);
    $SonderausweiseExport->DebugLevel($DebugLevel);
    $SonderausweiseExport->ExportSonderausweise ();

}
catch (Exception $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;
 
}
?>
