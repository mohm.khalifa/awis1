<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$FIL_ID=$_POST['txtREL_FIL_ID'];
	$LFDNR=$_POST['txtREL_LFDNR'];
	//$Form->DebugAusgabe(1,$_POST);	
	
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtREL_MENGE',1,1));
	//$Form->DebugAusgabe(1,$Felder);
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,13));
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_MENGE='.intval($_POST[$Feld]);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';
				$SQL .= ' ,REL_STATUS = 2';
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}				
			}
		}		
	}

	$Felder = explode(';',$Form->NameInArray($_POST, 'oldREL_BESCHAEDIGT_VR_',1,1)); // Nicht txt, da die bei CHECKBOX nicht generiert werden
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{			
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
		
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
					
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_VR='.$DB->FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';				
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}							
			}	
		}
	}

	$Felder = explode(';',$Form->NameInArray($_POST, 'oldREL_BESCHAEDIGT_VL_',1,1)); // Nicht txt, da die bei CHECKBOX nicht generiert werden
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{			
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
		
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
					
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_VL='.$DB->FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';				
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}							
			}	
		}
	}

	$Felder = explode(';',$Form->NameInArray($_POST, 'oldREL_BESCHAEDIGT_HR_',1,1)); // Nicht txt, da die bei CHECKBOX nicht generiert werden
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{			
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
		
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
					
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_HR='.$DB->FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';				
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}							
			}	
		}
	}

	$Felder = explode(';',$Form->NameInArray($_POST, 'oldREL_BESCHAEDIGT_HL_',1,1)); // Nicht txt, da die bei CHECKBOX nicht generiert werden
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{			
			$FeldName = substr($Feld,3);

			$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
		
			if($_POST['old'.$FeldName] != $AktWert)
			{
				$Key = intval(substr($FeldName,19));
					
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BESCHAEDIGT_HL='.$DB->FeldInhaltFormat('T',$AktWert,true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';				
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}							
			}	
		}
	}	
	
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtREL_BEMERKUNG',1,1));
	//$Form->DebugAusgabe(1,$Felder);
	foreach($Felder AS $Feld)
	{					
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE Raedereinlagerungen';
				$SQL .= ' SET REL_BEMERKUNG='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);			
				$SQL .= ' ,REL_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,REL_UserDat=SYSDATE';				
				$SQL .= ' WHERE REL_KEY=0'.$Key.'';
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908121043');
					throw new awisException('Fehler beim Speichern',908121043,$SQL,2);
				}				
			}
		}		
	}
}		
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);	
}
catch (Exception $ex)
{
	
}
?>