<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RRP','*');
	$TextKonserven[]=array('REH','*');
	$TextKonserven[]=array('RST','*');
	$TextKonserven[]=array('RPP','*');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15000 = $AWISBenutzer->HatDasRecht(15000);
	if($Recht15000==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	if (isset($_POST['sucEXPORTART']))
	{
		$Param['EXPORTART'] = $_POST['sucEXPORTART'];
		$Param['GELISTET'] = $_POST['sucGELISTET'];
		$Param['RRP_ATUNR'] = $_POST['sucRRP_ATUNR'];
		$Param['RPP_LAN_CODE'] = $_POST['sucRPP_LAN_CODE'];
		$Param['RRP_KENN1'] = $_POST['sucRRP_KENN1'];
		$Param['RRP_KENN2'] = $_POST['sucRRP_KENN2'];
		$Param['RRP_KENN_L2'] = $_POST['sucRRP_KENN_L2'];
		$Param['RST_REH_KEY'] = $_POST['sucRST_REH_KEY'];
		$Param['RST_TYP'] = $_POST['sucRST_TYP'];
		$Param['RST_TYP2'] = $_POST['sucRST_TYP2'];
		$Param['RST_TYP3'] = $_POST['sucRST_TYP3'];
		$Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');
		$AWISBenutzer->ParameterSchreiben("Formular_RRP_Export", serialize($Param));
	}
	
	//Pr�fen, ob mind. ein relevantes Feld gef�llt wurde
	$Prueffelder = array(
			0 => 'GELISTET',
			1 => 'RRP_ATUNR',
			2 => 'RST_REH_KEY',
			3 => 'RST_TYP',
			4 => 'RST_TYP2',
			5 => 'RST_TYP3',
			6 => 'RPP_LAN_CODE',
			7 => 'RRP_KENN1',
			8 => 'RRP_KENN2',
			9 => 'RRP_KENN_L2');
	
	$FeldNamen = array();
	
	foreach ($Prueffelder AS $Feld)
	{
		if (isset($_POST['suc' . $Feld]) and ($_POST['suc' . $Feld]== '' or $_POST['suc' . $Feld]== '-1'))
		{
			$FeldNamen[] = $Feld;
		}
	}

	if (count($FeldNamen) == count($Prueffelder))
	{
		//TODO Hinweistext farbig???
		
		$Hinweistext = 'Es muss mindestens ein Auswahlkriterium gew�hlt werden!<br>';
		$Form->ZeileStart();
		$Form->Hinweistext($Hinweistext);
		$Form->ZeileEnde();
	
		$Link = './reifenpflege_Main.php?cmdAktion=Export';
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();
	
		die();
	}
	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	//$_POST['sucEXPORTART']='Preisexport';
	
	$DateiName = $_POST['sucEXPORTART'];
	
	$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);
	
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');

	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
			break;
		case 2:                 // Excel 2007
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
			break;
	}
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setTitle(utf8_encode($DateiName));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode($_POST['sucEXPORTART']));
	
	$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($_POST);
	
	switch($_POST['sucEXPORTART'])
	{
		case 'Preise':
			// Alle Informationen sammeln
			$SQL  = 'SELECT SYSDATE AS EXPORTDATE,';
			$SQL .= ' RRP_KEY,';
			$SQL .= ' RRP_ATUNR,';
			$SQL .= ' RST_EAN,';
			$SQL .= ' RST_LIE_NR,';
			$SQL .= ' RST_LARTNR,';
			$SQL .= ' RST_BEZEICHNUNG,';
			$SQL .= ' RST_BREITE,';
			$SQL .= ' RST_QUERSCHNITT,';
			$SQL .= ' RST_INNENDURCHMESSER,';
			$SQL .= ' RST_BAUART,';
			$SQL .= ' RST_SPEEDINDEX,';
			$SQL .= ' RST_KBPREIS,';
			$SQL .= ' RST_TYP,';
			$SQL .= ' RST_TYP2,';
			$SQL .= ' RST_TYP3,';
			$SQL .= ' RPP_LAN_CODE,';
			$SQL .= ' COALESCE(RPP_VK,0) AS RPP_VK,';
			$SQL .= ' RPP_GUELTIGAB,';
			$SQL .= ' REH_BEZEICHNUNG';
			$SQL .= ' FROM reifenstamm';
			$SQL .= ' INNER JOIN reifenpflege ON rrp_rst_key=rst_key';
			$SQL .= ' LEFT JOIN reifenhersteller ON rst_reh_key=reh_key ';
			$SQL .= ' LEFT JOIN reifenpflegepreise ON rpp_rrp_key=rrp_key ';
			if ($Bedingung != '')
			{
				$SQL .= ' WHERE ' . substr($Bedingung, 4);
			}
			$SQL .= ' ORDER BY rrp_atunr, rst_breite, rst_querschnitt, rst_innendurchmesser, rpp_lan_code';
		
			$rsRRP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RRP'));		
			break;
		default:
			break;
	}
	
	//var_dump($SQL);
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode($DateiName));
	$SpaltenNr = 0;
	$ZeilenNr = 1;		

	if($_POST['sucEXPORTART']=='Preise')
	{
		//�berschrift
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($_POST['sucEXPORTART']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$SpaltenNr++;

		//bin zu doof, da� Datum anders auszugeben :-(
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('DU',$rsRRP->FeldInhalt('EXPORTDATE')));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$ZeilenNr++;

		// Daten zeilenweise exportieren
		$SpaltenNr = 0;
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['REH']['REH_BEZEICHNUNG']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RRP']['RRP_ATUNR']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_EAN']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_LIE_NR']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_LARTNR']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_BEZEICHNUNG']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_TYP']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_TYP2']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_TYP3']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_BREITE']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_QUERSCHNITT']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_BAUART']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_INNENDURCHMESSER']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_SPEEDINDEX']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($AWISSprachKonserven['RST']['RST_KBPREIS']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('LAND'));//$AWISSprachKonserven['RPP']['RPP_LAN_CODE']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('VK'));//$AWISSprachKonserven['RPP']['RPP_VK']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('GUELTIGAB'));//$AWISSprachKonserven['RPP']['RPP_GUELTIGAB']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			 
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('KEY'));//$AWISSprachKonserven['RRP']['RRP_KEY']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
		
		$ZeilenNr++;
		$SpaltenNr=0;
		while(!$rsRRP->EOF())
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('REH_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RRP_ATUNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_EAN'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LIE_NR'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LARTNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP2')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP3')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_BREITE')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_QUERSCHNITT')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_INNENDURCHMESSER')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_BAUART'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_SPEEDINDEX'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('N2',$rsRRP->FeldInhalt('RST_KBPREIS')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RPP_LAN_CODE'))),PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('N2',$rsRRP->FeldInhalt('RPP_VK')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			
			$Datum=$Form->PruefeDatum($rsRRP->FeldInhalt('RPP_GUELTIGAB'),false,false,true);
			$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, PHPExcel_Shared_Date::PHPToExcel($Datum));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RRP_KEY')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
				
			$ZeilenNr++;
			$SpaltenNr=0;
			
			$rsRRP->DSWeiter();
		}
	}
	
	for($S='A';$S<='F';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
			break;
		case 2:                 // Excel 2007
			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
			break;
	}
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}



/**
 * Bedingung zusammenbauen
 *
 * @param string $_POST
 * @return string
 */
function _BedingungErstellen()
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if (floatval($AWIS_KEY1) != 0)
	{
		$Bedingung.= 'AND RRP_KEY = :var_N0_RRP_KEY';
		$DB->SetzeBindevariable('RRP', 'var_N0_RRP_KEY', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
		return $Bedingung;
	}

	if (isset($_POST['sucGELISTET']) AND $_POST['sucGELISTET'] != '-1')
	{
		if ($_POST['sucGELISTET']<2)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS '.($_POST['sucGELISTET']==0?'':'NOT').' NULL ';
		}
		elseif ($_POST['sucGELISTET']==2)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS NOT NULL ';
			$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
		}
		elseif ($_POST['sucGELISTET']==3)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS NULL ';
			$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
		}
	}

	if (isset($_POST['sucRRP_ATUNR']) AND $_POST['sucRRP_ATUNR'] != '')
	{
		$Bedingung .= 'AND RRP_ATUNR ' . $DB->LIKEoderIST($_POST['sucRRP_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER);
	}

	if (isset($_POST['sucRRP_KENN1']) AND $_POST['sucRRP_KENN1'] != '')
	{
		$Bedingung .= 'AND RRP_KENN1 = :var_T_RRP_KENN1 ';
		$DB->SetzeBindevariable('RRP', 'var_T_RRP_KENN1', $_POST['sucRRP_KENN1'], awisDatenbank::VAR_TYP_TEXT);
	}

	if (isset($_POST['sucRRP_KENN2']) AND $_POST['sucRRP_KENN2'] != '')
	{
		$Bedingung .= 'AND RRP_KENN2 = :var_T_RRP_KENN2 ';
		$DB->SetzeBindevariable('RRP', 'var_T_RRP_KENN2', $_POST['sucRRP_KENN2'], awisDatenbank::VAR_TYP_TEXT);
	}

	if (isset($_POST['sucRRP_KENN_L2']) AND $_POST['sucRRP_KENN_L2'] != '')
	{
		$Bedingung .= 'AND RRP_KENN_L2 = :var_T_RRP_KENN_L2 ';
		$DB->SetzeBindevariable('RRP', 'var_T_RRP_KENN_L2', $_POST['sucRRP_KENN_L2'], awisDatenbank::VAR_TYP_TEXT);
	}

	if (isset($_POST['sucRPP_LAN_CODE']) AND $_POST['sucRPP_LAN_CODE'] != '-1')
	{
		$Bedingung .= 'AND RPP_LAN_CODE = :var_T_RPP_LAN_CODE ';
		$DB->SetzeBindevariable('RRP', 'var_T_RPP_LAN_CODE', $_POST['sucRPP_LAN_CODE'], awisDatenbank::VAR_TYP_TEXT);
	}
	
	if (isset($_POST['sucRST_REH_KEY']) AND $_POST['sucRST_REH_KEY'] != '-1')
	{
		$Bedingung .= 'AND RST_REH_KEY = :var_N0_RST_REH_KEY ';
		$DB->SetzeBindevariable('RRP', 'var_N0_RST_REH_KEY', $_POST['sucRST_REH_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}

	if (isset($_POST['sucRST_TYP']) AND $_POST['sucRST_TYP'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP) ';
		$DB->SetzeBindevariable('RRP', 'var_N0_RST_TYP', $_POST['sucRST_TYP'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['sucRST_TYP2']) AND $_POST['sucRST_TYP2'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP2 ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP2 ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP2) ';
		$DB->SetzeBindevariable('RRP', 'var_N0_RST_TYP2', $_POST['sucRST_TYP2'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['sucRST_TYP3']) AND $_POST['sucRST_TYP3'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP3 ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP3 ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP3) ';
		$DB->SetzeBindevariable('RRP', 'var_N0_RST_TYP3', $_POST['sucRST_TYP3'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	return $Bedingung;
}

?>