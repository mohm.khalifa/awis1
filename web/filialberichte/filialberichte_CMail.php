<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);
date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class CMail
{

private static $instance = NULL;
private $_DB;            
protected $_AWISBenutzer;
private $flagIsSunday = false;	
private $aktKW = '';
private $zuImportierendeKW = '';
private $firstDay = '';
private $lastDay = '';
private $_Werkzeug;

const PROTOKOLLDATEI = '/var/log/filialberichte.log';

public function __construct() {
  $this->_AWISBenutzer = awisBenutzer::Init();
  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  $this->_DB->Oeffnen();
  $this->_Werkzeug = new awisWerkzeuge();
}

  
public function getInstance() {
   if(self::$instance === NULL) {
      self::$instance = new self;
   }

   return self::$instance;
}

private function __clone() {}


public function import()
{
	$this->isSunday();

	if($this->flagIsSunday == true)
	{
		echo "RUN -> CMail";
		$this->pruefeAnzBerichte();
	}
	else 
	{
		echo "KEIN RUN EXIT-->CODE(0)";		
	}
	
}

public function pruefeAnzBerichte()
{		
	//NEU
	$SQL = ' select FIL_ID, GBL_EMAIL, TKDL_EMAIL, ANZAHL, EMAIL, FIL_LAN_WWSKENN ';
	$SQL.= ' from (';
	$SQL.= ' Select FIL_ID, FIL_LAN_WWSKENN';
	$SQL.= ' , (SELECT KKO_WERT FROM KontakteKommunikation WHERE KKO_KON_KEY = filialrolle(FIL_ID,25,SYSDATE ,\'KEY\') AND KKO_KOT_KEY = 7 AND ROWNUM=1) AS GBL_EMAIL';
	$SQL.= ' , (SELECT KKO_WERT FROM KontakteKommunikation WHERE KKO_KON_KEY = filialrolle(FIL_ID,26,SYSDATE ,\'KEY\') AND KKO_KOT_KEY = 7 AND ROWNUM=1) AS TKDL_EMAIL';
	$SQL.= ' , (SELECT COUNT(*) FROM FILIALBERICHTE WHERE FLB_FIL_ID = FIL_ID AND FLB_PEZ_STATUS=1';
	$SQL.= ' AND FLB_DATUM >='.$this->_DB->FeldInhaltFormat('D',$this->firstDay);
	$SQL.= ' AND FLB_DATUM <='.$this->_DB->FeldInhaltFormat('D',$this->lastDay).') as ANZAHL';
	$SQL.= ' , (SELECT LPAD(FIL_ID,\'4\',\'0\')||FUNC_EMAILENDUNG_FILIALE(FIL_ID) FROM DUAL) AS EMAIL';
	$SQL.= ' FROM V_FILIALPFAD';
	$SQL.= ' INNER JOIN FILIALEN ON FIL_ID = XX1_FIL_ID)'; 
	$SQL.= ' where anzahl < 5';
	$SQL.= ' order by fil_id';
	echo $SQL;
	//die();
	$rsAnz = $this->_DB->RecordSetOeffnen($SQL);
	
	
	while(!$rsAnz->EOF())
	{
		//Sende E-Mail	  	
	  	
		echo $rsAnz->FeldInhalt('EMAIL').PHP_EOL;
		echo $rsAnz->FeldInhalt('GBL_EMAIL').PHP_EOL;
		echo $rsAnz->FeldInhalt('TKDL_EMAIL').PHP_EOL;
		
		$Text = '';
		
 		if ($rsAnz->FeldInhalt('FIL_LAN_WWSKENN')=='NED')
 		{
 			$Text.= "!Belangrijke informatie voor de kwaliteitstest!";
			$Text.= "\r \n";			
 			$Text.= "U heeft ".$rsAnz->FeldInhalt('ANZAHL')." kwaliteitstests uitgevoerd. U moet 5 controles per week uitvoeren.";
 			$Text.= "\r \n";
 			$Text.= "\r \n"; 			 			
 		}
 		/*
 		if ($rsAnz->FeldInhalt('FIL_LAN_WWSKENN')=='CZE')
 		{
 			$Text.= "!D?le�it� informace ke kontrole zaji�t?n� kvality!";
			$Text.= "\r \n";
 			$Text.= "Po?et proveden�ch kontrol zaji�t?n� kvality je ".$rsAnz->FeldInhalt('ANZAHL').", pot?ebn� je 5 kus? za t�den.";
 			$Text.= "\r \n";
 			$Text.= "\r \n"; 			 			
 		}*/
		
	  	$Text.= "!Wichtige Information zum QS- Check!";
		$Text.= "\r \n";
 		$Text.= "Es sind ".$rsAnz->FeldInhalt('ANZAHL')." QS Checks durchgef�hrt worden, es sind 5 St�ck in der Woche erforderlich.";
 		$Text.= "\r \n";
 		
 		$Empfaenger = array();
 		$EmpfaengerBCC = array();
 		
 		if ($rsAnz->FeldInhalt('GBL_EMAIL')!='')
 		{
 			$Empfaenger[] = $rsAnz->FeldInhalt('GBL_EMAIL');
 		}
 		
 		if ($rsAnz->FeldInhalt('TKDL_EMAIL')!='')
 		{
 			$Empfaenger[] = $rsAnz->FeldInhalt('TKDL_EMAIL');
 		} 		
 		 		
 		//$EmpfaengerBCC[] = 'thomas.riedl@de.atu.eu';
 		
 		$this->_Werkzeug->EMail($rsAnz->FeldInhalt('EMAIL'),'QS - Check Filiale '.$rsAnz->FeldInhalt('FIL_ID'), $Text,2,'','awis@de.atu.eu','',$Empfaenger,'');
		//$this->_Werkzeug->EMail('thomas.riedl@de.atu.eu','QS - Check Filiale '.$rsAnz->FeldInhalt('FIL_ID'), $Text,2,'','awis@de.atu.eu','','thomas.riedl@de.atu.eu','thomas.riedl@de.atu.eu');
		
		file_put_contents(self::PROTOKOLLDATEI,date('c').';'.'Filiale '.$rsAnz->FeldInhalt('FIL_ID').': Anzahl '.$rsAnz->FeldInhalt('ANZAHL').PHP_EOL,FILE_APPEND);
		//die();
		
	  	$rsAnz->DSWeiter();
	}
}


public function firstMonday($kw,$year = null)
{
	if(is_null($year)) $year = date('Y');

	$date = mktime(0,0,0,1,4,$year);

	return mktime(0,0,0,1,date("d",$date) + (date("w",$date) == 0 ? - 6 : (1 - date("w",$date))),$year);
}

public function getKW($kw,$year = null)
{
	if(is_null($year)) $year = date('Y');
	
	return $this->firstMonday($year) + 604800 * ($kw - 1);
}


private function isSunday()
{
	echo "Datum:".date('D');
	
	if(date('D') == 'Mon')
	{
		$this->flagIsSunday = true;
		$this->aktKW = date('W')-1;
		echo "Kalenderwoche:".$this->aktKW."\n";
		$this->zuImportierendeKW = $this->aktKW;
		echo "AKTKW:".$this->zuImportierendeKW."\n";
		
		$this->firstDay = date("d.m.Y",$this->getKW($this->zuImportierendeKW,date('Y')));
		$this->lastDay = strtotime($this->firstDay)+432000;
		$this->lastDay = date("d.m.Y",$this->lastDay);
		
		//$this->lastDay = $this->lastDay.".".date('Y')."\n";
		
		echo $this->firstDay."\n";
		echo $this->lastDay."\n";
		
	}
	else 
	{
		$this->flagIsSunday = false;		
	}
}

}

?>