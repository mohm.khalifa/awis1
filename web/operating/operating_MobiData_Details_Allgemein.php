<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;
global $MDI;

//********************************************************
// Parameter ?
//********************************************************
$Param = array_fill(0,15,'');
$MDI->Form->DebugAusgabe(1,$_POST);
if(isset($_POST['cmdSuche_x']))
{
    $Param['KEY']=0;

    if (isset($_POST['sucMDI_NAME'])) {
        $Param['MDI_NAME']=$_POST['sucMDI_NAME'];
    }
    $Param['MDI_STATUS']=$_POST['sucMDI_STATUS'];
    $Param['MDI_DATUM']=$_POST['sucMDI_DATUM'];
    $Param['MDI_START']=$_POST['sucMDI_START'];
    $Param['MDI_ENDE']=$_POST['sucMDI_ENDE'];
    $Param['MDI_DATEI_DE']=$_POST['sucMDI_DATEI_DE'];
    $Param['MDI_DATEI_AT']=$_POST['sucMDI_DATEI_AT'];
    $Param['MDI_DATEI_CH']=$_POST['sucMDI_DATEI_CH'];

    $Param['ORDERBY']='';
    $Param['BLOCK']='';
    $Param['KEY']=0;

    $MDI->AWISBenutzer->ParameterSchreiben('MDISuche',serialize($Param));
}

$Param = unserialize($MDI->AWISBenutzer->ParameterLesen('MDISuche'));

//********************************************************
// Bedingung erstellen
//********************************************************
$Bedingung = $MDI->_BedingungErstellen($Param);

//***************************************
// Daten anzeigen
//***************************************
$MDI->Form->Formular_Start();

$SQL = 'SELECT MDJ_KEY, MDJ_BEZEICHNUNG FROM MOBIDATAJOBS';

$rsMDJ = $MDI->DB->RecordSetOeffnen($SQL);

$DS = 0;

while (!$rsMDJ->EOF()) {

    $SQL = 'SELECT * FROM (';
    $SQL .= 'SELECT * FROM MOBIDATAJOBS';
    $SQL .= ' LEFT JOIN MOBIDATAJOBSINFO';
    $SQL .= ' ON MOBIDATAJOBS.MDJ_KEY = MOBIDATAJOBSINFO.MDI_MDJ_KEY';
    $SQL .= ' WHERE MOBIDATAJOBSINFO.MDI_MDJ_KEY = '.$rsMDJ->FeldInhalt('MDJ_KEY');

    if($Bedingung!='')
    {
        $SQL .= ' '.$Bedingung;
    }

    $SQL .= ' ORDER BY MDI_DATUM DESC, MDI_START DESC';
    $SQL .= ') WHERE ROWNUM < 6';

    $rsMDI = $MDI->DB->RecordSetOeffnen($SQL, $MDI->DB->Bindevariablen('MDI'));

    $status = array();
    $DS = 0;
    while (!$rsMDI->EOF()) {
        $status[] = $rsMDI->FeldInhalt('MDI_STATUS');
        $datum[] = date('d.m.Y', strtotime($rsMDI->FeldInhalt('MDI_DATUM')));
        $Dateien = array();
        $DE[] = $rsMDI->FeldInhalt('MDI_DATEI_DE');
        $AT[] = $rsMDI->FeldInhalt('MDI_DATEI_AT');
        $CH[] = $rsMDI->FeldInhalt('MDI_DATEI_CH');
        $rsMDI->DSWeiter();
        $DS++;
    }

    //mindestens ein Fehler
    if (in_array(30, $status)) {
        $color = '#EED6D6'; //rot
    //keine Fehler aber mindestens ein aktiver Durchlauf oder kein Datensatz mit aktuellem Datum vorhanden
    } elseif (in_array(10, $status) or !in_array(date('d.m.Y'), $datum) or in_array('', $DE) or in_array('', $AT) or in_array('', $CH)) {
        $color = '#FBF7DC'; //gelb
        //keine Fehler und aktiven Durchl�ufe, nur erfolgreiche Durchl�ufe
    } elseif (in_array(20, $status)) {
        $color = '#D8EECF'; //gr�n
    //kein Durchlauf vorhanden
    } else {
        $color = 'white';
    }

    $rsMDI = $MDI->DB->RecordSetOeffnen($SQL, $MDI->DB->Bindevariablen('MDI'));

    if ($rsMDI->AnzahlDatensaetze() >= 1)
    {
        $MDI->Form->FormularBereichStart();
        $MDI->Form->FormularBereichInhaltStart($rsMDJ->FeldInhalt('MDJ_BEZEICHNUNG'), false, '', '', '', '', array(), '', '', 'background-color: ' . $color);
        $MDI->Form->ZeileStart();
        $MDI->Form->Erstelle_Liste_Ueberschrift('Status', 100);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Datum', 100);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Start', 80);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Ende', 80);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Datei (Deutschland)', 450);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Datei (�sterreich)', 450);
        $MDI->Form->Erstelle_Liste_Ueberschrift('Datei (Schweiz)', 450);
        $MDI->Form->ZeileEnde();

        $DS = 0;

        while (!$rsMDI->EOF()) {

            $HG = ($DS % 2);

            $MDI->Form->ZeileStart();

            if ($rsMDI->FeldInhalt('MDI_STATUS') == 30) {
                $status = 'Failed';
                $color = '#EED6D6'; //rot
                //keine Fehler aber mindestens ein aktiver Durchlauf oder kein Datensatz mit aktuellem Datum vorhanden
            } elseif ($rsMDI->FeldInhalt('MDI_STATUS') == 10) {
                $color = '#FBF7DC'; //gelb
                $status = 'Running';
                //keine Fehler und aktiven Durchl�ufe, nur erfolgreiche Durchl�ufe
            } elseif ($rsMDI->FeldInhalt('MDI_STATUS') == 20) {
                $color = '#D8EECF'; //gr�n
                $status = 'Succeeded';
            }

            //Wenn eine Datei fehlt
            if ($rsMDI->FeldInhalt('MDI_DATEI_DE') == '' or $rsMDI->FeldInhalt('MDI_DATEI_AT') == '' or $rsMDI->FeldInhalt('MDI_DATEI_CH') == '') {
                $color = '#FBF7DC'; //gelb
            }

            $MDI->Form->Erstelle_ListenFeld('MDI_STATUS', $status, 100, 100, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_DATUM', date('d.m.Y', strtotime($rsMDI->FeldInhalt('MDI_DATUM'))), 100, 100, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_START', $rsMDI->FeldInhalt('MDI_START'), 80, 80, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_ENDE', $rsMDI->FeldInhalt('MDI_ENDE'), 80, 80, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_DATEI_DE', $rsMDI->FeldInhalt('MDI_DATEI_DE') . ', ' . $rsMDI->FeldInhalt('MDI_DATEI_KB_DE') . ' KB' . ', ' . $rsMDI->FeldInhalt('MDI_ZEILEN_DE') . ' Zeilen', 450, 450, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_DATEI_AT', $rsMDI->FeldInhalt('MDI_DATEI_AT') . ', ' . $rsMDI->FeldInhalt('MDI_DATEI_KB_AT') . ' KB' . ', ' . $rsMDI->FeldInhalt('MDI_ZEILEN_AT') . ' Zeilen', 450, 450, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->Erstelle_ListenFeld('MDI_DATEI_CH', $rsMDI->FeldInhalt('MDI_DATEI_CH') . ', ' . $rsMDI->FeldInhalt('MDI_DATEI_KB_CH') . ' KB' . ', ' . $rsMDI->FeldInhalt('MDI_ZEILEN_CH') . ' Zeilen', 450, 450, false, ($DS % 2), 'background-color: ' . $color);
            $MDI->Form->ZeileEnde();

            $rsMDI->DSWeiter();
            $DS++;
        }
        $MDI->Form->FormularBereichInhaltEnde();
        $MDI->Form->FormularBereichEnde();
        $hinweis = true;
    }
    $rsMDJ->DSWeiter();
    $DS++;

    $datum = array();
    $DE = array();
    $AT = array();
    $CH = array();
}

if ($rsMDI->AnzahlDatensaetze() == 0 and !isset($hinweis)) {
    $MDI->Form->Hinweistext($MDI->AWISSprachkonserven['Wort']['KeineDatenVorhanden']);
    $hinweis = true;
}

$MDI->Form->Formular_Ende();

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$MDI->Form->SchaltflaechenStart();
$MDI->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $MDI->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');
$MDI->Form->SchaltflaechenEnde();
