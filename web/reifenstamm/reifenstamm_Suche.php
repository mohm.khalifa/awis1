<?php
/**
 * Suchmaske f�r die Auswahl Reifenstamm
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130430
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('RST','%');
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_ROLLRES');
	$TextKonserven[]=array('Liste','lst_WETGRIP');
	$TextKonserven[]=array('Liste','lst_NOISECL');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15300=$AWISBenutzer->HatDasRecht(15300);		
	if ($Recht15300 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./reifenstamm_Main.php?cmdAktion=Details>");	

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RST'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_STATUS'].':',150);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_IMPORTSTATUS'].':',150);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['txt_GELISTET'].':',75);
    $Form->ZeileEnde();
        
    $Form->ZeileStart();
    $Daten = explode('|',$AWISSprachKonserven['RST']['lst_RST_STATUS']);
    $Form->Erstelle_SelectFeld('*RST_STATUS',($Param['SPEICHERN']=='on'?$Param['RST_STATUS']:''), 150, true, '','-1~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Daten = explode('|',$AWISSprachKonserven['RST']['lst_RST_IMPORTSTATUS']);
    $Form->Erstelle_SelectFeld('*RST_IMPORTSTATUS',($Param['SPEICHERN']=='on'?$Param['RST_IMPORTSTATUS']:''), 150, true, '','-1~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Daten = explode("|",$AWISSprachKonserven['RST']['lst_gelistet']);
    $Form->Erstelle_SelectFeld('*GELISTET',($Param['SPEICHERN']=='on'?$Param['GELISTET']:''), 75, true, '','-1~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Form->ZeileEnde();
	
    $Form->Trennzeile('O');
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'].':',150);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ATUNR'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LIE_NR'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LARTNR'].':',230);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_EAN'].':',120);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*RST_REH_KEY',($Param['SPEICHERN']=='on'?$Param['RST_REH_KEY']:'0'),150,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->Erstelle_TextFeld('*RRP_ATUNR',($Param['SPEICHERN']=='on'?$Param['RRP_ATUNR']:''),6,75,true);
	$Form->Erstelle_TextFeld('*RST_LIE_NR',($Param['SPEICHERN']=='on'?$Param['RST_LIE_NR']:''),4,75,true);
	$Form->Erstelle_TextFeld('*RST_LARTNR',($Param['SPEICHERN']=='on'?$Param['RST_LARTNR']:''),30,230,true);
	$Form->Erstelle_TextFeld('*RST_EAN',($Param['SPEICHERN']=='on'?$Param['RST_EAN']:''),30,120,true);
	$Form->ZeileEnde();
	
    $Form->Trennzeile('O');
	
    $Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP'].':',150);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP2'].':',150);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP3'].':',150);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$SQL = 'SELECT RET_ID, RET_TYP FROM REIFENTYP ORDER BY RET_ID';
	$Form->Erstelle_SelectFeld('*RST_TYP',($Param['SPEICHERN']=='on'?$Param['RST_TYP']:'0'),150,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_SelectFeld('*RST_TYP2',($Param['SPEICHERN']=='on'?$Param['RST_TYP2']:'0'),150,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_SelectFeld('*RST_TYP3',($Param['SPEICHERN']=='on'?$Param['RST_TYP3']:'0'),150,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BREITE'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_QUERSCHNITT'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BAUART'].':',75);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_INNENDURCHMESSER'].':',140);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX_kurz'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX2_kurz'].':',100);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextFeld('*RST_BREITE',($Param['SPEICHERN']=='on'?$Param['RST_BREITE']:''),6,75,true);
	$Form->Erstelle_TextFeld('*RST_QUERSCHNITT',($Param['SPEICHERN']=='on'?$Param['RST_QUERSCHNITT']:''),10,100,true);
//	$Form->Erstelle_TextFeld('*RST_BAUART',($Param['SPEICHERN']=='on'?$Param['RST_BAUART']:''),4,65,true);
	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_BAUART']);
	$Form->Erstelle_SelectFeld('*RST_BAUART',($Param['SPEICHERN']=='on'?$Param['RST_BAUART']:''), 75, true, '','-1~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	
	$Form->Erstelle_TextFeld('*RST_INNENDURCHMESSER',($Param['SPEICHERN']=='on'?$Param['RST_INNENDURCHMESSER']:''),10,140,true);
	$SQL = 'SELECT SPI_ID, SPI_SYMBOL FROM SPEEDINDEX ORDER BY SPI_ID';
	$Form->Erstelle_SelectFeld('*RST_SPEEDINDEX',($Param['SPEICHERN']=='on'?$Param['RST_SPEEDINDEX']:'0'),100,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_SelectFeld('*RST_SPEEDINDEX2',($Param['SPEICHERN']=='on'?$Param['RST_SPEEDINDEX2']:'0'),100,true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LLKW'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX_kurz'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX2_kurz'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX3_kurz'].':',100);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_RF_kurz'].':',100);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextFeld('*RST_LLKW',($Param['SPEICHERN']=='on'?$Param['RST_LLKW']:''),4,100,true);
	$Form->Erstelle_TextFeld('*RST_LOADINDEX',($Param['SPEICHERN']=='on'?$Param['RST_LOADINDEX']:''),10,100,true);
	$Form->Erstelle_TextFeld('*RST_LOADINDEX2',($Param['SPEICHERN']=='on'?$Param['RST_LOADINDEX2']:''),10,100,true);
	$Form->Erstelle_TextFeld('*RST_LOADINDEX3',($Param['SPEICHERN']=='on'?$Param['RST_LOADINDEX3']:''),10,100,true);
	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_RF']);
	$Form->Erstelle_SelectFeld('*RST_RF',($Param['SPEICHERN']=='on'?$Param['RST_RF']:''), "130:100", true, '','-1~'. $AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	//$Form->Erstelle_TextFeld('*RST_RF',($Param['SPEICHERN']=='on'?$Param['RST_RF']:''),10,100,true);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'].':',300);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextFeld('*RST_BEZEICHNUNG',($Param['SPEICHERN']=='on'?$Param['RST_BEZEICHNUNG']:''),100,300,true);
	$Form->ZeileEnde();
	
	
	$Form->Trennzeile('O');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ROLLRES'].':',150);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_WETGRIP'].':',150);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISECL'].':',150);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISEPE'].':',150);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Daten = explode("|",$AWISSprachKonserven['Liste']['lst_ROLLRES']);
    $Form->Erstelle_SelectFeld('*RST_ROLLRES',$Param['SPEICHERN']=='on'?$Param['RST_ROLLRES']:'0', 150, true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Daten = explode("|",$AWISSprachKonserven['Liste']['lst_WETGRIP']);
    $Form->Erstelle_SelectFeld('*RST_WETGRIP',$Param['SPEICHERN']=='on'?$Param['RST_WETGRIP']:'0', 150, true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Daten = explode("|",$AWISSprachKonserven['Liste']['lst_NOISECL']);
    $Form->Erstelle_SelectFeld('*RST_NOISECL',$Param['SPEICHERN']=='on'?$Param['RST_NOISECL']:'0', 150, true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    
    $Noisepe = '';
    for ($i = 1; $i < 100; $i++)
    {
    $Noisepe = $Noisepe.'|'.$i.'~'.$i;
    }
    $Noisepe = substr($Noisepe,1);
    $Daten = explode("|",$Noisepe);
    $Form->Erstelle_SelectFeld('*RST_NOISEPE',$Param['SPEICHERN']=='on'?$Param['RST_NOISEPE']:'0', 150, true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
    $Form->ZeileEnde();
    
    $Form->Trennzeile('O');
    
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',150);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht15300&4) == 4)		
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201304301119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201304301120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>