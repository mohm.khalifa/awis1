<?php
/**
 * Filialberichte
 *
 * @author Christian Beierl
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$TextKonserven = array();
	$TextKonserven[]=array('FLB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','ZeitVon');
	$TextKonserven[]=array('Wort','ZeitBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht16000 = $AWISBenutzer->HatDasRecht(16000);
	if($Recht16000==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./filialberichte_loeschen.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./filialberichte_speichern.php');		
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));
		
		if (isset($_POST['txtFLB_FIL_ID']))
		{
			$Param['FIL_ID'] = $_POST['txtFLB_FIL_ID'];		
		}
		else 
		{
			$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
			if ($UserFilialen != '')
			{
				$Param['FIL_ID']=$UserFilialen;	
			}
		}
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));
		$Param['FIL_ID']='';	
		$Param['DATUM_VON']='';	
		$Param['DATUM_BIS']='';	
	}
	elseif(isset($_GET['FLB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['FLB_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));
	}
	elseif(isset($_POST['txtFLB']) and $AWIS_KEY1 != 0)
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		$Param = array();

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDERBY']='';

		//$Param['PEB_KEY']=$Form->Format('N0',$_POST['txtPEB_KEY'],true);
		$Param['FIL_ID']=$_POST['sucFLB_FIL_ID'];	
		$Param['DATUM_VON']=$_POST['sucFLB_DATUM_VON'];	
		$Param['DATUM_BIS']=$_POST['sucFLB_DATUM_BIS'];	
		
		$AWISBenutzer->ParameterSchreiben('Formular_FLB',serialize($Param));
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB'));

		if(!isset($Param['KEY']) or (intval($Param['KEY']) <= 0))
		{	
			$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
			if ($UserFilialen != '')
			{
				$Param['FIL_ID']=$UserFilialen;	
			}

			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDERBY']='';
			$AWISBenutzer->ParameterSchreiben('Formular_FLB',serialize($Param));
		}

		if(isset($_GET['FLBListe']))
		{
			$Param['KEY']=0;
		}
		
		
		$AWIS_KEY1=$Param['KEY'];
	}

	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
		{
			$ORDERBY = $Param['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' FLB_DATUM DESC';
		}		
	}
	else
	{
		$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	
	$SQL = 'Select Flb_Key, Flb_Datum, Flb_Fil_Id, Flb_Bemerkung, Flb_Pruefer, Flb_Pbe_Key, Flb_Pez_Status,FLB_USER,FLB_USERDAT';
	$SQL .= ',PBE_ABKUERZUNG, PBE_BEZEICHNUNG, PBE_ANZEIGE';
	$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM FILIALBERICHTE';
	$SQL .= ' LEFT JOIN PERSONALEINSBERICHTE ON FLB_PBE_KEY = PBE_KEY';
	
	$Bedingung=_BedingungErstellen($Param);

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	$SQL .= ' ORDER BY '.$ORDERBY;

	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_FLB',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	// Sortierung nach dem Ermitteln der Datenanzahl
	$rsPEI = $DB->RecordSetOeffnen($SQL);

	
	$Form->Formular_Start();
	$Form->SchreibeHTMLCode('<form name=frmFilialberichte action=./filialberichte_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

	$Param['KEY']=$AWIS_KEY1;
	$Param['ORDERBY']=$ORDERBY;
	$Param['BLOCK']=$Block;
	$AWISBenutzer->ParameterSchreiben('Formular_FLB',serialize($Param));

	$Form->Erstelle_HiddenFeld('FLB_KEY',$AWIS_KEY1);

	$EditRecht=(($Recht16000&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}
		
	if(isset($_GET['FLB_KEY']) AND $_GET['FLB_KEY'] == '-1' OR $AWIS_KEY1 == '-1')
	{				
		$Form->ZeileStart();
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_FIL_ID'].':',150);
		if($UserFilialen!='')
		{	
			$Form->Erstelle_TextFeld('FLB_FIL_ID',$UserFilialen,20,200,false,'','','','T');
			echo '<input type=hidden name="txtFLB_FIL_ID" value="'.$UserFilialen.'">';
		}
		else 
		{
			$Form->Erstelle_TextFeld('FLB_FIL_ID','',20,200,true);	
		}		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_DATUM'].':',150,'','');
    	$Form->Erstelle_TextFeld('FLB_DATUM','',20,200,True,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
		$Form->ZeileEnde();
    	
		$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_BEMERKUNG'].':',150,'','','');
   	 	$Form->Erstelle_TextFeld('FLB_BEMERKUNG','',60,400,True);
    	$Form->ZeileEnde();
		
    	$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_PRUEFER'].':',150,'','','');
   	 	$Form->Erstelle_TextFeld('FLB_PRUEFER','',20,200,True);
    	$Form->ZeileEnde();
	}
	elseif(isset($_GET['Edit']) OR isset($_GET['FLB_KEY']) > 0)
	{			
		if(isset($_GET['FLB_KEY']) > 0)
		{
			$AWIS_KEY1 = $_GET['FLB_KEY'];	
			$Form->Erstelle_HiddenFeld('Edit', $_GET['FLB_KEY']);
			$SQL = 'Select * from FILIALBERICHTE WHERE FLB_KEY='.$DB->FeldInhaltFormat('NO',$_GET['FLB_KEY']);
		}
		elseif(isset($_GET['Edit']))
		{
			$AWIS_KEY1 = $_GET['Edit'];
			$Form->Erstelle_HiddenFeld('Edit', $_GET['Edit']);
			$SQL = 'Select * from FILIALBERICHTE WHERE FLB_KEY='.$DB->FeldInhaltFormat('NO',$_GET['Edit']);
		}
		
		$rsEdit = $DB->RecordSetOeffnen($SQL);
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialberichte_Main.php?cmdAktion=Details&FLBListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsEdit->FeldInhalt('FLB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsEdit->FeldInhalt('FLB_USERDAT')));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_FIL_ID'].':',150);
		if($UserFilialen!='')
		{	
			$Form->Erstelle_TextFeld('FLB_FIL_ID',$UserFilialen,20,200,false,'','','','T');
			echo '<input type=hidden name="txtFLB_FIL_ID" value="'.$UserFilialen.'">';
		}
		else 
		{
			$Form->Erstelle_TextFeld('FLB_FIL_ID',$rsEdit->FeldInhalt('FLB_FIL_ID'),20,200,true);	
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_DATUM'].':',150,'','');
    	$Form->Erstelle_TextFeld('FLB_DATUM',$rsEdit->FeldInhalt('FLB_DATUM'),20,200,True,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
    	$Form->ZeileEnde();
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_BEMERKUNG'].':',150,'','');
   	 	$Form->Erstelle_TextFeld('FLB_BEMERKUNG',$rsEdit->FeldInhalt('FLB_BEMERKUNG'),60,400,True);
    	$Form->ZeileEnde();
		
    	$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_PRUEFER'].':',150,'','');
   	 	$Form->Erstelle_TextFeld('FLB_PRUEFER',$rsEdit->FeldInhalt('FLB_PRUEFER'),20,200,True);
    	$Form->ZeileEnde();
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['HinweisQSCheck'],700,'font-weight:bold','');
		$Form->ZeileEnde();
    	        	        	
		if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0 AND (floatval($rsPEI->FeldInhalt('FLB_PBE_KEY'))>0))
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Berichte'));
			$SubReg = new awisRegister(16001);
			$SubReg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:'Berichte'));
		}
        	
	}       
	else if(!isset($_GET['FLB_KEY']) OR isset($_GET['FLBListe']) == 1)
	{		
		$EditRecht = $Recht16000&2;
		$Icons = '';
	    $Form->ZeileStart();
        if((intval($Recht16000)&4)!=0)
        {
		   $Icons[] = array('new','./filialberichte_Main.php?cmdAktion=Details&FLB_KEY=-1');
		   $Form->Erstelle_ListeIcons($Icons,38,-1);
        }
		
        $Link = './filialberichte_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=FLB_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FLB_FIL_ID'))?'~':'');			
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_FIL_ID'],70,'',$Link);
		$Link = './filialberichte_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=FLB_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FLB_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_DATUM'],130,'',$Link);
		$Link = './filialberichte_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=FLB_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FLB_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_BEMERKUNG'],200,'',$Link);
		$Link = './filialberichte_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=FLB_PRUEFER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FLB_PRUEFER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_PRUEFER'],180,'',$Link);
		$Form->ZeileEnde();

		$PEIZeile=0;

		while(!$rsPEI->EOF())
		{
			$Form->ZeileStart();
			if(intval($Recht16000&4)>0)	
			{
				$Icons = array();

				//Wenn bereits abgeschlossen, dann kein �ndern bzw. l�schen mehr m�glich
				if ($rsPEI->FeldInhalt('FLB_PEZ_STATUS') != '1')
				{
					$Icons[0] = array('edit','./filialberichte_Main.php?cmdAktion=Details&Edit='.$rsPEI->FeldInhalt('FLB_KEY'));
					$Icons[1] = array('delete','./filialberichte_Main.php?cmdAktion=Details&Del='.$rsPEI->FeldInhalt('FLB_KEY'));
				}
				$Form->Erstelle_ListeIcons($Icons,38,($PEIZeile%2));
			}
	    
            $Form->Erstelle_ListenFeld('FLB_FIL_ID',$rsPEI->FeldInhalt('FLB_FIL_ID'),0,70,false,($PEIZeile%2),'','');
            $Form->Erstelle_ListenFeld('FLB_DATUM',substr($rsPEI->FeldInhalt('FLB_DATUM'),0,10),0,130,false,($PEIZeile%2),'','');
            $Form->Erstelle_ListenFeld('FLB_BEMERKUNG',$rsPEI->FeldInhalt('FLB_BEMERKUNG'),0,200,false,($PEIZeile%2),'','');
            $Form->Erstelle_ListenFeld('FLB_PRUEFER',$rsPEI->FeldInhalt('FLB_PRUEFER'),0,180,false,($PEIZeile%2),'','');
             
            $Link='./filialberichte_Main.php?cmdAktion=Details&FLB_KEY='.$rsPEI->FeldInhalt('FLB_KEY');
				
            $Form->Erstelle_ListenFeld('PBE_ANZEIGE',$rsPEI->FeldInhalt('PBE_ANZEIGE'),0,28,false,$PEIZeile%2,'',$Link,'T','L',$rsPEI->FeldInhalt('PBE_BEZEICHNUNG'));
					
			if(floatval($rsPEI->FeldInhalt('FLB_PBE_KEY'))>0)
			{
				$Icons = array();
				$Icons[] = array('report','./filialberichte_PDF_'.$rsPEI->FeldInhalt('PBE_ABKUERZUNG').'_Blanko.php?PEZ_XXX_KEY=0');
				$Form->Erstelle_ListeIcons($Icons,18,$PEIZeile%2);
						
				if ($rsPEI->FeldInhalt('FLB_PEZ_STATUS')=='1')
				{
					$Icons = array();
					$Icons[] = array('pdf','./filialberichte_PDF_'.$rsPEI->FeldInhalt('PBE_ABKUERZUNG').'_Blanko.php?PEZ_XXX_KEY='.$rsPEI->FeldInhalt('FLB_KEY'));				
					$Form->Erstelle_ListeIcons($Icons,18,$PEIZeile%2);
				}
			}
			
            $Form->ZeileEnde();

            $rsPEI->DSWeiter();
			$PEIZeile++;
		}
		$Link = './filialberichte_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	}

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht16000&6)!=0)		
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND FLB_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['FIL_ID']) AND $Param['FIL_ID']!='')
	{
		$Bedingung .= ' AND FLB_FIL_ID = '.$DB->FeldInhaltFormat('NO',$Param['FIL_ID'],false);
	}
	if(isset($Param['DATUM_VON']) AND $Param['DATUM_VON']!='')
	{
		$Bedingung .= ' AND FLB_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['DATUM_VON'],false);
	}

	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
	{
		$Bedingung .= ' AND FLB_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['DATUM_BIS'],false);
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>