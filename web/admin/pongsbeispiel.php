<?php
// Gantt hour example
//include ("../jpgraph.php");
//include ("../jpgraph_gantt.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_gantt.php");

$con = @OCILogon("awis","IlOuLua","AWIS3");

$db = "";

if (!empty($_GET["db"]))
{
	if (strtolower($_GET["db"]) != "awis3")
	{
		$db = "@" . strtolower($_GET["db"]) . ".atu.de";
	}
}

if($con)
{
//	$sql = "select jou_argument, to_char(jou_start, 'RRMMDD HH24:MI:SS') Anfang, to_char(jou_ende, 'RRRR-MM-DD HH24:MI:SS') Ende, round((jou_ende - jou_start) * 3600 * 24, 0) Dauer from journal ";
	$sql = "select nvl(jou_argument, 'exp_awisgesamt') BEZ, to_char(jou_start, 'RRMMDD HH24:MI:SS') Anfang, to_char(jou_ende, 'RRMMDD HH24:MI:SS') Ende, round((jou_ende - jou_start) * 3600 * 24, 0) Dauer, JOU_FEHLERCODE from journal" . $db . " ";
	$sql .= "where jou_start > sysdate - 1 ";
	$sql .= "and not (jou_argument like 'k%' and jou_funktion = 'Datei-Import') ";
	$sql .= "and round((jou_ende - jou_start) * 3600 * 24, 0) > 1 ";
	$sql .= "order by jou_start";

	$rsDaten = awisOpenRecordset($con, $sql);
	$rsDatenZeilen = $awisRSZeilen;

	awisLogoff($con);			// Wieder abmelden

}

$graph = new GanttGraph();
$graph->SetMarginColor('blue:1.7');
$graph->SetColor('white');
$graph->SetDateRange('2005-02-27 12:00','2005-02-28 12:00');

$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->scale->hour->SetBackgroundColor('lightyellow:1.5');
$graph->scale->hour->SetFont(FF_FONT1);
$graph->scale->day->SetBackgroundColor('lightyellow:1.5');
$graph->scale->day->SetFont(FF_FONT1,FS_BOLD);

$graph->title->Set("Zeitlicher Ablauf der Importe");
$graph->title->SetColor('white');
//$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);

$graph->ShowHeaders(GANTT_HDAY | GANTT_HHOUR);

$graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
$graph->scale->week->SetFont(FF_FONT1);
$graph->scale->hour->SetIntervall(1,5);
$graph->scale->SetDateLocale('de_DE');

$graph->scale->hour->SetStyle(HOURSTYLE_HM24);
$graph->scale->day->SetStyle(DAYSTYLE_SHORTDAYDATE3);


for($DS=0;$DS<$rsDatenZeilen;$DS++)
//for($DS=0;$DS<35;$DS++)
{
//	$bar = new GanttBar($DS," Blafasel " . $DS,"050201 04:00","050202 04:00","[5%]",10);
	$bar = new GanttBar($DS,$rsDaten["BEZ"][$DS],$rsDaten["ANFANG"][$DS],$rsDaten["ENDE"][$DS],$rsDaten["DAUER"][$DS],10);
//	$bar = new GanttBar($DS,$rsDaten["JOU_ARGUMENT"][$DS],$rsDaten["ANFANG"][$DS],$rsDaten["JOU_ENDE"][$DS],"[5%]",10);
	if ($rsDaten["JOU_FEHLERCODE"][$DS] == "0")
	{
//		$bar->SetColor('orange');
		$bar->SetPattern(BAND_SOLID,"green");
//		$bar->SetPattern(BAND_RDIAG,"yellow");
//		$bar->SetFillColor("red");
	}
	else
	{
		$bar->SetPattern(BAND_SOLID,"red");
	}
//	$bar->SetShadow(true,"gray");
	$graph->Add($bar);
}

$graph->Stroke();



?>


