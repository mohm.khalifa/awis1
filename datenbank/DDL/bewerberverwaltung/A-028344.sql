SET DEFINE OFF;

INSERT INTO "AWIS"."BEWERBERSTATUS" (BWS_BEZEICHNUNG, BWS_TEXTKONSERVE, BWS_USER, BWS_USERDAT, BWS_AKTIV, BWS_DATENSATZAKTIV, BWS_STEUER_ID, BWS_ZWISCHENSEITENTYP, BWS_AKTIONSSCRIPT) VALUES ('Absage nach Unterlagensichtung', 'BEW:BEW_STATUS_ABSUNTERLAGE', 'AWIS', TO_DATE('2017-08-31 12:38:58', 'YYYY-MM-DD HH24:MI:SS'), 'A', '0', '1005', '1', 'bewerberverwaltung_Details_Aktion_Absage_Unterlagensichtung.php');
INSERT INTO "AWIS"."BEWERBERSTATUS" (BWS_BEZEICHNUNG, BWS_TEXTKONSERVE, BWS_USER, BWS_USERDAT, BWS_AKTIV, BWS_DATENSATZAKTIV, BWS_STEUER_ID, BWS_ZWISCHENSEITENTYP, BWS_AKTIONSSCRIPT) VALUES ('Absage nach Zweitgespr�ch', 'BEW:BEW_STATUS_ABSVORSTELL2', 'AWIS', TO_DATE('2017-08-31 12:38:58', 'YYYY-MM-DD HH24:MI:SS'), 'A', '0', '1006', '1', 'bewerberverwaltung_Details_Aktion_Absage_VG2.php');

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('BEW_STATUS_ABSUNTERLAGE', 'BEW', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', 'Absage nach Unterlagensichtung', TO_DATE('2017-08-31 12:43:21', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('BEW_STATUS_ABSVORSTELL2', 'BEW', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', 'Absage nach Zweitgespr�ch', TO_DATE('2017-08-31 12:43:21', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('BEW_PDF_ABSAGE_VORSTELL2_TEXT', 'BEW', 'PDF Text', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns bei Ihnen f�r das erneut sehr angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch noch n�her kennen zu lernen und uns von Ihnen einen noch ausf�hrlicheren Eindruck zu verschaffen.

Nachdem wir uns �ber die wenigen interessantesten sehr gut qualifizierten Bewerbungen einen Eindruck machen k�nnten, m�ssen wir nun eine Entscheidung treffen um diese Stelle zu besetzten.

Es f�llt uns nicht leicht sehr gut qualifizierten Bewerbern mitzuteilen, dass wir uns dieses Mal nicht f�r Sie entschieden haben.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. ', TO_DATE('2017-08-31 13:26:35', 'YYYY-MM-DD HH24:MI:SS'));


INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('BEW_PDF_ABSAGE_UNTERLAGE_TEXT', 'BEW', 'PDF Text', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem Unternehmen verbundenen Interesse.

Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte Vakanz sorgf�ltig gepr�ft.

Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen, dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr weiterverfolgen k�nnen.

Wir bedauern Ihnen hier an dieser Stelle keine anderslautende Nachricht �bermitteln zu k�nnen.

F�r Ihre berufliche Neuorientierung w�nschen wir Ihnen alles Gute und viel Erfolg.

Wir w�rden uns freuen, wenn Sie sich bei einer erneuten Gelegenheit noch einmal bei A.T.U. bewerben m�chten. All unsere vakanten Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.', TO_DATE('2017-08-31 13:42:15', 'YYYY-MM-DD HH24:MI:SS'));



UPDATE "AWIS"."TEXTKONSERVEN" SET XTX_TEXT_DE = 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

Wir bedanken uns f�r das angenehme und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften. Wir haben uns gefreut, Sie in diesem Gespr�ch kennen zu lernen und uns von Ihnen einen ausf�hrlichen Eindruck zu verschaffen.

Nach Abw�gung aller Faktoren sind wir zu dem Ergebnis gelangt, dass wir Ihnen f�r diese Stelle leider kein Angebot unterbreiten k�nnen. Sehen Sie dieses Ergebnis nicht als Wertung Ihrer Person oder Qualifikation.

Wir w�nschen Ihnen, dass Sie bald eine Ihren Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben.

All unsere vakanten Stellen finden Sie tagesaktuell auf der Karriereseite unserer Homepage. '
WHERE XTX_KENNUNG = 'BEW_PDF_ABSAGE_VORSTELL_TEXT';


UPDATE "AWIS"."TEXTKONSERVEN" SET XTX_TEXT_DE = 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

wir bedanken uns f�r die �bersendung Ihrer Bewerbung und Ihrem Interesse an einer Mitarbeit in unserem Unternehmen.

Aufgrund der Vielzahl der eingehenden Bewerbungen, m�chten wir Sie um etwas Geduld bitten, da wir Ihre Unterlagen mit geb�hrender Sorgfalt pr�fen. Sobald wir die Pr�fung Ihrer Unterlagen abgeschlossen haben, werden wir uns mit Ihnen in Verbindung setzen.

Sollten Sie in der Zwischenzeit R�ckfragen haben, freuen wir uns �ber Ihren Anruf.'
WHERE XTX_KENNUNG = 'BEW_PDF_EINGANG_TEXT';