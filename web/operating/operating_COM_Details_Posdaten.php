<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
require_once("operating_COM_config.php");

global $COMWebSrv;
global $COMAppSrv;

global $comcon;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Posdaten','OCP_%');
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Posdaten');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_COM_POS_Errlog');
$TextKonserven[]=array('Wort','Mail_SB');
$TextKonserven[]=array('Wort','Mail_WORK');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Posdaten');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Posdaten'].'</title>';
?>
</head>
<body>
<?php


$Recht3112 = awisBenutzerRecht($con,3112);
if($Recht3112==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$comcon = awisLogonComCluster('DE',false);

//$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
//awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Posdaten action=./operating_COM_Main.php?cmdAktion=POS-Daten method=POST>';

echo "<p><center><h1><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Posdaten']."</b></h1></center><p>";

awis_FORM_FormularStart();

awis_FORM_Trennzeile('D');

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_Progress_Uebersicht'],'','width: 100%; margin: 5px 0px 5px 0px; text-align:center; background-color: silver; font-size: large; font-weight: bold;');
awis_FORM_ZeileEnde();

$SQL = "SELECT * FROM EXPGW_EXPORT_PROGRESS_POS ORDER BY JOBTYPE_ID, TRANSAKTIONSID";

$rsPIU=awisOpenRecordset($comcon,$SQL);
$rsPIUZeilen = $awisRSZeilen;

if($rsPIUZeilen>0)
{
	$styleAll='font-size: 85%;';
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBNAME'],120,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],130,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS'],50,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS_BEZEICHNUNG'],150,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ERWARTET'],80,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FREIGABE_PROZ'],100,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITET'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITUNG_PROZ'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FEHLER'],80,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_FEHLER_PROZ'],90,$styleAll);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],90,$styleAll);
	awis_FORM_ZeileEnde();	
	
	for($PIUZeile=0;$PIUZeile<$rsPIUZeilen;$PIUZeile++)
	{
		
		if ($rsPIU['JOBSTATUS'][$PIUZeile]=='E'){
			$styleDetail='background-color: FF0000; font-weight: bold;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='L'){
			$styleDetail='background-color: FFFF00;';	
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='T'){
			$styleDetail='background-color: 00FF00;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='M'){
			$styleDetail='background-color: FF6921;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='X'){
			$styleDetail='background-color: D1EFEE;';
		}
		elseif ($rsPIU['JOBSTATUS'][$PIUZeile]=='V'){
			$styleDetail='background-color: FF3F47;';
		}
		else{
			$styleDetail='';
		}		
		
		$style=$styleAll.$styleDetail;
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['JOBTYPE_ID'][$PIUZeile],0,50,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['DESCRIPTION'][$PIUZeile],0,120,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['TRANSAKTIONSID'][$PIUZeile],0,130,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['JOBSTATUS'][$PIUZeile],0,50,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['STATUS_BEZEICHNUNG'][$PIUZeile],0,150,false,($PIUZeile%2),$style,'','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['CREATED'][$PIUZeile],0,80,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['FREIGABE_PROZ'][$PIUZeile].' ('.$rsPIU['FREIGABE_CNT_FIL'][$PIUZeile].')',0,100,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['LOADED'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['LOAD_PC'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['ERROR'][$PIUZeile],0,80,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['ERROR_PC'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIU['POS_SYSTEM'][$PIUZeile],0,90,false,($PIUZeile%2),$style,'','T');
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();	
}

awis_FORM_Trennzeile('D');

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Bezeichnung'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Anzahl'],100);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],150);
awis_FORM_ZeileEnde();

$SQL = "SELECT COUNT(*) AS ANZAHL, POS_SYSTEM FROM POS_KOPF_LUECKEN GROUP BY POS_SYSTEM";

$rsPLC=awisOpenRecordset($comcon,$SQL);
$rsPLCZeilen = $awisRSZeilen;

if($rsPLCZeilen>0)
{
	for($PLCZeile=0;$PLCZeile<$rsPLCZeilen;$PLCZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Luecken'],0,200,false,($PLCZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPLC['ANZAHL'][$PLCZeile],0,100,false,($PLCZeile%2),'','','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPLC['POS_SYSTEM'][$PLCZeile],0,150,false,($PLCZeile%2),'','','T');
		if(($Recht3112&2)==2 && $PLCZeile==0)
		{
			echo "&nbsp;<input accesskey=l type=image src=/bilder/icon_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_liste']." (ALT+L)' name=cmdLuecken>";
		}
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Luecken'],0,200,false,'','','','T');	
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();
}

if(isset($_POST['cmdLuecken_x']) || isset($_GET['EXEC']))
{
	awis_FORM_FormularStart();
		awis_FORM_ZeileStart();
			awis_FORM_Erstelle_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Luecken']); 
		awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();
	
	/*if (isset($_GET['EXEC']))
	{
		$SQL="BEGIN COM_POS.PROC_INS_POS_VERARBEITUNG('".$_GET['EXEC']."','".$AWISBenutzer->BenutzerName()."'); END;";
		awisExecute($comcon,$SQL);
		
	}
	*/
	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Filiale'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Startzeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Endezeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],150);
	awis_FORM_ZeileEnde();
	
	$SQL="SELECT REGEXP_REPLACE(AA.PKL_ROWID,'[[:punct:]]','') as PKL_ROWID, AA.FILIALE, AA.STARTSTAMP, AA.STOPSTAMP, AA.STAT, AA.POS_SYSTEM, DECODE(BB.FILIALE,NULL,0,1) AS IMPORT_STOP, BB.BEMERKUNG AS IMPORT_STOP_BEMERKUNG FROM POS_KOPF_LUECKEN_STATUS AA, POS_IMPORT_STOP BB WHERE AA.FILIALE=BB.FILIALE(+)";
	$rsPLD=awisOpenRecordset($comcon,$SQL);
	$rsPLDZeilen = $awisRSZeilen;
	
	for($PLDZeile=0;$PLDZeile<$rsPLDZeilen;$PLDZeile++)
	{		
		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id']=$rsPLD['FILIALE'][$PLDZeile];
		
		$SQL="SELECT ERR_KEY, REGEXP_REPLACE(RID,'[[:punct:]]','') AS RID FROM POS_KOPF_LUECKEN AA,";
		$SQL.=" (SELECT ERR_KEY, DELIMITED.GET_NUMBER_WORD(LOG,4,'-') AS FILNR, ERR_DATUM FROM ERR_LOG";
		$SQL.=" WHERE ERR_STATUS<>(SELECT ERR_STAT_KEY FROM ERR_LOG_STATUS WHERE ERR_STAT_BEZEICHNUNG='Geschlossen')";
		$SQL.=" AND LOG LIKE '%TXNDAT%.%.log') BB";
		$SQL.=" WHERE AA.FILIALE=FILNR AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP+0.1";
		$SQL.=" AND AA.FILIALE=:var_N0_fil_id";
		$SQL.=" ORDER BY ERR_KEY DESC";

		$rsPLDErr=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsPLDErrZeilen = $awisRSZeilen;
		
		awis_FORM_ZeileStart();
		$Link='';
		if(($Recht3112&4)==4)
		{
			$Link = "./operating_COM_Main.php?cmdAktion=POS-Daten&EXEC=".$rsPLD['PKL_ROWID'][$PLDZeile]."";
		}
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsPLD['FILIALE'][$PLDZeile],0,100,false,($PLDZeile%2),'',$Link,'Z');
		awis_FORM_Erstelle_ListenFeld('STARTSTAMP',$rsPLD['STARTSTAMP'][$PLDZeile],0,200,false,($PLDZeile%2),'',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('STOPSTAMP',$rsPLD['STOPSTAMP'][$PLDZeile],0,200,false,($PLDZeile%2),'',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('POS_SYSTEM',$rsPLD['POS_SYSTEM'][$PLDZeile],0,150,false,($PLDZeile%2),'',$Link,'T');
		
		if($rsPLD['IMPORT_STOP'][$PLDZeile]!=0)
		{
			echo "&nbsp;<img src=/bilder/icon_warnung.png title='Import-STOP: ".$rsPLD['IMPORT_STOP_BEMERKUNG'][$PLDZeile]."'>";
		}
		
		if($rsPLDErrZeilen>0 && $rsPLDErr['RID'][0]==$rsPLD['PKL_ROWID'][$PLDZeile])
		{
			echo '<a href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=' . $rsPLDErr['ERR_KEY'][0] . ' target="_blank"><img border=0 src=/bilder/icon_exclamation.png title="' . $AWISSprachKonserven['Wort']['lbl_COM_POS_Errlog'] . '"></a>';
		}
		
		if(($Recht3112&4)==4)
		{
			if ($rsPLD['STAT'][$PLDZeile]==0)
			{
				echo "<img src=/bilder/ampel_gelb.png title='".$AWISSprachKonserven['Wort']['Verarbeitung_EXEC']."'></img>";
			}
			elseif ($rsPLD['STAT'][$PLDZeile]==1)
			{
				echo "<img src=/bilder/ampel_gruen.png title='".$AWISSprachKonserven['Wort']['Verarbeitung_OK']."'></img>";
			}
			elseif ($rsPLD['STAT'][$PLDZeile]==2)
			{
				echo "<img src=/bilder/ampel_rot.png title='".$AWISSprachKonserven['Wort']['Verarbeitung_ERROR']."'></img>";
			}
			//echo "&nbsp;<input accesskey=l type=image src=/bilder/sc_rakete.png title='".$AWISSprachKonserven['Wort']['lbl_ausfuehren']."' value='".$rsPLD['FILIALE'][$PLDZeile]."' name=cmdLuecken>";
		}
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}

awis_FORM_Trennzeile();

$SQL = "SELECT COUNT(*) AS ANZAHL, POS_SYSTEM FROM POS_KOPF_FEHLENDE GROUP BY POS_SYSTEM";

$rsPFC=awisOpenRecordset($comcon,$SQL);
$rsPFCZeilen = $awisRSZeilen;

if($rsPFCZeilen>0)
{
	for($PFCZeile=0;$PFCZeile<$rsPFCZeilen;$PFCZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend'],0,200,false,($PFCZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPFC['ANZAHL'][$PFCZeile],0,100,false,($PFCZeile%2),'','','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPFC['POS_SYSTEM'][$PFCZeile],0,150,false,($PFCZeile%2),'','','T');
		if(($Recht3112&2)==2 && $PFCZeile==0)
		{
			echo "&nbsp;<input accesskey=f type=image src=/bilder/icon_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_liste']." (ALT+F)' name=cmdFehlend>";
		}
	
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend'],0,200,false,'','','','T');
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();
}

if(isset($_POST['cmdFehlend_x'])||isset($_REQUEST['PFD_EXEC']))
{
	awis_FORM_FormularStart();
		awis_FORM_ZeileStart();
			awis_FORM_Erstelle_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend']); 
		awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();
	
	if (isset($_REQUEST['PFD_EXEC']) && $_REQUEST['PFD_EXEC']!='')
	{
		$rsMail="";
		$SQL="SELECT FILIALE, TO_CHAR(STARTSTAMP,'DD.MM.YY HH24:MI:SS') as STARTSTAMP, TO_CHAR(STOPSTAMP,'DD.MM.YY HH24:MI:SS') AS STOPSTAMP, DIFF_TAGE, POS_SYSTEM FROM POS_KOPF_FEHLENDE WHERE EXEC_HASH='".$_REQUEST['PFD_EXEC']."'";
		$rsMail=awisOpenRecordset($comcon,$SQL);
		$rsMailZeilen = $awisRSZeilen;
		
		//awis_Debug(1,$SQL);
		
		$MAIL_SUBJECT="WARNING Fehlende POS-Verarbeitung (".$rsMail['POS_SYSTEM'][0].")";
		$MAIL_TEXT="Hallo,\r\n";
		$MAIL_TEXT.="folgende POS-Verarbeitung (System: ".$rsMail['POS_SYSTEM'][0].") wurde noch nicht geliefert:\r\n";
		$MAIL_TEXT.="Filiale: ".$rsMail['FILIALE'][0]."\r\n"; 
		$MAIL_TEXT.="Kein Import seit: ".$rsMail['STARTSTAMP'][0]."\r\n";  
		//$MAIL_TEXT.="Start: ".$rsMail['STARTSTAMP'][0]."\r\n";  
		//$MAIL_TEXT.="Ende: ".$rsMail['STOPSTAMP'][0]."\r\n";
		$MAIL_TEXT.="Gruss\r\n";
		$MAIL_TEXT.="COM-Operating";

		if($rsMail['POS_SYSTEM'][0]=='FILSYS')
		{
			$MAIL_VT="filsys_ekat@de.atu.eu";
			$MAIL_VT_CC="atu-edv-k11-pc-db@de.atu.eu";
		}
		elseif ($rsMail['POS_SYSTEM'][0]=='AXAPTA')
		{
			$MAIL_VT="desktop-sst-operating@de.atu.eu";
			$MAIL_VT_CC="atu-edv-k11-pc-db@de.atu.eu";
		}
		elseif ($rsMail['POS_SYSTEM'][0]=='WWS')
		{
			$MAIL_VT="5666-EDV-Helpdesk-VAX@de.atu.eu";
			$MAIL_VT_CC="atu-edv-k11-pc-db@de.atu.eu";
		}
		else
		{
            $MAIL_VT="atu-edv-k11-pc-db@de.atu.eu";
            $MAIL_VT_CC="";
		}
		
		$SQL="BEGIN COM_MAIL.PROC_SEND_MAIL('".$MAIL_TEXT."','".$MAIL_VT."','".$MAIL_SUBJECT."','".$MAIL_VT_CC."'); END;";
		awisExecute($comcon,$SQL);
		
		$SQL="INSERT INTO POS_EMAIL_FEHLEND (EXEC_HASH) VALUES ('".$_REQUEST['PFD_EXEC']."')";
		awisExecute($comcon,$SQL);
	}
	
	$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
	awisExecute($comcon,$SQL);
	
	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Filiale'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Startzeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Endezeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Diff_Tage'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift('',34);
	awis_FORM_ZeileEnde();
	
	$SQL="SELECT AA.EXEC_HASH, AA.FILIALE, AA.STARTSTAMP, AA.STOPSTAMP, AA.DIFF_TAGE, NVL(BB.EXEC_HASH,0) AS EMAIL, POS_SYSTEM, DECODE(CC.FILIALE,NULL,0,1) AS IMPORT_STOP, CC.BEMERKUNG AS IMPORT_STOP_BEMERKUNG FROM POS_KOPF_FEHLENDE AA, POS_EMAIL_FEHLEND BB, POS_IMPORT_STOP CC WHERE AA.EXEC_HASH=BB.EXEC_HASH(+) AND AA.FILIALE=CC.FILIALE(+)";
	$rsPFD=awisOpenRecordset($comcon,$SQL);
	$rsPFDZeilen = $awisRSZeilen;
	
	for($PFDZeile=0;$PFDZeile<$rsPFDZeilen;$PFDZeile++)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id']=$rsPFD['FILIALE'][$PFDZeile];
		
		$SQL="SELECT ERR_KEY, EXEC_HASH FROM POS_KOPF_FEHLENDE AA,";
		$SQL.=" (SELECT ERR_KEY, DELIMITED.GET_NUMBER_WORD(LOG,4,'-') AS FILNR, ERR_DATUM FROM ERR_LOG";
		$SQL.=" WHERE ERR_STATUS<>(SELECT ERR_STAT_KEY FROM ERR_LOG_STATUS WHERE ERR_STAT_BEZEICHNUNG='Geschlossen')";
		$SQL.=" AND LOG LIKE '%TXNDAT%.%.log') BB";
		$SQL.=" WHERE AA.FILIALE=FILNR AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP";
		$SQL.=" AND AA.FILIALE=:var_N0_fil_id";
		$SQL.=" ORDER BY ERR_KEY DESC";
		
		$rsPFDErr=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsPFDErrZeilen = $awisRSZeilen;
		
		awis_FORM_ZeileStart();
		$FIL_LINK='"https://'.$_SERVER["SERVER_NAME"].'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsPFD['FILIALE'][$PFDZeile].'" target="_blank"';
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsPFD['FILIALE'][$PFDZeile],0,100,false,($PFDZeile%2),'',$FIL_LINK,'Z');
		awis_FORM_Erstelle_ListenFeld('STARTSTAMP',$rsPFD['STARTSTAMP'][$PFDZeile],0,200,false,($PFDZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('STOPSTAMP',$rsPFD['STOPSTAMP'][$PFDZeile],0,200,false,($PFDZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('DIFF_TAGE',$rsPFD['DIFF_TAGE'][$PFDZeile],0,150,false,($PFDZeile%2),'color: red;','','T');
		awis_FORM_Erstelle_ListenFeld('POS_SYSTEM',$rsPFD['POS_SYSTEM'][$PFDZeile],0,150,false,($PFDZeile%2),'','','T');
		
		if($rsPFD['IMPORT_STOP'][$PFDZeile]!=0)
		{
			echo "&nbsp;<img src=/bilder/icon_warnung.png title='Import-STOP: ".$rsPFD['IMPORT_STOP_BEMERKUNG'][$PFDZeile]."'>";
		}
		
		if($rsPFDErrZeilen>0 && $rsPFDErr['EXEC_HASH'][0]==$rsPFD['EXEC_HASH'][$PFDZeile])
		{
			echo '<a href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=' . $rsPFDErr['ERR_KEY'][0] . ' target="_blank"><img border=0 src=/bilder/icon_exclamation.png title="' . $AWISSprachKonserven['Wort']['lbl_COM_POS_Errlog'] . '"></a>';
		}
		
		if(($Recht3112&8)==8)
		{
			if ($rsPFD['EMAIL'][$PFDZeile]!='0'){
				echo "&nbsp;<img src=/bilder/icon_lkw.png title='".$AWISSprachKonserven['Wort']['Mail_WORK']."'>";
			}
			else{
//				echo "&nbsp;<input accesskey=l type=image src=/bilder/email.png title='".$AWISSprachKonserven['Wort']['Mail_SB']."' name=cmdEMail>";
				echo "&nbsp;<a href=./operating_COM_Main.php?cmdAktion=POS-Daten&PFD_EXEC=" . $rsPFD['EXEC_HASH'][$PFDZeile] . "><img border=0 src=/bilder/icon_mail.png name=cmdAnzeige_Fehlende title='" . $AWISSprachKonserven['Wort']['Mail_SB'] . "'></a>";
			}
			echo "<input type=hidden name='txtEMAIL_ID' value='".$rsPFD['EXEC_HASH'][$PFDZeile]."'>";
			echo "<input type=hidden name='txtAnzeige_Fehlende' value='TRUE'>";
		}
		else
		{
			if ($rsPFD['EMAIL'][$PFDZeile]!=0){
				echo "&nbsp;<img src=/bilder/baustelle.png title='".$AWISSprachKonserven['Wort']['Mail_SB']."'>";
			}
		}
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}

if(($Recht3112&4)==4)
{

awis_FORM_Trennzeile();

$SQL = "SELECT COUNT(*) AS ANZAHL, POS_SYSTEM FROM POS_KOPF_FEHLENDE_SMALL_DIFF GROUP BY POS_SYSTEM";

$rsPFS=awisOpenRecordset($comcon,$SQL);
$rsPFSZeilen = $awisRSZeilen;

if($rsPFSZeilen>0)
{
	for($PFSZeile=0;$PFSZeile<$rsPFSZeilen;$PFSZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend_Small_Diff'],0,200,false,($PFSZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPFS['ANZAHL'][$PFSZeile],0,100,false,($PFSZeile%2),'','','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPFS['POS_SYSTEM'][$PFSZeile],0,150,false,($PFSZeile%2),'','','T');
		if(($Recht3112&2)==2 && $PFSZeile==0)
		{
			echo "&nbsp;<input accesskey=f type=image src=/bilder/icon_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_liste']." (ALT+F)' name=cmdFehlendSmall>";
		}
	
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend_Small_Diff'],0,200,false,'','','','T');
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();
}

if(isset($_POST['cmdFehlendSmall_x']))
{
	awis_FORM_FormularStart();
		awis_FORM_ZeileStart();
			awis_FORM_Erstelle_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Fehlend_Small_Diff']); 
		awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();
	
	$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
	awisExecute($comcon,$SQL);
	
	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Filiale'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Startzeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Endezeit'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Diff_Tage'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift('',34);
	awis_FORM_ZeileEnde();
	
	$SQL="SELECT AA.EXEC_HASH, AA.FILIALE, AA.STARTSTAMP, AA.STOPSTAMP, AA.DIFF_TAGE, NVL(BB.EXEC_HASH,0) AS EMAIL, POS_SYSTEM FROM POS_KOPF_FEHLENDE_SMALL_DIFF AA, POS_EMAIL_FEHLEND BB WHERE AA.EXEC_HASH=BB.EXEC_HASH(+)";
	$rsPFSD=awisOpenRecordset($comcon,$SQL);
	$rsPFSDZeilen = $awisRSZeilen;
	
	for($PFSDZeile=0;$PFSDZeile<$rsPFSDZeilen;$PFSDZeile++)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id']=$rsPFSD['FILIALE'][$PFSDZeile];
		
		$SQL="SELECT ERR_KEY, EXEC_HASH FROM POS_KOPF_FEHLENDE AA,";
		$SQL.=" (SELECT ERR_KEY, DELIMITED.GET_NUMBER_WORD(LOG,4,'-') AS FILNR, ERR_DATUM FROM ERR_LOG";
		$SQL.=" WHERE ERR_STATUS<>(SELECT ERR_STAT_KEY FROM ERR_LOG_STATUS WHERE ERR_STAT_BEZEICHNUNG='Geschlossen')";
		$SQL.=" AND LOG LIKE '%TXNDAT%.%.log') BB";
		$SQL.=" WHERE AA.FILIALE=FILNR AND BB.ERR_DATUM BETWEEN AA.STARTSTAMP AND AA.STOPSTAMP";
		$SQL.=" AND AA.FILIALE=:var_N0_fil_id";
		$SQL.=" ORDER BY ERR_KEY DESC";
		
		$rsPFSDErr=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsPFSDErrZeilen = $awisRSZeilen;
		
		awis_FORM_ZeileStart();
		$FIL_LINK='"https://'.$_SERVER["SERVER_NAME"].'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsPFSD['FILIALE'][$PFSDZeile].'" target="_blank"';
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsPFSD['FILIALE'][$PFSDZeile],0,100,false,($PFSDZeile%2),'',$FIL_LINK,'Z');
		awis_FORM_Erstelle_ListenFeld('STARTSTAMP',$rsPFSD['STARTSTAMP'][$PFSDZeile],0,200,false,($PFSDZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('STOPSTAMP',$rsPFSD['STOPSTAMP'][$PFSDZeile],0,200,false,($PFSDZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('DIFF_TAGE',$rsPFSD['DIFF_TAGE'][$PFSDZeile],0,150,false,($PFSDZeile%2),'color: red;','','T');
		awis_FORM_Erstelle_ListenFeld('POS_SYSTEM',$rsPFSD['POS_SYSTEM'][$PFSDZeile],0,150,false,($PFSDZeile%2),'','','T');
		
		if($rsPFSDErrZeilen>0 && $rsPFSDErr['EXEC_HASH'][0]==$rsPFSD['EXEC_HASH'][$PFSDZeile])
		{
			echo '<a href=./operating_COM_Main.php?cmdAktion=ERROR-Log&Edit=' . $rsPFSDErr['ERR_KEY'][0] . ' target="_blank"><img border=0 src=/bilder/icon_exclamation.png title="' . $AWISSprachKonserven['Wort']['lbl_COM_POS_Errlog'] . '"></a>';
		}
		
/*		if(($Recht3112&8)==8)
		{
			if ($rsPFD['EMAIL'][$PFDZeile]!='0'){
				echo "&nbsp;<img src=/bilder/icon_lkw.png title='".$AWISSprachKonserven['Wort']['Mail_WORK']."'>";
			}
			else{
//				echo "&nbsp;<input accesskey=l type=image src=/bilder/email.png title='".$AWISSprachKonserven['Wort']['Mail_SB']."' name=cmdEMail>";
				echo "&nbsp;<a href=./operating_COM_Main.php?cmdAktion=POS-Daten&PFD_EXEC=" . $rsPFD['EXEC_HASH'][$PFDZeile] . "><img border=0 src=/bilder/icon_mail.png name=cmdAnzeige_Fehlende title='" . $AWISSprachKonserven['Wort']['Mail_SB'] . "'></a>";
			}
			echo "<input type=hidden name='txtEMAIL_ID' value='".$rsPFD['EXEC_HASH'][$PFDZeile]."'>";
			echo "<input type=hidden name='txtAnzeige_Fehlende' value='TRUE'>";
		}
		else
		{
			if ($rsPFD['EMAIL'][$PFDZeile]!=0){
				echo "&nbsp;<img src=/bilder/baustelle.png title='".$AWISSprachKonserven['Wort']['Mail_SB']."'>";
			}
		}*/
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}

}

awis_FORM_Trennzeile();

$SQL = "SELECT COUNT(*) AS ANZAHL, POS_SYSTEM FROM POS_IMPORT_OHNE_FREIGABE GROUP BY POS_SYSTEM";

$rsPIC=awisOpenRecordset($comcon,$SQL);
$rsPICZeilen = $awisRSZeilen;

if($rsPICZeilen>0)
{
	for($PICZeile=0;$PICZeile<$rsPICZeilen;$PICZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POSohneFreigabe'],0,200,false,($PICZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('',$rsPIC['ANZAHL'][$PICZeile],0,100,false,($PICZeile%2),'','','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIC['POS_SYSTEM'][$PICZeile],0,150,false,($PICZeile%2),'','','T');
		if(($Recht3112&2)==2 && $PICZeile==0)
		{
			echo "&nbsp;<input accesskey=f type=image src=/bilder/icon_lupe.png title='".$AWISSprachKonserven['Wort']['lbl_liste']." (ALT+F)' name=cmdImport>";
		}
	
		awis_FORM_ZeileEnde();
	}
}
else{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('',$AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POSohneFreigabe'],0,200,false,'','','','T');
	awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
	awis_FORM_ZeileEnde();	
}

if(isset($_POST['cmdImport_x']))
{
	awis_FORM_FormularStart();
		awis_FORM_ZeileStart();
			awis_FORM_Erstelle_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Import']); 
		awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();
	
	$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
	awisExecute($comcon,$SQL);
	
	$SQL="SELECT FIL_ID, IMPORT_AM, TRANSAKTIONSID, POS_SYSTEM ";
	$SQL.="FROM POS_IMPORT_OHNE_FREIGABE order by 1,3";
	$rsPIF=awisOpenRecordset($comcon,$SQL);
	$rsPIFZeilen=$awisRSZeilen;

	if ($rsPIFZeilen>0)
	{
		awis_FORM_FormularStart();
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Filiale'],100);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_Import_Am'],200);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_TID'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Posdaten']['OCP_POS_SYSTEM'],150);
		awis_FORM_ZeileEnde();
	
		for($PIFZeile=0;$PIFZeile<$rsPIFZeilen;$PIFZeile++)
		{
			awis_FORM_ZeileStart();
			$FIL_LINK='"https://'.$_SERVER["SERVER_NAME"].'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsPIF['FIL_ID'][$PIFZeile].'" target="_blank"';
			awis_FORM_Erstelle_ListenFeld('FILIALE',$rsPIF['FIL_ID'][$PIFZeile],0,100,false,($PIFZeile%2),'',$FIL_LINK,'Z');
			awis_FORM_Erstelle_ListenFeld('IMPORT_AM',$rsPIF['IMPORT_AM'][$PIFZeile],0,200,false,($PIFZeile%2),'','','DU');	
			awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsPIF['TRANSAKTIONSID'][$PIFZeile],0,150,false,($PIFZeile%2),'','','Z');
			awis_FORM_Erstelle_ListenFeld('POS_SYSTEM',$rsPIF['POS_SYSTEM'][$PIFZeile],0,150,false,($PIFZeile%2),'','','T');
			awis_FORM_ZeileEnde();
		}
	
		awis_FORM_FormularEnde();
	}
	else{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_FormularEnde();
	}
	
}

awis_FORM_FormularEnde();

awis_FORM_Trennzeile('D');

$SQL = "SELECT FILIALE, BEMERKUNG FROM POS_IMPORT_STOP ORDER BY FILIALE";

$rsPIS=awisOpenRecordset($comcon,$SQL);
$rsPISZeilen = $awisRSZeilen;

if($rsPISZeilen>0)
{
	awis_FORM_FormularStart();
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('Angehaltene A5 / POS-Importe',300);
	awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();

	awis_FORM_FormularStart();
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('Filiale',50);
	awis_FORM_Erstelle_Liste_Ueberschrift('Bemerkung',750);
	awis_FORM_ZeileEnde();
	
	$style='font-weight: bold;';
	
	for($PISZeile=0;$PISZeile<$rsPISZeilen;$PISZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('',$rsPIS['FILIALE'][$PISZeile],0,50,false,($PISZeile%2),$style,'','Z');
		awis_FORM_Erstelle_ListenFeld('',$rsPIS['BEMERKUNG'][$PISZeile],0,750,false,($PISZeile%2),$style,'','T');		
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
	
	awis_FORM_Trennzeile('D');
}

awis_FORM_FormularStart();
	awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('Offene A5/POS-Dateien auf COM-APP-Server ('.$COMAppSrv.')','','width: 100%; background-color: transparent;','"'.$COMWebSrv.'/schnelldiagnose/showfile_open_impdir.php?TYP=POS" target="_blank"');
	awis_FORM_ZeileEnde();
	awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('Offene T3/POS_VAL-Dateien auf COM-APP-Server ('.$COMAppSrv.')','','width: 100%; background-color: transparent;','"'.$COMWebSrv.'/schnelldiagnose/showfile_open_impdir.php?TYP=POS_VAL" target="_blank"');
	awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


if(isset($_POST['cmdLoeschen_x']))
{
	include('./operating_COM_Errlog_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./operating_COM_Errlog_speichern.php');
}

echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

