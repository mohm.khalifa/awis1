<?php
/**
 * Suchmaske f�r die Auswahl eines Versicherungsvorgangs
 *
 * @author    Stefan Oppl
 * @copyright ATU
 * @version   20090505
 *
 *
 */
global $AWISCursorPosition;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();

    $TextKonserven = array();
    $TextKonserven[] = array('VVS', '*');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'NurFehlerhafte');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9100 = $AWISBenutzer->HatDasRecht(9500);        // Rechte des Mitarbeiters

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=versicherungen_Main.php?cmdAktion=Details>");

    $Param = unserialize($AWISBenutzer->ParameterLesen('VVSSuche'));

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_FILID'] . ':', 240);
    $Form->Erstelle_TextFeld('*VVS_FILID', ($Param['SPEICHERN'] == 'on'?$Param['VVS_FILID']:''), 4, 200, true, '', '', '', 'T', '', $AWISSprachKonserven['VVS']['VVS_FILID'], '', 50);
    $AWISCursorPosition = 'sucVVS_FILID';
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_WANR'] . ':', 240);
    $Form->Erstelle_TextFeld('*VVS_WANR', ($Param['SPEICHERN'] == 'on'?$Param['VVS_WANR']:''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['VVS']['VVS_WANR'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_KFZKENNZ'] . ':', 240);
    $Form->Erstelle_TextFeld('*VVS_KFZKENNZ', ($Param['SPEICHERN'] == 'on'?$Param['VVS_KFZKENNZ']:''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['VVS']['VVS_KFZKENNZ'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_VORGANGNR'] . ':', 240);
    $Form->Erstelle_TextFeld('*VVS_VORGANGNR', ($Param['SPEICHERN'] == 'on'?$Param['VVS_VORGANGNR']:''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['VVS']['VVS_VORGANGNR'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_SCHADENKZ'] . ':', 240);
    $Form->Erstelle_TextFeld('*VVS_SCHADENKZ', ($Param['SPEICHERN'] == 'on'?$Param['VVS_SCHADENKZ']:''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['VVS']['VVS_SCHADENKZ'], '', 50);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    $Form->ZeileStart('display:block', 'FEHLERSTATUS');
    $Form->Erstelle_TextLabel('Fehlerhafte Vorg�nge' . ':', 240);
    $SQL = 'SELECT DISTINCT ven.VEN_KEY, ven.VEN_BEMERKUNG';
    $SQL .= ' FROM VERSVORGANGSSTATUS vvs';
    $SQL .= ' INNER JOIN VERSCHECKSTATUSNUMMERN vcn';
    $SQL .= ' ON vvs.vvs_vcn_key = vcn.VCN_KEY';
    $SQL .= ' INNER JOIN VERSSTATUSNUMMERN ven';
    $SQL .= ' ON vvs.vvs_ven_key      = ven.ven_key';
    $SQL .= ' WHERE vcn.VCN_KATEGORIE = \'BEARBEITUNG\' AND ven.VEN_KEY <> 0';
    $SQL .= ' ORDER BY VEN_KEY';
    
    $Zusatzoptionen = '-99~Keine Fehlerhaften|-1~Alle Fehlerhaften';
    $Zusatzoptionen = explode('|', $Zusatzoptionen);
    $Form->Erstelle_SelectFeld('*FEHLERSTATUS', ($Param['SPEICHERN'] == 'on'?$Param['FEHLERSTATUS']:''), '300:290', true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Zusatzoptionen);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 240);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 20, true, 'on');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905061503");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>