<?php
//global $AWIS_KEY1;
//global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','UngueltigerArtikel');
$TextKonserven[]=array('Meldung','Voraussetzung%');
$LoeschDelta=array();

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
	$Werkzeuge = new awisWerkzeuge();
	
	$AWISSprachKonservenSpeichern = $Form->LadeTexte($TextKonserven);	
	$Recht13000 = $AWISBenutzer->HatDasRecht(13000);
	if($Recht13000==0)
	{
	    awisEreignis(3,1000,'KundenKfzZuordnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonservenSpeichern['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonservenSpeichern['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	if (($Recht13000&2)!=0)
	{
		$RemoteDB='';					
		$RemoteConn='';
		
		if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
		{
			$RemoteDB='@COM_DE.ATU.DE';
			$RemoteConn='COM_DE';
		}
		else 
		{
			$RemoteDB='@COM_TEST_DE.ATU.DE';
			$RemoteConn='COM_TEST_DE';
		}	
		
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtKKZ_VERTRAGSNUMMER_',1,1));
		if(count($Felder)>0 AND $Felder[0]!='')
		{		
			foreach($Felder AS $Feld)
			{
				//$Form->DebugAusgabe(1,$Feld);
				
				$FeldTeile = explode('_',$Feld);
					
				$SQL = 'SELECT KKZ_VERTRAGSNUMMER, KKZ_LIEFERANT, KKZ_REFERENZFELD ';
				$SQL .= ' FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG'.$RemoteDB.' k';
				$SQL .= ' WHERE KKZ_KEY = '.$DB->FeldInhaltFormat('Z',$FeldTeile[2]);
				
				$rsKKZ = $DB->RecordSetOeffnen($SQL);			
				if(!$rsKKZ->EOF())
				{
					$FeldTeile[3]=$rsKKZ->FeldInhalt('KKZ_REFERENZFELD');
					$FeldTeile[4]=$rsKKZ->FeldInhalt('KKZ_LIEFERANT');
				}
				
				$Speichern = false;
	
				$FeldName = 'KKZ_VERTRAGSNUMMER';
				$WertNeu=$DB->FeldInhaltFormat($rsKKZ->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsKKZ->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr($Feld,3)],true);
				if($WertAlt!=$WertNeu)
				{
					$Speichern = true;
				}
	
				//$Form->DebugAusgabe(1,$FeldTeile[2],$FeldTeile[3],$WertNeu,$WertAlt,$SQL);
				
				$SQL = '';
	
				if($Speichern)
				{
					$SQL = 'UPDATE EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG'.$RemoteDB.' SET ';
					
					$SQL .= ' KKZ_VERTRAGSNUMMER = '.$DB->FeldInhaltFormat('Z',$_POST['txtKKZ_VERTRAGSNUMMER_'.$FeldTeile[2]],true);			
					if ($DB->FeldInhaltFormat('Z',$_POST['txtKKZ_VERTRAGSNUMMER_'.$FeldTeile[2]],true) != 'null')
					{
						$SQL .= ', KKZ_STATUS = 1';
						$LoeschDelta[$DB->FeldInhaltFormat('Z',$FeldTeile[2])]=0;
					}
					else 
					{
						$SQL .= ', KKZ_STATUS = 0';
						$LoeschDelta[$DB->FeldInhaltFormat('Z',$FeldTeile[2])]=1;
					}				
					$SQL .= ', KKZ_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', KKZ_USERDAT = sysdate';
					$SQL .= ' WHERE KKZ_KEY = '.$DB->FeldInhaltFormat('Z',$FeldTeile[2]);				
				}
				
				if($SQL!='')
				{
					//$Form->DebugAusgabe(1,$SQL);
					
					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),201010181339,$SQL,2);
					}
				}
				
				if(isset($LoeschDelta[$DB->FeldInhaltFormat('Z',$FeldTeile[2])]) && $LoeschDelta[$DB->FeldInhaltFormat('Z',$FeldTeile[2])]===1)
				{
					if (($Recht13000&4)!=0)
					{
						$COMDB = awisDatenbank::NeueVerbindung($RemoteConn);
						$COMDB->Oeffnen();
					
						$COMDB->TransaktionBegin();
						
						$SQL="BEGIN EXPERIAN_ATU.P_KUNDEN_KFZ.PROC_CREA_DEL_DELTA_KUNDENKFZ(".$COMDB->FeldInhaltFormat('Z',$WertAlt,false)."); COMMIT; END;";
						
						//$Form->DebugAusgabe(1,$SQL);
						
						if($COMDB->Ausfuehren($SQL)===false)
						{
							$COMDB->TransaktionRollback();
							throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),201010181339,$SQL,2);
						}
						$Form->Formular_Start();
							$Form->Hinweistext('Fuer die aktiven Kunden-KFZ der Vertragsnummer:'.$COMDB->FeldInhaltFormat('Z',$WertAlt,false).' wurde ein LoeschDelta erstellt.',1,'');
						$Form->Formular_Ende();
						$COMDB->TransaktionCommit();
					
						$COMDB->Schliessen();
					}
					else{
						$Form->Formular_Start();
							$Form->Hinweistext('Fehler bei Erstellung des LoeschDelta der Kunden-KFZ mit Vertragsnummer:'.$DB->FeldInhaltFormat('Z',$WertAlt,false).'. <p> '.$AWISSprachKonservenSpeichern['Fehler']['err_keineRechte'],1,'');
						$Form->Formular_Ende();
					}	
				}
			}
		}
		
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtKKC_VERTRAGSNUMMER_',1,1));
		if(count($Felder)>0 AND $Felder[0]!='')
		{		
			foreach($Felder AS $Feld)
			{
				//$Form->DebugAusgabe(1,$Feld);
				
				$FeldTeile = explode('_',$Feld);

				// Erstelle Clone-Zuordnung bzw. Clone-Loeschdelta
				$FeldName = 'KKC_VERTRAGSNUMMER';
				$WertRefKey=$DB->FeldInhaltFormat('Z',$FeldTeile[2]);
				$WertNeu=$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)],true);
								
				if($WertAlt!=$WertNeu)
				{
					$COMDB = awisDatenbank::NeueVerbindung($RemoteConn);
					$COMDB->Oeffnen();
					
					$COMDB->TransaktionBegin();
					
					$SQL="BEGIN EXPERIAN_ATU.P_KUNDEN_KFZ.PROC_CREA_KUNDENKFZ_CLONE(".$WertNeu.",".$WertAlt.",".$WertRefKey."); COMMIT; END;";
					
					
					if($COMDB->Ausfuehren($SQL)===false)
					{
						$COMDB->TransaktionRollback();
						throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),20131202134000,$SQL,2);
					}
					
					$Form->Formular_Start();
					$Form->Hinweistext('Setze Clone-Zuordnung f�r KKZ_KEY: '.$WertRefKey.' auf Wert:'.$WertNeu,1,'');
					$Form->Formular_Ende();
					
					$COMDB->TransaktionCommit();
					
					$COMDB->Schliessen();
				}				
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>