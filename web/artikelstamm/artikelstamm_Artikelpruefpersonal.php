<?php
/**
 * Bereichsverwaltung f�r die Personaleins�tze
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   20090122
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('APP', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht456 = $AWISBenutzer->HatDasRecht(456);
    if ($Recht456 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_APP'));

    //********************************************************
    // Parameter verarbeiten
    //********************************************************
    if (isset($_POST['cmdDSZurueck_x'])) {
        $Bedingung = '';
        $SQL = 'SELECT APP_ID FROM (SELECT APP_ID ';
        $SQL .= ' FROM Artikelpruefpersonal';
        $SQL .= ' WHERE APP_ID < ' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_ID']);
        if (($Recht456 & 16) == 0)        // Nur den eigenen Bereich anzeigen
        {
            $Bedingung .= ' AND (PBM_XBN_KEY = 0' . $AWISBenutzer->BenutzerID() . ')';
            $Bedingung .= ' AND PBR_PBZ_KEY = 6)';
        }
        $SQL .= ' ORDER BY APP_ID DESC';
        $SQL .= ') WHERE ROWNUM = 1';
        $rsAPP = $DB->RecordSetOeffnen($SQL);
        if (!$rsAPP->EOF()) {
            $AWIS_KEY1 = $rsAPP->FeldInhalt('APP_ID');
        } else {
            $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID']);
        }
    } elseif (isset($_POST['cmdDSWeiter_x'])) {
        $Bedingung = '';
        $SQL = 'SELECT APP_ID FROM (SELECT APP_ID ';
        $SQL .= ' FROM Artikelpruefpersonal';
        $SQL .= ' WHERE APP_ID > ' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_ID']);
        if (($Recht456 & 16) == 0)        // Nur den eigenen Bereich anzeigen
        {
            $Bedingung .= ' AND (PBM_XBN_KEY = 0' . $AWISBenutzer->BenutzerID() . ')';
        }
        $SQL .= ' ORDER BY APP_ID ASC';
        $SQL .= ') WHERE ROWNUM = 1';
        $rsAPP = $DB->RecordSetOeffnen($SQL);
        if (!$rsAPP->EOF()) {
            $AWIS_KEY1 = $rsAPP->FeldInhalt('APP_ID');
        } else {
            $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID']);
        }
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./artikelstamm_loeschen.php');
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./artikelstamm_speichern.php');
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
    } elseif (isset($_GET['APP_ID'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['APP_ID']);
    } elseif (isset($_POST['txtAPP_ID'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID']);
    } else {
        if (!isset($Param['KEY'])) {
            $Param = array();
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
            $Param['BLOCK'] = 1;
            $AWISBenutzer->ParameterSchreiben('Formular_APP', serialize($Param));
        }

        $AWIS_KEY1 = $Param['KEY'];
        if (isset($_GET['APPListe']) OR isset($_REQUEST['Block'])) {
            $Param['KEY'] = '';
            $AWIS_KEY1 = '';
        }
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    if (!isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY APP_NAME';
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    $SQL = 'SELECT DISTINCT Artikelpruefpersonal.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM Artikelpruefpersonal';

    $Bedingung = '';

    if ($AWIS_KEY1 != 0) {
        $Bedingung .= ' AND APP_ID = ' . floatval($AWIS_KEY1);
    } elseif ($Param['KEY']) {
        $Bedingung .= ' AND APP_ID = ' . $Param['KEY'];
    }

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $SQL .= $ORDERBY;

    if ($AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $AWISBenutzer->ParameterSchreiben('Formular_APP', serialize($Param), $AWISBenutzer->BenutzerID());

    $rsAPP = $DB->RecordSetOeffnen($SQL);

    if ($rsAPP->AnzahlDatensaetze() > 1 AND $AWIS_KEY1 == 0)                        // Liste anzeigen
    {
        $Form->SchreibeHTMLCode('<form name="frmArtikelpruefpersonal" action="./artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal" method="POST"  enctype="multipart/form-data">');

        $Form->Formular_Start();

        $Form->ZeileStart();
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=APP_NAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'APP_NAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APP']['APP_NAME'], 250, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=APP_AKTIVBIS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'APP_AKTIVBIS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APP']['APP_AKTIVBIS'], 150, '', $Link);
        $Form->ZeileEnde();

        $APPZeile = 0;
        while (!$rsAPP->EOF()) {
            $Form->ZeileStart();
            $Link = './artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal&APP_ID=' . $rsAPP->FeldInhalt('APP_ID') . '';
            $Form->Erstelle_ListenFeld('APP_NAME', $rsAPP->FeldInhalt('APP_NAME'), 0, 250, false, ($APPZeile % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('APP_AKTIVBIS', $rsAPP->FeldInhalt('APP_AKTIVBIS'), 0, 150, false, ($APPZeile % 2), '', '', 'D');
            $Form->ZeileEnde();

            $rsAPP->DSWeiter();
            $APPZeile++;
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht456 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))        // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->Schaltflaeche('script', 'cmdHilfe',
            "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=pruefpersonal','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
            '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');
        $Form->SchaltflaechenEnde();

        $Form->SchreibeHTMLCode('</form>');
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $Form->Formular_Start();
        $Form->SchreibeHTMLCode('<form name=frmArtikelpruefpersonal action=./artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsAPP->FeldInhalt('APP_ID');

        $Param['KEY'] = $AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben('Formular_APP', serialize($Param));

        $Form->Erstelle_HiddenFeld('APP_ID', $AWIS_KEY1);

        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a class=BilderLink href=./artikelstamm_Main.php?cmdAktion=Artikelpruefpersonal&APPListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAPP->FeldInhalt('APP_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', $rsAPP->FeldInhalt('APP_USERDAT')));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht456 & 2) != 0);

        if ($AWIS_KEY1 == 0) {
            $EditRecht = true;
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APP']['APP_ID'] . ':', 150);
        $Form->Erstelle_TextFeld('APP_ID', $rsAPP->FeldInhalt('APP_ID'), 10, 150, false, '', '', '', 'N0', 'L', '', '', 10);
        $AWISCursorPosition = 'txtAPP_ID';
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APP']['APP_NAME'] . ':', 150);
        $Form->Erstelle_TextFeld('APP_NAME', $rsAPP->FeldInhalt('APP_NAME'), 50, 350, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APP']['APP_AKTIVBIS'] . ':', 150);
        $Form->Erstelle_TextFeld('APP_AKTIVBIS', $rsAPP->FeldInhalt('APP_AKTIVBIS'), 10, 150, $EditRecht, '', '', '', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APP']['APP_KON_KEY'] . ':', 150);
        $SQL = 'SELECT KON_KEY, KON_NAME1 || \', \' || COALESCE(KON_NAME2,\'\') AS KONNAME FROM Kontakte ';
        $SQL .= ' WHERE KON_STATUS = \'A\' OR KON_KEY = 0' . $rsAPP->FeldInhalt('APP_KON_KEY');
        $SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
        $Form->Erstelle_SelectFeld('APP_KON_KEY', ($rsAPP->FeldInhalt('APP_KON_KEY')), 100, $EditRecht, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '',
            '', '');
        $Form->ZeileEnde();

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();

        $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        if (($Recht456 & 6) != 0)        //
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }

        if (($Recht456 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))        // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        if (($Recht456 & 8) == 8 AND !isset($_POST['cmdDSNeu_x'])) {
            $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
        }

        if (!isset($_POST['cmdDSNeu_x'])) {
            $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
            $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
        }

        $Form->Schaltflaeche('script', 'cmdHilfe',
            "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=pruefpersonal','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
            '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');

        $Form->SchaltflaechenEnde();

        $Form->SchreibeHTMLCode('</form>');
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>