<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">
<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
$SK_KEY=0;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";
$con = awisLogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Mitbewerber');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Mitbewerber','PKT_Mitbewerber');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Mitbewerber'].'</title>';
?>
</head>
<body>
<?
include ("ATU_Header.php");	// Kopfzeile

awis_ZeitMessung(0);

$Recht3010 = awisBenutzerRecht($con,3010);
if($Recht3010==0)
{
    awisEreignis(3,1000,'Mitbewerber',$_SERVER['PHP_AUTH_USER'],'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


$cmdAktion='';

if(isset($_GET['cmdAktion']))
{
	$cmdAktion = $_GET['cmdAktion'];
}
if(isset($_POST['cmdSuche_x']))
{
	$cmdAktion='Details';
}

if(isset($_POST['cmdXReset_x']))
{
	awis_BenutzerParameterSpeichern($con, "MitbewerberSuche", $_SERVER['PHP_AUTH_USER'],'');
	$cmdAktion='Suche';
}

/************************************************
* Daten anzeigen
************************************************/

awis_RegisterErstellen(3010, $con, $cmdAktion);

print "<input type=hidden name=cmdAktion value=Liste><br>";

//print "</form>";		// Form schließen wegen Schaltflächen

print "<input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";

		// Formulardaten löschen
if($cmdAktion=="Suche" OR $cmdAktion=="")
{
	print "&nbsp;<input title='".$AWISSprachKonserven['Wort']['lbl_reset']."' accesskey=r name=cmdXReset type=image src=/bilder/radierer.png onclick=location.href='./mitbewerber_Main.php?cmdAktion=Reset'>";
}

print "&nbsp;&nbsp;<input type=image title='".$AWISSprachKonserven['Wort']['lbl_hilfe']."' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=mitbewerber','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

//include "debug_info.php";
awislogoff($con);
echo '</form>';
?>
</body>
</html>

