<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KNZ','%');

	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Link_Route');
	$TextKonserven[]=array('Wort','Link_Maps');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen
	$Recht31000= $AWISBenutzer->HatDasRecht(31000);		// Geldentsorger in Filialinfo
	if(($Recht5001&12)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}
	
	if(($Recht31000&8)==8)
	{
	    $Edit = true;
	}
	else
	{
	    $Edit = false;
	}
	
	
	//Grundselect 
	$SQL  ='select * from (SELECT';
	$SQL .=' KZT_KEY ,';
	$SQL .=' KZT_FIL_ID ,';
	$SQL .=' KZT_TERMINAL_ID ,';
	$SQL .=' KZT_GUELTIG_AB ,';
	$SQL .=' KZT_GUELTIG_BIS ,';
	$SQL .=' KZT_USER ,';
	$SQL .=' KZT_USERDAT ,';
	$SQL .=' case when KZT_GUELTIG_AB <= trunc(sysdate) and KZT_GUELTIG_BIS >= trunc(sysdate) then 1 else 0 end as gueltig ';
	$SQL .=' FROM';
	$SQL .=' kartenzahlungenterminals)';
	
    
    if((isset($_GET['KZT_KEY']) and $_GET['KZT_KEY'] == -1) or (isset($_POST['txtKZT_KEY']) and $_POST['txtKZT_KEY'] == -1))
    {
        $Form->Erstelle_HiddenFeld('KZT_KEY', isset($_GET['KZT_KEY'])?$_GET['KZT_KEY']:(isset($_POST['txtKZT_KEY'])?$_POST['txtKZT_KEY']:0));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./filialinfos_Main.php?cmdAktion=Details".(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Form->InfoZeile($Felder,'');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_KARTENTERMINALS'],100, 'font-weight:bolder');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',150);
        $Form->Erstelle_TextFeld('!KNZ_TERMINALID',isset($_POST['txtKNZ_TERMINALID'])?$_POST['txtKNZ_TERMINALID']:'',10,150,$Edit,'','','','T');
        $Form->ZeileEnde();
        
        
        $Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_AB'].':',150);
		$Form->Erstelle_TextFeld('!KNZ_GUELTIGAB',isset($_POST['txtKNZ_GUELTIGAB'])?$_POST['txtKNZ_GUELTIGAB']:'',10,150,$Edit,'','','','D');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_BIS'].':',150);
		$Form->Erstelle_TextFeld('!KNZ_GUELTIGBIS',isset($_POST['txtKNZ_GUELTIGBIS'])?$_POST['txtKNZ_GUELTIGBIS']:'',10,150,$Edit,'','','','D');
		$Form->ZeileEnde();
        
    }
    elseif ((isset($_GET['KZT_KEY']) AND $_GET['KZT_KEY'] != -1) or (isset($_POST['txtKZT_KEY']) and $_POST['txtKZT_KEY'] != -1)) //Bestehnden DS editieren. 
    {
        
        $SQL .= ' WHERE KZT_KEY = ' . (isset($_GET['KZT_KEY'])?$_GET['KZT_KEY']:$_POST['txtKZT_KEY']);
        
        $Form->DebugAusgabe(1,$SQL);
        $rsTerminals = $DB->RecordSetOeffnen($SQL);
               
        $Form->Erstelle_HiddenFeld('KZT_KEY', $rsTerminals->FeldInhalt('KZT_KEY'));
               
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('KZT_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('KZT_USERDAT'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./filialinfos_Main.php?cmdAktion=Details".(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Form->InfoZeile($Felder,'');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_KARTENTERMINALS'],100, 'font-weight:bolder');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',150);
        $Form->Erstelle_TextFeld('!KNZ_TERMINALID',$rsTerminals->FeldInhalt('KZT_TERMINAL_ID'),10,150,false,'','','','T');
        $Form->ZeileEnde();
        
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_AB'].':',150);
        $Form->Erstelle_TextFeld('!KNZ_GUELTIGAB',$rsTerminals->FeldInhalt('KZT_GUELTIG_AB'),10,150,$Edit,'','','','D');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_BIS'].':',150);
        $Form->Erstelle_TextFeld('!KNZ_GUELTIGBIS',$rsTerminals->FeldInhalt('KZT_GUELTIG_BIS'),10,150,$Edit,'','','','D');
        $Form->ZeileEnde();
    }
    else
    {
       
        $SQL .= ' WHERE KZT_FIL_ID ='.$AWIS_KEY1;
        
        $Orderby = str_replace('~', ' desc ', isset($_GET['SSort'])?$_GET['SSort']:2); 
        $SQL .= ' ORDER BY ' . ($Orderby);
       
        
        $rsTerminals = $DB->RecordSetOeffnen($SQL);
        
        
        $Form->Formular_Start();
        // Infozeile zusammenbauen
        // $Felder = array();
        //$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('KZT_USER'));
        //$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('KZT_USERDAT'));
        //$Form->InfoZeile($Felder, '');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_KARTENTERMINALS'],100, 'font-weight:bolder');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        
        $Feldbreite['KNZ_TERMINALID']  = 120;
        $Feldbreite['KNZ_GUELTIG_AB'] = 130;
        $Feldbreite['KNZ_GUELTIG_BIS'] = 130;
        
        if($Edit)
        {
            $Icons = array();
            $Icons[] = array('new','./filialinfos_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&KZT_KEY=-1','g');
            $Form->Erstelle_ListeIcons($Icons,38,0);
        }
        
        
        
        $Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
        $Link .= '&SSort=KZT_TERMINAL_ID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='KZT_TERMINAL_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'],$Feldbreite['KNZ_TERMINALID'],'',$Link);
        
        $Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
        $Link .= '&SSort=KZT_GUELTIG_AB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='KZT_GUELTIG_AB'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_AB'],$Feldbreite['KNZ_GUELTIG_AB'],'',$Link);
        
        $Link = './filialinfos_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
        $Link .= '&SSort=KZT_GUELTIG_BIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='KZT_GUELTIG_BIS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_GUELTIG_BIS'],$Feldbreite['KNZ_GUELTIG_BIS'],'',$Link);
        
        $Form->ZeileEnde();
        
        $DS=0;
        while (!$rsTerminals->EOF())
        {
            $Form->ZeileStart();
            if($Edit)
            {
                $Icons = array();
                $Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&KZT_KEY='.$rsTerminals->FeldInhalt('KZT_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
                $Form->Erstelle_ListeIcons($Icons,38,($DS%2),'',$rsTerminals->FeldInhalt('GUELTIG')==0?'background-color: #FF0000;':'');
                 
            }
            $Form->Erstelle_ListenFeld('KZT_TERMINAL_ID',$rsTerminals->FeldInhalt('KZT_TERMINAL_ID'),0,$Feldbreite['KNZ_TERMINALID'],false,($DS%2),$rsTerminals->FeldInhalt('GUELTIG')==0?'background-color: #FF0000;':'','','T','L','');
            $Form->Erstelle_ListenFeld('KZT_GUELTIG_AB',$rsTerminals->FeldInhalt('KZT_GUELTIG_AB'),0,$Feldbreite['KNZ_GUELTIG_AB'],false,($DS%2),$rsTerminals->FeldInhalt('GUELTIG')==0?'background-color: #FF0000;':'','','T','L','');
            $Form->Erstelle_ListenFeld('KZT_GUELTIG_BIS',$rsTerminals->FeldInhalt('KZT_GUELTIG_BIS'),0,$Feldbreite['KNZ_GUELTIG_BIS'],false,($DS%2),$rsTerminals->FeldInhalt('GUELTIG')==0?'background-color: #FF0000;':'','','T','L','');
             
            $Form->ZeileEnde();
            $DS++;
            $rsTerminals->DSWeiter();
        }
    }   
	

	$Form->Formular_Ende();

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>