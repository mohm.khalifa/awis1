<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PBM','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4502 = $AWISBenutzer->HatDasRecht(4502);
	if($Recht4502==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Form->DebugAusgabe(1,$Recht4502);
	// Daten ermitteln
	$SQL = 'SELECT *';
	$SQL .= ' FROM Perseinsbereichemitglieder ';
	$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
	$SQL .= ' WHERE pbm_peb_key = 0'.$AWIS_KEY1;
	if(isset($_GET['PBM_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['PBM_KEY']);
		$SQL .= ' AND PBM_KEY = '.$AWIS_KEY2;
	}
	$SQL .= ' ORDER BY xbn_name, xbn_vorname';

	$rsPBM = $DB->RecordSetOeffnen($SQL);

	if($rsPBM->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht4502)&6)!=0)
		{
			$Form->Erstelle_HiddenFeld('txtPBREdit','Ja');
		}
		if((intval($Recht4502)&8)!=0)
		{
			$Icons[] = array('new','./personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&PBM_KEY=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=PBM_XBN_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PBM_XBN_KEY'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PBM']['PBM_XBN_KEY'],350,'',$Link);
		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=PBM_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PBM_GUELTIGAB'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PBM']['PBM_GUELTIGAB'],150,'',$Link);
		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=PBM_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PBM_GUELTIGBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PBM']['PBM_GUELTIGBIS'],150,'',$Link);
		$Form->ZeileEnde();


		$PEBZeile=0;
		$DS=0;
		while(!$rsPBM->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht4502&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&PBM_KEY='.$rsPBM->FeldInhalt('PBM_KEY'));
			}
			if(intval($Recht4502&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./personaleinsaetze_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&Del='.$rsPBM->FeldInhalt('PBM_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('XBN_NAME',$rsPBM->FeldInhalt('XBN_NAME').' '.$rsPBM->FeldInhalt('XBN_VORNAME'),0,350,false,($PEBZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('PBM_GUELTIGAB',$rsPBM->FeldInhalt('PBM_GUELTIGAB'),0,150,false,($PEBZeile%2),'','','D');
			$Form->Erstelle_ListenFeld('PBM_GUELTIGBIS',$rsPBM->FeldInhalt('PBM_GUELTIGBIS'),0,150,false,($PEBZeile%2),'','','D');
			$Form->ZeileEnde();

			$rsPBM->DSWeiter();
			$PEBZeile++;
		}

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsPBM->FeldInhalt('PBM_KEY');

		$Form->Erstelle_HiddenFeld('PBM_KEY',$AWIS_KEY2);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./personaleinsaetze_Main.php?cmdAktion=Bereiche&PBMListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPBM->FeldInhalt('PBM_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPBM->FeldInhalt('PBM_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4502&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PBM']['PBM_XBN_KEY'].':',150);
		$SQL = 'SELECT xbn_key, xbn_name || \' \' || xbn_vorname AS xbnname ';
		$SQL .= ' FROM Benutzer';
		$SQL .= ' WHERE LOWER(xbn_name) not like \'fil%\'';
		$SQL .= ' AND (xbn_key = 0'.$rsPBM->FeldInhalt('PBM_XBN_KEY').'';
		$SQL .= '      OR (NOT EXISTS (SELECT * FROM perseinsbereichemitglieder WHERE pbm_xbn_key = xbn_key';
		$SQL .= '          AND pbm_peb_key=0'.$AWIS_KEY2.')';
		$SQL .= '         AND xbn_status = \'A\'))';
		$SQL .= ' ORDER BY xbn_name, xbn_vorname';

		$Form->Erstelle_SelectFeld('PBM_XBN_KEY',$rsPBM->FeldInhalt('PBM_XBN_KEY'),50,$EditRecht,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','');
		$AWISCursorPosition='txtPBM_XBN_KEY';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PBM']['PBM_GUELTIGAB'].':',150);
		$Form->Erstelle_TextFeld('PBM_GUELTIGAB',$rsPBM->FeldInhalt('PBM_GUELTIGAB'),10,150,$EditRecht,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PBM']['PBM_GUELTIGBIS'].':',150);
		$Form->Erstelle_TextFeld('PBM_GUELTIGBIS',$rsPBM->FeldInhalt('PBM_GUELTIGBIS'),10,150,$EditRecht,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Zugriffsrechte'],400,awisFormular::FORMAT_UEBERSCHRIFT);
		$Form->ZeileEnde();

		$SQL = 'SELECT PBR_KEY, PBZ_ZUGRIFFSRECHT, PBZ_KEY';
		$SQL .= ' FROM PERSEINSBEREICHERECHTE';
		$SQL .= ' LEFT OUTER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBZ_KEY = PBZ_KEY AND PBR_PBM_KEY = 0'.$AWIS_KEY2;
		$SQL .= ' ORDER BY PBZ_KEY';
		$rsPBZ = $DB->RecordSetOeffnen($SQL);
		while(!$rsPBZ->EOF())
		{
			$Form->ZeileStart();

			$Form->Erstelle_TextLabel($rsPBZ->FeldInhalt('PBZ_ZUGRIFFSRECHT').':',350);

			$FeldName = 'PBR_KEY_'.$rsPBZ->FeldInhalt('PBR_KEY').'_'.$rsPBZ->FeldInhalt('PBZ_KEY');
			$Form->Erstelle_Checkbox($FeldName,($rsPBZ->FeldInhalt('PBR_KEY')==''?'':'on'),50,((int)$Recht4502&16),'on');
			$Form->ZeileEnde();
			$rsPBZ->DSWeiter();
		}

		$Form->Formular_Ende();
$Form->DebugAusgabe(1,(int)$Recht4502);
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200809111043);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>