<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_Matching
{

	private $_Funktionen;
	private $_DB;
	protected $_AWISBenutzer;		
	protected $preisListe = '';
	
	public function __construct()
	{
	  $this->_AWISBenutzer = awisBenutzer::Init();
	  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
	  $this->_DB->Oeffnen();
	}	
	
	public function starteMatching()
	{	
		//Matching anhand Warengruppen / Sortiment (Felgen...)
		/*
		$SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_WG IS NOT NULL AND GKZ_SG IS NOT NULL AND GKZ_AUTO='j'";
		$SQL .= " AND GKZ_GKP_KEY=".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
		
		$rsRSL = $this->_DB->RecordSetOeffnen($SQL);
		
		while(!$rsRSL->EOF())
		{
			$SQL =  'SELECT AST_ATUNR, AST_BEZEICHNUNGWW, AST_WGR_ID, AST_WUG_ID FROM V_ARTIKELSTAMMWGR WHERE AST_WGR_ID='.$this->_DB->FeldInhaltFormat('T',$rsRSL->FeldInhalt('GKZ_WG'));
			$SQL .= ' AND AST_WUG_ID='.$this->_DB->FeldInhaltFormat('T',$rsRSL->FeldInhalt('GKZ_SG'));		
			
			$rsInsert = $this->_DB->RecordSetOeffnen($SQL);
			
			while(!$rsInsert->EOF())
			{
				//Bei Radkappen die VPE berücksichtigen
				if ($rsInsert->FeldInhalt('AST_WGR_ID') == '07' and 
					$this->_DB->FeldInhaltFormat('Z', $rsInsert->FeldInhalt('AST_WUG_ID')) >= 500 and 
					$this->_DB->FeldInhaltFormat('Z', $rsInsert->FeldInhalt('AST_WUG_ID')) <= 599 and				
					strpos($rsInsert->FeldInhalt('AST_BEZEICHNUNGWW'),'VP1')!==false)
				{
					//Wenn VPE = 1 --> Preis durch 4 dividieren
					$Preis = $this->_DB->FeldInhaltFormat('N2',$rsRSL->FeldInhalt('GKZ_PREIS')) / 4;
					
					$SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_PREIS,GKA_USER,GKA_USERDAT) VALUES (';		
					$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsRSL->FeldInhalt('GKZ_KEY'));
					$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsInsert->FeldInhalt('AST_ATUNR'));
					$SQL .= ','.$this->_DB->FeldInhaltFormat('N2',$Preis);
					$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
		   			$SQL .= ',SYSDATE';
					$SQL .= ')';
				}			
				else 
				{				
					$SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES (';		
					$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsRSL->FeldInhalt('GKZ_KEY'));
					$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsInsert->FeldInhalt('AST_ATUNR'));
					$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
		   			$SQL .= ',SYSDATE';
					$SQL .= ')';
				}
							
	   			$rsInsert->DSWeiter();
			}
		
			$rsRSL->DSWeiter();
		}	
		*/
		//Matching der Reifen 
		$SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_AUTO='j' AND GKZ_WG IS NULL AND GKZ_SG IS NULL";
		$SQL .= " AND GKZ_GKP_KEY=28";
		$SQL .= " order by gkz_rx";
		
		$rsReifen = $this->_DB->RecordSetOeffnen($SQL);
		
		while(!$rsReifen->EOF())
		{
			$SQL  = "Select REI_AST_ATUNR FROM V_REIFENZUSATZINFOS WHERE";
			$SQL .= " REI_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_BREITE'));
			$SQL .= " AND REI_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_QUERSCHNITT'));
			$SQL .= " AND REI_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_BAUART'));
			$SQL .= " AND REI_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_INNENDURCHMESSER'));
			$SQL .= " AND REI_LOADINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_LOADINDEX1'));
			
			if($rsReifen->FeldInhalt('GKZ_LOADINDEX2') != '')
			{
				$SQL .= " AND REI_LOADINDEX2=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_LOADINDEX2'));
			}
			else 
			{
				$SQL .= " AND REI_LOADINDEX2 is null";
			}
			
			$SQL .= " AND REI_SPI_SYMBOL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SPEEDINDEX'));
			if($rsReifen->FeldInhalt('GKZ_GWS') != '')
			{
				$SQL .= " AND GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_GWS'));
			}
			else 
			{
				$SQL .= " AND GWS is null";
			}
			$SQL .= " AND RX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_RX'));
			if($rsReifen->FeldInhalt('GKZ_ROF') != '')
			{
				$SQL .= " AND ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_ROF'));
			}
			else 
			{
				$SQL .= " AND ROF is null";
			}
			if($rsReifen->FeldInhalt('GKZ_SEAL') != '')
			{
				$SQL .= " AND SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SEAL'));
			}
			else 
			{
				$SQL .= " AND SEAL is null";
			}
			
			//echo $SQL.chr(13).chr(10);
			
			$rsZuordnung = $this->_DB->RecordSetOeffnen($SQL);
								
			while(!$rsZuordnung->EOF())
			{			
				$SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES (';		
				$SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_KEY'));
				$SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsZuordnung->FeldInhalt('REI_AST_ATUNR'));
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
	   			$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				echo $this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_KEY')).'/';
				echo $this->_DB->FeldInhaltFormat('T',$rsZuordnung->FeldInhalt('REI_AST_ATUNR'));
				echo chr(13).chr(10);				
				
	   			$rsZuordnung->DSWeiter(); 		
			}
					
			$rsReifen->DSWeiter();
		}
	}	
}


?>