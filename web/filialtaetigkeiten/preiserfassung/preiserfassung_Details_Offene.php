<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PFA','%');
	$TextKonserven[]=array('PAF','%');
	$TextKonserven[]=array('MBW','MBW_NAME1');
	$TextKonserven[]=array('MBW','MBW_TELEFON');
	$TextKonserven[]=array('MBW','MBW_ORT');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AlleSetzen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8005= $AWISBenutzer->HatDasRecht(8005);		// Herstelle
	if(($Recht8005&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY MBW_NAME1, PFA_MBW_KEY';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT PreisAbfragenPreise.*, AST_KEY';
    $SQL .= ', COALESCE(AST_BEZEICHNUNGWW,DPR_BEZEICHNUNG) AS AST_BEZEICHNUNGWW';
	$SQL .= ', MBW_NAME1, MBW_ORT, MBW_TELEFON';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ', (SELECT DISTINCT DPR_BEZEICHNUNG FROM Dienstleistungspreise WHERE DPR_NUMMER = PFA_DLNR ) AS DLBEZ';
	$SQL .= ', COALESCE((SELECT DISTINCT AST_BEZEICHNUNGWW FROM Artikelstamm WHERE AST_ATUNR = PFA_KOMPLETTNR ),(SELECT DISTINCT DPR_BEZEICHNUNG FROM Dienstleistungspreise WHERE DPR_NUMMER = PFA_KOMPLETTNR)) AS KOMPLETTBEZ';
	$SQL .=', PFI_Baujahr, PFI_kmstand, KFZ_PS, KFZ_HERSTELLER, KFZ_MODELL, KFZ_TYP, KFZ_KRAFTSTOFFART, KFZ_ZYLINDER, KFZ_LIT';
	$SQL .= ' FROM PreisAbfragenPreise ';
	$SQL .= ' INNER JOIN Mitbewerber ON PFA_MBW_KEY = MBW_KEY';
	$SQL .= ' LEFT OUTER JOIN Artikelstamm ON PFA_AST_ATUNR = AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN Preisabfragengruppen ON PFG_KEY = PFA_PFG_KEY';
	$SQL .= ' LEFT OUTER JOIN Preisabfragenfahrzeuginfos ON PFG_PFI_KEY = PFI_KEY';
	$SQL .= ' LEFT OUTER JOIN V_KFZ_DATEN ON PFI_KTYPNR = KFZ_TYPNR';
    $SQL .= ' LEFT OUTER JOIN (SELECT DISTINCT DPR_NUMMER, DPR_BEZEICHNUNG FROM Dienstleistungspreise ) DL ON DPR_NUMMER = PFA_AST_ATUNR ';
	$SQL .= ' WHERE PFA_PAF_KEY = 0'.$AWIS_KEY1;

	if(!isset($_GET['Seite']) OR $_GET['Seite']=='Offene')
	{
		$SQL .= ' AND (PFA_PREIS IS NULL AND PFA_ERMITTLUNGSSTATUS = 1)';
	}
	else
	{
		$SQL .= ' AND (PFA_PREIS IS NOT NULL OR PFA_ERMITTLUNGSSTATUS > 1)';
	}


	$Filialen = $AWISBenutzer->FilialZugriff('',awisBenutzer::FILIALZUGRIFF_STRING);
	if($Filialen == '' AND isset($_GET['FIL_ID']))
	{
		$Filialen = $_GET['FIL_ID'];
	}

	if($Filialen != '')
	{
		$SQL .= ' AND PFA_FIL_ID IN ('.$Filialen.')';
	}


	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

	$SQL .= $ORDERBY;

	$rsPFA = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['PFA_PAF_KEY']=$AWIS_KEY1;
	$EditModus = ($Recht8005&2);
	if($EditModus)
	{
		$AWISCursorPosition='';			// Cursr im Detailbereich setzen
	}

    $AWIS_KEY2 = $rsPFA->FeldInhalt('PFA_FIL_ID');

	$Form->Formular_Start();
	$DS=0;
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_NAME1'],110);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_PREIS'],150);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_TELEFON'],120);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_ORT'],194);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_AST_ATUNR'],190);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_DLPREIS'],130);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_KOMPLETTPREIS'],90);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_ERMITTLUNGSSTATUS'],160);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_BEMERKUNG'],150);

	$Form->ZeileEnde();

	$LetzterMBW = 0;
	$LetztePFG=0;
	while(!$rsPFA->EOF())
	{
	    if($LetztePFG != $rsPFA->FeldInhalt('PFA_PFG_KEY'))
	    {
			$Form->Trennzeile();
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('#KFZ_HERSTELLER',$rsPFA->FeldInhalt('KFZ_HERSTELLER'),10,240,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_MODELL',$rsPFA->FeldInhalt('KFZ_MODELL'),10,240,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_TYP',$rsPFA->FeldInhalt('KFZ_TYP'),10,260,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_KBANR',$rsPFA->FeldInhalt(''),10,260,false,($DS%2));
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('#PFI_BAUJAHR','BJ: '.$rsPFA->FeldInhalt('PFI_BAUJAHR'),10,150,false,($DS%2));
			$Form->Erstelle_ListenFeld('#PFI_KMSTAND',$Form->Format('N0T',$rsPFA->FeldInhalt('PFI_KMSTAND')).' km',10,150,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_PS',$rsPFA->FeldInhalt('KFZ_PS').' PS',10,120,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_LIT',$rsPFA->FeldInhalt('KFZ_LIT').' l',10,120,false,($DS%2));
			$Form->Erstelle_ListenFeld('#KFZ_ZYLINDER',$rsPFA->FeldInhalt('KFZ_ZYLINDER').' Zyl',10,120,false,($DS%2));
			$Form->ZeileEnde();
	
	        $LetztePFG = $rsPFA->FeldInhalt('PFA_PFG_KEY');
	    }
			
		if($LetzterMBW!=$rsPFA->FeldInhalt('PFA_MBW_KEY'))
		{
			$Form->Trennzeile();
			$Form->ZeileStart();
			$Link = '/mitbewerb/mitbewerb_Main.php?cmdAktion=Details&MBW_KEY='.$rsPFA->FeldInhalt('PFA_MBW_KEY');
			$Form->Erstelle_ListenFeld('#MBW_NAME1',$rsPFA->FeldInhalt('MBW_NAME1'),10,250,false,($DS%2),'',$Link);
			$Form->Erstelle_ListenFeld('#MBW_TELEFON',$rsPFA->FeldInhalt('MBW_TELEFON'),10,150,false,($DS%2));
			$Form->Erstelle_ListenFeld('#MBW_ORT',$rsPFA->FeldInhalt('MBW_ORT'),10,190,false,($DS%2));

			$LetzterMBW=$rsPFA->FeldInhalt('PFA_MBW_KEY');
			$Form->ZeileEnde();
			$DS++;
		}

		$Form->ZeileStart();
		$Link = '';
		if ($rsPFA->FeldInhalt('AST_KEY') != '')
		{
			$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$rsPFA->FeldInhalt('PFA_AST_ATUNR');
		}
		$Form->Erstelle_ListenFeld('#PFA_AST_ATUNR',$rsPFA->FeldInhalt('PFA_AST_ATUNR'),10,120,false,($DS%2),'',$Link,'','L',$rsPFA->FeldInhalt('AST_BEZEICHNUNGWW'));

		$Form->Erstelle_ListenFeld('PFA_PREIS_'.$rsPFA->FeldInhalt('PFA_KEY'),$rsPFA->FeldInhalt('PFA_PREIS'),5,90,$EditModus,($DS%2));
		if($AWISCursorPosition=='')
		{
			$AWISCursorPosition = 'txtPFA_PREIS_'.$rsPFA->FeldInhalt('PFA_KEY');
		}
		$Form->Erstelle_ListenFeld('PFA_DLPREIS_'.$rsPFA->FeldInhalt('PFA_KEY'),$rsPFA->FeldInhalt('PFA_DLPREIS'),5,90,($rsPFA->FeldInhalt('PFA_DLNR')==''?false:$EditModus),($DS%2),'','','T','L',$rsPFA->FeldInhalt('DLBEZ'));
		$Form->Erstelle_ListenFeld('PFA_KOMPLETTPREIS_'.$rsPFA->FeldInhalt('PFA_KEY'),$rsPFA->FeldInhalt('PFA_KOMPLETTPREIS'),5,90,($rsPFA->FeldInhalt('PFA_KOMPLETTNR')==''?false:$EditModus),($DS%2));
		$Daten = explode('|',$AWISSprachKonserven['PFA']['lst_PFA_ERMITTLUNGSSTATUS']);
		$Form->Erstelle_SelectFeld('PFA_ERMITTLUNGSSTATUS_'.$rsPFA->FeldInhalt('PFA_KEY'),$rsPFA->FeldInhalt('PFA_ERMITTLUNGSSTATUS'),'160:150',$EditModus,'','','',1,'',$Daten);
		$Form->Erstelle_ListenFeld('PFA_BEMERKUNG_'.$rsPFA->FeldInhalt('PFA_KEY'),$rsPFA->FeldInhalt('PFA_BEMERKUNG'),15,150,$EditModus,($DS%2));

		$Form->ZeileEnde();
		$rsPFA->DSWeiter();
		$DS++;
	}
	$Link = './preiserfassung_Main.php?cmdAktion=Preisabfragen&Seite=Offene';
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201016061422");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201016061421");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>