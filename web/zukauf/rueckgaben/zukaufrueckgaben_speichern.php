<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
$Form->DebugAusgabe(1,$_POST);


	if(isset($_POST['txtZUL_KEY']))
	{
		$AWIS_KEY1=$_POST['txtZUL_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtZUL_',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZUL','ZUL_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZUL_ENTLASTUNG','ZUL_ENTLASTUNGGRUND');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZUL'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtZUL_KEY'])==0)
			{
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM zukauflieferscheine WHERE ZUL_Key=' . $_POST['txtZUL_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZUL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE zukauflieferscheine SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZUL_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZUL_userdat=sysdate';
					$SQL .= ' WHERE ZUL_key=0' . $_POST['txtZUL_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}


		//***********************************************
		// zukaufrueckgaben
		//***********************************************
	if(isset($_POST['txtZRU_KEY']))
	{
		$AWIS_KEY2=$_POST['txtZRU_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtZRU_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZRU','ZRU_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZRU_DATUM','ZRU_MENGE');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZLA'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtZRU_KEY'])==0)
			{
				$Speichern = true;

            	$SQL = 'SELECT ZUL_LIEFERMENGE';
            	$SQL .= ', (SELECT SUM(ZWA_MENGE) FROM ZUKAUFWERKSTATTAUFTRAEGE WHERE ZWA_ZUB_KEY = ZUL_ZUB_KEY) AS VERKAUFT';
            	$SQL .= ', (SELECT SUM(ZRU_MENGE) FROM ZUKAUFRUECKGABEN WHERE ZRU_ZUL_KEY = ZUL_KEY) AS RUECKGABE';
            	$SQL .= ' FROM ZUKAUFLIEFERSCHEINE';
            	$SQL .= ' WHERE ZUL_KEY = :var_N0_ZUL_KEY';
            	$BindeVariablen=array();
            	$BindeVariablen['var_N0_ZUL_KEY']=$AWIS_KEY1;
                $rsMenge = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
                $OffeneMenge = $rsMenge->FeldInhalt('ZUL_LIEFERMENGE')-$rsMenge->FeldInhalt('VERKAUFT')-$rsMenge->FeldInhalt('RUECKGABE');
            	if($OffeneMenge<=0)
            	{
            	    $Speichern=false;
            	}

				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO zukaufrueckgaben';
					$SQL .= '(ZRU_ZUL_KEY, ZRU_DATUM,ZRU_MENGE,ZRU_GRUND,ZRU_BEMERKUNG,ZRU_STATUS';
					$SQL .= ',ZRU_USER, ZRU_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtZRU_ZUL_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZRU_DATUM'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZRU_MENGE'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZRU_GRUND'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZRU_BEMERKUNG'],true);
					$SQL .= ', 1';	// Offene R�ckgabe
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$DB->Ausfuehren($SQL);
					
					$SQL = 'SELECT seq_ZRU_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$BindeVariablen=array();
            	$BindeVariablen['var_N0_ZRU_KEY']=$_POST['txtZRU_KEY'];
				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM zukaufrueckgaben WHERE ZRU_key=:var_N0_ZRU_KEY',$BindeVariablen);

				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZRU_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE zukaufrueckgaben SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZRU_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZRU_userdat=sysdate';
					$SQL .= ' WHERE ZRU_key=0' . $_POST['txtZRU_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}
	
	//**********************************************************************************************************
	//* Zukaufbestellungen: WA-Nummer �ndern, um eine Bestellung auf einem anderen Auftrag zu sehen
	//**********************************************************************************************************
	if(isset($_POST['txtZUB_KEY']))
	{
		$ZUB_KEY=$_POST['txtZUB_KEY'];
		$AWISWerkZeug = new awisWerkzeuge();
		if($AWISWerkZeug->BerechnePruefziffer($_POST['txtZUB_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN)===false)
		{
			$TextKonserven[]=array('ZPA','txtPruefzifferWAFalsch');
			$WAHinweis = $Form->LadeTexte($TextKonserven);
						
			$Form->ZeileStart();
			$Form->Hinweistext($WAHinweis['ZPA']['txtPruefzifferWAFalsch'].': '.$_POST['txtZUB_WANRKORR'],1);
			$Form->ZeileEnde();		
		}
		else
		{
			$SQL = 'UPDATE ZUKAUFBESTELLUNGEN';
			$SQL .= ' SET ZUB_WANRKORR = :var_T_ZUB_WANRKORR';
			$SQL .=	', ZUB_USER = :var_T_ZUB_USER';
			$SQL .=	', ZUB_USERDAT = SYSDATE';
			$SQL .= ' WHERE ZUB_KEY = :var_N0_ZUB_KEY';
			
			$DB->SetzeBindevariable('ZUB','var_T_ZUB_WANRKORR',$_POST['txtZUB_WANRKORR'],awisDatenbank::VAR_TYP_TEXT);
			$DB->SetzeBindevariable('ZUB','var_T_ZUB_USER',$AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);
			$DB->SetzeBindevariable('ZUB','var_N0_ZUB_KEY',$_POST['txtZUB_KEY'],awisDatenbank::VAR_TYP_TEXT);
			
			$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('ZUB'));
			
			// Benachrichtigung an den Fachbereich
			$Text = 'WA-Nummer '.$_POST['txtZUB_WANR'].' wurde auf '.$_POST['txtZUB_WANRKORR'].' durch die Filalie geaendert.';

			require_once 'awisMailer.inc';
			$Mail = new awisMailer($DB, $AWISBenutzer);
			$Mail->Betreff($Text);
			$Mail->Absender('awis@de.atu.eu');
			$Mail->AdressListe(awisMailer::TYP_TO,'zukauf@de.atu.eu',false,awisMailer::PRUEFAKTION_KEINE);
			$Mail->Text($Text,awisMailer::FORMAT_TEXT,true);
			$Mail->MailInWarteschlange($AWISBenutzer->BenutzerName());
		}
	}
	
	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>