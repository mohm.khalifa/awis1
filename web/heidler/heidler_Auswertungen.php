<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');


try
{
    $TextKonserven = array();
    $TextKonserven[]=array('HEIDLER','*');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_ALLE_0');
    $TextKonserven[]=array('Wort','AuswahlSpeichern');
    $TextKonserven[]=array('Wort','AlleAnzeigen');
    $TextKonserven[]=array('Wort','lbl_export');
    $TextKonserven[]=array('Wort','OffenePosten');

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht45000 = $AWISBenutzer->HatDasRecht(45000);

    if($Recht45000==0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Param = array();
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Heidler_Export'));

    $Form->SchreibeHTMLCode('<form name=frmExport action=./heidler_exportieren.php method=POST enctype="multipart/form-data">');
    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_LAGER'] . ':',200);
    $Daten = explode('|',$AWISSprachKonserven['HEIDLER']['HEIDLER_LST_LAGER']);
    $Form->Erstelle_SelectFeld('*LAGER',($Param['SPEICHERN']=='on'?$Param['LAGER']:''), 220, true, '', '', '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'] . ' von:',200);
    $Form->Erstelle_TextFeld('*DATUM_KOMMISSIONIERUNG_VON',($Param['SPEICHERN']=='on'?$Param['DATUM_KOMMISSIONIERUNG_VON']:''),10,220,true, '', '', '', 'D');
    $Form->Erstelle_TextLabel('bis:',30);
    $Form->Erstelle_TextFeld('*DATUM_KOMMISSIONIERUNG_BIS',($Param['SPEICHERN']=='on'?$Param['DATUM_KOMMISSIONIERUNG_BIS']:''),10,220,true, '', '', '', 'D');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_VERSANDART'] . ':',200);
    $Daten = explode('|',$AWISSprachKonserven['HEIDLER']['HEIDLER_LST_VERSANDART']);
    $Form->Erstelle_SelectFeld('*VERSANDART',($Param['SPEICHERN']=='on'?$Param['VERSANDART']:''), 220, true, '', '', '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['SATZKENNUNG'] . ':',200);
    $SQL_lst = 'SELECT trim(SATZKENNUNG), trim(SATZKENNUNG)';
    $SQL_lst .= ' FROM MV_HEIDLER';
    $SQL_lst .= ' GROUP BY trim(SATZKENNUNG)';
    $Form->Erstelle_SelectFeld('*SATZKENNUNG',($Param['SPEICHERN']=='on'?$Param['SATZKENNUNG']:''),220,true,$SQL_lst,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['MANDANT'] . ':',200);
    $Form->Erstelle_TextFeld('*MANDANT',($Param['SPEICHERN']=='on'?$Param['MANDANT']:''),10,220,true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_MANDANT'] . ':',200);
    $Form->Erstelle_TextFeld('*HEIDLER_MANDANT',($Param['SPEICHERN']=='on'?$Param['HEIDLER_MANDANT']:''),10,220,true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['ZUGESTELLT'] . ':',200);
    $Daten = explode('|',$AWISSprachKonserven['HEIDLER']['HEIDLER_LST_ZUGESTELLT']);
    $Form->Erstelle_SelectFeld('*ZUGESTELLT',($Param['SPEICHERN']=='on'?$Param['ZUGESTELLT']:''), 220, true, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
    $Form->ZeileEnde();

    // nur offene Posten aus dem Archiv exportieren
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['OFFENEPOS'].':',200);
    $Form->Erstelle_Checkbox('*OffenePosten',($Param['OFFENE']=='on'?'on':''),20,true,'on');
    $Form->ZeileEnde();

    // Auswahl kann gespeichert werden
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    $Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);

    $Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201611031120");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>