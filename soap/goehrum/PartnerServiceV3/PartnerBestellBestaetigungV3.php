<?php

class PartnerBestellBestaetigungV3
{

    /**
     * @var int $Kundennummer
     */
    protected $Kundennummer = null;

    /**
     * @var int $Bestellnummer
     */
    protected $Bestellnummer = null;

    /**
     * @var \DateTime $Bestelldatum
     */
    protected $Bestelldatum = null;

    /**
     * @var string $BestellReferenz
     */
    protected $BestellReferenz = null;

    /**
     * @var Versandart $Versand
     */
    protected $Versand = null;

    /**
     * @var int $Verarbeitungsnummer
     */
    protected $Verarbeitungsnummer = null;

    /**
     * @var \DateTime $Verarbeitungsdatum
     */
    protected $Verarbeitungsdatum = null;

    /**
     * @var ArrayOfBestaetigungPosition $Positionen
     */
    protected $Positionen = null;

    /**
     * @var boolean $BestellungAngelegt
     */
    protected $BestellungAngelegt = null;

    /**
     * @var string $Feedback
     */
    protected $Feedback = null;

    /**
     * @var OrderLieferadresse $LieferAdresse
     */
    protected $LieferAdresse = null;

    /**
     * @param int $Kundennummer
     * @param int $Bestellnummer
     * @param \DateTime $Bestelldatum
     * @param Versandart $Versand
     * @param int $Verarbeitungsnummer
     * @param \DateTime $Verarbeitungsdatum
     * @param boolean $BestellungAngelegt
     */
    public function __construct($Kundennummer, $Bestellnummer, \DateTime $Bestelldatum, $Versand, $Verarbeitungsnummer, \DateTime $Verarbeitungsdatum, $BestellungAngelegt)
    {
      $this->Kundennummer = $Kundennummer;
      $this->Bestellnummer = $Bestellnummer;
      $this->Bestelldatum = $Bestelldatum->format(\DateTime::ATOM);
      $this->Versand = $Versand;
      $this->Verarbeitungsnummer = $Verarbeitungsnummer;
      $this->Verarbeitungsdatum = $Verarbeitungsdatum->format(\DateTime::ATOM);
      $this->BestellungAngelegt = $BestellungAngelegt;
    }

    /**
     * @return int
     */
    public function getKundennummer()
    {
      return $this->Kundennummer;
    }

    /**
     * @param int $Kundennummer
     * @return PartnerBestellBestaetigungV3
     */
    public function setKundennummer($Kundennummer)
    {
      $this->Kundennummer = $Kundennummer;
      return $this;
    }

    /**
     * @return int
     */
    public function getBestellnummer()
    {
      return $this->Bestellnummer;
    }

    /**
     * @param int $Bestellnummer
     * @return PartnerBestellBestaetigungV3
     */
    public function setBestellnummer($Bestellnummer)
    {
      $this->Bestellnummer = $Bestellnummer;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBestelldatum()
    {
      if ($this->Bestelldatum == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Bestelldatum);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Bestelldatum
     * @return PartnerBestellBestaetigungV3
     */
    public function setBestelldatum(\DateTime $Bestelldatum)
    {
      $this->Bestelldatum = $Bestelldatum->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getBestellReferenz()
    {
      return $this->BestellReferenz;
    }

    /**
     * @param string $BestellReferenz
     * @return PartnerBestellBestaetigungV3
     */
    public function setBestellReferenz($BestellReferenz)
    {
      $this->BestellReferenz = $BestellReferenz;
      return $this;
    }

    /**
     * @return Versandart
     */
    public function getVersand()
    {
      return $this->Versand;
    }

    /**
     * @param Versandart $Versand
     * @return PartnerBestellBestaetigungV3
     */
    public function setVersand($Versand)
    {
      $this->Versand = $Versand;
      return $this;
    }

    /**
     * @return int
     */
    public function getVerarbeitungsnummer()
    {
      return $this->Verarbeitungsnummer;
    }

    /**
     * @param int $Verarbeitungsnummer
     * @return PartnerBestellBestaetigungV3
     */
    public function setVerarbeitungsnummer($Verarbeitungsnummer)
    {
      $this->Verarbeitungsnummer = $Verarbeitungsnummer;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVerarbeitungsdatum()
    {
      if ($this->Verarbeitungsdatum == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Verarbeitungsdatum);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Verarbeitungsdatum
     * @return PartnerBestellBestaetigungV3
     */
    public function setVerarbeitungsdatum(\DateTime $Verarbeitungsdatum)
    {
      $this->Verarbeitungsdatum = $Verarbeitungsdatum->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return ArrayOfBestaetigungPosition
     */
    public function getPositionen()
    {
      return $this->Positionen;
    }

    /**
     * @param ArrayOfBestaetigungPosition $Positionen
     * @return PartnerBestellBestaetigungV3
     */
    public function setPositionen($Positionen)
    {
      $this->Positionen = $Positionen;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBestellungAngelegt()
    {
      return $this->BestellungAngelegt;
    }

    /**
     * @param boolean $BestellungAngelegt
     * @return PartnerBestellBestaetigungV3
     */
    public function setBestellungAngelegt($BestellungAngelegt)
    {
      $this->BestellungAngelegt = $BestellungAngelegt;
      return $this;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
      return $this->Feedback;
    }

    /**
     * @param string $Feedback
     * @return PartnerBestellBestaetigungV3
     */
    public function setFeedback($Feedback)
    {
      $this->Feedback = $Feedback;
      return $this;
    }

    /**
     * @return OrderLieferadresse
     */
    public function getLieferAdresse()
    {
      return $this->LieferAdresse;
    }

    /**
     * @param OrderLieferadresse $LieferAdresse
     * @return PartnerBestellBestaetigungV3
     */
    public function setLieferAdresse($LieferAdresse)
    {
      $this->LieferAdresse = $LieferAdresse;
      return $this;
    }

}
