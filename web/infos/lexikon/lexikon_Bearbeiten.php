<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $RechteStufe;
global $awisRSInfo;			// Infos �ber das zuletzt ge�ffnete Recordset
global $awisRSZeilen;
global $awisDBFehler;

if(!isset($_POST['txtSuchbegriff']))
{
include ("ATU_Header.php");	// Kopfzeile
}


$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$PfadBasis = awis_BenutzerParameter($con,'SystemPfadWeb',$AWISBenutzer->BenutzerName()) . "/";
$PfadAnhang = $PfadBasis . awis_BenutzerParameter($con,'PfadLexikonAnhang',$AWISBenutzer->BenutzerName());
$PfadBilder = $PfadBasis . awis_BenutzerParameter($con,'PfadLexikonAbbildung',$AWISBenutzer->BenutzerName());


$RechteStufe = awisBenutzerRecht($con,1201);
if(($RechteStufe&2)!=2)
{
    awisEreignis(3,1000,'Lexikon bearbeiten',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if(!isset($_POST['txtLEX_KEY']) or $_POST['txtLEX_KEY']=='')
{
	$LexKey = awis_NameInArray($_POST,'cmdEdit_');
	$LexKey = intval(substr($LexKey,8));
}
else
{
	$LexKey = $_POST['txtLEX_KEY'];
}

if(isset($_POST['cmdNeu_x']))		// Neu hinzuf�gen
{
	$rsLexikon = array();
	$rsLexikonZeilen = 1;
}
else
{
	$SQL = 'SELECT * FROM Lexikon';
	$SQL .= ' WHERE LEX_KEY=0' . $LexKey;
	
	$rsLexikon = awisOpenRecordset($con,$SQL);
	$rsLexikonZeilen = $awisRSZeilen;
}

/*********************************************************************
* 
* Daten speichern
* 
*********************************************************************/
if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$rsLEX = awisOpenRecordset($con, "SELECT * FROM Lexikon WHERE LEX_KEY=0" . $_POST["txtLEX_KEY"]);
		
		$SQL = "DELETE FROM LEXIKON WHERE LEX_KEY=0" . $_POST["txtLEX_KEY"] . "";
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("lexikon_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		die('<br>Der Eintrag wurde erfolgreich gel�scht<br><br><a href=./lexikon_Main.php?cmdAktion=Liste>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmLEX method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtLEX_KEY value=" . $LexKey . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Eintrag l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}


if(isset($_POST['cmdSpeichern_x']))
{
	if($_POST['txtLEX_KEY']=='')
	{
		if($_POST['txtLEX_BEGRIFF']=='')		// Keinen Begriff angegeben
		{
			echo '<br><span class=HinweisText>Sie m�ssen eine Bezeichnung angeben, wenn Sie einen neuen Eintrag anlegen m�chten!.</span><br><br>';
			echo '<a href=./lexikon_Main.php>Zur�ck</a>';
			awisLogoff($con);
			die();
			
		}
		else
		{
			if(isset($_FILES['txtLEX_ANHANG_Datei']))
			{
				if(PruefeUpload($_FILES['txtLEX_ANHANG_Datei']))
				{
					if($_POST['txtLEX_ANHANG']=='')
					{
						$_POST['txtLEX_ANHANG']=$_FILES['txtLEX_ANHANG_Datei']['name'];
					}
				}
			}
			if(isset($_FILES['txtLEX_ABBILDUNG_Datei']))
			{
				if(PruefeUpload($_FILES['txtLEX_ABBILDUNG_Datei']))
				{
					if($_POST['txtLEX_ABBILDUNG']=='')
					{
						$_POST['txtLEX_ABBILDUNG']=$_FILES['txtLEX_ABBILDUNG_Datei']['name'];
					}
				}
			}
			
			
			$SQL = 'INSERT INTO AWIS.LEXIKON (LEX_BEGRIFF, LEX_ABKUERZUNG, LEX_KATEGORIE, LEX_BESCHREIBUNG, LEX_HERSTELLER,';
			$SQL .= ' LEX_SAMMELBEGRIFFE, LEX_GUELTIGVON, LEX_GUELTIGBIS, LEX_ABBILDUNG, LEX_ANHANG, LEX_USER, LEX_USERDAT) ';
			$SQL .= ' VALUES ( ';
			$SQL .= "'" . $_POST['txtLEX_BEGRIFF'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_ABKUERZUNG'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_KATEGORIE'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_BESCHREIBUNG'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_HERSTELLER'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_SAMMELBEGRIFFE'] . "'";
			$SQL .= ($_POST['txtLEX_GUELTIGVON']==''?',NULL':", TO_DATE('" . awis_format($_POST['txtLEX_GUELTIGVON'],'Datum') . "','DD.MM.RRRR')");
			$SQL .= ($_POST['txtLEX_GUELTIGBIS']==''?',NULL':", TO_DATE('" . awis_format($_POST['txtLEX_GUELTIGBIS'],'Datum') . "','DD.MM.RRRR')");
			$SQL .= ",'" . $_POST['txtLEX_ABBILDUNG'] . "'";
			$SQL .= ",'" . $_POST['txtLEX_ANHANG'] . "'";
			$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "', SYSDATE)";
			
			$erg = awisExecute($con, $SQL);
			if($erg == FALSE)
			{
				awisErrorMailLink('Lexikon','');
				awisLogoff($con);
				die();
			}
	
			$rsKey=awisOpenRecordset($con, 'SELECT seq_LEX_KEY.CURRVAL AS NR FROM DUAL');
			$LexKey=$rsKey['NR'][0];
			/****************************************
			* Dateiupload
			****************************************/
			if(is_uploaded_file($_FILES['txtLEX_ANHANG_Datei']['tmp_name']))
			{
				if(move_uploaded_file($_FILES['txtLEX_ANHANG_Datei']['tmp_name'],$PfadAnhang.'/'.$_POST['txtLEX_ANHANG'])===false)
				{
					awisLogoff($con);
					die('<span class=HinweisText>Dateiupload f�r die Datei ' . $_POST['txtLEX_ANHANG'] . ' konnte nicht durchgef�hrt werden.');
				}
			}
			if(is_uploaded_file($_FILES['txtLEX_ABBILDUNG_Datei']['tmp_name']))
			{
				if(move_uploaded_file($_FILES['txtLEX_ABBILDUNG_Datei']['tmp_name'],$PfadBilder.'/'.$_POST['txtLEX_ABBILDUNG'])===false)
				{
					awisLogoff($con);
					die('<span class=HinweisText>Dateiupload f�r die Datei ' . $_POST['txtLEX_ABBILDUNG'] . ' konnte nicht durchgef�hrt werden.');
				}
			}
		}
	}
	else		// Datensatz �ndern
	{
		if(isset($_FILES['txtLEX_ANHANG_Datei']))
		{
			if(PruefeUpload($_FILES['txtLEX_ANHANG_Datei']))
			{
				if($_POST['txtLEX_ANHANG']=='')
				{
					$_POST['txtLEX_ANHANG']=$_FILES['txtLEX_ANHANG_Datei']['name'];
				}
			}
		}
		if(isset($_FILES['txtLEX_ABBILDUNG_Datei']))
		{
			if(PruefeUpload($_FILES['txtLEX_ABBILDUNG_Datei']))
			{
				if($_POST['txtLEX_ABBILDUNG']=='')
				{
					$_POST['txtLEX_ABBILDUNG']=$_FILES['txtLEX_ABBILDUNG_Datei']['name'];
				}
			}
		}
		
		
		for($i=0;$i<=sizeof($awisRSInfo);$i++)
		{
			if(strchr($awisRSInfo[$i]['Name'],'KEY')=='' AND strchr($awisRSInfo[$i]['Name'],'USER')=='')
			{
				// Daten ge�ndert
				if(trim($_POST['txt' . $awisRSInfo[$i]['Name']]) != trim($_POST['txt' . $awisRSInfo[$i]['Name'].'_Old']))
				{
					// Daten auf der Platte von einem anderen Benutzer ge�ndert
					if(trim($rsLexikon[$awisRSInfo[$i]['Name']][0]) != trim($_POST['txt' . $awisRSInfo[$i]['Name'].'_Old']))
					{
						awisLogoff($con);
						
						echo '<span class=HinweisText>Die Daten wurden von ' . $rsLexikon['LEX_USER'][0] . ' ge�ndert. </span>';
						echo '<br>Original: ' . $_POST['txt' . $awisRSInfo[$i]['Name'].'_Old'];
						echo '<br>Ihr �nderungswunsch: ' . $_POST['txt' . $awisRSInfo[$i]['Name']];
						echo '<br>Aktueller Wert: ' . $rsLexikon[$awisRSInfo[$i]['Name']][0];
						
						die();
					}
					else
					{
							$SETCmd .= ', ' . $awisRSInfo[$i]['Name'] . "='" . trim($_POST['txt' . $awisRSInfo[$i]['Name']]) . "'";
					}
				}
			}
		}
	
		$SQL = "UPDATE Lexikon SET LEX_USER='" . $AWISBenutzer->BenutzerName() . "', LEX_USERDAT=SYSDATE";
		$SQL .= $SETCmd;
		$SQL .= ' WHERE LEX_KEY=0' . $_POST['txtLEX_KEY'];
	
		$erg = awisExecute($con, $SQL);
		if($erg == FALSE)
		{
			awisErrorMailLink('Lexikon','');
			awisLogoff($con);
			die();
		}
		
		
		/****************************************
		* Dateiupload
		****************************************/
		if(is_uploaded_file($_FILES['txtLEX_ANHANG_Datei']['tmp_name']))
		{
			if(move_uploaded_file($_FILES['txtLEX_ANHANG_Datei']['tmp_name'],$PfadAnhang.'/'.$_POST['txtLEX_ANHANG'])===false)
			{
				awisLogoff($con);
				die('<span class=HinweisText>Dateiupload f�r die Datei ' . $_POST['txtLEX_ANHANG'] . ' konnte nicht durchgef�hrt werden.');
			}
		}
		if($_POST['txtLEX_ANHANG_Old']!='' AND $_POST['txtLEX_ANHANG']=='')
		{	
			unlink($PfadAnhang.'/'.$_POST['txtLEX_ANHANG_Old']);
		}
		elseif($_POST['txtLEX_ANHANG_Old']!='' AND $_POST['txtLEX_ANHANG_Old'] !== $_POST['txtLEX_ANHANG'])
		{
			rename($PfadAnhang.'/'.$_POST['txtLEX_ANHANG_Old'],$PfadAnhang.'/'.$_POST['txtLEX_ANHANG']);
		}
		
		if(is_uploaded_file($_FILES['txtLEX_ABBILDUNG_Datei']['tmp_name']))
		{
			if(move_uploaded_file($_FILES['txtLEX_ABBILDUNG_Datei']['tmp_name'],$PfadBilder.'/'.$_POST['txtLEX_ABBILDUNG'])===false)
			{
				awisLogoff($con);
				die('<span class=HinweisText>Dateiupload f�r die Datei ' . $_POST['txtLEX_ABBILDUNG'] . ' konnte nicht durchgef�hrt werden.');
			}
		}
		if($_POST['txtLEX_ABBILDUNG_Old']!='' AND $_POST['txtLEX_ABBILDUNG']=='')
		{
			unlink($PfadBilder.'/'.$_POST['txtLEX_ABBILDUNG_Old']);
		}
		elseif($_POST['txtLEX_ABBILDUNG_Old']!='' AND $_POST['txtLEX_ABBILDUNG_Old'] !== $_POST['txtLEX_ABBILDUNG'])
		{
			rename($PfadBilder.'/'.$_POST['txtLEX_ABBILDUNG_Old'],$PfadBilder.'/'.$_POST['txtLEX_ABBILDUNG']);
		}
	}
		// Aktuellen Stand einlesen
	$SQL = 'SELECT * FROM Lexikon';
	$SQL .= ' WHERE LEX_KEY=0' . $LexKey;
	
	$rsLexikon = awisOpenRecordset($con,$SQL);
	$rsLexikonZeilen = $awisRSZeilen;

} //Ende Speichern











if($rsLexikonZeilen==0)		// Keine Daten
{
	echo 'Zu dem Key ' . $LexKey . ' wurde kein Eintrag gefunden.';
}
else
{
	echo "<form name=frmLexikon method=post action=./lexikon_Bearbeiten.php enctype='multipart/form-data'>";
	echo '<Table id=DatenTabelle border=1 width=100%>';

	echo '<tr><td id=FeldBez>Key:</td><td>' . (isset($rsLexikon['LEX_KEY'][0])?$rsLexikon['LEX_KEY'][0]:'') . '';
	echo '<input type=hidden name=txtLEX_KEY value=' . (isset($rsLexikon['LEX_KEY'][0])?$rsLexikon['LEX_KEY'][0]:'') . '></td></tr>';
	
		// Begriff
	echo '<tr><td id=FeldBez>Begriff:</td><td>';
	echo '<input type=hidden name=txtLEX_BEGRIFF_Old value='. chr(34) . (isset($rsLexikon['LEX_BEGRIFF'][0])?$rsLexikon['LEX_BEGRIFF'][0]:''). chr(34) . '>';
	echo '<input type=text size=50 name=txtLEX_BEGRIFF value=' . chr(34) . (isset($rsLexikon['LEX_BEGRIFF'][0])?$rsLexikon['LEX_BEGRIFF'][0]:'') . chr(34) . '>';
	echo '</td></tr>';
	
		// Abk�rzung
	echo '<tr><td id=FeldBez>Abk�rzung:</td><td>';
	echo '<input type=hidden name=txtLEX_ABKUERZUNG_Old value='. chr(34) . (isset($rsLexikon['LEX_ABKUERZUNG'][0])?$rsLexikon['LEX_ABKUERZUNG'][0]:'') . chr(34). '>';
	echo '<input type=text size=50 name=txtLEX_ABKUERZUNG value=' . chr(34) . (isset($rsLexikon['LEX_ABKUERZUNG'][0])?$rsLexikon['LEX_ABKUERZUNG'][0]:'') . chr(34) . '>';
	echo '</td></tr>';

		// Kategorie
	echo '<tr><td id=FeldBez>Kategorie:</td><td>';
	echo '<input type=hidden name=txtLEX_KATEGORIE_Old value=' . (isset($rsLexikon['LEX_KATEGORIE'][0])?$rsLexikon['LEX_KATEGORIE'][0]:'') . '>';
	echo '<select size=1 name=txtLEX_KATEGORIE>';
	echo '<option ' . ($rsLexikon['LEX_KATEGORIE'][0]=='EDV'?'selected':'') . ' value=\'EDV\'>EDV</option>';
	echo '<option ' . ($rsLexikon['LEX_KATEGORIE'][0]=='KFZ'?'selected':'') . ' value=\'KFZ\'>KFZ</option>';
	// value=' . chr(34) . $rsLexikon['LEX_KATEGORIE'][0] . chr(34) . '>';
	echo '</select></td></tr>';

		// Beschreibung
	echo '<tr><td id=FeldBez>Beschreibung:</td><td>';
	echo '<input type=hidden name=txtLEX_BESCHREIBUNG_Old value=' . chr(34). (isset($rsLexikon['LEX_BESCHREIBUNG'][0])?$rsLexikon['LEX_BESCHREIBUNG'][0]:'') . chr(34). '>';
	echo '<textarea cols=50 rows=10 name=txtLEX_BESCHREIBUNG>' . (isset($rsLexikon['LEX_BESCHREIBUNG'][0])?$rsLexikon['LEX_BESCHREIBUNG'][0]:'') . '</textarea>';
	echo '</td></tr>';

			// Hersteller
	echo '<tr><td id=FeldBez>Hersteller:</td><td>';
	echo '<input type=hidden name=txtLEX_HERSTELLER_Old value=' . (isset($rsLexikon['LEX_HERSTELLER'][0])?$rsLexikon['LEX_HERSTELLER'][0]:'') . '>';
	echo '<input type=text size=50 name=txtLEX_HERSTELLER value=' . chr(34) . (isset($rsLexikon['LEX_HERSTELLER'][0])?$rsLexikon['LEX_HERSTELLER'][0]:'') . chr(34) . '>';
	echo '</td></tr>';

			// Sammelbegriff
	echo '<tr><td id=FeldBez>Sammelbegriff:</td><td>';
	echo '<input type=hidden name=txtLEX_SAMMELBEGRIFFE_Old value=' . (isset($rsLexikon['LEX_SAMMELBEGRIFFE'][0])?$rsLexikon['LEX_SAMMELBEGRIFFE'][0]:'') . '>';
	echo '<input type=text size=50 name=txtLEX_SAMMELBEGRIFFE value=' . chr(34) . (isset($rsLexikon['LEX_SAMMELBEGRIFFE'][0])?$rsLexikon['LEX_SAMMELBEGRIFFE'][0]:'') . chr(34) . '>';
	echo '</td></tr>';

			// G�ltigkeit von
	echo '<tr><td id=FeldBez>G�ltig vom:</td><td>';
	echo '<input type=hidden name=txtLEX_GUELTIGVON_Old value=' . (isset($rsLexikon['LEX_GUELTIGVON'][0])?$rsLexikon['LEX_GUELTIGVON'][0]:'') . '>';
	echo '<input type=text size=10 name=txtLEX_GUELTIGVON value=' . chr(34) . (isset($rsLexikon['LEX_GUELTIGVON'][0])?$rsLexikon['LEX_GUELTIGVON'][0]:'') . chr(34) . '>';
	echo '</td></tr>';
	
			// G�ltigkeit bis
	echo '<tr><td id=FeldBez>G�ltig bis:</td><td>';
	echo '<input type=hidden name=txtLEX_GUELTIGBIS_Old value=' . (isset($rsLexikon['LEX_GUELTIGBIS'][0])?$rsLexikon['LEX_GUELTIGBIS'][0]:'') . '>';
	echo '<input type=text size=10 name=txtLEX_GUELTIGBIS value=' . chr(34) . (isset($rsLexikon['LEX_GUELTIGBIS'][0])?$rsLexikon['LEX_GUELTIGBIS'][0]:'') . chr(34) . '>';
	echo '</td></tr>';

				// Abbildung
	echo '<tr><td id=FeldBez>Bildname:</td><td>';
	echo '<input type=hidden name=txtLEX_ABBILDUNG_Old value=' . (isset($rsLexikon['LEX_ABBILDUNG'][0])?$rsLexikon['LEX_ABBILDUNG'][0]:'') . '>';
	echo '<input type=text size=30 name=txtLEX_ABBILDUNG value=' . chr(34) . (isset($rsLexikon['LEX_ABBILDUNG'][0])?$rsLexikon['LEX_ABBILDUNG'][0]:'') . chr(34) . '>';
	echo 'Datei kopieren von: <input name=txtLEX_ABBILDUNG_Datei type=file size=50 maxlength=100000 accept=\"text/*\">	';
	echo '</td></tr>';

				// Anhang
	echo '<tr><td id=FeldBez>Anhang (z.B. PDF):</td><td>';
	echo '<input type=hidden name=txtLEX_ANHANG_Old value=' . (isset($rsLexikon['LEX_ANHANG'][0])?$rsLexikon['LEX_ANHANG'][0]:'') . '>';
	echo '<input type=text size=30 name=txtLEX_ANHANG value=' . chr(34) . (isset($rsLexikon['LEX_ANHANG'][0])?$rsLexikon['LEX_ANHANG'][0]:'') . chr(34) . '>';
	echo 'Datei kopieren von: <input name=txtLEX_ANHANG_Datei type=file size=50 maxlength=100000 accept=\"text/*\">	';
	echo '</td></tr>';
	
	echo '</table>';



		// Schaltfl�chen
    echo "<br><hr><img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./lexikon_Main.php';>";
    echo "&nbsp;<input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	if(($RechteStufe&4)==4)
	{
		echo "&nbsp;<input type=image accesskey=n name=cmdNeu title='Hinzuf�gen (Alt+n)' src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
	if(($RechteStufe&8)==8)
	{
		echo "&nbsp;<input type=image accesskey=x title='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
	}
	echo "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=lexikon&HilfeBereich=Bearbeiten','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

	echo '</form>';
}





function PruefeUpload($Datei)
{
	// Alle g�ltigen Dateitypen festlegen
	$UploadTypen[]='application/vnd.ms-excel';


	if(array_search($Datei['type'],$UploadTypen)!=='')
	{
		if($Datei['size']<=1000000)
		{
			return true;
		}
	}

	echo '<br><span class=HinweisText>Die Datei '. $Datei['name']. ' ist nicht zul�ssig. Bitte wenden Sie sich an die Administration.</span><br>';
	return false;
}



?>

