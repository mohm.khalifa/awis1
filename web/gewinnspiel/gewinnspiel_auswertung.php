<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?
global $_POST;
global $_GET;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3500);
if(($RechteStufe&2)==0)
{
    awisEreignis(3,1000,'Auswertungen: Gewinnspiel',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if(!isset($_POST['txtGewinnfrage']))
{
	echo '<Form name=frmGewinnspiel method=post action=./gewinnspiel_auswertung.php>';
	echo '<table border=0 width=100%>';

	echo "<tr><td  id=FeldBez width=180>Ge<u>w</u>innfrage:</td>";
	echo "<td id=DatenTabelle><select name=txtGewinnfrage accesskey='w' size=1 tabindex=10>";
	
	$rsGWF=awisOpenRecordset($con,"SELECT * FROM GEWINNFRAGEN ORDER BY GWF_DATUM_ATU_TV");
	$rsGWFZeilen = $awisRSZeilen;
	
	for($i=0;$i<$rsGWFZeilen;$i++)
	{
		echo "<option " . ($i==0?"selected":"") . " value=" . $rsGWF["GWF_KEY"][$i] . ">" . $rsGWF["GWF_DATUM_ATU_TV"][$i] . ": " . $rsGWF["GWF_FRAGE"][$i] . "</option>";
	}
	
	echo "</select></td></tr>";
	unset($rsGWF);		// Speicher freigeben
	
	echo '</table>';
	
	echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
	echo '</Form>';
}
else
{	
	// Datei �ffnen
	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );
	
	/*******************************
	* Listen
	*******************************/

	//Gewinnfrage ausgeben
	$SQL = 'SELECT * FROM GEWINNFRAGEN WHERE GWF_KEY=0' . $_POST['txtGewinnfrage'];
	$rsGWF = awisOpenRecordset($con, $SQL);
	$rsGWFZeilen = $awisRSZeilen;
	
	echo '<table border=1>';
	echo '<tr>';
	echo '<td id=FeldBez>Gewinnfrage</td>';
	echo '<td>'.$rsGWF['GWF_FRAGE'][0] . '</td>';
	$Zeile = '';
	$Zeile = "Gewinnfrage;";
	$Zeile .= $rsGWF['GWF_FRAGE'][0];
	$Zeile .= "\n";
	fputs($fd, $Zeile);
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>Antwort A</td>';
	echo '<td>'.$rsGWF['GWF_ANTWORT_A'][0] . '</td>';
	$Zeile = '';
	$Zeile = "Antwort A;";
	$Zeile .= $rsGWF['GWF_ANTWORT_A'][0];
	$Zeile .= "\n";
	fputs($fd, $Zeile);
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>Antwort B</td>';
	echo '<td>'.$rsGWF['GWF_ANTWORT_B'][0] . '</td>';
	$Zeile = '';
	$Zeile = "Antwort B;";
	$Zeile .= $rsGWF['GWF_ANTWORT_B'][0];
	$Zeile .= "\n";
	fputs($fd, $Zeile);
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>Datum ATU-TV</td>';
	echo '<td>'.$rsGWF['GWF_DATUM_ATU_TV'][0] . '</td>';
	$Zeile = '';
	$Zeile = "Datum ATU-TV;";
	$Zeile .= $rsGWF['GWF_DATUM_ATU_TV'][0];
	$Zeile .= "\n";
	fputs($fd, $Zeile);
	echo '</tr>';
	echo '</table>';
	echo '<br><br>';
	
	//Teilnehmer des Gewinnspiels ausgeben
	//Antwort A
	//Tabelle Personal
	$SQL = '';
    $SQL = "SELECT GWS_PER_NR, PER_NACHNAME, PER_VORNAME, PER_FIL_ID, GWF_ANTWORT_A";
	$SQL .= " FROM GEWINNSPIELE";
	$SQL .= " INNER JOIN PERSONAL ON GWS_PER_NR = PER_NR";
	$SQL .= " INNER JOIN GEWINNFRAGEN ON GWF_KEY = GWS_GWF_KEY";
	$SQL .= " WHERE GWS_ANTWORT = 'A' AND GWS_GWF_KEY = ".$_POST['txtGewinnfrage'];
	$SQL .= " ORDER BY PER_FIL_ID, GWS_PER_NR, PER_NACHNAME, PER_VORNAME";

	$rsGWSTeilnehmer = awisOpenRecordset($con, $SQL);
    $rsGWSTeilnehmerAnz = $awisRSZeilen;

	echo '<table border=1 width=100%>';
	echo '<tr>';
	echo '<td id=FeldBez>Personalnummer</td>';
	echo '<td id=FeldBez>Name</td>';
	echo '<td id=FeldBez>Vorname</td>';
	echo '<td id=FeldBez>Filiale</td>';
	echo '<td id=FeldBez>Antwort</td>';
	echo '</tr>';
	
	fputs($fd, "\n\n");
	fputs($fd, "Personalnummer;Name;Vorname;Filiale;Antwort\n");

	for($i=0;$i<$rsGWSTeilnehmerAnz;$i++)
	{
		$Zeile='';
		echo '<tr>';
		echo '<td>' . $rsGWSTeilnehmer['GWS_PER_NR'][$i] . '</td>';
		$Zeile .= $rsGWSTeilnehmer['GWS_PER_NR'][$i];

		echo '<td>' . $rsGWSTeilnehmer['PER_NACHNAME'][$i] . '</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['PER_NACHNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['PER_VORNAME'][$i] .'</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['PER_VORNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['PER_FIL_ID'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['PER_FIL_ID'][$i];

		echo '<td>' . $rsGWSTeilnehmer['GWF_ANTWORT_A'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['GWF_ANTWORT_A'][$i];
		
		$Zeile .= "\n";
		fputs($fd, $Zeile);

	}
	
	//Teilnehmer des Gewinnspiels ausgeben
	//Antwort B
	//Tabelle Personal
    $SQL = '';
	$SQL = "SELECT GWS_PER_NR, PER_NACHNAME, PER_VORNAME, PER_FIL_ID, GWF_ANTWORT_B";
	$SQL .= " FROM GEWINNSPIELE";
	$SQL .= " INNER JOIN PERSONAL ON PER_NR = GWS_PER_NR";
	$SQL .= " INNER JOIN GEWINNFRAGEN ON GWF_KEY = GWS_GWF_KEY";
	$SQL .= " WHERE GWS_ANTWORT = 'B' AND GWS_GWF_KEY =". $_POST['txtGewinnfrage'];
	$SQL .= " ORDER BY PER_FIL_ID, GWS_PER_NR, PER_NACHNAME, PER_VORNAME";

	$rsGWSTeilnehmer = awisOpenRecordset($con, $SQL);
    $rsGWSTeilnehmerAnz = $awisRSZeilen;

	for($i=0;$i<$rsGWSTeilnehmerAnz;$i++)
	{
		$Zeile='';
		echo '<tr>';
		echo '<td>' . $rsGWSTeilnehmer['GWS_PER_NR'][$i] . '</td>';
		$Zeile .= $rsGWSTeilnehmer['GWS_PER_NR'][$i];

		echo '<td>' . $rsGWSTeilnehmer['PER_NACHNAME'][$i] . '</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['PER_NACHNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['PER_VORNAME'][$i] .'</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['PER_VORNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['PER_FIL_ID'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['PER_FIL_ID'][$i];

		echo '<td>' . $rsGWSTeilnehmer['GWF_ANTWORT_B'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['GWF_ANTWORT_B'][$i];
		
		$Zeile .= "\n";
		fputs($fd, $Zeile);
	}
	
	$SQL = '';
	//Teilnehmer des Gewinnspiels ausgeben
	//Antwort A
	//Tabelle Sonderausweise wegen Personal in ausl�ndischen Filialen
    $SQL = "SELECT GWS_PER_NR, SAW_NAME, SAW_VORNAME, SAW_FIL_ID, GWF_ANTWORT_A";
	$SQL .= " FROM GEWINNSPIELE";
	$SQL .= " INNER JOIN SONDERAUSWEISE ON GWS_PER_NR = SAW_PER_NR";
	$SQL .= " INNER JOIN GEWINNFRAGEN ON GWF_KEY = GWS_GWF_KEY";
	$SQL .= " WHERE GWS_ANTWORT = 'A' AND SAW_PER_NR < 900000 AND SAW_SAG_KEY = 1 AND GWS_GWF_KEY =". $_POST['txtGewinnfrage'];
	$SQL .= " ORDER BY SAW_FIL_ID, SAW_PER_NR, SAW_NAME, SAW_VORNAME";

	$rsGWSTeilnehmer = awisOpenRecordset($con, $SQL);
    $rsGWSTeilnehmerAnz = $awisRSZeilen;
    
    for($i=0;$i<$rsGWSTeilnehmerAnz;$i++)
	{
		$Zeile='';
		echo '<tr>';
		echo '<td>' . $rsGWSTeilnehmer['GWS_PER_NR'][$i] . '</td>';
		$Zeile .= $rsGWSTeilnehmer['GWS_PER_NR'][$i];

		echo '<td>' . $rsGWSTeilnehmer['SAW_NAME'][$i] . '</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['SAW_NAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['SAW_VORNAME'][$i] .'</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['SAW_VORNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['SAW_FIL_ID'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['SAW_FIL_ID'][$i];

		echo '<td>' . $rsGWSTeilnehmer['GWF_ANTWORT_A'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['GWF_ANTWORT_A'][$i];
		
		$Zeile .= "\n";
		fputs($fd, $Zeile);
	}
	
	$SQL = '';
	//Teilnehmer des Gewinnspiels ausgeben
	//Antwort B
	//Tabelle Sonderausweise wegen Personal in ausl�ndischen Filialen
    $SQL = "SELECT GWS_PER_NR, SAW_NAME, SAW_VORNAME, SAW_FIL_ID, GWF_ANTWORT_B";
	$SQL .= " FROM GEWINNSPIELE";
	$SQL .= " INNER JOIN SONDERAUSWEISE ON GWS_PER_NR = SAW_PER_NR";
	$SQL .= " INNER JOIN GEWINNFRAGEN ON GWF_KEY = GWS_GWF_KEY";
	$SQL .= " WHERE GWS_ANTWORT = 'B' AND SAW_PER_NR < 900000 AND SAW_SAG_KEY = 1 AND GWS_GWF_KEY =". $_POST['txtGewinnfrage'];
	$SQL .= " ORDER BY SAW_FIL_ID, SAW_PER_NR, SAW_NAME, SAW_VORNAME";

	$rsGWSTeilnehmer = awisOpenRecordset($con, $SQL);
    $rsGWSTeilnehmerAnz = $awisRSZeilen;
    
    for($i=0;$i<$rsGWSTeilnehmerAnz;$i++)
	{
		$Zeile='';
		echo '<tr>';
		echo '<td>' . $rsGWSTeilnehmer['GWS_PER_NR'][$i] . '</td>';
		$Zeile .= $rsGWSTeilnehmer['GWS_PER_NR'][$i];

		echo '<td>' . $rsGWSTeilnehmer['SAW_NAME'][$i] . '</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['SAW_NAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['SAW_VORNAME'][$i] .'</td>';
		$Zeile .= ';' . $rsGWSTeilnehmer['SAW_VORNAME'][$i];
		
		echo '<td>' . $rsGWSTeilnehmer['SAW_FIL_ID'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['SAW_FIL_ID'][$i];

		echo '<td>' . $rsGWSTeilnehmer['GWF_ANTWORT_B'][$i] . '</td>';
		$Zeile .= '; ' . $rsGWSTeilnehmer['GWF_ANTWORT_B'][$i];
		
		$Zeile .= "\n";
		fputs($fd, $Zeile);
	}
	
	echo '</table>';
	
	fclose($fd);
	
	// Datei ausgeben
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
}	

	
if(isset($_GET['ZurueckLink']))
{
	$ZurueckLink = $_GET['ZurueckLink'];
}
else
{
	$ZurueckLink = './gewinnspiel_Main.php';
}
echo "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $ZurueckLink . "';>";

echo "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

// JAVA Skript zum Cursor positionieren

echo "\n<Script Language=JavaScript>";
echo "document.getElementsByName('txtGewinnfrage')[0].focus();";
echo "\n</Script>";

?>
</body>
</html>

