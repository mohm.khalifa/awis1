<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('CPO','CPO_POTENTIALGESAMT');
$TextKonserven[]=array('CPO','CPO_POTENTIALATU');
$TextKonserven[]=array('CPO','CPO_FAHRZEUGE');
$TextKonserven[]=array('CPO','CPO_FAHRZEUGEATU');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CWV','CWV_KON_KEY');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('KON','KON_%');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','Kunden');
$TextKonserven[]=array('Wort','Interessenten');
$TextKonserven[]=array('Wort','Stillgelegt');
$TextKonserven[]=array('Wort','NichtZugeordnet');
$TextKonserven[]=array('Wort','Wiedervorlagen');
$TextKonserven[]=array('Wort','UeberfaelligeWV');
$TextKonserven[]=array('Wort','FaelligeWV');
$TextKonserven[]=array('Wort','ZukuenftigeWV');
$TextKonserven[]=array('Ausdruck','txtFirmaVertraulich');
$TextKonserven[]=array('Wort','Inkasso');
$TextKonserven[]=array('Wort','Passiv');
$TextKonserven[]=array('Wort','Bonuskunde');
$TextKonserven[]=array('Wort','Loeschen');
$TextKonserven[]=array('Wort','Rechnungslegung');

if(!isset($con))		// wenn direkt, alles initialisieren
{
	$con = awisLogon();
	$AWISBenutzer = new awisUser();
	$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
}
$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3713 = awisBenutzerRecht($con,3713,$AWISBenutzer->BenutzerName());
if($Recht3713==0)
{
    awisEreignis(3,1000,'CRM-Uebersicht',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Daten sollen als Audruck
$Ausdruck = isset($_GET['Ausdruck']);

if($Ausdruck)
{
	require_once('awisAusdruck.php');
	$Report = new awisAusdruck('L','A4',array('BriefpapierATU_DE_Seite_2_quer.pdf'),'Uebersicht');	
	$Report->NeueSeite(0,1);
}
else
{
	awis_FORM_FormularStart();
	echo '<form name=frmUebersicht method=post action=./crm_Main.php?cmdAktion=Uebersicht>';
}

//***********************************
//
//***********************************
$CADBetreuerID = '';
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$SQL = 'SELECT CAD_KON_KEY, CAD_STATUS, KON_NAME1, KON_NAME2, COUNT(*) AS Kunden';
$SQL .= ', SUM((SELECT SUM(CFZ_ANZAHL) from (SELECT DISTINCT CFZ_ANZAHL, CFZ_KEY, CPO_CAD_KEY FROM CRMPOTENTIAL
           INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY
           INNER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY
		) Daten WHERE CPO_CAD_KEY = CAD_KEY GROUP BY CPO_CAD_KEY)) AS CPO_FAHRZEUGE
		, SUM((
		SELECT ROUND(SUM(CFZ_ANZAHL*(CFZ_ANTEIL/100)),0) from (SELECT DISTINCT CFZ_ANZAHL, CFZ_ANTEIL, CFZ_KEY, CPO_CAD_KEY FROM CRMPOTENTIAL
		           INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY
		           INNER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY
		) Daten WHERE CPO_CAD_KEY = CAD_KEY GROUP BY CPO_CAD_KEY)) AS CPO_FAHRZEUGEATU
		';
		$SQL .= ', SUM((SELECT Umsatz
		from (
		SELECT DISTINCT ROUND(SUM(CPD_STATUS*COALESCE(CPV_BETRAG,CLV_BETRAG,0)*CFZ_ANZAHL*(CFZ_ANTEIL/100)),0) AS Umsatz, CPO_CAD_KEY
		FROM CRMPOTENTIAL
		          INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY
		           LEFT OUTER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY
		           LEFT OUTER JOIN CRMPOTENTIALVORGABEN
		                ON CPV_CPO_KEY = CPO_KEY
		                AND CPV_CFT_ID = CFZ_CFT_ID
		           LEFT OUTER JOIN CRMLEISTUNGSBEREICHVORGABEN ON CLV_CFT_ID = CFZ_CFT_ID
		                  AND CLV_CLB_ID = CPD_CLB_ID
		GROUP BY CPO_CAD_KEY
		) Daten WHERE CPO_CAD_KEY = CAD_KEY)) AS CPO_POTENTIALATU
		,
		SUM((SELECT Umsatz
		from (
		SELECT DISTINCT ROUND(SUM(CPD_STATUS*COALESCE(CPV_BETRAG,CLV_BETRAG,0)*CFZ_ANZAHL),0) AS Umsatz, CPO_CAD_KEY
		FROM CRMPOTENTIAL
		          INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY
		           LEFT OUTER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY
		           LEFT OUTER JOIN CRMPOTENTIALVORGABEN
		                ON CPV_CPO_KEY = CPO_KEY
		                AND CPV_CFT_ID = CFZ_CFT_ID
		           LEFT OUTER JOIN CRMLEISTUNGSBEREICHVORGABEN ON CLV_CFT_ID = CFZ_CFT_ID
		                  AND CLV_CLB_ID = CPD_CLB_ID
		GROUP BY CPO_CAD_KEY
		) Daten WHERE CPO_CAD_KEY = CAD_KEY)) AS CPO_POTENTIALGESAMT';
$SQL .= ' FROM CRMAdressen ';
$SQL .= ' LEFT OUTER JOIN Kontakte ON CAD_KON_KEY = KON_KEY';
$SQL .= ' LEFT OUTER JOIN CRMAkquisestand ON CAQ_CAD_KEY = CAD_KEY';

If(($Recht3713&2)==0)
{
	$SQL .= ' WHERE KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();
}

//$SQL .= ' WHERE KON_KEY in (7291,5922)';
$SQL .= ' GROUP BY CAD_KON_KEY, KON_NAME1, KON_NAME2, CAD_STATUS';
$SQL .= ' ORDER BY KON_NAME1, KON_NAME2, CAD_STATUS';

$rsCAD = awisOpenRecordset($con,$SQL);
$rsCADZeilen = $awisRSZeilen;
$Zeile = 30;
if($Ausdruck)
{			
	$Zeile = 30;
	$Report->_pdf->SetFont('Arial','B',12);	
	$Report->_pdf->SetXY(20,$Zeile);
	$Report->_pdf->Cell(60,5,$AWISSprachKonserven['KON']['KON_NAME1'],0,0,'C',0);
	$Report->_pdf->Cell(30,5,$AWISSprachKonserven['Wort']['Kunden'],0,0,'C',0);
	$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGEATU'],0,0,'C',0);
	//$Report->_pdf->Cell(35,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGEATU'],0,0,'L',0);
	$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGE'],0,0,'C',0);
	$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_POTENTIALATU'],0,0,'C',0);
	$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_POTENTIALGESAMT'],0,0,'C',0);
}
else
{
	awis_FORM_ZeileStart();
	$Link = './crm_Main.php?cmdAktion=Uebersicht';
	$Link .= '&CADSort=KON_NAME1'.((isset($_GET['CADSort']) AND ($_GET['CADSort']=='KON_NAME1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME1'],240,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Kunden'],150,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['CPO_FAHRZEUGEATU'],150,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['CPO_FAHRZEUGE'],150,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['CPO_POTENTIALATU'],150,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['CPO_POTENTIALGESAMT'],150,'',$Link);
	awis_FORM_ZeileEnde();
}

$LetzterKontakt='';

for($CADZeile=0;$CADZeile<$rsCADZeilen and $CADZeile<$rsCADZeilen;$CADZeile++)
{
	if($rsCAD['CAD_KON_KEY'][$CADZeile]!=$LetzterKontakt)
	{
		if($LetzterKontakt!=0)
		{
			if($Ausdruck)			
			{	
				$Zeile+=5;				
				$Report->_pdf->Line(20,$Zeile,$Report->SeitenBreite()-20,$Zeile);
				$Report->_pdf->SetFont('Arial','',10);					
				$Zeile+=2;				
				$Report->_pdf->SetXY(20,$Zeile);							
				$Report->_pdf->Cell(60,5,'',0,0,'L',0);				
				$Report->_pdf->Cell(30,5,$Summe['KUNDEN'],0,0,'R',0);				
				$Report->_pdf->Cell(40,5,$Summe['CPO_FAHRZEUGEATU'],0,0,'R',0);
				$Report->_pdf->Cell(40,5,$Summe['CPO_FAHRZEUGE'],0,0,'R',0);
				$Report->_pdf->Cell(40,5,$Summe['CPO_POTENTIALATU'],0,0,'R',0);
				$Report->_pdf->Cell(40,5,$Summe['CPO_POTENTIALGESAMT'],0,0,'R',0);				
				
				$Report->NeueSeite(0,1);
				$Zeile = 30;
				$Report->_pdf->SetFont('Arial','B',12);	
				$Report->_pdf->SetXY(20,$Zeile);
				$Report->_pdf->Cell(60,5,$AWISSprachKonserven['KON']['KON_NAME1'],0,0,'C',0);
				$Report->_pdf->Cell(30,5,$AWISSprachKonserven['Wort']['Kunden'],0,0,'C',0);
				$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGEATU'],0,0,'C',0);
				//$Report->_pdf->Cell(35,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGEATU'],0,0,'L',0);
				$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_FAHRZEUGE'],0,0,'C',0);
				$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_POTENTIALATU'],0,0,'C',0);
				$Report->_pdf->Cell(40,5,$AWISSprachKonserven['CPO']['CPO_POTENTIALGESAMT'],0,0,'C',0);
			}
			else 
			{				
				awis_FORM_Trennzeile();
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('#TEXT','',20,220,false,($CADZeile%2),'','','T','');
				awis_FORM_Erstelle_ListenFeld('#KUNDEN',$Summe['KUNDEN'],20,150,false,($CADZeile%2),'','','N0T','');
				awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGEATU',$Summe['CPO_FAHRZEUGEATU'],20,150,false,($CADZeile%2),'','','N0T','');
				awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGE',$Summe['CPO_FAHRZEUGE'],20,150,false,($CADZeile%2),'','','N0T','');
				awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALATU',$Summe['CPO_POTENTIALATU'],20,150,false,($CADZeile%2),'','','N0T','');
				awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALGESAMT',$Summe['CPO_POTENTIALGESAMT'],20,150,false,($CADZeile%2),'','','N0T','');
				awis_FORM_ZeileEnde();
			}
		}

		$Summe['KUNDEN']=0;
		$Summe['CPO_FAHRZEUGEATU']=0;
		$Summe['CPO_FAHRZEUGE']=0;
		$Summe['CPO_POTENTIALATU']=0;
		$Summe['CPO_POTENTIALGESAMT']=0;

		if($Ausdruck)
		{
			$Report->_pdf->SetFont('Arial','B',10);					
			$Zeile+=10;
			$Report->_pdf->SetXY(20,$Zeile);
			$Report->_pdf->Cell(60,5,$rsCAD['KON_NAME1'][$CADZeile].', '.$rsCAD['KON_NAME2'][$CADZeile],0,0,'L',0);
			$LetzterKontakt = $rsCAD['CAD_KON_KEY'][$CADZeile];			
		}
		else
		{
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListenFeld('#KON_NAME1',$rsCAD['KON_NAME1'][$CADZeile].', '.$rsCAD['KON_NAME2'][$CADZeile],20,1000,false,($CADZeile%2),'background-color:Black;color:white;','','T','');
			$LetzterKontakt = $rsCAD['CAD_KON_KEY'][$CADZeile];
			awis_FORM_ZeileEnde();
		}

		$SQL = 'SELECT COUNT(*) AS ANZAHL, BLOCK';
		$SQL .= ' FROM (';
		$SQL .= ' SELECT CASE WHEN CWV_DATUM < TRUNC(SYSDATE) THEN 1';
		$SQL .= '					  WHEN CWV_DATUM = TRUNC(SYSDATE) THEN 2';
		$SQL .= '					  ELSE 3 END AS BLOCK';
		$SQL .= ' FROM CRMWiedervorlagen';
		$SQL .= ' INNER JOIN CRMAdressen ON CWV_CAD_KEY = CAD_KEY';
		$SQL .= ' WHERE CWV_KON_KEY = 0'.$rsCAD['CAD_KON_KEY'][$CADZeile];
		$SQL .= ') DATEN';
		$SQL .= ' GROUP BY BLOCK';
		$SQL .= ' ORDER BY BLOCK';
		$rsCWV = awisOpenRecordset($con,$SQL);
		$rsCWVZeilen = $awisRSZeilen;

		if($Ausdruck)
		{
			$Report->_pdf->SetFont('Arial','',10);					
			$Zeile+=5;
			$Report->_pdf->SetXY(20,$Zeile);							
			$Report->_pdf->Cell(60,5,$AWISSprachKonserven['Wort']['Wiedervorlagen'],0,0,'L',0);							
			
			for($CWVZeile=0;$CWVZeile<$rsCWVZeilen;$CWVZeile++)
			{
				$Zeile+=5;
				$Report->_pdf->SetXY(20,$Zeile);											

				switch ($rsCWV['BLOCK'][$CWVZeile])
				{
					case 1:
						$Report->_pdf->Cell(60,5,$AWISSprachKonserven['Wort']['UeberfaelligeWV'].':',0,0,'L',0);													
						break;
					case 2:
						$Report->_pdf->Cell(60,5,$AWISSprachKonserven['Wort']['FaelligeWV'].':',0,0,'L',0);																			
						break;
					case 3:
						$Report->_pdf->Cell(60,5,$AWISSprachKonserven['Wort']['ZukuenftigeWV'].':',0,0,'L',0);																									
						break;
				}

				$Report->_pdf->Cell(30,5,(isset($rsCWV['ANZAHL'][$CWVZeile])?$rsCWV['ANZAHL'][$CWVZeile]:'0'),0,0,'L',0);																	
			}			
		}
		else
		{
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wiedervorlagen'],180);
			awis_FORM_ZeileEnde();


			for($CWVZeile=0;$CWVZeile<$rsCWVZeilen;$CWVZeile++)
			{
				awis_FORM_ZeileStart();

				switch ($rsCWV['BLOCK'][$CWVZeile])
				{
					case 1:
						awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['UeberfaelligeWV'].':',300);
						break;
					case 2:
						awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['FaelligeWV'].':',300);
						break;
					case 3:
						awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['ZukuenftigeWV'].':',300);
						break;
				}

				awis_FORM_Erstelle_TextLabel((isset($rsCWV['ANZAHL'][$CWVZeile])?$rsCWV['ANZAHL'][$CWVZeile]:'0'),100);

				awis_FORM_ZeileEnde();
			}
		}
	}

	switch ($rsCAD['CAD_STATUS'][$CADZeile])
	{
		case 'K':
			$Text = $AWISSprachKonserven['Wort']['Kunden'];
			break;
		case 'I':
			$Text = $AWISSprachKonserven['Wort']['Interessenten'];
			break;
		case 'S':
			$Text = $AWISSprachKonserven['Wort']['Stillgelegt'];
			break;
		case 'O':
			$Text = $AWISSprachKonserven['Wort']['Inkasso'];
			break;
		case 'R':
			$Text = $AWISSprachKonserven['Wort']['Rechnungslegung'];
			break;
		case 'L':
			$Text = $AWISSprachKonserven['Wort']['Loeschen'];
			break;
		case 'B':
			$Text = $AWISSprachKonserven['Wort']['Bonuskunde'];
			break;
		case 'P':
			$Text = $AWISSprachKonserven['Wort']['Passiv'];
			break;
		default:
			$Text = $AWISSprachKonserven['Wort']['NichtZugeordnet'];
			break;

	}

	if($Ausdruck)
	{
		$Report->_pdf->SetFont('Arial','',10);					
		$Zeile+=5;
		$Report->_pdf->SetXY(20,$Zeile);							
		$Report->_pdf->Cell(60,5,$Text,0,0,'L',0);				
		$Report->_pdf->Cell(30,5,$rsCAD['KUNDEN'][$CADZeile],0,0,'R',0);
		$Report->_pdf->Cell(40,5,$rsCAD['CPO_FAHRZEUGEATU'][$CADZeile],0,0,'R',0);
		$Report->_pdf->Cell(40,5,$rsCAD['CPO_FAHRZEUGE'][$CADZeile],0,0,'R',0);
		$Report->_pdf->Cell(40,5,$rsCAD['CPO_POTENTIALATU'][$CADZeile],0,0,'R',0);
		$Report->_pdf->Cell(40,5,$rsCAD['CPO_POTENTIALGESAMT'][$CADZeile],0,0,'R',0);				
	}
	else
	{
		awis_FORM_ZeileStart();

		awis_FORM_Erstelle_ListenFeld('#TEXT',$Text,20,220,false,($CADZeile%2),'','','T','');
		awis_FORM_Erstelle_ListenFeld('#KUNDEN',$rsCAD['KUNDEN'][$CADZeile],20,150,false,($CADZeile%2),'','','N0T','');
		awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGEATU',$rsCAD['CPO_FAHRZEUGEATU'][$CADZeile],20,150,false,($CADZeile%2),'','','N0T','');
		awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGE',$rsCAD['CPO_FAHRZEUGE'][$CADZeile],20,150,false,($CADZeile%2),'','','N0T','');
		awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALATU',$rsCAD['CPO_POTENTIALATU'][$CADZeile],20,150,false,($CADZeile%2),'','','N0T','');
		awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALGESAMT',$rsCAD['CPO_POTENTIALGESAMT'][$CADZeile],20,150,false,($CADZeile%2),'','','N0T','');
	}

	$Summe['KUNDEN']+=$rsCAD['KUNDEN'][$CADZeile];
	$Summe['CPO_FAHRZEUGEATU']+=$rsCAD['CPO_FAHRZEUGEATU'][$CADZeile];
	$Summe['CPO_FAHRZEUGE']+=$rsCAD['CPO_FAHRZEUGE'][$CADZeile];
	$Summe['CPO_POTENTIALATU']+=$rsCAD['CPO_POTENTIALATU'][$CADZeile];
	$Summe['CPO_POTENTIALGESAMT']+=$rsCAD['CPO_POTENTIALGESAMT'][$CADZeile];

	if($Ausdruck)
	{
		$Report->_pdf->SetFont('Arial','',8);
		$Report->_pdf->SetXY(10,$Report->SeitenHoehe()-10);
		$Report->_pdf->Cell(($Report->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L',0);
		$Report->_pdf->Cell(($Report->SeitenBreite()-20)/3,5,'Uebersicht',0,0,'C',0);
		$Report->_pdf->Cell(($Report->SeitenBreite()-20)/3,5,date('d.m.Y'),0,0,'R',0);
	}
	else
	{
		awis_FORM_ZeileEnde();
	}
}

if(!$Ausdruck)
{
	// Die letzte Summe
	awis_FORM_Trennzeile();
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('#TEXT','',20,220,false,($CADZeile%2),'','','T','');
	awis_FORM_Erstelle_ListenFeld('#KUNDEN',$Summe['KUNDEN'],20,150,false,($CADZeile%2),'','','N0T','');
	awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGEATU',$Summe['CPO_FAHRZEUGEATU'],20,150,false,($CADZeile%2),'','','N0T','');
	awis_FORM_Erstelle_ListenFeld('#CPO_FAHRZEUGE',$Summe['CPO_FAHRZEUGE'],20,150,false,($CADZeile%2),'','','N0T','');
	awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALATU',$Summe['CPO_POTENTIALATU'],20,150,false,($CADZeile%2),'','','N0T','');
	awis_FORM_Erstelle_ListenFeld('#CPO_POTENTIALGESAMT',$Summe['CPO_POTENTIALGESAMT'],20,150,false,($CADZeile%2),'','','N0T','');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}



if($Ausdruck)
{		
	$Report->Anzeigen();
}
else
{
	awis_FORM_Trennzeile();
	awis_FORM_SchaltflaechenStart();
	awis_FORM_Schaltflaeche('href','cmdDrucken','./crm_Uebersicht.php?Ausdruck','/bilder/drucker.png');
	//awis_FORM_Schaltflaeche('href', 'cmdDruckKonditionsblatt', './crm_DruckKonditionsblatt.php?DruckArt=1&CADKEY=0'.$AWIS_KEY1, '/bilder/pdf_gross.png', $AWISSprachKonserven['Wort']['lbl_KonditionsblattDrucken'], '.');	
	awis_FORM_SchaltflaechenEnde();
}
?>