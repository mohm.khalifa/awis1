<?php
//require_once 'awisDatenbank.inc';
//require_once 'awisBenutzer.inc';
//require_once 'awisFormular.inc';
require_once 'awisMailer.inc';
require_once 'awisBerichte.inc';

class awis_abgleich_funktionen
{
	/* werden im Konstruktor initialisiert */
	private $_Form;						// lokale Instanz der Klasse awisFormular
	private $_DB;						// lokale Instanz der DB-Klasse
	private $_CurPos;					// lokale Instanz der CursorPos
	private $_SprachKonserven;          // lokale Instanz von $AWISSprachKonserven
	//private $_Param;					// Parameter-Array => alle Felder benutzten Werte zu den Feldern sind hier drin
	
	const _LabelBreite = 200;			// Standard-Breite der Label (links vor Wert)
    const _UniqueDelimiter = '|*~*|';   // eindeutiges Trennzeichen (z.B. f�r gruppierte DropDown-Felder)
    const _SuchenInSBI = 1;
    const _SuchenInSBK = 2;
	
    public function __construct()
	{
		// globalen Klassenobjekte reinziehen
		global $Form;
		global $DB;
		global $AWISSprachKonserven;
		global $AWISCursorPosition;

		// globalen Klassenobjekte �ber lokale Instanzen verf�gbar machen
		$this->_Form = $Form;
		$this->_DB = $DB;
		$this->_CurPos = $AWISCursorPosition;
        
		// Textkonserven laden
		$TextKonserven = array();
		$TextKonserven[]=array('SBI','%');
		$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
		$TextKonserven[]=array('Liste', 'lst_JaNein');
		$this->_SprachKonserven = $Form->LadeTexte($TextKonserven);		
	}

    public function Label_Filter($Anzeige)
    {
        $this->_Form->ZeileStart();
        if ($Anzeige)
        {
            $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['Filter'] . ':', 800, 'font-weight:bolder');
        }
        else
        {
            $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['FilterGewaehlt'] . ':', 800, 'font-weight:bolder');
        }
        $this->_Form->ZeileEnde();    
    }

    public function Label_Abgleich()
    {
        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['DSAbgleichSingle'] . ':', 800, 'font-weight:bolder');
        $this->_Form->ZeileEnde();    
    }    
    
    public function Feld_AuswahlSpeichern(&$SuchParam, $Anzeigen)
    {
        if ($Anzeigen)
        {
            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['AUSWAHLSPEICHERN'] . ':', self::_LabelBreite);
            $this->_Form->Erstelle_Checkbox('*SBA_AUSWAHLSPEICHERN', ($SuchParam['SPEICHERN'] == 'on'? 'on': ''), 1 , true, 'on');
            $this->_Form->ZeileEnde();    
        }
    }
    
    public function Feld_UmsatzTagDatumVonBis(&$SuchParam, $Aendern)
    {
    	$Schriftart = "font-family:'Courier New',sans-serif";
    	$this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_UMSATZTAG'] . ' ' . $this->_SprachKonserven['SBI']['von'], self::_LabelBreite);
        $this->_Form->Erstelle_TextFeld('*SBA_DATUMVON', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['DatumVon']: ''), 10, 120, $Aendern, '', $Schriftart, '', 'D', 'L');
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['bis'], 30);
        $this->_Form->Erstelle_TextFeld('*SBA_DATUMBIS', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['DatumBis']: ''), 10, 120, $Aendern, '', $Schriftart, '', 'D', 'L');
        $this->_Form->ZeileEnde();
    }
    
    public function Feld_DatumVonBis(&$SuchParam, $Aendern)
    {
        $Schriftart = "font-family:'Courier New',sans-serif";
    	$this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_DATUM'] . ' ' . $this->_SprachKonserven['SBI']['von'], self::_LabelBreite);
        $this->_Form->Erstelle_TextFeld('*SBK_DATUMVON', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['DatumVon']: ''), 10, 120, $Aendern, '',$Schriftart,'', 'D', 'L');
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['bis'], 30);
        $this->_Form->Erstelle_TextFeld('*SBK_DATUMBIS', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['DatumBis']: ''),10, 120, $Aendern, '', $Schriftart, '', 'D', 'L');
        $this->_Form->ZeileEnde();
    }

    public function Feld_AbgleichVonBis(&$SuchParam, $Aendern)
    {
    	$Schriftart = "font-family:'Courier New',sans-serif";
    	$this->_Form->ZeileStart();
    	$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_DATUM'] . ' ' . $this->_SprachKonserven['SBI']['von'], self::_LabelBreite);
    	$this->_Form->Erstelle_TextFeld('*SBA_ABGLEICHDATUMVON', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['AbgleichDatumVon']: ''), 10, 120, $Aendern, '',$Schriftart,'', 'D', 'L');
    	$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['bis'], 30);
    	$this->_Form->Erstelle_TextFeld('*SBA_ABGLEICHDATUMBIS', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['AbgleichDatumBis']: ''),10, 120, $Aendern, '', $Schriftart, '', 'D', 'L');
    	$this->_Form->ZeileEnde();
    }
    /***
     * $Quelle = 1 => TuevGesellschaftenDaten   => TGD_FIL_ID
     * $Quelle = 2 => TuevKassenDaten           => TKD_FIL_ID
     * $Quelle = 3 => TuevAbgleich              => TGD_FIL_ID || TKD_FIL_ID
     */
    public function Feld_SelectFilId(&$SuchParam, $Aendern, $Quelle)
    {
    	if ($Quelle == 1)
    	{
	    	$SQL = '';
	    	$SQL =  'select distinct sbi.sbi_fil_id,regexp_replace(rpad(sbi.sbi_fil_id, 3,\'�\'),\'�\',\'&nbsp;\') || \' - \' || nvl(fil.fil_bez,\'Unbekannt\') from sbgeldentsorger sbi ' .
	    			'left join filialen fil on fil.fil_id = sbi.sbi_fil_id ' .
	    			'where sbi.sbi_abgeglichen = 0 order by sbi.sbi_fil_id';
	        $this->_Form->ZeileStart();  
	        $Breite = self::_LabelBreite;
	        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_FIL_ID'] . ':', $Breite);
	        $this->_Form->Erstelle_SelectFeld('*SBA_FILNR', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['FilId']: ''), 400, $Aendern,$SQL,$this->_SprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),"font-family:'Courier New',sans-serif");
	      
	        $this->_Form->ZeileEnde();   
    	}  
    	else if($Quelle == 2)
    	{
    		$SQL = '';
    		$SQL =  'select distinct sbk.sbk_fil_id,regexp_replace(rpad(sbk.sbk_fil_id, 3,\'�\'),\'�\',\'&nbsp;\') || \' - \' || nvl(fil.fil_bez,\'Unbekannt\') from sbkassendaten sbk ' .
    				'left join filialen fil on fil.fil_id = sbk.sbk_fil_id ' .
    				'where sbk.sbk_abgeglichen = 0 order by sbk.sbk_fil_id';
    		$this->_Form->ZeileStart();
    		$Breite = self::_LabelBreite;
    		$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_FIL_ID'] . ':', $Breite);
    		$this->_Form->Erstelle_SelectFeld('*SBK_FILNR', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['FilId']: ''), 400, $Aendern,$SQL,$this->_SprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),"font-family:'Courier New',sans-serif");
    		 
    		$this->_Form->ZeileEnde();
    	}  
    	else 
    		{
    			$SQL = '';
    			$SQL =  'SELECT distinct nvl(fil_sbi.fil_id,fil_sbk.fil_id) AS sba_fil_id, '.
    					'(CASE WHEN fil_sbi.fil_bez is null then fil_sbk.fil_id || \' - \' || fil_sbk.fil_bez '.
    					'ELSE fil_sbi.fil_id || \' - \' || fil_sbi.fil_bez '.
    					'END) as sba_fil_bez '.
    					'FROM sbabgleich sba  '.
    					'LEFT JOIN sbgeldentsorger sbi '.
    					'ON sbi.sbi_key = sba.sba_sbi_key '.
    					'LEFT JOIN sbkassendaten sbk '.
    					'ON sbk.sbk_key = sba.sba_sbk_key '.
    					'LEFT JOIN filialen fil_sbi '.
    					'ON fil_sbi.fil_id = sbi.sbi_fil_id  '.
    					'LEFT JOIN filialen fil_sbk '.
    					'ON fil_sbk.fil_id = sbk.sbk_fil_id '.
    					'WHERE sba_userdat >= (sysdate - 366)  '.
    					'ORDER BY sba_FIL_ID';
    			$this->_Form->ZeileStart();
    			$Breite = self::_LabelBreite;
    			$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_FIL_ID'] . ':', $Breite);
    			$this->_Form->Erstelle_SelectFeld('*SBA_FILNR', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['FilId']: ''), 400, $Aendern,$SQL,$this->_SprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),"font-family:'Courier New',sans-serif");
    			 
    			$this->_Form->ZeileEnde();
    		} 
    }
    
    public function Feld_Safebag(&$SuchParam, $Aendern,$Quelle)
    {
    	if($Quelle == 2)
    	{
    		$Schriftart = "font-family:'Courier New',sans-serif";
	    	$this->_Form->ZeileStart();
	        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_SAFEBAGNR'] . ':', self::_LabelBreite);
	        $this->_Form->Erstelle_TextFeld('*SBK_SAFEBAG', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['Safebag']: ''), 10, 200, $Aendern, '', $Schriftart, '', 'T', 'L');
	        $this->_Form->ZeileEnde();  
    	}
    	if($Quelle == 3)
    	{
    		$Schriftart = "font-family:'Courier New',sans-serif";
    		$this->_Form->ZeileStart();
    		$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_SAFEBAGNR'] . ':', self::_LabelBreite);
    		$this->_Form->Erstelle_TextFeld('*SBA_SAFEBAG', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['Safebagnr']: ''), 10, 200, $Aendern, '', $Schriftart, '', 'T', 'L');
    		$this->_Form->ZeileEnde();
    	}      
    }

    public function Feld_Entsorgt(&$SuchParam, $Aendern)
    {
    		$Daten = $this->_SprachKonserven['Liste']['lst_JaNein'];
    		$Daten = explode('|',$Daten);
    		$Schriftart = "font-family:'Courier New',sans-serif";
    		$this->_Form->ZeileStart();
    		$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['ENTSORGTTEXT'] . ':', self::_LabelBreite);
    		$this->_Form->Erstelle_SelectFeld('*SBK_ENTSORGT', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['Entsorgt']: ''), 400, $Aendern, '', $this->_SprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),$Schriftart);
    		$this->_Form->ZeileEnde();
    }
    
    
    public function Feld_SelectAbgleichArt(&$SuchParam, $Aendern,$Sachbearbeiter)
    {
    	$Schriftart = "font-family:'Courier New',sans-serif";
    	$this->_Form->ZeileStart();
        $SQL =  'SELECT sat_key, sat_kennung || \' - \' || sat_beschreibung ' . 
                'FROM sbabgleichart ';
		if($Sachbearbeiter)
		{
        	$SQL .='where sat_key in (2,4) ';
		}
        	$SQL .=	'ORDER BY sat_kennung';   
        	
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['AbgleichArt'] . ':', self::_LabelBreite);
        $this->_Form->Erstelle_SelectFeld('*SBA_ABGLEICHARTKEY', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['AbgleichArtKey']: ''), 400, $Aendern, $SQL, $this->_SprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),$Schriftart);
        $this->_Form->ZeileEnde();
    }    
    
    public function Feld_SelectSBIoderSBK(&$SuchParam, $Aendern)
    {
    	$Schriftart = "font-family:'Courier New',sans-serif";
    	$this->_Form->ZeileStart();
        $SQL =  'select ' . self::_SuchenInSBI . ' as key, \'' . $this->_SprachKonserven['SBI']['Geldentsorger'] . '\' as wert from dual ' . 
                'union ' .
                'select ' . self::_SuchenInSBK . ' as key, \'' . $this->_SprachKonserven['SBI']['Kasse'] . '\' as wert from dual';
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SuchenIn'] . ':', self::_LabelBreite);
        $this->_Form->Erstelle_SelectFeld('*SBI_SUCHENINKEY', ((($SuchParam['SPEICHERN'] == 'on') or (! $Aendern))? $SuchParam['SuchenInKey']: ''), 400, $Aendern, $SQL,'','','','','','','',array(),$Schriftart); //, $this->_SprachKonserven['Wort']['txt_BitteWaehlen']);
        $this->_Form->ZeileEnde();        
    }
    
    public function Feld_Bemerkung($NurTuev = 0, $TGD_KEY = '')
    {
        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['Bemerkung'] . ':', self::_LabelBreite, 'font-weight:bolder');        
        if ($NurTuev == 1)
        {
            $this->_Form->Erstelle_Textarea('SBA_BEMERKUNG_OBEN', '', 400, 74, 3, true);
        }
        else
        {
            $this->_Form->Erstelle_Textarea('SBA_BEMERKUNG_UNTEN', '', 400, 74, 3, true);
        }
       $this->_Form->ZeileEnde();         
    }
    
    public function ErstelleSBIBedingung($SuchParam)
    {
        $Bedingung = '';
	    if (isset($SuchParam['DatumVon']) AND (!empty($SuchParam['DatumVon'])))
	    {
	        $Bedingung .= ' AND (sbi_umsatztag >= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['DatumVon']) . ')';
	    }
	    
	    
	    if (isset($SuchParam['AbgleichDatumVon']) AND (!empty($SuchParam['AbgleichDatumVon'])))
	    {
	    	$Bedingung .= ' AND (sbi_umsatztag >= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['AbgleichDatumVon']) . ')';
	    }
	    
	    if (isset($SuchParam['DatumBis']) AND (!empty($SuchParam['DatumBis'])))
	    {
	        $Bedingung .= ' AND (sbi_umsatztag <= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['DatumBis']) . ')';
	    }
	    
	    if (isset($SuchParam['AbgleichDatumBis']) AND (!empty($SuchParam['AbgleichDatumBis'])))
	    {
	    	$Bedingung .= ' AND (sbi_umsatztag <= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['AbgleichDatumBis']) . ')';
	    }
    	if (isset($SuchParam['FilId']))
    	{
    		if ($SuchParam['FilId'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'] and $SuchParam['FilId']!= '' and $SuchParam['FilId']!= 0)
    	    {            
    	    	$Bedingung .= ' AND (sbi_fil_id = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['FilId']) . ')';
            }
    	}

    	if (isset($SuchParam['Safebagnr']))
    	{
    		if ($SuchParam['Safebagnr'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'] and $SuchParam['Safebagnr']!= '' and $SuchParam['Safebagnr']!= 0)
    		{
    			$Bedingung .= ' AND (sbi_safebagnr = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['Safebagnr']) . ')';
    		}
    	}
    
        return $Bedingung;
    }

    public function ErstelleSBKBedingung($SuchParam)
    {
        $Bedingung = '';
        
	    if (isset($SuchParam['DatumVon']) AND (!empty($SuchParam['DatumVon'])))
	    {
	        $Bedingung .= ' AND (to_char(sbk_datum,\'DD.MM.YYYY\') >= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['DatumVon']) . ')';
	    }
	    
	    if (isset($SuchParam['AbgleichDatumVon']) AND (!empty($SuchParam['AbgleichDatumVon'])))
	    {
	    	$Bedingung .= ' AND (to_char(sbk_datum,\'DD.MM.YYYY\') >= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['AbgleichDatumVon']) . ')';
	    }
	    
	    if (isset($SuchParam['DatumBis']) AND (!empty($SuchParam['DatumBis'])))
	    {
	        $Bedingung .= ' AND (to_char(sbk_datum,\'DD.MM.YYYY\') <= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['DatumBis']) . ')';
	    }
	    if (isset($SuchParam['AbgleichDatumBis']) AND (!empty($SuchParam['AbgleichDatumBis'])))
	    {
	    	$Bedingung .= ' AND (to_char(sbk_datum,\'DD.MM.YYYY\') <= ' . $this->_DB->FeldInhaltFormat('D', $SuchParam['AbgleichDatumBis']) . ')';
	    }
        
    	if (isset($SuchParam['FilId']) AND (!empty($SuchParam['FilId'])))
    	{
            if ($SuchParam['FilId'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'])
            {
            	$Bedingung .= ' AND (sbk_fil_id = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['FilId']) . ')';
            }
    	}        
    	if (isset($SuchParam['Safebag']) AND (!empty($SuchParam['Safebag'])))
    	{
    	    if ($SuchParam['Safebag'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'])
    	    {            
                $Bedingung .= ' AND (sbk_safebagnr = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['Safebag']) . ')';
            }
    	}
    	if (isset($SuchParam['Entsorgt']) )
    	{
    		if ($SuchParam['Entsorgt'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'])
    		{
    			$Bedingung .= ' AND (entsorgt = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['Entsorgt']) . ')';
    		}
    	}
    	if (isset($SuchParam['Safebagnr']))
    	{
    		if ($SuchParam['Safebagnr'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'] and $SuchParam['Safebagnr']!= '' and $SuchParam['Safebagnr']!= 0)
    		{
    			$Bedingung .= ' AND (sbk_safebagnr = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['Safebagnr']) . ')';
    		}
    	}
      
        
        return $Bedingung;        
    }    
    
    public function ErstelleTAGBedingung($SuchParam)
    {
        $Bedingung = '';
        /*  Um die TGD- und TKG- ErstellBedingung-Funktionen nutzen zu k�nnen,
         *  m�ssen die assoziativen Arrays die gleichen Namen haben,
         *  wie in den entsprechenden Funktionen
         */
        if ($SuchParam['SuchenInKey'] == self::_SuchenInSBI)
        {
            $Bedingung = $this->ErstelleSBIBedingung($SuchParam);
        }
        elseif ($SuchParam['SuchenInKey'] == self::_SuchenInSBK)
        {
            $Bedingung = $this->ErstelleSBKBedingung($SuchParam);
        }
        
        if (isset($SuchParam['AbgleichArtKey']) AND (!empty($SuchParam['AbgleichArtKey'])))
        {
            if ($SuchParam['AbgleichArtKey'] != $this->_SprachKonserven['Wort']['txt_BitteWaehlen'])
            {            
                $Bedingung .= ' AND (sat_key = ' . $this->_DB->FeldInhaltFormat('N0', $SuchParam['AbgleichArtKey']) . ')';
            }
        }
            
        return $Bedingung;
    }
    
    public function Felder_AbgleichSBI(&$rs)
    {
        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_FIL_ID'].':',100);
        //. ' / ' . $this->_SprachKonserven['TAG']['BelegDatum'] . ':', 180);
        $this->_Form->Erstelle_TextFeld('FILID', $rs->FeldInhalt('SBI_FIL_ID') . ' - '. $rs->FeldInhalt('FIL_BEZ') , 0, 260, false, '', '', '', 'T', 'L');
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_UMSATZTAG'] . ':', 100);
        $this->_Form->Erstelle_TextFeld('UMSATZTAG', $this->_Form->Format('D',$rs->FeldInhalt('SBI_UMSATZTAG')), 0, 300, false, '', '', '', 'T', 'L');
        $this->_Form->ZeileEnde();
        
        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_SAFEBAGNR'].':' , 100);
        //. ' / ' . $this->_SprachKonserven['TAG']['Preis'] . ':'
        $this->_Form->Erstelle_TextFeld('SBNR', $rs->FeldInhalt('SBI_SAFEBAGNR'), 0, 259, false,'color: red;', '', '', 'T', 'L');
        //. ' / ' . $this->_Form->Format('N2', $rs->FeldInhalt('TGD_PREIS'))
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_BETRAG'] . ':', 100);
        //$this->_SprachKonserven['TAG']['KfzKennzKurz'] . ' / ' . 
        $this->_Form->Erstelle_TextFeld('BETRAG', $rs->FeldInhalt('SBI_BETRAG'), 0, 72, false, '', '', '', 'T', 'R');
        $this->_Form->Erstelle_TextLabel('&#8364',1);
        //$rs->FeldInhalt('TGD_KFZKENNZ') . ' / ' .
        //$this->_Form->Erstelle_TextLabel($this->_SprachKonserven['TAG']['TuevNr'] . ':', 70);
        //$this->_Form->Erstelle_TextFeld('TUEVNR', $rs->FeldInhalt('TGD_TUEVNR') . ' - ' . $rs->FeldInhalt('TUE_BEZ'), 0, 300, false, '', '', '', 'T', 'L');
        $this->_Form->ZeileEnde();
    }
    
    public function Felder_AbgleichSBK(&$rs)
    {
        $this->_Form->ZeileStart();
        //$this->_Form->Erstelle_TextLabel('', 180+260);
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_FIL_ID'] . ':', 120);
        $this->_Form->Erstelle_TextFeld('FILNR', $rs->FeldInhalt('SBK_FIL_ID') . ' - ' . $rs->FeldInhalt('FIL_BEZ'), 0, 260, false, '', '', '', 'T', 'L');
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_UMSATZTAG'], 120);
        $this->_Form->Erstelle_TextFeld('DATUM', $this->_Form->Format('D', $rs->FeldInhalt('SBK_DATUM')), 0, 300, false, '', '', '', 'T', 'L');
        $this->_Form->ZeileEnde();
        
        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_SAFEBAGNR'] . ':', 120);
        $this->_Form->Erstelle_TextFeld('SAFEBAGNR', $rs->FeldInhalt('SBK_SAFEBAGNR'), 0, 260, false,'color: red;', '', '', 'T', 'L');
        $this->_Form->Erstelle_TextLabel($this->_SprachKonserven['SBI']['SBI_SOLLBETRAG'] . ':', 120);
        $this->_Form->Erstelle_TextFeld('BETRAG', $rs->FeldInhalt('SBK_BETRAG'), 0, 72, false, '', '', '', 'T', 'R');
        $this->_Form->Erstelle_TextLabel('&#8364',1);
        $this->_Form->ZeileEnde();
    }    
    
    public function AbgleichSpeichern($SBI_KEY, $SBK_KEY, $AbgleichArt, $Bemerkung,$Freigabe, $Benutzer)
    {
        $this->_DB->SetzeBindevariable('SBA', 'var_N0_sba_sbi_key', $SBI_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
        $this->_DB->SetzeBindevariable('SBA', 'var_N0_sba_sbk_key', $SBK_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
        $this->_DB->SetzeBindevariable('SBA', 'var_N0_sba_freigabe', $Freigabe, awisDatenbank::VAR_TYP_GANZEZAHL);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sat_kennung', $AbgleichArt, awisDatenbank::VAR_TYP_TEXT);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sba_bemerkung', $Bemerkung, awisDatenbank::VAR_TYP_TEXT);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sba_user', $Benutzer, awisDatenbank::VAR_TYP_TEXT);
            
        $SQL =  'insert into sbabgleich (' .
                'sba_sbi_key, sba_sbk_key, sba_sat_key, sba_bemerkung,sba_freigabe, sba_user, sba_userdat' .
                ') values (:var_N0_sba_sbi_key, :var_N0_sba_sbk_key, ' .
                '(select sat.sat_key from sbabgleichart sat where sat.sat_kennung = :var_T_sat_kennung), ' .
                ':var_T_sba_bemerkung,:var_N0_sba_freigabe, :var_T_sba_user, sysdate)';                        

        $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('SBA'));
		$this->_Form->DebugAusgabe(1, $SQL);        
        
        $this->_DB->SetzeBindevariable('SBI', 'var_N0_sbi_key', $SBI_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
		$SQL =  'update sbgeldentsorger set ' .
                'sbi_abgeglichen = 1 ' .
                'where sbi_key = :var_N0_sbi_key';

                $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('SBI'));
		$this->_Form->DebugAusgabe(1, $SQL);

        $this->_DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $SBK_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
		$SQL =  'update sbkassendaten set ' .
                'sbk_abgeglichen = 1 ' .
                'where sbk_key = :var_N0_sbk_key';

                $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('SBK'));
		$this->_Form->DebugAusgabe(1, $SQL);
    }
    
    public function AbgleichSpeichernP($SBK_KEY, $Bemerkung, $Benutzer,$AbgleichArt)
    {
        $this->_DB->SetzeBindevariable('SBA', 'var_N0_sba_sbi_key', 0, awisDatenbank::VAR_TYP_GANZEZAHL);
        $this->_DB->SetzeBindevariable('SBA', 'var_N0_sba_sbk_key', $SBK_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sat_kennung',$AbgleichArt, awisDatenbank::VAR_TYP_TEXT);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sba_bemerkung', $Bemerkung, awisDatenbank::VAR_TYP_TEXT);
        $this->_DB->SetzeBindevariable('SBA', 'var_T_sba_user', $Benutzer, awisDatenbank::VAR_TYP_TEXT);
    	
        $SQL =  'insert into sbabgleich (' .
                    'sba_sbi_key, sba_sbk_key, sba_sat_key, sba_bemerkung,sba_freigabe, sba_user, sba_userdat' .
                    ') values (:var_N0_sba_sbi_key, :var_N0_sba_sbk_key, ' .
                    '(select sat.sat_key from sbabgleichart sat where sat.sat_kennung = :var_T_sat_kennung), ' .
                    ':var_T_sba_bemerkung,1, :var_T_sba_user, sysdate)';
        
                $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('SBA'));
$this->_Form->DebugAusgabe(1, $SQL);

        $this->_DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $SBK_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
		$SQL =  'update sbkassendaten set ' .
                'sbk_abgeglichen = 1 ' .
                'where sbk_key = :var_N0_sbk_key';

                $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('SBK'));
$this->_Form->DebugAusgabe(1, $SQL);
    }    
 
    public function MailSenden($SBK_KEY,$SBI_KEY,$Abgleichart,$Benutzer)
    {        
        try
        {             	
        	$HinweisText = array();                                         // Meldugen, welche Berichte usw. erstellt wurden (in Array zwischenspeichern, da instanzieren der Berichteklasse in der Schleife den Output schluckt)       
            $Mail = new awisMailer($this->_DB, $Benutzer);                  // Mailer-Klasse instanzieren
        	$Werkzeuge = new awisWerkzeuge();
            $Absender = 'geldentsorgung@de.atu.eu';                     // Absender der Mails TODO
            $Empfaenger = '';                                               // Empf�nger der Mails => wird dynamisch in Schleife belegt
            $EmpfaengerCC = '';                      // CC-Empf�nger
            $EmpfaengerBCC = '';
            if($Abgleichart == 'AF')
            {
            	$SQL = 'SELECT mvt_bereich, mvt_betreff, mvt_text ';
            	$SQL .= 'FROM mailversandtexte ';
            	$SQL .= 'WHERE mvt_bereich = \'SBA_SACHB_SOLL\'';
            	
            	$rsSBA = $this->_DB->RecordSetOeffnen($SQL);
            	

            	$SQL3 = 'SELECT * ';
            	$SQL3 .= 'FROM sbgeldentsorger ';
            	$SQL3 .= 'WHERE sbi_key = :var_N0_sbi_key';
            	 
            	$this->_DB->SetzeBindevariable('SBI', 'var_N0_sbi_key', $SBI_KEY , awisDatenbank::VAR_TYP_GANZEZAHL);
            	
            	$rsSBI = $this->_DB->RecordSetOeffnen($SQL3,$this->_DB->Bindevariablen('SBI'));
            	 
            	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
            	{
            		$SQL = 'select kon_key,kon_name1,kon_name2,kko_wert from filialebenenrollenzuordnungen'.
            				' inner join kontakte on frz_kon_key = kontakte.kon_key' .
            				' inner join kontaktekommunikation  on kko_kon_key = kon_key and kko_kot_key = 7'.
            				' where frz_xxx_key = :var_N0_frz_xxx_key'.
            				' and frz_fer_key = 35'.
            				' and trunc(frz_gueltigab) <= trunc(sysdate)'.
            				' and trunc(frz_gueltigbis) >= trunc(sysdate)';
            		 
            		$this->_DB->SetzeBindevariable('KON', 'var_N0_frz_xxx_key', $rsSBI->FeldInhalt('SBI_FIL_ID') , awisDatenbank::VAR_TYP_GANZEZAHL);
            		$rsKon = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KON'));
            		 
            		while(! $rsKon->EOF())
            		{
            			$Empfaenger .= ','.$rsKon->FeldInhalt('KKO_WERT');
            			$rsKon->DSWeiter();
            		}
            		$Empfaenger = ltrim($Empfaenger,',');
            		$EmpfaengerBCC = 'geldentsorgung@de.atu.eu';
            	}
            	else
            	{
            		$Empfaenger = 'tobias.schaeffler@de.atu.eu';
            		$EmpfaengerBCC = 'stefan.oppl@de.atu.eu';
            	}
            	
            	$Betreff = $rsSBA->FeldInhalt('MVT_BETREFF');    // Betreff der Mail
            	
            	$Text = '';
            	 
            	 
            	//Geldentsorger
            	$Text = str_replace('#SBI_FIL_ID#',$rsSBI->FeldInhalt('SBI_FIL_ID'),$rsSBA->FeldInhalt('MVT_TEXT'));
            	$Text = str_replace('#SBI_UMSATZTAG#',$this->_Form->Format('D',$rsSBI->FeldInhalt('SBI_UMSATZTAG')),$Text);
            	$Text = str_replace('#SBI_SAFEBAGNR#',$rsSBI->FeldInhalt('SBI_SAFEBAGNR'),$Text);
            	$Text = str_replace('#SBI_BETRAG#',$rsSBI->FeldInhalt('SBI_BETRAG'),$Text);
            	 
            	 
            }
            else if($Abgleichart == 'B')
            {
            	$SQL = 'SELECT mvt_bereich, mvt_betreff, mvt_text ';
            	$SQL .= 'FROM mailversandtexte ';
            	$SQL .= 'WHERE mvt_bereich = \'SBA_SACHB_DIFF\'';
            	 
            	$rsSBA = $this->_DB->RecordSetOeffnen($SQL);
            	 
            	
            	$SQL2 = 'SELECT * ';
            	$SQL2 .= 'FROM sbkassendaten ';
            	$SQL2 .= 'WHERE sbk_key = :var_N0_sbk_key';
            	 
            	$this->_DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $SBK_KEY , awisDatenbank::VAR_TYP_GANZEZAHL);
            	
            	$rsSBK = $this->_DB->RecordSetOeffnen($SQL2,$this->_DB->Bindevariablen('SBK'));
            	
            	$SQL3 = 'SELECT * ';
            	$SQL3 .= 'FROM sbgeldentsorger ';
            	$SQL3 .= 'WHERE sbi_key = :var_N0_sbi_key';
            	
            	$this->_DB->SetzeBindevariable('SBI', 'var_N0_sbi_key', $SBI_KEY , awisDatenbank::VAR_TYP_GANZEZAHL);
            	 
            	$rsSBI = $this->_DB->RecordSetOeffnen($SQL3,$this->_DB->Bindevariablen('SBI'));
            	
            	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
            	{
            		$SQL = 'select kon_key,kon_name1,kon_name2,kko_wert from filialebenenrollenzuordnungen'.
            				' inner join kontakte on frz_kon_key = kontakte.kon_key' .
            				' inner join kontaktekommunikation  on kko_kon_key = kon_key and kko_kot_key = 7'.
            				' where frz_xxx_key = :var_N0_frz_xxx_key'.
            				' and frz_fer_key = 35'.
            				' and trunc(frz_gueltigab) <= trunc(sysdate)'.
            				' and trunc(frz_gueltigbis) >= trunc(sysdate)';
            		
            		
            		 
            		$this->_DB->SetzeBindevariable('KON', 'var_N0_frz_xxx_key', $rsSBI->FeldInhalt('SBI_FIL_ID') , awisDatenbank::VAR_TYP_GANZEZAHL);
            		$rsKon = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KON'));
            		 
            		while(! $rsKon->EOF())
            		{
            			$Empfaenger .= ','.$rsKon->FeldInhalt('KKO_WERT');
            			$rsKon->DSWeiter();
            		}
            		$Empfaenger = ltrim($Empfaenger,',');
            		$EmpfaengerBCC = 'geldentsorgung@de.atu.eu';
            	}
            	else
            	{
            		$Empfaenger = 'tobias.schaeffler@de.atu.eu';
            		$EmpfaengerBCC = 'stefan.oppl@de.atu.eu';
            	}
            	
           
            	$Betreff = $rsSBA->FeldInhalt('MVT_BETREFF');
            	
            	 
            	$Text = '';
            	
            	
            	//Geldentsorger
            	$Text = str_replace('#SBI_FIL_ID#',$rsSBI->FeldInhalt('SBI_FIL_ID'),$rsSBA->FeldInhalt('MVT_TEXT'));
            	$Text = str_replace('#SBI_UMSATZTAG#',$this->_Form->Format('D',$rsSBI->FeldInhalt('SBI_UMSATZTAG')),$Text);
            	$Text = str_replace('#SBI_SAFEBAGNR#',$rsSBI->FeldInhalt('SBI_SAFEBAGNR'),$Text);
            	$Text = str_replace('#SBI_BETRAG#',$rsSBI->FeldInhalt('SBI_BETRAG'),$Text);
            	
            	
            	//Kassensatz
            	$Text = str_replace('#SBK_FIL_ID#',$rsSBK->FeldInhalt('SBK_FIL_ID'),$Text);
            	$Text = str_replace('#SBK_UMSATZTAG#',$this->_Form->Format('D',$rsSBK->FeldInhalt('SBK_DATUM')),$Text);
            	$Text = str_replace('#SBK_SAFEBAGNR#',$rsSBK->FeldInhalt('SBK_SAFEBAGNR'),$Text);
            	$Text = str_replace('#SBK_BETRAG#',$rsSBK->FeldInhalt('SBK_BETRAG'),$Text);
            
	
            }
            else if($Abgleichart == 'K')
            {
            	$SQL = 'SELECT mvt_bereich, mvt_betreff, mvt_text ';
            	$SQL .= 'FROM mailversandtexte ';
            	$SQL .= 'WHERE mvt_bereich = \'SBA_RUECK_FIL\'';
            	
            	$rsSBA = $this->_DB->RecordSetOeffnen($SQL);
            	
            	$SQL2 = 'SELECT * ';
            	$SQL2 .= 'FROM sbkassendaten ';
            	$SQL2 .= 'WHERE sbk_key = :var_N0_sbk_key';
            	
            	$this->_DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $SBK_KEY , awisDatenbank::VAR_TYP_GANZEZAHL);
            	 
            	$rsSBK = $this->_DB->RecordSetOeffnen($SQL2,$this->_DB->Bindevariablen('SBK'));
            	
            	$SQL3 ='SELECT kko_wert ';
            	$SQL3 .= 'FROM kontaktekommunikation ';
            	$SQL3 .= 'WHERE kko_kon_key = filialrolle(:var_NO_FIL_ID,25,sysdate,\'kon_key\') ';
            	$SQL3 .= 'AND kko_kot_key   = 7 ';
            	$SQL3 .= 'AND rownum        = 1';
            	
            	$this->_DB->SetzeBindevariable('FIL', 'var_NO_FIL_ID', $rsSBK->FeldInhalt('SBK_FIL_ID') , awisDatenbank::VAR_TYP_GANZEZAHL);
            	
            	$rsFIL = $this->_DB->RecordSetOeffnen($SQL3,$this->_DB->Bindevariablen('FIL'));
            	
            	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
            	{
            		$Empfaenger = 'gl-'.str_pad($rsSBK->FeldInhalt('SBK_FIL_ID'), 4, '0', STR_PAD_LEFT) . '@de.atu.eu';
            		$EmpfaengerCC = $rsFIL->FeldInhalt('KKO_WERT');
            		$EmpfaengerBCC = 'geldentsorgung@de.atu.eu';
            	}
            	else
            	{
            		$Empfaenger = 'tobias.schaeffler@de.atu.eu';
            		$EmpfaengerCC = $rsFIL->FeldInhalt('KKO_WERT');
            		$EmpfaengerBCC = 'stefan.oppl@de.atu.eu';
            	}
            	$Betreff = $rsSBA->FeldInhalt('MVT_BETREFF');    // Betreff der Mail
  	
            	$Text = '';
            	$Text = str_replace('#SBK_FIL_ID#',$rsSBK->FeldInhalt('SBK_FIL_ID'),$rsSBA->FeldInhalt('MVT_TEXT'));
            	$Text = str_replace('#SBK_UMSATZTAG#',$this->_Form->Format('D',$rsSBK->FeldInhalt('SBK_DATUM')),$Text);
            	$Text = str_replace('#SBK_SAFEBAGNR#',$rsSBK->FeldInhalt('SBK_SAFEBAGNR'),$Text);
            	$Text = str_replace('#SBK_BETRAG#',$rsSBK->FeldInhalt('SBK_BETRAG'),$Text);
            }


            	$Mail->AnhaengeLoeschen();
            	$Mail->LoescheAdressListe();
                $Mail->DebugLevel(0);   
                $Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);      
                $Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
                $Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
                $Mail->Absender($Absender);
                $Mail->Betreff($Betreff);
                $Mail->Text($Text, awisMailer::FORMAT_HTML, true);
                $Mail->SetzeBezug('SBA','');
                $Mail->SetzeVersandPrioritaet(15);
                $Mail->MailInWarteschlange();

        }
        catch(Exception $ex)
        {
        	$this->_Form->Fehler_Anzeigen('INTERN', $ex->getMessage(),'MELDEN',6,'201210170000');
        }        
    }    
    
    
}



