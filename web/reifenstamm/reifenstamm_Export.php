<?php
/**
 * Suchmaske f�r die Auswahl Reifenpflege Exporte
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130502
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('RST','%');
	$TextKonserven[]=array('RPP','%');
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('RET','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','gelistet');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15300=$AWISBenutzer->HatDasRecht(15300);		
	if ($Recht15300 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
    $Form->DebugAusgabe(1,$_POST);
    
    $Form->SchreibeHTMLCode('<form name=frmExport action=./reifenstamm_Main.php?cmdAktion=Export method=post enctype="multipart/form-data">');
    
	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RST_Export'));

	$Form->DebugAusgabe(1,$Param);
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['lbl_export'].':',200);
	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_EXPORT']);
	$Form->Erstelle_SelectFeld('*EXPORTART',$Param['SPEICHERN']=='on'?$Param['EXPORTART']:'',"220:200", true, '','','','','',$Daten);
	$Form->ZeileEnde();
	$AWISCursorPosition='sucEXPORTART';
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['gelistet'].':',200);
	$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_gelistet']);
	$Form->Erstelle_SelectFeld('*GELISTET',$Param['SPEICHERN']=='on'?$Param['GELISTET']:'',"220:200", true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_STATUS'].':',200);
	$Daten = explode('|',$AWISSprachKonserven['RST']['lst_RST_STATUS']);
	$Form->Erstelle_SelectFeld('*RST_STATUS',$Param['SPEICHERN']=='on'?$Param['RST_STATUS']:'',"220:200", true, '','-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ATUNR'].':',200);
	$Form->Erstelle_TextFeld('*RRP_ATUNR',($Param['SPEICHERN']=='on'?$Param['RRP_ATUNR']:''),6,75,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'].':',200);
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*RST_REH_KEY',($Param['SPEICHERN']=='on'?$Param['RST_REH_KEY']:'0'),"220:200",true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP'].':',200);
	$SQL = 'SELECT RET_ID, RET_TYP FROM REIFENTYP ORDER BY RET_ID';
	$Form->Erstelle_SelectFeld('*RST_TYP',($Param['SPEICHERN']=='on'?$Param['RST_TYP']:'0'),"220:200",true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_SelectFeld('*RST_TYP2',($Param['SPEICHERN']=='on'?$Param['RST_TYP2']:'0'),"220:200",true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->Erstelle_SelectFeld('*RST_TYP3',($Param['SPEICHERN']=='on'?$Param['RST_TYP3']:'0'),"220:200",true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if(($Recht15300&32)==32)
	{
		$Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
	}

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}

catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306171036");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306171037");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>