<?php
	global $HTTP_GET_VARS;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;

	$Rechte_OEN = awisBenutzerRecht($con,402);	
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER']));
	$SQL = "SELECT * FROM ArtikelStamm, TeileInfos WHERE TEI_KEY1 = AST_KEY AND TEI_KEY2=0" . $Artikel[0];

	$rsASTArt = awisOpenRecordset($con, $SQL);
	if($rsASTArt==FALSE)
	{
		awisErrorMailLink("liefart_ATUArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsASTArtZeilen=$awisRSZeilen;

	print "<table id=DatenTabelle width=100% border=1>";
	print "<td id=FeldBez>ATU Nummer</td>";
	print "<td id=FeldBez>Bezeichnung</td>";	
	print "<td id=FeldBez>VK</td>";	
	print "<td id=FeldBez>Bestand Lager</td>";	
	print "<td id=FeldBez>Bestand Filialen</td>";
	print "</tr>";
	
	for($ASTArt=0;$ASTArt<$rsASTArtZeilen;$ASTArt++)
	{
		print "<tr>";
	
		print "<td><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=OENummern&Key=" . $rsASTArt["AST_KEY"][$ASTArt] . " onmouseover=\"window.status='ATU Artikel';return true;\" onfocus=\"window.status='ATU Artikel';return true;\" onmouseout=window.status='' >" . $rsASTArt["AST_ATUNR"][$ASTArt] . "</a></td>";

		print "<td>" . $rsASTArt["AST_BEZEICHNUNGWW"][$ASTArt] . "<br>" . $rsASTArt["AST_BEZEICHNUNG"][$ASTArt] . "</a></td>";
		print "<td align=right>" . number_format($rsASTArt["AST_VK"][$ASTArt],2,",",".") . "</a></td>";

		$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID, ASI_KEY FROM ARTIKELSTAMMINFOS WHERE ASI_AIT_ID IN (10,11,12) AND ASI_AST_ATUNR ='" . $rsASTArt["AST_ATUNR"][$ASTArt] . "'");
		$rsASIZeilen = $awisRSZeilen;
		$LagerBest = awis_ArtikelInfo(10, $rsASI, $rsASTArt["AST_ATUNR"][$ASTArt], "ASI_WERT");
		$LagerBest += awis_ArtikelInfo(11, $rsASI, $rsASTArt["AST_ATUNR"][$ASTArt], "ASI_WERT");
		print "<td align=right>" . $LagerBest . "</td>";
		print "<td align=right>" . awis_ArtikelInfo(12, $rsASI, $rsASTArt["AST_ATUNR"][$ASTArt], "ASI_WERT") . "</td>";
		
		print "</tr>";
		
		// Gebrauchsnummern anzeigen
		if($HTTP_GET_VARS["GNr"]=='Ja')		
		{
			$rsGNr = awisOpenRecordset($con, "SELECT * FROM Gebrauchsnummern, TEILEINFOS WHERE GNR_KEY = TEI_KEY2 AND TEI_KEY1 = " . $rsLiefArt["LAR_KEY"][$LiefArt]);
			$rsGNRZeilen = $awisRSZeilen;
			
			if($rsGNRZeilen>0)
			{
 				print "<tr><td></td><td colspan=4>";
				print "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
				for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
				{
					print "<td>Gebrauchsnummer:</td>";
					print "<td><input name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
					print "</td>";
					print "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
					print "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
					print "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
				}
				print "</tr></table>";
				print "</td></tr>";
			}
			else
			{
				print "\n<!--Keine Gebrauchsnummern gefunden-->\n";
			}

			unset($rsGNr);
		}
	}


	print "</table>";
	
	unset($rsLiefArt);

	
?>