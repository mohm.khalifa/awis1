<?php
require_once('db.inc.php');
require_once('awis_forms.inc.php');
require_once('register.inc.php');
require_once('datenexporte_erstelleabfrage.php');

global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

$con = awisLogon();
$AWISBenutzer = new awisUser();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('XDE','%');
$TextKonserven[]=array('XAB','XAB_BEZEICHNUNG');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_export');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','AktuellesSortiment');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Feldname');
$TextKonserven[]=array('Wort','Bedingung');
$TextKonserven[]=array('Wort','EinmalAbfrage');
$TextKonserven[]=array('Wort','GespeicherteAbfrage');
$TextKonserven[]=array('Wort','BedingungHinzufuegen');
$TextKonserven[]=array('Wort','NeueAbfrage');
$TextKonserven[]=array('Wort','Ausgabe');
$TextKonserven[]=array('Wort','DateiOeffnenLink');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Liste','txtVerknuepfung');
$TextKonserven[]=array('Liste','Bedingungen');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

// Parameter
$Trenner="\t";
$AWIS_KEY1=$_GET['XDE_KEY'];
$AWIS_KEY2=$_GET['XAB_KEY'];
$AWISSprache = awis_BenutzerParameter($con,"AnzeigeSprache");

	// Abfrage erstellen
$SQL = ''.ErstelleAbfrage($con, $_GET['XDE_KEY'], $_GET['XAB_KEY']).'';

//awisExecute($con,"alter session set NLS_SORT='XGERMAN_DIN_CI'");
//awisExecute($con," alter session set NLS_COMP='LINGUISTIC'");

$rsDaten = awisOpenRecordset($con,$SQL);
if(!is_array($rsDaten))
{
	die();
}
$rsDatenZeilen = $awisRSZeilen;

//***********************************************************************
// Liste mit den Feldnamen aufbauen
//***********************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM ExportDatenFelder';
$SQL .= ' WHERE XDF_XDE_KEY = 0'.$_GET['XDE_KEY'];
$SQL .= ' ORDER BY XDF_SORTIERUNG';
$rsXDF = awisOpenRecordset($con, $SQL);
$rsXDFZeilen = $awisRSZeilen;

$FeldLabels = array();
for($XDFZeile=0;$XDFZeile<$rsXDFZeilen;$XDFZeile++)
{
	if($rsXDF['XDF_TEXTKONSERVE'][$XDFZeile]!='')
	{
		$FeldKonserve = explode(':',$rsXDF['XDF_TEXTKONSERVE'][$XDFZeile]);
		$Label = awis_LadeTextKonserven($con,array(array($FeldKonserve[0],$FeldKonserve[1])),$AWISSprache);
		$FeldLabels[$FeldKonserve[1]]=$Label[$FeldKonserve[0]][$FeldKonserve[1]];
		
		$FeldLabels[$FeldKonserve[1]]=strtr($FeldLabels[$FeldKonserve[1]],'-./+','____');
	}
	else
	{
		$Label = awis_LadeTextKonserven($con,array(array(substr($rsXDF['XDF_FELDNAME'][$XDFZeile],0,3),$rsXDF['XDF_FELDNAME'][$XDFZeile])),$AWISSprache);
		$FeldLabels[$rsXDF['XDF_FELDNAME'][$XDFZeile]]=$Label[substr($rsXDF['XDF_FELDNAME'][$XDFZeile],0,3)][$rsXDF['XDF_FELDNAME'][$XDFZeile]];
		$FeldLabels[$FeldKonserve[1]]=strtr($FeldLabels[$FeldKonserve[1]],'-./+','____');
	}
}

@ob_clean();

header("Cache-Control: no-cache, must-revalidate");
header("Expires: 01 Jan 2000"); 
header('Pragma: public');
header('Cache-Control: max-age=0');
header('Content-type: application/xml');
header('Content-Disposition: attachment; filename="datenexport.xml"');

$Felder = array_keys($rsDaten);
$Zeile = '';
echo '<?xml version="1.0" encoding="ISO-8859-15" standalone="yes"?>';
echo "<Datenexport>\r\n";
for($DatenZeile=0;$DatenZeile<$rsDatenZeilen;$DatenZeile++)
{	
	echo "<Zeile ID=\"".$DatenZeile."\">\r\n";
	
	$Zeile = '';
	foreach($Felder AS $FeldName)
	{
		//echo "<".$FeldName.">";
		echo "<".$FeldLabels[$FeldName].">";
		//$Zeile .= $Trenner.$rsDaten[$FeldName][$DatenZeile];			
		echo "<![CDATA[".$rsDaten[$FeldName][$DatenZeile].']]>';
		//echo "</".$FeldName.">";
		echo "</".$FeldLabels[$FeldName].">";
	}
	
	echo "</Zeile>\r\n";
}

echo "</Datenexport>\r\n";
?>