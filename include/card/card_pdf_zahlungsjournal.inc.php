<?php

define("FPDF_FONTPATH","/daten/include/card/fpdf/font/");
require_once("card/card_pdf.inc.php");


class PDF_ZJ extends PDF
{
	//Klassenvariablen
	
	//Kopfzeile
	var $dblKopfFeldAbstand;
	var $arrKopfSpaltenbreiten;
	var $arrKopfText;
	var $arrKopfUmrandung;
	var $arrKopfPos;
	var $arrKopfAusrichtung;
	var $dblSchriftgroesseHeader;
	var $dblZeilenhoeheHeader;
		
	var $dblLinienStaerke;
	var $intSeite1;
	var $dblZeilenbreiteDrittel;
	var $strDatum;
	var $strZeit;
	var $strTitel;
	var $strRechnungsSumme;
	
	var $dblYFusszeile;
	var $dblAbstandFusszeile;
	var $dblYGrenzwert;
	
	//Konstruktor
	function PDF_ZJ($strAusrichtung='P',$strEinheit='mm',$strFormat='A4',$dblKorrekturX=0,$dblKorrekturY=0,
	$dblSeitenrandLinks=10,$dblSeitenrandOben=10,$dblSeitenrandRechts=10,$dblSeitenrandUnten=10,$strAbrDat='',$strRechnungsSumme='')
	{
		//Klassenvariablen vorbelegen
		
		//Kopfzeile
		$this->dblKopfFeldAbstand=1;
		$this->arrKopfSpaltenbreiten=array(18,100,27,75,20,0);
		$this->arrKopfText=array("Datum","Firma","Betrag","Buchungstext","Kunden-Nr","Rechnungs-Nr");
		$this->arrKopfUmrandung=array(0,0,0,0,0,0);
		$this->arrKopfPos=array(0,0,0,0,0,1);
		$this->arrKopfAusrichtung=array('C','L','C','L','C','C');
		$this->dblSchriftgroesseHeader=12;
		$this->dblZeilenhoeheHeader=$this->ptinmm($this->dblSchriftgroesseHeader)+2;
		
		$this->dblLinienStaerke=0.2;
		$this->intSeite1=1;
		$this->intTitelzeile=1;
		$this->dblZeilenbreiteDrittel=(297-$dblSeitenrandLinks-$dblSeitenrandRechts)/3;
		//setlocale("LC_TIME","de_DE");
		$this->strDatum=strftime("%A, ").trim(strftime("%e.")).strftime(" %B %Y");
		$this->strZeit=strftime("%H:%M:%S");
		$this->strTitel="Zahlungsjournal Monat ".$strAbrDat;
		$this->strRechnungsSumme=$strRechnungsSumme;
		
		$this->dblYFusszeile=15;
		$this->dblAbstandFusszeile=0;
		$this->dblYGrenzwert=210-$this->dblYFusszeile-$this->dblAbstandFusszeile;
	
		//Aufruf des Konstruktors in der Klasse PDF
		PDF::PDF($strAusrichtung,$strEinheit,$strFormat,$dblKorrekturX,$dblKorrekturY,$dblSeitenrandLinks,
		$dblSeitenrandOben,$dblSeitenrandRechts,$dblSeitenrandUnten,$intAutomatSeite);
		//$this->SetAutoPageBreak(false);
		$this->AddPage();
		$this->SetLineWidth($this->dblLinienStaerke);
		$this->AliasNbPages();
	}
	
	function zeileOffene_Vorgaenge($dblZeilenhoehe,$dblFeldAbstand,$arrSpaltenbreiten,$arrText,$arrUmrandung,$arrPos,$arrAusrichtung,
	$arrHintergrund=1,$arrRandZwischen=1)
	{
		$this->SetFillColor(255,255,255);
		
		for($i=0;$i<count($arrSpaltenbreiten);$i++)
		{
			if($i<count($arrSpaltenbreiten)-1)
				$dblSpaltenbreite=$arrSpaltenbreiten[$i]-$dblFeldAbstand;
			else
				$dblSpaltenbreite=$arrSpaltenbreiten[$i];
			
			if(is_array($arrHintergrund))
				$intHintergrund=$arrHintergrund[$i];
			else
				$intHintergrund=0;  //Default (transparent) von Zellhintergrund
				
			$this->Cell($dblSpaltenbreite,$dblZeilenhoehe,$arrText[$i],$arrUmrandung[$i],$arrPos[$i],$arrAusrichtung[$i],$intHintergrund);
			
			if($i<count($arrSpaltenbreiten)-1)
			{
				if(is_array($arrRandZwischen))
				{
					$varRandZwischen=$arrRandZwischen[$i];
					$this->Cell($dblFeldAbstand,$dblZeilenhoehe,'',$varRandZwischen,0,'L',1);
				}
				else
					$this->SetX($this->GetX()+$dblFeldAbstand);
			}
		}
	}
	
	function Header()
	{
		$this->SetFillColor(192,192,192);
		$this->SetDrawColor(0,0,0);
		$this->SetTextColor(0,0,0);
		
		if($this->intSeite1)
		{
			//Titelzeile
			$dblSchriftgroesseTitel=20;
			$dblSchriftgroesseUnterTitel=16;
			
			$dblZeilenhoeheTitel=$this->ptinmm($dblSchriftgroesseTitel+1);
			$dblZeilenhoeheUnterTitel=$this->ptinmm($dblSchriftgroesseUnterTitel+1);
			
			
			$aktX=$this->GetX();
			$aktY=$this->GetY();
			
			$this->Cell(0,$dblZeilenhoeheTitel,"",'T',1,'L',1);
			
			$this->SetXY($aktX,$aktY);
			
			$this->SetFont('Helvetica','B',$dblSchriftgroesseTitel);
			
			$this->Cell(0,$dblZeilenhoeheTitel,$this->strTitel,0,1,'C');
			
			$aktX=$this->GetX();
			$aktY=$this->GetY();
			
			$this->Cell(0,$dblZeilenhoeheUnterTitel,"",'B',1,'L',1);
			
			$this->SetXY($aktX,$aktY);
						
			$this->SetFont('Helvetica','BI',$dblSchriftgroesseUnterTitel);
			
			$this->Cell(0,$dblZeilenhoeheUnterTitel,$this->strRechnungsSumme,0,1,'C');
			
			
			$this->intSeite1=0;
		}
		
		//Kopfzeile
		if($this->intTitelzeile)
		{
			$this->SetFont('Helvetica','B',$this->dblSchriftgroesseHeader);
			
			$this->zeileOffene_Vorgaenge($this->dblZeilenhoeheHeader,$this->dblKopfFeldAbstand,$this->arrKopfSpaltenbreiten,$this->arrKopfText,
			$this->arrKopfUmrandung,$this->arrKopfPos,$this->arrKopfAusrichtung);
		}
	}
	
	function Footer()
	{
		$this->SetY($this->dblYGrenzwert);
		$dblSchriftgroesse=10.5;
		$this->SetFont('Helvetica','I',$dblSchriftgroesse);
		$dblZeilenhoehe=$this->ptinmm($dblSchriftgroesse)+1;
		
		$this->SetLineWidth($this->dblLinienStaerke);
		$this->Cell(0,$dblZeilenhoehe,"",'B',1);
			
		$this->zeileOffene_Vorgaenge($dblZeilenhoehe,0,array($this->dblZeilenbreiteDrittel,$this->dblZeilenbreiteDrittel,0),
		array($this->strDatum,"Seite ".$this->PageNo().' von {nb}',$this->strZeit),
		array(0,0,0),array(0,0,1),array('L','C','R'));
	}
	
	
	//Pr�ft ob ein Zeilenumbruch erfolgen soll
	function pruefUmbruch($strRgDatAkt,$strRgDatVor,$strRgDatNach,$intKundennr=0,$strKundenNrAkt='',$strKundenNrVor='',$strKundenNrNach='')
	{
		$dblYPos=$this->GetY();
		if(!$intKundennr)
		{
			$dblYPos+=$this->dblHoeheName+$this->dblHoeheDaten;
			
			if($strRgDatAkt!=$strRgDatVor)	//Zeile DTA
			{
				$dblYPos+=$this->dblHoeheDTA;
			}
			
			if($strRgDatAkt!=$strRgDatNach)	//Zeile Summe
			{
				$dblYPos+=$this->dblHoeheSumme;
			}			
		}
		else
		{
			if($strKundenNrAkt!=$strKundenNrVor)
			{
				$dblYPos+=$this->dblHoeheName;
			}
			
			if($strKundenNrAkt!=$strKundenNrNach)
			{
				$dblYPos+=$this->dblHoeheSummeKd;
			}
			
			
			if($strKundenNrAkt!=$strKundenNrVor||$strRgDatAkt!=$strRgDatVor)
			{
				$dblYPos+=$this->dblHoeheDTA;
			}
			
			if($strKundenNrAkt!=$strKundenNrNach||$strRgDatAkt!=$strRgDatNach)
			{
				$dblYPos+=$this->dblHoeheSumme;
			}
		
			$dblYPos+=$this->dblHoeheDaten;
		}
		
		if($dblYPos>$this->dblYGrenzwert)
		{
			$this->AddPage();
		}
	}
	
	function pruefUmbruchSchleife($dblHoehe)
	{
		$dblYPos=$this->GetY();
		$dblYPos+=$dblHoehe;
	
		if($dblYPos>$this->dblYGrenzwert)
		{
			$this->AddPage();
		}
	}
}
?>