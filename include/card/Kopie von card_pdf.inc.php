<?php

define("FPDF_FONTPATH","/daten/include/card/fpdf/font/");
require_once("card/fpdf/fpdf.php");


class PDF extends FPDF
{
	//Korrektur des Gesamten PDF-Dokumentes
	var $dblKorrekturX;		
	var $dblKorrekturY;
	
	function PDF($strAusrichtung='P',$strEinheit='mm',$strFormat='A4',$dblKorrekturX=0,$dblKorrekturY=0,$dblSeitenrandLinks=10,$dblSeitenrandOben=10,$dblSeitenrandRechts=10)
	{
		if($strAusrichtung=='')
			$strAusrichtung='P';
		if($strEinheit=='')
			$strEinheit='mm';
		if($strFormat=='')
			$strFormat='A4';
		
		FPDF::FPDF($strAusrichtung,$strEinheit,$strFormat);
		
		$this->SetMargins($dblSeitenrandLinks,$dblSeitenrandOben,$dblSeitenrandRechts);
		
		$this->dblKorrekturX=$dblKorrekturX;
		$this->dblKorrekturY=$dblKorrekturY;
	}
	
	//Umrechnung von pt in mm
	
	function ptinmm($dblPunkte)
	{
		return $dblPunkte*2.54/72*10;
	}
	
	//Umrechnung von mm in pt
	
	function mminpt($dblPunkte)
	{
		return $dblPunkte/2.54*72/10;
	}
	
	function briefkopf_atu($dblKorrPosX=0,$dblKorrPosY=0)
	{
		//Korrektur des Briefkopfes
	
		$dblKorrPosX+=$this->dblKorrekturX;
		$dblKorrPosY+=$this->dblKorrekturY;
		
		$strBild='/home/daten/include/card/atu_logo.jpg';
		
		$dblPosBildX=142;			//Bildposition X in mm
		$dblPosBildY=16;			//Bildposition Y in mm
		
		$intOrigPixBreite=1838;	//Bildbreite Originalbild in Pixel
		$intOrigPixHoehe=661;	//Bildhoehe Originalbild in Pixel
		
		$dblmmBreite=52;		//Gew�nschte Bildbreite in mm
		$dblmmHoehe=$dblmmBreite*$intOrigPixHoehe/$intOrigPixBreite;	//Bildhoehe in mm
				
		$this->Image($strBild,$dblPosBildX+$dblKorrPosX,$dblPosBildY+$dblKorrPosY,$dblmmBreite,$dblmmHoehe);
		
		$dblAktPosTextX=149;
		$dblZeilenhoehePt=9;
		
		
		$this->SetFont('Helvetica','',10.5);
		
		$dblAktPosTextY=38;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'GmbH & Co.');
		$dblAktPosTextY=42;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Kommanditgesellschaft');
		
		$this->SetFontSize(7.5);
		$dblAktPosTextY=47;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'92637 Weiden');
		
		$dblAktPosTextY=51.5;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Telefon +49 961 306-5830');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Telefon +49 961 306-5840');
		
		
		$dblAktPosTextY+=4.5;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Kommanditgesellschaft, Sitz');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Weiden i. d. OPf., Registergericht');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Weiden i. d. OPf., HRA 1312');
		
		$dblAktPosTextY+=4.5;
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Pers�nlich haftende Gesellschafterin:');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'ATU Auto-Teile-Unger GmbH');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Sitz Weiden i. d. OPf.');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Registergericht Weiden i.d. OPf.');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'HRB 745');
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX,$dblAktPosTextY+$dblKorrekturY,'Gesch�ftsf�hrer:');
		$this->Text($dblAktPosTextX+$dblKorrPosX+20.3,$dblAktPosTextY+$dblKorrekturY,'Peter Unger');
		
		
		$dblAktPosTextY+=$this->ptinmm($dblZeilenhoehePt);
		$this->Text($dblAktPosTextX+$dblKorrPosX+20.3,$dblAktPosTextY+$dblKorrekturY,'Werner Aichinger');
		
		$this->SetLineWidth(0.1);
		$this->Line(18+$dblKorrekturX,27.8+$dblKorrekturY,141+$dblKorrekturX,27.8+$dblKorrekturY);
		$this->Line(18+$dblKorrekturX,91+$dblKorrekturY,193+$dblKorrekturX,91+$dblKorrekturY);
		
		
		$this->SetFontSize(9);
		
		$this->Text(20+$dblKorrPosX,45+$dblKorrekturY,'ATU - 92633 Weiden i. d. OPf.');
		$this->Line(20+$dblKorrekturX,45.5+$dblKorrekturY,64.5+$dblKorrekturX,45.5+$dblKorrekturY);
		
	}
	
	function adressfeld($strAdresstext,$dblAdrPosX,$dblAdrPosY,$dblZellenhoehe=0,$dblZellenbreite=0,$dblKorrPosX=0,$dblKorrPosY=0)
	{
		//Korrektur des adressfeldes
	
		$dblKorrPosX+=$this->dblKorrekturX;
		$dblKorrPosY+=$this->dblKorrekturY;
		
		$this->SetXY($dblAdrPosX+$dblKorrPosX,$dblAdrPosY+$dblKorrPosY);
		$this->MultiCell($dblZellenbreite,$dblZellenhoehe,$strAdresstext);
	}
	
	//Gibt die aktuelle Position aus
	
	function position()
	{
		$this->SetFont('Helvetica','',16);
	
		$strAusgabe=$this->GetX()." / ".$this->GetY();
		
		$this->SetFillColor(255,255,0);
		$this->SetDrawColor(0,0,255);
		$this->SetTextColor(0,0,255);
		
		$this->Cell(60,10,$strAusgabe,1,0,'C',1);
	}
	
	function zeileOffene_Vorgaenge($dblZeilenhoehe,$dblFeldAbstand,$arrSpaltenbreiten,$arrText,$arrUmrandung,$arrPos,$arrAusrichtung)
	{
		
		
		for($i=0;$i<count($arrSpaltenbreiten);$i++)
		{
			if($i<count($arrSpaltenbreiten)-1)
				$dblSpaltenbreite=$arrSpaltenbreiten[$i]-$dblFeldAbstand;
			else
				$dblSpaltenbreite=$arrSpaltenbreiten[$i];
			
		
			$this->Cell($dblSpaltenbreite,$dblZeilenhoehe,$arrText[$i],$arrUmrandung[$i],$arrPos[$i],$arrAusrichtung[$i]);
			
			if($i<count($arrSpaltenbreiten)-1)
				$this->SetX($this->GetX()+$dblFeldAbstand);
		}
	}
	
	function Header()
	{
		$this->position();
	}
	
	function Footer()
	{
		$this->position();
	}
}
?>