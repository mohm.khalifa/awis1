<?php
/**
 * awisMDEArtikelstamm.inc
 *
 * Export der MDE Artikelstammdaten
 *
 * @author Patrick Gebhardt
 * @copyright ATU - Auto-Teile-Unger
 * @version 20100419
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisMDEArtikelstammProtokoll.inc');


class awisMDEArtikelstammExport
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;
		
	/**
	 * Importdateiname der Termine
	 * @var string
	 */
	private $_ExportDateiPfad = '/daten/daten/pcedv11/Daten/MDE/AWIS/';
	
	
	/**
	 * Emailbenachrichtigung
	 * @var string
	 **/
	private $_ExportDateiName = 'artikelstamm';
	
	
	/**
	 * Trennzeichen f�r Exportdatei
	 */
	private $_TZ = ';';

	/**
	 * Zeilenabschlusszeichen
	 */
	private $_Zeilenabschluss;
	
	
	
	/**
	 * Filialen
	 * @var array
	 */
	 private $_Filialen = '';
	 
	 
	 /**
	  * awisWerkzeuge
	  *
	  */
	 private $_AWISWerkzeuge;
	 
	/**
	 * Debug
	 */
	 private $_Debug;
	
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='', $Level = 10)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
		
		$this->AWISWerkzeuge = new awisWerkzeuge();
		
		$this->_Debug = new awisMDEArtikelstammProtokoll($Benutzer);
		$this->_Debug->DebugLevel($Level);
		
		$this->setZeilenabschlusszeichen(chr(13).chr(10));
	}
	
	/**
	 * setzt das Zeilenabschlusszeichen
	 */
	public function setZeilenabschlusszeichen($Trennzeichen)
	{
		$this->_Zeilenabschluss = $Trennzeichen;
	}
	
	
	/**
	 *
	 * Schr�nkt den Exportlauf auf bestimmte Filialen ein
	 *
	 */
	
	public function setFilialen($Filialen = '')
	{
		
		if (strlen($Filialen)>0)
		{
			$this->_Filialen = $Filialen;
		}
	}
	
	/**
	 *
	 * Holt die relevanten Filialen
	 *
	 */
	
	private function getFilialen()
	{
		if ($this->_Filialen!='')
		{
			return $this->_Filialen;
		}
		else
		{
			return false;
		}
		
	}
	
	
	/**
	 * Exportiert die Stammdaten
	 *
	 */
    public function erstelleStammdaten()
    {
        try
        {
        	
        	$SQLFil = 'select FIL_ID, LAN_CODE from V_FILIALEN_AKTUELL';
        	
        	if (($this->getFilialen())!==false)
        	{
        		$SQLFil .= ' WHERE FIL_ID in ('.$this->getFilialen().')';
        		
        	}
        	else
        	{
        		$this->_Debug->debugAusgabe('Exportiere f�r alle Filialen', 10);
        	}
        	
        	$SQLFil .= ' ORDER by 1 ASC';
        	$rsFil = $this->_DB->RecordSetOeffnen($SQLFil,$this->_DB->Bindevariablen('FIL',true));
        	
      
        	
        	while (!$rsFil->EOF())
        	{
        		$this->_Debug->debugAusgabe('Exportiere Filiale: ' . $rsFil->FeldInhalt('FIL_ID'),100);
        		$SQLArtikel  ='select fib.fib_FIL_ID, DATEN.AST_ATUNR, FIB_BESTAND, FIB_VK, DATEN.EAN, DATEN.AST_KENNUNG, ASP_BEZEICHNUNGWW   from filialbestand fib left join (SELECT';
				$SQLArtikel .='   ast.ast_atunr, ast.ast_kennung,  WGR_ID,   WUG_ID, ';
				$SQLArtikel .='   coalesce(ASP_BEZEICHNUNG,coalesce(AST_BEZEICHNUNGWW,ast_bezeichnung)) AS ASP_BEZEICHNUNGWW,';

 				$SQLArtikel .='   (select listagg(EAN_NUMMER,\'|\') within group(order by ean_key)   from EANNUMMERN';
				$SQLArtikel .='   inner join teileinfos on tei_key2 = ean_key and tei_ity_id2 = \'EAN\' ';
				$SQLArtikel .='   inner join artikelstamm east on tei_key1 = east.ast_key ';
				$SQLArtikel .='   where east.ast_key = ast.ast_key group by tei_key1) as ean';
				$SQLArtikel .=' FROM';
				$SQLArtikel .='   Artikelstamm ast';
				$SQLArtikel .=' LEFT OUTER JOIN ARTIKELSPRACHEN DE';
				$SQLArtikel .=' ON';
				$SQLArtikel .='   AST_ATUNR      = ASP_AST_ATUNR';
				$SQLArtikel .=' AND ASP_LAN_CODE = ' . $this->_DB->WertSetzen('MDE', awisDatenbank::VAR_TYP_TEXT, $rsFil->FeldInhalt('LAN_CODE'));
				$SQLArtikel .=' INNER JOIN Warenuntergruppen';
				$SQLArtikel .=' ON';
				$SQLArtikel .='   AST_WUG_KEY = WUG_KEY';
				$SQLArtikel .=' INNER JOIN Warengruppen';
				$SQLArtikel .=' ON';
				$SQLArtikel .='   WUG_WGR_ID = WGR_ID';
				$SQLArtikel .='   ) daten';
				$SQLArtikel .='   on FIB.FIB_AST_ATUNR = DATEN.AST_ATUNR';
				$SQLArtikel .='   where wgr_id <> \'00\' and wug_id <> \'000\' and  wgr_id <> \'12\' and coalesce(AST_KENNUNG,\'0\') not in (\'E\',\'S\' ) ';
				$SQLArtikel .='   and fib_fil_id = ' .$this->_DB->WertSetzen('MDE', awisDatenbank::VAR_TYP_GANZEZAHL, $rsFil->FeldInhalt('FIL_ID'));
				$SQLArtikel .=' ';
        		$rsArtikel = $this->_DB->RecordSetOeffnen($SQLArtikel,$this->_DB->Bindevariablen('MDE',true));
				
        		if ($rsArtikel->AnzahlDatensaetze()>0)
        		{

        			//Temp Datei erstellen.
        			$Datei = fopen($this->_ExportDateiPfad.$this->_ExportDateiName."_".$rsFil->FeldInhalt('FIL_ID')."_".date("Ymd").'.temp',"w+");
        						
        			while (!$rsArtikel->EOF())
        			{
        				$Zeile = $rsArtikel->FeldInhalt('AST_ATUNR') . $this->_TZ;
        				      				
        				//Doppelte Leerzeichen vernichten..
        				$Zeile .= preg_replace('#\s+#', ' ', $rsArtikel->FeldInhalt('ASP_BEZEICHNUNGWW')) . $this->_TZ;
        				$Zeile .= ($rsArtikel->FeldInhalt('EAN')!=''?$rsArtikel->FeldInhalt('EAN'):'0000000000000') . $this->_TZ;
        				$Zeile .= $rsArtikel->FeldInhalt('FIB_VK') . $this->_TZ;
        				$Zeile .= $rsArtikel->FeldInhalt('AST_KENNUNG') . $this->_TZ;
        				$Zeile .= $rsArtikel->FeldInhalt('LEER') . $this->_TZ;
        				$Zeile .= $rsArtikel->FeldInhalt('FIB_BESTAND');
        				 
        				
        				//$Zeile = preg_replace("/[^A-Za-z0-9 \| \; \. ]/", "",$Zeile);
        				 
        				 
        				$Zeile .=  $this->_Zeilenabschluss;;
        				 
        				fwrite($Datei, $Zeile);
        				 
        			
        				$rsArtikel->DSWeiter();
        			}
        			fclose($Datei);
        			
        			//Auf .txt umbennen
        			rename($this->_ExportDateiPfad.$this->_ExportDateiName."_".$rsFil->FeldInhalt('FIL_ID')."_".date("Ymd").'.temp', $this->_ExportDateiPfad.$this->_ExportDateiName."".$rsFil->FeldInhalt('FIL_ID').'.txt');
        		}
        		    		
        		$rsFil->DSWeiter();
        	}
        }
        catch (awisException $ex)
        {
            echo $this->_DB->LetzterSQL().PHP_EOL;
            echo 'AWIS-Fehler:'.$ex->getMessage().PHP_EOL;
        }
        catch (Exception $ex)
        {
            echo 'allg. Fehler:'.$ex->getMessage().PHP_EOL;
        }
        
    }
}



?>
