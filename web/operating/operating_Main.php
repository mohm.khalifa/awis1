<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $AWISBenutzer;
global $AWISSprache;
global $con;
global $awisRSZeilen;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

include ("ATU_Header.php");	// Kopfzeile

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Operating');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating'].'</title>';
?>
</head>
<body>
<?php

$RechteStufe = awisBenutzerRecht($con,3100);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'Operating',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$SQL="SELECT * FROM WEBOPERATING WHERE BITAND(WEO_RECHTEBIT,".$RechteStufe.")=WEO_RECHTEBIT order by weo_sortierung";

$rsCOMGruppe = awisOpenRecordset($con, $SQL);
$rsCOMGruppeZeilen = $awisRSZeilen;

print "<table border=0 width=100%><tr><th id=FeldBez></th><th align=left id=FeldBez>Programmpunkt</th><th align=left id=FeldBez>Beschreibung</th></tr>";

for($COMGruppeZeile=0;$COMGruppeZeile<$rsCOMGruppeZeilen;$COMGruppeZeile++)
{
	print "<tr><td></td><td><a href='./".$rsCOMGruppe['WEO_WEBSEITE'][$COMGruppeZeile]."'>".$rsCOMGruppe['WEO_BEZEICHNUNG'][$COMGruppeZeile]."</a></td><td>".$rsCOMGruppe['WEO_BESCHREIBUNG'][$COMGruppeZeile]."</td></tr><p>";	
}

print "</table>";

	print "<br><hr><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

	awislogoff($con);

?>
</body>
</html>

