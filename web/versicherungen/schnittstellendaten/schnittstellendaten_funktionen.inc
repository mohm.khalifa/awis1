<?php
require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisAenderungshistorie.inc';

/**
 * Created by PhpStorm.
 * User: oppl_s
 * Date: 20.02.2019
 * Time: 11:00
 */
class schnittstellendaten_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $Recht69000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;
    public $Aenderungshistorie;

    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht69000 = $this->AWISBenutzer->HatDasRecht(69000);
        $this->_EditRecht = (($this->Recht69000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_69000'));
        $this->AWISWerkzeug = new awisWerkzeuge();
        $this->Aenderungshistorie = new awisAenderungshistorie($this->DB, $this->AWISBenutzer);

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('VGD', '%');
        $TextKonserven[] = array('XAH', '%');
        $TextKonserven[] = array('VBA', 'VBA_LST_SB');
        $TextKonserven[] = array('VBA', 'VBA_STATUS');
        $TextKonserven[] = array('Wort', 'Quelle');
        $TextKonserven[] = array('Liste', 'lst_Deckungsquellen');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'Dateiname');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_Schnittstellendaten');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_69000', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht69000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'VGD'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND KEY = ' . $this->DB->WertSetzen('VGD', 'N0', $AWIS_KEY1);
        } else {
            if ($this->Param['QUELLE'] == 1) {
                // GDV-Deckungsabfragen
                if (isset($this->Param['VGD_KFZ_KZ']) and $this->Param['VGD_KFZ_KZ'] != '') {
                    $Bedingung .= ' AND KFZ_KZ ' . $this->DB->LikeOderIst($this->Param['VGD_KFZ_KZ'], awisDatenbank::AWIS_LIKE_UPPER, 'VGD');
                }
            } else {
                // Direktvermittlungen (z.B. HUK / WGV ...)
                if (isset($this->Param['VGD_KFZ_KZ']) and $this->Param['VGD_KFZ_KZ'] != '') {
                    $Bedingung .= ' AND KFZ_KZ ' . $this->DB->LikeOderIst($this->Param['VGD_KFZ_KZ'], awisDatenbank::AWIS_LIKE_UPPER, 'VGD');
                }
            }
        }

        if ($this->Param['QUELLE'] == 1) {
            // GDV-Deckungsabfragen
            $Bedingung .= ' AND QUELLTABELLE = ' . $this->DB->WertSetzen('VGD', 'T', 'VGD', false);
        } else {
            // Direktvermittlungen
            $Bedingung .= ' AND QUELLTABELLE = ' . $this->DB->WertSetzen('VGD', 'T', 'VBA', false);
        }

        return $Bedingung;
    }
}

?>