<?php
global $AWISCursorPosition;
global $AWISBenutzer;

$Form = new awisFormular();


// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('XBN','%');
$TextKonserven[]=array('KAB','KAB_ABTEILUNG');
$TextKonserven[]=array('XBL','XBL_LOGIN');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','txt_OhneZuordnung');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht1=$AWISBenutzer->HatDasRecht(1);

echo "<br>";

echo "<form name=frmSuche method=post action=./benutzerverwaltung_Main.php?cmdAktion=Details>";


/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('XBNSuche'));

$Form->Formular_Start();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_NAME'].':',190);
$Form->Erstelle_TextFeld('*XBN_NAME',($Param['SPEICHERN']=='on'?$Param['XBN_NAME']:''),20,200,true);
$AWISCursorPosition='sucXBN_NAME';
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_VORNAME'].':',190);
$Form->Erstelle_TextFeld('*XBN_VORNAME',($Param['SPEICHERN']=='on'?$Param['XBN_VORNAME']:''),20,200,true);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['XBL']['XBL_LOGIN'].':',190);
$Form->Erstelle_TextFeld('*XBL_LOGIN',($Param['SPEICHERN']=='on'?$Param['XBL_LOGIN']:''),20,200,true);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_STATUS'].':',190);
$KateogrieAlt = explode("|",$AWISSprachKonserven['XBN']['lst_XBN_STATUS']);
$Form->Erstelle_SelectFeld('*XBN_STATUS',($Param['SPEICHERN']=='on'?$Param['XBN_STATUS']:''),100,true,'','','A','','',$KateogrieAlt,'');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['KAB']['KAB_ABTEILUNG'].':',190);
$SQL = 'SELECT KAB_KEY, KAB_ABTEILUNG FROM Kontakteabteilungen ORDER BY KAB_ABTEILUNG';
$Form->Erstelle_SelectFeld('*KAB_KEY',($Param['SPEICHERN']=='on'?$Param['KAB_KEY']:''),100,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',array($AWISSprachKonserven['Wort']['txt_OhneZuordnung']),'');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
$Form->ZeileEnde();


$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

	// Zur�ck zum Men�
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht1&4) == 4)		// Hinzuf�gen erlaubt?
{
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
		// Hilfe-Datei
$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=benutzerverwaltung&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

$Form->SchaltflaechenEnde();


if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>