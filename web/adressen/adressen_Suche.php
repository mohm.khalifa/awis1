<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $EingabeFeld;
global $AWISCursorPosition;

try 
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Adressen','txt_SucheName');
	$TextKonserven[]=array('Adressen','lbl_SucheName');
	$TextKonserven[]=array('Adressen','txt_SucheKontakt');
	$TextKonserven[]=array('Adressen','lbl_SucheKontakt');
	$TextKonserven[]=array('Adressen','ADR_%');
	$TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$TextKonserven[]=array('Wort', 'AuswahlSpeichern');
	$TextKonserven[]=array('Wort', 'ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3000 = $AWISBenutzer->HatDasRecht(3000);
	
	if($Recht3000 == 0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	/**********************************************
	 * * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
	
	if (! isset($Param['SPEICHERN'])) 
	{
	    $Param['SPEICHERN'] = 'off';
	}
	
	$Form->Formular_Start();
    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./adressen_Main.php?cmdAktion=Details>");
	
    $Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Adressen']['txt_SucheName'].':',200);
	$Form->Erstelle_TextFeld('*SuchName',($Param['SPEICHERN']=='on'?$Param['SuchName']:''),20,200,true);
	$AWISCursorPosition='sucSuchName';
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Adressen']['txt_SucheKontakt'].':',200);
	$Form->Erstelle_TextFeld('*SuchKontakt',($Param['SPEICHERN']=='on'?$Param['SuchKontakt']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_ORT'].':',200);
	$Form->Erstelle_TextFeld('*ADR_ORT',($Param['SPEICHERN']=='on'?$Param['ADR_ORT']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_BEMERKUNG'].':',200);
	$Form->Erstelle_TextFeld('*ADR_BEMERKUNG',($Param['SPEICHERN']=='on'?$Param['ADR_BEMERKUNG']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Adressen']['ADR_AZG_KEY'].':',200);
	$SQL = 'SELECT AZG_KEY, AZG_BEZEICHNUNG';
	$SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' WHERE AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
	$SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
	$SQL .= ' ORDER BY AZG_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*ADR_AZG_KEY',($Param['SPEICHERN']=='on'?$Param['ADR_AZG_KEY']:''), 200,true,$SQL,'','','','',array(array('0',$AWISSprachKonserven['Wort']['Auswahl_ALLE'])));
    $Form->ZeileEnde();

	//**************************************************************
	//* Suchfelder aus Infodaten
	//**************************************************************
    $Form->Trennzeile();
	
	$SQL = 'SELECT ADRESSENINFOTYPEN.*';
	$SQL .= ' FROM ADRESSENINFOTYPEN';
	$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' WHERE AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
	$SQL .= ' AND ADT_SUCHFELD=1';
	$SQL .= ' ORDER BY ADT_INFORMATION';
	
	$rsADI = $DB->RecordSetOeffnen($SQL); 
	$rsADIZeilen = $rsADI->AnzahlDatensaetze();
	for($ADIZeile=0;$ADIZeile<$rsADIZeilen;$ADIZeile++)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($rsADI->FeldInhalt('ADT_INFORMATION').':',200);
	
		if($rsADI->FeldInhalt('ADT_DATENQUELLE')!='')
		{
			switch(substr($rsADI->FeldInhalt('ADT_DATENQUELLE'),0,3))
			{
				case 'TXT':
					$Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
					$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
					$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
					$Form->Erstelle_SelectFeld('*ADT_KEY_'.$rsADI->FeldInhalt('ADT_KEY'),'',$rsADI->FeldInhalt('ADT_BREITE'),true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
					break;
				case 'SQL':
					$Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
					$Form->Erstelle_SelectFeld('*ADT_KEY_'.$rsADI->FeldInhalt('ADT_KEY'),'',$rsADI->FeldInhalt('ADT_BREITE'),true,'',$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
					break;
				default:
					$Form->Erstelle_TextFeld('*ADT_KEY_'.$rsADI->FeldInhalt('ADT_KEY'),'',$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),true,'','','',$rsADI->FeldInhalt('ADT_FORMAT'),'','',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
					break;
			}
		}
		else
		{
			$Form->Erstelle_TextFeld('*ADT_KEY_'.$rsADI->FeldInhalt('ADT_KEY'),'',$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),true,'','','',$rsADI->FeldInhalt('ADT_FORMAT'));
		}
		$Form->ZeileEnde();
		$rsADI->DSWeiter();
	}
    
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	
	if (($Recht3000 & 4) != 0)
	{	// wenn Recht zum Anlegen
	   $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>

