<?php

require_once('awisRepkostenvers.inc');
require_once('awisBenutzer.inc');
require_once('awisJobVerwaltung.inc');

try
{
	$Benutzer = '';
	$DebugLevel = 0;
	$StartTag = '';
	$EndTag = '';
	$Testsystem = false;
	$WANummer = '';
	
	for($i=1;$i<$argc;$i++)
	{
		switch (strtolower($argv[$i]))
		{
			case '--benutzer':
				$Benutzer = $argv[++$i];
				break;
			
			case '--debuglevel':
				$DebugLevel = $argv[++$i];
				break;
				
			default:
				echo 'Parameter '.$argv[$i].' ist unbekannt.';
				
			case '--help':
				echo $argv[0].PHP_EOL;
				echo 'Optionen:'.PHP_EOL;
				echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
				echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
				die();
			}
		}

		$AWISBenutzer = awisBenutzer::Init($Benutzer);
    	$Form = new awisFormular();
    	$DB = awisDatenbank::NeueVerbindung('AWIS');
    	
    	$RepkoVers = new awisRepkostenvers($DebugLevel, $Benutzer);
    	$RepkoVers->LadeNeueDatenReparaturkostenvers();
    	$RepkoVers->ErstelleSchnittstellenDatei();
    	$RepkoVers->ErstelleRechnungRepkostenvers();
    	$RepkoVers->VerschiebeDateien();
    	
}
catch (awisException $ex)
{
	echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
	echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
	echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
}

?>