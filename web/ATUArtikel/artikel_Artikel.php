<?
/****************************************************************************************************
*
* 	Artikel-Liste
*
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Sept. 2003
*           Juni  2007
*
****************************************************************************************************/
// Variablen
global $awisRSZeilen;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $RechteStufe;			// ATU Artikel: Basiszugriff
global $con;
global $rsArtikel;
global $rsLiefArt;
global $LiefArt;
global $cmdAktion;
global $AWISBenutzer;

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
$SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER = ' AND ROWNUM < 0'.$MaxDSAnzahl;

$Zeit = array();
$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';
$rsOEPZeilen=0;
$DSNr=0;
$CursorFeld = '';				// FeldName, auf das der Cursor gesetzt werden soll

		// ATU Artikel: Bearbeitung
$Recht400 = awisBenutzerRecht($con,400); // CA 16.03.07
$BearbeitungsStufe = awisBenutzerRecht($con,401);
$FilialBestandRecht = awisBenutzerRecht($con, 406);

/**********************************************************************************************************
* Neuen ATU Artikel hinzuf�gen
**********************************************************************************************************/
if(($BearbeitungsStufe&4)==4 AND isset($_GET['Hinzufuegen']))
{
	echo "<form name=frmArtikel method=post action=./artikel_Main.php?cmdAktion=ArtikelInfos>";

	echo "<table border=0 width=100%>";
	echo "<tr><td><font size=5><b><i>Neuer ATU Artikel ";
	echo "</i></b></font></td>";
	echo "</tr></table>";


	//***************************************
	// N�chste Freie Nummer ermitteln
	//***************************************
	$NeueNummer = awis_BenutzerParameter($con,'NaechsteATUNr',$AWISBenutzer->BenutzerName());
	$rsAST = awisOpenRecordset($con,'SELECT COUNT(*) AS ANZ FROM Artikelstamm WHERE AST_ATUNR=\''.$NeueNummer . '\'');
	while($rsAST['ANZ'][0]<>0)
	{
		$NeueNummer = sprintf('@%05d',intval(substr($NeueNummer,1)+1));
		$rsAST = awisOpenRecordset($con,'SELECT COUNT(*) AS ANZ FROM Artikelstamm WHERE AST_ATUNR=\''.$NeueNummer . '\'');
	}
	$NaechsteNummer = sprintf('@%05d',intval(substr($NeueNummer,1)+1));
	awis_BenutzerParameterSpeichern($con,'NaechsteATUNr',-1,$NaechsteNummer);


		// Zeile 1
	echo "<table border=1 width=100% >";
	echo "<colgroup><col width=200><col width=*></colgroup>";
	echo "<tr>";
	echo "<td id=FeldBez>ATU-Nr</td><td><input name=txtAST_ATUNR size=10 value=$NeueNummer></td>";

		// Zeile 2
	echo "<table border=1 width=100% >";
	echo "<colgroup><col width=200><col><col width=*></colgroup>";
	echo "<td id=FeldBez>Ka<u>t</u>alogbezeichnung</td>";
	echo "<td><input type=text accesskey=t size=85 name=txtAST_BEZEICHNUNG value=\"" . htmlspecialchars($rsArtikel['AST_BEZEICHNUNG'][0]) . "\">";
	echo "<input type=hidden name=txtAST_BEZEICHNUNG_old value=\"" . htmlspecialchars($rsArtikel['AST_BEZEICHNUNG'][0]) . "\"></td></tr>";

		// Zeile 3
	echo "<tr><td id=FeldBez>KENN-Vorschlag:</td>";
	echo "<td><input type=text size=2 name=txtAST_KENNUNGVORSCHLAG value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\">";
	echo "<input type=hidden name=txtAST_KENNUNGVORSCHLAG_old value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\"></td>";
	echo "</td></tr></table>";

			// Neuen Datensatz (ATU Artikel) hinzuf�gen
	echo "<input type=hidden name=NeuSpeichern Value=True>";
	echo "<input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&NeuerDS=True'>";
	echo "</form>"	;

	if($BearbeitungsStufe>0)
	{
			// Cursor in das erste Feld setzen
		echo "<Script Language=JavaScript>";
		echo "document.getElementsByName(\"txtAST_ATUNR\")[0].focus();";
		echo "</Script>";
	}

	awisLogoff($con);
	die();		// Bearbeitung hier beenden
}

if(isset($_POST['NeuSpeichern']) AND ($BearbeitungsStufe&4)==4)
{
	if(strtoupper($_POST['txtAST_ATUNR']) == '')
	{
		echo '<span class=HinweisText>Die ATU Nummer muss angegeben werden.</span>';
		awisLogoff($con);
		die();
	}
	$rsArtikel = awisOpenRecordset($con, "SELECT AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW FROM AWIS.ARTIKELSTAMM WHERE AST_ATUNR='" . strtoupper($_POST['txtAST_ATUNR'] . "'"));
	if($awisRSZeilen>0)
	{
		echo '<span class=HinweisText>Der ATU Artikel ' . strtoupper($_POST['txtAST_ATUNR']) . ' ist bereits vorhanden</span>';
		echo '<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0' . $rsArtikel['AST_KEY'][0];
		if($CursorFeld=='')
		{
			$CursorFeld = 'EingabeFeld';
			echo ' name=EingabeFeld';
		}
		echo ">";
		echo $rsArtikel['AST_ATUNR'][0] . "</a>&nbsp;" . htmlspecialchars($rsArtikel['AST_BEZEICHNUNGWW'][0]) ;
		awisLogoff($con);
		die();
	}

	$SQL = "INSERT INTO AWIS.ArtikelStamm(";
	$SQL .= "AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG, AST_WUG_KEY, AST_VK, AST_IMQ_ID, AST_USER, AST_USERDAT)";
	$SQL .= "VALUES(";
	$SQL .= "'" . strtoupper($_POST['txtAST_ATUNR']) . "',";
	$SQL .= "'" . $_POST['txtAST_BEZEICHNUNG'] . "',";
	$SQL .= "'" . $_POST['txtAST_KENNUNGVORSCHLAG'] . "',";
//	$SQL .= "" . $_POST['txtAST_WUG_KEY'] . ",";
	$SQL .= "1,";
	$SQL .= "0.00,";
	$SQL .= "4,";
	$SQL .= "'" . $AWISBenutzer->BenutzerName() . "',";
	$SQL .= "SYSDATE";
	$SQL .= ")";

	$Erg = awisExecute($con, $SQL);
	if($Erg==FALSE)
	{
		awisErrorMailLink("0101061001-artikel_Artikel.php", 2, $awisDBFehler['message']);
	}

	$rsAST = awisOpenRecordset($con, "SELECT AST_KEY FROM AWIS.ArtikelStamm where AST_ATUNR='" . strtoupper($_POST['txtAST_ATUNR']) . "'");
	awis_BenutzerParameterSpeichern($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName(), $rsAST['AST_KEY'][0]);
}
/**********************************************************************************************************
* Ende HINZUF�GEN
**********************************************************************************************************/

/**********************************************************************************************************
* Artikel l�schen
**********************************************************************************************************/

if(awis_NameInArray($_POST, "cmdLoeschen_")!='')
{
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM AWIS.ArtikelStamm WHERE AST_KEY=0" . $_POST["txtAST_KEY"];
		$Erg = awisExecute($con, $SQL );
		if($Erg==FALSE)
		{
			awisErrorMailLink("0101061002-artikel_Artikel.php", 2, $awisDBFehler['message']);
		}
		die('<br>Der Artikel wurde erfolgreich gel�scht<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		echo "<form name=frmArtikel method=post>";

		echo "<input type=hidden name=" . awis_NameInArray($_POST, "cmdLoeschen_") . ">";
		echo "<input type=hidden name=txtAST_KEY value=" . $_POST["txtAST_KEY"] . ">";

		echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Artikel l�schen m�chten?</span><br><br>";
		echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

		echo "</form>";
		awisLogoff($con);
		die();
	}
}
/**********************************************************************************************************
* ENDE Artikel l�schen
**********************************************************************************************************/

		// Aktualisieren gew�hlt?
if(isset($_POST['cmdSuche_x']))
{
	$Param = (isset($_POST['txtAuswahlSpeichern'])?$_POST['txtAuswahlSpeichern']:'');
	$Param .= ";" . $_POST['txtAST_ATUNR'];
	$Param .= ";" . $_POST['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $_POST['txtTEI_SUCH'];
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_EAN'])?$_POST['txtTEI_SUCH_EAN']:'');			// Suche nach EAN-Nummer
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_GNR'])?$_POST['txtTEI_SUCH_GNR']:'');			// Suche nach Gebrauchsmuster
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_LAR'])?$_POST['txtTEI_SUCH_LAR']:'');			// Suche nach Lieferantenartikel
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_Exakt'])?$_POST['txtTEI_SUCH_Exakt']:'');		// Exakte Suche
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_LAS'])?$_POST['txtTEI_SUCH_LAS']:'');			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_OEN'])?$_POST['txtTEI_SUCH_OEN']:'');			// Suche nach OE-Nummer
	$Param .= ";" . $_POST['txtKommentar'];				// Kommentar im Artikel
	$Param .= ";" . (isset($_POST['txtTEI_SUCH_XXX'])?$_POST['txtTEI_SUCH_XXX']:'');			// Suche nach beliebiger Nummer
	$Param .= ";" . $_POST['txtWUG_KEY'];				// Suche nach Warenuntergruppe
	$Param .= ";" . $_POST['txtWGR_KEY'];				// Suche nach Warengruppe
	$Param .= ";" . (isset($_POST['txtOEPreise'])?$_POST['txtOEPreise']:'');				// Immer OE-Preise zeigen
	$Param .= ";" . (isset($_POST['txtFKAnzeige'])?$_POST['txtFKAnzeige']:'');				// FK zeigen

	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName(), "");

	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));
    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
}

	// SQL Anweisung erstellen
$SQL = "SELECT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG ";
$TabellenListe = " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN ";

if(isset($_GET['Liste']))		// Liste anzeigen
{
	$Artikel[0]='';
}
$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($_GET["Key"]) OR isset($_POST["txtAST_KEY"]) OR $Artikel[0]!='')
{
	if(isset($_POST["txtAST_KEY"]))
	{
		$ASTKEY = $_POST["txtAST_KEY"];
	}
	elseif(isset($_GET["Key"]))
	{
		$ASTKEY = $_GET["Key"];
	}
	else
	{
		$ASTKEY=$Artikel[0];
	}
	$Bedingung = " AND AST_KEY = " . $ASTKEY;


//**********************************************************
// Lieferantenartikelzuordnung l�schen
//**********************************************************
	if(awis_NameInArray($_POST, "cmdLARLoeschen_")!='' AND !isset($_POST["cmdLoeschAbbruch"]))
	{
		if(isset($_POST["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM TeileInfos WHERE TEI_KEY=0" . intval(substr(awis_NameInArray($_POST, "cmdLARLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg===FALSE)
			{
				awisErrorMailLink("0101061003-artikel_Artikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			echo "<form name=frmArtikel method=post>";

			echo "<input type=hidden name=" . awis_NameInArray($_POST, "cmdLARLoeschen_") . ">";

			echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

			echo "</form>";
			awisLogoff($con);
			die();
		}
	}

//****************************************************
// EAN Nummern Zuordnung l�schen
//****************************************************
	if(awis_NameInArray($_POST, "cmdEANLoeschen_")!='' AND !isset($_POST["cmdLoeschAbbruch"]))
	{
		if(isset($_POST["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM AWIS.TeileInfos WHERE TEI_KEY2=0" . intval(substr(awis_NameInArray($_POST, "cmdEANLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("0101061004-artikel_Artikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			echo "<form name=frmArtikel method=post>";

			echo "<input type=hidden name=" . awis_NameInArray($_POST, "cmdEANLoeschen_") . ">";

			echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

			echo "</form>";
			die();
		}
	}

//****************************************************
// OE Nummern zuordnung l�schen
//****************************************************
	if(awis_NameInArray($_POST, "cmdOENLoeschen_")!='' AND !isset($_POST["cmdLoeschAbbruch"]))
	{
		if(isset($_POST["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM AWIS.TeileInfos WHERE TEI_KEY2=0" . intval(substr(awis_NameInArray($_POST, "cmdOENLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("0101061005-artikel_Artikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			echo "<form name=frmArtikel method=post>";

			echo "<input type=hidden name=" . awis_NameInArray($_POST, "cmdOENLoeschen_") . ">";

			echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

			echo "</form>";
			awisLogoff($con);
			die();
		}
	}

//****************************************************
//
// Daten speichern
//
//****************************************************

			// Bei Daten�nderungen, nicht bei neuen Datens�tzen
			//nicht mehr aufrufen, da Funktion AWIS.AENDERN_ARTIKELSTAMMINFOS NICHT VORHANDEN
	if(awis_NameInArray($_POST, "cmdSpeichern")!='' AND ((isset($_POST['NeuSpeichern'])) AND $_POST['NeuSpeichern']!='True'))
	{
		//******************************
		// Artikelstamm speichern
		//******************************
		$SAVESQL='';
		if(isset($_POST["txtAST_BEZEICHNUNG"]))
		{
			if($_POST["txtAST_BEZEICHNUNG"] != $_POST["txtAST_BEZEICHNUNG_old"])
			{
				$SAVESQL .= ", AST_BEZEICHNUNG = '" . $_POST["txtAST_BEZEICHNUNG"] . "'";
			}
			if($_POST["txtAST_KENNUNGVORSCHLAG"] != $_POST["txtAST_KENNUNGVORSCHLAG_old"])
			{
				$SAVESQL .= ", AST_KENNUNGVORSCHLAG = '" . substr($_POST["txtAST_KENNUNGVORSCHLAG"],0,1) . "'";
			}

			$rsUpdateTest = awisOpenRecordset($con, "SELECT AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG, AST_USER, AST_UserDat FROM AWIS.ArtikelStamm WHERE AST_KEY=0" . $ASTKEY);
			if($_POST["txtAST_BEZEICHNUNG_old"] != $rsUpdateTest['AST_BEZEICHNUNG'][0] OR
				$_POST["txtAST_KENNUNGVORSCHLAG_old"] != $rsUpdateTest['AST_KENNUNGVORSCHLAG'][0])
			{
				echo '<span class=HinweisText>Die Daten wurden in der Zwischenzeit durch den Benutzer ' . $rsUpdateTest['AST_USER'][0] . ' ver�ndert.</span>';
				echo '<br><br>Bezeichnung: ' . $rsUpdateTest['AST_BEZEICHNUNG'][0] . '<br>';
				echo 'Kennvorschlag: ' . $rsUpdateTest['AST_KENNUNGVORSCHLAG'][0] . '<br>';
				echo '�nderungszeit:' . $rsUpdateTest['AST_USERDAT'][0] . '<br>';
				echo "<br><br><a href=./artikel_Main.php?Key=$ASTKEY&cmdAktion=ArtikelInfos>Bitte wiederholen Sie Ihre Eingabe.</a>";
				awisLogoff($con);
				die();
			}

			If($SAVESQL!='')
			{
				$SAVESQL = " UPDATE AWIS.ARTIKELSTAMM SET " . substr($SAVESQL,1) . " WHERE AST_KEY=" . $ASTKEY;
				$ExErg = awisExecute($con, $SAVESQL, FALSE);
				if($ExErg==FALSE)
				{
					awisErrorMailLink("0101061006-artikel_Artikel.php", 3, $awisDBFehler['message'],$FilialBestandRecht);
				}
			}


				/**********************************************************
				* Alternativartikel
				**********************************************************/

			$AltArt = explode(';',awis_NameInArray($_POST,'txtASI_120_',1));
			foreach($AltArt As $Eintrag)
			{
				$ASI_Key='0' . substr($Eintrag,11);

				$SQL='';
				if(isset($_POST[trim($Eintrag)]) AND $_POST[trim($Eintrag)]!='')	// �ndern oder neu
				{
		        	$SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $ASI_Key  . ",120,'" . $_POST[trim($Eintrag)] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
				}
				elseif((!isset($_POST[trim($Eintrag)]) or $_POST[trim($Eintrag)]=='') AND $ASI_Key!=0)	// L�schen
				{
					$SQL = "DELETE FROM ARTIKELSTAMMINFOS WHERE ASI_KEY=0" . $ASI_Key  . "";
				}
				if($SQL!='')
				{
					$Erg = awisExecute($con, $SQL);
					if($Erg==FALSE)
					{
						awisErrorMailLink("0101061007-artikel_Artikel.php", 2, $awisDBFehler);
						die();
					}
				}
			}		// Alternativartikel


				/**********************************************************
				* Zubeh�rartikel
				**********************************************************/

			$AltArt = explode(';',awis_NameInArray($_POST,'txtASI_121_',1));
			foreach($AltArt As $Eintrag)
			{
				$ASI_Key='0' . substr($Eintrag,11);

				$SQL='';
				if(isset($_POST[trim($Eintrag)]) AND $_POST[trim($Eintrag)]!='')	// �ndern oder neu
				{
		        	$SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $ASI_Key  . ",121,'" . $_POST[trim($Eintrag)] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
				}
				elseif((!isset($_POST[trim($Eintrag)]) or $_POST[trim($Eintrag)]=='') AND $ASI_Key!=0)	// L�schen
				{
					$SQL = "DELETE FROM ARTIKELSTAMMINFOS WHERE ASI_KEY=0" . $ASI_Key  . "";
				}
				if($SQL!='')
				{
					$Erg = awisExecute($con, $SQL);
					if($Erg==FALSE)
					{
						awisErrorMailLink("0101061008-artikel_Artikel.php", 2, $awisDBFehler);
						die();
					}
				}
			}		// Alternativartikel


		/**********************************************************
		* Verpackungseinheit (VPE)
		**********************************************************/

			$Eintrag = awis_NameInArray($_POST,'txtASI_60_',0);
			$EintragOld = awis_NameInArray($_POST,'txtASI_old_60_',0);

			if($_POST[$Eintrag]!=$_POST[$EintragOld])
			{
	        	$SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(0" . substr($Eintrag,10)  . ",60,'" . $_POST[$Eintrag] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
				$Erg = awisExecute($con, $SQL);
				if($Erg==FALSE)
				{
					awisErrorMailLink("0101061009-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
			}
		}				// Sind die wichtigsten Inputfelder gesetzt?
		$SAVESQL='';

		/***************************************
		* an Einkauf am
		****************************************/
		if(isset($_POST["txtMELDUNG"]))
		{

			if($_POST["txtMELDUNG"] != $_POST["txtMELDUNG_old"])
			{
				if($_POST["txtMELDUNG"]!='')
				{
					$Wert = awis_format($_POST["txtMELDUNG"],'Datum');
				}

		        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $_POST["txtMELDUNG_KEY"] . ",100,'" . $Wert  . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
		        $Erg = awisExecute($con, $SQL);
				if($Erg==FALSE)
				{
					awisErrorMailLink("0101061010-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
			}
		}


		/**************************************
		* durch
		**************************************/
		if(isset($_POST["txtMELDUNGDURCH"]))
		{
			if($_POST["txtMELDUNGDURCH"] != $_POST["txtMELDUNGDURCH_old"])
			{
		        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $_POST["txtMELDUNGDURCH_KEY"] . ",101,'" . $_POST["txtMELDUNGDURCH"] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
				$Erg = awisExecute($con, $SQL);
				if($Erg==FALSE)
				{
					awisErrorMailLink("0101061011-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
			}
		}

		/***************************************
		* Kataloginfos
		****************************************/
		if(isset($_POST["txtMusterVorhanden"]))
		{
	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $_POST["txtMusterVorhanden_KEY"] . ",61,'" . $_POST["txtMusterVorhanden"] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("0101061012-artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}
		}

/*
		if(isset($_POST["txtVerpackungsmenge"]))			// Verpackungsmenge
		{
echo '<hr>VM: ' . $_POST["txtVerpackungsmenge"];

	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $_POST["txtVerpackungsmenge_KEY"] . ",60,'" . $_POST["txtVerpackungsmenge"] . "','" . $AWISBenutzer->BenutzerName() . "', " . $_POST["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("0101061013-artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}
		}
*/
		/**************************************
		* LieferantenArtikel
		**************************************/

		if(isset($_POST["txtLAR_LARTNR_0"]) AND !isset($_POST['cmdSpeichernAbbruch']) AND $_POST["txtLAR_LIE_NR"]!='')
		{
			$rsLAR = awisOpenRecordset($con, "SELECT * FROM AWIS.LieferantenArtikel WHERE LAR_LARTNR='" . $_POST["txtLAR_LARTNR_0"] . "' AND LAR_LIE_NR='" . $_POST["txtLAR_LIE_NR"] . "'");
			$rsLARZeilen = $awisRSZeilen;

			if($rsLARZeilen==0)				// Nicht gefunden
			{
				// Lieferant suchen
				$rsLIE = awisOpenRecordset($con, "SELECT * FROM AWIS.Lieferanten WHERE LIE_NR='" . $_POST["txtLAR_LIE_NR"] . "'");
				if($awisRSZeilen==0)
				{
					echo "<span class=HinweisText>Der Lieferant " . $_POST["txtLAR_LIE_NR"] . " ist nicht vorhanden.</span>";
					echo "<br><br>Bitte korrigieren Sie Ihre Eingabe.";
					echo "<br><br><img src=/bilder/zurueck.png onclick=history.back();>";
				}
				else		// Lieferant vorhanden
				{
					if(isset($_POST["cmdSpeichernBestaetigung"]))
					{
						$SQL = "INSERT INTO AWIS.LieferantenArtikel(LAR_LARTNR, LAR_LIE_NR, LAR_IMQ_ID, LAR_USER, LAR_USERDAT, LAR_BEKANNTWW)";
						$SQL .= " VALUES (";
						$SQL .= "'" . $_POST['txtLAR_LARTNR_0'] . "',";
						$SQL .= "'" . $_POST["txtLAR_LIE_NR"] . "', ";
						$SQL .= "4, ";
						$SQL .= "'" . $AWISBenutzer->BenutzerName() . "', SYSDATE, 0";
						$SQL .=" )";
						$Erg = awisExecute($con, $SQL,TRUE,false,1);		// EXEC mit Transaktion
						if($Erg==FALSE)
						{
							awisErrorMailLink("0101061014-artikel_Artikel.php", 2, $awisDBFehler);
							die();
						}

						$SQL = "SELECT LAR_KEY FROM AWIS.LieferantenArtikel WHERE LAR_LARTNR='";
						$SQL .= $_POST['txtLAR_LARTNR_0'] . "' AND LAR_LIE_NR='";
						$SQL .= $_POST["txtLAR_LIE_NR"] . "' ";
						$rsLAR = awisOpenRecordset($con, $SQL);
						if($rsLAR==FALSE)
						{
							awisErrorMailLink("0101061015-artikel_Artikel.php", 2, $awisDBFehler);
							die();
						}

						// F�r die Teileinfos wird die ATUNR ben�tigt!
						$SQL = 'SELECT AST_ATUNR FROM ArtikelStamm WHERE AST_KEY = 0' . $_POST['txtAST_KEY'] . '';
						$recAST = awisOpenRecordset($con, $SQL);

						$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_WERT1, TEI_SUCH1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
						$SQL .= "VALUES('AST','A" . $_POST["txtAST_KEY"] . "";
						$SQL .= ",'" . $recAST['AST_ATUNR'][0] . "', ASCIIWORT('" . $recAST['AST_ATUNR'][0] ."')";
						unset($recAST);
						$SQL .= ", 'LAR', " . $rsLAR['LAR_KEY'][0] .  ", ASCIIWORT('" . $_POST["txtLAR_LARTNR_0"] . "'), '" . $_POST["txtLAR_LARTNR_0"] . "'" ;
						$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE";
						$SQL .= ")";
						$Erg = awisExecute($con, $SQL,TRUE,false,1);			// EXEC mit commit
						if($Erg==FALSE)
						{
							ocirollback($con);		// TODO: Pr�fen, warum das nicht funktioniert!
							awisErrorMailLink("0101061016-artikel_Artikel.php", 2, $awisDBFehler);

							// Eingef�gten Artikel wieder l�schen - weil ocirollback() nicht geht!!!
						//	awisExecute($con,"DELETE FROM LieferantenArtikel WHERE LAR_KEY=0". $rsLAR['LAR_KEY'][0],true,false);

							die();
						}


					}
					else	// Hinzuf�gen best�tigen
					{
						echo "<form name=frmArtikel method=post>";

						echo "<input type=hidden name=txtLAR_LARTNR_0>";
						echo "<input type=hidden name=cmdSpeichern>";
						echo "<input type=hidden name=txtAST_KEY value='" . $_POST["txtAST_KEY"] . "'>";
						echo "<input type=hidden name=txtLAR_KEY value='" . $rsLAR['LAR_KEY'][0] . "'>";

						echo "<table border=0>";
						echo "<tr><td>Lieferantenartikelnummer</td><td><input type=text name=txtLAR_LARTNR_0 value=" . $_POST["txtLAR_LARTNR_0"] . "></td></tr>";
						echo "<tr><td>Lieferant</td><td><input type=text name=txtLAR_LIE_NR value=" . $_POST["txtLAR_LIE_NR"] . "></td></tr>";
						echo "</table>";

						echo "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie die die neuen Lieferantenartikel anlegen wollen?</span><br><br>";
						echo "<input type=submit value=\"Ja, hinzuf�gen\" name=cmdSpeichernBestaetigung>";
						echo "<input type=submit value=\"Nein, nicht hinzuf�gen\" name=cmdSpeichernAbbruch>";

						echo "</form>";
						die();
					}	// Best�tigung?
				}		//Lieferant vorhanden?
			}
			else		// Lieferantenartikel vorhanden?
			{
				if(!isset($_POST["cmdSpeichernBestaetigung"]))
				{
					$SQL = "SELECT AST_ATUNR, TEI_KEY1 FROM AWIS.TeileInfos, AWIS.Artikelstamm";
					$SQL .= " WHERE TEI_ITY_ID2='LAR' AND TEI_KEY2 = " . $rsLAR["LAR_KEY"][0];
					$SQL .= " AND TEI_KEY1 = AST_KEY";

					$rsCheck = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen>0)
					{
						echo "<form name=frmArtikel method=post>";

						echo "<input type=hidden name=txtLAR_LARTNR_0>";
						echo "<input type=hidden name=cmdSpeichern>";
						echo "<input type=hidden name=txtAST_KEY value='" . $_POST["txtAST_KEY"] . "'>";
						echo "<input type=hidden name=txtLAR_KEY value='" . $rsLAR['LAR_KEY'][0] . "'>";

						echo "<span class=HinweisText>Es existiert bereits eine Zuordnung f�r diesen Lieferantenartikel!</span>"	;
						echo "<table border=0>";
						echo "<tr><td>Lieferantenartikelnummer</td><td><input type=text name=txtLAR_LARTNR_0 value='" . $_POST["txtLAR_LARTNR_0"] . "'></td></tr>";
						echo "<tr><td>Lieferant</td><td><input type=text name=txtLAR_LIE_NR value=" . $_POST["txtLAR_LIE_NR"] . "></td></tr>";
						echo "<tr><td>ATU Artikel</td><td><input type=hidden readonly name=txtLAR_AST_ATUNR value=" . $rsCheck["AST_ATUNR"][0] . ">" . $rsCheck["AST_ATUNR"][0] . "</td></tr>";
						echo "</table>";

						echo "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie die die neuen Lieferantenartikel zu diesem Artikel zuweisen wollen?</span><br><br>";

						echo "<input type=submit value=\"Ja, hinzuf�gen\" name=cmdSpeichernBestaetigung>";
						echo "<input type=submit value=\"Nein, nicht zuweisen\" name=cmdSpeichernAbbruch>";

						echo "</form>";
						die();
					}
					else
					{
						$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
						$SQL .= "VALUES('AST'," . $_POST["txtAST_KEY"] . ", 'LAR', " . $rsLAR["LAR_KEY"][0] .  ", ASCIIWORT('" . $_POST["txtLAR_LARTNR_0"] . "'), '" . $_POST["txtLAR_LARTNR_0"] . "'" ;
						$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE";
						$SQL .= ")";

						unset($rsCheck);
						awisExecute($con, $SQL);

					}
				}

				if(isset($_POST["cmdSpeichernBestaetigung"]))
				{
					$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
					$SQL .= "VALUES('AST'," . $_POST["txtAST_KEY"] . ", 'LAR', " . $rsLAR["LAR_KEY"][0] .  ", ASCIIWORT('" . $_POST["txtLAR_LARTNR_0"] . "'), '" . $_POST["txtLAR_LARTNR_0"] . "'" ;
					$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE";
					$SQL .= ")";

					awisExecute($con, $SQL);
				}
			}
		    unset($rsLAR);
			unset($rsLARZeilen);
		}

		/**************************************
		* EAN Nummer
		**************************************/

		if(isset($_POST["txtEAN_KEY_0"]))
		{
			$rsEAN = awisOpenRecordset($con, "SELECT * FROM AWIS.EANNummern WHERE EAN_NUMMER='" . $_POST["txtEAN_Nummer"] . "'");
			$rsEANZeilen = $awisRSZeilen;

			if($rsEANZeilen>0)
			{
				$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
				$SQL .= "VALUES('AST'," . $_POST["txtAST_KEY"] . ", 'EAN', " . $rsEAN["EAN_KEY"][0] .  ", ASCIIWORT('" . $_POST["txtEAN_Nummer"] . "'), '" . $_POST["txtEAN_Nummer"] . "'" ;
				$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE";
				$SQL .= ")";

				awisExecute($con, $SQL);

			}
			elseif($_POST["txtEAN_Nummer"]!='')
			{
				echo "<span class=HinweisText>EAN Nummer " . $_POST["txtEAN_Nummer"] . " nicht gefunden</span>";

				echo "<br><br>";

				die();
			}
		}

		/***************************************
		* OE Nummern
		****************************************/
//TODO: Weiter
		if(isset($_POST["txtOEN_KEY_0"]))
		{
			$rsOE = awisOpenRecordset($con, "SELECT * FROM AWIS.OENummern WHERE OEN_NUMMER='" . $_POST["txtOEN_NUMMER"] . "' AND OEN_HER_ID=0" . $_POST["txtOEN_HER_ID"]);
			$rsOEZeilen = $awisRSZeilen;

			if($rsOEZeilen>0)
			{
				$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
				$SQL .= "VALUES('AST'," . $_POST["txtAST_KEY"] . ", 'OEN', " . $rsOE["OEN_KEY"][0] .  ", ASCIIWORTOE('" . $_POST["txtOEN_NUMMER"] . "'), '" . $_POST["txtOEN_Nummer"] . "'" ;
				$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE";
				$SQL .= ")";

				awisExecute($con, $SQL);
			}
			elseif($_POST["txtOEN_NUMMER"]!='')
			{
				if(!isset($_POST["cmdSpeichernBestaetigung"]) AND !isset($_POST['cmdSpeichernAbbruch']))		// Speichern best�tigen
				{
					echo "<span class=HinweisText>OE Nummer " . $_POST["txtOEN_NUMMER"] . " f�r den Hersteller " . $_POST["txtOEN_HER_ID"] . " nicht gefunden</span>";

					echo "<br><br>";
					echo "<form name=frmOENummer method=post>";

					echo "<input type=hidden name=txtOEN_KEY_0>";
					echo "<input type=hidden name=cmdSpeichern>";
					echo "<input type=hidden name=txtOEN_NUMMER value='" . $_POST["txtOEN_NUMMER"] . "'>";
					echo "<input type=hidden name=txtOEN_HER_ID value='" . $_POST['txtOEN_HER_ID'] . "'>";
					echo "<input type=hidden name=txtAST_KEY value=0" . $_POST['txtAST_KEY'] . '>';
					echo "<input type=hidden name=txtAST_ATUNR value=0" . $_POST['txtAST_ATUNR'] . '>';

					echo "<span class=HinweisText>M�chten Sie eine neue OE-Nummer " . $_POST["txtOEN_NUMMER"] . " anlegen?</span>"	;

					echo "<br><br><input type=submit value=\"Ja, hinzuf�gen\" name=cmdSpeichernBestaetigung>";
					echo "<input type=submit value=\"Nein, nicht anlegen\" name=cmdSpeichernAbbruch>";

					echo "</form>";
					awisLogoff($con);
					die();

				}
				elseif(isset($_POST["cmdSpeichernBestaetigung"]) AND !isset($_POST['cmdSpeichernAbbruch'])) 	// Daten speichern
				{

					$SQL = 'INSERT INTO OENUMMERN(';
					$SQL .= 'OEN_NUMMER, OEN_HER_ID, OEN_SUCHNUMMER, OEN_MUSTER, OEN_IMQ_ID, OEN_USER, OEN_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= "'" . $_POST["txtOEN_NUMMER"] . "'";
					$SQL .= "," . $_POST["txtOEN_HER_ID"] . "";
					$SQL .= ",ASCIIWORTOE('" . $_POST["txtOEN_NUMMER"] . "')";
					$SQL .= ',0';
					$SQL .= ',4';
					$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
					$SQL .= ',SYSDATE)';

					if(awisExecute($con,$SQL)==FALSE)
					{
						awisErrorMailLink('0101061017-artikel_artikel.php',2,$awisDBFehler,'OE-Nummer anlegen');
						awisLogoff($con);
						die();
					}

					$SQL = "SELECT SEQ_GLOBAL_KEY.CURRVAL AS KEY FROM DUAL";
					$rsKey = awisOpenRecordset($con, $SQL);

					$SQL = "INSERT INTO TEILEINFOS(";
					$SQL .= "TEI_ITY_ID1, TEI_KEY1, TEI_SUCH1, TEI_WERT1, TEI_ITY_ID2, ";
					$SQL .= " TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT) ";
					$SQL .= "VALUES(";
					$SQL .= "'AST'";
					$SQL .= "," . $_POST['txtAST_KEY'];
					$SQL .= ",'" . $_POST['txtAST_ATUNR'] . "'";
					$SQL .= ",'" . $_POST['txtAST_ATUNR'] . "'";
					$SQL .= ", 'OEN'";
					$SQL .= ", " . $rsKey['KEY'][0];
					$SQL .= ", ASCIIWORTOE('" . $_POST["txtOEN_NUMMER"] . "')";
					$SQL .= ", '" . $_POST["txtOEN_NUMMER"] . "'";
					$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
					$SQL .= ", SYSDATE";
					$SQL .= ")";

					if(awisExecute($con,$SQL)==FALSE)
					{
						awisErrorMailLink('0101061018-artikel_artikel.php',2,$awisDBFehler,'OE-Nummer zuordnen');
						awisLogoff($con);
						die();
					}
				}
			}
		}
	}  // Ende Speichern von Daten
}
				//***********************************************************
elseif(!isset($_GET["Key"]) AND !isset($_GET["ATUNR"]))			// Artikel suchen (Liste anzeigen, oder einen)
				//***********************************************************
{

	$DSAnz=0;		// Insgesamt gelesene Zeilen
	if(isset($_GET['Export']))		// Export l�uft
	{
		echo 'Daten werden vorbereitet...';
		flush();
	}
	else
	{
		if(($RechteStufe & 1024) == 1024)		// Export erlaubt
		{
			if($Param[11]=='')		// Nicht bei Suche nach beliebiger Nummer
			{
				echo " <input border=0 type=image accesskey=X title='Export (Alt+X)' src=/bilder/tabelle.png name=cmdExport onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Export=True&Liste=True'>";
			}
			@unlink(awis_UserExportDateiName('.csv'));
		}
		if(($BearbeitungsStufe&4)==4)		// Neuen Artikel hinzuf�gen
		{
			echo "<a accesskey=n href=./artikel_Main.php?cmdAktion=ArtikelInfos&Hinzufuegen=True><img src=/bilder/plus.png border=0 title='Hinzuf�gen (Alt+N)'></a>";
		}


		// �berschrift
		echo "<table  width=100% id=DatenTabelle border=1>";

/*
		if($Param[11]!='on')			// NICHT Nach beliebiger Nummer => normale �berschrift
		{
			echo "\n<tr>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>ATUNR</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>BEZEICHNUNG</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>WWS</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>Cross</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>VK</span></td>";
			echo "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett>Bestand Lager</span></td>";
			echo "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett>Bestand Filialen</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett>WG</span></td>";
			echo "</tr>\n";
		}
*/

	}

	$rsArtikel='';
	$rsArtikelZeilen=0;

	// Reine ATU-Artikel
    $Bedingung = '';

	if($Param[1]!='')			// ATU-Nummer
	{
		$Bedingung = " AND AST_ATUNR " . awisLIKEoderIST($Param[1], True, False, False);
	}
	if($Param[2]!='')			// Artikel-Bezeichnung
	{
		$Bedingung .= " AND (UPPER(AST_BEZEICHNUNGWW) " . awisLIKEoderIST($Param[2], True, False, false);
		$Bedingung .= " OR UPPER(AST_BEZEICHNUNG) " . awisLIKEoderIST($Param[2], True, False, false) . ")";
	}
	if($Param[10]!='')			// txtKommentar
	{
		$Bedingung .= " AND AST_ATUNR IN (SELECT ASI_AST_ATUNR FROM AWIS.ArtikelStammInfos where ASI_AIT_ID IN (110,111) AND UPPER(ASI_WERT) " . awisLIKEoderIST($Param[10],TRUE) . ")";
	}
	if($Param[12]!='0')				// Warenuntergruppe
	{
		$Bedingung .= " AND AST_WUG_KEY = " . $Param[12] . "";
	}
	if($Param[13]!='-1' AND $Param[13]!='' )				// Warengruppe
	{
		$Bedingung .= " AND WGR_ID='" . $Param[13] . "'";
	}

	if($Param[3]=='')
	{
		If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
		{
			$Bedingung.=" AND BITAND(AST_IMQ_ID,2)=2";
		}

		If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
		{
			$Bedingung.=" AND AST_SUCH2 IS NULL";
		}
	}

	if($Bedingung!='')
	{

		$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'AST' AS FUNDSTELLE ";
		$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN ";

		$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID)" . $Bedingung;
		// TODO: Sortierung f�r die Artikelnummer einf�gen.
		//$SQL .= " ORDER BY AST_ATUNR ASC";

		if($MaxDSAnzahl>0)
		{
			$SQL = 'SELECT * FROM (' . $SQL . ') Daten WHERE ROWNUM <= ' . $MaxDSAnzahl;
		}

		$rsArtikel = awisOpenRecordset($con, $SQL);
		if($rsArtikel==FALSE)
		{
			awisErrorMailLink("0101061019-artikel_Artikel.php", 2, $awisDBFehler);
			die();
		}
		$rsArtikelZeilen = $awisRSZeilen;
		$DSAnz += $awisRSZeilen;

		_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,(isset($_GET['Export'])?$_GET['Export']:''),$FilialBestandRecht,'',$Param,$MaxDSAnzahl,$Recht400);
		flush();
	}

	if($Param[3]!='')		// Sonstige Nummern
	{
		if($Param[4]!='')		// EAN-Nummern
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'EAN' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";

			$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND ((TEI_ITY_ID2 = 'EAN' ";
			$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, TRUE) . ")";
			$SQL .= " AND (TEI_KEY1 = AST_KEY))";		// Verkn�pfung zur TeileInfo
			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}
			If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
			{
				$Bedingung.=" AND BITAND(AST_IMQ_ID,2)=2";
			}
			If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
			{
				$SQL = $SQL ." AND AST_SUCH2 IS NULL";
			}
			$SQL .= " ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("0101061020-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$_GET['Export'],$FilialBestandRecht,'',$Param,$MaxDSAnzahl,$Recht400);
			}
			flush();
		}

		if($Param[5]!='')		// Gebrauchsnummern
		{
			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$Bedingung = " AND upper(TEI_SUCH2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";
			}
			else
			{
				$Bedingung = " AND upper(TEI_WERT2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}

			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'GNR' AS FUNDSTELLE";
			$SQL .= " FROM Artikelstamm";
	   		$SQL .= " INNER JOIN WarenUntergruppen ON AST_WUG_Key = WUG_Key";
	   		$SQL .= " INNER JOIN WarenGruppen ON WUG_WGR_ID = WGR_ID";
			$SQL .= " WHERE AST_KEY IN (";
			$SQL .= " SELECT TEI_Key1 FROM TeileInfos";
			$SQL .= " WHERE TEI_KEY2 IN (SELECT TEI_Key1 from teileinfos where ";
			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " upper(TEI_SUCH2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . "";
			}
			else
			{
				$SQL .= " upper(TEI_WERT2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			}
			$SQL .= " AND TEI_ITY_ID2='GNR') AND TEI_ITY_ID1='AST')";

			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}
			If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
			{
				$Bedingung.=" AND BITAND(AST_IMQ_ID,2)=2";
			}
			If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
			{
				$SQL = $SQL ." AND AST_SUCH2 IS NULL";
			}
			$SQL .=" ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("0101061021-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$_GET['Export'],$FilialBestandRecht,'',$Param,$MaxDSAnzahl,$Recht400);
				flush();
			}
		}

		if($Param[6]!='')		// Lieferantenartikelnummer
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'LAR' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND ((TEI_ITY_ID2 = 'LAR' ";
			$SQL .= " AND (TEI_KEY1 = AST_KEY))";		// Verkn�pfung zur TeileInfo

			if($Param[7]!='on')
			{
				$Bedingung = " AND TEI_SUCH2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$Bedingung.=" AND TEI_SUCH2 in (select LAR_SUCH_LARTNR from lieferantenartikel where LAR_SUCH_LARTNR ". awisLIKEoderIST($Param[3], True, False, FALSE, 1);
					$Bedingung.=" AND LAR_REKLAMATIONSKENNUNG is null)";
				}
			}
			else
			{
				$Bedingung = " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$Bedingung.=" AND TEI_WERT2 in (select LAR_LARTNR from lieferantenartikel where LAR_LARTNR ". awisLIKEoderIST($Param[3], True, False, FALSE, 1);
					$Bedingung.= " AND LAR_REKLAMATIONSKENNUNG is null) ";
				}
			}

			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}
			If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
			{
				$Bedingung.=" AND BITAND(AST_IMQ_ID,2)=2";
			}
			If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
			{
				$Bedingung.=" AND AST_SUCH2 IS NULL";
				//$SQL = $SQL ." AND AST_SUCH2 IS NULL";
			}

			$SQL .= $Bedingung . " ORDER BY AST_ATUNR";
			//awis_Debug(1,$SQL);

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("0101061022-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$_GET['Export'],$FilialBestandRecht,'',$Param,$MaxDSAnzahl,$Recht400);
				flush();
			}
		}

		if($Param[8]!='')		// Lieferantenartikelsets
		{
			$SQL = "SELECT DISTINCT LieferantenArtikel.*, ArtikelStamm.*, 'LAS' AS FUNDSTELLE, T1.* ";
 			$SQL .= " FROM TeileInfos T1 INNER JOIN LieferantenArtikel ON T1.TEI_WERT1 = LAR_LARTNR";
			$SQL .= " LEFT OUTER JOIN TeileInfos T2 ON T1.TEI_SUCH1=T2.TEI_SUCH2 ";
			$SQL .= " LEFT OUTER JOIN ArtikelStamm ON T2.TEI_KEY1 = AST_KEY ";
			$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS' ";

			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " AND UPPER(T1.TEI_SUCH2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$SQL.=" AND TEI_SUCH2 in (select LAR_SUCH_LARTNR from lieferantenartikel where LAR_SUCH_LARTNR ". awisLIKEoderIST($Param[3], True, False, FALSE, 1);
					$SQL.=" AND LAR_REKLAMATIONSKENNUNG is null)";
				}
			}
			else
			{
				$SQL .= " AND T1.TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$SQL.=" AND TEI_WERT2 in (select LAR_LARTNR from lieferantenartikel where LAR_LARTNR ". awisLIKEoderIST($Param[3], True, False, FALSE, 1);
					$SQL.= " AND LAR_REKLAMATIONSKENNUNG is null) ";
				}
			}

			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}

//			$SQL .= ")) ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("0101061023-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$_GET['Export'],$FilialBestandRecht,'LAS',$Param,$MaxDSAnzahl,$Recht400);
				flush();
			}
		}	// Lieferantenartikelsets

$Zeit['VOR OE-Nr']=time();

		if($Param[9]!='')		// OE-Nummern
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'OEN' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (TEI_KEY1 = AST_KEY) AND ";
			$SQL .= " (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND (TEI_ITY_ID2 = 'OEN' ";

			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " AND TEI_SUCH2 " . awisLIKEoderIST($Param[3], True, False, False, 2) . ")";
			}
			else
			{
				$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}
			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}
			If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
			{
				$Bedingung.=" AND BITAND(AST_IMQ_ID,2)=2";
			}
			If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
			{
				$SQL = $SQL ." AND AST_SUCH2 IS NULL";
			}
			$SQL .= "";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("0101061024-artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$_GET['Export'],$FilialBestandRecht,'',$Param,$MaxDSAnzahl,$Recht400);
				flush();
			}
		}	// Lieferantenartikelsets
$Zeit['Nach OE-Nr']=time();

			//******************************************
			// Beliebige Nummer suchen
			//******************************************
		if($Param[11]!='')
		{

			echo '<br><font size=1>Die Ausgabe ist auf max. 100 Datens�tze pro Gruppe beschr�nkt</font>';

			// EAN Nummern
			echo '<table  width=750 id=DatenTabelle border=1>';
			$SQL = "SELECT * FROM EANNummern LEFT OUTER JOIN TeileInfos ON EAN_KEY=TEI_KEY2 ";
			$SQL .= " WHERE EAN_NUMMER " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			$SQL .= " AND (TEI_ITY_ID2='EAN' OR TEI_ITY_ID2 IS NULL)";
			$SQL .= $SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER;

			//awis_Debug(1,$SQL);

			$rsSuche = awisOpenRecordset($con, $SQL);

			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>Typ</td>';
				echo '<td id=FeldBez width=250>ATU Nummer</td></tr>';
			}

			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td><a href=./artikel_Main.php?cmdAktion=EANArtikel&EANKEY=' . $rsSuche['EAN_KEY'][$DSNr] . '>' . $rsSuche['EAN_NUMMER'][$DSNr] . '</a></td>';
				echo '<td width=150 >EAN-Nummer</td>';
				echo '<td width=200 >' . $rsSuche['EAN_TYP'][$DSNr] . '</td>';
				echo '<td width=200 ><a href=./artikel_Main.php?Key=' . $rsSuche["TEI_KEY1"][$DSNr] . '&cmdAktion=ArtikelInfos>' . $rsSuche['TEI_WERT1'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);

$Zeit['EAN']=time();


			// Gebrauchnsnummer
			$SQL = "SELECT * FROM GebrauchsNummern LEFT OUTER JOIN TeileInfos ON GNR_KEY = TEI_KEY2";
			$SQL .= " WHERE upper(GNR_Nummer) " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			$SQL .= " AND (TEI_ITY_ID2 = 'GNR' OR TEI_ITY_ID2 IS NULL)";
			$SQL .= $SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER;

			//awis_Debug(1,$SQL);

			$rsSuche = awisOpenRecordset($con, $SQL);

			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>Typ</td>';
				echo '<td id=FeldBez width=250>Lieferantenartikel</td></tr>';
			}

			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td>' . $rsSuche['GNR_NUMMER'][$DSNr] . '</a></td>';
				echo '<td width=150 >Gebrauchsnummer</td>';
				echo '<td width=200 >' . $rsSuche['EAN_TYP'][$DSNr] . '</td>';
				echo '<td width=200 ><a href=./artikel_Main.php?LARKEY=' . $rsSuche["TEI_KEY1"][$DSNr] . '&cmdAktion=Lieferantenartikel>' . $rsSuche['TEI_WERT1'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);
$Zeit['GNR']=time();


			// Lieferantenartikel-Nummer
			$SQL = "SELECT DISTINCT LAR_KEY, LAR_LIE_NR, LAR_LARTNR, TEI_KEY1, TEI_WERT1";
			$SQL .= " FROM LieferantenArtikel LEFT OUTER JOIN TeileInfos ON LAR_KEY = TEI_KEY2 ";

			if($Param[7]=='on')
			{
				$SQL .= " WHERE LAR_LARTNR " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$SQL.= " AND LAR_REKLAMATIONSKENNUNG is null ";
				}
			}
			else
			{
				$SQL .= " WHERE LAR_SUCH_LARTNR " . awisLIKEoderIST($Param[3], True, TRUE, FALSE, 1) . "";

				If (!($Recht400&2048)==2048) // TR 30.10.07 - Globale Bedingung. Ausblenden von Lieferantenartikel mit Reklamationskennung
				{
					$SQL.= " AND LAR_REKLAMATIONSKENNUNG is null ";
				}
			}

			$SQL .= " AND (TEI_ITY_ID2='LAR' OR TEI_ITY_ID1 IS NULL)";
			$SQL .= $SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER;

			//awis_Debug(1,$SQL);

			$rsSuche = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td>';
				echo '<td width=200 id=FeldBez>Lieferant</td><td width=250 id=FeldBez>ATU Nummer</td></tr>';
			}
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td width=150 ><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=' . $rsSuche['LAR_KEY'][$DSNr] . '>' . $rsSuche['LAR_LARTNR'][$DSNr] . '</a></td>';
				echo '<td width=150 >Lief-Art</td>';
				echo '<td width=200 >' . $rsSuche['LAR_LIE_NR'][$DSNr] . '</td>';
				echo '<td width=200 ><a href=./artikel_Main.php?Key=' . $rsSuche["TEI_KEY1"][$DSNr] . '&cmdAktion=ArtikelInfos>' . $rsSuche['TEI_WERT1'][$DSNr] . '</td>';

				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);
$Zeit['LAR']=time();

// TODO: Pr�fen, was $LiefArt sein soll.
			//**********************************
			// Lieferantenartikelsets
			//**********************************
			$SQL = "SELECT * ";
			$SQL .= " FROM TeileInfos T1 ";
			$SQL .= " INNER JOIN Lieferantenartikel ON T1.TEI_SUCH2 = LAR_LARTNR";
			$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
			$SQL .= " AND T1.TEI_SUCH2 " . awisLIKEoderIST($rsLiefArt['LAR_LARTNR'][$LiefArt],false,false,true,false);
			$SQL .= $SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER;


			If (!($Recht400&2048)==2048)
			{
				$SQL.=" AND LAR_REKLAMATIONSKENNUNG is null";
			}

			//awis_Debug(1,$SQL);

			$rsSuche = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>Lieferant</td></tr>';
			}
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td width=150 ><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=' . $rsSuche['LAR_KEY'][$DSNr] . '>' . $rsSuche['LAR_LARTNR'][$DSNr] . '</a></td>';
				echo '<td width=150 >Lief-Art-Set</td>';
				echo '<td width=200 >' . $rsSuche['LAR_LIE_NR'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);

$Zeit['LAS']=time();

			// OE-Nummern
			$SQL = "SELECT * FROM OENummern LEFT OUTER JOIN  Hersteller ON OEN_HER_ID = HER_ID";
			$SQL .= " LEFT OUTER JOIN TeileInfos ON OEN_KEY = TEI_KEY2";
			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " WHERe OEN_SUCHNUMMER " . awisLIKEoderIST($Param[3], True, false,false,2) . "";
			}
			else
			{
				$SQL .= " WHERE OEN_NUMMER " . awisLIKEoderIST($Param[3], True, False,FALSE, 0) . "";
			}
			$SQL .= " AND (TEI_ITY_ID2='OEN' OR TEI_ITY_ID2 IS NULL)";
			$SQL .= $SUCH_GRENZE_DATENAETZE_BELIEBIGE_NUMMER;

			//awis_Debug(1,$SQL);

			$rsSuche = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td>';
				echo '<td width=200 id=FeldBez>Hersteller</td><td width=250 id=FeldBez>gefundene Nummer</td></tr>';
			}
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td width=150 ><a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY=' . $rsSuche['OEN_KEY'][$DSNr] . '>' . $rsSuche['OEN_NUMMER'][$DSNr] . '</td>';
				echo '<td width=150 >OE-Nummer</td>';
				echo '<td width=200 >' . $rsSuche['HER_BEZEICHNUNG'][$DSNr] . '</td>';

				if($rsSuche['TEI_ITY_ID1'][$DSNr]=='AST')
				{
					echo '<td width=200 ><a href=./artikel_Main.php?Key=' . $rsSuche["TEI_KEY1"][$DSNr] . '&cmdAktion=ArtikelInfos>' . $rsSuche['TEI_WERT1'][$DSNr] . '</a> (' . $rsSuche['TEI_ITY_ID1'][$DSNr] . ')</td>';
				}
				else
				{
					if($rsSuche['TEI_WERT1'][$DSNr]=='')
					{
						echo '<td width=200 >::keine Zuordnung::</td>';
					}
					else
					{
						echo '<td width=200 ><a href=./artikel_Main.php?LARKEY=' . $rsSuche["TEI_KEY1"][$DSNr] . '&cmdAktion=Lieferantenartikel>' . $rsSuche['TEI_WERT1'][$DSNr] . '</a> (' . $rsSuche['TEI_ITY_ID1'][$DSNr] . ')</td>';
					}
				}
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);

		}	// Ende beliebige Nummer
$Zeit['OEN']=time();


	}

			// Export ist aktiv -> Dateidownlad anzeigen
	if(isset($_GET['Export']))
	{
		echo "<br><br><a accesskey=x onmouseover=\"status='�ffnen';return true;\" onmouseout=\"status='AWIS';return true;\" href=/export/" . $AWISBenutzer->BenutzerName() . ".csv><img src=/bilder/dateioeffnen.png border=0 title='�ffnen (Alt+X)'></a><br>";
		echo "<br><input type=image border=0 src=/bilder/NeueListe.png accesskey=t title='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'>";
	}
	else
	{
		echo "</table>";
	}

	if($Param[3]!='')		// Sonstige Nummern gesucht
	{
		$OEPRechte = awisBenutzerRecht($con, 403);
		if((($DSAnz==0 OR  $Param[14]=='on') AND ($Param[9]!='on' OR $Param[11]!='on'))  AND ($OEPRechte&4)==4)		// Keine gefunden und OE-Nummer oder OE-Preise anzeigen
		{
			$SQL = 'SELECT * FROM OENummernPreise, Hersteller, OENUMMERNPREISIMPORTQUELLEN WHERE OEP_HER_ID = HER_ID(+) AND OEP_OIQ_NR = OIQ_NR(+) ';
			$SQL .= " AND OEP_SUCHNUMMER " . awisLIKEoderIST($Param[3],false,false,false,2) . "";
			if($MaxDSAnzahl>0)
			{
				$SQL = $SQL . ' AND ROWNUM <= ' . $MaxDSAnzahl;
			}

			$SQL .= " ORDER BY OIQ_STAND DESC";

			$rsOEP = awisOpenRecordset($con, $SQL);
			$rsOEPZeilen = $awisRSZeilen;

			if($rsOEPZeilen!=0)		// Daten gefunden
			{
				echo "<table  width=100% id=DatenTabelle border=1>";
				echo '<tr><td colspan=6 align=center id=FeldBez>OE-Preise ohne OE-Nummern Zuordnung</td></tr>';
				echo '<tr><td id=FeldBez>OE-Nummer</td>';
				echo '<td id=FeldBez>Bezeichnung</td>';
				echo '<td id=FeldBez>Hersteller</td>';
				echo '<td id=FeldBez>Importquelle</td>';
				echo '<td id=FeldBez>Preis</td>';
				echo '<td id=FeldBez>Ersatz-OE</td>';
				echo '</tr>';
			}

			for($Zeile=0;$Zeile<$rsOEPZeilen;$Zeile++)
			{
				echo '<tr><td ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'. $rsOEP['OEP_NUMMER'][$Zeile] . '</td>';
				echo '<td ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEP['OEP_BEZEICHNUNG'][$Zeile] . '</td>';
				echo '<td ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEP['HER_BEZEICHNUNG'][$Zeile] . '</td>';
				echo '<td ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEP['OIQ_QUELLE'][$Zeile] . ', ' . $rsOEP['OIQ_STAND'][$Zeile] . '</td>';
				echo '<td align=right ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . awis_format($rsOEP['OEP_PREIS'][$Zeile],'Currency') . '</td>';
				echo '<td ' . (($Zeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>';
				echo '<a href=./artikel_Main.php?cmdAktion=OENummern&OEN_NUMMER=' . $rsOEP['OEP_ERSATZNR'][$DSNr] . '>'  . $rsOEP['OEP_ERSATZNR'][$Zeile] . '</a></td>';
				echo '<tr>';
			}
			unset($rsOEP);

			if($rsOEPZeilen!=0)		// Daten gefunden
			{
				echo '</table>';
			}
			else					// Keine Daten gefunden
			{
				echo '<span class=HinweisText>Keine OE-Preise gefunden</span>';
			}
		}
	}		// Ende OE-Preise

	echo "<br><font size=1>Es wurden " . ($DSAnz+$rsOEPZeilen) . " Artikel " . (($DSAnz+$rsOEPZeilen)>=$MaxDSAnzahl?'anzeigt. Es k�nnten jedoch weitere Datens�tze angezeigt werden':'gefunden').".</font>";

		//**********************************************************************************************
		// L�schartikel suchen, wenn kein Datensatz gefunden wurde und ein Artikel angegeben wurde
		//**********************************************************************************************
	if(($DSAnz+$rsOEPZeilen)==0 AND $Param[1]!='' AND ($Recht400&512)==512) // CA 17.09.07 Recht400=512. Pr�fen ob L�schartikel angezeigt werden d�rfen.
	{
		ZeigeGeloeschteArtikel($Param[1],$con);
	}

		//**********************************************************************************************
		//  St�ckliste
		//**********************************************************************************************
	if(($DSAnz+$rsOEPZeilen)==0 AND $Param[1]!='')
	{
		$SQL = 'SELECT DISTINCT PARTNR, AST_BEZEICHNUNGWW, STCKL_NR, AST_KEY ';
		$SQL .= ' FROM STUECKLISTE ';
		$SQL .= ' LEFT OUTER JOIN ARTIKELSTAMM ON PARTNR = AST_ATUNR';
		$SQL .= ' WHERE UPPER(STCKL_NR)'.awisLIKEoderIST($Param[1],true);
		$SQL .= ' ORDER BY STCKL_NR, PARTNR';
		$rsSTL = awisOpenRecordset($con,$SQL);
		$rsSTLZeilen = $awisRSZeilen;

		$LetzteSTL = '';

		if($rsSTLZeilen>0)
		{

			for($STLZeile=0;$STLZeile<$rsSTLZeilen;$STLZeile++)
			{
				if($LetzteSTL!=$rsSTL['STCKL_NR'][$STLZeile])
				{
					if($LetzteSTL!='')
					{
						echo '</table>';
					}
					echo '<h2>St�ckliste ' . $rsSTL['STCKL_NR'][$STLZeile] . '</h2>';
					echo '<table border=1>';
					echo '<tr>';
					echo '<td class=FeldBez>ATU-Nr</td>';
					echo '<td class=FeldBez>Artikelbezeichnung</td>';
					echo '</tr>';

					$LetzteSTL=$rsSTL['STCKL_NR'][$STLZeile];
				}
				echo '<tr>';

				echo '<td>';
				if($rsSTL['AST_KEY'][$STLZeile]!='')
				{
					echo '<a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key='.$rsSTL['AST_KEY'][$STLZeile].'>';
					echo ''.$rsSTL['PARTNR'][$STLZeile].'';
					echo '</a>';
					echo '</td>';
					echo '<td>';
					echo ''.$rsSTL['AST_BEZEICHNUNGWW'][$STLZeile].'';
					echo '</td>';
				}
				elseif(substr($rsSTL['PARTNR'][$STLZeile],0,1)=='+')
				{
					$rsLOA = awisOpenRecordset($con,'SELECT LOA_BEZEICHNUNGWW, LOA_LOESCHDATUM FROM LOESCHARTIKEL WHERE LOA_AST_ATUNR=\''.substr($rsSTL['PARTNR'][$STLZeile],1).'\' ORDER BY LOA_LOESCHDATUM DESC');
					if($rsLOA['LOA_LOESCHDATUM'][0]!='')
					{
						echo '<i>'.substr($rsSTL['PARTNR'][$STLZeile],1).'</i>';
					}
					echo '</td>';
					echo '<td>';
					echo '<i>'.$rsLOA['LOA_BEZEICHNUNGWW'][0].'</i><span class=HinweisText> Gel�scht am: '.$rsLOA['LOA_LOESCHDATUM'][0].'</span>';
					echo '</td>';
				}
				else
				{
					echo ''.$rsSTL['PARTNR'][$STLZeile].'';
					echo '</td>';
					echo '<td>';
					echo '<span class=HinweisText>unbekannter Artikel</span>';
					echo '</td>';
				}

				echo '</tr>';
			}
			echo '</table>';
		}
	}

	if(awisBenutzerRecht($con, 501))
	{
		echo "<br><br>Weitere Suche bei <a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&OEN_NUMMER=" . str_replace(' ','%20',$Param[3]) . "&ZurueckLink=/ATUArtikel/artikel_Main.php?cmdAktion=Suche>Fremdkauf.";
	}
}

//*****************************************************
//
// Einzelner Datensatz -> Formular anzeigen
//
//*****************************************************

if($DSAnz==-1)  //$rsArtikelZeilen==1)
{
	echo "<form name=frmArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));

	$SQL = "SELECT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG ";
	$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN";

	if(isset($_GET["ATUNR"]))	// ATUNR als Parameter angegeben
	{
		$SQL .= " WHERE AST_ATUNR='" . $_GET["ATUNR"] . "'";
	}
	else								// ASTKEY als Parameter angegeben
	{
		$SQL .= " WHERE AST_KEY=0" . $ASTKEY;
	}

	If (!($Recht400&128)==128) // CA 14.09.07 - Globale Bedingung. Ausblenden von Cross-Artikeln
	{
		$SQL .= " AND BITAND(AST_IMQ_ID,2)=2";
	}

    If (!($Recht400&256)==256) // CA 14.09.07 - Globale Bedingung. Ausblenden von Sonderartikeln (Such2)
	{
		$SQL .= " AND AST_SUCH2 IS NULL";
	}

	$SQL .= " AND AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID";

	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;
	if($rsArtikel===FALSE)
	{
		awisErrorMailLink("0101061025-artikel_Artikel.php", 2, $awisDBFehler);
		die();
	}

	$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID, ASI_KEY FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][0] . "'");
	$rsASIZeilen = $awisRSZeilen;

	if($rsASI===FALSE)
	{
		awisErrorMailLink("0101061026-artikel_Artikel.php", 2, $awisDBFehler);
		die();
	}

	if($rsArtikelZeilen<1)			// Keine Daten gefunden
	{
		if(ZeigeGeloeschteArtikel($_GET['ATUNR'],$con)==0 AND ($Recht400&512)==512) // CA 17.09.07 Recht400=512. Pr�fen ob L�schartikel angezeigt werden d�rfen.
		{
			echo "<span class=HinweisText>Mit den angegebenen Kriterien konnte leider kein Artikel gefunden werden.</span>";
		}
	}
	else
	{
		If(substr($rsArtikel['AST_ATUNR'][0],0,2)=='CA' OR substr($rsArtikel['AST_ATUNR'][0],0,2)=='CB')
		{
			$SuchArt = (substr($rsArtikel['AST_ATUNR'][0],0,2)=='CA'?'CB' . substr($rsArtikel['AST_ATUNR'][0],2):'CA' . substr($rsArtikel['AST_ATUNR'][0],2));
			$rsCACB=awisOpenRecordset($con, "SELECT AST_KEY, AST_ATUNR FROM AWIS.ArtikelStamm WHERE AST_ATUNR ='" . $SuchArt . "'");
		}

		echo "<input type=hidden name=txtAST_ATUNR value='" . $rsArtikel['AST_ATUNR'][0] . "'>";
		echo "<table border=0 width=100%>";
		echo "<tr><td><font size=4><b>A.T.U Artikel " . (isset($rsArtikel['AST_BEKANNTWW'][0]) AND $rsArtikel['AST_BEKANNTWW'][0]==1?"<i>":"") . $rsArtikel['AST_ATUNR'][0];
		echo '</i></b>';
		if(isset($rsCACB['AST_ATUNR'][0]) AND $rsCACB['AST_ATUNR'][0]!='')
		{
			echo ' (<a href=./artikel_Main.php?Key=' . $rsCACB['AST_KEY'][0] . '&cmdAktion=ArtikelInfos>' . $rsCACB['AST_ATUNR'][0] . '</a>)';
		}
		echo '</font></td><td align=center>';

		if(($BearbeitungsStufe&4)==4)		// Neuen Artikel hinzuf�gen
		{
			echo "&nbsp;<a accesskey=+ title='Neuer Artikel (Alt +)' href=./artikel_Main.php?cmdAktion=ArtikelInfos&Hinzufuegen=True><img src=/bilder/plus.png border=0 title='Hinzuf�gen (Alt+N)'></a>";
		}
		if($BearbeitungsStufe>0)		// Speichern-Knopf, sobal �nderungen m�glich sind
		{
			echo " <input type=image accesskey=S title='Speichern (Alt S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";
		}

		echo '</td>';
		echo "<td width=100><font size=1>" . $rsArtikel["AST_USER"][0] ."<br>"  . $rsArtikel["AST_USERDAT"][0] . "</font></td>";
//		echo "<td width=30 backcolor=#000000 align=right><img border=0 src=/bilder/NeueListe.png accesskey='T' title='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'></td>";
		echo "</tr></table>";


			// Zeile 1

		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=110><col width=100> <col width=90><col width=*> <col width=30><col width=30> <col width=30><col width=30> <col width=50><col width=40><col width=40></colgroup>";
		echo "<tr>";
		echo "<td  id=FeldBez>ATU-Nr</td><td id=FeldHervorgehoben><input type=hidden name=txtAST_KEY value=". $rsArtikel['AST_KEY'][0] . ">" . $rsArtikel['AST_ATUNR'][0] . "</td>";

		if($rsArtikel['AST_BEZEICHNUNGWW'][0]=='')			// Nichts gefunden -> evtl. L�schartikel
		{
			$rsLOA = awisOpenRecordset($con, "SELECT * FROM LOESCHARTIKEL WHERE LOA_AST_ATUNR ='" . $rsArtikel['AST_ATUNR'][0] . "' ORDER BY LOA_LOESCHDATUM DESC");
			echo "<td  id=FeldBez>Bez-WWS</td><td id=FeldHervorgehoben>" . (!isset($rsLOA['LOA_LOESCHDATUM'][0])?'---':'<font color=#FF0000>aus WWS gel�scht am ' . $rsLOA['LOA_LOESCHDATUM'][0]) . "</font></td>";
			unset($rsLOA);
		}
		else
		{
			echo "<td  id=FeldBez>Bez-WWS</td><td id=FeldHervorgehoben>" . ($rsArtikel['AST_BEZEICHNUNGWW'][0]==''?'- -':$rsArtikel['AST_BEZEICHNUNGWW'][0]) . "</td>";
		}
		echo "<td  id=FeldBez>WG</td><td id=FeldHervorgehoben>" . $rsArtikel['WGR_ID'][0] . "</td>";
		echo "<td  id=FeldBez>UWG</td><td id=FeldHervorgehoben>" . $rsArtikel['WUG_ID'][0] . "</td>";
		echo "<td  id=FeldBez>KENN1</td><td id=FeldHervorgehoben>" . $rsArtikel['AST_KENNUNG'][0] . "</td>";

		echo "<td backcolor=#000000 align=right><img border=0 src=/bilder/NeueListe.png accesskey='T' title='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'></td>";

		echo "</tr>";
		echo "</table>";

			// Zeile 2

		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=110><col width=*> <col width=90><col width=70> <col width=100><col width=200> </colgroup>";
		echo "<tr>";

		$SQL = "SELECT DISTINCT KFZ_HERST.BEZ AS Hersteller FROM ARTIKEL_KTYPNR_AWST, KFZ_TYP, KFZ_MODELL, KFZ_HERST  ";
		$SQL .= " WHERE ARTIKEL_KTYPNR_AWST.KTYPNR = KFZ_TYP.KTYPNR AND KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR AND KFZ_HERST.KHERNR = KFZ_MODELL.KHERNR";
		$SQL .= " AND ATU_NR='" . $rsArtikel['AST_ATUNR'][0] . "'";

		$rsFahrzeuge = awisOpenRecordset($con, $SQL);
		$Fahrzeuge='';
		for($FzNr=0;$FzNr<$awisRSZeilen;$FzNr++)
		{
			$Fahrzeuge .= ', ' . substr($rsFahrzeuge['HERSTELLER'][$FzNr],0,4);
		}
		echo "<td  id=FeldBez>Fahrzeuge</td><td id=FeldHervorgehoben>" . substr($Fahrzeuge,2) . "</td>";

		unset($rsFahrzeuge);

		$LiefKey = awis_ArtikelInfo(40, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
		echo "<td  id=FeldBez>Hauptlief.</td><td id=FeldHervorgehoben><a href=/stammdaten/lieferanten.php?LIENR=" . $LiefKey . "&Zurueck=artikel_LIEF&ASTKEY=" . $rsArtikel["AST_KEY"][0] . " >" . $LiefKey . "</td>";
		echo "<td  id=FeldBez>Lief-Art-Nr.</td><td id=FeldHervorgehoben>" . awis_ArtikelInfo(41, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
		echo "</tr>";
		echo "</table>";

			// Zeile 3

		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=110><col width=*><col width=50><col width=80> </colgroup>";
		echo "<tr>";
		if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Katalogbezeichung
		{
			echo "<td  id=FeldBez>Bez.-Katalo<u>g</u></td>";
			echo "<td><input type=text accesskey=g size=90 maxlength=100 name=txtAST_BEZEICHNUNG value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0] . "\">";
			echo "<input type=hidden name=txtAST_BEZEICHNUNG_old value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0]. "\"></td>";
		}
		else if(($Recht400&4096)==4096)
		{
			echo "<td  id=FeldBez>Bez.-Katalog</td><td>" . $rsArtikel['AST_BEZEICHNUNG'][0] . "</td>";
		}
		else if(($Recht400&128)==128
			 and $rsArtikel['WGR_ID'][0] != '01' and $rsArtikel['WGR_ID'][0] != '02'
			 and $rsArtikel['WGR_ID'][0] != '23' and $rsArtikel['WGR_ID'][0] != '14')
		{
			echo "<td  id=FeldBez>Bez.-Katalog</td><td>" . $rsArtikel['AST_BEZEICHNUNG'][0] . "</td>";
		}
		else
		{
			echo "<td width=600>&nbsp;</td><td>&nbsp;</td>";
		}

		echo "<td width=100>&nbsp;</td>";
		echo "<td  id=FeldBez>VK</td><td id=FeldHervorgehoben>" . awis_format($rsArtikel['AST_VK'][0],"Currency") . "</td>";

		$AltteilWert='0,00';
		if(awis_ArtikelInfo(190, $rsASI, $rsArtikel["AST_ATUNR"][0],'ASI_WERT')!='')
		{
			$SQL = 'SELECT ATW_BETRAG';
			$SQL .= ' FROM Altteilwerte';
			$SQL .= ' WHERE ATW_LAN_CODE=\'DE\' AND ATW_KENNUNG=\''.awis_ArtikelInfo(190, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT").'\'';

			$rsATW = awisOpenrecordset($con,$SQL);
			if(isset($rsATW['ATW_BETRAG'][0]))
			{
				$AltteilWert=$rsATW['ATW_BETRAG'][0];
			}
		}
		echo "<td  id=FeldBez width=80>Altteilwert</td><td id=FeldHervorgehoben>" . awis_format($AltteilWert,'Currency') . "</td>";
		echo "</tr>";
		echo "</table>";



			// Teil 2: Block aus drei Spalten
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=45%><col width=40%><col width=15%></colgroup>";
		echo "<tr>";

			// Erste Spalte
		echo '<td valign=top>';
			// Zeile A1

		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=*><col width=200><col width=250><col width=100></colgroup>";
		echo "<tr>";

		$Anz = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if((isset($_GET['AltArt']) AND $_GET['AltArt']=='Ja') OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ListeKey = explode("<br>", awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_KEY"));
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			$Nr = 0;
			foreach($ASTs AS $Eintrag=>$Wert)
			{
				if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Kennung
				{
					$Erg .= "<br><input type=text size=8 name=txtASI_120_" . $ListeKey[$Nr++] . ' value=' .  $Wert . '>';
					if($Wert!='')
					{
						$Erg .= "<a title='Geht zum Artikel'  href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR " . $Wert . '><img border=0 src=/bilder/info_klein.png></a>';
					}
				}
				else
				{
					$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
				}
			}

			echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ</a></td><td>" . substr($Erg,4) . "</td>";

		}
		else		// Nur die Anzahl
		{
			if(($Recht400&128)==128)
			{
				echo "<td id=FeldBez><a href=./artikel_Main.php?AltArt=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ</a></td><td>" . $Anz . "</td>";
			}
			else
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
			}
		}

		if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Kennung
		{
			echo "<td height=26 id=FeldBez>KENN-Vorschlag</td>";
			echo "<td><input type=text size=2 name=txtAST_KENNUNGVORSCHLAG value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\">";
			echo "<input type=hidden name=txtAST_KENNUNGVORSCHLAG_old value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\"></td>";
		}
		else
		{
			if(($Recht400&128)==128)
			{
				echo "<td height=26 id=FeldBez>KENN-Vorschlag</td><td>" . ($rsArtikel['AST_KENNUNGVORSCHLAG'][0]==''?'--':$rsArtikel['AST_KENNUNGVORSCHLAG'][0]) . "</td>";
			}
			else
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
			}
		}
		echo "</tr>";

				// Zeile A2
		echo "<tr>";


				/*************************
				* Zusatzinfos: ZUBEH�R
				*************************/

		$Anz = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if((isset($_GET['Zubehoer']) AND $_GET['Zubehoer']=='Ja') OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ListeKey = explode("<br>", awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_KEY"));
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			$Nr = 0;
			foreach($ASTs AS $Eintrag=>$Wert)
			{
					if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Kennung
					{
						$Erg .= '<br><input type=text size=8 name=txtASI_121_' . $ListeKey[$Nr++] . ' value=' .  $Wert . '>';
						if($Wert!='')
						{
							$Erg .= "<a title='Geht zum Artikel' href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=" . $Wert . '><img border=0 src=/bilder/info_klein.png></a>';
						}
					}
					else
					{
						$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
					}
			}
			echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r</a></td><td>" . substr($Erg,4) . "</td>";
		}
		else
		{
			if(($Recht400&128)==128)
			{
				echo "<td id=FeldBez><a href=./artikel_Main.php?Zubehoer=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r</a></td><td>" . $Anz . "</td>";
			}
			else
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
			}
		}


			/***************************
			* Muster vorhanden
			***************************/
		if(($Recht400&128)==128)
		{
			echo "<td height=26 id=FeldBez>Muster vorhanden</td><td>";
		}
		else
		{
			echo "<td>&nbsp;</td><td>";
		}

		$Wert = (awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT")==0?'Nein':'Ja');
		if(($BearbeitungsStufe & 16) == 16)		// Katalogfelder: Muster vorhanden
		{
			$Wert = awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT");
			echo "<input name=txtMusterVorhanden_KEY type=hidden value=0" . awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_KEY") . ">";
			echo "<select name=txtMusterVorhanden>";
			echo "<option " . ($Wert!=0?'selected':'') . " value=-1>Ja</option>";
			echo "<option " . ($Wert==0?'selected':'') . " value=0>Nein</option>";
			echo "</select></td>";
		}
		else
		{
			if(($Recht400&128)==128)
			{
				echo $Wert . "</td>";
			}
			else
			{
				echo "</td>";
			}
		}
		echo "</tr>";

				// Zeile A3
		echo "<tr>";


				/*************************
				* VPE
				*************************/

		echo "<td id=FeldBez>VPE";

		echo "</td><td>";
		if(($BearbeitungsStufe & 2)==2)
		{
			$Key = explode(',',awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_KEY"));
			$Key[0] = str_replace('<br>','',$Key[0]);

			echo '<input type=hidden size=8 name=txtASI_old_60_' .$Key[0] . " value='" . awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "'>";
			echo '<input type=text size=8 name=txtASI_60_' . $Key[0] . " value='" . awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "'>";
			echo '/<span title="VPE aus WW">'.awis_ArtikelInfo(191, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "</span>";
			echo '</td>';
		}
		else
		{
			echo '<span title="VPE aus Crossing">'.awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "</span>";
			echo '/<span title="VPE aus WW">'.awis_ArtikelInfo(191, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "</span>";

			echo '</td>';
		}

				/*************************
				* an Einkauf am
				*************************/

		$Mldg = awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_KEY");

		if(($BearbeitungsStufe & 8) == 8)
		{
			$Wert = awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
			echo '<td height=26 id=FeldBez>an Ein<u>k</u>auf am</td>';
			echo "<td><input type=hidden name=txtMELDUNG_old value=\"" . $Wert . "\">";
			echo "<input size=9 accesskey=k type=text name=txtMELDUNG value=\"" . $Wert . "\">";
			echo "<input type=hidden name=txtMELDUNG_KEY Value=0" . $Mldg . "></td>";
		}
		else		// Keine Bearbeitung erlaubt
		{
			if(($Recht400&128)==128)
			{
				echo "<td height=26 id=FeldBez>an Einkauf am</td><td>" . ($Mldg==''?"n.v.":$Mldg) . "</td>";
			}
			else
			{
				echo "<td>&nbsp;</td><td>&nbsp;</td>";
			}
		}


		echo '</tr></table>';

		echo '</td>';
		// Zweite Spalte
		echo '<td valign=top >';

				// Spalte B1
		echo "<table border=0 width=100% >";
		echo "<colgroup><col width=25%><col width=25%><col width=25%><col width=25%></colgroup>";


		echo "<tr>";
		if(($Recht400&4)==4)
		{
			echo '<td colspan=2 align=center id=FeldBez style="border-style: solid; border-width: 1px;"><font size=2>Absatz</font></td>';
		}else{echo "<td>&nbsp;</td>";}
		if(($Recht400&8)==8)
		{
			echo '<td colspan=2 align=center id=FeldBez style="border-style: solid; border-width: 1px;"><font size=2>Reklamationen</font></td>';
		}else{echo "<td>&nbsp;</td>";}
		echo '</tr>';
		echo '<tr>';
		if(($Recht400&4)==4)
		{
//			$Wert = awis_ArtikelInfo(33, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
//			$Wert += awis_ArtikelInfo(34, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
			$Wert = awis_ArtikelInfo(37, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Gesamt: Weiden, Werl, E-Commerce
			echo '<td  id=FeldBez style="border-style: solid; border-width: 1px;">Vorjahr</td><td id=FeldHervorgehoben style="border-style: solid; border-width: 1px;">' . awis_format($Wert,"Standardzahl,0") .'</td>';
		}else{echo "<td>&nbsp;</td>";}
		if(($Recht400&8)==8)
		{
			$Wert = awis_ArtikelInfo(131, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
			echo '<td id=FeldBez style="border-style: solid; border-width: 1px;">Vorjahr</td><td id=FeldHervorgehoben style="border-style: solid; border-width: 1px;">' . awis_format($Wert,"Standardzahl,0") . '</td>';
		}else{echo "<td>&nbsp;</td>";}
		echo '</tr>';
		echo '<tr>';

		// Absatz laufendes Jahr

		if(($Recht400&4)==4)
		{
//			$Wert = awis_ArtikelInfo(30, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
//			$Wert += awis_ArtikelInfo(31, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
			$Wert = awis_ArtikelInfo(38, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Gesamt: Weiden, Werl, E-Commerce
			echo '<td  id=FeldBez style="border-style: solid; border-width: 1px;">Jahr</td><td id=FeldHervorgehoben style="border-style: solid; border-width: 1px;">' . awis_format($Wert,"Standardzahl,0") . '</td>';
		}else{echo "<td>&nbsp;</td>";}
		if(($Recht400&8)==8)
		{
			$Wert = awis_ArtikelInfo(130, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
			echo '<td id=FeldBez style="border-style: solid; border-width: 1px;">Jahr</td><td id=FeldHervorgehoben style="border-style: solid; border-width: 1px;">' . awis_format($Wert,"Standardzahl,0") . '</td>';
		}else{echo "<td>&nbsp;</td>";}
		echo '</tr>';

		if(($Recht400&16)==16)
		{
			echo '<tr>';
			echo '<td height=26 id=FeldBez style="border-style: solid; border-width: 1px;">durch</td><td colspan=3>';

			if(($BearbeitungsStufe & 8) == 8)
			{
				$APP = awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
				echo "<input type=hidden name=txtMELDUNGDURCH_old Value=0" . $APP . '>';
				echo "<input type=hidden name=txtMELDUNGDURCH_KEY Value=0" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_KEY") . ">";
				echo "<select name=txtMELDUNGDURCH>";
				$rsAPP = awisOpenRecordset($con, "SELECT * FROM ArtikelPruefPersonal ORDER BY APP_NAME");
				for($APPNr=0;$APPNr<$awisRSZeilen;$APPNr++)
				{
					if($rsAPP["APP_ID"][$APPNr]==$APP OR $rsAPP["APP_AKTIV"][$APPNr]=='')
					{
						echo "<option  ";
						if($APP == $rsAPP["APP_ID"][$APPNr])
						{
							echo " selected ";
						}
						echo " value=" . $rsAPP["APP_ID"][$APPNr] . ">" . $rsAPP["APP_NAME"][$APPNr] . "</option>";
					}
				}
				echo "</select></td>";
			}
			else		// Keine Bearbeitung erlaubt
			{
				echo "" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
			}

			echo '</tr>';
		}
		else {echo "<tr><td></td></tr>";}

		echo "</table>";

		echo '</td>';

			// Dritte Spalte
		echo '<td valign=top>';
			if (($Recht400&2)==2)
			{

				echo "<table border=1 width=100% >";
				echo "<colgroup><col width=50%><col width=50%></colgroup>";
				echo "<tr>";
				echo "<td colspan=2 align=center id=FeldBez><font size=2>Bestand</font></td></td>";

				echo "</tr><tr>";
				echo "<td id=FeldBez>Weiden</td><td  id=FeldHervorgehoben>" . awis_format((awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT")-awis_ArtikelInfo(13, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT")),"Standardzahl,0") . "</td>";

				echo "</tr><tr>";
				echo "<td id=FeldBez>Werl</td><td id=FeldHervorgehoben>" . awis_format((awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT")-awis_ArtikelInfo(14, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT")),"Standardzahl,0") . "</td>";

				echo "</tr><tr>";
				$LagerBest = awis_format(awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0");
				$LagerBestFilialen = $LagerBest;

					// Neu, 14.2.2008. Jede Filiale sieht auch ihren eigenen Bestand
				$Anzeige = '';
				$ToolTipp='';
				if(awisBenutzerFilialen($con,$AWISBenutzer->Benutzername(),2)!='')
				{
					$SQL = 'SELECT FIB_BESTAND, FIB_FIL_ID';
					$SQL .= " FROM FilialBestand";
					$SQL .= " WHERE ";
					$SQL .= " FIB_AST_ATUNR " . awisLIKEoderIST($rsArtikel["AST_ATUNR"][0],1) . "";
					$SQL .= " AND FIB_FIL_ID IN(".awisBenutzerFilialen($con,$AWISBenutzer->Benutzername(),2).")";
					$rsFIB = awisOpenrecordset($con, $SQL);
					$Anzeige = '';
					for($i=0;$i<$awisRSZeilen;$i++)
					{
						$Anzeige .= '/'.$rsFIB['FIB_BESTAND'][$i].($awisRSZeilen>1?'('.$rsFIB['FIB_FIL_ID'][$i].')':'');
						$ToolTipp = 'Filialbestand der Filiale '.$rsFIB['FIB_FIL_ID'][$i].'';
					}
				}

				If($LagerBest<>0 AND $FilialBestandRecht>0)
				{
					echo "<td id=FeldBez>Filialen</td><td id=FeldHervorgehoben><a href=./filialbestand.php?ATUNR=" . $rsArtikel["AST_ATUNR"][0] . "&NurBestand=1&Zurueck=ArtikelInfos&ASTKEY=". $rsArtikel["AST_KEY"][0] . "><span title='Gesamtbestand in allen Filialen'>" . $LagerBest . "</span></a><span title='".$ToolTipp."'>".$Anzeige."</span></td>";
				}
				else
				{
					echo "<td id=FeldBez>Filialen</td><td id=FeldHervorgehoben>" . $LagerBest .  "".$Anzeige."</td>";
				}

				echo '</tr></table>';

			}
			else {
				echo "<table border=0 width=100% >";
				echo "<colgroup><col width=50%><col width=50%></colgroup>";
				echo "<tr>";
				//echo "<td>&nbsp;</td>";
				echo '</tr></table>';
			}
		echo '</td>';
		echo "</tr>";
		echo "</table>";


			// Artikel als Parameter speichern, um sie in den Unterregistern verwenden zu k�nnen
		awis_BenutzerParameterSpeichern($con, "401", $AWISBenutzer->BenutzerName(), $rsArtikel["AST_KEY"][0] . ";" . $rsArtikel["AST_ATUNR"][0]);
		// Unterregister speichern
		if(isset($_GET['Seite']))
		{
			$cmdAktion=$_GET['Seite'];
		}
		else
		{
			$cmdAktion = 'LieferantenArtikel';
		}
//		$cmdAktion = (isset($_GET['Seite'])?'LieferantenArtikel':$_GET['Seite']);
		awis_RegisterErstellen(41, $con);

		if($BearbeitungsStufe>0)
		{
			echo "<input type=hidden name=Speichern Value=True>";
//			echo " <input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";

				// Manuelle Crossing, falls Berechtigung vorhanden
			if(($BearbeitungsStufe&8)==8 AND $rsArtikel["AST_IMQ_ID"][0]==4 AND $rsArtikel["AST_WUG_KEY"][0]==1)
			{
				echo " <input type=image accesskey=x title='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Loeschen=True'>";
			}

			if(($BearbeitungsStufe&6)>0)
			{
					// Cursor in das erste Feld setzen
				echo "<Script Language=JavaScript>";
				echo "document.getElementsByName(\"txtAST_BEZEICHNUNG\")[0].focus();";
				echo "</Script>";
			}
		}

	}	// Datensatz gefunden?
}
echo "</form>";

if($CursorFeld!='')
{
	echo "<SCRIPT type='text/javascript'>";
	echo "document.getElementsByName('$CursorFeld')[0].focus()";
	echo "</SCRIPT>";
}

/**
 * Schreibt ein Recordset in die Seite
 *
 * @param string $rsArtikel
 * @param string $rsArtikelZeilen
 * @param string $con
 * @param string $Export
 * @param string $FilialBestandRecht
 * @param string $Typ
 * @author Sacha Kerres
 * @since  2005
 */
function _ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$Export,$FilialBestandRecht,$Typ='',$Param,$MaxDSAnzahl,$Recht400)
{
	global $CursorFeld;

	if($Export=='True')
	{
		if(file_exists(awis_UserExportDateiName('.csv')))
		{
			$fd = fopen(awis_UserExportDateiName('.csv'),'a+');	// Anh�ngen
		}
		else
		{
			$fd = fopen(awis_UserExportDateiName('.csv'),'w');		// Neu
			fputs($fd,"ATUNR;Bezeichnung WW;Bezeichnung Cross;VK;Lager-Best;Filial-Best;WGr;Funstelle\r\n");
		}

		for($i=0;$i<$rsArtikelZeilen;$i++)
		{
			fputs($fd,$rsArtikel["AST_ATUNR"][$i] . ';');
			fputs($fd,str_replace(';', ',', $rsArtikel["AST_BEZEICHNUNGWW"][$i]) . ';');
			fputs($fd,str_replace(';', ',', $rsArtikel["AST_BEZEICHNUNG"][$i]) . ';');
			fputs($fd,awis_format($rsArtikel["AST_VK"][$i],'Standardzahl') . ';');		// englisch wg. Excel

			$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AIT_ID IN (10,11,12,13,14) AND ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][$i] . "'");
			$LagerBest = (awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT")-awis_ArtikelInfo(13, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT"));
			$LagerBest += (awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT")-awis_ArtikelInfo(14, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT"));

			fputs($fd,$LagerBest . ';');
			fputs($fd,awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT") . ';');
			fputs($fd,$rsArtikel["WUG_WGR_ID"][$i] . ';');
			fputs($fd,$rsArtikel["FUNDSTELLE"][$i] );
			fputs($fd,"\r\n");
		}

		fclose($fd);
		echo '.';			// Anzeige
	}
	else
	{
		if($rsArtikelZeilen>0)
		{
			SchreibeUeberschrift($Param);
		}

		for($i=0;$i<$rsArtikelZeilen AND $i<$MaxDSAnzahl;$i++)
		{
			echo "<tr>";

			if($Typ=='')
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?Key=" . $rsArtikel["AST_KEY"][$i] . "&cmdAktion=ArtikelInfos";
				if($CursorFeld=='')
				{
					$CursorFeld = 'EingabeFeld';
					echo ' name=EingabeFeld ';
				}

				echo ">" . $rsArtikel["AST_ATUNR"][$i] . "</a> (" . $rsArtikel["FUNDSTELLE"][$i] . ") " . "</td>";
			}
			elseif($Typ=='LAS')		// Speziell bei LAS
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
				if($rsArtikel["AST_KEY"][$i]=='')
				{
					echo "" . $rsArtikel["TEI_WERT1"][$i] . " (" . $rsArtikel["FUNDSTELLE"][$i] . ") ";
				}
				else
				{
					echo "" . $rsArtikel["TEI_WERT1"][$i] . " (" . $rsArtikel["FUNDSTELLE"][$i] . ") ";
					echo "<br><a href=./artikel_Main.php?Key=" . $rsArtikel["AST_KEY"][$i] . "&cmdAktion=ArtikelInfos";
					if($CursorFeld!='')
					{
						$CursorFeld = 'EingabeFeld';
						echo ' name=EingabeFeld';
					}

					echo ">" . $rsArtikel["AST_ATUNR"][$i] . "</a>";
				}
				echo "</td>";
			}

			echo '<td ' . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';
			echo ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?'<b></b>' . $rsArtikel["AST_BEZEICHNUNGWW"][$i].'<br>':'');

			if(($Recht400&4)==4)
			{
				echo (($rsArtikel['AST_IMQ_ID'][$i]&4)==4?"<b>Cross: </b><i>" .$rsArtikel["AST_BEZEICHNUNG"][$i] . "</i>":'') . '</a></td>';
			}

			echo '<td ' . ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?(($i%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss "):'bgcolor=#FF0000 ') . '>' . ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?'X':'') . '</td>';
			echo '<td ' . ((($rsArtikel['AST_IMQ_ID'][$i]&4)==4)?(($i%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss "):'bgcolor=#FF0000 ') . '>' . ((($rsArtikel['AST_IMQ_ID'][$i]&4)==4)?'X':'') . '</td>';

			echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . awis_format($rsArtikel["AST_VK"][$i],'Currency') . "</a></td>";

			$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AIT_ID IN (10,11,12,13,14) AND ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][$i] . "'");
			$LagerBest = (awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT")-awis_ArtikelInfo(13, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT"));
			$LagerBest += (awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT")-awis_ArtikelInfo(14, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT"));
			echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $LagerBest . "</td>";

			// Bestand Filialen
			$LagerBest = awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT");
			If($LagerBest<>0 AND $FilialBestandRecht>0)
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right><a href=./filialbestand.php?ATUNR=" . $rsArtikel["AST_ATUNR"][$i] . "&NurBestand=1&Zurueck=ArtikelListe>" . $LagerBest . "</a></td>";
			}
			else
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . ($LagerBest==''?'- -':$LagerBest) . "</td>";
			}

			// Warengruppe
			echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $rsArtikel["WUG_WGR_ID"][$i] . "</td>";

			// Fremdk�ufe (auf Wunsch)
			if($Param[15]=='on')
			{
				$SQL = "SELECT AST_ATUNR,
						(
							SELECT SUM(FRK_Menge) AS Menge FROM TeileInfos
							INNER JOIN Fremdkaeufe ON TEI_SUCH2 = FRK_OEN_SUCHNUMMER
							WHERE TEI_KEY1 = Artikelstamm.AST_KEY
								AND TEI_ITY_ID2 = 'OEN'
								AND TRUNC(MONTHS_BETWEEN(sysdate,FRK_DATUM)/12) = 0
						) AS Mon12,
						(
							SELECT SUM(FRK_Menge) AS Menge
							 FROM TeileInfos
							INNER JOIN Fremdkaeufe ON TEI_SUCH2 = FRK_OEN_SUCHNUMMER
							WHERE TEI_KEY1 = Artikelstamm.AST_KEY
							AND TEI_ITY_ID2 = 'OEN'
							AND TRUNC(MONTHS_BETWEEN(sysdate,FRK_DATUM)/12) <= 1
						) AS Mon24
						FROM Artikelstamm
						WHERE AST_ATUNR ='" . $rsArtikel['AST_ATUNR'][$i] . "'";

				$rsFK = awisOpenRecordset($con,$SQL);

				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $rsFK['MON12'][0].'</td>';
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $rsFK['MON24'][0].'</td>';
			}

			echo "</tr>";
		}
	}
	flush();
}

/**
*
* �berschrift f�r die Ergebnisliste
*
* @author Sacha Kerres
*
*******************************************/
function SchreibeUeberschrift($Param)
{
			echo "\n<tr>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Eindeutige Artikelnummer'>ATUNR</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Bezeichnung f�r den Artikel'>BEZEICHNUNG</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='In der Warenwirtschaft vorhanden'>WWS</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Im Crossing vorhanden'>Cross</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Verkaufspreis'>VK</span></td>";
			echo "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett title='Bestand in allen L�gern'>Bestand Lager</span></td>";
			echo "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett title='Bestand in den Filialen'>Bestand Filialen</span></td>";
			echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Warengruppe'>WG</span></td>";
			if($Param[15]=='on')
			{
				echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Fremdk�ufe in den letzten 12 Monaten'>FK 12</span></td>";
				echo "\n<td id=FeldBez><span class=DatenFeldNormalFett title='Fremdk�ufe in den letzten 24 Monaten'>FK 24</span></td>";
			}

			echo "</tr>\n";
}

/**
*
*	 ZeigeGeloeschteArtikel()
*
* @author Sacha Kerres
* @param $ArtNummer string
* @param $con Ressource
* @return Anzahl der gefundenen Artikel
*
**********************************************************/
function ZeigeGeloeschteArtikel($ArtNummer,$con)
{
	global $awisRSZeilen;

	$rsLOA = awisOpenRecordset($con, "SELECT * FROM Loeschartikel WHERE LOA_AST_ATUNR " . awisLIKEoderIST($ArtNummer,1) . " ORDER BY LOA_LOESCHDATUM");
	$rsLOAZeilen = $awisRSZeilen;
	if($rsLOAZeilen>0)
	{
		echo '<br><span class=HinweisText>Gel�schter Artikel:</span><br><br>';

		for($LOAZeile=0;$LOAZeile<$rsLOAZeilen;$LOAZeile++)
		{
			echo '<table border=1 width=800>';

			echo '<tr><td width=150 id=FeldBez>ATU-Nr:</td><td>' . $rsLOA['LOA_AST_ATUNR'][$LOAZeile] . '</td>';
			echo '<tr><td id=FeldBez>Bezeichnung:</td><td>' . $rsLOA['LOA_BEZEICHNUNGWW'][$LOAZeile] . '</td>';
			echo '<tr><td id=FeldBez>Angelegt am:</td><td>' . $rsLOA['LOA_MBDAT'][$LOAZeile] . '</td>';
			echo '<tr><td id=FeldBez>Gel�scht am:</td><td>' . $rsLOA['LOA_LOESCHDATUM'][$LOAZeile] . '</td>';

			echo '</table><br>';
		}
	}
	return $rsLOAZeilen;
}

?>