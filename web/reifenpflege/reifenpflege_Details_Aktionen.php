<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RST', '%');
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('RRA', '%');
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
    $TextKonserven[]=array('Liste','lst_JaNein');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15000 = $AWISBenutzer->HatDasRecht(15000);
	if(($Recht15000)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$RRPParam = unserialize($AWISBenutzer->ParameterLesen('Formular_RRP_Aktionen'));

	if(!isset($RRPParam['BLOCK']) OR $RRPParam['RRP_KEY']!=$AWIS_KEY1)
	{
		$RRPParam['BLOCK']=1;
		$RRPParam['KEY']='';
		$RRPParam['RRP_KEY']=$AWIS_KEY1;
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY RRA_GUELTIGAB ASC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL  = 'select';
	$SQL .= ' RRA_KEY,';
	$SQL .= ' RRA_AKTIONSNAME,';
	$SQL .= ' RRA_LAN_CODE,';
	$SQL .= ' RRA_GUELTIGAB,';
	$SQL .= ' RRA_GUELTIGBIS,';
	$SQL .= ' RRA_BEMERKUNG,';
	$SQL .= ' row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' from reifenpflege';
	$SQL .= ' left join v_artikelstammwgr on ast_atunr=rrp_atunr';
	$SQL .= ' left join reifenstamm on rst_key=rrp_rst_key';
	$SQL .= ' left join v_reifenaktionen on';
	$SQL .= ' (';
	$SQL .= '   (rra_wgr_id = ast_wgr_id OR rra_wgr_id IS NULL)';
	$SQL .= '   AND (rra_wug_id = ast_wug_id OR rra_wug_id IS NULL)';
	$SQL .= '   AND (rra_kenn1 = rrp_kenn1 OR rra_kenn1 IS NULL)';
	$SQL .= '   AND (rra_reh_key = rst_reh_key OR rra_reh_key IS NULL)';
	$SQL .= "   AND (rra_atunrliste like '%' || rrp_atunr || '%' OR rra_atunrliste IS NULL)";
	$SQL .= '   AND rra_aktionsname IS NOT NULL';
	$SQL .= "   AND (rrp_kenn1 <> 'E' OR rrp_kenn1 IS NULL)";
	$SQL .= '   AND (';
	$SQL .= ' 		rra_reh_key IS NOT NULL ';
	$SQL .= ' 		OR rra_wgr_id IS NOT NULL ';
	$SQL .= ' 		OR rra_wug_id IS NOT NULL ';
	$SQL .= ' 		OR rra_kenn1 IS NOT NULL ';
	$SQL .= ' 		OR rra_atunrliste IS NOT NULL';
	$SQL .= ' 	  ) ';
	$SQL .= ' )';
	$SQL .= ' WHERE RRP_KEY=0'.$AWIS_KEY1;
	
	if(isset($_GET['RRAListe']))
	{
	    $AWIS_KEY2=0;
	}
	elseif(isset($_GET['RRA_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['RRA_KEY']);
	}
	elseif($AWIS_KEY2=='')
	{
		if($RRPParam['RRP_KEY']==$AWIS_KEY1)
		{
			$AWIS_KEY2 = $RRPParam['KEY'];
		}
		else
		{
			$RRPParam['BLOCK']=1;
			$RRPParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}

	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND RRA_KEY = 0'.$AWIS_KEY2;
		$_GET['RRA_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}

	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$RRPParam['BLOCK']=$Block;
		}
		elseif(isset($RRPParam['BLOCK']))
		{
			$Block=$RRPParam['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsRRA = $DB->RecordsetOeffnen($SQL);
	$Form->DebugAusgabe(1,$SQL);
    $Form->Formular_Start();

    $Form->ZeileStart();
    $Icons=array();
    if((intval($Recht15000)&6)!=0)
    {
    	$Icons[] = array('new','../reifenaktionen/reifenaktionen_Main.php?cmdAktion=Details&RRP_KEY='.$AWIS_KEY1.'&RRA_KEY=-1');
    }
    
    $Form->Erstelle_ListeIcons($Icons,38,-1);
    
    $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=RRA_AKTIONSNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_AKTIONSNAME')) ? '~' : '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_AKTIONSNAME'], 200, '', $Link);
     
    $Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
    $Link .= '&SSort=RRA_LAN_CODE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRA_LAN_CODE'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_LAN_CODE'],100,'',$Link);
    
    $Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
    $Link .= '&SSort=RRA_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGAB'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_GUELTIGAB'],100,'',$Link);
    
    $Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
    $Link .= '&SSort=RRA_GUELTIGBIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGBIS'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_GUELTIGBIS'],100,'',$Link);
    
    $Form->ZeileEnde();
    
    // Blockweise
    $StartZeile=0;
    if(isset($_GET['Block']))
    {
    	$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
    }
    
    $RRPParam['KEY']='';
    $RRPParam['RRP_KEY']=$AWIS_KEY1;
    
    $DS=0;

	//if($rsRRA->AnzahlDatensaetze()>=1 OR isset($_GET['RRAListe']) OR !isset($_GET['RRA_KEY']))
	if($rsRRA->AnzahlDatensaetze()>=1 OR isset($_GET['RRAListe']))
	{
		while(floatval($rsRRA->FeldInhalt('RRA_KEY'))!=0)
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht15000&2)>0)	// Ändernrecht
			{
				//$Icons[] = array('edit','../reifenaktionen/reifenaktionen_Main.php?cmdAktion=Details&RRP_KEY='.$AWIS_KEY1.'&RRA_KEY='.$rsRRA->FeldInhalt('RRA_KEY'));
				$Icons[] = array('edit','../reifenaktionen/reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY='.$rsRRA->FeldInhalt('RRA_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

    		$Style = '';
    		$Link = '../reifenaktionen/reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $rsRRA->FeldInhalt('RRA_KEY') . '';
    		$Form->Erstelle_ListenFeld('RRA_AKTIONSNAME', $rsRRA->FeldInhalt('RRA_AKTIONSNAME'), 50, 200, false, ($DS % 2), $Style, $Link, 'T');
    		$Form->Erstelle_ListenFeld('RRA_LAN_CODE', $rsRRA->FeldInhalt('RRA_LAN_CODE'), 0, 100, false, ($DS % 2), $Style, '', 'T');
    		$Form->Erstelle_ListenFeld('RRA_GUELTIGAB',$rsRRA->FeldInhalt('RRA_GUELTIGAB'),10,100,false,($DS%2),$Style,'','D');
			$Form->Erstelle_ListenFeld('RRA_GUELTIGBIS',$rsRRA->FeldInhalt('RRA_GUELTIGBIS'),10,100,false,($DS%2),$Style,'','D');
    		
			if($rsRRA->FeldInhalt('RRA_BEMERKUNG')!='')
            {
                $Form->Erstelle_HinweisIcon('info', 40,($DS%2),$rsRRA->FeldInhalt('RRA_BEMERKUNG'));
            }
			$Form->ZeileEnde();

    		$rsRRA->DSWeiter();
    		$DS++;
		}

		$Link = './reifenpflege_Main.php?cmdAktion=Details&Seite=Aktionen';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsRRA->AnzahlDatensaetze()<1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht15000&6);
		$AWIS_KEY2 = $rsRRA->FeldInhalt('RRA_KEY');
		$RRPParam['KEY']=$AWIS_KEY2;
		$RRPParam['RRP_KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('RRA_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('RRP_KEY',$AWIS_KEY1);

		echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
		
		$Form->Formular_Ende();
	}

	$AWISBenutzer->ParameterSchreiben('Formular_RRP_Aktionen',serialize($RRPParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201305301639");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201305301640");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>