<?php
global $con;
global $AWISBenutzer;
global $awisRSZeilen;
global $AWISSprache;			
global $AWIS_KEY1;
global $AWIS_KEY2;
global $CursorFeld;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','NeueKontaktart');
$TextKonserven[]=array('CAK','CAK_%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('KOT','KOT_TYP');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3700 = awisBenutzerRecht($con,3700,$AWISBenutzer->BenutzerName());
if($Recht3700==0)
{
    awisEreignis(3,1000,'CRM-AP',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$SQL = 'SELECT *';
$SQL .= ' FROM CRMAnsprechpartnerKontakte';
$SQL .= ' INNER JOIN Kommunikationstypen ON CAK_KOT_KEY = KOT_KEY';
$SQL .= ' WHERE CAK_CAP_KEY = 0'.$AWIS_KEY2; 
$rsCAK = awisOpenRecordset($con,$SQL);

if(isset($_GET['CAKKEY']))
{
	$SQL .= ' AND CAK_KEY=0'.intval($_GET['CAKKEY']);
}
if(!isset($_GET['CAKSort']))
{
	$SQL .= ' ORDER BY KOT_TYP';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CAKSort']);
}
//awis_Debug(1,$SQL);
$rsCAK = awisOpenRecordset($con,$SQL);
$rsCAKZeilen = $awisRSZeilen;


if(!isset($_GET['CAKKEY']) OR isset($_GET['CAKListe']))					// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	
	if(($Recht3700&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Unterseite=Kontaktdaten&CAPKEY='.$AWIS_KEY2.'&CAKKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}
	
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
	$Link .= '&CAKSort=CAK_KOT_KEY'.((isset($_GET['CAKSort']) AND ($_GET['CAKSort']=='CAK_KOT_KEY'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAK']['CAK_KOT_KEY'],200,'',$Link);

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
	$Link .= '&CAKSort=CAK_WERT'.((isset($_GET['CAKSort']) AND ($_GET['CAKSort']=='CAK_WERT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAK']['CAK_WERT'],300,'',$Link);
	
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
	$Link .= '&CAKSort=CAK_BEMERKUNG'.((isset($_GET['CAKSort']) AND ($_GET['CAKSort']=='CAK_BEMERKUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAK']['CAK_BEMERKUNG'],300,'',$Link);
	
	awis_FORM_ZeileEnde();
	
	for($CAPZeile=0;$CAPZeile<$rsCAKZeilen;$CAPZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3700&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Unterseite=Kontaktdaten&CAPKEY='.$AWIS_KEY2.'&CAKKEY='.$rsCAK['CAK_KEY'][$CAPZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Unterseite=Kontaktdaten&CAPKEY='.$AWIS_KEY2.'&Del='.$rsCAK['CAK_KEY'][$CAPZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CAPZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('KOT_TYP',$rsCAK['KOT_TYP'][$CAPZeile],20,200,false,($CAPZeile%2),'','','T');
		$Link = '';
		if(strpos($rsCAK['CAK_WERT'][$CAPZeile],'@')!==false)
		{
			$Link = 'mailto:'.$rsCAK['CAK_WERT'][$CAPZeile];
		}
		awis_FORM_Erstelle_ListenFeld('CAK_WERT',$rsCAK['CAK_WERT'][$CAPZeile],20,300,false,($CAPZeile%2),'',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('CAK_BEMERKUNG',$rsCAK['CAK_BEMERKUNG'][$CAPZeile],20,300,false,($CAPZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();
	
	echo '<input name=txtCAK_KEY type=hidden value=0'.(isset($rsCAK['CAK_KEY'][0])?$rsCAK['CAK_KEY'][0]:'').'>';
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Unterseite=Kontaktdaten&CAPKEY=".$AWIS_KEY2."&CAKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCAK['CAK_KEY'][0])?$rsCAK['CAK_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCAK['CAK_KEY'][0])?$rsCAK['CAK_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAK']['CAK_KOT_KEY'],150);
	$SQL = 'SELECT KOT_KEY, KOT_TYP FROM KommunikationsTypen WHERE BITAND(KOT_VERWENDUNG,1)=1 ORDER BY KOT_TYP';
	awis_FORM_Erstelle_SelectFeld('CAK_KOT_KEY',(isset($rsCAK['CAK_KOT_KEY'][0])?$rsCAK['CAK_KOT_KEY'][0]:''),200,($Recht3700&6),$con,$SQL);
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAK']['CAK_WERT'],150);
	awis_FORM_Erstelle_TextFeld('CAK_WERT',(isset($rsCAK['CAK_WERT'][0])?$rsCAK['CAK_WERT'][0]:''),60,200,($Recht3700&6));
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAK']['CAK_BEMERKUNG'],150);
	awis_FORM_Erstelle_TextFeld('CAK_BEMERKUNG',(isset($rsCAK['CAK_BEMERKUNG'][0])?$rsCAK['CAK_BEMERKUNG'][0]:''),60,200,($Recht3700&6));
	awis_FORM_ZeileEnde();
	
	awis_FORM_FormularEnde();
}
?>