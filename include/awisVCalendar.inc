<?php

class awisVCalendar {

    private $calendar = "";
    private $EmpfaengerMail = "";
    private $OptionaleEmpfaenger = "";
    private $AbsenderMail = "";
    private $Terminvon = "";
    private $Terminbis = "";
    private $Titel = "";
    private $Text = "";
    private $Texttyp = 1;
    private $Ort = "";

    /**
     * Erstellt VCalendarObjekt, gibt dieses jedoch nicht zur�ck -> gebeVCalendar-Funktion
     * Pro Zeile max. 75 Zeichen (inkludiert Line Breaks!)
     *
     * Validator zum Testen: https://icalendar.org/validator.html
     *
     * @param string $AbsenderMail
     * @param array|string $EmpfaengerMail Ein Empf�nger -> String; Ein/Mehrere Empf�nger -> Array
     * @param array|string $OptionaleEmpfaenger Ein Empf�nger -> String; Ein/Mehrere Empf�nger -> Array
     * @param string $Terminvon
     * @param string $Terminbis
     * @param string $Titel Termintitel
     * @param string $Text Nachrichtentext
     * @param int $Texttyp Nachrichtentexttyp: 1= String, 2=HTML
     * @param string $Ort Optionaler Ort als Text
     */
    public function erstelleVCalendarObjekt($AbsenderMail, $EmpfaengerMail,$OptionaleEmpfaenger="", $Terminvon, $Terminbis, $Titel, $Text,$Texttyp=1, $Ort="") {

        $this->AbsenderMail = $AbsenderMail;
        $this->EmpfaengerMail = $EmpfaengerMail;
        $this->OptionaleEmpfaenger = $OptionaleEmpfaenger;
        $this->Terminvon = $Terminvon;
        $this->Terminbis = $Terminbis;
        $this->Titel = $Titel;
        $this->Text = $Text;
        $this->Texttyp = $Texttyp;
        $this->Ort = $Ort;

        $lb = "\r\n"; //Line Break

        $vcal = "";
        $vcal .= "BEGIN:VCALENDAR".$lb;
        $vcal .= "VERSION:2.0".$lb;
        $vcal .= "PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN".$lb;
        $vcal .= "CALSCALE:GREGORIAN".$lb;
        $vcal .= "METHOD:PUBLISH".$lb;

        $vcal .= $this->erstelleVEVENT($Terminvon,$Terminbis);

        $vcal .= "END:VCALENDAR".$lb;

        $this->calendar = $vcal;

    }

    private function erstelleVEVENT($Tagvon,$Tagbis) {

        $lb = "\r\n"; //Line Break

        $vcalEVENT = "";
        $vcalEVENT .= "BEGIN:VEVENT".$lb;

        $vcalEVENT .= $this->ladeTeilnehmerliste();

        $vcalEVENT .= "UID:".date('Ymd').'T'.date('His')."-".rand()."".$lb; //Unique ID erstellen
        $vcalEVENT .= "DTSTAMP:".date('Ymd').'T'.date('His')."".$lb; //Erstellungs-Timestamp
        $vcalEVENT .= "DTSTART:".date('Ymd',strtotime($Tagvon)).'T'.date('His',strtotime($Tagvon)).$lb;
        $vcalEVENT .= "DTEND:".date('Ymd',strtotime($Tagbis)).'T'.date('His',strtotime($Tagbis)).$lb;
        if($this->Ort != "") {
            $vcalEVENT .= "LOCATION:" . $this->ical_split("LOCATION:", $this->Ort) . $lb;
        }
        $vcalEVENT .= "SUMMARY:".$this->ical_split("SUMMARY:",$this->Titel).$lb;
        if($this->Texttyp==2){
            $vcalEVENT .= "X-ALT-DESC;FMTTYPE=text/html:" . $this->ical_split("X-ALT-DESC;FMTTYPE=text/html:", $this->Text,true) . $lb;
        } else {
            $vcalEVENT .= "DESCRIPTION:" . $this->ical_split("DESCRIPTION:", $this->Text) . $lb;
        }
        $vcalEVENT .= "END:VEVENT".$lb;

        return $vcalEVENT;

    }

    /**
     * Nimmt die Empf�nger-/Absendermail-Adresse und generiert darau� die ATTENDEE/ORGANIZER-Liste f�r vCalendar
     *
     * @return string
     */
    private function ladeTeilnehmerliste() {

        $lb = "\r\n"; //Line Break

        $vcalTeilnehmer = "";
        $vcalTeilnehmer .= "ORGANIZER;".$this->ical_split("ORGANIZER;","mailto:".$this->AbsenderMail).$lb;
        if (is_array($this->EmpfaengerMail)) {
            foreach($this->EmpfaengerMail as $Empfaenger) {
                $vcalTeilnehmer .= "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;".$this->ical_split("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;","mailto:".$Empfaenger).$lb;
            }
        } elseif(is_string($this->EmpfaengerMail)){
            $vcalTeilnehmer .= "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;".$this->ical_split("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;","mailto:".$this->EmpfaengerMail).$lb;
        }
        if($this->OptionaleEmpfaenger != "") {
            if (is_array($this->OptionaleEmpfaenger) and count($this->OptionaleEmpfaenger)>=1) {
                foreach ($this->OptionaleEmpfaenger as $OptEmpfaenger) {
                    $vcalTeilnehmer .= "ATTENDEE;ROLE=OPT-PARTICIPANT;RSVP=TRUE;" . $this->ical_split("ATTENDEE;ROLE=OPT-PARTICIPANT;RSVP=TRUE;", "mailto:" . $OptEmpfaenger) . $lb;
                }
            } elseif (is_string($this->OptionaleEmpfaenger)) {
                $vcalTeilnehmer .= "ATTENDEE;ROLE=OPT-PARTICIPANT;RSVP=TRUE;" . $this->ical_split("ATTENDEE;ROLE=OPT-PARTICIPANT;RSVP=TRUE;", "mailto:" . $this->OptionaleEmpfaenger) . $lb;
            }
        }

        return $vcalTeilnehmer;
    }

    /**
     * Maximal 75 Zeichen pro Zeile -> Funktion die ich gefunden hab die das abschneidet
     *
     * @param $preamble
     * @param $value
     * @param boolean $istHTML optional: Bei HTML-Haupttext auf true damit Tags nicht verschwinden
     * @return string
     */
    private function ical_split($preamble, $value, $istHTML=false) {
        $value = $this->ersetzeUmlaute($value);
        if(!$istHTML) {
            $value = strip_tags($value);
        }
        $value = preg_replace('/\n+/', ' ', $value);
        $value = preg_replace('/\s{2,}/', ' ', $value);

        $preamble_len = strlen($preamble);

        $lines = array();
        while (strlen($value)>(75-$preamble_len)) {
            $space = (75-$preamble_len);
            $mbcc = $space;
            while ($mbcc) {
                $line = mb_substr($value, 0, $mbcc);
                $oct = strlen($line);
                if ($oct > $space) {
                    $mbcc -= $oct-$space;
                }
                else {
                    $lines[] = $line;
                    $preamble_len = 1; // Still take the tab into account
                    $value = mb_substr($value, $mbcc);
                    break;
                }
            }
        }
        if (!empty($value)) {
            $lines[] = $value;
        }

        return join($lines, "\n\t");
    }

    public function gebeVCalendarString() {
        return $this->calendar;
    }

    public function leereVCalendar() {
        $this->calendar ="";
        return true;
    }

    /**
     * Downloaded die aktuelle Kalender-Datei
     *
     * @param string $name Dateiname mit ".ics"-Endung
     */
    public function downloadVCalendar($name='') {
        $name=trim($name);
        if($name==''){
            $name='calendar.ics';
        } elseif(strtolower(substr($name,-4)) !='.ics' ) {
            $name= $name.".ics";
        }
        Header('Content-Length: '.strlen($this->calendar));
        Header('Content-disposition: attachment; filename='.$name);
        header('Content-Type: application/x-download');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        if(headers_sent()) {
            die('<B>Download error: </B> Some data has already been output to browser, can\'t send ICS file');
        }
        echo $this->calendar;
        sleep(2); //Sleep sonst kann es Probleme beim Herunterladen geben
    }

    /**
     * Umlaute werden nicht unterst�tzt -> ersetzen
     *
     * @param $string
     * @return string
     */
    private function ersetzeUmlaute($string)
    {
        $search = array("�", "�", "�", "�", "�", "�", "�", "�");
        $replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "");
        return str_replace($search, $replace, $string);
    }

}

?>