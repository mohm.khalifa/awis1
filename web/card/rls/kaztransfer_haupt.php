<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
	echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>
</script>
<?php


if(isset($strBuchen)&&$strBuchen!=''&&isset($strBa)&&$strBa!='')
{
	$arrKazId = explode(";",$strBuchen);
	$arrBa = explode(";",$strBa);
	
	if(count($arrKazId)==count($arrBa))
	{
		for($i=0;$i<count($arrKazId);$i++)
		{
			$intId = (integer)$arrKazId[$i];
			
			
				
			if($intId>0)
			{
				$intTransfer = 1;
			}
			else		//Wenn intId negativ �bergeben worden ist, so soll der Datensatz mit dieser Id nicht �bernommen werden --> intTransfer = 2
			{
				$intId *= (-1);
				$intTransfer = 2;
			}
			
			$intBa = (integer)$arrBa[$i];		//entspricht Buchungsart
			
			$strSql = "update kontoauszuege 
			set IKA_TRANSFER_IN_KONTOAUSZUEGE=".$intTransfer.",IKA_BUCHUNGSART=".$intBa.",IKA_AEND_BENUTZER='".$PHP_AUTH_USER."',
			IKA_AEND_DATUM=sysdate
			 where IKA_ID=".$intId;
			 			 
			abfrage("ora",$strSql);
		}
	}
}

//Daten f�r Listenfeld Buchungsarten

$strSql = "select BA_BUCHUNGSARTID, BA_BUCHUNGSART from BUCHUNGSARTEN where BA_KAZTRANSFER=1";
$arrBuchungsarten=abfrage("ora",$strSql);

?>

<html>
<head>
<title>&Uuml;bersicht Transfer Kontoausz&uuml;ge</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>
<?php


$strVariante='';
$strFilter='';
$strSqlFilter='';

if(isset($strDatum))
{
	$strSqlFilter=" where ika_buchungsdatum='".$strDatum."'";
	
	$strFilter="Filter: ".$strDatum;
}

if(isset($strBetrag))
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where ika_betrag=".str_replace(',','.',$strBetrag)."";
	}
	else
	{
		$strSqlFilter.=" and ika_betrag=".str_replace(',','.',$strBetrag)."";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: ".eurobetrag($strBetrag);
	}
	else
	{
		$strFilter.="/".eurobetrag($strBetrag);
	}	
}

if(isset($strBank)&&$strBank!='alle')
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where BAN_KUERZEL='".$strBank."'";
	}
	else
	{
		$strSqlFilter.=" and BAN_KUERZEL='".$strBank."'";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: ".$strBank;
	}
	else
	{
		$strFilter.="/".$strBank;
	}	
}

if(isset($strAuftraggeber)&&$strAuftraggeber!='')
{
	if($strSqlFilter=='')
	{
		$strSqlFilter=" where (upper(IKA_AUFTRAGGEBER1) like '%".strtoupper($strAuftraggeber)."%' or upper(IKA_AUFTRAGGEBER2) like '%".strtoupper($strAuftraggeber)."%')";
	}
	else
	{
		$strSqlFilter.=" and (upper(IKA_AUFTRAGGEBER1) like '%".strtoupper($strAuftraggeber)."%' or upper(IKA_AUFTRAGGEBER2) like '%".strtoupper($strAuftraggeber)."%')";
	}
	if($strFilter=='')
	{
		$strFilter="Filter: Suchmuster \"".$strAuftraggeber."\"";
	}
	else
	{
		$strFilter.="/Suchmuster \"".$strAuftraggeber."\"";
	}	
}

$strFilter=trim($strFilter);

if($strFilter!='')
	$strFilter='('.$strFilter.')';


$strTitel='';
$strUrl='';

if(isset($strIdChecked))
	$strUrl.='&strIdChecked='.$strIdChecked;
	
//strIdChecked	

	
if(isset($strIdChecked)&&$strIdChecked=='impo3')
{
	$intIka_Transfer=2;
	$strTitel.=" nicht &uuml;bernommen,";
}
else
{
	$strTitel.=" noch nicht &uuml;bernommen,";
	$intIka_Transfer=0;
}

$strTitel=trim($strTitel);

if($strTitel!='')
{
	$strTitel=" (".substr($strTitel,0,strlen($strTitel)-1).")";
}

$strFromWhere=" from kontoauszuege,bank where ika_transfer_in_kontoauszuege=".$intIka_Transfer." and ika_loesch=0" .$strIka_Bank.$strIka_KazDatum;

$intScrollanzahl = 3;

$strDb = "ora";

$strSql = "select * from
(select
IKA_ID, IKA_AUSZUGNR, IKA_KONTONR, IKA_BANK, IKA_AUSZUGSDATUM, IKA_BUCHUNGSDATUM, IKA_VALUTADATUM, IKA_BETRAG, IKA_WAEHRUNG, IKA_TEXTSCHLUESSEL,
IKA_BUCHUNGSTEXT, IKA_VERWENDUNGSZWECK1, IKA_VERWENDUNGSZWECK2, IKA_VERWENDUNGSZWECK3, IKA_KUNDENREFERENZ, IKA_STARTSALDO, IKA_SCHLUSSALDO, IKA_AUFTRAGGEBER1,
IKA_AUFTRAGGEBER2, IKA_AUFTRAGGEBER_BLZ, IKA_AUFTRAGGEBER_KONTO, IKA_TRANSFER_IN_KONTOAUSZUEGE, IKA_BUCHUNGSART, IKA_RLSBETRAG, IKA_AEND_BENUTZER,
IKA_AEND_DATUM, IKA_LOESCH,nvl(ban_kuerzel,'') ban_kuerzel from kontoauszuege,bank where ika_transfer_in_kontoauszuege=".$intIka_Transfer." and ika_loesch=0
and ika_bank=ban_blz(+) )".$strSqlFilter." order by ika_id";

include("card/card_blaettern_haupt.inc.php");	//Bl�ttern

$arrKazTransfer = abfrage("ora",$strSqlScroll);
			
$arrKazTransferDs = $arrKazTransfer["trefferarray"];

//Ermittelt die Anzahl noch nicht �bernommenen Datens�tze in kontoauszuege
$strSql="select count(*) anzahl from kontoauszuege where ika_transfer_in_kontoauszuege=0 and ika_loesch=0";
$arrAnzNNU = abfrage("ora",$strSql);
$intAnzNNU = $arrAnzNNU["trefferarray"]["ANZAHL"][0];

if($arrKazTransfer["zeilen"]==0)
{
	?>
	<script language="javascript">
		var obj=<?php echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_haupt.document;
		
		obj.getElementById("nntext").firstChild.nodeValue="<?php echo $intAnzNNU;?>";
	</script>
	<?php

	if($arrKazTransfer["zeilen"]==$intAnzNNU)
	{
	?>
	<script language="javascript">
		//Zeile noch nicht �bernommen im Hauptfenster ausblenden + Checkbox anhaken
		/*var obj=<?php echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_haupt.document;
		
		obj.getElementById("nnzeile").style.display="none";
		obj.getElementById("nnzeileleer").style.display="none";
		obj.getElementById("impo2").checked=false;
		obj.getElementById("impo1").checked=true;*/
	</script>
	</head>
	<body onload="<?php echo str_repeat("top.",$intDialog);?>window.returnValue='1';parent.self.close();">
	<?php
	}
	else	//Wenn keine Datens�tze in kontoauszuege vorhanden sind, welche ika_transfer_in_kontoauszuege=1 oder 2 haben, wird 2 an die aufrufende Seite zur�ckgegeben
	{
		if(isset($strBuchen)&&$strBuchen!=''&&isset($strBa)&&$strBa!='')
		{
		?>
		</head>
		<body onload="<?php echo str_repeat("top.",$intDialog);?>window.returnValue='3';parent.self.close();">
		
		
		<?php
		}
		else
		{
		?>
			</head>
			<body onload="<?php echo str_repeat("top.",$intDialog);?>window.returnValue='2';parent.self.close();">
		<?php
		}
	}
}
else
{


?>
	<script language="javascript">
	
	<?php if($intIka_Transfer==0)
	{
		$strSql="select count(*) anzahl from kontoauszuege where ika_transfer_in_kontoauszuege=0 and ika_loesch=0";

		$arrOffen = abfrage("ora",$strSql);
		$intAnzOffen = $arrOffen["trefferarray"]["ANZAHL"][0];
	?>
		var obj=<?php echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_haupt.document;
		
			
		/*obj.getElementById("nnzeile").style.display="inline";
		obj.getElementById("nnzeileleer").style.display="inline";
		
		if(obj.getElementById("impo1").checked)
		{
			obj.getElementById("impo2").checked=false;
			obj.getElementById("impo3").checked=false;	
		}*/
		
		obj.getElementById("nntext").firstChild.nodeValue="<?php echo $intAnzOffen;?>";
	<?php 
	}
	?>
	
	function init()
	{
		//Speicherbutton in Navigationsleiste unsichtbar machen
		
		try{ parent.kaztransfer_navigation.document.getElementById('speibut').style.visibility='hidden'; } catch(e){}	
		
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
				
		document.ondblclick = zoom;		//Zoomfenster
	}
	
	//Wird bei Click auf Checkbox aufgerufen und blendet Speicherbutton in Navigationsleiste ein oder aus
	
	function checkboxklick()
	{
		// Anderen Haken wenn n�tig ausschalten
		var objPChk = window.event.srcElement;
		var objid = parseInt(objPChk.id,10);
		
		var objSChk = document.getElementById(String(objid*-1));
		
		if(objPChk.checked&&objSChk.checked)
		{
			objSChk.checked = false;
		}
		
		//Speicherbutton ein-/ausblenden
		var obj = document.getElementsByName("cb");
		
		var boolHaken = false;
		
		for(i=0;i<obj.length&&!boolHaken;i++)
		{
			if(obj[i].checked)
				boolHaken = true;
		}
		if(boolHaken)
			parent.kaztransfer_navigation.document.getElementById('speibut').style.visibility='visible';
		else
			parent.kaztransfer_navigation.document.getElementById('speibut').style.visibility='hidden';
	}
	
	//Wird durch Klick auf Button Speichern in Navigationsleiste aufgerufen
	
	function buchen()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Datensatz �bersicht Transfer Kontoausz�ge buchen");	//Zeitmessung
		<?php } ?>
		var strIds = '';
		var strBa = '';
		
		var objChb = document.getElementsByName("cb");
		
		for(i=0;i<objChb.length;i++)
		{
			if(objChb[i].checked)
			{
				strIds += objChb[i].id + ';';
				intBa =	listeneintrag(document.getElementById("ba"+String(Math.abs(parseInt(objChb[i].id,10)))),"value");
				strBa +=  intBa + ';';
			}
		}
	
		strIds = strIds.substr(0,strIds.length-1);
		strBa = strBa.substr(0,strBa.length-1);
		
		location.href = "<?php echo $PHP_SELF."?intDialog=".$intDialog.$strUrl; ?>&strBuchen="+strIds+"&strBa="+strBa;
	}
	
	function filtern()
	{
		var parameter = "intDialog=1&intTransfer=<?php echo $intIka_Transfer;?>";
		
		<?php echo dialog("kaztransfer_filter.php"); ?>
	
		if(rueckgabe!='undefined')
		{
			var arrRueck = rueckgabe.split(";");
						
			strFilter='&strAuswahl='+arrRueck[1];
			
			if(arrRueck[0]!='')
			{
				strFilter+='&strDatum='+arrRueck[0];
			}
			
			if(arrRueck[2]!='')
			{
				strFilter+='&strBetrag='+arrRueck[2];
			}
			
			if(arrRueck[3]!='')
			{
				strFilter+='&strAuftraggeber='+arrRueck[3];
			}
			
			if(arrRueck[4]!='')
			{
				strFilter+='&strBank='+arrRueck[4];
			}
			
			strQuery = querystringlesen();
					
			intPos = strQuery.indexOf("&strAuswahl");
			
			if(intPos!=-1)
			{
				strLoesch = strQuery.slice(intPos);
				querystringverkuerzen(strLoesch);
			}
					
			querystringerweitern(strFilter);
			
			location.href='<?php echo $PHP_SELF; ?>?'+querystringlesen();
		}
	}

	
	
	
	
	</script>
	</head>
	<body onload="init();" style="overflow:hidden;">
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="top">
		<table cellspacing="0" cellpadding="0" border="0">
		<tr><td colspan="5" class="zwzeile">&nbsp;</td></tr>
		<tr><td colspan="5" align="center">
		Datens&auml;tze: <span class="titel"><?php echo $strPosition; ?></span><?php echo $strTitel;?><br><span class="titel"><?php echo $strFilter; ?></span></td></tr>
		<!--<tr><td colspan="5" class="zwzeile">&nbsp;</td></tr>-->
		</table>
	<table cellspacing="0" cellpadding="0">
			<tr><td colspan="6" class="zwzeile"><hr></td></tr>
			<tr><td class="titel">Valuta:</td><td class="titel">Datum:</td><td class="titel">Auftraggeber/Bankverbindung:</td>
			<td class="titel">Verwendungszweck:</td>
			<td class="titel">Betrag:</td>
			<td class="titel">Buchen:</td>
			</tr>
			<tr><td colspan="6" class="zwzeile"><hr></td></tr>
			
				<?php
				for($i=0;$i<$arrKazTransfer["zeilen"];$i++)
				{
					echo "<tr>";
					echo "<td class=\"text\"><input type=\"text\" class=\"val\" value=\"".
					$arrKazTransferDs["IKA_VALUTADATUM"][$i]."\" readonly></td>";
					echo "<td class=\"text\"><input type=\"text\" class=\"dat\" value=\"".
					$arrKazTransferDs["IKA_BUCHUNGSDATUM"][$i]."\" readonly></td>";
					echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"ag\" readonly>".
					$arrKazTransferDs["IKA_AUFTRAGGEBER1"][$i]."\n".
					$arrKazTransferDs["IKA_AUFTRAGGEBER2"][$i]."</textarea></td>";
					echo "<td class=\"text\"><input type=\"text\" class=\"but\" value=\"".
					$arrKazTransferDs["IKA_BUCHUNGSTEXT"][$i]."\" readonly></td>";
					echo "<td class=\"text\"><input type=\"text\" class=\"betr\" value=\"".
					eurobetrag($arrKazTransferDs["IKA_BETRAG"][$i])."\" readonly></td>";
					echo "<td rowspan=\"3\" align=\"center\" class=\"buch\">";
					echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
					echo "<td>";
					echo "<input type=\"checkbox\" id=\"".$arrKazTransferDs["IKA_ID"][$i]."\" name=\"cb\" onclick=\"checkboxklick();\"></td>";
					echo "<td>Ja</td></tr>";
					echo "<tr><td>";
					echo "<input type=\"checkbox\" id=\"-".$arrKazTransferDs["IKA_ID"][$i]."\" name=\"cb\" onclick=\"checkboxklick();\"></td>";
					echo "<td>Nein</td>";
					echo "</tr></table>"; 
					echo "</td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td colspan=\"2\" class=\"text\" rowspan=\"3\" align=\"center\" style=\"vertical-align:bottom;\">";
					echo "<span class=\"titel\">Bank/KAZ:</span><br>";
					echo "<input type=\"text\" class=\"bank\" value=\"".$arrKazTransferDs["BAN_KUERZEL"][$i]."\" readonly>";
					echo "<input type=\"text\" class=\"kaz\" value=\"".
					$arrKazTransferDs["IKA_AUSZUGNR"][$i]."/".$arrKazTransferDs["IKA_AUSZUGSDATUM"][$i]."\" readonly>";
					echo "</td>";
					echo "<td class=\"text\" rowspan=\"3\"><textarea class=\"verw\" readonly>".
					$arrKazTransferDs["IKA_VERWENDUNGSZWECK1"][$i]."\n".
					$arrKazTransferDs["IKA_VERWENDUNGSZWECK2"][$i]."\n".
					$arrKazTransferDs["IKA_VERWENDUNGSZWECK3"][$i]."</textarea></td>";
					echo "<td class=\"zeile2\">&nbsp;</td></tr>";
					echo "<tr>";
					echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"agb\" readonly>".
					$arrKazTransferDs["IKA_AUFTRAGGEBER_KONTO"][$i]."\n".
					$arrKazTransferDs["IKA_AUFTRAGGEBER_BLZ"][$i]."</textarea></td>";
					echo "<td class=\"zeile3\">&nbsp;</td></tr>";
					echo "<tr>";
					echo "<td class=\"zeile4\" colspan=\"2\">";
					echo element(array(
					"type"=>"select",
					"class"=>"ba",
					"id"=>"ba".$arrKazTransferDs["IKA_ID"][$i],
					"optionsarray"=>$arrBuchungsarten["trefferarray"],
					"optionstextfeld"=>"BA_BUCHUNGSART",
					"optionswertfeld"=>"BA_BUCHUNGSARTID",
					"optionsvorauswahlwert"=>3,
					"optionsvorauswahlfeld"=>"BA_BUCHUNGSARTID",
					"keineleereoption"=>1
					));
					
					echo "</td></tr>";
					
					echo "<tr><td colspan=\"6\" class=\"zwzeile\"><hr></td></tr>";
					
				}
				
			?>
			</table>
	</td></tr></table>
<?php
}	//Ende else
?>
</body>
</html>
<?php 
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>