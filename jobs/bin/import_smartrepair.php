<?php
require_once('/daten/include/awisSmartRepair.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
        //$Job = new awisJobProtokoll(18, 'Starte Werkstattauftraege - Import.');
        //$Job->SchreibeLog(5,1,'Suche offene Bestellungen...');

        $X = new awisSmartRepair('awis_jobs');
        $X->DebugLevel(999);
        $X->ImportiereDaten();
        //$Job->SchreibeLog(9,1,'Ende des Jobs.');
        die(0);
}
catch (awisException $ex)
{
        //$Job->SchreibeLog(9,4,'Fehler bei der Ausführung:'.$ex->getSQL(). ' / '.$ex->getMessage());
        echo 'Importfehler in Zeile '.$ex->getLine().'/'.$ex->getFile().': '.$ex->getMessage().'SQL:'.$ex->getSQL().PHP_EOL;
        file_put_contents(awisSmartRepair::PROTOKOLLDATEI,date('c').';'.'FEHLER;Importfehler in Zeile '.$ex->getLine().'/'.$ex->getFile().': '.$ex->getMessage().PHP_EOL,FILE_APPEND);
}catch (Exception $ex)
{
        //$Job->SchreibeLog(9,4,'Fehler bei der Ausführung:'.$ex->getMessage());
        echo 'Importfehler in Zeile '.$ex->getLine().'/'.$ex->getFile().': '.$ex->getMessage().PHP_EOL;
        file_put_contents(awisSmartRepair::PROTOKOLLDATEI,date('c').';'.'FEHLER;Importfehler in Zeile '.$ex->getLine().'/'.$ex->getFile().': '.$ex->getMessage().PHP_EOL,FILE_APPEND);
}

?>

