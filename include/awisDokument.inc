<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class awisDokument
{
	/**
	 * Datenbankverbindung
	 * @var awisDatenbank
	 */
	private $_DB = null;	
	/**
	 * Benutzer
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	/**
	 * Debuglevel f�r Ausgaben
	 * @var int
	 */
	private $_DebugLevel = 0;
	/**
	 * Standardpfad zu den Dokumenten
	 * @var string
	 */
	private $_DokumentenPfad = '';
	/**
	 * Teiler, der f�r die Ermittlung der Dokumenten-Verzeichnis Nr 
	 * @var int
	 */
	const TEILER = 500;
	/**
	 * Initialisierung der Klasse
	 * @param awisDatenbank $DB
	 * @param awisBenutzer $AWISBenutzer
	 */
	public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
	{
		$this->_DB = $DB;
		$this->_AWISBenutzer = $AWISBenutzer;
		
		$this->_DokumentenPfad = $Pfad = $this->_AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis');
	}
	/**
	 * Liest oder setzt den Debuglevel im Objekt
	 * @param int $NeuerLevel
	 * @return number
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = (int)$NeuerLevel;
		}
		
		return $this->_DebugLevel;
	}
	
	/**
	 * Liefert den ARRAY zu einem Dokument oder FALSE, wenn Sie nicht existiert
	 *
	 * @param int $DOC_KEY
	 * @param string $DOCTYP
	 * @return array
	 */
	function DokumentPfad($DOC_KEY, $DOCTYP)
	{
		global $AWISBenutzer;
		$Erg = Array();
	
		$SQL = 'SELECT *';
		$SQL .= ' FROM Dokumente';
		$SQL .= ' INNER JOIN Dokumentzuordnungen ON DOC_KEY = DOZ_DOC_KEY';
		$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC', 'N0',$DOC_KEY);
		$SQL .= ' AND DOZ_XXX_KUERZEL='.$this->_DB->WertSetzen('DOC', 'T',$DOCTYP);
	
		$rsDOC = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DOC'));
		if(!$rsDOC->EOF())
		{
			//$Pfad = awis_BenutzerParameter($this->_con,'Dokumenten-Stammverzeichnis',$this->_AWISBenutzer->BenutzerName());
			$Pfad = $this->_DokumentenPfad;
			$Pfad .= '/'.(intval($rsDOC->FeldInhalt('DOC_KEY')/self::TEILER));
			if(!is_dir($Pfad))
			{
			    echo 'Kein Dokument '.$Pfad.' gefunden';
			     
			    return false;
			}
			else
			{
				$Pfad .= '/'.$DOC_KEY.'.'.strtolower($rsDOC->FeldInhalt('DOC_ERWEITERUNG'));
				if(!is_file($Pfad))
				{
				    echo 'Kein Dokument '.$Pfad.' gefunden';
					return false;
				}
	
				$Erg['Pfad']=$Pfad;
				$Erg['Erweiterung']=$rsDOC->FeldInhalt('DOC_ERWEITERUNG');
				$Erg['Eigentuemer']=$rsDOC->FeldInhalt('DOC_XBN_KEY');
				$Erg['Bezeichnung']=$rsDOC->FeldInhalt('DOC_BEZEICHNUNG');
			}
			return $Erg;
		}
		else
		{
            echo 'Kein Dokument mit dem Key '.$DOC_KEY.' gefunden';
		    return false;
		}
	}
	
	/**
	 * L�scht ein Dokument und alle Bez�ge
	 *
	 * @param int $ID
	 */
	function DokumentLoeschen($DOC_KEY)
	{
		try 
		{
			$SQL = 'SELECT *';
			$SQL .= ' FROM Dokumente ';
			$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC', 'N0',$DOC_KEY);
			$rsDOC = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DOC'));
			if($rsDOC->FeldInhalt('DOC_KEY')!='')
			{
				$SQL = 'DELETE ';
				$SQL .= ' FROM Dokumente';
				$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC','N0',$DOC_KEY);
				$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->BindeVariablen('DOC'));
	
				$Pfad = $this->_DokumentenPfad;
				$Pfad .= '/'.(intval($rsDOC->FeldInhalt('DOC_KEY')/self::TEILER));
				$Pfad .= '/'.$rsDOC->FeldInhalt('DOC_KEY').'.'.strtolower($rsDOC->FeldInhalt('DOC_ERWEITERUNG'));
				if(is_file($Pfad))
				{
					unlink($Pfad);
				}
				
				return true;
			}
			else 
			{
			    echo 'Kein Dokument mit dem Key '.$DOC_KEY.' gefunden';
			}
		}
		catch (Exception $ex)
		{
			// Fehler beim L�schen?
			var_dump($ex);
		}			
		return false;
	}


    /**
     * Schreibt f�r ein bestehendes Dokument eine neue Zuordnung.
     *
     * @param $DOC_KEY des bestehenden Dokuments
     * @param $XTN_KUERZEL der neuen Zuordnung
     * @param $XXX_KEY der neuen Zuordnung
     */
    public function DokumentZuordnen($DOC_KEY, $XTN_KUERZEL, $XXX_KEY){


        $SQL  ='merge into DOKUMENTZUORDNUNGEN DEST using';
        $SQL .=' (select ';
        $SQL .= $this->_DB->WertSetzen('DZU', 'N0', $DOC_KEY) . ' as DOC_KEY';
        $SQL .=' from dual';
        $SQL .=' ) SRC on (dest.DOZ_DOC_KEY = src.DOC_KEY and dest.DOZ_XXX_KUERZEL = '.$this->_DB->WertSetzen('DZU', 'T', $XTN_KUERZEL);
        $SQL .= ' and dest.DOZ_XXX_KEY = ' . $this->_DB->WertSetzen('DZU','Z',$XXX_KEY);
        $SQL .= ')';
        $SQL .=' when not matched then';
        $SQL .='   insert';
        $SQL .='     (';
        $SQL .='       DOZ_DOC_KEY,';
        $SQL .='       DOZ_XXX_KUERZEL,';
        $SQL .='       DOZ_XXX_KEY';
        $SQL .='     )';
        $SQL .='     values';
        $SQL .='     (';
        $SQL .='       SRC.DOC_KEY,';
        $SQL .='      ' . $this->_DB->WertSetzen('DZU','T',$XTN_KUERZEL);
        $SQL .='      , ' .$this->_DB->WertSetzen('DZU','Z',$XXX_KEY);
        $SQL .='     )';

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('DZU',true));

    }


    /**
	 * Legt ein Dokument an und liefert den Pfad+ID in einem Array zur�ck
	 *
	 * @param string $DokNummer
	 * @param int $DokTyp
	 * @param string $DokDatum
	 * @param string $DokBemerkung
	 * @param string $Dateierweiterung
	 * @param array $Zuordnungen
	 */
	function ErzeugeDokumentenPfad($DokNummer, $DokTyp, $DokDatum, $DokBemerkung, $Dateierweiterung, array $Zuordnungen)
	{
		try
		{
			$Erg = array('doc_key'=>0,'pfad'=>'','fehler'=>0);
			$Pfad = '';
			$Key = 0;

			$this->_DB->TransaktionBegin();

			$SQL = 'INSERT INTO dokumente';
			//$SQL .= '(DOC_BEZEICHNUNG, doc_bemerkung, doc_datum, doc_dty_key,doc_erweiterung';
			$SQL .= '(DOC_BEZEICHNUNG, doc_bemerkung, doc_xbn_key ,doc_datum,doc_erweiterung';
			$SQL .= ',doc_user, doc_userdat)';
			$SQL .= 'VALUES(';
			$SQL .= ''.$this->_DB->WertSetzen('DOC', 'T', $DokNummer,true);
			$SQL .= ','.$this->_DB->WertSetzen('DOC', 'T', $DokBemerkung,true);
			$SQL .= ','.$this->_DB->WertSetzen('DOC', 'N0', $this->_AWISBenutzer->BenutzerID(),true);
			$SQL .= ','.$this->_DB->WertSetzen('DOC', 'D', $DokDatum,true);
			$SQL .= ','.$this->_DB->WertSetzen('DOC', 'T', $Dateierweiterung,true);
			$SQL .= ','.$this->_DB->WertSetzen('DOC', 'T', $this->_AWISBenutzer->BenutzerName());
			$SQL .= ',sysdate';
			$SQL .= ')';
			$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('DOC'));

			$SQL = 'SELECT seq_DOC_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $this->_DB->RecordSetOeffnen($SQL);
			$DOC_KEY=$rsKey->Feldinhalt('KEY');

			//********************************
			// Zuordnungen
			//********************************

            if ($DOC_KEY != 0) {
                foreach ($Zuordnungen AS $Zuordnung) {
                    $SQL = 'SELECT *';
                    $SQL .= ' FROM dokumentzuordnungen';
                    $SQL .= ' WHERE doz_xxx_kuerzel = ' . $this->_DB->WertSetzen('DZU', 'T', $Zuordnung[0]);
                    $SQL .= ' AND doz_xxx_key = ' . $this->_DB->WertSetzen('DZU', 'N0', $Zuordnung[1]);
                    $SQL .= ' AND doz_doc_key = ' . $this->_DB->WertSetzen('DZU', 'N0', $DOC_KEY);

                    $rsDOC = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('DZU'));

                    if ($rsDOC->EOF()) {
                        $SQL = 'INSERT INTO dokumentzuordnungen(doz_doc_key,doz_xxx_kuerzel,doz_xxx_key';
                        $SQL .= ') VALUES(' . $this->_DB->WertSetzen('DZU', 'N0', $DOC_KEY);
                        $SQL .= ',' . $this->_DB->WertSetzen('DZU', 'T', $Zuordnung[0]);
                        $SQL .= ',' . $this->_DB->WertSetzen('DZU', 'N0', $Zuordnung[1]);
                        $SQL .= ')';

                        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('DZU'));
                    }
                }

                $Pfad = $this->_AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis');
                $Pfad .= '/' . (intval($DOC_KEY / self::TEILER));
                if (!is_dir($Pfad)) {
                    mkdir($Pfad);
                }

                if (is_dir($Pfad)) {
                    $Pfad .= '/' . sprintf('%d', $DOC_KEY) . '.' . strtolower($Dateierweiterung);
                }
            }

			$Erg['doc_key']=$DOC_KEY;
			$Erg['pfad']=$Pfad;

			$this->_DB->TransaktionCommit();

			return $Erg;
		}
		catch(Exception $ex)
		{
			if($this->_DB->TransaktionAktiv())
			{
				$this->_DB->TransaktionRollback();
			}
			$Erg['fehler']=1;
			$Erg['fehlertext']=$ex->getMessage();
			return $Erg;
		}
	}

    /**
     * L�scht eine Dokumentenzuordnung
     *
     * @param $XTN_KUERZEL
     * @param $XXX_KEY
     * @param bool $DateiLoeschen, falls es keine Zuordnung mehr auf diese gibt
     */
	public function DokumentenZuordnungLoeschen($DOC_KEY, $XTN_KUERZEL, $XXX_KEY, $DateiLoeschen = true){

	    $SQL = 'delete from dokumentzuordnungen ';
        $SQL .= ' WHERE doz_xxx_kuerzel = '.$this->_DB->WertSetzen('DZU', 'T',$XTN_KUERZEL);
        $SQL .= ' AND doz_xxx_key = '.$this->_DB->WertSetzen('DZU', 'N0',$XXX_KEY);
        $SQL .= ' AND doz_doc_key = '.$this->_DB->WertSetzen('DZU','N0', $DOC_KEY);

        $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('DZU'));

        if($DateiLoeschen){
            $SQL = ' SELECT * from dokumentzuordnungen ';
            $SQL .= 'WHERE doz_doc_key = '.$this->_DB->WertSetzen('DZU','N0', $DOC_KEY);

            if($this->_DB->ErmittleZeilenAnzahl($SQL,$this->_DB->Bindevariablen('DZU')) == 0){
                $this->DokumentLoeschen($DOC_KEY);
            }
        }
    }

    /**
     * �ndert die Daten zu einem Dokument (Uploaddatum, Bemerkung, Bezeichnung)
     *
     * @param $DOCKEY
     * @param $Datum
     * @param $Bezeichnung
     * @param string $Bemerkung
     */
	public function DokumentenDatenAendern($DOCKEY, $Datum, $Bezeichnung, $Bemerkung='')
	{
	    $SQL = 'UPDATE Dokumente ';
	    $SQL .= ' SET DOC_DATUM='.$this->_DB->WertSetzen('DOC','D',$Datum,true);
	    $SQL .= ',DOC_BEZEICHNUNG='.$this->_DB->WertSetzen('DOC','T',$Bezeichnung,true);
	    $SQL .= ',DOC_BEMERKUNG='.$this->_DB->WertSetzen('DOC','T',$Bemerkung,true);
	    $SQL .= ',DOC_USER=' .$this->_DB->WertSetzen('DOC','T',$this->_AWISBenutzer->BenutzerName());
	    $SQL .= ',DOC_USERDAT=SYSDATE';
	    $SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC','N0', $DOCKEY);;

	    $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('DOC'));
	}

    /**
     * Liefert alle Dokumente(Pfad, Bezeichnung usw.) einer Zuordnung (XXX_KEY + XTN_KEY)
     *
     * @param      $XTN_KUERZEL
     * @param      $XXX_KEY
     * @param bool $ExistenzPruefen
     * @return array|bool
     */
    public function DokumenteEinerZuordnung($XTN_KUERZEL, $XXX_KEY,$ExistenzPruefen=false){

        $Erg = Array();

        $SQL = 'SELECT *';
        $SQL .= ' FROM Dokumente';
        $SQL .= ' INNER JOIN Dokumentzuordnungen ON DOC_KEY = DOZ_DOC_KEY';
        $SQL .= ' WHERE DOZ_XXX_KEY = '.$this->_DB->WertSetzen('DOC','N0',$XXX_KEY);
        $SQL .= ' AND DOZ_XXX_KUERZEL='.$this->_DB->WertSetzen('DOC','T',$XTN_KUERZEL);

        $rsDOC =  $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DOC'));
        while(!$rsDOC->EOF()){
            $Dokument = array();
            $Pfad = $this->_AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis',false);
            $Pfad .= '/'.(intval($rsDOC->FeldInhalt('DOC_KEY')/500));

            $Pfad .= '/'.$rsDOC->FeldInhalt('DOC_KEY').'.'.strtolower($rsDOC->FeldInhalt('DOC_ERWEITERUNG'));

            if($ExistenzPruefen and !is_file($Pfad)){
                return false;
            }

            $Dokument['DOC_KEY']=$rsDOC->FeldInhalt('DOC_KEY');
            $Dokument['Pfad']=$Pfad;
            $Dokument['Erweiterung']=$rsDOC->FeldInhalt('DOC_ERWEITERUNG');
            $Dokument['Eigentuemer']=$rsDOC->FeldInhalt('DOC_XBN_KEY');
            $Dokument['Bezeichnung']=$rsDOC->FeldInhalt('DOC_BEZEICHNUNG');

            $Erg[] = $Dokument;

            $rsDOC->DSWeiter();
        }

        return $Erg;
    }

}
