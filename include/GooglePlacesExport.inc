<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('PHPExcel.php');

function GooglePlacesOeffnungszeitenExport($Benutzer, $Datei, $Protokoll)
{
	Try
	{
		$AWISBenutzer = awisBenutzer::Init($Benutzer);
		$DB = awisDatenbank::NeueVerbindung('AWIS');
		$DB->Oeffnen();

		$fp = fopen($Protokoll,"a+");
		$heute = date("d.m.Y H:i:s");                         
		fputs($fp,"\n================================================================\n");
		fputs($fp,'Protokoll der Oeffnungszeitenaenderungen am: '.$heute."\n");
		fputs($fp,"================================================================\n");

		if (is_file($Datei))
		{
			//Öffnungszeiten der Filialen einlesen und speichern
			$SQL  = 'SELECT';
			$SQL .= ' fif.fif_fil_id AS FIL_ID, ';
			$SQL .= "'2:' ||";
			$SQL .= ' foz.foz_mofrvon';
			$SQL .= " || ':' ||";
			$SQL .= ' foz.foz_mofrbis';
			$SQL .= " || ',3:' ||";
			$SQL .= ' foz.foz_mofrvon';
			$SQL .= " || ':' ||";
			$SQL .= ' foz.foz_mofrbis';
			$SQL .= " || ',4:' ||";
			$SQL .= ' foz.foz_mofrvon';
			$SQL .= " || ':' ||";
			$SQL .= ' foz.foz_mofrbis';
			$SQL .= " || ',5:' ||";
			$SQL .= ' coalesce(foz.foz_dovon,foz.foz_mofrvon)';
			$SQL .= " || ':' ||";
			$SQL .= ' coalesce(foz.foz_dobis,foz.foz_mofrbis)';
			$SQL .= " || ',6:' ||";
			$SQL .= ' foz.foz_mofrvon';
			$SQL .= " || ':' ||";
			$SQL .= ' foz.foz_mofrbis';
			$SQL .= " || ',7:' ||";
			$SQL .= ' foz.foz_savon';
			$SQL .= " || ':' ||";
			$SQL .= 'foz.foz_sabis AS ZEITEN';
			$SQL .= ' FROM FILIALINFOS fif';
			$SQL .= ' INNER JOIN filialenoeffnungszeitenmodelle foz';
			$SQL .= ' ON fif.fif_wert = foz.foz_key';
			$SQL .= ' WHERE fif.fif_fit_id=125';
			$SQL .= ' ORDER BY fif.fif_fil_id';
			$rsFil = $DB->RecordSetOeffnen($SQL);

			$Oeffnungszeiten = array() ;
			$i=0;
			while (!$rsFil->EOF())
			{
				$Oeffnungszeiten[$rsFil->FeldInhalt('FIL_ID')] = $rsFil->FeldInhalt('ZEITEN');
				$rsFil->DSWeiter();
			}

			//Excel Datei öffnen und mit den eingelesenen Öffnungszeiten updaten
			$XLSXObj = PHPExcel_IOFactory::load($Datei); 
			$BlattObj = $XLSXObj->getActiveSheet();
			
			if($BlattObj->getCell('O1')->getValue()=='Hours')
			{
				for ($Zeile=2;$BlattObj->cellExists('A'.$Zeile);$Zeile++)
				{
					$Fil = $BlattObj->getCell('A'.$Zeile)->getValue();
					$ZeitenAlt = '';
					if(is_numeric($Fil))
					{
						$ProtZeile = '';
						$ZeitenAlt = $BlattObj->getCell('O'.$Zeile)->getValue();
						$ZeitenNeu = (isset($Oeffnungszeiten[$Fil])?$Oeffnungszeiten[$Fil]:'');
						if ($ZeitenAlt<>$ZeitenNeu)
						{
							$BlattObj->getCell('O'.$Zeile)->setValueExplicit($ZeitenNeu, PHPExcel_Cell_DataType::TYPE_STRING);
							$ProtZeile  = 'Fil.: '.$Fil;
							$ProtZeile .= ' Neu: '.$ZeitenNeu; 
							$ProtZeile .= ' Alt: '.$ZeitenAlt;
							$ProtZeile .= "\n";
							fputs($fp, $ProtZeile);
							$i++; 
						}
					}
				}
				
				$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);
				switch ($ExportFormat)
				{
					case 1:                 // Excel 5.0
						$WriterObj = new PHPExcel_Writer_Excel5($XLSXObj);
						break;
					case 2:				
						$WriterObj = new PHPExcel_Writer_Excel2007($XLSXObj);
						break;
					default:
						$WriterObj = new PHPExcel_Writer_Excel5($XLSXObj);
						break;
				}
				$WriterObj->save($Datei);
				$XLSXObj->disconnectWorksheets();
				fputs($fp,"\nÄnderungen: ". $i ."\n");
				fputs($fp,"\nEnde Protokoll\n");
			}
			else	
			{
				fputs($fp,'FEHLER: Inhalt in Datei <' .$Datei ."> ist falsch!\n");
			}
		}
		else
		{
			fputs($fp,'FEHLER: Datei <' .$Datei ."> ist nicht vorhanden!\n");
		}
		
	}
	
	catch (awisException $ex)
	{
		echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
	    echo 'SQL: '.$ex->getSQL().PHP_EOL;
	    fputs($fp,'FEHLER: '.$ex->getMessage()."\n");
	}
	catch (Exception $ex)
	{
		echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
	    fputs($fp,'FEHLER: '.$ex->getMessage()."\n");
	}
	
	fclose($fp);
}
