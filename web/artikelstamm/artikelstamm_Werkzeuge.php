<script>
    $(document).ready(function () {
        jQuery(".EingabeFeld,.EingabeFeldPflicht,.EingabeLabel,.EingabeFeldTextarea,.EingabeBild").tooltipster({
            animation: 'grow',
            contentAsHTML: true,
        });
    });
</script>
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;        // Aus AWISFormular

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISBenutzer = awisBenutzer::Init();
} catch (Exception $ex) {
    die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');

$Form = new AWISFormular();
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

try {
    $Form = new awisFormular();

    if ($AWISBenutzer->HatDasRecht(458) == 0) {
        $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200902050928");
        die();
    }

    $Register = new awisRegister(453);
    $Register->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200902050927");
    } else {
        echo 'AWIS: ' . $ex->getMessage();
    }
}
?>