<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("config.inc.php");
/*global $AWISBenutzer;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
*/
?>
</head>
<body>
<?php
global $con;
global $awisDBFehler;
global $awisRSZeilen;
$awis_sys_user = "system";
$awis_sys_pwd = "werder";

$db = awisDBServer();
$awis_link='';

// Keine Auswahl mehr, SK 14.02.2008
/*
if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = strtolower($_GET["txtDB"]);
}
else
{
	$db = strtolower(awisDBServer());
	$awis_link= "@" . $db . ".atu.de";
}
*/

// immer auf den lokalen Server (RAC)

awis_Debug(1,$_REQUEST);

function awisHoleObjektNamen ($con, $Objektnummer)
{
	$awisDBFehler='';
	
	$SQL = "Select object_type, object_name ";
	$SQL .= "From all_objects ";
	$SQL .= "WHERE object_id = " . $Objektnummer . " ";
	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	if (!empty($rsResult))
	{
		return($rsResult['OBJECT_TYPE'][0] . ': ' . $rsResult['OBJECT_NAME'][0]);
	}
	else
	{
		return('unbekannt');
	}
}

/***************************************************************************************************************************
*
*	Session-Details wenn eine SID mit �bergeben wurde
*
***************************************************************************************************************************/

if(!empty($_GET["SID"]))
{
	print '<h1 id=SeitenTitel>Session ' . $_GET["SID"] . " / " . $_GET["SERIAL"] . '</h1>';
	print "<table width=100% border=0><tr>";
	print '<td><a href="' . $_SERVER["PHP_SELF"] . '?cmdAktion=Sessions&txtDB='.(isset($_REQUEST['txtDB'])?$_REQUEST['txtDB']:'').'">verbergen</a></td>';
	print '<td align=right><a accesskey=b href="' . $_SERVER["PHP_SELF"] . '?cmdAktion=Sessions&SID=' . $_GET["SID"] . "&SERIAL=" . $_GET["SERIAL"] . "&INST_ID=" . $_GET["INST_ID"] . "&KILL=" . $_GET["SID"] . "," . $_GET["SERIAL"] . '">Session a<u>b</u>brechen</a></td>';
	print "</tr></table>";
	print "<table id=DatenTabelle width=100% border=1>";
	
	$SQL = "Select VSWN.* ";
	$SQL .= "From GV\$SQLTEXT_WITH_NEWLINES".$awis_link." VSWN, GV\$SESSION".$awis_link." VSE ";
	$SQL .= "WHERE VSE.SID = " . $_GET["SID"] . " ";
	$SQL .= "AND VSE.INST_ID = " . $_GET["INST_ID"] . " ";
//  	$SQL .= "AND VSE.SERIAL# = " . $_GET["SERIAL"] . " ";
	$SQL .= "AND VSE.PREV_SQL_ADDR = VSWN.ADDRESS ";
	$SQL .= "AND VSE.PREV_HASH_VALUE = VSWN.HASH_VALUE ";
	$SQL .= "ORDER BY VSWN.PIECE ";
	$rsResult = awisOpenRecordset($con, $SQL);
		
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;

	$Ergebnis = "";
	if ($rsResultZeilen > 0)
	{
		for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
		{
			$Ergebnis .= $rsResult['SQL_TEXT'][$ResultZeile];
		}
		print "<tr>";
		print "<td id=FeldBez>Vorheriger SQL-Text</td>";
		print "</tr>";
		echo '<tr><td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>';
		echo $Ergebnis;
		echo  '</td>';
		echo '</tr>';
	}
	
	$SQL = "Select VSWN.* ";
	$SQL .= "From GV\$SQLTEXT_WITH_NEWLINES".$awis_link." VSWN, GV\$SESSION".$awis_link." VSE ";
	$SQL .= "WHERE VSE.SID = " . $_GET["SID"] . " ";
	$SQL .= "AND VSE.INST_ID = " . $_GET["INST_ID"] . " ";
// 	$SQL .= "AND VSE.SERIAL# = " . $_GET["SERIAL"] . " ";
	$SQL .= "AND VSE.SQL_ADDRESS = VSWN.ADDRESS ";
	$SQL .= "AND VSE.SQL_HASH_VALUE = VSWN.HASH_VALUE ";
	$SQL .= "ORDER BY VSWN.PIECE ";
	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;
	
	$Ergebnis = "";
	if ($rsResultZeilen > 0)
	{
		for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
		{
			$Ergebnis .= $rsResult['SQL_TEXT'][$ResultZeile];
		}
		print "<tr>";
		print "<td id=FeldBez>SQL-Text</td>";
		print "</tr>";
		echo '<tr><td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>';
		echo $Ergebnis;
		echo  '</td>';
		echo '</tr>';
	}
	print "</table>";

	$SQL = "Select VSE.*, TO_CHAR(VSE.LOGON_TIME, 'DD.MM HH24:MI:SS') LOGON_TIME_FORMAT, ";
	$SQL .= "TO_CHAR(SYSDATE - (VSE.LAST_CALL_ET/86400), 'DD.MM HH24:MI:SS') LETZTE_AKTION ";
	$SQL .= "From GV\$SESSION".$awis_link." VSE ";
	$SQL .= "WHERE VSE.SID = " . $_GET["SID"] . " ";
	$SQL .= "AND VSE.INST_ID = " . $_GET["INST_ID"] . " ";
	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;

	if ($rsResultZeilen > 0)
	{
		print "<table id=DatenTabelle width=100% border=1>";
		print "<tr>";
		print "<td id=FeldBez>USERNAME</td>";
		print "<td id=FeldBez>OSUSER</td>";
		print "<td id=FeldBez>STATUS</td>";
		print "<td id=FeldBez>LOGON_TIME</td>";
		print "<td id=FeldBez>Letzte Aktion</td>";
		print "</tr>";
		print "<tr>";
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['USERNAME'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['OSUSER'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['STATUS'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['LOGON_TIME_FORMAT'][0] . '</td>';
		if ($rsResult['LAST_CALL_ET'][0] > 86400)
		{
			echo '<td class=HinweisText>' . $rsResult['LETZTE_AKTION'][0] . '</td>';
		}
		else
		{
			echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['LETZTE_AKTION'][0] . '</td>';
		}
		echo '</tr>';
		print "</table>";
	}
	else
	{
		echo "<br>Session " . $_GET["SID"] . " ist nicht mehr verf&uuml;gbar<br>";
	}
	flush();
	
	$SQL = "Select VSI.*, VIN.instance_name ";
	$SQL .= "From GV\$SESS_IO".$awis_link." VSI, GV\$INSTANCE".$awis_link." VIN ";
	$SQL .= "WHERE VSI.SID = " . $_GET["SID"] . " ";
	$SQL .= "AND VSI.INST_ID = " . $_GET["INST_ID"] . " ";
	$SQL .= "AND VSI.INST_ID=VIN.INST_ID";
	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;
	
	$Ergebnis = "";
	if ($rsResultZeilen > 0)
	{
		print "<table id=DatenTabelle width=100% border=1>";
		print "<tr>";
		print "<td id=FeldBez>INSTANCE</td>";
		print "<td id=FeldBez>BLOCK_GETS</td>";
		print "<td id=FeldBez>CONSISTENT_GETS</td>";
		print "<td id=FeldBez>PHYSICAL_READS</td>";
		print "<td id=FeldBez>BLOCK_CHANGES</td>";
		print "<td id=FeldBez>CONSISTENT_CHANGES</td>";
		print "</tr>";
		print "<tr>";
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['INSTANCE_NAME'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['BLOCK_GETS'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['CONSISTENT_GETS'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['PHYSICAL_READS'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['BLOCK_CHANGES'][0] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . '>' . $rsResult['CONSISTENT_CHANGES'][0] . '</td>';
		echo '</tr>';
		print "</table>";
	}
	else
	{
		echo "<br>Session " . $_GET["SID"] . " ist nicht mehr verf&uuml;gbar<br>";
	}
	print "<br>";
	flush();
	
	// Blockierende Sessions identifizieren
	/*$SQL = "select * from v\$lock ";
	$SQL .= "where sid in ( ";
	$SQL .= "select sid from v\$lock ";
	$SQL .= "where id1 || '~' || id2 in ";
	$SQL .= "(select id1 || '~' || id2 from v\$lock ";
	$SQL .= "where sid = " . $_GET["SID"] . ")) ";
	$SQL .= "and type in ('TM', 'TX') ";
	$SQL .= "order by type desc, id1, sid ";*/
	
	$SQL = "select * from gv\$lock ";
	$SQL .= "where sid in ( ";
	$SQL .= "select sid from gv\$lock ";
	$SQL .= "where id1 || '~' || id2 in ";
	$SQL .= "(select id1 || '~' || id2 from gv\$lock ";
	$SQL .= "where sid = " . $_GET["SID"] . ")) ";
	$SQL .= "and type in ('TM', 'TX') ";
	$SQL .= "and inst_id= " . $_GET["INST_ID"] . " ";
	$SQL .= "order by type desc, id1, sid ";
	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;
	
	if ($rsResultZeilen > 0)
	{
		$Blockierende_SID='';
		$Objektnummer = 0;
//awis_Debug(1,$rsResult,$SQL);		
		for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
		{
			if ($rsResult['TYPE'][$ResultZeile] == 'TX')		// Hier wird nur identifiziert, welche SID die blockierende ist
			{
				if ($rsResult['BLOCK'][$ResultZeile] == 1)
				{
					$Blockierende_SID = $rsResult['SID'][$ResultZeile];
				}
			}
			else							// Jetzt kommen die Daten mit den gesperrten Objekten
			{
				if ($rsResult['ID1'][$ResultZeile] <> $Objektnummer)
				{
					$Objektnummer = $rsResult['ID1'][$ResultZeile];
					$Objektname = awisHoleObjektNamen($con, $Objektnummer);
				}
				if ($_GET["SID"] == $Blockierende_SID)
				{
					if ($rsResult['SID'][$ResultZeile] <> $_GET["SID"])
					{
						echo "Session " . $Blockierende_SID . " <b>blockiert</b> die Session "  . $rsResult['SID'][$ResultZeile] . " seit " . $rsResult['CTIME'][$ResultZeile] . " (" . $Objektname . ")<br>";
					}
				}
				else
				{
					if ($rsResult['SID'][$ResultZeile] == $_GET["SID"])
					{
						echo "Session " . $_GET["SID"] . " wird seit " . $rsResult['CTIME'][$ResultZeile] . " Sekunden durch die Session " . $Blockierende_SID . " <b>blockiert</b> (" . $Objektname . ")<br>";
					}
				}
			}
			
		}
		echo "<br>";
	}

	if(!empty($_GET["KILL"]))
	{
		// Kontrolle welcher der aktuelle DB-Server ist
		$SQL = "Select * ";
		$SQL .= "From gv\$instance".$awis_link;
		$rsResult = awisOpenRecordset($con, $SQL);
		
		if($rsResult==FALSE)
		{
			awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
		}
		
		$SQL = "ALTER SYSTEM KILL SESSION '" . $_GET["KILL"] . "' IMMEDIATE";
		
 		$syscon = OCILogon( $awis_sys_user,$awis_sys_pwd,$db);
 	
		awisExecute($syscon,$SQL);
 		OCILogoff($syscon);
	}
}

/*
select distinct s.sid,
ob.kglhdnsp,ob.kglnaobj
    from v$session s, x$kglob ob, dba_kgllock lk
     where lk.kgllkhdl = ob.kglhdadr
      and  lk.kgllkuse = s.saddr
      and s.sid in (select sid from v$lock v2
where id1 || '~' || id2 in
(select id1 || '~' || id2 from v$lock v1
where sid = 13
and block = 1)
and sid <> 13)
      and ob.kglhdnsp = 0;
*/

echo '<form name=frmSuche method=get action=' . $_SERVER["PHP_SELF"] . '>';
echo '<input type="hidden" name="cmdAktion" value="Sessions">';

echo '<br>';
echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';

echo '<td colspan=3><input name=txtDB readonly accesskey=d value="'.awisDBServer().'"></td>';
// Keine Auswahl mehr, SK 14.02.2008
/*
echo '<td colspan=3><select accesskey=d name="txtDB">';

$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
{
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '"';
	if ($db == strtolower($rsResult['DBK_NAME'][$DBNr]))
	{
		echo ' selected="selected"';
	}
	echo '>' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
}

echo '</select></td>';
*/
echo '</tr>';
echo '</table>';
print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo '</form>';

if(isset($_REQUEST['cmdSuche_x'])||$awis_link!='')
{


/***************************************************************************************************************************
*
*	Session-�bersicht
*
***************************************************************************************************************************/

print "<h1 id=SeitenTitel>Sessions</h1>";
/*$SQL = "Select VSE.*, TO_CHAR(VSE.LOGON_TIME, 'DD.MM HH24:MI:SS') LOGON_TIME_FORMAT, ";
$SQL .= "TO_CHAR(SYSDATE - (VSE.LAST_CALL_ET/86400), 'DD.MM HH24:MI:SS') LETZTE_AKTION, VSS.NAME SSERVER, DLI.* ";
$SQL .= "From V\$SESSION VSE, V\$SHARED_SERVER VSS, (select sid Gesperrte_SID ";
$SQL .= "	from v\$lock ";
$SQL .= "	where request <> 0) DLI ";
$SQL .= "WHERE USERNAME IS NOT NULL ";
$SQL .= "AND VSE.PADDR = VSS.PADDR(+) ";
$SQL .= "AND VSE.SID = DLI.Gesperrte_SID(+) ";
$SQL .= "ORDER BY VSE.SID ";*/

$SQL = "Select VSE.*, TO_CHAR(VSE.LOGON_TIME, 'DD.MM HH24:MI:SS') LOGON_TIME_FORMAT, ";
$SQL .= "TO_CHAR(SYSDATE - (VSE.LAST_CALL_ET/86400), 'DD.MM HH24:MI:SS') LETZTE_AKTION, VSS.NAME SSERVER, DLI.*, VIN.INSTANCE_NAME ";
$SQL .= "From GV\$SESSION".$awis_link." VSE, GV\$SHARED_SERVER".$awis_link." VSS, GV\$INSTANCE".$awis_link." VIN, (select sid Gesperrte_SID from gv\$lock".$awis_link." where request <> 0) DLI ";
$SQL .= "WHERE USERNAME IS NOT NULL AND NOT (USERNAME LIKE 'SYSMAN' OR USERNAME LIKE 'DBSNMP') AND VSE.PADDR = VSS.PADDR(+) AND VSE.SID = DLI.Gesperrte_SID(+) AND VSE.INST_ID=VIN.INST_ID ORDER BY VSE.SID";

$rsResult = awisOpenRecordset($con, $SQL);

awis_Debug(1,$SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>SID</td>";
print "<td id=FeldBez title='Blockierende SID'>B</td>";
print "<td id=FeldBez>USERNAME</td>";
print "<td id=FeldBez>OSUSER</td>";
print "<td id=FeldBez>MODULE</td>";
print "<td id=FeldBez>MACHINE</td>";
print "<td id=FeldBez>LOGON_TIME</td>";
print "<td id=FeldBez>Letzte Aktion</td>";
print "<td id=FeldBez>SERVER</td>";
print "<td id=FeldBez>INSTANCE</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	if ($rsResult['LAST_CALL_ET'][$ResultZeile] > 86400)
	{
//		echo '<td class=HinweisText>' . $rsResult['LETZTE_AKTION'][$ResultZeile] . '</td>';
		echo '<td  class=HinweisText><a title="SID: ' . $rsResult['SID'][$ResultZeile]  . ' SERIAL#: ' . $rsResult['SERIAL#'][$ResultZeile] . '" href="' . $_SERVER["PHP_SELF"] . '?cmdAktion=Sessions&SID=' . $rsResult['SID'][$ResultZeile]  . '&SERIAL=' . $rsResult['SERIAL#'][$ResultZeile] . '&INST_ID=' . $rsResult['INST_ID'][$ResultZeile] . '&txtDB='.(isset($_REQUEST['txtDB'])?$_REQUEST['txtDB']:'').'">' .  $rsResult['SID'][$ResultZeile] . '</a></td>';
	}
	else
	{
//		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['LETZTE_AKTION'][$ResultZeile] . '</td>';
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title="SID: ' . $rsResult['SID'][$ResultZeile]  . ' SERIAL#: ' . $rsResult['SERIAL#'][$ResultZeile] . '" href="' . $_SERVER["PHP_SELF"] . '?cmdAktion=Sessions&SID=' . $rsResult['SID'][$ResultZeile]  . '&SERIAL=' . $rsResult['SERIAL#'][$ResultZeile] . '&INST_ID=' . $rsResult['INST_ID'][$ResultZeile] . '&txtDB='.(isset($_REQUEST['txtDB'])?$_REQUEST['txtDB']:'').'">' .  $rsResult['SID'][$ResultZeile] . '</a></td>';
	}
	
	// ggf. Blockierende Session heraufinden
	if ($rsResult['GESPERRTE_SID'][$ResultZeile])
	{
		$SQL = "select sid from gv\$lock".$awis_link;
		$SQL .= " where id1 || '~' || id2 in ";
		$SQL .= "(select id1 || '~' || id2 from gv\$lock".$awis_link. " ";
		$SQL .= "where sid = " . $rsResult['SID'][$ResultZeile] . " and inst_id = " . $rsResult['INST_ID'][$ResultZeile] . ") ";
		$SQL .= "and block = 1 ";
		$SQL .= "and inst_id = " . $rsResult['INST_ID'][$ResultZeile] . " ";
		$rsResult1 = awisOpenRecordset($con, $SQL);

		if($rsResult1==FALSE)
		{
			awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
		}
		$rsResultZeilen1=$awisRSZeilen;
		echo '<td class=HinweisText>';
		for($ResultZeile1=0;$ResultZeile1<$rsResultZeilen1;$ResultZeile1++)
		{
			echo $rsResult1['SID'][$ResultZeile1];
			if ($ResultZeile1<$rsResultZeilen1 - 1)
			{
				echo ', ';
			}
		}
		echo '</td>';
	}
	else
	{
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>&nbsp;</td>';
	}
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['USERNAME'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['OSUSER'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['MODULE'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['MACHINE'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>';
	if ($rsResult['STATUS'][$ResultZeile] == 'KILLED')
	{
		echo 'KILLED</td>';
	}
	else
	{
		echo $rsResult['LOGON_TIME_FORMAT'][$ResultZeile] . '</td>';
	}
	if ($rsResult['LAST_CALL_ET'][$ResultZeile] > 86400)
	{
		echo '<td class=HinweisText>' . $rsResult['LETZTE_AKTION'][$ResultZeile] . '</td>';
	}
	else
	{
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['LETZTE_AKTION'][$ResultZeile] . '</td>';
	}
	if ($rsResult['SERVER'][$ResultZeile] == 'SHARED')
	{
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['SSERVER'][$ResultZeile] . '</td>';
	}
	else
	{
		echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['SERVER'][$ResultZeile] . '</td>';
	}
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['INSTANCE_NAME'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
print "<br>";
print "Anzahl: " . $rsResultZeilen . "<br>";
print "<br>";
flush();


/***************************************************************************************************************************
*
*	Shared-Server-�bersicht
*
***************************************************************************************************************************/

print "<h1 id=SeitenTitel>Shared Server Monitor</h1>";

$SQL = "Select a.*, b.instance_name ";
$SQL .= "From GV\$SHARED_SERVER_MONITOR".$awis_link." a, GV\$INSTANCE".$awis_link." b ";
$SQL .= "WHERE a.inst_id=b.inst_id ";
$SQL .= "order by b.instance_name";
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>INSTANCE</td>";
print "<td id=FeldBez>MAXIMUM_CONNECTIONS</td>";
print "<td id=FeldBez>MAXIMUM_SESSIONS</td>";
print "<td id=FeldBez>SERVERS_STARTED</td>";
print "<td id=FeldBez>SERVERS_TERMINATED</td>";
print "<td id=FeldBez>SERVERS_HIGHWATER</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['INSTANCE_NAME'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['MAXIMUM_CONNECTIONS'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['MAXIMUM_SESSIONS'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['SERVERS_STARTED'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['SERVERS_TERMINATED'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['SERVERS_HIGHWATER'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
print "<br>";

/***************************************************************************************************************************
*
*	Die einzelnen Shared-Server
*
***************************************************************************************************************************/

print "<h1 id=SeitenTitel>Shared Server</h1>";

$SQL = "Select a.name, a.paddr, a.status, ";
$SQL .= "a.messages, a.bytes, a.breaks, a.CIRCUIT, a. idle, a.busy, a.requests, b.instance_name ";
$SQL .= "From GV\$SHARED_SERVER".$awis_link." a, GV\$INSTANCE".$awis_link." b ";
$SQL .= "WHERE a.inst_id=b.inst_id ";
$SQL .= "order by b.instance_name, a.name";
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_status_shared_server_und_sessions.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>INSTANCE</td>";
print "<td id=FeldBez>NAME</td>";
print "<td id=FeldBez>PADDR</td>";
print "<td id=FeldBez>STATUS</td>";
print "<td id=FeldBez>MESSAGES</td>";
print "<td id=FeldBez>BYTES</td>";
print "<td id=FeldBez>BREAKS</td>";
print "<td id=FeldBez>CIRCUIT</td>";
print "<td id=FeldBez>IDLE</td>";
print "<td id=FeldBez>BUSY</td>";
print "<td id=FeldBez>REQUESTS</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['INSTANCE_NAME'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['NAME'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['PADDR'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['STATUS'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['MESSAGES'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['BYTES'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['BREAKS'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['CIRCUIT'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['IDLE'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['BUSY'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['REQUESTS'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
}
?>
</body>
</html>