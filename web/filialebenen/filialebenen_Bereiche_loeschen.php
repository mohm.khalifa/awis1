<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Bereich
{
	$Tabelle = 'FRB';
	$Key=$_POST['txtFRB_KEY'];
	$FRB_KEY=$_POST['txtFRB_KEY'];

	$Felder=array();

	$Felder[]=array(awis_TextKonserve($con,'FRB_BEZEICHNUNG','FRB',$AWISSprache),$_POST['txtFRB_BEZEICHNUNG']);
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Mitglieder':
			$Tabelle = 'FBR';
			$Key=$_GET['Del'];
	
			$rsDaten = awisOpenRecordset($con, 'SELECT FBR_KEY,FBR_FRB_KEY,XBN_VOLLERNAME FROM filialebenenrollenberzugriffe INNER JOIN Benutzer ON FBR_XBN_KEY = XBN_KEY WHERE FBR_KEY=0'.$Key);
			$Felder=array();
			$Felder[]=array(awis_TextKonserve($con,'XBN_VOLLERNAME','XBN',$AWISSprache),$rsDaten['XBN_VOLLERNAME'][0]);
			$FRB_KEY = $rsDaten['FBR_FRB_KEY'][0];
			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'FRB':
			$SQL = 'DELETE FROM filialebenenrollenbereiche WHERE FRB_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;
		default:
			break;
		case 'FBR':
			$SQL = 'DELETE FROM filialebenenrollenberzugriffe WHERE FBR_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = $_POST['txtFRB_KEY'];
			break;
		default:
			break;
	}

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)==false)
		{
			awisErrorMailLink('filialebenen_stammdaten_loeschen',1,$awisDBError['messages'],'');
		}
		awis_BenutzerParameterSpeichern($con, "AktuellerFilialBereich" , $AWISBenutzer->BenutzerName(), $AWIS_KEY1);
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./filialebenen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';
	echo '<input type=hidden name=txtFRB_KEY value="'.$FRB_KEY.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}
?>