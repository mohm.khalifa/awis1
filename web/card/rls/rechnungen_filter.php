<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<html>
<head>
<title>&nbsp;</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets 


?>
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	<script language="JavaScript">
	
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.startzeit();	//Zeitmessung
	<?php } ?>
	
	function init()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
	}
	
	//Wird bei Klick auf das Bild eingabe_ok (Parameter Rechnungsnr) ausgeführt, d.h. übermittelt die Rechnungsnr nach card_haupt.php
	
	function filteranwenden()
	{
		var datobj = document.getElementById('filterdatum');
		
		if(datobj.value!=''&&!datumspruefung(datobj))
		{
			return;
		}
		
		var arrObj=document.getElementsByName('filter');
		var strIdChecked='';
		
		for(i=0;i<arrObj.length&&strIdChecked=='';i++)
		{
			if(arrObj[i].checked)
				strIdChecked=arrObj[i].id;
		}
				
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Rechnungen filtern");	//Zeitmessung
		<?php } ?>
		parent.window.returnValue = datobj.value+';'+strIdChecked;
		parent.self.close();
	}
	
	function geklickt(strGruppe)
	{
		var obj = window.event.srcElement;
		
		var objId= obj.id;
		
		var arrObj=document.getElementsByName(strGruppe);
		
		if(obj.checked)
		{
			for(i=0;i<arrObj.length;i++)
			{
				if(arrObj[i].id!=objId)
					arrObj[i].checked=false;
			}
		}
		else
		{
			obj.checked=true;
			return;
		}
	}
	
	function datumheute()
	{
		var obj = window.event.srcElement;
		obj.value = '<?php echo strftime("%d.%m.%y");?>';
		window.event.cancelBubble = true;
	}	
	
	
	</script>
	
	</head>
	<body onload="init();" style="overflow:hidden;">
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="middle">
		<table cellspacing="0" cellpadding="0" border="1">
		<tr><td colspan="2" style="font-weight:bold;text-align:center;">Filter Rechnungen</td></tr>
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td>alle</td><td align="center"><input type="checkbox" name="filter" id="alle" onclick="geklickt('filter');" checked></td>
		</tr>
		<tr>
		<td>offen</td><td align="center"><input type="checkbox" name="filter" id="offen" onclick="geklickt('filter');"></td>
		</tr>
		<tr>
		<td>ausgeglichen</td><td align="center"><input type="checkbox" name="filter" id="ausgeglichen" onclick="geklickt('filter');"></td>
		</tr>
		<tr><td colspan="2" class="oben" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td>DTA-Datum:</td><td><input type="text" class="datum" maxlength="8" id="filterdatum" ondblclick="datumheute();"></td>
		</tr>
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr><td colspan="2" height="5px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td colspan="2" align="center"><button type="button" class="but" onclick="filteranwenden();">Filter anwenden</button></td>
		</tr>
		<tr>
		<td colspan="2" align="center"><button type="button" class="but" onclick="self.close();">Fenster schlie&szlig;en</button></td>
		</tr>
		</table>
	
	</td></tr></table>
	<?php 
	$intDialog = 1;
	include("card/card_zeitende.inc.php");			// Zeitmessung	
?>
</body>
</html>