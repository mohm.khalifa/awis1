<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

?>
<html>
<head>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
?>
	try{
	parent.card_ladezeit.startzeit();	//Ladezeitmessung
	} catch(e){}
<?php } ?>
</script>
<title></title>
<?php

if(isset($strKunden_Nr))		//Nach Aufruf Liste Kunden suchen
{
	$strSql="select rec_rnr from (select max(rec_rnr) rec_rnr from rechnungen where rec_kunden_nr='".
	$strKunden_Nr."' and rec_rgdatum=(select max(rec_rgdatum) from rechnungen where rec_kunden_nr='".
	$strKunden_Nr."' and rec_loesch!=1) and rec_loesch!=1) where rec_rnr is not null";

	$arrRueckgabe = abfrage("ora",$strSql);
	
	if($arrRueckgabe["zeilen"])
	{
		$strRec_rnr = $arrRueckgabe["trefferarray"]["REC_RNR"][0];
	}
	else
	{
		echo "Keine Rechnungen vorhanden";
	}
}

if(isset($strRecBlaett_rnr))
{
	$strSqlBlaett='';
	
	if($strRichtung=='vor')
	{
		$strOperand='+';
	}
	else
	{
		$strOperand='-';
	}
		
	$strSqlInnen="select rownum nr,rec_rnr from
				(select rec_rnr from
					(select * from rechnungen where rec_loesch=0),
					(select * from rechnungen_konto where rek_loesch=0 and rek_betrag<0 and rek_betragart=19)
				where
					rec_rnr=rek_rnr
					and rec_kunden_nr=(select rec_kunden_nr from rechnungen where rec_rnr='".$strRecBlaett_rnr."' and rec_loesch=0)
				order by rek_datum desc,rec_rnr)";
	
	$strSqlBlaett="select rec_rnr from
					(".$strSqlInnen.")
					where nr =
					(select nr ".$strOperand." 1 from
					(".$strSqlInnen.")
					where rec_rnr='".$strRecBlaett_rnr."')";

	$arrBlaett=abfrage("ora",$strSqlBlaett);
	
	if($arrBlaett["zeilen"]>=1)
	{
		$strRec_rnr=$arrBlaett["trefferarray"]["REC_RNR"][0];
	}
	else
	{
		$strRec_rnr=$strRecBlaett_rnr;
	}
}

if(isset($strRec_rnr))
{
	if(isset($aendfeldname))	//�nderungen des Mahndatums bzw. Bemerkungen durchf�hren
	{
		$strSql="update rechnungen set ".$aendfeldname."='".stripslashes($aendfeldwert)."',rec_aend_benutzer='".$strRec_Aend_Benutzer."',
		rec_aend_datum=sysdate where rec_rnr='".$strRec_rnr."'";
		abfrage("ora",$strSql);
	}
	
	if(isset($intRek_buchid))	//Wenn Neuanlage/�nderung in iFrame (Tabelle "rechungen_konto")
	{
		if($intRek_buchid=='')		//Neuanlage Datensatz
		{
			$strSqlSeq = "select seq_rek_buchid.nextval seq from dual";
			$arrSeq = abfrage("ora",$strSqlSeq);
			$intSeq = $arrSeq["trefferarray"]["SEQ"][0];
						
			$strSql="insert into rechnungen_konto values($intSeq,
			'$strRec_rnr',$dblRek_betrag,'$datRek_datum',$intRek_betragart,$intRek_kontoauszugid,'$datRek_datum',
			'$strRek_Aend_Benutzer',sysdate,0,0)";
			abfrage("ora",$strSql);
		}
		else						//�nderung Datensatz
		{
			$strSql="update rechnungen_konto set ".
			(isset($dblRek_betrag)?"rek_betrag=".$dblRek_betrag.",":"").
			(isset($datRek_datum)?"rek_datum='".$datRek_datum."',":"").
			(isset($intRek_betragart)?"rek_betragart=".$intRek_betragart.",":"").
			(isset($intRek_kontoauszugid)?"rek_kontoauszugid=".$intRek_kontoauszugid.",":"").
			"rek_aend_benutzer='".$strRek_Aend_Benutzer."',rek_aend_datum=sysdate";
			if($intRek_loesch)
			{
				$strSql.=",rek_loesch=".$intRek_loesch;
			}
			
			$strSql.=" where rek_buchid=".$intRek_buchid;
			
			abfrage("ora",$strSql);
		}
	}
	
	if($formRgnrGesendet)		//$strRec_rnr wird von Navigation geliefert und ist 6- oder 10-stellig
	{
		$intStrLaenge=strlen($strRec_rnr);
		
		$strSql="select rec_rnr,rec_kunden_nr,rec_gekauftaufpan pan,rec_rgdatum,rec_mahnung1am,rec_mahnung2am,rec_mahnung3am,rec_zahlungsziel,rec_bemerkungen 
		from rechnungen where substr(rec_rnr,1,".$intStrLaenge.")='$strRec_rnr'";
	}
	else						//$strRec_rnr wird aufgrund Detailauswahl bei mehreren Treffern geliefert bzw. ist die vollst�ndige Rechnungsnr
	{
		$strSql="select rec_rnr,rec_kunden_nr,rec_gekauftaufpan pan,rec_rgdatum,rec_mahnung1am,rec_mahnung2am,rec_mahnung3am,rec_zahlungsziel,rec_bemerkungen
		from rechnungen where rec_rnr='$strRec_rnr'";		
	}
	
	$arrRechnungen=abfrage("ora",$strSql);
	
	
	if($arrRechnungen["zeilen"]==1)		//Wenn ein Datensatz gefunden wird, so wird dieser angezeigt
	{
		$strKundennr=$arrRechnungen["trefferarray"]["REC_KUNDEN_NR"][0];
		$strRec_rnr=$arrRechnungen["trefferarray"]["REC_RNR"][0];
		$strPan=$arrRechnungen["trefferarray"]["PAN"][0];
		$strAbrZeitraum=$arrRechnungen["trefferarray"]["REC_RGDATUM"][0];
				
		$strSql='select coalesce("Firma",\'\') || \' \' || coalesce("Abteilung",\'\') "Firma",
		"Name","Vorname","Strasse","PLZ","Ort" from '.$tabAdressen.' where "Kunden_Nr"=\''.$strKundennr.'\'
		 and "Adress_Art"=\'01\'';
		
		$arrAdressen=abfrage("db2",$strSql);
		
		$strSql='select coalesce("Name",\'\') || \' \' || coalesce("Vorname",\'\') "Name" 
		from '.$tabAdressen.' where PAN=\''.$strPan.'\'';
		$arrAdressenEinkaeufer=abfrage("db2",$strSql);
		
		$strSql="select a.summe+b.summe summe,a.summe mahnungen,b.summe rechnungen,a.mahngbanz mahngbanz from
		(select nvl(sum(mag_betrag),0) summe,nvl(count(mag_id),0) mahngbanz from mahngebuehren where mag_rnr = '".$strRec_rnr."'
		and mag_loesch=0) a,
		(select nvl(sum(rek_betrag),0) summe 
		from rechnungen_konto where rek_rnr='".$strRec_rnr."' and rek_loesch=0) b";
		
		
		$arrSumKonto=abfrage("ora",$strSql);
		
		
		$dblKontostand=feldwert($arrSumKonto,"SUMME");	//entspricht dem Kontostand bezogen auf die aktuelle Karte
		$strKontostand=eurobetrag($dblKontostand);		
		$dblKontostandOMG=feldwert($arrSumKonto,"RECHNUNGEN");
		$strKontostandOMG=eurobetrag($dblKontostandOMG);
		$dblMG=feldwert($arrSumKonto,"MAHNUNGEN");
		$strMG=eurobetrag($dblMG);
		$intMGAnz=feldwert($arrSumKonto,"MAHNGBANZ");
		
		
		//Ermittlung der Daten f�r Abrechnungszeitraum: summe entspricht der Summe aller Mahngeb�hrbuchungen bezogen
		//auf einen bestimmten Abrechnungszeitraum
		
		$strSql="select nvl(sum(mag_betrag),0) summe,mag_abrzeitraum datum from mahngebuehren 
		where mag_kunden_nr='".$strKundennr."' and mag_loesch=0 group by mag_abrzeitraum";
		
		$arrAbrechnungszeitraum=abfrage("ora",$strSql);
		
		//Ermittlung der Gesamt-Daten pro Kundennr: Anzahl Rechnungen,Offene Betr�ge, Mahngeb�hren 
		
		$strSql="select anz,(rechnungen+mahnungen) gesamt,mahnungen from
				(
				select
				(select count(*) from rechnungen 
				where exists (select 'x' from rechnungen_konto where rec_rnr=rek_rnr and rek_betragart=19 and rek_betrag<0 and rek_loesch=0)
				and rec_kunden_nr='".$strKundennr."' and rec_loesch=0) anz,
				(select 
				nvl(sum(rek_betrag),0) from rechnungen,(select rek_rnr,rek_betrag from rechnungen_konto where rek_loesch=0)
				where rec_rnr=rek_rnr and rec_kunden_nr='".$strKundennr."'
				and rec_loesch=0) rechnungen,
				(select 
				nvl(sum(mag_betrag),0) from rechnungen,(select mag_betrag,mag_rnr from mahngebuehren where mag_loesch=0)
				where rec_rnr=mag_rnr and rec_kunden_nr='".$strKundennr."'
				and rec_loesch=0) mahnungen
				from dual
				)";
		
		
		$arrGesamtzahlen=abfrage("ora",$strSql);
		
		$strGesamtbetrag=eurobetrag(feldwert($arrGesamtzahlen,"GESAMT"));		//entspricht dem Gesamtbetrag aller Rechnungen (inkl. Mahnungen) bezogen auf die Kundennr
		$dblMahnungsbetrag=feldwert($arrGesamtzahlen,"MAHNUNGEN");
		$strMahnungsbetrag=eurobetrag($dblMahnungsbetrag);
		
		include("card/css/css_card_master.php");	//Style-Sheets
		?>
		<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
		<script language="JavaScript">
			
		//Wird aufgerufen, wenn im Listenfeld mit den Mahndaten (id="abrechnungszeitraum") eine Auswahl getroffen wird
		
		function wahlmahndatum(id,kundennr)
		{
			<?php
			if($intZeitmessung)
			{
			?>
				parent.card_ladezeit.sendezeit('Mahngeb�hren aufrufen');	//Zeitmessung
			<?php } ?>
			
			var obj=document.getElementById(id);
			var datum=obj.options[obj.selectedIndex].text;
			
			obj.selectedIndex = 0;	//Auswahlliste auf 1. Eintrag zur�cksetzen
								
			var parameter = "intDialog=1&strAbrDatum="+datum+"&strKundennr="+kundennr;		//Parameter, welche an Dialogfenster �bergeben werden
			
			<?php echo dialog("mahngebuehren_frameset.php"); ?>		//�ffnet Dialogfenster
			
			if(rueckgabe!='undefined')
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr="+rueckgabe;		//Seite aufgrund Rnr von Mahngeb�hren aufbauen
			}
			else
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr=<?php echo $strRec_rnr; ?>";		//Seite neu aufbauen
			}
		}
		
		function restaenderung()		//Zus�tzlich zu der Funktion aenderung()
		{	
			//Alle Felder im IFrame "cardiframe" werden disabled gesetzt
			var obj=cardiframe.document.getElementsByName("grau");
			
			for(i=0;i<obj.length;i++)
			{
				obj[i].disabled = true;
			}
			
			//Alle L�schbutton im IFrame "cardiframe" werden ausgeblendet
			
			var obj=cardiframe.document.getElementsByName("loeschen");
			
			for(i=0;i<obj.length;i++)
			{
				obj[i].style.visibility = "hidden";
			}
			
			cardiframe.document.getElementById('neu').setAttribute("onclick","");		//Funktion onclick auf Button neuer Dastensatz ausschalten
			document.getElementById('rechnungen').setAttribute("onclick","");			//Funktion onclick Schriftzug "Rechnungen:" ausschalten
			
			document.getElementById('but_'+window.event.srcElement.id).style.visibility='visible';			//Button wird sichtbar
			//parent.cardnavigation.document.getElementById('speichern').style.visibility='visible';		//Speicherbutton in Navigationsleiste wird sichtbar		
		}
		//Wenn �nderungen in den Mahndatenfeldern gesendet werden
			
		function mahnsend()
		{
			var obj1 = window.event.srcElement.id.slice(4);
			var obj2 = document.getElementById(obj1);
			
			if(datumspruefung(obj2))
			{
				<?php
				if($intZeitmessung)
				{
				?>
					parent.card_ladezeit.sendezeit("Mahndatum �ndern");	//Zeitmessung
				<?php } ?>
				
				location.href="<?php echo $PHP_SELF."?strRec_rnr=".$strRec_rnr; ?>&strRec_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&aendfeldname="+obj1+"&aendfeldwert="+obj2.value;
			}
		}
		
		//Wenn �nderungen in der Textarea Bemerkungen gesendet werden
		
		function bemsend()
		{
			var obj1 = window.event.srcElement.id.slice(4);
			var obj2 = document.getElementById(obj1);
			
			var wert = textdbformat(obj2.value);
			<?php
			if($intZeitmessung)
			{
			?>
				parent.card_ladezeit.sendezeit("Bemerkungen �ndern");	//Zeitmessung
			<?php } ?>
			
			location.href="<?php echo $PHP_SELF."?strRec_rnr=".$strRec_rnr; ?>&strRec_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&aendfeldname="+obj1+"&aendfeldwert="+escape(wert);
		}
				
		function init()		//Aufruf �ber <body onload="init();">
		{
			document.ondblclick = zoom;
			<?php
			if($intZeitmessung)
			{
			?>
				parent.card_ladezeit.endezeit();	//Ladezeitmessung
			<?php } ?>
		}
		
		//Wird aufgerufen, wenn auf den Schriftzug "Rechnungen:" geklickt wird, soll Fenster mit allen Rechnungen zur
		//aktuellen Kundennr aufrufen
		
		function rechnungen()
		{
			<?php
			if($intZeitmessung)
			{
			?>
				top.card_ladezeit.sendezeit('Liste Rechnungen anzeigen');
			<?php } ?>
			
			var parameter = "strKundennr=<?php echo $strKundennr; ?>";		//Parameter, welche an Dialogfenster �bergeben werden
			
			<?php echo dialog("rechnungen_frameset.php"); ?>		//�ffnet Dialogfenster
			
			if(rueckgabe!='undefined')
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr="+rueckgabe;
			}
		}
		
		//Wird aufgerufen, wenn auf den Schriftzug "Mahngeb�hren:" geklickt wird
		
		function mahnungen()
		{
			if(document.getElementById("mahnungengesamt").style.display=='none')
			{
				//alert(unescape('Mahngeb%FChren f%FCr Kunden-Nr. %27<?php echo $strKundennr;?>%27\nwerden angezeigt.'));
				document.getElementById("mahnungen").title=unescape('Mahngeb%FChren f%FCr Kunden-Nr. %27<?php echo $strKundennr;?>%27');
				document.getElementById("mahnungengesamt").style.display='inline';
				document.getElementById("mahnungeneinzeln").style.display='none';
				document.getElementById("abrechnungszeitraum").style.display='none';
			}
			else
			{
				//alert(unescape('Mahngeb%FChren f%FCr Rechnungs-Nr. %27<?php echo $strRec_rnr;?>%27\nwerden angezeigt.'));
				document.getElementById("mahnungen").title=unescape('Mahngeb%FChren f%FCr Rechnungs-Nr. %27<?php echo $strRec_rnr;?>%27');
				document.getElementById("mahnungengesamt").style.display='none';
				document.getElementById("mahnungeneinzeln").style.display='inline';
				
			}
		}
		
		//Wird bei Klick auf Feld Mahngeb�hren (Summenanzeige) aufgerufen
		
		function mahnzeitraeume()
		{
			if(document.getElementById("abrechnungszeitraum").style.display=='none')
			{
				//document.getElementById("mahnungen").title=unescape('Mahngeb%FChren f%FCr aktuelle Rechnungs-Nr. %27<?php echo $strRec_rnr;?>%27 anzeigen');
				document.getElementById("abrechnungszeitraum").style.display='inline';
				document.getElementById("mahnungengesamt").style.display='none';
			}
			else
			{
				//document.getElementById("mahnungen").title=unescape('Mahngeb%FChren f%FCr aktuelle Kunden-Nr. %27<?php echo $strKundennr;?>%27 anzeigen');
				document.getElementById("abrechnungszeitraum").style.display='none';
				document.getElementById("mahnungengesamt").style.display='inline';
			}
		}
		
		//Bei Klick auf Kontostand:
		
		function rechnungenOMG()
		{
			if(document.getElementById("kontostandohne").style.display=='none')
			{
				document.getElementById("kontostand").title=unescape('Kontostand (ohne <?php echo $strMG; ?> Mahngeb%FChren)');
				document.getElementById("kontostandohne").style.display='inline';
				document.getElementById("kontostandmit").style.display='none';
			}
			else
			{
				document.getElementById("kontostand").title=unescape('Kontostand (inkl. <?php echo $strMG; ?> Mahngeb%FChren)');
				document.getElementById("kontostandohne").style.display='none';
				document.getElementById("kontostandmit").style.display='inline';
			}
		}
		
		//Wird bei Klick auf Feld Mahngeb�hren (Einzelanzeige) aufgerufen
		
		function mahnaktrechnung()
		{
			<?php
			if($intZeitmessung)
			{
			?>
				parent.card_ladezeit.sendezeit('Mahngeb�hren aufrufen');	//Zeitmessung
			<?php } ?>
						
			var parameter = "intDialog=1&strAbrDatum=<?php echo $strAbrZeitraum;?>&strKundennr=<?php echo $strKundennr;?>";		//Parameter, welche an Dialogfenster �bergeben werden
						
			<?php echo dialog("mahngebuehren_frameset.php"); ?>		//�ffnet Dialogfenster
			
			if(rueckgabe!='undefined')
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr="+rueckgabe;		//Seite aufgrund Rnr von Mahngeb�hren aufbauen
			}
			else
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr=<?php echo $strRec_rnr; ?>";		//Seite neu aufbauen
			}
		}
			
		</script>
		
		
		
		</head>
		<body onload="init();">
		<input type="hidden" id="Rec_rnr" value="<?php echo $strRec_rnr; ?>">
		<table cellpadding="0" cellspacing="0" align="center" id="tabelleaussen">
		<tr><td>
			<table cellpadding="0" cellspacing="0">
				<colgroup>
        			<col class="hpt_bes">
					<col class="hpt_adrdatz">
					<col class="hpt_abst">
					<col class="hpt_mahng">
					<col class="hpt_datz">
					<col class="hpt_button">
				</colgroup>
			<tr>
			<td class="hpt_bes">Kunden-Nr:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo $strKundennr; ?>" readonly></td>
			<td rowspan="6" class="hpt_abst">&nbsp;</td>
			<td class="hpt_mahng">1. Mahnung:</td><td class="text"><input type="text" name="grau" class="hpt_dat" id="rec_mahnung1am" maxlength="8" <?php //echo "onkeyup=\"aenderung('restaenderung');\"";?> value="<?php echo feldwert($arrRechnungen,"REC_MAHNUNG1AM"); ?>" readonly>
			<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="hpt_speibut" id="but_rec_mahnung1am" alt="speichern (Alt+S)" onclick="mahnsend();"></td>
			</tr>
			<tr>
			<td class="hpt_bes">Firma:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo feldwert($arrAdressen,"Firma"); ?>" readonly></td>
			<td class="hpt_mahng">2. Mahnung:</td><td class="text"><input type="text" name="grau" class="hpt_dat" id="rec_mahnung2am" maxlength="8" <?php //echo "onkeyup=\"aenderung('restaenderung');\"";?> value="<?php echo feldwert($arrRechnungen,"REC_MAHNUNG2AM"); ?>" readonly></td>
			<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="hpt_speibut" id="but_rec_mahnung2am" alt="speichern (Alt+S)" onclick="mahnsend();"></td>
			</tr>
			<tr>
			<td class="hpt_bes" style="width:50px;">Name/Vorname:</td><td class="text">
			<input type="text" name="grau" class="hpt_adrdat" value="<?php echo feldwert($arrAdressen,"Name")." ".feldwert($arrAdressen,"Vorname"); ?>" readonly></td>
			<td class="hpt_mahng">3. Mahnung:</td><td class="text"><input type="text" name="grau" class="hpt_dat" id="rec_mahnung3am" maxlength="8" <?php //echo "onkeyup=\"aenderung('restaenderung');\"";?> value="<?php echo feldwert($arrRechnungen,"REC_MAHNUNG3AM"); ?>" readonly></td>
			<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="hpt_speibut" id="but_rec_mahnung3am" alt="speichern (Alt+S)" onclick="mahnsend();"></td>
			</tr>
			<tr>
			<td class="hpt_bes">Adresse:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo feldwert($arrAdressen,"Strasse"); ?>" readonly></td>
			<td class="hpt_mahng">Zahlungsziel:</td><td class="text"><input type="text" name="grau" class="hpt_dat" id="rec_zahlungsziel" maxlength="8" onkeyup="aenderung('restaenderung');" value="<?php echo feldwert($arrRechnungen,"REC_ZAHLUNGSZIEL"); ?>"></td>
			<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="hpt_speibut" id="but_rec_zahlungsziel" alt="speichern (Alt+S)" onclick="mahnsend();"></td>
			</tr>
			<tr>
			<td class="hpt_bes">PLZ/Ort:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo feldwert($arrAdressen,"PLZ")." ".feldwert($arrAdressen,"Ort"); ?>" readonly></td>
			<td class="hpt_mahng">
			
			<?php
				if($dblMG==0)
				{
					echo "<span title=\"Kontostand (keine Mahngeb&uuml;hren)\">Kontostand:</span>";
					echo "</td><td colspan=\"2\" class=\"text\"><input type=\"text\" name=\"grau\" class=\"hpt_bet\" value=\"".$strKontostand."\" readonly></td>";
				}
				else
				{
					echo "<span id=\"kontostand\" title=\"Kontostand (inkl. ".$strMG.
					" Mahngeb&uuml;hren)\" onclick=\"rechnungenOMG();\" style=\"cursor: hand;\">Kontostand:</span>";
					echo "</td><td colspan=\"2\" class=\"text\">";
					echo "<input type=\"text\" id=\"kontostandmit\" name=\"grau\" class=\"hpt_bet\" value=\"".$strKontostand."\"";
					if($dblKontostand!=0)
						echo " style=\"color:red;\"";
					echo " readonly>";
					echo "<input type=\"text\" id=\"kontostandohne\" name=\"grau\" class=\"hpt_bet\" value=\"".$strKontostandOMG.
					"\" style=\"display:none;";
					if($dblKontostandOMG!=0)
						echo "color:red;";
					echo "\" readonly>";
					echo "</td>";
					
				}
			?>
			
			
			
			</tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			<td class="hpt_mahng">
			<?php
			if($arrAbrechnungszeitraum["zeilen"]>0)
			{
				if($dblMahnungsbetrag!=0)
					$strMahnfarbe=" color:red;";

				echo "<span id=\"mahnungen\" title=\"Mahngeb&uuml;hren f&uuml;r Rechnungs-Nr. '".$strRec_rnr.
				"'\" onclick=\"mahnungen();\" style=\"cursor: hand;".$strMahnfarbe."\">Mahngeb&uuml;hren:</span>";
				
				//echo "<span id=\"mahnungen\" title=\"Mahngeb&uuml;hren f&uuml;r aktuelle Kunden-Nr. '".$strKundennr."' anzeigen\" onclick=\"mahnungen();\" style=\"cursor: hand;".$strMahnfarbe."\">Mahngeb&uuml;hren:</span>";
			}
			else
			{
				echo "<span title=\"Mahngeb&uuml;hren f&uuml;r aktuelle Rechnungs-Nr. '".$strRec_rnr."'\">Mahngeb&uuml;hren:</span>";
			}
			?>
			</td>
			<td colspan="2" class="text">
			<?php
			
			echo "<input type=\"text\" id=\"mahnungeneinzeln\" name=\"grau\" class=\"hpt_bet\" value=\"".$strMG."\"";
			if($intMGAnz)
			{
				echo " style=\"cursor:hand;";
				if($dblMG!=0)
					echo "color:red;";
				echo "\" onclick=\"mahnaktrechnung();\" title=\"Buchungen Mahngeb&uuml;hren anzeigen\"";
			}
			
			echo " readonly>";
			
			if($arrAbrechnungszeitraum["zeilen"]>0)
			{
				echo "<input type=\"text\" id=\"mahnungengesamt\" name=\"grau\" class=\"hpt_bet\" value=\"".$strMahnungsbetrag.
				"\" style=\"display:none;cursor:hand;";
				if($dblMahnungsbetrag!=0)
					echo "color:red;";
				echo "\" onclick=\"mahnzeitraeume();\" title=\"Abrechnungszeitr&auml;ume Mahngeb&uuml;hren anzeigen\" readonly>";
			}
			
			
			//Falls f�r aktuelle Kunden-Nr Mahngeb�hren vorliegen, werden die Abrechnungsdaten, wo Mahngeb�hren angefallen sind,
			//angegeben: rot: Mahngeb�hr ist noch offen, schwarz: Mahngeb�hren sind bezahlt worden
			
			if($arrAbrechnungszeitraum["zeilen"]>0)
			{
				$strOptionen = "";
				$intMahngeb = 0;
				
				for($i=0;$i<count($arrAbrechnungszeitraum["trefferarray"]["DATUM"]);$i++)
				{
					if($arrAbrechnungszeitraum["trefferarray"]["SUMME"][$i]==0)
					{
						$strOptionen.="<option>".$arrAbrechnungszeitraum["trefferarray"]["DATUM"][$i]."</option>";
					}
					else
					{
						$strOptionen.="<option style=\"color:red;\">".$arrAbrechnungszeitraum["trefferarray"]["DATUM"][$i]."</option>";
						$intMahngeb = 1;
					}
				}
				
				echo "<select  name=\"grau\" id=\"abrechnungszeitraum\" class=\"hpt_bet\" style=\"display:none;\" onchange=\"wahlmahndatum(this.id,'".$strKundennr."');\">";
				
				if($intMahngeb)		//Wenn Mahngeb�hren f�llig sind ist 1. Eintrag "offen" ansonsten "bezahlt"
				{
					echo "<option style=\"color:red;\">offen</option>";
				}
				else
				{
					echo "<option>ausgeglichen</option>";
				}
				
				echo $strOptionen;
				
				echo "</select>";
			}
			?>
			</td>
			</tr>
			
			
			<tr><td colspan="6" class="hpt_zwischen">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-width:0px;">
				<tr><td class="hpt_zwischen" style="border-width:0px;">
				&nbsp;
				</td></tr>
				</table>
			</td></tr>
			<tr>
			<td class="hpt_bes">Kauf mit Pan:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo $strPan; ?>" readonly></td>
			<td>&nbsp;</td>
			<td class="hpt_mahng"><span id="rechnungen" title="Rechnungen anzeigen" onclick="rechnungen();" style="cursor: hand;">Rechnungen:</span></td><td colspan="2" class="text"><input type="text" name="grau" class="hpt_bet" value="<?php echo feldwert($arrGesamtzahlen,"ANZ"); ?>" readonly></td>
			</tr>
			<tr>
			<td class="hpt_bes">Eink&auml;ufer:</td><td class="text"><input type="text" name="grau" class="hpt_adrdat" value="<?php echo feldwert($arrAdressenEinkaeufer,"Name"); ?>" readonly></td>
			<td>&nbsp;</td>
			<td class="hpt_mahng">Gesamtsaldo:</td><td colspan="2" class="text"><input type="text" name="grau" class="hpt_bet" value="<?php echo $strGesamtbetrag; ?>" readonly></td>
			</tr>
			<tr><td colspan="6" class="hpt_zwischen">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-width:0px;">
				<tr><td class="hpt_zwischen" style="border-width:0px;">
				&nbsp;
				</td></tr>
				</table>
			</td></tr>
			<tr>
			<td colspan="6">
				<table border="0" cellpadding="0" cellspacing="0" align="center" style="margin:0px;border-width:0px" width="100%">
				<tr>
					<td class="bes" id="bes1">Rechnungs-Nr:</td>
					<td class="bes" id="bes3">Datum:</td>
					<td class="bes" id="bes2">Betrag:</td>
					<td class="bes" id="bes4">Buchungsart:</td>
					<td class="bes" id="bes5">Kontoauszug:</td>
				</tr>
				</table>
			</td>
			</tr>
			<tr><td colspan="6" class="hpt_zwischen">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-width:0px;">
				<tr><td class="hpt_zwischen" style="border-width:0px;">
				&nbsp;
				</td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr>
		<tr><td>
		<?php
		$strWeitereParam = "";
		if(isset($intSeq))
			$strWeitereParam.="&intSeq=".$intSeq;
		if(isset($intOhneKaz))
			$strWeitereParam.="&intOhneKaz=".$intOhneKaz;
		?>
		
		<iframe src="card_iframe.php?strRec_rnr=<?php echo $strRec_rnr.$strWeitereParam; ?>" name="cardiframe" width="100%" height="<?php echo $intIfr_hoehe;?>" align="left"
	   scrolling="auto" marginheight="10" marginwidth="0" frameborder="0"></iframe>
		</iframe>
	
		</td></tr>
		<tr><td class="hpt_zwischen">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr><td colspan="6" class="hpt_zwischen">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-width:0px;">
				<tr><td class="hpt_zwischen" style="border-width:0px;">
				&nbsp;
				</td></tr>
				</table>
			</td></tr>
			
			</table>
			
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<td class="hpt_bes" style="vertical-align:top;">Bemerkungen:</td>
			<td class="text" id="td_rec_bemerkungen"><textarea name="grau" id="rec_bemerkungen" onkeyup="aenderung('restaenderung');"><?php echo $arrRechnungen["trefferarray"]["REC_BEMERKUNGEN"][0]?></textarea></td>
			<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="hpt_speibut" id="but_rec_bemerkungen" alt="speichern (Alt+S)" onclick="bemsend();"></td>
			</tr>
			</table>
		</td></tr>
		</table>		
		<?php	
	}
	elseif($arrRechnungen["zeilen"]>1)		//Zur angegebenen Rechnungsnr sind doppelte Datens�tze vorhanden
	{
		include("card/css/css_card_master.php");	//Style-Sheets
		
		
		?>
		<script language="Javascript">
		function doppeltergnranzeigen()
		{
			<?php
			if($intZeitmessung)
			{
			?>
				top.card_ladezeit.sendezeit('Liste doppelte Rechnungsnr');
			<?php } ?>
						
			var parameter = "strRec_rnr=<?php echo $strRec_rnr; ?>";		//Parameter, welche an Dialogfenster �bergeben werden
			
			<?php echo dialog("doppelrgnr_frameset.php"); ?>		//�ffnet Dialogfenster
			
			if(rueckgabe!='undefined')
			{
				location.href="<?php echo $PHP_SELF; ?>?strRec_rnr="+rueckgabe;
			}
		}
		</script>
		
		
		</head><body onload="doppeltergnranzeigen();">
		
		
		<?php
	}
	else				//Zur angegebenen Rechnungsnr sind keine Datens�tze vorhanden
	{
		include("card/css/css_card_master.php");	//Style-Sheets
		
		echo "</head><body".($intZeitmessung?" onload=\"top.card_ladezeit.endezeit();\"":"").">";
		echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"top\">";
		echo "<br><span class=\"titel\">Die gesuchte Rechnungsnummer<br>".$strRec_rnr."<br>existiert nicht!</span>";
		echo "</td></tr></table>";
	}

}
else
{
	include("card/css/css_card_master.php");	//Style-Sheets
	
	?></head><?php
	
	if($intImport)	//Klick auf Import-Button in Navigationsleiste ist erfolgt  / Pr�fen ob aktuelles Datum Abrechnungsdatum ist
	{
		$datAktuellesDatum = strftime("%d.%m.%y");
		//$datAktuellesDatum = '27.12.02';
		
		//Abfrage ob Datens�tz mit aktellem Abrechnungsdatum vorhanden sind
		
		$strSql='select count(*) anzahl from '.$tabKalender.' where "Datum"=\''.db2datum($datAktuellesDatum).'\'';
					
		$arrRueck=abfrage("db2",$strSql);
		
		echo "<body onload=\"".($intZeitmessung?"top.card_ladezeit.endezeit();":"");
		
		if(feldwert($arrRueck,"ANZAHL")==0)
		{
			echo "alert('Das heutige Datum ".$datAktuellesDatum." ist kein Abrechnungstag!');";
		}
		echo "\">";
	}
	else
	{
		echo "<body".($intZeitmessung?" onload=\"try{top.card_ladezeit.endezeit();}catch(e){}\"":"").">";
	}
	?>
	<table cellspacing="0" cellpadding="0" width="100%" height="100%"><tr><td align="center" valign="middle">
	<table cellpadding="0" cellspacing="0" align="center" id="tabelleaussen">
	<tr><td align="center">
	<?php
	if($intImport||isset($datAbrZeitraum))		//Import wurde aufgerufen
	{
		if($intImport)	//Klick auf Import-Button in Navigationsleiste ist erfolgt
		{
			?>
			<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
			<script language="JavaScript">
			
			function start_import()
			{
				var obj = document.getElementById("datAbrZeitraum");
				
				if(datumspruefung(obj))
				{
					<?php
					if($intZeitmessung)
					{
					?>
						parent.card_ladezeit.sendezeit("Import starten (Rechnungsimport)");	//Zeitmessung
					<?php } ?>
					
					self.location.href="<?php echo $PHP_SELF; ?>?datAbrZeitraum="+obj.value;
				}
			}
			</script>
			<span class="titel">Rechnungsimport mit folgendem Abrechnungsdatum starten: </span>
			<input type="text" id="datAbrZeitraum" maxlength="8" class="hpt_dat" value="<?php echo $datAktuellesDatum; ?>"><br><br>
			<button type="button" onclick="start_import();">Import starten</button>
			<?php
		}
	
		if(isset($datAbrZeitraum))
		{
			//Datens�tze (Zahlungsart 20 sowie aktuelles Abrechnungsdatum) aus db2 in Array $arrSatz holen
			//Felder, welche gebraucht werden: Datum (Abrechnungsdatum), Rg-Nr, PAN, Betrag, Kdnr, Einkaeufer (Name, Vorname)
			
			
			// round(decimal("Betrag",11,3),2) => wandelt Datentyp "double" des Feldes "Betrag" in Datentyp "decimal" (8 Vorkomma-, 3 Nachkommastellen) um und rundet
			// den Wert auf 2 Stellen nach dem Komma
			
			$strSql='select "Datum","Position",round(decimal("Betrag",11,3),2) as "Betrag","Kunden_Nr","PAN","Rechnungs_Nr","VerwZweck","Kontoinhaber" as "Einkaeufer"
			from '.$tabDTA_Positionen.' where "Datum"=\''.db2datum($datAbrZeitraum).'\' and
			"Zahlungs_Art"=\'20\'';
		 	
			$arrRueck=abfrage("db2",$strSql);
			
			$arrSatz = $arrRueck["trefferarray"];
			$intZeilenSatz = $arrRueck["zeilen"];
			
			if($intZeilenSatz>0)
			{
				//Tabelle import_dta_positionen leeren
				
				$strSql="truncate table import_dta_positionen";
				
				$arrRueck = abfrage("ora",$strSql);
				
				//Datens�tze aus $arrSatz in oracle in Tabelle import_dta_positionen einf�gen
				
				for($i=0;$i<$intZeilenSatz;$i++)
				{
					//Ermitteln ob Datensatz in der Tabelle SONDERKONDITIONEN_ZAHLUNGSZIEL existiert. Wenn ja wird Zahlungsfrist statt Standard 21 Tage
					//aus dem Feld SKZ_ZAHLUNGSZIEL_IN_TAGEN �bernommen
					
					$strSql="select SKZ_ZAHLUNGSZIEL_IN_TAGEN from SONDERKONDITIONEN_ZAHLUNGSZIEL where SKZ_LOESCH=0 and SKZ_KUNDEN_NR='".$arrSatz["Kunden_Nr"][$i]."'";
					$arrZahlungsziel=abfrage("ora",$strSql);
					
					$intZahlungsziel=$intStandardZahlungsziel;
					
					if($arrZahlungsziel["zeilen"])
					{
						$intZahlungsziel=$arrZahlungsziel["trefferarray"]["SKZ_ZAHLUNGSZIEL_IN_TAGEN"][0];
					}
					
					// $arrSatz["Betrag"][$i] liefert Betrag mit , (z.B. 12567,89) und muss deshalb mit str_replace(",",".",$arrSatz["Betrag"][$i])
					// umgewandelt werden (z.B. in 12567.89)
				
					$strSql="insert into import_dta_positionen(IDP_DATUM,IDP_POSITION,IDP_BETRAG,IDP_KUNDEN_NR,IDP_PAN,
					IDP_RECHNUNGS_NR,IDP_VERWZWECK,IDP_IMPORTDATUM,IDP_BENUTZER,IDP_EINKAEUFER,IDP_ZAHLUNGSZIEL) 
					values ('".db2datumzurueck($arrSatz["Datum"][$i])."','".$arrSatz["Position"][$i]."',".
					str_replace(",",".",$arrSatz["Betrag"][$i]).",'".$arrSatz["Kunden_Nr"][$i]."','".$arrSatz["PAN"][$i]."','".$arrSatz["Rechnungs_Nr"][$i]."',
					'".$arrSatz["VerwZweck"][$i]."',sysdate,'".$PHP_AUTH_USER."','".$arrSatz["Einkaeufer"][$i]."',
					(to_date('".db2datumzurueck($arrSatz["Datum"][$i])."','DD.MM.YY')+".$intZahlungsziel."))";
					
					abfrage("ora",$strSql);
				}
				
				//Anzahl importierter Datens�tze sowie Betragssumme ermitteln
				
				$strSql="select count(IDP_DATUM) anzahl, sum(IDP_BETRAG) summe from import_dta_positionen";
				
				$arrRueck = abfrage("ora",$strSql);
				
				if($intZeilenSatz == feldwert($arrRueck,"ANZAHL"))  //Anzahl ausgew�hlter Zeilen aus db2 stimmt mit Anzahl in import_dta_positionen �berein
				{
					?>
					<span class="titel">Import in Tabelle IMPORT_DTA_POSITIONEN</span><br><br>
					<?php
				
					echo "Anzahl: <span class=\"titel\">".feldwert($arrRueck,"ANZAHL")."</span><br>";
					echo "Summe: <span class=\"titel\">".eurobetrag(feldwert($arrRueck,"SUMME"))."</span>";
					?>
					<br><br><button type="button" onclick="self.location.href='<?php echo $PHP_SELF; ?>?intDatTransfer=1';">Rechnungen buchen</button>
					<?php
				}
				else
				{
					?>
					<br><span class="titel">Fehler beim Import in Tabelle IMPORT_DTA_POSITIONEN!</span><br><br>
					<button type="button" onclick="self.location.href='<?php echo $PHP_SELF; ?>?intImport=1';">Vorgang wiederholen</button>
					<?php
				}
			}
			else	//keine Datens�tze am f�r angegebenes Abrechnungsdatum vorhanden
			{
				?>
				<span class="titel">F&uuml;r das angegebene Abrechnungsdatum <?php echo $datAbrZeitraum;?><br>sind keine Datens&auml;tze vorhanden!</span>
				<?php
			}
		}
	}
	elseif(isset($intDatTransfer))
	{
		
		$strProz="RECHNUNGS_IMPORT ( :str_rueckgabe )";		//Name der aufzurufenden Oracle-Prozedur
		$arrParameter=array("str_rueckgabe"=>500);			//Array mit Bind-Variablen
		$arrRueck=oracleprozedur($strProz,$arrParameter);	//Oracle-Prozedur aufrufen, R�ckgabewerte werden auf ass. Array zugewiesen
	
		echo $arrRueck["str_rueckgabe"];
	}
	else
	{
		?>
		<span class="titel">Herzlich Willkommen im RLS!</span>
		<?php
	}
	?>
	</td></tr></table>
	</td></tr></table>
	<?php
}

include("card/card_zeitende.inc.php");			// Zeitmessung
?>
</body>
</html>
