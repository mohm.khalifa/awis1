<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $_GET;				// Neue Variablen
global $_POST;
global $cmdAktion;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1300);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Personaleins�tze',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if (isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}

awis_RegisterErstellen(1300, $con, $cmdAktion);


print "<br><hr><input type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "&nbsp;<input type=image title='Hilfe (Alt+h)' alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=perseins&HilfeBereich=" . $cmdAktion . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);

?>
</body>
</html>

