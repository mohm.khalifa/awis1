<?php
// Variablen
global $awisRSZeilen;
global $con;
global $AWISBenutzer;


// lokale Variablen
$filSuche = '';		//Parameter der Anwender
$KatRechte = "0" . awisBenutzerRecht($con,151);

// Ende Variablen

echo "<br>";

echo "<form name=frmSuche method=post action=./telefon_Main.php?cmdAktion=Liste>";

echo "<table id=SuchMaske>";

	// Alle Parameter auslesen
	//	Es wird aber nicht alles eingebelndet!
$TelSuche = explode(";",awis_BenutzerParameter($con, "TelefonSuche", $AWISBenutzer->BenutzerName()));

/**********************************************
* * Eingabemaske
***********************************************/

echo "<table width=600 border=1>";

echo "<tr><td width=180>S<u>u</u>che:";

echo "</td>";
echo "<td><input type=text name=txtSuche accesskey='u' size=20 tabindex=1></td>";
echo "</tr>";

echo "<tr><td class=FeldBez colspan=2>Detailsuche</tr>";


// Zeile 1
echo "<tr><td width=180>Na<u>m</u>e:</td>";
echo "<td><input type=text name=txtName accesskey='m' size=20 tabindex=1>";
echo "&nbsp;<input type=checkbox name=txtAehnlicheSuchen value=on tabindex=2> �hnliche Namen suchen</td>";

echo "</tr>";

// Zeile 1a
echo "<tr><td width=180>Kontakt (Tel, Fax,..):</td>";
echo "<td>";
echo "<input type=text name=txtKontakt size=30 tabindex=3>";
echo "&nbsp;<select name=txtKontaktTyp accesskey='k' size=1 tabindex=4>";
echo "<option value=0>Alle</option>";

$rsKOT=awisOpenRecordset($con,"SELECT * FROM KOMMUNIKATIONSTYPEN WHERE BITAND(KOT_KZG_ID,$KatRechte)>0 ORDER BY KOT_TYP");
$rsKOTZeilen = $awisRSZeilen;
for($i=0;$i<$rsKOTZeilen;$i++)
{
	echo "<option value=" . $rsKOT["KOT_KEY"][$i] . ">" . $rsKOT["KOT_TYP"][$i] . "</option>";
}
echo "</select>";

echo "";

echo "</td></tr>";

// Zeile2
echo "<tr><td width=180><u>K</u>FZ-Kennzeichen:</td>";
echo "<td><input type=text name=txtKennzeichen accesskey='k' size=20 tabindex=5>";
echo "Typ: <input type=text name=txtFahrzeugTyp size=20 tabindex=6></td>";
echo "</tr>";

// Zeile3
echo "<tr><td width=180>K<u>a</u>tegorie:</td>";
echo "<td><select name=txtKategorie accesskey='a' size=1 tabindex=10>";
echo "<option value=0>Alle</option>";

$rsKategorien=awisOpenRecordset($con,"SELECT * FROM KontakteKategorien WHERE BITAND(KKA_KZG_ID,$KatRechte)=KKA_KZG_ID ORDER BY KKA_BEZEICHNUNG");
$rsKategorienZeilen = $awisRSZeilen;

for($i=0;$i<$rsKategorienZeilen;$i++)
{
	echo "<option value=" . $rsKategorien["KKA_KEY"][$i] . ">" . $rsKategorien["KKA_BEZEICHNUNG"][$i] . "</option>";
}

echo "</select>";
unset($rsKategorien);		// Speicher freigeben

// Zeile3a

$rsBereiche=awisOpenRecordset($con,"SELECT * FROM KontakteBereiche WHERE BITAND(KBE_KZG_ID,$KatRechte)=KBE_KZG_ID ORDER BY KBE_BEZEICHNUNG");
$rsBereicheZeilen = $awisRSZeilen;

echo "<tr><td width=180>B<u>e</u>reich:</td>";
echo "<td><select name=txtBereich accesskey='e' size=1 tabindex=10>";
echo "<option value=-1 selected>Alle</option>";

for($i=0;$i<$rsBereicheZeilen;$i++)
{
	echo "<option value=" . $rsBereiche["KBE_KEY"][$i] . ">" . $rsBereiche["KBE_BEZEICHNUNG"][$i] . "</option>";
}
echo "</select>";
unset($rsBereiche);		// Speicher freigeben

// Zeile4
echo "<tr><td width=180>ATU A<u>b</u>teilung:</td>";
echo "<td><select name=txtAbteilung accesskey='b' size=1 tabindex=10>";
echo "<option value=-1>Alle</option>";

$rsAbteilungen=awisOpenRecordset($con,"SELECT * FROM KontakteAbteilungen WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID ORDER BY KAB_ABTEILUNG");
$rsAbteilungenZeilen = $awisRSZeilen;
for($i=0;$i<$rsAbteilungenZeilen;$i++)
{
	echo "<option value=" . $rsAbteilungen["KAB_KEY"][$i] . ">" . $rsAbteilungen["KAB_ABTEILUNG"][$i] . "</option>";
}
echo "</select>";
unset($rsAbteilungen);

// Zeile5
echo "<tr><td width=180>Zust�n<u>d</u>igkeit:</td>";
echo "<td><input type=text name=txtZustaendigkeit accesskey='d' size=40 tabindex=15 " . $TelSuche[1] . "></td>";
echo "</tr>";

// Zeile6
echo "<tr><td width=180>ATU <u>G</u>eb�ude:</td>";
echo "<td><select name=txtGebaude accesskey='g' size=1 tabindex=20>";
echo "<option value=0>Alle</option>";

$rsGebaeude=awisOpenRecordset($con,"SELECT * FROM Gebaeude ORDER BY GEB_BEZEICHNUNG");
$rsGebaeudeZeilen = $awisRSZeilen;
for($i=0;$i<$rsGebaeudeZeilen;$i++)
{
	echo "<option value=" . $rsGebaeude["GEB_ID"][$i] . ">" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . "</option>";
}
echo "</select></tr>";

echo "<tr><td width=180>Bemerkung:</td>";
echo "<td><input type=text name=txtBemerkung size=40 tabindex=25></td>";
echo "</tr>";

$AenderungsRecht = awisBenutzerRecht($con,152);
if((intval($AenderungsRecht)&16384)==16384)	// Personalnummer nur f�r den Empfang
{
	echo "<tr><td width=180>Kontakte ohne Systembenutzer:</td>";
	echo "<td><input type=checkbox name=txtOhneSystem value=on tabindex=26></td>";
	echo "</tr>";

    echo "<tr><td width=180>Alle Status anzeigen:</td>";
    echo "<td><input type=checkbox name=txtAlleStatus value=on tabindex=26></td>";
    echo "</tr>";
}

echo "</table>";

echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png name=cmdSuche value=\"Aktualisieren\">";
//echo "<input tabindex=99 type=submit alt=\"Zur�cksetzen (Alt+r)\" accesskey=r value=Reset>";
//echo "<input type=hidden name=cmdAktion value=Liste>";

echo '<Script Language=JavaScript>';
echo "document.getElementsByName(\"txtSuche\")[0].focus();";
echo '</Script>';

?>

