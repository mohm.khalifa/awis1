<?php
global $AWIS_KEY1;

require_once('jpgraph/jpgraph_barcode.php');
require_once('register.inc.php');

$TextKonserven=array();
$TextKonserven[]=array('ADK','%');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Abbrechen');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','Filialbestand');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FehlerAbschluss');
$TextKonserven[]=array('Fehler','err_EingabeGroesserNull');
$TextKonserven[]=array('Wort','WirklichAbschliessen');
$TextKonserven[]=array('Wort','lbl_weiter');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$TXT_Abschliessen = $Form->LadeTexte($TextKonserven);

$Art= '';
$Hinweis = '';
$Form->DebugAusgabe(1,$_POST);
if(isset($_POST['cmd_Abschliessen_x']))
{	
	$Art = 'Filiale';
	
	$Key=$_POST['txtADK_KEY'];
	$ADKKey=$_POST['txtADK_KEY'];

	$AWIS_KEY1 = $Key;
		
	$SQL = 'SELECT *';
	$SQL .=' FROM ADRPOS ADP';
	$SQL .=' WHERE ADP_ADK_KEY='.intval($AWIS_KEY1);	
	
	$rsADP=$DB->RecordSetOeffnen($SQL);
	$Form->DebugAusgabe(1,$SQL);
	
	$Fehler='';
	if ($rsADP->AnzahlDatensaetze()==0)
	{				
		$Fehler = $TXT_Abschliessen['ADK']['HinweisKeineDaten'];
	}		
	
	// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($Fehler);
		$Form->ZeileEnde();
		$Speichern=false;				
		
		$Link='./adr_Main.php?cmdAktion=Details&ADK_KEY='.$AWIS_KEY1;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdADR',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();				
		die();						
	}
	else 
	{
		$Speichern = true;
	}
	
	
	$Felder=array();
	//$Felder[]=array($Form->LadeTextBaustein('ADK','ADK_FIL_ID'),$_POST['txtADK_FIL_ID']);
	$Felder[]=array($Form->LadeTextBaustein('ADK','ADK_VORGANGNR'),$_POST['txtADK_VORGANGNR']);	
	
}
elseif(isset($_POST['cmdAbschliessenOK']))	// Abschluss durchf�hren
{	
	$AWIS_KEY1 = $_POST['txtKey'];
	
	$SQL = '';
	switch ($_POST['txtArt'])
	{
		case 'Filiale':	

			//Pr�fen, ob die ADR-Abwicklung evtl. schon abgeschlossen ist
			$SQL = 'SELECT ADK_STATUS';
			$SQL .= ' FROM ADRKOPF';			
			$SQL .= ' WHERE ADK_key=' . $AWIS_KEY1 . '';
						
			$rsADK = $DB->RecordSetOeffnen($SQL);
			$Form->DebugAusgabe(1,$SQL);

			//if ($DB->FeldInhaltFormat('T',$rsADK->FeldInhalt('ADK_STATUS'))=='A')
			if ($rsADK->FeldInhalt('ADK_STATUS')=='A')
			{										
				$Form->ZeileStart();
				$Form->Hinweistext($TXT_Abschliessen['ADK']['ADRAbgeschlossenFiliale']);
				$Form->ZeileEnde();
				
				$Link='./adr_Main.php?cmdAktion=Details&ADK_KEY='.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();	
			}
													
			$SQL = 'UPDATE ADRKOPF';
			$SQL .= ' SET ADK_STATUS = \'A\'';			
			$SQL .= ', ADK_DATUMABSCHLUSS = SYSDATE';
			$SQL .= ', ADK_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', ADK_userdat=sysdate';
			$SQL .= ' WHERE ADK_KEY=0' . $AWIS_KEY1 . '';
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('adr_abschliessen_1',1,$awisDBError['messages'],'');
			}
			//$Form->DebugAusgabe(1,$SQL);									
			break;
		default:
			break;
	}
}
	
if($Art!='' and $Speichern == true)
{
	$Form->SchreibeHTMLCode('<form name=frmAbschliessen action=./adr_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
	$Form->Formular_Start();
	
	if ($Art == 'Filiale')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['Wort']['WirklichAbschliessen']);
		$Form->ZeileEnde();	
	}
	
	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}			
	
	$Form->Erstelle_HiddenFeld('ADK_Key',$ADKKey);
	$Form->Erstelle_HiddenFeld('Art',$Art);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdAbschliessenOK','','',$TXT_Abschliessen['Wort']['Ja'],'','');
	$Form->Schaltflaeche('submit','cmdAbschliessenAbbrechen','','',$TXT_Abschliessen['Wort']['Nein'],'');	
	$Form->ZeileEnde();
		

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>
	