<?php
global $Funktionen;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {

    if (isset($_POST['cmdSpeichern_x'])) {
        include 'grosskundenkarten_speichern.php';
    }

    if (isset($_POST['cmdLoeschen_x']) and isset($_POST['txtERM_NR'])) {
        $SQL = 'DELETE FROM EXPRABATTMODELLE WHERE ERM_NR = ' . $Funktionen->DB->WertSetzen('ERM', 'T', $_POST['txtERM_NR']);
        $Funktionen->DB->Ausfuehren($SQL, 'ERM', true, $Funktionen->DB->Bindevariablen('ERM', true));
    }

    if (isset($_GET['ERM_NR'])) {
        $AWIS_KEY1 = $_GET['ERM_NR'];
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
    }

    $SQL = 'SELECT ERM_NR, ERM_MODELLNAME , LISTAGG(ERD_BEZEICHNUNG || \' (WG \' || ERD_WG || \'): \' || ERM_RABATTSATZ || \'%\', \' / \') WITHIN GROUP (ORDER BY ERD_BEZEICHNUNG) AS WARENGRUPPEN 
            FROM EXPRABATTMODELLE INNER JOIN EXPRABATTDETAILS ON ERD_KEY = ERM_ERD_KEY';

    if ($AWIS_KEY1 != '') {
        $SQL .= ' WHERE ERM_NR = ' . $AWIS_KEY1;
    }

    $SQL .= ' GROUP BY ERM_NR, ERM_MODELLNAME';

    $rsERM = $Funktionen->DB->RecordSetOeffnen($SQL);

    if ($rsERM->AnzahlDatensaetze() == 1) {
        $AWIS_KEY1 = $rsERM->FeldInhalt('ERM_NR');
    }

    // Spaltenbreiten f�r Listenansicht
    $FeldBreiten = array();
    $FeldBreiten['AUFTRAG_ID'] = 130;
    $FeldBreiten['KNDNR'] = 90;
    $FeldBreiten['DATUM_KOMMISSIONIERUNG'] = 170;
    $FeldBreiten['REFERENZNR'] = 100;
    $FeldBreiten['ATU_NR'] = 110;
    $FeldBreiten['GEWICHT_NETTO'] = 110;
    $FeldBreiten['PACKSTKNR'] = 100;
    $FeldBreiten['TRACKING_NR'] = 250;
    $FeldBreiten['VERSANDART'] = 100;
    $FeldBreiten['SATZKENNUNG'] = 100;
    $FeldBreiten['MANDANT'] = 150;

    $Funktionen->Form->Formular_Start();
    $Funktionen->Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./grosskundenkarten_Main.php?cmdAktion=Modelle>");

    $Script = '<script>
            function setValue(id) { 
                if ($( "#" + id ).val()!="' . $Funktionen->AWISSprachkonserven["Wort"]["txt_BitteWaehlen"] . '") {
                    $( "#" + id ).parent().next().text($( "#txthiddenValues option[value="+$( "#" + id ).val()+"]" ).text());
                    $( "#txtERM_RABATTSATZ_" + id.slice(-1) ).prop("required",true);
                } else {
                    $( "#" + id ).parent().next().text("' . $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen'] . '");
                    $( "#txtERM_RABATTSATZ_" + id.slice(-1) ).removeAttr("required");
                }
            }
            </script>';

    $Funktionen->Form->SchreibeHTMLCode($Script);

    if (isset($AWIS_KEY1) and $AWIS_KEY1 != '') {
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./grosskundenkarten_Main.php?cmdAktion=Modelle accesskey=T title='" . $Funktionen->AWISSprachkonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Funktionen->Form->InfoZeile($Felder, '');

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERM_ID'], 80);
        $Funktionen->Form->Erstelle_TextLabel(($AWIS_KEY1 > 0?$AWIS_KEY1:$Funktionen->AWISSprachkonserven['ERM']['ERM_NEU']), 150, 'font-weight: bold;');
        $Funktionen->Form->Erstelle_HiddenFeld('ERM_NR', $AWIS_KEY1);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERM_MODELLNAME'], 80);
        $Funktionen->Form->Erstelle_TextFeld('ERM_MODELLNAME', $rsERM->FeldInhalt('ERM_MODELLNAME') ,240, 240, true);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERD_BEZEICHNUNG'], 200);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERD_WG'], 200);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_RABATTSATZ'], 200);
        $Funktionen->Form->ZeileEnde();

        $SQL = 'SELECT ERD_KEY, ERD_BEZEICHNUNG, ERD_WG, ERM_RABATTSATZ FROM EXPRABATTDETAILS 
                INNER JOIN EXPRABATTMODELLE ON ERM_ERD_KEY = ERD_KEY WHERE ERM_NR = ' . $AWIS_KEY1;
        $rsERD = $Funktionen->DB->RecordSetOeffnen($SQL);

        $DS = 2;
        $i = 1;
        while ($i <= 6) {
            $BezSQL = 'SELECT ERD_KEY, ERD_BEZEICHNUNG FROM EXPRABATTDETAILS';
            $WGSQL = 'SELECT ERD_KEY, ERD_WG FROM EXPRABATTDETAILS';
            $SatzSQL = 'SELECT EES_EANID, EES_EANID FROM EXPEANSWITCH';
            if (!$rsERD->EOF()) {
                $Funktionen->Form->ZeileStart();
                $Funktionen->Form->Erstelle_SelectFeld('ERD_KEY_' . $i, $rsERD->FeldInhalt('ERD_KEY'), 200, true, $BezSQL, $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen'], '', '', '', null, 'onchange="setValue(this.id);"');
                $Funktionen->Form->Erstelle_TextFeld('ERD_WG_' . $i, $rsERD->FeldInhalt('ERD_WG'), 200, 200, false);
                $Funktionen->Form->Erstelle_SelectFeld('ERM_RABATTSATZ_' . $i, $rsERD->FeldInhalt('ERM_RABATTSATZ'), 200, true, $SatzSQL, $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen']);
                $Funktionen->Form->ZeileEnde();

                $rsERD->DSWeiter();
            } else {
                $Funktionen->Form->ZeileStart();
                $Funktionen->Form->Erstelle_SelectFeld('ERD_KEY_' . $i, '', 200, true, $BezSQL, "" . $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen'], '', '', '', null, 'onchange="setValue(this.id);"');
                $Funktionen->Form->Erstelle_TextFeld('ERD_WG_' . $i, $Funktionen->AWISSprachkonserven["Wort"]["txt_BitteWaehlen"], 200, 200, false);
                $Funktionen->Form->Erstelle_SelectFeld('ERM_RABATTSATZ_' . $i, '', 200, true, $SatzSQL, $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen']);
                $Funktionen->Form->ZeileEnde();
            }

            $DS++;
            $i++;
        }
        $Funktionen->Form->Erstelle_SelectFeld('hiddenValues', '', 200, true, $WGSQL, '', '', '', 'display: none');
    } elseif (($rsERM->AnzahlDatensaetze() > 1)) {
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_NR'], 80);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_MODELLNAME'], 240);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_RABATTSAETZE'], 700);
        $Funktionen->Form->ZeileEnde();

        $DS = 2;
        while (!$rsERM->EOF()) {
            $Funktionen->Form->ZeileStart();
            $Link = './grosskundenkarten_Main.php?cmdAktion=Modelle&ERM_NR=' . $rsERM->FeldInhalt('ERM_NR');
            $Funktionen->Form->Erstelle_ListenFeld('ERM_NR', $rsERM->FeldInhalt('ERM_NR'), 80, 80, false, ($DS % 2), '', $Link);
            $Funktionen->Form->Erstelle_ListenFeld('ERM_MODELLNAME', $rsERM->FeldInhalt('ERM_MODELLNAME'), 240, 240, false, ($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('WARENGRUPPEN', substr($rsERM->FeldInhalt('WARENGRUPPEN'), 0, 100) . (strlen($rsERM->FeldInhalt('WARENGRUPPEN')) > 100?'...':''), 700, 700, false, ($DS % 2), '', '', 'T', 'L', $rsERM->FeldInhalt('WARENGRUPPEN'));
            $Funktionen->Form->ZeileEnde();

            $DS++;
            $rsERM->DSWeiter();
        }
    } else {
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachkonserven['Fehler']['err_keineDaten']);
        $Funktionen->Form->ZeileEnde();
    }

    $Funktionen->Form->Formular_Ende();

    $Funktionen->Form->SchaltflaechenStart();
    $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');

    if ($AWIS_KEY1 != '') {
        $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_speichern'], 'S');
        if ($AWIS_KEY1 > 0) {
            $Funktionen->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_loeschen'], 'L', '', 27, 27, '', true);
        }
    }
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_hinzufuegen'], 'N');

    $Funktionen->Form->SchaltflaechenEnde();

    $Funktionen->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {

    if ($Funktionen->Form instanceof awisFormular) {

        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}