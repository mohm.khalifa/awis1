<?php
/**
 * Bericht1
 * Bericht f�r die �bersicht nach Filialen oder Mitarbeiter
 *
 * Parameter
 *  TAG         aktueller Tag, nachdem die KW errechnet wird
 *  Typ         M f�r Mitarbeiter, F f�r Filialansicht
 */
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();


$Param['TAG']=$Form->PruefeDatum($_GET['TAG'],false,false,false);
$Param['TYP']=$_GET['Typ'];

// Ermitteln, welche Gebiete man sehen darf

$Recht11000 = $AWISBenutzer->HatDasRecht(11000);

//$Recht11000 = 1;

$GebieteListe = ',0';
if(($Recht11000&2)!=2)
{
    $SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
    $SQL .= ' FROM Filialebenen';
    $SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
    $SQL .= ' AND EXISTS';
    $SQL .= '  (select DISTINCT XX1_FEB_KEY';
    $SQL .= '  FROM V_FILIALEBENENROLLEN';
    $SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
    $SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS)  >= trunc(sysdate)';
    //$SQL .= '  WHERE XX1_KON_KEY = '.'2375';      // Zum Testen: Ott (GBL)
    $SQL .= '  WHERE XX1_KON_KEY = '.$DB->WertSetzen('FEB','N0',$AWISBenutzer->BenutzerKontaktKEY());
    $SQL .= '  AND  XX1_FEB_KEY = FEB_KEY';
    $SQL .= ')';
    $SQL .= ' ORDER BY 2';
    $rsFEB = $DB->RecordSetOeffnen($SQL,$DB->BindeVariablen('FEB'));
    $GebieteListe = '';
    while(!$rsFEB->EOF())
    {
        $GebieteListe .= ','.$rsFEB->FeldInhalt('FEB_KEY');
        $rsFEB->DSWeiter();
    }
    unset($rsFEB);
    if($GebieteListe=='')
    {
        $GebieteListe = ',0';
    }
}

//$GebieteListe = ',0,400';

$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
/*
$RechteStufe = $AWISBenutzer->HatDasRecht(4500);
if($RechteStufe==0)
{
	$Form->DebugAusgabe(1,$AWISBenutzer->BenutzerName());
	$Form->Fehler_KeineRechte();
}
*/
$TextKonserven=array();
$TextKonserven[]=array('WTP','%');
$TextKonserven[]=array('Wort','wrd_Stand');
$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

//$AWISSprachKonserven['WTP']['tit_Bericht_1']
$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,($Param['TYP']=='M'?'Mitarbeiter�bersicht':'Filial�bersicht'));

$Zeile = $Ausdruck->NeueSeite(0,1);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->setXY(10,200);
$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

$Ausdruck->_pdf->setXY(10,12);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);

//
// Daten
//


$SQL ='SELECT DISTINCT FEB_BEZEICHNUNG, FEB_KEY';
//if($Param['TYP']=='M')  // Nach Mitarbeitern
{
	$SQL .= ', SUM(XXX_VK) AS UMSATZ';
}
/*else
{
	$SQL .= ', MAX(
				(SELECT SUM(WTP_VK) FROM
					(SELECT DISTINCT WTP_WPO_KEY AS WPOKEY, WTP_VK FROM WerkstattterminePlanungen) DATEN1
					WHERE WPOKEY = WPO_KEY GROUP BY WPOKEY)) AS UMSATZ';
}
*/
$SQL .= ', SUM(WTP_ZEITMIN) AS MIN, SUM(WTP_ANFAHRTSZEIT) AS FAHRZEIT, COUNT(DISTINCT WTE_WANR) AS ANZWA';
$SQL .= ', TO_CHAR(WTP_TAG,\'D\') AS TAG';
$SQL .= ',TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'IW\') AS KW';
$SQL .= ($Param['TYP']=='M'?', KON_KEY, KON_NAME1, KON_NAME2':', WTE_FIL_ID ');
$SQL .= ' FROM WerkstattterminePlanungen';
//$SQL .= ' INNER JOIN WerkstattterminePositionen ON WTP_WPO_KEY = WPO_KEY ';
$SQL .= ' INNER JOIN WERKSTATTTERMINEPOSITIONEN ON WTP_WPO_KEY = WPO_KEY AND WPO_IMPORTSTATUS <> \'X\'';
$SQL .= ' INNER JOIN Werkstatttermine ON WPO_WTE_KEY = WTE_KEY';
if(($Recht11000&2)!=2)
{
    $SQL .= ' INNER JOIN FILIALEBENENZUORDNUNGEN ON WTE_FIL_ID = FEZ_FIL_ID AND FEZ_FEB_KEY IN ('.substr($GebieteListe,1).') AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
}
else
{
    $SQL .= ' INNER JOIN FILIALEBENENZUORDNUNGEN ON WTE_FIL_ID = FEZ_FIL_ID AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
}
$SQL .= ' INNER JOIN FILIALEBENEN ON FEZ_FEB_KEY = FEB_KEY';
$SQL .= ' INNER JOIN kontakte ON WTP_KON_KEY = KON_KEY';
if($Param['TYP']=='M')  // Nach Mitarbeitern
{
	$SQL .= ' INNER JOIN V_SRPEP_UMSATZ ON XXX_WPO_KEY =WPO_KEY AND XXX_KON_KEY = KON_KEY AND TO_CHAR(XXX_WTP_TAG,\'YYYY-IW\') = TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'YYYY-IW\')';
}
else        // Nach Filialen
{
	$SQL .= ' INNER JOIN V_SRPEP_UMSATZ ON XXX_WPO_KEY =WPO_KEY AND XXX_KON_KEY = KON_KEY AND TO_CHAR(XXX_WTP_TAG,\'YYYY-IW\') = TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'YYYY-IW\')';
}
$SQL .= ' WHERE ';
$SQL .= ' TO_CHAR(WTP_TAG,\'YYYY-IW\') = TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'YYYY-IW\')';
$SQL .= ' AND WTP_STATUS < 8'; //TR 07.10.10
if($Param['TYP']=='M')  // Nach Mitarbeitern
{
    $SQL .= ' GROUP BY FEB_BEZEICHNUNG,FEB_KEY,KON_NAME1, KON_NAME2, KON_KEY, TO_CHAR(WTP_TAG,\'D\')';
    $SQL .= ' ORDER BY FEB_BEZEICHNUNG,FEB_KEY,KON_NAME1, KON_NAME2, KON_KEY, TO_CHAR(WTP_TAG,\'D\')';
    $GruppenFeld = 'KON_KEY';
}
else        // Nach Filialen
{
    $SQL .= ' GROUP BY FEB_BEZEICHNUNG,FEB_KEY,WTE_FIL_ID, TO_CHAR(WTP_TAG,\'D\')';
    $SQL .= ' ORDER BY FEB_BEZEICHNUNG,FEB_KEY,WTE_FIL_ID, TO_CHAR(WTP_TAG,\'D\')';
    $GruppenFeld = 'WTE_FIL_ID';
}





//$Form->DebugAusgabe(1,$SQL);
//die();

$rsWTP = $DB->RecordSetOeffnen($SQL);
//die($DB->LetzterSQL());


$SQL = 'SELECT XKA_TAG FROM KALENDER WHERE TO_CHAR(XKA_TAG,\'YYYY-IW\') = TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'YYYY-IW\') AND TO_CHAR(XKA_TAG,\'D\') = 1';
$rsMontag = $DB->RecordSetOeffnen($SQL);

$SQL = 'SELECT XKA_TAG FROM KALENDER WHERE TO_CHAR(XKA_TAG,\'YYYY-IW\') = TO_CHAR('.$DB->FeldInhaltFormat('D',$_GET['TAG']).',\'YYYY-IW\') AND TO_CHAR(XKA_TAG,\'D\') = 6';
$rsSamstag = $DB->RecordSetOeffnen($SQL);

// �berschrift f�r den Bericht
$Zeile = 30;
$Ausdruck->_pdf->setXY(10,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter�bersicht':'Filial�bersicht'),0,0,'L',0);
$Ausdruck->_pdf->cell(15,6,'KW '.$rsWTP->FeldInhalt('KW'),0,0,'L',0);
$Ausdruck->_pdf->cell(50,6,'('.$Form->Format('D', $rsMontag->FeldInhalt('XKA_TAG')). ' bis ' .$Form->Format('D', $rsSamstag->FeldInhalt('XKA_TAG')).')',0,0,'L',0);
$Zeile +=6;

$Ausdruck->_pdf->setXY(10,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter':'Filiale'),1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Montag',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Dienstag',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Mittwoch',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Donnerstag',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Freitag',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Samstag',1,0,'L',0);
$Ausdruck->_pdf->cell(31,6,'Summe',1,0,'L',0);
//$Zeile+=5;

$LetzteGruppe = 0;          // Gruppe pro Mitarbeiter / Filiale
$LetzteFEB = 0;             // Filialebene-Gruppe
$LetzteFEBBez = '';         // Bezeieichnung f�r die Filailebene
$Summen = array();
for($Tag=1;$Tag<=6;$Tag++)
{
    $Summen['T'.$Tag]['Umsatz']=0;
    $Summen['T'.$Tag]['Min']=0;
    $Summen['T'.$Tag]['AnzWA']=0;
    $Summen['T'.$Tag]['Fahrzeit']=0;
    $Summen['T'.$Tag]['Zeit']=0;
}


while(!$rsWTP->EOF())
{
    // Summe pro Mitarbeiter oder Filiale
    if($LetzteGruppe!=$rsWTP->FeldInhalt($GruppenFeld))
	{
		if($LetzteGruppe>0)
		{
			$Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
			//$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteGruppe]['Umsatz']),0,0,'R',0);
			$Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteGruppe]['Umsatz']),0,0,'R',0);
			$Ausdruck->_pdf->setXY((7*31)+30,$Zeile+5);
			//$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteGruppe]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteGruppe]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteGruppe]['Fahrzeit']/60)),0,0,'R',0);
			$Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteGruppe]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteGruppe]['Fahrzeit']/60)),0,0,'R',0);
		}

        // Passt das noch auf die Seite ?
        if($Zeile > $Ausdruck->SeitenHoehe()-40)
        {
            $Zeile = $Ausdruck->NeueSeite(0,2);
            $Ausdruck->_pdf->SetFont('Arial','',6);
            $Ausdruck->_pdf->setXY(10,200);
            $Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

            $Ausdruck->_pdf->setXY(10,12);
            $Ausdruck->_pdf->SetFont('Arial','',6);
            $Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);

            $Zeile = 30;
            $Ausdruck->_pdf->setXY(10,$Zeile);
            $Ausdruck->_pdf->SetFont('Arial','',12);
            $Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter�bersicht':'Filial�bersicht'),0,0,'L',0);
            $Ausdruck->_pdf->cell(15,6,'KW '.$rsWTP->FeldInhalt('KW'),0,0,'L',0);
            $Ausdruck->_pdf->cell(50,6,'('.$Form->Format('D', $rsMontag->FeldInhalt('XKA_TAG')). ' bis ' .$Form->Format('D', $rsSamstag->FeldInhalt('XKA_TAG')).')',0,0,'L',0);
            //$Ausdruck->_pdf->cell(10,6,'KW '.date('w',$Form->PruefeDatum($_GET['TAG'],false,false,true)),0,0,'L',0);
            $Zeile +=6;

            $Ausdruck->_pdf->setXY(10,$Zeile);
            $Ausdruck->_pdf->SetFont('Arial','',10);
            $Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter':'Filiale'),1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Montag',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Dienstag',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Mittwoch',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Donnerstag',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Freitag',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Samstag',1,0,'L',0);
            $Ausdruck->_pdf->cell(31,6,'Summe',1,0,'L',0);
            $Zeile += 5;
        }
        else
        {
            $Zeile+=($LetzteGruppe==0?5:10);
        }

        // Ein neues Gebiet?
        if($LetzteFEB != $rsWTP->FeldInhalt('FEB_KEY'))
        {
            if($LetzteFEB>0)
            {
            $Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
            $Ausdruck->_pdf->Line(10,$Zeile,10,$Zeile+10);
            $Ausdruck->_pdf->Line(10,$Zeile+10,60+(7*31),$Zeile+10);
            for($i=0;$i<8;$i++)
            {
                $Ausdruck->_pdf->Line(60+($i*31),$Zeile,60+($i*31),$Zeile+10);
            }

                // Die Summen f�r die einzelnen Tage
                for($Tag=1;$Tag<=6;$Tag++)
                {
                    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile);
                    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFEB.'T'.$Tag]['Umsatz']),0,0,'R',0);
                    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteFEB.'T'.$Tag]['Umsatz']),0,0,'R',0);
                    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile+5);
                    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteFEB.'T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);
                    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteFEB.'T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);
                }

                $Ausdruck->_pdf->setXY(10,$Zeile);
                $Ausdruck->_pdf->SetFillColor(230,230,230);
                $Ausdruck->_pdf->cell(50,10,'Summe '.$LetzteFEBBez,1,0,'R',1);
                $Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
                //$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFEB]['Umsatz']),0,0,'R',0);
                $Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteFEB]['Umsatz']),0,0,'R',0);
                $Ausdruck->_pdf->setXY((7*31)+30,$Zeile+5);
                //$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFEB]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteFEB]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB]['Fahrzeit']/60)),0,0,'R',0);
                $Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteFEB]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB]['Fahrzeit']/60)),0,0,'R',0);
                $Zeile+=10;
                $Ausdruck->_pdf->Line(10,$Zeile,60+(7*31),$Zeile);
            }

            $LetzteFEB = $rsWTP->FeldInhalt('FEB_KEY');
            $LetzteFEBBez = $rsWTP->FeldInhalt('FEB_BEZEICHNUNG');

            $Summen[$LetzteFEB]['Umsatz']=0;
            $Summen[$LetzteFEB]['Min']=0;
            $Summen[$LetzteFEB]['AnzWA']=0;
            $Summen[$LetzteFEB]['Fahrzeit']=0;
            $Summen[$LetzteFEB]['Zeit']=0;

            for($Tag=1;$Tag<=6;$Tag++)
            {
                $Summen[$LetzteFEB.'T'.$Tag]['Umsatz']=0;
                $Summen[$LetzteFEB.'T'.$Tag]['Min']=0;
                $Summen[$LetzteFEB.'T'.$Tag]['AnzWA']=0;
                $Summen[$LetzteFEB.'T'.$Tag]['Fahrzeit']=0;
                $Summen[$LetzteFEB.'T'.$Tag]['Zeit']=0;
            }
        }


        // K�stchen f�r den n�chsten Mitarbeiter malen
		$Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
		$Ausdruck->_pdf->Line(10,$Zeile,10,$Zeile+10);
		$Ausdruck->_pdf->Line(10,$Zeile+10,60+(7*31),$Zeile+10);
		for($i=0;$i<8;$i++)
		{
			$Ausdruck->_pdf->Line(60+($i*31),$Zeile,60+($i*31),$Zeile+10);
		}

		$LetzteGruppe=$rsWTP->FeldInhalt($GruppenFeld);

		$Summen[$LetzteGruppe]['Umsatz']=0;
		$Summen[$LetzteGruppe]['Min']=0;
		$Summen[$LetzteGruppe]['AnzWA']=0;
		$Summen[$LetzteGruppe]['Fahrzeit']=0;
		$Summen[$LetzteGruppe]['Zeit']=0;

		$Ausdruck->_pdf->setXY(10,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$rsWTP->FeldInhalt(($Param['TYP']=='M'?'KON_NAME1':'WTE_FIL_ID')),0,0,'L',0);
		$Ausdruck->_pdf->SetFont('Arial','',8);
		$Ausdruck->_pdf->cell(25,6,'Umsatz Eur:',0,0,'R',0);
		$Ausdruck->_pdf->setXY(10,$Zeile+5);
		//$Ausdruck->_pdf->cell(50,6,'Zeit; Anz WA; Fahrt:',0,0,'R',0);
		$Ausdruck->_pdf->cell(50,6,'Anz WA; Fahrt:',0,0,'R',0);

		$Ausdruck->_pdf->SetFont('Arial','',10);
	}

    // Summe pro Mitarbeiter / Filiale f�r einen Tag drucken
	$TagPos = $rsWTP->FeldInhalt('TAG')*31;

	$Ausdruck->_pdf->setXY($TagPos+30,$Zeile);
	$Ausdruck->_pdf->cell(31,6,$Form->Format('N0T',$rsWTP->FeldInhalt('UMSATZ')),0,0,'R',0);
	//$Ausdruck->_pdf->cell(31,6,$Form->Format('N2T',$rsWTP->FeldInhalt('UMSATZ')),0,0,'R',0);

	$Ausdruck->_pdf->setXY($TagPos+30,$Zeile+5);
	//$Ausdruck->_pdf->cell(31,6,$Form->Format('N1',($rsWTP->FeldInhalt('MIN'))/60).'; '.$Form->Format('N0',($rsWTP->FeldInhalt('ANZWA'))).'; '.$Form->Format('N1',($rsWTP->FeldInhalt('FAHRZEIT')/60)),0,0,'R',0);
	$Ausdruck->_pdf->cell(31,6,$Form->Format('N0',($rsWTP->FeldInhalt('ANZWA'))).'; '.$Form->Format('N1',($rsWTP->FeldInhalt('FAHRZEIT')/60)),0,0,'R',0);

    // Summe pro Mitarbeiter / Filiale
	//$Summen[$LetzteGruppe]['Umsatz'] += $DB->FeldInhaltFormat('N2',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteGruppe]['Umsatz'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteGruppe]['Zeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('MIN'));
	$Summen[$LetzteGruppe]['AnzWA'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('ANZWA'));
	$Summen[$LetzteGruppe]['Fahrzeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('FAHRZEIT'));

    // Gesamtsumme f�r das Gebiet
	//$Summen[$LetzteFEB]['Umsatz'] += $DB->FeldInhaltFormat('N2',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteFEB]['Umsatz'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteFEB]['Zeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('MIN'));
	$Summen[$LetzteFEB]['AnzWA'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('ANZWA'));
	$Summen[$LetzteFEB]['Fahrzeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('FAHRZEIT'));

    // Summen pro Tag
	//$Summen['T'.$rsWTP->FeldInhalt('TAG')]['Umsatz'] += $DB->FeldInhaltFormat('N2',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen['T'.$rsWTP->FeldInhalt('TAG')]['Umsatz'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen['T'.$rsWTP->FeldInhalt('TAG')]['Zeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('MIN'));
	$Summen['T'.$rsWTP->FeldInhalt('TAG')]['AnzWA'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('ANZWA'));
	$Summen['T'.$rsWTP->FeldInhalt('TAG')]['Fahrzeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('FAHRZEIT'));

    // Summen pro Gebiet pro Tag
	//$Summen[$LetzteFEB.'T'.$rsWTP->FeldInhalt('TAG')]['Umsatz'] += $DB->FeldInhaltFormat('N2',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteFEB.'T'.$rsWTP->FeldInhalt('TAG')]['Umsatz'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('UMSATZ'));
	$Summen[$LetzteFEB.'T'.$rsWTP->FeldInhalt('TAG')]['Zeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('MIN'));
	$Summen[$LetzteFEB.'T'.$rsWTP->FeldInhalt('TAG')]['AnzWA'] += $DB->FeldInhaltFormat('N0',$rsWTP->FeldInhalt('ANZWA'));
	$Summen[$LetzteFEB.'T'.$rsWTP->FeldInhalt('TAG')]['Fahrzeit'] += $DB->FeldInhaltFormat('N1',$rsWTP->FeldInhalt('FAHRZEIT'));

	$rsWTP->DSWeiter();
}

// Letzte Summe f�r den Mitarbeiter / Filiale
$Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
if($LetzteGruppe>0)
{
	//$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteGruppe]['Umsatz']),0,0,'R',0);
	$Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteGruppe]['Umsatz']),0,0,'R',0);
	$Ausdruck->_pdf->setXY((7*31)+30,$Zeile+5);
	//$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteGruppe]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteGruppe]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteGruppe]['Fahrzeit']/60)),0,0,'R',0);
	$Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteGruppe]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteGruppe]['Fahrzeit']/60)),0,0,'R',0);
	$Ausdruck->_pdf->Line(10,$Zeile+10,50+(7*31),$Zeile+10);
}

// Passt das noch auf die Seite ?
if($Zeile > $Ausdruck->SeitenHoehe()-40)
{
    $Zeile = $Ausdruck->NeueSeite(0,2);
    $Ausdruck->_pdf->SetFont('Arial','',6);
    $Ausdruck->_pdf->setXY(10,200);
    $Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

    $Ausdruck->_pdf->setXY(10,12);
    $Ausdruck->_pdf->SetFont('Arial','',6);
    $Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);

    $Zeile = 30;
    $Ausdruck->_pdf->setXY(10,$Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',12);
    $Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter�bersicht':'Filial�bersicht'),0,0,'L',0);
    $Ausdruck->_pdf->cell(15,6,'KW '.$rsWTP->FeldInhalt('KW'),0,0,'L',0);
    $Ausdruck->_pdf->cell(50,6,'('.$Form->Format('D', $rsMontag->FeldInhalt('XKA_TAG')). ' bis ' .$Form->Format('D', $rsSamstag->FeldInhalt('XKA_TAG')).')',0,0,'L',0);
    //$Ausdruck->_pdf->cell(10,6,'KW '.date('w',$Form->PruefeDatum($_GET['TAG'],false,false,true)),0,0,'L',0);
    $Zeile +=6;

    $Ausdruck->_pdf->setXY(10,$Zeile);
    $Ausdruck->_pdf->SetFont('Arial','',10);
    $Ausdruck->_pdf->cell(50,6,($Param['TYP']=='M'?'Mitarbeiter':'Filiale'),1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Montag',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Dienstag',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Mittwoch',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Donnerstag',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Freitag',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Samstag',1,0,'L',0);
    $Ausdruck->_pdf->cell(31,6,'Summe',1,0,'L',0);
    $Zeile += 5;
}

// ***********************************************
// Letzte Summe f�r das Gebiet
// ***********************************************
$Zeile+=10;
$Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
$Ausdruck->_pdf->Line(10,$Zeile,60+(7*31),$Zeile);
$Ausdruck->_pdf->Line(10,$Zeile,10,$Zeile+10);
$Ausdruck->_pdf->Line(10,$Zeile+10,60+(7*31),$Zeile+10);
for($i=0;$i<8;$i++)
{
    $Ausdruck->_pdf->Line(60+($i*31),$Zeile,60+($i*31),$Zeile+10);
}

// Die Summen f�r die einzelnen Tage
for($Tag=1;$Tag<=6;$Tag++)
{
    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile);
    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFEB.'T'.$Tag]['Umsatz']),0,0,'R',0);
    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteFEB.'T'.$Tag]['Umsatz']),0,0,'R',0);
    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile+5);
    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteFEB.'T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);
    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteFEB.'T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB.'T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);
}

$Ausdruck->_pdf->setXY(10,$Zeile);
$Ausdruck->_pdf->SetFillColor(230,230,230);
$Ausdruck->_pdf->cell(50,10,'Summe '.$LetzteFEBBez,1,0,'R',1);
$Ausdruck->_pdf->setXY(30+(7*31),$Zeile);
//$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFEB]['Umsatz']),0,0,'R',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen[$LetzteFEB]['Umsatz']),0,0,'R',0);
$Ausdruck->_pdf->setXY((7*31)+30,$Zeile+5);
//$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFEB]['Zeit'])/60).'; '.$Form->Format('N0',($Summen[$LetzteFEB]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB]['Fahrzeit']/60)),0,0,'R',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen[$LetzteFEB]['AnzWA'])).'; '.$Form->Format('N1',($Summen[$LetzteFEB]['Fahrzeit']/60)),0,0,'R',0);
$Zeile+=10;
$Ausdruck->_pdf->Line(10,$Zeile,60+(7*31),$Zeile);

// ***********************************************
// Gesamt-Summe �ber alle Mitarbeiter / Filialen
// ***********************************************
$Ausdruck->_pdf->setXY(10,$Zeile);
$Ausdruck->_pdf->cell(50,6,'Summe:',0,0,'L',0);

for($Tag=1;$Tag<=6;$Tag++)
{
    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile);
    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen['T'.$Tag]['Umsatz']),0,0,'R',0);
    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen['T'.$Tag]['Umsatz']),0,0,'R',0);
    $Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile+5);
    //$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen['T'.$Tag]['Zeit'])/60).'; '.$Form->Format('N0',($Summen['T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen['T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);
    $Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen['T'.$Tag]['AnzWA'])).'; '.$Form->Format('N1',($Summen['T'.$Tag]['Fahrzeit']/60)),0,0,'R',0);

    $Summen['Umsatz']+=$Summen['T'.$Tag]['Umsatz'];
    $Summen['Zeit']+=$Summen['T'.$Tag]['Zeit'];
    $Summen['AnzWA']+=$Summen['T'.$Tag]['AnzWA'];
    $Summen['Fahrzeit']+=$Summen['T'.$Tag]['Fahrzeit'];
}
$Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile);
//$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen['Umsatz']),0,0,'R',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N0T',$Summen['Umsatz']),0,0,'R',0);
$Ausdruck->_pdf->setXY(($Tag*31)+30,$Zeile+5);
//$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen['Zeit'])/60).'; '.$Form->Format('N0',($Summen['AnzWA'])).'; '.$Form->Format('N1',($Summen['Fahrzeit']/60)),0,0,'R',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N0',($Summen['AnzWA'])).'; '.$Form->Format('N1',($Summen['Fahrzeit']/60)),0,0,'R',0);


$Ausdruck->Anzeigen();