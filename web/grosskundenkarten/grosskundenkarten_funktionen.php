<?php
require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: pirschel_p
 * Date: 26.01.2017
 * Time: 13:28
 */
class grosskundenkarten_funktionen
{
    const RABATTMODELLE_AKTIV = false;
    const PRINT_BASE_DIR = '/win/applicationdata2/AWIS/08-Grosskundenkarten/'; //Basisverzeichnis
    const PRINT_STATUS_FILE_NAME = 'status.txt'; //Status der Queue
    const PRINT_PDF_FILE_NAME = 'grosskundenkarte'; //PDF die gedruckt werden soll


    public $AWISSprachkonserven;
    public $DB;
    public $AWISBenutzer;
    public $Form;
    public $Recht; //Recht 59000
    private $_rsKunde;
    private $_rsERM;
    private $_AWISWerkzeuge;

    private $_TBL_GROSSKUNDENKARTEN = "";
    private $_TBL_GROSSKUNDEST = "";

    private $_QUEUE_DIR = '';
    private $_STATUS_FILE = '';
    private $_PDF_FILE = '';


    public function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->Form = new awisFormular(3);
        $this->_AWISWerkzeuge = new awisWerkzeuge();

        if ($this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK or $this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING) {
            $this->_TBL_GROSSKUNDENKARTEN = " TESTGROSSKUNDENKARTEN ";
            $this->_TBL_GROSSKUNDEST = " TESTGROSSKUNDEST ";
        } else {
            $this->_TBL_GROSSKUNDENKARTEN = " EXPERIAN_KOM.EXPERIAN_GROSSKUNDENKARTEN@COM_DE.ATU.DE ";
            $this->_TBL_GROSSKUNDEST = " EXPGW.ATU_GROSSKUNDEST@COM_DE.ATU.DE ";
        }

        $Sprachvariablen = array();
        $Sprachvariablen[] = array('ERM', '%');
        $Sprachvariablen[] = array('Wort', 'Auswahl_ALLE');
        $Sprachvariablen[] = array('Wort', 'AuswahlSpeichern');
        $Sprachvariablen[] = array('Wort', 'DatumVom');
        $Sprachvariablen[] = array('Wort', 'DatumBis');
        $Sprachvariablen[] = array('Wort', 'lbl_zurueck');
        $Sprachvariablen[] = array('Wort', 'lbl_drucken');
        $Sprachvariablen[] = array('Wort', 'lbl_speichern');
        $Sprachvariablen[] = array('Wort', 'lbl_loeschen');
        $Sprachvariablen[] = array('Wort', 'lbl_hinzufuegen');
        $Sprachvariablen[] = array('Wort', 'lbl_suche');
        $Sprachvariablen[] = array('Wort', 'lbl_trefferliste');
        $Sprachvariablen[] = array('Wort', 'lbl_weiter');
        $Sprachvariablen[] = array('Wort', 'lbl_reset');
        $Sprachvariablen[] = array('Wort', 'lbl_hilfe');
        $Sprachvariablen[] = array('Wort', 'txt_BitteWaehlen');
        $Sprachvariablen[] = array('Wort', 'ttt_AuswahlSpeichern');
        $Sprachvariablen[] = array('Fehler', 'err_keineDaten');
        $Sprachvariablen[] = array('Fehler', 'err_keineDatenbank');
        $Sprachvariablen[] = array('Fehler', 'err_keineRechte');
        $Sprachvariablen[] = array('TITEL', 'tit_Grosskundenkarten');
        $Sprachvariablen[] = array('Liste', 'lst_ALLE_0');
        $Sprachvariablen[] = array('Liste', 'lst_JaNein');

        $this->AWISSprachkonserven = $this->Form->LadeTexte($Sprachvariablen);
        $this->Recht = $this->AWISBenutzer->HatDasRecht(59000);

        if ($this->Recht == 0) {
            $this->Form->Fehler_KeineRechte();
        }

        $this->_QUEUE_DIR = self::PRINT_BASE_DIR . $this->_AWISWerkzeuge->awisLevel() . '/';
        $this->_STATUS_FILE = $this->_QUEUE_DIR . self::PRINT_STATUS_FILE_NAME;
        $this->_PDF_FILE = $this->_QUEUE_DIR . self::PRINT_PDF_FILE_NAME;
    }



    /**
     * Erstellt das AJAX mit den Kundendaten
     *
     * @param $KUNDENNR
     */
    public function KundenDatenAJAX($KUNDENNR)
    {
        $Script = 'function changeValue() {$( "#sucERM_NR" ).val($( "#txtSELECT_ERM_NR" ).val()).change()}';
        $this->Form->SchreibeHTMLCode('<script>' . $Script . '</script>');

        $SQL = 'SELECT DISTINCT NAME1, NAME2, NAME3, KUNDEN_NR, ERZ_ERM_NR ';
        $SQL .= 'FROM ' . $this->_TBL_GROSSKUNDENKARTEN . ' ';
        $SQL .= 'LEFT JOIN EXPRABATTZUORDNUNGEN ON KUNDEN_NR = ERZ_KUNDEN_NR ';
        $SQL .= 'WHERE KUNDEN_NR = ' . $this->DB->WertSetzen('EXP', 'TN', $KUNDENNR);

        $this->_rsKunde = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('EXP', true));

        if ($this->_rsKunde->FeldInhalt('ERZ_ERM_NR') != '') {
            $this->Form->ZeileStart();
            $this->Form->Hinweistext($this->AWISSprachkonserven['ERM']['HINWEISTEXT'], AWISFormular::HINWEISTEXT_BENACHRICHTIGUNG);
            $this->Form->ZeileEnde();
        }

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachkonserven['ERM']['NAME1'], 200);
        $this->Form->Erstelle_TextLabel($this->_rsKunde->FeldInhalt('NAME1'), 200);
        $this->Form->ZeileEnde();
        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachkonserven['ERM']['NAME2'], 200);
        $this->Form->Erstelle_TextLabel($this->_rsKunde->FeldInhalt('NAME2'), 200);
        $this->Form->ZeileEnde();
        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachkonserven['ERM']['NAME3'], 200);
        $this->Form->Erstelle_TextLabel($this->_rsKunde->FeldInhalt('NAME3'), 200);
        $this->Form->ZeileEnde();

        if (self::RABATTMODELLE_AKTIV) {
            $this->Form->ZeileStart();
            $this->Form->Erstelle_TextLabel($this->AWISSprachkonserven['ERM']['ERM_NR'], 200);
            $this->Form->AuswahlBox(
                '!ERM_NR',
                'box2',
                '',
                'ERM_Daten',
                '',
                200,
                20,
                $this->_rsKunde->FeldInhalt('ERZ_ERM_NR') != '' ? $this->_rsKunde->FeldInhalt('ERZ_ERM_NR') : '',
                'T',
                true,
                '',
                '',
                '',
                0,
                '',
                '',
                '',
                0,
                'display: none',
                'autocomplete=off'
            );
            $this->Form->Erstelle_SelectFeld(
                'SELECT_ERM_NR',
                ($this->_rsKunde->FeldInhalt('ERZ_ERM_NR') != '' ? $this->_rsKunde->FeldInhalt('ERZ_ERM_NR') : ''),
                200,
                true,
                'SELECT DISTINCT ERM_NR, ERM_MODELLNAME FROM EXPRABATTMODELLE ORDER BY 1',
                $this->AWISSprachkonserven['Wort']['txt_BitteWaehlen'],
                '',
                '',
                '',
                '',
                'onChange=changeValue();'
            );
            $this->Form->ZeileEnde();
            $this->Form->AuswahlBoxHuelle('box2', '', '', '');
        }

        $this->Form->SchreibeHTMLCode('<script>$(window).load(changeValue())</script>');
    }

    /**
     * Erstellt das AJAX mit den Modelldaten
     *
     * @param $ERM_NR
     */
    public function ModellDatenAJAX($ERM_NR)
    {
        $SQL = 'SELECT ERM_NR, ERM_RABATTSATZ, ERD_BEZEICHNUNG FROM EXPRABATTMODELLE 
                INNER JOIN EXPRABATTDETAILS ON ERM_ERD_KEY = ERD_KEY WHERE ERM_NR = ' . $this->DB->WertSetzen('ERM', 'TN', $ERM_NR);
        $this->_rsERM = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('ERM', true));

        $first = true;
        while (!$this->_rsERM->EOF()) {
            $this->Form->ZeileStart();
            $this->Form->Erstelle_TextLabel($first ? $this->AWISSprachkonserven['ERM']['ERM_RABATTSAETZE'] : '', 200);
            $this->Form->Erstelle_TextLabel($this->_rsERM->FeldInhalt('ERM_RABATTSATZ') . '% ' . $this->_rsERM->FeldInhalt('ERD_BEZEICHNUNG'), 200);
            $this->Form->ZeileEnde();
            $this->_rsERM->DSWeiter();
            $first = false;
        }
    }

    /**
     * Erstellt das AJAX auf der Druckenseite
     *
     * @param $Land
     */
    public function DruckAJAX($SuchString, $Land)
    {

        $SQL = 'SELECT KUNDEN_NR, trim(NAME1) || \' \' || trim(NAME2) || \' \' || trim(NAME3) as NAME, KARTENANZAHL, rownum as ZEILENNR   FROM (';

        $SQL .= 'SELECT
                      DATEN.KUNDEN_NR,
                      DATEN.NAME1,
                      DATEN.NAME2,
                      DATEN.NAME3,
                      DATEN.GKB_STAAT,
                      DATEN.ZAHL_ART,
                      COUNT(DATEN.KARTEN_NR) AS KARTENANZAHL
                    FROM
                      (
                        SELECT KND.KUNDEN_NR, KND.NAME1, KND.NAME2, KND.NAME3, LAND.GKB_STAAT, LAND.ZAHL_ART, KND.KARTEN_NR
                        FROM ' . $this->_TBL_GROSSKUNDENKARTEN . ' KND
                        INNER JOIN ' . $this->_TBL_GROSSKUNDEST . ' LAND
                        ON KND.KUNDEN_NR = LAND.KUNDEN_NR
                        WHERE ZAHL_ART = \'K\'
                        AND GEDRUCKT = 1
                        UNION ALL
                        SELECT KND.KUNDEN_NR, KND.NAME1, KND.NAME2, KND.NAME3, LAND.GKB_STAAT, LAND.ZAHL_ART, KND.KARTEN_NR
                        FROM ' . $this->_TBL_GROSSKUNDENKARTEN . ' KND
                        INNER JOIN ' . $this->_TBL_GROSSKUNDEST . ' LAND
                        ON KND.KUNDEN_NR = LAND.KUNDEN_NR ';
        if (self::RABATTMODELLE_AKTIV) {
            $SQL .= '
                        INNER JOIN EXPRABATTZUORDNUNGEN ERZ
                        ON KND.KUNDEN_NR = ERZ.ERZ_KUNDEN_NR ';
        }
        $SQL .= '
                        WHERE ZAHL_ART = \'G\'
                        AND GEDRUCKT = 1
                      ) DATEN';

        $Where = '';
        switch ($Land) {
            case '-1':
                break;
            case '0':
                $Where .= ' AND DATEN.GKB_STAAT IS NULL ';
                break;
            case '4G': //Deutsche Barzahler
                $Where .= ' AND DATEN.GKB_STAAT = ' . $this->DB->WertSetzen('ERM', 'T', '4') . ' ';
                $Where .= ' AND DATEN.ZAHL_ART = ' . $this->DB->WertSetzen('ERM', 'T', 'G') . ' ';
                $Where .= ' AND (DATEN.NAME3 <> ' . $this->DB->WertSetzen('ERM', 'T', 'Mana Ara') . ' ';
                $Where .= ' OR DATEN.NAME3 is null) '; //Nur Suche nach 'nicht Mana Ara'
                break;
            case '4K': // Deutsche Unbarzahler
                $Where .= ' AND DATEN.GKB_STAAT = ' . $this->DB->WertSetzen('ERM', 'T', '4') . ' ';
                $Where .= ' AND DATEN.ZAHL_ART = ' . $this->DB->WertSetzen('ERM', 'T', 'K') . ' ';
                break;
            case 'G': //Allgemeine Barzahler
                $Where .= ' AND DATEN.ZAHL_ART = ' . $this->DB->WertSetzen('ERM', 'T', $Land) . ' ';
                $Where .= ' AND (DATEN.NAME3 <> ' . $this->DB->WertSetzen('ERM', 'T', 'Mana Ara') . ' ';
                $Where .= ' OR DATEN.NAME3 is null) '; //Nur Suche nach 'nicht Mana Ara'
                break;
            case 'MA': //Mana Ara
                $Where .= ' AND DATEN.NAME3 = \'Mana Ara\' ';
                break;
            default: //Allgemein Länder: 4 = Deutschland, 38 = Österreich
                $Where .= ' AND DATEN.GKB_STAAT = ' . $this->DB->WertSetzen('ERM', 'T', $Land) . ' ';
                $Where .= ' AND (DATEN.NAME3 <> ' . $this->DB->WertSetzen('ERM', 'T', 'Mana Ara') . ' ';
                $Where .= ' OR DATEN.NAME3 is null) '; //Nur Suche nach 'nicht Mana Ara'
        }

        if ($SuchString) {
            $tSuchString = '*' . $SuchString . '*';
            $Where .= ' AND ( upper(DATEN.NAME1)  ' . $this->DB->LikeOderIst($tSuchString, awisDatenbank::AWIS_LIKE_UPPER, 'ERM');
            $Where .= ' or upper(DATEN.NAME2)  ' . $this->DB->LikeOderIst($tSuchString, awisDatenbank::AWIS_LIKE_UPPER, 'ERM');
            $Where .= ' or upper(DATEN.NAME3)  ' . $this->DB->LikeOderIst($tSuchString, awisDatenbank::AWIS_LIKE_UPPER, 'ERM');
            $Where .= ' or upper(DATEN.KUNDEN_NR)  ' . $this->DB->LikeOderIst($tSuchString, awisDatenbank::AWIS_LIKE_UPPER, 'ERM');
            $Where .= ')';
        }

        if ($Where) {
            $SQL .= ' WHERE ' . substr($Where, 5);
        }

        $MaxDS = $this->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $SQL .= ' GROUP BY
                      DATEN.KUNDEN_NR,
                      DATEN.NAME1,
                      DATEN.NAME2,
                      DATEN.NAME3,
                      DATEN.GKB_STAAT,
                      DATEN.ZAHL_ART
                    ORDER BY
                      DATEN.KUNDEN_NR DESC';

        $SQL .= ' ) WHERE ROWNUM <= ' . $MaxDS;


        $rsERM = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('ERM', false));

        if($rsERM->AnzahlDatensaetze() == $MaxDS ){
            $this->Form->ZeileStart();
            $this->Form->Hinweistext('Es wurden mehr als ' . $MaxDS . ' Datensätze gefunden. Sollte der gesuchte Datensatz nicht in der Liste sein, bitte die Suche weiter einschränken',awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
            $this->Form->ZeileEnde();
        }

        $FeldBreiten['Checkbox'] = 16;
        $FeldBreiten['Kundennummer'] = 140;
        $FeldBreiten['Kundenname'] = 400;
        $FeldBreiten['Kartenanzahl'] = 100;

        $this->Form->ZeileStart();
        $this->Form->Erstelle_ListenCheckbox('checkAll', '', $FeldBreiten['Checkbox'], true, 'on', '', '', '', '', '-2');
        $this->Form->Erstelle_Liste_Ueberschrift('Kundennummer', $FeldBreiten['Kundennummer']);
        $this->Form->Erstelle_Liste_Ueberschrift('Kundenname', $FeldBreiten['Kundenname']);
        $this->Form->Erstelle_Liste_Ueberschrift('Kartenanzahl', $FeldBreiten['Kartenanzahl']);
        $this->Form->ZeileEnde();

        echo '<script> $( document ).ready(function() {
            $("#txtcheckAll").change(function() {
                $(".kdcheck").prop(\'checked\',  $("#txtcheckAll").prop(\'checked\'));
            });  
        }); 
            </script>';

        if ($rsERM->AnzahlDatensaetze() > 0) {

            while (!$rsERM->EOF()) {

                $this->Form->ZeileStart();
                $Attr = ' data-kdnr="'.$rsERM->FeldInhalt('KUNDEN_NR').'" ';
                $this->Form->Erstelle_ListenCheckbox('check_'.$rsERM->FeldInhalt('KUNDEN_NR'), $rsERM->FeldInhalt('KUNDEN_NR'), $FeldBreiten['Kundennummer'] / 10, $FeldBreiten['Kundennummer'],'on','','','','',0,false,$Attr,'kdcheck');
                $this->Form->Erstelle_ListenFeld('Kundennummer', $rsERM->FeldInhalt('KUNDEN_NR'), $FeldBreiten['Kundennummer'] / 10, $FeldBreiten['Kundennummer']);
                $this->Form->Erstelle_ListenFeld('Kundenname', $rsERM->FeldInhalt('NAME'), $FeldBreiten['Kundenname'] / 10, $FeldBreiten['Kundenname']);
                $this->Form->Erstelle_ListenFeld('Kartenanzahl', $rsERM->FeldInhalt('KARTENANZAHL'), $FeldBreiten['Kartenanzahl'] / 10, $FeldBreiten['Kartenanzahl']);
                $this->Form->ZeileEnde();

                $rsERM->DSWeiter();
            }

        } else {
            $this->Form->ZeileStart();
            $this->Form->Hinweistext($this->AWISSprachkonserven['Fehler']['err_keineDaten']);
            $this->Form->ZeileEnde();
        }
    }

    public function holeKartenTabelle()
    {
        return $this->_TBL_GROSSKUNDENKARTEN;
    }

    public function holeDestTabelle()
    {
        return $this->_TBL_GROSSKUNDEST;
    }

    public function isIE()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
            return true;
        }

        return true;
    }


    public function DruckSpeicherort($EAN = ''){
        return $this->_PDF_FILE . ($EAN!=''?'_'.$EAN:'') . '.pdf';
    }

    public function getQueueStatus(){
        return file_get_contents($this->_STATUS_FILE);
    }
}