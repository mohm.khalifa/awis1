<?php

//Blanko -> Formular VisitCheck

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('CV','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 10;

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

if($_GET['PEI_KEY'] == 0)
{
	$Ausdruck->_pdf->AddFont($Schriftart,'','');
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',16);
	//$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 20);
	$Ausdruck->_pdf->Image('../bilder/atulogo_neu.png', 21, 19, 18, 6);
	$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(120,8,'Check - Besuch','LTRB',0,'C',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
	$Zeile+=8;
	$Spalte+=30;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',18);
	//$Ausdruck->_pdf->cell(120,8,'AAAAA'.iconv("UTF-8","CP1250", "�?").'Check -  Besuch GBL/TKDL: Typ A','LTRB',0,'C',0);
	$Ausdruck->_pdf->cell(120,8,'Check -  Besuch GBL/TKDL: Typ A','LTRB',0,'C',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
	$Zeile+=8;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
	$Zeile+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 22.06.2012','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.2','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(30,5,'Version 1.3','LBTR',0,'C',0);
	$Zeile+=10;
	$Spalte-=30;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CV']['CV_FILIALE'].':','',0,'L',0);
	//$Spalte=+5;
	$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
	$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CV']['CV_BesuchszeitVon'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
	$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CV']['CV_BesuchszeitBis'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
	$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CV']['CV_Datum'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(75,4,'','','L',0);
	$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
	$Ausdruck->_pdf->cell2(20,4,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
	$Ausdruck->_pdf->cell2(75,4,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->cell2(5,4,'A.','TL',0,'C',1);
	$Ausdruck->_pdf->Multicell(70,4,substr($AWISSprachKonserven['CV']['CV_A'],3),'TRB','L',1);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',1);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_A1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 69.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 69.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_A2'],'TRB','L',0);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 77.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 77.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'B.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_B'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 93.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 93.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 101.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 101.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 109.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 109.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'4.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B4'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 117.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 117.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'5.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B5'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 125.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 125.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'6.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B6'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 133.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 133.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'C.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_C'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_C1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 149.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 149.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'D.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_D'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_D1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 165.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 165.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_D2'],'TRB','L',0);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 173.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 173.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_D3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 181.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 181.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'E.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_E'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 197.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 197.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);

	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 205.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 205.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);	

	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);

	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'4.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E4'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);	
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'F.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_F'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);	
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,8,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 245.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 245.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
/*	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F4'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	// Anforderung 1179, Ott, H., 28.03.2012
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F6'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 229.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 229.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	//Ende Anforderung 1179
	
	$Zeile+=8;
	$Spalte-=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Multicell(5,8,'4.','TLB','C',0);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F5'],'TRB','L',0);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
*/	
			
	
	$Zeile = 270;
	$Spalte = 15;	
	
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(60,4,'Unterschrift GL/WL:','T',0,'C',0);
	
	$Spalte+=120;
	
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(60,4,'Unterschrift GBL/TKDL:','T',0,'C',0);

	$Zeile+=14;
	$Spalte=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',9);
	$Ausdruck->_pdf->cell2(60,4,'Filialcheck GBL TKDL  - A -  ' . date('m.Y'),'',0,'L',0);	

	// Blanko-Ampel Aus Vertikal:
	///*
	$Zeile = 255;
	$Spalte = 101;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Image('../bilder/ampel02_aus.jpg',$Spalte,$Zeile,'','');
	//*/
	
	// Blanko-Ampel Voll Vertikal:
	/*
	$Zeile = 265;
	$Spalte = 98;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Image('../bilder/ampel02_voll.jpg',$Spalte,$Zeile,'','');
	*/	
	
	// Blanko-Ampel Aus Horizontal:
	/*
	$Zeile = 265;
	$Spalte = 92;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Image('../bilder/ampel_aus_neu_quer.jpg',$Spalte,$Zeile,'','');
	*/
	
	// Blanko-Ampel Voll Horizontal:
	/*
	$Zeile = 265;
	$Spalte = 92;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Image('../bilder/ampel_voll_neu_quer.jpg',$Spalte,$Zeile,'','');
	*/	
	
}
elseif($_GET['PEI_KEY'] != 0)
{
	
	$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
	$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
	$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
	$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
		
	$rsBericht = $DB->RecordSetOeffnen($SQL);	
	
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',16);
	//$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 20);
	$Ausdruck->_pdf->Image('../bilder/atulogo_neu.png', 21, 19, 18, 6);
	$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(120,8,'Check - Besuch','LTRB',0,'C',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
	$Zeile+=8;
	$Spalte+=30;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',18);
	$Ausdruck->_pdf->cell2(120,8,'Check - Besuch GBL/TKDL: Typ A','LTRB',0,'C',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
	$Zeile+=8;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
	$Zeile+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 22.06.2012','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.2','LTRB',0,'L',0);
	$Ausdruck->_pdf->cell2(30,5,'Version 1.3','LBTR',0,'C',0);
	$Zeile+=10;
	$Spalte-=30;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CV']['CV_FILIALE'].':','',0,'L',0);
	//$Spalte=+5;
	$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'C',0);
	$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CV']['CV_BesuchszeitVon'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
	$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CV']['CV_BesuchszeitBis'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
	$Ausdruck->_pdf->cell2(20,4,$AWISSprachKonserven['CV']['CV_Datum'].':','',0,'C',0);
	$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(75,4,'','','L',0);
	$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
	$Ausdruck->_pdf->cell2(20,4,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
	$Ausdruck->_pdf->cell2(75,4,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
	
	// Block A - Ueberschrift
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->cell2(5,4,'A.','TL',0,'C',1);
	$Ausdruck->_pdf->Multicell(70,4,substr($AWISSprachKonserven['CV']['CV_A'],3),'TRB','L',1);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',1);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	$Zeile+=8;
	$Spalte-=75;
	
	// Block A --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_A1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//Setze X und nicht X
	//-------------------
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=2';
	
	//echo $SQL;
	
	$rsA1 = $DB->RecordSetOeffnen($SQL);
	
	
	
	if($rsA1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 69.5, 5, 5),'LTRB','C',1);  	
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,69.5,97.5,74.5);
	  $Ausdruck->_pdf->Line(97.5, 69.5, 92.5, 74.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 69.5, 5, 5),'LTRB','C',1);  
	}
	
	if($rsA1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 69.5, 5, 5),'LTRB','L',1);  	
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,69.5,112,74.5);
	  $Ausdruck->_pdf->Line(112, 69.5, 107, 74.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 69.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsA1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsA1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block A --> 2.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_A2'],'TRB','L',0);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=3';
	
	$rsA2 = $DB->RecordSetOeffnen($SQL);
	
	if($rsA2->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 77.5, 5, 5),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,77.5,97.5,82.5);
	  $Ausdruck->_pdf->Line(97.5, 77.5, 92.5, 82.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 77.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsA2->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 77.5, 5, 5),'LTRB','L',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,77.5,112,82.5);
	  $Ausdruck->_pdf->Line(112, 77.5, 107, 82.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 77.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsA2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsA2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> Ueberschrift
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'B.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_B'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=5';
	
	$rsB1 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 93.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,93.5,97.5,98.5);
	  $Ausdruck->_pdf->Line(97.5, 93.5, 92.5, 98.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 93.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 93.5, 5, 5),'LTRB','L',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,93.5,112,98.5);
	  $Ausdruck->_pdf->Line(112, 93.5, 107, 98.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 93.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> 2.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=6';
	
	$rsB2 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB2->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 101.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,101.5,97.5,106.5);
	  $Ausdruck->_pdf->Line(97.5, 101.5, 92.5, 106.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 101.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB2->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 101.5, 5, 5),'LTRB','L',1);
	   $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,101.5,112,106.5);
	  $Ausdruck->_pdf->Line(112, 101.5, 107, 106.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 101.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> 3.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=7';
	
	$rsB3 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB3->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 109.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,109.5,97.5,114.5);
	  $Ausdruck->_pdf->Line(97.5, 109.5, 92.5, 114.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 109.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB3->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 109.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,109.5,112,114.5);
	  $Ausdruck->_pdf->Line(112, 109.5, 107, 114.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 109.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB3->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB3->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> 4.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'4.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B4'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=8';
	
	$rsB4 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB4->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 117.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,117.5,97.5,122.5);
	  $Ausdruck->_pdf->Line(97.5, 117.5, 92.5, 122.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 117.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB4->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 117.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,117.5,112,122.5);
	  $Ausdruck->_pdf->Line(112, 117.5, 107, 122.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 117.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB4->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB4->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block 4 --> 5.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'5.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B5'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=9';
	
	$rsB5 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB5->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 125.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,125.5,97.5,130.5);
	  $Ausdruck->_pdf->Line(97.5, 125.5, 92.5, 130.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 125.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB5->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 125.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,125.5,112,130.5);
	  $Ausdruck->_pdf->Line(112, 125.5, 107, 130.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 125.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB5->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB5->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block B --> 6.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'6.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_B6'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=10';
	
	$rsB6 = $DB->RecordSetOeffnen($SQL);
	
	if($rsB6->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 133.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,133.5,97.5,138.5);
	  $Ausdruck->_pdf->Line(97.5, 133.5, 92.5, 138.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 133.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsB6->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 133.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,133.5,112,138.5);
	  $Ausdruck->_pdf->Line(112, 133.5, 107, 138.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 133.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsB6->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsB6->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block C --> Ueberschrift
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'C.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_C'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block C --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_C1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=12';
	
	$rsC1 = $DB->RecordSetOeffnen($SQL);
	
	if($rsC1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 149.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,149.5,97.5,154.5);
	  $Ausdruck->_pdf->Line(97.5, 149.5, 92.5, 154.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 149.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsC1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 149.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,149.5,112,154.5);
	  $Ausdruck->_pdf->Line(112, 149.5, 107, 154.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 149.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsC1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsC1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block D --> Ueberschrift
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'D.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_D'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block D --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_D1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=14';
	
	$rsD1 = $DB->RecordSetOeffnen($SQL);
	
	if($rsD1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 165.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,165.5,97.5,170.5);
	  $Ausdruck->_pdf->Line(97.5, 165.5, 92.5, 170.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 165.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsD1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 165.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,165.5,112,170.5);
	  $Ausdruck->_pdf->Line(112, 165.5, 107, 170.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 165.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsD1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsD1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block D --> 2.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_D2'],'TRB','L',0);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=15';
	
	$rsD2 = $DB->RecordSetOeffnen($SQL);
	
	if($rsD2->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 173.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,173.5,97.5,178.5);
	  $Ausdruck->_pdf->Line(97.5, 173.5, 92.5, 178.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 173.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsD2->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 173.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,173.5,112,178.5);
	  $Ausdruck->_pdf->Line(112, 173.5, 107, 178.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 173.5, 5, 5),'LTRB','L',1);
	}
	
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsD2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsD2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block D --> 3.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_D3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=16';
	
	$rsD3 = $DB->RecordSetOeffnen($SQL);
	
	if($rsD3->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 181.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,181.5,97.5,186.5);
	  $Ausdruck->_pdf->Line(97.5, 181.5, 92.5, 186.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 181.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsD3->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 181.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,181.5,112,186.5);
	  $Ausdruck->_pdf->Line(112, 181.5, 107, 186.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 181.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsD3->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsD3->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block E --> Ueberschrift
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'E.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_E'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block E --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=18';
	
	$rsE1 = $DB->RecordSetOeffnen($SQL);
	
	if($rsE1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 197.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,197.5,97.5,202.5);
	  $Ausdruck->_pdf->Line(97.5, 197.5, 92.5, 202.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 197.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsE1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 197.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,197.5,112,202.5);
	  $Ausdruck->_pdf->Line(112, 197.5, 107, 202.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 197.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsE1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsE1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block E --> 2.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=401';
	
	$rsE2 = $DB->RecordSetOeffnen($SQL);
	
	if($rsE2->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 205.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,205.5,97.5,210.5);
	  $Ausdruck->_pdf->Line(97.5, 205.5, 92.5, 210.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 205.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsE2->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 205.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,205.5,112,210.5);
	  $Ausdruck->_pdf->Line(112, 205.5, 107, 210.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 205.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsE2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsE2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;	
	
	// Block E --> 3.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E3'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=402';
	
	$rsE3 = $DB->RecordSetOeffnen($SQL);
	
	if($rsE3->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,213.5,97.5,218.5);
	  $Ausdruck->_pdf->Line(97.5, 213.5, 92.5, 218.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsE3->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,213.5,112,218.5);
	  $Ausdruck->_pdf->Line(112, 213.5, 107, 218.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsE3->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsE3->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;

	// Block E --> 4.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'4.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_E4'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=405';
	
	$rsE4 = $DB->RecordSetOeffnen($SQL);
	
	if($rsE4->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,221.5,97.5,226.5);
	  $Ausdruck->_pdf->Line(97.5, 221.5, 92.5, 226.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsE4->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,221.5,112,226.5);
	  $Ausdruck->_pdf->Line(112, 221.5, 107, 226.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsE4->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsE4->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;	
	
	// Block F --> Ueberschrift
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->SetFillColor(190,190,190);
	$Ausdruck->_pdf->Multicell(6,8,'F.','TLB','C',1);
	$Spalte+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->Multicell(70,8,substr($AWISSprachKonserven['CV']['CV_F'],3),'TRB','L',1);
	$Spalte+=70;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(10,8,'','LTRB','C',1);
	$Ausdruck->_pdf->cell2(20,8,'','LTRB','L',1);
	$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;
	
	// Block F --> 1.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F1'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=404';
	
	$rsF1 = $DB->RecordSetOeffnen($SQL);
	
	if($rsF1->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,237.5,97.5,242.5);
	  $Ausdruck->_pdf->Line(97.5, 237.5, 92.5, 242.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsF1->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,237.5,112,242.5);
	  $Ausdruck->_pdf->Line(112, 237.5, 107, 242.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsF1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;	
	
	
	// Block F --> 2.
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,8,'2.','TL',0,'C',0);
	$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F2'],'TRB','L',0);
	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
	$Zeile-=4;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Spalte+=75;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=403';
	
	$rsF2 = $DB->RecordSetOeffnen($SQL);
	
	if($rsF2->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 245.5, 5, 5),'LTRB','C',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(92.5,245.5,97.5,250.5);
	  $Ausdruck->_pdf->Line(97.5, 245.5, 92.5, 250.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 245.5, 5, 5),'LTRB','C',1);
	}
	
	if($rsF2->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 245.5, 5, 5),'LTRB','L',1);
		
	  $Ausdruck->_pdf->SetLineWidth(1); 
	  $Ausdruck->_pdf->Line(107,245.5,112,250.5);
	  $Ausdruck->_pdf->Line(112, 245.5, 107, 250.5);
	  $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else 
	{
	  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 245.5, 5, 5),'LTRB','L',1);
	}
	$Ausdruck->_pdf->SetFont($Schriftart,'',6);
	$Text = substr($rsF2->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF2->FeldInhalt('PEZ_BEMERKUNG'),70,70);
	$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
	$Zeile+=8;
	$Spalte-=75;	
	
	
	/*
	// OP 20.12.2011 ID=400 ist alter Bericht bis 31.12.2011. 
	// Wenn hier kein Ergebnis kommt diesen Punkt nicht anzeigen
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=400';
	
	$rsF1 = $DB->RecordSetOeffnen($SQL);
	
	//Block F --> 1. --> alter Stand bis 31.12.2011
	if(!is_null($rsF1->FeldInhalt('PEZ_WERT')) && $rsF1->FeldInhalt('PEZ_WERT') != '')
	{
		// f�r Berechnung der Prozente weiter unten
		$AnzFelder = 13;
		
		$Zeile+=8;
		$Spalte-=75;
		
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->Multicell(5,8,'','TLB','C',0);
		$Spalte+=5;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F1'],'TRB','L',0);
		$Spalte+=70;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		
		
		
		if($rsF1->FeldInhalt('PEZ_WERT') == '1')
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
		  
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(92.5,213.5,97.5,217.5);
		  $Ausdruck->_pdf->Line(97.5, 213.5, 92.5, 217.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
		}
		
		if($rsF1->FeldInhalt('PEZ_WERT') == '0')
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(107, 213.5,112,217.5);
		  $Ausdruck->_pdf->Line(112, 213.5, 107, 217.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
		}	
		$Ausdruck->_pdf->SetFont($Schriftart,'',6);
		$Text = substr($rsE1->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsE1->FeldInhalt('PEZ_BEMERKUNG'),70,70);
		$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
		// Ausgabe der drei Punkte ...
		$Zeile+=8;
		$Spalte-=75;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->Multicell(5,8,'','TLB','C',0);
		$Spalte+=5;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F2'],'TRB','L',0);
		$Spalte+=70;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
		$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
		$Ausdruck->_pdf->cell2(75,8,'','LTRB','L',1);
		
	}
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=401';
		
	$rsF3 = $DB->RecordSetOeffnen($SQL);
	
	if(!is_null($rsF3->FeldInhalt('PEZ_WERT')) && $rsF3->FeldInhalt('PEZ_WERT') != '')
	{
		//Ab 01.01.2012 --> drei neue Felder
		
		//f�r Berechnung der Prozent weiter unten
		$AnzFelder = 16;
		
		$Zeile+=8;
		$Spalte-=75;
		
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'1.','TL',0,'C',0);
		$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F3'],'TRB','L',0);
		$Zeile+=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
		$Zeile-=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Spalte+=75;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		
		if($rsF3->FeldInhalt('PEZ_WERT') == '1')
		{
		  // Rechteck und das K�stchen
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(92.5,213.5,97.5,218.5);
		  $Ausdruck->_pdf->Line(97.5, 213.5, 92.5, 218.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
			// Rechteck und das K�stchen leer
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 213.5, 5, 5),'LTRB','C',1);
		}
		
		if($rsF3->FeldInhalt('PEZ_WERT') == '0')
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(107,213.5,112,218.5);
		  $Ausdruck->_pdf->Line(112, 213.5, 107, 218.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 213.5, 5, 5),'LTRB','L',1);
		}	
		$Ausdruck->_pdf->SetFont($Schriftart,'',6);
		$Text = substr($rsF3->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF3->FeldInhalt('PEZ_BEMERKUNG'),70,70);
		$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	
		//*************************************************************************************************************************
		//*************************************************************************************************************************
	}
		
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=402';
		
	$rsF4 = $DB->RecordSetOeffnen($SQL);
		
	if(!is_null($rsF4->FeldInhalt('PEZ_WERT')) && $rsF4->FeldInhalt('PEZ_WERT') != '')
	{
		
		//f�r Berechnung der Prozent weiter unten
		$AnzFelder = 16;
		
		$Zeile+=8;
		$Spalte-=75;
	
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'2.','TL',0,'C',0);
		$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F4'],'TRB','L',0);
		$Zeile+=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
		$Zeile-=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Spalte+=75;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		
		
		
		if($rsF4->FeldInhalt('PEZ_WERT') == '1')
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
		  
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(92.5,221.5,97.5,226.5);
		  $Ausdruck->_pdf->Line(97.5, 221.5, 92.5, 226.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 221.5, 5, 5),'LTRB','C',1);
		}
		
		if($rsF4->FeldInhalt('PEZ_WERT') == '0')
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(107, 221.5,112,226.5);
		  $Ausdruck->_pdf->Line(112, 221.5, 107, 226.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 221.5, 5, 5),'LTRB','L',1);
		}	
		$Ausdruck->_pdf->SetFont($Schriftart,'',6);
		$Text = substr($rsF4->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF4->FeldInhalt('PEZ_BEMERKUNG'),70,70);
		$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	}
		//*************************************************************************************************************************
		//*************************************************************************************************************************
	
	// Anforderung 1179, Ott, H., 28.03.2012
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=404';
		
	$rsF6 = $DB->RecordSetOeffnen($SQL);
		
	if(!is_null($rsF6->FeldInhalt('PEZ_WERT')) && $rsF6->FeldInhalt('PEZ_WERT') != '')
	{
		
		//f�r Berechnung der Prozent weiter unten
		$AnzFelder = 16;
		
		$Zeile+=8;
		$Spalte-=75;
	
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'3.','TL',0,'C',0);
		$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_F6'],'TRB','L',0);
		$Zeile+=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->cell2(5,4,'','LB',0,'C',0);
		$Zeile-=4;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Spalte+=75;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		
		
		
		if($rsF6->FeldInhalt('PEZ_WERT') == '1')
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 229.5, 5, 5),'LTRB','C',1);
		  
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(92.5,229.5,97.5,234.5);
		  $Ausdruck->_pdf->Line(97.5, 229.5, 92.5, 234.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 229.5, 5, 5),'LTRB','C',1);
		}
		
		if($rsF6->FeldInhalt('PEZ_WERT') == '0')
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 229.5, 5, 5),'LTRB','L',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(107, 229.5,112,234.5);
		  $Ausdruck->_pdf->Line(112, 229.5, 107, 234.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 229.5, 5, 5),'LTRB','L',1);
		}	
		$Ausdruck->_pdf->SetFont($Schriftart,'',6);
		$Text = substr($rsF6->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF6->FeldInhalt('PEZ_BEMERKUNG'),70,70);
		$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	}
	//Ende Anforderung 1179
	
		//*************************************************************************************************************************
		//*************************************************************************************************************************
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID=403';
			
	$rsF5 = $DB->RecordSetOeffnen($SQL);
	
	if(!is_null($rsF5->FeldInhalt('PEZ_WERT')) && $rsF5->FeldInhalt('PEZ_WERT') != '')
	{
	
		//f�r Berechnung der Prozent weiter unten
		$AnzFelder = 16;
		
		$Zeile+=8;
		$Spalte-=75;
		
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont($Schriftart,'',10);
		$Ausdruck->_pdf->Multicell(5,8,'4.','TLB','C',0);
		$Spalte+=5;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->Multicell(70,8,$AWISSprachKonserven['CV']['CV_F5'],'TRB','L',0);
		$Spalte+=70;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		
		
		
		if($rsF5->FeldInhalt('PEZ_WERT') == '1')
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
		  
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(92.5,237.5,97.5,242.5);
		  $Ausdruck->_pdf->Line(97.5, 237.5, 92.5, 242.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(92.5, 237.5, 5, 5),'LTRB','C',1);
		}
		
		if($rsF5->FeldInhalt('PEZ_WERT') == '0')
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
			
		  $Ausdruck->_pdf->SetLineWidth(1); 
		  $Ausdruck->_pdf->Line(107, 237.5,112,242.5);
		  $Ausdruck->_pdf->Line(112, 237.5, 107, 242.5);
		  $Ausdruck->_pdf->SetLineWidth(0); 
		}
		else 
		{
		  $Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(107, 237.5, 5, 5),'LTRB','L',1);
		}	
		$Ausdruck->_pdf->SetFont($Schriftart,'',6);
		$Text = substr($rsF5->FeldInhalt('PEZ_BEMERKUNG'),0,70)."\n".substr($rsF5->FeldInhalt('PEZ_BEMERKUNG'),70,70);
		$Ausdruck->_pdf->cell2(75,8,$Text,'LTRB','L',1);
	}*/
	
	
	$Zeile = 270;
	$Spalte = 15;	
	
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(60,4,'Unterschrift GL/WL:','T',0,'C',0);
	
	$Spalte+=120;
	
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->cell2(60,4,'Unterschrift GBL/TKDL:','T',0,'C',0);

	$Zeile+=14;
	$Spalte=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',9);
	$Ausdruck->_pdf->cell2(60,4,'Filialcheck GBL TKDL  - A -  ' . date('m.Y'),'',0,'L',0);	
	
	
	//Auswertung
	$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
	$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";
	
	$rsGesamt = $DB->RecordSetOeffnen($SQL);
	
	$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');
	
	//echo "Gesamtanzahl:".$Gesamtanzahl;
	
	$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
	$SQL .= "AND PEZ_WERT = '0'";
	
	$GesamtFalsch = $DB->RecordSetOeffnen($SQL);
	
	$Falsch = $GesamtFalsch->FeldInhalt('ANZ');
	
	//echo "Falsch".$Falsch;
	
	$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) * 100); 
	
	$Zeile = 255;
	$Spalte = 101;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);	
	
	if($Prozentsatz >= 90 && $Prozentsatz <= 100)
	{
		$Ausdruck->_pdf->Image('../bilder/ampel02_gruen.jpg',$Spalte,$Zeile,'','');
		//$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
		//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
		$Zeile+=18;
		$Spalte=15;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->cell2(180,5,'(GR�N)','',0,'C',0);
	}
	elseif($Prozentsatz >= 80 && $Prozentsatz <= 90)
	{
		$Ausdruck->_pdf->Image('../bilder/ampel02_gelb.jpg',$Spalte,$Zeile,'','');
		//$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
		//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
		$Zeile+=18;
		$Spalte=15;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->cell2(180,5,'(GELB)','',0,'C',0);
	}
	elseif($Prozentsatz < 80)
	{
		$Ausdruck->_pdf->Image('../bilder/ampel02_rot.jpg',$Spalte,$Zeile,'','');
		//$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
		//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
		$Zeile+=18;
		$Spalte=15;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->cell2(180,5,'(ROT)','',0,'C',0);
	}
	
	$Spalte = 15;
	$Zeile = 277;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',8);
	$Ausdruck->_pdf->cell2(180,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
		
}


$Ausdruck->Anzeigen();


?>