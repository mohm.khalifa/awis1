--  Datei erstellt -Mittwoch-September-13-2017
--------------------------------------------------------

REM INSERTING into OTFORMULARE
SET DEFINE OFF;
UPDATE "AWIS"."OTFORMULARE" SET OFO_HINWEISE = '<b>Bitte beachten:<br>
Verwenden Sie dieses Formular bitte nicht f�r Erweiterungen/�nderungen von Berechtigungen, <br>
Freischaltung von weiteren Programmen, �nderung von Ordnernamen oder Anpassung von Laufwerksfreischaltungen.</b>' WHERE OFO_KEY = 2;

REM INSERTING into OTFORMULARE
SET DEFINE OFF;
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_benachrichtigungan', 'OTF_FRM_007', 'Benachrichtigung an:', 'Benachrichtigung an:', 'Benachrichtigung an:', 'Benachrichtigung an:', 'Benachrichtigung an:', 'Benachrichtigung an:', 'Benachrichtigung an:', TO_DATE('2017-09-13 14:42:26', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_titel', 'OTF_FRM_007', 'Titel', 'Titel', 'Titel', 'Titel', 'Titel', 'Titel', 'Titel', TO_DATE('2017-09-13 14:42:29', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_istzustand', 'OTF_FRM_007', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', 'Ist-Zustand oder Problembeschreibung', TO_DATE('2017-09-13 14:43:11', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_sollzustand', 'OTF_FRM_007', 'Zielsetzung', 'Zielsetzung', 'Zielsetzung', 'Zielsetzung', 'Zielsetzung', 'Zielsetzung', 'Zielsetzung', TO_DATE('2017-09-13 14:43:29', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_nutzen_vorteil', 'OTF_FRM_007', 'Nutzen/Vorteil', 'Nutzen/Vorteil', 'Nutzen/Vorteil', 'Nutzen/Vorteil', 'Nutzen/Vorteil', 'Nutzen/Vorteil', 'Nutzen/Vorteil', TO_DATE('2017-09-13 14:43:44', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_allgbemerkungen', 'OTF_FRM_007', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', 'Allgemeine Bemerkungen:', TO_DATE('2017-09-13 14:44:28', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_TEXT_FR, XTX_TEXT_CH, XTX_TEXT_AT, XTX_USERDAT) VALUES ('txt_loesungsvorschlag', 'OTF_FRM_007', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', 'L&ouml;sungsvorschlag', TO_DATE('2017-09-13 14:45:10', 'YYYY-MM-DD HH24:MI:SS'));

COMMIT;