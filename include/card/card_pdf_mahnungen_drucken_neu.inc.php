<?php

define("FPDF_FONTPATH","/daten/include/card/fpdf/font/");
require_once("card/card_pdf.inc.php");


class PDF_MDR extends PDF
{
	//Klassenvariablen
	
	//Kopfzeile
	var $dblKopfFeldAbstand;
	var $arrKopfSpaltenbreiten;
	var $arrKopfText;
	var $arrKopfUmrandung;
	var $arrKopfPos;
	var $arrKopfAusrichtung;
	var $dblSchriftgroesseHeader;
	var $dblZeilenhoeheHeader;
		
	var $dblLinienStaerke;
	var $intSeite1;
	var $dblZeilenbreiteDrittel;
	var $strDatum;
	var $strZeit;
	var $strTitel;
	var $strUnterTitel;
	
	var $dblHoeheDTA;
	var $dblHoeheName;
	var $dblHoeheDaten;
	var $dblHoeheSumme;
	var $dblHoeheSummeKd;
	var $dblYFusszeile;
	var $dblAbstandFusszeile;
	var $dblYGrenzwert;
	
	//Konstruktor
	function PDF_MDR($strAusrichtung='P',$strEinheit='mm',$strFormat='A4',$dblKorrekturX=0,$dblKorrekturY=0,
	$dblSeitenrandLinks=10,$dblSeitenrandOben=10,$dblSeitenrandRechts=10,$dblSeitenrandUnten=10,$intAutomatSeite=1,$strTitel='',$strUnterTitel='')
	{
		//Klassenvariablen vorbelegen
		
		//Kopfzeile
		/*$this->dblKopfFeldAbstand=1;
		$this->arrKopfSpaltenbreiten=array(40,30,20,30,20,30,30,30,0);
		$this->arrKopfText=array("RG-Nr","RG-Betrag","MG -","G-Betrag","MG +","ZG-EG","Offen","Zahlungsziel","Mahnstand");
		$this->arrKopfUmrandung=array(0,0,0,0,0,0,0,0,0);
		$this->arrKopfPos=array(0,0,0,0,0,0,0,0,1);
		$this->arrKopfAusrichtung=array('L','C','C','C','C','C','C','C','L');
		$this->dblSchriftgroesseHeader=12;
		$this->dblZeilenhoeheHeader=$this->ptinmm($this->dblSchriftgroesseHeader)+2;
		
		$this->dblLinienStaerke=0.2;
		$this->intSeite1=1;
		$this->dblZeilenbreiteDrittel=(297-$dblSeitenrandLinks-$dblSeitenrandRechts)/3;
		setlocale("LC_TIME","de_DE");
		$this->strDatum=strftime("%A, ").trim(strftime("%e.")).strftime(" %B %Y");
		$this->strZeit=strftime("%H:%M:%S");
		$this->strTitel=$strTitel;
		$this->strUnterTitel=$strUnterTitel;
		
		$this->dblHoeheDTA=0;
		$this->dblHoeheName=0;
		$this->dblHoeheDaten=0;
		$this->dblHoeheSumme=0;
		$this->dblHoeheSummeKd=0;
		$this->dblYFusszeile=15;
		$this->dblAbstandFusszeile=0;
		$this->dblYGrenzwert=210-$this->dblYFusszeile-$this->dblAbstandFusszeile;*/
	
		//Aufruf des Konstruktors in der Klasse PDF
		PDF::PDF($strAusrichtung,$strEinheit,$strFormat,$dblKorrekturX,$dblKorrekturY,$dblSeitenrandLinks,
		$dblSeitenrandOben,$dblSeitenrandRechts,$dblSeitenrandUnten,$intAutomatSeite);
		/*$this->SetAutoPageBreak(false);
		$this->AddPage();
		$this->SetLineWidth($this->dblLinienStaerke);
		$this->AliasNbPages();*/
	}
	
	function Footer()	//Fusszeile
	{$dblSchriftgroesse=7;
		
		$this->SetFont('Helvetica','',$dblSchriftgroesse);
		
		$dblZeilenhoehe=3;//$this->ptinmm($dblSchriftgroesse)+1;
		$dblZeilenbreite=30;
		
		$x_pos=20;
		$y_pos=263;
		$this->SetXY($x_pos,$y_pos);	
		
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"ATU Auto-Teile-Unger",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"GmbH & Co. KG",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Verwaltung:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Dr.-Kilian-Stra�e 11",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"92637 Weiden",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Postfach",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"92633 Weiden i.d.OPf.",'',0,'',0);
		
		$dblZeilenbreite=32;
		
		$x_pos=50;
		$y_pos=263;
		$this->SetXY($x_pos,$y_pos);	
		
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Zentrallager:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Dr.-Kilian-Stra�e 4/12",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"92637 Weiden i.d.OPf.",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Zentrale:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Tel. +49 (0)961 306-0",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Durchwahl:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Tel. +49 (0)961 306-5830",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Fax +49 (0)961 306-5840",'',0,'',0);
		
		$dblZeilenbreite=35;
		
		$x_pos=82;
		$y_pos=263;
		$this->SetXY($x_pos,$y_pos);	
		
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Kommanditgesellschaft,",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Sitz Weiden i.d.OPf.",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Registergericht",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Weiden i.d.OPf., HRA 1312",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"UST-Id Nr.: DE134043104",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Steuer-Nr.: 255/150/02505",'',0,'',0);
		
		$dblZeilenbreite=50;
		
		$x_pos=117;
		$y_pos=263;
		$this->SetXY($x_pos,$y_pos);	
		
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Pers�nlich haftende Gesellschafterin:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"ATU Auto-Teile-Unger GmbH,",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Sitz Weiden i.d.OPf.,",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Registergericht Weiden i.d.OPf., HRB 745",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Gesch�ftsf�hrer: Karsten Engel, G�nter",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"F�ckersperger, Dirk M�ller, Manfred Ries",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Gerichtsstand Weiden i.d.OPf.",'',0,'',0);
		
		$dblZeilenbreite=35;
		
		$x_pos=167;
		$y_pos=263;
		$this->SetXY($x_pos,$y_pos);	
		
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Bankverbindung:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"HypoVereinsbank Weiden",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"BLZ 753 200 75",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"Kto. 348 651 072",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"SWIFT-CODE:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"HYVEDEMM454",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"IBAN:",'',0,'',0);
		$y_pos=$y_pos+$dblZeilenhoehe;
		$this->SetXY($x_pos,$y_pos);	
		$this->Cell($dblZeilenbreite,$dblZeilenhoehe,"DE40 753200750348651072",'',0,'',0);
	}
	
	
	
	function briefkopf_atu($dblKorrPosX=0,$dblKorrPosY=0)
	{
		//Korrektur des Briefkopfes
	
		$dblKorrPosX+=$this->dblKorrekturX;
		$dblKorrPosY+=$this->dblKorrekturY;
		
		$strBild='/daten/web/bilder/atulogo_grau.jpg';
		//$strBild='/daten/include/card/atu_logo.jpg';
		
		$dblPosBildX=162;			//Bildposition X in mm
		$dblPosBildY=21;			//Bildposition Y in mm
		
		//$intOrigPixBreite=1838;	//Bildbreite Originalbild in Pixel
		$intOrigPixBreite=1655;	//Bildbreite Originalbild in Pixel
		//$intOrigPixHoehe=661;	//Bildhoehe Originalbild in Pixel
		$intOrigPixHoehe=775;	//Bildhoehe Originalbild in Pixel
		
		//$dblmmBreite=52;		//Gew�nschte Bildbreite in mm
		$dblmmHoehe=$dblmmBreite*$intOrigPixHoehe/$intOrigPixBreite;	//Bildhoehe in mm
		$dblmmBreite=42;
		//$dblmmHoehe=20;
			
		$this->Image($strBild,$dblPosBildX+$dblKorrPosX,$dblPosBildY+$dblKorrPosY,$dblmmBreite,$dblmmHoehe);
		
		$this->SetFont('Helvetica','',7);
		
		//$this->SetFontSize(7);
		
		$this->Text(22+$dblKorrPosX,51+$dblKorrekturY,'ATU 92633 Weiden i. d. OPf.');
		//$this->Line(20+$dblKorrekturX,45.5+$dblKorrekturY,64.5+$dblKorrekturX,45.5+$dblKorrekturY);

		$this->SetFont('Helvetica','',7);
		$this->Text(22+$dblKorrPosX,96+$dblKorrekturY,'Ihre Nachricht vom');	
		$this->Text(67+$dblKorrPosX,96+$dblKorrekturY,'Ihr Zeichen');	
		$this->Text(112+$dblKorrPosX,96+$dblKorrekturY,'Unser Zeichen');	
		$this->Text(157+$dblKorrPosX,96+$dblKorrekturY,'Datum');	
		
		$this->SetFont('Helvetica','',10);
		$this->Text(157+$dblKorrPosX,100+$dblKorrekturY,strftime("%d.%m.%Y"));			
		
		$this->SetXY(2,101);
		$this->Write(5,'___');
	}
	
	function neueseite_atu($dblKorrPosX=0,$dblKorrPosY=0)
	{
		//Korrektur des Briefkopfes
	
		$dblKorrPosX+=$this->dblKorrekturX;
		$dblKorrPosY+=$this->dblKorrekturY;
		
		$strBild='/daten/web/bilder/atulogo_grau.jpg';
		//$strBild='/daten/include/card/atu_logo.jpg';
		
		$dblPosBildX=162;			//Bildposition X in mm
		$dblPosBildY=21;			//Bildposition Y in mm
		
		//$intOrigPixBreite=1838;	//Bildbreite Originalbild in Pixel
		$intOrigPixBreite=1655;	//Bildbreite Originalbild in Pixel
		//$intOrigPixHoehe=661;	//Bildhoehe Originalbild in Pixel
		$intOrigPixHoehe=775;	//Bildhoehe Originalbild in Pixel
		
		//$dblmmBreite=52;		//Gew�nschte Bildbreite in mm
		$dblmmHoehe=$dblmmBreite*$intOrigPixHoehe/$intOrigPixBreite;	//Bildhoehe in mm
		$dblmmBreite=42;
		//$dblmmHoehe=20;
			
		$this->Image($strBild,$dblPosBildX+$dblKorrPosX,$dblPosBildY+$dblKorrPosY,$dblmmBreite,$dblmmHoehe);
	}
	
	
	function adressfeld($strAdresstext,$dblAdrPosX,$dblAdrPosY,$dblZellenhoehe=0,$dblZellenbreite=0,$dblKorrPosX=0,$dblKorrPosY=0)
	{
		//Korrektur des adressfeldes
	
		$dblKorrPosX+=$this->dblKorrekturX;
		$dblKorrPosY+=$this->dblKorrekturY;
		
		$this->SetXY($dblAdrPosX+$dblKorrPosX,$dblAdrPosY+$dblKorrPosY);
		$this->MultiCell($dblZellenbreite,$dblZellenhoehe,$strAdresstext);
	}
	
	//Gibt die aktuelle Position aus
	
	function position()
	{
		$this->SetFont('Helvetica','',16);
	
		$strAusgabe=$this->GetX()." / ".$this->GetY();
		
		$this->SetXY(-10,-10);
		
		$this->SetFillColor(255,255,0);
		$this->SetDrawColor(0,0,255);
		$this->SetTextColor(0,0,255);
		
		$this->Cell(60,10,$strAusgabe,1,0,'C',1);
	}
	
	/*function zeileOffene_Vorgaenge($dblZeilenhoehe,$dblFeldAbstand,$arrSpaltenbreiten,$arrText,$arrUmrandung,$arrPos,$arrAusrichtung)
	{		
		for($i=0;$i<count($arrSpaltenbreiten);$i++)
		{
			if($i<count($arrSpaltenbreiten)-1)
				$dblSpaltenbreite=$arrSpaltenbreiten[$i]-$dblFeldAbstand;
			else
				$dblSpaltenbreite=$arrSpaltenbreiten[$i];
			
		
			$this->Cell($dblSpaltenbreite,$dblZeilenhoehe,$arrText[$i],$arrUmrandung[$i],$arrPos[$i],$arrAusrichtung[$i]);
			
			if($i<count($arrSpaltenbreiten)-1)
				$this->SetX($this->GetX()+$dblFeldAbstand);
		}
	}
	
	function Header()
	{
		$this->SetFillColor(192,192,192);
		$this->SetDrawColor(0,0,0);
		$this->SetTextColor(0,0,0);
		
		if($this->intSeite1)
		{
			//Titelzeile
			$dblSchriftgroesseTitel=20;
			$dblSchriftgroesseUnterTitel=8;
			
			$dblZeilenhoeheTitel=$this->ptinmm($dblSchriftgroesseTitel+1);
			$dblZeilenhoeheUnterTitel=$this->ptinmm($dblSchriftgroesseUnterTitel+1);
			
			
			$aktX=$this->GetX();
			$aktY=$this->GetY();
			
			$this->Cell(0,$dblZeilenhoeheTitel,"",'T',1,'L',1);
			
			$this->SetXY($aktX,$aktY);
			
			$this->SetFont('Helvetica','B',$dblSchriftgroesseTitel);
			
			$this->Cell(0,$dblZeilenhoeheTitel,$this->strTitel,0,1,'C');
			
			$aktX=$this->GetX();
			$aktY=$this->GetY();
			
			$this->Cell(0,$dblZeilenhoeheUnterTitel,"",'B',1,'L',1);
			
			$this->SetXY($aktX,$aktY);
						
			$this->SetFont('Helvetica','I',$dblSchriftgroesseUnterTitel);
			
			$this->Cell(0,$dblZeilenhoeheUnterTitel,$this->strUnterTitel,0,1,'C');
			
			
			$this->intSeite1=0;
		}
		
		//Kopfzeile
				
		$this->SetFont('Helvetica','B',$this->dblSchriftgroesseHeader);
		
		$this->zeileOffene_Vorgaenge($this->dblZeilenhoeheHeader,$this->dblKopfFeldAbstand,$this->arrKopfSpaltenbreiten,$this->arrKopfText,
		$this->arrKopfUmrandung,$this->arrKopfPos,$this->arrKopfAusrichtung);
	}
	
	function Footer()
	{
		$this->SetY($this->dblYGrenzwert);
		$dblSchriftgroesse=10.5;
		$this->SetFont('Helvetica','I',$dblSchriftgroesse);
		$dblZeilenhoehe=$this->ptinmm($dblSchriftgroesse)+1;
		
		$this->SetLineWidth($this->dblLinienStaerke);
		$this->Cell(0,$dblZeilenhoehe,"",'B',1);
			
		$this->zeileOffene_Vorgaenge($dblZeilenhoehe,0,array($this->dblZeilenbreiteDrittel,$this->dblZeilenbreiteDrittel,0),
		array($this->strDatum,"Seite ".$this->PageNo().' von {nb}',$this->strZeit),
		array(0,0,0),array(0,0,1),array('L','C','R'));
	}
	
	
	//Pr�ft ob ein Zeilenumbruch erfolgen soll
	function pruefUmbruch($strRgDatAkt,$strRgDatVor,$strRgDatNach,$intKundennr=0,$strKundenNrAkt='',$strKundenNrVor='',$strKundenNrNach='')
	{
		$dblYPos=$this->GetY();
		if(!$intKundennr)
		{
			$dblYPos+=$this->dblHoeheName+$this->dblHoeheDaten;
			
			if($strRgDatAkt!=$strRgDatVor)	//Zeile DTA
			{
				$dblYPos+=$this->dblHoeheDTA;
			}
			
			if($strRgDatAkt!=$strRgDatNach)	//Zeile Summe
			{
				$dblYPos+=$this->dblHoeheSumme;
			}			
		}
		else
		{
			if($strKundenNrAkt!=$strKundenNrVor)
			{
				$dblYPos+=$this->dblHoeheName;
			}
			
			if($strKundenNrAkt!=$strKundenNrNach)
			{
				$dblYPos+=$this->dblHoeheSummeKd;
			}
			
			
			if($strKundenNrAkt!=$strKundenNrVor||$strRgDatAkt!=$strRgDatVor)
			{
				$dblYPos+=$this->dblHoeheDTA;
			}
			
			if($strKundenNrAkt!=$strKundenNrNach||$strRgDatAkt!=$strRgDatNach)
			{
				$dblYPos+=$this->dblHoeheSumme;
			}
		
			$dblYPos+=$this->dblHoeheDaten;
		}
		
		if($dblYPos>$this->dblYGrenzwert)
		{
			$this->AddPage();
		}
	}
	
	function pruefUmbruchSchleife($dblHoehe)
	{
		$dblYPos=$this->GetY();
		$dblYPos+=$dblHoehe;
	
		if($dblYPos>$this->dblYGrenzwert)
		{
			$this->AddPage();
		}
	}*/
}
?>
