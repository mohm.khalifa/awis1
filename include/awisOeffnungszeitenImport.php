<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisFormular.inc';

class awisOeffnungszeitenImport
{

    //Trenner
    const TZ = ';';

    //Fehler
    const ERR_TRENNZEICHEN = -1;
    const ERR_SPALTENANZAHL = -2;
    const ERR_FEHLENDEUEBERSCHRIFT = -3;
    const ERR_FOZ_ID_UNBEKANNT = -4;
    const ERR_DATUM_FALSCH = -5;
    const ERR_UNERWARTET = -99;

    //Rückgabewerte im Gutfall
    const DS_ANGELEGT = 1;
    const DS_GEUPDATED = 2;
    const DS_BEREITSVORHANDEN = 3;

    private $DB;

    private $Benutzer;

    private $Form;

    private $DateiSpalten;

    private $DSProtokoll;

    private $DebugLevel = 999;

    private $Debug;

    public function __construct()
    {
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Benutzer = awisBenutzer::Init();
        $this->Form = new awisFormular();

        $this->initDateiSpalten();
    }

    public function __destruct()
    {
        $this->Form->DebugAusgabe(1,$this->Debug);
    }

    private function initDateiSpalten()
    {

        $this->DateiSpalten['FIL_ID'] = 'FIL_ID';
        $this->DateiSpalten['GUELTIG_AB'] = 'GUELTIG_AB';
        $this->DateiSpalten['MODELL'] = 'MODELL';
    }


    public function ImportiereDatei($Pfad)
    {

        try {

            $Dateihandle = fopen($Pfad, 'r');
            $Ueberschriften = trim(fgets($Dateihandle));


            $this->Log('Überschrift: ' . $Ueberschriften,999);
            $this->PruefeTrennzeichen($Ueberschriften);
            $this->PruefeSpaltenanzahl($Ueberschriften);

            $Ueberschriften = explode(self::TZ, $Ueberschriften);
            $Ueberschriften = array_flip($Ueberschriften);

            $this->PruefeUeberschriftenArray($Ueberschriften);


            while (($Zeile = fgets($Dateihandle)) !== false) {

                $this->Log('Lese Zeile: ' . $Zeile,999);
                if ($Zeile != '') {
                    $this->PruefeTrennzeichen($Zeile);
                    $this->PruefeSpaltenanzahl($Zeile);
                    $ZeileArray = explode(self::TZ, $Zeile);
                    $FIL_ID = $ZeileArray[$Ueberschriften['FIL_ID']];
                    $Gueltigab = $ZeileArray[$Ueberschriften['GUELTIG_AB']];
                    $ModellId = $ZeileArray[$Ueberschriften['MODELL']];

                    $RetVal = $this->MergeOeffnungszeit($FIL_ID, $Gueltigab, $ModellId);
                    $this->DSProtokoll($Zeile, $RetVal);
                }
            }
            $this->DB->TransaktionCommit();
            return $this->DSProtokoll;
        } catch (Exception $e) {
            $this->DB->TransaktionRollback();
            throw new Exception('Fehler beim Importieren der Datei ','20200515092233',$e);
        }


    }

    private function DSProtokoll($Zeile, $Meldung)
    {
        $this->DSProtokoll[] = ['Zeile' => $Zeile, 'Meldung' => $Meldung];
    }

    private function MergeOeffnungszeit($FIL_ID, $Gueltigab, $FOZ_ID)
    {

        $FIL_ID = trim($FIL_ID);
        $Gueltigab = trim($Gueltigab);
        $FOZ_ID = trim($FOZ_ID);

        if (($this->Form->PruefeDatum($Gueltigab, false, false, false, 0)==0) or (count(explode('.',$Gueltigab)) !== 3)) {
            throw new Exception("Fehlerhaftes Datum für Filiale $FIL_ID Modell $FOZ_ID: $Gueltigab", self::ERR_DATUM_FALSCH);
        }

        $SQL = 'select';
        $SQL .= '     FIL_ID,';
        $SQL .= '     FOZ_KEY';
        $SQL .= ' from';
        $SQL .= '     V_FILIALEN_AKTUELL left';
        $SQL .= '     join FILIALENOEFFNUNGSZEITENMODELLE on LAN_CODE = FOZ_LAN_CODE';
        $SQL .= '                                            and FOZ_ID = ' . $this->DB->WertSetzen('FOZ', 'T', $FOZ_ID);
        $SQL .= ' where';
        $SQL .= '     FIL_ID = ' . $this->DB->WertSetzen('FOZ', 'N0', $FIL_ID);

        $rsFOZ = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('FOZ'));

        $FOZ_KEY = $rsFOZ->FeldInhalt('FOZ_KEY');

        if ($rsFOZ->AnzahlDatensaetze() > 1) {
            throw new Exception('Mehr als 1 DS für das Öffnungszeitenmodell gefunden: ' . $FOZ_ID, self::ERR_UNERWARTET);
        } elseif ($rsFOZ->AnzahlDatensaetze() == 0) {
            throw new Exception('Öffnungszeitenmodell(oder Filiale) nicht bekannt: ' . $FOZ_ID . ' => ' . $FIL_ID, self::ERR_FOZ_ID_UNBEKANNT);
        }elseif($FOZ_KEY == ''){
            throw new Exception('Öffnungszeitenmodell nicht bekannt: ' . $FOZ_ID . ' => ' . $FIL_ID, self::ERR_FOZ_ID_UNBEKANNT);
        }

        //Das Kriterium ist immer FILIALNUMMER und das GÜLTIG_AB Datum, weil anderen Datum = anderer Datensatz
        $SQL = 'select * from FILIALENOEFFNUNGSZEITENPFLEGE ';
        $SQL .= ' where FZP_FIL_ID = ' . $this->DB->WertSetzen('FZP', 'N0', $FIL_ID);
        $SQL .= ' and trunc(FZP_GUELTIG_AB) = ' . $this->DB->WertSetzen('FZP', 'D', $Gueltigab);

        $rsFZP = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('FZP'));

        if ($rsFZP->AnzahlDatensaetze() == 0) { //Kein DS => Insert
            $SQL = 'insert into FILIALENOEFFNUNGSZEITENPFLEGE (';
            $SQL .= '     FZP_FIL_ID,';
            $SQL .= '     FZP_FOZ_KEY,';
            $SQL .= '     FZP_GUELTIG_AB,';
            $SQL .= '     FZP_USER,';
            $SQL .= '     FZP_USERDAT';
            $SQL .= ' ) values (';
            $SQL .= $this->DB->WertSetzen('FZP', 'N0', $FIL_ID);
            $SQL .= ',' . $this->DB->WertSetzen('FZP', 'N0', $FOZ_KEY);
            $SQL .= ',' . $this->DB->WertSetzen('FZP', 'D', $Gueltigab);
            $SQL .= ',' . $this->DB->WertSetzen('FZP', 'T', $this->Benutzer->BenutzerName());
            $SQL .= ' ,    sysdate';
            $SQL .= ' )';

            $this->DB->Ausfuehren($SQL, '', true, $this->DB->Bindevariablen('FZP'));

            return self::DS_ANGELEGT;
        } elseif ($rsFZP->AnzahlDatensaetze() == 1 and $rsFZP->FeldInhalt('FZP_FOZ_KEY') != $FOZ_KEY) { //FOZ_KEY ist unterschiedlich => Bestehenden DS Anpassen
            $SQL = 'update FILIALENOEFFNUNGSZEITENPFLEGE';
            $SQL .= ' set';
            $SQL .= '     FZP_FOZ_KEY = ' . $this->DB->WertSetzen('FZP', 'N0', $FOZ_KEY);
            $SQL .= ',     FZP_USER = ' . $this->DB->WertSetzen('FZP', 'T', $this->Benutzer->BenutzerName());
            $SQL .= ',     FZP_USERDAT = sysdate';
            $SQL .= ' where';
            $SQL .= '     FZP_FIL_ID = ' . $this->DB->WertSetzen('FZP', 'N0', $FIL_ID);
            $SQL .= '     and FZP_GUELTIG_AB = ' . $this->DB->WertSetzen('FZP', 'D', $Gueltigab);

            $this->DB->Ausfuehren($SQL, '', true, $this->DB->Bindevariablen('FZP'));

            return self::DS_GEUPDATED;
        } else {
            return self::DS_BEREITSVORHANDEN;
        }
    }

    private function PruefeUeberschriftenArray(array $Ueberschriften)
    {
        foreach ($this->DateiSpalten as $Spalte) {
            if (!isset($Ueberschriften[$Spalte])) {
                throw new Exception('Fehlende Uberschrift: ' . $Spalte, self::ERR_FEHLENDEUEBERSCHRIFT);
            }
        }
    }

    private function PruefeSpaltenanzahl($Zeile)
    {
        if (count(explode(self::TZ, $Zeile)) != count($this->DateiSpalten)) {
            throw new Exception('Falsche Spaltenanzahl in Zeile: ' . $Zeile, self::ERR_SPALTENANZAHL);
        }
    }

    private function PruefeTrennzeichen($Zeile)
    {
        if (!strpos($Zeile, self::TZ)) {
            throw new Exception('Falsches Trennzeichen in Zeile: ' . $Zeile, self::ERR_TRENNZEICHEN);
        }
    }

    public function DebugLevel($Level)
    {
        $this->DebugLevel = $Level;
    }

    public function Log($Text, $Level)
    {
        if ($this->DebugLevel >= $Level) {
            $this->Debug .= date('c') . ' ' . $Text . '<br>';
        }
    }

}