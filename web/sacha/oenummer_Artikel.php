<?
/****************************************************************************************************
* 
* 	Artikel-Liste f�r OENummern
* 
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Dez. 2003
* 
****************************************************************************************************/
// Variablen
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff

$OENRechtestufe = awisBenutzerRecht($con, 403) ;

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

		//********************************************************************
		// Aktualisieren gew�hlt?
		// Block muss identisch sein zu artikel_Artikel.php !!!!
		//********************************************************************
if(isset($HTTP_POST_VARS['cmdSuche_x']))
{
	$Param = $HTTP_POST_VARS['txtAuswahlSpeichern'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_ATUNR'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_Exakt'];		// Exakte Suche
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
	
	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $_SERVER['PHP_AUTH_USER'] , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER'], "");
	
	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $_SERVER['PHP_AUTH_USER']));
    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER']));
}
	// SQL Anweisung erstellen
$SQL = "SELECT * FROM OENummern, Hersteller ";

$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($HTTP_GET_VARS["OENKEY"]) OR $Artikel[0]!='')
{
	if(isset($HTTP_GET_VARS["OENKEY"]))
	{
		$OENKEY = $HTTP_GET_VARS["OENKEY"];
//		awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER'], $OENKEY);
	}
	else
	{
		$OENKEY=$Artikel[0];
	}
	$Bedingung = " AND OEN_KEY = " . $OENKEY;

//**********************************************************
// Daten speichern
//**********************************************************

	if(awis_NameInArray($HTTP_POST_VARS, "cmdSpeichern")!='')
	{

		$SQL = "UPDATE OENummern SET ";
		$SQL .= "OEN_HER_ID = '" . $HTTP_POST_VARS['txtOEN_HER_ID'] . "'";
		$SQL .= ",OEN_BEMERKUNG= '" . $HTTP_POST_VARS['txtOEN_BEMERKUNG'] . "'";	
		$SQL .= ",OEN_MUSTER= '" . $HTTP_POST_VARS['txtOEN_MUSTER'] . "'";	
		$SQL .= ",OEN_IMQ_ID=4";
		$SQL .= ",OEN_USER='" . $_SERVER['PHP_AUTH_USER'] . "'";
		$SQL .= ",OEN_USERDAT=SYSDATE";
		$SQL .= " WHERE OEN_KEY=0" . $HTTP_POST_VARS['txtOEN_KEY'];
		
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("oenummer_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}  // Ende Speichern von Daten
}
													//***********************************************************
elseif(!isset($HTTP_GET_VARS["OENKEY"]))			// Artikel suchen (Liste anzeigen, oder einen)
													//***********************************************************
{
	$DSAnz=0;		// Insgesamt gelesene Zeilen

	$Bedingung = "";
	if($Param[9]=='on' AND $Param[3] != '')
	{
		if($Param[7]=='on')			// NICHT Exakte suche
		{
			$Bedingung .= " AND UPPER(OEN_NUMMER) " . awisLIKEoderIST($Param[3],TRUE) . "";
		}
		else
		{
			$Bedingung .= " AND OEN_SUCHNUMMER " . awisLIKEoderIST($Param[3],TRUE,TRUE,FALSE,TRUE) . "";
		}
	}
	if($Param[1]!='')		// ATU-Nummer
	{
		$SQL .= ', TeileInfos';
		$Bedingung .= 'AND TEI_KEY2 = OEN_KEY AND TEI_KEY1 IN (SELECT AST_KEY FROM ArtikelStamm WHERE AST_ATUNR ' . awisLIKEoderIST($Param[1],TRUE,TRUE,FALSE,TRUE) . ')';
	}
	if($Bedingung == '')	
	{
		awisLogoff($con);
		die('<span class=HinweisText>Mit diesen Kriterien ist keine Suche in OE Nummern m�glich</span>');
	}
	
	$SQL .= " WHERE OEN_HER_ID = HER_ID " . $Bedingung;
	$SQL .= " ORDER BY OEN_NUMMER";

	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;
	if($rsArtikelZeilen == 1)	
	{
		$DSAnz=-1;			// Einzelanzeige
		$OENKEY = $rsArtikel["OEN_KEY"][0];
	}
	else
	{
		print "<table width=100% border=1>";
		
		print "\n<tr>";	// �berschrift
		print "\n<td id=FeldBez>OE-Nummer</td>";
		print "\n<td id=FeldBez>Hersteller</td>";
		print "\n<td id=FeldBez>Bemerkung</td>";
		print "</tr>\n";

 		for($OENNr=0;$OENNr<$rsArtikelZeilen;$OENNr++)		
		{
			print "<tr>";

			print "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY=" . $rsArtikel["OEN_KEY"][$OENNr] . ">";
			print "" . $rsArtikel["OEN_NUMMER"][$OENNr] . "</a></td>";
			print "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["HER_BEZEICHNUNG"][$OENNr] . "</td>";
			print "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["OEN_BEMERKUNG"][$OENNr] . "</td>";
			
			print "</tr>";		
		}
		
		print "</table>";
		print "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $OENNr Artikel gefunden.</font>";
	}
}

/*********************************************************************
* 
* Datensatz l�schen oder zur�cksetzen
* 
**********************************************************************/
//print "<hr>$SQL<hr>";
if(awis_NameInArray($HTTP_POST_VARS, "cmdLARLoeschen")!='' AND !isset($HTTP_POST_VARS["cmdSpeichernAbbruch"]))
{
die("1");

	if(isset($HTTP_POST_VARS["cmdSpeichernBestaetigung"]))
	{
		if($HTTP_POST_VARS['txtLAR_BEKANNTWW']=='0')
		{
			$SQL = "DELETE FROM LieferantenArtikel WHERE LAR_KEY=" . $HTTP_POST_VARS['txtOEN_KEY'];
		}
		else
		{
			$SQL = "UPDATE LieferantenArtikel SET LAR_USER='LOESCHEN' WHERE LAR_KEY=" . $HTTP_POST_VARS['txtOEN_KEY'];
		}
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
	else
	{
		print "<form name=frmArtikel method=post>";
				
		print "<input type=hidden name=txtOEN_KEY value='" . $HTTP_POST_VARS['txtOEN_KEY'] . "'>";
		print "<input type=hidden name=txtLAR_BEKANNTWW value='" . $HTTP_POST_VARS['txtLAR_BEKANNTWW'] . "'>";
		print "<input type=hidden name=cmdLARLoeschen>";

		print "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Lieferantenartikel " . ($HTTP_POST_VARS['txtLAR_BEKANNTWW']==0?'l�schen':'zur�cksetzen') . " wollen?</span><br><br>";
		print "<input accesskey=j type=submit value=\"Ja, Aktion ausf�hren\" name=cmdSpeichernBestaetigung>";
		print "<input accesskey=n type=submit value=\"Nein, Aktion nicht ausf�hren\" name=cmdSpeichernAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();
	}
}

//********************************************************************
// Einzelner Datensatz -> Formular anzeigen
//********************************************************************
//die("1");

if($DSAnz==-1)
{
	print "<form name=frmLieferantenArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER']));

	$SQL = "SELECT * FROM OENummern, Hersteller ";
	$SQL .= " ";
	$SQL .= "";
	$SQL .= " WHERE OEN_KEY=0" . $OENKEY;
	$SQL .= " AND OEN_HER_ID = HER_ID";
	
	$rsArtikel = awisOpenRecordset($con, $SQL);
	if($rsArtikel==FALSE)
	{
		awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
		awisLogoff($con);		
		die();
	}

	if($awisRSZeilen==1)
	{
			// Aktuelle Daten speichern
		awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $_SERVER['PHP_AUTH_USER'], $rsArtikel["OEN_KEY"][0] . ";" . $rsArtikel["OEN_NUMMER"][0] . ";" . $rsArtikel["OEN_HER_ID"][0]);
		
		print "<table border=0 width=100%>";
		print "<tr><td><font size=5><b>OE-Nummer " . $rsArtikel['OEN_NUMMER'][0] . "</b></font></td>";
		print "<td width=100><font size=1>" . $rsArtikel["OEN_USER"][0] ."<br>"  . $rsArtikel["OEN_USERDAT"][0] . "</font></td>";
		print "<td width=40 backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=OENummern&Liste=True';></td>";
		print "</tr></table>";

	
			// Zeile 1
		print "<table border=1 width=100% >";
		print "<colgroup><col width=120><col width=*></colgroup>";
		print "<tr>";
		print "<td id=FeldBez>Nummer</td><td><input type=hidden name=txtOEN_KEY value=". $rsArtikel['OEN_KEY'][0] . ">" . $rsArtikel['OEN_NUMMER'][0] . "</td>";
		print "</tr>";	
		print "</table>";


			// Zeile 2
		print "<table border=1 width=100% >";
		print "<colgroup><col width=120><col width=500><col width=120><col width=*></colgroup>";

		If(($OENRechtestufe & 2)==2)		// Bearbeiten
		{
			print "<tr><td id=FeldBez><table border=0 width=100%><tr><td id=FeldBez>Hersteller</td><td align=right>";
			print "<a href=/stammdaten/hersteller.php?HERID=" . $rsArtikel['OEN_HER_ID'][0] . "><img src=/bilder/suche.png border=0></a>";
			print "</td></tr></table></td><td>";
			print "<select name=txtOEN_HER_ID>";
			$rsHER = awisOpenRecordset($con, "SELECT HER_ID, HER_BEZEICHNUNG FROM HERSTELLER ORDER BY HER_BEZEICHNUNG");
			for($HERZeile=0;$HERZeile<$awisRSZeilen;$HERZeile++)
			{
				print "<option " . ($rsArtikel['OEN_HER_ID'][0]==$rsHER['HER_ID'][$HERZeile]?'selected':'') . " value=" . $rsHER['HER_ID'][$HERZeile] . ">";
				print $rsHER['HER_BEZEICHNUNG'][$HERZeile];
				print "</option>";
			}
			echo '</select>';
			echo '</td>';
			print "<td id=FeldBez>Muster</td><td><select name=txtOEN_MUSTER>";
			print "<option value=0 " . ($rsArtikel['OEN_MUSTER'][0]==0?'selected':'') . ">Nein</option>";
			print "<option value=1 " . ($rsArtikel['OEN_MUSTER'][0]==1?'selected':'') . ">Ja</option>";
			print "</select></td>";
		}
		else
		{
			print "<tr><td id=FeldBez>Hersteller</td><td>" . $rsArtikel["HER_BEZEICHNUNG"][0] . "</td>";
			print "<td id=FeldBez>Muster</td><td>" . ($rsArtikel['OEN_MUSTER'][0]==0?'Nein':'Ja') . "</td>";
		}

		print "</tr>";	
		print "</table>";
	
				// Zeile 3
		print "<table border=1 width=100%>";
		print "<colgroup><col width=120></col><col width=*></col></colgroup>";
		print "<tr>";
		If(($OENRechtestufe & 2)==2)
		{
			print "<td id=FeldBez>Bemerkung</td><td><textarea cols=90 rows=3 type=text name=txtOEN_BEMERKUNG>" . $rsArtikel['OEN_BEMERKUNG'][0] . "</textarea></td>";
		}
		else
		{
			print "<td id=FeldBez>Bemerkung</td><td>" . $rsArtikel['OEN_BEMERKUNG'][0] . "</td>";
		}
		print "</tr>";	
		print "</table>";

		print "<hr>";


		// Unterregister mit abh�ngigen Daten
		$cmdAktion = ($HTTP_GET_VARS["Seite"]==''?"ATUArtikel":$HTTP_GET_VARS["Seite"]);
		awis_RegisterErstellen(43, $con);


		If(($OENRechtestufe & 2)==2)
		{
			print "<input type=hidden name=Speichern Value=True>";
			print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";

						// Cursor in das erste Feld setzen
			print "<Script Language=JavaScript>";
			print "document.getElementsByName(\"txtOEN_HER_ID\")[0].focus();";
			print "</Script>";

		}
	}
	else
	{
		print "<span class=HinweisText>Keine OE-Nummern</span>";
	}
			
	unset($rsGNr);

}

print "</form>";

