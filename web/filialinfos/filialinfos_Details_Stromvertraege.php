<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen
	if(($Recht5001&16384)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(isset($_GET['SSort']))
	{
		$ORDERBY='ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}
	else
	{
		$ORDERBY = ' ORDER BY GSR_RAHMENVERTR DESC';
	}


		$SQL = 'select V.*,L.*';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		$SQL .= ' FROM GSVRAHMENVERTRAEGE V';
		$SQL .= ' inner join GSVLIEFERANTEN L ON GSR_LIEFERANT = GSL_KEY';
		$SQL .= ' where gsr_key in (';
	    $SQL .= ' select gsi_gsr_key from gsvinfos ';
		$SQL .= ' WHERE gsi_xxx_key = ' . $DB->WertSetzen('SVT', 'N0',$AWIS_KEY1);
	    $SQL .= ' group by gsi_gsr_key)';
		$SQL .= ' and gsr_vertragsart = 0';

	if ((isset($_GET['GSI_KEY'])) or (isset($_POST['txtGSI_KEY'])))
	{
		$SQL .= ' AND GSR_KEY = '. $DB->WertSetzen('SVT', 'N0',(isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']));
	}

	$rsSVT = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SVT', true));

	$Form->Formular_Start();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STROMVERTRAEGE'],100, 'font-weight:bolder');
	$Form->Trennzeile('O');
	$Form->ZeileEnde();


	if ((isset($_GET['GSI_KEY'])) or (isset($_POST['txtGSI_KEY'])) and $rsSVT->AnzahlDatensaetze() < 2) //Bestehnden DS editieren.
	{
		$AWIS_KEY2 = isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY'];
		if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))>0) {
			$edit = false;
		}
		else {
			$edit = true;
		}

		$SQL = ' select GST_FELDNAME,GSI_WERT from gsvinfotypen';
		$SQL .= ' inner join gsvinfos on gsi_gst_key = gst_key ';
		$SQL .= ' AND gsi_xxx_key = ' . $DB->WertSetzen('GSI', 'N0',$AWIS_KEY1);
		$SQL .= ' AND gsi_gsr_key = ' . $DB->WertSetzen('GSI', 'N0',(isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']));
		$SQL .= ' where GST_BEREICH = \'Strom\'';

		$rsInfo = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSI', true));


		$InfoFelder = array();

		while (!$rsInfo->EOF()) {

			$InfoFelder[$rsInfo->FeldInhalt('GST_FELDNAME')] =
				$rsInfo->FeldInhalt('GSI_WERT');

			$rsInfo->DSWeiter();
		}

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialinfos_Main.php?cmdAktion=Details&Seite=" . $_GET['Seite'] ." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('GSR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('GSR_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUNDENNUMMER'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_KUNDENNUMMER',isset($InfoFelder['SVT_KUNDENNUMMER'])?$InfoFelder['SVT_KUNDENNUMMER']:'', 10, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LEISTUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_LEISTUNG',isset($InfoFelder['SVT_LEISTUNG'])?$InfoFelder['SVT_LEISTUNG']:'', 10, 100, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KW'], 50);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ZAEHLERSTANDAM'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_ZAEHLERSTAND_AM',isset($InfoFelder['SVT_ZAEHLERSTAND_AM'])?$InfoFelder['SVT_ZAEHLERSTAND_AM']:'', 10, 150, $edit, '', '', '', 'D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIEPREIS'] . ' '. date('Y'). ':', 200);
		$Form->Erstelle_TextFeld('SVT_ENERGIEPREIS',isset($InfoFelder['SVT_REINERENERGIEPREIS'])?$InfoFelder['SVT_REINERENERGIEPREIS']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ZAEHLERNUMMER'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_ZAEHLERNUMMER',isset($InfoFelder['SVT_ZAEHLERNUMMER'])?$InfoFelder['SVT_ZAEHLERNUMMER']:'', 10, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERBRAUCH'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_VERBRAUCH',isset($InfoFelder['SVT_VERBRAUCH'])?$InfoFelder['SVT_VERBRAUCH']:'', 10, 100, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'], 50);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LEISTUNGSSTAND'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_ZAEHLERLEISTUNG',isset($InfoFelder['SVT_ZAEHLERLEISTUNG'])?$InfoFelder['SVT_ZAEHLERLEISTUNG']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSCHLUSSLEISTUNG'] . ':', 200);
		$Form->Erstelle_TextFeld('SVT_ANSCHLUSSLEISTUNG',isset($InfoFelder['SVT_ANSCHLUSSLEISTUNG'])?$InfoFelder['SVT_ANSCHLUSSLEISTUNG']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_DECODE'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_DE_CODE',isset($InfoFelder['SVT_DE_CODE'])?$InfoFelder['SVT_DE_CODE']:'', 30, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TENRFERNABFRAGE'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_TELNRFERNABFRAGE',isset($InfoFelder['SVT_TELNRFERNABFRAGE'])?$InfoFelder['SVT_TELNRFERNABFRAGE']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_HT'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_HT',isset($InfoFelder['SVT_HT'])?$InfoFelder['SVT_HT']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_NETZSPANNUNG'] . ':', 200);
		$Form->Erstelle_TextFeld('SVT_NETZSPANNUNG',isset($InfoFelder['SVT_NETZSPANNUNG'])?$InfoFelder['SVT_NETZSPANNUNG']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 730);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_NT'] . ':', 150);
		$Form->Erstelle_TextFeld('SVT_NT',isset($InfoFelder['SVT_NT'])?$InfoFelder['SVT_NT']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_NETZNUTZUNGSVERTRAG'] . ':', 200);
		$Form->Erstelle_Checkbox('SVT_NETZNUTZUNGSVERTRAG',isset($InfoFelder['SVT_NETZNUTZUNGSVERTRAG'])?$InfoFelder['SVT_NETZNUTZUNGSVERTRAG']:'',50,$edit,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 730);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KOMPESATIONSANLAGE'] . ':', 146);
		$Form->Erstelle_Checkbox('SVT_KOMPENTATIONSANLAGE',isset($InfoFelder['SVT_KOMPENTATIONSANLAGE'])?$InfoFelder['SVT_KOMPENTATIONSANLAGE']:'',50,$edit,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();


		$SQL = 'select GSR_KEY,GSR_RAHMENVERTR';
		$SQL .= ' FROM GSVRAHMENVERTRAEGE V';
		$SQL .= ' WHERE gsr_vertragsart = 0';
		$SQL .= ' and trunc(gsr_vertragslaufzeit) >= trunc(sysdate)';
		$SQL .= ' and gsr_key not in (select distinct gsi_gsr_key from gsvinfos where gsi_xxx_key='.$DB->WertSetzen('GSR', 'N0',$AWIS_KEY1).')';

		if($AWIS_KEY2 != -1) {
			$editVertr = false;
		}
		else{
			$editVertr = true;
		}

		$Form->ZeileStart();
		$Form->AuswahlBox('ajaxRahmenvertr','box1','','GSR_RAHMENVERTR','txtRAV_RAHMENVERTR,txtGSR_RAHMENVERTR,txtGSR_VERTRAGSART',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
		$Form->AuswahlBox('ajaxHTPREIS','box2','','GSR_HTPREIS','txtRAV_RAHMENVERTR,txtGSR_RAHMENVERTR,txtGSR_VERTRAGSART',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STROMVERTRAG'] . ':',160, 'font-weight:bolder;color:blue');
		if($AWIS_KEY2 != -1) {
			$Form->Erstelle_TextFeld('!RAV_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_RAHMENVERTR'),7,400,false,'','','');
		} else {
			$Form->Erstelle_SelectFeld('!RAV_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_KEY'), 700,$editVertr,$SQL,'~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'', '','','','onChange="key_ajaxRahmenvertr(event);key_ajaxHTPREIS(event);"','',$DB->Bindevariablen('GSR', true),'');
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->Erstelle_HiddenFeld('RAV_KEY',isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']);
		$Form->Erstelle_HiddenFeld('GSR_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_KEY'));
		$Form->Erstelle_HiddenFeld('GSR_VERTRAGSART',0);

		ob_start();
		if($AWIS_KEY1 <> '-1') {

			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIELIEFERANT'],560, 'font-weight:bolder;color:blue');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEITEN'],100, 'font-weight:bolder;color:blue');
			$Form->Trennzeile('O');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'] . ':', 160, '', '');
			$Form->Erstelle_TextFeld('!GSL_FIRMA', $rsSVT->FeldInhalt('GSL_FIRMA'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEIT'].':',250);
			$Form->Erstelle_TextFeld('!GSL_VERTRAGSLAUFZEIT',$Form->Format('D',$rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT')),7,400,false,'','','','D');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_VORNAME', $rsSVT->FeldInhalt('GSL_VORNAME'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGZUM'].':',250);
			$Form->Erstelle_TextFeld('!GSL_KUENDIGUNGZUM',$Form->Format('D',$rsSVT->FeldInhalt('GSR_KUENDIGUNG')),7,400,false,'','','','D');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_ANSPRECHPARTNER', $rsSVT->FeldInhalt('GSL_ANSPRECHPARTNER'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGSFRIST'].':',250);
			$Form->Erstelle_TextFeld('!GSL_KUENDIGUNGSFRIST',$rsSVT->FeldInhalt('GSR_KUENDIGUNGSFRIST'),20,400,false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsSVT->FeldInhalt('GSL_STRASSE'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSSTATUS'].':',250);
			$Form->Erstelle_Checkbox('GSL_VETRAGSSTATUS',$rsSVT->FeldInhalt('GSR_VERTRAGSSTATUS'),50,false,true);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GEKUENDIGT'],180);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_PLZ', $rsSVT->FeldInhalt('GSL_PLZ'), 30, 60, false);
			$Form->Erstelle_TextFeld('!GSL_ORT', $rsSVT->FeldInhalt('GSL_ORT'), 30, 340, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PREISANPASSUNG'].':',250);
			$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_KUNDE',$rsSVT->FeldInhalt('GSR_PREISANPASSUNG_KUNDE'),50,false,true);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EVU'].':',180);
			$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_EVU',$rsSVT->FeldInhalt('GSR_PREISANPASSUNG_LIEFERANT'),50,false,true);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsSVT->FeldInhalt('GSL_STRASSE'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_TELNR_FIRMA', $rsSVT->FeldInhalt('GSL_TELNR_FIRMA'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_FAXNR', $rsSVT->FeldInhalt('GSL_FAXNR'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_MAIL', $rsSVT->FeldInhalt('GSL_MAIL'), 30, 300, false);
			$Form->ZeileEnde();
		}
		$Inhalt = ob_get_clean();
		$Form->ZeileStart();
		$Form->AuswahlBoxHuelle('box1','','',$Inhalt);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIEKOSTEN'],560, 'font-weight:bolder;color:blue');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_MESSKOSTEN'],100, 'font-weight:bolder;color:blue');
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LEISTUNGSPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_LEISTUNGSPREIS_A',isset($InfoFelder['SVT_LEISTUNGSPREIS_A'])?$InfoFelder['SVT_LEISTUNGSPREIS_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURKW'].':',120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_LEISTUNGSPREIS']);
		$Form->Erstelle_SelectFeld('SVT_LEISTUNGSPREIS_B',isset($InfoFelder['SVT_LEISTUNGSPREIS_B'])?$InfoFelder['SVT_LEISTUNGSPREIS_B']:'','210',$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_MESSSTELLENBETRIEB'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_MESSSTELLENBETRIEB_A', isset($InfoFelder['SVT_MESSSTELLENBETRIEB_A'])?$InfoFelder['SVT_MESSSTELLENBETRIEB_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURPRO'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('SVT_MESSSTELLENBETRIEB_B',isset($InfoFelder['SVT_MESSSTELLENBETRIEB_B'])?$InfoFelder['SVT_MESSSTELLENBETRIEB_B']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_BONUS'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_BONUS',isset($InfoFelder['SVT_BONUS'])?$InfoFelder['SVT_BONUS']:'', 30, 100, $edit);
		$Form->ZeileEnde();

		ob_start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_HTPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('!RAV_HTPREIS_A', $rsSVT->FeldInhalt('GSR_HTPREIS_A'), 3, 100, false);
		$Form->ZeileEnde();
		$Inhalt = ob_get_clean();
		$Form->ZeileStart();
		$Form->AuswahlBoxHuelle('box2','','',$Inhalt,230,0);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_B', isset($InfoFelder['SVT_HT_PREIS_B'])?$InfoFelder['SVT_HT_PREIS_B']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 109);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_MESSUNG'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_MESSUNG_A',isset($InfoFelder['SVT_MESSUNG_A'])?$InfoFelder['SVT_MESSUNG_A']:'', 3, 109, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURPRO'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('SVT_MESSUNG_B',isset($InfoFelder['SVT_MESSUNG_B'])?$InfoFelder['SVT_MESSUNG_B']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ABSCHLAG'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('SVT_ABSCHLAG_A',isset($InfoFelder['SVT_ABSCHLAG_A'])?$InfoFelder['SVT_ABSCHLAG_A']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_C',isset($InfoFelder['SVT_HT_PREIS_C'])?$InfoFelder['SVT_HT_PREIS_C']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_D',isset($InfoFelder['SVT_HT_PREIS_D'])?$InfoFelder['SVT_HT_PREIS_D']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 109);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ABRECHNUNG'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_ABRECHNUNG_A',isset($InfoFelder['SVT_ABRECHNUNG_A'])?$InfoFelder['SVT_ABRECHNUNG_A']:'', 3, 109, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURPRO'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('SVT_ABRECHNUNG_B',isset($InfoFelder['SVT_ABRECHNUNG_B'])?$InfoFelder['SVT_ABRECHNUNG_B']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('SVT_ABSCHLAG_B',isset($InfoFelder['SVT_ABSCHLAG_B'])?$InfoFelder['SVT_ABSCHLAG_B']:'', 30, 100, $edit);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_E',isset($InfoFelder['SVT_HT_PREIS_E'])?$InfoFelder['SVT_HT_PREIS_E']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_F',isset($InfoFelder['SVT_HT_PREIS_F'])?$InfoFelder['SVT_HT_PREIS_F']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 120);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_G',isset($InfoFelder['SVT_HT_PREIS_G'])?$InfoFelder['SVT_HT_PREIS_G']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_HT_PREIS_H',isset($InfoFelder['SVT_HT_PREIS_H'])?$InfoFelder['SVT_HT_PREIS_H']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 108);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STEUERNABGABEN'],560, 'font-weight:bolder;color:blue');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_NTPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('SVT_NT_PREIS', isset($InfoFelder['SVT_NT_PREIS'])?$InfoFelder['SVT_NT_PREIS']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 330);


		$SQL = ' select * from gsvinfotypen';
		$SQL .= ' inner join gsvinfos on GSI_GST_KEY = GST_KEY';
		$SQL .= ' where GST_BEREICH in(';
		$SQL .= ' select FIL_LAN_WWSKENN from filialen';
		$SQL .= ' where FIL_ID ='.$DB->WertSetzen('FIL', 'N0',$AWIS_KEY1).')';

		$rsFIL = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL', true));

		$LaenderFelder = array();

		while (!$rsFIL->EOF()) {

			$LaenderFelder[$rsFIL->FeldInhalt('GST_FELDNAME')] =
				$rsFIL->FeldInhalt('GSI_WERT');

			$rsFIL->DSWeiter();
		}

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWKGPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('!SVT_KWKG_PREIS',isset($LaenderFelder['SVT_KWKG_PREIS'])?$LaenderFelder['SVT_KWKG_PREIS']:'', 3, 109, false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ABGABE'],120);
		$Form->Erstelle_TextFeld('!SVT_ABGABE',isset($LaenderFelder['SVT_ABGABE'])?$LaenderFelder['SVT_ABGABE']:'', 3, 150, false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LASTEN'] . ':', 276);
		$Form->Erstelle_TextFeld('!SVT_UMLAGE',isset($LaenderFelder['SVT_UMLAGE'])?$LaenderFelder['SVT_UMLAGE']:'', 3, 100, false);


		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_BLINDSTROM'] . ':', 120);
		$Form->Erstelle_TextFeld('!SVT_BLINDSTROM', isset($InfoFelder['SVT_BLINDSTROM'])?$InfoFelder['SVT_BLINDSTROM']:'', 3, 108, false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 330);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EEGPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('!SVT_EEG_PREIS',isset($LaenderFelder['SVT_EEG_PREIS'])?$LaenderFelder['SVT_EEG_PREIS']:'', 3, 109, false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_OFFSHORE'],120);
		$Form->Erstelle_TextFeld('!SVT_OFFSHORE',isset($LaenderFelder['SVT_OFFSHORE'])?$LaenderFelder['SVT_OFFSHORE']:'', 3, 150, false);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_INFO'] . ':', 120);
		$Form->Erstelle_Textarea('SVT_INFO',isset($InfoFelder['SVT_INFO'])?$InfoFelder['SVT_INFO']:'',500,80,5,$edit,'font: bold');
		$Form->ZeileEnde();


	}
	else {
		$Form->ZeileStart();

		$Feldbreite['RAV_RAHMENVERTR'] = 250;
		$Feldbreite['RAV_LAUFZEITBIS'] = 200;

		
		$Icons = array();
		$Icons[] = array('new', './filialinfos_Main.php?cmdAktion=Details&Seite=' . $_GET['Seite'] . '&GSI_KEY=-1', 'g');
		$Form->Erstelle_ListeIcons($Icons, 38, 0);


		$Link = './filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
		$Link .= '&SSort=GSR_RAHMENVERTR' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'GSR_RAHMENVERTR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_RAHMENVERTR'], $Feldbreite['RAV_RAHMENVERTR'], '', $Link);

		$Link = './filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
		$Link .= '&SSort=GSR_VERTRAGSLAUFZEIT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'GSR_VERTRAGSLAUFZEIT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_LAUFZEITBIS'], $Feldbreite['RAV_LAUFZEITBIS'], '', $Link);

		$Form->ZeileEnde();

		if ($rsSVT->AnzahlDatensaetze() > 0) {
			$DS = 0;
			while (!$rsSVT->EOF()) {
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array(
					'edit',
					'./filialinfos_Main.php?cmdAktion=Details'. (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'').'&GSI_KEY=' . $rsSVT->FeldInhalt('GSR_KEY')
				);
				if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))<0) {
					$Icons[] = array(
						'delete',
						'./filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Del=' . $rsSVT->FeldInhalt('GSR_KEY').'&FIL_ID='.$AWIS_KEY1
					);
				}

				if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))>0){
					$Style = 'background-color: #FF0000;';
				} else {
					$Style = '';
				}
				$Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2), '',$Style);
				$Form->Erstelle_ListenFeld('KZT_TERMINAL_ID', $rsSVT->FeldInhalt('GSR_RAHMENVERTR'), 0, $Feldbreite['RAV_RAHMENVERTR'], false, ($DS % 2),
					$Style, '', 'T', 'L', '');
				$Form->Erstelle_ListenFeld('KZT_GUELTIG_AB', $rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'), 0, $Feldbreite['RAV_LAUFZEITBIS'], false, ($DS % 2),
					$Style, '', 'T', 'L', '');
				$Form->ZeileEnde();
				$DS++;
				$rsSVT->DSWeiter();
			}
			$Form->Formular_Ende();
		}
	}
	$Form->ZeileStart();
	$Form->Trennzeile('O');
	$Form->ZeileEnde();

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>