<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $rsErgebnis;
global $AWISSprachKonserven;
global $meldung;
global $Fehler;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	
	
	$script = "<script type='text/javascript'>
	
	function Vers(obj) {
			
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkWert != 2 || checkText == '::bitte w�hlen::')
    	       { 
    	           document.getElementById('VERS').style.display = 'none'; 	    
				   document.getElementsByName('txtBES_ART')[0].className = 'InputText'; 
				   document.getElementsByName('txtBES_ART')[0].removeAttribute('required');
				   document.getElementsByName('txtBES_STATUS')[0].className = 'InputText'; 
				   document.getElementsByName('txtBES_STATUS')[0].removeAttribute('required');
	           	   document.getElementsByName('txtBES_VERSNR')[0].className = 'InputText'; 
				   document.getElementsByName('txtBES_VERSNR')[0].removeAttribute('required');
				   document.getElementsByName('txtBES_ART')[0].selectedIndex = '';
				   document.getElementsByName('txtBES_STATUS')[0].selectedIndex = '';
				   document.getElementsByName('txtBES_VERSNR')[0].value = '';
			   }
    	       else 
    	        {
					document.getElementById('VERS').style.display = 'block';
					document.getElementsByName('txtBES_ART')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_ART')[0].setAttribute('required','');
					document.getElementsByName('txtBES_STATUS')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_STATUS')[0].setAttribute('required','');
					document.getElementsByName('txtBES_VERSNR')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_VERSNR')[0].setAttribute('required','');
				}
		}
	
	</script>";
	
	echo $script;
	
	
	$FeldBreiten = array();
	$FeldBreiten['DATUM'] = 90;
	$FeldBreiten['BETRAG'] = 90;
	$FeldBreiten['BUCHUNGSART'] = 150;
	$FeldBreiten['VERWENDUNGSZWECK'] = 200;
	$FeldBreiten['ART'] = 40;
	$FeldBreiten['STATUS'] = 80;
	$FeldBreiten['VERSNR'] = 150;
	$FeldBreiten['ZUSATZINFO'] = 150;
	$FeldBreiten['Blank'] = 38;
	
	$SQL  = ' select ZKEY,DATUM,BID,STANDID,VS_ID,BETRAG,BUCHUNGSART,VERWENDUNGSZWECK,ART,STANDBEM as STATUS,SCHADENSNR as VERSNR,ZUSATZINFO,Z_USER,Z_USERDAT, ';
	$SQL .='row_number() OVER (';
	// sonst pauschal nach Pr�fdatum
	$SQL .= ' order by ZKEY';
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select ZKEY,DATUM,BETRAG,a.bid,c.standid,c.VS_ID,b.BUCHUNGSART,VERWENDUNGSZWECK,c.ART,d.STANDBEM,c.SCHADENSNR,a.ZUSATZINFO,Z_USER,Z_USERDAT ';
	$SQL .= 'from zahlungen a';
	$SQL .= ' inner join buchungsarten b on a.bid=b.bid ';
	$SQL .= ' left join vers_new c on a.zuvsid = c.VS_ID ';
	$SQL .= ' left join stand d on c.STANDID = d.STANDID and ORT = \'VERS\'';
		
	
	$BEARBNR = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
	$SQL .= 'WHERE a.bearbeitungsnr=\''.$BEARBNR.'\'';
	if(isset($_GET['ZKEY']) or $AWIS_KEY2 != '')
	{
		if($AWIS_KEY2 == '')
		{
			$AWIS_KEY2 = $_GET['ZKEY'];
		}
		$SQL .= 'and zkey='.$AWIS_KEY2;
		$Form->Erstelle_HiddenFeld('ZKEY',$AWIS_KEY2);
	}
	$SQL .= ' and b.VERALTET = 0';
	//$SQL .= ' order by DATUM';
	//$SQL .= 'group by qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat';
	$SQL .= ')';
		
		
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	
	$Recht40007 = $AWISBenutzer->HatDasRecht(40007);
	
	
	if($Recht40007==0)
	{
		$Form->Fehler_KeineRechte();
		die;
	}
	
	
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
			
	}
		
		
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsZahlungen = $DBSCHAD->RecordSetOeffnen($SQL);
		
	$Form->Erstelle_HiddenFeld('BEARBNR',$BEARBNR);
	$Form->Erstelle_HiddenFeld('VS_ID',$rsZahlungen->FeldInhalt('VS_ID'));
	

	if(!isset($_GET['ZKEY']) and $AWIS_KEY2 == '')
	{
		if($meldung != '')
		{
			$Form->Hinweistext($meldung);
		}
		else
		{
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		$Form->ZeileStart('width:100%;');
		if(($Recht40007&8)==8)
		{
			$Icons = array();
			$Icons[] = array('new','./schaden_Main.php?cmdAktion=Details&Seite=Konto&ZKEY=-1'.'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'],'g');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		elseif((($Recht40007&2)==2 or ($Recht40007&4)==4))
		{
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
		}	
			
			
		$Link= '';
		// �berschrift der Listenansicht mit Sortierungslink: Filiale
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_DATUM'], $FeldBreiten['DATUM'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_LANG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_LANG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BETRAG'], $FeldBreiten['BETRAG'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_AKTIV,QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_AKTIV,QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BUCHUNGSART'], $FeldBreiten['BUCHUNGSART'], '', '');
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_VERWENDUNGSZWECK'], $FeldBreiten['VERWENDUNGSZWECK'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ART'], $FeldBreiten['ART'], '', $Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_STATUS'], $FeldBreiten['STATUS'], '', $Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_VERSNR'], $FeldBreiten['VERSNR'], '', $Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ZUSATZINFO'], $FeldBreiten['ZUSATZINFO'], '', $Link);
		$Form->ZeileEnde();
			
		$DS = 0;
		while(! $rsZahlungen->EOF())
		{
				
			$Form->ZeileStart('width:100%');
			if((($Recht40007&2)==2 or ($Recht40007&4)==4))
			{
				$Icons = array();
				if(($Recht40007&2)==2)
				{
					$Icons[] = array('edit','./schaden_Main.php?cmdAktion=Details&Seite=Konto&ZKEY='.$rsZahlungen->FeldInhalt('ZKEY').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				if(($Recht40007&4)==4)
				{
					$Icons[] = array('delete','./schaden_Main.php?cmdAktion=Details&Seite=Konto&BEARBEITUNGSNR='.$AWIS_KEY1.'&Del='.$rsZahlungen->FeldInhalt('ZKEY').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			}
			else 
			{
				if(($Recht40007&8)==8)
				{
					$Form->Erstelle_ListenFeld('BES_BLANK','', 0, $FeldBreiten['Blank'], false, ($DS%2), '','', 'T', 'L','');
				}
			}
			$TTT = $rsZahlungen->FeldInhalt('DATUM');
			$Form->Erstelle_ListenFeld('BES_DATUM',$Form->Format('D',$rsZahlungen->FeldInhalt('DATUM')), 0, $FeldBreiten['DATUM'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('BETRAG');
			$Form->Erstelle_ListenFeld('BES_BETRAG',$Form->Format('N2',$rsZahlungen->FeldInhalt('BETRAG')).'&euro;', 0, $FeldBreiten['BETRAG'], false, ($DS%2), '','', 'T', 'R', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('BUCHUNGSART');
			$Form->Erstelle_ListenFeld('BES_BUCHUNGSART',$rsZahlungen->FeldInhalt('BUCHUNGSART'), 0, $FeldBreiten['BUCHUNGSART'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('VERWENDUNGSZWECK');
			$Form->Erstelle_ListenFeld('BES_VERWENDUNGSZWECK',$rsZahlungen->FeldInhalt('VERWENDUNGSZWECK'), 0, $FeldBreiten['VERWENDUNGSZWECK'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('ART');
			$Form->Erstelle_ListenFeld('BES_ART',$rsZahlungen->FeldInhalt('ART'), 0, $FeldBreiten['ART'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('STATUS');
			$Form->Erstelle_ListenFeld('BES_STATUS',$rsZahlungen->FeldInhalt('STATUS'), 0, $FeldBreiten['STATUS'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('VERSNR');
			$Form->Erstelle_ListenFeld('BES_VERSNR',$rsZahlungen->FeldInhalt('VERSNR'), 0, $FeldBreiten['VERSNR'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsZahlungen->FeldInhalt('ZUSATZINFO');
			$Form->Erstelle_ListenFeld('BES_ZUSATZINFO',$rsZahlungen->FeldInhalt('ZUSATZINFO'), 0, $FeldBreiten['ZUSATZINFO'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsZahlungen->DSWeiter();
		}
			
			
		$Link = './schaden_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else 
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schaden_Main.php?cmdAktion=Details&Seite=Konto&BEARBEITUNGSNR=".$_GET['BEARBEITUNGSNR']." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZahlungen->FeldInhalt('Z_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZahlungen->FeldInhalt('Z_USERDAT'));
		$Form->ZeileStart();
		$Form->InfoZeile($Felder,'');
		$Form->ZeileEnde();
		
		$FehlerText = '';
		if(($Fehler&1) == 1)
		{
			$FehlerText .= $AWISSprachKonserven['BES']['FEHLER_BETRAG'];
		}
		if(($Fehler&2) == 2)
		{
			if($FehlerText != '')
			{
				$FehlerText.= '<br>';
			}
			$FehlerText .= $AWISSprachKonserven['BES']['FEHLER_DATUM'];
		}
		if($FehlerText != '')
		{
			$Form->Hinweistext($FehlerText);
		}
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DATUM'] . ':',160);
		$Form->Erstelle_TextFeld('!BES_DATUM',(isset($_POST['txtBES_DATUM'])?$_POST['txtBES_DATUM']:($rsZahlungen->FeldInhalt('DATUM') == ''?date('d.m.Y',time()):$rsZahlungen->FeldInhalt('DATUM'))), 10, 223,true, '','', '','D', 'L','','',10);
		$Form->ZeileEnde();
		
		$AWISCursorPosition = 'txtBES_DATUM';
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRAG'] . ':',160);
		$Form->Erstelle_TextFeld('!BES_BETRAG',isset($_POST['txtBES_BETRAG'])?$_POST['txtBES_BETRAG']:$Form->Format('N2',$rsZahlungen->FeldInhalt('BETRAG')), 10, 95,true, '','text-align:right', '','T', 'L');
		$Form->Erstelle_TextLabel('&euro;',160,'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BUCHUNGSART'] . ':',160);
		$SQL ='select BID,BUCHUNGSART from BUCHUNGSARTEN WHERE VERALTET = 0 and BID != 0 ';
		if($rsZahlungen->FeldInhalt('BID') == '')
		{
			$BitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		}
		elseif($rsZahlungen->FeldInhalt('BID') != 2)
		{
			$SQL .= ' and BID != 2';
			$BitteWaehlen = '';
		}
		else
		{
			$SQL .= ' and BID = 2';
			$BitteWaehlen = '';
		}
		$SQL .=' order by BUCHUNGSART ';
		$Form->Erstelle_SelectFeld('!BES_BUCHUNGSART',isset($_POST['txtBES_BUCHUNGSART'])?$_POST['txtBES_BUCHUNGSART']:$rsZahlungen->FeldInhalt('BID'),110,true,$SQL,$BitteWaehlen,'','','','','onchange="Vers(this)";','',array(),'','SCHAD');
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERWENDUNGSZWECK'] . ':',160);
		$Form->Erstelle_TextFeld('BES_VERWENDUNGSZWECK',isset($_POST['txtBES_VERWENDUNGSZWECK'])?$_POST['txtBES_VERWENDUNGSZWECK']:$rsZahlungen->FeldInhalt('VERWENDUNGSZWECK'), 46, 500,true, '','', '','T', 'L');
		$Form->ZeileEnde();
		
		if($rsZahlungen->FeldInhalt('BID') == 2 or isset($_POST['txtBES_BUCHUNGSART']) and $_POST['txtBES_BUCHUNGSART'] == 2)
		{
			$Form->ZeileStart('display:block','VERS"ID="VERS');
			$Pflicht = '!';
		}
		else
		{
			$Form->ZeileStart('display:none','VERS"ID="VERS');
			$Pflicht = '';
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ART'] . ':',160);
		$SQL ='select ART,ART as ARTNAME,null,ARTBEZEICHNUNG from ART order by ART ';
		$Form->Erstelle_SelectFeld($Pflicht.'BES_ART',isset($_POST['txtBES_ART'])?$_POST['txtBES_ART']:$rsZahlungen->FeldInhalt('ART'),110,true,$SQL, '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STATUS'] . ':',160);
		$SQL ='select STANDID,STANDBEM from STAND where ORT = \'VERS\' and veraltet=0 order by STANDBEM ';
		$Form->Erstelle_SelectFeld($Pflicht.'BES_STATUS',isset($_POST['txtBES_STATUS'])?$_POST['txtBES_STATUS']:$rsZahlungen->FeldInhalt('STANDID'),110,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERSNR'] . ':',160);
		$Form->Erstelle_TextFeld($Pflicht.'BES_VERSNR',isset($_POST['txtBES_VERSNR'])?$_POST['txtBES_VERSNR']:$rsZahlungen->FeldInhalt('VERSNR'), 46, 500,true, '','', '','T', 'L','','',25);
		$Form->ZeileEnde();
		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ZUSATZINFO'] . ':',160);
		$Form->Erstelle_TextFeld('BES_ZUSATZINFO',isset($_POST['txtBES_ZUSATZINFO'])?$_POST['txtBES_ZUSATZINFO']:$rsZahlungen->FeldInhalt('ZUSATZINFO'), 46, 500,true, '','', '','T', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		
		/*
		$TTT =  $rsZahlungen->FeldInhalt('ART');
		$Form->Erstelle_ListenFeld('BES_ART',$rsZahlungen->FeldInhalt('ART'), 0, $FeldBreiten['ART'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsZahlungen->FeldInhalt('STATUS');
		$Form->Erstelle_ListenFeld('BES_STATUS',$rsZahlungen->FeldInhalt('STATUS'), 0, $FeldBreiten['STATUS'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsZahlungen->FeldInhalt('VERSNR');
		$Form->Erstelle_ListenFeld('BES_VERSNR',$rsZahlungen->FeldInhalt('VERSNR'), 0, $FeldBreiten['VERSNR'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsZahlungen->FeldInhalt('ZUSATZINFO');
		$Form->Erstelle_ListenFeld('BES_ZUSATZINFO',$rsZahlungen->FeldInhalt('ZUSATZINFO'), 0, $FeldBreiten['ZUSATZINFO'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$Form->ZeileEnde();
		*/
	}
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>