<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    global $MVT;
    require_once('mailtexte_funktionen.inc');

    $MVT = new mailtexte_funktionen();
    $MVT->Form->SchreibeHTMLCode("<link rel=stylesheet type=text/css href=" . $MVT->AWISBenutzer->CSSDatei(3) . ">");
    ?>
    <title>Mailtexte</title>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

try {

    $MVT->RechteMeldung(0);

    $Register = new awisRegister(640);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201409101522");
    } else {
        echo 'AWIS: ' . $ex->getMessage();
    }
}

?>
</body>
</html>