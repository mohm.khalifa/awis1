<?php
/**
 * benutzerverwaltung_speichern.php
 *
 * Speichert die Daten im Modul artikelstamm
 *
 * @author  Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 20080428
 * @todo -Keine-
 *
 * �nderungen
 * Wer		Wann		Was
 * ---------------------------------------------------------------
 */

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('XBN','%');
$TextKonserven[]=array('XBL','%');

$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');

$TXT_Speichern = $Form->LadeTexte($TextKonserven);
if(isset($_POST['txtXBN_KEY']))
{
	$AWIS_KEY1 = $_POST['txtXBN_KEY'];

	if(intval($_POST['txtXBN_KEY'])===0)		// Neuer Benutzer
	{
		$Fehler = '';
		// Daten auf Vollst�ndigkeit pr�fen
		$Pflichtfelder = array('XBN_NAME','XBN_VOLLERNAME');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['XBN'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}


		$SQL = 'INSERT INTO Benutzer';
		$SQL .= '(XBN_NAME, XBN_VORNAME, XBN_VOLLERNAME';
		$SQL .= ',XBN_ANREDE, XBN_KON_KEY, XBN_STATUS, XBN_FILIALEN, XBN_BEMERKUNG, XBN_USER, XBN_USERDAT';
		$SQL .= ')VALUES (';
		$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_NAME'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_VORNAME'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_VOLLERNAME'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtXBN_ANREDE'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtXBN_KON_KEY'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_STATUS'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_FILIALEN'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtXBN_BEMERKUNG'],false);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if($DB->Ausfuehren($SQL)===false)
		{
			$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200804291852');
			die();
		}
		$SQL = 'SELECT seq_xbn_key.CurrVal AS KEY FROM DUAL';
		$rsKey = $DB->RecordSetOeffnen($SQL);
		$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
		$AWISBenutzer->ParameterSchreiben("AktuellerXBN",$rsKey->FeldInhalt('KEY'));
		
		//**************************************************************************
		//* Benutzer kopieren
		//**************************************************************************
		if(isset($_POST['txtKOPIEVON']) AND $_POST['txtKOPIEVON']!='')
		{
			// Gruppen �bernehmen
			$SQL = 'SELECT * FROM BENUTZERGRUPPEN WHERE XBG_XBN_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtKOPIEVON'],true);
			$rsXBG = $DB->RecordSetOeffnen($SQL);
			
			$SQL = '';
			while(!$rsXBG->EOF())
			{
				$SQL = ' INSERT INTO BENUTZERGRUPPEN(XBG_XBN_KEY, XBG_XBB_KEY, XBG_USER, XBG_USERDAT)';
				$SQL .= ' VALUES(';
				$SQL .= ' '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);	
				$SQL .= ','.$rsXBG->FeldInhalt('XBG_XBB_KEY');	
				$SQL .= ',\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', SYSDATE)';	
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201001141002');
					die();
				}
				
				$rsXBG->DSWeiter();
			}
		}
	}
	else 					// ge�nderter Artikel
	{
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtXBN_',1,1));
		$FehlerListe = array();
		$UpdateFelder = '';

		$AWISBenutzer->ParameterSchreiben("AktuellerXBN",$_POST['txtXBN_KEY']);
		$rsXBN =$DB->RecordSetOeffnen('SELECT * FROM Benutzer WHERE XBN_KEY=' . $_POST['txtXBN_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
				// Alten und neuen Wert umformatieren!!
				switch ($FeldName)
				{
					default:
						$WertNeu=$DB->FeldInhaltFormat($rsXBN->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);
				}
				$WertAlt=$DB->FeldInhaltFormat($rsXBN->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsXBN->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsXBN->FeldInhalt($FeldName),true);
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}
		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsXBN->FeldInhalt('XBN_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
				$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->Fehler_Anzeigen('GleichzeitigsSpeichern',$Meldung,'WIEDERHOLEN',0,'200809021400');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE Benutzer SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', XBN_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', XBN_userdat=sysdate';
			$SQL .= ' WHERE XBN_key=0' . $_POST['txtXBN_KEY'] . '';
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041224');
			}
		}

		$AWIS_KEY1=$_POST['txtXBN_KEY'];
	//awis_Debug(1,$FeldListe,$SQL,$Felder);
	}
}

//***********************************************************************************
//** BenutzerLogins speichern und in der Teileinfos zuordnen
//***********************************************************************************
if(isset($_POST['txtXBL_KEY']))
{
	if(intval($_POST['txtXBL_KEY'])===0)		// Neuer Artikel
	{
		$Fehler = '';
		$Pflichtfelder = array('XBL_LOGIN');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['LAR'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}

		// Daten umformatieren
		$_POST['txtXBL_LOGIN']=$DB->FeldInhaltFormat('TU',$_POST['txtXBL_LOGIN']);

		$SQL = 'INSERT INTO BenutzerLogins';
		$SQL .= '(XBL_LOGIN, XBL_XBN_KEY';
		$SQL .= ',XBL_USER, XBL_USERDAT';
		$SQL .= ')VALUES (';
		$SQL .= ' ' . $DB->FeldInhaltFormat('TU',$_POST['txtXBL_LOGIN'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		
		if($DB->Ausfuehren($SQL)===false)
		{
			$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200804291850');
		}
				
		$SQL = 'SELECT seq_xbl_key.CurrVal AS KEY FROM DUAL';
		$rsKey = $DB->RecordSetOeffnen($SQL);
		$AWIS_KEY2=0;		// Keine Details anzeigen, zur�ck zur Liste
	}
	else		// zu�ndernder Login
	{
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtXBL_',1,1));
		$FehlerListe = array();
		$UpdateFelder = '';

		$SQL = 'SELECT * FROM BenutzerLogins WHERE xbl_key=0' . $_POST['txtXBL_KEY'];
		$rsXBL = $DB->RecordSetOeffnen($SQL);


		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			switch ($FeldName)
			{
				case 'XBL_LOGIN':
					$_POST[$Feld]=$DB->FeldInhaltFormat('TU',$_POST[$Feld]);
					break;
			}


			if(isset($_POST['old'.$FeldName]))
			{
				switch ($FeldName)
				{
					default:
						$WertNeu=$DB->FeldInhaltFormat($rsXBL->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);
				}
				$WertAlt=$DB->FeldInhaltFormat($rsXBL->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsXBL->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsXBL->FeldInhalt($FeldName),true);
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsXBN->FeldInhalt('XBL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
				$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->Fehler_Anzeigen('GleichzeitigsSpeichern',$Meldung,'WIEDERHOLEN',0,'200809021401');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE BenutzerLogins SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', XBL_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', XBL_userdat=sysdate';
			$SQL .= ' WHERE XBL_key=0' . $_POST['txtXBL_KEY'] . '';
	//awis_Debug(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041225');
			}
		}
		$AWIS_KEY2 = 0;//$_POST['txtXBL_KEY'];
	}
}

//***********************************************************************************
//** Benutzergruppen speichern
//***********************************************************************************
if(isset($_POST['txtXBG_XBB_KEY']))
{
	if(intval($_POST['oldXBG_XBB_KEY'])==0)		// Neue Gruppe
	{
		$Fehler = '';
		$Pflichtfelder = array();
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['LAR'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}

		$SQL = 'INSERT INTO Benutzergruppen';
		$SQL .= '(XBG_XBB_KEY, XBG_XBN_KEY';
		$SQL .= ',XBG_USER, XBG_USERDAT';
		$SQL .= ')VALUES (';
		$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtXBG_XBB_KEY'],false);
		$SQL .= ',' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if($DB->Ausfuehren($SQL)===false)
		{
			$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041200');
		}
		$AWIS_KEY2=0;//Liste zeigen
	}
	else		// zu�ndernder Artikel
	{
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtXBG_',1,1));
		$FehlerListe = array();
		$UpdateFelder = '';

		$SQL = 'SELECT * FROM Benutzergruppen WHERE XBG_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$AWIS_KEY1).' AND XBG_XBB_KEY=0' . $_POST['oldXBG_XBB_KEY'];
		$rsXBG = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen==0)
		{
			die('Keine Daten');
		}

		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);

			if(isset($_POST['old'.$FeldName]))
			{
				// Alten und neuen Wert umformatieren!!
				//echo '##'.$awisRSInfoName[$FeldName]['TypKZ'].'<br>';
				switch ($FeldName)
				{
					default:
						$WertNeu=$DB->FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				}
				$WertAlt=$DB->FeldInhaltFormat($rsXBL->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsXBL->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsXBL->FeldInhalt($FeldName),true);
		//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsXBN->FeldInhalt('XBG_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
				$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->MeldungAnzeigen(1, $Meldung, 200803271009, $TXT_Speichern['Fehler']['err_EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE Benutzergruppen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', XBG_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', XBG_userdat=sysdate';
			$SQL .= ' WHERE XBG_XBN_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1).' AND XBG_XBB_key=0' . $DB->FeldInhaltFormat('N0',$_POST['oldXBG_XBB_KEY']) . '';
	//awis_Debug(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041201');
			}
		}
		$AWIS_KEY2 = 0;//Liste zeigen
	}
}

//***********************************************************************************
//** Benutzerrechte speichern
//***********************************************************************************
if(isset($_POST['txtXBA_XRC_ID']))
{
	$Rechte = explode(';',$Form->NameInArray($_POST,'txtXRC_BIT_',1,1));
	$Stufe = 0;
	if(isset($Rechte[0]) AND $Rechte[0]!='')
	{
		foreach($Rechte AS $Recht)
		{
			$Stufe |= pow(2,floatval(substr($Recht,11)));
		}
	}

	$DB->TransaktionBegin();
	
	$DB->Ausfuehren('DELETE FROM BenutzerACLs WHERE xba_xxx_key = 0'.$AWIS_KEY1.' AND xba_xrc_id = 0'.$_POST['txtXBA_XRC_ID']);
	
	$SQL = 'INSERT INTO BenutzerACLs(xba_xxx_key,xba_xrc_id,xba_stufe,xba_typ,xba_user,xba_userdat) ';
	$SQL .= ' VALUES('.$AWIS_KEY1.','.$_POST['txtXBA_XRC_ID'].','.$Stufe;
	$SQL .= ','.$DB->FeldInhaltFormat('T','B');
	$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
	$SQL .= ',SYSDATE)';
	
	if($DB->Ausfuehren($SQL)===false)
	{
		$DB->TransaktionRollback();
		$Form->Fehler_Anzeigen('SpeicherFehler',1,'MELDEN',5,'200809021402');
	}
	$DB->TransaktionCommit();

	$AWIS_KEY2=0;
	if($Stufe == 0)
	{
		$SQL = 'SELECT seq_xba_key.CurrVal AS KEY FROM DUAL';
		$rsKey = $DB->RecordSetOeffnen($SQL);
		$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	}
}

//***********************************************************************************
//** Benutzeroptionen speichern
//***********************************************************************************

$Felder = explode(';',$Form->NameInArray($_POST, 'txtXBO_WERT',1,1));
if(count($Felder)>0 AND $Felder[0]!='')
{
	foreach($Felder AS $Feld)
	{
		$SQL ='';
		$IDs = explode('_',$Feld);
		if($IDs[3]=='' AND $_POST[$Feld]!=='')			// Neuer Datensatz
		{
			$SQL = 'INSERT INTO BenutzerOptionen';
			$SQL .= '(XBO_WERT, XBO_XBT_ID, XBO_XBN_KEY, XBO_IMQ_ID, XBO_USER, XBO_USERDAT)';
			$SQL .= 'VALUES(';
			$SQL .= ' '.$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
			$SQL .= ','.$IDs[2];
			$SQL .= ','.$AWIS_KEY1;
			$SQL .= ',4';
			$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ',sysdate';
			$SQL .= ')';
		}
		elseif($IDs[3]!='' AND $_POST[$Feld]=='')		// L�schen
		{
			$SQL = 'DELETE from BenutzerOptionen';
			$SQL .= ' WHERE XBO_KEY = 0'.$IDs[3].' AND XBO_XBN_KEY = 0'.$AWIS_KEY1;
		}
		elseif($_POST[$Feld]!='')						// �ndern
		{
			$WertAlt=$DB->FeldInhaltFormat($IDs[4],$_POST['old'.substr($Feld,3)]);
			$WertNeu=$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
			if($WertAlt!=$WertNeu)
			{
				$SQL = 'UPDATE BenutzerOptionen ';
				$SQL .= 'SET XBO_WERT= '.$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
				$SQL .= ',XBO_USER='.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
				$SQL .= ',XBO_USERDAT=sysdate';
				$SQL .= ' WHERE XBO_KEY = 0'.$IDs[3].' AND XBO_XBN_KEY = 0'.$AWIS_KEY1;
			}
		}
		if($SQL!='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',1,'MELDEN',5,'200809021421');
			}
		}
	}
}
?>