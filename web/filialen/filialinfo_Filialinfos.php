<?php
require_once('awisFilialen.inc');
	global $con;
	global $awisRSZeilen;
	global $AWISBenutzer;
	global $rsFiliale;
	//global $FILID;
	//global $txtFIL_PLZ;
	//global $txtFIL_BEZ;
	//global $txtFIL_ORT;
	//global $txtFIL_ID;
	//global $NeuEroef;			// enth�lt ja, wenn Neuer�ffnung
	//global $txtAuswahlSpeichern;
			// Filialnummer wurde �bergeben
	$FIL_ID = '';
	If(isset($_REQUEST['FIL_ID']))
	{
		$FIL_ID = intval($_REQUEST['FIL_ID']);
	}
	elseif(isset($_REQUEST['txtFIL_ID']))
	{
		$FIL_ID = intval($_REQUEST['txtFIL_ID']);
	}	
	elseif(!isset($_GET['Liste']))
	{
		$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
		if(substr($foo,0,7)=="FIL_ID=")
		{
			$FIL_ID=substr($foo,7,99);
		}
	}
	elseif(isset($_GET['Liste']))
	{
		awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()),'');
	}
	
	$MitarbeiterSuche = FALSE;		// Zeigt an, ob nach Mitarbeiter gesucht wurde

	$RechteStufe = awisBenutzerRecht($con, 100);
	if($RechteStufe==0)
	{
	     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
	     die("Keine ausreichenden Rechte!");
	}

	$RechteSchluessel = awisBenutzerRecht($con, 1503);

    echo "<input type=hidden name=cmdAktion value=Filialinfos>";

	// Lokale Variablen
	$Bedingung = '';		// Bedingung f�r SQL
	$SQL = '';				// SQL Abfragestring

		// Alle Filialen anzeigen
	if((isset($_GET['Liste'])) AND $FIL_ID == '')
	{
		$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
		if(substr($foo,0,7)=="FIL_ID=")
		{
			$FIL_ID=substr($foo,7,99);
		}
	}

		// Aktualisieren wurde gew�hlt
	if((!isset($_GET['FIL_ID']) and isset($_POST["cmdSuche_x"])))
	{
		if($_POST['txtFIL_BEZ']!='' AND strstr($_POST['txtFIL_BEZ'],"*")=='')
		{
			$FIL_BEZ = '%' . $_POST['txtFIL_BEZ'] . '%';
		}
		else
		{
			$FIL_BEZ = $_POST['txtFIL_BEZ'];
		}

		if($_POST['txtFIL_ORT']!='' AND strstr($_POST['txtFIL_ORT'],"*")=='')
		{
			$FIL_ORT = '*' . $_POST['txtFIL_ORT'] . '*';
		}
		else
		{
			$FIL_ORT = $_POST['txtFIL_ORT'];
		}
		
		if($_POST['txtFIL_STRASSE']!='' AND strstr($_POST['txtFIL_STRASSE'],"*")=='')
		{
			$FIL_STRASSE = '*' . $_POST['txtFIL_STRASSE'] . '*';
		}
		else
		{
			$FIL_STRASSE = $_POST['txtFIL_STRASSE'];
		}

		$Parameter=array();
		$Parameter[]=isset($_POST["txtFIL_ID"])?$_POST["txtFIL_ID"]:'';
		$Parameter[]=isset($_POST["txtFIL_BEZ"])?$_POST["txtFIL_BEZ"]:'';
		$Parameter[]=isset($_POST["txtFIL_ORT"])?$_POST["txtFIL_ORT"]:'';
		$Parameter[]=isset($_POST["txtFIL_PLZ"])?$_POST["txtFIL_PLZ"]:'';
		$Parameter[]=isset($_POST["txtAuswahlSpeichern"])?$_POST["txtAuswahlSpeichern"]:'';
		$Parameter[]=isset($_POST["txtFIL_Mitarbeiter"])?$_POST["txtFIL_Mitarbeiter"]:'';
		$Parameter[]=isset($_POST["txtFIL_MitarbeiterVorname"])?$_POST["txtFIL_MitarbeiterVorname"]:'';
		$Parameter[]=isset($_POST["txtFIL_MitarbeiterPersnr"])?$_POST["txtFIL_MitarbeiterPersnr"]:'';
		$Parameter[]=isset($_POST["txtNachbarFilialen"])?$_POST["txtNachbarFilialen"]:'';
		$Parameter[]=isset($_POST["txtFIL_STRASSE"])?$_POST["txtFIL_STRASSE"]:'';
		$Parameter[]=isset($_POST["txtFIL_LAND"])?$_POST["txtFIL_LAND"]:'';
		
		awis_BenutzerParameterSpeichern($con, "FilialSuche", $AWISBenutzer->BenutzerName(), implode(";",$Parameter));
		// Bedingungen zusammenbauen
		if($FIL_ID!='')
		{
			$Bedingung .= "AND FIL_ID " . awisLIKEoderIST($FIL_ID, false);
		}
		if($FIL_BEZ!='')
		{
			$Bedingung .= "AND (UPPER(FIL_BEZ) " . awisLIKEoderIST($FIL_BEZ, 1, 0, 0,0);			// Leerzeichen etc.
			$Bedingung .= " OR SUCHWORT(FIL_BEZ)" . awisLIKEoderIST($FIL_BEZ, 1, 1,1 ) . ")";		// wg Umlauten
		}
		if($FIL_STRASSE!='')
		{
			$Bedingung .= "AND (SUCHWORT(FIL_STRASSE) " . awisLIKEoderIST($FIL_STRASSE, 1, 1, 1);			// Leerzeichen etc.
			$Bedingung .= " OR SUCHWORT(FIL_STRASSE)" . awisLIKEoderIST($FIL_STRASSE, 1, 1,1 ) . ")";		// wg Umlauten
		}
		if($FIL_ORT!='')
		{
			$Bedingung .= "AND (SUCHWORT(FIL_ORT) " . awisLIKEoderIST($FIL_ORT, 1, 1, 1);
			$Bedingung .= " OR SUCHWORT(FIL_ORTSTEIL)" . awisLIKEoderIST($FIL_ORT, 1, 1,1 ) . ")";
		}
		if($_POST["txtFIL_PLZ"]!='')
		{
			$Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($_POST["txtFIL_PLZ"]);
		}
		if($_POST["txtFIL_LAND"]!='0')
		{
			$Bedingung .= "AND FIL_LAN_WWSKENN='" . $_POST["txtFIL_LAND"] . "' ";
		}
		if((isset($_POST["txtFIL_Mitarbeiter"]) AND $_POST["txtFIL_Mitarbeiter"]!='') 
			OR (isset($_POST["txtFIL_MitarbeiterVorname"]) AND $_POST["txtFIL_MitarbeiterVorname"]!='') 
			OR (isset($_POST["txtFIL_MitarbeiterPersnr"]) AND $_POST["txtFIL_MitarbeiterPersnr"]!=''))
		{
			$MitarbeiterSuche = TRUE;

			if($_POST["txtFIL_Mitarbeiter"]!='')
			{
				$Bedingung .= "AND SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $_POST["txtFIL_Mitarbeiter"]. "%",1,0,1);
				$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			}
			if($_POST["txtFIL_MitarbeiterVorname"]!='')
			{
				$Bedingung .= " AND SUCHWORT(PER_VORNAME) " . awisLIKEoderIST("%" . $_POST["txtFIL_MitarbeiterVorname"]. "%",1,0,1);
				$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			}
			if($_POST["txtFIL_MitarbeiterPersnr"]!='')
			{
				$Bedingung .= " AND ASCIIWORT(PER_NR) = ASCIIWORT(" . $_POST["txtFIL_MitarbeiterPersnr"]. ")";
				$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			}			
		}
		
	}
	elseif(isset($_GET['FEB_KEY']))
	{
		// Alle Filialen einer Ebene
		$Bedingung = "AND FIL_ID IN (0";
		$FilObj = new awisFilialen();
		$FilListe = $FilObj->FilialenEinerEbene($_GET['FEB_KEY']);
		foreach($FilListe AS $FilialZeile)
		{
			$Bedingung .= ','.$FilialZeile['FEZ_FIL_ID'];
		}

		$Bedingung .= ')';
		
	}
	elseif($FIL_ID!='')		// Aufruf von dieser Seite -> eine Filiale zeigen
	{
		$Bedingung = "AND FIL_ID = " . $FIL_ID;
		awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), (isset($_POST["txtFIL_ID"])?$_POST["txtFIL_ID"]:'').';');
	}
	else						// Direkter Einsprung in die Seite
	{
		$Params = explode(";",awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName()));
		if(count($Params)<10)
		{
						
		}

		if($Params[0]!='')
		{
			$Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0],0);
		}
		if(isset($Params[1]) AND $Params[1]!='')
		{
			$Bedingung .= "AND (UPPER(FIL_BEZ) " . awisLIKEoderIST("%" . $Params[1] . "%",1,0,0,0);
			$Bedingung .= " OR SUCHWORT(FIL_BEZ)" . awisLIKEoderIST("%".$Params[1]."%", 1, 1,1 ) . ")";		// wg Umlauten
		}
		if(isset($Params[9]) AND $Params[9]!='')
		{
			$Bedingung .= "AND (SUCHWORT(FIL_STRASSE) " . awisLIKEoderIST('%'.$Params[9].'%',1,1,1);
			$Bedingung .= " OR SUCHWORT(FIL_STRASSE)" . awisLIKEoderIST("%".$Params[9]."%", 1, 1,1 ) . ")";		// wg Umlauten
		}
		if(isset($Params[10]) AND $Params[10]!='0')
		{
			$Bedingung .= "AND FIL_LAN_WWSKENN='" . $Params[10] . "' ";
		}		
		if(isset($Params[2]) AND $Params[2]!='')
		{
			$Bedingung .= "AND (SUCHWORT(FIL_ORT) " . awisLIKEoderIST('%'.$Params[2].'%',1,1,1);
			$Bedingung .= " OR SUCHWORT(FIL_ORTSTEIL)" . awisLIKEoderIST('%'.$Params[2].'%',1,1,1) . ")";
		}
		if(isset($Params[3]) AND $Params[3]!='')
		{
			$Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3],0);
		}
		if(isset($Params[5]) AND$Params[5]!='')
		{
			$Bedingung .= "AND SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $Params[5] . "%",1,0,1);
			$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			$MitarbeiterSuche = TRUE;
		}
		if(isset($Params[6]) AND $Params[6]!='')
		{
			$Bedingung .= " AND SUCHWORT(PER_VORNAME) " . awisLIKEoderIST("%" . $Params[6] . "%",1,0,1);
			$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			$MitarbeiterSuche = TRUE;
		}
		if(isset($Params[7]) AND $Params[7]!='')
		{
			$Bedingung .= " AND ASCIIWORT(PER_NR) = ASCIIWORT(" . $_POST["txtFIL_MitarbeiterPersnr"]. ")";
			$Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";
			$MitarbeiterSuche = TRUE;
		}
		if(isset($Params[8]) AND $Params[8]!='')	// Nachbarfililialen -> wird in filialino_Filialinfos_000.php separat ausgelesen
		{
		}
	}

	if(awis_BenutzerParameter($con,"FilialenAlle",$AWISBenutzer->BenutzerName()))
	{
		//$SQL = "SELECT * FROM FILIALEN LEFT OUTER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN WHERE FIL_ID IN (select fif_fil_id from filialinfos where fif_wert IS NOT NULL AND fif_fit_id = 21) ";
		$SQL = "SELECT * FROM FILIALEN WHERE FIL_ID IN (select fif_fil_id from filialinfos where fif_wert IS NOT NULL AND fif_fit_id = 21) ";
	}
	else
	{
		if(isset($_REQUEST['NeuEroef']))
		{
			$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NULL ";
		}
		else
		{
			$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ";
		}
	}

	// Mitarbeiter anzeigen
	if($MitarbeiterSuche == TRUE)
	{
		$SQL = str_replace("FILIALEN","FILIALEN, PERSONAL, PERSONALEINSATZPLANUNG, LAENDER",$SQL);
		$SQL .= " AND FIL_ID = PEP_FIL_ID AND PER_NR = PEP_PER_NR AND FIL_LAN_WWSKENN = LAN_WWSKENN(+)";
		$SQL .= " AND PEP_KALENDERWOCHE = " . date('W');
	}

	if($Bedingung!='')
	{
		$SQL .= " AND ( " . substr($Bedingung,4) . ")";
	}

	if($MitarbeiterSuche == TRUE)
	{
		$SQL .= " ORDER BY PER_NACHNAME, PER_VORNAME";
	}
	else
	{
		$SQL .= " ORDER BY FIL_ID";
	}

	// L�nder IMMER hinzuf�gen wg. GBL International!
	$SQL = 'SELECT A.*, Laender.* FROM (' . $SQL . ') A LEFT OUTER JOIN Laender ON FIL_LAN_WWSKENN = Laender.LAN_WWSKENN';
	$SQL .= ' WHERE FIL_PLZ IS NOT NULL';

awis_Debug(1,$SQL);

	$rsFiliale = awisOpenRecordset($con, $SQL);
	$rsFilialeAnz = $awisRSZeilen;

	If($rsFilialeAnz>1 or $MitarbeiterSuche == TRUE)		// Liste gefunden
	{
		if($MitarbeiterSuche == TRUE and $rsFilialeAnz>=1)
		{
			echo "<h4>Einsatzplan in der KW " . date('W') . '</h4><hr>';
		}

		echo "<table border=0 width=100%>";
		for($i=0;$i<$rsFilialeAnz;$i++)
		{
			echo "<tr><td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
			echo "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
			if($MitarbeiterSuche == TRUE)
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFiliale["PER_NACHNAME"][$i] . ", " . $rsFiliale["PER_VORNAME"][$i];
				If($RechteStufe & 4)
				{
					echo "&nbsp;(" . $rsFiliale["PER_NR"][$i] . ")";
				}
				echo "</td>";
				echo '<td></td>';		// Vor�bergehend, bis gekl�rt ist, ob die Nummer angezeigt werden soll.
//				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">". $rsFiliale["PER_TELEFON"][$i] . "</td>";
//				echo "<tr>";
			}
			else
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">". $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td>";
			}
			if(($RechteSchluessel&1)==1)
			{
				echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href=../schluessel/schluessel_Main.php?cmdAktion=Vergaben&txtFIL_ID=".$rsFiliale["FIL_ID"][$i]."&Anzeigen=true><img border=0 src=/bilder/sc_svergabe.png title='Schl&uuml;sselvergabe-Detail anzeigen' </a></td>";

			}
			echo "</tr>";
		}
		echo "</table>";
		echo "<font size=1><br>Es wurden $i Filialen gefunden, die die Bedingungen erf�llen.</font>";
	}
	else					// Nur eine Filiale gefunden, Daten anzeigen
	{
		$SeitenZusatz = awis_BenutzerParameter($con, "FilialInfo_Version", $AWISBenutzer->BenutzerName());
		if($SeitenZusatz=='')
		{
			$SeitenZusatz='001';		//SK: Default
		}
		include("./filialinfo_Filialinfos_" .$SeitenZusatz . ".php");
		//rsFiliale wird als Datenquelle "�bergeben"
	}

?>