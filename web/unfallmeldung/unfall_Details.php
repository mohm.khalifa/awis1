<?php
require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;                // UnfallMeldungenInfoTypen: UFT_BEREICHID
global $ArbEingest;
global $ArbAufgen;
global $Krankmeld;
global $VerTaet;
global $BetrRat;
global $PersCheck;

try {
    $PersCheck = false;
    $PersNr = false;
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('UNF', '%');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_freigeben');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_UNF'));
    $SuchParam['SUCH_UNF_FILABGESCHL'] = '';
    $Recht21020 = $AWISBenutzer->HatDasRecht(21020);
    $Recht21060 = $AWISBenutzer->HatDasRecht(21060);
    $Recht21061 = $AWISBenutzer->HatDasRecht(21061);
    if ($Recht21020 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $AWIS_KEY1 = (isset($_POST['txtUNF_KEY']) ? $_POST['txtUNF_KEY'] : (isset($_GET['UNF_KEY']) ? $_GET['UNF_KEY'] : 0));
    $UNF = new awis_unfall_funktionen();

    $ZeigeSchaltfl�che = false;

    $Form->SchreibeHTMLCode('<form name=frmUnfallDetails action="./unfall_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '') . '" method="POST" enctype="multipart/form-data">');

    //$SuchParam = array();
    if (isset($_POST['cmdSuche_x'])) {
        $SuchParam['SUCH_UNF_BereichKey'] = $_POST['sucUNF_BEREICHKEY'];
        $SuchParam['SUCH_UNF_BESCHAEFTIGUNG'] = $_POST['sucUNF_BESCHAEFTIGUNG'];
        $SuchParam['SUCH_UNF_ORT'] = $_POST['sucUNF_ORT'];
        $SuchParam['SUCH_UNF_URSACHE'] = $_POST['sucUNF_URSACHE'];
        $SuchParam['SUCH_UNF_URSACHESPEZ'] = $_POST['sucUNF_URSACHESPEZ'];
        $SuchParam['SUCH_UNF_AKTENZEICHEN'] = $_POST['sucUNF_AKTENZEICHEN'];
        $SuchParam['SUCH_UNF_FilNr'] = $_POST['sucUNF_FILNR'];
        $SuchParam['SUCH_UNF_VERTRIEBSBEZIRK'] = $_POST['sucUNF_VERTRIEBSBEZIRK'];
        $SuchParam['SUCH_UNF_PersNr'] = $_POST['sucUNF_PERSNR'];
        $SuchParam['SUCH_UNF_Name'] = $_POST['sucUNF_NAME'];
        $SuchParam['SUCH_UNF_ArtKey'] = $_POST['sucUNF_ARTKEY'];
        $SuchParam['SUCH_UNF_DatumVon'] = $_POST['sucUNF_DATUMVON'];
        $SuchParam['SUCH_UNF_DatumBis'] = $_POST['sucUNF_DATUMBIS'];
        $SuchParam['SUCH_UNF_FILABGESCHL'] = isset($_POST['sucUNF_FILABGESCHLOSSEN']) ? 'on' : '';
        $SuchParam['SUCH_UNF_BAGATELL'] = isset($_POST['sucUNF_BAGATELL']) ? 'on' : '';
        $SuchParam['SPEICHERN'] = isset($_POST['sucUNF_AUSWAHLSPEICHERN']) ? 'on' : '';
        $AWISBenutzer->ParameterSchreiben("Formular_UNF", serialize($SuchParam));
    }

    if (isset($_POST['cmdSpeichern_x'])) {
        $UNF->SpeichernFil($AWISBenutzer->BenutzerName(1));

        $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_HINWEIS_BEI_SPEICHERN'],awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
        $ArbEingest = $UNF->PruefeEingaben('ArbEingestellt');
        $ArbAufgen = $UNF->PruefeEingaben('ArbAufgen');
        $Krankmeld = $UNF->PruefeEingaben('Krankmeld');
        //Die Pr�fung von Verunfallter T�tigkeit (VerTaet) wird ausgeschalten, da das Feld nun in der Maske ausgeblendet ist.
        $VerTaet = false;
        $BetrRat = $UNF->PruefeEingaben('BetrRat');
        $Filiale = $UNF->PruefeEingaben('Filiale');

        if (!$ArbEingest and !$ArbAufgen and !$Krankmeld and !$VerTaet and !$BetrRat) {
            $AWIS_KEY1 = 0;
        }
    }
    if (isset($_POST['cmdSpeichernOK'])) {
        $AWIS_KEY1 = $_POST['txtUNF_KEY'];
        $UNF->Speichern($AWISBenutzer->BenutzerName(1));
        $Filiale = $UNF->PruefeEingaben('Filiale');
        if (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) <> '') {
            $FilZug = true;
        } else {
            $FilZug = false;
        }
        if ($Filiale and $FilZug) {
            $AWIS_KEY1 = 0;                                     // auf 0 setzen, dass nach dem Speichern wieder die Listenansicht angezeigt wird
        }
        //$AWIS_KEY1 = 0;
    }
    if (isset($_POST['cmdSpeichernAbbrechen'])) {
        $AWIS_KEY1 = $_POST['txtUNF_KEY'];
        $Form->Hinweistext('Daten wurden nicht gespeichert.');
    }

    if (isset($_POST['cmdFreigeben_x'])) {
        $UNF->Freigeben();
        $AWIS_KEY1 = 0;                                     // auf 0 setzen, dass nach dem Speichern wieder die Listenansicht angezeigt wird
    }
    if (isset($_POST['cmdLoeschen_x']) or isset($_POST['cmdLoeschenOK'])) {
        include('./unfall_loeschen.php');
    }
    if (!isset($_POST['cmdDSNeu_x'])) {
        if (!isset($_GET['UNFSort'])) {
            if (isset($SuchParam['ORDER']) and $SuchParam['ORDER'] != '') {
                $ORDERBY = $SuchParam['ORDER'];
            } else {
                $ORDERBY = ' order by unf_name, unf_vorname';
                $SuchParam['ORDER'] = $ORDERBY;
            }
        } else {
            $SuchParam['ORDER'] = str_replace('~', ' DESC ', $_GET['UNFSort']);
            $ORDERBY = ' ORDER BY ' . $SuchParam['ORDER'];
            $SuchParam['ORDER'] = $ORDERBY;
        }
        //$SuchParam['ORDER']= '';
        //$AWISBenutzer->ParameterSchreiben("Formular_UNF", serialize($SuchParam));
        $Form->DebugAusgabe(1, $ORDERBY);

        $SQL = 'select unf.*, ' .
            'row_number() OVER (';

        $SQL .= $ORDERBY;

        $SQL .= ') AS ZeilenNr ' .
            'from v_unfallmeldung unf';

        $Bedingung = '';
        $BindeVariablen = array();
        if ($AWIS_KEY1 > 0) {                                                        // wenn UNF_KEY vorhanden
            //$SQL .= 'where unf.unf_key = ' . $AWIS_KEY1;
            $Bedingung .= ' and unf.unf_key = :var_UnfKey';            // dann nach UNF_KEY filtern
            $BindeVariablen['var_UnfKey'] = $AWIS_KEY1;
        }

        if (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) <> '') {
            $SuchParam['SUCH_UNF_FilNr'] = $FilZugriff;
        }

        $Bedingung .= $UNF->ErstelleBedingung($SuchParam);
        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }

        $SQL .= $ORDERBY;

        if ($AWIS_KEY1 > 0) {    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
            $MaxDS = 1;
            $ZeilenProSeite = 1;
            $Block = 1;
        } else {    // Zum Bl�ttern in den Daten
            $Block = 1;
            if (isset($_REQUEST['Block'])) {
                $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            }

            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $Form->DebugAusgabe(1, $SQL);
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
        }
    }
    if (isset($_GET['aktiv'])) {
        $AktivDaten = explode(awis_unfall_funktionen::_DecodeEncodeSperatorString, base64_decode($_GET['aktiv']));
        if ($AktivDaten[1] == awis_unfall_funktionen::_AktivString) {
            $UNF->NegiereAktivInaktiv($_GET['UFT_KEY'], $AWISBenutzer->BenutzerName(1));
            unset($_GET['UNF_KEY']);
            $AWIS_KEY1 = 0;
        }
    }

    if (isset($_GET['UNF_VONBGANERKANNT'])) {
        $UNF->NegiereVonBGAnerkannt($_GET['UFT_KEY'], $AWISBenutzer->BenutzerName(1));
        unset($_GET['UNF_KEY']);
        $AWIS_KEY1 = 0;
    }

    if (!isset($_POST['cmdDSNeu_x'])) {
        $rsUNF = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    }
    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->Formular_Start();

    // Detailansicht (Neu/Bearbeiten)
    // - wenn UNF_KEY da => Detailansicht
    // - bei New oder Edit => Detailansicht
    if ($AWIS_KEY1 > 0
        or isset($_POST['cmdDSNeu_x'])
        or isset($_POST['cmdEdit_x'])
        or isset($_POST['cmdDSWeiter_x'])
        or isset($_POST['cmdDSZurueck_x'])) {
        $Form->Erstelle_HiddenFeld('UNF_KEY', $AWIS_KEY1);                // Merker f�r Speichern
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] =
            array('Style' => 'font-size:smaller;', 'Inhalt' => '<a href=./unfall_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '') . ' accesskey=T title=' . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0 ? $rsUNF->FeldInhalt('UNF_USER') : $AWISBenutzer->BenutzerName()));    // bei Edit => User aus DB; bei New => aktUser
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0 ? $rsUNF->FeldInhalt('UNF_USERDAT') : date('d.m.Y H:i:s')));            // bei Edit => Dat aus DB; bei New => aktDat
        $Form->InfoZeile($Felder, '');

        if (isset($_POST['cmdDSNeu_x'])) {
            if (($Recht21020 & 4) != 0) {
                $ZeigeSchaltfl�che = true;
            }
            $AWIS_KEY1 = 0;
            $UNF->ErstelleSchritt01(true, false);
        } elseif (isset($_POST['cmdEdit_x'])) {
            if ($AWIS_KEY1 > 0 and (($Recht21020 & 2) != 0)) {
                $ZeigeSchaltfl�che = true;
            }
            $UNF->ErstelleSchritt01(true, false);
        } elseif (isset($_POST['cmdDSWeiter_x'])) {
            if ((($Recht21020 & 4) != 0) or (($Recht21020 & 2) != 0)) {
                $ZeigeSchaltfl�che = true;
            }

            if ($_POST['txtUNF_PERSNR'] == '') {
                $Form->Hinweistext($AWISSprachKonserven['UNF']['INFO_PERSNR_FEHLT']);
                $UNF->ErstelleSchritt01(true, false);
            } else if ($_POST['txtUNF_PERSNR'] <> '' and (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) <> '')) {
                $SQL = 'select * from personal where per_nr =' . $_POST['txtUNF_PERSNR'];
                $SQL .= 'and per_fil_id =' . $_POST['txtUNF_FIL_ID'];
                $rsPers = $DB->RecordSetOeffnen($SQL);
                if ($rsPers->AnzahlDatensaetze() == 0) {
                    $Form->Hinweistext(str_replace('$FIL$', $_POST['txtUNF_FIL_ID'], $AWISSprachKonserven['UNF']['INFO_PERSNR']));
                    $UNF->ErstelleSchritt01(true, false);
                    $PersCheck = true;
                } else {
                    $UNF->ErstelleSchritt02();
                }
            } else {
                if (($_POST['txtUNF_PERSNR'] <> '' and (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) <> '')) or ((($FilZugriff =
                            $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) == ''))) {
                    $UNF->ErstelleSchritt02();
                }
            }
        } elseif (isset($_POST['cmdDSZurueck_x'])) {
            if ((($Recht21020 & 4) != 0) or (($Recht21020 & 2) != 0)) {
                $ZeigeSchaltfl�che = true;
            }
            $UNF->ErstelleSchritt01(true, true);
        } else {
            if (($Recht21020 & 2) != 0) {
                $ZeigeSchaltfl�che = true;
            }
            if ($ArbEingest or $ArbAufgen or $Krankmeld or $VerTaet or $BetrRat) {
                if ($ArbEingest) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_ARBEITEINGESTELLT']);
                    $Form->ZeileEnde();
                }
                if ($ArbAufgen) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_ARBEITAUFGENOMMEN']);
                    $Form->ZeileEnde();
                }
                if ($Krankmeld) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_KRANKMELDUNGVORL']);
                    $Form->ZeileEnde();
                }
                if ($VerTaet) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_VERUNFALLTER']);
                    $Form->ZeileEnde();
                }
                if ($BetrRat) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_BETRIEBSRAT']);
                    $Form->ZeileEnde();
                }
                $Form->Trennzeile('L');
                $Form->Trennzeile('O');
                $UNF->ErstelleSchritt02();
            } else {
                if (!isset($_POST['cmdSpeichern_x'])) {
                    $UNF->ErstelleDetailAnsicht();
                }
            }
        }
    } else {
        if (($Recht21020 & 4) != 0) {
            $ZeigeSchaltfl�che = true;
        }

        $Form->ZeileStart();

        // Spaltenbreiten f�r Listenansicht
        $FeldBreiten = array();
        $FeldBreiten['PersNr'] = 60;
        $FeldBreiten['Name'] = 320;
        $FeldBreiten['Bereich'] = 120;
        $FeldBreiten['Filialen'] = 50;
        $FeldBreiten['Art'] = 50;
        $FeldBreiten['Zeitpunkt'] = 80;
        $FeldBreiten['DauerIst'] = 70;
        $FeldBreiten['DauerSoll'] = 50;

        $TestRecht = true;    // todo
        $TestRecht2 = true;    // todo
        $TestRecht3 = true;    // todo
        if ($TestRecht)    // Rechte f�r Status setzen / sehen definieren
        {    // Icons f�r Eingaben abgeschlossen anzeigen, wenn berechtigt
            $FeldBreiten['Name'] -= (4 * 20);
            $FeldBreiten['EingabeFilFertig'] = 16;
            $FeldBreiten['EingabeTuevFertig'] = 16;
            $FeldBreiten['EingabeUAbtFertig'] = 16;
            $FeldBreiten['EingabeFertig'] = 16;
            $FeldBreiten['UNF_VONBGANERKANNT'] = 16;
        }

        if ($TestRecht2)    // todo
        {
            $FeldBreiten['Name'] -= 20;
            $FeldBreiten['PdfBGHW'] = 16;
        }

        if ($TestRecht3)    // todo
        {
            $FeldBreiten['Name'] -= 20;
            $FeldBreiten['PdfTUEV'] = 16;
        }

        if ($rsUNF->AnzahlDatensaetze() > 0) {
            // �berschrift der Listenansicht mit Sortierungslink: PersNr
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_PERSNR' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_PERSNR')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['UNF_PERSNR'], $FeldBreiten['PersNr'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Name
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_NAME' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_NAME')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['UNF_NAME'], $FeldBreiten['Name'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Unfallbereich
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UFT_BEREICH' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UFT_BEREICH')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['BEREICH'], $FeldBreiten['Bereich'], '', $Link);
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_FIL_ID' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_FIL_ID')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['UNF_FIL_ID'], $FeldBreiten['Filialen'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Unfallart-ID
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UFT_ART' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UFT_ART')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ARTKURZ'], $FeldBreiten['Art'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Zeitpunkt
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_DATUM' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_DATUM')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ZEITPUNKT'], $FeldBreiten['Zeitpunkt'], '', $Link, '', 'C');
            // �berschrift der Listenansicht mit Sortierungslink: Dauer (Ist)
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_AUSFALL' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_AUSFALL')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['DAUERIST'], $FeldBreiten['DauerIst'], '', $Link, '', 'C');
            // �berschrift der Listenansicht mit Sortierungslink: Dauer (Soll)
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=AUSFALLSOLL' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'AUSFALLSOLL')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['DAUERSOLL'], $FeldBreiten['DauerSoll'], '', $Link, '', 'C');

            // �berschrift f�r Icons f�r Eingaben abgeschlossen anzeigen, wenn berechtigt
            // �berschrift der Listenansicht mit Sortierungslink: Eingabe Filiale/Abteilung fertig
            $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_EINGABEFILFERTIG' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_EINGABEFILFERTIG')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
            $Form->Erstelle_Liste_Ueberschrift('F', $FeldBreiten['EingabeFilFertig'], '', $Link, $AWISSprachKonserven['UNF']['TTT_FILFERTIG'], 'C');
            if (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) == '') {
                // �berschrift der Listenansicht mit Sortierungslink: Eingabe Unfallabteilung fertig
                $Link =
                    './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_EINGABEUABTFERTIG' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_EINGABEUABTFERTIG')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Form->Erstelle_Liste_Ueberschrift('U', $FeldBreiten['EingabeUAbtFertig'], '', $Link, $AWISSprachKonserven['UNF']['TTT_UNFALLFERTIG'], 'C');
                // �berschrift der Listenansicht mit Sortierungslink: Eingabe Arbeitssicherheit/T�V fertig
                $Link =
                    './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_EINGABETUEVFERTIG' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_EINGABETUEVFERTIG')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Form->Erstelle_Liste_Ueberschrift('T', $FeldBreiten['EingabeTuevFertig'], '', $Link, $AWISSprachKonserven['UNF']['TTT_TUEVFERTIG'], 'C');
                // �berschrift der Listenansicht mit Sortierungslink: Eingabe komplett fertig
                $Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_EINGABEFERTIG' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_EINGABEFERTIG')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['EingabeFertig'], '', $Link, $AWISSprachKonserven['UNF']['TTT_FERTIG'], 'C');
                // �berschrift der Listenansicht mit Sortierungslink: Eingabe komplett fertig

                //Auskommentiert. wird in sp�teren Commit gel�scht
                /*$Link = './unfall_Main.php?cmdAktion=Details&UNFSort=UNF_VONBGANERKANNT' . ((isset($_GET['UNFSort']) AND ($_GET['UNFSort'] == 'UNF_VONBGANERKANNT')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
                $Form->Erstelle_Liste_Ueberschrift('G', $FeldBreiten['EingabeFertig'], '', $Link, $AWISSprachKonserven['UNF']['UNF_VONBGANERKANNT'], 'C');
                */
            }

            if ((($Recht21060 & 1) != 0))    // todo
            {
                // �berschrift der Listenansicht: PDF Berufsgenossenschaft
                $Form->Erstelle_Liste_Ueberschrift('B', $FeldBreiten['PdfBGHW'], '', '', $AWISSprachKonserven['UNF']['TTT_PdfBGHW'], 'C');
            }

            if ((($Recht21061 & 1) != 0))    // todo
            {
                // �berschrift der Listenansicht: PDF T�V
                $Form->Erstelle_Liste_Ueberschrift('T', $FeldBreiten['PdfTUEV'], '', '', $AWISSprachKonserven['UNF']['TTT_PdfTUEV'], 'C');
            }

            $Form->ZeileEnde();

            $DS = 0;    // f�r Hintergrundfarbumschaltung
            while (!$rsUNF->EOF()) {
                $Form->ZeileStart();

                if (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) <> '' and $rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG') == 1) {
                    $Link = '';
                } else {
                    $Link = ((($Recht21020 & 2) == 2) ? './unfall_Main.php?cmdAktion=Details&UNF_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '') : '');
                }
                $TTT = $rsUNF->FeldInhalt('UNF_PERSNR') . ' (' . $rsUNF->FeldInhalt('UNF_NAME') . ', ' . $rsUNF->FeldInhalt('UNF_VORNAME') . ')';
                $Form->Erstelle_ListenFeld('*UNF_PERSNR', $rsUNF->FeldInhalt('UNF_PERSNR'), 7, $FeldBreiten['PersNr'], false, ($DS % 2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsUNF->FeldInhalt('UNF_NAME') . ', ' . $rsUNF->FeldInhalt('UNF_VORNAME') . ' (' . $rsUNF->FeldInhalt('UNF_PERSNR') . ')';
                $Form->Erstelle_ListenFeld('*UNF_NAME', $rsUNF->FeldInhalt('UNF_NAME') . ' ' . $rsUNF->FeldInhalt('UNF_VORNAME'), 50, $FeldBreiten['Name'], false, ($DS % 2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsUNF->FeldInhalt('UFT_BEREICHID') . ' ' . $rsUNF->FeldInhalt('UFT_BEREICH') . ' (' . $rsUNF->FeldInhalt('UNF_FIL_ID') . ')';
                $Form->Erstelle_ListenFeld('*UNF_BEREICH', $rsUNF->FeldInhalt('UFT_BEREICH'), 50, $FeldBreiten['Bereich'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
                if ($rsUNF->FeldInhalt('UNF_FIL_ID') <> '9999') {
                    $TTT = $AWISSprachKonserven['UNF']['UNF_FIL_ID'] . ': ' . $rsUNF->FeldInhalt('UNF_FIL_ID');
                } else {
                    $TTT = '';
                }
                $Form->Erstelle_ListenFeld('*UNF_FIL_ID', $rsUNF->FeldInhalt('UNF_FIL_ID') <> '9999' ? $rsUNF->FeldInhalt('UNF_FIL_ID') : '', 50, $FeldBreiten['Filialen'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
                $TTT = $rsUNF->FeldInhalt('UFT_ARTID') . ' ' . $rsUNF->FeldInhalt('UFT_ART');
                $Form->Erstelle_ListenFeld('*UNF_ART', $rsUNF->FeldInhalt('UFT_ARTID'), 5, $FeldBreiten['Art'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
                $TTT = $Form->Format('D', $rsUNF->FeldInhalt('UNF_DATUM')) . ' (' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERVON')) . ' - ' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERBIS')) . ')';
                $Form->Erstelle_ListenFeld('*UNF_ZEITPUNKT', $Form->Format('Dk', $rsUNF->FeldInhalt('UNF_DATUM')), 10, $FeldBreiten['Zeitpunkt'], false, ($DS % 2), '', '', 'T', 'C', $TTT);
                $TTT = $Form->Format('N2', $rsUNF->FeldInhalt('UNF_AUSFALL')) . ': ' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERVON')) . ' - ' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERVON'));
                $Form->Erstelle_ListenFeld('*UNF_DAUERIST', $Form->Format('N2', $rsUNF->FeldInhalt('UNF_AUSFALL')), 6, $FeldBreiten['DauerIst'], false, ($DS % 2), '', '', 'T', 'C', $TTT);
                $TTT = $Form->Format('N2', $rsUNF->FeldInhalt('UNF_AUSFALLSOLL')) . ': ' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERVON')) . ' - ' . $Form->Format('D', $rsUNF->FeldInhalt('UNF_DAUERVON'));
                $Form->Erstelle_ListenFeld('*UNF_DAUERSOLL', $Form->Format('N2', $rsUNF->FeldInhalt('AUSFALLSOLL')), 6, $FeldBreiten['DauerSoll'], false, ($DS % 2), '', '', 'T', 'C', $TTT);

                // Icons f�r Eingaben abgeschlossen anzeigen, wenn berechtigt
                // Leider jedes Icon einzeln zeichnen lassen, im Array hauts mit den Breiten nicht hin!
                $Icons = array();
                if ($rsUNF->FeldInhalt('UNF_VERBANDBUCH') == 1) {
                    $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY'));
                } else {
                    if ($rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG') == 1) {
                        if ($rsUNF->FeldInhalt('UNF_ARZTBESUCHT') == '0') {
                            $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&Ready=-1' . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                        } else {
                            $Icons[] =
                                array('flagge_gruen', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&Ready=' . $rsUNF->FeldInhalt(
                                        'UNF_EINGABEFILFERTIG'
                                    ) . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                        }
                    } else {
                        $Icons[] =
                            array('flagge_rot', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&Ready=' . $rsUNF->FeldInhalt(
                                    'UNF_EINGABEFILFERTIG'
                                ) . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                    }
                }
                //$Icons[] = array((($rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG')==1)? 'flagge_gruen': 'flagge_rot'), './unfall_Main.php?cmdAktion=Details&UFT_KEY=0'.$rsUNF->FeldInhalt('UNF_KEY').'&Ready='.$rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG').(isset($_GET['Block'])?'&Block='.$_GET['Block']:''));
                $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['EingabeFilFertig'], ($DS % 2));
                if (($FilZugriff = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING)) == '') {
                    $Icons = array();
                    if ($rsUNF->FeldInhalt('UNF_VERBANDBUCH') == 1) {
                        $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY'));
                    } else {
                        if ($rsUNF->FeldInhalt('UNF_EINGABEUABTFERTIG') == 1) {
                            $Icons[] =
                                array('flagge_gruen', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&aktiv=' . base64_encode(
                                        $rsUNF->FeldInhalt('UNF_EINGABEUABTFERTIG') . awis_unfall_funktionen::_DecodeEncodeSperatorString . awis_unfall_funktionen::_AktivString
                                    ));
                        } else {
                            $Icons[] =
                                array('flagge_rot', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&aktiv=' . base64_encode(
                                        $rsUNF->FeldInhalt('UNF_EINGABEUABTFERTIG') . awis_unfall_funktionen::_DecodeEncodeSperatorString . awis_unfall_funktionen::_AktivString
                                    ));
                        }
                    }
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['EingabeUAbtFertig'], ($DS % 2));
                    $Icons = array();
                    if ($rsUNF->FeldInhalt('UNF_VERBANDBUCH') == 1) {
                        $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY'));
                    } else {
                        $Icons[] =
                            array((($rsUNF->FeldInhalt('UNF_EINGABETUEVFERTIG') == 1) ? 'flagge_gruen' : 'flagge_rot'), './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&Ready=' . $rsUNF->FeldInhalt(
                                    'UNF_EINGABETUEVFERTIG'
                                ) . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                    }
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['EingabeTuevFertig'], ($DS % 2));
                    $Icons = array();
                    if ($rsUNF->FeldInhalt('UNF_VERBANDBUCH') == 1) {
                        $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY'));
                    } else {
                        $Icons[] =
                            array((($rsUNF->FeldInhalt('UNF_EINGABEFERTIG') == 1) ? 'flagge_gruen' : 'flagge_rot'), './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&Ready=' . $rsUNF->FeldInhalt(
                                    'UNF_EINGABEFERTIG'
                                ) . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                    }
                    if ($rsUNF->FeldInhalt('UNF_ANZEIGEANBG') != '') {
                        $ttt = $Form->Format('D', $rsUNF->FeldInhalt('UNF_ANZEIGEANBG'));
                    } else {
                        $ttt = '';
                    }
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['EingabeFertig'], ($DS % 2), $ttt);

                    $Icons = array();
                    if ($rsUNF->FeldInhalt('UNF_VERBANDBUCH') == 1) {
                        $Icons[] = array('flagge_orange', './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY'));
                    } else {
                        $Icons[] =
                            array((($rsUNF->FeldInhalt('UNF_VONBGANERKANNT') == 1) ? 'flagge_gruen' : 'flagge_rot'), './unfall_Main.php?cmdAktion=Details&UFT_KEY=0' . $rsUNF->FeldInhalt('UNF_KEY') . '&UNF_VONBGANERKANNT=' . $rsUNF->FeldInhalt(
                                    'UNF_VONBGANERKANNT'
                                ) . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : ''));
                    }
                    if ($rsUNF->FeldInhalt('UNF_VONBGANERKANNT') != '') {
                        $ttt = $Form->Format('T', $rsUNF->FeldInhalt('UNF_VONBGANERKANNT'));
                    } else {
                        $ttt = '';
                    }
                    //Auskommentiert. wird in sp�teren Commit gel�scht
                    //$Form->Erstelle_ListeIcons($Icons, $FeldBreiten['UNF_VONBGANERKANNT'], ($DS % 2), $ttt);
                }
                if ((($Recht21060 & 1) != 0))    // todo
                {
                    $Icons = array();
                    $Icons[] = array('pdf', '/berichte/drucken.php?XRE=17&ID=' . base64_encode('UNF_KEY=' . urlencode('=~') . $rsUNF->FeldInhalt('UNF_KEY')));
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['PdfBGHW'], ($DS % 2));
                }

                if ((($Recht21061 & 1) != 0))    // todo
                {
                    $Icons = array();
                    $Icons[] = array('pdf', '/berichte/drucken.php?XRE=18&ID=' . base64_encode($Param = 'UNF_KEY=' . urlencode('=~') . $rsUNF->FeldInhalt('UNF_KEY')));
                    $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['PdfTUEV'], ($DS % 2));
                }

                $Form->ZeileEnde();
                $DS++;
                $rsUNF->DSWeiter();
            }
            $Link = './unfall_Main.php?cmdAktion=Details';
            $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        } else {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
            $Form->ZeileEnde();
            $Form->ZeileEnde(); //Doppelt, weil irgendwo ein DIV-Fehler von Vorg�ngerkollege..
        }
    }
    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ($ZeigeSchaltfl�che and (isset($_POST['cmdDSWeiter_x']) and $_POST['txtUNF_PERSNR'] <> '') and !$PersCheck) {    // wenn Detailansicht UND Recht zum �ndern/Anlegen
        $Form->Schaltflaeche('image', 'cmdSpeichern', './unfall_Main.php?cmdAktion=Details&Del=' . $AWIS_KEY1, '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (!isset($_POST['cmdDSNeu_x'])) {
        if ($ZeigeSchaltfl�che and ($Recht21020 & 8) != 0 and $rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG') == 1
            and isset($_POST['cmdDSWeiter_x']) and $AWIS_KEY1 > 0) {    // wenn Detailansicht UND Recht zum �ndern/Anlegen
            $Form->Schaltflaeche('image', 'cmdFreigeben', '', '/bilder/cmd_schluessel.png', $AWISSprachKonserven['Wort']['lbl_freigeben'], 'F');
        }
    }

    if ($ZeigeSchaltfl�che and ($AWIS_KEY1 <= 0) and !(isset($_POST['cmdDSNeu_x']) or isset($_POST['cmdEdit_x'])
            or isset($_POST['cmdDSWeiter_x']) or isset($_POST['cmdDSZurueck_x']))) {    // wenn Listenansicht UND Recht zum Anlegen
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    if ($ZeigeSchaltfl�che and ($AWIS_KEY1 > 0)
        and !($ArbEingest or $ArbAufgen or $Krankmeld or $VerTaet or $BetrRat or isset($_POST['cmdDSZurueck_x']) or isset($_POST['cmdDSWeiter_x']) or isset($_POST['cmdEdit_x']))) {    // wenn Detailansicht UND Recht zum �ndern
        $Form->Schaltflaeche('image', 'cmdEdit', '', '/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_aendern'], 'E');
    }

    if ($ZeigeSchaltfl�che and (isset($_POST['cmdDSZurueck_x']) or isset($_POST['cmdEdit_x']) or isset($_POST['cmdDSNeu_x'])) or (isset($_POST['txtUNF_PERSNR']) and $_POST['txtUNF_PERSNR'] == '') or $PersCheck) {
        $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'N');
    }

    if ($ZeigeSchaltfl�che and ($ArbEingest or $ArbAufgen or $Krankmeld or $VerTaet or $BetrRat)) {    // wenn Detailansicht UND Recht zum �ndern/Anlegen
        //./personaleinsaetze_Main.php?cmdAktion=Details&Del='.$rsPEI->FeldInhalt('PEI_KEY'))
        $Form->Schaltflaeche('image', 'cmdSpeichern', './unfall_Main.php?cmdAktion=Details&Del=' . $AWIS_KEY1, '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (!isset($_POST['cmdDSNeu_x'])) {
        if ($ZeigeSchaltfl�che and ($Recht21020 & 8) != 0 and $rsUNF->FeldInhalt('UNF_EINGABEFILFERTIG') == 1
            and ($ArbEingest or $ArbAufgen or $Krankmeld or $VerTaet or $BetrRat) and $AWIS_KEY1 > 0) {    // wenn Detailansicht UND Recht zum �ndern/Anlegen
            $Form->Schaltflaeche('image', 'cmdFreigeben', '', '/bilder/cmd_schluessel.png', $AWISSprachKonserven['Wort']['lbl_freigeben'], 'F');
        }
    }

    if ($ZeigeSchaltfl�che and ($Recht21020 & 16) == 16 and $AWIS_KEY1 > 0 and isset($_POST['cmdDSWeiter_x'])) {
        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
    }

    $Form->SchaltflaechenEnde();
    $AWISBenutzer->ParameterSchreiben("Formular_UNF", serialize($SuchParam));
    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        //$Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>