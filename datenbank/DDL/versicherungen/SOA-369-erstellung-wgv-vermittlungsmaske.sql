

SET DEFINE OFF;
INSERT INTO "AWIS"."RECHTE" (XRC_ID, XRC_RECHT) VALUES ('9800', 'Versicherungen Vermittlungen');

INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('9800', '0', 'Anzeigen der Daten');
INSERT INTO "AWIS"."RECHTESTUFEN" (XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG) VALUES ('9800', '1', 'Bearbeiten der Daten');

INSERT INTO "AWIS"."WEBMASKEN" (XWM_ID, XWM_BEZEICHNUNG, XWM_BEMERKUNG, XWM_USER, XWM_USERDAT, XWM_STATUS) VALUES ('9800', 'Versicherungen Vermittlungen', 'Onlineversicherungen', 'Nina Rösch', TO_DATE('2017-11-24 08:15:51', 'YYYY-MM-DD HH24:MI:SS'), 'A');

INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_SORTIERUNG, XRG_NEUEHILFE) VALUES ('98000', '9800', 'Suche', '<u>S</u>che', 'versvermittlungen_Main.php?cmdAktion=Suche', './versvermittlungen_Suche.php', 'Suchseite Versicherungvermittlungen', '9800', '1', '0');
INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_SORTIERUNG, XRG_NEUEHILFE) VALUES ('98001', '9800', 'Details', '<u>D</u>etails', 'versvermittlungen_Main.php?cmdAktion=Details', './versvermittlungen_Details.php', 'Zeigt Details zu den Vermittlungen', '9800', '1', '0');

INSERT INTO "AWIS"."WEBMENUEPUNKTE" (XMP_XME_KEY, XMP_BEZEICHNUNG, XMP_XRC_ID, XMP_STUFE, XMP_GRUPPENID, XMP_SORTIERUNG, XMP_LINK, XMP_USER, XMP_USERDAT, XMP_GUELTIGAB, XMP_GUELTIGBIS) VALUES ('14', 'Vermittlungen', '9800', '1', '0', '211', '/versicherungen/vermittlungen/versvermittlungen_Main.php', 'Nina Rösch', TO_DATE('2009-06-26 18:05:54', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2030-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));


INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('MENUE_257_TEXT', 'MENUE', 'Versicherungsvermittlungen', 'Versicherungsvermittlungen');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('MENUE_257_HINWEIS', 'MENUE', 'Pflegen und Anzeigen von Versicherungsvermittlungen', 'Pflegen und Anzeigen von Versicherungsvermittlungen');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('reg_98000', 'Register', 'Suche', 'S<u>u</u>che', TO_DATE('2017-03-15 16:27:33', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('reg_98001', 'Register', 'Details', '<u>D</u>etails', TO_DATE('2017-03-15 16:27:33', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('tit_versvermittlungen', 'TITEL', 'Versicherungsvermittlungen', 'Versicherungsvermittlungen', TO_DATE('2017-03-17 08:21:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNNAME3', 'VBA', 'Vorname', 'Vorname');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNNAME1', 'VBA', 'Nachname', 'Nachname');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VSNR', 'VBA', 'Versicherungsscheinnummer', 'Versicherungsscheinnr');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_SNR', 'VBA', 'Schadennummer', 'Schadennummer');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_SCHADENDATUM', 'VBA', 'Schadendatum', 'Schadendatum');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VU', 'VBA', 'Versicherungsunternehmen', 'Versicherungsunternehmen');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VBA_KFZKZ', 'VBA', 'KFZ-Kennzeichen', 'KFZ-Kennzeichen', TO_DATE('2017-11-24 11:20:53', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_ERSTELLDATUM', 'VBA', 'Erstelldatum', 'Erstelldatum');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VIN', 'VBA', 'Fahrgestellnummer', 'Fahrgestellnummer');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VORSTABZUG', 'VBA', 'Vorsteuerabzugsberechtigt', 'Vorsteuerabzugsberechtigt');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_ERFASSUNG', 'VBA', 'Erfassung', 'Erfassung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_STATUS', 'VBA', 'Status der Vermittlung', 'Status der Vermittlung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_AX_VORGANGNR', 'VBA', 'Zuordnung zu AX-Auftrag', 'Zuordnung zu AX-Auftrag');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_ERSTZULASSUNG', 'VBA', 'Erstzulassung', 'Erstzulassung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_DATEINAME', 'VBA', 'Dateiname', 'Dateiname');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_SB', 'VBA', 'Selbstbeteiligung', 'Selbstbeteiligung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_USER', 'VBA', 'Bearbeiter', 'Bearbeiter');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VBA_VERSBEZ', 'VBA', 'Versicherung', 'Versicherung', TO_DATE('2017-11-27 11:22:53', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNSTRASSE', 'VBA', 'Straße/Hausnummer', 'Straße/Hausnummer');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNPLZ', 'VBA', 'Postleitzahl', 'Postleitzahl');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNORT', 'VBA', 'Ort', 'Ort');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNKOMM1', 'VBA', 'Telefonnummer1', 'Telefonnummer1');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VBA_VNKOMM2', 'VBA', 'Telefonnummer2', 'Telefonnummer2');

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VBA_KFZHSN', 'VBA', 'HSN', 'HSN', TO_DATE('2017-11-24 11:20:53', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VBA_KFZTSN', 'VBA', 'TSN', 'TSN', TO_DATE('2017-11-24 11:20:53', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."PROGRAMMPARAMETER" (XPP_ID, XPP_BEZEICHNUNG, XPP_DEFAULT, XPP_UEBERSCHREIBBAR, XPP_BEMERKUNG, XPP_KODIERT, XPP_XRC_ID, XPP_DATENTYP) VALUES ('200136', 'Formular_VBA', 'a:0:{}', '255', 'Parameter für Versicherungsvermittlungen', '1', '0', 'T');

commit;
