<?php

class ArrayOfBestellPosition implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BestellPosition[] $BestellPosition
     */
    protected $BestellPosition = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BestellPosition[]
     */
    public function getBestellPosition()
    {
      return $this->BestellPosition;
    }

    /**
     * @param BestellPosition[] $BestellPosition
     * @return ArrayOfBestellPosition
     */
    public function setBestellPosition(array $BestellPosition = null)
    {
      $this->BestellPosition = $BestellPosition;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BestellPosition[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BestellPosition
     */
    public function offsetGet($offset)
    {
      return $this->BestellPosition[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BestellPosition $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BestellPosition[] = $value;
      } else {
        $this->BestellPosition[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BestellPosition[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BestellPosition Return the current element
     */
    public function current()
    {
      return current($this->BestellPosition);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BestellPosition);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BestellPosition);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BestellPosition);
    }

    /**
     * Countable implementation
     *
     * @return BestellPosition Return count of elements
     */
    public function count()
    {
      return count($this->BestellPosition);
    }

}
