<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";

$con = awisLogon();

?>

<title></title>
</head>

<body>

<h2>AWIS - Rechtevergabe</h2>

F�r die Anwender und die Gruppen k�nnen Rechte vergeben werden. Die Stufe bei den Rechten legt dabei fest
welche Aktion sie zu dem angegeben Thema ausf�hren d�rfen. Generell ist es so, dass ein Anwender ein Recht hat,
wenn der Wert gr��er als 0 ist.<br>
<table border=1 width=100%>
<tr>
	<td id=FeldBez>Rechte</td>
	<td id=FeldBez>Beschreibung</td>
</tr>

<tr>
	<td valign=top><a name=1><?print awisRechteName($con,1)?></a></td>
	<td>Mit diesem Recht wird die Benutzerverwaltung aktiviert. Der Anwender erh�lt somit alle Rechte f�r das
		Anlegen, L�schen und �ndern eines Benutzers und einer Gruppe. Dar�ber hinaus k�nnen auch Rechte 
		vergeben werden.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>

	<tr><td>1</td><td>Lesender Zugriff erlaubt</td></tr>
	<tr><td>2</td><td>Kennw�rter der Benutzer �ndern</td></tr>
	<tr><td>4</td><td>Informationen der Benutzer �ndern</td></tr>
	<tr><td>8</td><td>Benutzer l�schen</td></tr>
	<tr><td>16</td><td>Benutzer hinzuf�gen</td></tr>
	<tr><td>32</td><td>Benutzer in Gruppen aufnehmen</td></tr>
	<tr><td>64</td><td>Benutzer aus Gruppen l�schen</td></tr>

	<tr><td>128</td><td>Gruppen hinzuf�gen</td></tr>
	<tr><td>256</td><td>Gruppen �ndern</td></tr>
	<tr><td>512</td><td>Gruppen l�schen</td></tr>
	
	<tr><td>1024</td><td>Rechte vergeben</td></tr>
	
	</td></table>
</tr>


<tr>
	<td valign=top><a name=5><?print awisRechteName($con,5)?></a></td>
	<td>Um sein aktuelles Kennwort �ndern zu k�nnen, muss dieses Recht vergeben werden.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Zugriff erlaubt</td></tr>
	</td></table>
</tr>

<tr>
	<td valign=top><a name=6><?print awisRechteName($con,6)?></a></td>
	<td>Im AWIS werden eine Reihe von Benutzerparametern gespeichert. Diese unterteilen sich in drei Kategorien.
	<br>Die erste enth�lt tempor�re Parameter. Diese haben die ID gr��er als 9000.
	<br>Die zweite sind Programmparameter, die ein Anwender nicht �ndern kann, die aber das Verhalten der Programmteile
		bestimmen. Dazu z�hlen z.B. die Werte in den Suchmasken. Diese Parameter haben eine Zugriffsstufe von 255.
	<br>Die letzte Kategorie sind Parameter, mit denen der Anwender das Verhalten der Programme festlegen kann. Diese 
		Parameter enthalten Zugriffsstufen von 1 bis 254.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Allgemeine Parameter �ndern</td></tr>
	<tr><td>>1</td><td>Spezielle Parameter �ndern. Eine genaue Liste muss der Tabelle <i>Programmparameter</i> entnommen werden.</td></tr>
	</td></table>
	
</tr>


<tr>
	<td valign=top><a name=10><?print awisRechteName($con,10)?></a></td>
	<td>Interne Berechtigung, nur f�r ADMINS
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Zugriff erlaubt</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=20><?print awisRechteName($con,20)?></a></td>
	<td>Mit diesem Recht k�nnen die Rechte und Gruppenmitgliedschaften von Benutzern eingesehen werden.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Eigene Rechte und Gruppenmitgliedschaft einsehen.</td></tr>
	<tr><td>2</td><td>Rechte und Gruppenmitgliedschaft anderer Anwender einsehen.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=21><?print awisRechteName($con,21)?></a></td>
	<td>Mit diesem Recht k�nnen die Mitarbeiter angezeigt werden, die einen Zugriff auf einen bestimmten Programmpunkt haben.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Zugriff erlaubt</td></tr>
	</td></table>
</tr>

<tr>
	<td valign=top><a name=22><?print awisRechteName($con,22)?></a></td>
	<td>Erlaubt das Bearbeiten der Feldbezeichnungen. Diese in "S�tze" eingeteilt. Ein Satz ist eine Auswahl von
	Bezeichnungen f�r einen Feldnamen, z.B. f�r ein Thema oder eine bestimmte Sprache. �ber den Benutzerparameter wird gesteuert,
	welche S�tze bearbeitet werden k�nnen. Das Benutzerparameter kann nur durch die EDV ver�ndert werden.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Zugriff erlaubt</td></tr>
	</td></table>
</tr>

<!--Filialen-Rechte-->

<tr>
	<td valign=top><a name=100><?print awisRechteName($con,100)?></a></td>
	<td>Dieses Recht erlaubt einen Zugriff auf den Programmbereich <i>Filialen</i>. Damit k�nnen Filialen gesucht und 
		Informationen �ber die Filialen angezeigt werden: Filialinfos, Neuer�ffnungen und Umz�ge. Neben diesen Rechten
		werden weitere Rechte f�r die Filialinfo vergeben.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Filialen suchen und Grundinformationen anzeigen</td></tr>
	<tr><td>2</td><td>Filialen nach Mitarbeitern suchen.</td></tr>
</td></table>
</tr>


<tr>
	<td valign=top><a name=110><?print awisRechteName($con,110)?></a></td>
	<td>Zugriff auf die Filialpreise in der Filialinfo.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige des Stundensatzes der Filiale.</td></tr>
	<tr><td>2</td><td>�nderung des Stundensatzes der Filiale.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=111><?print awisRechteName($con,111)?></a></td>
	<td>Zugriff auf die T�V-Informationen in der Filialinfo.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>NUR Anzeige der T�V Preise der Filiale als Zusammenfassung (von-bis).</td></tr>
	<tr><td>2</td><td>Anzeige der T�V Organisationen.</td></tr>
	<tr><td>4</td><td>Bemerkung zu den T�V Organisationen �ndern.</td></tr>
	<tr><td>8</td><td>T�V-Zeiten �ndern.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=112><?print awisRechteName($con,112)?></a></td>
	<td>Zugriff auf den Schichtplan in der Filialinfo. Damit wird ein Link aktiviert, der f�r einen Mitarbeiter
		den ausf�hrlichen Schichtplan anzeigt.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Link aktivieren.</td></tr>
	</td></table>
</tr>



<tr>
	<td valign=top><a name=113><?print awisRechteName($con,113)?></a></td>
	<td>Zugriff auf die Telefonnummern der Filiale.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige aller Telefonnummern.</td></tr>
	<tr><td>2</td><td>�ndern von Telefonnummern, die auf manuelle Bearbeitung gesetzt sind.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=114><?print awisRechteName($con,114)?></a></td>
	<td>Zugriff auf die Ausstattung der Filiale. Hiermit werden Ger�te und Einrichtungsgegenst�nde aufgelistet.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Ausstattung.</td></tr>
	<tr><td>2</td><td>Bearbeitung der Ausstattung. Es kann nur die Menge ver�ndert werden.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=115><?print awisRechteName($con,115)?></a></td>
	<td>Zugriff auf das Personal der Filialen. Es werden alle Mitarbeiter mit Ihren T�tigkeiten und dem Schichtplan
		angezeigt.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Mitarbeiter.</td></tr>
<?php
	If((awisBenutzerRecht($con, 115)&2)==2)
	{
		print "<tr><td>2</td><td>Anzeige der erweiterten Schichtplankennzeichen.</td></tr>";
	}

?>
    </td></table>
</tr>



<tr>
	<td valign=top><a name=118><?print awisRechteName($con,118)?></a></td>
	<td>Bearbeitung von Filialinformationen.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
<?php
	$RechteStufe118 = awisBenutzerRecht($con, 118);
	If(($RechteStufe118&1)==1)
	{
		print "<tr><td>1</td><td>-nicht belegt-</td></tr>";
	}
	If(($RechteStufe118&2)==2)
	{
		print "<tr><td>2</td><td>Wegbeschreibung.</td></tr>";
	}
	If(($RechteStufe118&4)==4)
	{
		print "<tr><td>4</td><td>Anlieferungstag.</td></tr>";
	}
	If(($RechteStufe118&8)==8)
	{
		print "<tr><td>8</td><td>Kommissioniertag.</td></tr>";
	}
	If(($RechteStufe118&16)==16)
	{
		print "<tr><td>16</td><td>Gebietsleiter.</td></tr>";
	}
	If(($RechteStufe118&32)==32)
	{
		print "<tr><td>32</td><td>Verkaufsleiter.</td></tr>";
	}
?>
    </td></table>
</tr>


<tr>
	<td valign=top><a name=116><?print awisRechteName($con,116)?></a></td>
	<td>Anzeige aller �briger Informationen. Dazu z�hlen z.B. die Innung, das Inventurdatum, die Sollbesetzung,
	das Tarifgebiet und die Ausbilder.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Informationen.</td></tr>
	</td></table>
</tr>




<tr>
	<td valign=top><a name=120><?print awisRechteName($con,116)?></a></td>
	<td>Anzeige der Dienstleistungspreise in den Filialen.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>ungleich 0</td><td>Anzeige der Informationen.</td></tr>
	</td></table>
</tr>


<!-- Telefonverzeichnis -->

<tr>
	<td valign=top><a name=150><?print awisRechteName($con,150)?></a></td>
	<td>
		Mit diesem Recht wird festgelegt, ob ein Anwender Telefonverzeichnis suchen
		darf.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Zugriff erlaubt</td></tr>
		<tr><td>2</td><td>Neue Kontakte hinzuf�gen</td></tr>
		<tr><td>4</td><td>Kontakte l�schen</td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=151><?print awisRechteName($con,151)?></a></td>
	<td>
		Mit diesem Recht wird festgelegt, welche Eintr�ge ein Anwender im Telefonverzeichnis angezeigt
		bekommt. Die Stufen sind in den Daten hinterlegt (Tabelle KONTAKTEZUGRIFFSGRUPPEN). Derzeit sind
		folgende Stufen definiert. Jeder Datensatz in den Kontakten ist einer Stufe zugeordnet. Die Stufe
		1024 ist f�r die Bearbeitung der Daten definiert.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>F�r alle AWIS User.</td></tr>
		<tr><td>1024</td><td>Pers�nliche Informationen (nur die KontakteKommunikation).</td></tr>
		<tr><td>8192</td><td>Ausschlie�lich f�r die Vermittlung.</td></tr>
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=152><?print awisRechteName($con,152)?></a></td>
	<td>
		Mit diesem Recht wird festgelegt, welche Eintr�ge ein Anwender im Telefonverzeichnis �ndern
		darf. Dabei sind die Felder und Eintr�ge unterschiedlichen Gruppen zugeordnet (Tabelle 
		KONTAKTEZUGRIFFSGRUPPEN).
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>F�r alle AWIS User.</td></tr>
		<tr><td>2</td><td>Stufe 1.<br>Kontaktkategorie, Abteilung, Zust�ndigkeit</td></tr>
		<tr><td>4</td><td>Stufe 2.<br>Name �ndern, ATU Geb�ude</td></tr>
		<tr><td>1024</td><td>Pers�nliche Informationen bei Kontaktekommunikation.</td></tr>
		<tr><td>8192</td><td>Kontaktekommunikation f�r die Vermittlung.</td></tr>
		</table>
	</td>
</tr>



<tr>
	<td valign=top><a name=200><?print awisRechteName($con,200)?></a></td>
	<td>Reifensuche (Absatz)
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Informationen.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=250><?print awisRechteName($con,250)?></a></td>
	<td>Reifensuche
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Informationen.</td></tr>
	<tr><td>128</td><td>Anzeige der deutschen Preise.</td></tr>
	<tr><td>256</td><td>Anzeige der �sterreichischen Preise.</td></tr>
	<tr><td>512</td><td>Anzeige der tschechischen Preise.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=251><?print awisRechteName($con,251)?></a></td>
	<td>Reifensuche KB-Liste
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Anzeige der Informationen.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=252><?print awisRechteName($con,252)?></a></td>
	<td>Feste CC Sortierung bei der Reifensuche. Diese Eintragung stellt eine Einschr�nkung dar.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>CC Sortierung nicht �nderbar.</td></tr>
	</td></table>
</tr>


<tr>
	<td valign=top><a name=300><?print awisRechteName($con,300)?></a></td>
	<td>Nachl�sse.
	<table border=1 width=100%>
	<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
	<tr><td>1</td><td>Nachlassarten einsehen.</td></tr>
	<tr><td>2</td><td>Nachlassarten pflegen.</td></tr>	
	</td></table>
</tr>


<tr>
	<td valign=top><a name=8><?print awisRechteName($con,8)?></a></td>
	<td>
		Mit diesem Recht k�nnen Daten automatisert ver�ndert werden.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>M�gliche Felder einsehen.</td></tr>
		<tr><td>2</td><td>Felder l�schen.</td></tr>
		<tr><td>4</td><td>Felder hinzuf�gen.</td></tr>
		<tr><td>8</td><td>Auftr�ge einstellen.</td></tr>
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,400)?></a></td>
	<td>
		Mit diesem Recht k�nnen ATU Artikel eingesehen werden.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Suchen nach Artikeln.</td></tr>
		<tr><td>2</td><td>Anzeige der EAN-Nummern.</td></tr>
		<tr><td>4</td><td>Hinzuf�gen von ATU Artikeln.</td></tr>
		<tr><td>256</td><td>Export der ATU Artikel als CSV Datei.</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td valign=top><a name=8><?print awisRechteName($con,401)?></a></td>
	<td>
		Mit diesem Recht k�nnen ATU Artikel bearbeitet werden.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>2</td><td>Bearbeiten der Alternativbezeichnung und des KENN-Vorschlags.</td></tr>
		<tr><td>4</td><td>Bearbeiten der EAN Nummern Zuordnung in den Artikeln.</td></tr>
		<tr><td>8</td><td>Bearbeiten des 'Meldung an Einkauf' Eintrags beim Artikel.</td></tr>
		<tr><td>16</td><td>Bearbeiten der Katalog-Felder.</td></tr>
		<tr><td>32</td><td>Bearbeiten der Crossing Kommentare.</td></tr>
		<tr><td>64</td><td>Bearbeiten der Einkauf Kommentare.</td></tr>
		<tr><td>128</td><td>Bearbeiten der Einkauf TKD.</td></tr>		
		</table>
	</td>
</tr>
<tr>
	<td valign=top><a name=8><?print awisRechteName($con,402)?></a></td>
	<td>
		Mit diesem Recht k�nnen ATU Artikel bearbeitet werden.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Lesen der Lieferantenartikel</td></tr>
		<tr><td>2</td><td>Bearbeiten des Bemerkungsfeldes.</td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,403)?></a></td>
	<td>
		Einsehen und Bearbeiten der OENummern.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Einsehen der OENummern bei den ATU Artikeln.</td></tr>
		<tr><td>2</td><td>Bearbeiten der Zuordnung von OENummern zu ATU Artikel.</td></tr>
		<tr><td>4</td><td>Einsehen von OE-Preisen zu den OE-Nummern.</td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,404)?></a></td>
	<td>
		Einsehen und Bearbeiten der EAN-Nummern.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Einsehen der EAN-Nummern bei den ATU Artikeln.</td></tr>
		<tr><td>2</td><td>Bearbeiten EAN-Nummern.</td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,500)?></a></td>
	<td>
		Auswertungen f�r Filialen.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Austattung.</td></tr>
		<tr><td>2</td><td></td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,600)?></a></td>
	<td>
		Einsehen und Bearbeiten des Lieferantenstamms.
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Einsehen des Lieferantenstamms.</td></tr>
		<tr><td>2</td><td>Bearbeiten des Lieferantenstamms.</td></tr>		
		<tr><td>4</td><td>Hinzuf�gen von Lieferanten.</td></tr>		
		</table>
	</td>
</tr>

<tr>
	<td valign=top><a name=8><?print awisRechteName($con,700)?></a></td>
	<td>
		RLS-Rechte
		<table border=1>
		<tr><td  id=FeldBez>Rechte-Wert</td><td id=FeldBez>M�gliche Aktionen der Anwender</td></tr>
		<tr><td>1</td><td>Lesen.</td></tr>
		<tr><td>2</td><td>�ndern.</td></tr>		
		<tr><td>4</td><td>L�schen.</td></tr>		
		</table>
	</td>
</tr>


</table>


<?php
awisLogoff($con);
?>
</body>

</html>
