<?PHP
require_once 'XML/XUL.php';
 
$doc = &XML_XUL::createDocument();
$win = &$doc->createElement( 'window' );
$doc->addRoot( $win );

$tabbox = &$doc->createElement( 'Tabbox' );
$win->appendChild( $tabbox );

$browser1 = &$doc->createElement( 'browser', array( 'src' => 'http://pecl.php.net' ) );
$browser2 = &$doc->createElement( 'browser', array( 'src' => 'http://pear.php.net' ) );

$tabbox->addTab( 'PECL', $browser1 );
$tabbox->addTab( 'PEAR', $browser2 );

$doc->send();
?>
