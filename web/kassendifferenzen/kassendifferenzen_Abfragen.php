<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl  = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    
    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition         = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld     = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite     = $AWISBenutzer->ParameterLesen('BildschirmBreite');
    
    $TextKonserven   = array();
    $TextKonserven[] = array('','*');
    $TextKonserven[] = array('VSK','*');
    $TextKonserven[] = array('Wort','Seite');
    $TextKonserven[] = array('Wort','lbl_suche');
    $TextKonserven[] = array('Wort','lbl_weiter');
    $TextKonserven[] = array('Wort','lbl_speichern');
    $TextKonserven[] = array('Wort','lbl_trefferliste');
    $TextKonserven[] = array('Wort','lbl_aendern');
    $TextKonserven[] = array('Wort','lbl_hilfe');
    $TextKonserven[] = array('Wort','lbl_hinzufuegen');
    $TextKonserven[] = array('Wort','lbl_loeschen');
    $TextKonserven[] = array('Wort','lbl_zurueck');
    $TextKonserven[] = array('Wort','lbl_DSZurueck');
    $TextKonserven[] = array('Wort','lbl_DSWeiter');
    $TextKonserven[] = array('Wort','lbl_drucken');
    $TextKonserven[] = array('Wort','lbl_Hilfe');
    $TextKonserven[] = array('Wort','txt_BitteWaehlen');
    $TextKonserven[] = array('Wort','FilialenDeutschAlle');
    $TextKonserven[] = array('Wort','ttt_PositionHinzufuegen');
   
    $Form                = new awisFormular();
    $DB                  = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    
    $Recht35000 = $AWISBenutzer->HatDasRecht(35000);
    
    if ($Recht35000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }
    
    $flagNewDS = false;
    
    if (isset($_GET["New"])) {
        $flagNewDS = true;
    }
    
    $Form->SchreibeHTMLCode('<form name=frmAbfragen action=./visitenkarten_Main.php?cmdAktion=Abfragen method=post>');
    $Form->Formular_Start();

    if (isset($_GET['Del']) && count($_POST ) == 0) //L�schen von Datens�tzen
    {
        $SQL = 'DELETE from  Visitenkartenabfragen WHERE VSA_KEY = ' . $_GET['VSA_KEY']; 
        if($DB->Ausfuehren($SQL)===false)
        {
            throw new awisException('Fehler beim Loeschen',201409181134,$SQL,awisException::AWIS_ERR_SYSTEM);
        }
        $SQL = 'DELETE from  Visitenkartenabfragenfilialen WHERE VSF_VSA_ID = ' . $_GET['VSA_KEY'];
        if($DB->Ausfuehren($SQL)===false)
        {
            throw new awisException('Fehler beim Loeschen',201409231433,$SQL,awisException::AWIS_ERR_SYSTEM);
        }
        
        $SQL = 'DELETE FROM Visitenkarten where VSK_VSA_ID = ' . $_GET['VSA_KEY'];
        if($DB->Ausfuehren($SQL)===false)
        {
            throw new awisException('Fehler beim Loeschen',201409231433,$SQL,awisException::AWIS_ERR_SYSTEM);
        }
        
        
    }
    elseif (count($_POST ) <> 0)
    {
        //Erst Kopfdatensatz
        $SQL ='INSERT';
        $SQL .=' INTO VISITENKARTENABFRAGEN';
        $SQL .='   (';
        $SQL .='     VSA_NAME,';
        $SQL .='     VSA_DATUMVON,';
        $SQL .='     VSA_DATUMBIS,';
        $SQL .='     VSA_USERDAT,';
        $SQL .='     VSA_USER';
        $SQL .='   )';
        $SQL .='   VALUES';
        $SQL .='   (';
        $SQL .= '\'' . $_POST['txtVSA_NAME'] . '\',';
        $SQL .= '\'' .    $_POST['txtVSK_DATUMVON'] . '\',';
        $SQL .= '\'' .    $_POST['txtVSK_DATUMBIS'] . '\',';
        $SQL .= '    sysdate,';
        $SQL .=  '\'' .   $AWISBenutzer->BenutzerName();
        $SQL .=    '\')';
        
     
        if($DB->Ausfuehren($SQL)===false)
        {
            throw new awisException('Fehler beim Hinzuf�gen des Datensatzes',201409231421,$SQL,awisException::AWIS_ERR_SYSTEM);
        }
        
        //Dann Die Filialzuordnung
        
        if (isset($_POST['sucFilialenDeutschAlle'])) //Haken angehakt
        {
            $SQL ='SELECT FIL_ID';
            $SQL .=' FROM';
            $SQL .='   (SELECT FIL_ID ,';
            $SQL .='     (SELECT LAN_Code FROM Laender WHERE lan_wwskenn = fil_lan_wwskenn';
            $SQL .='     ) AS LAN_LAND';
            $SQL .='   FROM Filialen fil';
            $SQL .='   LEFT OUTER JOIN v_FilialPfad';
            $SQL .='   ON FIL_ID        = XX1_FIL_ID';
            $SQL .='   WHERE XX1_Ebene IS NOT NULL';
            $SQL .='   )';
            $SQL .=' WHERE LAN_LAND = \'DE\' group by FIL_ID';
            
            $rsVSAFIL = $DB->RecordSetOeffnen($SQL);
            $DelFlag = false;
            $FalscheFilialen = '';
            while (!$rsVSAFIL->EOF()) {
                
                
                $SQLCheck ='select * from visitenkartenabfragenfilialen vsf';
                $SQLCheck .=' inner join visitenkartenabfragen vsa';
                $SQLCheck .=' on vsf.vsf_vsa_id = vsa.vsa_key';
                $SQLCheck .=' where (to_char(vsa.vsa_datumbis,\'DD.MM.YYYY\') >= to_char(to_date(\''  .  $_POST['txtVSK_DATUMBIS'] .  '\', \'DD.MM.YYYY\'), \'DD.MM.YYYY\')'; 
                $SQLCheck .=' AND (to_char(vsa.vsa_datumvon,\'DD.MM.YYYY\') <= to_char(to_date(\''  .  $_POST['txtVSK_DATUMVON'] .  '\', \'DD.MM.YYYY\'), \'DD.MM.YYYY\'))'; 
                $SQLCheck .= 'AND VSF.VSF_FILID =' .  $rsVSAFIL->FeldInhalt('FIL_ID') . ')';

                $rsVSFCheck = $DB->RecordSetOeffnen($SQLCheck);
                
                if($rsVSFCheck->AnzahlDatensaetze() >= 1)
                {                    
                    $DelFlag = true;
                    $FalscheFilialen .= $rsVSAFIL->FeldInhalt('FIL_ID') . ', ';
                    
                }
                else 
                {

                    $SQL ='INSERT';
                    $SQL .=' INTO visitenkartenabfragenfilialen';
                    $SQL .='   (';
                    $SQL .='     VSF_FILID,';
                    $SQL .='     VSF_VSA_ID,';
                    $SQL .='     VSF_USER,';
                    $SQL .='     VSF_USERDAT';
                    $SQL .='   )';
                    $SQL .='   VALUES';
                    $SQL .='   (';
                    $SQL .='     \'' .  $rsVSAFIL->FeldInhalt('FIL_ID') . '\',';
                    $SQL .='     (Select max(VSA_KEY) as lkey from Visitenkartenabfragen),';
                    $SQL .='     \'' . $AWISBenutzer->BenutzerName() . '\',';
                    $SQL .='     sysdate';
                    $SQL .='   )';
                    
                    if($DB->Ausfuehren($SQL)===false)
                    {
                        throw new awisException('Fehler beim Hinzuf�gen des Datensatzes',201409231422,$SQL,awisException::AWIS_ERR_SYSTEM);
                    }
                     
                }
                              
                $rsVSAFIL->DSWeiter();
            }
            
        }
        elseif(strlen($_POST['txtVSK_FILIALEN'])) //nur einzelne Filialen
        { 
            $Filialen = explode(',',$_POST['txtVSK_FILIALEN']);
            foreach($Filialen as $Filiale) 
            {
                $SQL ='INSERT';
                $SQL .=' INTO visitenkartenabfragenfilialen';
                $SQL .='   (';
                $SQL .='     VSF_FILID,';
                $SQL .='     VSF_VSA_ID,';
                $SQL .='     VSF_USER,';
                $SQL .='     VSF_USERDAT';
                $SQL .='   )';
                $SQL .='   VALUES';
                $SQL .='   (';
                $SQL .='     \'' .  $Filiale . '\',';
                $SQL .='     (Select max(VSA_KEY) as lkey from Visitenkartenabfragen),';
                $SQL .='     \'' . $AWISBenutzer->BenutzerName() . '\',';
                $SQL .='     sysdate';
                $SQL .='   )';
            
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Hinzuf�gen des Datensatzes',201409231422,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            } 
        }       
        else 
        {
            $Form->Hinweistext($AWISSprachKonserven['VSK']['VSK_FILFALSCH']);
        }

        
        //Importierte die aktuellen Daten 
        if($DelFlag == true) //L�schen wenn es bereits eine Abfrage im Zeitraum f�r eine der zu hinzuzuf�genden Filialen gibt. Da die Maske nur von einer Person bedient wird, und es immer nur beim letzten Datensatz zu so einem Problem kommen kann, kann man den Key mit max() abfragen.
        {
            $SQLDelete = 'DELETE from  Visitenkartenabfragenfilialen WHERE VSF_VSA_ID = (select max(vsa_key) from visitenkartenabfragen)' ;
            $DB->Ausfuehren($SQLDelete);
            $SQLDelete = 'Delete from VISITENKARTENABFRAGEN where vsa_key = (select max(vsa_key) from visitenkartenabfragen)';
            $DB->Ausfuehren($SQLDelete);
            $Form->Hinweistext($AWISSprachKonserven['VSK']['VSK_ABFRAGEVORHANDEN'] .  $FalscheFilialen );
        }
        else 
        {
            
            $SQLImport ='insert into visitenkarten';
            $SQLImport .=' (VSK_PER_NR,';
            $SQLImport .='       VSK_VORNAME,';
            $SQLImport .='       VSK_NACHNAME,';
            $SQLImport .='       VSK_TAETIGKEIT,';
            $SQLImport .='       VSK_TELEFON,';
            $SQLImport .='       VSK_FAX ,';
            $SQLImport .='       VSK_EMAIL,';
            $SQLImport .='       VSK_FILID,';
            $SQLImport .='       VSK_FILBEZEICHNUNG,';
            $SQLImport .='       VSK_FILSTRASSE,';
            $SQLImport .='       VSK_FILORT,';
            $SQLImport .='       VSK_FILPLZ,';
            $SQLImport .='       VSK_USER,';
            $SQLImport .='       VSK_USERDAT,';
            $SQLImport .='       VSK_STATUS, VSK_VSA_ID)';
            $SQLImport .=' (Select PER_NR,';
            $SQLImport .='       PER_VORNAME,';
            $SQLImport .='       PER_NACHNAME,';
            $SQLImport .='       PET_TAETIGKEIT,';
            $SQLImport .='       Telefon,';
            $SQLImport .='       Fax,';
            $SQLImport .='       email,';
            $SQLImport .='       Filialnummer,';
            $SQLImport .='       Filbezeichnung,';
            $SQLImport .='       Filstrasse,';
            $SQLImport .='       FilOrt,';
            $SQLImport .='       FilPLZ,';
            $SQLImport .='       \'IMPORT\' as Username,';
            $SQLImport .='       sysdate as Userdat,';
            $SQLImport .='       1 as status, ' . '(Select max(VSA_KEY) as lkey from Visitenkartenabfragen)' . ' as VSK_VSA_ID from(';
            $SQLImport .=' SELECT PER_NR,PER_NACHNAME,PER_VORNAME, PET_TAETIGKEIT,(Select fif_wert from filialinfos where fif_fit_id = 1 and fif_fil_id = per_fil_id and fif_imq_id = 2) as Telefon,(Select fif_wert from filialinfos where fif_fit_id = 2 and fif_fil_id = per_fil_id and fif_imq_id = 2) as Fax,lpad(per_fil_id,4,0)||\'@de.atu.eu\' as email,';
            $SQLImport .=' per_fil_id as Filialnummer,';
            $SQLImport .=' (Select fif_wert from filialinfos where fif_fit_id = 61 and fif_fil_id = per_fil_id and fif_imq_id = 32) as FilBezeichnung,';
            $SQLImport .=' (Select fif_wert from filialinfos where fif_fit_id = 62 and fif_fil_id = per_fil_id and fif_imq_id = 32) as FilStrasse,';
            $SQLImport .=' (Select fif_wert from filialinfos where fif_fit_id = 64 and fif_fil_id = per_fil_id and fif_imq_id = 32) as FilOrt,';
            $SQLImport .=' (Select fif_wert from filialinfos where fif_fit_id = 63 and fif_fil_id = per_fil_id and fif_imq_id = 32) as FilPLZ';
            $SQLImport .=' FROM PERSONAL';
            $SQLImport .=' INNER JOIN PERSONALEINSATZPLANUNG ON PER_Nr = PEP_PER_Nr';
            $SQLImport .=' INNER JOIN PERSONALTAETIGKEITEN';
            $SQLImport .=' ON PET_ID = PER_PET_ID';
            $SQLImport .=' LEFT OUTER JOIN Laender ON per_lan_wws_kenn = LAN_WWSKENN';
            $SQLImport .=' WHERE ';
            $SQLImport .=' (PER_AUSTRITT IS NULL OR PER_AUSTRITT > sysdate)';
            $SQLImport .=' and (PER_EINTRITT < sysdate)';
            $SQLImport .=' and personaltaetigkeiten.pet_lan_wws_kenn = \'BRD\'';
            $SQLImport .=' and (PER_NR not between 971000 AND 979999)';
            $SQLImport .=' and (PET_TAETIGKEIT not like \'%Azubi%\') ';
            $SQLImport .=' and (PET_TAETIGKEIT not like \'%Auszubi%\') ';
            $SQLImport .=' )';
            $SQLImport .=' group by PER_NR,PER_NACHNAME,PER_VORNAME, PET_TAETIGKEIT,Telefon,Fax,email,Filbezeichnung,Filstrasse,FilPLZ,FilOrt,Filialnummer)';

            if($DB->Ausfuehren($SQLImport)===false)
            {
                throw new awisException('Fehler beim Hinzuf�gen des Datensatzes','201409261416',$SQL,awisException::AWIS_ERR_SYSTEM);
            }
        
        }
        
    }
    
    if ((!isset($_GET['New']) && !isset($_GET['VSA_KEY']) OR (count($_POST ) == 0 && !isset($_GET['New'])))) { //Kein neuer Datensatz anlegen, also Liste anzeigen
        
        $Param = $AWISBenutzer->ParameterLesen('AktuellerWEB');
        
        $SQL = 'Select * from Visitenkartenabfragen';
        
        
        $rsVSA = $DB->RecordSetOeffnen($SQL);
        
     
        if ($AWIS_KEY1 != 0) {
            $EditRecht = $Recht35000 & 2;
            $Icons     = '';
            $Form->ZeileStart();

            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }
        
        $IconsUeberschrift[] = array('new','visitenkarten_Main.php?cmdAktion=Abfragen&Seite=Abfragen&New=1');

        $Form->Erstelle_ListeIcons($IconsUeberschrift, 38, -1, $AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen'] );
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_KEY'], 60, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_FILIALEN'], 200, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_DATUMVON'], 200, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_DATUMBIS'], 200, '');
        $Form->ZeileEnde();
        
        $FGKZeile = 0;
        $DS       = 0;
        
        while (!$rsVSA->EOF()) {
            $Form->ZeileStart();
            if (intval($Recht35000 & 4) > 0) // �ndernrecht
                {
                $VSK_KEY = $rsVSA->FeldInhalt('VSA_KEY');
                    
                $Icons[0] = array(
                    'delete',
                    './visitenkarten_Main.php?cmdAktion=Abfragen&Seite=Zuordnung&Del=1' . '&VSA_KEY=' . $VSK_KEY
                );
                $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));
            }

            
            $Export[0] = array(
                'report',
                './visitenkarten_Export.php?ExportVSK_KEY=' .$VSK_KEY
            );
            
            
            $Link = './visitenkarten_Main.php?cmdAktion=Details&VSA_KEY=' . $rsVSA->FeldInhalt('VSA_KEY') . '';
            $Form->Erstelle_ListenFeld('VSA_KEY', $rsVSA->FeldInhalt('VSA_KEY'), 0, 60, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('VSA_NAME', $rsVSA->FeldInhalt('VSA_NAME'), 0, 200, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('VSA_DATUMVON', $rsVSA->FeldInhalt('VSA_DATUMVON'), 0, 200, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('VSA_DATUMBIS', $rsVSA->FeldInhalt('VSA_DATUMBIS'), 0, 200, false, ($FGKZeile % 2), '', '');
          
            $Form->Erstelle_ListeIcons($Export, 20, ($FGKZeile % 2));
            
            $Form->ZeileEnde();
            
            $FGKZeile++;
            $rsVSA->DSWeiter();
            
            
        }
    } elseif ($flagNewDS == true) { //Neuer Datensatz 
        $Felder = array();
        
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./visitenkarten_Main.php?cmdAktion=Abfragen"   .  "><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Form->InfoZeile($Felder, '');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_NAME'] . ':', 150, '', '', 'Name der Abfrage');
        $Form->Erstelle_TextFeld('VSA_NAME', '', 20, 200, True);
        
        $Form->ZeileEnde();
        $Form->ZeileStart();
        
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_DATUMVON'] . ':', 150, '', '', 'Datum im Format 25.10.2010 eingeben');
        $Form->Erstelle_TextFeld('VSK_DATUMVON', '', 20, 200, True, '', '', '', 'D', 'L', '', $Form->PruefeDatum(date('d.m.Y')), 10);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_DATUMBIS'] . ':', 150, '', '', 'Datum im Format 25.10.2010 eingeben');
        $Form->Erstelle_TextFeld('VSK_DATUMBIS', '', 20, 200, True, '', '', '', 'D', 'L', '', $Form->PruefeDatum(date('d.m.Y')), 10);
        
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILIALEN'] . ':', 150, '', '', 'Filialenangabe mit Komma trennen --> Example (90,200,30)');
        $Form->Erstelle_TextFeld('VSK_FILIALEN', '', 20, 200, True);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FilialenDeutschAlle'] . ':', 300,'','','Abfrage allen deutschen Filialen freigeben?');
        $Form->Erstelle_Checkbox('*FilialenDeutschAlle', 'off', 30, true, 'on', '', '');
        $Form->ZeileEnde();
    }
  
      $Form->Formular_Ende();
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if($flagNewDS == true OR isset($_GET['VSA_KEY']))
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
    
    $Form->SchaltflaechenEnde();
    
    $Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        if($ex)
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905131842");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>