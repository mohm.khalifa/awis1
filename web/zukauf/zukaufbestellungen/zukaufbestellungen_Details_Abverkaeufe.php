<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZWA','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10003 = $AWISBenutzer->HatDasRecht(10003);
	if(($Recht10003&64)==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZWA_DATUM';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}


	// Daten ermitteln
	$SQL = 'SELECT ZUKAUFWERKSTATTAUFTRAEGE.*';
	$SQL .= ' FROM ZUKAUFWERKSTATTAUFTRAEGE';
	$SQL .= ' WHERE ZWA_ZUB_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);

	$AWIS_KEY2=0;
	if(isset($_GET['ZWA_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['ZWA_KEY']);
		$SQL .= ' AND ZWA_KEY = '.$AWIS_KEY2;
	}

	$SQL .= $ORDERBY;

	$rsZWA = $DB->RecordSetOeffnen($SQL);

	if($rsZWA->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		$Icons=array();
		$Form->Erstelle_ListeIcons($Icons,58,-1);

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Abverkaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZWA_DATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZWA_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZWA']['ZWA_DATUM'],120,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Abverkaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZWA_MENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZWA_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZWA']['ZWA_MENGE'],150,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Abverkaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZWA_VK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZWA_VK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZWA']['ZWA_VK'],150,'',$Link);

		$Form->ZeileEnde();

		$PEBZeile=0;
		$DS=0;
		while(!$rsZWA->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht10003&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Abverkaeufe&ZWA_KEY='.$rsZWA->FeldInhalt('ZWA_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,58,($DS%2));

			$Form->Erstelle_ListenFeld('#ZWA_DATUM',$rsZWA->FeldInhalt('ZWA_DATUM'),0,120,false,($PEBZeile%2),'','','D');
			$Form->Erstelle_ListenFeld('#ZWA_MENGE',$rsZWA->FeldInhalt('ZWA_MENGE'),0,150,false,($PEBZeile%2),'','','Nx','','');
			$Form->Erstelle_ListenFeld('#ZWA_VK',$rsZWA->FeldInhalt('ZWA_VK'),0,150,false,($PEBZeile%2),'','','N2');
			$Form->ZeileEnde();

			$rsZWA->DSWeiter();
			$PEBZeile++;
		}
		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsZWA->FeldInhalt('ZWA_KEY');

		$Form->Erstelle_HiddenFeld('ZWA_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('ZWA_ZUB_KEY',$AWIS_KEY1);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./zukaufbestellungen_Main.php?cmdAktion=Details&ZWAListe=1&Seite=Abverkaeufe accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZWA->FeldInhalt('ZWA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZWA->FeldInhalt('ZWA_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=false;

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_DATUM'].':',190);
		$Form->Erstelle_TextFeld('ZWA_DATUM',$rsZWA->FeldInhalt('ZWA_DATUM'),10,300,$EditRecht,'','','','D');
		$AWISCursorPosition='txtZWA_DATUM';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_WANR'].':',190);
		$Form->Erstelle_TextFeld('ZWA_WANR',$rsZWA->FeldInhalt('ZWA_WANR'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_UMSATZ'].':',190);
		$Form->Erstelle_TextFeld('ZWA_UMSATZ',$rsZWA->FeldInhalt('ZWA_UMSATZ'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_NACHLASS'].':',190);
		$Form->Erstelle_TextFeld('ZWA_NACHLASS',$rsZWA->FeldInhalt('ZWA_NACHLASS'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_VK'].':',190);
		$Form->Erstelle_TextFeld('ZWA_VK',$rsZWA->FeldInhalt('ZWA_VK'),10,300,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_ATUNUMMER'].':',190);
		$Form->Erstelle_TextFeld('ZWA_ATUNUMMER',$rsZWA->FeldInhalt('ZWA_ATUNUMMER'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_BEZEICHNUNG'].':',190);
		$Form->Erstelle_TextFeld('ZWA_BEZEICHNUNG',$rsZWA->FeldInhalt('ZWA_BEZEICHNUNG'),80,900,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_KBANR'].':',190);
		$Form->Erstelle_TextFeld('ZWA_KBANR',$rsZWA->FeldInhalt('ZWA_KBANR'),20,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_KTYP'].':',190);
		$Form->Erstelle_TextFeld('ZWA_KTYP',$rsZWA->FeldInhalt('ZWA_KTYP'),20,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZWA']['ZWA_FAHRGESTELLNR'].':',190);
		$Form->Erstelle_TextFeld('ZWA_FAHRGESTELLNR',$rsZWA->FeldInhalt('ZWA_FAHRGESTELLNR'),20,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();


		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200908131432);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>