<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $AWISBenutzer;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$con = awisLogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

?>
</head>
<body>
<?php

include ("ATU_Header.php");	// Kopfzeile
$con = awisLogon();

$RechteStufe = awisBenutzerRecht($con,507);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Auswertungen Lieferkontrolle',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$cmdAktion='';

if(isset($_GET['cmdAktion']))
{
	$cmdAktion = $_GET['cmdAktion'];
}
if(isset($_POST['cmdSuche_x']))
{
	$cmdAktion='Liste';
}

awis_RegisterErstellen(55, $con, $cmdAktion);

print "<input type=hidden name=cmdAktion value=Liste>";

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";

awislogoff($con);

?>
</body>
</html>

