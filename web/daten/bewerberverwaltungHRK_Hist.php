<?php
//**************************************************************
// Liefert die Daten für den Serienbrief des CRM
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');



// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('HRK','%');

$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$FeldBreiten = array();
$FeldBreiten['Aktion'] = 100;
$FeldBreiten['Alt'] = 170;
$FeldBreiten['Neu'] = 170;
$FeldBreiten['User'] = 150;
$FeldBreiten['Userdat'] = 150;

// Infozeile zusammenbauen
$Felder = array();
$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a title='".$AWISSprachKonserven['HRK']['HRK_HISTCLOSE']."' onclick=closeHist('".$_GET['Tabelle']."') target='_self' name='HRK_HISTCLOSE'><img border=0 src=/bilder/icon_delete.png></a>");
$Form->InfoZeile($Felder,'');


$SQL  = ' Select * from HRKOPF ';
$SQL .= 'where HRK_KEY='.$_GET['KEY'];

$rsHRK = $DB->RecordSetOeffnen($SQL);

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_NACHNAME'] . ':',85);
$Form->Erstelle_TextFeld('HRK_NAME',$rsHRK->FeldInhalt('HRK_NACHNAME'), 10, 90,false, '','', '','', 'L');
	
$Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_VORNAME'] . ':',73);
$Form->Erstelle_TextFeld('HRK_VORNAME',$rsHRK->FeldInhalt('HRK_VORNAME'), 10, 90,false, '','', '','', 'L');

$Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_GEBURTSDATUM'] . ':',108);
$Form->Erstelle_TextFeld('HRK_GEBURTSDATUM',$Form->Format('D',$rsHRK->FeldInhalt('HRK_GEBURTSDATUM')), 10, 100,false, '','', '','', 'L');


$Form->ZeileEnde();



$SQL2  = 'select BEH_AKTION,BEH_ALT,\'Firmierung: \'||FIRMIERUNG||\', Tätigkeit: \'||TAETIGKEIT||\', Eingangsdatum: \'||EINGANGSDATUM as BEH_NEU,BEH_USER,BEH_USERDAT from( ';
$SQL2 .= 'Select BEH_AKTION,BEH_ALT,BEH_USER,BEH_USERDAT, ';
$SQL2 .= '(select hrf_firmierung from bewerberhrzuord ';
$SQL2 .= 'inner join hrfirmierung on hrf_key = bwh_hrf_key where bwh_key=beh_key) as Firmierung,';
$SQL2 .= '(select hrt_such from bewerberinfos ';
$SQL2 .= 'inner join HRTAETIGKEITEN on hrt_key = bif_wert where bif_bwh_key=beh_key and bif_bit_id=3) as Taetigkeit,';
$SQL2 .= '(select bif_wert from bewerberinfos where bif_bwh_key=beh_key and bif_bit_id=2) as Eingangsdatum from ';
$SQL2 .= 'BEWERBERHISTORY hist where BEH_HRK_KEY='.$_GET['KEY'].')';
$SQL2 .= ' where BEH_AKTION = \'Aktivität +\'';;
$SQL2 .= ' order by BEH_USERDAT DESC';
	
$rsHist = $DB->RecordSetOeffnen($SQL2);


$Form->ZeileStart('width:100%;');
$Link= '';
// Überschrift der Listenansicht mit Sortierungslink: Filiale
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_AKTION'], $FeldBreiten['Aktion'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_ALT'], $FeldBreiten['Alt'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_NEU'], $FeldBreiten['Neu'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_USER'], $FeldBreiten['User'], '', $Link);
// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMP_QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMP_QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_USERDAT'], $FeldBreiten['Userdat'], '', $Link);
$Form->ZeileEnde();
$DS2 = 0;
if($rsHist->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('<div id=srcHist style="width: 100%; height: 180px;  overflow-y: scroll;">');
}

while(! $rsHist->EOF())
{
	$Form->ZeileStart('width:100%');
	$TTT = $rsHist->FeldInhalt('BEH_AKTION');
	$Form->Erstelle_ListenFeld('BEH_AKTION', $rsHist->FeldInhalt('BEH_AKTION'), 0, $FeldBreiten['Aktion'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_ALT');
	$Form->Erstelle_ListenFeld('BEH_ALT',strlen($rsHist->FeldInhalt('BEH_ALT')) > 20 ? substr($rsHist->FeldInhalt('BEH_ALT'),0,17).'...':$rsHist->FeldInhalt('BEH_ALT'), 0, $FeldBreiten['Alt'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_NEU');
	$Form->Erstelle_ListenFeld('BEH_NEU',strlen($rsHist->FeldInhalt('BEH_NEU')) > 20 ? substr($rsHist->FeldInhalt('BEH_NEU'),0,17).'...':$rsHist->FeldInhalt('BEH_NEU'), 0, $FeldBreiten['Neu'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_USER');
	$Form->Erstelle_ListenFeld('BEH_USER',$rsHist->FeldInhalt('BEH_USER'), 0, $FeldBreiten['User'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$TTT =  $rsHist->FeldInhalt('BEH_USERDAT');
	$Form->Erstelle_ListenFeld('BEH_USERDAT',$rsHist->FeldInhalt('BEH_USERDAT'), 0, $FeldBreiten['Userdat'], false, ($DS2%2), '','', 'T', 'L', $TTT);
	$Form->ZeileEnde();
	$DS2++;
	$rsHist->DSWeiter();
}

if($rsHist->AnzahlDatensaetze() > 9)
{
	$Form->SchreibeHTMLCode('</div>') ;
}


?>