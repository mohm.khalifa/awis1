<?php

require_once 'awisBerichte.inc';

class ber_Gutscheinaktion
    extends awisBerichte
{
    /**
     * @var reifen_funktionen
     */
    private $GUT;

    public function init(array $Parameter)
    {

        global $REX;
        $this->REX = $REX;

        $AWIS_KEY1 = $_POST['txtGUT_KEY'];


        //********************************************************
        // SQL der Seite
        //********************************************************
        $SQL  ='SELECT GUT_KEY,';
        $SQL .='   GUT_VORGANGSNUMMER,';
        $SQL .='   GUT_BETRAG,';
        $SQL .='   GUT_NACHNAME,';
        $SQL .='   GUT_VORNAME,';
        $SQL .='   GUT_STRASSE,';
        $SQL .='   GUT_KREDITINSITUT,';
        $SQL .='   GUT_PLZ,';
        $SQL .='   GUT_BIC,';
        $SQL .='   GUT_ORT,';
        $SQL .='   GUT_IBAN,';
        $SQL .='   GUT_USER,';
        $SQL .='   GUT_USERDAT,';
        $SQL .='   GUT_STATUS,';
        $SQL .='   GUT_GUTSCHEINNR,';
        $SQL .='   GUT_IBAN_LAND,';
        $SQL .='   GUT_FIL_ID,';
        $SQL .='   FIL_ORT';
        $SQL .=' FROM GUTSCHEINAKTION ';
        $SQL .= ' left join filialen ';
        $SQL .= ' on gut_fil_id = filialen.fil_id ';
        $SQL .= 'WHERE GUT_KEY =  ' . $this->_DB->WertSetzen('GUT','N0',$AWIS_KEY1);



      //  $REX->DB->TransaktionBegin();
        $this->_rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('GUT'));
       /* $SQL = 'UPDATE REIFENREKLAMATIONEN set REX_STATUS = \'G\'';
        $SQL .= 'WHERE REX_KEY in ( ' . $REX_KEYs . ')';
        $this->REX->DB->Ausfuehren($SQL);*/
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        global $GUT;
        $this->SetTitle('Reifenreklamation');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISBenutzer->BenutzerName());
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(1);        // Erste Seite aus der Vorlage importieren
        $this->useTemplate($pdf_vorlage);

        $LetzterHersteller = '';
        $PosCnt = 1;
        while (!$this->_rec->EOF()) {


            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesse']);

            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Zeile += 5;
            //Vorgangsnummer
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->TextInKaestchen($this->_rec->FeldInhalt('GUT_VORGANGSNUMMER'),5,5);


            //Betrag
            $this->_Spalte = $this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandLinkerRandBetrag'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->TextInKaestchen($this->_rec->FeldInhalt('GUT_BETRAG','N2'),5,5,'N2');


            //Gutscheinnummer
            $this->_Zeile = $this->GetY() + $this->_Parameter['AbstandGSnrVonVorgang'] ;
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->TextInKaestchen($this->_rec->FeldInhalt('GUT_GUTSCHEINNR'),5,5);


            //Name
            $this->_Zeile = $this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandKundenDatenVonSeiteOben'] ;
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_NAME'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_NACHNAME'), 0, 0, 'L');


            //Vorname
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_VORNAME'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_VORNAME'), 0, 0, 'L');


            //Adresse
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_ADRESSE'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_STRASSE'), 0, 0, 'L');


            //PLZ
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_PLZ'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_PLZ'), 0, 0, 'L');


            //ORT
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_ORT'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_ORT'), 0, 0, 'L');

            //IBAN
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_IBAN'], 0, 0, 'L');
            $this->SetXY($this->GetX()-1, $this->GetY()+7);
            $this->TextInKaestchen($this->_rec->FeldInhalt('GUT_IBAN_LAND') . $this->_rec->FeldInhalt('GUT_IBAN'),4,4,'IBAN');


            //Kreditinsitut
            $this->_Zeile = $this->GetY();
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'];
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_KREDITINSTITUT'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_KREDITINSITUT'), 0, 0, 'L');


            //BIC
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['KundendatenLabelBreite'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_BIC'], 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_BIC'), 0, 0, 'L');


            //Unterschriftenzeile oben
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten'] * 2;
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['BreiteDatum'], $this->_Parameter['ZeilenHoehe'], date('d.m.Y'), 0, 0, 'L');
            $this->Cell($this->_Parameter['BreiteOrt'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_ORT'), 0, 0, 'L');
            $this->Cell($this->_Parameter['BreiteUnterschrift'], $this->_Parameter['ZeilenHoehe'], '', 0, 0, 'L');

            //Unterschriftenzeile unten
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseUnterschrift']);
            $this->_Zeile = $this->GetY() +   $this->_Parameter['ZeilenAbstandKundendaten']-2;
            $this->_Spalte = $this->_Parameter['LinkerRand']  + $this->_Parameter['AbstandLinkerRandKundenadresse'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);


            $this->Line($this->GetX(),$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5,50,$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5);
            $this->Line($this->GetX()+35,$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5,100,$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5);
            $this->Line($this->GetX()+90,$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5,180,$this->_Zeile+ $this->_Parameter['ZeilenAbstandKundendaten']*1.5);
            $this->_Zeile = $this->GetY() + 1;
            $this->SetXY($this->GetX(),$this->_Zeile);
            $this->Cell($this->_Parameter['BreiteDatum'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_DATUM'], '', 0, 'L');
            $this->Cell($this->_Parameter['BreiteOrt'], $this->_Parameter['ZeilenHoehe'],$GUT->AWISSprachKonserven['GUT']['GUT_PDF_ORT'], '', 0, 'L');
            $this->Cell($this->_Parameter['BreiteUnterschrift'], $this->_Parameter['ZeilenHoehe'], $GUT->AWISSprachKonserven['GUT']['GUT_PDF_UNTERSCHRIFT'], 'U', 0, 'L');





            $this->_rec->DSWeiter();

        }




        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************
       // $this->REX->DB->TransaktionCommit();
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function TextInKaestchen($Text, $Kaestchenhoehe, $Kaestchenbreite,$Format = ''){
        $Len = strlen($Text);
        $Text = str_split($Text);
        $i=0;
        foreach($Text as $T){

            if($Format=='N2'){
                if($i+3 == $Len){
                    $this->Cell(2, 8, $T, 0, 0, 'C');
                }else{
                    $this->Cell($Kaestchenbreite, $Kaestchenhoehe, $T, 1, 0, 'C');
                }
            }elseif($Format=='IBAN'){
                if(($i%4)==0){
                    $this->Cell(1.5, 8, '', 0, 0, 'C');
                }
                $this->Cell($Kaestchenbreite, $Kaestchenhoehe, $T, 1, 0, 'C');
            }
            else
            {
                $this->Cell($Kaestchenbreite, $Kaestchenhoehe, $T, 1, 0, 'C');
            }
            $i++;

        }
        //$this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('GUT_VORGANGSNUMMER'), 0, 0, 'L');

    }

    private function NeueSeite()
    {
        $this->AddPage('P');
    }
}