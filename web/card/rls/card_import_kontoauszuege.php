<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

?>

<html>
<head>
<title>Import</title>
<?php
	include("card/css/css_card_master.php");	//Style-Sheets
?>

</head>
<?php
if($intZeitmessung)
{
?>
	<script language="JavaScript">
		top.card_ladezeit.startzeit();
	</script>
<?php
}
?>
<body onload="init();">
<?php


//Abfrage ob Datens�tze in kontoauszuege vorhanden sind, welche noch nicht nach kontoauszuege transferiert worden sind
$strSql = "select count(*) anzahl from kontoauszuege where IKA_TRANSFER_IN_KONTOAUSZUEGE=0 and IKA_LOESCH=0";

$arrTransferAnz=abfrage("ora",$strSql);
$intTransferAnz = $arrTransferAnz["trefferarray"]["ANZAHL"][0];


if($intTransferAnz&&!(isset($strIdChecked)&&$strIdChecked=='impo1')||$strIdChecked=='impo3')
{
	?>
	<script language="JavaScript">
	function kontoauszug()
	{
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit("Zu transferierende Kontoausz�ge anzeigen");	//Zeitmessung
		<?php } ?>
		
		var parameter = "intDialog=1&<?php echo $QUERY_STRING;?>";
	
		<?php echo dialog("kaztransfer_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			if(rueckgabe=='1')
				alert(unescape("Alle Kontoausz%FCge des aktuellen Imports\nsind verarbeitet worden!"));
			else
			{
				if(rueckgabe=='2')
					alert(unescape("F%FCr diese Abfrage liegen keine entsprechenden Datens%E4tze vor!"));
				else
				{
					if(rueckgabe=='3')
						alert(unescape("F%FCr diese Abfrage liegen keine weiteren Datens%E4tze vor!"));
				}
			}
		}
	}
	
	</script>
	<?php
}
else
{
	$strAusgabe="alert('Zurzeit liegen keine Datens�tze vor,\\nwelche noch nicht �bernommen worden sind!');";

	/*$arrFeldlaenge = array(5,24,12,8,8,8,23,3,3,27,27,27,27,35,23,23,27,27,12,24);  //entspricht den Feldl�ngen in der Importdatei
	$arrFeldtyp = array(1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,0);					// 0 = Text oder Datum, 1 = Zahl
	$arrFeldnamen = array("IKA_AUSZUGNR","IKA_KONTONR","IKA_BANK","IKA_AUSZUGSDATUM",
	"IKA_BUCHUNGSDATUM","IKA_VALUTADATUM","IKA_BETRAG","IKA_WAEHRUNG","IKA_TEXTSCHLUESSEL",
	"IKA_BUCHUNGSTEXT","IKA_VERWENDUNGSZWECK1","IKA_VERWENDUNGSZWECK2","IKA_VERWENDUNGSZWECK3",
	"IKA_KUNDENREFERENZ","IKA_STARTSALDO","IKA_SCHLUSSALDO","IKA_AUFTRAGGEBER1","IKA_AUFTRAGGEBER2",
	"IKA_AUFTRAGGEBER_BLZ","IKA_AUFTRAGGEBER_KONTO");
	
	$strDatei = $strPfadKaz.$strDateinameKaz;
	
	$intZeilenzaehler = 0;
	
	if(is_file($strDatei)&&is_readable($strDatei))	//Wenn Datei vorhanden und lesbar ist
	{
		$intSchalterDoppelt = 0;
		$handlerLesen=fopen($strDatei,"r");	//Datei zum Lesen �ffnen
	
		while(($strZeile=fgets($handlerLesen,10000))&&!$intSchalterDoppelt)	//Zeilenweise lesen
		{
			$intZeilenzaehler++;	//Z�hlt die Zeilen
			$intPoszaehler = 0;		//Position innerhalb der Zeile
			$strSqlPruef = "select count(*) anzahl from kontoauszuege where ";		//Anfang SQL-Statement Pruefung ob DS schon vorhanden
			$strSqlAnf = "insert into kontoauszuege(ika_id,"; 		//Anfang SQL-Statement
			$strSqlEnde = " values(seq_ika_id.nextval,";			//Ende SQL-Statement
			
			for($i=0;$i<count($arrFeldlaenge);$i++)
			{
				$divWert = trim(substr($strZeile,$intPoszaehler,$arrFeldlaenge[$i]));
				$intNull = empty($divWert);		//Testet ob Nullwert vorliegt
						
				if($arrFeldtyp[$i])	//Zahl
				{
					$divWert = str_replace(",",".",$divWert);
				}
				else	//Text oder Datum
				{
					$divWert = "'".str_replace("'","''",$divWert)."'";
				}
				
				if($intNull)	//Aufbau von $strSqlPruef gem�ss Variable $intNull
				{
					$strSqlPruef .= $arrFeldnamen[$i]." is null and ";
				}
				else
				{
					$strSqlPruef .= $arrFeldnamen[$i]."=".$divWert." and ";
				}
												
				$strSqlAnf .= $arrFeldnamen[$i].",";
				$strSqlEnde .= $divWert.",";
				
				$intPoszaehler += $arrFeldlaenge[$i]+1;		//Postitionszaehler hochsetzen
			}
			
			$strSqlPruef = substr($strSqlPruef,0,strlen($strSqlPruef)-4);	//letztes and wegschneiden
			
			$arrPruef = abfrage("ora",$strSqlPruef);
		
			if($arrPruef["trefferarray"]["ANZAHL"][0]>0)	//Wenn doppelter Datensatz vorhanden ist
			{
				$intSchalterDoppelt=1;
			}
			else
			{
				$strSqlAnf .= "IKA_AEND_BENUTZER,IKA_AEND_DATUM)";
				//$strSqlAnf = substr($strSqlAnf,0,strlen($strSqlAnf)-1).")";	//letztes Komma weg und Klammer setzen
				$strSqlEnde .= "'".$PHP_AUTH_USER."',sysdate)";
				//$strSqlEnde = substr($strSqlEnde,0,strlen($strSqlEnde)-1).")";	//letztes Komma weg und Klammer setzen
				
				$strSql = $strSqlAnf.$strSqlEnde;
								
				$arrRueckgabe = abfrage("ora",$strSql,1);		//1 = Es wird vorerst kein Commit gesetzt
			}
		}	//Ende while-Schleife
	
		if($intSchalterDoppelt)		//Wenn bereits identische Datens�tze importiert worden sind
		{
			@ocirollback($hdlOraVerbindung);
			$strAusgabe="alert('Die Datei \"".$strDateinameKaz."\" enth�lt Datens�tze,\\nwelche bereits importiert worden sind!\\n\\n�berpr�fen Sie bitte die Datei!');";
		}
		else
		{		
			$intRueck = @ocicommit($hdlOraVerbindung) or orafehler($hdlOraAnweisung,$strSql);  //Commit wird gesetzt, wenn alle Datens�tze eingelesen sind
		
			if($intRueck)
			{
				rename($strDatei,$strDatei."_ok");	//Importdatei umbenennen
				$strAusgabe= "alert('".$intZeilenzaehler." Datens�tze erfolgreich importiert!');location.href='".$PHP_SELF."';";
			}
		}
		
		fclose($handlerLesen);		//Dateihandler schlie�en
	}
	else
	{
		$strAusgabe="alert('Stellen Sie bitte die Datei \"".$strDateinameKaz."\" bereit!');";
	}
	*/
}

//JavaScript-Block f�r Ausgabe (wird �ber body onload aufgerufen)

echo "<script language=\"JavaScript\">";
echo "function init(){";

if(isset($strAusgabe)&&$strAusgabe!='')
{
	if($intZeitmessung)
		echo "parent.card_ladezeit.endezeit();";
	echo $strAusgabe;
}
else
{
	echo "kontoauszug();";
}
echo "}";
echo "</script>";

include("card/card_zeitende.inc.php");			// Zeitmessung
?>
</body>
</html>