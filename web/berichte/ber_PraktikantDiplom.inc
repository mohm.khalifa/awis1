<?php
require_once 'awisBerichte.inc';

class ber_PraktikantDiplom
	extends awisBerichte
{
	private $pdf_vorlage = '';
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		// Daten f�r den Bericht laden
        $SQL = "";
        $SQL .= "SELECT * FROM v_praktikumsvereinbarung";
		
		// Alle Parameter auswerten
		// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
		//			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
		$Bedingung = "";
		if (isset($Parameter['PVS_KEY']))
		{
			$Param = explode('~',$Parameter['PVS_KEY']);
			$Bedingung .= " AND pVs_key " . self::_VergleichsOperatorPruefen($Param[0]) . " " . $this->_DB->FeldInhaltFormat('N0',$Param[1],false);
		}
		
		if ($Bedingung != "")
		{
			$SQL .= " WHERE " . substr($Bedingung, 4);
		}

		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
 
		// Alle Textbausteine laden
		$TextKonserven[]=array('PVS','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eiegntliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetFillColor(192, 192, 192);
		
		$this->ErstelleDoc01();
		
        $this->FertigStellen($Ausgabeart);        
	}

	
	private function ErstelleDoc01()
	{
        $this->NeueSeite();
        //$this->useTemplate($this->pdf_vorlage);
        // Templates gingen zwar in ENTWICK und STAG, aber kurioserweise im PROD nicht.
        // => wegen Lifeschaltung, hier die L�sung, das Bild zu laden und nicht die pdf-Hintergrundvorlage
        $this->Image('/daten/web/dokumente/vorlagen/Zertifikat_Hintergrund.jpg', 0, 0, self::SeitenBreite(), self::SeitenHoehe());
        $this->UeberschriftDiplom();
        		
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		        
		$this->TextblockDiplom01();
		$this->TextblockDiplom02();
		$this->TextblockDiplom03();
		$this->GrussDiplom();		
	}
	
	
	private function UeberschriftDiplom()
	{
	    $KompletteBreite = self::SeitenBreite();
	    
        //$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY(0, 0);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['KopfzeileSchriftgroesse'] * 2);
        $this->SetTextColor(255, 255, 255);
        $this->SetFillColor(222, 0, 49);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 6, $this->_AWISSprachkonserven['PVS']['pdf_DiplomTeilnahme'], 0, 0, 'C', true);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetTextColor(-1);
        $this->SetFillColor(192, 192, 192);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 7;	    	    
	}	
	
	private function TextblockDiplom01()
	{
	    $this->SetMargins($this->_Parameter['LinkerRand'] * 2, $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'] * 5);
        $this->_Spalte = $this->_Parameter['LinkerRand'] * 2;
        $KompletteBreite = self::SeitenBreite() - ($this->_Parameter['LinkerRand'] * 7);	    
	    
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 9;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_DiplomText01']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5;	    	    
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['KopfzeileSchriftgroesse']);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 2, $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_VORNAME') . ' ' . $this->_rec->FeldInhalt('P_NAME'), 0, 0, 'C');
        //$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        //$this->SetXY($this->_Spalte, $this->_Zeile);
        //$this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 2, $this->_rec->FeldInhalt('P_NAME'), 0, 0, 'C');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 2, $this->HoleZeitraum(), 0, 0, 'C');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;
                
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], str_replace('$PERIOD$', $this->HoleZeitraum(), $this->HolePraktikumsArt($this->_rec->FeldInhalt('PVS_PART'))));
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;
  
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_DiplomFiliale'] . ' - ' . $this->HoleFiliale(1), 0, 0, 'C');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(2), 0, 0, 'C');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(3), 0, 0, 'C');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;        
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);        
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_DiplomText03']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;        
	}     
    
	private function TextblockDiplom02()
	{
	    $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);        
        $Text = $this->_AWISSprachkonserven['PVS']['pdf_DiplomText04'];
        $Text = str_replace('$TRAINEE$', $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_NAME'), $Text);
        $Text = str_replace('$AREA$', $this->HoleTaetigkeitbezeichnung(), $Text);
        $this->Write($this->_Parameter['Zeilenhoehe'], $Text);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;	    
	}
	
	private function TextblockDiplom03()
	{
	    $this->SetXY($this->_Spalte, $this->_Zeile);
        if ($this->_rec->Feldinhalt('PVS_ANR_ID') == 2)
        {
            $Text = $this->_AWISSprachkonserven['PVS']['pdf_DiplomText05Frau'];
        }
        else 
        {
            $Text = $this->_AWISSprachkonserven['PVS']['pdf_DiplomText05Mann'];
        }
        
        $Anrede = '';
        if ($this->_rec->FeldInhalt('PVS_ANR_ID') == 2)
        {
        	$Anrede .= $this->_AWISSprachkonserven['PVS']['pdf_AllgFrau'];
        }
        else
        {
        	$Anrede .= $this->_AWISSprachkonserven['PVS']['pdf_AllgHerr'];
        }
        
        $this->Write($this->_Parameter['Zeilenhoehe'], str_replace('$TRAINEE$', $Anrede . ' ' . $this->_rec->FeldInhalt('P_NAME'), $Text));
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;	    	    
	}

    private function GrussDiplom()
    {
		//*****************************************************************************************************************************************************************************
		// Gru� (mfg)
		//*****************************************************************************************************************************************************************************
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_DiplomGruss01']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];    
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_DiplomGruss02']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_VORNAME') . ' ' . $this->_rec->FeldInhalt('B_NAME'));
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];    
        $this->SetXY($this->_Spalte, $this->_Zeile);        
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_Praktikumsbetreuer'] . ' ' . $this->_rec->FeldInhalt('F_BEZ'));        
    }	
	
    
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
		
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{		/*
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,4,$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
			*/
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
	    //$this->pdf_vorlage = $this->ImportPage(1);
	    /*
	    $this->useTemplate($pdf_vorlage);
		
		$Zeile = $this->_Parameter['Kopfzeile1-Oben'];
		$Spalte = $this->_Spalte;

		// Berichtsparameter anzeigen
		$this->SetXY($Spalte,$Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Ueberschrift-Groesse']);
		$this->Cell(30, 4, $this->_AWISSprachkonserven['PVS']['pdf_OibKopf'] ,0 ,0 ,'L' ,'');

		$Zeile+=5;

		// Trennlinie
		$this->SetXY($Spalte,$Zeile);
		$this->Line($this->_Spalte,$Zeile,self::SeitenBreite()-self::RECHTERRAND,$Zeile);
		$Zeile+=2;
		*/
	}	

    public function FertigStellen($Ausgabeart)        
    {
        //*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
    }

    
    
    private function HolePraktikumsArt($PraktikumartKey)
    {
    	$RetVal = '';
    	
    	switch ($PraktikumartKey)
    	{
    		case 1:		// Sch�lerp.
    			$RetVal = $this->_AWISSprachkonserven['PVS']['pdf_DiplomOibText02'];
    		break;
    		case 2:		// berufsvorb. P.
    			$RetVal = $this->_AWISSprachkonserven['PVS']['pdf_DiplomBvbText02'];
    		break;
    		case 3:		// freiwilliges P.
    		break;
    		case 4:		// Lehrstellenbewerber
    		break;
    	}
    	    	
		return $RetVal;    	
    }   
     
    private function HolePraktikumsGrund($PraktikumgrundKey)
    {
    	$RetVal = '';
    	
        $Daten = explode('|', $this->_AWISSprachkonserven['PVS']['lst_PGrund']);
		foreach ($Daten AS $Element)
        {
        	$ListeElement = explode('~', $Element);
            if ($ListeElement[0] == $PraktikumgrundKey)
            {
            	$RetVal .= $ListeElement[1];
                break; 
			}                
		}    	
		return $RetVal;
    }
    
    private function HoleFiliale($Line = 1)
    {
        $Filiale = '';
        switch ($Line)
        {
            case 1: $Filiale = $this->_rec->Feldinhalt('F_BEZ'); break;
            case 2: $Filiale = $this->_rec->Feldinhalt('F_STRASSE'); break;
            case 3: $Filiale = $this->_rec->Feldinhalt('F_PLZ') . " " . $this->_rec->Feldinhalt('F_ORT'); break;
            default: $Filiale = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt']; break;
        }
    	return $Filiale;
    }    

    private function HoleZeitraum()
    {
        if ($this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) == $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS')))
    	{
    	    $Zeitraum = $this->_AWISSprachkonserven['PVS']['pdf_Am'] . " ";
    	    $Zeitraum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " ";
    	}
    	else 
    	{
            $Zeitraum = $this->_AWISSprachkonserven['PVS']['pdf_Von'] . " "
    				  . $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " "
    				  . $this->_AWISSprachkonserven['PVS']['pdf_Bis'] . " "
    			      . $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'));
    	}    	
    	return $Zeitraum;
    }
    
    private function HolePraktikantDatum($MitDatum = true, $NurDatum = false, $HerrMitN = false)
    {
        $PraktikantDatum = '';
        if (! $NurDatum) 
        {
        	if ($HerrMitN)
        	{
        		if ($this->_rec->FeldInhalt('PVS_ANR_ID') == 2)
        		{
        			$PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_AllgFrau'] . " ";
        		}
        		else 
        		{
        			$PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_AllgHerr'] . " ";
        		}
        	}
        	else 
        	{	
            	$PraktikantDatum .= $this->_rec->FeldInhalt('P_ANREDE') . " ";
        	}
            $PraktikantDatum .= $this->_rec->FeldInhalt('P_VORNAME') . " ";
            $PraktikantDatum .= $this->_rec->FeldInhalt('P_NAME') . " ";
        }
        
        if ($MitDatum)
        {
        	if ($this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) == $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS')))
        	{
        	    $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Am'] . " ";
        	    $PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " ";
        	}
        	else 
        	{
        	    $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Von'] . " ";
            	$PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " ";
            	$PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Bis'] . " ";
                $PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'));
        	}
        }
        
    	return $PraktikantDatum;    	
    }       
    
    public function HoleTaetigkeitbezeichnung()
    {
        $Taetigkeit = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
        $Daten = explode('|', $this->_AWISSprachkonserven['PVS']['lst_Taet']);
        foreach ($Daten AS $Felder)
        {
            $Feld = explode('~', $Felder);
            if ($this->_rec->FeldInhalt('PVS_TAETIGKEIT') == $Feld[0])
            {
                $Taetigkeit = $Feld[1];
                break;
            }
        }

        return $Taetigkeit;
    }    

    public function HoleTaetigkeitbezeichnungBewertung($Geschlecht = '')
    {
        if ($Geschlecht == '')
        {
            if ($this->_rec->FeldInhalt('PVS_ANR_ID') == 2)
            {
                $Geschlecht = 'FRAU';
            }
            else 
            {
                $Geschlecht = 'MANN';
            }
        }
        
        $Taetigkeit = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
        
        switch ($this->_rec->FeldInhalt('PVS_TAETIGKEIT'))
        {
            case 1: 
                if ($Geschlecht == 'MANN')
                {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungVerkaufMann'];
                }
                elseif ($Geschlecht == 'FRAU')
                {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungVerkaufFrau'];
                } 
            break;
            case 2: 
                if ($Geschlecht == 'MANN')
                {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungWerkstattMann'];
                }
                elseif ($Geschlecht == 'FRAU')
                {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungWerkstattFrau'];
                } 
            break;        
        }

        return $Taetigkeit;
    }        
}
?>