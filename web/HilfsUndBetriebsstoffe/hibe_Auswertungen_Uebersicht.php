<?php
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $Zeile;
global $AWISBenutzer;

require_once('fpdf.php'); //20130211,ho
require_once 'fpdi.php';

ini_set('max_execution_time',300);

$RechteStufe = awisBenutzerRecht($con, 1100);
if($RechteStufe<256)
{
   awisEreignis(3,1100,'Hilfs- und Betriebsstoffe Auswertungen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

/***************************************
*
* Daten laden
*
***************************************/

if($_POST['txtAuswertung']==1)		// Alle Daten
{
	$SQL = 'SELECT
	FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, MIT_BEZEICHNUNG, MIT_KEY, HBT_BEZEICHNUNG,
	HBS_SERIENNUMMER, HBS_INVENTARNUMMER, HBE_DATUMVOM, HBE_DATUMBIS, HBS_BEMERKUNG, HBS_BAUJAHR, VKG_ID, FIL_BEZ
	FROM FILIALEN
		 INNER JOIN HILFSUNDBETRIEBSSTOFFEINSAETZE ON FILIALEN.FIL_ID = HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_FIL_ID
		 INNER JOIN HILFSUNDBETRIEBSSTOFFE ON HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_HBS_KEY = HILFSUNDBETRIEBSSTOFFE.HBS_KEY
		 INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HILFSUNDBETRIEBSSTOFFTYPEN.HBT_KEY = HILFSUNDBETRIEBSSTOFFE.HBS_HBT_KEY
		 INNER JOIN HILFSUNDBETRIEBSSTOFFGRUPPEN ON HILFSUNDBETRIEBSSTOFFTYPEN.HBT_HBG_KEY = HILFSUNDBETRIEBSSTOFFGRUPPEN.HBG_KEY
		 INNER JOIN FILIALINFOS ON FILIALINFOS.FIF_FIL_ID = FILIALEN.FIL_ID
		 INNER JOIN VERKAUFSGEBIETE ON FILIALINFOS.FIF_WERT = VERKAUFSGEBIETE.VKG_ID
		 INNER JOIN MITARBEITER ON MITARBEITER.MIT_KEY = VERKAUFSGEBIETE.VKG_TKDL_MIT_KEY

	WHERE FILIALINFOS.FIF_FIT_ID = 21';

	if($_POST['txtAuchAlte']!='on')
	{
		$SQL .= ' AND HBE_DATUMBIS >= SYSDATE';
	}

	if($_POST['txtMIT_KEY']<>"-1")
	{
		$SQL .= ' AND MIT_KEY=0' . $_POST['txtMIT_KEY'];
	}
	$SQL .= ' ORDER BY MITARBEITER.MIT_BEZEICHNUNG, VERKAUFSGEBIETE.VKG_ID, FILIALEN.FIL_BEZ, HILFSUNDBETRIEBSSTOFFTYPEN.HBT_BEZEICHNUNG';
}
elseif($_POST['txtAuswertung']==2)
{
	$SQL = "SELECT
	FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, MIT_BEZEICHNUNG, MIT_KEY, HBT_BEZEICHNUNG,
	HBS_SERIENNUMMER, HBS_INVENTARNUMMER, HBE_DATUMVOM, HBE_DATUMBIS, HBS_BEMERKUNG, HBS_BAUJAHR, VKG_ID, FIL_BEZ
	FROM FILIALEN
		 INNER JOIN HILFSUNDBETRIEBSSTOFFEINSAETZE ON FILIALEN.FIL_ID = HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_FIL_ID
		 INNER JOIN HILFSUNDBETRIEBSSTOFFE ON HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_HBS_KEY = HILFSUNDBETRIEBSSTOFFE.HBS_KEY
		 INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HILFSUNDBETRIEBSSTOFFTYPEN.HBT_KEY = HILFSUNDBETRIEBSSTOFFE.HBS_HBT_KEY
		 INNER JOIN HILFSUNDBETRIEBSSTOFFGRUPPEN ON HILFSUNDBETRIEBSSTOFFTYPEN.HBT_HBG_KEY = HILFSUNDBETRIEBSSTOFFGRUPPEN.HBG_KEY
		 INNER JOIN FILIALINFOS ON FILIALINFOS.FIF_FIL_ID = FILIALEN.FIL_ID
		 INNER JOIN VERKAUFSGEBIETE ON FILIALINFOS.FIF_WERT = VERKAUFSGEBIETE.VKG_ID
		 INNER JOIN MITARBEITER ON MITARBEITER.MIT_KEY = VERKAUFSGEBIETE.VKG_TKDL_MIT_KEY

	WHERE FILIALINFOS.FIF_FIT_ID = 21";

	$SQL .= " AND ((HBE_DATUMVOM >= TO_DATE('";
	$SQL .= awis_format($_POST['txtDatumVom'],'Datum');
	$SQL .= "','DD.MM.YYYY')";
	$SQL .= " AND HBE_DATUMVOM <= TO_DATE('";
	$SQL .= awis_format($_POST['txtDatumBis'],'Datum');
	$SQL .= "','DD.MM.YYYY'))";

	$SQL .= " OR (HBE_DATUMBIS >= TO_DATE('";
	$SQL .= awis_format($_POST['txtDatumVom'],'Datum');
	$SQL .= "','DD.MM.YYYY')";
	$SQL .= " AND HBE_DATUMBIS <= TO_DATE('";
	$SQL .= awis_format($_POST['txtDatumBis'],'Datum');
	$SQL .= "','DD.MM.YYYY')))";


	if($_POST['txtMIT_KEY']<>"-1")
	{
		$SQL .= ' AND MIT_KEY=0' . $_POST['txtMIT_KEY'];
	}

	$SQL .=	' ORDER BY MITARBEITER.MIT_BEZEICHNUNG, VERKAUFSGEBIETE.VKG_ID, FILIALEN.FIL_BEZ, HILFSUNDBETRIEBSSTOFFTYPEN.HBT_BEZEICHNUNG';
}
elseif($_POST['txtAuswertung']==3)
{
	$SQL=	'SELECT HBS_INVENTARNUMMER, HBS_SERIENNUMMER,';
    $SQL.=	' HBT_BEZEICHNUNG, HER_BEZEICHNUNG,';
    $SQL.=	' HBE_FIL_ID, FIL_BEZ, HBS_BAUJAHR';
    $SQL.=	' FROM HILFSUNDBETRIEBSSTOFFEINSAETZE INNER JOIN HILFSUNDBETRIEBSSTOFFE';
    $SQL.=	' ON HBE_HBS_KEY = HBS_KEY';
    $SQL.=	' INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HBS_HBT_KEY = HBT_KEY';
    $SQL.=	' INNER JOIN HERSTELLER ON HBT_HER_ID = HER_ID';
	$SQL.=	' LEFT JOIN V_FILIALEN ON HBE_FIL_ID = FIL_ID';
    $SQL.=	' WHERE HBE_DATUMBIS >= SYSDATE';
	if (isset($_POST['txtFIL_ID']) && $_POST['txtFIL_ID']!='-1')
	{
    	$SQL.=	" AND HBE_FIL_ID='0".$_POST['txtFIL_ID']."'";
	}
    $SQL.=	' ORDER BY HBE_FIL_ID';
}

awis_Debug(1,$_REQUEST);
awis_Debug(1,$SQL);

if($_POST['txtAuswertung']!=3)
{
	
/***************************************
* Neue PDF Datei erstellen
***************************************/

define('FPDF_FONTPATH','font/');
$pdf = new fpdi('l','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);


$rsHBE = awisOpenRecordset($con,$SQL);
$rsHBEZeilen = $awisRSZeilen;

if($rsHBEZeilen==0)
{
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,370,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}
else 		// Daten gefunden
{
	$Seite=0;
	$LinkerRand = 10;
	$LetzteFIL_ID='';
	$LetzteMIT_KEY = '';

	for($i=0;$i<$rsHBEZeilen;$i++)
	{
		if($LetzteFIL_ID != $rsHBE['FIL_ID'][$i] OR $Zeile>30)
		{
			$LetzteFIL_ID = $rsHBE['FIL_ID'][$i];
			NeueSeite($pdf,$Zeile,$ATULogo,15,$Seite);


			// �berschrift setzen
			$pdf->SetFont('Arial','',10);				// Schrift setzen
			$pdf->setXY($LinkerRand,16);				// Cursor setzen
			$Text = $rsHBE['FIL_BEZ'][$i];
			$Text .= "\n" . $rsHBE['FIL_STRASSE'][$i];
			$Text .= "\n" . $rsHBE['FIL_PLZ'][$i] . ' ' . $rsHBE['FIL_ORT'][$i];
			$pdf->multicell(90,5,$Text,1,'L',1);

			$pdf->setXY($LinkerRand,31);				// Cursor setzen
			$Text = $rsHBE['MIT_BEZEICHNUNG'][$i] . ', Gebiet: ' . $rsHBE['VKG_ID'][$i];
			$pdf->multicell(90,5,$Text,1,'L',1);


			if($LetzteMIT_KEY != $rsHBE['MIT_KEY'][$i])
			{
				$LetzteMIT_KEY = $rsHBE['MIT_KEY'][$i];
				$Seite = 1;
			}

			$RasterZeile = 40;
			$Zeile = 1;

			$pdf->SetFillColor(220,220,220);

			$pdf->setXY($LinkerRand,$RasterZeile+($Zeile*5));				// Cursor setzen
			$pdf->Cell(80,5,'Bezeichnung',1,0,'L',1);
			$pdf->setXY($LinkerRand+80,$RasterZeile+($Zeile*5));
			$pdf->Cell(50,5,'Serien-Nr',1,0,'L',1);
			$pdf->setXY($LinkerRand+130,$RasterZeile+($Zeile*5));
			$pdf->Cell(30,5,'Inventar-Nr',1,0,'L',1);
			$pdf->setXY($LinkerRand+160,$RasterZeile+($Zeile*5));
			$pdf->Cell(20,5,'Baujahr',1,0,'L',1);
			$pdf->setXY($LinkerRand+180,$RasterZeile+($Zeile*5));
			$pdf->Cell(20,5,'Einsatz ab',1,0,'L',1);
			$pdf->setXY($LinkerRand+200,$RasterZeile+($Zeile*5));
			$pdf->Cell(20,5,'Einsatz bis',1,0,'L',1);
			$pdf->setXY($LinkerRand+220,$RasterZeile+($Zeile*5));
			$pdf->Cell(50,5,'Bemerkung',1,0,'L',1);

			$Zeile++;
		}	// �berschrift

		$pdf->SetFillColor(255,255,255);

		$pdf->setXY($LinkerRand,$RasterZeile+($Zeile*5));				// Cursor setzen
		$pdf->Cell(80,5,$rsHBE['HBT_BEZEICHNUNG'][$i],1,0,'L',1);
		$pdf->setXY($LinkerRand+80,$RasterZeile+($Zeile*5));
		$pdf->Cell(50,5,$rsHBE['HBS_SERIENNUMMER'][$i],1,0,'L',1);
		$pdf->setXY($LinkerRand+130,$RasterZeile+($Zeile*5));
		$pdf->Cell(30,5,$rsHBE['HBS_INVENTARNUMMER'][$i],1,0,'L',1);
		$pdf->setXY($LinkerRand+160,$RasterZeile+($Zeile*5));
		$pdf->Cell(20,5,$rsHBE['HBS_BAUJAHR'][$i],1,0,'L',1);
		$pdf->setXY($LinkerRand+180,$RasterZeile+($Zeile*5));
		$pdf->Cell(20,5,$rsHBE['HBE_DATUMVOM'][$i],1,0,'L',1);
		$pdf->setXY($LinkerRand+200,$RasterZeile+($Zeile*5));
		$pdf->Cell(20,5,($rsHBE['HBE_DATUMBIS'][$i]=='31.12.2099'?'':$rsHBE['HBE_DATUMBIS'][$i]),1,0,'L',1);
		$pdf->setXY($LinkerRand+220,$RasterZeile+($Zeile*5));
		$pdf->Cell(50,5,$rsHBE['HBS_BEMERKUNG'][$i],1,0,'L',1);

		$Zeile++;

	}

}	// Ende Datenaufbereitung


/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
//$pdf->saveas($DateiName);
$pdf->Output($DateiName, 'F');
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";

}
else 
{

	$DateiName = awis_UserExportDateiName('.csv');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		
	$rsExport = awisOpenRecordset($con,$SQL,true,true);
	$rsExportZeilen = $awisRSZeilen;

	$fd = fopen($DateiName,'w+');
	if($fd==FALSE)
	{
		die($DateiName);
	}
	
	fputs($fd, "INVENTAR-NR;SERIEN-NR;GER�T;HERSTELLER;FILIALE;FILIALBEZEICHNUNG;BAUJAHR\n");
	for($DSNr=0;$DSNr<$rsExportZeilen;$DSNr++)
	{
		fputs($fd,$rsExport['HBS_INVENTARNUMMER'][$DSNr] . ';' . $rsExport['HBS_SERIENNUMMER'][$DSNr] . ';' . $rsExport['HBT_BEZEICHNUNG'][$DSNr] . ';' . $rsExport['HER_BEZEICHNUNG'][$DSNr] . ';' . $rsExport['HBE_FIL_ID'][$DSNr] . ';' . $rsExport['FIL_BEZ'][$DSNr] . ';' . $rsExport['HBS_BAUJAHR'][$DSNr] . "\n");
	}
	fclose($fd);
	
	echo '<br><a href='.$DateiNameLink.' target="_blank">CSV Datei �ffnen</a>';
	
}

/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,&$Seite)
{
	if($Seite > 0)		// Fu�zeile?
	{
		$pdf->SetFont('Arial','',6);					// Schrift setzen
		$pdf->setXY($LinkerRand+270,200);					// Cursor setzen
		$pdf->Cell(50,3,'Seite ' . $Seite,0,0,'L',0);

		$pdf->setXY($LinkerRand,202);					// Cursor setzen
		$pdf->SetFont('Arial','',3);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(270,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
	}
	$Seite++;

	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,270,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(270,6,"�bersicht Hilfs- und Betriebsstoffe",0,0,'C',0);

	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',6);				// Schrift setzen
	$pdf->cell(270,6,"Stand: " . date('d.m.Y'),0,0,'C',0);
	if($_POST['txtAuswertung']==2)
	{
		$pdf->Cell(270,6,'Alle �nderungen vom ' . $_POST['txtDatumVom'] . ' bis zum ' . $_POST['txtDatumBis'],0,0,'C',0);
	}


	$Zeile = 22;
}
?>