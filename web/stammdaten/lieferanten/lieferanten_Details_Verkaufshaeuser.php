<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LVK', '%');
    $TextKonserven[] = array('XBN', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht610 = $AWISBenutzer->HatDasRecht(610);
    if ($Recht610 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LVK'));
    if (!isset($_GET['SSort'])) {
        if (isset($Param['ORDERBY']) AND $Param['ORDERBY'] != '') {
            $ORDERBY = $Param['ORDERBY'];
        } else {
            $ORDERBY = ' ORDER BY LVK_NAME1, LVK_NAME2';
        }
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SSort']);
    }
    $Param['ORDERBY'] = $ORDERBY;

    // Daten ermitteln
    $SQL = 'SELECT LIEFERANTENVERKAUFSHAEUSER.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM LIEFERANTENVERKAUFSHAEUSER ';
    $SQL .= ' WHERE LVK_LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);

    $AWIS_KEY2 = 0;
    if (isset($_GET['LVK_KEY'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('N0', $_GET['LVK_KEY']);
        $SQL .= ' AND LVK_KEY = ' . $AWIS_KEY2;
    }

    $SQL .= $ORDERBY;

    if ($Param['KEY'] != $AWIS_KEY1) {
        $Param['BLOCK'] = 1;
    }
    $Param['KEY'] = $AWIS_KEY2;

    $AWISBenutzer->ParameterSchreiben('Formular_LVK', serialize($Param));
    
    // Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
    if ($AWIS_KEY2 <= 0) {
        // Zum Blättern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
            $AWISBenutzer->ParameterSchreiben('Formular_LVK', serialize($Param));
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsLVK = $DB->RecordSetOeffnen($SQL);

    if ($rsLVK->AnzahlDatensaetze() > 1 OR $AWIS_KEY2 == 0)                        // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();

        if ((intval($Recht610) & 4) != 0) {
            $Icons[] = array('new', './lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $AWIS_KEY1 . '&Seite=Verkaufshaeuser&LVK_KEY=-1');
            $Form->Erstelle_ListeIcons($Icons, 58, -1);
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LVK_NAME1' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LVK_NAME1'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVK']['LVK_NAME1'] . ',' . $AWISSprachKonserven['LVK']['LVK_NAME2'], 350, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LVK_STRASSE' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LVK_STRASSE'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVK']['LVK_STRASSE'], 350, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LVK_LAN_CODE' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LVK_LAN_CODE'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVK']['LVK_LAN_CODE'], 70, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LVK_ORT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LVK_ORT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVK']['LVK_ORT'], 300, '', $Link);

        $Form->ZeileEnde();

        $PEBZeile = 0;
        $DS = 0;
        while (!$rsLVK->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (intval($Recht610 & 4) > 0)    // Ändernrecht
            {
                $Icons[] = array('edit', './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser&LVK_KEY=' . $rsLVK->FeldInhalt('LVK_KEY'));
            }
            if (intval($Recht610 & 4) > 0)    // Ändernrecht
            {
                $Icons[] = array('delete', './lieferanten_Main.php?cmdAktion=Details&Seite=Verkaufshaeuser&Del=' . $rsLVK->FeldInhalt('LVK_KEY'));
            }
            if ($rsLVK->FeldInhalt('LVK_EMAIL') != '')    // EMail vorhanden?
            {
                $Icons[] = array('mail', 'mailto:' . $rsLVK->FeldInhalt('LVK_EMAIL'));
            }
            $Form->Erstelle_ListeIcons($Icons, 58, ($DS % 2));

            $Form->Erstelle_ListenFeld('#LVK_NAME1', $rsLVK->FeldInhalt('LVK_NAME1') . ' ' . $rsLVK->FeldInhalt('LVK_NAME2'), 0, 350, false, ($PEBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#LVK_STRASSE', $rsLVK->FeldInhalt('LVK_STRASSE'), 0, 350, false, ($PEBZeile % 2), '', '', 'T', '', '');
            $Form->Erstelle_ListenFeld('#LVK_LAN_CODE', $rsLVK->FeldInhalt('LVK_LAN_CODE'), 0, 70, false, ($PEBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#LVK_ORT', $rsLVK->FeldInhalt('LVK_ORT'), 0, 300, false, ($PEBZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsLVK->DSWeiter();
            $PEBZeile++;
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $AWIS_KEY1 . '' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $Form->Formular_Start();

        $AWIS_KEY2 = $rsLVK->FeldInhalt('LVK_KEY');

        $Form->Erstelle_HiddenFeld('LVK_KEY', $AWIS_KEY2);
        $Form->Erstelle_HiddenFeld('LVK_LIE_NR', $AWIS_KEY1);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a class=BilderLink href=./lieferanten_Main.php?cmdAktion=Details&LVKListe=1&Seite=Verkaufshaeuser accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLVK->FeldInhalt('LVK_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLVK->FeldInhalt('LVK_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht610 & 2) != 0);

        if ($AWIS_KEY1 == 0) {
            $EditRecht = true;
        }

        $Form->ZeileStart();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_NAME1'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_NAME1', $rsLVK->FeldInhalt('LVK_NAME1'), 40, 300, $EditRecht, '', '', '', 'T');
        $AWISCursorPosition = 'txtLVK_NAME1';
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_NAME2'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_NAME2', $rsLVK->FeldInhalt('LVK_NAME2'), 40, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_STRASSE'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_STRASSE', $rsLVK->FeldInhalt('LVK_STRASSE'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_PLZ'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_PLZ', $rsLVK->FeldInhalt('LVK_PLZ'), 5, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_LAN_CODE'] . ':', 150);
        $SQL = 'SELECT LAN_CODE, LAN_LAND';
        $SQL .= ' FROM Laender';
        $SQL .= ' ORDER BY LAN_LAND';
        $Form->Erstelle_SelectFeld('LVK_LAN_CODE', $rsLVK->FeldInhalt('LVK_LAN_CODE'), 0, $EditRecht, $SQL, '', $AWISBenutzer->BenutzerSprache(), '', '',
            '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_ORT'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_ORT', $rsLVK->FeldInhalt('LVK_ORT'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_ANSPRECHPARTNER'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_ANSPRECHPARTNER', $rsLVK->FeldInhalt('LVK_ANSPRECHPARTNER'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_TELEFON'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_TELEFON', $rsLVK->FeldInhalt('LVK_TELEFON'), 20, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_FAX'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_FAX', $rsLVK->FeldInhalt('LVK_FAX'), 20, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_EMAIL'] . ':', 150);
        $Link = 'mailto:' . $rsLVK->FeldInhalt('LVK_EMAIL');
        $Form->Erstelle_TextFeld('LVK_EMAIL', $rsLVK->FeldInhalt('LVK_EMAIL'), 80, 900, $EditRecht, '', '', $Link, 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_KENNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_KENNUNG', $rsLVK->FeldInhalt('LVK_KENNUNG'), 10, 100, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_VKH_ID'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_VKH_ID', $rsLVK->FeldInhalt('LVK_VKH_ID'), 10, 100, $EditRecht);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_DATUMVOM'] . ':', 150);
        $Form->Erstelle_TextFeld('LVK_DATUMVOM', $rsLVK->FeldInhalt('LVK_DATUMVOM'), 10, 150, $EditRecht, '', '', '', 'D', '', '', date('d.m.Y'));
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LVK']['LVK_DATUMBIS'] . ':', 120);
        $Form->Erstelle_TextFeld('LVK_DATUMBIS', $rsLVK->FeldInhalt('LVK_DATUMBIS'), 10, 150, $EditRecht, '', '', '', 'D', '', '', '31.12.2030');
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('PROBLEM', $ex->getMessage(), 'MELDEN', 3, 200809111043);
} catch (Exception $ex) {
    echo 'allg. Fehler:' . $ex->getMessage();
}
?>