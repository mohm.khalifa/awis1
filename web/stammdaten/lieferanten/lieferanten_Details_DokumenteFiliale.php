<?php

require_once('register.inc.php');
require_once('awisDokumente.php');
require_once('awis_forms.inc.php');

try {
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $AWISCursorPosition;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIE', '*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'DateiOeffnen');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('FEHLER', 'err_UngueltigesUploadDateiFormat');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht614 = $AWISBenutzer->HatDasRecht(614);
    if ($Recht614 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->Formular_Start();

    if (isset($_GET['DOC_KEY'])) {
        $AWIS_KEY2 = $_GET['DOC_KEY'];
    } elseif (isset($_POST['txtDOC_KEY'])) {
        $AWIS_KEY2 = $_POST['txtDOC_KEY'];
    } else {
        $AWIS_KEY2 = 0;
    }

    if (isset($_GET['DEL'])) {
        $Doc = new awisDokumente();
        $Doc->DokumentLoeschen($_GET['DEL']);
        $AWIS_KEY2 = 0;
    }

    if (isset($_POST['cmdSpeichern_x'])) {
        //*****************************************************
        // Dokumente speichern
        //*****************************************************
        if (isset($_POST['txtDOC_BEZEICHNUNG']) && $_POST['txtDOC_BEZEICHNUNG'] != '') {
            if ($AWIS_KEY2 == 0) {
                $Felder = awis_NameInArray($_POST, 'txtDOC_', 1, 1);
                if ($Felder != '') {
                    $Doc = new awisDokumente();
                    $Zuordnungen = array('LIF' => $_POST['txtLIE_NR']);    // Zuordnung LIE, aber um zwischen LIE und LIE f�r Filiale sichtbar zu trennen => LIF 
                    $AWIS_KEY2 = $Doc->DokumentSpeichern($AWIS_KEY2, 'DOC', $_POST['txtDOC_BEZEICHNUNG'], $_POST['txtDOC_DATUM'], $_POST['txtDOC_BEMERKUNG'],
                        $AWISBenutzer->BenutzerID(), $Zuordnungen);
                }
            } else {
                $SQL = 'UPDATE dokumente SET ';
                $SQL .= 'doc_bezeichnung = :var_T_doc_bezeichnung, ';
                $SQL .= 'doc_datum = :var_T_doc_datum, ';
                $SQL .= 'doc_bemerkung = :var_T_doc_bemerkung ';
                $SQL .= 'WHERE doc_key = :var_T_doc_key';

                $BindeVariablen = array();
                $BindeVariablen['var_T_doc_bezeichnung'] = $_POST['txtDOC_BEZEICHNUNG'];
                $BindeVariablen['var_T_doc_datum'] = $_POST['txtDOC_DATUM'];
                $BindeVariablen['var_T_doc_bemerkung'] = $_POST['txtDOC_BEMERKUNG'];
                $BindeVariablen['var_T_doc_key'] = intval($AWIS_KEY2);
                $DB->Ausfuehren($SQL, '', false, $BindeVariablen);
            }
        }
    }

    $SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente ';
    $SQL .= 'INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key ';
    $SQL .= 'INNER JOIN Benutzer ON doc_xbn_key = xbn_key ';
    $SQL .= 'WHERE doz_xxx_key = :var_N0_lie_nr ';
    $SQL .= 'AND doz_xxx_kuerzel = \'LIF\'';

    if (isset($_GET['DokumentSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['DokumentSort']);
    } else {
        $SQL .= ' ORDER BY doc_datum';
    }

    $BindeVariablen = array();
    $BindeVariablen['var_N0_lie_nr'] = intval($AWIS_KEY1);
    $rsDocs = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

    if (!isset($_GET['DOC_KEY']))        // Liste anzeigen					
    {
        $Spalten = array();
        $Spalten['IconsNew'] = array(
            'Bezeichnung' => '',
            'Breite' => 38,
            'HeaderLink' => './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale&DOC_KEY=0',
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_NeuesDokument']
        );
        $Spalten['Datum'] = array(
            'Bezeichnung' => $AWISSprachKonserven['LIE']['DOC_Datum'],
            'Breite' => 100,
            'HeaderLink' => './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale' . '&DokumentSort=doc_datum' . ((isset($_GET['DokumentSort']) AND ($_GET['DokumentSort'] == 'doc_datum'))?'~':''),
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_UeberschriftDatum']
        );
        $Spalten['Bezeichnung'] = array(
            'Bezeichnung' => $AWISSprachKonserven['LIE']['DOC_Bezeichnung'],
            'Breite' => 350,
            'HeaderLink' => './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale' . '&DokumentSort=doc_bezeichnung' . ((isset($_GET['DokumentSort']) AND ($_GET['DokumentSort'] == 'doc_bezeichnung'))?'~':''),
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_UeberschriftBezeichnung']
        );
        $Spalten['Erweiterung'] = array(
            'Bezeichnung' => $AWISSprachKonserven['LIE']['DOC_ErweiterungKurz'],
            'Breite' => 60,
            'HeaderLink' => './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale' . '&DokumentSort=doc_erweiterung' . ((isset($_GET['DokumentSort']) AND ($_GET['DokumentSort'] == 'doc_erweiterung'))?'~':''),
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_UeberschriftErweiterung']
        );
        $Spalten['Eigentuemer'] = array(
            'Bezeichnung' => $AWISSprachKonserven['LIE']['DOC_Eigentuemer'],
            'Breite' => 180,
            'HeaderLink' => './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale' . '&DokumentSort=xbn_name' . ((isset($_GET['DokumentSort']) AND ($_GET['DokumentSort'] == 'xbn_name'))?'~':''),
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_UeberschriftEigentuemer']
        );
        $Spalten['IconsShow'] = array(
            'Bezeichnung' => '',
            'Breite' => 20,
            'HeaderLink' => '',
            'HeaderToolTip' => $AWISSprachKonserven['LIE']['DOC_ttt_ZeigeDokument']
        );

        $IconsArray = array();
        if ((($Recht614 & 4) == 4)) {
            $IconsArray[] = array('new', './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale&DOC_KEY=0', '', $Spalten['IconsNew']['HeaderToolTip']);
        }
        $Form->ZeileStart();
        $Form->Erstelle_ListeIcons($IconsArray, $Spalten['IconsNew']['Breite'], -1, $Spalten['IconsNew']['HeaderToolTip']);
        $Form->Erstelle_Liste_Ueberschrift($Spalten['Datum']['Bezeichnung'], $Spalten['Datum']['Breite'], '', $Spalten['Datum']['HeaderLink'], $Spalten['Datum']['HeaderToolTip']);
        $Form->Erstelle_Liste_Ueberschrift($Spalten['Bezeichnung']['Bezeichnung'], $Spalten['Bezeichnung']['Breite'], '', $Spalten['Bezeichnung']['HeaderLink'],
            $Spalten['Bezeichnung']['HeaderToolTip']);
        $Form->Erstelle_Liste_Ueberschrift($Spalten['Erweiterung']['Bezeichnung'], $Spalten['Erweiterung']['Breite'], '', $Spalten['Erweiterung']['HeaderLink'],
            $Spalten['Erweiterung']['HeaderToolTip']);
        $Form->Erstelle_Liste_Ueberschrift($Spalten['Eigentuemer']['Bezeichnung'], $Spalten['Eigentuemer']['Breite'], '', $Spalten['Eigentuemer']['HeaderLink'],
            $Spalten['Eigentuemer']['HeaderToolTip']);
        $Form->Erstelle_Liste_Ueberschrift('', $Spalten['IconsShow']['Breite'], '', '', $Spalten['IconsShow']['HeaderToolTip']);
        $Form->ZeileEnde();

        $DS = 1;
        while (!$rsDocs->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (intval($Recht614 & 6) > 0)    // �ndernrecht
            {
                $Icons[] = array(
                    'edit',
                    './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale&DOC_KEY=0' . $rsDocs->FeldInhalt('DOC_KEY'),
                    '',
                    $AWISSprachKonserven['LIE']['DOC_ttt_BearbeiteDokument']
                );
                $Icons[] = array(
                    'delete',
                    './lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale&DEL=0' . $rsDocs->FeldInhalt('DOC_KEY'),
                    '',
                    $AWISSprachKonserven['LIE']['DOC_ttt_LoescheDokument']
                );
            }
            $Form->Erstelle_ListeIcons($Icons, $Spalten['IconsNew']['Breite'], ($DS % 2));
            $Form->Erstelle_ListenFeld('#DOC_DATUM', $rsDocs->FeldInhalt('DOC_DATUM'), 10, $Spalten['Datum']['Breite'], false, ($DS % 2), '', '', 'D');
            $Form->Erstelle_ListenFeld('#DOC_BEZEICHNUNG', $rsDocs->FeldInhalt('DOC_BEZEICHNUNG'), 40, $Spalten['Bezeichnung']['Breite'], false, ($DS % 2), '', '', 'T', 'L',
                $rsDocs->FeldInhalt('DOC_BEMERKUNG'));
            $Form->Erstelle_ListenFeld('#DOC_ERWEITERUNG', $rsDocs->FeldInhalt('DOC_ERWEITERUNG'), 5, $Spalten['Erweiterung']['Breite'], false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#DOC_EIGENTUEMER', $rsDocs->FeldInhalt('XBN_NAME'), 20, $Spalten['Eigentuemer']['Breite'], false, ($DS % 2), '', '', 'T');

            $Link = '/dokumentanzeigen.php?doctab=LIF&dockey=' . $rsDocs->FeldInhalt('DOC_KEY') . '';
            $Form->Erstelle_ListenBild('href', 'DOC', $Link, '/bilder/dateioeffnen_klein.png', $AWISSprachKonserven['Wort']['DateiOeffnen'], ($DS % 2), '', 17, 17, 20);

            $Form->ZeileEnde();

            $DS++;
            $rsDocs->DSWeiter();
        }
    } else {
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./lieferanten_Main.php?cmdAktion=Details&Seite=DokumenteFiliale&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsDocs->FeldInhalt('DOC_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsDocs->FeldInhalt('DOC_USERDAT'));
        $Form->InfoZeile($Felder);

        if ($AWIS_KEY2 <= 0) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Datum'] . ':', 250);
            $Form->Erstelle_TextFeld('DOC_DATUM', date('d.m.Y', time()), 10, 50, true);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Bezeichnung'] . ':', 250);
            $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG', '', 52, 350, true);
            $AWISCursorPosition = 'txtDOC_BEZEICHNUNG';
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_UploadDatei'] . ':', 250);
            $Form->Erstelle_DateiUpload('DOC', 300);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Bemerkung'] . ':', 250);
            $Form->Erstelle_Textarea('DOC_BEMERKUNG', '', 350, 52, 4, true);
            $Form->ZeileEnde();
        } else {
            $SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente';
            $SQL .= ' INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key';
            $SQL .= ' INNER JOIN Benutzer ON doc_xbn_key = xbn_key';
            $SQL .= ' WHERE doc_key = :var_N0_doc_key';

            $BindeVariablen = array();
            $BindeVariablen['var_N0_doc_key'] = intval($AWIS_KEY2);
            $rsDoc = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

            $Form->Erstelle_HiddenFeld('DOC_KEY', $rsDoc->FeldInhalt('DOC_KEY'));

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Datum'] . ':', 250);
            $Form->Erstelle_TextFeld('DOC_DATUM', $Form->Format('D', $rsDoc->FeldInhalt('DOC_DATUM')), 10, 50, true);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Bezeichnung'] . ':', 250);
            $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG', $rsDoc->FeldInhalt('DOC_BEZEICHNUNG'), 52, 350, true);
            $AWISCursorPosition = 'txtDOC_BEZEICHNUNG';
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_UploadDatei'] . ':', 250);
            $Link = '/dokumentanzeigen.php?doctab=LIF&dockey=' . $AWIS_KEY2 . '';
            $Form->Erstelle_ListenBild('href', 'DOC', $Link, '/bilder/dateioeffnen_klein.png', $AWISSprachKonserven['Wort']['DateiOeffnen'], 0, '', 17, 17, 40);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_ErweiterungLang'] . ':', 100);
            $Form->Erstelle_TextLabel($rsDoc->FeldInhalt('DOC_ERWEITERUNG'), 50);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['DOC_Bemerkung'] . ':', 250);
            $Form->Erstelle_Textarea('DOC_BEMERKUNG', $rsDoc->FeldInhalt('DOC_BEMERKUNG'), 350, 52, 4, true);
            $Form->ZeileEnde();
        }
    }

    $Form->Formular_Ende();

    $Form->SetzeCursor($AWISCursorPosition);

    unset($rsDocs);
    unset($rsDoc);
} catch (Exception $ex) {
}

?>