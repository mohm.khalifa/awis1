<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('MFI','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen
	$Recht3021= $AWISBenutzer->HatDasRecht(3021);		// Mitbewerber in Filialinfo
	if(($Recht5001&4)==0 OR ($Recht3021&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if($AWIS_KEY1=='')		// Kann sein, das das leer ist (SK 17.09.2009)
	{
		$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
		$AWIS_KEY1=$FIL_ID[0];
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILMBW'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');

	if(isset($_GET['Del']) OR isset($_POST['cmdLoeschenOK']))
	{
		include('./filialinfo_loeschen.php');
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY MFI_ENTFERNUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}

	$SQL = 'SELECT MBW_KEY, MBW_NAME1, MBW_STRASSE, MBW_HAUSNUMMER, MBW_PLZ, MBW_ORT, MFI_ENTFERNUNG';
	$SQL .= ', MBW_PRUEFUNGDURCH, MBW_PRUEFUNGAM, MFI_KEY';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Mitbewerber';
	$SQL .= ' INNER JOIN MitbewerberFilialen ON MBW_KEY = MFI_MBW_KEY AND MFI_FIL_ID ='.$DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);

	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_FILMBW',serialize($Param));
		}
		elseif(isset($Param['BLOCK']) AND $Param['FIL_ID']==$AWIS_KEY1)
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		if($Block==0)
		{
			$Block=1;
		}
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsMBW = $DB->RecordSetOeffnen($SQL);
	if($rsMBW->EOF() AND $Block>1)
	{
		$Block = 1;
		$StartZeile=1;
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		$SQL .= $ORDERBY;
		$rsMBW = $DB->RecordSetOeffnen($SQL);

	}




	$Param['FIL_ID']=$AWIS_KEY1;
	$AWISBenutzer->ParameterSchreiben('Formular_FILMBW',serialize($Param));

	if($rsMBW->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datensätzen!
	{
		$Param['BLOCK']=0;
		$AWISBenutzer->ParameterSchreiben('Formular_FILMBW',serialize($Param));
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	else
	{
		$DetailAnsicht = false;

		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['MBW_NAME1']=200;
			$Breiten['MBW_STRASSE']=200;
			$Breiten['MBW_PLZ']=60;
			$Breiten['MBW_ORT']=200;
			$Breiten['MFI_ENTFERNUNG']=60;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['MBW_NAME1']=250;
			$Breiten['MBW_STRASSE']=250;
			$Breiten['MBW_PLZ']=60;
			$Breiten['MBW_ORT']=250;
			$Breiten['MFI_ENTFERNUNG']=80;
		}
		else
		{
			$Breiten['MBW_NAME1']=200;
			$Breiten['MBW_STRASSE']=250;
			$Breiten['MBW_PLZ']=60;
			$Breiten['MBW_ORT']=200;
			$Breiten['MFI_ENTFERNUNG']=100;
		}

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Icons = array();
		if(($Recht3021&2)==2)
		{
			$Icons[] = array('new','/mitbewerb/mitbewerb_Main.php?cmdAktion=Details&MBW_KEY=-1','g','','_blank');
		}
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MBW_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MBW_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_NAME1'],$Breiten['MBW_NAME1'],'',$Link);
		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MBW_STRASSE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MBW_STRASSE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_STRASSE'],$Breiten['MBW_STRASSE'],'',$Link);
		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MBW_ORT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MBW_ORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_ORT'],$Breiten['MBW_ORT'],'',$Link);
		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MFI_ENTFERNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MFI_ENTFERNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MFI']['MFI_ENTFERNUNG'],90,'',$Link);
		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MBW_PRUEFUNGAM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MBW_PRUEFUNGAM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_PRUEFUNGAM'],120,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsMBW->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Icons = array();
			if(($Recht3021&4)==4)
			{
				$Icons[] = array('edit','../mitbewerb/mitbewerb_Main.php?cmdAktion=Details&MBW_KEY='.$rsMBW->FeldInhalt('MBW_KEY'),'',$AWISSprachKonserven['MFI']['MitbewerberBearbeiten'],'_blank');
			}
			if(($Recht3021&8)==8)
			{
				$Icons[] = array('delete','./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Mitbewerb&Del='.$rsMBW->FeldInhalt('MFI_KEY'),'',$AWISSprachKonserven['MFI']['MitbewerberzuordnungLoeschen']);
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Text = (strlen($rsMBW->FeldInhalt('MBW_NAME1'))>(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_NAME1'),0,(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_NAME1'));
			$Form->Erstelle_ListenFeld('MBW_NAME1',$Text,0,$Breiten['MBW_NAME1'],false,($DS%2),'','','T','L',($Text!=$rsMBW->FeldInhalt('MBW_NAME1')?$rsMBW->FeldInhalt('MBW_NAME1'):''));
			$Text = (strlen($rsMBW->FeldInhalt('MBW_STRASSE').' '.$rsMBW->FeldInhalt('MBW_HAUSNUMMER'))>(int)($Breiten['MBW_STRASSE']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_STRASSE').' '.$rsMBW->FeldInhalt('MBW_HAUSNUMMER'),0,(int)($Breiten['MBW_STRASSE']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_STRASSE').' '.$rsMBW->FeldInhalt('MBW_HAUSNUMMER'));
			$Form->Erstelle_ListenFeld('MBW_STRASSE',$Text,0,$Breiten['MBW_STRASSE'],false,($DS%2),'','','T','L',$rsMBW->FeldInhalt('MBW_STRASSE').' '.$rsMBW->FeldInhalt('MBW_HAUSNUMMER'));
			$Text = (strlen($rsMBW->FeldInhalt('MBW_ORT'))>(int)($Breiten['MBW_ORT']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_ORT'),0,(int)($Breiten['MBW_ORT']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_ORT'));
			$Form->Erstelle_ListenFeld('MBW_ORT',$Text,0,$Breiten['MBW_ORT'],false,($DS%2),'','','T','L',$rsMBW->FeldInhalt('MBW_PLZ').' '.$rsMBW->FeldInhalt('MBW_ORT'));

			$Form->Erstelle_ListenFeld('MFI_ENTFERNUNG',$rsMBW->FeldInhalt('MFI_ENTFERNUNG'),0,90,false,($DS%2),'','','N3','R','');
			$Form->Erstelle_ListenFeld('MBW_PRUEFUNGAM',$rsMBW->FeldInhalt('MBW_PRUEFUNGAM'),0,100,false,($DS%2),'','','D','Z',$rsMBW->FeldInhalt('MBW_PRUEFUNGDURCH'));

			$Parameter = array('$NAME1'=>urlencode($rsMBW->FeldInhalt('MBW_NAME1')),
						   '$NAME2'=>urlencode($rsMBW->FeldInhalt('MBW_NAME2')),
						   '$STRASSE'=>urlencode($rsMBW->FeldInhalt('MBW_STRASSE')),
						   '$HAUSNUMMER'=>urlencode($rsMBW->FeldInhalt('MBW_HAUSNUMMER')),
						   '$PLZ'=>urlencode($rsMBW->FeldInhalt('MBW_PLZ')),
						   '$ORT'=>urlencode($rsMBW->FeldInhalt('MBW_ORT')),
						   '$LAN_CODE'=>urlencode($rsMBW->FeldInhalt('MBW_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Karte,$Parameter);
			$Form->Erstelle_ListenBild('href','#MAPS',$Link,'/bilder/icon_globus.png','',($DS%2),'');

			$Form->ZeileEnde();

			$rsMBW->DSWeiter();
			$DS++;
		}

		$Link = './filialinfo_Main.php?cmdAktion=Filialinfos'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		@file_put_contents('/tmp/filialinfo_Infos_mitbewerb.err',serialize($ex),FILE_APPEND);
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>