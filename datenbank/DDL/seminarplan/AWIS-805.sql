ALTER TABLE SEMINARHOTEL ADD SEH_HOMEPAGE varchar2(4000);

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('SEH_HOMEPAGE', 'SEH', 'Textlabel f�r die Homepage', 'Homepage:', TO_DATE('2019-03-15 08:07:26', 'YYYY-MM-DD HH24:MI:SS'));

UPDATE "AWIS"."TEXTKONSERVEN" SET XTX_TEXT_DE =
'Folgende Platzhalter werden ersetzt: <br>
<br><br>
<b>#Vorname Nachname#</b> Vorname und Nachname des Teinehmers<br>
<b>#Seminarnummer#</b> Nummer es Seminars (z.B. 2018-01-15)<br>
<b>#Terminnummer#</b> Seminarnummer+Seminarzusatz (z.B. 2018-01-15-0001)<br>
<b>#Seminartitel#</b> Titel des Seminars<br>
<b>#Personalnummer#</b> Personalnummer des Teilnehmers<br>
<b>#Filialnummer#</b> Filialnummer des Teilnehmers<br>
<b>#Datumvon#</b> Beginn des Seminars<br>
<b>#Datumbis#</b> Ende des Seminars<br>
<b>#KW#</b> Kalenderwoche des Seminars<br>
<b>#Schulungsort#</b> Ort (z.B. A.T.U Academy) des Seminars<br>
<b>#Region#</b> Region des Teilnehmers<br>
<b>#SET_KEY#</b> Key zum Datensatz (F�r Links)<br>
<b>#Strasse#</b> Stra�e des Seminars<br>
<b>#Ort#</b> Ort des Seminars<br>
<b>#PLZ#</b> Postleitzahl des Seminars<br>
<b>#TauschText#</b> Eingetragener Freitext des Teilnehmers beim Teilnehmertausch<br>
<b>#HOTELHomepage#</b> Homepage des Hotels<br>
<b>#HOTELDatumvon#</b> Anreise des Teilnehmers an Hotel<br>
<b>#HOTELDatumbis#</b> Abreise des Teilnehmers von Hotel<br>
<b>#HOTELDatumvonALT#</b> Alte Anreise des Teilnehmers an Hotel (bei �nderungen)<br>
<b>#HOTELDatumbisALT#</b> Alte Abreise des Teilnehmers von Hotel (bei �nderungen)<br>
<b>#KWHotel#</b> Kalenderwoche der Hotelanreise<br>
<b>#KWHotelALT#</b> Alte Kalenderwoche der Hotelanreise (bei �nderungen)<br>
<b>#LINK#</b> Link zur AWIS-Umgebung (Entwick, Staging, Prod) <br>
<b>#LINKAFB#</b> Link zur Anfahrtsbeschreibung <br>
<b>#LINKLZB#</b> Link zum Lernzielblatt <br>
<b>#LINKTH#</b> Link zum Hotel des Teilnehmers<br>
<br>

Leerzeilen m�ssen �ber das HTML-Tag "&lt;br&gt;" eingef�gt werden. '

COMMIT;