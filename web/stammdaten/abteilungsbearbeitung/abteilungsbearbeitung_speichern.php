<?php

global $Funktionen;

if ($Funktionen->AWIS_KEY1 == "-1") {
    //Erstellung

    $SQL = "INSERT INTO KONTAKTEABTEILUNGEN "; //Abteilung erstellen
    $SQL .= "(";
    $SQL .= "KAB_ABTEILUNG";
    $SQL .= ",KAB_KZG_ID";
    $SQL .= ",KAB_USER";
    $SQL .= ",KAB_USERDAT";
    $SQL .= ",KAB_BEMERKUNG";
    $SQL .= ")";
    $SQL .= " VALUES ";
    $SQL .= "(";
    $SQL .= $Funktionen->DB->WertSetzen("KAB", "T", $_POST['txtKAB_ABTEILUNG']);
    $SQL .= ", 1";
    $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "T", $Funktionen->AWISBenutzer->BenutzerName());
    $SQL .= ", sysdate";
    $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "T", $_POST['txtKAB_BEMERKUNG']);
    $SQL .= ")";
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));

    $SQL = "SELECT KAB_KEY FROM Kontakteabteilungen WHERE KAB_ABTEILUNG = " . $Funktionen->DB->WertSetzen("KAB", "T", $_POST['txtKAB_ABTEILUNG']);
    $rsKAB = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen("KAB"));

    $Funktionen->AWIS_KEY1 = $rsKAB->FeldInhalt("KAB_KEY");


} elseif ($Funktionen->AWIS_KEY1 <> "0") { //Änderungen
    //Alle Updates zu Kontakteabteilungen

    $UpdateSQL = "";

    if (isset($_POST['txtKAB_ABTEILUNG']) and $_POST['txtKAB_ABTEILUNG'] <> $_POST['oldKAB_ABTEILUNG']) {
        $UpdateSQL .= ", KAB_ABTEILUNG = " . $Funktionen->DB->WertSetzen("KAB", "T", $_POST['txtKAB_ABTEILUNG']);
    }
    if (isset($_POST['txtKAB_BEMERKUNG']) and $_POST['txtKAB_BEMERKUNG'] <> $_POST['oldKAB_BEMERKUNG']) {
        $UpdateSQL .= ", KAB_BEMERKUNG = " . $Funktionen->DB->WertSetzen("KAB", "T", $_POST['txtKAB_BEMERKUNG']);
    }
    if (strlen($UpdateSQL) > 0) {
        $UpdateSQL = substr($UpdateSQL, 1);
        $SQL = "UPDATE KONTAKTEABTEILUNGEN SET ";
        $SQL .= $UpdateSQL;
        $SQL .= ", KAB_USER = ". $Funktionen->DB->WertSetzen("KAB", "T", $Funktionen->AWISBenutzer->BenutzerName());
        $SQL .= ", KAB_USERDAT = sysdate ";
        $SQL .= " where KAB_KEY = " . $Funktionen->DB->WertSetzen("KAB", "T", $Funktionen->AWIS_KEY1);
        $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));

    }
}
//Anprechpartner und Mitglieder zu einer Abteilung hinzufügen ist bei Änderung/Erstellen gleich

if (isset($_POST['txtKAB_ABTEILUNGSBETREUERIT'])) { //Ansprechpartner hinzufügen
    foreach ($_POST['txtKAB_ABTEILUNGSBETREUERIT'] as $KON_KEY) {
        $SQL = "INSERT INTO KONTAKTEINFOS ";
        $SQL .= "(";
        $SQL .= "KIN_KON_KEY";
        $SQL .= ",KIN_ITY_KEY";
        $SQL .= ",KIN_WERT";
        $SQL .= ",KIN_IMQ_ID";
        $SQL .= ",KIN_USER";
        $SQL .= ",KIN_USERDAT";
        $SQL .= ")";
        $SQL .= " VALUES ";
        $SQL .= "(";
        $SQL .= $Funktionen->DB->WertSetzen("KAB", "N0", $KON_KEY);
        $SQL .= ", 322";
        $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "N0", $Funktionen->AWIS_KEY1);
        $SQL .= ",4";
        $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "T", $Funktionen->AWISBenutzer->BenutzerName());
        $SQL .= ", sysdate";
        $SQL .= ")";
        $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));
    }
}

if (isset($_POST['txtKAB_ABTEILUNGSMITGLIED'])) { //Personen zu Abteilung hinzufügen
    foreach ($_POST['txtKAB_ABTEILUNGSMITGLIED'] as $KON_KEY) {
        $SQL = "INSERT INTO KONTAKTEABTEILUNGENZUORDNUNGEN ";
        $SQL .= "(";
        $SQL .= "KZA_KON_KEY";
        $SQL .= ",KZA_KAB_KEY";
        $SQL .= ",KZA_USER";
        $SQL .= ",KZA_USERDAT";
        $SQL .= ")";
        $SQL .= " VALUES ";
        $SQL .= "(";
        $SQL .= $Funktionen->DB->WertSetzen("KAB", "N0", $KON_KEY);
        $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "N0", $Funktionen->AWIS_KEY1);
        $SQL .= "," . $Funktionen->DB->WertSetzen("KAB", "T", $Funktionen->AWISBenutzer->BenutzerName());
        $SQL .= ", sysdate";
        $SQL .= ")";
        $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));
    }
}

