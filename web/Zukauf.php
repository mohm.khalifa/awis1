<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try 
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	$AWISBenutzer = awisBenutzer::Init();

}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZZZ','%');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);
?>

<?php

try 
{
	$Form = new awisFormular();
    $Form->Formular_Start();
    
    $Form->ZeileStart();
    $WartungsBildHTML = '<img src="/bilder/Wartung.bmp" alt="wartung" height="520" width="1200">';
    $Form->SchreibeHTMLCode($WartungsBildHTML);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    
    $Text = $AWISSprachKonserven['ZZZ']['ZZZ_WARTUNG_ZUKAUF'];

    if(file_exists('/daten/web/dokumente/Zukauf.pdf')){
        $Text = str_replace('#LINK#','<a href="/dokumente/Zukauf.pdf">Link zur Organisationsanweisung</a>',$Text);
    }else{
        $Text = str_replace('#LINK#','',$Text);
    }


    $Form->Erstelle_TextLabel($Text, '1280', 'text-align:center');
    $Form->ZeileEnde();
    
    $Form->Formular_Ende();
	
}
catch (Exception $ex)
{
	echo  $ex->getMessage();
}
?>
