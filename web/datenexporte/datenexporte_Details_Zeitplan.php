<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XDZ','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','XDZ_%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Liste','WochenTageNamen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht4001 = $AWISBenutzer->HatDasRecht(4001);
	if($Recht4001==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_DatenexporteZeitplan'));
	$Schritt = 0;		// Fuer Neuanlagen
	
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_GET['XDZ_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['XDZ_KEY']);
	}
	elseif(isset($_POST['cmdAssistent_x']))
	{
	    $AWIS_KEY2 = -1;
	}
	elseif($AWIS_KEY2<=0) 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['XDZ_KEY']))
		{
			$Param['XDZ_KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='XDZ_KEY DESC';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY2=$Param['KEY'];
	}
	elseif(isset($_GET['Liste']))
	{
	    $AWIS_KEY2=$Param['KEY']=0;
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = ' ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY XDZ_KEY DESC';
			$Param['ORDER']='XDZ_KEY DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************

	$Bedingung = '';

	if(isset($_GET['XDZ_KEY']) OR isset($_POST['cmdAssistent_x']))
	{
		$Bedingung .= ' AND XDZ_KEY = '.$DB->WertSetzen('XDZ','N0',$AWIS_KEY2);
	}

	$Bedingung .= ' AND XDZ_XAB_KEY = '.$DB->WertSetzen('XDZ','N0',$AWIS_KEY1);

	if($AWIS_KEY2>=1)
	{
	   $Bedingung .= ' AND XDZ_KEY = '.$DB->WertSetzen('XDZ','N0',$AWIS_KEY2);
	}
	
	
	$SQL = 'SELECT EXPORTABFRAGENZEITPUNKTE.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ", func_textkonserve('XDZ_STANDARDINTERVALL','Liste','".$AWISBenutzer->BenutzerSprache()."',XDZ_STANDARDINTERVALL) AS STANDARDINTERVALL";
	$SQL .= ' FROM EXPORTABFRAGENZEITPUNKTE';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
//	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->BindeVariablen('XDZ',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;


	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsXDZ = $DB->RecordsetOeffnen($SQL,$DB->BindeVariablen('XDZ'));
	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmDatenExporteZeitplaene action=./datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan method=POST enctype="multipart/form-data">';

	if(($rsXDZ->AnzahlDatensaetze()>1 AND !isset($_GET['XDZ_KEY']) AND !isset($_POST['txtXDZ_KEY'])) OR isset($_GET['Liste']))						// Liste anzeigen
	{
		$DetailAnsicht = false;

        $Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Icons=array();
		if((intval($Recht4001)&6)!=0)
		{
			$Icons[] = array('new','./datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan&XDZ_KEY=-1');
		}
		
		$Form->Erstelle_ListeIcons($Icons,38,-1);
		
		
		$Link = './datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XDZ_DATEINAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XDZ_DATEINAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XDZ']['XDZ_DATEINAME'],290,'',$Link);

		$Link = './datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XDZ_STANDARDINTERVALL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XDZ_STANDARDINTERVALL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XDZ']['XDZ_STANDARDINTERVALL'],200,'',$Link);

		$Link = './datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XDZ_INTERVALL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XDZ_INTERVALL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XDZ']['XDZ_INTERVALL'],130,'',$Link);

		$Link = './datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XDZ_NAECHSTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XDZ_NAECHSTERSTART'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XDZ']['XDZ_NAECHSTERSTART'],150,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsXDZ->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Icons = array();
			if(intval($Recht4001&2)>0)	// �ndernrecht
			{
				$Icons[] = array('edit','./datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan&XDZ_KEY='.$rsXDZ->FeldInhalt('XDZ_KEY'));
				$Icons[] = array('delete','./datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan&Del='.$rsXDZ->FeldInhalt('XDZ_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			
				
			$Form->Erstelle_ListenFeld('XDZ_DATEINAME',$rsXDZ->FeldInhalt('XDZ_DATEINAME'),0,290,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('XDZ_STANDARDINTERVALL',$rsXDZ->FeldInhalt('STANDARDINTERVALL'),0,200,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('XDZ_INTERVALL',$rsXDZ->FeldInhalt('XDZ_INTERVALL'),0,130,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('XDZ_NAECHSTERSTART',$rsXDZ->FeldInhalt('XDZ_NAECHSTERSTART'),0,150,false,($DS%2),'','','T','L');

			$Form->ZeileEnde();

			$rsXDZ->DSWeiter();
			$DS++;
		}

		$Link = './datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');


	}
	else
	{
		$DetailAnsicht = true;
		$AWIS_KEY2 = $rsXDZ->FeldInhalt('XDZ_KEY');

		$Param['KEY']=$AWIS_KEY2;
        $EditModus = ($Recht4001&6);

		$Form->Erstelle_HiddenFeld('XDZ_KEY', $AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('XDZ_XAB_KEY', $AWIS_KEY1);
		$Form->Erstelle_HiddenFeld('XDZ_XBN_KEY', $AWISBenutzer->BenutzerId());

		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./datenexporte_Main.php?cmdAktion=Details&Seite=Zeitplan&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXDZ->FeldInhalt('XDZ_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXDZ->FeldInhalt('XDZ_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
        $EditRecht=(($Recht4001&2)!=0);

		//***************************************************************************
		// Wenn noch nichts gespeichert wurde => Assistenten anzeigen
		//***************************************************************************
		if($rsXDZ->FeldInhalt('XDZ_KEY')=='' OR (isset($_POST['txtXDZ_KEY']) AND $_POST['txtXDZ_KEY']=='') AND $AWIS_KEY2<=0)
		{
			$Schritt = 1;

			if(isset($_POST['txtSchritt']))
			{
				if(isset($_POST['cmdAssistentZurueck_x']))
				{
					$Schritt = $_POST['txtSchritt']-1;			// Einen Schritt zur�ck
				}
				else 
				{
					$Schritt = $_POST['txtSchritt']+1;
				}
			}
			
			if($Schritt<1)
			{
				$Schritt=1;
			}
			
			$Form->Erstelle_HiddenFeld('Schritt', $Schritt);
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Neuer Zeitplan, Schritt '.$Schritt, 1000,'Ueberschrift');
			$Form->ZeileEnde();

			$DetailAnsicht = false;
			
			// 0~Einmalig|1~T�glich|2~W�chentlich|3~Monatserster|4~Monatsletzter|5~1.Montag im Monat|6~Letzter Freitag im Monat
			
			switch($Schritt)
			{
				case 1:				// Schritt 1
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_PRIORITAET'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_PRIORITAET']);
					$Form->Erstelle_SelectFeld('!XXX_PRIORITAET',(isset($_POST['txtXXX_PRIORITAET'])?$_POST['txtXXX_PRIORITAET']:''),"300:290",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_STANDARDINTERVALL'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_STANDARDINTERVALL']);
					$Form->Erstelle_SelectFeld('!XXX_STANDARDINTERVALL',(isset($_POST['txtXXX_STANDARDINTERVALL'])?$_POST['txtXXX_STANDARDINTERVALL']:''),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
					$Form->ZeileEnde();
						
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_DATEINAME'].':',190);
					$Form->Erstelle_TextFeld('!XXX_DATEINAME',(isset($_POST['txtXXX_DATEINAME'])?$_POST['txtXXX_DATEINAME']:''),50,500,$EditRecht,'','','','T','',$AWISSprachKonserven['XDZ']['ttt_XDZ_DATEINAME'],'$NAM_$JAH_$MON_$TAG_$STD_$MIN.csv');
					$Form->ZeileEnde();

					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_BENACHRICHTIGUNG'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_BENACHRICHTIGUNG']);
					$Form->Erstelle_SelectFeld('!XXX_BENACHRICHTIGUNG',(isset($_POST['txtXXX_BENACHRICHTIGUNG'])?$_POST['txtXXX_BENACHRICHTIGUNG']:''),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
					$Form->ZeileEnde();
						
					break;	

				case 2:			// Schritt 2 => Details zum Zeitplan
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_STANDARDINTERVALL'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_STANDARDINTERVALL']);
					$Form->Erstelle_SelectFeld('!XXX_STANDARDINTERVALL',$_POST['txtXXX_STANDARDINTERVALL'],"300:290",false,'','','','','',$ListenDaten);
					$Form->ZeileEnde();

					//0~Einmalig|1~T�glich|2~W�chentlich|3~Monatlich|4~Monatserster
					//|5~Monatsletzter|6~1.Montag im Monat|7~Letzter Freitag im Monat
					switch ($_POST['txtXXX_STANDARDINTERVALL'])
					{
						case 0:		// Einmalig
							$Form->ZeileStart();
							$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_NAECHSTERSTART'].':',190);
							$Form->Erstelle_TextFeld('!XXX_NAECHSTERSTART',date('d.m.Y'),14,200,$EditRecht,'','','','DU');
							$Form->Erstelle_HiddenFeld('XXX_INTERVALL',0);
							$Form->ZeileEnde();
							break;
						case 1:		// T�glich
						case 2:		// W�chentlich
							$Form->ZeileStart();
							$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_SERVICETAGE'].':',190);
							
							$TageDerKW = array_flip(explode(';','6'));
							$TageNamen = explode('|',$AWISSprachKonserven['Liste']['WochenTageNamen']);
							
																			
							for($i=0;$i<7;$i++)
							{
								$Form->Erstelle_Checkbox('XXX_Tage_'.$i, (isset($TageDerKW[$i])?'on':''), 20, $EditModus, 'on', '', '', '');
								$Form->Erstelle_TextLabel($TageNamen[$i],150);
								
								if($i==3)
								{
									$Form->ZeileEnde();
									$Form->ZeileStart();
									$Form->Erstelle_TextLabel('&nbsp;',190);
								}
							}
							$Form->ZeileEnde();
							
							if($_POST['txtXXX_STANDARDINTERVALL']==2)
							{
							    $Form->Erstelle_HiddenFeld('XXX_INTERVALL', 1440);
							}
							else 
							{
    							$Form->ZeileStart();
    							$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_INTERVALL'].':',190);
    							$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_INTERVALL']);
    							$Form->Erstelle_SelectFeld('!XXX_INTERVALL','',"300:290",$EditModus,'',$OptionBitteWaehlen,'1','','',$ListenDaten);
    							$Form->ZeileEnde();
							}
            							
							$Form->ZeileStart();
							$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_ZEITVON'].':',190);
							$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_ZEIT']);
							$Form->Erstelle_SelectFeld('!XXX_ZEITVON','',"100:90",$EditModus,'',$OptionBitteWaehlen,'2:00','','',$ListenDaten);
							
							$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_ZEITBIS'].':',190);
							$Form->Erstelle_SelectFeld('!XXX_ZEITBIS','',"100:90",$EditModus,'',$OptionBitteWaehlen,'22:00','','',$ListenDaten);
							$Form->ZeileEnde();
							
							// N�chster Start ist heute
							$Form->Erstelle_HiddenFeld('XXX_NAECHSTERSTART', date('d.m.Y'));
							
							break;
						case 3:	// Monatlich => TODO: noch umsetzen 
						case 4:	// Monatserster
						case 5:	// Monatsletzter
						case 6: // 1.Montag im Monat
						case 7: // Letzter Freitag im Monat
							break;
					}
					break;
				case 3:			// Zusammenfassung und Speichern
					
					// Alle Felder nocheinal sichern					
					$XXXFelder = $Form->NameInArray($_POST, 'txtXXX_',2);
					foreach($XXXFelder AS $Feld)
					{
						if($Feld!='')
						{
							$Form->Erstelle_HiddenFeld(substr(str_replace('XXX', 'XDZ',$Feld),3), $_POST[$Feld]);
						}
					}
					$Form->Erstelle_HiddenFeld('XDZ_KEY', '');


					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_PRIORITAET'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_PRIORITAET']);
					$Form->Erstelle_SelectFeld('*XXX_PRIORITAET',(isset($_POST['txtXXX_PRIORITAET'])?$_POST['txtXXX_PRIORITAET']:''),"300:290",false,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
					$Form->ZeileEnde();
											
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_INTERVALL'].':',190);
					$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_INTERVALL']);
					$Form->Erstelle_SelectFeld('*XXX_INTERVALL',$_POST['txtXXX_INTERVALL'],"300:290",false,'',$OptionBitteWaehlen,'1','','',$ListenDaten);
					$Form->ZeileEnde();
						
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_NAECHSTERSTART'].':',190);
					$Form->Erstelle_TextFeld('*XXX_NAECHSTERSTART',$_POST['txtXXX_NAECHSTERSTART'],14,200,false,'','','','DU');
					$Form->ZeileEnde();
						
					$DetailAnsicht=true;
			}
			
			if($Schritt<3)
			{
				$XXXFelder = $Form->NameInArray($_POST, 'txtXXX_',2);
				foreach($XXXFelder AS $Feld)
				{
					if($Feld!='')
					{
						$Form->Erstelle_HiddenFeld(substr($Feld,3), $_POST[$Feld]);
					}
				}
			}
		}
		else 
		{
			$Form->Erstelle_HiddenFeld('XDZ_KEY', $rsXDZ->FeldInhalt('XDZ_KEY'));
			$Form->Erstelle_HiddenFeld('XDZ_XAB_KEY', $rsXDZ->FeldInhalt('XDZ_XAB_KEY'));
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_PRIORITAET'].':',190);
			$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_PRIORITAET']);
			$Form->Erstelle_SelectFeld('!XDZ_PRIORITAET',$rsXDZ->FeldInhalt('XDZ_PRIORITAET'),"300:290",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_NAECHSTERSTART'].':',190);
			$Form->Erstelle_TextFeld('!XDZ_NAECHSTERSTART',$rsXDZ->FeldInhalt('XDZ_NAECHSTERSTART'),14,200,$EditRecht,'','','','DU');
			$AWISCursorPosition='txtXDZ_NAECHSTERSTART';
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_DATEINAME'].':',190);
			$Form->Erstelle_TextFeld('!XDZ_DATEINAME',$rsXDZ->FeldInhalt('XDZ_DATEINAME'),50,500,$EditRecht,'','','','T');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_STANDARDINTERVALL'].':',190);
			$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_STANDARDINTERVALL']);
			$Form->Erstelle_SelectFeld('!XDZ_STANDARDINTERVALL',$rsXDZ->FeldInhalt('XDZ_STANDARDINTERVALL'),"300:290",false,'',$OptionBitteWaehlen,'','','',$ListenDaten);
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_BENACHRICHTIGUNG'].':',190);
			$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_BENACHRICHTIGUNG']);
			$Form->Erstelle_SelectFeld('!XDZ_BENACHRICHTIGUNG',$rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG'),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
			$Form->ZeileEnde();

			
			if($rsXDZ->FeldInhalt('XDZ_STANDARDINTERVALL')>0)        // Nicht bei den einmaligen
			{
    			$Form->ZeileStart();
    			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_SERVICETAGE'].':',190);
    			
    			$TageDerKW = array_flip(explode(';',$rsXDZ->FeldInhalt('XDZ_SERVICETAGE')));
    			$TageNamen = explode('|',$AWISSprachKonserven['Liste']['WochenTageNamen']);
    			
    			for($i=0;$i<7;$i++)
    			{
    				$Form->Erstelle_Checkbox('Tage_'.$i, (isset($TageDerKW[$i])?'on':''), 50, $EditModus, 'on', '', '', '');
    				$Form->Erstelle_TextLabel($TageNamen[$i],190);
    				
    				if($i==3)
    				{
    					$Form->ZeileEnde();
    					$Form->ZeileStart();
    					$Form->Erstelle_TextLabel('&nbsp;',190);
    				}
    			} 
    			$Form->ZeileEnde();
    	
    			if($rsXDZ->FeldInhalt('XDZ_STANDARDINTERVALL')!=2)
    			{
        			$Form->ZeileStart();
        			$Form->Erstelle_TextLabel($AWISSprachKonserven['XDZ']['XDZ_INTERVALL'].':',190);
        			$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['XDZ_INTERVALL']);
        			$Form->Erstelle_SelectFeld('!XDZ_INTERVALL',$rsXDZ->FeldInhalt('XDZ_INTERVALL'),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
        			$Form->ZeileEnde();
    			}
			}			
		}

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->Formular_Ende();
    $Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht4001&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht4001&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if($Schritt > 0 AND $Schritt<3) // AND ($Recht4001&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdAssistent', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	}
	
	if(($Recht4001&8)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}
		
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
	$AWISBenutzer->ParameterSchreiben('Formular_DatenexporteZeitplan',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>