<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FEB','FEB_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Dateiname');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3804 = awisBenutzerRecht($con,3804,$AWISBenutzer->BenutzerName());
if($Recht3804==0)
{
    awisEreignis(3,1000,'FEB',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


if(!isset($_POST['cmd_Import_x']))
{
	echo '<form name=frmImport method=post action=./filialebenen_Main.php?cmdAktion=Import enctype="multipart/form-data">';

	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'].':',150);
	awis_FORM_Erstelle_DateiUpload('Importdatei',200,30,20000000);
	awis_FORM_ZeileEnde();


	awis_FORM_FormularEnde();
	awis_FORM_SchaltflaechenStart();
	awis_FORM_Schaltflaeche('image','cmd_Import','','/bilder/eingabe_ok.png',$AWISSprachKonserven['Wort']['lbl_weiter'],'W');
	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}
else
{
	if(isset($_FILES['Importdatei']))
	{
		if(($fd = fopen($_FILES['Importdatei']['tmp_name'],'r'))!==false)
		{
			$Zeile = fgets($fd);
			$DateiInfo = explode("\t",$Zeile);
			if($DateiInfo[0]==='AWIS')
			{
				$Importfunktion = "import_".$DateiInfo[3];

				$Erg = $Importfunktion($DateiInfo[2], $fd, $DateiInfo[4]);

				echo "Statusreport AWIS<br>";
				echo "Datum\t".awis_FeldInhaltFormat('DU',date('d.m.Y H:i'))."<br>";
				if($Erg['Code']==0)
				{
					echo "Status\tErfolgreich importiert.<br>";
				}
				else
				{
					echo "Fehler\t".$Erg['Code']."<br>";
					echo "Meldung\t".$Erg['Letzte Meldung']."<br>";
				}
				echo "<br>";
				echo 'Meldungen im Detail:<br>';
				if(is_array($Erg))
				{
					foreach ($Erg['Meldungen'] AS $Meldung)
					{
						echo $Meldung."<br>";
					}
				}
			}
			else
			{
				echo 'Ung�ltige Import Datei.<br>';
			}

		}
	}
}

/**
 * Import der Filialrollenzuordnungen
 *
 * @param string $Version
 * @param ressource $fd
 */
function import_FRZ($Version, $fd, $BereichsID)
{
	global $AWISBenutzer;
	global $awisRSZeilen;
	global $con;
awis_Debug(1,'Import FRZ');
	if($Version != 1.0)
	{
		$Erg = array('Code'=>2, 'Letzte Meldung'=>'Falsche Dateiversion!', 'Meldungen'=>array());
		return $Erg;
	}

	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array());

	$SQL = 'SELECT DISTINCT Filialebenenrollen.*';
	$SQL .= ' FROM Filialebenenrollenbereiche';
	$SQL .= ' LEFT OUTER JOIN Filialebenenrollen ON FER_FRB_KEY = FRB_KEY';
	$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';
	awis_debug(1,$SQL);
	$rsFER = awisOpenRecordset($con, $SQL);
	if($awisRSZeilen==0)
	{
awis_Debug(1, $SQL);
		$Erg = array('Code'=>2, 'Letzte Meldung'=>'Keine ausreichenden Berechtigungen f�r diesen Bereich!', 'Meldungen'=>array());
		return $Erg;
	}

	$Erg['Meldungen'][] = 'Datei FRZ wird importiert';

	$Zeile = trim(fgets($fd));
	$Ueberschriften = explode("\t",$Zeile);
	if($Ueberschriften[0]!=='Rolle' AND $Ueberschriften[1]!=='Rolle')
	{
		return(array('Code'=>1,'Letzte Meldung'=>'Falsche Ueberschrift','Zeile'=>0,'Meldungen'=>array()));
	}
	$Ueberschriften = array_flip($Ueberschriften);
	
	$Zeile = trim(fgets($fd));
	$Rollen = array();
	$DS = 1;
	while(!feof($fd))
	{
		$Daten = explode("\t",$Zeile);
		$AWIS_KEY1 = 0;
		switch(strtoupper((trim($Daten[$Ueberschriften['Aktion']]))))
		{
			case 'I':
				if($Daten[$Ueberschriften['Rolle']]!='')		// Rolle muss angegeben sein
				{
					if(array_key_exists(strtoupper($Daten[$Ueberschriften['Rolle']]),$Rollen)===false)		// Rolle im Cache?
					{
						$SQL = 'SELECT FER_KEY, FER_BEZEICHNUNG ';
						$SQL .= ' FROM FilialebenenRollen';
						$SQL .= ' WHERE UPPER(fer_bezeichnung) = '.awis_FeldInhaltFormat('TU',$Daten[$Ueberschriften['Rolle']]);
						$SQL .= ' AND FER_FRB_KEY = 0'.$BereichsID;

						$rsFER = awisOpenRecordset($con, $SQL);
						if($awisRSZeilen==0)		// Neue Rolle
						{
							$Erg['Meldungen'][]='Neue Rolle '.$Daten[$Ueberschriften['Rolle']].' anlegen.';

							$SQL = 'INSERT INTO FilialEbenenRollen';
							$SQL .= '(FER_BEZEICHNUNG,FER_BEMERKUNG,FER_FRB_KEY,FER_GUELTIGAB,FER_GUELTIGBIS,FER_USER,FER_USERDAT)';
							$SQL .= ' VALUES (';
							$SQL .= ' '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Rolle']]);
							$SQL .= ' , \'Import\'';
							$SQL .= ' , '.awis_FeldInhaltFormat('N0',$BereichsID);
							$SQL .= ' , '.awis_FeldInhaltFormat('D',date('d.m.Y'));
							$SQL .= ' , '.awis_FeldInhaltFormat('D','30.12.2030');
							$SQL .= ' , \''.$AWISBenutzer->BenutzerName().'\'';
							$SQL .= ' , SYSDATE';
							$SQL .= ' )';

							if(awisExecute($con, $SQL)===false)
							{
								$Erg['Code']=3;
								$Erg['Zeile'] = $DS;
								$Erg['Letzte Meldung']='Fehler beim Schreiben der Rolle '.$Daten[$Ueberschriften['Rolle']];
								return $Erg;
							}
							$SQL = 'SELECT seq_FER_KEY.CurrVal AS KEY FROM DUAL';
							$rsKey = awisOpenRecordset($con,$SQL);

							$Rollen[strtoupper($Daten[$Ueberschriften['Rolle']])]=$rsKey['KEY'][0];
							$AWIS_KEY1 = $rsKey['KEY'][0];
						}
						else		// Rolle ist vorhanden
						{
							$Rollen[strtoupper($rsFER['FER_BEZEICHNUNG'][0])]=$rsFER['FER_KEY'][0];
							$AWIS_KEY1 = $rsFER['FER_KEY'][0];
						}
					}
					else
					{
						$AWIS_KEY1=(isset($Rollen[strtoupper($Daten[$Ueberschriften['Rolle']])])?$Rollen[strtoupper($Daten[$Ueberschriften['Rolle']])]:0);
					}
					//awis_debug(1,$AWIS_KEY1);
					$SQL = 'SELECT * ';
					$SQL .= ' FROM Kontakte';
					$SQL .= ' WHERE KON_PER_NR = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['PersNr']]);
					$SQL .= ' AND KON_STATUS = \'A\'';

					$rsKON = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen==1)
					{
						$Erg['Meldungen'][]='  - Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' gefunden.';

						if($Daten[$Ueberschriften['Gebiet']]!='')		// Zuordnung zu einem Gebiet
						{
							$XTNKuerzel = 'FEB';
							
							$SQL = ' SELECT * ';
							$SQL .= ' FROM FilialEbenen ';
							$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',$Daten[$Ueberschriften['Gebiet']]);
							$SQL .= ' AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE) AND trunc(FEB_GUELTIGAB) <= trunc(SYSDATE)';
							$SQL .= ' ORDER BY FEB_GUELTIGAB DESC';
	
							$rsFEB = awisOpenRecordset($con, $SQL);
							if($awisRSZeilen==0)
							{
								$Erg['Code']=4;
								$Erg['Zeile'] = $DS;
								$Erg['Letzte Meldung']='Zeile '.$DS.': Kann das Gebiet '.$Daten[$Ueberschriften['Gebiet']].' nicht finden';
	
								return $Erg;
							}
							
							$XXXKey = $rsFEB['FEB_KEY'][0];
						}
						elseif($Daten[$Ueberschriften['Filiale']]!='')
						{
							$XTNKuerzel = 'FIL';
							$XXXKey = (int)$Daten[$Ueberschriften['Filiale']];
						}
						
						if($XXXKey!==0)
						{
							$SQL = 'SELECT * ';
							$SQL .= ' FROM FILIALEBENENROLLENZUORDNUNGEN';							
							//$SQL .= ' WHERE FRZ_XXX_KEY = '.awis_FeldInhaltFormat('N0',$rsFEB['FEB_KEY'][0]);
							$SQL .= ' WHERE FRZ_XXX_KEY = '.awis_FeldInhaltFormat('N0',$XXXKey);								 
							$SQL .= ' AND FRZ_XTN_KUERZEL = \''.$XTNKuerzel.'\'';
							$SQL .= ' AND FRZ_FER_KEY = '.awis_FeldInhaltFormat('N0',$AWIS_KEY1);
							$SQL .= ' AND FRZ_KON_KEY = '.awis_FeldInhaltFormat('N0',$rsKON['KON_KEY'][0]);
							$SQL .= ' AND TRUNC(FRZ_GUELTIGAB) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
							$SQL .= ' AND TRUNC(FRZ_GUELTIGBIS) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
	
							$rsFRZ = awisOpenRecordset($con, $SQL);
							if($awisRSZeilen==0)	// Neu
							{
								$Erg['Meldungen'][]='Zeile '.$DS.': Datensatz wird hinzugef�gt: Rolle '.$AWIS_KEY1.' f�r '.$XXXKey.'/'.$XTNKuerzel;
	
								$SQL = 'INSERT INTO FILIALEBENENROLLENZUORDNUNGEN';
								$SQL .= '(FRZ_XXX_KEY,FRZ_XTN_KUERZEL,FRZ_FER_KEY,FRZ_KON_KEY,FRZ_GUELTIGAB,FRZ_GUELTIGBIS,FRZ_BEMERKUNG,FRZ_USER,FRZ_USERDAT)';
								$SQL .= ' VALUES (';
								$SQL .= '  '.awis_FeldInhaltFormat('N0',$XXXKey);
								$SQL .= ', \''.$XTNKuerzel.'\'';
								$SQL .= ', '.awis_FeldInhaltFormat('N0',$AWIS_KEY1);
								$SQL .= ', '.awis_FeldInhaltFormat('N0',$rsKON['KON_KEY'][0]);
								$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
								$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
								$SQL .= ', '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
								$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
								$SQL .= ', SYSDATE';
								$SQL .= ')';
	
								if(awisExecute($con, $SQL)===false)
								{
									$Erg['Code']=5;
									$Erg['Zeile'] = $DS;
									$Erg['Letzte Meldung']='Zeile '.$DS.': Fehler beim Schreiben der Rollenzuordnung '.$Daten[$Ueberschriften['Rolle']];
									return $Erg;
								}
							}
							else 		// schon vorhanden
							{
								$Erg['Meldungen'][]='Zeile '.$DS.': Datensatz ist vorhanden, wird ignoriert.';
							}
						}
					}
					elseif($awisRSZeilen==0)
					{
						$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' ('.$Daten[$Ueberschriften['PersNr']].') NICHT gefunden.';
					}
					else
					{
						$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' ist nicht eindeutig.';
					}
				}
				break;

			case 'U':
				if(!is_numeric($Daten[$Ueberschriften['KEY']]))
				{
					$Erg['Meldungen'][]='KEY '.$Daten[$Ueberschriften['KEY']].' f�r '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' ist nicht g�ltig.';
				}
				else
				{
					//
					// Rollen pr�fen und evtl. neu anlegen
					//
					$SQL = 'SELECT FER_KEY, FER_BEZEICHNUNG ';
					$SQL .= ' FROM FilialebenenRollen';
					$SQL .= ' WHERE fer_bezeichnung = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Rolle']]);
					$SQL .= ' AND FER_FRB_KEY = 0'.$BereichsID;

					$rsFER = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen==0)		// Neue Rolle
					{
						$Erg['Meldungen'][]='Neue Rolle '.$Daten[$Ueberschriften['Rolle']].' anlegen.';

						$SQL = 'INSERT INTO FilialEbenenRollen';
						$SQL .= '(FER_BEZEICHNUNG,FER_BEMERKUNG,FER_FRB_KEY,FER_GUELTIGAB,FER_GUELTIGBIS,FER_USER,FER_USERDAT)';
						$SQL .= ' VALUES (';
						$SQL .= ' '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Rolle']]);
						$SQL .= ' , \'Import\'';
						$SQL .= ' , '.awis_FeldInhaltFormat('N0',$BereichsID);
						$SQL .= ' , '.awis_FeldInhaltFormat('D',date('d.m.Y'));
						$SQL .= ' , '.awis_FeldInhaltFormat('D','30.12.2030');
						$SQL .= ' , \''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ' , SYSDATE';
						$SQL .= ' )';

						if(awisExecute($con, $SQL)===false)
						{
							$Erg['Code']=3;
							$Erg['Zeile'] = $DS;
							$Erg['Letzte Meldung']='Fehler beim Schreiben der Rolle '.$Daten[$Ueberschriften['Rolle']];
							return $Erg;
						}
						$SQL = 'SELECT seq_FER_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = awisOpenRecordset($con,$SQL);

						$Rollen[strtoupper($Daten[$Ueberschriften['Rolle']])]=$rsKey['KEY'][0];
						$AWIS_KEY1 = $rsKey['KEY'][0];
					}
					else		// Rolle ist vorhanden
					{
						$Rollen[strtoupper($rsFER['FER_BEZEICHNUNG'][0])]=$rsFER['FER_KEY'][0];
						$AWIS_KEY1 = $rsFER['FER_KEY'][0];
					}


					//
					// Kontakte
					//
					$SQL = 'SELECT * ';
					$SQL .= ' FROM Kontakte';
					$SQL .= ' WHERE KON_PER_NR = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['PersNr']]);
					//$SQL .= ' AND KON_STATUS = \'A\'';

					$rsKON = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen==1)
					{
						$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' gefunden.';
						$XXXKey = 0;

						if($Daten[$Ueberschriften['Gebiet']]!='')		// Zuordnung zu einem Gebiet
						{
							$XTNKuerzel = 'FEB';
							// Filialebene ermitteln
							$SQL = ' SELECT * ';
							$SQL .= ' FROM FilialEbenen ';
							$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',trim($Daten[$Ueberschriften['Gebiet']]));
							$SQL .= ' AND trunc(FEB_GUELTIGBIS) >= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
							$SQL .= ' AND trunc(FEB_GUELTIGAB) <= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
							$SQL .= ' ORDER BY FEB_GUELTIGAB DESC';

							$rsFEB = awisOpenRecordset($con, $SQL);
							if($awisRSZeilen==0)
							{
								$Erg['Code']=4;
								$Erg['Zeile'] = $DS;
								$Erg['Letzte Meldung']='Zeile '.$DS.': Kann das Gebiet '.$Daten[$Ueberschriften['Gebiet']].' nicht finden';

								return $Erg;
							}

							$XXXKey = $rsFEB['FEB_KEY'][0];
						}
						elseif($Daten[$Ueberschriften['Filiale']]!='')		// Rolle direkt auf eine Filiale
						{
							$XTNKuerzel = 'FIL';
							$XXXKey = (int)$Daten[$Ueberschriften['Filiale']];
						}

						if($XXXKey!==0)
						{
							$SQL = 'UPDATE FILIALEBENENROLLENZUORDNUNGEN SET';
							$SQL .= '  FRZ_XXX_KEY = '.awis_FeldInhaltFormat('N0',$XXXKey);
							$SQL .= ', FRZ_XTN_KUERZEL = \''.$XTNKuerzel.'\'';
							$SQL .= ', FRZ_FER_KEY = '.awis_FeldInhaltFormat('N0',$AWIS_KEY1);
							$SQL .= ', FRZ_KON_KEY = '.awis_FeldInhaltFormat('N0',$rsKON['KON_KEY'][0]);
							$SQL .= ', FRZ_GUELTIGAB = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
							$SQL .= ', FRZ_GUELTIGBIS = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
							$SQL .= ', FRZ_BEMERKUNG = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
							$SQL .= ', FRZ_USER = \''.$AWISBenutzer->BenutzerName().'\'';
							$SQL .= ', FRZ_USERDAT = SYSDATE';
							$SQL .= ' WHERE FRZ_KEY = '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['KEY']],false);

							if(awisExecute($con, $SQL)===false)
							{
								$Erg['Code']=5;
								$Erg['Zeile'] = $DS;
								$Erg['Letzte Meldung']='Zeile '.$DS.': Fehler beim Schreiben der Rollenzuordnung '.$Daten[$Ueberschriften['Rolle']];
								return $Erg;
							}

						}
						else
						{
							//Fehlermeldung
							$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' hat weder Gebiets noch Filialzuordnung.';
						}
					}
					elseif($awisRSZeilen>1)
					{
						$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' ist nicht eindeutig.';
					}
					else
					{
						$Erg['Meldungen'][]='Kontakt '.$Daten[$Ueberschriften['Nachname']].' '.$Daten[$Ueberschriften['Vorname']].' ('.$Daten[$Ueberschriften['PersNr']].') NICHT gefunden.';
					}
				}
				break;
			case 'D':
				break;
			default:
				default:
					$Erg['Code']=7;
					$Erg['Zeile'] = $DS;
					$Erg['Letzte Meldung']='Unbekannte Aktion :'.($Daten[$Ueberschriften['Aktion']]==''?'Leer':$Daten[$Ueberschriften['Aktion']]).'.';
					return $Erg;
				break;
		}

		$Zeile = trim(fgets($fd));
		$DS++;
	}


	return $Erg;
}

/**
 * Filialebenen
 *
 * @param string $Version
 * @param ressource $fd
 * @return array
 */
function import_FEB($Version, $fd)
{
	global $AWISBenutzer;
	global $awisRSZeilen;
	global $con;

	if($Version != "1.0")
	{
		$Erg = array('Code'=>2, 'Letzte Meldung'=>'Falsche Dateiversion!', 'Meldungen'=>array());
		return $Erg;
	}

	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array());

	$Zeile = trim(fgets($fd));
	$Ueberschriften = explode("\t",$Zeile);
	$Ueberschriften = array_flip($Ueberschriften);
	if(!isset($Ueberschriften['Bezeichnung']))
	{
		return(array('Code'=>1,'Letzte Meldung'=>'Falsche Ueberschrift','Zeile'=>0,'Meldungen'=>array()));
	}

	$Zeile = fgets($fd);
	$Rollen = array();
	$DS = 1;
	while(!feof($fd))
	{
		$Daten = explode("\t",$Zeile);
		if($Daten[$Ueberschriften['Bezeichnung']]!='')
		{
			switch(strtoupper((trim($Daten[$Ueberschriften['Aktion']]))))
			{
				case 'I':
					$SQL = ' SELECT * ';
					$SQL .= ' FROM FilialEbenen ';
					$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',trim($Daten[$Ueberschriften['Bezeichnung']]));
					$SQL .= ' AND TRUNC(FEB_GUELTIGAB) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
					$SQL .= ' AND TRUNC(FEB_GUELTIGBIS) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
					$rsFEB = awisOpenRecordset($con, $SQL);
					awis_Debug(1,$SQL);
					if($awisRSZeilen==0)
					{
						$Erg['Meldungen'][]='Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['Bezeichnung']].' neu anlegen.';
						$FEBFEBKey = null;
						if($Daten[$Ueberschriften['GehoertZu']]!=='' OR $Daten[$Ueberschriften['Ebene']]>1)
						{
							$SQL = ' SELECT FEB_KEY ';
							$SQL .= ' FROM FilialEbenen ';
							$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',trim($Daten[$Ueberschriften['GehoertZu']]));
							$SQL .= ' AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE) AND trunc(FEB_GUELTIGAB) <= trunc(SYSDATE)';
							$SQL .= ' ORDER BY FEB_GUELTIGAB DESC';

							$rsFEB = awisOpenRecordset($con, $SQL);
							if($awisRSZeilen==0)
							{
								$Erg['Code']=5;
								$Erg['Zeile'] = $DS;
								$Erg['Letzte Meldung']='�bergeordnete Ebene '.$Daten[$Ueberschriften['GehoertZu']].' existiert nicht.';
								return $Erg;
							}

							$FEBFEBKey=$rsFEB['FEB_KEY'][0];
						}

						$SQL = 'INSERT INTO Filialebenen';
						$SQL .= '(FEB_BEZEICHNUNG,FEB_EBENE,FEB_FEB_KEY,FEB_GUELTIGAB,FEB_GUELTIGBIS,FEB_BEMERKUNG,';
						$SQL .=' FEB_ANWENDUNG,FEB_SORTIERUNG,FEB_USER,FEB_USERDAT)';
						$SQL .= 'VALUES(';
						$SQL .= '  '.awis_FeldInhaltFormat('T',trim($Daten[$Ueberschriften['Bezeichnung']]));
						$SQL .= ', '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['Ebene']]);
						$SQL .= ', '.awis_FeldInhaltFormat('N0',$FEBFEBKey);
						$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
						$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
						$SQL .= ', '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
						$SQL .= ', 1';
						$SQL .= ','.$DS;
						$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', SYSDATE';
						$SQL .= ')';

						if(awisExecute($con, $SQL)===false)
						{
							$Erg['Code']=5;
							$Erg['Zeile'] = $DS;
							$Erg['Letzte Meldung']='Fehler beim Schreiben der Filialebene '.$Daten[$Ueberschriften['Bezeichnung']];
							return $Erg;
						}
						else
						{
							$Erg['Meldungen'][]='Filialebene '.$Daten[$Ueberschriften['Bezeichnung']].' wurde erfolgreich angelegt.';
						}
					}
					else
					{
						$Erg['Meldungen'][]='Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['Bezeichnung']].' ist vorhanden, wird ignoriert.';
					}
					break;
				case 'D':
					$SQL = ' DELETE ';
					$SQL .= ' FROM FilialEbenen ';
					if(isset($Daten[$Ueberschriften['KEY']]))
					{
						$SQL .= ' WHERE FEB_KEY = 0'.$Daten[$Ueberschriften['KEY']];
					}
					else
					{
						$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',trim($Daten[$Ueberschriften['Bezeichnung']]));
						$SQL .= ' AND TRUNC(FEB_GUELTIGAB) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
						$SQL .= ' AND TRUNC(FEB_GUELTIGBIS) = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
					}

					if(awisExecute($con, $SQL)===false)
					{
						$Erg['Code']=8;
						$Erg['Zeile'] = $DS;
						$Erg['Letzte Meldung']='Fehler beim L�schen einer Filialebene '.$Daten[$Ueberschriften['Bezeichnung']];
						return $Erg;
					}
					else
					{
						$Erg['Meldungen'][]=('Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['Bezeichnung']].' wurde gel�scht.');
					}
					break;
				case 'U':					// Immer den Letzten Aktualisieren
					$SQL = ' SELECT * ';
					$SQL .= ' FROM FilialEbenen ';
					if(isset($Ueberschriften['KEY']))
					{
						$SQL .= ' WHERE FEB_KEY = 0'.$Daten[$Ueberschriften['KEY']];
					}
					else
					{
						$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',$Daten[$Ueberschriften['Bezeichnung']]);
						$SQL .= ' ORDER BY FEB_GUELTIGBIS DESC';
					}
					$rsFEB = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen!=0)
					{
						$SQL = 'UPDATE FilialEbenen';
						$SQL .= ' SET ';
						$SQL .= '  FEB_BEZEICHNUNG = '.awis_FeldInhaltFormat('T',trim($Daten[$Ueberschriften['Bezeichnung']]));
						$SQL .= ', FEB_EBENE = '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['Ebene']]);
						$SQL .= ', FEB_GUELTIGAB = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
						$SQL .= ', FEB_GUELTIGBIS = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
						$SQL .= ', FEB_BEMERKUNG = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
						$SQL .= ', FEB_USER = \''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', FEB_USERDAT = SYSDATE';
						$SQL .= ' WHERE FEB_KEY = 0'.$rsFEB['FEB_KEY'][0];

						if(awisExecute($con, $SQL)===false)
						{
							$Erg['Code']=10;
							$Erg['Zeile'] = $DS;
							$Erg['Letzte Meldung']='Fehler beim �ndern der Filialebene '.$Daten[$Ueberschriften['Bezeichnung']];
							return $Erg;
						}
					}
					else
					{
						$Erg['Code']=8;
						$Erg['Zeile'] = $DS;
						$Erg['Letzte Meldung']='Ebene konnte nicht gefunden werden. Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['Bezeichnung']];
						return $Erg;
					}
					break;
				default:
					$Erg['Code']=7;
					$Erg['Zeile'] = $DS;
					$Erg['Letzte Meldung']='Unbekannte Aktion'.$Daten[$Ueberschriften['Aktion']];
					return $Erg;
			}
		}

		$Zeile = fgets($fd);
		$DS++;
	}

	return $Erg;
}


/**
 * FilialebenenZuordnungen
 *
 * @param string $Version
 * @param ressource $fd
 * @return array
 */
function import_FEZ($Version, $fd)
{
	global $AWISBenutzer;
	global $awisRSZeilen;
	global $con;

	if($Version != "1.0")
	{
		$Erg = array('Code'=>2, 'Letzte Meldung'=>'Falsche Dateiversion!', 'Meldungen'=>array());
		return $Erg;
	}

	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array());

	$Zeile = fgets($fd);
	$Ueberschriften = explode("\t",trim($Zeile));

	$Ueberschriften = array_flip($Ueberschriften);
	if(!isset($Ueberschriften['FilialID']))
	{
		return(array('Code'=>1,'Letzte Meldung'=>'Falsche Ueberschrift','Zeile'=>0,'Meldungen'=>array()));
	}
	$Zeile = fgets($fd);
	$Rollen = array();
	$DS = 1;
	while(!feof($fd))
	{
		$Daten = explode("\t",trim($Zeile));
		if($Daten[$Ueberschriften['FilialID']]!='')
		{
			switch(strtoupper(($Daten[$Ueberschriften['Aktion']])))
			{
				case 'I':
					$SQL = ' SELECT * ';
					$SQL .= ' FROM FilialEbenen ';
					$SQL .= ' WHERE UPPER(FEB_BEZEICHNUNG)='.awis_FeldInhaltFormat('TU',$Daten[$Ueberschriften['Filialebene']]);
					$SQL .= ' AND TRUNC(FEB_GUELTIGAB) <= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
					$SQL .= ' AND TRUNC(FEB_GUELTIGBIS) >= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
					$SQL .= ' ORDER BY FEB_GUELTIGAB DESC';
					$rsFEB = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen==0)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Letzte Meldung']='Zeile '.$DS.': Keine passende Ebene fuer die Filiale '.$Daten[$Ueberschriften['FilialID']].' gefunden.';
						return $Erg;
					}
					else
					{
						$SQL = 'SELECT *';
						$SQL .= ' FROM Filialen';
						$SQL .= ' WHERE FIL_ID = '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['FilialID']]).'';
						$rsFIL = awisOpenRecordset($con, $SQL);
						if($awisRSZeilen==0)
						{
							$Erg['Code']=6;
							$Erg['Zeile'] = $DS;
							$Erg['Letzte Meldung']='Zeile '.$DS.': Filiale '.$Daten[$Ueberschriften['FilialID']].' existiert nicht.';
							return $Erg;
						}
						else
						{
							$SQL = 'SELECT *';
							$SQL .= ' FROM FilialebenenZuordnungen';
							$SQL .= ' WHERE FEZ_FIL_ID = '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['FilialID']]).'';
							$SQL .= ' AND FEZ_FEB_KEY = '.awis_FeldInhaltFormat('N0',$rsFEB['FEB_KEY'][0]).'';
							$SQL .= ' AND TRUNC(FEZ_GUELTIGAB) <= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
							$SQL .= ' AND TRUNC(FEZ_GUELTIGBIS) >= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);

							$rsFEZ = awisOpenRecordset($con, $SQL);
							if($awisRSZeilen==0)
							{
								$SQL = 'INSERT INTO FilialebenenZuordnungen';
								$SQL .= '(FEZ_FEB_KEY,FEZ_FIL_ID,FEZ_GUELTIGAB,FEZ_GUELTIGBIS,FEZ_BEMERKUNG,FEZ_USER,FEZ_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= '  '.awis_FeldInhaltFormat('N0',$rsFEB['FEB_KEY'][0]).'';
								$SQL .= ', '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['FilialID']]).'';
								$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
								$SQL .= ', '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
								$SQL .= ', '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
								$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
								$SQL .= ', SYSDATE';
								$SQL .= ')';
								if(awisExecute($con, $SQL)===false)
								{
									$Erg['Code']=5;
									$Erg['Zeile'] = $DS;
									$Erg['Letzte Meldung']='Fehler beim Schreiben der Filialebene '.$Daten[$Ueberschriften['Bezeichnung']];
									return $Erg;
								}
								else
								{
									$Erg['Meldungen'][]='Filiale '.$Daten[$Ueberschriften['FilialID']].' wurde erfolgreich zugeordnet.';
								}
							}
							else
							{
								$Erg['Meldungen'][]='Zeile '.$DS.': Filiale '.$Daten[$Ueberschriften['FilialID']].' ist bereits zugeordnet, wird ignoriert.';
							}
						}
					}
					break;
				case 'D':
					$SQL = ' DELETE FROM FilialebenenZuordnungen';
					if(isset($Daten[$Ueberschriften['KEY']]))
					{
						$SQL .= ' WHERE FEZ_KEY = 0'.$Daten[$Ueberschriften['KEY']];
					}
					else
					{
						$SQL .= ' WHERE FEZ_FIL_ID = '.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['FilialID']]).'';
						$SQL .= ' AND FEZ_FEB_KEY = '.awis_FeldInhaltFormat('N0',$rsFEB['FEB_KEY'][0]).'';
						$SQL .= ' AND TRUNC(FEZ_GUELTIGAB) <= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
						$SQL .= ' AND TRUNC(FEZ_GUELTIGBIS) >= '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
					}

					if(awisExecute($con, $SQL)===false)
					{
						$Erg['Code']=8;
						$Erg['Zeile'] = $DS;
						$Erg['Letzte Meldung']='Fehler beim L�schen einer Filialebene '.$Daten[$Ueberschriften['Bezeichnung']];
						return $Erg;
					}
					else
					{
						$Erg['Meldungen'][]=('Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['Bezeichnung']].' wurde gel�scht.');
					}
					break;
				case 'U':
					$SQL = ' SELECT * ';
					$SQL .= ' FROM FilialebenenZuordnungen ';
					$SQL .= ' WHERE FEZ_KEY = 0'.awis_FeldInhaltFormat('N0',$Daten[$Ueberschriften['KEY']]);
					$rsFEZ = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen!=0)
					{
						$SQL = 'UPDATE FilialebenenZuordnungen SET ';
						$SQL .= 'FEZ_GUELTIGAB = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigAb']]);
						$SQL .= ', FEZ_GUELTIGBIS = '.awis_FeldInhaltFormat('D',$Daten[$Ueberschriften['GueltigBis']]);
						$SQL .= ', FEZ_BEMERKUNG = '.awis_FeldInhaltFormat('T',$Daten[$Ueberschriften['Bemerkung']]);
						$SQL .= ', FEZ_USER = \''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', FEZ_USERDAT = SYSDATE';
						$SQL .= ' WHERE FEZ_KEY = 0'.$rsFEZ['FEZ_KEY'][0];

						if(awisExecute($con, $SQL)===false)
						{
							$Erg['Code']=5;
							$Erg['Zeile'] = $DS;
							$Erg['Letzte Meldung']='Fehler beim �ndern der Filialebenenzuordnungen '.$Daten[$Ueberschriften['FilialID']];
							return $Erg;
						}
					}
					else
					{
						$Erg['Code']=8;
						$Erg['Zeile'] = $DS;
						$Erg['Letzte Meldung']='Filialenzuordnungen konnte nicht gefunden werden. Zeile '.$DS.': Ebene '.$Daten[$Ueberschriften['FilialID']];
						return $Erg;
					}
					break;
				default:
					$Erg['Code']=7;
					$Erg['Zeile'] = $DS;
					$Erg['Letzte Meldung']='Unbekannte Aktion'.$Daten[$Ueberschriften['Aktion']];
					return $Erg;
			}
		}

		$Zeile = fgets($fd);
		$DS++;
	}

	return $Erg;
}
?>
