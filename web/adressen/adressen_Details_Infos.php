<?php
global $Recht3000;
global $AWIS_KEY1;
global $AWISBenutzer;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try 
{
	if(isset($_GET['Del']) AND isset($_GET['Seite']) AND $_GET['Seite']=='Infos')
	{
		include('./adressen_loeschen.php');
	}
	
	$TextKonserven = array();
	$TextKonserven[]=array('ADI','ADI_%');
	$TextKonserven[]=array('ADT','ADT_%');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprache = $AWISBenutzer->ParameterLesen('AnzeigeSprache');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht3000 = $AWISBenutzer->HatDasRecht(3000);
	if($Recht3000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}
	if($AWIS_KEY1!='')		// ADR_KEY
	{
		$EditModus = ($Recht3000&6);
		$Form->Formular_Start();
		$SQL = 'SELECT ADT_KEY, ADT_INFORMATION, ADT_DATENQUELLE, ADT_BEMERKUNG, ADT_BREITE, ADT_ZEICHEN, ADT_FORMAT, ADI_WERT, ADI_KEY';
		$SQL .= ' FROM ADRESSENINFOTYPEN';
		$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
		$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
		$SQL .= ' LEFT OUTER JOIN ADRESSENINFORMATIONEN ON ADI_ADT_KEY=ADT_KEY AND ADI_XXX_KEY = 0'.$AWIS_KEY1;
		$SQL .= ' INNER JOIN Adressen on ADR_KEY='.$AWIS_KEY1;
		$SQL .= ' and Adressen.ADR_AZG_KEY = AZG_KEY';
		$SQL .= ' WHERE BITAND(ADT_VERWENDUNG,1)=1';
		$SQL .= ' ORDER BY ADT_SORTIERUNG, ADT_INFORMATION';
		
		$rsADI = $DB->RecordSetOeffnen($SQL);
		$rsADIZeilen = $rsADI->AnzahlDatensaetze();
		for($ADIZeile=0;$ADIZeile<$rsADIZeilen;$ADIZeile++)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($rsADI->FeldInhalt('ADT_INFORMATION').':',200,'','',$rsADI->FeldInhalt('ADT_BEMERKUNG'));

			if($rsADI->FeldInhalt('ADT_DATENQUELLE')!='')
			{
				switch(substr($rsADI['ADT_DATENQUELLE'][$ADIZeile],0,3))
                {
			        case 'TXT':
				        $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
				        $Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
				        $Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
				        $Form->Erstelle_SelectFeld('ADI_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
			        break;
			        case 'SQL':
				        $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
				        $Form->Erstelle_SelectFeld('ADI_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'',$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			        break;
			        default:
			            $Form->Erstelle_TextFeld('ADI_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'),'','',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
			        break;
		         }
		   }
	       else
	       {
	           $Form->Erstelle_TextFeld('ADI_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'));
	       }
	       $Form->ZeileEnde();
	       $rsADI->DSWeiter();
        }
		$Form->Trennzeile();
	}
	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>