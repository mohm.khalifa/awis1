<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'vcard_funktionen.inc';

    global $AWISCursorPosition;
    global $KON;

    try {
    $KON = new vcard_funktionen();


    echo "<link rel=stylesheet type=text/css href='" . $KON->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>VCards</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$KON->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(71000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$KON->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($KON->Form instanceof awisFormular) {
        $KON->Form->DebugAusgabe(1, $ex->getSQL());
        $KON->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $KON->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($KON->Form instanceof awisFormular) {
        $KON->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>