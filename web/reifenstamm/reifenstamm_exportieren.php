<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RRP','*');
	$TextKonserven[]=array('REH','*');
	$TextKonserven[]=array('RST','*');
	$TextKonserven[]=array('RPP','*');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15300 = $AWISBenutzer->HatDasRecht(15300);
	if($Recht15300==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	if (isset($_POST['sucEXPORTART']))
	{
		$Param['EXPORTART'] = $_POST['sucEXPORTART'];
		$Param['GELISTET'] = $_POST['sucGELISTET'];
		$Param['RRP_ATUNR'] = $_POST['sucRRP_ATUNR'];
		$Param['RST_REH_KEY'] = $_POST['sucRST_REH_KEY'];
		$Param['RST_TYP'] = $_POST['sucRST_TYP'];
		$Param['RST_TYP2'] = $_POST['sucRST_TYP2'];
		$Param['RST_TYP3'] = $_POST['sucRST_TYP3'];
		$Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');
		$AWISBenutzer->ParameterSchreiben("Formular_RRP_Export", serialize($Param));
	}
	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$DateiName = $_POST['sucEXPORTART'];
	
	$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);
	
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');

	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
			break;
		case 2:                 // Excel 2007
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
			break;
	}
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setTitle(utf8_encode($DateiName));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode($_POST['sucEXPORTART']));
	
	$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($_POST);
	
	switch($_POST['sucEXPORTART'])
	{
		case 'Herstellerdaten':
			// Alle Informationen sammeln
			$SQL  = 'SELECT SYSDATE AS EXPORTDATE,';
			$SQL .= ' RRP_KEY,';
			$SQL .= ' RRP_ATUNR,';
			$SQL .= ' RRP_WGR_ID,';
			$SQL .= ' RRP_WUG_ID,';
			$SQL .= ' RRP_GUELTIGAB,';
			$SQL .= ' RRP_GUELTIGBIS,';
			$SQL .= ' RST_KEY,';
			$SQL .= ' RST_STATUS,';
			$SQL .= ' RST_EAN,';
			$SQL .= ' REH_BEZEICHNUNG,';
			$SQL .= ' RST_BEZEICHNUNG,';
			$SQL .= ' RST_LIE_NR,';
			$SQL .= ' RST_LARTNR,';
			$SQL .= ' RST_KBPREIS,';
			$SQL .= ' RST_BREITE,';
			$SQL .= ' RST_QUERSCHNITT,';
			$SQL .= ' RST_BAUART,';
			$SQL .= ' RST_INNENDURCHMESSER,';
			$SQL .= ' RST_FELGENRIPPE,';
			$SQL .= ' RST_LLKW,';
			$SQL .= ' RST_LOADINDEX,';
			$SQL .= ' RST_LOADINDEX2,';
			$SQL .= ' RST_LOADINDEX3,';
			$SQL .= ' RST_RF,';
			$SQL .= ' RST_SPEEDINDEX,';
			$SQL .= ' RST_SPEEDINDEX2,';
			$SQL .= ' RST_ZUSATZBEMERKUNG,';
			$SQL .= ' RST_ZUSATZBEMERKUNG2,';
			$SQL .= ' RST_ZUSATZBEMERKUNG3,';
			$SQL .= ' RST_TYP,';
			$SQL .= ' RST_TYP2,';
			$SQL .= ' RST_TYP3,';
			$SQL .= ' RST_ROLLRES,';
			$SQL .= ' RST_WETGRIP,';
			$SQL .= ' RST_NOISEPE,';
			$SQL .= ' RST_NOISECL,';
			$SQL .= ' RST_WINTERTAUGLICHKEIT,';
			$SQL .= ' RST_EISGRIFF,';
			$SQL .= ' RST_INFO,';
			$SQL .= ' RST_URL4,';
			$SQL .= ' RST_URL5,';
			$SQL .= ' RST_BEMERKUNG';
			$SQL .= ' FROM reifenstamm';
			$SQL .= ' FULL OUTER JOIN reifenpflege ON rrp_rst_key=rst_key';
			$SQL .= ' LEFT JOIN reifenhersteller ON rst_reh_key=reh_key ';
			if ($Bedingung != '')
			{
				$SQL .= ' WHERE ' . substr($Bedingung, 4);
			}
			$SQL .= ' ORDER BY rrp_atunr, rrp_wgr_id, rrp_wug_id, rst_breite, rst_querschnitt, rst_innendurchmesser';
		
	
			$rsRRP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RST'));

			break;
		default:
			break;
	}
	
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode($DateiName));
	$SpaltenNr = 0;
	$ZeilenNr = 1;		

	if ($_POST['sucEXPORTART']=='MusterHerstellerdaten' or $_POST['sucEXPORTART']=='Herstellerdaten') 
	{
		//Überschrift
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($_POST['sucEXPORTART']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$SpaltenNr++;

		if ($_POST['sucEXPORTART']!='MusterHerstellerdaten')
		{
			//bin zu doof das Datum anders auszugeben :-(
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('DU',$rsRRP->FeldInhalt('EXPORTDATE')));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		}
		// Daten zeilenweise exportieren
		
		$ZeilenNr++;
		$SpaltenNr = 0;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'STATUS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ATUNR');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WG');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'SORT');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'GUELTIGAB');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'GUELTIGBIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'EAN');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'HERSTELLER');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'BEZEICHNUNG');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LIE_NR');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LARTNR');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'BRUTTO');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'BREITE');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'QUERSCHNITT');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'BAUART');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'INNENDURCHMESSER');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'FELGENRIPPE');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LLKW');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LOADINDEX');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LOADINDEX2');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LOADINDEX3');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'RF');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'SPEEDINDEX');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'SPEEDINDEX2');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ZUSATZBEMERKUNG');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ZUSATZBEMERKUNG2');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ZUSATZBEMERKUNG3');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'TYP');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'TYP2');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'TYP3');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ROLLRES');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WETGRIP');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'NOISEPE');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'NOISECL');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'BEMERKUNG');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WINTERTAUGLICHKEIT');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'EISGRIFF');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'INFO');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'URL4');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'URL5');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'RRP_KEY');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'RST_KEY');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
		$ZeilenNr++;
		$SpaltenNr=0;
		if ($_POST['sucEXPORTART']=='Herstellerdaten')
		{
			while(!$rsRRP->EOF())
			{
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_STATUS'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RRP_ATUNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RRP_WGR_ID'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RRP_WUG_ID'))),PHPExcel_Cell_DataType::TYPE_STRING);
				
				$Datum=$Form->PruefeDatum($rsRRP->FeldInhalt('RRP_GUELTIGAB'),false,false,true);
				$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, PHPExcel_Shared_Date::PHPToExcel($Datum));
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
				
				$Datum=$Form->PruefeDatum($rsRRP->FeldInhalt('RRP_GUELTIGBIS'),false,false,true);
				$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, PHPExcel_Shared_Date::PHPToExcel($Datum));
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
				
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_EAN'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('REH_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LIE_NR'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LARTNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('N2',$rsRRP->FeldInhalt('RST_KBPREIS')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_BREITE')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('T',$rsRRP->FeldInhalt('RST_QUERSCHNITT')),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_BAUART'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_INNENDURCHMESSER')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_FELGENRIPPE'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LLKW'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_LOADINDEX'))),PHPExcel_Cell_DataType::TYPE_STRING);

				if($rsRRP->FeldInhalt('RST_LOADINDEX2')!='')
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_LOADINDEX2')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				else
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '',PHPExcel_Cell_DataType::TYPE_STRING);

				if($rsRRP->FeldInhalt('RST_LOADINDEX3')!='')
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_LOADINDEX3')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				else
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '',PHPExcel_Cell_DataType::TYPE_STRING);

				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_RF'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_SPEEDINDEX'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_SPEEDINDEX2'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG2'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG3'))),PHPExcel_Cell_DataType::TYPE_STRING);

				if($rsRRP->FeldInhalt('RST_TYP')!='')
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				else
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '',PHPExcel_Cell_DataType::TYPE_STRING);
				
				if($rsRRP->FeldInhalt('RST_TYP2')!='')
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP2')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				else
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '',PHPExcel_Cell_DataType::TYPE_STRING);
									
				if($rsRRP->FeldInhalt('RST_TYP3')!='')
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_TYP3')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				else
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '',PHPExcel_Cell_DataType::TYPE_STRING);
									
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_ROLLRES'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_WETGRIP'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_NOISEPE'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_NOISECL'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_BEMERKUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
                $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_WINTERTAUGLICHKEIT')=='X'?$rsRRP->FeldInhalt('RST_WINTERTAUGLICHKEIT'):'')),PHPExcel_Cell_DataType::TYPE_STRING);
                $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_EISGRIFF'))),PHPExcel_Cell_DataType::TYPE_STRING);
                $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_INFO'))),PHPExcel_Cell_DataType::TYPE_STRING);
                $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_URL4'))),PHPExcel_Cell_DataType::TYPE_STRING);
                $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRP->FeldInhalt('RST_URL5'))),PHPExcel_Cell_DataType::TYPE_STRING);

				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RRP_KEY')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
				
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('Z',$rsRRP->FeldInhalt('RST_KEY')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
				
				$ZeilenNr++;
				$SpaltenNr=0;
				
				$rsRRP->DSWeiter();
			}
		}
		elseif ($_POST['sucEXPORTART']=='MusterHerstellerdaten')
		{
			$Statustext='10=Neu/20=Bearbeitung/30=Aktiv/40=Inaktiv';
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Statustext,PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'CN4003',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '01',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '100',PHPExcel_Cell_DataType::TYPE_STRING);
			
			$Datum=$Form->PruefeDatum('01.10.2013',false,false,true);
			$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, PHPExcel_Shared_Date::PHPToExcel($Datum));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
			
			$Datum=$Form->PruefeDatum('31.12.2030',false,false,true);
			$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, PHPExcel_Shared_Date::PHPToExcel($Datum));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
			
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '1234567890123',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'CONTINENTAL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'CONTI eCONTACT',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '1234',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '4711',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '100,10',PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 225,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 55,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'R',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 16,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'C',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 73,PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 73,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 73,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'EL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'W',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 1,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 2,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 3,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'A',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'A',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '80',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'A',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'X',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, ' ',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'URL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'URL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '0',PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '0',PHPExcel_Cell_DataType::TYPE_NUMERIC);
		}
	}
	
	for($S='A';$S<='F';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
			break;
		case 2:                 // Excel 2007
			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
			break;
	}
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}



/**
 * Bedingung zusammenbauen
 *
 * @param string $_POST
 * @return string
 */
function _BedingungErstellen()
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if (floatval($AWIS_KEY1) != 0)
	{
		$Bedingung.= 'AND RST_KEY = :var_N0_RST_KEY';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_KEY', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
		return $Bedingung;
	}

	if (isset($_POST['sucGELISTET']) AND $_POST['sucGELISTET'] != '-1')
	{
		if ($_POST['sucGELISTET']<2)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS '.($_POST['sucGELISTET']==0?'':'NOT').' NULL ';
		}
		elseif ($_POST['sucGELISTET']==2)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS NOT NULL ';
			$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
		}
		elseif ($_POST['sucGELISTET']==3)
		{
			$Bedingung .= 'AND RRP_AST_KEY IS NULL ';
			$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
		}
	}
	
	if (isset($_POST['sucRST_STATUS']) AND $_POST['sucRST_STATUS'] != '-1')
	{
		$Bedingung .= 'AND RST_STATUS = :var_N0_RST_STATUS ';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_STATUS', $_POST['sucRST_STATUS'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['sucRRP_ATUNR']) AND $_POST['sucRRP_ATUNR'] != '')
	{
		$Bedingung .= 'AND RRP_ATUNR ' . $DB->LIKEoderIST($_POST['sucRRP_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER);
	}

	if (isset($_POST['sucRST_REH_KEY']) AND $_POST['sucRST_REH_KEY'] != '-1')
	{
		$Bedingung .= 'AND RST_REH_KEY = :var_N0_RST_REH_KEY ';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_REH_KEY', $_POST['sucRST_REH_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}

	if (isset($_POST['sucRST_TYP']) AND $_POST['sucRST_TYP'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP) ';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_TYP', $_POST['sucRST_TYP'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['sucRST_TYP2']) AND $_POST['sucRST_TYP2'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP2 ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP2 ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP2) ';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_TYP2', $_POST['sucRST_TYP2'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['sucRST_TYP3']) AND $_POST['sucRST_TYP3'] != '-1')
	{
		$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP3 ';
		$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP3 ';
		$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP3) ';
		$DB->SetzeBindevariable('RST', 'var_N0_RST_TYP3', $_POST['sucRST_TYP3'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	return $Bedingung;
}

?>