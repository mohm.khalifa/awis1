<?php

class BestandV3 extends Serialize
{

    /**
     * @var BestandsabfrageV3 $OriginalArtikel
     */
    protected $OriginalArtikel = null;

    /**
     * @var ArrayOfBestandsabfrageV3 $ErsatzArtikel
     */
    protected $ErsatzArtikel = null;

    /**
     * @var ArrayOfBestandsabfrageV3 $AlternativArtikel
     */
    protected $AlternativArtikel = null;

    /**
     * @var ArrayOfBestandsabfrageV3 $ZusatzArtikel
     */
    protected $ZusatzArtikel = null;

    /**
     * @var string $Feedback
     */
    protected $Feedback = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BestandsabfrageV3
     */
    public function getOriginalArtikel()
    {
      return $this->OriginalArtikel;
    }

    /**
     * @param BestandsabfrageV3 $OriginalArtikel
     * @return BestandV3
     */
    public function setOriginalArtikel($OriginalArtikel)
    {
      $this->OriginalArtikel = $OriginalArtikel;
      return $this;
    }

    /**
     * @return ArrayOfBestandsabfrageV3
     */
    public function getErsatzArtikel()
    {
      return $this->ErsatzArtikel;
    }

    /**
     * @param ArrayOfBestandsabfrageV3 $ErsatzArtikel
     * @return BestandV3
     */
    public function setErsatzArtikel($ErsatzArtikel)
    {
      $this->ErsatzArtikel = $ErsatzArtikel;
      return $this;
    }

    /**
     * @return ArrayOfBestandsabfrageV3
     */
    public function getAlternativArtikel()
    {
      return $this->AlternativArtikel;
    }

    /**
     * @param ArrayOfBestandsabfrageV3 $AlternativArtikel
     * @return BestandV3
     */
    public function setAlternativArtikel($AlternativArtikel)
    {
      $this->AlternativArtikel = $AlternativArtikel;
      return $this;
    }

    /**
     * @return ArrayOfBestandsabfrageV3
     */
    public function getZusatzArtikel()
    {
      return $this->ZusatzArtikel;
    }

    /**
     * @param ArrayOfBestandsabfrageV3 $ZusatzArtikel
     * @return BestandV3
     */
    public function setZusatzArtikel($ZusatzArtikel)
    {
      $this->ZusatzArtikel = $ZusatzArtikel;
      return $this;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
      return $this->Feedback;
    }

    /**
     * @param string $Feedback
     * @return BestandV3
     */
    public function setFeedback($Feedback)
    {
      $this->Feedback = $Feedback;
      return $this;
    }

}
