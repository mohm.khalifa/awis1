<?php
/**
 * Formular: 01 - Hardware
 * @author Sacha Kerres
 *
 */


$FormularKennung = '001';
$DetailBlock = '';
$DetailBlock110 = '';


$DetailFelder = $Form->NameInArray($_POST, 'txtDETAILS', 2, 0);
$Form->DebugAusgabe(3,$DetailFelder);
$OT_EOL = "\r\n";



if(isset($_POST['txtOTF_WERT_141']) and $_POST['txtOTF_WERT_141'] != '' )
{
    $DetailBlock .= $OT_EOL.'Auslieferungsfiliale:'.$OT_EOL . $_POST['txtOTF_WERT_141'] . $OT_EOL;
}

//****************************
// Neueintritt / Nachbesetzung
//****************************

$Block='190';
$Daten='';

$DetailBlock190='';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Neueintritt']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Neueintritt']=='on')
{
    $DetailBlock190 .= ' Neueintritt'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nachbesetzung']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nachbesetzung']=='on')
{
    $DetailBlock190 .= '  Nachbesetzung'.$OT_EOL;
}
if($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-VorherigerMA']!='')
{
    $DetailBlock190 .= $OT_EOL.'Name des vorherigen Mitarbeiters:'.$OT_EOL;
    $DetailBlock190 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-VorherigerMA'].$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BestehenderMA']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BestehenderMA']=='on')
{
    $DetailBlock190 .= '  bestehender Mitarbeiter'.$OT_EOL;
}
if($DetailBlock190!='')
{
    $DetailBlock .= $OT_EOL.'Mitarbeiter:'.$OT_EOL . $DetailBlock190 . $OT_EOL;
}

//****************************
// Hardware
//****************************

$Block='120';
$Daten='';

$DetailBlock140='';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-ThinClient']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-ThinClient']=='on')
{
    $DetailBlock140 .= ' Thin Client(Igel)'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Notebook']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Notebook']=='on')
{
    $DetailBlock140 .= '  Notebook'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Handy']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Handy']=='on')
{
    $DetailBlock140 .= '  Handy'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Smartphone']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Smartphone']=='on')
{
    $DetailBlock140 .= '  Smartphone'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges']!='')
{
    $DetailBlock140 .= $OT_EOL.'Sonstiges:'.$OT_EOL;
    $DetailBlock140 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges'].$OT_EOL;
}
if($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck']!='')
{
    $DetailBlock140 .= $OT_EOL.'Verwendungszweck:'.$OT_EOL;
    $DetailBlock140 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck'].$OT_EOL;
}
if($DetailBlock140!='')
{
    $DetailBlock .= $OT_EOL.'Hardware:'.$OT_EOL . $DetailBlock140 . $OT_EOL;
}

//****************************
// Drucker
//****************************

$Block='140';
$Daten='';

$DetailBlock140='';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-SW']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-SW']=='on')
{
    $DetailBlock140 .= '  Schwarz-wei�-Drucker'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FARBE']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FARBE']=='on')
{
    $DetailBlock140 .= '  Farbdrucker'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges']!='')
{
    $DetailBlock140 .= $OT_EOL.'Sonstiges:'.$OT_EOL;
    $DetailBlock140 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges'].$OT_EOL;
}
if($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck']!='')
{
    $DetailBlock140 .= $OT_EOL.'Verwendungszweck:'.$OT_EOL;
    $DetailBlock140 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck'].$OT_EOL;
}
if($DetailBlock140!='')
{
    $DetailBlock .= $OT_EOL.'Drucker:'.$OT_EOL . $DetailBlock140 . $OT_EOL;
}



//****************************
// Zusatzausstattung
//****************************
$Block='150';
$Daten='';

$DetailBlock150='';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Maus']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Maus']=='on')
{
    $DetailBlock150 .= '  Maus'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Tastatur']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Tastatur']=='on')
{
    $DetailBlock150 .= '  Tastatur'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Monitor']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Monitor']=='on')
{
    $DetailBlock150 .= '  Monitor'.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefon']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefon']=='on')
{
    $DetailBlock150 .= '  Telefon'.$OT_EOL;
}

$Block = '180';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetZentrale']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetZentrale']=='on')
{


    $DetailBlock150 .= '  HeadsetZentrale:'.$OT_EOL;
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetTyp']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetTyp']!='') {
        $DetailBlock150 .= '   HeadsetTyp: ' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetTyp'].$OT_EOL;
    }
}
$Block = '160';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetFiliale']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetFiliale']=='on')
{
    $DetailBlock150 .= '  HeadsetFiliale:'.$OT_EOL;
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle']!='') {
        $DetailBlock150 .= '   Nebenstelle :' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle'].$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp']!='') {
        $DetailBlock150 .= '   Telefontyp :' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp'].$OT_EOL;
    }
}
$Block = '170';
if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Repeater']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Repeater']=='on')
{
    $DetailBlock150 .= '  Repeater:'.$OT_EOL;
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle']!='') {
        $DetailBlock150 .= '   Nebenstelle: ' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Nebenstelle'].$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp']!='') {
        $DetailBlock150 .= '   Telefontyp: ' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Telefontyp'].$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetVerbunden']) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetVerbunden']!='') {
        $DetailBlock150 .= '   HeadsetVerbunden: ' . $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-HeadsetVerbunden'].$OT_EOL;
    }
}

$Block = '150';
if($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges']!='')
{
    $DetailBlock150 .= $OT_EOL.'Sonstiges:'.$OT_EOL;
    $DetailBlock150 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges'].$OT_EOL;
}
if($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck']!='')
{
    $DetailBlock150 .= $OT_EOL.'Verwendungszweck:'.$OT_EOL;
    $DetailBlock150 .= ''.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Zweck'].$OT_EOL;
}
if($DetailBlock150!='')
{
    $DetailBlock .= $OT_EOL.'Zusatzausstattung:'.$OT_EOL . $DetailBlock150 . $OT_EOL;
}



// Bei Filialen Sonderbehandlung f�r das Anzeigefeld
if(isset($_POST['txtOTF_WERT_110']) AND $_POST['txtOTF_WERT_110']!='')
{
    $_POST['txtOTF_WERT_108']='Filiale';
    $_POST['txtOTF_WERT_109']=$_POST['txtOTF_WERT_110'];
}
else 
{
    $_POST['txtOTF_WERT_110']='~';
}

//*********************************************************
//* Bemerkung
//*********************************************************
$Block='900';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']!='')
{
    $DetailBlock .= $OT_EOL.'Allgemeine Hinweise:'.$OT_EOL;
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']. $OT_EOL;
}


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']!='') {

    $Namelist = '';
    foreach($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn'] as $KEY)
    {
        $Namelist .= ';'.$KEY;
    }
     $Namelist = substr($Namelist,1);

    $DetailBlock110 .= $Namelist;

    $DetailBlock .= $OT_EOL.'Benachrichtigung an:'.$OT_EOL;
    $DetailBlock .= $Namelist. $OT_EOL;
}

?>