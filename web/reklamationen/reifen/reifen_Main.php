<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'reifen_funktionen.inc';

    global $AWISCursorPosition;
    global $REX;

    try {
    $REX = new reifen_funktionen();

    if(isset($_POST['cmdPDF_x']) and isset($_POST['txtDruckKey'])){
        $_GET['XRE'] = 66;
        $_GET['ID'] = 'bla';
        require_once '/daten/web/berichte/drucken.php';
    }

    echo "<link rel=stylesheet type=text/css href='" . $REX->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $REX->AWISSprachKonserven['TITEL']['tit_ReklamationenReifen'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$REX->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(52000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$REX->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>