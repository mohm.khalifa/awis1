set pagesize 0;
set heading off;
set feedback off;
set verify off;
set term off;
set trimspool on;
set linesize 32676;

spool /win/applicationdata2/AWIS/06-Smartkat/awis_apollo_zusatzreferenzen.csv REPLACE;

SELECT
       LIEFARTNR|| ';' ||
       LIEFNR|| ';' ||
       ATUNR_REF|| ';' ||
       REFTYP|| ';' ||
       REPLACE (REFTEXT,';','') as REFTEXT
       FROM V_SMARTKAT_ALTARTI_APOLLO
;

spool off;
exit;
