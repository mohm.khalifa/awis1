<?php
global $con;
global $awisRSZeilen;


$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
$BSBreite = awis_BenutzerParameter($con, "BildschirmBreite" , $_SERVER['PHP_AUTH_USER']);

$TextKonserven = array();
$TextKonserven[]=array('MIU','MIU_%');
$TextKonserven[]=array('Liste','lst_JaNein');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010);

$MaxDSAnzahl = 500;

$SQL = 'SELECT MIU_KEY, MIU_MUT_KEY, MIU_BEMERKUNG, MUT_UNTERNEHMENSTYP, MIU_MBW_KEY';
$SQL .= ' FROM mitbewerbutypen INNER JOIN mitbewerbutypentypen ON miu_mut_key = mut_key';
$SQL .= ' WHERE miu_mbw_key=0'.$Param;
if(isset($_GET['Edit']))
{
	$SQL .= ' AND miu_key=0'.$_GET['Edit'];
}

awis_Debug(1,$SQL);
$rsMIU = awisOpenRecordset($con, $SQL);
$rsMIUZeilen = $awisRSZeilen;

//**************************************
// Liste anzeigen
//**************************************
if($rsMIUZeilen > 1 OR !isset($_GET['Edit']))
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3010&128)==128)	// Unternehmenstypen zuordnen
	{
		$Icons[] = array('new','./mitbewerber_Main.php?cmdAktion=Details&Seite=Unternehmenstypen&Edit=0');
		awis_FORM_Erstelle_ListeIcons($Icons,34,-1);
	}
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MIU']['MIU_MUT_KEY'],400);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MIU']['MIU_BEMERKUNG'],($BSBreite<1024?250:400));
	awis_FORM_ZeileEnde();

	if(!$rsMIUZeilen>$MaxDSAnzahl)
	{
		$rsMIUZeilen=$MaxDSAnzahl;
	}
	for($rsMIUZeile=0;$rsMIUZeile<$rsMIUZeilen;$rsMIUZeile++)
	{
		awis_FORM_ZeileStart();

		$Icons = array();
		if(($Recht3010&128)==128)	// Ändernrecht
		{
			$Icons[] = array('edit','./mitbewerber_Main.php?cmdAktion=Details&Seite=Unternehmenstypen&Edit='.$rsMIU['MIU_KEY'][$rsMIUZeile]);
		}
		if(($Recht3010&65536)==65536)	// Löschrecht
		{
			$Icons[] = array('delete','./mitbewerber_Main.php?cmdAktion=Details&Seite=Unternehmenstypen&Del='.$rsMIU['MIU_KEY'][$rsMIUZeile]);
		}

		awis_FORM_Erstelle_ListeIcons($Icons,34,($rsMIUZeile%2));
		awis_FORM_Erstelle_ListenFeld('MUT_UNTERNEHMENSTYP',$rsMIU['MUT_UNTERNEHMENSTYP'][$rsMIUZeile],0,400,false,($rsMIUZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MIU_BEMERKUNG',$rsMIU['MIU_BEMERKUNG'][$rsMIUZeile],0,($BSBreite<1024?250:400),false,($rsMIUZeile%2),'','');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 			// einen Datensatz anzeigen
{
	awis_FORM_FormularStart();

	echo '<input type=hidden name=txtMIU_KEY value='.(isset($rsMIU['MIU_KEY'][0])?$rsMIU['MIU_KEY'][0]:'').'>';
	echo '<input type=hidden name=txtMIU_MBW_KEY value='.$Param.'>';

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MIU']['MIU_MUT_KEY'],150,'');
	$SQL = 'SELECT MUT_KEY,MUT_Unternehmenstyp FROM Mitbewerbutypentypen ';
	$SQL .= ' WHERE MUT_KEY NOT IN (SELECT MIU_MUT_KEY FROM mitbewerbutypen WHERE MIU_MBW_KEY = 0'.$Param.' AND MIU_MUT_KEY<>0'.(isset($rsMIU['MIU_MUT_KEY'][0])?$rsMIU['MIU_MUT_KEY'][0]:'').')';
	$SQL .= ' ORDER BY MUT_Unternehmenstyp';
	awis_FORM_Erstelle_SelectFeld('MIU_MUT_KEY',(isset($rsMIU['MIU_MUT_KEY'][0])?$rsMIU['MIU_MUT_KEY'][0]:''),300,(($Recht3010&128)!=0),$con,$SQL,'','','MUT_UNTERNEHMENSTYP','','','');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MIU']['MIU_BEMERKUNG'],150,'');
	awis_FORM_Erstelle_TextFeld('MIU_BEMERKUNG',(isset($rsMIU['MIU_BEMERKUNG'][0])?$rsMIU['MIU_BEMERKUNG'][0]:''),80,0,($Recht3010&128),'','','');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}




?>