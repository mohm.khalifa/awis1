<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Benutzerparameter');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new awisFormular(); 
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Benutzerparameter'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if($AWISBenutzer->HatDasRecht(6)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"200812180900");
		die();
	}

	$Register = new awisRegister(6);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

	if($AWISCursorPosition!=='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180830");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</body>
</html>