<?php

class awisCoreStoreModel {
    public $id; //int
    public $name; //String
    public $shortName; //String
    public $street; //String
    public $zipCode; //String
    public $city; //String
    public $country; //Country
    public $phone; //Phone
    public $openingHours; //OpeningHours
    public $generalInspection; //GeneralInspection
    public $geoLocation; //GeoLocation
    public $googlePlaceId; //String
    public $note; //String
    public $online; //boolean
    public $aboutUsFooter; //String
    public $eMailAddress; //String
    public $onlineAppointment; //boolean

}

class Country {
    public $isoCode; //String
    public $isoCurrencyCode; //String

}
class Phone {
    public $national; //String
    public $international; //String

}
class Monday {
    public $start; //String
    public $end; //String

}
class Tuesday {
    public $start; //String
    public $end; //String

}
class Wednesday {
    public $start; //String
    public $end; //String

}
class Thursday {
    public $start; //String
    public $end; //String

}
class Friday {
    public $start; //String
    public $end; //String

}
class Saturday {
    public $start; //String
    public $end; //String

}
class OpeningHours {
    public $monday; //Monday
    public $tuesday; //Tuesday
    public $wednesday; //Wednesday
    public $thursday; //Thursday
    public $friday; //Friday
    public $saturday; //Saturday
    public $sunday; //array( undefined )

}
class GeneralInspection {
    public $monday; //String
    public $tuesday; //String
    public $wednesday; //String
    public $thursday; //String
    public $friday; //String
    public $saturday; //String
    public $sunday; //array( undefined )

}
class GeoLocation {
    public $latitude; //double
    public $longitude; //double

}



