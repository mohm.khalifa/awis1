<?php

require_once ('awisDatenbank.inc');
require_once ('awisBenutzer.inc');

ini_set ( 'max_execution_time', 0 );

date_default_timezone_set ( 'Europe/Berlin' );

error_reporting ( "E_ALL" );

class SGHImport 
{
	private $_Funktionen;
	private $_DB;
	protected $_AWISBenutzer;
	
	protected $pfadSGHDaten = '/daten/daten/sgh/SGHDATEN/';
	protected $pfadSGHKassendaten = '/daten/daten/sgh/SGHKassendaten/';
	
	protected $SGHDaten = array ();
	protected $SGHKassendaten = array ();
	protected $SGHDetaildaten = array ();
	protected $aktZeile;
	
	public function __construct() 
	{
		$this->_AWISBenutzer = awisBenutzer::Init ();
		$this->_DB = awisDatenbank::NeueVerbindung ( 'AWIS' );
		$this->_DB->Oeffnen ();
	}
	
	public function starteImport() 
	{
		$this->aktSGHDateien ();
		$this->importSGHDaten ();
		$this->sghDatensatzSpeichern ();
		$this->sghKassendatenSpeichern ();
	}
	
	protected function aktSGHDateien() 
	{
		$checkFile = '';
		$element = 0;
		
		if (($handle = opendir($this->pfadSGHDaten)) !== false) 
		{
			while (($file = readdir($handle)) !== false) 
			{
				$checkFile = '';
				$checkFile = substr ( $file, 0, 3 );
				
				if ($file == '.' || $file == '..') 
				{
				
				} 
				else 
				{
					if ($checkFile == 'imp') 
					{
					
					} 
					else 
					{
						echo 'Importiere ' .$file ."\n";
						$this->SGHDATEN [$element] = $file;
						$element ++;
					}
				}
			}
			//var_dump($this->SGHDATEN);
		} 
		else 
		{
			echo "Problem mit Pfad\n";
		}
		echo "\n";	
	}
	
	protected function importSGHDaten() 
	{
		//Auslesen der Datei - Anzahl der Datens�tze in Importprotokoll speichern
		for($element = 0; $element < count($this->SGHDATEN); $element++) 
		{
			$sghDateiname = "";
			$anzahl = 0;
			$anzahlZeilenEntfernt = 0;
			$fd = null;
			
			$sghDateiname = $this->SGHDATEN [$element];
			echo "bearbeite Datei: " .$sghDateiname ."\n";
			
			if (($fd = fopen($this->pfadSGHDaten .$sghDateiname,'r')) === false) 
			{
				echo "Fehler beim �ffnen der Datei\n";
			} 
			else 
			{
				while (!feof($fd)) 
				{
					$this->aktZeile = trim(fgets($fd));
					
					if ($this->aktZeile != "") 
					{
						$this->aktZeile = str_replace ( '"', '', $this->aktZeile );
						$this->SGHDetaildaten[] = explode ( ';', $this->aktZeile );
						$anzahl ++;
					} 
					else 
					{
						$anzahlZeilenEntfernt ++;
					}
				}
				fclose ( $fd );
			}

			echo "Anzahl der eingelesenen Datensetze: " . $anzahl ."\n";
			echo "Anzahl der Zeilen die entfernt wurden: " . $anzahlZeilenEntfernt ."\n";
		}
		echo "\n";
	}
	
	protected function sghDatensatzSpeichern() 
	{
		try
		{
			echo "INSERT INTO SGHDIENSTLEISTER ...\n";
			$this->_DB->TransaktionBegin();

			for($i = 0; $i < count($this->SGHDetaildaten); $i ++) 
			{
				$SQL = "INSERT INTO SGHDIENSTLEISTER (SGD_RNRSGH,SGD_RNRDAT,SGD_RNRTRM,SGD_FILNR,";
				$SQL .= "SGD_WANR,SGD_EINBAUDATUM,SGD_ATUNR,SGD_MENGE,SGD_BETRAG,SGD_KFZKENNZ,";
				$SQL .= "SGD_MAPPE,SGD_LIEFERANTENNR,SGD_USER,SGD_USERDAT) VALUES (";
				$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [0], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->SGHDetaildaten [$i] [1], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [2], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [3], false );
				
				if (strlen ( $this->SGHDetaildaten [$i] [4] ) == 14) 
				{
					$AXWANR = "";
					$AXWANR = substr ( $this->SGHDetaildaten [$i] [4], 6, 7 );
					
					if (strlen ( $AXWANR ) == 7) 
					{
						if (substr ( $AXWANR, 0, 1 ) == 0) 
						{
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', substr ( $AXWANR, 1 ), false );
						} 
						else 
						{
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', substr ( $this->SGHDetaildaten [$i] [4], 6, 7 ), false );
						}
					}
				} 
				elseif (strlen ( $this->SGHDetaildaten [$i] [4] ) == 7) 
				{
					$WANR = "";
					$WANR = $this->SGHDetaildaten [$i] [4];
					if (substr ( $WANR, 0, 1 ) == 0) {
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', substr ( $WANR, 1 ), false );
					} elseif (substr ( $WANR, 6, 1 ) == 0) {
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', substr ( $WANR, 0, 6 ), false );
					}
				} 
				else 
				{
					$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [4], false );
				}
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->SGHDetaildaten [$i] [5], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [6], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $this->SGHDetaildaten [$i] [7], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->SGHDetaildaten [$i] [8], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [9], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [10], false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->SGHDetaildaten [$i] [11], false );
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				//echo $SQL;
				//echo "\n";
				$this->_DB->Ausfuehren($SQL,'',true);
			}

			$this->_DB->TransaktionCommit();
		}
		catch (awisException $e)
		{
			echo $e->getMessage(). '/' . $e->getSQL();
			$this->_DB->TransaktionRollback();
		}
		catch (Exception $e)
		{
			echo $e->getMessage() . '/' . $e->getCode();
			$this->_DB->TransaktionRollback();
		}
	}
	
	protected function sghKassendatenSpeichern() 
	{
		try
		{
			$countInsert = 0;
			$countUpdate = 0;

			echo "Import SGH Kassendaten\n";
			$this->_DB->TransaktionBegin();
			
			$SQL  = 'Select DISTINCT FILIALNUMMER,';
			$SQL .= ' TRANS_DATUM,';
			$SQL .= ' substr(ZEIT,1,50) AS ZEIT,';
			$SQL .= ' BSA,';
			$SQL .= ' VORGANGSNR,';
			$SQL .= ' ARTIKELNUMMER,';
			$SQL .= ' ARTIKELBEZEICHNUNG,';
			$SQL .= ' ANZAHL,';
			$SQL .= ' BRUTTOPREIS,';
			$SQL .= ' KFZ_KENNZEICHEN,';
			$SQL .= ' DWH$INSDATE AS DWHINSDATE,';
			$SQL .= ' DWH$AUDIT AS DWHAUDIT,';
			$SQL .= ' WA_GEN_DATUM';
			//PROD DWH
			$SQL .= ' FROM DWH.V_SGH_KASSENDATEN_V2@DWH ';
			//TEST BACKUP DWH 
			//$SQL .= ' FROM DWH.V_SGH_KASSENDATEN_V2@DWH_ODS ';
			
			echo "$SQL\n";
			
			$rsKassendatenViewDWH = $this->_DB->RecordSetOeffnen ( $SQL );
			
			echo "Anzahl der eingelesenen Datensetze: " . $rsKassendatenViewDWH->AnzahlDatensaetze() ."\n";
			
			while (!$rsKassendatenViewDWH->EOF()) 
			{
				$SQL = "Select * from SGHKASSENDATEN WHERE SGK_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'FILIALNUMMER' ) );
				$SQL .= " AND SGK_DATUM=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'TRANS_DATUM' ) );
				$SQL .= " AND SGK_ZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ZEIT' ) );
				$SQL .= " AND SGK_WANR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'VORGANGSNR' ) );
				$SQL .= " AND SGK_ARTNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ARTIKELNUMMER' ) );
				$SQL .= " AND SGK_MENGE=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'ANZAHL' ) );
				$SQL .= " AND SGK_UMSATZ=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendatenViewDWH->FeldInhalt ( 'BRUTTOPREIS' ) );
				
				$rsKassendaten = $this->_DB->RecordSetOeffnen ($SQL);
				
				if ($rsKassendaten->EOF()) 
				{
					$SQL = "Select * from SGHKASSENDATENABGL WHERE SKS_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'FILIALNUMMER' ) );
					$SQL .= " AND SKS_DATUM=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'TRANS_DATUM' ) );
					$SQL .= " AND SKS_ZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ZEIT' ) );
					$SQL .= " AND SKS_WANR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'VORGANGSNR' ) );
					$SQL .= " AND SKS_ARTNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ARTIKELNUMMER' ) );
					$SQL .= " AND SKS_MENGE=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'ANZAHL' ) );
					$SQL .= " AND SKS_UMSATZ=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendatenViewDWH->FeldInhalt ( 'BRUTTOPREIS' ) );
					$SQL .= " AND SKS_KFZKENNZ=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'KFZ_KENNZEICHEN' ) );
					
					$rsKassendatenAbgl = $this->_DB->RecordSetOeffnen ($SQL);
					
					if ($rsKassendatenAbgl->EOF()) 
					{
						$SQL = "Select * from SGHKASSENDATENRABATT WHERE SKR_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'FILIALNUMMER' ) );
						$SQL .= " AND SKR_DATUM=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'TRANS_DATUM' ) );
						$SQL .= " AND SKR_ZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ZEIT' ) );
						$SQL .= " AND SKR_WANR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'VORGANGSNR' ) );
						$SQL .= " AND SKR_ARTNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ARTIKELNUMMER' ) );
						$SQL .= " AND SKR_MENGE=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'ANZAHL' ) );
						$SQL .= " AND SKR_UMSATZ=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendatenViewDWH->FeldInhalt ( 'BRUTTOPREIS' ) );
						$SQL .= " AND SKR_KFZKENNZ=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'KFZ_KENNZEICHEN' ) );
					
						$rsKassendatenRabatt = $this->_DB->RecordSetOeffnen ($SQL);
					
						if ($rsKassendatenRabatt->EOF()) 
						{
							$SQL = "INSERT INTO SGHKASSENDATEN (SGK_FILID,SGK_DATUM,SGK_ZEIT,";
							$SQL .= "SGK_BSA,SGK_WANR,SGK_ARTNR,SGK_MENGE,SGK_UMSATZ,";
							$SQL .= "SGK_KFZKENNZ,SGK_DWHINSDATE,SGK_DWHAUDIT,SGK_WAGENDAT,SGK_USER,SGK_USERDAT) VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'FILIALNUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'TRANS_DATUM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ZEIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'BSA' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'VORGANGSNR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'ARTIKELNUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'ANZAHL' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendatenViewDWH->FeldInhalt ( 'BRUTTOPREIS' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenViewDWH->FeldInhalt ( 'KFZ_KENNZEICHEN' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'DWHINSDATE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendatenViewDWH->FeldInhalt ( 'DWHAUDIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenViewDWH->FeldInhalt ( 'WA_GEN_DATUM' ), false );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$countInsert ++;
						
							$this->_DB->Ausfuehren($SQL,'',true); 
						}
					}
				}
				
				$rsKassendatenViewDWH->DSWeiter ();
			}

			
			//Nachreinigung
			$SQL  = 'DELETE'; 
			$SQL .= ' FROM SGHKASSENDATEN kd'; 
			$SQL .= ' WHERE EXISTS(';
			$SQL .= 'SELECT 1';
			$SQL .= ' FROM SGHKASSENDATENABGL';
			$SQL .= ' WHERE kd.SGK_FILID = SKS_FILID';
			$SQL .= ' AND kd.SGK_DATUM = SKS_DATUM'; 
			$SQL .= ' AND kd.SGK_ZEIT = SKS_ZEIT'; 
			$SQL .= ' AND kd.SGK_BSA = SKS_BSA';
			$SQL .= ' ) '; 
			$SQL .= 'OR EXISTS(';
			$SQL .= ' SELECT 1';
			$SQL .= ' FROM SGHKASSENDATENRABATT';
			$SQL .= ' WHERE kd.SGK_FILID = SKR_FILID'; 
			$SQL .= ' AND kd.SGK_DATUM = SKR_DATUM';
			$SQL .= ' AND kd.SGK_ZEIT = SKR_ZEIT';
			$SQL .= ' AND kd.SGK_BSA = SKR_BSA';
			$SQL .= ' )';

			$this->_DB->Ausfuehren($SQL,'',true);

			$this->_DB->TransaktionCommit();
		}
		catch (awisException $e)
		{
			echo $e->getMessage(). '/' . $e->getSQL();
			$this->_DB->TransaktionRollback();
		}
		catch (Exception $e)
		{
			echo $e->getMessage() . '/' . $e->getCode();
			$this->_DB->TransaktionRollback();
		}
			
		echo "Ende Import Kassendaten\n\n";
	}

} //Ende Klasse
?>
