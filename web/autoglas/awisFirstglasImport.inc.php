<?php

/******************************************************************************
 *
 *                          IMPORT SCRIPT V1.0
 *                          @author Christian Beierl
 *                       
 *                          @todo Einzelne Files importieren
 *                          LOGS' & Fehlerbehandlung
 *  	
 ******************************************************************************/

require_once('awisDatenbank.inc');
require_once('awisFirstglasFunktionen.php');
require_once('awisBenutzer.inc');

class FirstGlasImport {

    private static $instance = NULL;

    private $_Funktionen;
    protected $pfadFile = '../../../daten/web/firstglas/Daten/Auftrag/';
    protected $pfadGlasKopf = '../../../daten/web/firstglas/Daten/Kopf/';
    protected $lastKassendaten = '';

    protected $pfadKassendaten = '../../../daten/web/firstglas/Daten/Kasse/';
    //protected $KassendatenFile = 'estermann.txt';
    protected $KassendatenFile = '';
    protected $lastKassendatenFile = '';
    protected $unb_Kassendaten = array();
    protected $ber_Kassendaten = array();

    protected $lastFile = '';
    protected $aktFile = array();
    protected $aktFileKassendaten = array();
    private $fikFiles = array();
    protected $aktDate;
    protected $aktFileReadyToImport;
    protected $processingFlag = 0;
    protected $diffDays;
    protected $error_msg;
    protected $pruefen;
    private $_DB;
    private $unb_Kopfsatz = array();
    private $unb_Positionssatz = array();
    private $Anzahl_DS_Positionssatz;
    private $unb_Stornosatz = array();
    private $ber_Kopfsatz = array();
    private $ber_temp_Positionssatz = array();
    private $temp_fehlerPositionssatz = array();
    protected $fehlerPositionssatz = array();
    protected $bol_checkPossatz;
    protected $ber_Positionssatz = array();
    protected $ber_Stornosatz = array();
    protected $aktZeile;
    protected $aktZeilennummer;

    protected $aktKopfnummer;
    protected $aktPosnummer;
    protected $aktStornonummer;
    protected $aktKassennummer;

    protected $posZeilen;
    protected $stornoZeilen;
    private $_VorgangNr;
    protected $_AWISBenutzer;
    protected $checkPossatz;
    protected $XDI_KEY='';
    protected $KassendatenVerarbeitung = 0;

    //Konstruktur
    private function __construct() {
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->openDBConnection();
        $this->_Funktionen = new FirstglasFunktionen();
    }

    public function getInstance() {
        if(self::$instance === NULL) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone() {}


    public function setPfadFile($pfadFile) {

        $this->pfadFile = '';
        $this->pfadFile = $pfadFile;
    }

    public function setXDIKEY($XDIKEY) {
        $this->XDI_KEY = '';
        $this->XDI_KEY = $XDIKEY;
    }

    public function getXDIKEY() {
        return $this->XDI_KEY;
    }


    public function getPfadFile() {
        return $this->pfadFile;
    }

    public function runImport() {

        $this->_Funktionen->SchreibeStartZeit("runImport");
        $this->getAktFile();
        $this->getLastFileImportedDate();

        $this->getAktFileKassendaten();
        $this->checkImportKassenFiles();
        $this->_Funktionen->SchreibeEndeZeit("runImport");
    }


    private function checkImportFiles() {
    //---CHECKEN UND FUNKTION F�R EINZELIMPORT - FILE SCHREIBEN
    //var_dump($this->aktFile);

        for($zaehler = 0;$zaehler<count($this->fikFiles);$zaehler++) {

        //------------------------------------------------------
        //Arrays l�schen----------------------------------------
        //------------------------------------------------------

            array_splice($this->ber_Kopfsatz,0);
            array_splice($this->ber_Positionssatz,0);
            array_splice($this->ber_Stornosatz,0);
            array_splice($this->ber_temp_Positionssatz,0);
            array_splice($this->fehlerPositionssatz,0);
            array_splice($this->temp_fehlerPositionssatz,0);
            array_splice($this->unb_Kopfsatz,0);
            array_splice($this->unb_Positionssatz,0);
            array_splice($this->unb_Stornosatz,0);

            for($element=0;$element<count($this->aktFile);$element++) {
                $this->aktFileReadyToImport = '';
                $cutExtension = '';
                $cutExtension = substr($this->aktFile[$element],0,8);

                if ($cutExtension == $this->fikFiles[$zaehler]) {
                //echo "<br>";
                //echo "***********************************************************";
                    var_dump($this->fikFiles[$zaehler]);
                    //echo "***********************************************************";
                    //echo "<br>";

                    //Aktuelles File holen
                    //----------------------------------------------
                    $this->aktFileReadyToImport = $this->aktFile[$element];

                    //$SQL = "Select max(XDI_KEY),XDI_BEMERKUNG from IMPORTPROTOKOLL WHERE XDI_BEREICH='FGA'";
                    $SQL = "Select * from IMPORTPROTOKOLL WHERE XDI_BEREICH='FGA' ORDER BY XDI_KEY DESC";

                    $rsCheck = $this->_DB->RecordSetOeffnen($SQL);

                    if($rsCheck->FeldInhalt('XDI_BEMERKUNG') != 0) {
                        $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
                        $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FGA',false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->pfadFile.$this->aktFileReadyToImport,false);
                        $SQL .= ',SYSDATE';
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','0',false);
                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                        //echo $SQL;

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }

                        //Selektiere XDI_KEY
                        $SQL =  "Select XDI_KEY,XDI_BEREICH,XDI_DATEINAME,XDI_DATUM ,XDI_MD5,XDI_BEMERKUNG,XDI_USER,XDI_USERDAT ";
                        $SQL .= "FROM IMPORTPROTOKOLL WHERE XDI_BEREICH='FGA' AND XDI_DATUM = (Select MAX(XDI_DATUM) FROM IMPORTPROTOKOLL)";

                        $rsXDI = $this->_DB->RecordSetOeffnen($SQL);
                        //var_dump($rsXDI);
                        //Setze XDI - KEY
                        $this->setXDIKEY($rsXDI->FeldInhalt('XDI_KEY'));

                        $this->readInArray();


                        //Nach Update Setze Status auf OK
                        //------------------------------------------------------------
                        /*
                        $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
                        $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FG',false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->pfadFile.$this->aktFileReadyToImport,false);
                        $SQL .= ',' . SYSDATE;
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','OK',false);
                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';
                        */

                        $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
                        $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FGA',false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->pfadFile.$this->aktFileReadyToImport,false);
                        $SQL .= ',SYSDATE';
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','1',false);
                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                    /*
                    $SQL = 'UPDATE IMPORTPROTOKOLL SET ';
                    $SQL .= ',XDI_BEREICH=' . $this->_DB->FeldInhaltFormat('T',$rsXDI->FeldInhalt("XDI_BEREICH"),false);
                    $SQL .= ',XDI_DATEINAME=' . $this->_DB->FeldInhaltFormat('T',$rsXDI->FeldInhalt("XDI_DATEINAME"),false);
                    $SQL .= ',XDI_DATUM=SYSDATE';
                    $SQL .= ',XDI_BEMERKUNG=' . $this->_DB->FeldInhaltFormat('T',"OK",false);
                    $SQL .= ',XDI_USER=' . $this->_DB->FeldInhaltFormat('T',$rsXDI->FeldInhalt("XDI_USER"),false);
                    $SQL .= ',XDI_USERDAT=SYSDATE';
                    $SQL .= ' WHERE XDI_KEY='. $this->_DB->FeldInhaltFormat('NO',$rsXDI->FeldInhalt("XDI_KEY"),false);
                    */
                        //echo $SQL;

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }

                        //Wenn importiert benenne File um!!!
                        rename($this->pfadFile.$this->aktFileReadyToImport,$this->pfadFile."imp_".$this->aktFileReadyToImport);
                    }
                    else {
                        echo "<br>";
                        echo "--------------------------------------------------";
                        echo "<br>";
                        echo "Prozess l�uft";
                        echo "<br>";
                        echo "--------------------------------------------------";
                        echo "<br>";
                    }
                }
                else {
                //echo "N�chstes Element nicht vorhanden";
                }

            }
        }

    }



    private function checkImportKassenFiles() {
    //---CHECKEN UND FUNKTION F�R EINZELIMPORT - FILE SCHREIBEN
    //var_dump($this->aktFile);

        array_splice($this->ber_Kopfsatz,0);
        array_splice($this->ber_Positionssatz,0);
        array_splice($this->ber_Stornosatz,0);
        array_splice($this->ber_temp_Positionssatz,0);
        array_splice($this->fehlerPositionssatz,0);
        array_splice($this->temp_fehlerPositionssatz,0);
        array_splice($this->unb_Kopfsatz,0);
        array_splice($this->unb_Positionssatz,0);
        array_splice($this->unb_Stornosatz,0);

        for($element=0;$element<count($this->aktFileKassendaten);$element++) {

        //echo count($this->aktFileKassendaten);
        //echo $element;

            $this->KassendatenFile = '';

            array_splice($this->ber_Kopfsatz,0);
            array_splice($this->ber_Positionssatz,0);
            array_splice($this->ber_Stornosatz,0);
            array_splice($this->ber_temp_Positionssatz,0);
            array_splice($this->fehlerPositionssatz,0);
            array_splice($this->temp_fehlerPositionssatz,0);
            array_splice($this->unb_Kopfsatz,0);
            array_splice($this->unb_Positionssatz,0);
            array_splice($this->unb_Stornosatz,0);

            $this->KassendatenFile = $this->aktFileKassendaten[$element];
                /*
                echo "<br>";
                echo $this->KassendatenFile;
                echo "<br>";
                */
            $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
            $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FGK',false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->pfadKassendaten.$this->KassendatenFile,false);
            $SQL .= ',SYSDATE';
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','LAUEFT...',false);
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            $SQL =  "Select XDI_KEY,XDI_BEREICH,XDI_DATEINAME,XDI_DATUM ,XDI_MD5,XDI_BEMERKUNG,XDI_USER,XDI_USERDAT ";
            $SQL .= "FROM IMPORTPROTOKOLL WHERE XDI_BEREICH='FGK' AND XDI_DATUM = (Select MAX(XDI_DATUM) FROM IMPORTPROTOKOLL)";

            //$SQL = 'Select * from IMPORTPROTOKOLL WHERE XDI_BEMERKUNG='. $this->_DB->FeldInhaltFormat('T',$this->pfadKassendaten.$this->KassendatenFile,false);
            //echo $SQL;
            $rsXDI = $this->_DB->RecordSetOeffnen($SQL);
            //var_dump($rsXDI);

            //Setze XDI - KEY
            $this->setXDIKEY($rsXDI->FeldInhalt('XDI_KEY'));

            //---------------------------------------------------------------------
            //TODO: PRUEFE BIS ZU WELCHEN DATUM DIE VORGANGSDATEN IMPORTIERT WURDEN
            //---------------------------------------------------------------------

            $this->ImportKassendaten(false);
            //$this->readInArray();


            $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
            $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FGK',false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->pfadKassendaten.$this->KassendatenFile,false);
            $SQL .= ',SYSDATE';
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','ENDE DES IMPORTES...',false);
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

                /*
                $SQL = 'UPDATE IMPORTPROTOKOLL SET ';
                $SQL .= ' XDI_BEREICH=' . $this->_DB->FeldInhaltFormat('T', $rsXDI->FeldInhalt("XDI_BEREICH"),false);
                $SQL .= ',XDI_DATEINAME=' . $this->_DB->FeldInhaltFormat('T', $rsXDI->FeldInhalt("XDI_DATEINAME"),false);
                $SQL .= ',XDI_DATUM=SYSDATE';
                $SQL .= ',XDI_MD5=' . $this->_DB->FeldInhaltFormat('T', $rsXDI->FeldInhalt("MD5"),false);
                $SQL .= ',XDI_BEMERKUNG=' . $this->_DB->FeldInhaltFormat('T',"OK",false);
                $SQL .= ',XDI_USER=' . $this->_DB->FeldInhaltFormat('T', $rsXDI->FeldInhalt("XDI_USER"),false);
                $SQL .= ',XDI_USERDAT=SYSDATE';
                $SQL .= ' WHERE XDI_KEY='. $this->getXDIKEY();
                */
            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false) {
            }

            //Wenn importiert benenne File um!!!
            rename($this->pfadKassendaten.$this->KassendatenFile,$this->pfadKassendaten."imp_".$this->KassendatenFile);

        }

    }


    private function readInArray() {
            /*
            echo "<br>";
            echo "*************************************************";
            echo "FILE_READY_TO_IMPORT".$this->aktFileReadyToImport;
            echo "*************************************************";
            echo "<br>";
            */

        $this->aktKopfnummer = 0;
        $this->aktPosnummer = 0;
        $this->aktStornonummer = 0;

        if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false) {
            echo $this->error_msg = "Fehler beim �ffnen der Datei";
        }
        else {
        //Anfang Zeilennummer auf 1 setzen
            $this->aktZeilennummer = 1;

            $this->_Funktionen->SchreibeModulStatus("runImport","Start Import - Kassendaten");

            while(! feof($fd)) {
                $this->aktZeile = fgets($fd);

                if(substr($this->aktZeile,0,1) == 'K') {
                    $this->unb_Kopfsatz = explode(';',$this->aktZeile);

                    //Wieviel Kopfdatens�tze sind vorhanden
                    $this->aktKopfnummer++;
                    $this->checkKopfArray();
                }
            }
            fclose($fd);

            $this->_Funktionen->SchreibeModulStatus("runImport","Ende Import - Kassendaten");

            //Lese komplette Positionsdaten und Stornodaten ein!
            //Filezeiger zur�cksetzen

            $fd = null;
            $this->posZeilen =0;


            $this->_Funktionen->SchreibeModulStatus("runImport","Start Import - Positionsdaten");

            if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false) {
                echo $this->error_msg = "Fehler beim �ffnen der Datei";
            }
            else {
            //Anfang Zeilennummer auf 1 setzen
                $this->aktZeilennummer = 1;

                while(! feof($fd)) {
                    $this->aktZeile = fgets($fd);

                    if(substr($this->aktZeile,0,1) == 'P') {
                        $this->unb_Positionssatz[$this->posZeilen] = explode(';',$this->aktZeile);
                        $this->posZeilen++;
                        //Wieviel Positionsdatens�tze sind vorhanden
                        $this->aktPosnummer++;
                    }

                //elseif(substr($this->aktZeile,0,1) == 'S')
                //{
                //$this->unb_Stornsatz[$this->stornoZeilen] = explode(';',$this->aktZeile);
                //var_dump($this->Stornosatz);
                //}
                }
                $this->checkPosArray();
            //var_dump($this->unb_Positionssatz);
            }
            fclose($fd);

            $this->_Funktionen->SchreibeModulStatus("runImport","Ende Import - Positionsdaten");

            $fd = null;

            if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false) {
                echo $this->error_msg = "Fehler beim �ffnen der Datei";
            }
            else {
            //Anfang Zeilennummer auf 1 setzen
            //$this->aktZeilennummer = 1;
                $this->_Funktionen->SchreibeModulStatus("runImport","Start Import - Stornodaten");


                while(! feof($fd)) {
                    $this->aktZeile = fgets($fd);

                    if(substr($this->aktZeile,0,1) == 'S') {
                        $this->unb_Stornosatz = explode(';',$this->aktZeile);

                        //CHECK STORNO - ARRAY
                        $this->checkStornoArray();

                        //Wieviel Stornodatens�tze sind vorhanden
                        $this->aktStornonummer++;
                    }
                }
            }
            fclose($fd);

            $this->_Funktionen->SchreibeModulStatus("runImport","Ende Import - Stornodaten");

            //Ausgabe der importierten Datens�tze
            //Sp�ter in LOG - FILE Auswertung einbauen
            //---------------------------------------------------

            echo "Anzahl Kopfdatens�tze".$this->aktKopfnummer;
            echo "Anzahl Positionsdatens�tze".$this->aktPosnummer;
            echo "Anzhal Stornodatens�tze".$this->aktStornonummer;


    }//Else Zweig
    //var_dump($this->unb_Stornosatz);
    }

    //Kopfdatens�tze �berpr�fen!!!!!
    //------------------------------

    private function checkKopfArray() {
        $fehler = false;
        //Bereinigung KFZ-KENNZEICHEN ARRAY POSITION[4]
        $gewKFZKennz = '';
        $this->pruefen = new FirstglasFunktionen();
        $gewKFZKennz = $this->pruefen->wandleKFZKennzeichen($this->unb_Kopfsatz[4]);
        $this->unb_Kopfsatz[4] = $gewKFZKennz;

        //Ueberpruefen der VorgangsNummer
        $pruefeVorgangsNummer = false;
        $pruefeVorgangsNummer = $this->pruefen->pruefeVorgangNr($this->unb_Kopfsatz[2]);

        //echo "VORGANGSNR".$this->unb_Kopfsatz[2];
        //echo "Status".$pruefeVorgangsNummer;

        if($pruefeVorgangsNummer == true) {

        }
        else {
            $fehler = true;
        //Schreibe falschen Vorgang in die Fehlertabelle
        }

        //Checken SB - Kopfdaten
        $SB = ctype_digit($this->unb_Kopfsatz[9]);
        $SB = $this->pruefen->pruefeSB($this->unb_Kopfsatz[9]);
        $this->unb_Kopfsatz[9] = $SB;


        if($fehler == false) {
            $this->copyToCleanKopfArray();
            //Array bereinigen
            array_splice($this->unb_Kopfsatz,0);
            //Schreibt alle �berpr�ften Kopfs�tze in das Array ber_Kopfsatz
            //var_dump($this->ber_Kopfsatz);

            //Aufruf nach Check - Speichere Kopfsatzdaten
            $this->safeKopfsatz();
        }
        else {
        //Fehler in Tab "FGKOPFDATENFEHLER" aufnehmen
            $ImportFile = $this->getXDIKEY();

            $SQL = "INSERT INTO FGKOPFDATENFEHLER (FKF_KENNUNG,FKF_FILID,FKF_VORGANGNR,";
            $SQL .= "FKF_KFZBEZ,FKF_KFZKENNZ,FKF_KBANR,FKF_FAHRGESTELLNR,FKF_VERSICHERUNG,";
            $SQL .= "FKF_VERSSCHEINNR,FKF_SB,FKF_MONTAGEDATUM,FKF_ZEITSTEMPEL,FKF_STEUERSATZ,";
            $SQL .= "FKF_KUNDENNAME,FKF_IMP_KEY,FKF_USER,FKF_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[0],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->unb_Kopfsatz[1],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[2],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[3],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[4],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[5],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[6],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[7],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[8],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->unb_Kopfsatz[9],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$this->unb_Kopfsatz[10],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->unb_Kopfsatz[11],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[12],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[13],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$ImportFile,false);
            //$SQL .= ',null';
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--

            //echo "<br>";
            //echo "FEHLER";
            //echo $SQL;
            //echo "<br>";

            if($this->_DB->Ausfuehren($SQL)===false) {
            //throw new awisException('',,$SQL,2);
            }

            //Bereinige Array
            //----------------------------------
            array_splice($this->unb_Kopfsatz,0);

            $fehler = false;

        }
    }//------ Ende checkKopfArray


    //Ueberpruefen des StornoArray's
    //------------------------------

    private function checkStornoArray() {
    //Pruefen ob bei den Stornodaten das FELD "SB" korrekt ist
    //-------------------------------------------------------

    //var_dump($this->unb_Stornosatz);

        $SB = ctype_digit($this->unb_Stornosatz[9]);
        $SB = $this->pruefen->pruefeSB($this->unb_Stornosatz[9]);
        $this->unb_Stornosatz[9] = $SB;


        $this->copyToCleanStornoArray();
        //Array bereinigen
        array_splice($this->unb_Stornosatz,0);
        //Schreibt alle �berpr�ften Kopfs�tze in das Array ber_Kopfsatz
        //var_dump($this->unb_Stornosatz);

        //Aufruf nach Check - Speichere Kopfsatzdaten
        //Speicher Stornodatensatz
        $this->safeStornosatz();
    }


    //----------------Wenn ben�tigt wird--------------------------


    private function checkPosArray() {
    //-------Fehler verbleiben in den unbereinigten Array-----
    //-------Schreibe Daten in das bereinigte Array-----------

        $Pruefe_Anzahl = false;
        $Pruef_Array_Anzahl = Array();
        $Pruefe_Alle_Anzahl = false;
        $Pruef_Array_Oempreis = Array();
        $Pruefe_Alle_Oempreis = false;
        $Pruef_Zaehler = 0;

        $pruef_Anzahl = false;
        $pruef_Oempreis = false;

        $Pruefe_Oempreis = false;

        //------CHECKE DS - GRUPPE------
        //------------------------------
        //echo "<br>";
        //var_dump($this->unb_Positionssatz);
        //echo "<br>";

        for($i=0;$i<count($this->unb_Positionssatz);$i++) {
            $Pruefe_Anzahl = false;
            $Pruefe_Oempreis = false;
            $pruef_Oempreis = false;
            $pruef_Anzahl = false;


            //echo 'Variable_i:'.$i;

            for($y=0;$y<count($this->unb_Positionssatz[$i]);$y++) {
                $Pruefe_Anzahl = false;
                $Pruefe_Oempreis = false;
                $pruef_Oempreis = false;
                $pruef_Anzahl = false;

                //var_dump($this->unb_Positionssatz);

                if ($i == 0) {
                //------Erste VorgangsNr auslesen
                    if($y == 2) {
                        $this->_VorgangNr = $this->unb_Positionssatz[$i][1];

                    }
                }

                if ($y == 2) {
                    if($this->unb_Positionssatz[$i][1] == $this->_VorgangNr) {
                    //echo "POSITIONSSATZNR:".$this->unb_Positionssatz[$i][1];
                    //echo "AKTVORGANGNR:".$this->_VorgangNr;
                    //Solange alter Vorgang �berpr�fe diesen ist Gruppe korrekt
                    //Speichere ihn in das bereinigte Array bei Fehlerschreibe in in das Fehlerarray
                    //echo "TESTAUSGABE:  ".$this->unb_Positionssatz[$i][4] = $this->unb_Positionssatz[0][4];
                    //echo "R�CKGABEWERT  :".$Pruefe_Anzahl = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][4]);
                        $Pruefe_Anzahl = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][4]);
                        //echo "<br>";
                        //echo "WERT VON ANZAHL:".$Pruefe_Anzahl;
                        //echo "<br>";
                        //var_dump($this->unb_Positionssatz[$i][4]);
                        //echo "<br>";
                        $Pruef_Array_Anzahl[$Pruef_Zaehler] = $Pruefe_Anzahl;
                        //echo "<br>";
                        //echo "Pr�farray- Anzahl - Ausgabe:";
                        //var_dump($Pruef_Array_Anzahl);

                        //echo "<br>";
                        //echo "----------OEMPREIS------------------------------------------------";
                        //echo "<br>";
                        //echo "R�CKGABEWERT OEMPREIS".$Pruefe_Oempreis = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][6]);
                        $Pruefe_Oempreis = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][6]);
                        //echo "WERT VON OEMPREIS:".$Pruefe_Oempreis;
                        //echo "<br>";
                        //var_dump($this->unb_Positionssatz[$i][6]);
                        //var_dump($Pruefe_Alle_Oempreis);
                        //echo "<br>";
                        $Pruef_Array_Oempreis[$Pruef_Zaehler] = $Pruefe_Oempreis;
                        //echo "<br>";
                        //echo "Pr�farray - OEMPREIS - Ausgabe";
                        //var_dump($Pruef_Array_Oempreis);
                        //echo "<br>";
                        //Zaehle zusammenh�ngenge Datens�tze
                        $Pruef_Zaehler++;
                        //var_dump($Pruef_Array_Anzahl);

                        $index = count($this->unb_Positionssatz);
                        $index = $index - 1;

                        if ($i == $index) {
                        //echo "Letztes Element erreicht";
                            $this->bol_checkPossatz = True;
                            //echo "<br>";
                            //echo "Zaehler:".$Pruef_Zaehler;
                            //echo "<br>";

                            $count = 1;
                            $count_Oempreis = 1;

                            for ($j=0;$j<$Pruef_Zaehler;$j++) {
                            //echo "<br>";
                                $Pruef_Array_Anzahl[$j];
                                //echo "<br>";

                                //$pruef_Anzahl = false;
                                //$Pruef_Array_Oempreis = false;

                                $pruef_Anzahl = $Pruef_Array_Anzahl[$j];
                                $pruef_Oempreis = $Pruef_Array_Oempreis[$j];

                                //echo "<br>";
                                //echo "Pruefwert_Anzahl:".$pruef_Anzahl;
                                //echo "<br>";

                                //echo "<br>";
                                //echo "Pruefwert_OEMPREIS:".$pruef_Oempreis;
                                //echo "<br>";

                                if($pruef_Anzahl == true) {
                                //echo "<br>";
                                //echo "Zaehlerstand:".$count;
                                //echo "<br>";
                                //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count++;

                                    if($count == $Pruef_Zaehler && $this->bol_checkPossatz == True) {
                                        $Pruefe_Alle_Anzahl = true;
                                    //echo "PRUEFE_ALLE".$Pruefe_Alle_Anzahl;
                                    }
                                }
                                else {
                                    $this->bol_checkPossatz = false;
                                }

                                if($pruef_Oempreis == true) {
                                //echo "<br>";
                                //echo "Zaehlerstand:".$count_Oempreis;
                                //echo "<br>";
                                //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count_Oempreis++;

                                    if($count_Oempreis == $Pruef_Zaehler && $this->bol_checkPossatz == True) {
                                        $Pruefe_Alle_Oempreis = true;
                                    //echo "PRUEFE_ALLE_OEMPREIS".$Pruefe_Alle_Oempreis;
                                    }
                                }
                                else {
                                    $this->bol_checkPossatz = false;
                                }
                            }
                            //SCHROTT

                            if ($Pruefe_Alle_Anzahl == true && $Pruefe_Alle_Oempreis == true) {
                            //------------------------------------------
                            //Dann kopiere Datensatzgruppe
                            //------------------------------------------
                            //echo "Wert von I:".$i;

                                $f = ($i - $Pruef_Zaehler);
                                $f++;

                                //echo "WERT VON F:".$f;
                                //echo "<br>";

                                //echo "<br>";
                                array_splice($this->ber_temp_Positionssatz,0);

                                for ($r=0;$r<$Pruef_Zaehler;$r++) {
                                //echo "Wert von f in letzter Schleife".$f;
                                //echo "Wert i".$i;
                                //echo "Wert f".$f;

                                    for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++) {
                                    //bereinigte Gruppe zwischenspeichern
                                    //-----------------------------------
                                    //echo "Wert r".$r;
                                        $this->ber_temp_Positionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                    }

                                    $f++;
                                }

                                //bereinigte Gruppe an Array ber_Positionssatz anf�gen
                                //------------------------------------------------------------------------
                                //echo "<br>";
                                //var_dump($this->ber_temp_Positionssatz);
                                //echo "<br>";
                                $this->copyGruppeToCleanArray();

                                array_splice($this->ber_temp_Positionssatz,0);
                                $Pruefe_Anzahl = false;
                                array_splice($Pruef_Array_Anzahl,0);
                                //var_dump($Pruef_Array_Anzahl);
                                $Pruefe_Alle_Anzahl = false;
                                array_splice($Pruef_Array_Oempreis,0);
                                //var_dump($Pruefe_Alle_Oempreis);
                                $Pruefe_Alle_Oempreis = false;
                                $Pruef_Zaehler = 0;
                                $count=0;
                                $count_Oempreis=0;
                            //------------------------------------------
                            }
                            else {
                            //FehlerDS schreiben

                                for ($r=0;$r<$Pruef_Zaehler;$r++) {
                                    for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++) {

                                        $this->temp_fehlerPositionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                    }
                                    $f++;
                                }
                                $this->copyGruppeFehlerToCleanArray();


                                array_splice($this->temp_fehlerPositionssatz,0);
                                $Pruefe_Anzahl = false;
                                array_splice($Pruef_Array_Anzahl,0);
                                //var_dump($Pruef_Array_Anzahl);
                                $Pruefe_Alle_Anzahl = false;
                                array_splice($Pruef_Array_Oempreis,0);
                                //var_dump($Pruefe_Alle_Oempreis);
                                $Pruefe_Alle_Oempreis = false;
                                $Pruef_Zaehler = 0;
                                $count=0;
                                $count_Oempreis=0;
                            }

                        }
                    }
                    else {
                    //echo "<br>";
                    //echo "Zaehler:".$Pruef_Zaehler;
                    //echo "<br>";
                        $this->bol_checkPossatz = True;

                        $count = 1;
                        $count_Oempreis = 1;

                        for ($j=0;$j<$Pruef_Zaehler;$j++) {
                        //echo "<br>";
                        //echo $Pruef_Array_Anzahl[$j];
                        //echo "<br>";

                            $pruef_Anzahl = false;

                            $pruef_Anzahl = $Pruef_Array_Anzahl[$j];
                            $pruef_Oempreis = $Pruef_Array_Oempreis[$j];

                            //echo "<br>";
                            //echo "Pruefwert_Anzahl:".$pruef_Anzahl;
                            //echo "<br>";

                            //echo "<br>";
                            //echo "Pruefwert_OEMPREIS:".$pruef_Oempreis;
                            //echo "<br>";

                            if($pruef_Anzahl == true) {
                            //echo "<br>";
                            //echo "Zaehlerstand:".$count;
                            //echo "<br>";
                            //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                $count++;

                                if($count == $Pruef_Zaehler && $this->bol_checkPossatz == True) {
                                    $Pruefe_Alle_Anzahl = true;
                                //echo "PRUEFE_ALLE".$Pruefe_Alle_Anzahl;
                                }
                            }
                            else {
                                $this->bol_checkPossatz = false;
                            }

                            if($pruef_Oempreis == true) {
                            //echo "<br>";
                            //echo "Zaehlerstand:".$count_Oempreis;
                            //echo "<br>";
                            //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                $count_Oempreis++;

                                if($count_Oempreis == $Pruef_Zaehler && $this->bol_checkPossatz == True) {
                                    $Pruefe_Alle_Oempreis = true;
                                //echo "PRUEFE_ALLE_OEMPREIS".$Pruefe_Alle_Oempreis;
                                }
                            }
                            else {
                                $this->bol_checkPossatz = false;
                            }

                            if ($Pruefe_Alle_Anzahl == true && $Pruefe_Alle_Oempreis == true && $this->bol_checkPossatz == True) {
                            //echo "<br>";
                            //echo "DS IST RICHTIG";
                            //echo "<br>";
                            //------------------------------------------
                            //Dann kopiere Datensatzgruppe
                            //------------------------------------------

                                $f = ($i - $Pruef_Zaehler);
                                //$f++;
                                    /*
                                    echo "<br>";
                                    echo "Wert von I".$i;
                                    echo "<br>";
                                    echo "<br>";
                                    echo "Wert von F=".$f;
                                    echo "<br>";
                                    */

                                //echo "<br>";
                                for ($r=0;$r<$Pruef_Zaehler;$r++) {
                                //echo "Wert von f".$f;
                                //echo "Wert i".$i;
                                //echo "Wert f".$f;

                                    for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++) {
                                    //bereinigte Gruppe zwischenspeichern
                                    //-----------------------------------
                                    //echo "Wert r".$r;
                                        $this->ber_temp_Positionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                    }

                                    $f++;
                                }

                                $i--;
                                //bereinigte Gruppe an Array ber_Positionssatz anf�gen
                                //------------------------------------------------------------------------
                                //echo "<br>";
                                //var_dump($this->ber_temp_Positionssatz);
                                //echo "<br>";
                                $this->copyGruppeToCleanArray();
                                array_splice($this->ber_temp_Positionssatz,0);
                                $Pruefe_Anzahl = false;
                                array_splice($Pruef_Array_Anzahl,0);
                                //var_dump($Pruef_Array_Anzahl);
                                $Pruefe_Alle_Anzahl = false;
                                array_splice($Pruef_Array_Oempreis,0);
                                //var_dump($Pruefe_Alle_Oempreis);
                                $Pruefe_Alle_Oempreis = false;
                                $Pruef_Zaehler = 0;
                                $count=0;
                                $count_Oempreis=0;
                            //var_dump($this->unb_Positionssatz);
                            }
                            else {
                                $control = $j;
                                $control++;

                                if($control == $Pruef_Zaehler) {

                                    if ($this->bol_checkPossatz == false) {
                                    //echo "<br>";
                                    //echo "Pruefzaehler:".$Pruef_Zaehler;
                                    //echo "<br>";
                                    //Setze Zaehler um pos -1 zur�ck
                                        $i--;

                                        //Fehlerhafte Positionss�tze

                                        $f = ($i - $Pruef_Zaehler);
                                        $f++;

                                        //echo "WERT VON F:".$f;
                                        //echo "<br>";

                                        //echo "<br>";
                                        array_splice($this->temp_fehlerPositionssatz,0);

                                        for ($r=0;$r<$Pruef_Zaehler;$r++) {
                                        //echo "Wert von f in letzter Schleife".$f;
                                        //echo "Wert i".$i;
                                        //echo "Wert f".$f;

                                            for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++) {
                                            //bereinigte Gruppe zwischenspeichern
                                            //-----------------------------------
                                            //echo "Wert r".$r;
                                                $this->temp_fehlerPositionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                            }

                                            $f++;
                                        }
                                        $this->copyGruppeFehlerToCleanArray();
                                          /*
                                          echo "WERT VON I:".$i;
                                          echo $this->unb_Positionssatz[$i][0];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][1];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][2];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][3];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][4];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][5];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][6];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][7];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][8];

                                          echo "<br>";
                                          echo "Pruefzaehler-ENDE:".$Pruef_Zaehler;
                                          echo "<br>";
                                          */
                                        //L�sche Array's
                                        //---------------------------------------------
                                        array_splice($this->temp_fehlerPositionssatz,0);
                                        $Pruefe_Anzahl = false;
                                        array_splice($Pruef_Array_Anzahl,0);
                                        //var_dump($Pruef_Array_Anzahl);
                                        $Pruefe_Alle_Anzahl = false;
                                        array_splice($Pruef_Array_Oempreis,0);
                                        //var_dump($Pruefe_Alle_Oempreis);
                                        $Pruefe_Alle_Oempreis = false;
                                        $Pruef_Zaehler = 0;
                                        $count=0;
                                        $count_Oempreis=0;

                                    //echo "DS FALSCH ABER SOWAS VON FALSCH";
                                    }
                                //array_splice($Pruef_Array_Anzahl,0);
                                //array_splice($Pruef_Array_Oempreis,0);
                                }

                            //---Schreibe falsche DS in FEHLERTABELLE
                            //------------------------------------------


                            }
                        }
                        $i++;
                        $this->_VorgangNr = $this->unb_Positionssatz[$i][1];
                        $i--;

                    //echo "<br>";
                    //echo "Neuer DS";
                    //echo "<br>";
                    }
                }
            }

        }

        //Wenn fertig ->SafePossatz
        $this->safePossatz();
        //Wenn fertig ->Fehler wegspeichern
        $this->safeFehlerPositionssatz();
    }

    private function copyGruppeToCleanArray() {
    //Letzte Stelle ermitteln
    //echo "CHECKCHECK_---------------------------------------------------------------------------------";
    //var_dump($this->ber_temp_Positionssatz);
    //echo "CHECKCHECK_---------------------------------------------------------------------------------";

        for ($i=0;$i<count($this->ber_temp_Positionssatz);$i++) {
            $r = count($this->ber_Positionssatz);

            for($y=0;$y<count($this->ber_temp_Positionssatz[$i]);$y++) {
                $this->ber_Positionssatz[$r][$y] = $this->ber_temp_Positionssatz[$i][$y];

            }
        }
    //var_dump($this->ber_Positionssatz);
    }



    public function copyGruppeFehlerToCleanArray() {
    //Letzte Stelle ermitteln
    //echo "CHECKCHECK_---------------------------------------------------------------------------------";
    //var_dump($this->ber_temp_Positionssatz);
    //echo "CHECKCHECK_---------------------------------------------------------------------------------";

        for ($i=0;$i<count($this->temp_fehlerPositionssatz);$i++) {
            $r = count($this->fehlerPositionssatz);

            for($y=0;$y<count($this->temp_fehlerPositionssatz[$i]);$y++) {
                $this->fehlerPositionssatz[$r][$y] = $this->temp_fehlerPositionssatz[$i][$y];
            }
        }
    //var_dump($this->fehlerPositionssatz);
    }

    public function showPosFehlerArray() {
        echo "FEHLERARRAY*******************************************************<br>";
        var_dump($this->fehlerPositionssatz);
        echo "FEHLERARRAY-------------------------------------------------------<br>";
    }

    public function showCleanPosArray() {
        echo "BEREINIGTES ARRAY*************************************************************<br>";
        var_dump($this->ber_Positionssatz);
        echo "ENDE BEREINIGTES ARRAY********************************************************<br>";
    }


    private function copyToCleanKopfArray() {
        for($element=0;$element<count($this->unb_Kopfsatz);$element++) {
            $this->ber_Kopfsatz[$element] = $this->unb_Kopfsatz[$element];
        }
    //var_dump($this->ber_Kopfsatz);
    }

    private function copyToCleanStornoArray() {
        for($element=0;$element<count($this->unb_Stornosatz);$element++) {
            $this->ber_Stornosatz[$element] = $this->unb_Stornosatz[$element];
        }
    //var_dump($this->unb_Stornsatz);
    }




    private function safeStornosatz() {
    //Ueberpr�fe ob bereits eine gleiche Vorgangsnummer vorhanden ist!
    //Get Aktuellen IMP - KEY der gerade Importiert wurde
        $ImportFile = $this->getXDIKEY();

        $SQL =  "Select * from FGSTORNODATEN";
        $SQL .= " WHERE FGS_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
        //echo $SQL;
    
        $rsStornoDaten = $this->_DB->RecordSetOeffnen($SQL);
        if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == '') {

            $SQL = "INSERT INTO FGSTORNODATEN (FGS_KENNUNG,FGS_FILIALNR,FGS_VORGANGNR,";
            $SQL .= "FGS_KFZBEZ,FGS_KFZKENNZ,FGS_KBANR,FGS_FAHRGESTELLNR,FGS_VERSICHERUNG,";
            $SQL .= "FGS_VERSSCHEINNR,FGS_SB,FGS_MONTAGEDATUM,FGS_ZEITSTEMPEL,FGS_STEUERSATZ,";
            $SQL .= "FGS_AKTIV,FGS_FGK_KEY,FGS_IMP_KEY,FGS_USER,";
            $SQL .= "FGS_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[0],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[1],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[3],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[4],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[5],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[6],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[7],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[8],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->ber_Stornosatz[9],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[10],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[11],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[12],false);
            //$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[13],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',1,false);
            $SQL .= ',null';
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$ImportFile,false);
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--
            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false) {
            //throw new awisException('',,$SQL,2);
            }

            //echo "<br>";
            //echo "Kein Vorgang gefunden";
            array_splice($this->ber_Stornosatz,0);
        //echo "<br>";
        //var_dump($this->ber_Stornosatz);

        }
        else {
            $ImportFile = $this->getXDIKEY();

            if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == $this->ber_Stornosatz[2]) {
            //echo "<br>";
            //echo "VORGANGSNR stimmt �berein";
            //echo "<br>";

            //Zeitstempel wandeln wegen der �berpr�fung des Datum's
            //------------------------------------------------------------------

                $wand_Datum = strtotime($this->ber_Stornosatz[11]);
                $wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

                if($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL') == $wand_Datum) {
                //Wenn �bereinstimmung
                //echo "<br>";
                //echo "Zeitstempel stimmt �berein - kein UPDATE";
                //echo "<br>";
                }
                else {
                    if($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL') <= $this->ber_Stornosatz[11]) {
                    //Alte Daten in HISTORY - Tabelle verschieben
                    //-------------------------------------------
                        $SQL = 'INSERT INTO FGSTORNODATEN_HIST SELECT * FROM FGSTORNODATEN WHERE';
                        $SQL .= ' FGS_VORGANGNR='.$this->ber_Stornosatz[2];
                        $SQL .= 'AND FGS_KEY='.$rsStornoDaten->FeldInhalt('FGS_KEY');

                        //echo $SQL;

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        //throw new awisException('',,$SQL,2);
                        }

                        //--------------------�NDERN--------------------------------
                        //----------------------------------------------------------
                        //---------------UPDATE Kopfdatensatz -- DS-----------------
                        //----------------------------------------------------------
                        $SQL =  "Select * from FGSTORNODATEN";
                        $SQL .= " WHERE FGS_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);

                        $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

                        //var_dump($rsKopfDatenUpdate);

                        $SQL = 'UPDATE FGSTORNODATEN SET ';
                        $SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[0],false);
                        $SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[1],false);
                        $SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
                        $SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[3],false);
                        $SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[4],false);
                        $SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[5],false);
                        $SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[6],false);
                        $SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[7],false);
                        $SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[8],false);
                        $SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[9],false);
                        $SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[10],false);
                        $SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[11],false);
                        $SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[12],false);
                        $SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[13],false);
                        $SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGS_FGK_KEY'),true);
                        $SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$ImportFile,true);
                        $SQL .= ',FGS_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                        $SQL .= ',FGS_USERDAT=sysdate';
                        $SQL .= ' WHERE FGS_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        //throw new awisException('',,$SQL,2);
                        }

                    //Selektiere Positionsdaten -> POS_HISTORY
                    //echo "<br>";
                    //echo "Zeitstempel unterscheidet sich - Update notwendig";
                    //echo "<br>";
                    }
                }
            }

        }
    }


    private function safeKopfsatz() {
    //Ueberpr�fe ob bereits eine gleiche Vorgangsnummer vorhanden ist!

        $ImportFile = $this->getXDIKEY();

        $SQL =  "Select * from FGKOPFDATEN";
        $SQL .= " WHERE FGK_VORGANGNR=".$this->ber_Kopfsatz[2];

        $rsKopfDaten = $this->_DB->RecordSetOeffnen($SQL);
        if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == '') {
            $SQL = "INSERT INTO FGKOPFDATEN (FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
            $SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
            $SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
            $SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_FGN_KEY,FGK_FCN_KEY,FGK_USER,FGK_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[0],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kopfsatz[1],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[3],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[4],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[5],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[6],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[7],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[8],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->ber_Kopfsatz[9],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$this->ber_Kopfsatz[10],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[12],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[13],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$ImportFile,false);
            //$SQL .= ',null';
            $SQL .= ',null';
            $SQL .= ',null';
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--
            if($this->_DB->Ausfuehren($SQL)===false) {
            //throw new awisException('',,$SQL,2);
            }

            //echo "<br>";
            //echo "Kein Vorgang gefunden";
            array_splice($this->ber_Kopfsatz,0);
        //echo "<br>";
        //var_dump($this->ber_Kopfsatz);

        }
        else {
            if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == $this->ber_Kopfsatz[2]) {
            //echo "<br>";
            //echo "VORGANGSNR stimmt �berein";
            //echo "<br>";

            //Zeitstempel wandeln wegen der �berpr�fung des Datum's
            //------------------------------------------------------------------

                $wand_Datum = strtotime($this->ber_Kopfsatz[11]);
                $wand_Datum = date('d.m.Y H:i:s',$wand_Datum);


                if($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL') == $wand_Datum) {
                //Wenn �bereinstimmung
                //echo "<br>";
                //echo "Zeitstempel stimmt �berein - kein UPDATE";
                //echo "<br>";
                }
                else {
                    if($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL') < $wand_Datum) {
                    //echo "DS wird ge�ndert";
                    //Alte Daten in HISTORY - Tabelle verschieben
                    //-------------------------------------------
                    //echo $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);
                        $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);


                        //KOPF
                        //echo "<br>";
                        //echo "KOPFDATENSATZ";
                        $this->ber_Kopfsatz[2];
                        //echo "<br>";

                        //$SQLAKTKOPFCHECK = 'Select * from FGKOPFDATEN WHERE FGK_VORGANGNR='.$this->ber_Kopfsatz[1].' AND FGK_ZEITSTEMPEL='.$this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);

                        //echo $SQLAKTKOPFCHECK;

                        //$rsCheckAktKopfsatz = $this->_DB->RecordSetOeffnen($SQLAKTKOPFCHECK);

                        //if ($rsCheckAktKopfsatz->AnzahlDatensaetze() == 0)
                        //{

                        $SQLCHECKHISTORY = 'Select * from FGKOPFDATEN_HIST WHERE FKH_ZEITSTEMPEL='.$this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);

                        //echo $SQLCHECKHISTORY;

                        $rsCheck_History = $this->_DB->RecordSetOeffnen($SQLCHECKHISTORY);

                        if($rsCheck_History->AnzahlDatensaetze() == 0) {
                            $SQL = 'INSERT INTO FGKOPFDATEN_HIST SELECT * FROM FGKOPFDATEN WHERE ';
                            $SQL .= 'FGK_VORGANGNR='.$this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);
                            //$SQL .= 'AND FGK_KEY='.$rsKopf->FeldInhalt('FGK_KEY');

                            if($this->_DB->Ausfuehren($SQL)===false) {
                            //throw new awisException('',,$SQL,2);
                            }
                        }
                        //}


                        ////Ende CheckAktKopfsatz
                        //--------------------�NDERN--------------------------------
                        //----------------------------------------------------------
                        //---------------UPDATE Kopfdatensatz -- DS-----------------
                        //----------------------------------------------------------
                        $ImportFile = $this->getXDIKEY();

                        $SQL =  "Select * from FGKOPFDATEN";
                        $SQL .= " WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);

                        $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

                        //var_dump($rsKopfDatenUpdate);

                        $SQL = 'UPDATE FGKOPFDATEN SET ';
                        $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[0],false);
                        $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kopfsatz[1],false);
                        $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);
                        $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[3],false);
                        $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[4],false);
                        $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[5],false);
                        $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[6],false);
                        $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[7],false);
                        $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[8],false);
                        $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N',$this->ber_Kopfsatz[9],false);
                        $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('D',$this->ber_Kopfsatz[10],false);
                        $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);
                        $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[12],false);
                        $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[13],false);
                        $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$ImportFile,true);
                        $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                        $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_FGN_KEY'),true);
                        $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_FCN_KEY'),true);
                        $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatenUpdate->FeldInhalt('FGK_FREIGABEGRUND'),true);
                        $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('D',$rsKopfDatenUpdate->FeldInhalt('FGK_DATUMZUGANG'),true);
                        $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                        $SQL .= ',FGK_USERDAT=sysdate';
                        $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);

                        //echo $SQL;

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        //throw new awisException('',,$SQL,2);
                        }

                    //Selektiere Positionsdaten -> POS_HISTORY
                    //echo "<br>";
                    //echo "Zeitstempel unterscheidet sich - Update notwendig";
                    //echo "<br>";
                    }
                }
            }

        }
    }

    //TODO: MONTAG
    //----------------------------------------------------------------------------
    //--------------------Positionsdatens�tze anpassen
    //----------------------------------------------------------------------------

    public function safePossatz() {
    //Sp�ter - selektiere nur die DS die noch keinen Status gesetzt haben
    //var_dump($rsKopf);
    //Selektiere alle KOPFDATEN
        $ImportFile = $this->getXDIKEY();

        $SQL = "Select * from FGKOPFDATEN WHERE FGK_FGN_KEY IS NULL";

        //Selektiere History Datensatz
        $rsKopf = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsKopf->EOF()) {
            $Vorgang_NR = $rsKopf->FeldInhalt('FGK_VORGANGNR');
            $KOPF_ID = $rsKopf->FeldInhalt('FGK_KEY');
            $DateTime =  $rsKopf->FeldInhalt('FGK_ZEITSTEMPEL');
            $DateTime = strtotime($DateTime);

            //echo '<br>KOPF:'.$KOPF_ID;
            //echo '<br>AnzPosSatz:'.count($this->ber_Positionssatz);


            for ($y=0;$y<count($this->ber_Positionssatz);$y++) {
            //echo '<br>y:'.$y;
            //echo '<br>PosSatz:'.$this->ber_Positionssatz[$y][1];

                if($this->ber_Positionssatz[$y][1] == $Vorgang_NR) {
                    $SQL = "Select * from FGPOSITIONSDATEN WHERE FGP_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][1],false);
                    $rsPos = $this->_DB->RecordSetOeffnen($SQL);



                    if($rsPos->EOF()) {
                    //echo '<br>Keine Daten gefunden';
                    }
                    //echo '<br>rsPOS'.$rsPos->FeldInhalt('FGP_KEY');
                    //echo '<br>Anz'.$rsPos->AnzahlDatensaetze();
                    //-----------------------------------------------------
                    //----------Wurde kein Datensatz gefunden
                    //-----------------------------------------------------
                    if($rsPos->FeldInhalt('FGP_VORGANGNR') == '') {
                        for($h=0;$h<count($this->ber_Positionssatz);$h++) {
                            $wand_Datum = strtotime($this->ber_Positionssatz[$h][8]);

                            if($this->ber_Positionssatz[$h][1] == $Vorgang_NR && $wand_Datum == $DateTime) {
                            //Check ob DS schon vorhanden ist
                                $SQL = 'Select * from FGPOSITIONSDATEN WHERE FGP_VORGANGNR='.$this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][1],false);
                                $SQL .= ' AND FGP_ARTNR='.$this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][2],false);
                                $SQL .= ' AND FGP_ANZAHL='. $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][4],false);
                                $SQL .= ' AND FGP_OEMPREIS='. $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][6],false);
                                $SQL .= ' AND FGP_ZEITSTEMPEL='. $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$h][8],false);

                                //echo $SQL;

                                $rsCheck = $this->_DB->RecordSetOeffnen($SQL);

                                if ($rsCheck->AnzahlDatensaetze() == 0) {
                                    $SQL = "INSERT INTO FGPOSITIONSDATEN (FGP_KENNUNG,FGP_VORGANGNR,";
                                    $SQL .= "FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,FGP_OEMPREIS,";
                                    $SQL .= "FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,";
                                    $SQL .= "FGP_USER,FGP_USERDAT) VALUES (";
                                    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][0],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][1],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][2],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][3],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][4],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][5],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][6],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][7],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$h][8],false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',0,false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopf->FeldInhalt('FGK_KEY'),false);
                                    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                    $SQL .= ',SYSDATE';
                                    $SQL .= ')';

                                    if($this->_DB->Ausfuehren($SQL)===false) {
                                    }
                                }

                            }
                            else {
                            //Checke ob History - Daten vorhanden sind
                            //Datum wandeln

                                $SQL = 'Select * from FGKOPFDATEN_HIST WHERE FKH_VORGANGNR='.$this->ber_Positionssatz[$h][1].' AND FKH_ZEITSTEMPEL='.$this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$h][8],false);

                                //echo "HISTORISIERUNG POSITIONSSAETZE".$SQL;

                                $rsHistory = $this->_DB->RecordSetOeffnen($SQL);

                                if ($rsHistory->AnzahlDatensaetze()>0) {
                                    $DateTime_Hist = $rsHistory->FeldInhalt('FKH_ZEITSTEMPEL');
                                    $DateTime_Hist = strtotime($DateTime_Hist);
                                    $wand_Datum_Hist = strtotime($this->ber_Positionssatz[$h][8]);

                                    if($this->ber_Positionssatz[$h][1] == $Vorgang_NR && $wand_Datum_Hist == $DateTime_Hist) {
                                        $SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
                                        $SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
                                        $SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT) VALUES(';
                                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][0],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][1],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][2],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][3],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][4],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][5],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][6],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][7],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$h][8],false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',0,false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsHistory->FeldInhalt('FKH_KEY'),false);
                                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                        $SQL .= ',SYSDATE';
                                        $SQL .= ')';

                                        if($this->_DB->Ausfuehren($SQL)===false) {
                                        }

                                    }

                                }
                            }
                        }
                        break;
                    }
                    else {
                        while(!$rsPos->EOF()) {
                            if(($rsPos->FeldInhalt('FGP_VORGANGNR') == $this->ber_Positionssatz[$y][1]) AND ($rsPos->FeldInhalt('FGP_ARTNR') == $this->ber_Positionssatz[$y][2])) {
                                $wand_Datum = awisFormular::PruefeDatum($this->ber_Positionssatz[$y][8],1,1,1);
                                $wand_Datum = strtotime($this->ber_Positionssatz[$y][8]);
                                //$wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

                                if(($rsPos->FeldInhalt('FGP_VORGANGNR') == $this->ber_Positionssatz[$y][1]) AND ($rsPos->FeldInhalt('FGP_ARTNR') == $this->ber_Positionssatz[$y][2])) {
                                //echo "VORGANGNR STIMMT �BEREIN----------------------------------------";
                                //echo "<br>";
                                    /*
                                    echo "<br>Zeitstempel:".$rsPos->FeldInhalt('FGP_ZEITSTEMPEL');
                                    echo "<br>Zeitstempel:".awisFormular::PruefeDatum($rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),1,1,1);
                                    echo "<br>ZETI:".$wand_Datum;
                                    echo "<br>ZETI:".$this->ber_Positionssatz[$y][8];
                                    echo "<br>";
                                    */
                                    if(awisFormular::PruefeDatum($rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),1,1,1) < $wand_Datum) {
                                    //echo "******************************************************************************";
                                    //echo "UPDATE************************************************************************";
                                    //echo "******************************************************************************";

                                    //----------------------------------------------
                                    //Verschiebe DS in die History Tabelle
                                    //----------------------------------------------
                                     /*
                                     echo "<br>";
                                     echo "VORGANGSNR".$rsPos->FeldInhalt('FGP_VORGANGNR');
                                     echo "<br>";
                                     */
                                        $SQL = 'Select * from FGKOPFDATEN_HIST WHERE FKH_VORGANGNR='.$this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_VORGANGNR'),false). 'ORDER BY FKH_KEY';

                                        $rsKopfHist = $this->_DB->RecordSetOeffnen($SQL);

                                        $anzahlDS = $this->_DB->ErmittleZeilenAnzahl($SQL);
                                        if($anzahlDS > 1) {
                                        //Gehe zu letzten DS
                                        //-----------------------
                                            $rsKopfHist->DSLetzter();
                                            $IDHistorisierung = $rsKopfHist->FeldInhalt('FKH_KEY');
                                        /*
                                        echo "<br>";
                                        echo "ID HISTORISIERUNG";
                                        echo $IDHistorisierung;
                                        echo "<br>";
                                        */
                                        }
                                        else {
                                        //Liefere ID zur�ck

                                            if($anzahlDS == 1) {
                                                $IDHistorisierung = $rsKopfHist->FeldInhalt('FKH_KEY');
                                            /*
                                            echo "<br>";
                                            echo "ID HISTORISIERUNG";
                                            echo $IDHistorisierung;
                                            echo "<br>";
                                            */
                                            }

                                        }

                                        //Ermittle h�chsten Zeitstempel


                                        //Selektiere die dazugeh�rigen Positionss�tze
                                        //$SQL = 'Select * from FGPOSITIONSDATEN WHERE FGP_VORGANGNR='.$rsPos->FeldInhalt('FGP_VORGANGNR');
                                        //$rsPosHist = $this->_DB->RecordSetOeffnen($SQL);

                                        $SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
                                        $SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
                                        $SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT) VALUES(';
                                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_KENNUNG'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_VORGANGNR'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsPos->FeldInhalt('FGP_ARTNR'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_ARTBEZ'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_ANZAHL'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_EINHEIT'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_OEMPREIS'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$rsPos->FeldInhalt('FGP_EKNETTO'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',0,false);
                                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopfHist->FeldInhalt('FKH_KEY'),false);
                                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                        $SQL .= ',SYSDATE';
                                        $SQL .= ')';

                                        //echo $SQL;

                                        //var_dump($rsPos);

                                        //$SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST SELECT * FROM FGPOSITIONSDATEN WHERE';
                                        //$SQL .= ' FGP_VORGANGNR='.$rsPos->FeldInhalt('FGP_VORGANGNR');
                                        //$SQL .= ' AND FGP_KEY = ' . $rsPos->FeldInhalt('FGP_KEY');

                                        //echo $SQL;

                                        if($this->_DB->Ausfuehren($SQL)===false) {
                                        }
                                        //----------------------------------------------
                                        //F�hre Update aus
                                        //----------------------------------------------
                                        $SQL = 'UPDATE FGPOSITIONSDATEN SET ';
                                        $SQL .= ' FGP_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][0],false);
                                        $SQL .= ',FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][1],false);
                                        $SQL .= ',FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$y][2],false);
                                        $SQL .= ',FGP_ARTBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][3],false);
                                        $SQL .= ',FGP_ANZAHL=' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$y][4],false);
                                        $SQL .= ',FGP_EINHEIT=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][5],false);
                                        $SQL .= ',FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$y][6],false);
                                        $SQL .= ',FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$y][7],false);
                                        $SQL .= ',FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$y][8],false);
                                        $SQL .= ',FGP_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat('N2','0',false);
                                        $SQL .= ',FGP_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsPos->FeldInhalt('FGP_FGK_KEY'),false);
                                        $SQL .= ',FGP_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                                        $SQL .= ',FGP_USERDAT=sysdate';
                                        $SQL .= ' WHERE FGP_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_VORGANGNR'),false);
                                        $SQL .= ' AND FGP_ARTNR='.$this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][2],false);
                                        if($this->_DB->Ausfuehren($SQL)===false) {
                                        }
                                    }
                                    else {
                                    //echo '<br>   N�chster Datensatz';
                                        $rsPos->DSWeiter();
                                    //echo '<br>DS-Nr:'.$rsPos->DSNummer();
                                    }
                                }

                            }

                            if(!$rsPos->EOF()) {
                                $rsPos->DSWeiter();
                            }
                        //echo '<br>DS-Nr:'.$rsPos->DSNummer();
                    }//Ende While

                }//ENDE ELSE
                }
                else {
                //echo '<br>Andere Vorgangsnummer';
                }
            }//Ende FOR
            //echo '<br><hr>N�chster Kopffsatz';
            $rsKopf->DSWeiter();
        }

    //Wenn gleiche DS mit VorgangsNr hintereinander kommen









    } //Ende der Funktion


    public function safeFehlerPositionssatz() {
    //Hole alle Positionsdatens�tze f�r diese Nummer
    //Durchlaufe alle DS im Fehlerarray
        for($i=0;$i<count($this->fehlerPositionssatz);$i++) {
            /*
            echo "<br>";
            echo "Wert von I:".$i;
            echo "<br>";
            */
        //echo $VorgangNr = $this->fehlerPositionssatz[$i][1];
            $VorgangNr = $this->fehlerPositionssatz[$i][1];
            //echo "<br>";
            //Aktuelle VorgangsNr
            //$VorgangNr = $this->fehlerPositionssatz[$i][1];
            $SQL = "Select * from FGPOSITIONSDATENFEHLER WHERE FPF_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);
            $rsFehlerPos = $this->_DB->RecordSetOeffnen($SQL);

            //$rsFehlerPos->FeldInhalt('FPF_VORGANGNR');
            if ($rsFehlerPos->FeldInhalt('FPF_VORGANGNR') == '') {
                for ($h=$i;$h<count($this->fehlerPositionssatz);$h++) {
                            /*
                            echo "<br>";
                            echo "Variable H hat den Wert";
                            echo $h;
                            echo "<br>";

                            echo "<br>";
                            echo $VorgangNr;
                            echo "<br>";
                            echo $this->fehlerPositionssatz[$h][1];
                            echo "<br>";
                            */

                    if($VorgangNr == $this->fehlerPositionssatz[$h][1]) {
                        $SQL = "INSERT INTO FGPOSITIONSDATENFEHLER (FPF_KENNUNG,FPF_VORGANGNR,";
                        $SQL .= "FPF_ARTNR,FPF_ARTBEZ,FPF_ANZAHL,FPF_EINHEIT,FPF_OEMPREIS,";
                        $SQL .= "FPF_EKNETTO,FPF_ZEITSTEMPEL,";
                        $SQL .= "FPF_USER,FPF_USERDAT) VALUES (";
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$h][0],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$h][1],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->fehlerPositionssatz[$h][2],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$h][3],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->fehlerPositionssatz[$h][4],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$h][5],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->fehlerPositionssatz[$h][6],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->fehlerPositionssatz[$h][7],false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->fehlerPositionssatz[$h][8],false);
                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }


                        //Schreibe dazugeh�rigen Kopfdatensatz weg
                        //Checke ob Kopfdatensatz in Tabelle FGKopfdatenfehler vorhanden ist
                        $SQL = "Select * from FGKOPFDATENFEHLER WHERE FKF_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);
                        $rsFehlerKopf = $this->_DB->RecordSetOeffnen($SQL);

                        if ($rsFehlerKopf->FeldInhalt('FKF_VORGANGNR') == '') {
                            $SQL = "INSERT INTO FGKOPFDATENFEHLER (FKF_KENNUNG,FKF_FILID,FKF_VORGANGNR,";
                            $SQL .= "FKF_KFZBEZ,FKF_KFZKENNZ,FKF_KBANR,FKF_FAHRGESTELLNR,FKF_VERSICHERUNG,";
                            $SQL .= "FKF_VERSSCHEINNR,FKF_SB,FKF_MONTAGEDATUM,FKF_ZEITSTEMPEL,FKF_STEUERSATZ,";
                            $SQL .= "FKF_KUNDENNAME,FKF_IMP_KEY,FKF_USER,FKF_USERDAT) Select ";
                            $SQL .= "FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
                            $SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
                            $SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
                            $SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_USER,FGK_USERDAT from FGKOPFDATEN";
                            $SQL .= " WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);

                            if($this->_DB->Ausfuehren($SQL)===false) {
                            }
                            //L�sche Datensatz
                            //L�sche Kopfdatensatz aus der TABELLE FGKOPFDATEN
                            $SQL = "Delete from FGKOPFDATEN WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);

                            //echo $SQL;

                            if($this->_DB->Ausfuehren($SQL)===false) {
                            }
                        }
                        else {
                        //Update DS und L�sche DS aus der Tabelle FGKOPFDATEN
                        //---------------------------------------------------
                            $SQL = "Delete from FGKOPFDATEN WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);

                            if($this->_DB->Ausfuehren($SQL)===false) {
                            }





                        }
                    //Verlasse Schleife und gehe zum n�chsten Vorgang
                    }
                    else {
                        break;
                    }


            }//Ende Vorschleife
            //break;

            }
            else {
            //-----------------------------------------------
            //Pr�fe Kopfdatens�tze
            //-----------------------------------------------
                for($z=0;$z<count($this->fehlerPositionssatz);$z++) {
                    $SQL = "Select * from FGKOPFDATENFEHLER WHERE FKF_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$z][1],false);
                    $rsFehlerKopf = $this->_DB->RecordSetOeffnen($SQL);

                    if ($rsFehlerKopf->FeldInhalt('FKF_VORGANGNR') == $this->fehlerPositionssatz[$z][1]) {
                        $SQL = "Delete from FGKOPFDATEN WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->fehlerPositionssatz[$i][1],false);

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }
                    }
                    else {

                    }

                }
            }
        } //Ende FOR


    } //Ende Funktion


    public function ImportKassendaten($flag) {
        $count = 0;

        $count++;

        $this->pruefen = new FirstglasFunktionen();

        if($flag == false) {
            echo "<br>";
            echo $this->pfadKassendaten.$this->KassendatenFile;
            echo "<br>";

            if(($fd = fopen($this->pfadKassendaten.$this->KassendatenFile,'r')) === false) {
                echo $this->error_msg = "Fehler beim �ffnen der Datei";
            }
            else {
                $this->_Funktionen->SchreibeModulStatus("runImport","Start Import - Kassendaten");

                //Anfang Zeilennummer auf 1 setzen

                while(! feof($fd)) {
                    array_splice($this->ber_Kassendaten,0);

                    $this->aktZeile = fgets($fd);
                    //echo $this->aktZeile;

                    if(substr($this->aktZeile,0,1) == 'D') {
                    //Kennung
                        $this->ber_Kassendaten[0] = substr($this->aktZeile,0,1);
                        //FilialNr
                        $this->ber_Kassendaten[1] = substr($this->aktZeile,1,3);
                        //Datum
                        $this->ber_Kassendaten[2] = substr($this->aktZeile,4,8);
                        //Uhrzeit
                        $this->ber_Kassendaten[3] = substr($this->aktZeile,12,8);
                        //BSA
                        $this->ber_Kassendaten[4] = substr($this->aktZeile,20,2);
                        //WANR
                        $this->ber_Kassendaten[5] = substr($this->aktZeile,23,6);
                        //VorgangsNr
                        $this->ber_Kassendaten[6] = substr($this->aktZeile,30,13);
                        //ARTNR
                        $this->ber_Kassendaten[7] = substr($this->aktZeile,44,6);
                        //Menge
                        $this->ber_Kassendaten[8] = substr($this->aktZeile,53,2);
                        //Betrag
                        $this->ber_Kassendaten[9] = substr($this->aktZeile,57,10);
                        //KFZKENNZ

                        $unbKFZKennzeichen = '';
                        $gewKFZKennz = '';

                        $this->ber_Kassendaten[10] = substr($this->aktZeile,67,9);
                        $this->ber_Kassendaten[10] = $this->pruefen->wandleKFZKennzeichen($this->ber_Kassendaten[10]);

                        //$this->ber_Kassendaten[10] = $gewKFZKennz;

                        //var_dump($this->ber_Kassendaten);

                        $this->safeKassendaten(false);

                        $count++;
                    }
                }

                fclose($fd);

                $this->_Funktionen->SchreibeModulStatus("runImport","Ende Import - Kassendaten:");

                $fd = null;
                $this->posZeilen =0;


            /*
            $SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
            $SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T','FG',false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->KassendatenFile.$this->$KassendatenFile,false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',SYSDATE,false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','OK',false);
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            //echo $SQL;

            if($this->_DB->Ausfuehren($SQL)===false)
            {
            }
            */





            }

        }
        else {

        //Wenn True �bergeben wurde -- Kassendaten sind als File vorhanden!
        //-----------------------------------------------------------------
            $this->safeKassendaten(true);

        }
    }

    public function safeKassendaten($flag) {
        $ImportFile = $this->getXDIKEY();



        //Kassendaten speichern
        if($flag == false) {
        //Importiere Kassendaten aus FILE
            $SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
            $SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
            $SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS";
            $SQL .= ") VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[0],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kassendaten[1],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$this->ber_Kassendaten[2],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[3],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[4],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[5],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[6],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[7],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kassendaten[8],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Kassendaten[9],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kassendaten[10],false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T','',false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$ImportFile,false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',0,True);
            $SQL .= ')';

            if($this->_DB->Ausfuehren($SQL)===false) {
            }
        }
        else {
        //Importiere Kassendaten aus den DWH in die Tabelle FKASSENDATEN
        //@TODO DWH - Daten

        }

    }








    public function ImportGlasKopf() {
    //L�sche Tabelle FGINZUGANG
        $SQL = "TRUNCATE TABLE FGINZUGANG";

        if($this->_DB->Ausfuehren($SQL)===false) {
        }

        if(($fd = fopen($this->pfadGlasKopf.'glas_kopf.res','r')) === false) {
            echo $this->error_msg = "Fehler beim �ffnen der Datei";
        }
        else {
        //Anfang Zeilennummer auf 1 setzen
        //$this->aktZeilennummer = 1;
            $nr = 0;

            while(!feof($fd)) {
                $this->aktZeile = fgets($fd);

                $SQL = "INSERT INTO FGINZUGANG (FGI_VORGANGNR,FGI_USER,FGI_USERDAT) VALUES (";
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->aktZeile,false);
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
                $nr++;

            }
            fclose($fd);

            echo "Anzahl DS eingefuegt".$nr;
        }

    }




    public function ImportGlasStamm() {
    //L�sche Tabelle FGINZUGANG
        $SQL = "TRUNCATE TABLE FGLASSTAMM";

        if($this->_DB->Ausfuehren($SQL)===false) {
        }

        if(($fd = fopen($this->pfadGlasKopf.'glas_stamm.res','r')) === false) {
            echo $this->error_msg = "Fehler beim �ffnen der Datei";
        }
        else {
        //Anfang Zeilennummer auf 1 setzen
        //$this->aktZeilennummer = 1;
            $nr = 0;

            while(!feof($fd)) {
                $this->aktZeile = fgets($fd);

                $SQL = "INSERT INTO FGLASSTAMM (FGL_ARTNR,FGL_USER,FGL_USERDAT) VALUES (";
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->aktZeile,false);
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($this->_DB->Ausfuehren($SQL)===false) {
                }
                $nr++;

            }
            fclose($fd);

            echo "Anzahl DS eingefuegt".$nr;
        }

    }


    public function getAktFile() {
    //Aktuelles Verzeichnis durchlaufen
        $checkFile = '';
        //Anz der Dateien im Verzeichnis
        $element = 0;
        $FileNR = 1;

        if ($handle = opendir($this->pfadFile)) {
            while(false !== ($file = readdir($handle))) {
                $checkFile = '';

                //echo $file.PHP_EOL;
                $checkFile = substr($file,0,3);

                if ($file == '.' || $file == '..') {

                }
                else {
                    if($checkFile == 'imp') {
                    //echo "<br>";
                    //echo $checkFile.PHP_EOL;
                    //echo "File wurde bereits importiert".PHP_EOL;
                    //---------------------------------------------
                    }
                    else {
                    //echo "<br>";
                    //echo $checkFile.PHP_EOL;
                    //echo "File wurde noch nicht importiert".PHP_EOL;
                    //echo "Datei die importiert werden soll: ";
                    //Nimm vorhandene Files in die Dokumentenliste auf
                        $this->aktFile[$element] = $file;
                        $element++;
                    //echo "</br>";
                    }
                //echo $FileNR++;
                }

            }

            var_dump($this->aktFile);
        }
    //-------------------------------------------------------------------------
    //Sortiere Array - Aufsteigend!
    //-------------------------------------------------------------------------




    //Array mit den aktuellen zu importierenden Daten
    //echo $this->aktFile[0];
    }


    public function getAktFileKassendaten() {
    //Aktuelles Verzeichnis durchlaufen
        $checkFile = '';
        //Anz der Dateien im Verzeichnis
        $element = 0;
        $FileNR = 1;
        $Datum='';

        $SQL = 'Select max(FGK_ZEITSTEMPEL) AS DATETIME FROM FGKOPFDATEN';

        $rsControl = $this->_DB->RecordSetOeffnen($SQL);

        $MAXFILE = $rsControl->FeldInhalt('DATETIME');

        $Datum = substr($MAXFILE, 0, 10);
        $Datum = strtotime($Datum);



        if ($handle = opendir($this->pfadKassendaten)) {
            while(false !== ($file = readdir($handle))) {
                $checkFile = '';

                //echo $file.PHP_EOL;
                $checkFile = substr($file,0,3);

                if ($file == '.' || $file == '..') {

                }
                else {
                    if($checkFile == 'imp') {
                    //echo "<br>";
                    //echo $checkFile.PHP_EOL;
                    //echo "File wurde bereits importiert".PHP_EOL;
                    //---------------------------------------------
                    }
                    else {
                    //Alle Kassendaten importieren
                    //WandleDatum
                        $DatumKasse = substr($file,9,16);
                        $DatumKasse = substr($DatumKasse,0,6);
                        $Tag = substr($DatumKasse,4,2);
                        $Monat = substr($DatumKasse,2,2);
                        $Jahr = substr($DatumKasse,0,2);
                        $wandDatum = $Tag.'.'.$Monat.'.20'.$Jahr;
                        $wandDatum = strtotime($wandDatum);


                        if($wandDatum <= $Datum) {
                        //Pr�fung ob Datum bereits importiert wurde

                            $this->aktFileKassendaten[$element] = $file;
                            $element++;
                        }

                    //echo "</br>";
                    }
                //echo $FileNR++;
                }

            }

            echo "<br>";
            echo "Kassendaten -- FILES die zur Verarbeitung anstehen";
            var_dump($this->aktFileKassendaten);
            echo "<br>";

        }
    //Array mit den aktuellen zu importierenden Daten
    //echo $this->aktFile[0];
    }

    public function openDBConnection() {
        try {
            $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
            $this->_DB->Oeffnen();
            $_AWISBenutzer = awisBenutzer::Init();
            echo "<link rel=stylesheet type=text/css href=" . $_AWISBenutzer->CSSDatei() .">";
        }
        catch (Exception $ex){
            die($ex->getMessage());
        }
    }



    public function getLastFileImportedDate() {
        $FileDateiName = '';

        $SQL = "Select * from IMPORTPROTOKOLL WHERE XDI_BEREICH='FGA' ORDER BY XDI_KEY DESC";

        $rsZuletztImportiert = $this->_DB->RecordSetOeffnen($SQL);

        $FileDateiName = $rsZuletztImportiert->FeldInhalt('XDI_DATEINAME');

        //$test = '20090429.txt';
        //$FileDateiName = '20090505.txt';

        $pos = strrpos($rsZuletztImportiert->FeldInhalt('XDI_DATEINAME'), '/');
        $this->lastFile = substr($FileDateiName,$pos + 1,8);
        echo $this->lastFile;

        $this->diffDays();
    }


    private function diffDays() {
        $this->aktDate = date('Ymd');
        //echo "<br>";
        //echo $this->aktDate;
        //echo $lastdayMonth = strtotime("last day month");
        $this->diffDays = strtotime($this->aktDate) - strtotime($this->lastFile);
        $this->diffDays = $this->diffDays / 86400;
        //echo "<br>";
        $this->diffDays = round($this->diffDays);

        //Erzeuge Files
        for ($element = 0;$element < $this->diffDays;$element++) {
            echo "<br>";
            $this->lastFile = strtotime($this->lastFile) + 86400;
            $this->lastFile = date('Ymd',$this->lastFile);
            echo $this->lastFile;
            $this->fikFiles[$element] = $this->lastFile;

        }

        //Array aufsteigend sortieren
        //echo "<br>";
        //var_dump($this->fikFiles);

        $this->checkImportFiles();

    }


} // Ende der Klasse
?>