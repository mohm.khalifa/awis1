<html>
<head>
<meta http-equiv="content-type" content="text/html"; charset="windows-1252">
</head>
<?php

global $AWISWebSrv;

require_once("operating_AWIS_config.php");


require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if ($AWISBenutzer->HatDasRecht(3124) == 0 || $AWISBenutzer->HatDasRecht(3124)&32 !== 32 )
	{
	    $Form->Fehler_KeineRechte();
	}


function autolink($str, $attributes=array(), $www) {
    $attrs = '';
    foreach ($attributes as $attribute => $value) {
        $attrs .= " {$attribute}=\"{$value}\"";
    }
 
    $str = ' ' . $str;
    $str = preg_replace(
        "`([^\"=\'>])(/home20/([\.\']|[^\s<]+[^\s<\.)]))`i",
//        '`([^"=\'>])(/home20/[^\s<]+[^\s<\.)])`i',
       "$1<a href=".$www."$2".$attrs.">$2</a>",
        $str
    );
    $str = substr($str, 1);
     
    return $str;
}


if (!empty($_REQUEST["file"])) {
	
	if ($_REQUEST["file"]=="/" || substr($_REQUEST["file"],0,1)=="/") 
	{
		print "<h1>Kein Zugriff auf ROOT-Verzeichnis erlaubt</h1>";
	  	exit (0);	
	}
	elseif (substr($_REQUEST["file"],0,3)=="../" || substr($_REQUEST["file"],0,2)=="..")	
	{
		print "<h1>Pfadwechsel nicht erlaubt.</h1>";
	  	exit (0);	
	}		
	
  print "<h1>". $AWISAppSrv . ' - ' . basename ($_REQUEST["file"]) ."</h1>";
  $myfile = @fopen ($_REQUEST["file"], "r");
  if ($myfile == FALSE) {
    print "<h2>Kann Datei nicht &ouml;ffnen.</h2>";
    exit (0);
  };
} else {
  print "<h1>Keine Datei angegeben</h1>";
  exit (0);
}

$dir_back=substr($_REQUEST["file"],0,strripos($_REQUEST["file"],"/"));
if($dir_back!="/")
{
        echo "<h2><a href=./showdir.php?dir=$dir_back>Verzeichnis nach oben</a></h2>";
}

if (!empty($_REQUEST["search"]))
{
$search=explode(",",$_REQUEST["search"]);
$search_text=htmlspecialchars($_REQUEST["search"]);
echo "<h2>Anzeige ab Suchtext: >>".$_REQUEST["search"]."<<<h2>";
}
else
{
$search="";
$search_text="";
}

echo "<form name='frmSuche' method=get action=./showfile_sqlldr.php>";
echo "<input name=search type=text size=50 value='".$search_text."'> * <input type=submit value=Suchen>";
echo "<h5><b>* Suchhinweis: Leere Suche zeigt komplette Datei an.";
echo " | Mehrere Suchwörter durch Komma trennen => Ab 1.ten Worttreffer wird angezeigt. (Bsp.: Tabelle,Satz,erfolgreich)</b><h5>";
echo "<input name=file type=hidden value='".$_REQUEST["file"]."'>";
echo "</form>";

print "<table border=\"0\">";

$line_nr = 0;
$line_match=0;
$WWWPath=$AWISWebSrv.'/admin/appserver/showfile_log.php?file=';

while (!feof ($myfile)) {
$grepmatch=array();
$cnt=0;
 if (!empty($search))
 {
  $line = fgets ($myfile, 4096);

  $line=autolink($line,array("target"=>"_blank"),$WWWPath);
  if (strpos($line,"href")>0)
  {
   $cnt=1;
  }

  for($i=0;$i<count($search);$i++)
  {	
    if($line_match==0)
    {		
  	if(stristr($line,$search[$i])!== FALSE)
	{
 	 $line_match=1;
	 $line_nr++;
   	 print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
	  if ($cnt>0){
        	print "<td><code>". $line ."</code></td></tr>";
	  }
	  else{
        	print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
	  }
	 break;
  	}
    }else
    {
	 $line_nr++;
         print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
	  if ($cnt>0){
                print "<td><code>". $line ."</code></td></tr>";
          }
          else{
                print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
          }
	 break;	
    }
  }
 }
 else
 {	
  $line = fgets ($myfile, 4096);
  $line=autolink($line,array("target"=>"_blank"),$WWWPath);
  if (strpos($line,"href")>0)
  {
   $cnt=1;
  }
  $line_nr++;
  print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
  if ($cnt>0){
	print "<td><code>". $line ."</code></td></tr>";	
  }
  else{
    	print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
  }
 }
}
fclose($myfile);
print "</table>";

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</html>
