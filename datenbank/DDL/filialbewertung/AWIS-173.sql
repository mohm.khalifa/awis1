--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
SET DEFINE OFF;

ALTER TABLE WEBMENUEPUNKTE 
ADD (XMP_GUELTIGAB DATE DEFAULT '01.01.2017' NOT NULL);

COMMENT ON COLUMN WEBMENUEPUNKTE.XMP_GUELTIGAB IS 'Moduleinblendung ab';

ALTER TABLE WEBMENUEPUNKTE 
ADD (XMP_GUELTIGBIS DATE DEFAULT '31.12.2030' NOT NULL);

COMMENT ON COLUMN WEBMENUEPUNKTE.XMP_GUELTIGBIS IS 'Moduleinblendung bis';

INSERT INTO RECHTE
  (XRC_ID, XRC_RECHT) 
  VALUES
  (64000, 'Filialbewertung Zentrale');
COMMIT;
  
INSERT INTO RECHTESTUFEN
  (XRS_XRC_ID,XRS_BIT,XRS_BESCHREIBUNG)
  VALUES
  (64000,0,'Anzeigen' );
COMMIT;

INSERT INTO WEBMENUEPUNKTE
  ( XMP_KEY,XMP_XME_KEY,XMP_BEZEICHNUNG,XMP_XRC_ID,XMP_STUFE,XMP_GRUPPENID,XMP_SORTIERUNG,XMP_LINK,XMP_USER,XMP_USERDAT,XMP_GUELTIGAB,XMP_GUELTIGBIS
  )
  VALUES
  ( 256,1,'Filialen bewerteten die Zentrale',64000,1,0,1,'https://www.netigate.se/a/s.aspx?s=450013X96871863X94799','Tobias Schäffler',sysdate,'14.08.2017 06:00:00','24.08.2017 20:00:00');
COMMIT;

--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into TEXTKONSERVEN
SET DEFINE OFF;
INSERT
INTO TEXTKONSERVEN
  ( XTX_KENNUNG, XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT
  )
  VALUES
  ('MENUE_256_TEXT','MENUE','Filialen bewerten die Zentrale','Filialen bewerten die Zentrale','Filialen bewerten die Zentrale','Filialen bewerten die Zentrale','Filialen bewerten die Zentrale', 'Filialen bewerten die Zentrale', 'Filialen bewerten die Zentrale', 'Filialen bewerten die Zentrale', sysdate );
  
INSERT
INTO TEXTKONSERVEN
  (XTX_KENNUNG, XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT
  )
  VALUES
  ( 'MENUE_256_HINWEIS','MENUE', 'Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen', 'Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen','Abfrage der Zufriedenheit mit Zentralen Abteilungen',sysdate);
commit;