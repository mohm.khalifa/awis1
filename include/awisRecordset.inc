<?php

/**
 * awisRecordset
 *
 * Datenset
 *
 * @author    Sacha Kerres
 * @copyright .T.U Auto Teile Unger
 * @version   200807
 */
class awisRecordset
{
    /**
     * Feldname
     *
     */
    const FELDINFO_NAME = 'Name';
    /**
     * Gr��e des Feldes
     *
     */
    const FELDINFO_GROESSE = 'Groesse';
    /**
     * Feldtyp
     *
     */
    const FELDINFO_TYP = 'Typ';
    /**
     * Format f�r die Formatierung mit Hilfe von FeldInhaltFormat()
     *
     */
    const FELDINFO_FORMAT = 'TypKZ';
    /**
     * Stellen vor dem Komma
     *
     */
    const FELDINFO_VORKOMMA = 'Vorkomma';
    /**
     * Stellen nach dem Komma
     *
     */
    const FELDINFO_NACHKOMMA = 'Nachkomma';
    /**
     * Anzahl der Datens�tze
     *
     * @var int
     */
    private $_AnzDS = 0;

    /**
     * Anzahl der Spalten im Recordset
     *
     * @var int
     */
    private $_AnzSpalten = 0;

    /**
     * Die Daten als Tabelle
     *
     * @var array
     */
    private $_Daten = array();

    /**
     * Informationen zu den einzelnen Feldern
     *
     * @var array
     */
    private $_MetaDaten = array();

    /**
     * Aktuelle Position im Recordset
     *
     * @var int
     */
    private $_DSZeiger = -1;

    /**
     * Ende der Daten erreicht
     *
     * @var boolean
     */
    private $_EOF = true;

    /**
     * Anfang der Daten erreicht
     *
     * @var boolean
     */
    private $_BOF = true;

    /**
     * Formular
     *
     * @var awisFormular
     */
    private $_Form = null;

    /**
     * Initialisierung
     *
     */
    public function __construct()
    {
        if (is_object($this->_Form)) {
            $this->_Form = new awisFormular();
        }
    }

    /**
     * Laden der Daten
     *
     * @param array $Daten
     * @param array $MetaDaten
     * @param int   $Zeilen
     * @param int   $Spalten
     * @internal
     * @magic
     */
    public function __Laden($Daten, $MetaDaten, $Zeilen, $Spalten)
    {
        $this->_AnzDS = $Zeilen;
        $this->_AnzSpalten = $Spalten;
        $this->_Daten = $Daten;
        if (is_array($this->_Daten)) {
            $this->_Daten = array_change_key_case($this->_Daten, CASE_UPPER);
            $this->DSErster();
        }

        $this->_MetaDaten = $MetaDaten;
    }

    /**
     * Liefert das gesamte Recordset als Tabelle zur�ck
     *
     * @return array
     */
    public function Tabelle()
    {
        return $this->_Daten;
    }

    /**
     * Liefert den aktuellen Datensatz als Tabelle zur�ck
     *
     * @return array
     */
    public function Datensatz()
    {
        $Datensatz = array();
        foreach ($this->_MetaDaten AS $Feld) {
            $Datensatz[$Feld['Name']] = ($this->_DSZeiger >= 0?$this->_Daten[$Feld['Name']][$this->_DSZeiger]:'');
        }

        return $Datensatz;
    }

    /**
     * Liefert die eingelesenen Daten als XML Text
     *
     * @param string $Root
     * @param string $Zeile
     *
     * @return SimpleXMLElement
     */
    public function &XML($Root = '', $ZeilenName = '')
    {
        $XML = '<?xml version="1.0" encoding="ISO-8859-15"?>';
        $XML .= '<' . ($Root == ''?'Daten':$Root) . ' Zeilen="' . $this->_AnzDS . '">';

        $this->DSErster();
        $Zeile = 1;
        while (!$this->EOF()) {
            $XML .= "<" . ($ZeilenName == ''?'Zeile':$ZeilenName) . " ID=\"" . $Zeile . "\">";

            for ($Spalte = 1; $Spalte <= $this->_AnzSpalten; $Spalte++) {
                $XML .= "<" . $this->FeldInfo($Spalte, 'Name') . ">";
                //$XML .= ''.$this->FeldInhalt($Spalte).'';
                $XML .= '<![CDATA["' . $this->FeldInhalt($Spalte) . '"]]>';
                $XML .= "</" . $this->FeldInfo($Spalte, 'Name') . ">";
            }

            $XML .= "</" . ($ZeilenName == ''?'Zeile':$ZeilenName) . ">";
            $this->DSWeiter();
            $Zeile++;
        }
        $XML .= '</' . ($Root == ''?'Daten':$Root) . '>';
        $Erg = new SimpleXMLElement($XML);

        return $Erg;
    }

    /**
     * Liefert ein einzelnes Datenfeld des aktuellen Datensatzes
     *
     * @param        string /int $Feld
     * @param string $Format
     * @return string
     */
    public function FeldInhalt($Feld, $Format = '')
    {
        $SpaltenWert = '';
        if (is_numeric($Feld)) {
            if (isset($this->_MetaDaten[$Feld]['Name']) AND isset($this->_Daten[$this->_MetaDaten[$Feld]['Name']][$this->_DSZeiger])) {
                $SpaltenWert = $this->_Daten[$this->_MetaDaten[$Feld]['Name']][$this->_DSZeiger];
            }
        } elseif (isset($this->_Daten[$Feld][$this->_DSZeiger])) {
            $SpaltenWert = $this->_Daten[$Feld][$this->_DSZeiger];
        }

        //PG 15.09.2016: Parameter wurde bisher nicht ausgwertet.
        if($Format!=''){
            $Form = new awisFormular();
            $SpaltenWert = $Form->Format($Format,$SpaltenWert);
        }

        return ($SpaltenWert);
    }

    /**
     * Liefert einen Wert eines einzelnen Datenfeldes des aktuellen Datensatzes, oder aber, den POST Wert, falls dieser vorhanden ist.
     *
     * @param        $Feld Feldname in Datenbank
     * @param string $Format awisForumular::Format() Formate
     * @param bool   $POSTbenutzen true=POSTs/Arraywert benutzen, falls vorhanden | false=FeldInhalt aus DB verwenden
     * @param array  $POSTarray Default: $_POST, ggf. kann ein anderes Array reingeschickt werden
     * @return mixed Wert
     */
    public function FeldOderPOST($Feld, $Format='',$POSTbenutzen = true, array $POSTarray = array()){
        //Wenn kein anderes Array �bergeben wurde, dann $_POST benuten
        if(count($POSTarray===0)){
            $POSTarray = $_POST;
        }

        //Feld als txt o. suc vorhanden + soll �berhaubt der Wert aus dem Postarray genutzt werden?
        if(isset($POSTarray['txt'.$Feld]) and $POSTbenutzen){
            $Wert = $POSTarray['txt'.$Feld];
        }elseif(isset($POSTarray['suc'.$Feld]) and $POSTbenutzen){
            $Wert = $POSTarray['suc'.$Feld];
        }else{
            $Wert = self::FeldInhalt($Feld,$Format);
        }

        if($Format!=''){
            $Form = new awisFormular();
            $Wert = $Form->Format($Format,$Wert);
        }

        return $Wert;
    }

    /**
     * Liefert Informationation �ber ein einzelnes Feld eines Recordsets
     *
     * @param        string /int $Feld
     * @param string $FeldInfo
     * @return string
     */
    public function FeldInfo($Feld, $FeldInfo)
    {
        $FeldNr = -1;

        if (is_numeric($Feld)) {
            $FeldNr = $Feld;
        } else {
            foreach ($this->_MetaDaten as $SpaltenNr => $Spalte) {
                if (strcasecmp($Spalte['Name'], $Feld) == 0) {
                    $FeldNr = $SpaltenNr;
                    break;
                }
            }
        }

        if ($FeldNr >= 0) {
            if (isset($this->_MetaDaten[$FeldNr][$FeldInfo])) {
                return $this->_MetaDaten[$FeldNr][$FeldInfo];
            }
        }

        return '';
    }

    /**
     * Liefert eine Array mit allen Spaltennamen eines RecordSets
     *
     * @return array
     */
    public function SpaltenNamen()
    {
        $Erg = array();
        foreach ($this->_MetaDaten as $SpaltenNr => $Spalte) {
            $Erg[] = $Spalte['Name'];
        }

        return $Erg;
    }

    /**
     * Springt zum ersten Datensatz
     *
     */
    public function DSErster()
    {
        if ($this->_AnzDS > 0) {
            $this->_DSZeiger = 0;
            $this->_EOF = false;
            $this->_BOF = false;
        } else {
            $this->_DSZeiger = -1;
            $this->_EOF = true;
            $this->_BOF = true;
        }
    }

    /**
     * Springt zum letzten Datensatz
     *
     */
    public function DSLetzter()
    {
        if ($this->_AnzDS > 0) {
            $this->_DSZeiger = $this->_AnzDS - 1;
            $this->_EOF = false;
            $this->_BOF = false;
        } else {
            $this->_DSZeiger = -1;
            $this->_EOF = true;
            $this->_BOF = true;
        }
    }

    /**
     * Geht zum n�chsten Datensatz
     *
     */
    public function DSWeiter()
    {
        if ((++$this->_DSZeiger) < $this->_AnzDS) {
            $this->_EOF = false;
            $this->_BOF = false;
        } else {
            $this->_DSZeiger = -1;
            $this->_EOF = true;
        }
    }

    /**
     * Geht zum vorherigen Datensatz
     *
     */
    public function DSZurueck()
    {
        if ((--$this->_DSZeiger) >= 0) {
            $this->_BOF = false;
            $this->_EOF = false;
        } else {
            $this->_DSZeiger = -1;
            $this->_BOF = true;
        }
    }

    /**
     * Liefert die aktuelle Datensatz-Nummer. Kann
     * fuer die Methode GeheZu() verwendet werden.
     *
     * @see GeheZu()
     * @return int
     */
    public function DSNummer()
    {
        return $this->_DSZeiger;
    }

    /**
     * Gibt an, ob �ber das Ende der Datens�tze positioniert wurde
     *
     * @return boolean
     */
    public function EOF()
    {
        return $this->_EOF;
    }

    /**
     * Gibt an, ob �ber den Anfang der Datens�tze positioniert wurde
     *
     * @return boolean
     */
    public function BOF()
    {
        return $this->_BOF;
    }

    /**
     * Springt zu einem bestimmten Datensatz
     *
     * @param int $DSNr
     * @return boolean
     */
    public function GeheZu($DSNr)
    {
        if ($DSNr >= 0 and $DSNr < $this->_AnzDS) {
            $this->_DSZeiger = $DSNr;

            $this->_EOF = $this->_BOF = false;

            return true;
        }

        return false;
    }

    /**
     * Liefert die aktuelle Anzahl von Datens�tzen im RecordSet
     *
     * @return int
     */
    public function AnzahlDatensaetze()
    {
        return $this->_AnzDS;
    }

    /**
     * Liefert die aktuelle Anzahl von Spalten im RecordSet
     *
     * @return int
     */
    public function AnzahlSpalten()
    {
        return $this->_AnzSpalten;
    }

    /**
     * Liefert ein Array mit allen Feldnamen im Recordset
     *
     * @return array
     */
    public function Feldliste()
    {
        $Felder = array();

        foreach ($this->_MetaDaten as $SpaltenNr => $Spalte) {
            $Felder[] = $Spalte['Name'];
        }

        return $Felder;
    }
}
