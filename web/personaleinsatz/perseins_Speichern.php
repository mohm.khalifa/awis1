<?php
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $con;

//var_dump($_POST);

/********************************************
* Speichern von Datens�tzen
********************************************/

if(isset($_POST['cmdSpeichern_x']))		
{
	if($_POST['txtMEI_MEA_KEY']==8)
	{
		$rsFIL = awisOpenRecordset($con, 'SELECT COUNT(*) AS Anzahl FROM Filialen WHERE FIL_ID=0' . $_POST['txtMEI_FIL_ID'] . '');
		if($rsFIL['ANZAHL'][0]==0)
		{
			echo '<span class=HinweisText>Es wurde keine g�ltige Filiale angegeben: ' . $_POST['txtMEI_FIL_ID'] . '.</span>';
			echo '<br><br><a href=./perseins_Main.php?cmdAktion=Einsaetze>Zur�ck</a>';
			awisLogoff($con);
			die();
		}
		unset($rsFIL);
	}

	if(!isset($_POST['txtMEI_KEY']))		// Neuer Datensatz
	{
		// Ge�ndert am 09.01.06 durch CA - Keine Bereichspr�fung vorhanden
		$SQL_BEREICH="SELECT MIT_BEREICH FROM MITARBEITER WHERE MIT_KEY=".$_POST['txtMEI_MIT_KEY'];
		$rsMitBereich=awisOpenRecordset($con, $SQL_BEREICH);

		//$SQL = "SELECT MEP_STATUS FROM MITARBEITEREINSAETZEPRUEFUNGEN WHERE MEP_TAG='" . awis_format($_POST['txtMEI_TAG'], 'OracleDatum') . "'";
		$SQL = "SELECT MEP_STATUS FROM MITARBEITEREINSAETZEPRUEFUNGEN WHERE MEP_TAG='" . awis_format($_POST['txtMEI_TAG'], 'OracleDatum') . "' AND MEP_BEREICH='".$rsMitBereich['MIT_BEREICH'][0]."'";
		$rsTest = awisOpenRecordset($con, $SQL);
		if(isset($rsTest['MEP_STATUS'][0]) AND $rsTest['MEP_STATUS'][0]=='A')	// Abgeschlossen
		{
			echo '<span class=HinweisText>F�r den ' . $_POST['txtMEI_TAG'] . ' sind keine neuen Eintr�ge m�glich, da dieser Tag als abgeschlossen gekennzeichnet wurde.</span>';
		}
		else
		{	
			$SQL = 'INSERT INTO MitarbeiterEinsaetze(';
			$SQL .= 'MEI_MIT_KEY, MEI_TAG, MEI_VONXTZ_ID, MEI_BISXTZ_ID, MEI_MEA_KEY, MEI_FIL_ID, MEI_BEMERKUNG, MEI_USER, MEI_USERDAT)';
			$SQL .= 'VALUES (';
			
			$SQL .= $_POST['txtMEI_MIT_KEY'];
			$SQL .= ", TO_DATE('" . $_POST['txtMEI_TAG'] . "','DD.MM.RRRR')";
			$SQL .= ", " . $_POST['txtMEI_VONXTZ_ID'];
			$SQL .= ", " . $_POST['txtMEI_BISXTZ_ID'];
			$SQL .= ", " . $_POST['txtMEI_MEA_KEY'];
			$SQL .= ", " . ($_POST['txtMEI_FIL_ID']==''?'NULL':"'" . $_POST['txtMEI_FIL_ID'] . "'");
			$SQL .= ", '" . $_POST['txtMEI_BEMERKUNG'] . "'";
			$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
			$SQL .= ',SYSDATE)';
		
		//	var_dump($SQL);
		
			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Speichern.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
		}
	}
	else		// Datensatz �ndern
	{
		$SQL = '';
		$chkFehler = '';
		$rschkMEI = awisOpenRecordset($con, 'SELECT * FROM MitarbeiterEinsaetze WHERE MEI_KEY=' . $_POST['txtMEI_KEY']);

		// Datens�tze pr�fen		
		if($_POST['txtMEI_MIT_KEY'] != $_POST['txtMEI_MIT_KEY_old'])
		{
			if($_POST['txtMEI_MIT_KEY_old'] != $rschkMEI['MEI_MIT_KEY'][0])
			{
				$chkFehler .= 'Mitarbeiter(' . $_POST['txtMEI_MIT_KEY_old'] . '/' . $rschkMEI['MEI_MIT_KEY'][0] . ')';
			}
			else
			{
				$SQL .= ', MEI_MIT_KEY=' . $_POST['txtMEI_MIT_KEY'];
			}
		}
		if($_POST['txtMEI_TAG'] != $_POST['txtMEI_TAG_old'])
		{
			if($_POST['txtMEI_TAG_old'] != str_replace('-','.',$rschkMEI['MEI_TAG'][0]))
			{
				$chkFehler .= 'Datum';
			}
			else
			{
				$SQL .= ", MEI_TAG=TO_DATE('" . awis_format($_POST['txtMEI_TAG'],'OracleDatum') . "','DD.MM.RRRR')";
			}
		}
		if($_POST['txtMEI_VONXTZ_ID'] != $_POST['txtMEI_VONXTZ_ID_old'])
		{
			if($_POST['txtMEI_VONXTZ_ID_old'] != $rschkMEI['MEI_VONXTZ_ID'][0])
			{
				$chkFehler .= 'Zeit von(' . $_POST['txtMEI_VONXTZ_ID_old'] . '/' . $rschkMEI['MEI_VONXTZ_ID'][0] . ')';
			}
			else
			{
				$SQL .= ', MEI_VONXTZ_ID=' . $_POST['txtMEI_VONXTZ_ID'];
			}
		}
		if($_POST['txtMEI_BISXTZ_ID'] != $_POST['txtMEI_BISXTZ_ID_old'])
		{
			if($_POST['txtMEI_BISXTZ_ID_old'] != $rschkMEI['MEI_BISXTZ_ID'][0])
			{
				$chkFehler .= 'Zeit bis';
			}
			else
			{
				$SQL .= ', MEI_BISXTZ_ID=' . $_POST['txtMEI_BISXTZ_ID'];
			}
		}
		if($_POST['txtMEI_MEA_KEY'] != $_POST['txtMEI_MEA_KEY_old'])
		{
			if($_POST['txtMEI_MEA_KEY_old'] != $rschkMEI['MEI_MEA_KEY'][0])
			{
				$chkFehler .= 'Aktion';
			}
			else
			{
				$SQL .= ', MEI_MEA_KEY=' . $_POST['txtMEI_MEA_KEY'];
			}
		}
		if($_POST['txtMEI_FIL_ID'] != $_POST['txtMEI_FIL_ID_old'])
		{
			if($_POST['txtMEI_FIL_ID_old'] != $rschkMEI['MEI_FIL_ID'][0])
			{
				$chkFehler .= 'Filiale';
			}
			else
			{
				$SQL .= ", MEI_FIL_ID='" . $_POST['txtMEI_FIL_ID'] . "'";
			}
		}

			// B�se Sonderzeichen raus
		//$Bem = str_replace("\"", "", $_POST['txtMEI_BEMERKUNG']);
		//$Bem = str_replace("'", "", $Bem);
		$Bem = str_replace("'", "", $_POST['txtMEI_BEMERKUNG']);
		$Bem = str_replace("\\", "", $Bem);
		$Bem = str_replace("<", "", $Bem);
		$Bem = str_replace(">", "", $Bem);
		
		if($Bem != stripcslashes($_POST['txtMEI_BEMERKUNG_old']))
		{
			if(stripcslashes($_POST['txtMEI_BEMERKUNG_old']) != $rschkMEI['MEI_BEMERKUNG'][0])
			{
				$chkFehler .= 'Bemerkung '.$Bem .'<br> Alter Wert:'.$_POST['txtMEI_BEMERKUNG_old'].'<br> DB Wert:'.$rschkMEI['MEI_BEMERKUNG'][0];
			}
			else
			{
				$SQL .= ", MEI_BEMERKUNG='" . $Bem . "'";
			}
		}

		If($chkFehler!='')
		{
			echo '<span class=Hinweistext>PROBLEM : ';
			echo '<br>Folgende Felder wurden in der Zwischenzeit von einem anderen Anwender ver�ndert!';
			echo '<br><br>' . $chkFehler . '<br>';
			echo '<br>Ge�ndert von: ' . $rschkMEI['MEI_USER'][0];
			echo '<br>Ge�ndert am: ' . $rschkMEI['MEI_USERDAT'][0];			
			echo '</span>';
		}
		elseif($SQL!='')
		{
			$SQL = 'UPDATE MitarbeiterEinsaetze SET ' . substr($SQL, 1) . ' WHERE MEI_KEY=' . $_POST['txtMEI_KEY'];
			$Erg = awisExecute($con, $SQL);
			if($Erg===FALSE)
			{
				awisErrorMailLink("perseins_Speichern.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}

		}
	}
}	// Ende Speichern
?>