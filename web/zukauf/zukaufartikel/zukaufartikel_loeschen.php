<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Fehler','err_keineRechte');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtZLA_KEY']))
		{
			$Tabelle = 'ZLA';
			$Key=$_POST['txtZLA_KEY'];

			$SQL = 'SELECT ZLA_KEY, ZLA_BEZEICHNUNG, ZLA_ARTIKLENUMMER';
			$SQL .= ' FROM Zukaufartikel ';
			$SQL .= ' WHERE ZLA_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$ZLAKey = $rsDaten->FeldInhalt('ZLA_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ZLA','ZLA_BEZEICHNUNG'),$rsDaten->FeldInhalt('ZLA_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('ZLA','ZLA_ARTIKLENUMMER'),$rsDaten->FeldInhalt('ZLA_ARTIKLENUMMER'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case 'Preise':
					$Tabelle = 'ZAP';
					$Key=$_GET['Del'];
					$ZLAKey=$_GET['Key'];
					$ZALKey=$_GET['Edit'];

					$SQL = 'SELECT ZAP_KEY, ZAP_ZAL_KEY, ZAP_GUELTIGAB, ZAP_VKPR';
					$SQL .= ' FROM Zukaufartikellieferantenpreise';
					$SQL .= ' WHERE ZAP_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZAP','ZAP_GUELTIGAB'),$rsDaten->FeldInhalt('ZAP_GUELTIGAB'));
					$Felder[]=array($Form->LadeTextBaustein('ZAP','ZAP_VKPR'),$rsDaten->FeldInhalt('ZAP_VKPR'));
					break;
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Lieferanten':
					$Tabelle = 'ZAL';
					$Key=$_GET['Del'];
					$ZLAKey=$_GET['ZLA_KEY'];

					$SQL = 'SELECT ZAL_KEY, ZAL_LIE_NR, ZAL_BESTELLNUMMER';
					$SQL .= ' FROM Zukaufartikellieferanten';
					$SQL .= ' WHERE ZAL_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZAL','ZAL_LIE_NR'),$rsDaten->FeldInhalt('ZAL_LIE_NR'));
					$Felder[]=array($Form->LadeTextBaustein('ZAL','ZAL_BESTELLNUMMER'),$rsDaten->FeldInhalt('ZAL_BESTELLNUMMER'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'ZLA':
				$SQL = 'DELETE FROM Zukaufartikel WHERE ZLA_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=-1;
				break;
			case 'ZAL':
				$SQL = 'DELETE FROM Zukaufartikellieferanten WHERE ZAL_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtZLAKey'];
				break;
			case 'ZAP':
				$SQL = 'DELETE FROM Zukaufartikellieferantenpreise WHERE ZAP_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtZLAKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			$DB->Ausfuehren($SQL,'',false);
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./zukaufartikel_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').(isset($ZLAKey)?'&Key='.$ZLAKey:'').(isset($ZALKey)?'&Edit='.$ZALKey:'').' method=post>');
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('ZLAKey',$ZLAKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>