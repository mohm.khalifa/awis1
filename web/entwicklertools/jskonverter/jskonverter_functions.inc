<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class jskonverter_functions {

    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $AWISSprachKonserven;
    public $Recht60000;
    public $JSK;

    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht60000 = $this->AWISBenutzer->HatDasRecht(60000);

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('JSK', '%');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Register', 'ttt_600000');
        $TextKonserven[] = array('TITEL', 'tit_JSKonverter');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }


    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht60000 & $Bit) != $Bit) {
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            die();
        }
    }

    function JSzuPHP($JS) {
        $PHP = preg_replace('/[\']/','\\\'',$JS);
        $PHP = preg_replace("/\r\n/","'.PHP_EOL;\recho '",$PHP);
        $PHP = 'echo \'' . $PHP;
        $PHP = $PHP . "'.PHP_EOL;";
        return $PHP;
    }
    function PHPzuJS($PHP) {
        $JS = preg_replace('/(echo [\'])/','',$PHP);
        $JS = preg_replace('/([\'][.]PHP[_]EOL[;])/','',$JS);
        $JS = str_replace('\\\'','\'',$JS);
        return $JS;
    }
}
?>