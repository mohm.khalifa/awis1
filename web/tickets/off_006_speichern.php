<?php
/**
 * Formular: 06 -personalisiertes Postfach Filialmitarbeiter
 * @author Tobias Sch�ffler
 *
 */
$FormularKennung = '006';
$DetailBlock = '';
$DetailBlock100 = '';
$DetailBlockPP = '';

$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'lst_*');
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'txt_*');
$TextFormular = $Form->LadeTexte($TextKonserven);

$DetailFelder = $Form->NameInArray($_POST, 'txtDETAILS', 2, 0);
$Form->DebugAusgabe(3,$DetailFelder);
$OT_EOL = "\r\n";

$Form->DebugAusgabe(3,$_POST);

$Block='110';
$Postfach = $Form->WerteListe($TextFormular['OTF_FRM_'.$FormularKennung]['lst_PersPostfach'],$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PersPostfach']);
$DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALISIERT'].$OT_EOL;
$DetailBlock .= $Postfach.$OT_EOL;
$DetailBlockPP .= $Postfach;

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']!='') {
    $Funktion = $Form->WerteListe($TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion'],$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']);
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTION'];
    $DetailBlock .= $Funktion.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']!='') {
    $DetailBlock .= '     '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext'].$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALE'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'].$OT_EOL;
    $Ticket['StringVal']['User Abteilung'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'];
}

$Block='115';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'].$OT_EOL;
    $Ticket['StringVal']['Personalnummer_Freitext'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'];
}

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_VERTRIEBSLEITER'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter'].$OT_EOL;
    $Ticket['StringVal']['Vertriebsleiter_Freitext'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter'];
}

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLE'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'].$OT_EOL;
    $Ticket['StringVal']['Kostenstelle ATU'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'];
}

$Block='120';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt']!='') {
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALEALT'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt'].$OT_EOL;
    $Ticket['StringVal']['Filiale_alt'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt'];
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALENEU'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu'].$OT_EOL;
    $Ticket['StringVal']['User Abteilung'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu'];
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt']!='') {
    $Funktion = $Form->WerteListe($TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion'],$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt']);
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONALT'];
    $DetailBlock .= $Funktion.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt']!='') {
    $DetailBlock .= '     '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt'].$OT_EOL;
}

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu']!='') {
    $Funktion = $Form->WerteListe($TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion'],$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu']);
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONNEU'];
    $DetailBlock .= $Funktion.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu']!='') {
    $DetailBlock .= '     '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu'].$OT_EOL;
}

$Block='125';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'].$OT_EOL;
    $Ticket['StringVal']['Personalnummer_Freitext'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'];
}

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLENEU'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu'].$OT_EOL;
    $Ticket['StringVal']['Kostenstelle ATU'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu'];
}



$Block = '130';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']!='') {
    $Funktion = $Form->WerteListe($TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion'],$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion']);
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTION'];
    $DetailBlock .= $Funktion.$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']!='') {
    $DetailBlock .= '     '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext'].$OT_EOL;
}
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']!='') {
    $DetailBlock .= $OT_EOL.'     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALE'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'].$OT_EOL;
    $Ticket['StringVal']['User Abteilung'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'];
}
$Block = '135';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'].$OT_EOL;
    $Ticket['StringVal']['Personalnummer_Freitext'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'];
}

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']!='') {
    $DetailBlock .= '     '.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLE'];
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'].$OT_EOL;
    $Ticket['StringVal']['Kostenstelle ATU'] = $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'];
}

//**************************************************
// G�ltig ab
// AM 03.11.2020
//**************************************************

$Block='10';

if(isset($_POST['txtOTF_WERT_'.$Block]) AND $_POST['txtOTF_WERT_'.$Block]!='') {

    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_GUELTIGAB'].$OT_EOL;
    $DetailBlock .= $_POST['txtOTF_WERT_'.$Block].$OT_EOL;
}

//*********************************************************
//* Bemerkung
//*********************************************************
$Block='900';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']!='') {
    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_ALLGEMEIN'].$OT_EOL;
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'].$OT_EOL;
}


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='100';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']!='') {

    $Namelist = '';
    foreach($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn'] as $KEY) {
        $Namelist .= ';'.$KEY;
    }
    $Namelist = substr($Namelist,1);
    $DetailBlock110 .= $Namelist;

    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_BENACHRICHTIGUNG'].$OT_EOL;
    $DetailBlock .= $Namelist. $OT_EOL;
}


?>