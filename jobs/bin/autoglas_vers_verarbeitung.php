<?php

require_once('awisBenutzer.inc');
require_once('awisAutoglas_DateiTransfer.inc');
require_once("firstglas_Shuttle.php");

require_once("versicherungen_Shuttle.php");
require_once('awisFirstglasImport_neu.php');
require_once("awisVersicherungenImport.inc");
require_once("versicherungen_reporting.php");
require_once("awisVersDatenAbholung.inc");
require_once("awisJobs.inc");

//require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Load = 'transaction';

    for($i = 1; $i < $argc; $i++)
    {
        switch(strtolower($argv[$i]))
        {

            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--testsystem':
                $Testsystem = true;
                break;
            case '--starttag':
                $StartTag = $argv[++$i];
                break;
            case '--endtag':
                $EndTag = $argv[++$i];
                break;

            case '--abfrage':
                $Abfrage = $argv[++$i];
                break;

            case '--load':
                $Load = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --testsystem                       Daten aus dem Testsystem abfragen.' . PHP_EOL;
                //                             echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.' . PHP_EOL;
                echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.' . PHP_EOL;
                echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.' . PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AG_DTF = new awisAutoglas_DateiTransfer($DebugLevel, $Benutzer);
    $awisJobs = new awisJobs($Benutzer);

    switch($Abfrage)
    {
        //Scheduler ruft immer mit Parameter "AutoglasVerarbeitung" auf und handelt somit alle Schritte bis zum break ab
        //Sollte innerhalb der einzelnen Methoden ein Fehler auftreten, und der Job abbrechen
        //kann der Operator an x-beliebiger Stelle wieder aufsetzen (--abfrage <Stelle in Case>)

        case 'HoleVersDaten':
            $Abholung = new VersDatenAbholung($DebugLevel, $Benutzer);
            $Abholung->HoleVersDaten($Load);
            $Abholung->pruefeLoads();
            break;

        case 'AutoglasVerarbeitung':
            $Autoglas_Shuttle = new firstglasShuttle();
            $Autoglas_Shuttle->Shuttle();
            break;


        //Dies ist ein eigener Scheduleraufruf, da das Abholen und Exportieren in Abhaengigkeit zueinander abgehandelt werden muss
        //Sollten z. B. bereits Daten exportiert worden sein, aber wir noch kein Ergebnis zurueck erhalten haben,
        //dann sollte nach X-Stunden eine Meldung ausgegeben werden (um evtl.dann reagieren zu koennen) und ein weiterer Export darf dann nicht erfolgen

        case 'Versicherungsverarbeitung':
            $Vers_Shuttle = new VersicherungenShuttle($DebugLevel, $Benutzer);
            $Vers_Shuttle->Shuttle();
            break;

        case 'Versicherungsdateien_exportieren':
            $AG_DTF->Versicherungsdateien_exportieren();
            $AG_DTF->GepruefteHUKDateien_exportieren();
            $AG_DTF->Kassendaten_exportieren();
            $AG_DTF->KassendatenStorno_exportieren();
            break;

        //Das Abholen der Versicherungsdateien muss als eigener Eintrag in der Jobliste aufgefuehrt werden,
        //da dies oefter am Tag ausgefuehrt werden muss.
        case 'Versicherungsdateien_abholen':
            $AG_DTF->Versicherungsdateien_abholen();
            $AG_DTF->PruefeaufErgebnisDateien();
            $AG_DTF->Auftragsdaten_abholen();
            $AG_DTF->Glasstamm_abholen();

            break;
    }
    unset($AG_DTF);
}
catch(awisException $ex)
{
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
}
catch(Exception $ex)
{
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
}
?>
