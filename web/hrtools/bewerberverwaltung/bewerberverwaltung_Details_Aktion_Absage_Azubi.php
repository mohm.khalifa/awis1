<?php
global $AWIS_KEY2;                //Bewerbung
global $Aktionsmeldung;
require_once 'awisMailer.inc';
try {
    $TextKonserven = array();
    $TextKonserven[] = array('BEW', '%');

    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $MailSprachKonserven = $Form->LadeTexte($TextKonserven);

    $SQL = 'select B.ANR_ANREDE, b.HRK_TITEL, B.HRK_VORNAME, B.HRK_NACHNAME, B.HRK_EMAIL, a.BWH_KEY , C.MVT_BETREFF, C.MVT_TEXT, D.HRF_EMAIL_BEZEICHNUNG, EINGANGSART, D.HRF_KEY from V_BEWERBERVERWALTUNG a';
    $SQL .= ' inner join V_HRKOPF b';
    $SQL .= ' on a.HRK_KEY = B.HRK_KEY';
    $SQL .= ' inner join MAILVERSANDTEXTE c';
    $SQL .= ' on C.MVT_BEREICH = \'BEW_ABSAGE_AZUBI\'';
    $SQL .= ' inner join HRFIRMIERUNG d';
    $SQL .= ' on a.HRF_KEY = D.HRF_KEY';
    $SQL .= ' WHERE a.BWH_KEY' . $DB->LikeOderIst($AWIS_KEY2);

    $rsMail = $DB->RecordSetOeffnen($SQL);
    $Werkzeuge = new awisWerkzeuge();
    //  var_dump($rsMail->FeldInhalt('EINGANGSART'));

    if (isset($_POST['cmdStatusWeiterM']) or isset($_POST['cmdStatusWeiterO'])) //Entweder Automatische Mail oder Outlook
    {
        $Absender = 'karriere@de.atu.eu';                     // Absender der Mails
        $Betreff = '';
        $EmpfaengerCC = '';

        if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) {

            //Email nehmen, wenn keine Email hinterlegt, schick an shuttle.
            $Empfaenger = $rsMail->FeldInhalt('HRK_EMAIL') == ''?'shuttle@de.atu.eu':$rsMail->FeldInhalt('HRK_EMAIL');
        } else {
            $Empfaenger = 'simone.stoeberl@de.atu.eu';
            //$Empfaenger = 'pascal.pirschel@de.atu.eu';
            $EmpfaengerCC = 'sonja.wenzl@de.atu.eu';
        }

        $EmpfaengerBCC = '';

        $Betreff .= $rsMail->FeldInhalt('MVT_BETREFF');
        $Betreff = str_replace('#FIRMIERUNG#', $rsMail->FeldInhalt('HRF_EMAIL_BEZEICHNUNG'), $Betreff);
        $Firmierung = $rsMail->Feldinhalt('HRF_KEY');

        if (isset($_POST['cmdStatusWeiterM'])) //Mail �ber System
        {
            $Mail = new awisMailer($DB, $AWISBenutzer);          // Mailer-Klasse instanzieren

            $Text = $rsMail->FeldInhalt('MVT_TEXT');
            $Text = str_replace('#GEEHERT#', $rsMail->FeldInhalt('ANR_ANREDE') == 'Herr'?'geehrter':'geehrte', $Text);
            $Text = str_replace('#ANREDE#', $rsMail->FeldInhalt('ANR_ANREDE'), $Text);
            $Text = str_replace('#HRK_TITEL#', $rsMail->FeldInhalt('HRK_TITEL'), $Text);

            $Text = str_replace('#HRK_VORNAME#', $rsMail->FeldInhalt('HRK_VORNAME'), $Text);
            $Text = str_replace('#HRK_NACHNAME#', $rsMail->FeldInhalt('HRK_NACHNAME'), $Text);

            $Signatur = '';
            //Textkonserven haben hinten eine Nummer f�r die Firmierung. Wenn die da ist, nimm die, wenn ned Standard ATU Signatur.
            if (isset($MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_AZUBI_' . $Firmierung])) {
                $Signatur = $MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_AZUBI_' . $Firmierung];
            } else {
                $Signatur = $MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_AZUBI_1'];
            }

            $Signatur = "<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>" . $Signatur . " <o:p></o:p></span></font></p>";

            $Text .= $Signatur;

            $Mail->AnhaengeLoeschen();
            $Mail->LoescheAdressListe();
            $Mail->DebugLevel(0);
            $Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
            $Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
            $Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
            $Mail->Absender($Absender);
            $Mail->Betreff($Betreff);
            $Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
            $Mail->SetzeBezug('BWH', $AWIS_KEY2);
            $Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
            $Mail->MailInWarteschlange();
            //$Mail->MailSenden();
        } elseif (isset($_POST['cmdStatusWeiterO'])) //Mail �ber Outlook
        {

            $Text = $MailSprachKonserven['BEW']['BEW_PDF_ABSAGE_AZUBI_TEXT'];

            $Signatur = '';
            //Textkonserven haben hinten eine Nummer f�r die Firmierung. Wenn die da ist, nimm die, wenn ned Standard ATU Signatur.
            if (isset($MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_' . $Firmierung])) {
                $Signatur = $MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_' . $Firmierung];
            } else {
                $Signatur = $MailSprachKonserven['BEW']['BEW_MAIL_SIGNATUR_1'];
            }

            //Leider aufgrund der L�ngenbeschr�nkung der URL nicht m�glich.
            //$Text .= $Signatur;

            $Text = str_replace('<br>', '', $Text); //BRs rausschmei�en, weil die im Mailtext mit drin sind. 

            $Text = str_replace('#GEEHERT#', $rsMail->FeldInhalt('ANR_ANREDE') == 'Herr'?'geehrter':'geehrte', $Text);
            $Text = str_replace('#ANREDE#', $rsMail->FeldInhalt('ANR_ANREDE'), $Text);
            $Text = str_replace('#HRK_TITEL#', $rsMail->FeldInhalt('HRK_TITEL'), $Text);

            $Text = str_replace('#HRK_VORNAME#', $rsMail->FeldInhalt('HRK_VORNAME'), $Text);
            $Text = str_replace('#HRK_NACHNAME#', $rsMail->FeldInhalt('HRK_NACHNAME'), $Text);

            $Text = strip_tags($Text);

            $Text = rawurlencode($Text);

            $Betreff = rawurlencode($Betreff);

            $Aktionsmeldung = '<a href="mailto:' . $Empfaenger . '?subject=' . $Betreff . ($EmpfaengerCC != ''?'&amp;cc=' . $EmpfaengerCC:'') . ($EmpfaengerBCC != ''?'&bcc=' . $EmpfaengerBCC:'') . '&amp;body=' . $Text . '">
             <br>Email �ffnen</a>';
        }
    } elseif (isset($_POST['cmdStatusWeiterS'])) //Serienbrief
    {
        //ob_start();
        $_GET['XRE'] = '58';
        $_GET['ID'] = 'bla';
        if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) {
            $Aktionsmeldung .= '<br><a href="/berichte/drucken.php?XRE=58&ID=1&BWH_KEY=' . $AWIS_KEY2 . '"> Serienbrief �ffnen </a>';
        } else {
            $Aktionsmeldung .= '<br><a href="/berichte/drucken.php?XRE=58&ID=1&BWH_KEY=' . $AWIS_KEY2 . '"> Serienbrief �ffnen </a>';
        }
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
}

?>