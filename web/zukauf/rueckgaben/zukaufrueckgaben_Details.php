<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $DB;
global $AWIS_KEY1;
global $AWIS_KEY2;
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUL','%');
	$TextKonserven[]=array('ZUB','tit_Zukaufbestellung');
	$TextKonserven[]=array('ZUB','ZUB_WANRKORR');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('ZRU','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','ZUL_ENTLASTUNG');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10050 = $AWISBenutzer->HatDasRecht(10050);
	if($Recht10050==0)
	{
	    awisEreignis(3,1000,'Zukaufrueckgaben',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));
	
	$AWISWerkzeug = new awisWerkzeuge();

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = Array();
		
		$Param['ZUL_ARTIKELNUMMER'] = $_POST['sucZUL_ARTIKELNUMMER'];
		$Param['ZUL_FIL_ID'] = $_POST['sucZUL_FIL_ID'];
		$Param['ZUL_LIE_NR'] = $_POST['sucZUL_LIE_NR'];
		$Param['ZLH_KEY'] = $_POST['sucZLH_KEY'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];
		$Param['ZUL_LIEFERSCHEINNR'] = $_POST['sucZUL_LIEFERSCHEINNR'];
		$Param['ZUL_WANR'] = $_POST['sucZUL_WANR'];
		$Param['ZUL_ENTLASTUNG'] = $_POST['sucZUL_ENTLASTUNG'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufrueckgaben_loeschen.php');
	}
	elseif(isset($_POST['cmdSenden_x']))
	{
		include('./zukaufrueckgaben_senden.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufrueckgaben_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['ZUL_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZUL_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$Param['BLOCK']=1;
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZUL_DATUM DESC';
			$Param['ORDER']='ZUL_DATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL = 'SELECT ZUKAUFLIEFERSCHEINE.*, Zukaufbestellungen.*, ZAL_ZLA_KEY, MWS_SATZ, ZLA_ARTIKELNUMMER, ZLH_KEY, ZLH_KUERZEL, ZLH_BEZEICHNUNG';
    $SQL .= ', FIL_BEZ, COALESCE(ZWA_MENGE,0) AS ZWA_MENGE, LIE_NAME1';
    $SQL .= ', (ZUL_LIEFERMENGE-COALESCE(ZWA_MENGE,0)) AS OFFEN';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZUKAUFLIEFERSCHEINE';
	$SQL .= ' INNER JOIN LIEFERANTEN ON ZUL_LIE_NR = LIE_NR';
	$SQL .= ' INNER JOIN Filialen ON ZUL_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN Zukaufbestellungen ON ZUL_ZUB_KEY = ZUB_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUL_ZAL_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKEL ON ZLA_KEY = ZAL_ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN MEHRWERTSTEUERSAETZE ON MWS_ID = ZUL_MWS_ID';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLH_KEY = ZUL_ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	
	// Leere Lieferscheine werden ignoriert
	$Bedingung .= ' AND ZUL_LIEFERMENGE > 0 ';
	// Wenn einer in Bearbeitung ist, nicht mehr anzeigen
	$Bedingung .= ' AND (ZUL_LIEFERMENGE - COALESCE(ZWA_MENGE,0))<>0';

	// Und auch noch die Summe �ber alle Lieferscheine zu einer Bestellposition ansehen
	$Bedingung .= ' AND (SELECT SUM(ZUL_LIEFERMENGE) FROM Zukauflieferscheine';
	$Bedingung .= '      WHERE ZUL_ZUB_KEY = ZUB_KEY';
	$Bedingung .= '      GROUP BY ZUL_ZUB_KEY) > 0';
	
	$Bedingung .= ' AND EXISTS(';
	$Bedingung .= '      SELECT *';
	$Bedingung .= '      FROM ZUKAUFLIEFERSCHEINE ZUL';
	$Bedingung .= ' 	 LEFT OUTER JOIN Zukaufbestellungen ON ZUL_ZUB_KEY = ZUB_KEY';
	$Bedingung .= '      LEFT OUTER JOIN ZUKAUFWERKSTATTAUFTRAEGE ZWA ON ZWA_ZUB_KEY = ZUB_KEY';
	$Bedingung .= '      WHERE ZUL.ZUL_WANR=Zukauflieferscheine.ZUL_WANR AND ZUL.ZUL_ZAL_KEY = Zukauflieferscheine.ZUL_ZAL_KEY';
	$Bedingung .= '      GROUP BY ZUL.ZUL_WANR, ZUL.ZUL_ZAL_KEY';
	$Bedingung .= '      HAVING SUM(ZUL.ZUL_LIEFERMENGE)  > 0 AND (SUM(ZUL.ZUL_LIEFERMENGE) - SUM(COALESCE(ZWA.ZWA_MENGE,0)))<>0)';
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('ZUL',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZUL = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('ZUL'));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name="frmZukaufLieferscheine" action="./zukaufrueckgaben_Main.php?cmdAktion=Details" method="POST">');

	if($rsZUL->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZUL->AnzahlDatensaetze()>1 AND !isset($_GET['ZUL_KEY']))						// Liste anzeigen
	{
		$Param['KEY']=0;
		
		switch($AWISBenutzer->ParameterLesen('BildschirmBreite'))
		{
			case '800':
				$Breiten['ZUL_ARTIKELBEZEICHNUNG']=150;
				$Laengen['ZUL_ARTIKELBEZEICHNUNG']=18;
				break;
			default:
				$Breiten['ZUL_ARTIKELBEZEICHNUNG']=350;
				$Laengen['ZUL_ARTIKELBEZEICHNUNG']=100;
				break;
		}
		
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_DATUM'],110,'',$Link);

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=OFFEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='OFFEN'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['OffeneMenge'],50,'',$Link);
		
		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort='.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_ARTIKELNUMMER,ZUL_DATUM~'))?'ZUL_ARTIKELNUMMER~,ZUL_DATUM~':'ZUL_ARTIKELNUMMER,ZUL_DATUM~');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'],150,'',$Link);

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_ARTIKELBEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'],$Breiten['ZUL_ARTIKELBEZEICHNUNG'],'',$Link);
		
		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=LIE_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],150,'',$Link,'','L');
		
		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_WANR'],120,'',$Link,'','L');
		
		$Form->ZeileEnde();
		
		$DS=0;
		while(!$rsZUL->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details&ZUL_KEY=0'.$rsZUL->FeldInhalt('ZUL_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),0,110,false,($DS%2),'',$Link,'D','Z',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'));
			$Form->Erstelle_ListenFeld('OFFEN',$rsZUL->FeldInhalt('OFFEN'),0,50,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('ZUL_ARTIKELNUMMER',$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER'),0,150,false,($DS%2),'');
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUL->FeldInhalt('ZLH_KUERZEL'),0,60,false,($DS%2),'','','T','L',$rsZUL->FeldInhalt('ZLH_BEZEICHNUNG'));
			
			$ToolTippText = '';
			$Text = $rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG');
			if(strlen($rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'))>$Laengen['ZUL_ARTIKELBEZEICHNUNG'])
			{
				$Text = substr($rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'),0,$Laengen['ZUL_ARTIKELBEZEICHNUNG']-2).'...';
				$ToolTippText = $rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG');
			}
			$Form->Erstelle_ListenFeld('ZUL_ARTIKELBEZEICHNUNG',$Text,0,$Breiten['ZUL_ARTIKELBEZEICHNUNG'],false,($DS%2),'','','T','L',$ToolTippText);

			$Form->Erstelle_ListenFeld('LIE_NAME1',substr($rsZUL->FeldInhalt('LIE_NAME1'),0,15),0,150,false,($DS%2),'','','T','L',$rsZUL->FeldInhalt('LIE_NAME1'));
			if(($rsZUL->FeldInhalt('ZUL_WANR')!=''
					AND $AWISWerkzeug->BerechnePruefziffer($rsZUL->FeldInhalt('ZUL_WANR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN)===false)
				OR $rsZUL->FeldInhalt('ZUL_WANR')=='')
			{
				$WANr = ($rsZUL->FeldInhalt('ZUB_WANRKORR')!=''?$rsZUL->FeldInhalt('ZUB_WANRKORR'):($rsZUL->FeldInhalt('ZUB_WANR')==''?$rsZUL->FeldInhalt('ZUL_WANR'):$rsZUL->FeldInhalt('ZUB_WANR')));
			}
			else
			{
				$WANr = ($rsZUL->FeldInhalt('ZUL_WANR')==''?'???':$rsZUL->FeldInhalt('ZUL_WANR'));
			}
			$Form->Erstelle_ListenFeld('ZUL_WANR',$WANr.($rsZUL->FeldInhalt('ZUB_WANRKORR')==''?'':'*'),0,120,false,($DS%2),'','','T','L');

			// Entlastungsgrund als Tooltipptext
			if($rsZUL->FeldInhalt('ZUL_ENTLASTUNGGRUND')!='')
			{
				$Form->Erstelle_ListenBild('ohne', 'info', '', '/bilder/icon_info.png',$rsZUL->FeldInhalt('ZUL_ENTLASTUNGGRUND'),($DS%2));
			}
			
			$Form->ZeileEnde();

			$rsZUL->DSWeiter();
			$DS++;
		}

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
		
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->SchaltflaechenEnde();
	}	
	else
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$Param['KEY']=$AWIS_KEY1 = $rsZUL->FeldInhalt('ZUL_KEY');

		$EditModus = ($Recht10050&6);

		$Form->Erstelle_HiddenFeld('ZUL_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		
		
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufrueckgaben_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIE_NR'].':',200);
		$SQL = 'SELECT LIE_NR, LIE_NAME1 ';
		$SQL .= ' FROM LIEFERANTEN';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZUL_LIE_NR',$rsZUL->FeldInhalt('ZUL_LIE_NR'),500,false,$SQL);
		$Form->Erstelle_HiddenFeld('LIE_NR', $rsZUL->FeldInhalt('ZUL_LIE_NR'));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERSCHEINNR'].':',200);
		$Form->Erstelle_TextFeld('ZUL_LIEFERSCHEINNR',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EXTERNEID'].':',200);
		$Form->Erstelle_TextFeld('ZUL_EXTERNEID',$rsZUL->FeldInhalt('ZUL_EXTERNEID'),20,200,false,'','','','T');
		$Form->ZeileEnde();

			// Bestellinformationen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_DATUM'].':',200);
		$Form->Erstelle_TextFeld('ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),20,200,false,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('ZUL_FIL_ID',$rsZUL->FeldInhalt('ZUL_FIL_ID'),5,100,false,'','','','T');
		$Link = ($rsZUL->FeldInhalt('ZUL_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsZUL->FeldInhalt('ZUL_FIL_ID'));
		$Form->Erstelle_TextFeld('#FIL_BEZ',$rsZUL->FeldInhalt('FIL_BEZ'),20,200,false,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WANR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUL_WANR',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_WANR')),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUB_WANRKORR',$rsZUL->FeldInhalt('ZUB_WANRKORR'),20,200,false,'','','','T');
	 	$Form->Erstelle_HiddenFeld('ZUB_WANR',$rsZUL->FeldInhalt('ZUB_WANR'));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_AUFTRAGSNUMMER'].':',200);
	 	$Form->Erstelle_TextFeld('ZUL_AUFTRAGSNUMMER',$rsZUL->FeldInhalt('ZUL_AUFTRAGSNUMMER'),20,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'].':',200);
		$Form->Erstelle_TextFeld('ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),10,200,false,'','','','N3');
		$Form->ZeileEnde();

			// Nummer und Hersteller
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'].':',200);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELNUMMER',$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER'),20,200,false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_HERSTELLER'].':',190,'');
		$Form->Erstelle_TextFeld('ZUL_HERSTELLER',$rsZUL->FeldInhalt('ZUL_HERSTELLER'),7,100,false);
        $Form->Erstelle_TextLabel($rsZUL->FeldInhalt('ZLH_BEZEICHNUNG'),190);
		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELBEZEICHNUNG',$rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'),50,200,false);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ENTLASTUNG'].':',200);
		$Gruende = explode("|",$AWISSprachKonserven['Liste']['ZUL_ENTLASTUNG']);
		$Form->Erstelle_SelectFeld('!ZUL_ENTLASTUNG',$rsZUL->FeldInhalt('ZUL_ENTLASTUNG'),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$Gruende);
		if($EditModus)
		{
			$AWISCursorPosition = 'txtZUL_ENTLASTUNG';
		}

		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ENTLASTUNGGRUND'].':',200);
		$Form->Erstelle_TextArea('!ZUL_ENTLASTUNGGRUND',$rsZUL->FeldInhalt('ZUL_ENTLASTUNGGRUND'),600,80,5,$EditModus);
		$Form->ZeileEnde();

		if($AWIS_KEY1>0)
		{
			switch($rsZUL->FeldInhalt('ZUL_ENTLASTUNG'))
			{
				case 1:		// Teiler�ckgabe
				
					$Form->Formular_Ende();
					$Reg = new awisRegister(10055);
					$Reg->ZeichneRegister('');
					break;
					
				case 2:		// WA-Nummer �ndern
					
					$Form->Erstelle_HiddenFeld('ZUB_KEY', $rsZUL->FeldInhalt('ZUB_KEY'));
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',200);
					$Form->Erstelle_TextFeld('!ZUB_WANRKORR',$rsZUL->FeldInhalt('ZUB_WANRKORR'),11,100,$EditModus,'','','','T','','','',11);
					$Form->ZeileEnde();					
					
					$Form->Formular_Ende();

					//***************************************
					// Schaltfl�chen f�r dieses Register
					//***************************************
					$Form->SchaltflaechenStart();
					
					$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
					
					if(($Recht10050&6)!=0 AND $DetailAnsicht)
					{
						$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
					}
					
					if(($Recht10050&6)!=0 AND $DetailAnsicht AND $AWIS_KEY2>0)
					{
						$Form->Schaltflaeche('href', 'cmdSenden', './zukaufrueckgaben_senden.php?ZRU_KEY='.$AWIS_KEY2, '/bilder/cmd_antenne.png', $AWISSprachKonserven['Wort']['lbl_senden'], 'M');
					}
					
					$Form->SchaltflaechenEnde();
						
					break;
				default:
					// Beim ersten Aufruf ohne Entlastungsgrund
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
					if(($Recht10050&6)!=0 AND $DetailAnsicht)
					{
						$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
					}
					$Form->SchaltflaechenEnde();
						
			}
		}

	}

	$Form->SchreibeHTMLCode('</form>');
	
	// Zum Schluss die Parameter speichern, wenn bisher alls ohne Fehler ablief
	$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));
	
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZUL_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZUL_ARTIKELNUMMER']) AND $Param['ZUL_ARTIKELNUMMER']!='')
	{
		$Bedingung .= ' AND (ZUL_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUL_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . '';
		$Bedingung .= ' OR ZLA_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUL_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['ZLH_KEY']) AND $Param['ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZUL_HERSTELLER';
		$Bedingung .= ' )';
	}
	if(isset($Param['ZUL_FIL_ID']) AND $Param['ZUL_FIL_ID']!='')
	{
		$DB->SetzeBindevariable('ZUL', 'var_N0_ZUL_FIL_ID', $Param['ZUL_FIL_ID'],awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND ZUL_FIL_ID = :var_N0_ZUL_FIL_ID';
	}

	if(isset($Param['ZUL_LIE_NR']) AND $Param['ZUL_LIE_NR']!='')
	{
		$DB->SetzeBindevariable('ZUL', 'var_N0_ZUL_LIE_NR', $Param['ZUL_LIE_NR'],awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND ZUL_LIE_NR = :var_N0_ZUL_LIE_NR';
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		$Bedingung .= ' AND ZUL_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSZ_SORTIMENT']) . ' ';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND ZUL_DATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND ZUL_DATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}

	if(isset($Param['ZUL_EXTERNEID']) AND $Param['ZUL_EXTERNEID']!='')
	{
		$Bedingung .= ' AND ZUL_EXTERNEID ' . $DB->LIKEoderIST($Param['ZUL_EXTERNEID'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['ZUL_WANR']) AND $Param['ZUL_WANR']!='')
	{
		$Bedingung .= ' AND ZUL_WANR ' . $DB->LIKEoderIST($Param['ZUL_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	if(isset($Param['ZUL_ZSZ_KEY']) AND $Param['ZUL_ZSZ_KEY']>0)
	{
		$Bedingung .= ' AND ZUL_ZSZ_KEY= ' . intval($Param['ZUL_ZSZ_KEY']) . ' ';
	}
	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		//$Bedingung .= ' AND (ZAL_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZUL_TAUSCHTEIL']) AND $Param['ZUL_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZUL_TAUSCHTEIL = ' . intval($Param['ZUL_TAUSCHTEIL']) . ' ';
	}

	if(isset($Param['ZUL_ENTLASTUNG']) AND $Param['ZUL_ENTLASTUNG']!=0)
	{
	    if($Param['ZUL_ENTLASTUNG']==-1)
	    {
	        $Bedingung .= ' AND ZUL_ENTLASTUNG IS NULL';
	    }
	    else
	    {
		    $Bedingung .= ' AND ZUL_ENTLASTUNG = ' . intval($Param['ZUL_ENTLASTUNG']) . ' ';
	    }
	}

	$Bedingung .= ' AND ZUL_ABSCHLUSSART IS NULL';
	$Bedingung .= ' AND COALESCE(ZUB_ABSCHLUSSART,1) = 1';
	
	
	
	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>