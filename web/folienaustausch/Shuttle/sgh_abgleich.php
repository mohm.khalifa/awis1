<?php

require_once ('awisDatenbank.inc');
require_once ('awisBenutzer.inc');

ini_set ( 'max_execution_time', 0 );

date_default_timezone_set ( 'Europe/Berlin' );

error_reporting ( "E_ALL" );

class SGHAbgleich {
	
	protected $_AWISBenutzer;
	protected $_DB;
	
	public function __construct() 
	{
		$this->_AWISBenutzer = awisBenutzer::Init ();
		$this->_DB = awisDatenbank::NeueVerbindung ('AWIS');
		$this->_DB->Oeffnen ();
	}
	
	public function starteAbgleich() 
	{
		$this->Abgleich();
	}
	
	protected function Abgleich()
	{
		try 
		{
			$this->_DB->TransaktionBegin(); 
			
			$SQL  = 'SELECT * from SGHDIENSTLEISTER';
			
			$DSAnz = $this->_DB->ErmittleZeilenAnzahl($SQL);
			echo 'Anzahl Datens�tze zur Pr�fung: ' .$DSAnz ."\n";
			$rsDienstleister = $this->_DB->RecordSetOeffnen($SQL);
			
			$i = 0;
			while (!$rsDienstleister->EOF()) 
			{
				$i++;
				echo "--------------------------------------------------------------------------------\n";
				echo "Pr�fe WA: " .$rsDienstleister->FeldInhalt ('SGD_WANR') .'('.$i.'/'.$DSAnz.')'."\n";
				
				// lt Herrn Roy Bohn, 13.03.2012, soll f�r die Preisfindung des Artikels das
				// Generierungsdatum des WA herangezogen werden.

				$SQL  = 'SELECT a.*,';
				$SQL .= ' a.SGK_MENGE*a.SGK_UMSATZ AS UmsatzIst,';
				$SQL .= ' a.SGK_MENGE*a.SSD_VK AS UmsatzSoll';
				$SQL .= ' FROM ';
				$SQL .= ' (';
				$SQL .= ' SELECT * ';
				$SQL .= ' FROM SGHKASSENDATEN';
				$SQL .= ' INNER JOIN SGHARTPFLEGE';
				$SQL .= ' ON SSD_ARTNR = SGK_ARTNR'; 
				$SQL .= ' WHERE SGK_WANR = ' .$this->_DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_WANR'));
				$SQL .= ' AND SGK_ARTNR = ' .$this->_DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_ATUNR'));
				$SQL .= ' AND SGK_FILID = ' .$this->_DB->FeldInhaltFormat('T',$rsDienstleister->FeldInhalt('SGD_FILNR'));
				$SQL .= ' AND SSD_EK = ' .$this->_DB->FeldInhaltFormat ( 'N2', $rsDienstleister->FeldInhalt('SGD_BETRAG'));
				$SQL .= ' AND TRUNC(SGK_WAGENDAT) >= TRUNC(SSD_GUELTIGAB) AND TRUNC(SGK_WAGENDAT) <= TRUNC(SSD_GUELTIGBIS)';
				$SQL .= ' ) a';
		
				$rsAbgleich = $this->_DB->RecordSetOeffnen($SQL);

				if ((!$rsAbgleich->EOF()) and ($rsAbgleich->AnzahlDatensaetze() == 1))
				{
					echo "Kassendatensatz gefunden\n";
					
					if ($rsAbgleich->FeldInhalt('UmsatzIst') == $rsAbgleich->FeldInhalt('UmsatzSoll')) 
					{
						echo "Abgleich m�glich WA: " .$rsDienstleister->FeldInhalt ('SGD_WANR') ."\n";
						
						$SQL = "Select * from SGHDIENSTLEISTERABGL WHERE SGS_RNRSGH=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_RNRSGH' ) );
						$SQL .= " AND SGS_RNRDAT=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsDienstleister->FeldInhalt ( 'SGD_RNRDAT' ) );
						$SQL .= " AND SGS_FILNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_FILNR' ) );
						$SQL .= " AND SGS_WANR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_WANR' ) );
						$SQL .= " AND SGS_EINBAUDATUM=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsDienstleister->FeldInhalt ( 'SGD_EINBAUDATUM' ) );
						$SQL .= " AND SGS_ATUNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_ATUNR' ) );
						$SQL .= " AND SGS_MENGE=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsDienstleister->FeldInhalt ( 'SGD_MENGE' ) );
						$SQL .= " AND SGS_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' ) );
						$SQL .= " AND SGS_KFZKENNZ=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_KFZKENNZ' ) );
						$SQL .= " AND SGS_MAPPE=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_MAPPE' ) );
						$SQL .= " AND SGS_LIEFERANTENNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_LIEFERANTENNR' ) );
						
						$rsDienstleisterAbgl = $this->_DB->RecordSetOeffnen($SQL);
						
						if ($rsDienstleisterAbgl->EOF()) 
						{
							echo "Insert in SGHDIENSTLEISTERABGL\n";
							
							$SQL = "INSERT INTO SGHDIENSTLEISTERABGL (SGS_RNRSGH,SGS_RNRDAT,SGS_RNRTRM,SGS_FILNR,";
							$SQL .= "SGS_WANR,SGS_EINBAUDATUM,SGS_ATUNR,SGS_MENGE,SGS_BETRAG,SGS_KFZKENNZ,";
							$SQL .= "SGS_MAPPE,SGS_LIEFERANTENNR,SGS_USER,SGS_USERDAT) VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_RNRSGH' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsDienstleister->FeldInhalt ( 'SGD_RNRDAT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_RNRTRM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_FILNR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_WANR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsDienstleister->FeldInhalt ( 'SGD_EINBAUDATUM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_ATUNR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsDienstleister->FeldInhalt ( 'SGD_MENGE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_KFZKENNZ' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_MAPPE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsDienstleister->FeldInhalt ( 'SGD_LIEFERANTENNR' ), false );
							$SQL .= ",'AUTOABGLEICH'";
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL,'',true);
							
							$SQL = "SELECT seq_SGS_KEY.CurrVal AS KEY FROM DUAL";
							$rsSeqSGSKey = $this->_DB->RecordSetOeffnen ($SQL);
							$SGS_KEY = $rsSeqSGSKey->FeldInhalt('KEY');
						}
						else
						{
							echo "Eintrag vorhanden SGHDIENSTLEISTERABGL\n";
							$SGS_Key = $rsDienstleisterAbgl->FeldInhalt('SGS_KEY');
						}
						
						$SQL = "Select * from SGHKASSENDATENABGL WHERE SKS_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsAbgleich->FeldInhalt ( 'SGK_FILID' ) );
						$SQL .= " AND SKS_DATUM=" . $this->_DB->FeldInhaltFormat ( 'DU', $rsAbgleich->FeldInhalt ( 'SGK_DATUM' ) );
						$SQL .= " AND SKS_ZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_ZEIT' ) );
						$SQL .= " AND SKS_BSA=" . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_BSA' ) );
						$SQL .= " AND SKS_WANR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_WANR' ) );
						$SQL .= " AND SKS_ARTNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_ARTNR' ) );
						$SQL .= " AND SKS_MENGE=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsAbgleich->FeldInhalt ( 'SGK_MENGE' ) );
						$SQL .= " AND SKS_UMSATZ=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsAbgleich->FeldInhalt ( 'SGK_UMSATZ' ) );
						$SQL .= " AND SKS_KFZKENNZ=" . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_KFZKENNZ' ) );
						$SQL .= " ORDER BY SKS_KEY DESC";
						
						$rsKassendatenAbgl = $this->_DB->RecordSetOeffnen($SQL);
						
						if ($rsKassendatenAbgl->EOF())
						{
							echo "Insert in SGHKASSENDATENABGL\n";
							
							$SQL = "INSERT INTO SGHKASSENDATENABGL (SKS_FILID,SKS_DATUM,SKS_ZEIT,";
							$SQL .= "SKS_BSA,SKS_WANR,SKS_ARTNR,SKS_MENGE,SKS_UMSATZ,";
							$SQL .= "SKS_KFZKENNZ,SKS_DWHAUDIT,SKS_DWHINSDATE,SKS_WAGENDAT,SKS_USER,SKS_USERDAT) VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'NO', $rsAbgleich->FeldInhalt ( 'SGK_FILID' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsAbgleich->FeldInhalt ( 'SGK_DATUM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_ZEIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_BSA' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_WANR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_ARTNR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsAbgleich->FeldInhalt ( 'SGK_MENGE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsAbgleich->FeldInhalt ( 'SGK_UMSATZ' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAbgleich->FeldInhalt ( 'SGK_KFZKENNZ' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsAbgleich->FeldInhalt ( 'SGK_DWHAUDIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsAbgleich->FeldInhalt ( 'SGK_DWHINSDATE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsAbgleich->FeldInhalt ( 'SGK_WAGENDAT' ), false );
							$SQL .= ",'AUTOABGLEICH'";
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL,'',true);
							
							$SQL = "SELECT seq_SKS_KEY.CurrVal AS KEY FROM DUAL";
							$rsSeqSKSKey = $this->_DB->RecordSetOeffnen ($SQL);
							$SKS_KEY = $rsSeqSGSKey->FeldInhalt('KEY');
						}
						else
						{
							echo "Eintrag vorhanden SGHKASSENDATENABGL\n";
							$SKS_KEY = $rsKassendatenAbgl->FeldInhalt('SKS_KEY');
						}
						
						echo "Insert in SGHABGLEICH\n";
						
						$SQL = "INSERT INTO SGHABGLEICH (SGA_SGS_KEY,SGA_SKS_KEY,SGA_TYP,";
						$SQL .= "SGA_ABGLEICHDATUM,SGA_USER,SGA_USERDAT) VALUES (";
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'NO', $SGS_KEY, false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $SKS_KEY, false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '1', false );
						$SQL .= ',SYSDATE';
						$SQL .= ",'AUTOABGLEICH'";
						$SQL .= ',SYSDATE';
						$SQL .= ')';
						
						$this->_DB->Ausfuehren($SQL,'',true);
	
						echo "Delete from SGHDIENSTLEISTER\n";
						
						$SQL  = 'Delete from SGHDIENSTLEISTER';
						$SQL .= ' WHERE SGD_KEY=' .$this->_DB->FeldInhaltFormat('NO',$rsDienstleister->FeldInhalt('SGD_KEY'));
		
						$this->_DB->Ausfuehren($SQL,'',true);
						
						echo "Delete from SGHKASSENDATEN\n";
						
						$SQL  = 'Delete from SGHKASSENDATEN';
						$SQL .= ' WHERE SGK_KEY=' .$this->_DB->FeldInhaltFormat('NO',$rsAbgleich->FeldInhalt('SGK_KEY'));
		
						$this->_DB->Ausfuehren ($SQL,'',true);
					}
					else
					{
						echo "Differenz vorhanden WA: " .$rsDienstleister->FeldInhalt ('SGD_WANR') ."\n";
						echo "  Menge.......: " .$rsAbgleich->FeldInhalt('SGK_MENGE') ."\n";
						echo "  Kassenumsatz: " .$rsAbgleich->FeldInhalt('UmsatzIst') ."\n";
						echo "  Sollumsatz..: " .$rsAbgleich->FeldInhalt('UmsatzSoll') ."\n";
					}
				}
				elseif ($rsAbgleich->AnzahlDatensaetze() > 1)
				{
					echo "mehrere Kassendatens�tze (";
					echo $rsAbgleich->AnzahlDatensaetze().") zum WA gefunden, kein automatischer Abgleich\n";
				}
				else
				{
					echo "Kassendatensatz nicht gefunden\n";
				}
							
				$rsDienstleister->DSWeiter();
			}

			//$this->_DB->TransaktionRollback();
			$this->_DB->TransaktionCommit();
						
		} //endTry
		
		catch (awisException $e)
		{
			echo $e->getMessage(). '/' . $e->getSQL();
			$this->_DB->TransaktionRollback();
		}
		catch (Exception $e)
		{
			echo $e->getMessage() . '/' . $e->getCode();
			$this->_DB->TransaktionRollback();
		}
		
	}//End of Abgleich

}//End of class

?>
