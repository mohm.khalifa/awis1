<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************
	// Filialebenenrollenbereiche
	//***********************************************
//	$Form->DebugAusgabe(1,$_POST);

	$AWIS_KEY1=$_POST['txtPEB_KEY'];

	$Felder = $Form->NameInArray($_POST, 'txtPEB_',1,1);

	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('PEB','PEB_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtPEB_KEY'])==0)
		{
			$Speichern = true;

			if($Speichern)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO perseinsbereiche';
				$SQL .= '(PEB_BEZEICHNUNG,PEB_BEMERKUNG';
				$SQL .= ',PEB_USER, PEB_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPEB_BEZEICHNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPEB_BEMERKUNG'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if($DB->Ausfuehren($SQL)===false)
				{
					die();
				}
				$SQL = 'SELECT seq_PEB_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
			}
		}
		else 					// gešnderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsPEB = $DB->RecordSetOeffnen('SELECT * FROM perseinsbereiche WHERE PEB_key=' . $_POST['txtPEB_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPEB->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPEB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPEB->FeldInfo($FeldName,'TypKZ'),$rsPEB->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPEB->FeldInhalt('PEB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE perseinsbereiche SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PEB_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PEB_userdat=sysdate';
				$SQL .= ' WHERE PEB_key=0' . $_POST['txtPEB_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}

	$Felder = $Form->NameInArray($_POST, 'txtPBM_',1,1);
	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('PBM','PBM_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtPBM_KEY'])==0)
		{
			$Speichern = true;

			if($Speichern)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO perseinsbereichemitglieder';
				$SQL .= '(PBM_PEB_KEY, PBM_XBN_KEY, PBM_GUELTIGAB, PBM_GUELTIGBIS';
				$SQL .= ',PBM_USER, PBM_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' '.$AWIS_KEY1;
				$SQL .= ',' . $DB->FeldInhaltFormat('N',$_POST['txtPBM_XBN_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtPBM_GUELTIGAB'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtPBM_GUELTIGBIS'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if($DB->Ausfuehren($SQL)===false)
				{
					die();
				}
				$SQL = 'SELECT seq_PBM_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
		}
		else 					// gešnderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsPBM = $DB->RecordSetOeffnen('SELECT * FROM perseinsbereichemitglieder WHERE PBM_key=' . $_POST['txtPBM_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPBM->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPBM->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPBM->FeldInfo($FeldName,'TypKZ'),$rsPBM->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPBM->FeldInhalt('PBM_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE perseinsbereichemitglieder SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PBM_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PBM_userdat=sysdate';
				$SQL .= ' WHERE PBM_key=0' . $_POST['txtPBM_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200809181323,$SQL,2);
				}
			}
			$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_POST['txtPBM_KEY']);
		}

		//**************************************
		//* Rechtevergabe
		//**************************************
		if(isset($_POST['txtPBM_KEY']))
		{
			$SQL = 'DELETE FROM PERSEINSBEREICHERECHTEVERGABE WHERE PBR_PBM_KEY=0'.$AWIS_KEY2;
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809300904,$SQL,2);
			}
		}
		$Felder = $Form->NameInArray($_POST, 'txtPBR_KEY_',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			foreach($Felder as $Feld)
			{
				$FeldInfos = explode('_',$Feld);
				$SQL = 'INSERT INTO PERSEINSBEREICHERECHTEVERGABE';
				$SQL .= '(PBR_PBM_KEY, PBR_PBZ_KEY, PBR_USER, PBR_USERDAT)';
				$SQL .= ' VALUES(';
				$SQL .= ' '.$AWIS_KEY2;
				$SQL .= ','.$DB->FeldInhaltFormat('N0',$FeldInfos[3],false);
				$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', sysdate)';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200809300904,$SQL,2);
				}
			}
		}
		$AWIS_KEY2=0;		// Detailseite nicht mehr anzeigen
	}	// Ende Mitglieder (mit Rechten)

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>