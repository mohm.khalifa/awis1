<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try {
    $TextKonserven = array();
    $TextKonserven[] = array('BES', '*');
    $TextKonserven[] = array('FIL', 'FIL_GEBIET');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht40015 = $AWISBenutzer->HatDasRecht(40015);

    if ($Recht40015 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->SchreibeHTMLCode('<form name="frmSBRsuche" action="./schadfilrueck_Main.php?cmdAktion=Auswertungen" method="POST"  >');
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES_Auswertung'));

    $Form->Formular_Start();

    /**
     * Filialfeld
     **/

    $SQL = 'select feb_key,feb_bezeichnung from v_filialebenenrollen_aktuell';
    $SQL .= ' inner join Filialebenen on xx1_feb_key = feb_key';


        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'] . ':', 200);
        $Form->Erstelle_TextFeld('!*SBR_FIL_ID', ($Param['SPEICHERN'] == 'on'?$Param['FIL_ID']:''), 10, 150, true, '', '', '', 'T', 'L', '', '', 10);
        $Form->ZeileEnde();
        $AWISCursorPosition = 'sucSBR_FIL_ID';

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['DATUM_VON'] . ':', 200);
        $Form->Erstelle_TextFeld('*DATUM_VOM', (isset($Param['SPEICHERN']) && $Param['SPEICHERN'] == 'on'?$Param['DATUM_VOM']:getdate()['mday'].'.'.getdate()['mon'].'.'.(getdate()['year']-1)), 10, 200, true, '', '', '', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['DATUM_BIS'] . ':', 200);
        $Form->Erstelle_TextFeld('*DATUM_BIS', (isset($Param['SPEICHERN']) && $Param['SPEICHERN'] == 'on'?$Param['DATUM_BIS']:getdate()['mday'].'.'.getdate()['mon'].'.'.getdate()['year']), 10, 200, true, '', '', '', 'D');
        $Form->ZeileEnde();

        // Auswahl kann gespeichert werden
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
        $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 20, true, 'on');
        $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdDrucken', '', '/bilder/cmd_pdf.png');
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>