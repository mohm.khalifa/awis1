<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $DB;
global $AWIS_KEY1;
global $AWIS_KEY2;
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('MIS','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

$Form->DebugAusgabe(1,$_POST);
//die();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht24000 = $AWISBenutzer->HatDasRecht(24000);
	if($Recht24000==0)
	{
	    awisEreignis(3,1000,'Zukaufrueckgaben',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_MIS'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = Array();
		
		$Param['KFZKennzeichen'] = $_POST['sucKFZKennzeichen'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$Param['BLOCK']=1;
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY FILIALE, KAUF_JAHR, KAUF, KAUFZEIT, BSA';
			$Param['ORDER']='FILIALE, KAUF_JAHR, KAUF, KAUFZEIT, BSA';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL = 'SELECT a.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ODS.FK_UMSATZ@DWH a';
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
//$Form->DebugAusgabe(1,$SQL);
//die();
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('MIS',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
$Form->DebugAusgabe(1,$SQL);
//die();
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
$Form->DebugAusgabe(1,$SQL);
//die();

	$rsMIS = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('MIS'));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name="frmMIS" action="./mis_Main.php?cmdAktion=Details" method="POST">');

	if($rsMIS->EOF())
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	else // Liste anzeigen
	{
		$Param['KEY']=0;
		
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FILIALE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FILIALE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['FILIALE'],50,'',$Link);

		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KAUF_JAHR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KAUF_JAHR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['KAUF_JAHR'],40,'',$Link);
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KAUF'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KAUF'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['KAUF'],50,'',$Link);
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KAUFZEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KAUFZEIT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['KAUFZEIT'],70,'',$Link,'','C');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=BSA'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BSA'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['BSA'],40,'',$Link);
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KUNDEN_NR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KUNDEN_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['KUNDEN_NR'],80,'',$Link);

		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['ATUNR'],65,'',$Link);
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=BETRAG_NETTO'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BETRAG_NETTO'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['BETRAG_NETTO'],80,'',$Link,'','R');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=URSPRUNGSBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='URSPRUNGSBETRAG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['URSPRUNGSBETRAG'],80,'',$Link,'','R');

		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=TEXT1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TEXT1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['TEXT1'],90,'',$Link);
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=MENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['MENGE'],50,'',$Link,'','R');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=CARMASTERDATA'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CARMASTERDATA'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['CARMASTERDATA'],80,'',$Link,'','R');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FAHRZEUGTYP'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FAHRZEUGTYP'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['FAHRZEUGTYP'],40,'',$Link,'','R');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KENNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KENNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['KENNUNG'],40,'',$Link,'','R');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=LKZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LKZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['LKZ'],50,'',$Link,'','L');
		
		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=LIEFERSCHEIN_NR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIEFERSCHEIN_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['LIEFERSCHEIN_NR'],80,'',$Link);

		$Link = './mis_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=RECHNUNGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RECHNUNGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MIS']['RECHNUNGSNUMMER'],100,'',$Link);
		
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsMIS->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			//$Form->Erstelle_ListenFeld('ZUL_DATUM',$rsMIS->FeldInhalt('ZUL_DATUM'),0,130,false,($DS%2),'','','D','Z',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'));
			$Form->Erstelle_ListenFeld('FILIALE',$rsMIS->FeldInhalt('FILIALE'),0,50,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('KAUF_JAHR',$rsMIS->FeldInhalt('KAUF_JAHR'),0,40,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('KAUF',$rsMIS->FeldInhalt('KAUF'),0,50,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('KAUFZEIT',$rsMIS->FeldInhalt('KAUFZEIT'),0,70,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('BSA',$rsMIS->FeldInhalt('BSA'),0,40,false,($DS%2),'','','T','C');
			$Form->Erstelle_ListenFeld('KUNDEN_NR',$rsMIS->FeldInhalt('KUNDEN_NR'),0,80,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('ATUNR',$rsMIS->FeldInhalt('ATUNR'),0,65,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('BETRAG_NETTO',$Form->Format ('N2',$rsMIS->FeldInhalt('BETRAG_NETTO')),0,80,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('URSPRUNGSBETRAG',$Form->Format ('N2',$rsMIS->FeldInhalt('URSPRUNGSBETRAG')),0,80,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('TEXT1',$rsMIS->FeldInhalt('TEXT1'),0,90,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('MENGE',$rsMIS->FeldInhalt('MENGE'),0,50,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('CARMASTERDATA',$rsMIS->FeldInhalt('CARMASTERDATA'),0,80,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('FAHRZEUGTYP',$rsMIS->FeldInhalt('FAHRZEUGTYP'),0,40,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('KENNUNG',$rsMIS->FeldInhalt('KENNUNG'),0,40,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('LKZ',$rsMIS->FeldInhalt('LKZ'),0,50,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('LIEFERSCHEIN_NR',$rsMIS->FeldInhalt('LIEFERSCHEIN_NR'),0,80,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('RECHNUNGSNUMMER',$rsMIS->FeldInhalt('RECHNUNGSNUMMER'),0,100,false,($DS%2),'','','T','L');
			
			$Form->ZeileEnde();

			$rsMIS->DSWeiter();
			$DS++;
		}

		$Link = './zukaufrueckgaben_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	
	// Zum Schluss die Parameter speichern, wenn bisher alls ohne Fehler ablief
	$AWISBenutzer->ParameterSchreiben('Formular_MIS',serialize($Param));
	
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201210081407");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201210081408");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if(isset($Param['KFZKennzeichen']) AND $Param['KFZKennzeichen']!='')
	{
		$Bedingung .= ' AND TEXT1 ' . $DB->LIKEoderIST($Param['KFZKennzeichen'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>