<?php
require_once('awisServiceleistungen.inc');
require_once('awisBenutzer.inc');

try {
    $Benutzer = '';
    $DebugLevel = 0;

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                echo 'Debug auf ' . $DebugLevel . ' setzen...' . PHP_EOL;
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }

    $AWISUser = awisBenutzer::init($Benutzer);

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $X = new awisServiceleistungen($Benutzer);

    $X->DebugLevel($DebugLevel);
    // $X->ExportiereDaten(awisServiceleistungen::EXPORT_FLEETINOVATION);
    $X->ImportiereRueckmeldungen(awisServiceleistungen::EXPORT_FLEETINOVATION);
} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'FEHLERZeile: ' . $ex->getLine() . PHP_EOL;
}
?>

