<?php
require_once('awisFilialen.inc');
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FER','%');
$TextKonserven[]=array('FRZ','%');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht122 = awisBenutzerRecht($con,122,$AWISBenutzer->BenutzerName());
if($Recht122==0)
{
    awisEreignis(3,1000,'Filialbetreuung',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//********************************************************
// Daten suchen
//********************************************************

awis_FORM_FormularStart();

$Filiale = new awisFilialen($AWIS_KEY1);
$BetreuerListe = $Filiale->BetreuerListe(0);

awis_FORM_ZeileStart();

awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],250);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_BEMERKUNG'],350);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'],100);


awis_FORM_ZeileEnde();

$DS = 0;
foreach($BetreuerListe AS $Betreuer)
{
	awis_FORM_ZeileStart();

	awis_FORM_Erstelle_ListenFeld('#FER_BEZEICHNUNG',$Betreuer['FER_BEZEICHNUNG'],0,200,false,($DS%2),'','','T','L',$Betreuer['FER_BEMERKUNG']);
	$Link = '/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey=0'.$Betreuer['KON_KEY'];
	awis_FORM_Erstelle_ListenFeld('#KON_NAME',$Betreuer['KON_NAME1'].' '.$Betreuer['KON_NAME2'],0,250,false,($DS%2),'',$Link,'T','L');
	awis_FORM_Erstelle_ListenFeld('#FRZ_BEMERKUNG',$Betreuer['FRZ_BEMERKUNG'],0,350,false,($DS%2),'','','T','L');
	awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGAB',$Betreuer['FRZ_GUELTIGAB'],0,100,false,($DS%2),'','','D','L');


	awis_FORM_ZeileEnde();
	$DS++;
}

awis_FORM_FormularEnde();

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>