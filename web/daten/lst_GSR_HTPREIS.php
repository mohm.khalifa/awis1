<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['txtRAV_RAHMENVERTR']))
{
    var_dump($_GET);
    die();
}

try
{
    $DS=0;

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('RAV','%');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Fehler = '';

    $SQL = 'select *';
    $SQL .= ' FROM GSVRAHMENVERTRAEGE V';
    $SQL .= ' inner join GSVLIEFERANTEN L ON GSR_LIEFERANT = GSL_KEY';
    $SQL .= ' WHERE gsr_vertragsart =' .$DB->WertSetzen('GSR','T',$_GET['txtGSR_VERTRAGSART']).'';
    $SQL .= ' AND gsr_key ='.$DB->WertSetzen('GSR','T',$_GET['txtRAV_RAHMENVERTR']).'';

     $rsGSR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSR', true));

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_HTPREIS'] . ':', 120);
    $Form->Erstelle_TextFeld('!RAV_HTPREIS_A', $rsGSR->FeldInhalt('GSR_HTPREIS_A'), 3, 100, false);
    $Form->ZeileEnde();

}
catch (Exception $ex)
{
    $Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>