<?php
global $con;
global $Recht10001;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZAH','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','AktuellesSortiment');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','KeineDatenVorhanden');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10001 = awisBenutzerRecht($con,10001,$AWISBenutzer->BenutzerName());		// Artikelstamm
$Recht10002 = awisBenutzerRecht($con,10002,$AWISBenutzer->BenutzerName());		// Bestellungen
if(($Recht10001&32)==0)
{
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	die();
}

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$SQL = "SELECT ZAH_ZLA_KEY, TO_CHAR(ZAH_ARCHIVDATUM,'DD.MM.YYYY HH24:MI') AS ARCHIVDATUM";
$SQL .= ', ZAH_WG1, ZAH_WG2, ZAH_WG3, ZAH_EK, ZAH_VK, ZAH_RABATT1, ZAH_RABATT2, ZAH_AST_ATUNR, ZAH_USER';
$SQL .= ' FROM Zukauflieferantenartikelarchiv ';
$SQL .= ' WHERE ZAH_ZLA_KEY=0'.$AWIS_KEY1;

if(!isset($_GET['SSort']))
{
	$SQL .= ' ORDER BY ZAH_ARCHIVDATUM DESC';
}
else 
{
	$SortFelder = explode(';',$_GET['SSort']);
	$OrderBy = '';
	foreach($SortFelder AS $SortFeld)
	{
		$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
	}
	$SQL .= ($OrderBy==''?'':' ORDER BY '.$OrderBy);
}
awis_Debug(1,$SQL);
$rsZAH = awisOpenRecordset($con, $SQL);
$rsZAHZeilen = $awisRSZeilen;

if($rsZAHZeilen>0)
{
	awis_FORM_FormularStart();
	awis_FORM_ZeileStart();
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_ARCHIVDATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_ARCHIVDATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_ARCHIVDATUM'],150,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_USER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_USER'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_USER'],150,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_WG1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG1'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_WG2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG2'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG2'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_WG3'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG3'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG3'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_VK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_VK'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_VK'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_EK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_EK'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_EK'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_RABATT1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_RABATT1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_RABATT1'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_RABATT2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_RABATT2'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_RABATT2'],100,'',$Link);
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Link .= '&SSort=ZAH_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_AST_ATUNR'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_AST_ATUNR'],150,'',$Link);

	awis_FORM_ZeileEnde();
 
		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZAHZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZAHZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufartikel_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['SSort'])?'&SSort='.$_GET['SSort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		
		awis_FORM_ZeileEnde();
	}

	for($ZAHZeile=$StartZeile;$ZAHZeile<$rsZAHZeilen and $ZAHZeile<$StartZeile+$MaxDSAnzahl;$ZAHZeile++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('*ZAH_ARCHIVDATUM',$rsZAH['ARCHIVDATUM'][$ZAHZeile],20,150,false,($ZAHZeile%2),'','','DU');
		awis_FORM_Erstelle_ListenFeld('*ZAH_USER',$rsZAH['ZAH_USER'][$ZAHZeile],20,150,false,($ZAHZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('*ZAH_WG1',$rsZAH['ZAH_WG1'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('*ZAH_WG2',$rsZAH['ZAH_WG2'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('*ZAH_WG3',$rsZAH['ZAH_WG3'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('*ZAH_VK',$rsZAH['ZAH_VK'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','N2');
		awis_FORM_Erstelle_ListenFeld('*ZAH_EK',$rsZAH['ZAH_EK'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','N2');
		awis_FORM_Erstelle_ListenFeld('*ZAH_RABATT1',$rsZAH['ZAH_RABATT1'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','N2');
		awis_FORM_Erstelle_ListenFeld('*ZAH_RABATT2',$rsZAH['ZAH_RABATT2'][$ZAHZeile],20,100,false,($ZAHZeile%2),'','','N2');
		awis_FORM_Erstelle_ListenFeld('*ZAH_AST_ATUNR',$rsZAH['ZAH_AST_ATUNR'][$ZAHZeile],20,150,false,($ZAHZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 
{
	awis_FORM_Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
}

awis_FORM_FormularEnde();

?>