<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $SpeichernFrame1;
global $SpeichernFrame2;
global $SpeichernFrame3;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	$AWISSpeicherKonserven = $Form->LadeTexte($TextKonserven);
	
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$Fehler=false;
	
	$Form = new awisFormular();
	//Wenn die Absicht bestand eine komplette Vertretung anzulegen
	if((isset($_POST['txtSBV_VERTRETER']) and $_POST['txtSBV_VERTRETER'] != $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen']) 
	    or (isset($_POST['txtSBV_ZUVERTRETEN']) and $_POST['txtSBV_ZUVERTRETEN'] != $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen'])) 
	{
        $FehlerFelder = '';
	    //Ist ein Verteter gew�hlt?
	    if((isset($_POST['txtSBV_VERTRETER']) and $_POST['txtSBV_VERTRETER'] == $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen'] ))
	    {
	        $FehlerFelder .= 'Vertreter. '; 
	        $Fehler = true;
	    }
	    //Ist ein zuvertretneder gew�hlt
	    if (isset($_POST['txtSBV_ZUVERTRETEN']) and $_POST['txtSBV_ZUVERTRETEN']  == $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen'] )
	    {

	        $FehlerFelder .= 'Zuvertretender. ';
	        $Fehler = true;
	    } 

	    if(!$Fehler)//Passt alles. -->Speichern
	    {
	        
    	    $SQL  ='INSERT';
    	    $SQL .=' INTO';
    	    $SQL .='   SACHBEARBEITERVERTRETER';
    	    $SQL .='   (';
    	    $SQL .='     SBV_VERTRETER_SB_ID,';
    	    $SQL .='     SBV_ZUVERTRETEN_SB_ID,';
    	    $SQL .='     SBV_GUELTIG_AB,';
    	    $SQL .='     SBV_GUELTIG_BIS,';
    	    $SQL .='     SBV_USER,';
    	    $SQL .='     SBV_USERDAT';
    	    $SQL .='   )';
    	    $SQL .='   VALUES';
    	    $SQL .='   (';
    	    $SQL .= $DBSCHAD->WertSetzen('SBV', 'T', $_POST['txtSBV_VERTRETER']) . ' ,';
    	    $SQL .= $DBSCHAD->WertSetzen('SBV', 'T', $_POST['txtSBV_ZUVERTRETEN']) . ' ,';
    	    $SQL .='    trunc(sysdate),';
    	    $SQL .='     null,';
    	    $SQL .='\''.$AWISBenutzer->BenutzerName().'\',';				
    	    $SQL .='     sysdate';
    	    $SQL .='   )';
    	    
    	    $Form->DebugAusgabe(1,$SQL);
    	    $DBSCHAD->Ausfuehren($SQL,'',false,$DBSCHAD->Bindevariablen('SBV',true));
    	    $SpeichernOK[1] = true;
    	    $SpeichernFrame1 = 'Komplette Vertretung wurde angelegt';
	    }
	    else
	    {
	        $SpeichernOK[1] = false;
	        $SpeichernFrame1=$AWISSpeicherKonserven['Fehler']['err_KeinWert'] . $FehlerFelder;
	    }
	}
	
	//Wenn die Absicht bestand einzelne Wiedervorlagen umzuschreiben
	if((isset($_POST['sucSBV_WV_ID']) and $_POST['sucSBV_WV_ID'] != '')
	    or (isset($_POST['txtSBV_WV_WEITER_AN']) and $_POST['txtSBV_WV_WEITER_AN'] != $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen']))
	{
	    $FehlerFelder = '';
	    //Ist ein Verteter gew�hlt?
	    if((isset($_POST['sucSBV_WV_ID']) and $_POST['sucSBV_WV_ID'] == '' ))
	    {
	        $FehlerFelder .= 'WV-Inhaber. ';
	        $Fehler = true;
	    }
	    //Ist ein zuvertretneder gew�hlt
	    if (isset($_POST['txtSBV_WV_WEITER_AN']) and $_POST['txtSBV_WV_WEITER_AN']  == $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen'] )
	    {
	
	        $FehlerFelder .= 'WV Umschreiben auf. ';
	        $Fehler = true;
	    }
	      
	    if(!$Fehler)//Passt alles. -->Speichern
	    {         
	        //Neuen Sachbearbeitername ermitteln
	        $SQLSB = 'Select sbname from SACHBEARBEITER where ID ='.$DBSCHAD->WertSetzen('SBV','N0',$_POST['txtSBV_WV_WEITER_AN']);
	        $rsWV_AN = $DBSCHAD->RecordSetOeffnen($SQLSB,$DBSCHAD->Bindevariablen('SBV',true));
	         
	        $WV_AN = $rsWV_AN->FeldInhalt('SBNAME');	 
	        foreach($_POST as $key => $Wert) 
	        {
	            
	            if(substr($key, 0, 6)=='txtWV_') //Ausgew�hlte Comboboxen
	            {
	                $SQLUpdateWV  ='UPDATE';
	                $SQLUpdateWV .='   WIEDERVORLAGEN_NEW';
	                $SQLUpdateWV .=' SET';
	                $SQLUpdateWV .='   WV_AN      = '.$DBSCHAD->WertSetzen('SBV','T',$WV_AN).',';
	                $SQLUpdateWV .='   WV_USER    = '.$DBSCHAD->WertSetzen('SBV', 'T', $AWISBenutzer->BenutzerName()).',';
	                $SQLUpdateWV .='   WV_USERDAT = sysdate';
	                $SQLUpdateWV .=' WHERE';
	                $SQLUpdateWV .='   WV_ID = '.$DBSCHAD->WertSetzen('SBV', 'N0', substr($key,6));
	                
	                $DBSCHAD->Ausfuehren($SQLUpdateWV,'',true,$DBSCHAD->Bindevariablen('SBV',true));
	                $Form->DebugAusgabe(1,$SQLUpdateWV);
	                $SpeichernOK[2]=true;
	                $SpeichernFrame2 = 'Wiedervorlagen wurden umgeschrieben,';
	            }
	        }       
	    }
	    else
	    {
	        $SpeichernOK[2] = false;
	        $SpeichernFrame2=$AWISSpeicherKonserven['Fehler']['err_KeinWert'] . $FehlerFelder;
	    }
	}
	
	
	//Wenn die Absicht bestand Wiedervorlagen zu erzeugen
	if((isset($_POST['sucSBV_oWV_ID']) and $_POST['sucSBV_oWV_ID'] != '')
	    or (isset($_POST['txtSBV_oWV_WEITER_AN']) and $_POST['txtSBV_oWV_WEITER_AN'] != $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen']))
	{
	    $FehlerFelder = '';
	    //Ist ein Verteter gew�hlt?
	    if((isset($_POST['sucSBV_oWV_ID']) and $_POST['sucSBV_oWV_ID'] == '' ))
	    {
	        $FehlerFelder .= 'Vorgangs-Sachbearbeiter. ';
	        $Fehler = true;
	    }
	    //Ist ein zuvertretneder gew�hlt
	    if (isset($_POST['txtSBV_oWV_WEITER_AN']) and $_POST['txtSBV_oWV_WEITER_AN']  == $AWISSpeicherKonserven['Wort']['txt_BitteWaehlen'] )
	    {
	
	        $FehlerFelder .= 'WV Umschreiben auf. ';
	        $Fehler = true;
	    }
	     
	    if(!$Fehler)//Passt alles. -->Speichern
	    {
	        //Neuen Sachbearbeitername ermitteln
	        $SQLSB = 'Select sbname from SACHBEARBEITER where ID ='.$DBSCHAD->WertSetzen('SBV','N0',$_POST['txtSBV_oWV_WEITER_AN']);
	        $rsoWV_AN = $DBSCHAD->RecordSetOeffnen($SQLSB,$DBSCHAD->Bindevariablen('SBV',true));
	
	        $WV_AN = $rsoWV_AN->FeldInhalt('SBNAME');
	        foreach($_POST as $key => $Wert)
	        {
	             
	            if(substr($key, 0, 7)=='txtoWV_') //Ausgew�hlte Comboboxen
	            {
	                $SQLInsertVW  ='INSERT';
	                $SQLInsertVW .=' INTO';
	                $SQLInsertVW .='   WIEDERVORLAGEN_NEW';
	                $SQLInsertVW .='   (';
	                $SQLInsertVW .='     WV_DATUM,';
	                $SQLInsertVW .='     BEARBEITUNGSNR,';
	                //$SQLInsertVW .='     VS_ID,';
	                $SQLInsertVW .='     WV_GRUND,';
	                $SQLInsertVW .='     WV_AN,';
	                $SQLInsertVW .='     SCHADEN_BESCHWERDE,';
	                $SQLInsertVW .='     WV_USER,';
	                $SQLInsertVW .='     WV_USERDAT';
	                $SQLInsertVW .='   )';
	                $SQLInsertVW .='   VALUES';
	                $SQLInsertVW .='   (';
	                $SQLInsertVW .='     trunc(sysdate),';
	                $SQLInsertVW .= $DBSCHAD->WertSetzen('SBV', 'T', substr($key,7))     .',';
	                //$SQLInsertVW .='     :v3,';
	                $SQLInsertVW .='     \'Aufgabenverteilung\','; //WV_GRUND
	                $SQLInsertVW .= $DBSCHAD->WertSetzen('SBV', 'T', $WV_AN) . ',';
	                $SQLInsertVW .='     (select art from schaeden_new where BEARBEITUNGSNR = '.$DBSCHAD->WertSetzen('SBV', 'T', substr($key,7)).'),'; //Schaden beschwerde
	                $SQLInsertVW .='     '.$DBSCHAD->WertSetzen('SBV', 'T', $AWISBenutzer->BenutzerName()).',';;
	                $SQLInsertVW .='     sysdate';
	                $SQLInsertVW .='   )';
	                
	                //die;
	                $DBSCHAD->Ausfuehren($SQLInsertVW,'',true,$DBSCHAD->Bindevariablen('SBV',true));
	                $Form->DebugAusgabe(1,$SQLInsertVW);
	                $SpeichernOK[3]=true;
	                $SpeichernFrame3 = 'Wiedervorlagen wurden erzeugt.';
	            }
	        }
	    }
	    else
	    {
	        $SpeichernOK[3] = false;
	        $SpeichernFrame3=$AWISSpeicherKonserven['Fehler']['err_KeinWert'] . $FehlerFelder;
	    }
	}
	
	
	
	
	
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_HRT_SUCH) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = '"'.$_POST['txtHRT_BEZEICHNUNG'].'" befindet sich schon in der Datenbank.';
		if(isset($_POST['txtHRT_KEY']))
		{
			$AWIS_KEY1 = $_POST['txtHRT_KEY'];
		}
		$Form->Hinweistext($Hinweis);
	}
	elseif($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_HRT_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Form->Hinweistext('Der einzuf�gende Datensatz befindet sich schon in der Datenbank');
	}
	elseif($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_HRT_FIRM) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Form->Hinweistext('Die Firmierungszuordnung existiert bereits in der Datenbank');
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
