<?php
require_once 'crm_funktionen.inc';

$CRM = new crm_funktionen();

const TRENNER = ';';
const LINEFEED = "\n";

switch($_GET['DL']){
    case 'KON':
        $SQL = $CRM->KON_SQL();
        break;
    default:
    case 'CGB':
        $SQL = $CRM->CGB_SQL();
        break;
}

$rsCSV= $CRM->DB->RecordSetOeffnen($SQL);

//Spaltenueberschriften
echo implode(TRENNER,$rsCSV->SpaltenNamen());
echo LINEFEED;

while(!$rsCSV->EOF()){
    $Zeile = '';

    for($i=1;$i<=$rsCSV->AnzahlSpalten();$i++){
        echo $rsCSV->FeldInhalt($i) . ($rsCSV->AnzahlSpalten()>$i?TRENNER:'');
    }

    echo LINEFEED;

    $rsCSV->DSWeiter();
}

header('Pragma: public');
header('Cache-Control: max-age=0');
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename="'.$_GET['DL'].'.csv"');
