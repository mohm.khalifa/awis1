<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen



$strSql="select ban_kuerzel from bank order by ban_kuerzel";

$arrBank=abfrage("ora",$strSql);

if($intTransfer==2)
	$strTitel="(nicht &uuml;bernommen)";
else
	$strTitel="(noch nicht &uuml;bernommen)";
?>
<html>
<head>
<title>&nbsp;</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets 


?>
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	<script language="JavaScript">
	
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.startzeit();	//Zeitmessung
	<?php } ?>
	
	function init()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
	}
	
	//Wird bei Klick auf das Bild eingabe_ok (Parameter Rechnungsnr) ausgeführt, d.h. übermittelt die Rechnungsnr nach card_haupt.php
	
	function filteranwenden()
	{
		var datobj = document.getElementById('filterdatum');
		
		if(datobj.value!=''&&!datumspruefung(datobj))
		{
			return;
		}
		
		var objBetrag = document.getElementById('filterbetrag');
		var dblBetrag = objBetrag.value;
		
		if(dblBetrag!=''&&!betragspruef(objBetrag,7))
			return;
		
		var objBank = document.getElementById('filterbank');
		
		var strBank = listeneintrag(objBank,"text");
		
		var objAuftraggeber = document.getElementById('filterauftraggeber');
		var strAuftraggeber = objAuftraggeber.value;
		
		if(strAuftraggeber.search(/;/)!=-1)
		{
			alert("Das Zeichen ; ist nicht erlaubt!");
			objAuftraggeber.select();
			return;
		}
		
		var arrObj=document.getElementsByName('filter');
		var strIdChecked='';
		
		for(i=0;i<arrObj.length&&strIdChecked=='';i++)
		{
			if(arrObj[i].checked)
				strIdChecked=arrObj[i].id;
		}
		
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Kontoauszüge filtern");	//Zeitmessung
		<?php } ?>
		parent.window.returnValue = datobj.value+';alle;'+dblBetrag+';'+strAuftraggeber+';'+strBank;
		parent.self.close();
	}
	
	function datumheute()
	{
		var obj = window.event.srcElement;
		obj.value = '<?php echo strftime("%d.%m.%y");?>';
		window.event.cancelBubble = true;
	}	
	
	
	</script>
	
	</head>
	<body onload="init();" style="overflow:hidden;">
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="middle">
		<table cellspacing="0" cellpadding="0" border="1">
		<tr><td colspan="2" style="text-align:center;"><span style="font-weight:bold;">Filter Kontoausz&uuml;ge</span><br><?php echo $strTitel;?></td></tr>
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr><td colspan="2" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td>Datum:</td><td><input type="text" class="datum" maxlength="8" id="filterdatum" ondblclick="datumheute();"></td>
		</tr>
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr><td colspan="2" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td>Betrag:</td><td><input type="text" class="betrag" maxlength="10" id="filterbetrag"></td>
		</tr>		
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr><td colspan="2" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td>Bank:</td><td><select id="filterbank">
		<option>alle</option>
		<?php
		for($i=0;$i<$arrBank["zeilen"];$i++)
		{
			echo "<option>".$arrBank["trefferarray"]["BAN_KUERZEL"][$i]."</option>";
		}
		?>
		</select>
		</td>
		</tr>		
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td colspan="2" align="center">Auftraggeber:</td>
		</tr>
		<tr>
		<td colspan="2" align="center"><input type="text" maxlength="20" id="filterauftraggeber"></td>
		</tr>
		<tr><td colspan="2" class="unten" height="1px" style="font-size:1px">&nbsp</td></tr>
		<tr><td colspan="2" height="5px" style="font-size:1px">&nbsp</td></tr>
		<tr>
		<td colspan="2" align="center"><button type="button" class="but" onclick="filteranwenden();">Filter anwenden</button></td>
		</tr>
		<tr>
		<td colspan="2" align="center"><button type="button" class="but" onclick="self.close();">Fenster schlie&szlig;en</button></td>
		</tr>
		</table>
	
	</td></tr></table>
	<?php 
	$intDialog = 1;
	include("card/card_zeitende.inc.php");			// Zeitmessung	
?>
</body>
</html>