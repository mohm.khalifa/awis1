<?php
require_once 'awisBerichte.inc';

class ber_Bewerberverwaltung_Vorstellungsgespraech extends awisBerichte
{
	
    
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
	 
	    
		//$this->_Dateiname = $Parameter['DATEINAME'];
		//$this->_BerichtsVorlage= '/daten/web/dokumente/vorlagen/' . $Parameter['VORLAGE'];
		//$this->_UebschriftGueltig = $Parameter['UEBERSCHRIFT_GUELTIG'];
		//$this->_ZusatzSommer = $Parameter['ZUSATZ_SOMMER'];
		//$this->_ZusatzWinter = $Parameter['ZUSATZ_WINTER'];

	
		
	
		//*************************************
		// Daten fuer den Bericht laden
		//*************************************
		$SQL  ='select ';
        $SQL .=' a.HRF_FIRMIERUNG HRF_FIRMIERUNG, HRF_KEY,';
        $SQL .=' a.EINGANGSDATUM EINGANGSDATUM,';
        $SQL .=' a.EINGANGSART EINGANGSART,';
        $SQL .=' B.HRK_ANR_ID HRK_ANR_ID,';
        $SQL .=' B.HRK_TITEL HRK_TITEL,';
        $SQL .=' B.HRK_VORNAME HRK_VORNAME,';
        $SQL .=' B.HRK_NACHNAME HRK_NACHNAME,';
        $SQL .=' B.HRK_STRASSEHNR HRK_STRASSEHNR,';
        $SQL .=' B.HRK_PLZ HRK_PLZ, a.lan_kz,';
        $SQL .=' B.HRK_ORT HRK_ORT,';
        $SQL .=' C.ANR_ANREDE,';
        $SQL .=' VSDAT.BIF_WERT VORSTELLDATUM,';
        $SQL .=' VSUHR.BIF_WERT VORSTELLZEIT,';
        $SQL .=' VSORT.BIF_WERT VORSTELLORT,';
        $SQL .=' VSPLZ.BIF_WERT VORSTELLPLZ,';
        $SQL .=' VSLOKAT.BIF_WERT VORSTELLLOKATION,';
        $SQL .=' VSSTR.BIF_WERT VORSTELLSTR';
        $SQL .=' from V_BEWERBERVERWALTUNG a';
        $SQL .=' inner join hrkopf b';
        $SQL .=' on a.hrk_key = b.hrk_key';
        $SQL .=' inner join anreden c';
        $SQL .=' on b.hrk_anr_id = c.anr_id';
        $SQL .=' inner join BEWERBERINFOS VSDAT';
        $SQL .=' on a.BWH_KEY = VSDAT.BIF_BWH_KEY and VSDAT.BIF_BIT_ID = 18';
        $SQL .=' inner join BEWERBERINFOS VSUHR';
        $SQL .=' on a.BWH_KEY = VSUHR.BIF_BWH_KEY and VSUHR.BIF_BIT_ID = 19';
        $SQL .=' inner join BEWERBERINFOS VSORT';
        $SQL .=' on a.BWH_KEY = VSORT.BIF_BWH_KEY and VSORT.BIF_BIT_ID = 20';
        $SQL .=' inner join BEWERBERINFOS VSPLZ';
        $SQL .=' on a.BWH_KEY = VSPLZ.BIF_BWH_KEY and VSPLZ.BIF_BIT_ID = 21';
        $SQL .=' inner join BEWERBERINFOS VSLOKAT';
        $SQL .=' on a.BWH_KEY = VSLOKAT.BIF_BWH_KEY and VSLOKAT.BIF_BIT_ID = 22';
        $SQL .=' inner join BEWERBERINFOS VSSTR';
        $SQL .=' on a.BWH_KEY = VSSTR.BIF_BWH_KEY and VSSTR.BIF_BIT_ID = 23';
        $SQL .=' WHERE BWH_KEY'.$this->_DB->LikeOderIst($_GET['BWH_KEY']);
        
		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);		

        if ($this->_rec->FeldInhalt('HRF_KEY') == 1)
		{
		    $this->_BerichtsVorlage = '/daten/web/dokumente/vorlagen/' .'Vorlage_BewerberverwaltungATU.pdf';
		}
		elseif ($this->_rec->FeldInhalt('HRF_KEY') == 3)
		{
		    $this->_BerichtsVorlage = '/daten/web/dokumente/vorlagen/' .'Vorlage_BewerberverwaltungESTATO.pdf';
		}
		else
		{
		    $this->_BerichtsVorlage ='/daten/web/dokumente/vorlagen/' . 'Vorlage_BewerberverwaltungATU.pdf';
		}
		

		if ($this->_rec->FeldInhalt('HRF_KEY') == 1)
		{
		    $this->_Konserve = 'ATU';
		}
		elseif ($this->_rec->FeldInhalt('HRF_KEY') == 3)
		{
		    $this->_Konserve = 'ESTATO';
		}
		else
		{
		    $this->_Konserve = 'ATU';
		}
		
		
		 
		if(file_exists($this->_BerichtsVorlage))
		{
		    $this->setSourceFile($this->_BerichtsVorlage);
		}
		else
		{
		    $this->_BerichtsVorlage='';
		}
		
		
		//*************************************
		// Alle Textbausteine laden
		//*************************************
		$TextKonserven[]=array('BEW','*');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
		
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		//Wochentage-Array f�llen
	    $wochentag[0] = "Sonntag";
	    $wochentag[1] = "Montag";
	    $wochentag[2] = "Dienstag";
	    $wochentag[3] = "Mittwoch";
	    $wochentag[4] = "Donnerstag";
	    $wochentag[5] = "Freitag";
	    $wochentag[6] = "Samstag";
	    
	    $SQL  ='SELECT BVORNAME.BIF_WERT||\' \'||BNACHNAME.BIF_WERT as TEILNEHMER, BART.BIF_WERT as ART FROM V_BEWERBERVERTEILER BVORNAME';
        $SQL .=' INNER JOIN V_BEWERBERVERTEILER BNACHNAME';
        $SQL .=' ON BNACHNAME.DS=BVORNAME.DS and BNACHNAME.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BNACHNAME.BIF_BIT_ID=27';
        $SQL .=' INNER JOIN V_BEWERBERVERTEILER BART';
        $SQL .=' ON BART.BIF_BWH_KEY=BVORNAME.BIF_BWH_KEY and BART.DS=BVORNAME.DS and BART.BIF_BIT_ID=28';
        $SQL .=' WHERE BVORNAME.BIF_BIT_ID=26 and BART.BIF_WERT <> \'Bewerber\'';
        $SQL .=' and BART.BIF_WERT <> \'MANUELL\' and BVORNAME.BIF_BWH_KEY'.$this->_DB->LikeOderIst($_GET['BWH_KEY']);
	    $rsTeilnehmer = $this->_DB->RecordSetOeffnen($SQL);
		// Startpositionen setzen
		
	    $this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator('Bewerberverwaltung');
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins(0,0,0);
		$this->AliasNbPages();
		$this->SetAutoPageBreak(false);
	
		$this->NeueSeite();
		
		// Fusszeile Seite 1
		
		//$this->SetXY(10,self::SeitenHoehe()-3);
		//$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L');
		//$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtKonditionen'],0,0,'C');
		//$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],date('d.m.Y'),0,0,'R');
	
		$this->_Zeile += 18;
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse-Klein']);
        $this->MultiCell(100,$this->_Parameter['Zeilenhoehe']/2,$this->_AWISSprachkonserven['BEW']['BEW_PDF_ANSCHRIFT_'.$this->_Konserve],0,'L');
		$this->_Zeile+=$this->_Parameter['Zeilenhoehe']*1.5;
		$this->SetXY($this->_Spalte,$this->_Zeile);
		
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ANR_ANREDE')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
	
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],($this->_rec->FeldInhalt('HRK_TITEL')!=''?$this->_rec->FeldInhalt('HRK_TITEL').' ' : '' ).$this->_Form->Format('T',$this->_rec->FeldInhalt('HRK_VORNAME') . ' ' . $this->_rec->FeldInhalt('HRK_NACHNAME')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
	
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('HRK_STRASSEHNR'). ' ' .$this->_rec->FeldInhalt('CAD_HAUSNUMMER')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('LAN_KZ').'-'.$this->_rec->FeldInhalt('HRK_PLZ'). ' ' .$this->_rec->FeldInhalt('HRK_ORT')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		
		$this->_Zeile += 28;
		$this->_Spalte += 130;
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
		$this->Cell(50,$this->_Parameter['Zeilenhoehe'],date("d.m.Y",time()),0,0,'L');
		
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Text-Groesse']);
		
		$Betreff = 'Ihre Bewerbung bei ';
		if ($this->_rec->Feldinhalt('HRF_KEY') == 3)
		{
		    $Betreff.= 'ESTATO';
		}
		else
		{
		    $Betreff.= 'ATU';
		}
		$this->Cell(50,$this->_Parameter['Zeilenhoehe'],$Betreff,0,0,'L');
		
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
		
		$Text = $this->_AWISSprachkonserven['BEW']['BEW_PDF_VORST_TEXT'];
		$Text = str_replace('#GEEHRT#', $this->_rec->FeldInhalt('ANR_ANREDE')=='Herr'?'geehrter':'geehrte', $Text) ;
		$Text = str_replace('#ANREDE#', $this->_rec->FeldInhalt('ANR_ANREDE'), $Text) ;
		$Text = str_replace('#HRK_TITEL#', $this->_rec->FeldInhalt('HRK_TITEL'), $Text) ;
		
		$Text = str_replace('#HRK_VORNAME#', $this->_rec->FeldInhalt('HRK_VORNAME'), $Text) ;
		$Text = str_replace('#HRK_NACHNAME#', ' '.$this->_rec->FeldInhalt('HRK_NACHNAME'), $Text) ;
		
		$Text = str_replace('#DATUM#', $this->_rec->Feldinhalt('VORSTELLDATUM'), $Text);
		$Text = str_replace('#WOCHENTAG#', $wochentag[date('w',strtotime($this->_rec->Feldinhalt('VORSTELLDATUM')))], $Text);
		$Text = str_replace('#UHRZEIT#', $this->_rec->Feldinhalt('VORSTELLZEIT') . ' Uhr', $Text);
		$Text = str_replace('#ORT#', $this->_rec->Feldinhalt('VORSTELLORT'), $Text);
		$Text = str_replace('#PLZ#', $this->_rec->Feldinhalt('VORSTELLPLZ'), $Text);
		$Text = str_replace('#LOKATION#', $this->_rec->Feldinhalt('VORSTELLLOKATION'), $Text);
		$Text = str_replace('#STRA�E#', $this->_rec->Feldinhalt('VORSTELLSTR'), $Text);
		
		$Teilnehmer = "";
		while (!$rsTeilnehmer->EOF())
		{
    		$Teilnehmer .= ", " . $rsTeilnehmer->Feldinhalt('TEILNEHMER');
    		$rsTeilnehmer->DSWeiter();
		}

		$Text = str_replace('#TEILNEHMER#', substr($Teilnehmer, 1), $Text);

		$Text = str_replace('#IHRE#', $rsTeilnehmer->AnzahlDatensaetze()>2?'Ihre':'Ihr', $Text);
		$Text = str_replace('#ERWARTET#', $rsTeilnehmer->AnzahlDatensaetze()>2?'erwarten':'erwartet', $Text);
		
		$this->MultiCell(160,$this->_Parameter['Zeilenhoehe'],$Text);
		
		$this->_Zeile = $this->getY();
		
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
		
		$this->MultiCell(160,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['BEW']['BEW_PDF_SIGNATUR_'.$this->_rec->FeldInhalt('HRF_KEY')]);
     
		
		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		
		//var_dump ($Ausgabeart);
		$Ausgabeart = 1;
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
	}
	
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{	
	   

	    if($this->_BerichtsVorlage!='')
	    {
	        $pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
	       
	        $this->useTemplate($pdf_vorlage);
	    }
	}
	public function Footer()
	{
	 
	   
	    $this->SetFont($this->_Parameter['Schriftart'], '', 6);
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->_Zeile = 278;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    // Cursor setzen
	    $this->MultiCell(160,$this->_Parameter['Zeilenhoehe']-2,$this->_AWISSprachkonserven['BEW']['BEW_PDF_'.$this->_rec->FeldInhalt('HRF_KEY').'_FOOTER_SPALTE1']);

	    $this->_Spalte = $this->_Parameter['LinkerRand'] + 55;
	    $this->_Zeile = 278;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    // Cursor setzen
	    $this->MultiCell(160,$this->_Parameter['Zeilenhoehe']-2,$this->_AWISSprachkonserven['BEW']['BEW_PDF_'.$this->_rec->FeldInhalt('HRF_KEY').'_FOOTER_SPALTE2']);
	     
	    
	    $this->_Spalte = $this->_Parameter['LinkerRand'] + 125;
	    $this->_Zeile = 278;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    // Cursor setzen
	    $this->MultiCell(160,$this->_Parameter['Zeilenhoehe']-2,$this->_AWISSprachkonserven['BEW']['BEW_PDF_'.$this->_rec->FeldInhalt('HRF_KEY').'_FOOTER_SPALTE3']);
	    
	    
	     
	    //$Ausdruck->_pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
		  
	}
	
}