<?php

require_once('awisMailer.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $Anzahl = 0;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }

        }

        $AWISBenutzer = awisBenutzer::Init($Benutzer);
        $Form = new awisFormular();
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $AWISMail = new awisMailer($DB, $AWISBenutzer);
        $AWISMail->DebugLevel($DebugLevel);
        $AWISMail->SMSBestaetigungen(time());
}
catch(Exception $ex)
{
        echo 'FEHLER : '.$ex->getMessage().PHP_EOL;
}
?>

