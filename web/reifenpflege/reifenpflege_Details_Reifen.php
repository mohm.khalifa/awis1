<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
	$TextKonserven=array ();
	$TextKonserven[]=array('RST','*' );
	$TextKonserven[]=array('RRP','*' );
	$TextKonserven[]=array('REH','*' );
	$TextKonserven[]=array('Wort','Seite' );
	$TextKonserven[]=array('Wort','lbl_suche' );
	$TextKonserven[]=array('Wort','lbl_weiter' );
	$TextKonserven[]=array('Wort','lbl_speichern' );
	$TextKonserven[]=array('Wort','lbl_trefferliste' );
	$TextKonserven[]=array('Wort','lbl_aendern' );
	$TextKonserven[]=array('Wort','lbl_hilfe' );
	$TextKonserven[]=array('Wort','lbl_hinzufuegen' );
	$TextKonserven[]=array('Wort','lbl_loeschen' );
	$TextKonserven[]=array('Wort','lbl_zurueck' );
	$TextKonserven[]=array('Wort','lbl_DSZurueck' );
	$TextKonserven[]=array('Wort','lbl_DSWeiter' );
	$TextKonserven[]=array('Wort','lbl_drucken' );
	$TextKonserven[]=array('Wort','lbl_Hilfe' );
	$TextKonserven[]=array('Wort','txt_BitteWaehlen' );
	$TextKonserven[]=array('Wort','FilialenDeutschAlle' );
	$TextKonserven[]=array('Wort','Reifensuche' );
	$TextKonserven[]=array('Liste','lst_KENN1' );
	$TextKonserven[]=array('Liste','lst_KENN2' );
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_RST_IMPORTSTATUS');
	$TextKonserven[]=array('Liste','lst_RST_STATUS');
	$TextKonserven[]=array('Liste','lst_ROLLRES');
	$TextKonserven[]=array('Liste','lst_WETGRIP');
	$TextKonserven[]=array('Liste','lst_NOISECL');
	
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15000 = $AWISBenutzer->HatDasRecht(15300);
	if(($Recht15000)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

		
	//********************************************************
	// Daten suchen
	//********************************************************
	$SQL  = 'SELECT RST_KEY,';
	$SQL .= ' RST_BEZEICHNUNG,';
	$SQL .= ' RST_REH_KEY,';
	$SQL .= ' RST_LIE_NR,';
	$SQL .= ' RST_LARTNR,';
	$SQL .= ' RST_BREITE,';
	$SQL .= ' RST_QUERSCHNITT,';
	$SQL .= ' RST_BAUART,';
	$SQL .= ' RST_INNENDURCHMESSER,';
	$SQL .= ' RST_LLKW,';
	$SQL .= ' RST_LOADINDEX,';
	$SQL .= ' RST_LOADINDEX2,';
	$SQL .= ' RST_LOADINDEX3,';
	$SQL .= ' RST_RF,';
	$SQL .= ' RST_SPEEDINDEX,';
	$SQL .= ' RST_SPEEDINDEX2,';
	$SQL .= ' RST_ZUSATZBEMERKUNG,';
	$SQL .= ' RST_ZUSATZBEMERKUNG2,';
	$SQL .= ' RST_ZUSATZBEMERKUNG3,';
	$SQL .= ' RST_TYP,';
	$SQL .= ' RST_TYP2,';
	$SQL .= ' RST_TYP3,';
	$SQL .= ' RST_ROLLRES,';
	$SQL .= ' RST_WETGRIP,';
	$SQL .= ' RST_NOISEPE,';
	$SQL .= ' RST_NOISECL,';
	$SQL .= ' RST_KBPREIS,';
	$SQL .= ' RST_EAN,';
	$SQL .= ' RST_FELGENRIPPE,';
	$SQL .= ' RST_XDI_KEY,';
	$SQL .= ' RST_IMPORTSTATUS,';
	$SQL .= ' RST_STATUS,';
	$SQL .= ' RST_BEMERKUNG,';
	$SQL .= ' RST_WINTERTAUGLICHKEIT,';
	$SQL .= ' RST_INFO,';
	$SQL .= ' RST_USER,';
	$SQL .= ' RST_USERDAT';
	$SQL .= ' FROM REIFENSTAMM';
	$SQL .= ' INNER JOIN REIFENPFLEGE ON RRP_RST_KEY = RST_KEY';
	$SQL .= ' WHERE RRP_KEY=0'.$AWIS_KEY1;

	$rsRST = $DB->RecordSetOeffnen($SQL);
	$Form->Formular_Start();
	
	if ($rsRST->AnzahlDatensaetze () > 0)
	{
		$LabelbreiteSP1=160;
		$DatenbreiteSP1=160;
		$LabelbreiteSP2=160;
		$DatenbreiteSP2=160;
		$LabelbreiteSP3=160;
		$DatenbreiteSP3=160;
		$LabelbreiteSP4=160;
		$DatenbreiteSP4=160;
		
		$EditRecht = false;
		$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_STATUS'] . ':', $LabelbreiteSP1);
        $Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_STATUS']);
        $Form->Erstelle_SelectFeld('RST_STATUS',$rsRST->FeldInhalt('RST_STATUS')!=''?$rsRST->FeldInhalt('RST_STATUS'):'10' , $DatenbreiteSP1,($AWIS_KEY1==''?false:$EditRecht), '', '','','','',$Daten);
        $AWISCursorPosition = 'txtRST_STATUS';
        
        if ($AWIS_KEY1!='')
        {
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_IMPORTSTATUS'] . ':', $LabelbreiteSP2);
	        $Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_IMPORTSTATUS']);
	        $Form->Erstelle_SelectFeld('RST_IMPORTSTATUS',$rsRST->FeldInhalt('RST_IMPORTSTATUS')!=''?$rsRST->FeldInhalt('RST_IMPORTSTATUS'):'10' , $DatenbreiteSP2, false, '', '','','','',$Daten);
        }
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['HERSTELLER'] . ':', $LabelbreiteSP1);
        $SQL = "SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ";
        $Form->Erstelle_SelectFeld('RST_REH_KEY', $rsRST->FeldInhalt('RST_REH_KEY'), $DatenbreiteSP1, $EditRecht, $SQL, $OptionBitteWaehlen);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LIE_NR'] . ':', $LabelbreiteSP2);
        $Form->Erstelle_TextFeld('RST_LIE_NR', ($rsRST->FeldInhalt('RST_LIE_NR')), 4, $DatenbreiteSP2, $EditRecht);

        $Link = '../reifenstamm/reifenstamm_Main.php?cmdAktion=Details&RST_KEY=' . $rsRST->FeldInhalt('RST_KEY') . '';
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LARTNR'] . ':', $LabelbreiteSP3);
        $Form->Erstelle_TextFeld('RST_LARTNR', ($rsRST->FeldInhalt('RST_LARTNR')), 30, 240, $EditRecht, '', '', $Link);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_EAN'] . ':', $LabelbreiteSP1);
        $Form->Erstelle_TextFeld('RST_EAN', ($rsRST->FeldInhalt('RST_EAN')), 20, $DatenbreiteSP1, $EditRecht,'','','','','','','',13);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'] . ':', $LabelbreiteSP1);
        $Form->Erstelle_TextFeld('RST_BEZEICHNUNG', ($rsRST->FeldInhalt('RST_BEZEICHNUNG')), 50,$EditRecht?$DatenbreiteSP1:400,$EditRecht);
        $Form->ZeileEnde();
        
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP'] . ':', $LabelbreiteSP1);
       	$SQL = "SELECT RET_ID, RET_TYP FROM REIFENTYP ";
       	$Form->Erstelle_SelectFeld('RST_TYP',$rsRST->FeldInhalt('RST_TYP'),$DatenbreiteSP1,$EditRecht,$SQL,$OptionBitteWaehlen);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP2'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_SelectFeld('RST_TYP2',$rsRST->FeldInhalt('RST_TYP2'), $DatenbreiteSP2,$EditRecht,$SQL,$OptionBitteWaehlen);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP3'] . ':', $LabelbreiteSP3);
       	$Form->Erstelle_SelectFeld('RST_TYP3',$rsRST->FeldInhalt('RST_TYP3'), $DatenbreiteSP3,$EditRecht,$SQL,$OptionBitteWaehlen);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BREITE'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_BREITE', ($rsRST->FeldInhalt('RST_BREITE')), 4, $DatenbreiteSP1, $EditRecht);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_QUERSCHNITT'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_TextFeld('RST_QUERSCHNITT', ($rsRST->FeldInhalt('RST_QUERSCHNITT')), 4, $DatenbreiteSP2, $EditRecht);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_INNENDURCHMESSER'] . ':', $LabelbreiteSP3);
       	$Form->Erstelle_TextFeld('RST_INNENDURCHMESSER', ($rsRST->FeldInhalt('RST_INNENDURCHMESSER')), 4, $DatenbreiteSP3, $EditRecht);
       	$Form->ZeileEnde();
        	
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BAUART'] . ':', $LabelbreiteSP1);
       	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_BAUART']);
       	$Form->Erstelle_SelectFeld('RST_BAUART',$rsRST->FeldInhalt('RST_BAUART'), $DatenbreiteSP1, $EditRecht, '',$OptionBitteWaehlen,'','','',$Daten);
        	 
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LLKW'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_TextFeld('RST_LLKW',($rsRST->FeldInhalt('RST_LLKW')),10, $DatenbreiteSP2,$EditRecht);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_LOADINDEX',($rsRST->FeldInhalt('RST_LOADINDEX')),10, $DatenbreiteSP1,$EditRecht);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX2'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_TextFeld('RST_LOADINDEX2',($rsRST->FeldInhalt('RST_LOADINDEX2')),10, $LabelbreiteSP2,$EditRecht);
       	$Form->ZeileEnde();
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX3'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_LOADINDEX3',($rsRST->FeldInhalt('RST_LOADINDEX3')),10, $DatenbreiteSP1,$EditRecht);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_RF'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_TextFeld('RST_RF',($rsRST->FeldInhalt('RST_RF')),10, $LabelbreiteSP2,$EditRecht);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX_kurz'] . ':', $LabelbreiteSP1);
       	$SQL = "SELECT SPI_SYMBOL AS KEY,SPI_SYMBOL AS WERT FROM SPEEDINDEX ";
       	$Form->Erstelle_SelectFeld('RST_SPEEDINDEX',$rsRST->FeldInhalt('RST_SPEEDINDEX'), $DatenbreiteSP1,$EditRecht,$SQL,$OptionBitteWaehlen);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX2_kurz'] . ':', $LabelbreiteSP2);
       	$Form->Erstelle_SelectFeld('RST_SPEEDINDEX2',$rsRST->FeldInhalt('RST_SPEEDINDEX2'), $DatenbreiteSP2,$EditRecht,$SQL,$OptionBitteWaehlen);
       	$Form->ZeileEnde();
       	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_FELGENRIPPE'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_FELGENRIPPE',($rsRST->FeldInhalt('RST_FELGENRIPPE')),10, $DatenbreiteSP1,$EditRecht);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')),30, $DatenbreiteSP1,$EditRecht);
       	$Form->ZeileEnde();
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG2'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG2',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG2')),30, $LabelbreiteSP1,$EditRecht);
       	$Form->ZeileEnde();
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG3'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG3',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG3')),30, $DatenbreiteSP1,$EditRecht);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ROLLRES'] . ':', $LabelbreiteSP1);
       	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_ROLLRES']);
       	$Form->Erstelle_SelectFeld('RST_ROLLRES',$rsRST->FeldInhalt('RST_ROLLRES') , $DatenbreiteSP1, $EditRecht, '', $OptionBitteWaehlen,'','','',$Daten);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_WETGRIP'] . ':', $LabelbreiteSP2);
       	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_WETGRIP']);
       	$Form->Erstelle_SelectFeld('RST_WETGRIP',$rsRST->FeldInhalt('RST_WETGRIP') , $DatenbreiteSP2, $EditRecht, '', $OptionBitteWaehlen,'','','',$Daten);
       	$Form->ZeileEnde();
        	 
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISECL'] . ':', $LabelbreiteSP1);
       	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_NOISECL']);
       	$Form->Erstelle_SelectFeld('RST_NOISECL',$rsRST->FeldInhalt('RST_NOISECL') , $DatenbreiteSP1, $EditRecht, '', $OptionBitteWaehlen,'','','',$Daten);
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISEPE'] . ':', $LabelbreiteSP2);
       	$Noisepe = '';
       	for ($i = 1; $i < 100; $i++)
       	{
	       	$Noisepe = $Noisepe.'|'.$i.'~'.$i;
       	}
       	$Noisepe = substr($Noisepe,1);
       	$Daten = explode("|",$Noisepe);
       	$Form->Erstelle_SelectFeld('RST_NOISEPE',$rsRST->FeldInhalt('RST_NOISEPE'), $DatenbreiteSP2, $EditRecht, '', $OptionBitteWaehlen,'','','',$Daten);
       	$Form->ZeileEnde();
        	
       	$Form->ZeileStart();
       	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_KBPREIS'] . ':', $LabelbreiteSP1);
       	$Form->Erstelle_TextFeld('RST_KBPREIS',($rsRST->FeldInhalt('RST_KBPREIS')),10, $DatenbreiteSP1,$EditRecht,'','','','N2');
       	$Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_WINTERTAUGLICHKEIT'] . ':', $LabelbreiteSP1);
        $Daten = explode("|",$AWISSprachKonserven['RST']['lst_WINTERTAUGLICHKEIT']);
        $Form->Erstelle_SelectFeld('RST_WINTERTAUGLICHKEIT',($rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT')!== NULL and $rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT') == 'X')?$rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT'):' ',$DatenbreiteSP1,$EditRecht,'','','','','',$Daten);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_INFO'] . ':', $LabelbreiteSP1);
        $Form->Erstelle_TextFeld('RST_INFO',($rsRST->FeldInhalt('RST_INFO')),15, $DatenbreiteSP1,$EditRecht,'','','','T','L','','',15);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BEMERKUNG'].':',$LabelbreiteSP1);
        $Form->Erstelle_TextArea('RST_BEMERKUNG',$rsRST->FeldInhalt('RST_BEMERKUNG'),800,99,5,$EditRecht);
        $Form->ZeileEnde();
	} 
	else 
	{
        $Form->Hinweistext($AWISSprachKonserven['RST']['err_KeinReifenVorhanden']);
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Reifensuche'] . ':', 100);
        $Form->AuswahlBox('RST_SUCHE', 'box1', 'txtRRP_RST_KEY', 'RST_Liste', '', 300, 100, '','T',true,'','','',50,'',$AWISSprachKonserven['RST']['ttt_Reifensuche']);
        $Form->ZeileEnde();
        
    	$Form->ZeileStart('font-size:8pt');
    	$Form->Erstelle_TextLabel('&nbsp',100);
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['txt_Reifensuche'],600);
    	$Form->ZeileEnde();
                
        $Form->ZeileStart();
        $Form->AuswahlBoxHuelle('box1','AuswahlListe','');
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');
        
	}
	
	$Form->Formular_Ende();
}

catch ( Exception $ex ) 
{
	if ($Form instanceof awisFormular) 
	{
		$Form->Fehler_Anzeigen ( 'INTERN', $ex->getMessage (), 'MELDEN', 6, "200905131842" );
	} 
	else 
	{
		echo 'allg. Fehler:' . $ex->getMessage ();
	}
}
?>