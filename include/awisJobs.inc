<?php
/**
 * Klasse zum Verwalten von Jobs
 *
 * @author Sacha Kerres
 * @version 201301
 *
 */
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisMailer.inc';

class awisJobs
{
    /**
     * Datenbankobjekt
     * @var awisDatenbank
     */
    private $_DB = null;
    /**
     * AWIS-Benutzer
     * @var awisBenutzer
     */
    private $_AWISBenutzer = null;
    /**
     * Name des Knotens f�r die Jobverwaltung
     * @var string
     */
    private $_KnotenName = '';
    /**
     * Debuglevel f�r Ausgaben
     * @var int
     */
    private $_DebugLevel = 0;
    /**
     * Mail-Objekt
     * @var awisMailer
     */
    private $_MailObj = null;
    /**
     * Prozess-ID Datei f�r den Job
     * @var string
     */
    private $_PID_Datei = '/daten/jobs/pid/awisJobs.pid';

    /**
     * Eigene Prozess-ID
     * @var int
     */
    private $_PID = 0;

    /**
     * Prozessgruppen mit ihren aktuellen Prozessen
     * @var array
     */
    private $_Gruppen = array();

    /**
     * Fehler, wenn der User keinen Zugriff hat
     * @var int
     */
    const ERR_ZUGRIFF = 1;
    /**
     * Initialisiert die Klasse
     * @param string $Benutzer              Loginname zum Ausf�hren des Jobs
     * @throws Exception
     */

    public function __construct($Benutzer)
    {
        // Datenbank initialisieren
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_KnotenName = explode('.',getenv('HOSTNAME'));
        $this->_KnotenName = $this->_KnotenName[0];
        $this->_MailObj = new awisMailer($this->_DB, $this->_AWISBenutzer);
    }

    /**
     * Liest oder setzt den Debuglevel
     * @param int [optional] $NeuerLevel            Neuer Debuglevel
     * @return number
     */
    public function DebugLevel($NeuerLevel = null)
    {
        if(!is_null($NeuerLevel))
        {
            $this->_DebugLevel = intval($NeuerLevel);
        }

        return $this->_DebugLevel;
    }


    /**
     * Startet und �berwacht Jobs f�r einen Knoten
     */
    public function StarteJobs()
    {
        $this->_SchreibeMeldung('I','Pruefe, ob offene Jobs da sind...');

        if(is_file($this->_PID_Datei))
        {
            if($this->WatchDog()==true)
            {
                return false;           // Anderer Scheduler
            }
        }

        // PID Dtatei erzeugen um anzuzeigen, dass was getan wird
        $this->_PID = getmypid();
        $this->_SchreibeMeldung('I','Erzeuge eine PID ('.$this->_PID.')...');
        file_put_contents($this->_PID_Datei,$this->_PID);

        while(1)
        {
            file_put_contents($this->_PID_Datei.'.chk', '1');

            $SQL = 'SELECT *';
            $SQL .= ' FROM jobliste';
            $SQL .= ' WHERE xjo_status=1';
            $SQL .= ' AND (xjo_naechsterstart <= SYSDATE AND xjo_naechsterstart IS NOT NULL)';
            $SQL .= ' AND (XJO_ERLAUBTEKNOTEN IS NULL OR XJO_ERLAUBTEKNOTEN LIKE '.$this->_DB->FeldInhaltFormat('TU','%'.$this->_KnotenName.'%').')';
            $SQL .= ' ORDER BY xjo_naechsterstart, xjo_key';

            $rsXJO = $this->_DB->RecordSetOeffnen($SQL);
            if($rsXJO->EOF())
            {
                break;
            }

            $this->_PruefeMounts();
            
            while(!$rsXJO->EOF())
            {
                $this->_PruefeMounts();

                $this->_SchreibeMeldung('I','+ Auftrag : ('.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').') ist dran...');

                if(!isset($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]))
                {
                    $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);
                }

                //Evtl. wurde der Scheduler neugestartet, sodass zwar eine PID in der DB steht, die Resource dazu aber nicht mehr in der aktuellen Schedulerinstanz enthalten ist
                if($rsXJO->FeldInhalt('XJO_AKTUELLEPID') != '' and $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid'] == ''){
                    //Prozess existiert noch
                    if(posix_kill($rsXJO->FeldInhalt('XJO_AKTUELLEPID'),0)){
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid']=$rsXJO->FeldInhalt('XJO_AKTUELLEPID');
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job']= 'Alte Instanz von ' . $rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG');
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']= 'OLD';
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key']=$rsXJO->FeldInhalt('XJO_KEY');
                        if($rsXJO->FeldInhalt('XJO_MAXLAUFZEIT')>0)
                        {
                            $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']=time() + ($rsXJO->FeldInhalt('XJO_MAXLAUFZEIT')*60);
                        }
                        else	// Maximale Laufzeit ist ein ganzer Tag
                        {
                            $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']=time() + (1440*60);
                        }
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['start']=time();
                    }
                }

                if($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid']==0 and !$this->_Wartungsmodus())
                {
                    $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job']=$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG');
                    $this->_SchreibeMeldung('I','+ Auftrag : ('.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').') wird gestartet...');

                    $Befehl = $rsXJO->FeldInhalt('XJO_BEFEHLSZEILE');

                    switch ($rsXJO->FeldInhalt('XJO_TYP'))
                    {
                        case 2:                 // PHP Skript
                            // Befehl aufrufen
                            $Befehl = '/usr/local/php/bin/php -c /etc/php.ini -f '.$Befehl;
                            break;
                        case 1:         // bash Skript
                            // Befehl aufrufen
                            //PG: 26.06.2017: Erstmal den Befehl bauen, nicht gleich ausf�hren!
                            $Befehl = '/bin/bash ' . $Befehl;
                            break;
                        default:
                            throw new Exception('Falscher Typ:', self::ERR_FALSCHERTYP);
                            break;
                    }

                    $P = array(0=>array('pipe','r')
                    , 1=>array('file',($rsXJO->FeldInhalt('XJO_PROTOKOLLDATEI')==''?'/var/log/awis/awisJobs.log':$rsXJO->FeldInhalt('XJO_PROTOKOLLDATEI')), 'a')
                    , 2=>array('file',($rsXJO->FeldInhalt('XJO_FEHLERDATEI')==''?'/var/log/awis/awisJobs.log':$rsXJO->FeldInhalt('XJO_FEHLERDATEI')), 'a')
                    );

                    $v = '/daten/jobs';

                    $e = NULL;
                    $p = proc_open($Befehl, $P, $pip, $v, $e);

                    if(is_resource($p))
                    {
                        $PInfo = proc_get_status($p);

                        $this->_SchreibeMeldung('I','+ Auftrag : ('.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').') laeuft mit PID '.$PInfo['pid'].'...');
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid']=$PInfo['pid'];
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']=$p;
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key']=$rsXJO->FeldInhalt('XJO_KEY');
                        if($rsXJO->FeldInhalt('XJO_MAXLAUFZEIT')>0)
                        {
                            $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']=time() + ($rsXJO->FeldInhalt('XJO_MAXLAUFZEIT')*60);
                        }
                        else	// Maximale Laufzeit ist ein ganzer Tag
                        {
                            $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']=time() + (1440*60);
                        }
                        $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['start']=time();

                        // Startversuch protokollieren
                        $SQL = 'UPDATE jobliste';
                        $SQL .= ' SET XJO_LETZTERSTART = SYSDATE';
                        $SQL .= ' , XJO_AKTUELLERKNOTEN = '.$this->_DB->FeldInhaltFormat('T',$this->_KnotenName);
                        $SQL .= ' , XJO_AKTUELLEPID = '.$this->_DB->FeldInhaltFormat('T',$PInfo['pid']);
                        $SQL .= ' , XJO_LETZTEMELDUNG = '.$this->_DB->FeldInhaltFormat('T','Job wurde gestartet...');
                        $SQL .= ' , XJO_USER = '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                        $SQL .= ' , XJO_USERDAT = SYSDATE';
                        $SQL .= ' WHERE XJO_KEY = '.$rsXJO->FeldInhalt('XJO_KEY');
                        $this->_DB->Ausfuehren($SQL);

                    }
                }
                elseif($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid']!=0)
                {
                    $this->_SchreibeMeldung('I','+ Pruefe Job '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].'...');

                    //Es kann sein, dass der Scheduler abgeraucht ist und er au�er der PID keine Infos zum Job hat
                    if(!is_resource($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']) and $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res'] == 'OLD'){
                        //L�uft der Job trotzdem noch?
                        if(posix_kill($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid'],0)){
                            if(time() > $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende'])                            {
                                $this->_SchreibeMeldung('W','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' lief zuerst in einem anderen Scheduler, laeuft nun aber zu lange und wird nun beendet...');

                                // Prozess beenden
                                $Erg = posix_kill ($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid'],9);
                                $this->JobEnde($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key'],0,true,$Erg);
                                $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);
                            }
                            else
                            {
                                $this->_SchreibeMeldung('I','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' lief zuerst in einem anderen Scheduler und laeuft noch bis maximal '.date('c',$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']).'...');
                            }
                        }else{
                            $this->_SchreibeMeldung('W','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' lief zuerst in einem anderen Scheduler, laeuft nun aber gar nicht mehr. Wei� nicht was ich tun soll ... :(');

                            $this->_MailObj->Betreff('WARNUNG-AWIS-Jobs: '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'));
                            $Empf = $rsXJO->FeldInhalt('XJO_WARNMAIL');
                            $Empf = ($Empf!=''?$Empf:'shuttle@de.atu.eu;');
                            $EmpfaengerListe=explode(';',$Empf);
                            $this->_MailObj->LoescheAdressListe();
                            foreach($EmpfaengerListe AS $Empfaenger)
                            {
                                $this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
                            }
                            $this->_MailObj->Absender($this->_AWISBenutzer->EMailAdresse());
                            $this->_MailObj->Prioritaet(awisMailer::PRIO_HOCH);
                            $Hinweis  = 'Irgendetwas ist mit unseren Scheduler passiert. Der Job lief zuerst auf einem anderen Scheduler. Als dann ein neuer Scheduler gestartet wurde, ';
                            $Hinweis .=  ' war der Prozess des Jobs nicht mehr da. Bitte Log des Jobs pr�fen und ggf. wiederholen. ' . PHP_EOL;
                            $Hinweis .= 'Jobname:      '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').PHP_EOL;
                            $Hinweis .= 'PID war:      '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['pid'].PHP_EOL;
                            $Hinweis .= 'Abbruch:       Laufzeit wurde ueberschritten.'.PHP_EOL;

                            $this->_MailObj->Text($Hinweis,awisMailer::FORMAT_TEXT,true);
                            if($this->_MailObj->MailSenden()==false)
                            {
                                $this->_MailObj->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
                            }

                            $this->JobEnde($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key'],0,0);
                            $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);

                        }

                    }else{
                        //Normaler Job der in dieser Instanz vom Scheduler gestartet
                        $PInfo = @proc_get_status($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']);
                        if($PInfo['running'])
                        {
                            if(time() > $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende'])
                            {
                                $this->_SchreibeMeldung('W','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' laeuft zu lange. Wird beendet...');

                                // Prozess beenden
                                fclose($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']);
                                $Erg = proc_close($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['res']);
                                $this->_SchreibeMeldung('W','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' wurde beendet...');
                                $this->JobEnde($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key'],$PInfo['exitcode'],true,$Erg);
                                $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);
                            }
                            else
                            {
                                $this->_SchreibeMeldung('I','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' laeuft noch bis maximal '.date('c',$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['ende']).'...');
                            }
                        }
                        else
                        {
                            if($PInfo['exitcode']==0)			// Kein Fehler beim Ende
                            {
                                $this->_SchreibeMeldung('I','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' ist fertig...');

                                $this->JobEnde($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key'],$PInfo['exitcode'],0);
                                $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);
                            }
                            else		// Ende mit einem Fehler
                            {
                                $this->_SchreibeMeldung('F','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' ist auf Fehler ('.$PInfo['exitcode'].') gelaufen. termsig='.$PInfo['termsig'].', stopsig='.$PInfo['stopsig'].'.');

                                $this->JobEnde($this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['key'],$PInfo['exitcode'],0);
                                $this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]=array('pid'=>0,'job'=>'','res'=>null);
                            }
                        }
                    }

                }
                sleep(5);

                $rsXJO->DSWeiter();

            }
        }
    }

    /**
     * Dokumentiert ein Job-Ende
     * @param int $XJO_KEY
     */
    public function JobEnde($XJO_KEY,$ExitCode,$Gestoppt,$Erg=null)
    {
        try
        {
            $this->_SchreibeMeldung('D','+ JobEnde fuer '.$XJO_KEY.' wird ausgefuehrt');

            $SQL = 'SELECT *';
            $SQL .= ' FROM jobliste';
            $SQL .= ' WHERE xjo_key='.$this->_DB->WertSetzen('XJO', 'N0', $XJO_KEY);
            $rsXJO = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XJO'));

            //***********************************************************************
            //* Jobliste wieder aktualisieren und n�chsten Termin berechnen
            //***********************************************************************
            $StartNeu = strtotime($rsXJO->Feldinhalt('XJO_NAECHSTERSTART'));

            $this->_SchreibeMeldung('D','+ Naechster '.$XJO_KEY.' Start '.date('d.m.Y H:i:s',$StartNeu));

            if($StartNeu<0)
            {
                $StartNeu = time();
            }

            // Einmaljobs
            if($rsXJO->Feldinhalt('XJO_INTERVALL')<=0)
            {
                $SQL = 'UPDATE jobliste';
                $SQL .= ' SET xjo_naechsterstart = null';
                $SQL .= ' ,xjo_letztesende = SYSDATE';
                $SQL .= ' , XJO_AKTUELLERKNOTEN = NULL';
                $SQL .= ' , XJO_AKTUELLEPID = NULL';
                $SQL .= ' , XJO_LETZTEMELDUNG = '.$this->_DB->FeldInhaltFormat('T','War ein einmaliger Job...');
                $SQL .= ',xjo_status=0';
                $SQL .= ', xjo_user=\'job\', xjo_userdat=SYSDATE';
                $SQL .= ' WHERE xjo_key = '.$rsXJO->Feldinhalt('XJO_KEY');

                $this->_DB->Ausfuehren($SQL);

                return true;
            }

            while($StartNeu < time())
            {
                $StartNeu = strtotime(date('d.m.Y H:i:s',$StartNeu).' '.$rsXJO->Feldinhalt('XJO_INTERVALL').' minutes');
                $this->_SchreibeMeldung('D','Verwende neue Berechnungsmethode, da die neue Startzeit in der Vergangenheit liegt. Startneu: ' . $StartNeu);
            }

            $StartZeit = date('H:i:s',$StartNeu);
            if($StartZeit=='00:00:00')
            {
                $StartZeit=='00:00:01';
            }
            $this->_SchreibeMeldung('D','Lege neue Startzeit fest: '.$StartZeit);

            $Tag=0;
            if(strtotime($StartZeit) < strtotime($rsXJO->FeldInhalt('XJO_SERVICESTART')))
            {
                $StartZeit = $rsXJO->Feldinhalt('XJO_SERVICESTART');
                $this->_SchreibeMeldung('D','Ausserhalb der Servicezeit: zu frueh. Setze neue Zeit: '.$StartZeit);
            }
            elseif(strtotime($StartZeit) > strtotime($rsXJO->FeldInhalt('XJO_SERVICEENDE')))
            {
                $Tag += 1;
                $StartZeit = $rsXJO->Feldinhalt('XJO_SERVICESTART');
                $this->_SchreibeMeldung('D','Ausserhalb fuer '.$XJO_KEY.' der Servicezeit: zu spaet. Setze neue Zeit: '.$StartZeit);
            }
            $StartNeu = date('d.m.Y',($StartNeu+($Tag*(60*60*24)))).' '.$StartZeit;
            $this->_SchreibeMeldung('D','Naechster Starttag: '.$StartNeu);
            // Wochentage ber�cksichtigen
            $TageArray = explode(';',$rsXJO->Feldinhalt('XJO_SERVICETAGE'));
            $i=0;
            $StartNeu = strtotime($StartNeu);
            while(array_search(date('w',$StartNeu),$TageArray)===false)
            {
                // Einen Tag weiter, fr�hester Termin!
                $StartNeu = strtotime(date('d.m.Y H:i:s',($StartNeu+(60*60*24)).' '.$StartZeit));
                if($i++>10)
                {
                    break;
                }
            }
            $this->_SchreibeMeldung('D','Naechster Startzeitpunkt '.$XJO_KEY.': '.date('c',$StartNeu));

            $SQL = 'UPDATE jobliste';
            $SQL .= ' SET xjo_naechsterstart = '.$this->_DB->FeldInhaltFormat('DU',date('d.m.Y H:i:s',$StartNeu));
            $SQL .= ' ,xjo_letztesende = SYSDATE';
            $SQL .= ' , XJO_AKTUELLERKNOTEN = NULL';
            $SQL .= ' , XJO_AKTUELLEPID = NULL';
            $SQL .= ' , XJO_LETZTEMELDUNG = '.$this->_DB->FeldInhaltFormat('T','Job wurde beendet. Exit-Code:'.$ExitCode.'. Abbruch:'.($Gestoppt?'Ja':'Nein').'.');
            $SQL .= ',xjo_status=1';
            $SQL .= ', xjo_user=\'job\', xjo_userdat=SYSDATE';
            $SQL .= ' WHERE xjo_key = '.$rsXJO->Feldinhalt('XJO_KEY');

            $this->_DB->Ausfuehren($SQL);

            $this->_SchreibeMeldung('I','+ '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' bekommt neuen Starttermin fuer '.$XJO_KEY.' um '.date('d.m.Y H:i:s',$StartNeu).'.');

            // Mail beim Abschluss
            if($rsXJO->FeldInhalt('XJO_WARNMAIL')!='' AND $Gestoppt)
            {
                $this->_MailObj->Betreff('WARNUNG-AWIS-Jobs: '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'));
                $EmpfaengerListe=explode(';',$rsXJO->FeldInhalt('XJO_WARNMAIL'));
                $this->_MailObj->LoescheAdressListe();
                foreach($EmpfaengerListe AS $Empfaenger)
                {
                    $this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
                }
                $this->_MailObj->Absender($this->_AWISBenutzer->EMailAdresse());
                $this->_MailObj->Prioritaet(awisMailer::PRIO_HOCH);
                $Hinweis  = 'Jobname:      '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').PHP_EOL;
                $Hinweis .= 'Abbruch:       Laufzeit wurde ueberschritten.'.PHP_EOL;

                $this->_MailObj->Text($Hinweis,awisMailer::FORMAT_TEXT,true);
                if($this->_MailObj->MailSenden()==false)
                {
                    $this->_MailObj->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
                }
                $this->_SchreibeMeldung('W','+ Warn-Mail fuer den Job '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' wurde versendet...');
            }

            if($rsXJO->FeldInhalt('XJO_INFOMAIL')!='')
            {
                $this->_MailObj->Betreff('INFO-AWIS-Jobs:'.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'));
                $EmpfaengerListe=explode(';',$rsXJO->FeldInhalt('XJO_INFOMAIL'));
                $this->_MailObj->LoescheAdressListe();
                foreach($EmpfaengerListe AS $Empfaenger)
                {
                    $this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
                }
                $this->_MailObj->Absender($this->_AWISBenutzer->EMailAdresse());
                $this->_MailObj->Prioritaet(awisMailer::PRIO_NIEDRIG);
                $Hinweis  = 'Jobname:      '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').PHP_EOL;
                $Hinweis .= 'Start:        '.$rsXJO->FeldInhalt('XJO_LETZTERSTART').PHP_EOL;
                $Hinweis .= 'Ende:         '.date('d.m.Y H:i:s').PHP_EOL;
                $Hinweis .= 'Exit-Code:    '.$ExitCode.PHP_EOL;
                $Hinweis .= 'Abbruch:      '.($Gestoppt?'Ja':'Nein').PHP_EOL;
                $Hinweis .= 'Neuer Termin: '.date('d.m.Y H:i:s',$StartNeu).PHP_EOL;

                $this->_MailObj->Text($Hinweis,awisMailer::FORMAT_TEXT,true);
                if($this->_MailObj->MailSenden()==false)
                {
                    $this->_MailObj->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
                }
                $this->_SchreibeMeldung('I','+ Info-Mail fuer den Job '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' wurde versendet...');
            }

            if($rsXJO->FeldInhalt('XJO_FEHLMAIL')!='' AND $ExitCode > 0)
            {
                $this->_MailObj->Betreff('FEHLER-AWIS-Jobs: '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'));
                $EmpfaengerListe=explode(';',$rsXJO->FeldInhalt('XJO_FEHLMAIL'));
                $this->_MailObj->LoescheAdressListe();
                foreach($EmpfaengerListe AS $Empfaenger)
                {
                    $this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
                }
                $this->_MailObj->Absender($this->_AWISBenutzer->EMailAdresse());
                $this->_MailObj->Prioritaet(awisMailer::PRIO_HOCH);
                $Hinweis  = 'Jobname:        '.$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').PHP_EOL;
                $Hinweis .= 'Start:          '.$rsXJO->FeldInhalt('XJO_LETZTERSTART').PHP_EOL;
                $Hinweis .= 'Ende:           '.date('d.m.Y H:i:s').PHP_EOL;
                $Hinweis .= 'Exit-Code:      '.$ExitCode.PHP_EOL;
                $Hinweis .= 'Abbruch:        '.($Gestoppt?'Ja':'Nein').PHP_EOL;
                $Hinweis .= 'Neuer Termin:   '.date('d.m.Y H:i:s',$StartNeu).PHP_EOL;
                $Hinweis .= 'Protokolldatei: '.$rsXJO->FeldInhalt('XJO_FEHLERDATEI').PHP_EOL;

                $this->_MailObj->Text($Hinweis,awisMailer::FORMAT_TEXT,true);
                if($this->_MailObj->MailSenden()==false)
                {
                    $this->_MailObj->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
                }
                $this->_SchreibeMeldung('W','+ Fehler-Mail fuer den Job '.$this->_Gruppen[$rsXJO->FeldInhalt('XJO_GRUPPE')]['job'].' wurde versendet...');
            }
        }
        catch (Exception $ex)
        {
            $this->_SchreibeMeldung('F','+ Fehler  '.$ex->getMessage());
            $this->_SchreibeMeldung('F','+   Zeile '.$ex->getLine());
            $this->_SchreibeMeldung('F','+   Nr    '.$ex->getCode());
        }
    }
    /**
     * Funktion pr�ft, ob der Jobscheduler aktiv ist
     * Liefert FALSE, wenn der Prozess noch l�uft, TRUE wenn er nicht l�uft oder beendet wurde
     */
    public function Watchdog()
    {
        if(!is_file($this->_PID_Datei))
        {
            return false;
        }

        $PID = file_get_contents($this->_PID_Datei);
        if($PID==$this->_PID)
        {
            $this->_SchreibeMeldung('I','Ich laufe noch...');
            return false;
        }

        $Meldung = system('ps -j '.$PID .' | grep '.$PID .' -c');
        $this->_SchreibeMeldung('I','Pruefe ob Scheduler mit folgender PID noch l�uft '.$PID.' :'.$Meldung);
        if($Meldung=='0')
        {
            $this->_SchreibeMeldung('I','  Prozess nicht vorhanden.');
            unlink($this->_PID_Datei);
            return false;
        }
        else
        {
            $this->_SchreibeMeldung('W','  Anderer Scheduler laeuft auf diesem Knoten.');
            $Anzahl = file_get_contents($this->_PID_Datei.'.chk');
            $Anzahl = (int)$Anzahl+1;

            if($Anzahl>10)
            {
                $this->_SchreibeMeldung('W','   das war die '.$Anzahl.'. Ueberpruefung. Breche Scheduler ab');
                $Erg = system('/bin/kill -15 '.$PID);
                if($Erg !== false)
                {
                    file_put_contents($this->_PID_Datei.'.chk', '1');
                }
                else
                {
                    $this->_SchreibeMeldung('F','   Kann Scheduler nicht beenden...');
                }
            }
            else
            {
                file_put_contents($this->_PID_Datei.'.chk', $Anzahl);
            }
        }

        return true;
    }

    /**
     * N�chsten Startzeitpunkt des Jobs setzen
     * @param int $XJO_KEY
     * @param string $StartNeu
     *
     * @version 1.1 - PG - 04.05.2016: $StartNeu darf nun leer sein, um einen Job zu deaktivieren.
     */
    public function SetzeNaechstenStart($XJO_KEY, $StartNeu)
    {
        try
        {
            $SQL = 'UPDATE jobliste';
            $SQL .= ' SET XJO_NAECHSTERSTART = '.($StartNeu!=''?$this->_DB->WertSetzen('XJO', 'DU', $StartNeu):'null');
            $SQL .= ' WHERE xjo_key='.$this->_DB->WertSetzen('XJO', 'N0', $XJO_KEY);

            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('XJO'));

            $this->_SchreibeMeldung('D','Manuelle Startzeit gesetzt fuer '.$XJO_KEY.': '.$StartNeu);

            return true;
        }
        catch (Exception $ex)
        {
            $this->_SchreibeMeldung('F','Fehler beim Setzen der manuelle Startzeit gesetzt fuer '.$XJO_KEY);
            return false;
        }
    }

    /**
     * Screibt eine Debug-Meldung auf den Bildschirm
     * @param string $Code
     * @param string $Meldung
     */
    private function _SchreibeMeldung($Code,$Meldung)
    {
        echo date('d.m.Y H:i:s');
        echo ';'.$this->_KnotenName;
        echo ';' . getmypid();
        echo ';'.$Code;
        echo ';'.$Meldung.PHP_EOL;
    }

    /**
     *
     * Liefert oder setzt das Wartungsmodus-Flag
     *
     * @param string $NeuerWert
     *
     * @return bool
     * @throws awisException
     */
    private function _Wartungsmodus($NeuerWert = ''){

        if($NeuerWert !== ''){
            if($NeuerWert=='1'){
                $this->_MailObj->LoescheAdressListe();
                $this->_MailObj->AdressListe(awisMailer::TYP_TO,'shuttle@de.atu.eu');
                $this->_MailObj->Betreff('ERROR Scheduler geht in den Wartungsmodus ');
                $this->_MailObj->Text('Scheduler wird nun keine Jobs mehr anstarten. Dringend handeln!',awisMailer::FORMAT_TEXT,true);
                $this->_MailObj->Prioritaet(1);
                $this->_MailObj->MailSenden();
            }
            $this->_SchreibeMeldung('D','Setze Wartungsmodus auf: '. $NeuerWert);
            $this->_AWISBenutzer->ParameterSchreiben('SchedulerWartungsModus',$NeuerWert);
        }
        $Wartungsmodus = $this->_AWISBenutzer->ParameterLesen('SchedulerWartungsModus',true);
        if($Wartungsmodus == '1' or $Wartungsmodus == 'M'){
            $this->_SchreibeMeldung('D','Scheduler ist im Wartungsmodus. ');
            return true;
        }

        return false;
    }

    /**
     * Pr�ft die Mounts und setzt ggf. den Wartungsmodus
     * @return bool
     */
    private function _PruefeMounts(){

        //Wenn manuell auf Wartungsmodusgesetzt wurde, dann nichts machen
        $Wartungsmodus = $this->_AWISBenutzer->ParameterLesen('SchedulerWartungsModus',true);
        if($Wartungsmodus=='M'){
            return false;
        }elseif($Wartungsmodus=='A'){
            //Wartungsmodus ist manuell aus
            return true;
        }

        $Erg = shell_exec('timeout 10 df');

        //Kein Timeout bekommen => Alles gut
        if(strlen($Erg) > 20){
            $this->_Wartungsmodus(0);
            return true;
        }else{
            if($this->_DebugLevel >= 90){
                $this->_SchreibeMeldung('D','PruefeMountsErgebnis: '. $Erg);
            }
            $this->_Wartungsmodus(1);
            return false;
        }
    }
}