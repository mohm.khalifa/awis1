<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht3950;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
    $TextKonserven = array();
    $TextKonserven[]=array('RFA','*');
    $TextKonserven[]=array('RFM','*');
    $TextKonserven[]=array('RFP','*');
    $TextKonserven[]=array('AST','*');
    
    $TextKonserven[]=array('FIL','FIL_ID');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('FIL','FIL_BEZ');
    
    $TextKonserven[]=array('FIB','FIB_BESTAND');
    $TextKonserven[]=array('FIB','FIB_BESTAND_kurz');
    
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','Soll');
	
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht3910 = $AWISBenutzer->HatDasRecht(3910);
	$Recht3950 = $AWISBenutzer->HatDasRecht(3950);

	if(($Recht3950&1)==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_POST);
	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RueckfuehrungOffen'));
	if(!isset($Param['KEY']))
	{
		$Param = array();
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['SPEICHERN']='off';
		$Param['DATUM_VOM']='';
		$Param['DATUM_BIS']='';
	}
    
    $Form->SchreibeHTMLCode('<form name="frmRueckfuehrungOffen" action="./rueckfuehrung_Main.php?cmdAktion=Offen" method="POST">');
			
	$Form->Formular_Start();	
	
	// Wiederholt auf der Seite oder neuer Aufruf?
	$WerteVorblenden='on';
	if (isset($_POST['txtRFP_AST_ATUNR']))		// Neue Werte eingegeben
	{
		$Param['DATUM_VOM']=isset($_POST['txtDATUM_VOM'])?$Form->Format('D',$_POST['txtDATUM_VOM'],true):$Param['DATUM_VOM'];
		$Param['DATUM_BIS']=isset($_POST['txtDATUM_BIS'])?$Form->Format('D',$_POST['txtDATUM_BIS'],true):$Param['DATUM_BIS'];
		$Param['RFM_FIL_ID']=$Form->Format('T',$_POST['txtRFM_FIL_ID'],true);
		$Param['RFP_AST_ATUNR']=$Form->Format('T',$_POST['txtRFP_AST_ATUNR'],true);
		$Param['FIL_GEBIET']=(isset($_POST['txtFIL_GEBIET'])?$Form->Format('N0',$_POST['txtFIL_GEBIET'],true):'');
		$Param['SPEICHERN']=isset($_POST['txtAuswahlSpeichern'])?'on':'off';
	}
	elseif(isset($_GET['Neu']) AND $Param['SPEICHERN']=='off')
	{
		$Param['DATUM_VOM']=date('d.m.Y', mktime(0,0,0,1,1,date('Y')));
		$Param['DATUM_BIS']=date("d.m.Y",mktime(0,0,0,date('m')-1,date('d'),date('Y')));
		$Param['RFM_FIL_ID']='';
		$Param['RFP_AST_ATUNR']='';
		$Param['FIL_GEBIET']='';
		$Param['ORDER']='';
		$Param['BLOCK']='';
		$WerteVorblenden='off';
	}		
	
	$Form->DebugAusgabe(1,$Param);
    
	$AWISCursorPosition = 'txtDATUM_VOM';
    // TODO: Vorbelegung Datumswerte �ber Parameter steuern
    // Datumsbereich vom festlegen, aktuell 1.1.YYYY
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('#DATUM_VOM',$Param['DATUM_VOM'],20,200,(($Recht3950&2)==2),'','','','D','L','','',10);
	$Form->ZeileEnde();
	
	// Datumsbereich bis festlegen, aktuell: Tagesdatum - 1Monat
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('#DATUM_BIS',$Param['DATUM_BIS'],20,200,(($Recht3950&4)==4),'','','','D','L','','',10);		
	$Form->ZeileEnde();

	$FilialListe=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	//$FilialListe='90,100';

	// Gebiet

	//Alle Gebiete anzeigen	
	if (($Recht3950&8)==8)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',190);
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
		$SQL .= ' FROM Filialebenen';
		$SQL .= ' WHERE FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
		$SQL .= ' ORDER BY 2';		
		$Form->Erstelle_SelectFeld('FIL_GEBIET',($WerteVorblenden=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();
	}
	elseif($FilialListe=='') // nur berechtigte Gebiete anzeigen
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',190);
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
		$SQL .= ' FROM Filialebenen';
		$SQL .= ' WHERE FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
		$SQL .= ' AND EXISTS';
		$SQL .= '  (select DISTINCT XX1_FEB_KEY';
		$SQL .= '  FROM V_FILIALEBENENROLLEN';
		$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
		$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND FRZ_GUELTIGAB <= SYSDATE AND FRZ_GUELTIGBIS >= SYSDATE';
		$SQL .= '  WHERE  XX1_FEB_KEY = FEB_KEY';		
		$SQL .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();			
		$SQL .= ')';
		$SQL .= ' ORDER BY 2';	
		$Form->Erstelle_SelectFeld('FIL_GEBIET',($WerteVorblenden=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();
	}
	else 
	{
		$AWISCursorPosition = 'txtRFP_AST_ATUNR';
	}
						
	//Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFM']['RFM_FIL_ID'].':',190);

	if($FilialListe!='')
	{
		$SQL = 'SELECT FIL_ID, FIL_ID || \' - \' || FIL_BEZ';
		$SQL .= ' FROM FILIALEN';
		$SQL .= ' WHERE FIL_ID IN('.$FilialListe.')';
		$SQL .= ' ORDER BY 1';
		$Form->Erstelle_SelectFeld('RFM_FIL_ID',($WerteVorblenden=='on'?$Param['RFM_FIL_ID']:'0'),400,true,$SQL);
	}
	else
	{
		$Form->Erstelle_TextFeld('RFM_FIL_ID',$WerteVorblenden=='on'?$Param['RFM_FIL_ID']:'',20,200,true,'','','','T','L','','',10);
	}
	$Form->ZeileEnde();
	
	//Artikel
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFP']['RFP_AST_ATUNR'].':',190);
	$Form->Erstelle_TextFeld('RFP_AST_ATUNR',$WerteVorblenden=='on'?$Param['RFP_AST_ATUNR']:'',20,200,true,'','','','T','L','','',10);
	$Form->ZeileEnde();
	
	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',187);
	$Form->Erstelle_Checkbox('AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':'off'),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	//Neu ist nur bei erstem Aufruf gesetzt, d.h. nichts machen
	if (!isset($_GET['Neu']))
	{
		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = _BedingungErstellen($Param,$BindeVariablen);
		
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if (!isset($_GET['Sort']))
		{
			if (isset($Param['ORDER']) and $Param['ORDER'] != '')
			{
				$ORDERBY = $Param['ORDER'];
			}
			else
			{
				$ORDERBY = ' rfm_fil_id, rfp_ast_atunr, rfa_nr DESC, rfa_lfdnr DESC';
			}
		}
		else
		{
			$ORDERBY = ' ' . str_replace ( '~', ' DESC ', $_GET ['Sort'] );
		}
		$Param['ORDER'] = $ORDERBY;
		$AWISBenutzer->ParameterSchreiben('Formular_RueckfuehrungOffen',serialize($Param));
	
		$SQL  = 'SELECT';
		$SQL .= ' ANFDAT.*,';
		$SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILENNR';
		$SQL .= ' FROM ';
		$SQL .= ' (';
		$SQL .= ' SELECT ';
		$SQL .= ' RFM_FIL_ID, ';
		$SQL .= ' (SELECT FIL_BEZ';
		$SQL .= '  FROM FILIALEN';
		$SQL .= '  WHERE FIL_ID = RFM_FIL_ID) AS FIL_BEZ,';
		$SQL .= ' (SELECT XX1_EBENE';
		$SQL .= '  FROM V_FILIALPFAD';
		$SQL .= '  WHERE XX1_FIL_ID = RFM_FIL_ID) AS FEB_BEZEICHNUNG,';
		$SQL .= ' RFP_AST_ATUNR,';
		$SQL .= ' (SELECT AST_BEZEICHNUNGWW';
		$SQL .= '  FROM ARTIKELSTAMM';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR) AS AST_BEZEICHNUNGWW,';
		$SQL .= ' RFA_KEY,';
		$SQL .= ' RFA_NR,';
		$SQL .= ' RFA_LFDNR,';
		$SQL .= ' RFA.RFA_DATUM,';
		$SQL .= ' NVL (RFM_MENGE, 0) AS RFM_MENGE,';
		$SQL .= ' NVL (RFP_MINDESTBESTAND, 0) AS RFP_MINDESTBESTAND,';
		$SQL .= ' (SELECT NVL (BESTAND, 0)';
		$SQL .= '  FROM V_FILIALBESTAND_AKTUELL';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR';
		$SQL .= '  AND FIL_ID = RFM.RFM_FIL_ID) AS FIL_BEST';
		$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN RFM ';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGENPOS RFP';
		$SQL .= ' ON RFM.RFM_RFP_KEY = RFP.RFP_KEY';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGEN RFA';
		$SQL .= ' ON RFA.RFA_KEY = RFP.RFP_RFA_KEY';
		$SQL .= ' WHERE RFA.RFA_RFS_KEY					= 10';
		$SQL .= ' AND RFM.RFM_MENGE						> 0';
		$SQL .= ' AND NVL (RFP.RFP_MINDESTBESTAND, 0)	= 0';
		if($Bedingung!='')
		{
			$SQL .= ' AND ' . substr($Bedingung,4);
		}		
		$SQL .= ' ) ANFDAT';
		$SQL .= ' WHERE FIL_BEST > 0';
		$SQL .= ' ORDER BY ' .$ORDERBY;
		
		//*****************************************************************
		// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll oder CSV 
		//*****************************************************************
		if(($AWIS_KEY1<=0))
		{
			//************************************************
			// Aktuellen Datenblock festlegen
			//************************************************
			$Block = 1;
			if(isset($_REQUEST['Block']))
			{
				$Block=$Form->Format('N0',$_REQUEST['Block'],false);
				$Param['BLOCK']=$Block;
			}
			elseif(isset($Param['BLOCK']) and ($Param['BLOCK'] != ''))
			{
				$Block=intval($Param['BLOCK']);
			}
	
			//************************************************
			// Zeilen begrenzen
			//************************************************
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
			$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

			$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',$StartZeile,false);
			$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',$StartZeile+$ZeilenProSeite,false) ;
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr >= :var_N0_ZEILE_VON AND  ZeilenNr < :var_N0_ZEILE_BIS';
			//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
		}
		else
		{
			$MaxDS = 1;
			$ZeilenProSeite=1;
			$Block = 1;
		}
		
		$Form->DebugAusgabe(1,$BindeVariablen);
		$Form->DebugAusgabe(1,$SQL);
		$rsRueck = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
				
		if($rsRueck->EOF())
		{
			echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
		} 
		elseif (($rsRueck->AnzahlDatensaetze()>0) or (isset($_REQUEST['Liste']))) // Liste anzeigen
		{
			$Form->Formular_Start();

			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			if (($Param['RFM_FIL_ID']=='') OR ($Param['FIL_GEBIET']!=0))
			{
				$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=rfm_fil_id'.((isset($_GET['Sort']) AND ($_GET['Sort']=='rfm_fil_id'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFM']['RFM_FIL_ID'],255,'',$Link);
		
				//$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				//$Link .= '&Sort=fil_bez'.((isset($_GET['Sort']) AND ($_GET['Sort']=='fil_bez'))?'~':'');
				//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],100,'',$Link);
				
				$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=feb_bezeichnung'.((isset($_GET['Sort']) AND ($_GET['Sort']=='feb_bezeichnung'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_GEBIET'],60,'',$Link);
			}
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=rfp_ast_atunr'.((isset($_GET['Sort']) AND ($_GET['Sort']=='rfp_ast_atunr'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['RFM']['RFM_AST_ATUNR'], 65, '', $Link );
	
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=ast_bezeichnungww'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ast_bezeichnungww'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],380,'',$Link);			
			
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=rfa_datum'.((isset($_GET['Sort']) AND ($_GET['Sort']=='rfa_datum'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_DATUM'],130,'',$Link);			
			
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFP_MINDESTBESTAND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFP_MINDESTBESTAND'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Soll'],80,'text-align:right',$Link);			
			
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=FIL_BEST'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_BEST'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIB']['FIB_BESTAND_kurz'],80,'text-align:right',$Link);			
			
			$Form->ZeileEnde ();

			$DS = 0;
			while (!$rsRueck->EOF())
			{
				$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
				
				if (($Param['RFM_FIL_ID']=='') OR ($Param['FIL_GEBIET']!=0))
				{
					$Form->Erstelle_ListenFeld('RFM_FIL_ID',$rsRueck->FeldInhalt('RFM_FIL_ID'),0,30,false,($DS%2),'','','T','L');
					$Form->Erstelle_ListenFeld('FIL_BEZ',$rsRueck->FeldInhalt('FIL_BEZ'),0,220,false,($DS%2),'','','T','L');
					$Form->Erstelle_ListenFeld('FEB_BEZEICHNUNG',$rsRueck->FeldInhalt('FEB_BEZEICHNUNG'),0,60,false,($DS%2),'','','T','L');
				}
				$Form->Erstelle_ListenFeld('RFP_AST_ATUNR',$rsRueck->FeldInhalt('RFP_AST_ATUNR'),0,65,false,($DS%2),'','','T','L');
				$Form->Erstelle_ListenFeld('AST_BEZEICHNUNGWW',$rsRueck->FeldInhalt('AST_BEZEICHNUNGWW'),0,380,false,($DS%2),'','','T','L');
				
/*				if(($Recht3910&16)==16)
					{	
					$Link = './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$rsRueck->FeldInhalt('RFA_KEY').'';
				}
				else{
					$Link ='';
				}
*/
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$rsRueck->FeldInhalt('RFA_KEY').'';
				
				$ANFNR = $Form->Format('D',$rsRueck->FeldInhalt('RFA_DATUM')).' - '.str_pad($Form->Format('T',$rsRueck->FeldInhalt('RFA_LFDNR')), 3, "0", STR_PAD_LEFT);			
				$Form->Erstelle_ListenFeld('RFA_DATUM',$ANFNR,0,130,false,($DS%2),'',$Link,'T','L');
				$Form->Erstelle_ListenFeld('RFP_MINDESTBESTAND',$rsRueck->FeldInhalt('RFP_MINDESTBESTAND'),0,80,false,($DS%2),'','','T','R');
				$Form->Erstelle_ListenFeld('FIL_BEST',$rsRueck->FeldInhalt('FIL_BEST'),0,80,false,($DS%2),'','','T','R');
				
				$Form->ZeileEnde();
				
				$rsRueck->DSWeiter();
				$DS ++;
			}
				
			$Link = './rueckfuehrung_Main.php?cmdAktion=Offen&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
			$Form->BlaetternZeile ($MaxDS, $ZeilenProSeite, $Link, $Block, '');


		}
	}
	$Form->Formular_Ende();
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	
	$Form->SchaltflaechenStart();	
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
	if (($Recht3950&16)==16)
	{
		$LinkPDF = '/berichte/drucken.php?XRE=16&ID';
		$Form->Schaltflaeche('href', 'cmdPDF', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'], 'W');		
	}
	if (($Recht3950&32)==32)
	{
		$LinkCSV = './rueckfuehrung_Offen_Export.php?cmdAktion=CSV';
		$Form->Schaltflaeche('href', 'cmdCSV', $LinkCSV, '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['Wort']['lbl_export'], 'W');		
	}
	$Form->SchaltflaechenEnde();

	$AWISBenutzer->ParameterSchreiben('Formular_RueckfuehrungOffen',serialize($Param));
	
	$Form->SetzeCursor ($AWISCursorPosition );
	$Form->SchreibeHTMLCode ('</form>');
}

catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201205161124");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201205161125");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function _BedingungErstellen($Param, &$BindeVariablen)
{
	global $Form;
	global $AWISBenutzer;
	global $DB;
	
	$Bedingung = '';
	if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
	{
		$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) >= :var_D_DATUM_VOM';
		$BindeVariablen['var_D_DATUM_VOM'] = $Form->Format('D',$Param['DATUM_VOM'],false);
	}
	
	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
	{
		$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) <= :var_D_DATUM_BIS';
		$BindeVariablen['var_D_DATUM_BIS'] = $Form->Format('D',$Param['DATUM_BIS'],false);
	}

	if(isset($Param['RFP_AST_ATUNR']) AND $Param['RFP_AST_ATUNR']!='')
	{
		$Bedingung .= ' AND RFP.RFP_AST_ATUNR = :var_T_RFP_AST_ATUNR';
		$BindeVariablen['var_T_RFP_AST_ATUNR'] = $Form->Format('T',$Param['RFP_AST_ATUNR'],true);
	}
	
	if(isset($Param['RFM_FIL_ID']) AND $Param['RFM_FIL_ID']!='')
	{
		$Bedingung .= ' AND rfm.rfm_fil_id = :var_N_RFM_FIL_ID';
		$BindeVariablen['var_N_RFM_FIL_ID'] = $Form->Format('N0',$Param['RFM_FIL_ID'],false);
	}

	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET']!=0)
	{
		$Bedingung .= ' AND EXISTS (';
		$Bedingung .= '  SELECT *';
		$Bedingung .= '  FROM V_FILIALPFAD';
		$Bedingung .= '  WHERE REGEXP_LIKE (XX1_PFADKEYS, :var_N_FIL_GEBIET)';
		$Bedingung .= '  AND XX1_FIL_ID = RFM.RFM_FIL_ID)';
		
		$BindeVariablen['var_N_FIL_GEBIET'] = $Form->Format('N0',$Param['FIL_GEBIET'],false);
	}

	return $Bedingung;
}

?>

