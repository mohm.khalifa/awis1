<?php
require_once('awisBenutzer.inc');

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 12.09.2016
 * Time: 13:03
 */
class eurechnungen_funktionen
{

    private $_AWISBenutzer;
    private $_DB;
    private $_Form;
    public $_AWISSprachkonserven;
    private $_aendern;
    private $_EREBedinnung;
    private $_rsERE;
    private $_rsERP;

    public function __construct()
    {
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_Form = new awisFormular(3);


        $TextKonserven = array();
        $TextKonserven[] = array('ERE', '%');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'WirklichLoeschen');
        $TextKonserven[] = array('Wort', 'Ja');
        $TextKonserven[] = array('Wort', 'Nein');
        $TextKonserven[] = array('FIL', '%');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
        $Recht47000 = $this->_AWISBenutzer->HatDasRecht(47000);

        if ($Recht47000 == 0) {
            $this->_Form->Fehler_KeineRechte();
        }

        if (($Recht47000 & 4) == 4) {
            $this->_aendern = true;
        } else {
            $this->_aendern = false;
        }
    }

    public function erstelleKopfAjax($ERE_ID)
    {
        global $AWIS_KEY1;

        if($ERE_ID!=''){
            $AWIS_KEY1 = $ERE_ID;
        }

        $FeldBreiten = array();
        $FeldBreiten['Labels'] = 220;
        $FeldBreiten['Werte'] = 220;
        $FeldBreiten['FIL_PLZ'] = 60;
        $FeldBreiten['FIL_ORT'] = $FeldBreiten['Werte'] - $FeldBreiten['FIL_PLZ'];
        $FeldBreiten['EKU_PLZ'] = 60;
        $FeldBreiten['EKU_ORT'] = $FeldBreiten['Werte'] - $FeldBreiten['EKU_PLZ'] - 40;

        $SQL = ' select * from V_FILIALEN_AKTUELL ';
        $SQL .= ' where ';
        $FIL = substr($ERE_ID,3, 3);
        if (intval($FIL)) {
            $SQL .= ' FIL_ID = ' . $this->_DB->WertSetzen('FIL', 'N0', $FIL);
        } else {
            $SQL .= ' lower(FIL_BEZ) like ' . $this->_DB->WertSetzen('FIL', 'T', '%' . mb_strtolower($FIL) . '%');
        }

        $rsFil = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FIL', true));

        $this->BedingungErstellen(array());
        $rsERE = $this->rsERE($this::ReturnRecordset);


        $SQL = 'SELECT EKU_KEY,';
        $SQL .= '   EKU_USTIDNR,';
        $SQL .= '   EKU_ANREDE,';
        $SQL .= '   EKU_NAME,';
        $SQL .= '   EKU_STRASSE,';
        $SQL .= '   EKU_LAN_NR,';
        $SQL .= '   EKU_PLZ,';
        $SQL .= '   EKU_ORT,';
        $SQL .= '   EKU_GUELTIG,';
        $SQL .= '   EKU_PRUEFDATUM,';
        $SQL .= '   EKU_PRUEFDATUM ';
        $SQL .= ' FROM EUKUNDEN ';
        $SQL .= ' where EKU_GUELTIG = 1';
        $SQL .= ' and EKU_USTIDNR = ' . $this->_DB->WertSetzen('EKU', 'T', isset($_GET['sucEKU_USTIDNR'])?$_GET['sucEKU_USTIDNR']:$rsERE->FeldInhalt('EKU_USTIDNR'));


        $rsEKU = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('EKU'));
        $this->_Form->DebugAusgabe(1, $this->_DB->LetzterSQL());

        $EKU_Aendern = false;
        if($rsEKU->FeldInhalt('EKU_KEY')==''){
            $EKU_Aendern = true;
            $this->_Form->Erstelle_HiddenFeld('EKU_PRUEFDATUM',date('d.m.Y'));
            $this->_Form->Erstelle_HiddenFeld('EKU_GUELTIG',1);
        }

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERK_DATUM'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextFeld('!ERK_DATUM', isset($_POST['txtERK_DATUM'])?$_POST['txtERK_DATUM']:$rsERE->FeldInhalt('ERK_DATUM'), $FeldBreiten['Werte'] / 10,
            $FeldBreiten['Werte'], true, '', '', '', 'D');

        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['EKU_LAN_NR'] . ':', $FeldBreiten['Labels']);
        $SQL = 'select lan_nr, lan_land from LAENDER';
        $this->_Form->Erstelle_SelectFeld('!EKU_LAN_NR',$rsEKU->FeldInhalt('EKU_LAN_NR'),($FeldBreiten['Werte']-52) . ':' . ($FeldBreiten['Werte']-52), $EKU_Aendern, $SQL, '', 276);

        $this->_Form->ZeileEnde();


        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['FIL']['FIL_ID'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ID')!=''?$rsFil->FeldInhalt('FIL_ID'):'Unbekannte Filiale', $FeldBreiten['Werte']);
        $this->_Form->Erstelle_HiddenFeld('ERK_FIL_ID', $rsFil->FeldInhalt('FIL_ID'));

        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['EKU_ANREDE'] . ':', $FeldBreiten['Labels']);
        $SQL = 'Select anr_id, anr_anrede from anreden';
        $this->_Form->Erstelle_SelectFeld('!EKU_ANREDE', $rsEKU->FeldInhalt('EKU_ANREDE'), $FeldBreiten['Werte'] -52 . ':' . ($FeldBreiten['Werte']-52), $EKU_Aendern, $SQL, '', 3);
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['FIL']['FIL_BEZ'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_BEZ'), $FeldBreiten['Werte']);
        $this->_Form->Erstelle_HiddenFeld('EKU_KEY', $rsEKU->FeldInhalt('EKU_KEY'));
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['EKU_NAME'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextFeld('!EKU_NAME', $rsEKU->FeldInhalt('EKU_NAME'), $FeldBreiten['Werte'] / 10, $FeldBreiten['Werte'], $EKU_Aendern, '', '', '', 'T');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['FIL']['FIL_STRASSE'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_STRASSE'), $FeldBreiten['Werte']);

        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['EKU_STRASSE'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextFeld('!EKU_STRASSE',$rsEKU->FeldInhalt('EKU_STRASSE'), $FeldBreiten['Werte'] / 10, $FeldBreiten['Werte'], $EKU_Aendern, '', '', '', 'T');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['FIL']['FIL_PLZ'] . '/' . $this->_AWISSprachkonserven['FIL']['FIL_ORT'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_PLZ'), $FeldBreiten['FIL_PLZ']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_ORT'), $FeldBreiten['FIL_ORT']);

        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['EKU_PLZ'] . '/' . $this->_AWISSprachkonserven['ERE']['EKU_ORT'] . ':', $FeldBreiten['Labels']);

        $this->_Form->Erstelle_TextFeld('!EKU_PLZ',$rsEKU->FeldInhalt('EKU_PLZ'), $FeldBreiten['EKU_PLZ'] / 10, $FeldBreiten['EKU_PLZ'], $EKU_Aendern, '', '', '', 'T');
        $this->_Form->Erstelle_TextFeld('!EKU_ORT',$rsEKU->FeldInhalt('EKU_ORT'), $FeldBreiten['EKU_ORT'] / 10, $FeldBreiten['EKU_ORT'], $EKU_Aendern, '', '', '', 'T');


        $this->_Form->ZeileEnde();
    }

    const ReturnRecordset = 1;

    const ReturnSQL = 2;

    const ReturnZeilenanzahl = 3;

    const ReturnBlaetternRS = 4;

    public function rsERE($ReturnTyp = ReturnRecordset, $Sort = '')
    {

        //Grund SELECT: Alle Derzeit G�ltigen Datens�tze
        $SQL = 'SELECT  ERK_ID,';
        $SQL .= '  ERK_DATUM,';
        $SQL .= '  ERK_FIL_ID,';
        $SQL .= '  ERK_USER,';
        $SQL .= '  ERK_USERDAT,';
        $SQL .= '  ERE_KEY,';
        $SQL .= '  ERE_EKU_KEY,';
        $SQL .= '  ERE_ERK_ID,';
        $SQL .= '  ERE_STORNO,';
        $SQL .= '  ERE_USER,';
        $SQL .= '  ERE_USERDAT,';
        $SQL .= '  EKU_KEY,';
        $SQL .= '  EKU_USTIDNR,';
        $SQL .= '  EKU_ANREDE,';
        $SQL .= '  EKU_NAME,';
        $SQL .= '  EKU_STRASSE,';
        $SQL .= '  EKU_LAN_NR,';
        $SQL .= '  EKU_PLZ,';
        $SQL .= '  EKU_ORT,';
        $SQL .= '  EKU_GUELTIG,';
        $SQL .= '  EKU_PRUEFDATUM,';
        $SQL .= '  EKU_USER,';
        $SQL .= '  EKU_USERDAT, ';
        $SQL .= '  ANR_ANREDE,';
        $SQL .= '  lan.LAN_CODE,  ';
        $SQL .= '  fil.lan_code as LAN_CODE_FIL,  ';
        $SQL .= '  FIL_BEZ ';

        if ($Sort != '') {
            $SQL .= ',row_number() over (order by ' . $Sort . ') AS ZeilenNr ';
        } else {
            $SQL .= ' ,rownum as Zeilennr';
        }
        $SQL .= ' FROM EURECHNUNGSKOPF erk';
        $SQL .= ' LEFT JOIN EURECHNUNG ere';
        $SQL .= ' ON ERK.ERK_ID = ERE.ERE_ERK_ID';
        $SQL .= ' LEFT JOIN EUKUNDEN eku';
        $SQL .= ' ON EKU.EKU_KEY = ERE.ERE_EKU_KEY ';
        $SQL .= ' LEFT JOIN ANREDEN ANR ';
        $SQL .= ' ON EKU.eku_anrede = ANR.ANR_ID ';
        $SQL .= ' LEFT JOIN LAENDER LAN ';
        $SQL .= ' ON EKU.EKU_LAN_NR = LAN.LAN_NR ';
        $SQL .= ' LEFT JOIN V_FILIALEN_AKTUELL FIL ';
        $SQL .= ' ON ERK.ERK_FIL_ID = FIL.FIL_ID ';

        if ($this->_EREBedinnung <> '')
        {
            $SQL .= ' WHERE ' .substr($this->_EREBedinnung, 4);
        }

        if ($Sort != '') {
            $SQL .= ' ORDER BY ' . $Sort;
        }

        switch ($ReturnTyp) {
            case self::ReturnSQL:
                return $SQL;
                break;
            case self::ReturnZeilenanzahl:
                return $this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->Bindevariablen('ERE', false));
                break;
            case self::ReturnRecordset:
                if ($this->_rsERE == '') {
                    $this->_rsERE = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ERE', true));
                }
                return $this->_rsERE;
                break;
            case self::ReturnBlaetternRS:
                return $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ERE', false));
                break;
        }
    }

    public function erstelleATUNRAjax($ATUNr)
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;

        if (isset($_GET['txtERK_ID'])) {
            $AWIS_KEY1 = $_GET['txtERK_ID'];
        }

        if (isset($_GET['txtERP_KEY'])) {
            $AWIS_KEY2 = $_GET['txtERP_KEY'];
        }

        $rsERP = $this->rsERP($this::ReturnRecordset, $AWIS_KEY2);
        $this->BedingungErstellen(array());
        $rsERE = $this->rsERE($this::ReturnRecordset);

        $FeldBreiten['Labels'] = 220;
        $FeldBreiten['Werte'] = 200;
        $FeldBreiten['ERP_BETRAG'] = 60;
        $FeldBreiten['ERP_MWST'] = 30;
        $FeldBreiten['ERP_MWST_KZ'] = 50;
        $FeldBreiten['ERP_BETRAG_NETTO_LABEL'] = 50;
        $FeldBreiten['ERP_MENGE_LABEL'] = 45;
        $FeldBreiten['ERP_MENGE'] = 150;

        $SQL = 'select ';
        $SQL .= '    COALESCE(AST_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW ';
        $SQL .= ' ,AST_VK ';
        $SQL .= ' from ARTIKELSTAMM ';
        $SQL .= ' where upper(AST_ATUNR) = ' . $this->_DB->WertSetzen('AST', 'T', mb_strtoupper($ATUNr));
        $rsATUNr = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST', true));

        //Bei ATU Artikeln, darf die Mehrwertsteuer nicht ge�ndert werden (Laut Altapplikation)
        if ($rsATUNr->AnzahlDatensaetze() > 0) {
            $MwStAendern = true; //20.12.2016. Lt. Taubmann Christina muss die MWST �nderbar bleiben
            $Bezeichnung = $rsATUNr->FeldInhalt('AST_BEZEICHNUNGWW');
            $Preis = $rsATUNr->FeldInhalt('AST_VK','N2');
            $this->_Form->Erstelle_HiddenFeld('ERP_VK_ORG', $Preis);
        } else {
            $MwStAendern = true;
            $Bezeichnung = isset($_POST['txtERP_ZUSATZTEXT'])?$_POST['txtERP_ZUSATZTEXT']:$rsERP->FeldInhalt('ERP_ZUSATZTEXT');
            $Preis = isset($_POST['txtERP_BETRAG'])?$_POST['txtERP_BETRAG']:$rsERP->FeldInhalt('ERP_VK_NEU','N2');
        }

        //Bei Preissenkungen m�ssen nicht alle Felder angezeigt werden.
        $MinAnsicht = '';
        if (mb_strtoupper($ATUNr) == 'PREISSENKUNG' or isset($_GET['txtPOS_TYP']) and $_GET['txtPOS_TYP'] > 1) {
            $MinAnsicht = 'display: none';
        }

        if (!$MinAnsicht) {
            $this->_Form->ZeileStart($MinAnsicht);
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_ZUSATZTEXT'] . ':', $FeldBreiten['Labels']);
            $this->_Form->Erstelle_TextFeld('!ERP_ZUSATZTEXT', $Bezeichnung, $FeldBreiten['Werte'] / 10, $FeldBreiten['Werte'] - 15, $this->_aendern, 'D', '', '', 'T');
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_MENGE'] . ':', $FeldBreiten['ERP_MENGE_LABEL']);
            $this->_Form->Erstelle_TextFeld('!ERP_MENGE', isset($_POST['txtERP_MENGE'])?$_POST['txtERP_MENGE']:$rsERP->FeldInhalt('ERP_MENGE'), $FeldBreiten['ERP_MENGE'] / 10,
                $FeldBreiten['ERP_MENGE'], $this->_aendern, 'D', '', '', 'T');
            $this->_Form->ZeileEnde();
        }

        $this->_Form->ZeileStart();

        if (!$MinAnsicht) {
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_BETRAG'] . ':', $FeldBreiten['Labels'], $MinAnsicht);
            $this->_Form->Erstelle_TextFeld('!ERP_BETRAG', $Preis, ($FeldBreiten['ERP_BETRAG'] / 10), $FeldBreiten['ERP_BETRAG'], $this->_aendern, $MinAnsicht, '', '', 'T', '', '',
                '', '', '', '');
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_MWST'] . ':', $FeldBreiten['ERP_MWST'] + 15, $MinAnsicht);
        }
        $LKZ = $this->_DB->RecordSetOeffnen('SELECT LAN_CODE FROM V_FILIALEN_AKTUELL WHERE FIL_ID = ' . $this->_DB->WertSetzen('FIL', 'N0', $rsERE->FeldInhalt('ERK_FIL_ID')),
            $this->_DB->Bindevariablen('FIL'))->FeldInhalt(1);

        $SQL = " select  EST_KEY, EST_STEUERSATZ$LKZ  as Steuersatz from eusteuer ";
        if (!$MwStAendern) {
            $SQL .= ' WHERE EST_ID = 0 order by EST_DATUM desc ';
            $rsMwST = $this->_DB->RecordSetOeffnen($SQL);
            $MwStSatz = $rsMwST->FeldInhalt(2);
            $ERP_BETRAG_NETTO = round($Preis / (100 + $MwStSatz) * 100, 2);
            $ERP_MWST = $Preis - $ERP_BETRAG_NETTO;
            $this->_Form->Erstelle_HiddenFeld('ERP_EST_KEY', $rsMwST->FeldInhalt(1));
            $this->_Form->Erstelle_TextFeld('!ERP_MWST', $MwStSatz, $FeldBreiten['ERP_MWST'] / 10, $FeldBreiten['ERP_MWST'], true, $MinAnsicht, '', '', 'T', 'L', '', '', '', '',
                '', 'readonly=readonly');
        } else {
            $MwSTWert = isset($_POST['txtERP_EST_KEY'])?$_POST['txtERP_EST_KEY']:$rsERP->FeldInhalt('ERP_EST_KEY');
            $ERP_MWST = isset($_POST['txtERP_MWST'])?$_POST['txtERP_MWST']:$rsERP->FeldInhalt('ERP_MWSTZ');
            $ERP_BETRAG_NETTO = isset($_POST['txtERP_BETRAG_NETTO'])?$_POST['txtERP_BETRAG_NETTO']:$rsERP->FeldInhalt('ERP_BETRAG_NETTO','N2');
            $this->_Form->Erstelle_SelectFeld('!ERP_EST_KEY', $MwSTWert, $FeldBreiten['ERP_MWST_KZ'], $MwStAendern, $SQL, '%', '', '', $MinAnsicht);
        }
        $this->_Form->Erstelle_TextLabel('%', 30, $MinAnsicht);

        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_MWST'] . ':', $FeldBreiten['ERP_MWST'] + 15, $MinAnsicht);
        $this->_Form->Erstelle_TextFeld('ERP_MWST_WERT', $ERP_MWST, $FeldBreiten['ERP_MWST'] / 10, $FeldBreiten['ERP_MWST'], true, $MinAnsicht, '', '', 'T', 'L', '', '', '', '',
            '', '');

        if ($MinAnsicht) {
            $LabelBreite = $FeldBreiten['Werte'] + 20;
        } else {
            $LabelBreite = $FeldBreiten['ERP_BETRAG_NETTO_LABEL'];
        }
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachkonserven['ERE']['ERP_BETRAG_NETTO'] . ':', $LabelBreite);
        $this->_Form->Erstelle_TextFeld('!ERP_BETRAG_NETTO', $ERP_BETRAG_NETTO, $FeldBreiten['ERP_BETRAG'] / 10, $FeldBreiten['ERP_BETRAG'], true, 'D', '', '', 'T', 'L', '', '',
            '', '', '', '');

        $this->_Form->ZeileEnde();

        echo "<script type=\"application/javascript\" src=\"eurechnungen.js\"></script>";
    }

    public function rsERP($ReturnTyp, $ERP_KEY = 0)
    {
        global $AWIS_KEY1;
        $SQL = 'SELECT * from EURechnungspositionen';
        $SQL .= ' WHERE ERP_ERK_ID = ' . $this->_DB->WertSetzen('ERP', 'T', $AWIS_KEY1);
        if ($ERP_KEY) {
            $SQL .= ' and ERP_KEY = ' . $this->_DB->WertSetzen('ERP', 'N0', $ERP_KEY);
        }
        $SQL .= ' ORDER by ERP_POSITION asc';

        switch ($ReturnTyp) {
            case $this::ReturnSQL:
                return $SQL;
                break;
            case $this::ReturnZeilenanzahl:
                return $this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->Bindevariablen('ERP', false));
                break;
            case $this::ReturnRecordset:
                if ($this->_rsERP == '') {
                    $this->_rsERP = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ERP', true));
                }

                return $this->_rsERP;
                break;
        }
    }


    public function BedingungErstellen($Param = array())
    {
        global $AWIS_KEY1;
        $this->_EREBedinnung = '';

        if ($AWIS_KEY1 != '') {
            $this->_EREBedinnung .= ' AND ERK_ID=' . $this->_DB->WertSetzen('ERE','T',$AWIS_KEY1);
        }elseif(isset($Param['ERE_RECHNUNGSNUMMER']) and $Param['ERE_RECHNUNGSNUMMER'] != ''){
            $this->_EREBedinnung .= ' AND ERK_ID =' . $this->_DB->WertSetzen('ERE','T',$Param['ERE_RECHNUNGSNUMMER']);
        }

    }

    public function WirklichLoeschen()
    {
        global $AWIS_KEY1;
        $this->_Form->SchreibeHTMLCode('<form name=frmLoeschen action=./eurechnungen_Main.php?cmdAktion=' . $_GET['cmdAktion'] . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');
        $this->_Form->Erstelle_HiddenFeld('ERK_ID', $AWIS_KEY1);

        $this->_Form->Formular_Start();
        $this->_Form->ZeileStart();
        $this->_Form->Hinweistext($this->_AWISSprachkonserven['Wort']['WirklichLoeschen']);
        $this->_Form->ZeileEnde();

        $this->_Form->Trennzeile();

        $this->_Form->ZeileStart();
        $this->_Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $this->_AWISSprachkonserven['Wort']['Ja'], '');
        $this->_Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $this->_AWISSprachkonserven['Wort']['Nein'], '');
        $this->_Form->ZeileEnde();

        $this->_Form->SchreibeHTMLCode('</form>');

        $this->_Form->Formular_Ende();
        die;
    }

}