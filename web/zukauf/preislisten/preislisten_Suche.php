<?php
require_once 'preislisten_funktionen.inc';

try
{
    $ZPL = new preislisten_funktionen();
    $Speichern = isset($ZPL->Param['SPEICHERN']) && $ZPL->Param['SPEICHERN'] == 'on'?true:false;

    $ZPL->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./preislisten_Main.php?cmdAktion=Details>";

	$ZPL->Form->Formular_Start();
    
	$ZPL->Form->ZeileStart();
	$ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL']['ZPL_BEZEICHNUNG'].':',190);
	$ZPL->Form->Erstelle_TextFeld('*ZPL_BEZEICHNUNG',($Speichern?$ZPL->Param['ZPL_BEZEICHNUNG']:''),10,110,true);
	$ZPL->AWISCursorPosition='sucZPL_BEZEICHNUNG';
	$ZPL->Form->ZeileEnde();


    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL']['ZPL_LIE_NR'].':',190);
    $ZPL->Form->Erstelle_TextFeld('*ZPL_LIE_NR',($Speichern?$ZPL->Param['ZPL_LIE_NR']:''),10,110,true);
    $ZPL->AWISCursorPosition='sucZPL_LIE_NR';
    $ZPL->Form->ZeileEnde();


    // Auswahl kann gespeichert werden
    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $ZPL->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $ZPL->Form->ZeileEnde();

	$ZPL->Form->Formular_Ende();

	$ZPL->Form->SchaltflaechenStart();
	$ZPL->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$ZPL->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$ZPL->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $ZPL->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($ZPL->Recht10070&4) == 4){
		$ZPL->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $ZPL->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$ZPL->Form->SchaltflaechenEnde();

	$ZPL->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($ZPL->Form instanceof awisFormular)
	{
		$ZPL->Form->DebugAusgabe(1, $ex->getSQL());
		$ZPL->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$ZPL->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($ZPL->Form instanceof awisFormular)
	{
		$ZPL->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>