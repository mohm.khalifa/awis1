<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('SVT','Import*');
	$TextKonserven[]=array('Liste','SVT_IMPORTMODUS');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);
	if($Recht19000==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	$Form->Formular_Start();

	if(!empty($_FILES))
	{
        if(($fd = fopen($_FILES['IMPORT']['tmp_name'],'r'))!==false)
        {
            $Zeile = fgets($fd);
        }
		else
		{
			$Form->ZeileStart();
			$Form->Hinweistext(str_replace('$1','CSV',$AWISSprachKonserven['FEHLER']['err_UngueltigesUploadDateiFormat']),'Warnung','');
			$Form->ZeileEnde();
		}
    }



    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['ImportDatei'].':', 250);
	$Form->Erstelle_DateiUpload('IMPORT',600,30,10000000,'');
    $AWISCursorPosition='IMPORT';
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['ImportModus'].':', 250);
    $ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVT_IMPORTMODUS']);
	$Form->Erstelle_SelectFeld('IMPORTMODUS', 1, 300, true, null,  $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$ListenDaten);
    $AWISCursorPosition='IMPORT';
    $Form->ZeileEnde();

    $Form->Formular_Ende();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (Exception $ex)
{

}

?>