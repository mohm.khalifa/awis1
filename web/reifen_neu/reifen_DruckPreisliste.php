<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('LIK','%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('Wort','EANNummer');
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht15101 = $AWISBenutzer->HatDasRecht(15101);
	if($Recht15101==0)	
	{
		$Form->Fehler_KeineRechte();
	}
	
	//$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');	

	$Param = unserialize($AWISBenutzer->ParameterLesen("Formular_Reifen_Preislistendruck"));	

	$Spalte = 10;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	$AnzZeilen=0;
	
	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Preisliste');
	
	$Zeile=_Erzeuge_Seite_Preisliste();

	//Land zu der eingegebenen Filiale ermitteln
	$SQL = 'SELECT LAN_CODE';
	$SQL.= ' FROM FILIALEN';
	$SQL.= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
	$SQL.= ' WHERE FIL_ID = '.$Param['FIL_ID'];
	
	$rsLAN = $DB->RecordSetOeffnen($SQL);
	if ($rsLAN->AnzahlDatensaetze()==0)
	{
		$Form->Hinweistext('Ung�ltige Filiale!');
		die();		
	}
	else 
	{
		$LAN_CODE = $rsLAN->FeldInhalt('LAN_CODE');
	}
	
	
	$SQL = 'select a.*, aa.Anz';
	$SQL.= ' from ( select aa.*';
	$SQL.= ' ,(SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = RRE_AST_ATUNR AND ASI_AIT_ID = 140) AS Sort';
	$SQL.= ' from (';
	$SQL.= ' select r.*, aa.ast_wgr_id, H.REH_BEZEICHNUNG FROM ';
	$SQL.= ' ( select * from v_reifen union select * from v_reifen_sonderbestellung where rre_ungelistet=0 ) r';
	$SQL.= ' INNER JOIN ARTIKELSTAMM A ON A.AST_ATUNR = RRE_AST_ATUNR';
	$SQL.= ' INNER JOIN V_ARTIKELSTAMMWGR AA ON AA.AST_ATUNR = RRE_AST_ATUNR';
	$SQL.= ' LEFT JOIN REIREIFENHERSTELLER H ON RRE_REH_KEY = REH_KEY';
	$SQL.= ' LEFT JOIN REIREIFENAKTIONEN ';
    $SQL.= ' ON ((RRA_RRE_RRP_KEY = RRE_RRP_KEY)';
    $SQL.= ' OR (RRA_WGR_ID = AST_WGR_ID AND RRA_WUG_ID = AST_WUG_ID)';
    $SQL.= ' OR (RRA_KENN1 = a.AST_KENNUNG)';
    $SQL.= ' OR (RRA_KENN2 = AST_KENN2)';
    $SQL.= ' OR (RRA_REH_KEY = RRE_REH_KEY))';
    // G�ltigkeiten Pr�fen
    $SQL.= ' AND RRA_GUELTIGAB <= SYSDATE AND RRA_GUELTIGBIS >= SYSDATE';
	$SQL.= ' ) aa';
	$SQL.= ' ) a';
	$SQL.= ' left join';
	$SQL.= ' (';
	$SQL.= ' select Sort, count(*) as Anz';
	$SQL.= ' from (';
	$SQL.= ' select aa.*';
	$SQL.= ' ,(SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = RRE_AST_ATUNR AND ASI_AIT_ID = 140) AS Sort';
	$SQL.= ' from (';
	$SQL.= ' select * FROM ';
	$SQL.= ' ( select * from v_reifen union select * from v_reifen_sonderbestellung where rre_ungelistet=0 )';
	$SQL.= ' INNER JOIN ARTIKELSTAMM A ON A.AST_ATUNR = RRE_AST_ATUNR';
	$SQL.= ' INNER JOIN V_ARTIKELSTAMMWGR AA ON AA.AST_ATUNR = RRE_AST_ATUNR';	
	$SQL.= ' ) aa';
	$SQL.= ' ) ';
	$SQL.= ' group by sort';
	$SQL.= ' ) aa on  aa.Sort = a.Sort';		
  	
  	if (isset($Param['WGR_ID']) and $Param['WGR_ID']!='')
	{
		$SQL.= ' WHERE AST_WGR_ID = \''.$Param['WGR_ID'].'\'';
	}	
	$SQL.=' ORDER BY a.Sort';		

	//echo $SQL;
	//die();
	
	$rsREI = $DB->RecordSetOeffnen($SQL);
	$rsREIZeilen = $rsREI->AnzahlDatensaetze();

	if($rsREIZeilen==0)
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}

	$Sort = $rsREI->FeldInhalt('SORT');	
	
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(285,5,$rsREI->FeldInhalt('SORT'),1,0,'C',0);	
	$Zeile+=5;
	$AnzZeilen++;
	
	for($REIZeile=0;$REIZeile<$rsREIZeilen;$REIZeile++)
	{					
		if ($Sort != $rsREI->FeldInhalt('SORT'))
		{					
			if($AnzZeilen + 2 + $DB->FeldInhaltFormat('Z',$rsREI->FeldInhalt('ANZAHL')) >= 32)
			{
				$AnzZeilen=0;
				$Zeile=_Erzeuge_Seite_Preisliste();
			}
			else 
			{
				//Leerzeile			
				$Zeile+=5;	
			}			
						
			$AnzZeilen++;		
			$Ausdruck->_pdf->SetFont('Arial','B',8);		
			$Ausdruck->_pdf->SetFillColor(255,255,000);			
			$Ausdruck->_pdf->setXY($Spalte,$Zeile);
			$Ausdruck->_pdf->cell(285,5,$rsREI->FeldInhalt('SORT'),1,0,'C',1);	
			$Zeile+=5;	
			$AnzZeilen++;		
		}
		
		//if(($Ausdruck->SeitenHoehe()-20)<$Zeile)		
		if($AnzZeilen >= 32)
		{
			$AnzZeilen=0;
			$Zeile=_Erzeuge_Seite_Preisliste();
			$AnzZeilen++;		
		}

		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(15,5,$rsREI->FeldInhalt('RRE_AST_ATUNR'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->cell(30,5,$rsREI->FeldInhalt('REH_BEZEICHNUNG'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+45,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('RRE_BREITE'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+55,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('RRE_QUERSCHNITT'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+65,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('RRE_BAUART'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+75,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('RRE_INNENDURCHMESSER'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+85,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('LLKW'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+95,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('LOADINDEX'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('LOADINDEX2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('SPEEDINDEX'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+125,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('LOADINDEX3'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+135,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('SPEEDINDEX2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+145,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('RF'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+155,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('ZUSATZBEMERKUNG2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+165,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('ZUSATZBEMERKUNG3'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+175,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('ZUSATZBEMERKUNG'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+185,$Zeile);
		$Ausdruck->_pdf->cell(50,5,$rsREI->FeldInhalt('BEZEICHNUNG'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+235,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('AST_KENNUNG'),1,0,'L',0);	
		
		$Feld = 'AST_VK';
				
		if ($LAN_CODE != 'DE')
		{
			$Feld = 'AST_VK'.$LAN_CODE;
		}
			
		$VK = $rsREI->FeldInhalt($Feld);
					
		if ($rsREI->FeldInhalt('RRE_UNGELISTET') == '0' and ($Param['FIL_ID']!=''))
		{
			$SQL = 'SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=\'' . $rsREI->FeldInhalt('RRE_AST_ATUNR') . '\' AND FIB_FIL_ID='.$Param['FIL_ID'];
			
			$rsVK = $DB->RecordSetOeffnen($SQL);
			
			if ($rsVK->AnzahlDatensaetze()>0)
			{
				$VK = $rsVK->FeldInhalt('FIB_VK');
			}
		}
		
		if ($rsREI->FeldInhalt('AST_SUCH2')!='')
		{
			if ($rsREI->FeldInhalt('VKVORBEREITUNG')!='')
			{
				$Ausdruck->_pdf->SetFont('Arial','B',8);	
				$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
				$Ausdruck->_pdf->cell(10,5,'i.V.',1,0,'L',0);										
			}
			else
			{
				$Ausdruck->_pdf->SetFont('Arial','B',8);	
				$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
				$Ausdruck->_pdf->cell(10,5,'',1,0,'L',0);						
			}
		}
		else
		{
			$Ausdruck->_pdf->SetFont('Arial','B',8);	
			$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
			$Ausdruck->_pdf->cell(10,5,awisFormular::Format('N2',$VK),1,0,'L',0);		
		}

		$Ausdruck->_pdf->setXY($Spalte+255,$Zeile);
		$Ausdruck->_pdf->cell(10,5,($rsREI->FeldInhalt('RRA_AKTIONPROZENT')=='0'?'':$rsREI->FeldInhalt('RRA_AKTIONPROZENT')),1,0,'L',0);	
		
		
		if($rsREI->FeldInhalt('RRA_AKTIONTEXT')!='')
		{
			$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
			$Ausdruck->_pdf->cell(20,5,$rsREI->FeldInhalt('RPR_AKTIONTEXT'),1,0,'L',0);	
		}
		elseif($rsREI->FeldInhalt('AST_SUCH2')!='')
		{
			if ($rsREI->FeldInhalt('VKVORBEREITUNG') == '')
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'i.V.',1,0,'L',0);	
				
			}
			elseif($rsREI->FeldInhalt('VKVORBEREITUNG') != '')
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,$rsREI->FeldInhalt('VKVORBEREITUNG'),1,0,'L',0);	
			}
			else
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'',1,0,'L',0);	
			}
		}
		else
		{
			$Aktion=0;
			//Spalte Aktion berechnen
			if ($rsREI->FeldInhalt('RRA_AKTIONPROZENT') != '')
			{
				$Aktion = (100 - $rsREI->FeldInhalt('RRA_AKTIONPROZENT')) / 100; 
				$Aktion = str_replace(',','.',$Aktion) * str_replace(',','.',$VK);
				
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,($Aktion==0?'':$Aktion),1,0,'L',0);	
			}
			else 
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'',1,0,'L',0);	
			}		
		}

		$Zeile+=5;
		$AnzZeilen++;
		
		$Sort = $rsREI->FeldInhalt('SORT');
		
		$rsREI->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Preisliste()
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	global $AWIS_KEY1;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	//$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Ausdruck->_pdf->SetFillColor(255,165,000);
	
	$Zeile = 10;
	
	$Ausdruck->_pdf->SetFont('Arial','B',15);		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	//$Ausdruck->_pdf->cell(15,5,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);				
	$Ausdruck->_pdf->cell(50,10,'Preisliste',0,0,'L',0);				
	
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
		
	$Zeile+=5;
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);		
	$Zeile=$Zeile+10;		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	//$Ausdruck->_pdf->cell(15,5,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);				
	$Ausdruck->_pdf->cell(15,5,'ATU-Nr',1,0,'L',1);
		
	$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(170,5,'Reifenkennzeichen',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+185,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(50,5,'Bezeichnung',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+235,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'K',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'VK',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+255,$Zeile);	
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'%',1,0,'L',1);	

	$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(20,5,'Aktion',1,0,'L',1);		
				
	$Zeile+=5;	
	
	return $Zeile;
}

?>