<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('MIU','%');
	$TextKonserven[]=array('MIU','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AlleSetzen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3023= $AWISBenutzer->HatDasRecht(3023);		// Herstelle
	if(($Recht3023&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$SQL = 'SELECT MITBEWERBUTYPEN.*, MUT_UNTERNEHMENSTYP, MUT_KEY ';
	$SQL .= ' FROM MitbewerbUTypenTypen ';
	$SQL .= ' LEFT OUTER JOIN MITBEWERBUTYPEN ON MUT_KEY = MIU_MUT_KEY AND MIU_MBW_KEY = 0'.$AWIS_KEY1;
	$SQL .= ' WHERE MUT_MBK_KEY = 28 AND MUT_STATUS = \'A\'';		// Vertragsverkstätten
	$SQL .= ' ORDER BY nlssort(MUT_UNTERNEHMENSTYP,\'NLS_SORT=BINARY\')';

	$rsMIU = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['FIL_ID']=$AWIS_KEY1;

	$Form->Formular_Start();
	$EditModus = ($Recht3023&6);

	while(!$rsMIU->EOF())
	{
		$Form->ZeileStart();

		$Form->Erstelle_TextLabel($rsMIU->FeldInhalt('MUT_UNTERNEHMENSTYP').':',200);
		$Name = 'MIU_MUT_KEY_'.$rsMIU->FeldInhalt('MIU_KEY').'_'.$rsMIU->FeldInhalt('MUT_KEY');
		$Form->Erstelle_Checkbox($Name,($rsMIU->FeldInhalt('MIU_KEY')==''?'off':'on'),50,$EditModus,'on');

		$Form->ZeileEnde();

		$rsMIU->DSWeiter();
	}

	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>