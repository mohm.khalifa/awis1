<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class hersteller_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $Recht602;
    public $AWISSprachKonserven;
    public $Param;
    public $AWIS_KEY1;
    public $AWIS_KEY2;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht602 = $this->AWISBenutzer->HatDasRecht(602);


        $this->Param = unserialize($this->AWISBenutzer->ParameterLesen('Formular_HER'));

        $TextKonserven[] = array('HER', '%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_HER',serialize($this->Param));
    }

    public function erstelleHERBedingung($Param = array())
    {
        $Bedingung = '';

        if ($this->AWIS_KEY1 != '') {
            $Bedingung .= ' AND HER_ID = ' . $this->DB->WertSetzen('HER', 'T', $this->AWIS_KEY1);
            $Param['WHERE'] = $Bedingung;

            return $Bedingung;
        }

        $Param['WHERE'] = $Bedingung;

        return $Bedingung;
    }

}