<?php
/**
 * Abstrakte Klasse f�r die Zuk�ufe
 *
 * @author Sacha Kerres
 * @version 200901
 * @copyright ATU Auto Teile Unger
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisProtokoll.inc');


class awisZukauf_Import
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	protected $_DB = null;

	/**
	 * Debugausgaben
	 *
	 * @var int
	 */
	protected $_DebugLevel = 0;

	/**
	 * Lieferantennummer im AWIS
	 *
	 * @var int
	 */
	protected  $_LIE_NR = 0;

	/**
	 * Ziel-E-Mail bei Preisabweichungen
	 *
	 * @var string
	 */
	protected $_EMAIL_PreisProblem = array('shuttle@de.atu.eu');

	/**
	 * Empf�nger f�r Fehler-Mails
	 *
	 * @var string
	 */
	protected $_EMAIL_Fehler = array('shuttle@de.atu.eu');
	/**
	 * Empf�nger f�r Fehler-Mails
	 *
	 * @var string
	 */
	protected $_EMAIL_Infos = array('shuttle@de.atu.eu');

	/**
	 * Aktueller Benutzer
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	/**
	 * AWIS Werkzeuge
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeug = null;
	/**
	 * ZukaufObjekt
	 * @var awisZukauf
	 */
	private $_ZukaufObj = null; 
	/**
	 * Standard MWSt-Satz
	 * @var int
	 */
	const MWST_DE_STANDARD = 2;
	const MWST_DE_ALTTEIL = 3;
	const MWST_DE_REDUZIERT = 1;
	
	const MWST_ID_STANDARD = 2;
	
	
	const MWST_ID_AT_OHNE = 22;
	const MWST_ID_AT_STANDARD = 21;
	const MWST_ID_AT_REDUZIERT = 20;
	
	
	/**
	 * Importquelle f�r die automatisch generierten Daten
	 *
	 */
	const IMPORTQUELLEN_ID = 4096;


	const ARTIKELIDTYP_EAN = 1;
	
	const ARTIKELIDTYP_ATUNROHNE = 4;
	
	public function __construct($Benutzer)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISWerkzeug = new awisWerkzeuge();
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);

		$this->_EMAIL_Infos = explode(';',$this->_AWISWerkzeug->LeseAWISParameter('zukauf.conf','EMAIL_INFOS'));
		$this->_EMAIL_PreisProblem = explode(';',$this->_AWISWerkzeug->LeseAWISParameter('zukauf.conf','EMAIL_PREISPROBLEME'));
		$this->_EMAIL_Fehler = explode(';',$this->_AWISWerkzeug->LeseAWISParameter('zukauf.conf','EMAIL_FEHLER'));
	}


	/**
	 * Artikelstammimport
	 * @param unknown $LIE_NR
	 * @deprecated
	 */
	public function ImportArtikelStamm($LIE_NR)
	{
        switch($LIE_NR)
        {
            case '8252':
                $this->_ImportWerthenbach();
                break;
        }
	}


	/**
	 * Neuen Debuglevel setzen
	 * @param int $NeuerLevel
	 */
	public function DebugLevel($NeuerLevel=null)
	{
	    if(!is_null($NeuerLevel))
	    {
	        $this->_DebugLevel = $NeuerLevel;
	    }

	    return $this->_DebugLevel;
	}
	
	
	/**
	 * Importiert eine Artikelstammdatei eines Lieferanten
	 */
	public function ImportArtikelstammdatei($Dateiname)
	{
	    $Protokoll = new awisProtokoll($this->_DB, $this->_AWISBenutzer);
	    $Protokoll->Init($this->_DebugLevel,__METHOD__,__CLASS__);
	    
	    if(($fd=fopen($Dateiname,'r'))===false)
        {
            throw new Exception('Kann Datei '.$Dateiname.' nicht oeffnen', 1);   
        }

        $Protokoll->KonsolenAusgabe('Datei '.$Dateiname.' wird gelesen...', 1);
        
        $TrennZeichen = "\t";
        $FeldTrennung = null;
        
        $Zeile = fgetcsv($fd,null,$TrennZeichen);
        if(!isset($Zeile[1]))
        {
            throw new Exception('Dateiformat passt nicht.', 2);   
        }
        
        // Felder aus der Tabelle
        $PflichtFelder = array(
                  'LIEFERANT'
                , 'LAND' 
                , 'HSK' 
                , 'TECDOCID' 
                , 'HERSTELLER' 
                , 'ARTIKELNUMMER' 
                , 'TECDOCARTNR'
                , 'BEZEICHNUNG'
                , 'GHBRUTTOPREISOMWST'
                , 'MWST' 
                , 'VKLISTE'
                , 'EKBASIS' 
                , 'RABATT1' 
                , 'RABATT2'
                , 'RABATT3' 
                , 'EK' 
                , 'WAEHRUNG' 
                , 'MENGENEINHEIT'
                , 'EINHEITPROVK'
                , 'DATUMAB'
                , 'PREISBINDUNGBIS'
                , 'BESTELLNUMMER' 
                , 'WARENGRUPPE1'
                , 'WARENGRUPPE2' 
                , 'WARENGRUPPE3'
                , 'ALTTEILWERT'
                , 'ATUSORTIMENT'
                , 'ATUNR'
                , 'ATUNROHNEPREIS'
                , 'EAN' 
                , 'GEWICHT' 
                , 'SPERRE' 
                , 'SPERRGRUND' 
                , 'PREISEINHEIT' 
        );
        $Felder = array();
        foreach ($PflichtFelder as $Feld){
            $ArraySearch = array_search(mb_strtoupper($Feld), $Zeile);
        	if(!$ArraySearch){
        		$Protokoll->KonsolenAusgabe('Zeile: '.serialize($Zeile), 0, awisProtokoll::AUSGABETYP_FEHLER);
        		throw new Exception('Kann Spalte "'.$Feld.'" nicht finden',3);
        	}else {
        		$Felder[$Feld] =$ArraySearch;
        	}
        }
        
       
	    
        // Fehler Dateien �ffnen
        $fd_err = fopen($Dateiname.'.err','w');
        $fd_ok = fopen($Dateiname.'.ok','w');
        
        //**************************************
        //* Lauf 1: Hersteller suchen
        //**************************************
        
        $LIE_NR = '';
        $Hersteller = array();
        $DS=1;
        $ZLH_Cache = array();
        
        // Stammdaten betimmen
        // Hersteller ermitteln
        while(!feof($fd))
        {
            $Zeile = fgetcsv($fd,null,$TrennZeichen);
            if($Zeile[$Felder['LIEFERANT']]=='')
            {
                continue;
            }
            $DS++;
            if($LIE_NR=='')
            {
                $LIE_NR = $Zeile[$Felder['LIEFERANT']];
                switch($LIE_NR)
                {
                    case '0255':
                        require_once 'awisZukauf_TROST2.inc';
                        $this->_ZukaufObj = new awisZukauf_TROST2($this->_DebugLevel);
                        break;
                    case '8164':
                        require_once 'awisZukauf_WUETSCHNER.inc';
                        $this->_ZukaufObj = new awisZukauf_WUETSCHNER($this->_DebugLevel);
                        break;
                    case '9169':
                        require_once 'awisZukauf_LEITNER.inc';
                        $this->_ZukaufObj = new awisZukauf_LEITNER($this->_DebugLevel);
                        break;
                    case '1742':
                        require_once 'awisZukauf_HESS.inc';
                        $this->_ZukaufObj = new awisZukauf_HESS($this->_DebugLevel);
                        break;
                    case '2620':
                        require_once 'awisZukauf_BIRNER.inc';
                        $this->_ZukaufObj = new awisZukauf_BIRNER($this->_DebugLevel);
                        break;
                    case '9173':
                    	require_once 'awisZukauf_GOEHRUM.inc';
                    	$this->_ZukaufObj = new awisZukauf_GOEHRUM($this->_DebugLevel);
                    	break;
                    default:
                        $Protokoll->KonsolenAusgabe('Lieferant '.$LIE_NR.' noch nicht implementiert.', 0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
                        die();
                        break;
                }
            }    
            
            $HerstellerID = $Zeile[$Felder['TECDOCID']].'~'.$Zeile[$Felder['HSK']];
            $Hersteller[$HerstellerID]=$Zeile[$Felder['HERSTELLER']];    
        }
        
        $Protokoll->KonsolenAusgabe('Es wurden '.count($Hersteller).' Hersteller gefunden', 2);

        $ImportAbbrechen = false;
        
        // Hersteller pr�fen und anlegen
        $this->_DB->TransaktionBegin();
        foreach($Hersteller AS $ID=>$HerstellerName)
        {
            $TecDocID = '';
            $HLK = '';
            
            $HerstellerIDs = explode('~',$ID);
            if($HerstellerIDs[0]!='')           // Ist eine TecDoc Nummer?
            {
                if(!is_numeric($HerstellerIDs[0]))
                {
                    $Protokoll->KonsolenAusgabe('Hersteller-TecDocID '.$HerstellerIDs[0].' ist nicht numerisch.', 0, awisProtokoll::AUSGABETYP_FEHLER);
                    $ImportAbbrechen=true;
                }
                else 
                {
                    $TecDocID=$HerstellerIDs[0];
                }
            }
            elseif(isset($HerstellerIDs[1]) AND $HerstellerIDs[1]!='')
            {
                $HLK = $HerstellerIDs[1];
            }
            else
            {
                $Protokoll->KonsolenAusgabe('Kein Hersteller '.$ID.'->'.$HerstellerName, 0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
                $Protokoll->KonsolenAusgabe('Zeile: '.$Zeile, 0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
                $ImportAbbrechen=true;
            }
            
            if(!$ImportAbbrechen)
            {
                $ZLH_KEY = $this->_ZukaufObj->_PruefeHersteller($HLK, $LIE_NR,$HerstellerName,$TecDocID);
                if($ZLH_KEY>0)
                {
                    $Protokoll->KonsolenAusgabe('Hersteller '.$HerstellerIDs[0].' passt.', 80);
                }
                else // Hersteller bekannt
                {
                    $Protokoll->KonsolenAusgabe('Hersteller '.$HerstellerIDs[0].' konnte nicht angelegt werden.', 0);
                }
            }            
            else 
            {
                $Protokoll->KonsolenAusgabe('Import wird abgebrochen.', 0);
                die();
            }

            
            $ZLH_Cache[$ID]=$ZLH_KEY;
        }
        
        $this->_DB->TransaktionCommit();
        
        //*******************************************************
        //*******************************************************
        // Nun Artikel selber anlegen / aktualisieren
        //*******************************************************
        //*******************************************************
        
        $Protokoll->KonsolenAusgabe('**Runde 2: Artikel einlesen...', 5, awisProtokoll::AUSGABETYP_INFO);
        
        fseek($fd, 0, SEEK_SET);
        $Zeile = fgetcsv($fd,null,$TrennZeichen);           // Kopfzeile lesen
        $DS=0;
        while(!feof($fd))
        {
            $Zeile = fgetcsv($fd,null,$TrennZeichen);
            
            $Daten = array();
            $HLK_KEY = '';

            if($Zeile[$Felder['LIEFERANT']]=='')
            {
                continue;
            }
            //*******************************************************
            // Zun�chst Hauptartikel suchen, ob einer da ist
            //*******************************************************
            // Felder f�r PruefeArtikel
            $Daten['LIENR'] = $Zeile[$Felder['LIEFERANT']];
            $Daten['ARTIKELNUMMER'] = $this->_ZukaufObj->EntferneSonderzeichen($Zeile[$Felder['ARTIKELNUMMER']]);
            $Daten['KATALOGNUMMER'] = $Zeile[$Felder['BESTELLNUMMER']];
            $Daten['ARTIKELBEZEICHNUNG'] = $Zeile[$Felder['BEZEICHNUNG']];
            $Daten['TAUSCHTEILKENNZEICHEN'] = (trim($Zeile[$Felder['ALTTEILWERT']])==''?'0':'1');

            // Felder f�r die Neuanlagen von Artikeln
            // TODO: Pr�fen, ob die auch f�r Updates verwendet werden.
            $Daten['ATU_NR'] = $Zeile[$Felder['ATUNR']];
            $Daten['ATU_SORTIMENT'] = $Zeile[$Felder['ATUSORTIMENT']];
            $Daten['EXTERNEID'] = '::Artikelstammimport::';           // Bleibt leer
            
            $HerstellerID = $Zeile[$Felder['TECDOCID']].'~'.$Zeile[$Felder['HSK']];
            $HLK_KEY = $ZLH_Cache[$HerstellerID];
            
            $HerstellerCode = (isset($Zeile[$Felder['TECDOCID']])?$Zeile[$Felder['TECDOCID']]:$Zeile[$Felder['HSK']]);
            $Protokoll->KonsolenAusgabe('Hersteller '.$HerstellerCode, 5, awisProtokoll::AUSGABETYP_INFO);
            
            $Daten['HERSTELLER_CODE'] = $HerstellerCode;
            
            $ZLA_KEY = $this->_ZukaufObj->_PruefeArtikel($Daten, $HLK_KEY);

            $Protokoll->KonsolenAusgabe('Hauptartikel '.$ZLA_KEY, 90, awisProtokoll::AUSGABETYP_INFO);
            
            //*******************************************************
            // Jetzt einen passenden Lieferantenartikel suchen
            //*******************************************************
            
            $Daten['WANR'] = '';
            
            $Daten['WGR1'] = $Zeile[$Felder['WARENGRUPPE1']];
            $Daten['WGR2'] = $Zeile[$Felder['WARENGRUPPE2']];
            $Daten['WGR3'] = $Zeile[$Felder['WARENGRUPPE3']];
            $Daten['PREISEINHEIT'] = (isset($Zeile[$Felder['PREISEINHEIT']]) AND $Zeile[$Felder['PREISEINHEIT']]!=''?$Zeile[$Felder['PREISEINHEIT']]:1);
            $Daten['MENGENEINHEITENBEZ'] = $Zeile[$Felder['MENGENEINHEIT']];
                
            $ZAL_KEY = $this->_ZukaufObj->_PruefeArtikelLieferantenDaten($Daten, $ZLA_KEY);
            
            $Protokoll->KonsolenAusgabe('Lieferantenartikel '.$ZAL_KEY, 90, awisProtokoll::AUSGABETYP_INFO);
            
            //********************************************************
            // Jetzt den Preis anpassen
            //********************************************************
            $Daten['LAND']=$Zeile[$Felder['LAND']];
            $Daten['VKPREIS']=$this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['GHBRUTTOPREISOMWST']],false);
            $DatumAb = $Zeile[$Felder['DATUMAB']];

            if($Zeile[$Felder['DATUMAB']] == ''){
                $DatumAb = date('d.m.Y');
            }
            $Daten['BESTELLDATUM']=$DatumAb;
            $Daten['RABATT1']=$this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['RABATT1']],false);
            $Daten['RABATT2']=$this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['RABATT2']],false);
            $Daten['RABATT3']=$this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['RABATT3']],false);
            $Daten['EKPREIS']=$this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['EKBASIS']],false);
            $Daten['ALTTEILWERT']=$Zeile[$Felder['ALTTEILWERT']];
            
            
            $Daten['MWST']=$Zeile[$Felder['MWST']];
            switch($Daten['LAND'])
            {
                case 'DE':
                    if($this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['MWST']])==19.0)
                    {
                        $Daten['MWSTID']=self::MWST_ID_STANDARD;
                    }
                    elseif($this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['MWST']])==7.0)
                    {
                        $Daten['MWSTID']=self::MWST_ID_REDUZIERT;
                    }
                    else 
                    {
                        $Daten['MWSTID']=self::MWST_ID_OHNE;
                    }
                    break;
                case 'AT':
                    if($this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['MWST']])==20.0)
                    {
                        $Daten['MWSTID']=self::MWST_ID_AT_STANDARD;
                    }
                    elseif($this->_DB->FeldInhaltFormat('N2',$Zeile[$Felder['MWST']])==10.0)
                    {
                        $Daten['MWSTID']=self::MWST_ID_AT_REDUZIERT;
                    }
                    else 
                    {
                        $Daten['MWSTID']=self::MWST_ID_AT_OHNE;
                    }
                    break;
            }
            
            $Daten['BESTELLMENGE']=1;
            $Daten['ALTTEILBELASTUNGSMENGE']=0;
            $Daten['PREISBINDUNG_BIS']=$Zeile[$Felder['PREISBINDUNGBIS']];
            
            $ZAP_KEY = $this->_ZukaufObj->_ErmittlePreis($Daten, $ZAL_KEY, true);
            
            $Protokoll->KonsolenAusgabe('Preissatz '.$ZAP_KEY, 90, awisProtokoll::AUSGABETYP_INFO);
            
            
            // EAN Nummer speichern
            if($Zeile[$Felder['EAN']]!='')
            {
                if($this->_SpeichereArtikelzusatzId($ZLA_KEY,self::ARTIKELIDTYP_EAN,$Zeile[$Felder['EAN']]))
                {
                    $Protokoll->KonsolenAusgabe('EAN '.$Zeile[$Felder['EAN']].' wurde angelegt.', 10,awisProtokoll::AUSGABETYP_INFO);
                }
            }
            
            // ATU Nummer ohne Preiseinflu�
            if($Zeile[$Felder['ATUNROHNEPREIS']]!='')
            {
                if($this->_SpeichereArtikelzusatzId($ZLA_KEY,self::ARTIKELIDTYP_ATUNROHNE,$Zeile[$Felder['ATUNROHNEPREIS']]))
                {
                    $Protokoll->KonsolenAusgabe('ATUNR '.$Zeile[$Felder['ATUNROHNEPREIS']].' wurde angelegt.', 10,awisProtokoll::AUSGABETYP_INFO);
                }
            }
            
            
            
            // Erfolgreichen DS schreiben
            fputs($fd_ok, implode("\t",$Zeile).PHP_EOL);
        }
	}
	
	
	/**
	 * Artikelstamm vom Werthenbach importieren
	 * @deprecated
	 */
	private function _ImportWerthenbach()
	{
	    // TODO: In Programmparameter umwandeln!
        $Pfad = '/daten/daten/pccommon/Kerres/';
        $Dateiname = 'wb_artikel.csv';
        $Trennzeichen = "\t";
        $Lieferant = '8252';

        $ZLH_Cache=array();

        if(($fp = fopen($Pfad.$Dateiname,'r'))===false)
        {
            throw new Exception('Kann Datei nicht oeffnen', 20111215);
        }

        if($this->_DebugLevel>90)
        {
            echo 'Datei '.$Dateiname.' geoeffnet...'.PHP_EOL;
        }
        $Zeile = trim(fgets($fp),"\n\r");
        // "HERSTELLER";"ARTIKELNUMMER";"BEZEICHNUNG";"UVP";"EK";"MENGEEINHEIT"
        // ;"DATUM";"HERSTELLER_KUERZEL"
        $Felder = array_change_key_case(array_flip(explode($Trennzeichen,trim($Zeile))),CASE_UPPER);

        while(!feof($fp))
        {
            $Zeile = trim(fgets($fp),"\n\r");

            $Daten = explode($Trennzeichen, $Zeile);

            $Daten[$Felder['HERSTELLER']]=$Daten[$Felder['HERSTELLER_KUERZEL']];

            if(!isset($Daten[$Felder['HERSTELLER_KUERZEL']]) OR trim($Daten[$Felder['HERSTELLER_KUERZEL']])=='')
            {
                switch($Lieferant)
                {
                    case '8252':        // Werthenbach
                        $Daten[$Felder['HERSTELLER_KUERZEL']]='+WB+';
                        $Daten[$Felder['HERSTELLER']]='Werthenbach Eigenartikel';
                        break;
                }
            }
            if(array_key_exists($Daten[$Felder['HERSTELLER_KUERZEL']], $ZLH_Cache)===false)
            {
                $ZHK_KEY = '';

                if($this->_DebugLevel>90)
                {
                    echo ' Lade Hersteller '.$Daten[$Felder['HERSTELLER_KUERZEL']].' in den Cache...'.PHP_EOL;
                }

                $SQL = 'SELECT ZLH_KEY, ZHK_KEY';
                $SQL .= ' FROM Zukauflieferantenhersteller';
                $SQL .= ' INNER JOIN Zukaufherstellercodes ON ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = :var_T_zhk_lie_nr';
                $SQL .= ' 									AND ZHK_CODE = :var_T_zhk_code';
                $Bindevariablen=array();
                $Bindevariablen['var_T_zhk_lie_nr'] = $Lieferant;
                $Bindevariablen['var_T_zhk_code'] = $Daten[$Felder['HERSTELLER_KUERZEL']];

                $rsZLH = $this->_DB->RecordSetOeffnen($SQL,$Bindevariablen);
                if($rsZLH->EOF())        // Neuer Hersteller
                {
                    $SQL = 'SELECT ZLH_KEY';
                    $SQL .= ' FROM Zukauflieferantenhersteller';
                    $SQL .= ' WHERE ZLH_KUERZEL = :var_T_zlh_kuerzel';

                    $Bindevariablen=array();
                    $Bindevariablen['var_T_zlh_kuerzel'] = $Daten[$Felder['HERSTELLER_KUERZEL']];

                    $rsZLH = $this->_DB->RecordSetOeffnen($SQL,$Bindevariablen);
                    if($rsZLH->EOF())
                    {
                        if($this->_DebugLevel>90)
                        {
                            echo ' Lege neuen Hersteller an: '.$Daten[$Felder['HERSTELLER_KUERZEL']].'...'.PHP_EOL;
                        }
                        $SQL = 'INSERT INTO ZUKAUFLIEFERANTENHERSTELLER(';
                        $SQL .= 'ZLH_KUERZEL,ZLH_BEZEICHNUNG,ZLH_LIE_NR,ZLH_BEMERKUNG';
                        $SQL .= ',ZLH_USERDAT,ZLH_USER)';
                        $SQL .= 'VALUES (';
                        $SQL .= ' '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['HERSTELLER_KUERZEL']]);
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['HERSTELLER']]);
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Lieferant);
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T','Anlage durch Import Werthenbach');
                        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                        $SQL .= ', SYSDATE)';

                        if($this->_DB->Ausfuehren($SQL)===false)
                        {
                            throw new awisException('Fehler beim Speichern der Hersteller', 201112151426);
                        }

                        $SQL = 'SELECT seq_ZLH_KEY.CurrVal AS KEY FROM DUAL';
                    	$rsKey = $this->_DB->RecordSetOeffnen($SQL);
                    	$ZLH_KEY=$rsKey->FeldInhalt('KEY');

                        if($this->_DebugLevel>50)
                        {
                            echo '  Neuer Hersteller '.$ZLH_KEY.' angelegt...'.PHP_EOL;
                        }
                    }
                    else
                    {
                        $ZLH_KEY = $rsZLH->FeldInhalt('ZLH_KEY');
                    }
                }
                else
                {
                    $ZLH_KEY = $rsZLH->FeldInhalt('ZLH_KEY');
                    $ZHK_KEY = $rsZLH->FeldInhalt('ZHK_KEY');
                }

                // Herstellercode hinterlegen
                if($ZHK_KEY=='')
                {
                    $SQL ='INSERT INTO ZUKAUFHERSTELLERCODES(';
                    $SQL .= 'ZHK_ZLH_KEY,ZHK_LIE_NR,ZHK_CODE,ZHK_BEMERKUNG';
                    $SQL .= ',ZHK_USER,ZHK_USERDAT)';
                    $SQL .= 'VALUES (';
                    $SQL .= ' '.$ZLH_KEY;
                    $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Lieferant);
                    $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['HERSTELLER_KUERZEL']]);
                    $SQL .= ', '.$this->_DB->FeldInhaltFormat('T','Anlage durch Import Werthenbach');
                    $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                    $SQL .= ', SYSDATE)';

                    if($this->_DB->Ausfuehren($SQL)===false)
                    {
                        throw new awisException('Fehler beim Speichern der Hersteller', 201112151426);
                    }

                    if($this->_DebugLevel>50)
                    {
                        echo '    Neuer Herstellercode '.$Daten[$Felder['HERSTELLER_KUERZEL']].' angelegt...'.PHP_EOL;
                        var_dump($SQL);
                    }
                }

                if($this->_DebugLevel>50)
                {
                    echo '    Hersteller-ID '.$ZLH_KEY.' gefunden...'.PHP_EOL;
               }

                // Und in den Cache �bernhemen
                $ZLH_Cache[$Daten[$Felder['HERSTELLER_KUERZEL']]]=$ZLH_KEY;
            }
            else
            {
                $ZLH_KEY = $ZLH_Cache[$Daten[$Felder['HERSTELLER_KUERZEL']]];
            }

            // Artikelnummer muss immmer komprimiert sein!
            $Artikelnummer = $this->_AWISWerkzeug->ArtNrKomprimiert($Daten[$Felder['ARTIKELNUMMER']]);
            $ZAL_KEY = '';

            $SQL = 'SELECT ZLA_KEY, ZAL_KEY';
            $SQL .= ' FROM Zukaufartikel';
            $SQL .= ' LEFT OUTER JOIN Zukaufartikellieferanten ON ZLA_KEY = ZAL_ZLA_KEY';
            $SQL .= ' WHERE ZLA_ARTIKELNUMMER = :var_zla_artikelnummer';
            $SQL .= ' AND ZLA_ZLH_KEY = :var_zla_zlh_key';

            $Bindevariablen=array();
            $Bindevariablen['var_zla_artikelnummer']=$Artikelnummer;
            $Bindevariablen['var_zla_zlh_key']=$ZLH_KEY;
            $rsZLA = $this->_DB->RecordSetOeffnen($SQL,$Bindevariablen);
            if($rsZLA->EOF())
            {
                if($this->_DebugLevel>50)
                {
                    echo '    Lege neuen Hauptartikel an: '.$Artikelnummer.''.PHP_EOL;
                }
                $SQL = 'INSERT INTO ZUKAUFARTIKEL';
                $SQL .= '(ZLA_ARTIKELNUMMER,ZLA_ZLH_KEY,ZLA_BEZEICHNUNG,';
                $SQL .= 'ZLA_MENGENEINHEIT,ZLA_VK,ZLA_GUELTIGAB,ZLA_GUELTIGBIS,';
                $SQL .= 'ZLA_SORTIMENT,ZLA_STATUS,ZLA_USER,ZLA_USERDAT';
                $SQL .= ') VALUES (';
                $SQL .= ' '.$this->_DB->FeldInhaltFormat('T',$Artikelnummer);
                $SQL .= ', '.$ZLH_KEY;
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['BEZEICHNUNG']]);
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['MENGEEINHEIT']]);
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('N2',$Daten[$Felder['UVP']]);
                $SQL .= ', SYSDATE';
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('D','31.12.2030');
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['SORTIMENT']]);
    			$SQL .= ', \'A\'';
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                $SQL .= ', SYSDATE)';

                $this->_DB->Ausfuehren($SQL,'',true);

                $SQL = 'SELECT seq_ZLA_KEY.CurrVal AS KEY FROM DUAL';
            	$rsKey = $this->_DB->RecordSetOeffnen($SQL);
            	$ZLA_KEY=$rsKey->FeldInhalt('KEY');
                if($this->_DebugLevel>50)
                {
                    echo '  Neuer ZLA Artikel '.$ZLA_KEY.' angelegt...'.PHP_EOL;
                }
            }
            else
            {
                $ZLA_KEY = $rsZLA->FeldInhalt('ZLA_KEY');
                $ZAL_KEY = $rsZLA->FeldInhalt('ZAL_KEY');

                // Sortiment eintragen, wenn noch keins drin ist
                $SQL = 'UPDATE ZUKAUFARTIKEL';
                $SQL .= ' SET ';
                $SQL .= '  ZLA_SORTIMENT='.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['SORTIMENT']]);
                $SQL .= ', ZLA_USER = '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                $SQL .= ', ZLA_USERDAT = SYSDATE';
                $SQL .= ' WHERE ZLA_KEY = '.$ZLA_KEY;
                $SQL .= ' AND ZLA_SORTIMENT IS NULL'; 

                $this->_DB->Ausfuehren($SQL,'',true);
            }

            // Neuer Lieferantenartikel
            if($ZAL_KEY=='')
            {
                $SQL = 'INSERT INTO ZUKAUFARTIKELLIEFERANTEN(';
                $SQL .= 'ZAL_ZLA_KEY,ZAL_LIE_NR,ZAL_BEZEICHNUNG,ZAL_WG1,ZAL_WG2,ZAL_WG3';
                $SQL .= ',ZAL_EINHEITENPROVK,ZAL_MENGENEINHEIT,ZAL_BESTELLNUMMER';
                $SQL .= ',ZAL_MWS_ID,ZAL_CREADAT,ZAL_USER,ZAL_USERDAT)';
                $SQL .= 'VALUES (';
                $SQL .= 'var_N0_ZAL_ZLA_KEY,var_T_ZAL_LIE_NR,var_T_ZAL_BEZEICHNUNG,var_T_ZAL_WG1,var_T_ZAL_WG2,var_T_ZAL_WG3';
                $SQL .= ',var_N0_ZAL_EINHEITENPROVK,var_T_ZAL_MENGENEINHEIT,var_T_ZAL_BESTELLNUMMER';
                $SQL .= ',var_N0_ZAL_MWS_ID,var_D_ZAL_CREADAT,var_T_ZAL_USER,var_D_ZAL_USERDAT)';
                $BindeVariablen = array();
                
                $SQL .= ' '.$ZLA_KEY;
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Lieferant);
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['BEZEICHNUNG']]);
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['WGRP']]);
                $SQL .= ', null';
                $SQL .= ', null';
                $SQL .= ', 1';
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['MENGEEINHEIT']]);
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Daten[$Felder['ARTIKELNUMMER']]);
                $SQL .= ', '.self::MWST_ID_STANDARD;
                $SQL .= ', SYSDATE';
                $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
                $SQL .= ', SYSDATE)';

                $this->_DB->Ausfuehren($SQL);

                if($this->_DebugLevel>50)
                {
                    echo '    Neuer ZAL Artikel '.$ZAL_KEY.' angelegt...'.PHP_EOL;
                }
            }
            else
            {
                $SQL = 'UPDATE ZUKAUFARTIKELLIEFERANTEN';
                $SQL .= ' SET ';
                $SQL .= ' ZAL_BEZEICHNUNG =  '.$this->_DB->WertSetzen('ZAL','T',$Daten[$Felder['BEZEICHNUNG']]);
                $SQL .= ',ZAL_WG1 =  '.$this->_DB->WertSetzen('ZAL','T',$Daten[$Felder['WGRP']]);
                $SQL .= ',ZAL_BESTELLNUMMER = '.$this->_DB->WertSetzen('ZAL','T',$Daten[$Felder['ARTIKELNUMMER']]);
                $SQL .= ' WHERE ZAL_KEY = '.$this->_DB->WertSetzen('ZAL','N0',$ZAL_KEY);
                
                $this->_DB->Ausfuehren($SQL);
                
            }
        }
	}
	
	/**
	 * Speichert eine Zusatznummer, z.B. eine EAN
	 * @param unknown $ZLA_KEY     Zukaufartikel
	 * @param unknown $IDTyp       IDTyp nach den Konstanten
	 * @param unknown $ID          ID (z.B. EAN)
	 */
	private function _SpeichereArtikelzusatzId($ZLA_KEY, $IDTyp, $ID)
	{
	    $SQL = 'SELECT ZAI_KEY';
	    $SQL .= ' FROM ZUKAUFLIEFERANTENARTIKELIDS';
	    $SQL .= ' WHERE ZAI_ZLA_KEY = '.$this->_DB->WertSetzen('ZAI', 'N0', $ZLA_KEY);
	    $SQL .= ' AND ZAI_AID_NR = '.$this->_DB->WertSetzen('ZAI', 'N0', $IDTyp);
	    $SQL .= ' AND ZAI_ID = '.$this->_DB->WertSetzen('ZAI', 'T', $ID);
	    $rsZAI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ZAI'));
	    if($rsZAI->EOF())
	    {
    	    $SQL = 'INSERT INTO ZukaufLieferantenArtikelIDs';
    	    $SQL .= '(ZAI_ZLA_KEY,ZAI_AID_NR,ZAI_ID,ZAI_USER,ZAI_USERDAT,ZAI_IMQ_ID)';
    	    $SQL .= 'VALUES(';
    	    $SQL .= ''.$this->_DB->WertSetzen('ZAI', 'N0', $ZLA_KEY);
    	    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'N0', $IDTyp);
    	    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'N0', $ID);
    	    $SQL .= ', '.$this->_DB->FeldInhaltFormat('T','Preisliste');
    	    $SQL .= ', sysdate';
    	    $SQL .= ', 16384';	// Importquelle (ExternerZukauf)
    	    $SQL .= ')';
    	    try {
    	    	$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('ZAI'));
    	    	
    	    } catch (awisException $e) {
    	    	if(strpos($e->getMessage(), 'PK_ZAI_ZLA_KEY')){
    	    		echo 'ID schon vorhanden. Mache weiter...' . PHP_EOL;
    	    	}
    	    }
    	    
    	    return true;
	    }
	    
	    return false;
	}
}
