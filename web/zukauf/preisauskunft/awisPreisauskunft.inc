<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

/**
 * Preisauskunft f�r Zukaufartikel
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @uses awisDatenbank
 * @uses awisFormular
 *
 */
class awisPreisauskunft
{
	/**
	 * Lieferantennummer
	 *
	 * @var string
	 */
	private $_LieferantenNummer = '0000';

	/**
	 * Ermittelter VK Preis (Brutto)
	 *
	 * @var float
	 */
	private $_Preis = 0.0;

	/**
	 * Eigener Bestand
	 *
	 * @var int
	 */
	private $_BestandEigen = 0;

	/**
	 * Best�nde in den Nachbarfilialen
	 *
	 * @var array
	 */
	private $_BestandNachbar = array();
	/**
	 * Datenbank Verbindung
	 *
	 * @var awisDatenbank;
	 */
	private $_DB = null;

	/**
	 * AWISBenutzer (f�r Parameter!)
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 *  Level f�r die Ausgaben
	 *
	 * @var int
	 */
	private $_DebugLevel = 10;
	/**
	 * Fehler, wenn die Filiale ungueltig ist
	 *
	 */
	const ERR_FILIALE = 10001;

	/**
	 * Artikelnummer
	 * 1~Lieferantenartikelnummer|2~OE-Nummer|3~EAN Nummer
	 *
	 */
	const SUCHTYP_LIEFERANTENARTIKELNR = 1;

	/**
	 * OE-Nummer
	 *
	 */
	const SUCHTYP_OENR = 2;

	/**
	 * EAN-Nummer
	 *
	 */
	const SUCHTYP_EANNR = 3;

	/**
	 * Bestellnummer des Lieferanten
	 *
	 */
	const SUCHTYP_BESTELLNR = 4;

	/**
	 * ATU Nummer
	 *
	 */
	const SUCHTYP_ATUNR = 5;
	/**
	 * Initialisierung
	 *
	 */
	public function __construct()
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init();

		$this->_BestandNachbar = array('FIL_ID'=>0,'FIB_BESTAND'=>0);

		ini_set('soap.wsdl_cache_enabled',0);
	}

	/**
	 * Ermittelt die Lieferantennummer f�r eine Preisauskunft
	 *
	 * @return string
	 */
	public function LieferantenNummer()
	{
		return $this->_LieferantenNummer;
	}

	/**
	 * Liefert den ermittelten Preis
	 *
	 * @return float
	 */
	public function Preis()
	{
		return $this->_Preis;
	}

	/**
	 * Liefert den zuletzt bekannten Bestand in der Filiale
	 *
	 * @return int
	 */
	public function BestandEigen()
	{
		return $this->_BestandEigen;
	}


	/**
	 * Liefert den zuletzt bekannten Bestand in der n�chstgelegenen Filiale
	 *
	 * @return array
	 */
	public function BestandNachbar($Feld)
	{
		return $this->_BestandNachbar[$Feld];
	}



	/**
	 * Sucht Artikel nach einem Parameter
	 *
	 * @param string $Suchnummer
	 * @param int $Suchtyp
	 * @param int $Filiale
	 * @param int $Hersteller
	 * @return SimpleXMLElement
	 */
	public function SucheArtikel($Suchnummer, $Suchtyp, $Filiale, $Hersteller, $Menge)
	{
		try
		{
			$Artikeldaten = new SimpleXMLElement('<Artikelanfrage><Anfragezeitpunkt>'.date('c').'</Anfragezeitpunkt></Artikelanfrage>');
			$Parameter = $Artikeldaten->addChild('Parameter');
			$Parameter->addChild('Suchnummer',$Suchnummer);
			$Parameter->addChild('Suchtyp',$Suchtyp);
			$Parameter->addChild('Filiale',$Filiale);
			$Parameter->addChild('Hersteller',$Hersteller);

			//*******************************************************
			// Schritt 1: Parameter pr�fen
			//*******************************************************

			// Schritt 1.1: Filiale
			$SQL = 'SELECT fil_id, fli_lie_nr, lie_name1';
			$SQL .= ', (SELECT LIN_WERT FROM LIEFERANTENINFOS WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY=4) AS bestelltelefon ';
			$SQL .= ', COALESCE((SELECT TO_NUMBER(LIN_WERT) FROM LIEFERANTENINFOS WHERE LIN_LIE_NR = LIE_NR AND LIN_ITY_KEY=7),0) AS artikelanfragen';
			$SQL .= ' FROM Filialen';
			$SQL .= ' INNER JOIN FilialenLieferanten ON FIL_ID = FLI_FIL_ID';
			$SQL .= ' INNER JOIN Lieferanten ON FLI_LIE_NR = LIE_NR';
			$SQL .= ' WHERE TO_NUMBER(FIL_ID) = '.$this->_DB->FeldInhaltFormat('N0',$Filiale);
			$SQL .= ' ORDER BY FLI_SORTIERUNG';

			$rsFIL = $this->_DB->RecordsetOeffnen($SQL);
			if($rsFIL->EOF())
			{
				throw new Exception('Ungueltige Filiale',self::ERR_FILIALE);
			}

			$Lieferanten = $Artikeldaten->addChild('Lieferanten');

			while(!$rsFIL->EOF())
			{
				$Lieferant = $Lieferanten->addChild('Lieferant');
				$Lieferant->addChild('LIE_NR',$rsFIL->FeldInhalt('FLI_LIE_NR'));
				$Lieferant->addChild('Lieferantenname',htmlentities($rsFIL->FeldInhalt('LIE_NAME1')));
				$Lieferant->addChild('Bestelltelefon',htmlentities($rsFIL->FeldInhalt('BESTELLTELEFON')));
				$Lieferant->addChild('Dienst_Artikelanfragen',htmlentities($rsFIL->FeldInhalt('ARTIKELANFRAGEN')));

				$rsFIL->DSWeiter();
			}


			//*******************************************************
			// Schritt 2: Lieferanten w�hlen
			//*******************************************************
			$ArtikelListe = $Artikeldaten->addChild('Artikelliste');

			foreach($Artikeldaten->xpath('/Artikelanfrage/Lieferanten/Lieferant') AS $Lieferant)
			{
				if($this->_DebugLevel >= 5)
				{
					echo 'Suche beim Lieferant: '.$Lieferant->LIE_NR.'<br>';
				}

				if($Lieferant->Dienst_Artikelanfragen==1)
				{
					//*******************************************************
					// Schritt 3: Anfrage beim Lieferanten
					//*******************************************************


					$Params = array('trace'=>1,'soap_version'=>SOAP_1_2
							, 'exceptions'=>1
							, 'encoding'=>'iso-8859-1'
							, 'location'=>'http://svweblx009.server.atu.de/soap/awisSOAP.php'
							);

					$SC = new SoapClient('http://svweblx009.server.atu.de/soap/awisSOAP.php?wsdl',$Params);

					//awisFormular::DebugAusgabe(1,$SC->__getfunctions());

					$AnfrageParameter = array();

					$AnfrageParameter['lie_nr']=(string)$Lieferant->LIE_NR;
					$AnfrageParameter['menge']=$Menge;

					switch ($Suchtyp)
					{
						case self::SUCHTYP_LIEFERANTENARTIKELNR:
							$AnfrageParameter['artikelnummer']=strtoupper($Suchnummer);
							break;
						case self::SUCHTYP_BESTELLNR:
							$AnfrageParameter['bestellnummer']=strtoupper($Suchnummer);
							break;
						case self::SUCHTYP_OENR:
							$AnfrageParameter['oenummer']=strtoupper($Suchnummer);
							break;
						case self::SUCHTYP_EANNR:
							$AnfrageParameter['eannummer']=strtoupper($Suchnummer);
							break;
						case self::SUCHTYP_ATUNR:
							$SQL = 'SELECT ZAI_ZLA_KEY, ZAL_BESTELLNUMMER';
							$SQL .= ' FROM ZUKAUFLIEFERANTENARTIKELIDS';
							$SQL .= ' LEFT OUTER JOIN Zukaufartikellieferanten ON zai_zla_key = zal_zla_key AND ZAL_LIE_NR = '.$this->_DB->FeldInhaltFormat('T',$Lieferant->LIE_NR);
							$SQL .= ' WHERE ZAI_AID_NR = 4 AND ZAI_ID = '.$this->_DB->FeldInhaltFormat('T',$Suchnummer);

							$AnfrageParameter['bestellnummer']=strtoupper($Suchnummer);
							break;
					}

					$Erg = $SC->Zukauf_PreisAnfrage(array('user'=>'awis_jobs','userpwd'=>'dfh358df34jkdf'),$AnfrageParameter);
					$xml = simplexml_load_string($Erg);
echo '<hr>XML<hr>'.htmlentities($xml->asXML()).'<hr>';
					$ArtikelHinzufuegen = True;

					foreach($xml->xpath('/Daten/Artikelliste/ArtikelTreffer') AS $ArtikelTreffer)
					{
						echo '<br>'.$ArtikelTreffer->Artikelnummer;

						$Artikel = null;
						foreach($ArtikelListe AS $Artikel)
						{
							if(strcasecmp($Artikel->Artikelnummer,$ArtikelTreffer->Artikelnummer)===0)
							{
								break;
							}
							$Artikel=null;
						}

						if(is_null($Artikel))
						{
							$Artikel = $ArtikelListe->addChild('Artikel');
							$Artikel->addChild('Artikelnummer',$ArtikelTreffer->Artikelnummer);
							if($Artikel->Zukaufartikel->ZLA_BEZEICHNUNG!='')
							{
								$Artikel->addChild('Artikelbezeichnung',$Artikel->Zukaufartikel->ZLA_BEZEICHNUNG);
							}
							else
							{
								$Artikel->addChild('Artikelbezeichnung',$ArtikelTreffer->Artikelbezeichnung);
							}
							$Artikel->addChild('ATUNr',$ArtikelTreffer->Zukaufartikel->ZLA_AST_ATUNR);
							$Artikel->addChild('HerstellerCode',$ArtikelTreffer->Hersteller->ZLH_KUERZEL);
							$Artikel->addChild('HerstellerName',$ArtikelTreffer->Hersteller->ZLH_BEZEICHNUNG);
							$Artikel->addChild('Sortiment',$ArtikelTreffer->Warengruppe->ZSZ_SORTIMENT);
						}

						$LieferantenInfos = $Artikel->addChild('Lieferant');
						//$Artikel->Lieferant->addChild('LieferantenNr',$xml->Lieferant->lie_nr);
						$LieferantenInfos->addChild('LieferantenNr',$xml->Lieferant->lie_nr);
						$LieferantenInfos->addChild('Liefermenge',$ArtikelTreffer->Verfuegbarkeit->Liefermenge);
						$LieferantenInfos->addChild('NurTelefonisch',$ArtikelTreffer->Verfuegbarkeit->NurTelefonisch);
						$LieferantenInfos->addChild('VKKunde',$ArtikelTreffer->Finanzen->VK);
						$LieferantenInfos->addChild('MwSt',$ArtikelTreffer->Finanzen->MwSt);
						$Text = '';
						foreach($ArtikelTreffer->Verfuegbarkeit->Verkaufshaus AS $Verkaufshaus)
						{
							$Text .= '~'.$Verkaufshaus->Lieferanzeige;
						}
						$LieferantenInfos->addChild('LieferHinweis',substr($Text,1));
					}

					echo '<hr>Preisanfrage<hr>';
					echo htmlentities($xml->asXML());
					echo '<hr>Parameter<hr>';
					var_dump($AnfrageParameter);
					echo '<hr>Artikeldaten<hr>';
					echo htmlentities($Artikeldaten->asXML());
					echo '<hr>';
				}
				elseif($this->_DebugLevel>=2)
				{
					echo '   Bei diesem Lieferanten ist keine Anfrage m�glich!<br>';
				}
			}



			//*******************************************************
			// Schritt 4: Artikelliste auswerten
			//*******************************************************

			//*******************************************************
			// Schritt 5: Zukaufartikel suchen
			//*******************************************************

			//*******************************************************
			// Schritt 6: Preisberechnung durchf�hren
			//*******************************************************

			//*******************************************************
			// Schritt 7: ATU Infos einf�gen
			//*******************************************************



		}
		catch (awisException $ex)
		{
			$Fehler = $Artikeldaten->addChild('Fehler');
			$Fehler->addChild('Fehlermeldung',$ex->getMessage());
			$Fehler->addChild('Fehlercode',$ex->getCode());
			$Fehler->addChild('Fehlerzeile',$ex->getLine());
			$Fehler->addChild('Fehlerdatei',$ex->getFile());
			$Fehler->addChild('SQL',$ex->getSQL());
		}

		return $Artikeldaten;
	}



	public function ErmittlePreis($ZLA_KEY, $Filiale)
	{
		$Gefunden = false;

		$this->_Preis = 0;
		$this->_LieferantenNummer = '';

		$SQL = 'SELECT FLI_LIE_NR, LAN_CODE ';
		$SQL .= ' FROM FILIALENLIEFERANTEN ';
		$SQL .= ' INNER JOIN FILIALEN ON FLI_FIL_ID = FIL_ID ';
		$SQL .= ' INNER JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		$SQL .= ' WHERE FLI_FIL_ID = 0'.$Filiale;
		$rsFLI = $this->_DB->RecordsetOeffnen($SQL);
		$this->_LieferantenNummer = $rsFLI->FeldInhalt('FLI_LIE_NR');
		$Land = $rsFLI->FeldInhalt('LAN_CODE');


		$AIT_ID = '';
		switch ($Land)
		{
			case 'DE':
				break;
			case 'IT':
				$AIT_ID = 53;
				break;
			case 'NL':
				$AIT_ID = 52;
				break;
			case 'AT':
				$AIT_ID = 50;
				break;
			case 'CZ':
				$AIT_ID = 51;
				break;
			case 'CH':
				$AIT_ID = 54;
				break;
		}


		$SQL = 'SELECT AST_ATUNR';
		$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
		if($AIT_ID!='')
		{
			$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID='.$AIT_ID.' AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS AST_VK';
		}
		else
		{
			$SQL .= ',AST_VK';
		}
		$SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
		$SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = \''.$Land.'\' AND ATW_KENNUNG = ASI_WERT';
		$SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
		$SQL .= ' FROM ZukaufArtikel ';
		$SQL .= ' INNER JOIN Artikelstamm ON ZLA_AST_ATUNR = AST_ATUNR';
		$SQL .= ' WHERE ZLA_KEY='.$this->_DB->FeldInhaltFormat('N0',$ZLA_KEY);
		$rsAST = $this->_DB->RecordsetOeffnen($SQL);

		if(!$rsAST->EOF())			// Wir kennen eine ATU Nummer -> diesen Preis verwenden
		{
			$VP = $rsAST->FeldInhalt('VP');
			if($VP=='' OR $VP==0)
			{
				$VP = 1;
			}
/*			$this->_Preis = $this->_DB->FeldInhaltFormat('N2',$rsAST->FeldInhalt('AST_VK'))/$VP;

			if($rsAST->FeldInhalt('ATW_BETRAG')!='')
			{
				$this->_Preis -= awisFeldFormat('N2',$rsAST->FeldInhalt('ATW_BETRAG'));
			}
*/
			//***********************************************************
			// Einkaufspreis aus den Lieferantendaten bilden
			//***********************************************************
			$SQL = 'SELECT ZAP_EK, ZAP_VKBRUTTO, ZAP_EINHEITENPROVK, MWS_SATZ';
			$SQL .= ' FROM ZUKAUFARTIKELLIEFERANTEN ';
			$SQL .= ' INNER JOIN  ZUKAUFARTIKELLIEFERANTENPREISE ON ZAP_ZAL_KEY = ZAL_KEY';
			$SQL .= ' INNER JOIN FILIALENLIEFERANTEN ON FLI_LIE_NR = ZAL_LIE_NR AND FLI_FIL_ID = 0'.$Filiale;
			$SQL .= ' INNER JOIN Mehrwertsteuersaetze ON MWS_ID = ZAP_MWS_ID';
			$SQL .= ' WHERE ZAL_ZLA_KEY = 0'.$this->_DB->FeldInhaltFormat('N0',$ZLA_KEY);
			$SQL .= ' AND ZAP_LAN_CODE = '.$this->_DB->FeldInhaltFormat('T',$Land);
			$SQL .= ' AND ZAP_GUELTIGAB <= SYSDATE';
			$SQL .= ' ORDER BY ZAP_GUELTIGAB DESC';
			$rsZAP = $this->_DB->RecordsetOeffnen($SQL);
			if(!$rsZAP->EOF())
			{
				$VKMin = $rsZAP->FeldInhalt('ZAP_EK')*($this->_DB->FeldInhaltFormat('N2',$this->_AWISBenutzer->ParameterLesen('ZUKAUF_AUFSCHLAG')))*(1+(($this->_DB->FeldInhaltFormat('N2',$rsZAP->FeldInhalt('MWS_SATZ'))/100.0)));
				awisFormular::DebugAusgabe(1, $VKMin);

				if($VKMin>$this->_Preis)
				{
					$this->_Preis = $rsZAP->FeldInhalt('ZAP_VKBRUTTO');
				}
			}

			$MwSt = (1+($this->_DB->FeldInhaltFormat('N4',$rsZAP->FeldInhalt('MWS_SATZ'))/100.0));

			$VKPreisKunde = $this->_DB->FeldInhaltFormat('N2',$rsAST->FeldInhalt('AST_VK'))/$VP;
			// Altteilwert abziehen
			if($rsAST->FeldInhalt('ATW_BETRAG')!='')
			{
				$VKPreisKunde -= $this->_DB->FeldInhaltFormat('N2',$rsAST->FeldInhalt('ATW_BETRAG'));
			}

			// Vergleich, wie der Listenpreis ist
			$UntersterVK = 1.35*$this->_DB->FeldInhaltFormat('N4',$rsZAP->FeldInhalt('ZAP_EK'))*$MwSt;
			// =(VKPR-(VKPR*RABATT/100))*(1+(MWST/100))
			$VKAbschlag = $this->_DB->FeldInhaltFormat('N4',$rsZAP->FeldInhalt('ZAP_VKBRUTTO'))*0.9;

			if($VKPreisKunde<$UntersterVK)
			{
				// WENN(VK_CALC>VK_MIN;VK_CALC;VK_GH)
				if($VKAbschlag > $UntersterVK)
				{
					$VKPreisKunde = $VKAbschlag;
				}
				else
				{
					$VKPreisKunde = $rsZAP->FeldInhalt('ZAP_VKBRUTTO')*$MwSt;
				}
			}
			$this->_Preis = $VKPreisKunde;


			$Gefunden = true;

			//*************************************************
			//Filialbestand ermitteln
			//*************************************************
			$SQL = 'SELECT FIB_BESTAND';
			$SQL .= ' FROM FilialBestand';
			$SQL .= ' WHERE FIB_AST_ATUNR = '.$this->_DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_ATUNR'));
			$SQL .= ' AND FIB_FIL_ID = '.$this->_DB->FeldInhaltFormat('N0',$Filiale);

			$rsFIB = $this->_DB->RecordsetOeffnen($SQL);
			if(!$rsFIB->EOF())
			{
				$this->_BestandEigen = $rsFIB->FeldInhalt('FIB_BESTAND');
			}
			unset($rsFIB);

			//***********************************************
			//
			// Bestand Nachbarfiliale
			//
			//***********************************************
			$SQL = 'SELECT DATEN.* ';
			$SQL .=' FROM(';
			$SQL .= 'SELECT CASE WHEN FEG_FIL_ID_VON = 0'.$Filiale.' THEN FEG_FIL_ID_NACH';
			$SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
			$SQL .= ',FIB_BESTAND';
			$SQL .= ' FROM FILIALENENTFERNUNGEN';
			$SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = '.$Filiale.' THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID';
			$SQL .= ' INNER JOIN FilialebenenZuordnungen ON FIL_ID = FEZ_FIL_ID';
			$SQL .= ' INNER JOIN FilialBestand ON FIB_FIL_ID = FIL_ID AND FIB_AST_ATUNR = '.$this->_DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_ATUNR'));
			$SQL .= ' WHERE (FEG_FIL_ID_VON = 0'.$Filiale;
			$SQL .= ' OR FEG_FIL_ID_NACH = 0'.$Filiale.')';
			$SQL .= ' AND FIB_BESTAND > 0';
			$SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
			$SQL .= ') DATEN ';
			$SQL .= ' WHERE ROWNUM < 3';

			$rsFIB = $this->_DB->RecordsetOeffnen($SQL);
			if(!$rsFIB->EOF())
			{
				$FilID = $rsFIB->FeldInhalt('FEG_FIL_ID');
				$this->_BestandNachbar = array('FIL_ID'=>$FilID, 'FIB_BESTAND'=>$rsFIB->FeldInhalt('FIB_BESTAND'));
			}
			unset($rsFIB);
		}
		else
		{
			//*************************************************************
			//
			// Suche einen Preis f�r den passenden Lieferanten der Filiale
			//
			//*************************************************************
			$SQL = 'SELECT ZAP_VKBRUTTO, ZAL_LIE_NR';
			$SQL .= ' FROM ZUKAUFARTIKELLIEFERANTEN ';
			$SQL .= ' INNER JOIN  ZUKAUFARTIKELLIEFERANTENPREISE ON ZAP_ZAL_KEY = ZAL_KEY';
			$SQL .= ' INNER JOIN FILIALENLIEFERANTEN ON FLI_LIE_NR = ZAL_LIE_NR AND FLI_FIL_ID = 0'.$Filiale;
			$SQL .= ' WHERE ZAL_ZLA_KEY = 0'.$ZLA_KEY;
			$SQL .= ' AND ZAP_LAN_CODE = '.$this->_DB->FeldInhaltFormat('T',$Land);
			$SQL .= ' AND ZAP_GUELTIGAB <= SYSDATE';
			$SQL .= ' ORDER BY ZAP_GUELTIGAB DESC';
			$rsZAP = $this->_DB->RecordsetOeffnen($SQL);
			if($rsZAP->FeldInhalt('ZAP_VKBRUTTO'))
			{
				$this->_Preis = $rsZAP->FeldInhalt('ZAP_VKBRUTTO');
				$this->_LieferantenNummer = $rsZAP->FeldInhalt('ZAL_LIE_NR');

				$Gefunden = true;
			}
		}

		//************************************
		//
		//
		//
		//************************************
		if(!$Gefunden)
		{

		}


		return $Gefunden;
	}


}