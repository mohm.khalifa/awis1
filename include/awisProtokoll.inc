<?php
/**
 * Klasse zum Protokollieren von Fehlern, zum Erstellen von Bildschirmausgaben
 * @author kerres_s
 * @copyright ATU
 */
class awisProtokoll
{
    /**
     * Datenbankschnittstelle
     * @var awisDatenbank
     */
    private $_DB = null;
    
    /**
     * AWIS Benutzer
     * @var awisBenutzer
     */
    private $_AWISBenutzer = null;
    
    /**
     * Aktueller DebigLevel f�r Ausgaben
     * @var int
     */
    private $_DebugLevel = 0;
    /**
     * Programm bzw. Modulname f�r die Ausgabe
     * @var string
     */
    private $_ProgrammName = '';
    /**
     * Name der Klasse, die diese Methode verwendet
     * @var string
     */
    private $_KlassenName = '';

    /**
     * Bekannte Typen f�r die Ausgabe
     * @var array
     */
    private $_TypAusgaben = array(1=>'Info', 2=>'Warnung', 3=>'Fehler', 4=>'Fataler Fehler');

    /**
     * Zwischenspeicher f�r die Ausgabe
     * @var array
     */
    private $_Buffer = array();

    /**
     * Hilfsvariable, um zu Pr�fen, ob Ausgabeart xY aufgetreten ist.
     *
     * @var array
     */
    private $_IstAufgetreten = array();


    const AUSGABETYP_INFO = 1;
    const AUSGABETYP_WARNUNG = 2;
    const AUSGABETYP_FEHLER = 3;
    const AUSGABETYP_FATALERFEHLER = 4;


    /**
     * awisProtokoll constructor.
     * @param awisDatenbank $DB
     * @param awisBenutzer $AWISBenutzer
     */
    public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
    {
        $this->_DB = $DB;
        $this->_AWISBenutzer = $AWISBenutzer;
    }
    
    /**
     * Setzt die Parameter f�r die Protokollierung
     * @param int $DebugLevel
     * @param string $ProgrammName
     * @param string $KlassenName
     */
    public function Init($DebugLevel, $ProgrammName, $KlassenName)
    {
        $this->_DebugLevel = $DebugLevel;
        $this->_ProgrammName = $ProgrammName;
        $this->_KlassenName = $KlassenName;
    }


    /**
     * Returnt den Buffer
     * @return array
     */
    public function HoleBuffer(){
        return $this->_Buffer;
    }

    /**
     * Gibt einen Text auf der Konsole oder einer WebSeite aus. Wenn ein Fehlertyp ausgegeben wird, erfolgt die Ausgabe auf den Fehlerkanal
     * @param string    $Text           Auszugebender Text
     * @param integer   $DebugLevel     Debuglevel, ab dem Ausgaben erfolgen sollen
     * @param integer   $Typ            Typ der Ausgabe (Info, Warnung, ...)
     * @param bool      $InBufferSpeichern gibt an, ob die Ausgabe gebuffert werden soll, um sie sp�ter mit HoleBuffer() zu holen
     * @return boolean
     */
    public function KonsolenAusgabe($Text, $DebugLevel, $Typ=self::AUSGABETYP_INFO, $InBufferSpeichern=false)
    {
        $this->_IstAufgetreten[$Typ] = true;

        if($this->_DebugLevel<$DebugLevel)
        {
            return false;
        }
        
        $Kanal = STDOUT;
        if($Typ==self::AUSGABETYP_FEHLER OR $Typ==self::AUSGABETYP_FATALERFEHLER)
        {
            $Kanal = STDERR;
        }
        
        
        $Ausgabe = date('c').';'.getenv('HOSTNAME').';'.$this->_KlassenName.';'.$this->_ProgrammName.';'.$this->_TypAusgaben[$Typ].';'.$Text;
        if($InBufferSpeichern){
            $this->_Buffer[] = $Ausgabe;
        }
        if(php_sapi_name()=='cli')            // In CLI ausgef�hrt
        {
            fputs($Kanal,$Ausgabe.PHP_EOL);
        }
        else 
        {
            switch($Typ)
            {
                case self::AUSGABETYP_INFO :
                    echo '<span style="">'.$Ausgabe;
                    break;
                case self::AUSGABETYP_WARNUNG :
                    echo '<span style="font-weight:bold">'.$Ausgabe;
                    break;
                case self::AUSGABETYP_FEHLER :
                    echo '<span style="color:red;font-weight:bold">'.$Ausgabe;
                    break;
                case self::AUSGABETYP_FATALERFEHLER :
                    echo '<span style="color:red;background:yellow;font-weight:bold">'.$Ausgabe;
                    break;
            }
            echo '</span><br>';
        }
        
        return true;
    }

    /**
     * Pr�ft, ob Ausgabetyp Xy aufgetreten ist.
     *
     * @param $Typ AUSGABETYP_XY
     * @return bool
     */
    public function IstAufgetreten($Typ){
        if(isset($this->_IstAufgetreten[$Typ]) and $this->_IstAufgetreten[$Typ] == true){
            return true;
        }
        return false;
    }
    
}