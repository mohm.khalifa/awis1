<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('AAT','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT AAT_KEY FROM (SELECT AAT_KEY FROM AdressenAktionenTypen';
	$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON AAT_AZG_KEY = AZG_KEY';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' WHERE (AZM_XBN_Key=0' . awisBenutzerID();
	$SQL .= ')';
	$SQL .= ' AND UPPER(AAT_BEZEICHNUNG) < '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtAAT_BEZEICHNUNG']));
	$SQL .= ' ORDER BY AAT_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsAAT = awisOpenRecordset($con, $SQL);
	if(isset($rsAAT['AAT_KEY'][0]))
	{
		$AWIS_KEY1=$rsAAT['AAT_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtAAT_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT AAT_KEY FROM (SELECT AAT_KEY FROM AdressenAktionenTypen';
	$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON AAT_AZG_KEY = AZG_KEY';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' WHERE (AZM_XBN_Key=0' . awisBenutzerID();
	$SQL .= ')';
	$SQL .= ' AND UPPER(AAT_BEZEICHNUNG) > '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtAAT_BEZEICHNUNG']));
	$SQL .= ' ORDER BY AAT_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsAAT = awisOpenRecordset($con, $SQL);

	if(isset($rsAAT['AAT_KEY'][0]) AND $rsAAT['AAT_KEY'][0]!='')
	{
		$AWIS_KEY1=$rsAAT['AAT_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtAAT_KEY'];
	}
}


if($AWIS_KEY1>0)
{
	$Bedingung .= ' AND AAT_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY1).'';
}
elseif(isset($_GET['AAT_KEY']))
{
	$Bedingung .= ' AND AAT_KEY ='.awis_FeldInhaltFormat('Z',$_GET['AAT_KEY']).'';
}
elseif(isset($_POST['txtAAT_KEY']))
{
	$Bedingung .= ' AND AAT_KEY  ='.awis_FeldInhaltFormat('Z',$_POST['txtAAT_KEY']).'';
}


//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM AdressenAktionenTypen';
$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON AAT_AZG_KEY = AZG_KEY';
$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
$SQL .= ' WHERE (AZM_XBN_Key=0' . awisBenutzerID();
$SQL .= ')';

if($Bedingung!='')
{
	$SQL .= ' ' . $Bedingung;
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY AAT_BEZEICHNUNG';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}
awis_Debug(1,$SQL);
$rsAAT = awisOpenRecordset($con, $SQL);
$rsAATZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if(($rsAATZeilen<>1 AND !isset($_GET['AAT_KEY']))OR isset($_GET['Liste']))						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen&AAT_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=AAT_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AAT_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AAT']['AAT_BEZEICHNUNG'],450,'',$Link);
	awis_FORM_ZeileEnde();

	for($AATZeile=0;$AATZeile<$rsAATZeilen;$AATZeile++)
	{
		awis_FORM_ZeileStart();
		
		$Icons = array();
		if(($Recht3708&6)>0)	// �ndernrecht
		{
			$Icons[] = array('edit','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen&AAT_KEY='.$rsAAT['AAT_KEY'][$AATZeile]);
			$Icons[] = array('delete','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen&Del='.$rsAAT['AAT_KEY'][$AATZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($AATZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('AAT_BEZEICHNUNG',$rsAAT['AAT_BEZEICHNUNG'][$AATZeile],0,450,false,($AATZeile%2));
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmAdressenAktionenTypen action=./adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsAAT['AAT_KEY'][0])?$rsAAT['AAT_KEY'][0]:0);

	echo '<input type=hidden name=txtAAT_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Stammdaten&Seite=Aktionen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAAT['AAT_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAAT['AAT_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$TabellenKuerzel='AAT';
	$EditRecht=(($Recht3708&2)!=0);
	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AAT']['AAT_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('AAT_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsAAT['AAT_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtAAT_BEZEICHNUNG';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AAT']['AAT_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('AAT_BEMERKUNG',($AWIS_KEY1===0?'':$rsAAT['AAT_BEMERKUNG'][0]),350,80,5,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AAT']['AAT_STATUS'].':',150);
	$StatusText = explode("|",$AWISSprachKonserven['AAT']['lst_AAT_STATUS']);
	awis_FORM_Erstelle_SelectFeld('AAT_STATUS',($AWIS_KEY1===0?'':$rsAAT['AAT_STATUS'][0]),130,$EditRecht,$con,'','','A','','',$StatusText,'');
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AAT']['AAT_AZG_KEY'].':',150);
	$SQL = 'SELECT AZG_KEY, KAB_ABTEILUNG || \' - \' || AZG_BEZEICHNUNG AS Bereich';
	$SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' INNER JOIN KONTAKTEABTEILUNGEN ON AZG_KAB_KEY=KAB_KEY';
	$SQL .= ' WHERE AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
	$SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
	$SQL .= ' ORDER BY AZG_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('AAT_AZG_KEY',($AWIS_KEY1===0?'':$rsAAT['AAT_AZG_KEY'][0]),200,true,$con,$SQL);
	awis_FORM_ZeileEnde();
	awis_FORM_FormularEnde();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>