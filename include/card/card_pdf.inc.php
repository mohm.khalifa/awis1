<?php

define("FPDF_FONTPATH","/daten/include/card/fpdf/font/");
require_once("card/fpdf/fpdf.php");

class PDF extends FPDF
{
	//Korrektur des Gesamten PDF-Dokumentes
	var $dblKorrekturX;		
	var $dblKorrekturY;
	
	function PDF($strAusrichtung='P',$strEinheit='mm',$strFormat='A4',
	$dblKorrekturX=0,$dblKorrekturY=0,$dblSeitenrandLinks=10,$dblSeitenrandOben=10,$dblSeitenrandRechts=10,$dblSeitenrandUnten=10)
	{
		if($strAusrichtung=='')
			$strAusrichtung='P';
		if($strEinheit=='')
			$strEinheit='mm';
		if($strFormat=='')
			$strFormat='A4';
		
		FPDF::FPDF($strAusrichtung,$strEinheit,$strFormat);
		
		$this->SetMargins($dblSeitenrandLinks,$dblSeitenrandOben,$dblSeitenrandRechts);
				
		$this->dblKorrekturX=$dblKorrekturX;
		$this->dblKorrekturY=$dblKorrekturY;
	}
	
	//Umrechnung von pt in mm
	
	function ptinmm($dblPunkte)
	{
		return $dblPunkte*2.54/72*10;
	}
	
	//Umrechnung von mm in pt
	
	function mminpt($dblPunkte)
	{
		return $dblPunkte/2.54*72/10;
	}
}
?>