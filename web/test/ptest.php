<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Filesystemhistorie</title>
<link rel=stylesheet type=text/css href=/ATU.css>
</head>

<body>

<?php

include("funktionen.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("admin.inc.php");
//require_once("image.inc.php");
include ("ATU_Header.php");	// Kopfzeile
include( "pc4p_init.inc" );

//$test = array("Wert1" => array(1, 11), "Wert2" => array(22, 2222), "Wert3" => array(3333, 333));
//print_r($test);
//maxSpaltenbreite($test);


$admcon = awisAdminLogon();

if($admcon)
{
	$rname = "atlx06su72";
	$rsStat = create_fshist_stat_array($admcon,$rname);
print_r($rsStat);
print "<hr>";
	$rsbreite = maxSpaltenbreite($rsStat);
	$rscolsize = calcColsize($rsbreite);
print "<hr>";


	$PDF = &pc4p_create_pdf( array( "Author" => "Stefan Pongratz", "Title" => "PC4P-Test" ) );
	$PDF->pc4p_set_compatibility( "1.4" );
	$PDF->pc4p_set_compression( 9 );
	$PDF->pc4p_set_savepdf( "drive", "tabelle.pdf" );

	$Page1 = &pc4p_create_page( $PDF, "a4", "landscape" );
	$Page1->pc4p_set_margin(array("top"=>20, "bottom"=>20, "left"=>30, "right"=>10));
	$Page1->pc4p_set_newpage( 1 );

	$table3 = &pc4p_create_object( $Page1, "table" );
	$table3->pc4p_create_tablematrix( count($rsStat) );
	$table3->pc4p_set_width( 400 );
	$table3->pc4p_set_tableheader( 0 );

	$table3->pc4p_set_tablecolsize( $rscolsize );

	for( $i = 0; $i < count($rsStat["FS_ID"]); $i++) 
	{
		$table3->pc4p_add_tablerow();
			$text1 = &pc4p_create_object(  $table3->cell[ 0 ][ $i ], "text" );
			$text1->pc4p_set_alignment( "right" );
//		$text1->pc4p_set_text( "Ders" );
			$text1->pc4p_set_text( $rsStat["MAXFREI"][$i] );
			$text2 = &pc4p_create_object(  $table3->cell[ 1 ][ $i ], "text" );
			$text2->pc4p_set_alignment( "left" );
			$text2->pc4p_set_text( " a table - ".$i );
	}
	$table3->pc4p_set_tableborder();

	// Now I draw the PDF and (hopefully) get the output
	$PDF->pc4p_draw();

	awisLogoff($admcon);
}
else
{
	print ("Angemeldung fehlgeschlagen!");
} // If($admcon)	

print "Fertsch! ".time()."<br>";

//exit;

/* OCIDefineByPos example thies@thieso.net (980219) */

/*
include("config.inc.php");
include("funktionen.inc.php");

//tabelle("Text");

$test = array("Wert1" => 1, "Wert2" => 2, "Wert3" => 3);
tabelle($test, 5);
echo "<p>";
tabelle("select FG_DATUM Datum, FG_FEIERTAG Feiertag from feiertage");
echo "<p>";

$sql = "select EI_KEY Nr, EI_EIGENSCHAFT Eigenschaft, ET_Bez Typ from eigenschaften, eigenschaftstypen";
$sql = $sql." where ei_et_id = et_id";
tabelle($sql);


exit;

*/


/* the define MUST be done BEFORE ociexecute! */
/*
OCIDefineByName($stmt,"FG_DATUM",&$datum);
OCIDefineByName($stmt,"FG_FEIERTAG",&$ftag);

OCIExecute($stmt);

while (OCIFetch($stmt))
{
        echo "Datum:".$datum." ";
        echo "Feiertag:".$ftag."<br>";
}
*/
?>

<a href=tabelle.pdf target=_self>tabelle.pdf</a>
</BODY>
</HTML>

