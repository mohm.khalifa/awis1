<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once 'sicherheitsanlagen_funktionen.inc';

    global $AWISCursorPosition;
    global $BMV;

    try {
    $BMV = new sicherheitsanlagen_funktionen();
    

    echo "<link rel=stylesheet type=text/css href='" . $BMV->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $BMV->AWISSprachKonserven['TITEL']['tit_Sicherheitsanlagen'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$BMV->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(50000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));


} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>