<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
    $AWISWerkzeuge = new awisWerkzeuge();
    
	//***********************************************************************************
	//** Bestellungen �ndern / anlegen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZUB_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('ZUB','ZUB_%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtZUB_KEY'];

		$rsZUB = $DB->RecordSetOeffnen('SELECT * FROM Zukaufbestellungen WHERE ZUB_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
		if(!$rsZUB->EOF())
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';


			if($_POST['txtZUB_WANRKORR']!='')
			{
				if(!$AWISWerkzeuge->BerechnePruefziffer($_POST['txtZUB_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_PruefzifferFalsch'].': '.$TXT_Speichern['ZUB']['ZUB_WANRKORR'].'<br>';
				}
			}
			if($Fehler!='')
			{
				$Form->Hinweistext($Fehler,1);
			}
            else 
            {
    			$FeldListe = '';
    			foreach($Felder AS $Feld)
    			{
    				$FeldName = substr($Feld,3);
    				if(isset($_POST['old'.$FeldName]))
    				{
    					// Alten und neuen Wert umformatieren!!
    					$WertNeu=$DB->FeldInhaltFormat($rsZUB->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
    					$WertAlt=$DB->FeldInhaltFormat($rsZUB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
    					$WertDB=$DB->FeldInhaltFormat($rsZUB->FeldInfo($FeldName,'TypKZ'),$rsZUB->FeldInhalt($FeldName),true);
    			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
    					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
    					{
    						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
    						{
    							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
    						}
    						else
    						{
    							$FeldListe .= ', '.$FeldName.'=';
    
    							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
    							{
    								$FeldListe.=' null';
    							}
    							else
    							{
    								$FeldListe.=$WertNeu;
    							}
    						}
    					}
    				}
    			}
    
    			if(count($FehlerListe)>0)
    			{
    				$Meldung = str_replace('%1',$rsZUB->FeldInhalt('ZUB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
    				foreach($FehlerListe AS $Fehler)
    				{
    					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
    					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
    				}
    				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
    			}
    			elseif($FeldListe!='')
    			{
    				$SQL = 'UPDATE zukaufbestellungen SET';
    				$SQL .= substr($FeldListe,1);
    				$SQL .= ', ZUB_user=\''.$AWISBenutzer->BenutzerName().'\'';
    				$SQL .= ', ZUB_userdat=sysdate';
    				$SQL .= ' WHERE ZUB_key=0' . $_POST['txtZUB_KEY'] . '';
    
    				if($DB->Ausfuehren($SQL)===false)
    				{
    					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
    				}

    				// E-Mail zur Info
    				if($_POST['txtZUB_WANRKORR']!='')
    				{
    				    include 'awisMailer.inc';
    				    $Mail = new awisMailer($DB, $AWISBenutzer);
    				    $Mail->Absender($AWISBenutzer->EMailAdresse());
    				    $Mail->Betreff('Umbuchung Zukaufnummer');
    				    $Mail->Text('Bestellung wurde von '.$rsZUB->FeldInhalt('ZUB_WANR').' auf '.$_POST['txtZUB_WANRKORR'].' ge&auml;ndert',awisMailer::FORMAT_HTML,true);
    				    $Mail->AdressListe(awisMailer::TYP_TO,'zukauf@de.atu.eu');
    				    $Mail->SetzeBezug('zub', $_POST['txtZUB_KEY']);
    				    $Mail->MailInWarteschlange($AWISBenutzer->BenutzerName());
    				}
    			}
   			}
		}
	}// Zukaufbestellung
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>