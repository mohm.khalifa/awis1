<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $COMWebSrv;
require_once("operating_COM_config.php");

global $con;
global $comcon;
global $dwh37con;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Allgemein');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Mail_SB');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Allgemein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Allgemein'].'</title>';
?>
</head>
<body>
<?php

$Recht3111 = awisBenutzerRecht($con,3111);
if($Recht3111==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$MNU_ID='';

if (isset($_REQUEST['MNU_ID']))
{
	$MNU_ID=$_REQUEST['MNU_ID'];
}

$MNU_ITEM_ID='';

if (isset($_REQUEST['MNU_ITEM_ID']))
{
	$MNU_ITEM_ID=$_REQUEST['MNU_ITEM_ID'];
}

$JOBID='';

if (isset($_REQUEST['JOBID']))
{
	$JOBID=$_REQUEST['JOBID'];
}

$JOBNAME='';

if (isset($_REQUEST['JOBNAME']))
{
	$JOBID=$_REQUEST['JOBNAME'];
}

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Allgemein action=./operating_COM_Main.php?cmdAktion=Allgemein method=POST>';

echo "<p><center><h2><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Allgemein']."</b></h2></center><p>";

If(($Recht3111&1)==1){
	$MNU_LINK="<li><a href=./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=1>ATU => Easycash</a></li><p>";
	echo $MNU_LINK;
}

if ($MNU_ID=='1')
{
	If(($Recht3111&2)==2){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=1";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">Transaktions-&Uuml;bersicht</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='1')
	{	
		$SQL="SELECT * FROM EXPGW_EXPORT_JOBTYP_LAST";
		if(!isset($_GET['Sort']))
		{
			$SQL .= ' ORDER BY JOBTYPE_ID';
			$SORT_LINK='';
		}
		else
		{
			$SORT_LINK='&Sort='.$_GET['Sort'];
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		$rsEJL=awisOpenRecordset($comcon,$SQL);
		$rsEJLZeilen=$awisRSZeilen;

		if ($rsEJLZeilen>0)
		{
		
		awis_FORM_FormularStart();
		
		$SQL="SELECT 'Nächster Freigabe-Lauf - '||(case when job_name like '%WAD%' then 'A4/DBM' else (case when job_name like '%POS%' then 'A5/POS' ELSE 'Allgemein' END)END)||': '|| ";
		$SQL.="TO_CHAR(NEXT_RUN_DATE,'DD.MM.YYYY HH24:MI:SS') AS FREIGABE_NEXT_RUN ";
		$SQL.="FROM (SELECT job_name, NEXT_RUN_DATE, ROW_NUMBER() OVER (partition by job_name ORDER BY NEXT_RUN_DATE) AS RNUM ";
		$SQL.="FROM SYS.ALL_SCHEDULER_JOBS ";
		$SQL.="WHERE enabled='TRUE' and (SCHEDULE_NAME LIKE '%ATU2EXP%' ";
		$SQL.="or regexp_like (job_name,'*FREIGABE_[PW][OA][SW]*'))) WHERE RNUM=1 ";
		$SQL.="order by NEXT_RUN_DATE";
		
		$rsFNR=awisOpenRecordset($comcon,$SQL);
		$rsFNRZeilen=$awisRSZeilen;
		
		if($rsFNRZeilen>0)
		{
			for($FNRZeile=0;$FNRZeile<$rsFNRZeilen;$FNRZeile++)
			{
				awis_FORM_ZeileStart();
					awis_FORM_Erstelle_TextFeld('FREIGABE_NEXT_RUN',$rsFNR['FREIGABE_NEXT_RUN'][$FNRZeile],'',500,'','','','','T','L');
				awis_FORM_ZeileEnde();
			}
			awis_FORM_Trennzeile('O');
		}
		
		if ($JOBID==''){
			$JOB_LINK='';
		}
		else {
			$JOB_LINK="&JOBID=".$JOBID;
		}
		
		awis_FORM_ZeileStart();
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],150,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=DESCRIPTION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='DESCRIPTION'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBBEZEICHNUNG'],300,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=TSCREATE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TSCREATE'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_CREATE'],200,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=TSTRANSFER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TSTRANSFER'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_TRANSFER'],200,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=ARBEITSSTATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ARBEITSSTATUS'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS'],50,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEZEICHNUNG'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS_BEZEICHNUNG'],200,'',$Link);
		awis_FORM_ZeileEnde();
		
		awisExecute($comcon,'ALTER SESSION DISABLE PARALLEL QUERY');
		
			for($EJLZeile=0;$EJLZeile<$rsEJLZeilen;$EJLZeile++)
			{
				$Link=$MNU_ITEM_LINK."&JOBID=".urlencode($rsEJL['JOBTYPE_ID'][$EJLZeile]).$SORT_LINK;
				
				if ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='E'){
					$Hintergrund='background-color: FF0000';
				}
				elseif ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='L'){
					$Hintergrund='background-color: FFFF00';	
				}
				elseif ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='T'){
					$Hintergrund='background-color: 00FF00';
				}
				elseif ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='M'){
					$Hintergrund='background-color: FF6921';
				}
				elseif ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='X'){
					$Hintergrund='background-color: D1EFEE';
				}
				elseif ($rsEJL['ARBEITSSTATUS'][$EJLZeile]=='V'){
					$Hintergrund='background-color: FF3F47';
				}
				else{
					$Hintergrund='';
				}
				
				$BindeVariablen=array();
				$BindeVariablen['var_N0_jobtype_id']=$rsEJL['JOBTYPE_ID'][$EJLZeile];
				
				$SQL="SELECT /*+ NO_PARALLEL(EXPGW_EXPORT_JOBTYP_LAST5) */ COUNT(*) AS CNT, JOBTYPE_ID FROM EXPGW_EXPORT_JOBTYP_LAST5 WHERE JOBTYPE_ID=:var_N0_jobtype_id AND (ARBEITSSTATUS NOT like 'T' and ARBEITSSTATUS NOT like 'C') GROUP BY JOBTYPE_ID";
				$rsEJLDetailStatCNT=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
				
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsEJL['TRANSAKTIONSID'][$EJLZeile],0,150,false,($EJLZeile%2),$Hintergrund,$Link,'Z');
				awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsEJL['JOBTYPE_ID'][$EJLZeile],0,50,false,($EJLZeile%2),$Hintergrund,$Link,'Z');
				awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsEJL['DESCRIPTION'][$EJLZeile],0,300,false,($EJLZeile%2),$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('TSCREATE',$rsEJL['TSCREATE'][$EJLZeile],0,200,false,($EJLZeile%2),$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('TSTRANSFER',$rsEJL['TSTRANSFER'][$EJLZeile],0,200,false,($EJLZeile%2),$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('ARBEITSSTATUS',$rsEJL['ARBEITSSTATUS'][$EJLZeile],0,50,false,($EJLZeile%2),$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('BEZEICHNUNG',$rsEJL['BEZEICHNUNG'][$EJLZeile],0,200,false,($EJLZeile%2),$Hintergrund,$Link,'T');				
				if ((isset($rsEJLDetailStatCNT['CNT'][0]) && $rsEJLDetailStatCNT['CNT'][0]!=0) && (isset($rsEJLDetailStatCNT['JOBTYPE_ID'][0]) && $rsEJLDetailStatCNT['JOBTYPE_ID'][0]==$rsEJL['JOBTYPE_ID'][$EJLZeile]))
				{
					$Icons=array();
					$Icons[]=array('zoom',$MNU_ITEM_LINK."&JOBID=".urlencode($rsEJL['JOBTYPE_ID'][$EJLZeile]));
					awis_FORM_Erstelle_ListeIcons($Icons,10,($EJLZeile%2));
				}	
				awis_FORM_ZeileEnde();
				
				
				if ($JOBID==$rsEJL['JOBTYPE_ID'][$EJLZeile])
				{	
					awis_FORM_Trennzeile();
						
					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_jobtype_id']=$JOBID;
					
					$SQL="SELECT /*+ NO_PARALLEL(EXPGW_EXPORT_JOBTYP_LAST5) */ * FROM EXPGW_EXPORT_JOBTYP_LAST5 WHERE JOBTYPE_ID=:var_N0_jobtype_id";
					if(!isset($_GET['SortSub']))
					{
						$SQL .= ' ORDER BY TRANSAKTIONSID DESC';
					}
					else
					{
						//awis_Debug(1,str_replace('~',' DESC ',$_GET['SortSub']));
						$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['SortSub']);
					}
					
					$rsEJLDetail=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
					$rsEJLDetailZeilen=$awisRSZeilen;
					
					awis_FORM_ZeileStart();
						$Link = $MNU_ITEM_LINK.$JOB_LINK.$SORT_LINK."&SortSub=TRANSAKTIONSID".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='TRANSAKTIONSID'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],110,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=JOBTYPE_ID".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='JOBTYPE_ID'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=DESCRIPTION".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='DESCRIPTION'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBBEZEICHNUNG'],250,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=TSCREATE".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='TSCREATE'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_CREATE'],150,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=TSTRANSFER".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='TSTRANSFER'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_TRANSFER'],150,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=ARBEITSSTATUS".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='ARBEITSSTATUS'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS'],50,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=BEZEICHNUNG".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='BEZEICHNUNG'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ARBEITSSTATUS_BEZEICHNUNG'],180,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=ERWARTET".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='ERWARTET'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ERWARTET'],60,'font-size: 75%;',$Link,$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_ERWARTET']);
//						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=ABGEHOLT".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='ABGEHOLT'))?'~':'');
//					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_ABGEHOLT'],60,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=VERARBEITET".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='VERARBEITET'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITET'],70,'font-size: 75%;',$Link,$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_VERARBEITET']);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=VERARBEITUNG_PROZ".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='VERARBEITUNG_PROZ'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_VERARBEITUNG_PROZ'],80,'font-size: 75%;',$Link,$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_VERARBEITUNG_PROZ']);
					awis_FORM_ZeileEnde();
					
					$boldText=0;
					
					for($EJLDetailZeile=0;$EJLDetailZeile<$rsEJLDetailZeilen;$EJLDetailZeile++)
					{
						if ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='E'){
							$Hintergrund='background-color: FF0000';
						}
						elseif ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='L'){
							$Hintergrund='background-color: FFFF00';	
						}
						elseif ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='T'){
							$Hintergrund='background-color: 00FF00';
						}
						elseif ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='M'){
							$Hintergrund='background-color: FF6921';
						}
						elseif ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='X'){
							$Hintergrund='background-color: D1EFEE';
						}
						elseif ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]=='V'){
							$Hintergrund='background-color: FF3F47';
						}		
						else{
							$Hintergrund='';
						}
						
						if ($boldText===0 && ($rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]==='M' || $rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile]==='T'))
						{
							$boldText=1;
							$styleList='font-weight: bold;font-size: 75%;'.$Hintergrund;
						}
						else 
						{
							$styleList='font-size: 75%;'.$Hintergrund;
						}
						
						
						$LinkTF="\"#\" onclick=\"window.open('./operating_COM_Details_Allgemein_TID_Files.php?TID=".$rsEJLDetail['TRANSAKTIONSID'][$EJLDetailZeile]."','','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');\""; 					
						
						awis_FORM_ZeileStart();
						awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsEJLDetail['TRANSAKTIONSID'][$EJLDetailZeile],0,110,false,($EJLDetailZeile%2),$styleList,$LinkTF,'Z');
						awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsEJLDetail['JOBTYPE_ID'][$EJLDetailZeile],0,50,false,($EJLDetailZeile%2),$styleList,'','Z');
						awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsEJLDetail['DESCRIPTION'][$EJLDetailZeile],0,250,false,($EJLDetailZeile%2),$styleList,'','T');
						awis_FORM_Erstelle_ListenFeld('TSCREATE',$rsEJLDetail['TSCREATE'][$EJLDetailZeile],0,150,false,($EJLDetailZeile%2),$styleList,'','DU');
						awis_FORM_Erstelle_ListenFeld('TSTRANSFER',$rsEJLDetail['TSTRANSFER'][$EJLDetailZeile],0,150,false,($EJLDetailZeile%2),$styleList,'','DU');
						awis_FORM_Erstelle_ListenFeld('ARBEITSSTATUS',$rsEJLDetail['ARBEITSSTATUS'][$EJLDetailZeile],0,50,false,($EJLDetailZeile%2),$styleList,'','T');
						awis_FORM_Erstelle_ListenFeld('BEZEICHNUNG',$rsEJLDetail['BEZEICHNUNG'][$EJLDetailZeile],0,180,false,($EJLDetailZeile%2),$styleList,'','T');
						awis_FORM_Erstelle_ListenFeld('ERWARTET',$rsEJLDetail['ERWARTET'][$EJLDetailZeile],0,60,false,($EJLDetailZeile%2),$styleList,'','Z','',$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_ERWARTET']);
						//awis_FORM_Erstelle_ListenFeld('ABGEHOLT',$rsEJLDetail['ABGEHOLT'][$EJLDetailZeile],0,60,false,($EJLDetailZeile%2),$styleList,'','Z');
						awis_FORM_Erstelle_ListenFeld('VERARBEITET',$rsEJLDetail['VERARBEITET'][$EJLDetailZeile],0,70,false,($EJLDetailZeile%2),$styleList,'','Z','',$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_VERARBEITET']);
						awis_FORM_Erstelle_ListenFeld('VERARBEITUNG_PROZ',$rsEJLDetail['VERARBEITUNG_PROZ'][$EJLDetailZeile],0,80,false,($EJLDetailZeile%2),$styleList,'','Z','',$AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TT_VERARBEITUNG_PROZ']);
						awis_FORM_ZeileEnde();
					}
					awis_FORM_Trennzeile();			
				}
				
			}
		
		awisExecute($comcon,'ALTER SESSION ENABLE PARALLEL QUERY');
										
		awis_FORM_FormularEnde();
		}
	}
	
	If(($Recht3111&4)==4){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=2";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">Scheduler-&Uuml;bersicht</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='2')
	{
		//$SQL="SELECT * FROM ATU2EXP_SCHEDULER WHERE ENABLED='TRUE'";
		
		$Recht3117=awisBenutzerRecht($con,3117);
		
		
		if(($Recht3117&8)==8)
		{
			$SQL="SELECT * FROM ATU2EXP_SCHEDULER ";			
		}
		else {
			$SQL="SELECT * FROM ATU2EXP_SCHEDULER ";
			$SQL .="WHERE (OWNER, JOB_NAME) IN (SELECT JOB_OWNER, JOB_NAME FROM AWIS_SCHEDULER_OPERATING WHERE BITAND(AWIS_RECHT_3117_BIT,".$Recht3117.")>0) ";
		}
		
		if(!isset($_GET['Sort']))
		{
			$SORT_LINK='';
			$SQL .= ' ORDER BY NEXT_START_UTS, OWNER, JOB_NAME';
		}
		else
		{
			$SORT_LINK='&Sort='.$_GET['Sort'];
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		$rsAES=awisOpenRecordset($comcon,$SQL);
		
		$rsAESZeilen=$awisRSZeilen;

//		awis_Debug(1,$SQL);
//		awis_Debug(1,$rsAES);
		
		if ($rsAESZeilen>0)
		{
			if ($JOBNAME==''){
				$JOB_LINK='';
			}
			else {
				$JOB_LINK="&JOBNAME=".$JOBNAME;
			}
				
			
			awis_FORM_FormularStart();
		
			awis_FORM_ZeileStart();
			If(($Recht3111&16384)==16384){
				awis_FORM_Erstelle_Liste_Ueberschrift('EXEC',40,'font-size: 85%; font_weight:bold;');
			}
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=OWNER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='OWNER'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_OWNER'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=JOB_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOB_NAME'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBNAME'],280,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=STATE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='STATE'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_STATE'],90,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=NEXT_START_UTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='NEXT_START_UTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_NEXT_START'],150,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_START_UTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_START_UTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LAST_START'],150,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_OPERATION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_OPERATION'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LAST_OPERATION'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_STATUS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LAST_STATUS'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=COMMENTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='COMMENTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_COMMENT'],150,'font-size: 85%;',$Link);
			awis_FORM_ZeileEnde();
			
			for($AESZeile=0;$AESZeile<$rsAESZeilen;$AESZeile++)
			{
				$JOBNAME=urlencode($rsAES['OWNER'][$AESZeile]).'.'.urlencode($rsAES['JOB_NAME'][$AESZeile]);
				$Link=$MNU_ITEM_LINK."&JOBNAME=".$JOBNAME.$SORT_LINK.'#EXEC_'.$JOBNAME;
				
				$Hintergrund='';
				
				if ($rsAES['STATE'][$AESZeile]=='RUNNING')
				{
					$Hintergrund='background-color: FFFF00';
				}elseif ($rsAES['LAST_STATUS'][$AESZeile]=='FAILED')
				{						
					$Hintergrund='background-color: FF0000';
				}elseif ($rsAES['LAST_STATUS'][$AESZeile]=='SUCCEEDED' AND $rsAES['STATE'][$AESZeile]!='DISABLED')
				{
					$Hintergrund='background-color: 00FF00';
				}else{
					if($rsAES['LAST_STATUS'][$AESZeile]=='SUCCEEDED' AND $rsAES['RUN_TODAY'][$AESZeile]==1)
					{
						$Hintergrund='background-color: 00FF00';
					}else{
						$Hintergrund='';
					}
				}
				
				awis_FORM_ZeileStart('margin: 0px 0px 5px 0px;');
				If(($Recht3111&16384)==16384){
					//awis_FORM_Erstelle_ListenBild('href','EXEC_'.$JOBNAME,'./operating_COM_Details_Allgemein_RunScheduler.php?JOBNAME='.$JOBNAME.'" target="_blank','/bilder/icon_go.png','',($AESZeile%2),'','16','16',40,'C'); 
					$LinkRS="onclick=\"window.open('./operating_COM_Details_Allgemein_RunScheduler.php?JOBNAME=".$JOBNAME."','','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');"; 					
					awis_FORM_Erstelle_ListenBild('script','EXEC_'.$JOBNAME,'','/bilder/icon_go.png" '.$LinkRS,'COM - Scheduler-Job',($AESZeile%2),'','16','16',40,'C'); 					
				}					
				awis_FORM_Erstelle_ListenFeld('OWNER',$rsAES['OWNER'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('JOB_NAME',$rsAES['JOB_NAME'][$AESZeile],0,280,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('STATE',$rsAES['STATE'][$AESZeile],0,90,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('NEXT_START',$rsAES['NEXT_START'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('LAST_START',$rsAES['LAST_START'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('LAST_OPERATION',$rsAES['LAST_OPERATION'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('LAST_STATUS',$rsAES['LAST_STATUS'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('COMMENTS',$rsAES['COMMENTS'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 70%;'.$Hintergrund,$Link,'T');
				awis_FORM_ZeileEnde();
				
				if (isset($_GET['JOBNAME']) && $_GET['JOBNAME']===$JOBNAME)
				{
					$SCHEDJOB=explode('.',$JOBNAME);
					
					$BindeVariablen=array();
					$BindeVariablen['var_T_owner']=$SCHEDJOB[0];
					$BindeVariablen['var_T_job_name']=$SCHEDJOB[1];
					
					$SQL = 'SELECT * FROM ( ';
					$SQL .='SELECT AA.LOG_ID, AA.LOG_DATE, AA.JOB_CLASS, AA.OPERATION, AA.STATUS, ';
					$SQL .='AA.USER_NAME, AA.ADDITIONAL_INFO AS LOG_ADDITIONAL_INFO, BB.REQ_START_DATE, BB.ACTUAL_START_DATE, BB.RUN_DURATION, ';
					$SQL .='BB.INSTANCE_ID AS INST_ID, BB.SESSION_ID, BB.SLAVE_PID, BB.CPU_USED, BB.ERROR#, BB.ADDITIONAL_INFO AS RUN_ADDITIONAL_INFO, ';
					$SQL .='ROW_NUMBER() OVER (ORDER BY AA.LOG_ID DESC) AS RNUM ';
					$SQL .='FROM SYS.ALL_SCHEDULER_JOB_LOG AA, SYS.ALL_SCHEDULER_JOB_RUN_DETAILS BB ';
					$SQL .='WHERE AA.OWNER=:var_T_owner AND AA.JOB_NAME=:var_T_job_name ';
					$SQL .='AND AA.LOG_ID=BB.LOG_ID(+)) ';
					$SQL .='WHERE RNUM<11';
					
					$rsSHL=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
					$rsSHLZeilen=$awisRSZeilen;
					
					if ($rsSHLZeilen>0)
					{
						
						$style='font-size: 70%';
						
						for($SHLZeile=0;$SHLZeile<$rsSHLZeilen;$SHLZeile++)
						{
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_ID',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_DATE',200,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('USER_NAME',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('INST_ID',50,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('SID,SERIAL#',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('SLAVE_PID',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('JOB_CLASS',200,$style);							 	
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_ID'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_DATE'][$SHLZeile],'',200,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['USER_NAME'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['INST_ID'][$SHLZeile],'',50,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['SESSION_ID'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['SLAVE_PID'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['JOB_CLASS'][$SHLZeile],'',200,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('OPERATION',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('STATUS',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('REQ_START_DATE',300,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('ACTUAL_START_DATE',300,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('RUN_DURATION',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('CPU_USED',120,$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['OPERATION'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['STATUS'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['REQ_START_DATE'][$SHLZeile],'',300,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['ACTUAL_START_DATE'][$SHLZeile],'',300,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['RUN_DURATION'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['CPU_USED'][$SHLZeile],'',120,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('ERROR#',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_ADDITIONAL_INFO',400,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('RUN_ADDITIONAL_INFO',400,$style);							 	
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['ERROR#'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_ADDITIONAL_INFO'][$SHLZeile],'',400,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['RUN_ADDITIONAL_INFO'][$SHLZeile],'',400,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_Trennzeile('D');
							
						}
						
					}
				
				}
							
			}
			
			awis_FORM_FormularEnde();
		}
		else {
			awis_FORM_Hinweistext('Sieh haben keine Rechte um diese Aktion auszuführen');
		}
	}

	If(($Recht3111&512)==512){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=3";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">Freigabe-&Uuml;bersicht</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='3')
	{	
		$SQL="SELECT DISTINCT JOBTYP, BEZEICHNUNG FROM EXPERIAN_ATU.V_EXPERIAN_FREIGABE_LAST_14";
		if(!isset($_GET['Sort']))
		{
			$SQL .= ' ORDER BY JOBTYP';
			$SORT_LINK='';
		}
		else
		{
			$SORT_LINK='&Sort='.$_GET['Sort'];
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		$rsEFL=awisOpenRecordset($comcon,$SQL);
		$rsEFLZeilen=$awisRSZeilen;

//		awis_Debug(1,$SQL);
		
		if ($rsEFLZeilen>0)
		{
		
		awis_FORM_FormularStart();
		
		if ($JOBID==''){
			$JOB_LINK='';
		}
		else {
			$JOB_LINK="&JOBID=".$JOBID;
		}
		
		awis_FORM_ZeileStart();
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=JOBTYP'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYP'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,'',$Link);
			$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEZEICHNUNG'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBBEZEICHNUNG'],300,'',$Link);
		awis_FORM_ZeileEnde();
		
			for($EFLZeile=0;$EFLZeile<$rsEFLZeilen;$EFLZeile++)
			{
				$Link=$MNU_ITEM_LINK."&JOBID=".urlencode($rsEFL['JOBTYP'][$EFLZeile]).$SORT_LINK;
				
				$Hintergrund='';
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('JOBTYP',$rsEFL['JOBTYP'][$EFLZeile],0,50,false,($EFLZeile%2),$Hintergrund,$Link,'Z');
				awis_FORM_Erstelle_ListenFeld('BEZEICHNUNG',$rsEFL['BEZEICHNUNG'][$EFLZeile],0,300,false,($EFLZeile%2),$Hintergrund,$Link,'T');
				awis_FORM_ZeileEnde();
				
				
				if ($JOBID==$rsEFL['JOBTYP'][$EFLZeile])
				{	
					awis_FORM_Trennzeile();
						
					$BindeVariablen=array();
					$BindeVariablen['var_N0_jobtype_id']=$JOBID;
					
					$SQL="SELECT * FROM EXPERIAN_ATU.V_EXPERIAN_FREIGABE_LAST_14 WHERE JOBTYP=:var_N0_jobtype_id";
					if(!isset($_GET['SortSub']))
					{
						$SQL .= ' ORDER BY TRANSAKTIONSID DESC';
					}
					else
					{
						//awis_Debug(1,str_replace('~',' DESC ',$_GET['SortSub']));
						$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['SortSub']);
					}
					
//					awis_Debug(1,$SQL);
					
					$rsEFLDetail=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
					$rsEFLDetailZeilen=$awisRSZeilen;
					
					awis_FORM_ZeileStart();
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=JOBTYP".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='JOBTYP'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],50,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=BEZEICHNUNG".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='BEZEICHNUNG'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBBEZEICHNUNG'],250,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK.$JOB_LINK.$SORT_LINK."&SortSub=TRANSAKTIONSID".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='TRANSAKTIONSID'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],120,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=FREIGABE".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='FREIGABE'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_FREIGABE'],150,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=ABGEHOLT".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='FREIGABE'))?'~':'');	
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID_TRANSFER'],150,'font-size: 75%;',$Link);
						$Link = $MNU_ITEM_LINK."&JOBID=".$JOBID."&SortSub=AUDITINFO".((isset($_GET['SortSub']) AND ($_GET['SortSub']=='AUDITINFO'))?'~':'');
					awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_AUDITINFO'],300,'font-size: 75%;',$Link);
					awis_FORM_ZeileEnde();
					
					$boldText=0;
					
					for($EFLDetailZeile=0;$EFLDetailZeile<$rsEFLDetailZeilen;$EFLDetailZeile++)
					{
						if ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='E'){
							$Hintergrund='background-color: FF0000';
						}
						elseif ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='L'){
							$Hintergrund='background-color: FFFF00';	
						}
						elseif ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='T'){
							$Hintergrund='background-color: 00FF00';
						}
						elseif ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='M'){
							$Hintergrund='background-color: FF6921';
						}
						elseif ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='X'){
							$Hintergrund='background-color: D1EFEE';
						}
						elseif ($rsEFLDetail['ARBEITSSTATUS'][$EFLDetailZeile]=='V'){
							$Hintergrund='background-color: FF3F47';
						}		
						else{
							$Hintergrund='';
						}
						
						if ($boldText===0)
						{
							$boldText=1;
							$styleList='font-weight: bold;font-size: 75%;'.$Hintergrund;
						}
						else 
						{
							$styleList='font-size: 75%;'.$Hintergrund;
						}
						
						awis_FORM_ZeileStart();
						awis_FORM_Erstelle_ListenFeld('JOBTYP',$rsEFLDetail['JOBTYP'][$EFLDetailZeile],0,50,false,($EFLDetailZeile%2),$styleList,'','Z');
						awis_FORM_Erstelle_ListenFeld('BEZEICHNUNG',$rsEFLDetail['BEZEICHNUNG'][$EFLDetailZeile],0,250,false,($EFLDetailZeile%2),$styleList,'','T');
						awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsEFLDetail['TRANSAKTIONSID'][$EFLDetailZeile],0,120,false,($EFLDetailZeile%2),$styleList,'','Z');
						awis_FORM_Erstelle_ListenFeld('FREIGABE',$rsEFLDetail['FREIGABE'][$EFLDetailZeile],0,150,false,($EFLDetailZeile%2),$styleList,'','DU');
						awis_FORM_Erstelle_ListenFeld('ABGEHOLT',$rsEFLDetail['ABGEHOLT'][$EFLDetailZeile],0,150,false,($EFLDetailZeile%2),$styleList,'','DU');
						awis_FORM_Erstelle_ListenFeld('AUDITINFO',$rsEFLDetail['AUDITINFO'][$EFLDetailZeile],0,300,false,($EFLDetailZeile%2),$styleList,'','T');
						awis_FORM_ZeileEnde();
					}
					awis_FORM_Trennzeile();			
				}
				
			}
		
		awis_FORM_FormularEnde();
		}
	}
	
	If(($Recht3111&8192)==8192){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=4";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">Log-File-&Uuml;bersicht</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='4')
	{
		$LogDir_DatenLog='/home20/dbdaten/dfue/experian/daten/log/';
		$LogDir_Log='/home20/dbdaten/dfue/experian/log/';
		
		$LogDir_URL=$COMWebSrv.'/schnelldiagnose/showdir_log.php?dir=';
		
		echo "<ul type='disc'>";
		echo "<li><a href=".$LogDir_URL.$LogDir_DatenLog." target='_blank'>LogFiles - A4/A5/T3/AX-Transfer/...</a>";
		echo "<li><a href=".$LogDir_URL.$LogDir_Log." target='_blank'>LogFiles - Preislisten/Kunden-KFZ/WWS/...</a></li>";
		echo "</ul>";
	}
	
}

// DWH56

If(($Recht3111&1024)==1024){
	$MNU_LINK="<li><a href=./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=4>COM => DWH56</a></li><p>";
	echo $MNU_LINK;
}

if ($MNU_ID=='4')
{
	If(($Recht3111&2048)==2048){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=1";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - INT_STG Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='1')
	{
		$dwh56con=@OCILogon("AWIS_READ","AWIS_READ","DWH56");
		
		if ($dwh56con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_INT_STG_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH56=awisOpenRecordset($dwh56con,$SQL);
			$rsDWH56Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH56Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH56Zeile=0;$DWH56Zeile<$rsDWH56Zeilen;$DWH56Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH56['SOURCE_TAB'][$DWH56Zeile],0,300,false,($DWH56Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH56['JOBTYPE_ID'][$DWH56Zeile],0,100,false,($DWH56Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH56['LADEN_JA_NEIN'][$DWH56Zeile],0,150,false,($DWH56Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH56['TRANSAKTIONSID'][$DWH56Zeile],0,200,false,($DWH56Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh56con);
		}
	}
	
	If(($Recht3111&4096)==4096){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=2";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - STG_ODS Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='2')
	{
		$dwh56con=@OCILogon("AWIS_READ","AWIS_READ","DWH56");
		
		if ($dwh56con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_STG_ODS_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH56=awisOpenRecordset($dwh56con,$SQL);
			$rsDWH56Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH56Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH56Zeile=0;$DWH56Zeile<$rsDWH56Zeilen;$DWH56Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH56['SOURCE_TAB'][$DWH56Zeile],0,300,false,($DWH56Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH56['JOBTYPE_ID'][$DWH56Zeile],0,100,false,($DWH56Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH56['LADEN_JA_NEIN'][$DWH56Zeile],0,150,false,($DWH56Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH56['TRANSAKTIONSID'][$DWH56Zeile],0,200,false,($DWH56Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh56con);
		}
	}
}

// DWH37

If(($Recht3111&8)==8){
	$MNU_LINK="<li><a href=./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=2>COM => DWH37</a></li><p>";
	echo $MNU_LINK;
}

if ($MNU_ID=='2')
{
	If(($Recht3111&16)==16){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=1";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - INT_STG Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='1')
	{
		$dwh37con=@OCILogon("AWIS_READ","AWIS_READ","DWH37");
		
		if ($dwh37con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_INT_STG_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH37=awisOpenRecordset($dwh37con,$SQL);
			$rsDWH37Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH37Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH37Zeile=0;$DWH37Zeile<$rsDWH37Zeilen;$DWH37Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH37['SOURCE_TAB'][$DWH37Zeile],0,300,false,($DWH37Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH37['JOBTYPE_ID'][$DWH37Zeile],0,100,false,($DWH37Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH37['LADEN_JA_NEIN'][$DWH37Zeile],0,150,false,($DWH37Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH37['TRANSAKTIONSID'][$DWH37Zeile],0,200,false,($DWH37Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh37con);
		}
	}
	
	If(($Recht3111&32)==32){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=2";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - STG_ODS Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='2')
	{
		$dwh37con=@OCILogon("AWIS_READ","AWIS_READ","DWH37");
		
		if ($dwh37con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_STG_ODS_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH37=awisOpenRecordset($dwh37con,$SQL);
			$rsDWH37Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH37Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH37Zeile=0;$DWH37Zeile<$rsDWH37Zeilen;$DWH37Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH37['SOURCE_TAB'][$DWH37Zeile],0,300,false,($DWH37Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH37['JOBTYPE_ID'][$DWH37Zeile],0,100,false,($DWH37Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH37['LADEN_JA_NEIN'][$DWH37Zeile],0,150,false,($DWH37Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH37['TRANSAKTIONSID'][$DWH37Zeile],0,200,false,($DWH37Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh37con);
		}
	}
}

// DWH26

If(($Recht3111&64)==64){
	$MNU_LINK="<li><a href=./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=3>COM => DWH26</a></li><p>";
	echo $MNU_LINK;
}

if ($MNU_ID=='3')
{
	If(($Recht3111&128)==128){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=1";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - INT_STG Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='1')
	{
		$dwh26con=@OCILogon("AWIS_READ","AWIS_READ","DWH26");
		
		if ($dwh26con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_INT_STG_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH26=awisOpenRecordset($dwh26con,$SQL);
			$rsDWH26Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH26Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH26Zeile=0;$DWH26Zeile<$rsDWH26Zeilen;$DWH26Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH26['SOURCE_TAB'][$DWH26Zeile],0,300,false,($DWH26Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH26['JOBTYPE_ID'][$DWH26Zeile],0,100,false,($DWH26Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH26['LADEN_JA_NEIN'][$DWH26Zeile],0,150,false,($DWH26Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH26['TRANSAKTIONSID'][$DWH26Zeile],0,200,false,($DWH26Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh26con);
		}
	}
	
	If(($Recht3111&256)==256){
		$MNU_ITEM_LINK="./operating_COM_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=2";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">WAD - STG_ODS Offen</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='2')
	{
		$dwh26con=@OCILogon("AWIS_READ","AWIS_READ","DWH26");
		
		if ($dwh26con!=FALSE)
		{
			
			$SQL="SELECT SOURCE_TAB, JOBTYPE_ID, LADEN_JA_NEIN, TRANSAKTIONSID FROM ODS.V_ODS_COM_WAD_STG_ODS_OFFEN";
			if(!isset($_GET['Sort']))
			{
				$SQL .= ' ORDER BY SOURCE_TAB';
				$SORT_LINK='';
			}
			else
			{
				$SORT_LINK='&Sort='.$_GET['Sort'];
				$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
			}
			
			$rsDWH26=awisOpenRecordset($dwh26con,$SQL);
			$rsDWH26Zeilen=$awisRSZeilen;
	
			//awis_Debug(1,$SQL);
				
			if ($rsDWH26Zeilen>0)
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				
					$Link = $MNU_ITEM_LINK.'&Sort=SOURCE_TAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SOURCE_ATB'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_SOURCE_TAB'],300,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=JOBTYPE_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOBTYPE_ID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_JOBTYP'],100,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=LADEN_JA_NEIN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LADEN_JA_NEIN'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_LADEN_JA_NEIN'],150,'',$Link);
					$Link = $MNU_ITEM_LINK.'&Sort=TRANSAKTIONSID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TRANSAKTIONSID'))?'~':'');
				awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Allgemein']['OCA_TID'],200,'',$Link);

				awis_FORM_ZeileEnde();				
				
				for($DWH26Zeile=0;$DWH26Zeile<$rsDWH26Zeilen;$DWH26Zeile++)
				{
				
					awis_FORM_ZeileStart();

					awis_FORM_Erstelle_ListenFeld('SOURCE_TAB',$rsDWH26['SOURCE_TAB'][$DWH26Zeile],0,300,false,($DWH26Zeile%2),'',$Link,'T');
					awis_FORM_Erstelle_ListenFeld('JOBTYPE_ID',$rsDWH26['JOBTYPE_ID'][$DWH26Zeile],0,100,false,($DWH26Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('DESCRIPTION',$rsDWH26['LADEN_JA_NEIN'][$DWH26Zeile],0,150,false,($DWH26Zeile%2),'',$Link,'Z');
					awis_FORM_Erstelle_ListenFeld('TRANSAKTIONSID',$rsDWH26['TRANSAKTIONSID'][$DWH26Zeile],0,200,false,($DWH26Zeile%2),'',$Link,'Z');					
						
					awis_FORM_ZeileEnde();
					
				}	
				awis_FORM_FormularEnde();
			}
			
			ocilogoff($dwh26con);
		}
	}
}

if(isset($_POST['cmdLoeschen_x']))
{
	include('./operating_COM_Errlog_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./operating_COM_Errlog_speichern.php');
}

if(isset($_POST['cmdLuecken_x']))
{	
	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Filiale'],100);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Versuch'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Import_Am'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_COM_Dbmdaten']['OCD_Version_Fehlt_Seit'],200);
	awis_FORM_ZeileEnde();
	
	$SQL="SELECT * FROM EXPERIAN_ATU.V_DBM_VERSION_FEHLEND ORDER BY 1, 2";
	$rsDVF=awisOpenRecordset($comcon,$SQL);
	$rsDVFZeilen = $awisRSZeilen;
	
	for($DVFZeile=0;$DVFZeile<$rsDVFZeilen;$DVFZeile++)
	{
		awis_FORM_ZeileStart();
		$Link='';
		if(($Recht3111&4)==4)
		{
			$Link = "./operating_COM_Main.php?cmdAktion=DBM-Daten&EXEC=".$rsDVF['PKL_ROWID'][$DVFZeile]."";
		}
		awis_FORM_Erstelle_ListenFeld('FILIALE',$rsDVF['FILIALE'][$DVFZeile],0,100,false,($DVFZeile%2),'height: 27px; ',$Link,'Z');
		awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION',$rsDVF['IMPORT_VERSION'][$DVFZeile],0,200,false,($DVFZeile%2),'height: 27px; ',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('IMPORT_VERSION_FEHLT',$rsDVF['IMPORT_VERSION_FEHLT'][$DVFZeile],0,200,false,($DVFZeile%2),'height: 27px; ',$Link,'T');
	
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}


echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

