<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';
require_once 'awisMailer.inc';

class heidler_funktionen
{
    public $AWIS_KEY1;
    public $AWISSprachKonserven;
    public $_DB;
    public $_Form;
    public $_AWISBenutzer;

    private $DPDRestHeader = [
        'Version'                     => '100',
        'Language'                    => 'de_DE',
        'PartnerCredentials-Name'     => 'DPD Cloud Service Alpha2',
        'PartnerCredentials-Token'    => '33879594E70436D58685',
        'UserCredentials-cloudUserID' => '639134',
        'UserCredentials-Token'       => '338746249733968',
    ];

    public function __construct()
    {
        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('HEIDLER', '%');
        $TextKonserven[] = array('NDP', '%');
        $TextKonserven[] = array('NDK', '%');
        $TextKonserven[] = array('PER', '%');
        $TextKonserven[] = array('FIL','FIL_BEZ');
        $TextKonserven[] = array('Wort', 'Name');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'wrd_Stand');
        $TextKonserven[] = array('Wort', 'NOX');
        $TextKonserven[] = array('Wort', 'DPD');
        $TextKonserven[] = array('Wort', 'Dienstleister');
        $TextKonserven[] = array('Wort', 'Meldefrist');
        $TextKonserven[] = array('Wort', 'Meldegrund');
        $TextKonserven[] = array('Wort', 'Ansprechpartner_Rueckfragen');
        $TextKonserven[] = array('Wort', 'Ansprechpartner');
        $TextKonserven[] = array('Wort', 'Differenzmenge');
        $TextKonserven[] = array('Wort', 'Transportschaden');
        $TextKonserven[] = array('Wort', 'NachtraeglichZugestellt');
        $TextKonserven[] = array('Wort', 'Zugestellt');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Wort','Ja');
        $TextKonserven[] = array('Wort','Nein');

        $this->_AWISBenutzer = awisBenutzer::Init();

        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $this->_Form = new awisFormular();

        $this->AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    public function SwitchVersender ($VersenderID) {

        // Parameter $versender = 1 --> alle Reifen auf NOX (Versandart: 1 / Mandant: 38)
        // Parameter $versender = 2 --> alle Reifen auf DPD (Versandart: 2 / Mandant: 38)

        $SQL = 'UPDATE NVS_HEIDLER@NVSWEN ';
        $SQL .= 'SET VERSANDART = ' . $this->_DB->WertSetzen('NVS','T',$VersenderID);
        $SQL .= ' ,MANDANT = ' . $this->_DB->WertSetzen('NVS','T', ($VersenderID === 1?'38':'38'));

        if ($VersenderID === 1) {
            $SQL .= ' ,SONDERDIENST = null';
        } else {
            $SQL .= ' ,SONDERDIENST = ' . $this->_DB->WertSetzen('NVS','T','TYRE');
        }

        $SQL .= ' WHERE VERBUCHTE_MENGE = 0';
        $SQL .= ' AND WARENGRUPPE IN (\'01\',\'02\',\'23\',\'04\',\'14\')';

        $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('NVS'));
    }

    public function HeidlerAjax()
    {

        $SQL = 'SELECT SATZKENNUNG FROM NVS_HEIDLER@NVSWEN';
        $AnzahlGesamt = $this->_DB->ErmittleZeilenAnzahl($SQL);

        $SQL = 'SELECT ';
        $SQL .= ' CASE WHEN VERSANDART = 1 THEN';
        $SQL .= '   \'NOX\'';
        $SQL .= ' ELSE';
        $SQL .= '   \'DPD\'';
        $SQL .= ' END AS VERSENDER';
        $SQL .= ' ,KNDNR';
        $SQL .= ' ,EMPF_NAME1';
        $SQL .= ' ,EMPF_NAME2';
        $SQL .= ' ,EMPF_NAME3';
        $SQL .= ' ,EMPF_LKZ';
        $SQL .= ' ,GEWICHT_NETTO';
        $SQL .= ' ,ATU_NR';
        $SQL .= ' ,WARENGRUPPE';
        $SQL .= ' ,SORTIMENT';
        $SQL .= ' ,TO_DATE(DATUM_KOMMISSIONIERUNG || \' \' || ZEIT_KOMMISSIONIERUNG, \'DD.MM.RR HH24:MI:SS\') AS DATUM_KOMMISSIONIERUNG';
        $SQL .= ' ,MENGE';
        $SQL .= ' ,VERBUCHTE_MENGE';
        $SQL .= ' ,LAGERKZ';
        $SQL .= ' ,row_number() OVER (';
        $SQL .= '  order by DATUM_KOMMISSIONIERUNG) as ZEILENNR';
        $SQL .= ' FROM NVS_HEIDLER@NVSWEN';
        $SQL .= ' WHERE MENGE <> VERBUCHTE_MENGE';

        $AnzahlJetzt = $this->_DB->ErmittleZeilenAnzahl($SQL);
        $ZeilenProSeite = $this->_AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $Block = ((isset($_GET['txtBlock']) and $_GET['txtBlock'] != '')?$_GET['txtBlock']:1);
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $this->_DB->ErmittleZeilenAnzahl($SQL);

        $this->_Form->DebugAusgabe(1, $SQL);
        if (!isset($AWIS_KEY1) OR $AWIS_KEY1 == '') {
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
        }

        $rsHeidler = $this->_DB->RecordSetOeffnen($SQL);

        // Spaltenbreiten f?r Listenansicht
        $FeldBreiten = array();
        $FeldBreiten['VERSENDER'] = 100;
        $FeldBreiten['KNDNR'] = 80;
        $FeldBreiten['EMPF_NAME1'] = 200;
        $FeldBreiten['EMPF_LKZ'] = 100;
        $FeldBreiten['GEWICHT_NETTO'] = 100;
        $FeldBreiten['ATU_NR'] = 100;
        $FeldBreiten['WARENGRUPPE'] = 100;
        $FeldBreiten['SORTIMENT'] = 100;
        $FeldBreiten['DATUM_KOMMISSIONIERUNG'] = 170;
        $FeldBreiten['MENGE'] = 70;
        $FeldBreiten['VERBUCHTE_MENGE'] = 130;

        $this->_Form->Formular_Start();

        if (($rsHeidler->AnzahlDatensaetze() >= 1)) {

            $this->_Form->ScrollBereichStart('box', 600, 0);

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['HEIDLER_ANZAHL_GESAMT'], 120);
            $this->_Form->Erstelle_TextFeld('gesamt', $AnzahlGesamt, 20, 100);
            $this->_Form->ZeileEnde();
            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['HEIDLER_ANZAHL_OFFEN'], 120);
            $this->_Form->Erstelle_TextFeld('offen', $AnzahlJetzt, 20, 100);
            $this->_Form->ZeileEnde();
            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['STAND'], 120);
            $this->_Form->Erstelle_TextFeld('Stand', date('d.m.y H:i:s'), 20, 200, false);
            $this->_Form->ZeileEnde();

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['VERSENDER'], $FeldBreiten['VERSENDER'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['KNDNR'], $FeldBreiten['KNDNR'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['EMPF_NAME1'], $FeldBreiten['EMPF_NAME1'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['EMPF_LKZ'], $FeldBreiten['EMPF_LKZ'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['GEWICHT_NETTO'], $FeldBreiten['GEWICHT_NETTO'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['ATU_NR'], $FeldBreiten['ATU_NR'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['WARENGRUPPE'], $FeldBreiten['WARENGRUPPE'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['SORTIMENT'], $FeldBreiten['SORTIMENT'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'], $FeldBreiten['DATUM_KOMMISSIONIERUNG'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['MENGE'], $FeldBreiten['MENGE'], '');
            $this->_Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['HEIDLER']['VERBUCHTE_MENGE'], $FeldBreiten['VERBUCHTE_MENGE'], '');
            $this->_Form->ZeileEnde();

            $DS = 0;    // fuer Hintergrundfarbumschaltung
            while (!$rsHeidler->EOF()) {
                $this->_Form->ZeileStart();

                $TTT = $rsHeidler->FeldInhalt('VERSENDER');
                $this->_Form->Erstelle_ListenFeld('VERSENDER', $rsHeidler->FeldInhalt('VERSENDER'), 0, $FeldBreiten['VERSENDER'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('KNDNR');
                $this->_Form->Erstelle_ListenFeld('KNDNR', $rsHeidler->FeldInhalt('KNDNR'), 0, $FeldBreiten['KNDNR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('EMPF_NAME1');
                $this->_Form->Erstelle_ListenFeld('EMPF_NAME1', $rsHeidler->FeldInhalt('EMPF_NAME1'), 0, $FeldBreiten['EMPF_NAME1'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('EMPF_LKZ');
                $this->_Form->Erstelle_ListenFeld('EMPF_LKZ', $rsHeidler->FeldInhalt('EMPF_LKZ'), 0, $FeldBreiten['EMPF_LKZ'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('GEWICHT_NETTO');
                $this->_Form->Erstelle_ListenFeld('GEWICHT_NETTO', $rsHeidler->FeldInhalt('GEWICHT_NETTO'), 0, $FeldBreiten['GEWICHT_NETTO'], false, ($DS % 2), '', '', 'N2', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('ATU_NR');
                $this->_Form->Erstelle_ListenFeld('ATU_NR', $rsHeidler->FeldInhalt('ATU_NR'), 0, $FeldBreiten['ATU_NR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('WARENGRUPPE');
                $this->_Form->Erstelle_ListenFeld('WARENGRUPPE', $rsHeidler->FeldInhalt('WARENGRUPPE'), 0, $FeldBreiten['WARENGRUPPE'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('SORTIMENT');
                $this->_Form->Erstelle_ListenFeld('SORTIMENT', $rsHeidler->FeldInhalt('SORTIMENT'), 0, $FeldBreiten['SORTIMENT'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG');
                $this->_Form->Erstelle_ListenFeld('DATUM_KOMMISSIONIERUNG', $rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG'), 0, $FeldBreiten['DATUM_KOMMISSIONIERUNG'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('MENGE');
                $this->_Form->Erstelle_ListenFeld('MENGE', $rsHeidler->FeldInhalt('MENGE'), 0, $FeldBreiten['MENGE'], false, ($DS % 2), '', '', 'Z', 'L', $TTT);

                $TTT = $rsHeidler->FeldInhalt('VERBUCHTE_MENGE');
                $this->_Form->Erstelle_ListenFeld('VERBUCHTE_MENGE', $rsHeidler->FeldInhalt('VERBUCHTE_MENGE'), 0, $FeldBreiten['VERBUCHTE_MENGE'], false, ($DS % 2), '', '', 'Z', 'L', $TTT);

                $this->_Form->ZeileEnde();
                $DS++;
                $rsHeidler->DSWeiter();
            }

            $Link = './heidler_Main.php?cmdAktion=OffenePos';
            $this->_Form->SchreibeHTMLCode('<form name="frmDetails" action="' . $Link . '" method=POST  enctype="multipart/form-data">');
            $this->_Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
            $this->_Form->SchreibeHTMLCode('</form>');
        } else {
            $this->_Form->ZeileStart();
            $this->_Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineDaten']);
            $this->_Form->ZeileEnde();
        }

        $this->_Form->Erstelle_HiddenFeld('AUFTRAG_ID', $this->AWIS_KEY1);

        $this->_Form->ScrollBereichEnde();
    }

    /**
     * @param $Versandart int
     * @param $TrackingNummern string
     */
    public function PaketStatus($Versandart, $TrackingNummern)
    {
        $LabelBreite["Puffer"] = 30;
        $LabelBreite["Label"] = 200;
        $LabelBreite["Info"] = 500;



        if ($Versandart == 2) {

            $TrackingNummern = array_filter(explode('|', $TrackingNummern));

            foreach ($TrackingNummern as $TrackingNummer) {

                $SQL = 'SELECT HEADLINE, LAST_DESCRIPTION, STATUS_DATE, DELIVERED_STATUS, DEPOT_ZIPCODE, DEPOT_CITY, STATUS_ID 
                        FROM (SELECT * FROM DPD_STATUS@NVSWEN 
                        WHERE TRACKING_NR = ' . $this->_DB->WertSetzen('DPD', 'T', $TrackingNummer) . ' and import_date is not null
                        ORDER BY import_DATE desc) where rownum = 1 ';
                $rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('DPD'));

                $this->_Form->ZeileStart();
                $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['TRACKING_NR'] . ':', $LabelBreite["Label"]);
                $this->_Form->Erstelle_TextLabel($TrackingNummer, $LabelBreite["Info"]);
                $this->_Form->ZeileEnde();

                if ($rec->AnzahlDatensaetze() > 0) {
                    //Status
                    $this->_Form->ZeileStart();
                    $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                    $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['STATUS'] . ':', $LabelBreite["Label"]);
                    $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('HEADLINE'), $LabelBreite ["Info"]);
                    $this->_Form->ZeileEnde();
                    if ($rec->FeldInhalt('HEADLINE') != $rec->FeldInhalt('LAST_DESCRIPTION')) {
                        $this->_Form->ZeileStart();
                        $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                        $this->_Form->Erstelle_TextLabel('', $LabelBreite["Label"]);
                        $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('LAST_DESCRIPTION'), $LabelBreite["Info"]);
                        $this->_Form->ZeileEnde();
                    }

                    //Letztes Update
                    $this->_Form->ZeileStart();
                    $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                    $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['LETZTESUPDATE'] . ':', $LabelBreite["Label"]);
                    $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('STATUS_DATE'), $LabelBreite["Info"]);
                    $this->_Form->ZeileEnde();

                    //Letztes Depot
                    if ($rec->FeldInhalt('DELIVERED_STATUS') == false) {
                        $this->_Form->ZeileStart();
                        $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['LETZTESDEPOT'] . ':', $LabelBreite["Label"]);
                        $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('DEPOT_ZIPCODE') . ' ' . $rec->FeldInhalt('DEPOT_CITY'), $LabelBreite["Info"]);
                        $this->_Form->ZeileEnde();
                    }
                } else {
                    $this->_Form->ZeileStart();
                    $this->_Form->Hinweistext($this->AWISSprachKonserven["Fehler"]["err_keineDaten"], awisFormular::HINWEISTEXT_HINWEIS);
                    $this->_Form->ZeileEnde();
                }


                }
            } elseif ($Versandart == 1 or $Versandart == 4) {
                $TrackingNummern = array_filter(explode('|', $TrackingNummern));
                foreach ($TrackingNummern as $TrackingNummer) {
                    $SQL = 'SELECT feld.BESCHREIBUNG as FELD_BESCHREIBUNG, code.BESCHREIBUNG as CODE_BESCHREIBUNG, src.SCAN_ORT, src.SCAN_FELD, src.DATUM FROM NOX_STATUS@NVSWEN src
                        LEFT JOIN NOX_STATUS_SCAN_FELD@NVSWEN feld
                        ON feld.SCAN_FELD = src.SCAN_FELD
                        LEFT JOIN NOX_STATUS_STATUSCODE@NVSWEN code
                        ON code.STATUSCODE = src.STATUSCODE
                        WHERE src.TRACKING_NR = ' . $this->_DB->WertSetzen('NOX', 'T', $TrackingNummer);
                    $rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('NOX'));

                    $this->_Form->ZeileStart();
                    $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['TRACKING_NR'] . ':', $LabelBreite["Label"]);
                    $this->_Form->Erstelle_TextLabel($TrackingNummer, $LabelBreite["Info"]);
                    $this->_Form->ZeileEnde();

                    if ($rec->AnzahlDatensaetze() > 0) {

                        $this->_Form->ZeileStart();
                        $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['STATUS'] . ':', $LabelBreite["Label"]);
                        $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('FELD_BESCHREIBUNG'), $LabelBreite ["Info"]);
                        $this->_Form->ZeileEnde();

                        if ($rec->FeldInhalt('CODE_BESCHREIBUNG') != '') {
                            $this->_Form->ZeileStart();
                            $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                            $this->_Form->Erstelle_TextLabel('', $LabelBreite["Label"]);
                            $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('CODE_BESCHREIBUNG'), $LabelBreite["Info"]);
                            $this->_Form->ZeileEnde();
                        }

                        //Letztes Update
                        $this->_Form->ZeileStart();
                        $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['LETZTESUPDATE'] . ':', $LabelBreite["Label"]);
                        $this->_Form->Erstelle_TextLabel($rec->FeldInhalt('DATUM'), $LabelBreite["Info"]);
                        $this->_Form->ZeileEnde();

                        //Letztes Depot
                        if ($rec->FeldInhalt('SCAN_FELD') != '019') {
                            $this->_Form->ZeileStart();
                            $this->_Form->Erstelle_TextLabel('', $LabelBreite["Puffer"]);
                            $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['HEIDLER']['LETZTESDEPOT'] . ':', $LabelBreite["Label"]);
                            $this->_Form->Erstelle_TextLabel(preg_replace('/\d+\s/', '', $rec->FeldInhalt('SCAN_ORT')), $LabelBreite["Info"]);
                            $this->_Form->ZeileEnde();
                        }
                    } else {
                        $this->_Form->ZeileStart();
                        $this->_Form->Hinweistext($this->AWISSprachKonserven["HEIDLER"]["KEINE_DATEN"], awisFormular::HINWEISTEXT_HINWEIS);
                        $this->_Form->ZeileEnde();
                    }
                }
            }
        }

        /**
         * @param $Versandart int
         * @param $TrackingNummern string
         * @return string
         */
    public function PaketStatusKurz($Versandart, $TrackingNummern) {
        $TrackingNummern = array_filter(explode('|', $TrackingNummern));
        $Return = '';
        if ($Versandart == 2) {
            //###################### NEU MACHEN #####################
            //Status selektrieren aus Tabelle DPD_STATUS; bei null "Mit den angegebenen Parameter konnte nichts gefunden werden"


            $Return = '';
            foreach ($TrackingNummern as $TrackingNummer) {

                $SQL = 'select';
                $SQL .= '  HEADLINE';
                $SQL .= ' ,LAST_DESCRIPTION';
                $SQL .= ' ,DEPOT_ZIPCODE';
                $SQL .= ' ,DEPOT_CITY';
                $SQL .= ' ,STATUS_DATE';
                $SQL .= ' ,DELIVERED_STATUS';
                $SQL .= ' ,STATUS_ID';
                $SQL .= ' from';
                $SQL .= '   (';
                $SQL .= '       select *';
                $SQL .= '       from DPD_STATUS@NVSWEN';
                $SQL .= '       where TRACKING_NR = ' . $this->_DB->WertSetzen('DPD', 'T', $TrackingNummer);
                $SQL .= '       and IMPORT_DATE is not null';
                $SQL .= '       order by IMPORT_DATE desc';
                $SQL .= '   )';
                $SQL .= ' where rownum = 1';

                $rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('DPD'));

                $Return .= $rec->FeldInhalt('HEADLINE');

                if ($rec->FeldInhalt('HEADLINE') != $rec->FeldInhalt('LAST_DESCRIPTION')) {
                    if ($rec->FeldInhalt('STATUS_ID') == 'DELIVERED') {
                        $Return .= ' ' . $rec->FeldInhalt('STATUS_DATE');
                    } else {
                        $Return .= '; ' . $rec->FeldInhalt('LAST_DESCRIPTION');
                    }
                }
                if ($rec->FeldInhalt('DELIVERED_STATUS') == false) {
                    $Return .= ' (';
                    $Return .= $rec->FeldInhalt('DEPOT_CITY');
                    $Return .= ')';
                }

            }

        } elseif ($Versandart == 1 or $Versandart == 4) {
            $Return = '';
            foreach ($TrackingNummern as $TrackingNummer) {

                $SQL = 'select';
                $SQL .= '  FELD.BESCHREIBUNG as FELD_BESCHREIBUNG';
                $SQL .= ' ,CODE.BESCHREIBUNG as CODE_BESCHREIBUNG';
                $SQL .= ' ,SRC.SCAN_ORT';
                $SQL .= ' ,SRC.SCAN_FELD';
                $SQL .= ' ,SRC.DATUM';
                $SQL .= ' from NOX_STATUS@NVSWEN SRC left join NOX_STATUS_SCAN_FELD@NVSWEN FELD';
                $SQL .= ' on FELD.SCAN_FELD = SRC.SCAN_FELD';
                $SQL .= ' left join NOX_STATUS_STATUSCODE@NVSWEN CODE';
                $SQL .= ' on CODE.STATUSCODE = SRC.STATUSCODE';
                $SQL .= ' where SRC.TRACKING_NR = ' . $this->_DB->WertSetzen('NOX', 'T', $TrackingNummer);

                $rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('NOX'));

                if ($rec->FeldInhalt('FELD_BESCHREIBUNG') != '') {
                    $Depot = preg_replace('/\d+\s/', '', $rec->FeldInhalt('SCAN_ORT'));
                    $Return .= $rec->FeldInhalt('FELD_BESCHREIBUNG');
                    $Return .= ($rec->FeldInhalt('SCAN_FELD') != '019' ? ' | ' . $this->AWISSprachKonserven["HEIDLER"]["DEPOT_WORT"] . ' ' . $Depot : ' ' . $rec->FeldInhalt('DATUM'));
                }
            }
        }

        return ($Return != ''?$Return:$this->AWISSprachKonserven["HEIDLER"]["KEINE_DATEN"]);
    }

    /*
     * Gibt den aktuell g�ltigen vollen MWST-Satz f�r DE zur�ck
     */
    public function mwstSatz () {
        $SQL = '  SELECT mws_satz ';
        $SQL .= ' FROM mehrwertsteuersaetze';
        $SQL .= ' WHERE mws_id = 2';
        $rec = $this->_DB->RecordSetOeffnen($SQL);

        return intval($rec->FeldInhalt('MWS_SATZ'));
    }

    /**
     * Gibt die Filialbezeichnung aus
     *
     * @param integer $FilNr Filialnummer die Eingegeben wurde
     */
    public function erstelleFilBezAjax($FilNr) {

        $SQL = 'select * from filialen ';
        $SQL .= ' where ';
        $SQL .= ' FIL_ID = ' . $this->_DB->WertSetzen('FIL','N0',$FilNr);

        $rsFil = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FIL',true));

        $FeldBreiten['Labels'] = 200;
        $FeldBreiten['Werte'] = 200;

        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['FIL']['FIL_BEZ'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextLabel($rsFil->FeldInhalt('FIL_BEZ'),$FeldBreiten['Werte']);
    }

    /**
     * Gibt Listenfeld mit der Artikelbezeichnung aus
     *
     * @param string  $Atunr ATU-Artikelnummer die Eingegeben wurde
     * @param integer $i     Zaehlerstand aus der Pos-Schleife
     *
     */
    public function erstelleArtBezAjax($Atunr,$i) {

        $SQL = 'select * from ';
        $SQL .= ' (';
        $SQL .= '   SELECT ast_atunr, ast_bezeichnungww FROM artikelstamm ';
        $SQL .= '   UNION ALL ';
        $SQL .= '   SELECT hba_artnr as ast_atunr, hba_artbezl as ast_bezeichnungww FROM hubstamm ';
        $SQL .= ' )';
        $SQL .= ' where ';
        $SQL .= ' AST_ATUNR = ' . $this->_DB->WertSetzen('AST','T',strtoupper($Atunr));

        $rsAst = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('AST',true));

        $this->_Form->Erstelle_ListenFeld('NDP_AST_BEZEICHNUNGWW_'.$i ,$rsAst->FeldInhalt('AST_BEZEICHNUNGWW'),50,320,true);
    }

    /**
     * Gibt den Namen aus
     *
     * @param integer $PersonalNr Personenalnummer die Eingegeben wurde
     */
    public function erstelleAnsprechpartnerAjax($PersNr) {

        $SQL = 'select vorname || \' \' || name as persname from v_personal_komplett ';
        $SQL .= ' where ';
        $SQL .= ' PERSNR = ' . $this->_DB->WertSetzen('PER','N0',$PersNr);

        $rsFil = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER',true));

        $FeldBreiten['Labels'] = 180;
        $FeldBreiten['Werte'] = 200;

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->AWISSprachKonserven['Wort']['Name'] . ':', $FeldBreiten['Labels']);
        $this->_Form->Erstelle_TextFeld('NDK_ANSPRECHPARTNER', $rsFil->FeldInhalt('PERSNAME'),$FeldBreiten['Werte'], 200, true);
        $this->_Form->ZeileEnde();
    }

    /**
     * Versendet die Mail an die Fachabteilung wenn Pfleger das moechte
     * @param integer $NVK_KEY Key der Tabelle NVSDIFFERENZKOPF
     */
    public function versendeMail ($NDK_KEY) {

        //**************************************************************
        //*                Mailtext laden                              *
        //**************************************************************
        $SQL = 'SELECT mvt_bereich, mvt_betreff, mvt_text ';
        $SQL .= 'FROM mailversandtexte ';
        $SQL .= 'WHERE mvt_bereich = '.$this->_DB->WertSetzen('MVT','T', 'NDK_INFO_WARENBESTAENDE');

        $rsMVT = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('MVT'));

        $mailBetreff = $rsMVT->FeldInhalt('MVT_BETREFF');
        $mailText = $rsMVT->FeldInhalt('MVT_TEXT');

        //**************************************************************
        //*            Kopfdaten laden                                 *
        //**************************************************************
        $SQL = '  SELECT *';
        $SQL .= ' FROM nvsdifferenzkopf';
        $SQL .= ' WHERE ndk_key = ' . $this->_DB->WertSetzen('NDK', 'N0', $NDK_KEY);

        $rsNDK = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('NDK'));

        //**************************************************************
        //*            Positionsdaten laden                            *
        //**************************************************************
        $SQL = '  SELECT *';
        $SQL .= ' FROM nvsdifferenzpos';
        $SQL .= ' WHERE ndp_ndk_key = ' . $this->_DB->WertSetzen('NDP', 'N0', $NDK_KEY);
        $SQL .= ' ORDER BY ndp_key';

        $rsNDP = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('NDP'));

        //**************************************************************
        //*            Betreff bauen                                   *
        //**************************************************************
        $mailNr = intval($rsNDK->FeldInhalt('NDK_ANZMAILS')) + 1; // Fortlaufende Nummer damit FA die Reifenfolge kennt

        $mailBetreff = str_replace('#NDK_FIL_ID#', $rsNDK->FeldInhalt('NDK_FIL_ID'), $mailBetreff);
        $mailBetreff = str_replace('#NDK_LIEFERSCHEINDATUM#', substr($rsNDK->FeldInhalt('NDK_LIEFERSCHEINDATUM'),0,10), $mailBetreff);
        $Daten = explode('|',$this->AWISSprachKonserven['NDK']['lst_NDK_DIENSTLEISTER']);
        $mailBetreff = str_replace('#NDK_DIENSTLEISTER#', substr($Daten[$rsNDK->FeldInhalt('NDK_DIENSTLEISTER')-1],2), $mailBetreff); // hier muss noch der Key aufgel�st werden
        $mailBetreff = str_replace('#NDK_ANZMAILS#', $mailNr, $mailBetreff);
        if($mailNr > 1) {
            // Wenn Meldung mehr als 1x per Mail gesendet wurde kam es sicher aus dem Archiv. -> Hinweis an den Betreff
            $mailBetreff .= ' (aus Archiv)';
        }

        //**************************************************************
        //*            Mailbody bauen                                  *
        //**************************************************************
        $mailText = str_replace('#ANSPRECHPARTNER#', $rsNDK->FeldInhalt('NDK_ANSPRECHPARTNER'), $mailText);
        $mailText = str_replace('#LIEFERSCHEINDATUM#', substr($rsNDK->FeldInhalt('NDK_LIEFERSCHEINDATUM'),0,10), $mailText);

        // 1=Differenzmenge / 2=Transportschaden
        if($rsNDK->FeldInhalt('NDK_GRUND') == 1) {
            $mailText = str_replace('#X_DIFF#', 'X', $mailText);
            $mailText = str_replace('#X_TRNS#', '', $mailText);
        } elseif($rsNDK->FeldInhalt('NDK_GRUND') == 2) {
            $mailText = str_replace('#X_DIFF#', '', $mailText);
            $mailText = str_replace('#X_TRNS#', 'X', $mailText);
        }

        $mailText = str_replace('#X_ZUGE#', $rsNDK->FeldInhalt('NDK_ZUGESTELLT')==1?'X':'', $mailText);

        // ab jetzt werden ans Ende des Mailtext die Positionen angefuegt.
        while(!$rsNDP->EOF()) {
            $mailText .= '<tr>';
            $mailText .= '<td>'.$rsNDP->FeldInhalt('NDP_AST_ATUNR').'</td>';
            $rsArtBez = $this->getArtikelbezeichnung($rsNDP->FeldInhalt('NDP_AST_ATUNR'));
            $mailText .= '<td>'.$rsArtBez->FeldInhalt('AST_BEZEICHNUNGWW').'</td>';
            $mailText .= '<td>'.$rsNDP->FeldInhalt('NDP_MENGE').'</td>';
            $mailText .= '<td>'.$rsNDP->FeldInhalt('NDP_BEMERKUNG').'</td>';
            $mailText .= '</tr>';

            $rsNDP->DSWeiter();
        }

        // Jetzt noch die Abschluss-Tags ausgeben
        $mailText .= '</table>';
        $mailText .= '</body>';
        $mailText .= '</html>';

        $this->_MailObj = new awisMailer($this->_DB, $this->_AWISBenutzer);
        // Alte Daten loeschen
        $this->_MailObj->LoescheAdressListe();
        $this->_MailObj->AnhaengeLoeschen();

        $empfaengerMailAdresse = $this->_AWISBenutzer->ParameterLesen('NDK: Empfaengermail Differenzmeldung', true);
        $this->_MailObj->Absender(str_pad($rsNDK->FeldInhalt('NDK_FIL_ID'),4,'0', STR_PAD_LEFT).'@de.atu.eu');
        $this->_MailObj->SetzeVersandPrioritaet(100);
        $this->_MailObj->AdressListe(awisMailer::TYP_TO,$empfaengerMailAdresse, awisMailer::PRUEFE_LOGIK,awisMailer::PRUEFAKTION_ERR);
        $this->_MailObj->SetzeBezug('NDK', $NDK_KEY);
        $this->_MailObj->Betreff($mailBetreff);
        $this->_MailObj->Text($mailText, awisMailer::FORMAT_HTML, true);

        $this->_MailObj->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());

        // fortlaufende Nummer der Mails in Datenbank schreiben
        $SQL = '  UPDATE nvsdifferenzkopf set ndk_anzmails = ' . $this->_DB->WertSetzen('NDK','N0', $mailNr);
        $SQL .= ' WHERE ndk_key = ' . $this->_DB->WertSetzen('NDK', 'N0', $NDK_KEY);
        $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('NDK'));
    }

    public function ladeDaten ($Fil_ID, $AWIS_KEY1 = '') {
        $SQL = '  SELECT *';
        $SQL .= ' FROM nvsdifferenzkopf';
        $SQL .= ' WHERE ndk_key = ';
        $SQL .= ' (';
        $SQL .= '   SELECT max(ndk_key) as ndk_key';
        $SQL .= '   FROM nvsdifferenzkopf';
        $SQL .= '   WHERE ndk_fil_id = ' . $this->_DB->WertSetzen('NDK', 'N0', $Fil_ID);
        $SQL .= ' )';
            $SQL .= ' AND ndk_key = ' . $this->_DB->WertSetzen('NDK', 'N0', $AWIS_KEY1);


        return $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('NDK', true));
    }

    public function loeschePosition ($NDP_KEY) {
        $SQL = 'DELETE FROM nvsdifferenzpos';
        $SQL .= ' WHERE ndp_key = ' . $this->_DB->WertSetzen('NDP', 'N0', $NDP_KEY);

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('NDP'));
    }

    public function getMaximalePosNummer () {
        $posNr = 0;
        foreach($_POST as $key => $value) {
            $Feldname = substr($key,0,17);
            if($Feldname == 'txtNDP_AST_ATUNR_' OR $Feldname == 'sucNDP_AST_ATUNR_') {
                $dummy = intval(substr($key,17));
                $posNr = $dummy>$posNr?$dummy:$posNr;
            }
        }

        return intval($posNr);
    }

    public function getArtikelbezeichnung ($AST_ATUNR) {
        $SQL = '  SELECT ast_bezeichnungww';
        $SQL .= ' FROM ';
        $SQL .= ' (';
        $SQL .= '   SELECT ast_atunr, ast_bezeichnungww FROM artikelstamm ';
        $SQL .= '   UNION ALL ';
        $SQL .= '   SELECT hba_artnr as ast_atunr, hba_artbezl as ast_bezeichnungww FROM hubstamm ';
        $SQL .= ' )';
        $SQL .= ' WHERE ast_atunr = ' . $this->_DB->WertSetzen('AST', 'T', strtoupper($AST_ATUNR));

        return $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST'));
    }

    public function getAnsprechpartner ($PersNr) {
        $SQL = 'select vorname || \' \' || name as PERS_NAME from v_personal_komplett ';
        $SQL .= ' where ';
        $SQL .= ' PERSNR = ' . $this->_DB->WertSetzen('PER','N0',$PersNr);

        $rsFil = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER',true));

        if($rsFil->AnzahlDatensaetze() == 1) {
            return $rsFil->FeldInhalt('PERS_NAME');
        } else {
            return '';
        }
    }
}