<?php

include_once 'awisBenutzer.inc';
include_once 'awisDatenbank.inc';
include_once 'awisJobs.inc';

/**
 * Class awisReifenAX
 */
class awisReifenAX
{
    /**
     * @var object
     */
    private $_AWISBenutzer;

    /**
     * @var object
     */
    private $_DB;

    /**
     * @var int
     */
    private $_DebugLevel = 99;

    /**
     * @var string
     */
    private $_ExportPfad;

    /**
     * @var object
     */
    private $_AwisJobs;

    /**
     * @var string
     */
    private $_DebugText;

    /**
     * awisReifenAX constructor.
     * @param $Benutzer
     * @throws awisException
     */
    public function __construct($Benutzer, $DebugLevel = 99)
    {
        $this->_DebugLevel = $DebugLevel;
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_awisJobs = new awisJobs($Benutzer);

        $this->setExportPfad();
    }

    /**
     * awisReifenAX destructor
     */
    public function __destruct()
    {
        echo $this->_DebugText;
    }

    /**
     * Setzt den Exportpfad -> kommt aus der DB
     * @throws awisException
     */
    public function setExportPfad () {
        $this->_ExportPfad = $this->_AWISBenutzer->ParameterLesen('ReifenAXExportPfad');
        $this->debugAusgabe('Exportpfad gesetzt' . $this->_ExportPfad, 10);
    }

    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .=  ': ' . $Typ;
            $this->_DebugText .=  ': ' . $Text;
            $this->_DebugText .=  PHP_EOL;
        }
    }

    /**
     * Exportiert die Reifendaten f�r AX
     */
    public function exportReifenDaten () {
        $SQL = " SELECT *";
        $SQL.= " FROM v_reifen_export_ax_eu";

        $this->debugAusgabe($SQL, 50);

        $rsReifen = $this->_DB->RecordSetOeffnen($SQL);

        $this->debugAusgabe('Anzahl Datensaetze SQL: ' . $rsReifen->AnzahlDatensaetze(), 1);


        $this->debugAusgabe('Dateiname festlegen / Datei oeffnen');
        $praefix = 'reifenAWIS_';
        $datum = strftime('%Y%m%d%H%M%S');
        $fp = fopen($this->_ExportPfad . $praefix . $datum . '.txt', 'a');
        $i = 0;

        while(!$rsReifen->EOF()) {
            $Output = '';
            $Output.= $rsReifen->FeldInhalt('REI_AST_ATUNR');
            $Output.= ';';
            $Output.= $rsReifen->FeldInhalt('RIN_WINTERTAUGLICHKEIT');
            $Output.= ';';
            $Output.= $rsReifen->FeldInhalt('RIN_EISGRIFF');
            $Output.= chr(13) . chr(10);

            fputs($fp, $Output);

            $rsReifen->DSWeiter();
            $i++;
        }

        $this->debugAusgabe('Daten exportiert. Anzahl: ' . $i, 1);

        fclose($fp);
    }
}