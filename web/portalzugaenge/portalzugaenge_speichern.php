<?php
require_once 'awis_portalzugaenge.inc';
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('PZU','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','KennwoerterStimmenNichtUeberein');
$TextKonserven[]=array('Meldung','PersonalNrNichtGueltig');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$Portal = new awis_Portalzugaenge();

	$TXT_Speichern = $Form->LadeTexte($TextKonserven);

	$AWIS_KEY1 = $_POST['txtPZU_KEY'];
$Form->DebugAusgabe(1,$_POST);
	//***********************************************************************************
	//** Portalzugaenge
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPZU_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY1 = $_POST['txtPZU_KEY'];

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler='';
		if($_POST['txtPZU_BENUTZERKENNWORT1']!==$_POST['txtPZU_BENUTZERKENNWORT2'])
		{
			$Fehler .= $TXT_Speichern['Meldung']['KennwoerterStimmenNichtUeberein'];
		}

		if(($AWISBenutzer->HatDasRecht(2100)&16)==0)
		{
			$SQL = 'SELECT *';
			$SQL .= ' FROM Personal_komplett';
			$SQL .= ' WHERE PERSNR = '.$DB->FeldInhaltFormat('N0',$_POST['txtPZU_PERSNR']);
			$SQL .= ' AND GEB_DATUM = '.$DB->FeldInhaltFormat('D',$_POST['txtPZU_GEBDATUM']);
			$rsPER = $DB->RecordSetOeffnen($SQL);
			if($rsPER->EOF())
			{
				$Fehler .= $TXT_Speichern['Meldung']['PersonalNrNichtGueltig'];
			}
		}

		$rsPZU = $DB->RecordSetOeffnen('SELECT * FROM Portalzugaenge WHERE PZU_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
		if($rsPZU->EOF())
		{
			$PflichtfelderTest = array('PZU_PERSNR','PZU_GEBDATUM','PZU_BENUTZERNAME','PZU_EMAIL');
			$Pflichtfelder= array();
			foreach($PflichtfelderTest as $Feld)
			{
				if(isset($_POST['txt'.$Feld]))
				{
					$Pflichtfelder[] = $Feld;
				}
			}
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PZU'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
				//$Kennwort = crypt($_POST['txtPZU_BENUTZERKENNWORT1'],'$2a$07$AWIS'.strtr(microtime(),'. ','po').'AWIS$');
			$Kennwort = $Portal->KennwortVerschluesseln($_POST['txtPZU_BENUTZERKENNWORT1']);
			$SQL = 'INSERT INTO Portalzugaenge';
			$SQL .= '(PZU_PERSNR,PZU_GEBDATUM,PZU_BENUTZERNAME,PZU_KENNWORT,PZU_EMAIL,PZU_SPERRE,PZU_NEWSLETTER';
			$SQL .= ',PZU_USER,PZU_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPZU_PERSNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtPZU_GEBDATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPZU_BENUTZERNAME'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$Kennwort,true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPZU_EMAIL'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPZU_SPERRE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPZU_NEWSLETTER'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200909151906);
			}
			$SQL = 'SELECT seq_PZU_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
		}
		else
		{
			$PflichtfelderTest = array('PZU_PERSNR','PZU_GEBDATUM');
			$Pflichtfelder= array();
			foreach($PflichtfelderTest as $Feld)
			{
				if(isset($_POST['txt'.$Feld]))
				{
					$Pflichtfelder[] = $Feld;
				}
			}
			
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PZU'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
				
			$KennwortAblauf = '';
			$Kennwort = '';
			if($_POST['oldPZU_BENUTZERKENNWORT1']!=$_POST['txtPZU_BENUTZERKENNWORT1'])
			{
				$Kennwort = $Portal->KennwortVerschluesseln($_POST['txtPZU_BENUTZERKENNWORT1']);
				$KennwortAblauf = date('d.m.Y');
			}

			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]) AND substr($FeldName,0,20)!='PZU_BENUTZERKENNWORT')
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPZU->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPZU->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPZU->FeldInfo($FeldName,'TypKZ'),$rsPZU->FeldInhalt($FeldName),true);

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPZU->FeldInhalt('PZU_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='' OR $Kennwort!='')
			{
				$SQL = 'UPDATE Portalzugaenge SET';
				$SQL .= substr($FeldListe,1);
				if($Kennwort!='')
				{
					$SQL .= ($FeldListe!=''?',':'').' PZU_KENNWORT='.$DB->FeldInhaltFormat('T',$Kennwort,false);
					$SQL .= ', PZU_KENNWORTABLAUF = SYSDATE';
				}
				else
				{
					$SQL .= ', PZU_KENNWORTABLAUF = null';
				}
				$SQL .= ', PZU_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PZU_userdat=sysdate';
				$SQL .= ' WHERE PZU_key=0' . $_POST['txtPZU_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',808181117,$SQL,2);
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>