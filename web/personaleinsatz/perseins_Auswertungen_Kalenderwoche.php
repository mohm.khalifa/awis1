<?php
global $con;
global $awisRSZeilen;
global $RechteStufe;

require_once 'fpdi.php';

		$Summen = array();	
		$RasterZeile=9999;

		$SQL = "select MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0) AS MEAKEY, MEA_BEZEICHNUNG, DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, to_char(MEI_TAG,'RRRR') AS Jahr, to_char(MEI_TAG,'IW') AS Monat, COUNT(*) AS Anzahl, MIT_KEY ";
		$SQL .= ' FROM Mitarbeiter INNER JOIN MitarbeiterEinsaetze ON MIT_KEY = MEI_MIT_KEY';
		$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
		$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';
		$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
		$SQL .= ' WHERE MEA_KEY <> 9';
		$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

		if($_POST['txtDatumVom']!='')		//txtDatumVom
		{
			$SQL .= " AND MEI_TAG>=TO_DATE('" . $_POST['txtDatumVom'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtDatumBis']!='')		//txtDatumBis
		{
			$SQL .= " AND MEI_TAG<=TO_DATE('" . $_POST['txtDatumBis'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtFIL_ID']!='')		//txtFIL_ID
		{
			$SQL .= " AND MEI_FIL_ID=" . $_POST['txtFIL_ID'] . "";
		}	
		if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
		{
			$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
		}	
		if($_POST['txtMEI_MIT_KEY']!='-1')
		{
			$SQL .= " AND MEI_MIT_KEY=" . $_POST['txtMEI_MIT_KEY'] . "";
		}
		if($_POST['txtMIT_MTA_ID']!='-1')
		{
			$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
		}
		
		if(($RechteStufe&2)==2)	// Einschr�nkung Technik-�bersicht
		{
			$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
		}
		$SQL .= " GROUP BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'IW'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
		$SQL .= " HAVING to_char(MEI_TAG,'IW') <= 26";
		$SQL .= " ORDER BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'IW'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
	
		awis_Debug(1,$SQL);
		
		$rsMEI = awisOpenRecordset($con, $SQL );
		$rsMEIZeilen = $awisRSZeilen;


		$pdf = new fpdi('l','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
		$ATULogo = $pdf->ImportPage(1);				

		if($rsMEIZeilen==0)
		{
			$pdf->addpage();
			$pdf->SetFont('Arial','',10);
			$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
		}
		
		$RasterZeile=9999;
		$LetzterMitarbeiter = '';
		$LetztesJahr = '';
		$LetztesREZ='';
		$LetztesMEA='';
		
		for($i=0;$i<$rsMEIZeilen;$i++)
		{
				// Seitenwechsel?
			if($LetztesJahr!=$rsMEI['JAHR'][$i] OR $LetztesREZ!=$rsMEI['REZ_BEZEICHNUNG'][$i] OR $LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
			{
				$LetztesJahr=$rsMEI['JAHR'][$i];
				$LetzterMitarbeiter=$rsMEI['MIT_KEY'][$i];
				$LetztesREZ=$rsMEI['REZ_BEZEICHNUNG'][$i];
				$LetztesMEA='';

				$RasterZeile = 9999;	// Neue Seite erzwingen
			}

				// Neue Seite beginnen
			if($RasterZeile > 190)	
			{
				foreach($Summen AS $Pos=>$Wert)
				{					
					$pdf->setXY((27*6)+100,$Pos-10);
					$pdf->cell(20,6,$Wert,1,0,'R',0);
					//$pdf->setXY((13*12)+100,$Pos-10);
				}
				$Summen = array();		// Neue Summen

				$pdf->addpage();
				$pdf->useTemplate($ATULogo,270,4,20);

				$pdf->SetFont('Arial','',6);
				$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);
	
				$RasterZeile = 10;
				$pdf->SetFont('Arial','',20);
				$pdf->text(10,$RasterZeile,"Filialbesuche vom " . $_POST['txtDatumVom'] . ' bis ' . $_POST['txtDatumBis']);
				$RasterZeile = 40;

				$pdf->setXY(270,12);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);
	
				$pdf->SetFillColor(240,240,240);
				$pdf->SetDrawColor(0,0,0);
				$pdf->setXY(10,$RasterZeile-10);
	
				$pdf->SetFont('Arial','',12);
				$pdf->cell(96,6,$rsMEI['MIT_BEZEICHNUNG'][$i] . ' (' . $rsMEI['REZ_BEZEICHNUNG'][$i] .')',1,0,'L',1);
	
				$pdf->SetFont('Arial','',10);
				for($Std=1;$Std<=26;$Std++)
				{
					$pdf->setXY(($Std*6)+100,$RasterZeile-10);
					$pdf->SetDrawColor(0,0,0);
					$pdf->cell(6,6,$Std,1,0,'C',1);
				}
				$pdf->setXY(($Std*6)+100,$RasterZeile-10);
				$pdf->SetDrawColor(0,0,0);
				$pdf->cell(20,6,'Gesamt',1,0,'C',1);
				
			}

					// Neue Zeile
			if($LetztesMEA != $rsMEI['MEAKEY'][$i])
			{
				if($RasterZeile>180)
				{
					$RasterZeile=9999;
					$i--;
					continue;
				}
				$pdf->setXY(10,$RasterZeile-4);
				$pdf->cell(96,6,$rsMEI['MEA_BEZEICHNUNG'][$i] . ' ' . $rsMEI['FILIALE'][$i],1,0,'L',1);
				$pdf->line(106,$RasterZeile-4,276,$RasterZeile-4);				
				$pdf->line(106,$RasterZeile+2,276,$RasterZeile+2);
				
				$RasterZeile+=6;
				$LetztesMEA = $rsMEI['MEAKEY'][$i];
			}
					// Auf den Monat positionieren
			$pdf->setXY(($rsMEI['MONAT'][$i]*6)+100,$RasterZeile-10);
			$pdf->cell(6,6,$rsMEI['ANZAHL'][$i],1,0,'C',0);

			//awis_Debug(1,$Summen[$RasterZeile]);
			if (isset($Summen[$RasterZeile]))
			{
				$Summen[$RasterZeile] += $rsMEI['ANZAHL'][$i];
			}
			else 
			{
				$Summen[$RasterZeile] = $rsMEI['ANZAHL'][$i];
			}
		}	// Ende for
		
		foreach($Summen AS $Pos=>$Wert)
		{
			$pdf->setXY((27*6)+100,$Pos-10);
			$pdf->cell(20,6,$Wert,1,0,'R',0);
		}
		$Summen = array();		// Neue Summen
		
		
		//2. Teil ab KW 27
		$SQL = "select MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0) AS MEAKEY, MEA_BEZEICHNUNG, DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, to_char(MEI_TAG,'RRRR') AS Jahr, to_char(MEI_TAG,'IW') AS Monat, COUNT(*) AS Anzahl, MIT_KEY ";
		$SQL .= ' FROM Mitarbeiter INNER JOIN MitarbeiterEinsaetze ON MIT_KEY = MEI_MIT_KEY';
		$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
		$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';
		$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
		$SQL .= ' WHERE MEA_KEY <> 9';
		$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

		if($_POST['txtDatumVom']!='')		//txtDatumVom
		{
			$SQL .= " AND MEI_TAG>=TO_DATE('" . $_POST['txtDatumVom'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtDatumBis']!='')		//txtDatumBis
		{
			$SQL .= " AND MEI_TAG<=TO_DATE('" . $_POST['txtDatumBis'] . "','DD.MM.RRRR')";
		}	
		if($_POST['txtFIL_ID']!='')		//txtFIL_ID
		{
			$SQL .= " AND MEI_FIL_ID=" . $_POST['txtFIL_ID'] . "";
		}	
		if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
		{
			$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
		}	
		if($_POST['txtMEI_MIT_KEY']!='-1')
		{
			$SQL .= " AND MEI_MIT_KEY=" . $_POST['txtMEI_MIT_KEY'] . "";
		}
		if($_POST['txtMIT_MTA_ID']!='-1')
		{
			$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
		}
		
		if(($RechteStufe&2)==2)	// Einschr�nkung Technik-�bersicht
		{
			$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
		}
		$SQL .= " GROUP BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'IW'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
		$SQL .= " HAVING to_char(MEI_TAG,'IW') >= 27";
		$SQL .= " ORDER BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'IW'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
	
		awis_Debug(1,$SQL);
		
		$rsMEI = awisOpenRecordset($con, $SQL );
		$rsMEIZeilen = $awisRSZeilen;
		
		//$pdf = new fpdi('l','mm','a4');
		//$pdf->open();
		//$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
		//$ATULogo = $pdf->ImportPage(1);				

		if($rsMEIZeilen==0)
		{
			$pdf->addpage();
			$pdf->SetFont('Arial','',10);
			$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
		}
		
		$RasterZeile=9999;
		$LetzterMitarbeiter = '';
		$LetztesJahr = '';
		$LetztesREZ='';
		$LetztesMEA='';
		
		for($i=0;$i<$rsMEIZeilen;$i++)
		{
				// Seitenwechsel?
			if($LetztesJahr!=$rsMEI['JAHR'][$i] OR $LetztesREZ!=$rsMEI['REZ_BEZEICHNUNG'][$i] OR $LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
			{
				$LetztesJahr=$rsMEI['JAHR'][$i];
				$LetzterMitarbeiter=$rsMEI['MIT_KEY'][$i];
				$LetztesREZ=$rsMEI['REZ_BEZEICHNUNG'][$i];
				$LetztesMEA='';

				$RasterZeile = 9999;	// Neue Seite erzwingen
			}

				// Neue Seite beginnen
			if($RasterZeile > 190)	
			{
				foreach($Summen AS $Pos=>$Wert)
				{
					$pdf->setXY((27*6)+100,$Pos-10);
					$pdf->cell(20,6,$Wert,1,0,'R',0);
					//$pdf->setXY((13*12)+100,$Pos-10);
				}
				$Summen = array();		// Neue Summen

				$pdf->addpage();
				$pdf->useTemplate($ATULogo,270,4,20);

				$pdf->SetFont('Arial','',6);
				$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);
	
				$RasterZeile = 10;
				$pdf->SetFont('Arial','',20);
				$pdf->text(10,$RasterZeile,"Filialbesuche vom " . $_POST['txtDatumVom'] . ' bis ' . $_POST['txtDatumBis']);
				$RasterZeile = 40;

				$pdf->setXY(270,12);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);
	
				$pdf->SetFillColor(240,240,240);
				$pdf->SetDrawColor(0,0,0);
				$pdf->setXY(10,$RasterZeile-10);
	
				$pdf->SetFont('Arial','',12);
				$pdf->cell(96,6,$rsMEI['MIT_BEZEICHNUNG'][$i] . ' (' . $rsMEI['REZ_BEZEICHNUNG'][$i] .')',1,0,'L',1);
	
				$pdf->SetFont('Arial','',10);
				for($Std=1;$Std<=26;$Std++)
				{
					$pdf->setXY(($Std*6)+100,$RasterZeile-10);
					$pdf->SetDrawColor(0,0,0);
					$pdf->cell(6,6,$Std+26,1,0,'C',1);
				}
				$pdf->setXY(($Std*6)+100,$RasterZeile-10);
				$pdf->SetDrawColor(0,0,0);
				$pdf->cell(20,6,'Gesamt',1,0,'C',1);
				
			}

					// Neue Zeile
			if($LetztesMEA != $rsMEI['MEAKEY'][$i])
			{
				if($RasterZeile>180)
				{
					$RasterZeile=9999;
					$i--;
					continue;
				}
				$pdf->setXY(10,$RasterZeile-4);
				$pdf->cell(96,6,$rsMEI['MEA_BEZEICHNUNG'][$i] . ' ' . $rsMEI['FILIALE'][$i],1,0,'L',1);
				$pdf->line(106,$RasterZeile-4,276,$RasterZeile-4);				
				$pdf->line(106,$RasterZeile+2,276,$RasterZeile+2);
				
				$RasterZeile+=6;
				$LetztesMEA = $rsMEI['MEAKEY'][$i];
			}
					// Auf den Monat positionieren
			//awis_Debug(1, $rsMEI['MONAT']);					
			//awis_Debug(1, $rsMEI['ANZAHL']);					
			$pdf->setXY((($rsMEI['MONAT'][$i]-26)*6)+100,$RasterZeile-10);
			$pdf->cell(6,6,$rsMEI['ANZAHL'][$i],1,0,'C',0);

			if (isset($Summen[$RasterZeile]))
			{
				$Summen[$RasterZeile] += $rsMEI['ANZAHL'][$i];
			}
			else 
			{
				$Summen[$RasterZeile] = $rsMEI['ANZAHL'][$i];
			}
		}		// Ende for
		foreach($Summen AS $Pos=>$Wert)
		{
			$pdf->setXY((27*6)+100,$Pos-10);
			$pdf->cell(20,6,$Wert,1,0,'R',0);
		}
		$Summen = array();		// Neue Summen
		

?>