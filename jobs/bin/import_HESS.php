<?php
require_once('/daten/include/awisZukauf_HESS.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
	//$Job = new awisJobProtokoll(5, 'Starte HESS - Import.');

	$ZUK = new awisZukauf_HESS((isset($argv[1])?(int)$argv[1]:0),false);

        //$Job->SchreibeLog(3,1,'Suche offene Bestellungen...');

	if($ZUK->LeseBestellungen((isset($argv[2])?$argv[2]:0),'','','')>0)
	{
        	//$Job->SchreibeLog(3,1,'Speichere Bestellungen...');
		$ZUK->SpeichereBestellungen();
	}

        //$Job->SchreibeLog(1,0,'Ende des Jobs.');
}
catch (Exception $ex)
{
        echo $ex->getMessage().PHP_EOL;
        echo $ex->getLine().PHP_EOL;
        echo $ex->getFile().PHP_EOL;
	//if(is_object($Job))
	{
	        //$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
	}
}
?>
