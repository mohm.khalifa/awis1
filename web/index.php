<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="SHORTCUT ICON" href="./favicon.ico">
<meta http-equiv="content-type" content="text/html; charset=ISO-8852-15">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisBenutzer.inc');
try
{
    $AWISBenutzer = awisBenutzer::Init();
}
catch (Exception $ex)
{
    echo "<link rel=stylesheet type=text/css href=/css/awis.css>";
    echo '</head>';
    echo '<body>';
        
    echo '<div class=FehlerBox>';
    echo '<div class=FehlerBoxUeberschrift>';
    echo 'Es ist ein Problem aufgetreten.';
    echo '</div>';
    echo '<div class=FehlerBoxKaefer>';
    for($i=0;$i<9;$i++)
    {
        echo '<img alt=BUG title="Problem" src="/bilder/totenkopf.ico">';
    }
    echo '</div>';
    echo '<div class=FehlerBoxText>';
    echo 'Initialisierung des Benutzers ist fehlgeschlagen. Bitte an den Helpdesk wenden.';
    echo '</div>';
    echo '<div class=FehlerBoxText>';
    echo $ex->getMessage();
    echo '</div>';
    die();
}



$Version = $AWISBenutzer->ParameterLesen('Startseiten-Version');

//$Version=3;

if($Version==1)
{
	echo '<title>Awis - ATU webbasierendes Informationssystem</title>';
	echo '</head>';
	echo '<html>';
	echo '<body>';

	require_once("db.inc.php");		// DB-Befehle
	require_once("register.inc.php");
	require_once("sicherheit.inc.php");
									// Benutzerdefinierte CSS Datei
	print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
	include ("ATU_Header.php");	// Kopfzeile

	$con = awisLogon();
	        // Beginn Hauptmen�
	print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Hauptmen�</h1></td><td align=right>Anmeldename: " . $AWISBenutzer->BenutzerName() . "</td></tr></table>";



	        // �berschrift der Tabelle
	print "<table border=0 width=100%><tr><th id=FeldBez></th><th align=left id=FeldBez>Programmpunkt</th><th align=left id=FeldBez>Beschreibung</th></tr>";

	if(awisBenutzerRecht($con,3000)>0)
	{
		print "<tr><td></td><td><a href=./adressen/adressen_Main.php accesskey=n>Adresse<u>n</u></a></td><td>Adressenverwaltung.</td></tr>";
	}

	//if(awisBenutzerRecht($con,400)>0)
	//{
	//	print "<tr><td></td><td><a href=./ATUArtikel/artikel_Main.php accesskey=u>A.T.<u>U</u> Artikel</a></td><td>Informationen zu den ATU Artikeln.</td></tr>";
	//}

	if(awisBenutzerRecht($con,450)>0)
	{
		print "<tr><td></td><td><a href=./artikelstamm/artikelstamm_Main.php accesskey=r>A.T.<u>U</u> Artikel</a></td><td>Informationen zu den ATU Artikeln.</td></tr>";
	}

	// In das Men� darf jeder. Rechte werden im n�chsten Men� entschieden. --> Weg 16.04.2008 TR
	if((awisBenutzerRecht($con,502)>0) or (awisBenutzerRecht($con,200)>0) or (awisBenutzerRecht($con,505)>0)
		or (awisBenutzerRecht($con,500)>0) or (awisBenutzerRecht($con,501)>0) or (awisBenutzerRecht($con,507)>0)
		or (awisBenutzerRecht($con,540)>0)or (awisBenutzerRecht($con,506)>0)
		or ((awisBenutzerRecht($con,150) & 16)==16) or (awisBenutzerRecht($con,407)>0))
		{
			print "<tr><td></td><td><a href=./auswertungen/auswertungen_Main.php>Auswertungen</a></td><td>Auswertungen aus der Datenbank.</td></tr>";
		}

	if(awisBenutzerRecht($con,3300))
	{
	    print "<tr><td></td><td><a href=./schaden/schaden_Main.php>Beschwerdeverwaltung</a></td><td>Beschwerde-Verwaltung.</td></tr>";
	}

	if(awisBenutzerRecht($con,3700)>0)
	{
	    print "<tr><td></td><td><a href=./crm/crm_Main.php accesskey=C><u>C</u>RM</a></td><td>CRM System.</td></tr>";
	}

	if(awisBenutzerRecht($con,4001)>0)
	{
		print "<tr><td></td><td><a href=./datenexporte/datenexporte_Main.php>Datenexporte</a></td><td>Daten aus AWIS Exportieren.</td></tr>";
	}

	if(awisBenutzerRecht($con,120)>0)
	{
	    print "<tr><td></td><td><a href=./dienstleistungspreise/dienstleistungspreise_Main.php accesskey=P>Dienstleistungs<u>p</u>reise</a></td><td>Dienstleistungspreise f�r Leistungen der Filialen anzeigen.</td></tr>";
	}
	if(awisBenutzerRecht($con,100))
	{
	    print "<tr><td></td><td><a href=./filialinfos/filialinfos_Main.php>Filialinfo</a></td><td>Informationen f�r Filialen anzeigen.</td></tr>";
	}
	if(awisBenutzerRecht($con,9100))
	{
	    print "<tr><td></td><td><a href=./firstglas/firstglas_Main.php>First Glass</a></td><td>Scheibenaustausch</td></tr>";
	}
	if(awisBenutzerRecht($con,3500))
	{
	    print "<tr><td></td><td><a href=./gewinnspiel/gewinnspiel_Main.php>Gewinnspiel</a></td><td>Gewinnfrage zu ATU-TV.</td></tr>";
	}
	if(awisBenutzerRecht($con,1400))
	{
	    print "<tr><td></td><td><a href=./hifireklamationen/hifireklamationen_Main.php>HiFi-Reklamationen</a></td><td>Reklamationen HiFi.</td></tr>";
	}
	if(awisBenutzerRecht($con,1100))
	{
	    print "<tr><td></td><td><a href=./HilfsUndBetriebsstoffe/hibe_Main.php>Hilfs- und Betriebsstoffe</a></td><td>Verwaltung der Hilfs- und Betriebsstoffe in den Filialen.</td></tr>";
	}
	if(awisBenutzerRecht($con,1200)>0)
	{
		print "<tr><td></td><td><a href=./infos/infos_Main.php>Informationen</a></td><td>Allgemeine Informationen.</td></tr>";
	}
	/*
	if(awisBenutzerRecht($con,1800))
	{
	    print "<tr><td></td><td><a href=./kassentexte/kassentexte_Main.php>Kassentexte</a></td><td>Problembest&auml;nde aus den Kassentexten.</td></tr>";
	}
	*/
	if(awisBenutzerRecht($con,3200))
	{
	print "<tr><td></td><td><a href=http://reifentraining.de/atu45C5E5D2C71E66277B1B8073087FECF718032006>Michelin-WBT</a></td><td>Web-Based-Training der Firma Michelin. Nur in Citrix-Umgebung (ICA/IGEL)</td></tr>";
	}
	if(awisBenutzerRecht($con,3010))
	{
	    print "<tr><td></td><td><a href=./mitbewerber/mitbewerber_Main.php>Mitbewerber</a></td><td>Mitbewerber der Filialen.</td></tr>";
	}
	if(awisBenutzerRecht($con,300))
	{
	    print "<tr><td></td><td><a href=./nachlaesse/nachlaesse_Main.php>Nachl�sse</a></td><td>Automatische und manuelle Nachl�sse.</td></tr>";
	}
	/*if(awisBenutzerRecht($con,301))
	{
		print "<tr><td></td><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=nachlass_pflege_" .
		$AWISBenutzer->BenutzerName() . "&form=nachlass_pflege>Nachlasspflege</a></td><td>Testversion der Nachlasspflege.</td></tr>";
	}*/
	if(awisBenutzerRecht($con,4600)>0)
	{
	    print "<tr><td></td><td><a href=./filialtaetigkeiten/index.php>Operative Filialaufgaben</a></td><td>Durchf�hren von operativen Filialaufgaben.</td></tr>";
	}
	if(awisBenutzerRecht($con,1300)>0)
	{
	    print "<tr><td></td><td><a href=./personaleinsatz/perseins_Main.php>Personaleins�tze</a></td><td>Eins�tze des Personals in den Filialen.</td></tr>";
	}
	if(awisBenutzerRecht($con,4500)>0)
	{
	    print "<tr><td></td><td><a href=./personaleinsaetze/personaleinsaetze_Main.php>Personaleins�tze (neu)</a></td><td>Neue Personalein&auml;tze.</td></tr>";
	}
	if(awisBenutzerRecht($con,1600))
	{
	    print "<tr><td></td><td><a href=./personalverkauf/persvk_Main.php>Personalverkauf</a></td><td>Personalverkauf.</td></tr>";
	}
	if(awisBenutzerRecht($con,2000))
	{
	    print "<tr><td></td><td><a href=./preislisten/preislisten_Main.php>Preislisten</a></td><td>Pr�fung von Preislisten.</td></tr>";
	}
	if(awisBenutzerRecht($con,800)>0)
	{
	    print "<tr><td></td><td><a href=./redaktionssystem/redaktionssystem_Main.php>Redaktionssystem</a></td><td>Redaktionssystem f�r Filialsystem.</td></tr>";
	}
	if(awisBenutzerRecht($con,250))
	{
	    print "<tr><td></td><td><a href=./reifen/reifen_Main.php>Reifen</a></td><td>Reifensuche.</td></tr>";
	}
	//if(awisBenutzerRecht($con,3900))
	//{
	//   print "<tr><td></td><td><a href=./rueckfuehrung/index.php>R&uuml;ckf&uuml;hrung</a></td><td>R&uuml;ckf&uuml;hrung von Ware.</td></tr>";
	//}
	if(awisBenutzerRecht($con,1500))
	{
	    print "<tr><td></td><td><a href=./schluessel/schluessel_Main.php>Schl&uuml;ssel</a></td><td>Schl&uuml;ssel.</td></tr>";
	}
	if(awisBenutzerRecht($con,1700))
	{
	    print "<tr><td></td><td><a href=./sonderausweis/sonderausweis_Main.php>Sonderausweise</a></td><td>Sonderausweise.</td></tr>";
	}
	if(awisBenutzerRecht($con,700))
	{
		// entfernt S.K 19.06.07
	//    if($_SERVER["HTTP_HOST"]=='atlx22su91') //Testserver
	//        print "<tr><td></td><td><a href=\"./card/rls/card_frameset.php\">RLS</a></td><td>ATU-Card-Rechnungskunden</td></tr>";
	//    else
	        print "<tr><td></td><td><a href=\"http://rls.server.atu.de/card/rls/card_frameset.php\">RLS</a></td><td>ATU-Card-Rechnungskunden</td></tr>";
	}

	if(awisBenutzerRecht($con,150))
	{
	//	print "<tr><td></td><td><a target=NeuesFenster href=http://atux04hp/atu/telefon.htm>Telefonverzeichnis</a></td><td>Telefonverzeichnis der ATU</td></tr>";
		print "<tr><td></td><td><a href=./telefon/telefon_Main.php>Telefonverzeichnis</a></td><td>Telefonverzeichnis der ATU</td></tr>";
	}

	if(awisBenutzerRecht($con,9500))
	{
	    print "<tr><td></td><td><a href=versicherungen/vorgangsbearbeitung/versicherungen_Main.php>Versicherungen</a></td><td>Onlineversicherungen</td></tr>";
	}

	if(awisBenutzerRecht($con,10000))
	{
		print "<tr><td></td><td><a href=./zukauf/index.php>Zukauf</a></td><td>Verwaltung des Teilezukaufs.</td></tr>";
	}

	if(awisBenutzerRecht($con,1000)>0)
	{
		//print "<tr><td></td><td><a href=http://ecws01wi03.server.atu.de>Zum ATU Internet...</a></td><td>ATU Internetauftritt.</td></tr>";
		print "<tr><td></td><td><a href=http://www.atu.eu>Zum ATU Internet...</a></td><td>ATU Internetauftritt. Nur in Citrix-Umgebung (ICA/IGEL)</td></tr>";
	}

		/*********************************************************/
		// Programmparameter und sonstige Einstellungen
		/*********************************************************/
	//echo "<tr><td colspan=3><hr></td></tr>";
	echo '<tr><td>&nbsp;</td></tr>';

	if ((awisBenutzerRecht($con,1)) or (awisBenutzerRecht($con,10)) or (awisBenutzerRecht($con,22)>0)
		 or (awisBenutzerRecht($con,1900)) or (awisBenutzerRecht($con,3100)) or (awisBenutzerRecht($con,5))
		 or (awisBenutzerRecht($con,20)>0) or (awisBenutzerRecht($con,6)>0) or (awisBenutzerRecht($con,600)>0)
		 or (awisBenutzerRecht($con,1001)>0) or (awisBenutzerRecht($con,10)>0) or (awisBenutzerRecht($con,9000)))
		 {
			echo "<tr><th id=FeldBez></th><td id=FeldBez colspan=1>Administration</td></tr>";
		 }
	//echo "<tr><td colspan=3><hr></td></tr>";
	if(awisBenutzerRecht($con,1) or awisBenutzerRecht($con,10))
	{
	    print "<tr><td></td><td><a href=./admin/index.php>Administration</a></td><td>Verwaltung von Benutzern und Rechten, Server, ...</td></tr>";
	}
	if(awisBenutzerRecht($con,22)>0)
	{
		print "<tr><td></td><td><a href=./feldnamen/feldnamen_Main.php>Feldbezeichnungen</a></td><td>Anpassen der Feldbezeichnungen f�r Datenfelder.</td></tr>";
	}
	if(awisBenutzerRecht($con,1900))
	{
		print "<tr><td></td><td><a href=./helpdesk/helpdesk_Main.php>Helpdesk</a></td><td>Funktionen f�r den Helpdesk.</td></tr>";
	}
	if(awisBenutzerRecht($con,3100))
	{
		print "<tr><td></td><td><a href=./operating/operating_Main.php>Operating</a></td><td>Operatingoberfl&auml;che f&uuml;r die ATU - ORACLE-Datenbanken.</td></tr>";
	}
	if(intval(PHP_VERSION)<5)
	{
		if (substr($AWISBenutzer->BenutzerName(),0,4)!='fil-')
		{
			if(awisBenutzerRecht($con,5))
			{
				print "<tr><td></td><td><a href=./KennwortAendern.php>Kennwort �ndern</a></td><td>�ndern eines Kennworts. Beachten Sie die neuen <a href=/kennwortrichtlinien.pdf style=\"text-decoration:underline;\">Kennwortrichtlinien</a> vom 26.08.2003</td></tr>";
			}
		}
	}
	if(awisBenutzerRecht($con,6)>0)
	{
		print "<tr><td></td><td><a href=./BenutzerParameter.php>Parameter</a></td><td>Anpassen der Programmparameter.</td></tr>";
	}
	if(awisBenutzerRecht($con,20)>0)
	{
		print "<tr><td></td><td><a href=./admin/rechte_Main.php>Rechte�bersicht</a></td><td>Anzeige der aktuellen Programmrechte.</td></tr>";
	}
	if(awisBenutzerRecht($con,6)>0)
	{
		echo "<tr><td></td><td><a href=./ShortCutLeiste.php>Shortcut Leiste</a></td><td>Konfiguration der AWIS Shortcut Leiste.</td></tr>";
	}

	if(awisBenutzerRecht($con,600)>0)	// Allgmeines Recht setzen!
	{
		print "<tr><td></td><td><a href=./stammdaten/stammdaten_Main.php>Stammdaten pflegen</a></td><td>Pflege der Stammdaten, z.B. Hersteller.</td></tr>";
	}

		/*********************************************************/
		// Internet-Zugang und Allg. Informationen
		/*********************************************************/
	//print "<tr><td colspan=3><hr></td></tr>";

	if(awisBenutzerRecht($con,1001)>0)
	{
		echo '<tr><td>&nbsp;</td></tr>';
		echo "<tr><th id=FeldBez></th><td id=FeldBez colspan=1>TEST-Systeme</td></tr>";

	//	print "<tr><td colspan=3><hr></td></tr>";
		print "<tr><td></td><td><a href=http://atutest.awis.atu.de/index.php>AWIS Testsystem</a></td><td>AWIS Testsystem mit neuen, noch nicht freigegebenen Funktionen.</td></tr>";
	}

	if(awisBenutzerRecht($con,10)>0)
	{
		if($_SERVER['REMOTE_ADDR=']="125.0.9.191" or $_SERVER['REMOTE_ADDR']=="125.0.7.173")
		{
			print "<tr><td></td><td><a href=http://atlx22su81/test/latextest.php>Test</a></td><td>Latex-Testseite</td></tr>";
			print "<tr><td></td><td><a href=http://atlx22su81/test/pwdtest.php>PWD-Test</a></td><td>Testseite</td></tr>";
		}
	}

	if(awisBenutzerRecht($con,9000))
	{
		print "<tr><td></td><td><a href=./beispiel/beispiel_Main.php>Beispiel</a></td><td>Erstellen einer Beispielseite fuer die Dokumentation</td></tr>";
	}


	print "</table>";	//Ende Hauptmen�


	// Fenster schlie�en
	//print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=self.close();>";
	if(awisBenutzerRecht($con,10)>0)
	{
	 include("debug_info.php");
	}
}
elseif($Version==2)
{
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    
    global $AWISCursorPosition;		// Aus AWISFormular
    
    try
    {
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        $AWISBenutzer = awisBenutzer::Init();
        echo '<link rel="stylesheet" type="text/css" href="' . $AWISBenutzer->CSSDatei() .'">';
    }
    catch (Exception $ex)
    {
        die($ex->getMessage());
    }
    
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('TITEL','tit_Hauptmenue');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Hauptmenue'].' - V2</title>';
    
    echo '</head>';
    echo '<body>';
    include ("awisHeader.inc");	// Kopfzeile
    
    try
    {
        $Form = new awisFormular();
    
        $Menue = new awisMenue(1);
        $Menue->ErstelleMenueSeite();
    
        if($AWISBenutzer->HatDasRecht(10)>0)
        {
            include("debug_info.php");
        }
    }
    catch (Exception $ex)
    {
        if($Form instanceof awisFormular)
        {
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
        }
        else
        {
            echo 'AWIS: '.$ex->getMessage();
        }
    }
}
elseif($Version==3)
{
	require_once('awisDatenbank.inc');
	require_once('awisBenutzer.inc');
	require_once('awisFormular.inc');

	global $AWISCursorPosition;		// Aus AWISFormular

	try
	{
		$DB = awisDatenbank::NeueVerbindung('AWIS');
		$DB->Oeffnen();
		$AWISBenutzer = awisBenutzer::Init();
		echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) .">";
	}
	catch (Exception $ex)
	{
		die($ex->getMessage());
	}

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TITEL','tit_Hauptmenue');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');

	$Form = new awisFormular();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Hauptmenue'].'</title>';

	echo '</head>';
	echo '<body>';
	include ("awisHeader3.inc");	// Kopfzeile

	try
	{
		
		$Menue = new awisMenue(1);
		$Menue->ErstelleMenueSeite();
		
		$Form->awisInfotafel(1,"Etwas",2);
		$Form->awisInfotafel(1,"Zweiter Bereich",1);
		$Form->awisInfotafel(1,"Dritter Bereich",1);
		$Form->awisInfotafel(1,"Vier",1);
		$Form->awisInfotafel(1,"F&uuml;nf",1);
		$Form->awisInfotafel(1,"Und die ganze Breite",3);
		
		if($AWISBenutzer->HatDasRecht(10)>0)
		{
		    $Form->SchreibeHTMLCode('<div class=HMBereichDebug>');
		    include("debug_info.php");
		    $Form->SchreibeHTMLCode('</div>');
		}
	}
	catch (Exception $ex)
	{
		if($Form instanceof awisFormular)
		{
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
		}
		else
		{
			echo 'AWIS: '.$ex->getMessage();
		}
	}
}
?>
</body>
</html>