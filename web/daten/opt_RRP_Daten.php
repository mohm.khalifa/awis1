<?php
//**************************************************************
// Liefert alternative Artikelbezeichnung
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['RRP_REH_KEY']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_BREITE']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_QUERSCHNITT']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_INNENDURCHMESSER']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_LOADINDEX']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_SPEEDINDEX']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_ZUSATZBEMERKUNG']))
{
	die('##PARAM##');
}
if(!isset($_GET['RRP_BEZEICHNUNG']))
{
	die('##PARAM##');
}


try
{
	$OptionArray = array();
	
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$SQL = 'SELECT REH_KURZBEZEICHNUNG FROM REIFENHERSTELLER';
	$SQL .= ' WHERE REH_KEY='.$DB->FeldInhaltFormat('N0',$_GET['RRP_REH_KEY']);
	$rsREH = $DB->RecordSetOeffnen($SQL);
	
	$DS = 1;
	$OptionArray[$DS] = str_pad(trim($rsREH->FeldInhalt('REH_KURZBEZEICHNUNG')),9," ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_BREITE'], 3, " ", STR_PAD_LEFT).'/';
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_QUERSCHNITT'], 3, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad('R', 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_INNENDURCHMESSER'], 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_LOADINDEX'], 3, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_SPEEDINDEX'], 2, " ",STR_PAD_RIGHT);
	
	$DS = 2;
	$OptionArray[$DS] = str_pad(trim($rsREH->FeldInhalt('REH_KURZBEZEICHNUNG')),9," ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_BREITE'], 3, " ", STR_PAD_LEFT).'/';
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_QUERSCHNITT'], 3, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad('ZR', 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_INNENDURCHMESSER'], 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_LOADINDEX'], 3, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_SPEEDINDEX'], 2, " ",STR_PAD_RIGHT);
	
	$DS = 3;
	$OptionArray[$DS] = str_pad(trim($rsREH->FeldInhalt('REH_KURZBEZEICHNUNG')),9," ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_BREITE'], 3, " ", STR_PAD_LEFT).'/';
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_QUERSCHNITT'], 3, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad('R', 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_INNENDURCHMESSER'], 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_LOADINDEX'], 3, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_SPEEDINDEX'], 2, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .trim($_GET['RRP_ZUSATZBEMERKUNG']).' ';
	$OptionArray[$DS] = $OptionArray[$DS] .trim($_GET['RRP_BEZEICHNUNG']).' ';
	
	$DS = 4;
	$OptionArray[$DS] = str_pad(trim($rsREH->FeldInhalt('REH_KURZBEZEICHNUNG')),9," ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_BREITE'], 3, " ", STR_PAD_LEFT).'/';
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_QUERSCHNITT'], 3, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad('ZR', 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_INNENDURCHMESSER'], 2, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_LOADINDEX'], 3, " ",STR_PAD_LEFT);
	$OptionArray[$DS] = $OptionArray[$DS] .str_pad($_GET['RRP_SPEEDINDEX'], 2, " ",STR_PAD_RIGHT);
	$OptionArray[$DS] = $OptionArray[$DS] .trim($_GET['RRP_ZUSATZBEMERKUNG']).' ';
	$OptionArray[$DS] = $OptionArray[$DS] .trim($_GET['RRP_BEZEICHNUNG']).' ';
	
	
	if (isset($_GET['WERT']))
	{
		echo $_GET['WERT'];
	}
//	echo $SQL.'#~#';		// Trennzeichen
	echo '#~#';		// Trennzeichen
	
	$DS=0;
	if(isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ'] != '')
	{
		$Zusatz = explode('~', urldecode($_GET['ZUSATZ']));
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
		}
		else
		{
			echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
		}
	}
	if (empty($OptionArray))
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="">::--::</option>';
		}
		else
		{
			echo '#+##+#::--::';
		}
	}
	
	//echo '#~#';		// Trennzeichen zum Kennzeichen der Daten
	$DS=0;	
	foreach($OptionArray AS $OptionText)
	{
		if(++$DS>200)
		{
			if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
			{
				echo '<option value="">...</option>';
			}
			else
			{
				echo '#+##+#...';
			}
			break;
		}
	
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$OptionText.'">'.$OptionText.'</option>';
		}
		else
		{
			echo '#+#'.$OptionText.'#+#'.$OptionText.'';
		}
	}
}
catch(Exception $ex)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="-1">::Zugriffsfehler::</option>';
	}
	else
	{
		echo '#+#-1#+#::Zugriffsfehler::';
	}
}

// Auswahlliste in Abhängigkeit der Rechte anzeigen
/* 
$SQL = 'SELECT DISTINCT LVK_KEY AS KEY, LVK_ORT || \', \' || LVK_STRASSE AS ANZEIGE ';
$SQL .= ' FROM LIEFERANTENVERKAUFSHAEUSER';
$SQL .= ' WHERE LVK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_GET['LIE_NR']);
$SQL .= ' AND ((LVK_DATUMVOM <= SYSDATE AND LVK_DATUMBIS >= SYSDATE)';
$SQL .= ' OR LVK_KEY = '.$DB->FeldInhaltFormat('N0',$_GET['WERT'],false).')';
$SQL .= ' ORDER BY 2';

$rsLVK = $DB->RecordSetOeffnen($SQL);
 */

/* 
if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsLVK->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		echo '<option value="'.$rsLVK->FeldInhalt('KEY').'">'.$rsLVK->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsLVK->FeldInhalt('KEY').'#+#'.$rsLVK->FeldInhalt('ANZEIGE').'';
	}

	$rsLVK->DSWeiter();
}
*/
?>