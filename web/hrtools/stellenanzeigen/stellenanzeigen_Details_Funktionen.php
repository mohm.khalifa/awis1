<?php
//var_dump($_POST);
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;        // Aus AWISFormular
global $Form;
global $AWISBenutzer;

$Form = new awisFormular();

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

//Vorschau erstellen
function erstelleAjaxLiveAnsicht()
{
    global $AWISCursorPosition;        // Aus AWISFormular
    global $Form;
    global $AWISBenutzer;

    //String aus GET holen und decoden
    $HTML = ersetzeUmlaute($_GET['sucajaxBox1']);
    $HTML = rawurldecode($HTML);

    //CSS f�r die Vorschau (wie auf atu.de, stand 04.03.2016)
    $jobCSS = "<style type=\"text/css\">
				.job_dyn_description * {
					margin: 0;
					padding: 0;
					color: #333;
					font-size: 12px;
					font-family: 'Arial', 'helvetica', sans-serif;
					border: 0
				}
				
				.job_dyn_description ul {
				    padding-left: 15px !important;
				}
				</style>";
    echo $jobCSS;

    //HTML-Tags einf�gen und leere Boxen ausfiltern
    $AufgabenText = "<p><strong>Diese Aufgaben bringen Sie auf Hochtouren</strong></p>";
    $ProfilText = "<p><strong>Mit diesem Profil machen Sie das Rennen</strong></p>";
    $mitte = strpos($HTML, '*BOXMITTE*');
    if ($mitte == 0) {
        $AufgabenText = "";
    }

    if (strlen(str_replace('*BOXMITTE*', '', $HTML)) == $mitte) {
        $ProfilText = "";
    }

    if (($AufgabenText != "") and ($ProfilText != "")) {
        $HTML = $AufgabenText . "<ul><li>" . str_replace("*BOXMITTE*", "</li></ul><p>&nbsp;</p>" . $ProfilText . "<ul><li>", $HTML) . "</li></ul>";
    } elseif (($AufgabenText == "") and ($ProfilText != "")) {
        $HTML = $ProfilText . str_replace("*BOXMITTE*", "<ul><li>", $HTML) . "</li></ul>";
    } elseif (($AufgabenText != "") and ($ProfilText == "")) {
        $HTML = $AufgabenText . "<ul><li>" . str_replace("*BOXMITTE*", "</li></ul>", $HTML);
    } else {
        $HTML = "";
    }

    $HTML = str_replace("<br />", "</li><li>", $HTML);
    $HTML = str_replace('<li></li>', '', $HTML); //Leere l�schen

    $DIVstart = '<div class="job_dyn_description"><div class="job_dyn_description_left" style="float: left; width: 380px; margin-bottom: 20px;">';
    $DIVmitte = '</div><div class="job_dyn_description_middle" style="width: 47px !important; min-height: 1px; display: block; float: left; "></div><div class="job_dyn_description_right" style="float: left; width: 380px; margin-bottom: 20px;">';
    $DIVende = '</div></div>';

    //Ausgabe zusammenfummeln
    $HTML = $DIVstart . $HTML . $DIVende;

    //Vorschau zeigen
    $Form->SchreibeHTMLCode('<div name="VorschauDiv">');
    $Form->Erstelle_TextLabel('', 30); //Platzhalter
    $Form->Erstelle_TextLabel($HTML, 600);
    $Form->SchreibeHTMLCode("</div>");
}

//Code-Ausgabe erstellen
//&#13;&#10; == Zeilenumbruch in TextArea
function erstelleAjaxHTMLCodeAusgabe()
{
    global $AWISCursorPosition;        // Aus AWISFormular
    global $Form;
    global $AWISBenutzer;

    //String aus GET holen und decoden
    $HTML = ersetzeUmlaute($_GET['sucajaxBox2']);
    $HTML = rawurldecode($HTML);

    //HTML zusammensetzen und leere Boxen abfangen
    $AufgabenText = "<p><strong>Diese Aufgaben bringen Sie auf Hochtouren</strong></p>&#13;&#10;";
    $ProfilText = "<p><strong>Mit diesem Profil machen Sie das Rennen</strong></p>&#13;&#10;";
    $mitte = strpos($HTML, '*BOXMITTE*');
    if ($mitte == 0) {
        $AufgabenText = "";
    }

    if (strlen(str_replace('*BOXMITTE*', '', $HTML)) == $mitte) {
        $ProfilText = "";
    }

    if (($AufgabenText != "") and ($ProfilText != "")) {
        $HTML = $AufgabenText . "<ul><li>" . str_replace("*BOXMITTE*", "</li></ul>&#13;&#10;<p>&amp;nbsp;</p>&#13;&#10;" . $ProfilText . "<ul><li>", $HTML) . "</li></ul>";
    } elseif (($AufgabenText == "") and ($ProfilText != "")) {
        $HTML = $ProfilText . str_replace("*BOXMITTE*", "<ul><li>", $HTML) . "</li></ul>";
    } elseif (($AufgabenText != "") and ($ProfilText == "")) {
        $HTML = $AufgabenText . "<ul><li>" . str_replace("*BOXMITTE*", "</li></ul>", $HTML);
    } else {
        $HTML = "";
    }

    $HTML = str_replace("<br />", "</li>&#13;&#10;<li>", $HTML);
    $HTML = str_replace('<li></li>', '', $HTML); //Leere l�schen

    //Ausgeben
    $Form->Trennzeile('O');
    $Form->ZeileStart();

    $Form->Erstelle_TextLabel('', 100);
    $Form->Erstelle_Textarea('HTMLCodeAusgabe', $HTML, 600, 70, 10, true, 'width: 807px', '', '', 'readonly');
    $Form->ZeileEnde();
}

/**
 * @param string  $html
 * @param string  $startTag
 * @param string  $startErsatz
 * @param string  $endTag
 * @param string  $endErsatz
 * @param boolean $liste
 * @return string
 *
 * @throws -1 Wenn Formatierung falsch
 */
function ersetzeHTML($html, $startTag, $startErsatz, $endTag = "", $endErsatz = "", $liste = false, $listenTrenner = "<br />")
{
    //Variablen f�llen
    if ($endTag == "") {
        $endTag = $startTag;
    }
    if ($endErsatz == "") {
        $endErsatz = $startErsatz;
    }

    $startAmount = substr_count($html, $startTag);
    $endAmount = substr_count($html, $endTag);

    //Wenn keine gerade Anzahl an Tags
    if (($startAmount <> $endAmount) or ($startTag == $endTag and ($startAmount & 1) == 1)) {
        return -1;
    } //Tags ersetzen
    elseif ($startAmount > 0) {
        $j = 0;
        if ($endErsatz <> $startErsatz or $endTag <> $startTag) {
            for ($i = 1; $i <= $startAmount + $endAmount; $i++) {
                if (($j & 1) == 0) {
                    $html = preg_replace('/' . $startTag . '/', $startErsatz, $html, 1);
                    $j += 1;
                } else {
                    $html = preg_replace('/' . $endTag . '/', $endErsatz, $html, 1);
                    $j += 1;
                }
            }
        } else {
            $html = str_replace($startTag, $startErsatz, $html);
        }

        //Wenn Liste, dann <li></li> dazufummeln
        if ($liste) {
            $htmlArray = explode($listenTrenner, $html);
            $html = "";
            $i = 0;
            $inListe = false;
            foreach ($htmlArray as $line) {
                if (strpos($line, $startErsatz) !== false) {
                    $inListe = true;
                } elseif (strpos($line, $endErsatz) !== false) {
                    $inListe = false;
                    $line = "<li>" . str_replace("</ul>", "</li></ul>", $line);
                }

                if ($inListe) {
                    $line = str_replace("<br />", "", $line);
                    if (strpos($line, $startErsatz) !== false) {
                        $line = str_replace("<ul>", "<ul><li>", $line) . "</li>";
                    } else {
                        $line = "<li>" . $line . "</li>";
                    }
                }

                $html .= $line . $listenTrenner;
            }
        }
    }

    return $html;
}

/**
 * @param string $string
 * @return string
 */
function ersetzeUmlaute($string)
{
    //URL-Codierung ist schei�e
    $string = str_replace("%C3%A4", '�', $string);
    $string = str_replace("%C3%B6", '�', $string);
    $string = str_replace("%C3%BC", '�', $string);
    $string = str_replace("%C3%84", '�', $string);
    $string = str_replace("%C3%96", '�', $string);
    $string = str_replace("%C3%9C", '�', $string);
    $string = str_replace("%C3%9F", '�', $string);
    $string = str_replace("%E2%80%93", '-', $string);
    $string = str_replace("%EF%82%A7%09", '', $string);
    $string = str_replace("o%09", '', $string);

    return $string;
}

?>