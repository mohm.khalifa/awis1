<?php
// require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;				// UnfallMeldungenInfoTypen: UFT_BEREICHID
global $AWIS_KEY2;				// UnfallMeldungenInfoTypen: UFT_KEY
global $Aktion;					// Merker/Wert f�r GET: cmdAktion (Reiter)
global $Seite;					// Merker/Wert f�r GET: Seite (Subreiter)
global $ListeAnzeigen;			// Flag: Liste anzeigen (nicht Detailansicht)

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21040 = $AWISBenutzer->HatDasRecht(21040);		// Stammdaten
	
	if($Recht21040 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}

	$Aktion = (isset($_GET['cmdAktion'])? $_GET['cmdAktion']: '');
	$Seite = (isset($_GET['Seite'])? $_GET['Seite']: '');
	
	$Form->SchreibeHTMLCode('<form name=frmUnfallStammdaten action=./unfall_Main.php?cmdAktion=' . $Aktion . '&Seite=' . $Seite . ' method=POST enctype="multipart/form-data">');
	
	if (isset($_POST['cmdSpeichern_x']))
	{
		include('./unfall_speichern_stammdaten.php');
	}
	elseif (isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']) or isset($_POST['cmdLoeschenAbbrechen']))
	{
		include('./unfall_loeschen_stammdaten.php');
	}
	
	$Register = new awisRegister(21040);
	$Register->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));	

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht21040 & 2) >= 2)
	and (isset($_GET['New']) or isset($_GET['Edit']) 
	or isset($_POST['txtNEW']) or isset($_POST['txtEDIT']))
	and (! $ListeAnzeigen))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');	
	}
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>