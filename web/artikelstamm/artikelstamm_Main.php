<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">

    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');

    global $AWISCursorPosition;        // Aus AWISFormular

    try {
        $AWISBenutzer = awisBenutzer::Init();
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() . ">";
        echo '<script src="/jquery_ie11.js" type="text/javascript"></script>';
        echo '<script src="/js/jquery-ui.js" type="text/javascript"></script>';
    } catch (Exception $ex) {
        die($ex->getMessage());
    }

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('TITEL', 'tit_Artikelstamm');
    $TextKonserven[] = array('TITEL', 'pop_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_reset');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');

    $Form = new AWISFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    echo '<title>' . $AWISSprachKonserven['TITEL']['tit_Artikelstamm'] . '</title>';
    ?>
</head>
<body>
<script src="/jquery_ie11.js"></script>
<script src="/jquery.scrollUp.min.js"></script>
<script src="/popup.js"></script>
<script src="/formularBereich.js"></script>
<script src="/jquery.tooltipster.js"></script>
<script src="/jquery-ui.js"></script>
<script>
    function setzeFarbe() {
        return true;
    }
</script>
<link rel=stylesheet type=text/css href="/css/jquery-ui.css">
<link rel=stylesheet type=text/css href="/css/tooltipster.css">
<link rel=stylesheet type=text/css href="/css/tooltipster-punk.css">
<link rel=stylesheet type=text/css href="/css/popup.css">

<?php
include("awisHeader.inc");    // Kopfzeile

try {
    $Form = new awisFormular();
    //Leeres PopUp f�r die Seite erzeugen
    $Form->PopupDialog('', '', array(array('', $AWISSprachKonserven['TITEL']['pop_zurueck'], '', '')), 1, 'MasterPopUp');
    if ($AWISBenutzer->HatDasRecht(450) == 0) {
        $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200809161548");
        die();
    }

    $Register = new awisRegister(450);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    if ($AWISCursorPosition !== '') {
        $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
        $Form->SchreibeHTMLCode("document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();");
        $Form->SchreibeHTMLCode('</Script>');
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    } else {
        echo 'AWIS: ' . $ex->getMessage();
    }
}

?>
</body>
</html>