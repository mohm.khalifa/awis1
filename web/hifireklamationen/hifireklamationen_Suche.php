<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;
global $con;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php

$Rechtestufe = awisBenutzerRecht($con, 1400);
		//  1=Einsehen
		//	2=Bearbeiten
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Reklamtionen_Hifi',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

echo "<form name=frmHifireklamationen method=post action=./hifireklamationen_Main.php?cmdAktion=Liste>";

// Tabelle aufbauen
echo "<div>";
echo "<table width=100% id=DatenTabelle border=1><tr>";
echo '<tr><td width=250 ><b>Bearbeitungsnummer</b></td>';
echo '<td><input type=text name=txtBearbeitungsnummer size=30></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>RMA-Nummer</b></td>';
echo '<td><input type=text name=txtRMA size=30></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>ATU Filial-Nummer</b></td>';
echo '<td><input type=text name=txtFilialNr size=10></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>ATU Artikel-Nummer</b></td>';
echo '<td><input type=text name=txtArtikelNr size=10></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>Ger&auml;te-Nr / Serien-Nr</b></td>';
echo '<td><input type=text name=txtGeraetenummer size=30></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>Ger&auml;tebez. / -typ</b></td>';
echo '<td><input type=text name=txtGeraetetyp size=30></td>';
echo '</tr>';
echo '<tr><td width=250 ><b>Ger&auml;teeigent&uuml;mer</b></td>';
echo '<td><input type=text name=txtEigentuemer size=30></td>';
echo '</tr>';
echo "</table>";
echo "</div>";

//print "<input type=hidden name=cmdAktion value=ArtikelInfos>";
print "<hr><br>&nbsp;<input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren>";
print "&nbsp;<img accesskey=r tabindex=99 src=/bilder/radierer.png alt='Formularinhalt l�schen (Alt+r)' name=cmdReset onclick=location.href='./hifireklamationen_Main.php?Reset=True';>";

print "</form>";

// Cursor in das erste Feld setzen
print "<Script Language=JavaScript>";
print "document.getElementsByName(\"txtBearbeitungsnummer\")[0].focus();";
echo '</SCRIPT>';

?>
</body>
</html>