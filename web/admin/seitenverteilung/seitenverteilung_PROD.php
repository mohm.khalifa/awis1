<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<script src="seitenverteilung.js"></script>

<?php
//var_dump($_POST);
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular


$Form = new awisFormular();

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Form->Formular_Start();
$Form->SchreibeHTMLCode('<form name=frmVerteilenPROD method=post action=./seitenverteilung_Main.php?cmdAktion=PRODing>');

if(($AWISBenutzer->HatDasRecht(8)&2) != 2)
{
    $Form->DebugAusgabe(1,'Keine Rechte');
    $Form->Fehler_KeineRechte();
}


$AW = new awisWerkzeuge();
if($AW->awisLevel()!=awisWerkzeuge::AWIS_LEVEL_ENTWICK)
{
	$Form->Hinweistext('Abgleich nur vom Entwicklungssystem aus m�glich!',awisFormular::HINWEISTEXT_HINWEIS);
	die;
}

$Form->ZeileStart();
$Form->Hinweistext('Abgleich ins <b>Testsystem</b>',awisFormular::HINWEISTEXT_HINWEIS);
$Form->ZeileEnde();
$Form->Trennzeile('L');



if(isset($_POST['cmdVerteilen_x']))
{
    system("/bin/rm /daten/webdaten/logs/unison_PROD.log",$Erg);
    
    if($Erg==0)
    {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Abgleich wurde beauftragt.',0);
        $Form->ZeileEnde();
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Fortschritt: ', 100);
    $Form->Erstelle_TextFeld('Prozent', '0', 3, 50, true,'','','','','','','','','','','disabled');
    $Form->Erstelle_TextLabel('% ', 100);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->SchreibeHTMLCode('<div id="ladebalken" style="display: block; background: red; width: 0px;">&nbsp;</div>');
    $Form->ZeileEnde();
    
}


if(file_exists('/daten/webdaten/logs/unison_PROD.log'))
{
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Der letzte Abgleich wurde durchgef&uuml;hrt. Kontrollieren Sie Ihr Ergebnis.',0);
    $Form->ZeileEnde();
   

    $Form->Trennzeile('L');
    
    $Form->ZeileStart();
    echo '<table border=0 width=100%>';
    echo '<tr><td><b>Zeile</b></td><td><b>Protokoll</b></td></tr>';

    $LogDatei = file('/daten/webdaten/logs/unison_PROD.log');
    
    foreach($LogDatei as $ZeileNr => $ZeilenInhalt)
    {
        echo '<tr>';
        echo '<td width=50 align=right>' . $ZeileNr . ':</td>';

        If(strstr($ZeilenInhalt,'CONFLICT') OR strstr($ZeilenInhalt,'<-?->'))
        {
            echo '<td ><font color=#FF0000><b>' . $ZeilenInhalt . '</b></font></td>';
        }
        elseif(strstr($ZeilenInhalt,"local") AND strstr($ZeilenInhalt,"ap-"))
        {
            echo '<td ><font size=4 color=#0000FF><b>' . $ZeilenInhalt . '</b></font></td>';
        }
        elseif(strstr($ZeilenInhalt,'new file --') OR strstr($ZeilenInhalt,'changed  --'))
        {
            echo '<td ><font size=3 color=#00CC00><b>' . $ZeilenInhalt . '</b></font></td>';
        }
        elseif(strstr($ZeilenInhalt,'deleted ')!='')
        {
            echo '<td ><font size=3 color=#FFFF00><b>' . $ZeilenInhalt . '</b></font></td>';
        }
        else
        {
            echo '<td >' . $ZeilenInhalt . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
    $Form->ZeileEnde();
    
}


$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png','Zur&uuml;ck','Z');

if(!isset($_POST['cmdVerteilen_x'])) //W�hrend der Verteilung keine neue starten.
{
	$Form->Schaltflaeche('image', 'cmdVerteilen', '', '/bilder/cmd_verteilen.png', 'Verteilung starten / aktualisieren', 'N');
}

$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form>');

//Muss nach Formende stehen, da ansonsten "document" in Javascript noch nicht existiert. 
if(isset($_POST['cmdVerteilen_x']))
{
	echo '  <script type="text/javascript">animiere()</script>';
}

?>