<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $AWIS_KEY1;

$EingabeFeld='';		// Zum Cursor setzen

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Adressen','ADR_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Adressen','txtAdresstypen');
$TextKonserven[]=array('Adressen','txtAZG_OEFFENTLICH');
$TextKonserven[]=array('Adressen','txtNeuerAdressenTyp');
$TextKonserven[]=array('Adressen','txtAnredenListe');

$TXT_Baustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


$Recht3000 = awisBenutzerRecht($con,3000);
if($Recht3000==0)
{
	die('Keine Rechte');
}


if(isset($_POST['cmdLoeschen_x']) OR isset($_POST['cmdLoeschenOK']) or isset($_GET['DelADI']))
{
	include('./adressen_loeschen.php');
}

//********************************************************
// Parameter ?
//********************************************************
//awis_Debug(1,$_POST);
if(isset($_POST['cmdSuche_x']))
{
	$Param = '0';									//ADR KEY
	$Param .= ';' . $_POST['sucSuchName'];			// Namen
	$Param .= ';' . $_POST['sucSuchKontakt'];		// Kontaktinfo
	$Param .= ';' . $_POST['sucADR_AZG_KEY'];		// Gruppe

	awis_BenutzerParameterSpeichern($con, "AdressenSuche" , $_SERVER['PHP_AUTH_USER'] , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuelleAdresse" , $_SERVER['PHP_AUTH_USER'] , 0);
}
			// Neuen Datensatz anlegen
elseif(($Recht3000&2) == 2 AND (isset($_GET['DSNeu']) OR isset($_POST['cmdDSNeu_x'])))
{
	$Param = '-1;;;';
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./adressen_speichern.php');
	$Param = $AWIS_KEY1.';;;';
}
elseif(isset($_GET['ADR_KEY']))
{
	$Param = ''.$_GET['ADR_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$AWIS_KEY1=0;
	if(!isset($_GET['Liste']))
	{
		$AWIS_KEY1 = awis_BenutzerParameter($con, "AktuelleAdresse" , $_SERVER['PHP_AUTH_USER']);
	}
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, 'AdressenSuche', $_SERVER['PHP_AUTH_USER']);
		awis_BenutzerParameterSpeichern($con, "AktuelleAdresse" , $_SERVER['PHP_AUTH_USER'],0);
	}
	else
	{
		$Param = $AWIS_KEY1.';;;;';
	}

}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

if($Param[0]!='' AND $Param[0]!=0 )			// Eine bestimmte Adresse
{
	$Bedingung .= 'AND ADR_KEY = ' . $Param[0] . ' ';
}

// Name angegeben?
if($Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(ADR_Name1) ' . awisFeldFormat('T',$Param[1],'DB',false,'UPPER,LIKE');
	$Bedingung .= 'OR  UPPER(ADR_Name2) ' . awisFeldFormat('T',$Param[1],'DB',false,'UPPER,LIKE').' ';

	$Bedingung .= 'OR  UPPER(AAP_Nachname) ' . awisFeldFormat('T',$Param[1],'DB',false,'UPPER,LIKE') . ' ';
	$Bedingung .= 'OR  UPPER(AAP_Vorname) ' . awisFeldFormat('T',$Param[1],'DB',false,'UPPER,LIKE') . ' ';
	$Bedingung .= ')';
}

if($Param[2]!='')
{
	$Bedingung .= 'OR  UPPER(AKO_Wert) ' . awisFeldFormat('T',$Param[2],'DB',false,'UPPER,LIKE') . ' ';
}

if($Param[3]!='' AND $Param[3]!='0')			// Nur f�r eine Gruppe
{
	$Bedingung .= 'AND ADR_AZG_KEY = ' . $Param[3] . ' ';
}

$SQL = 'SELECT DISTINCT Adressen.*, azg_bezeichnung';
$SQL .= ' FROM Adressen';
$SQL .= ' LEFT OUTER JOIN ADRESSENANSPRECHPARTNER ON ADR_Key = AAP_ADR_Key';
$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADR_AZG_Key = AZG_Key';
$SQL .= ' LEFT OUTER JOIN ADRESSENKONTAKTE K1 ON ADR_Key = AKO_XXX_Key';
$SQL .= ' LEFT OUTER JOIN ADRESSENKONTAKTE K2 ON AAP_Key = AKO_XXX_Key';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}
if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY ADR_Name1, ADR_Name2';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$_SERVER['PHP_AUTH_USER']);
$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ROWNUM <='.$MaxDSAnzahl;

$rsADR = awisOpenRecordset($con, $SQL);
$rsADRZeilen = $awisRSZeilen;
//awis_Debug(1,$SQL,$Param);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsADRZeilen==0 AND $Param[2]!=-1)		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Adressen gefunden.</span>';
}											//********************
elseif($rsADRZeilen>1)						// Liste anzeigen
{											//********************

	awis_FORM_FormularStart();


	awis_FORM_ZeileStart();
	$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=ADR_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_NAME1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Adressen']['ADR_NAME1'],350,'',$Link);
	$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=ADR_NAME2'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_NAME2'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Adressen']['ADR_NAME2'],200,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Adressen']['ADR_STRASSE'],250);
	$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=ADR_PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_PLZ'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Adressen']['ADR_PLZ'],100,'',$Link);
	$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=ADR_ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_ORT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Adressen']['ADR_ORT'],200,'',$Link);
	awis_FORM_ZeileEnde();

	if(!$rsADRZeilen>$MaxDSAnzahl)
	{
		$rsADRZeilen=$MaxDSAnzahl;
	}
	for($ADRZeile=0;$ADRZeile<$rsADRZeilen;$ADRZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './adressen_Main.php?cmdAktion=Details&Seite=Infos&ADR_KEY='.$rsADR['ADR_KEY'][$ADRZeile].'';
		awis_FORM_Erstelle_ListenFeld('ADR_NAME1',$rsADR['ADR_NAME1'][$ADRZeile],0,350,false,($ADRZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('ADR_NAME2',$rsADR['ADR_NAME2'][$ADRZeile],0,200,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('ADR_STRASSE',$rsADR['ADR_STRASSE'][$ADRZeile].' '.$rsADR['ADR_HAUSNUMMER'][$ADRZeile],0,250,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('ADR_PLZ',$rsADR['ADR_LAN_CODE'][$ADRZeile].'-'.$rsADR['ADR_PLZ'][$ADRZeile],0,100,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('ADR_ORT',$rsADR['ADR_ORT'][$ADRZeile],0,200,false,($ADRZeile%2),'','');
		awis_FORM_ZeileEnde();
	}


	awis_FORM_FormularEnde();
}											//****************************************
else										// Eine einzelne oder neue Adresse
{											//****************************************
	echo '<form name=frmAdressen action=./adressen_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';

	awis_BenutzerParameterSpeichern($con, "AktuelleAdresse" , $_SERVER['PHP_AUTH_USER'] , $rsADR['ADR_KEY'][0]);
	$AWIS_KEY1=$rsADR['ADR_KEY'][0];

	echo '<input type=hidden name=txtADR_KEY value='.$rsADR['ADR_KEY'][0] . '>';
	echo '<input type=hidden name=txtAdressTypen value=1>';	// Firma oder Privat

	awis_FORM_FormularStart();

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$TXT_Baustein['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADR['ADR_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADR['ADR_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');


	awis_FORM_ZeileStart();

	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_ANREDE'],150);
	$Anreden = explode(";",$TXT_Baustein['Adressen']['txtAnredenListe']);
	awis_FORM_Erstelle_SelectFeld('ADR_ANREDE',$rsADR['ADR_ANREDE'][0],200,(($Recht3000&6)!=0),$con,'','','','','',$Anreden,'');
	$EingabeFeld='txtADR_ANREDE';
	awis_FORM_ZeileEnde();


	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_KONZERN'],150);
	awis_FORM_Erstelle_TextFeld('ADR_KONZERN',$rsADR['ADR_KONZERN'][0],20,300,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_NAME1'],150);
	awis_FORM_Erstelle_TextFeld('ADR_NAME1',$rsADR['ADR_NAME1'][0],50,350,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_NAME2'],150);
	awis_FORM_Erstelle_TextFeld('ADR_NAME2',$rsADR['ADR_NAME2'][0],50,350,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_POSTFACH'],150);
	awis_FORM_Erstelle_TextFeld('ADR_POSTFACH',$rsADR['ADR_POSTFACH'][0],10,100,(($Recht3000&6)!=0));
	awis_FORM_Erstelle_TextLabel('&nbsp;'.$TXT_Baustein['Adressen']['ADR_POSTFACHPLZ'],120);
	awis_FORM_Erstelle_TextFeld('ADR_POSTFACHPLZ',$rsADR['ADR_POSTFACHPLZ'][0],10,200,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_STRASSE'].'/'.$TXT_Baustein['Adressen']['ADR_HAUSNUMMER'].':',150);
	awis_FORM_Erstelle_TextFeld('ADR_STRASSE',$rsADR['ADR_STRASSE'][0],50,330,(($Recht3000&6)!=0));
	awis_FORM_Erstelle_TextFeld('ADR_HAUSNUMMER',$rsADR['ADR_HAUSNUMMER'][0],10,200,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_LAN_CODE'].'/'.$TXT_Baustein['Adressen']['ADR_PLZ'].'/'.$TXT_Baustein['Adressen']['ADR_ORT'].':',150);
	$SQL = 'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND';
	awis_FORM_Erstelle_SelectFeld('ADR_LAN_CODE',$rsADR['ADR_LAN_CODE'][0],-40,(($Recht3000&6)!=0),$con,$SQL,false,($rsADR['ADR_LAN_CODE'][0]==''?'DE':''),'LAN_CODE', '');
	awis_FORM_Erstelle_TextFeld('ADR_PLZ',$rsADR['ADR_PLZ'][0],8,80,(($Recht3000&6)!=0));
	awis_FORM_Erstelle_TextFeld('ADR_ORT',$rsADR['ADR_ORT'][0],50,300,(($Recht3000&6)!=0));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_BEMERKUNG'],150);
	awis_FORM_Erstelle_Textarea('ADR_BEMERKUNG',$rsADR['ADR_BEMERKUNG'][0],300,80,5,(($Recht3000&6)!=0),'border-style=solid;');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Adressen']['ADR_AZG_KEY'].':',150);
	$SQL = 'SELECT AZG_KEY, KAB_ABTEILUNG || \' - \' || AZG_BEZEICHNUNG AS Bereich';
	$SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
	$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
	$SQL .= ' INNER JOIN KONTAKTEABTEILUNGEN ON AZG_KAB_KEY=KAB_KEY';
	$SQL .= ' WHERE AZM_XBN_Key=0' . awisBenutzerID();
	$SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
	$SQL .= ' ORDER BY AZG_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ADR_AZG_KEY',$rsADR['ADR_AZG_KEY'][0],200,true,$con,$SQL,'','','','','','');
	//awis_FORM_Erstelle_SelectFeld('SuchGruppe','',300,true,$con,$SQL,false,'','','',array(array('','0',$AWISSprachKonservenSuche['Wort']['Auswahl_ALLE'])));
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['RegisterSeite'])?$_POST['RegisterSeite']:'Infos'));

	awis_RegisterErstellen(3001, $con, $RegisterSeite);
	echo '<hr>';

	if(($Recht3000&6)!==0)
	{
		echo "&nbsp;<input accesskey=s type=image src=/bilder/diskette.png title='".$TXT_Baustein['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
	}
	if(($Recht3000&8)!==0)
	{
		echo "&nbsp;<input accesskey=x type=image src=/bilder/Muelleimer_gross.png title='".$TXT_Baustein['Wort']['lbl_loeschen']." (ALT+X)' name=cmdLoeschen>";
	}
	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsADR, $_POST, $rsAZG, $SQL, $AWISSprache);

if($EingabeFeld!=='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$EingabeFeld."\")[0].focus();";
	echo '</Script>';
}

?>