<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisDIT.php';

class awisZwangsabschluss
{
    /**
     * @var int
     */
    protected $_DebugLevel = 0;

    /**
     * @var String
     */
    protected $_DebugText = "";

    /**
     * @var int
     */
    protected $_Ersteller = 0;

    /**
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisFormular
     */
    protected $_Form;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var awisDIT
     */
    protected $_DIT;

    /**
     * AwisSprachKonserven
     */
    protected $_AWISSprachKonserven;

    /**
     * Fehlercounter
     */
    private $_ERROR_Counter = 0;

    /**
     * awisZwangsabschluss constructor
     * @param $Benutzer
     */
    function __construct($Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_Form = new awisFormular();
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_DIT = new awisDIT($this->_Benutzer,$this->_DB,'LKK');
        $this->_Werkzeuge = new awisWerkzeuge();
        $TextKonserven = array();
        $TextKonserven[]=array('LKK','LKK_ZWANGSABSCHLUSS%');
        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);
    }


    /**
     * Setzt und returnt das Debuglevel
     *
     * @param int $Level
     * @return int
     */
    public function DebugLevel($Level = 0)
    {
        if ($Level != 0) {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Setzt und returnt den Ersteller
     *
     * @param int $Ersteller
     * @return int
     */
    public function setzeErsteller($Ersteller = 0)
    {
        if ($Ersteller != 0) {
            $this->_Ersteller = $Ersteller;
        }

        return $this->_Ersteller;
    }

    /**
     * F�hrt die komplette Verarbeitung aus und schreibt bei Fehlern eine ERROR-Mail
     */
    public function verarbeitung()
    {
        $this->erstelleDITTask();
        $this->schliesseDITTask();
        if($this->_ERROR_Counter > 0){
            $this->schreibeERRORMail();
        }
    }


    /**
     * Erstelle DIT Tasks einen Tag vor dem Zwangsabschluss f�r alle Lieferkontrollen, die noch aktiv sind
     *
     * Tasks f�r Samstag, Sonntag und Montag werden am Freitag erstellt -> Kein Durchlauf am Samstag und Sonntag
     */
    public function erstelleDITTask()
    {
        if(date('N') <= 5) { //Wenn Montag-Freitag
            $this->debugAusgabe("Bin in der erstelleDITTask-Funktion!", 10);
            $SQL = "";
            $SQL .= "SELECT LKK_KEY,LKK_FIL_ID,LKK_LIEFERSCHEINNR FROM Lieferkontrollenkopf WHERE ";
            if (date('N') == 5) { //Wochentag = Freitag -> Tasks f�r Samstag und Montag
                $SQL .= " LKK_DATUMKOMM <= (sysdate - 4) ";
            } else {
                $SQL .= " LKK_DATUMKOMM <= (sysdate - 6) ";
            }
            $SQL .= " and LKK_DATUMKOMM >= (sysdate - 7) ";
            $SQL .= " and LKK_STATUS = 'O'";
            $SQL .= " and LKK_FIL_ID NOT IN (49,95)"; //Franchise-Filialen sollen keine DIT-Tickets bekommen laut Hr. Gleissner

            $rsLKK = $this->_DB->RecordSetOeffnen($SQL);

            if ($rsLKK->AnzahlDatensaetze() > 0) {
                $this->debugAusgabe("Habe Lieferkontrollen kurz vorm Zwangsabschluss gefunden! Anzahl: " . $rsLKK->AnzahlDatensaetze(), 99);
                $i = 0;
                while (!$rsLKK->EOF()) {

                    $this->debugAusgabe("Erstelle jetzt einen Task fuer die Filiale: " . $rsLKK->FeldInhalt('LKK_FIL_ID'), 99);
                    $XBN_KEY = $this->_DIT->holeFilialXBN($rsLKK->FeldInhalt('LKK_FIL_ID'));
                    if ($XBN_KEY > 0) {
                        $TaskId = $this->_DIT->ErstelleTask($this->_AWISSprachKonserven['LKK']['LKK_ZWANGSABSCHLUSS_TITEL'], $XBN_KEY, $this->ersetzePlatzhalter($this->_AWISSprachKonserven['LKK']['LKK_ZWANGSABSCHLUSS_BESCHREIBUNG'], $rsLKK->FeldInhalt('LKK_KEY')), date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), 1, 0, $this->_Ersteller, $rsLKK->FeldInhalt('LKK_KEY'));
                        if($TaskId == null){
                            $this->_ERROR_Counter++;
                            $this->debugAusgabe("Konnte Task nicht erstellen! LKK_KEY: ".$rsLKK->FeldInhalt('LKK_KEY'), 99);
                        }else{
                            $this->debugAusgabe("Habe Task erstellt!", 99);
                            $i++;
                        }
                    } else {
                        $this->debugAusgabe("Konnte keine Kontaktperson fuer Filiale " . $rsLKK->FeldInhalt('LKK_FIL_ID') . " finden!", 10);
                    }

                    $rsLKK->DSWeiter();
                }
                if($i>0) {
                    $this->debugAusgabe("Habe Tasks erstellt! Anzahl: " . $i, 10);
                } else {
                    $this->debugAusgabe("Habe versucht Tasks zu erstellen habe es aber nicht geschafft!", 10);
                }
            } else {
                $this->debugAusgabe("Habe keine Tasks zum Erstellen", 10);
            }
        } else { //Wenn Samstag/Sonntag
            $this->debugAusgabe("Kein Durchlauf der erstelleDITTask-Funktion da Samstag oder Sonntag ist!", 10);
        }
    }


    /**
     * Schliesse DIT Task sobald Zwangsabschluss durchgef�hrt wurde
     */
    public function schliesseDITTask()
    {
        $this->debugAusgabe("Bin in der schliesseDITTask-Funktion!",10);

        $SQL = "Select DIL_TASKID from DITAPILOG ";
        $SQL .= " where DIL_XTN_KUERZEL = 'LKK' and DIL_STATUS = 1 and DIL_XXX_KEY in ( ";
        $SQL .= "  Select LKK_KEY from Lieferkontrollenkopf ";
        $SQL .= "  where LKK_DATUMKOMM <= sysdate - 7 ";
        $SQL .= " )";

        $rsLKK = $this->_DB->RecordSetOeffnen($SQL);

        if($rsLKK->AnzahlDatensaetze()>0){
            $this->debugAusgabe("Habe Tasks die abgeschlossen werden k�nnen gefunden! Anzahl: ".$rsLKK->AnzahlDatensaetze(),99);
            $i = 0;
            while(!$rsLKK->EOF()){
                $this->debugAusgabe("Schliesse jetzt TaskID ".$rsLKK->FeldInhalt('DIL_TASKID')." ab!",99);
                $Antwort = $this->_DIT->schliesseTaskAb($rsLKK->FeldInhalt('DIL_TASKID'));
                if($Antwort == "ok"){
                    $this->debugAusgabe("Habe Task abgeschlossen!",99);
                    $i++;
                } else{
                    $this->debugAusgabe("Habe Task versucht einen Task abzuschliessen aber es ging nicht. TaskID: ".$rsLKK->FeldInhalt('DIL_TASKID'),99);
                    if($Antwort != "error"){
                        $this->_DIT->updateTaskStatus($rsLKK->FeldInhalt('DIL_TASKID'));
                        $this->debugAusgabe("Habe aber den Taskstatus im AWIS geupdated da kein ERROR",99);
                    } else{
                        $this->_ERROR_Counter++;
                    }
                }
                $rsLKK->DSWeiter();
            }
            $this->debugAusgabe("Habe Tasks abgeschlossen! Anzahl: ".$i,10);
        } else {
            $this->debugAusgabe("Habe keine Tasks zum Schliessen gefunden!",10);
        }
    }


    /**
     * Ersetzt Platzhalter (bspw. #LKK_LINK#)
     *
     * @param string $Text
     * @param int $LKK_KEY
     * @return string $Text
     */
    private function ersetzePlatzhalter($Text = "",$LKK_KEY)
    {
        $SQL = "";
        $SQL .= "Select LKK_KEY,LKK_LIEFERSCHEINNR from Lieferkontrollenkopf where LKK_KEY =".$this->_DB->WertSetzen('LKK','Z',$LKK_KEY);

        $rsLKK = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('LKK'));

        if($Text != '') {
            $Text = str_replace('#LKK_LINK#','#LINK#/mde/lieferkontrolle/lieferkontrolle_Main.php?cmdAktion=Details&LKK_KEY='.$rsLKK->FeldInhalt('LKK_KEY'), $Text);
            $Text = str_replace('#LKK_LIEFERSCHEINNR#', $rsLKK->FeldInhalt('LKK_LIEFERSCHEINNR'), $Text);

            switch ($this->_Werkzeuge->awisLevel()) {
                case awisWerkzeuge::AWIS_LEVEL_ENTWICK:
                    $Text = str_replace('#LINK#', 'https://127.0.0.1:8080', $Text);
                    break;
                case awisWerkzeuge::AWIS_LEVEL_SHUTTLE:
                    $Text = str_replace('#LINK#', 'https://awis.server.atu.de', $Text);
                    break;
                case awisWerkzeuge::AWIS_LEVEL_STAGING:
                    $Text = str_replace('#LINK#', 'https://awis-staging.server.atu.de', $Text);
                    break;
                case awisWerkzeuge::AWIS_LEVEL_PRODUKTIV:
                default:
                    $Text = str_replace('#LINK#', 'https://awis.server.atu.de', $Text);
                    break;
            }
        }
        return $Text;
    }


    /**
     * Schreibt eine ERROR-Mail falls Fehler aufgetreten sind
     */
    private function schreibeERRORMail()
    {
        $this->debugAusgabe("Schreibe ERROR Mail",99);

        $this->_Werkzeuge->EMail($this->_Werkzeuge->LeseAWISParameter('awis.conf','FEHLER_MAIL_EMPFAENGER'), ("ERROR")." ZwangsabschlussDITTasks - DATUM " . date('Ymd'),
            $this->_DebugText, 2, '',
            'awis' . strval(($this->_Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) ? (awisWerkzeuge::AWIS_LEVEL_SHUTTLE) : (awisWerkzeuge::AWIS_LEVEL_ENTWICK)) . '@de.atu.eu');
    }


    /**
     * Debugausgabe ausgeben
     *
     * @param string $text
     * @param int $debugLevel
     */
    private function debugAusgabe($text, $debugLevel)
    {
        $this->_DebugText = $this->_DebugText.$text."\r\n";
        if ($this->_DebugLevel >= $debugLevel) {
            echo $text . ((substr($text, -(strlen(PHP_EOL))) == PHP_EOL)?'':PHP_EOL);
        }
    }

}