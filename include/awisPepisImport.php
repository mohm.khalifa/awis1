<?php
/**
 * Klasse fuer den Import von der Gebietszuordnungen aus Pepis f�r Smart-Repair-Mitarbeiter
 *
 *
 * @author Thomas Riedl
 * @version 20100318
 * @copyright ATU Auto Teile Unger
 *
 *
 */

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisPepisImport
{
	/**
	* Datenbankverbindung
	*
	* @var awisDatenbank
	*/
	private $_DB = null;		

	/**
	 * Aktueller Benutzer
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;

	/**
	 * Empf�nger f�r Mails
	 *
	 * @var string
	 */
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');

	public function __construct($Benutzer='')
	{	
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);		
	}
	
	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);	
		}
		
		return $this->_DebugLevel;
	}

	/**
	* Funktion ImportZuordnungen
	*
	*/
	public function ImportZuordnungen()
	{
		try
		{
			$Werkzeug = new awisWerkzeuge();
			
			awisWerkzeuge::EMail($this->_EMAIL_Infos,'OK IMPORT PEPISZUORDNUNGEN BEGINNT','',1,'','awis@de.atu.eu');
			
			//Alle Smart - Repair Mitarbeiter selektieren
			$SQL = ' SELECT * ';
			$SQL.= ' FROM PERSONAL ';
			$SQL.= ' INNER JOIN PERSONALINFOS ON PIF_PER_NR = PER_NR AND PIF_ITY_KEY = '.$this->_DB->WertSetzen("PER",'Z',12);
			$SQL.= ' WHERE PER_PET_ID IN ';
			$SQL.= ' (SELECT PET_ID FROM PERSONALTAETIGKEITEN ';
			$SQL.= ' WHERE PET_TAETIGKEIT = '. $this->_DB->WertSetzen('PER', 'T',"Smart-Repair-Lackmeister");
			$SQL.= ' OR PET_TAETIGKEIT = '. $this->_DB->WertSetzen('PER', 'T', "Smart-Repair-Monteur"). ")";
			$SQL.= ' ORDER BY PER_NR';


			$rsPER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
		
			$Neu=0;
			$Aenderung=0;
               		

			//Alle Mitarbeiter durchgehen
			while(!$rsPER->EOF())
			{		
				$Fehler = 'Zuordnung Personal: '.$rsPER->FeldInhalt('PER_NR').' '.$rsPER->FeldInhalt('PIF_WERT');
				file_put_contents('/var/log/awis/pepis.log',$Fehler."\n",FILE_APPEND);
			
				//Entsprechenden Kontakt im AWIS dazu suchen
				$SQL = 'SELECT * FROM KONTAKTE WHERE KON_STATUS ='. $this->_DB->WertSetzen('PER', 'T','A'). ' AND KON_PER_NR = '. $this->_DB->WertSetzen('PER', 'Z',$rsPER->FeldInhalt('PER_NR'));

				$rsKON = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
				
				//Wenn zu der Personalnummer ein entsprechender Kontakt vorhanden ist
				if ($rsKON->AnzahlDatensaetze() == 1)
				{
					//Pr�fen, ob der entsprechende Kontakt zun den Filialrollen zugeordnet ist
					$SQL = ' SELECT * FROM FILIALEBENENROLLENZUORDNUNGEN ';
					$SQL.= ' INNER JOIN FILIALEBENEN ON FRZ_XXX_KEY = FEB_KEY ';
					$SQL.= ' WHERE FRZ_XTN_KUERZEL = '.$this->_DB->WertSetzen('PER', 'T','FEB').' AND FRZ_FER_KEY = '.$this->_DB->WertSetzen('PER', 'Z',39);
					$SQL.= ' AND FRZ_KON_KEY = '.$this->_DB->WertSetzen('PER', 'Z',$rsKON->FeldInhalt('KON_KEY')).' ';
					$SQL.= ' AND TRUNC(FRZ_GUELTIGAB) <= TRUNC(SYSDATE) ';
					$SQL.= ' AND TRUNC(FRZ_GUELTIGBIS) >= TRUNC(SYSDATE) ';
					$SQL.= ' AND TRUNC(FEB_GUELTIGAB) <= TRUNC(SYSDATE) ';
					$SQL.= ' AND TRUNC(FEB_GUELTIGBIS) >= TRUNC(SYSDATE)';
					
					$rsFRZ = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
									
					//Wenn eine aktuelle Zuordnung vorhanden ist
					if ($rsFRZ->AnzahlDatensaetze()==1)
					{								
						//echo 'Zuordnung Filialebene: '.$rsFRZ->FeldInhalt('FEB_BEZEICHNUNG').'<br>';	
					    
						if ($rsPER->FeldInhalt('PIF_WERT') != $rsFRZ->FeldInhalt('FEB_BEZEICHNUNG'))
						{
							//echo '�nderung der Zuordnung'.PHP_EOL;
						    						    
							//Alte Zuordnung auf inaktiv setzten und neue Zuordnung hinzuf�gen
							$SQL = 'UPDATE FILIALEBENENROLLENZUORDNUNGEN ';
							$SQL.= ' SET FRZ_GUELTIGBIS = SYSDATE-1';
							$SQL.= ' , FRZ_USER = '.$this->_DB->WertSetzen('PER', 'T','AWIS');
							$SQL.= ' , FRZ_USERDAT = SYSDATE';
							$SQL.= ' WHERE FRZ_KEY = '.$this->_DB->WertSetzen('PER', 'Z',$rsFRZ->FeldInhalt('FRZ_KEY'));
							
							$Aenderung+=1;							
							
							$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('PER'));
							
							//FEB_KEY ermitteln
							$SQL = 'SELECT * FROM FILIALEBENEN ';
							$SQL.= 'WHERE FEB_BEZEICHNUNG = ' .$this->_DB->WertSetzen('PER', 'T',$rsPER->FeldInhalt('PIF_WERT'));
							$SQL.= ' AND FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
							
							$rsFEB = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
							
							if ($rsFEB->AnzahlDatensaetze()!= 1)
							{
								$Fehler = 'FEHLER: Zu dem Gebiet '.$rsPER->FeldInhalt('PIF_WERT') .' wurden im AWIS '.$rsFEB->AnzahlDatensaetze() .'Datens�tze gefunden!';			
								awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
								//echo $Fehler.PHP_EOL;
								file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
							}			
							else 
							{							
								//Neue Zuordnung hinzuf�gen
								$SQL = 'INSERT INTO FILIALEBENENROLLENZUORDNUNGEN ';
								$SQL.= '(FRZ_XXX_KEY, FRZ_XTN_KUERZEL, FRZ_FER_KEY, FRZ_KON_KEY, ';
								$SQL.= 'FRZ_GUELTIGAB, FRZ_GUELTIGBIS, FRZ_BEMERKUNG, FRZ_USER, FRZ_USERDAT) ';
								$SQL.= 'VALUES (';
								$SQL.= '' . $this->_DB->WertSetzen('PER', 'Z',$rsFEB->FeldInhalt('FEB_KEY'),false);
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','FEB',false);
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'Z',39,false);
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'Z',$rsKON->FeldInhalt('KON_KEY'),false);
								$SQL.= ', SYSDATE';				
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'D','31.12.2030',false);
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','Import Zuordnung aus PEPIS',false);
								$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','AWIS',false);
								$SQL.= ', SYSDATE';				
								$SQL.= ')';														
								
								$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('PER'));
								
							}				
						}
						else 
						{
							$Zuordnung = 'Zuordnung AWIS: '.$rsKON->FeldInhalt('KON_PER_NR').' '.$rsPER->FeldInhalt('PIF_WERT');
							file_put_contents('/var/log//awis/pepis.log',$Zuordnung."\n",FILE_APPEND);
						}

						
						//Wenn Austrittsdatum hinterlegt ist, dieses bei den FILIALEBENENROLLENZUORDNUNGEN auch mit hinterlegen
						if (strtotime($rsPER->FeldInhalt('PER_AUSTRITT')) != strtotime($rsFRZ->FeldInhalt('FRZ_GUELTIGBIS')))
						{
							if ($rsPER->FeldInhalt('PER_AUSTRITT')!='' and $rsFRZ->AnzahlDatensaetze()==1)
							{
								$SQL = 'UPDATE FILIALEBENENROLLENZUORDNUNGEN ';
								$SQL.= ' SET FRZ_GUELTIGBIS = '. $this->_DB->WertSetzen('PER', 'D',$rsPER->FeldInhalt('PER_AUSTRITT'));
								$SQL.= ' , FRZ_USER = '.$this->_DB->WertSetzen('PER', 'T','AWIS');
								$SQL.= ' , FRZ_USERDAT = SYSDATE';
								$SQL.= ' WHERE FRZ_KEY = '.$this->_DB->WertSetzen('PER', 'Z',$rsFRZ->FeldInhalt('FRZ_KEY'));
								
								$this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('PER'));
								
							}				
						}
					}		
					else if ($rsFRZ->AnzahlDatensaetze()>1)
					{
						$Fehler = 'Der Mitarbeiter ist mehrmals zu Filialebenenrollen zugeordnet!';
						awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
						file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
					}
					//Wenn Mitarbeiter bereits ausgetreten ist
					else if ($rsPER->FeldInhalt('PER_AUSTRITT') != '' AND 
							 strtotime($rsPER->FeldInhalt('PER_AUSTRITT')) <= strtotime(date("d.m.Y")))
					{
						//echo 'OK --> Mitarbeiter keine Zuordnung aber bereits ausgetreten.'.PHP_EOL;
					}
					//Wenn Mitarbeiter noch nicht zugeordnet ist
					else 
					{
						
						//FEB_KEY ermitteln
						$SQL = 'SELECT * FROM FILIALEBENEN ';
						$SQL.= 'WHERE FEB_BEZEICHNUNG = '.$this->_DB->WertSetzen('PER', 'T',$rsPER->FeldInhalt('PIF_WERT'));
						$SQL.= ' AND FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
						
						$rsFEB=$this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('PER'));
						
						if ($rsFEB->AnzahlDatensaetze()!= 1)
						{
							$Fehler = 'FEHLER: Zu dem Gebiet '.$rsPER->FeldInhalt('PIF_WERT') .' wurden im AWIS '.$rsFEB->AnzahlDatensaetze() .'Datens�tze gefunden!';
							awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
							file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
						}			
						else 
						{
						
							$SQL = 'INSERT INTO FILIALEBENENROLLENZUORDNUNGEN ';
							$SQL.= '(FRZ_XXX_KEY, FRZ_XTN_KUERZEL, FRZ_FER_KEY, FRZ_KON_KEY, ';
							$SQL.= 'FRZ_GUELTIGAB, FRZ_GUELTIGBIS, FRZ_BEMERKUNG, FRZ_USER, FRZ_USERDAT) ';
							$SQL.= 'VALUES (';
							$SQL.= '' . $this->_DB->WertSetzen('PER', 'Z',$rsFEB->FeldInhalt('FEB_KEY'),false);
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','FEB',false);
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'Z',39,false);
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'Z',$rsKON->FeldInhalt('KON_KEY'),false);
							$SQL.= ', SYSDATE';
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'D','31.12.2030');
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','Import Zuordnung aus PEPIS',false);
							$SQL.= ',' . $this->_DB->WertSetzen('PER', 'T','AWIS',false);
							$SQL.= ', SYSDATE';				
							$SQL.= ')';				
						
							$Neu+=1;							
							
							$this->_DB->Ausfuehren($SQL,'',true, $this->_DB->Bindevariablen('PER'));
						}			
					}
				}
				//Wenn zu dieser Personalnummer mehrere Kontakte vorhanden sind --> Fehlermeldung
				else if ($rsKON->AnzahlDatensaetze()>1)
				{			
					$Fehler = 'Zu der Personalnummer '.$rsPER->FeldInhalt('PER_NR').' sind mehere Kontakte vorhanden!';
									
					//Pr�fen, ob Mitarbeiter bereits ausgetreten ist
					if ($rsPER->FeldInhalt('PER_AUSTRITT') != '' AND 
						strtotime($rsPER->FeldInhalt('PER_AUSTRITT')) <= strtotime(date("d.m.Y")))
					{
						//Pr�fen, ob irgendein Kontakt zugeordnet ist
						$SQL = 'SELECT * FROM FILIALEBENENROLLENZUORDNUNGEN ';
						$SQL.= 'WHERE FRZ_KON_KEY '; 
						$SQL.= 'IN (SELECT KON_KEY FROM KONTAKTE WHERE KON_PER_NR = '.$this->_DB->WertSetzen('PER', 'Z',$rsPER->FeldInhalt('PER_NR')).")";
						
						$rsFRZKON=$this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('PER'));
						
						if ($rsFRZKON->AnzahlDatensaetze()==1)
						{
							//Wenn bereits deaktiviert --> OK
							if (strtotime($rsFRZKON->FeldInhalt('FRZ_GUELTIGBIS')) <=  strtotime(date("d.m.Y")))
							{				
								$Fehler.='1 Kontakt zugeordnet und bereits deaktiviert.';
							}
							else 
							{
								$Fehler.='1 Kontakt zugeordnet und nicht deaktiviert.';
							}
							
						}
						else 
						{
							$Fehler.='Mehrere Kontakte zugeordnet und nicht deaktiviert.';
						}					 	
					}
					else
					{				
						$Fehler.='Personal ist noch aktiv!';													
					}
					
					awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
					file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
					
				}
				//Wenn Kontakt nicht vorhanden ist
				else 
				{
					//Pr�fen, ob Mitarbeiter schon ausgetreten ist
					if ($rsPER->FeldInhalt('PER_AUSTRITT') != '' AND 
						 strtotime($rsPER->FeldInhalt('PER_AUSTRITT')) <= strtotime(date("d.m.Y")))		 
					{
						//echo 'OK --> Kein Kontakt aber Mitarbeiter bereits ausgetreten'.PHP_EOL;
					}
					//Eintrittsdatum liegt in der Zukunft
					else if ($rsPER->FeldInhalt('PER_EINTRITT') != '' AND 
						 strtotime($rsPER->FeldInhalt('PER_EINTRITT')) > strtotime(date("d.m.Y")))		 
					{
						$Fehler = 'WARNING: KEIN KONTAKT zu der Personalnummer '.$rsPER->FeldInhalt('PER_NR').' VORHANDEN.';
						$Fehler.=' Eintrittsdatum liegt in der Zukunft';
						awisWerkzeuge::EMail($this->_EMAIL_Infos,'WARNING PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
						file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
					}
					else 
					{
						$Fehler = 'FEHLER: KEIN KONTAKT zu der Personalnummer '.$rsPER->FeldInhalt('PER_NR').' VORHANDEN';
						awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
						file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
						//echo $Fehler.PHP_EOL;
					}
				}
				$rsPER->DSWeiter();		
			}
			
			//echo $Aenderung.PHP_EOL;
			//echo $Neu.PHP_EOL;
						
			//Pr�fen, ob alle Mitarbeiter zugeordnet sind			
			$SQL = ' SELECT * ';
			$SQL.= ' FROM PERSONAL ';
			$SQL.= ' INNER JOIN PERSONALINFOS ON PIF_PER_NR = PER_NR AND PIF_ITY_KEY = '.$this->_DB->WertSetzen('PER', 'Z',12);
			$SQL.= ' LEFT JOIN KONTAKTE ON KON_PER_NR = PER_NR ';
			$SQL.= ' LEFT JOIN FILIALEBENENROLLENZUORDNUNGEN ON FRZ_KON_KEY = KON_KEY ';
			$SQL.= ' AND FRZ_GUELTIGAB <= SYSDATE AND FRZ_GUELTIGBIS >= SYSDATE ';
			$SQL.= ' AND FRZ_XTN_KUERZEL = '.$this->_DB->WertSetzen('PER', 'T','FEB').' AND FRZ_FER_KEY = '.$this->_DB->WertSetzen('PER', 'Z',39);
			$SQL.= ' LEFT JOIN FILIALEBENEN ON FRZ_XXX_KEY = FEB_KEY ';
			$SQL.= ' WHERE PER_PET_ID IN ';
			$SQL.= ' (SELECT PET_ID FROM PERSONALTAETIGKEITEN ';
			$SQL.= ' WHERE PET_TAETIGKEIT = '. $this->_DB->WertSetzen('PER', 'T','Smart-Repair-Lackmeister');
			$SQL.= ' OR PET_TAETIGKEIT = '.$this->_DB->WertSetzen('PER', 'T','Smart-Repair-Monteur').')';
			$SQL.= ' AND (KON_KEY IS NULL OR FRZ_KEY IS NULL OR FEB_BEZEICHNUNG <> PIF_WERT) ';
			$SQL.= ' AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT > SYSDATE)';
								
			$rsPER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
			
			if ($rsPER->AnzahlDatensaetze()>0)
			{
				$Fehler = 'FEHLENDE ZUORDNUNGEN: '.$rsPER->AnzahlDatensaetze().PHP_EOL;
				awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
				file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
			}
						
			//Pr�fen, ob noch alte Zuordnungen vorhanden sin
			$SQL = ' SELECT * FROM FILIALEBENENROLLENZUORDNUNGEN ';
			$SQL.= ' INNER JOIN FILIALEBENEN ON FRZ_XXX_KEY = FEB_KEY ';
			$SQL.= ' INNER JOIN KONTAKTE ON KON_KEY = FRZ_KON_KEY ';
			$SQL.='  LEFT JOIN PERSONAL ON KON_PER_NR = PER_NR ';
			$SQL.= ' AND PER_PET_ID IN ';
			$SQL.= ' (SELECT PET_ID FROM PERSONALTAETIGKEITEN ';
			$SQL.= ' WHERE PET_TAETIGKEIT = '.$this->_DB->WertSetzen('PER', 'T','Smart-Repair-Lackmeister');
			$SQL.= ' OR PET_TAETIGKEIT = '. $this->_DB->WertSetzen('PER', 'T','Smart-Repair-Monteur').')';
			$SQL.= ' WHERE FRZ_XTN_KUERZEL = '.$this->_DB->WertSetzen('PER', 'T','FEB').' AND FRZ_FER_KEY = '.$this->_DB->WertSetzen('PER', 'Z',39);
			$SQL.= ' AND FRZ_GUELTIGAB <= SYSDATE ';
			$SQL.= ' AND FRZ_GUELTIGBIS >= SYSDATE ';
			$SQL.= ' AND PER_NR IS NULL';
						
			$rsPER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
			
			if ($rsPER->AnzahlDatensaetze()>0)
			{
				$Fehler = 'ZU VIELE ZUORDNUNGEN: '.$rsPER->AnzahlDatensaetze().PHP_EOL;
				awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
				file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
			}
			
			//Pr�fen, ob fehlende Personalnummern vorhanden sin
			$SQL = ' SELECT * FROM FILIALEBENENROLLENZUORDNUNGEN ';
			$SQL.= ' INNER JOIN FILIALEBENEN ON FRZ_XXX_KEY = FEB_KEY ';
			$SQL.= ' INNER JOIN KONTAKTE ON KON_KEY = FRZ_KON_KEY ';
			$SQL.='  LEFT JOIN PERSONAL ON KON_PER_NR = PER_NR ';
			$SQL.= ' AND PER_PET_ID IN ';
			$SQL.= ' (SELECT PET_ID FROM PERSONALTAETIGKEITEN ';
			$SQL.= ' WHERE PET_TAETIGKEIT = '.$this->_DB->WertSetzen('PER', 'T','Smart-Repair-Lackmeister');
			$SQL.= ' OR PET_TAETIGKEIT = '.$this->_DB->WertSetzen('PER', 'T','Smart-Repair-Monteur').')';
			$SQL.= ' WHERE FRZ_XTN_KUERZEL = '.$this->_DB->WertSetzen('PER', 'T','FEB').' AND FRZ_FER_KEY = '.$this->_DB->WertSetzen('PER', 'Z',39);
			$SQL.= ' AND FRZ_GUELTIGAB <= SYSDATE ';
			$SQL.= ' AND FRZ_GUELTIGBIS >= SYSDATE ';
			$SQL.= ' AND KON_PER_NR IS NULL';
						
			$rsPER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PER'));
			
			if ($rsPER->AnzahlDatensaetze()>0)
			{
				$Fehler = 'FEHLENDE PERSNR: '.$rsPER->AnzahlDatensaetze().PHP_EOL;
				awisWerkzeuge::EMail($this->_EMAIL_Infos,'ERROR PEPISZUORDNUNGEN',$Fehler,2,'','awis@de.atu.eu');
				file_put_contents('/var/log//awis/pepis.log',$Fehler."\n",FILE_APPEND);
				//echo $Fehler.PHP_EOL;				
			}
			awisWerkzeuge::EMail($this->_EMAIL_Infos,'OK IMPORT PEPISZUORDNUNGEN BEENDET','',1,'','awis@de.atu.eu');					
		}
		catch (Exception $ex)
		{
			if($ex->getCode()==1)
			{

			}
			throw new Exception('Fehler beim Import der Pepiszuordnungen: '.$ex->getMessage().' - '.$ex->getCode(),12);
                }
        }
}
?>
