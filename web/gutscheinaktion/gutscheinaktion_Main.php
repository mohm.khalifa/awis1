<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'gutscheinaktion_funktionen.inc';

    global $AWISCursorPosition;
    global $GUT;

    try {
    $GUT = new gutscheinaktion_funktionen();

    if(isset($_POST['cmdPDF_x'])){
        $_GET['XRE'] = 67;
        $_GET['ID'] = 'bla';

        require_once '/daten/web/berichte/drucken.php';
    }

    echo "<link rel=stylesheet type=text/css href='" . $GUT->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $GUT->AWISSprachKonserven['TITEL']['tit_Gutscheinaktion'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$GUT->RechteMeldung(1); //Anzeigenrecht?

$Register = new awisRegister(55000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$GUT->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    echo  $ex->getMessage();die;
    if ($GUT->Form instanceof awisFormular) {
        $GUT->Form->DebugAusgabe(1, $ex->getSQL());
        $GUT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $GUT->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($GUT->Form instanceof awisFormular) {
        $GUT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>