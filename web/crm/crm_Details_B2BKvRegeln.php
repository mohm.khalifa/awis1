<?php

global $AWISBenutzer;
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[]=array('CBR','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3718 = awisBenutzerRecht($con,3718,$AWISBenutzer->BenutzerName());
if($Recht3718==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
    echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
    die();
}

$SQL = 'SELECT *';
$SQL .= ' FROM CRMADRESSEN';
$SQL .= ' LEFT JOIN CRMB2BKVREGELN';
$SQL .= ' ON CAD_KEY = CBR_CAD_KEY';
$SQL .= ' WHERE CAD_KEY=:var_N0_CAD_KEY';
$BindeVariablen=array();
$BindeVariablen['var_N0_CAD_KEY']=$AWIS_KEY1;
$rsCBR = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);

if(isset($rsCBR['CAD_KEY'][0])) {
    awis_FORM_FormularStart();
    $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    $EditRecht = (($Recht3718 & 2) != 0);

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CBR']['Status'].':', 0);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_REGELAKTIV'] . ':', 300);
    $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
    awis_FORM_Erstelle_SelectFeld('CBR_REGELAKTIV', $rsCBR['CBR_REGELAKTIV'][0], 200, $EditRecht, $con, '', '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
    awis_FORM_ZeileEnde();

    awis_FORM_Trennzeile();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CBR']['Verteilbedingungen'].':', 0);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_BETRAG'] . ':', 300);
    awis_FORM_Erstelle_TextFeld('CBR_BETRAG', $rsCBR['CBR_BETRAG'][0], 15, 200, $EditRecht);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_KMSTAND'] . ':', 300);
    awis_FORM_Erstelle_TextFeld('CBR_KMSTAND', $rsCBR['CBR_KMSTAND'][0], 8, 200, $EditRecht);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_FAHRZEUGALTER'] . ':', 300);
    awis_FORM_Erstelle_TextFeld('CBR_FAHRZEUGALTER', $rsCBR['CBR_FAHRZEUGALTER'][0], 8, 200, $EditRecht);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_MAILEMPFAENGER'] . ':', 300);
    awis_FORM_Erstelle_TextFeld('CBR_MAILEMPFAENGER', $rsCBR['CBR_MAILEMPFAENGER'][0], 60, 200, $EditRecht);
    awis_FORM_ZeileEnde();

    awis_FORM_Trennzeile();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CBR']['Auftragsbezogenes_Regelwerk'].':', 0);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CBR']['CBR_AUFTRAGBEZOGEN'] . ':', 300);
    $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
    awis_FORM_Erstelle_SelectFeld('CBR_AUFTRAGBEZOGEN', $rsCBR['CBR_AUFTRAGBEZOGEN'][0], 200, $EditRecht, $con, '', '', 0, '', '', $Daten);
    awis_FORM_ZeileEnde();

    echo '<input type=hidden name=CBR_KEY value=0' . $rsCBR['CBR_KEY'][0] . '>';
    echo '<input type=hidden name=CAD_KEY value=0' . $AWIS_KEY1 . '>';

    awis_FORM_ZeileEnde();
}