<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('REH','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_zurueck');


try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$Form->DebugAusgabe(1,'speichern',$_POST);
	$Form->DebugAusgabe(1,'speichern',$_GET);

	//***********************************************
	// Reifenpflege
	//***********************************************
	if(isset($_POST['txtREH_KEY']))
	{
		if($_POST['txtREH_KEY']=='')
		{
			$AWIS_KEY1=-1;
		}
		else
		{
			$AWIS_KEY1=$_POST['txtREH_KEY'];
		}
		
		$Felder = $Form->NameInArray($_POST, 'txtREH_',1,1);
		
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
				
			if ($AWIS_KEY1==-1) //Neuer Hersteller
			{
				$Felder = $Form->NameInArray($_POST, 'txtREH_',1,1);
			
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';
				$Pflichtfelder = array('REH_BEZEICHNUNG','REH_KURZBEZEICHNUNG','REH_ATUNRPRAEFIX');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['REH'][$Pflichtfeld].'<br>';
					}
				}

				if($Fehler=='')
				{
					$_POST['txtREH_BEZEICHNUNG'] = strtoupper($_POST['txtREH_BEZEICHNUNG']);
					$_POST['txtREH_KURZBEZEICHNUNG'] = strtoupper($_POST['txtREH_KURZBEZEICHNUNG']);
					
					$_POST['txtREH_ATUNRPRAEFIX']=str_replace("\r\n",',',$_POST['txtREH_ATUNRPRAEFIX']);
					$_POST['txtREH_ATUNRPRAEFIX']=str_replace("\t",',',$_POST['txtREH_ATUNRPRAEFIX']);
					$_POST['txtREH_ATUNRPRAEFIX']=str_replace(' ','',$_POST['txtREH_ATUNRPRAEFIX']);
					$_POST['txtREH_ATUNRPRAEFIX']=str_replace(';',',',$_POST['txtREH_ATUNRPRAEFIX']);
					$_POST['txtREH_ATUNRPRAEFIX']=str_replace('/',',',$_POST['txtREH_ATUNRPRAEFIX']);
					$_POST['txtREH_ATUNRPRAEFIX']=strtoupper($_POST['txtREH_ATUNRPRAEFIX']);
					$i = 1;
					while ($i>0)
					{
						$_POST['txtREH_ATUNRPRAEFIX']=str_replace(',,',',',$_POST['txtREH_ATUNRPRAEFIX'],$i);
					}
				}
				
				//Pr�fung, ob schon vorhanden
				$SQL = 'SELECT REH_KEY';
				$SQL .= ' FROM REIFENHERSTELLER';
				$SQL .= ' WHERE REH_BEZEICHNUNG='.$DB->FeldInhaltFormat('T',$_POST['txtREH_BEZEICHNUNG'],false);
				$rsREH = $DB->RecordSetOeffnen($SQL);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
					
				if (!$rsREH->EOF())
				{
					$Fehler .= $TXT_Speichern['REH']['err_HerstellerBereitsVorhanden'].' '.$_POST['txtREH_BEZEICHNUNG'].'<br>';
				}
					
				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();
						
					$Link = './reifenhersteller_Main.php?cmdAktion=Details&REH_KEY='.$AWIS_KEY1;
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
					$Form->SchaltflaechenEnde();
						
					die();
				}
			
				$Speichern = true;
			
				if(isset($_POST['bestaetigung']))
				{
					$Speichern=isset($_POST['cmdSpeichern_x']);
				}

				if ($Speichern)
				{
					$SQL  = 'INSERT';
					$SQL .= ' INTO REIFENHERSTELLER (';
					$SQL .= ' REH_BEZEICHNUNG';
					$SQL .= ' ,REH_KURZBEZEICHNUNG';
					$SQL .= ' ,REH_ATUNRPRAEFIX';
					$SQL .= ' ,REH_USER';
					$SQL .= ' ,REH_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= '  '.$DB->FeldInhaltFormat('T',$_POST['txtREH_BEZEICHNUNG'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtREH_KURZBEZEICHNUNG'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtREH_ATUNRPRAEFIX'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ' ,SYSDATE)';

					//$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					
					$SQL = 'SELECT seq_REH_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
						
				}
					
			} //end Neuer Hersteller
			else //ge�nderter Hersteller
			{
				if (isset($_POST['txtREH_SONDERBESTNR']) AND $_POST['txtREH_SONDERBESTNR']!='')
				{
					$_POST['txtREH_SONDERBESTNR']=str_replace("\r\n",',',$_POST['txtREH_SONDERBESTNR']);
					$_POST['txtREH_SONDERBESTNR']=str_replace("\t",',',$_POST['txtREH_SONDERBESTNR']);
					$_POST['txtREH_SONDERBESTNR']=str_replace(' ','',$_POST['txtREH_SONDERBESTNR']);
					$_POST['txtREH_SONDERBESTNR']=str_replace(';',',',$_POST['txtREH_SONDERBESTNR']);
					$_POST['txtREH_SONDERBESTNR']=str_replace('/',',',$_POST['txtREH_SONDERBESTNR']);
					$_POST['txtREH_SONDERBESTNR']=strtoupper($_POST['txtREH_SONDERBESTNR']);
					$i = 1;
					while ($i>0)
					{
						$_POST['txtREH_SONDERBESTNR']=str_replace(',,',',',$_POST['txtREH_SONDERBESTNR'],$i);
					}
				}
				
				$FehlerListe = array();
				$UpdateFelder = '';
					
				$rsREH= $DB->RecordSetOeffnen('SELECT * FROM REIFENHERSTELLER WHERE REH_key=' . $_POST['txtREH_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
			
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsREH->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsREH->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsREH->FeldInfo($FeldName,'TypKZ'),$rsREH->FeldInhalt($FeldName),true);
						//echo $WertAlt. '/'. $WertNeu . '<br>';
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
			
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
					
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsREH->FeldInhalt('REH_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE REIFENHERSTELLER SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', REH_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', REH_userdat=sysdate';
					$SQL .= ' WHERE REH_key=0' . $_POST['txtREH_KEY'] . '';
			
					$DB->Ausfuehren($SQL,'',false);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
				}
			}//End ge�nderter Hersteller
		}//end if($Felder!='')
	}//end if(isset($_POST['txtREH_KEY']))
}//end Try		

catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>