<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'jskonverter_functions.inc';

    global $JSK;

    try {
    $JSK = new jskonverter_functions();

    echo "<link rel=stylesheet type=text/css href='" . $JSK->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $JSK->AWISSprachKonserven['TITEL']['tit_JSKonverter'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$JSK->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(60000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

} catch (awisException $ex) {
    if ($JSK->Form instanceof awisFormular) {
        $JSK->Form->DebugAusgabe(1, $ex->getSQL());
        $JSK->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201701301500");
    } else {
        $JSK->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($JSK->Form instanceof awisFormular) {
        $JSK->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201701301500");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>