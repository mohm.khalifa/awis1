<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';


class crm_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht68000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;

    private $_VerfuegbareSprachen = null;
    private $_VerfuegbareSprachenSF = null;


    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht68000 = $this->AWISBenutzer->HatDasRecht(68000);
        $this->_EditRecht = (($this->Recht68000 & 0) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_StammCRM'));
        $this->AWISWerkzeug = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('TITEL', 'tit_CRM');
        $TextKonserven[] = array('CRM', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_StammCRM', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht68000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'CRM'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }


    public function VerfuegbareSprachenArray($SelectFeld = true){

        if($this->_VerfuegbareSprachen == null){
            $SQL  ='select';
            $SQL .='     replace(COLUMN_NAME,\'CRM_TEXT_\',\'\') as SPRACHE';
            $SQL .=' from';
            $SQL .='     sys.ALL_TAB_COLUMNS';
            $SQL .=' where';
            $SQL .='     owner = user';
            $SQL .='     and upper(TABLE_NAME) = upper(\'TEXTKONSERVEN\')';
            $SQL .='     and upper(COLUMN_NAME) like \'CRM_TEXT_%\'';
            $SQL .=' order by';
            $SQL .='    1';

            $rsCRM = $this->DB->RecordSetOeffnen($SQL);

            while(!$rsCRM->EOF()){
                $Sprache = $rsCRM->FeldInhalt('SPRACHE');
                $Beschreibung = isset($this->AWISSprachKonserven['CRM']["CRM_TEXT_$Sprache"])?$this->AWISSprachKonserven['CRM']["CRM_TEXT_$Sprache"]:$Sprache;
                $this->_VerfuegbareSprachen[$Sprache] = $Beschreibung;
                $this->_VerfuegbareSprachenSF[] = $Sprache.'~'.$Beschreibung;
                $rsCRM->DSWeiter();
            }
        }
        if($SelectFeld){
            return $this->_VerfuegbareSprachenSF;
        }else{
            return $this->_VerfuegbareSprachen;
        }

    }

    public function TextkonservenSelectfeldSQL(){
        $SQL  ='select distinct';
        $SQL .='     CRM_BEREICH,';
        $SQL .='     coalesce(B.XTN_TABELLENNAME,CRM_BEREICH) as BESCHREIBUNG';
        $SQL .=' from';
        $SQL .='     TEXTKONSERVEN a';
        $SQL .='     left join TABELLENKUERZEL B on a.CRM_BEREICH = B.XTN_KUERZEL';
        $SQL .=' order by 2';
        return $SQL;
    }

    function leseTemplateUeberschrift($Template, $alsString = false){
        $Template = fopen($Template,'r');
        $Ueberschrift = false;
        if($Template){
            $Ueberschrift = fgets($Template);
            if(!$alsString){
                $Ueberschrift = explode(';',$Ueberschrift);
            }
        }

        return $Ueberschrift;
    }

    public function KON_SQL(){
        $SQL = 'SELECT DISTINCT *';
        $SQL .= " FROM (SELECT kon.kon_key, kon.kon_name1||', '|| COALESCE(kon.kon_name2,'') || ' KON_KEY => ' || kon.kon_key AS kontaktname";
        $SQL .= ' FROM kontakte kon INNER JOIN v_accountrechte b';
        $SQL .= ' ON kon_key = b.xbn_kon_key';
        $SQL .= " WHERE (b.xrc_id = 3704 AND BITAND (b.xba_stufe, 16) = 16 AND kon.kon_status='A')";
        $SQL .= ') ORDER BY kontaktName';

        return $SQL;
    }

    public function CGB_SQL(){
        $SQL = "SELECT CGB_KEY, CGB_Gebiet || ' CGB_KEY => ' || CGB_KEY as CGB_Gebiet FROM CRMGebiete ORDER BY CGB_Gebiet";

        return $SQL;
    }

}