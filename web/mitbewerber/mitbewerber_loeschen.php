<?php
global $Param;
global $awisRSInfo;
global $awisDBError;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

//awis_Debug(1,$_POST,$_GET);
$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamter Mitbewerber
{
	$Tabelle = 'MBW';
	$Key=$_POST['txtMBW_KEY'];

	$Felder=array();
	$Felder[]=array(awis_TextKonserve($con,'MBW_NAME1','MBW',$AWISSprache),$_POST['txtMBW_NAME1']);
	$Felder[]=array(awis_TextKonserve($con,'MBW_NAME2','MBW',$AWISSprache),$_POST['txtMBW_NAME2']);
	$Felder[]=array(awis_TextKonserve($con,'MBW_ORT','MBW',$AWISSprache),$_POST['txtMBW_ORT']);

}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Filialen':
			$Tabelle = 'MFI';
			$Key=$_GET['Del'];

			$rsDaten = awisOpenRecordset($con, 'SELECT MFI_FIL_ID,MFI_BEMERKUNG FROM MitbewerberFilialen WHERE MFI_KEY=0'.$Key);
			$Felder=array();

			$Felder[]=array(awis_TextKonserve($con,'MFI_FIL_ID','MFI',$AWISSprache),$rsDaten['MFI_FIL_ID'][0]);
			$Felder[]=array(awis_TextKonserve($con,'MFI_BEMERKUNG','MFI',$AWISSprache),$rsDaten['MFI_BEMERKUNG'][0]);
			break;
		case 'Unternehmenstypen':
			$Tabelle = 'MIU';
			$Key=$_GET['Del'];
			$rsDaten = awisOpenRecordset($con, 'SELECT MUT_UNTERNEHMENSTYP, MIU_BEMERKUNG FROM MitbewerbUTypen INNER JOIN MitbewerbUTypenTypen ON MIU_MUT_KEY = MUT_KEY WHERE MIU_KEY=0'.$Key);
			$Felder=array();

			$Felder[]=array(awis_TextKonserve($con,'MUT_UNTERNEHMENSTYP','MUT',$AWISSprache),$rsDaten['MUT_UNTERNEHMENSTYP'][0]);
			$Felder[]=array(awis_TextKonserve($con,'MIU_BEMERKUNG','MIU',$AWISSprache),$rsDaten['MIU_BEMERKUNG'][0]);

			break;
		case 'Hersteller':
			$Tabelle = 'MBH';
			$Key=$_GET['Del'];

			$rsDaten = awisOpenRecordset($con, 'SELECT MBH_KEY, HER_BEZEICHNUNG, MBH_BEMERKUNG FROM MitbewerbHersteller INNER JOIN Hersteller ON MBH_HER_ID = HER_ID WHERE MBH_KEY=0'.$Key);
			$Felder=array();

			//$Felder[]=array(awis_TextKonserve($con,'MBH_KEY','HER',$AWISSprache),$rsDaten['MBH_KEY'][0]);
			$Felder[]=array(awis_TextKonserve($con,'HER_BEZEICHNUNG','HER',$AWISSprache),$rsDaten['HER_BEZEICHNUNG'][0]);
			$Felder[]=array(awis_TextKonserve($con,'MBH_BEMERKUNG','MBH',$AWISSprache),$rsDaten['MBH_BEMERKUNG'][0]);
			break;
		case 'Produkte':
			$Tabelle = 'MPG';
			$Key=$_GET['Del'];

			$rsDaten = awisOpenRecordset($con, 'SELECT MGT_GRUPPE, MPG_BEMERKUNG FROM MitbewerbProdGruppen INNER JOIN MitbewerbProdGruppenTypen ON MPG_MGT_ID = MGT_ID WHERE MPG_KEY=0'.$Key);
			$Felder=array();

			$Felder[]=array(awis_TextKonserve($con,'MGT_GRUPPE','MGT',$AWISSprache),$rsDaten['MGT_GRUPPE'][0]);
			$Felder[]=array(awis_TextKonserve($con,'MPG_BEMERKUNG','MPG',$AWISSprache),$rsDaten['MPG_BEMERKUNG'][0]);
			break;
		case 'Aenderungen':
				// Status sofort �ndern!
			$SQL = 'UPDATE MitbewerbAenderungen SET MAE_STATUS=\'X\',MAE_USERDAT=SYSDATE,MAE_USER=\''.$AWISBenutzer->BenutzerName().'\' WHERE mae_key='.intval($_GET['Del']);
			if(awisExecute($con,$SQL)===false)
			{
				awisErrorMailLink('mitbewerber_loeschen_1',1,$awisDBError['messages'],'');
			}


			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
{

//awis_Debug(1,$_POST);
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'MBW':
			$SQL = 'DELETE FROM Mitbewerber WHERE mbw_key=0'.$_POST['txtKey'];
			awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $AWISBenutzer->BenutzerName(),'');
			break;
		case 'MFI':
			$SQL = 'DELETE FROM MitbewerberFilialen WHERE mfi_key=0'.$_POST['txtKey'];
			break;
		case 'MIU':
			$SQL = 'DELETE FROM MitbewerbUTypen WHERE miu_key=0'.$_POST['txtKey'];
			break;
		case 'MBH':
			$SQL = 'DELETE FROM MitbewerbHersteller WHERE mbh_key=0'.$_POST['txtKey'];
			break;
		case 'MPG':
			$SQL = 'DELETE FROM MitbewerbProdGruppen WHERE mpg_key=0'.$_POST['txtKey'];
			break;

		default:
			break;
	}
//awis_Debug(1,$SQL);

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)===false)
		{
			awisErrorMailLink('mitbewerber_loeschen_1',1,$awisDBError['messages'],'');
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./mitbewerber_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}


?>