<?php
global $HER;        // Zum Cursor setzen
try {
    
    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_GET['HER_ID'])) {
        $HER->AWIS_KEY1 = $_GET['HER_ID'];
    } elseif (isset($_POST['txtHER_ID'])) {
        $HER->AWIS_KEY1 = $_POST['txtHER_ID'];
    } else {
        $HER->AWIS_KEY1 = '';
    }
    $Param['HER_ID'] = $HER->AWIS_KEY1;
    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['HER_ID'] = $_POST['txtHER_ID'];

        $Param['HER_ID'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./hersteller_loeschen.php');
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./hersteller_speichern.php');
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $HER->AWIS_KEY1 = -1;
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {
        if (!$HER->AWIS_KEY1 > 0) {
            if (!isset($Param['HER_ID'])) {

                $Param['HER_ID'] = '';
                $Param['WHERE'] = '';
                $Param['ORDER'] = '';
            }

            if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
               $Param['HER_ID'] = 0;
            }
            $HER->AWIS_KEY1 = $Param['HER_ID'];
        }
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************

    if (!isset($_GET['Sort'])) {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '') {
            $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY HER_ID asc';
            $Param['ORDER'] = 'HER_ID asc';
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = $HER->erstelleHERBedingung($Param);

    $SQL = 'SELECT hersteller.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM hersteller';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $HER->Form->DebugAusgabe(1, $SQL);

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($HER->AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $HER->Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $HER->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $HER->DB->ErmittleZeilenAnzahl($SQL, $HER->DB->Bindevariablen('HER', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $HER->DB->WertSetzen('HER', 'N0', $StartZeile) . ' AND  ZeilenNr<' . $HER->DB->WertSetzen('HER', 'N0',
                ($StartZeile + $ZeilenProSeite));
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsHER = $HER->DB->RecordsetOeffnen($SQL, $HER->DB->Bindevariablen('HER', true));

    $HER->Form->DebugAusgabe(1, $HER->DB->LetzterSQL(), $Param, $_GET, $_POST, $HER->AWIS_KEY1);
    
    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmhersteller  method=POST>';
    $DetailAnsicht = false;
    $HER->Form->Formular_Start();
    if ($rsHER->EOF() AND !isset($_POST['cmdDSNeu_x']))        // Keine Meldung bei neuen Datens�tzen!
    {
        $HER->Form->Hinweistext($HER->AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsHER->AnzahlDatensaetze() > 1 AND !isset($_GET['HER_ID']))                        // Liste anzeigen
    {

        $SpaltenBreite = array();
        $SpaltenBreite['HER_ID'] = 100;
        $SpaltenBreite['HER_BEZEICHNUNG'] = 400;

        $HER->Form->ZeileStart();
        $Link = './hersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=HER_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HER_ID'))?'~':'');
        $HER->Form->Erstelle_Liste_Ueberschrift($HER->AWISSprachKonserven['HER']['HER_ID'], $SpaltenBreite['HER_ID'], '', $Link);
        $Link = './hersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=HER_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HER_BEZEICHNUNG'))?'~':'');
        $HER->Form->Erstelle_Liste_Ueberschrift($HER->AWISSprachKonserven['HER']['HER_BEZEICHNUNG'], $SpaltenBreite['HER_BEZEICHNUNG'], '', $Link);
        $HER->Form->ZeileEnde();

        $DS = 0;
        while (!$rsHER->EOF()) {
            $HER->Form->ZeileStart();
            $Link = './hersteller_Main.php?cmdAktion=Details&HER_ID=' . $rsHER->FeldInhalt('HER_ID') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $HER->Form->Erstelle_ListenFeld('HER_ID', $rsHER->FeldInhalt('HER_ID'), 0, $SpaltenBreite['HER_ID'], false, ($DS % 2), '', $Link, 'T');
            $HER->Form->Erstelle_ListenFeld('HER_BEZEICHNUNG', $rsHER->FeldInhalt('HER_BEZEICHNUNG'), 0, $SpaltenBreite['HER_BEZEICHNUNG'], false, ($DS % 2), '', '', 'T', 'L');
            $HER->Form->ZeileEnde();

            $rsHER->DSWeiter();
            $DS++;
        }

        $Link = './hersteller_Main.php?cmdAktion=Details';
        $HER->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {//Ein DS
        $DetailAnsicht = true;
        $HER->AWIS_KEY1 = $rsHER->FeldInhalt('HER_ID');

        $Param['HER_ID'] = $HER->AWIS_KEY1;

        $HER->Form->Erstelle_HiddenFeld('HER_ID', $HER->AWIS_KEY1);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./hersteller_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $HER->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHER->FeldInhalt('HER_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHER->FeldInhalt('HER_USERDAT'));
        $HER->Form->InfoZeile($Felder, '');

        $HER->Form->ZeileStart();
        $HER->Form->Erstelle_TextLabel($HER->AWISSprachKonserven['HER']['HER_ID'] . ':', 170);
        $HER->Form->Erstelle_TextFeld('!HER_ID', $rsHER->FeldInhalt('HER_ID'), 10, 600, false, '', '', '', 'T');
        $HER->Form->ZeileEnde();

        $HER->Form->ZeileStart();
        $HER->Form->Erstelle_TextLabel($HER->AWISSprachKonserven['HER']['HER_BEZEICHNUNG'] . ':', 170);
        $HER->Form->Erstelle_TextFeld('!HER_BEZEICHNUNG', $rsHER->FeldInhalt('HER_BEZEICHNUNG'), 10, 600, false, '', '', '', 'T');
        $HER->Form->ZeileEnde();

        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'TecDoc'));

        $Register = new awisRegister(603);
        $Register->ZeichneRegister($RegisterSeite);
    }

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $HER->Form->Formular_Ende();
    $HER->Form->SchaltflaechenStart();

    $HER->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $HER->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (($HER->Recht602 & 2) == 2 AND $DetailAnsicht) {
        $HER->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $HER->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $HER->Form->SchaltflaechenEnde();

    $HER->Form->SchreibeHTMLCode('</form>');
    
} catch (awisException $ex) {
    if ($HER->Form instanceof awisFormular) {
        $HER->Form->DebugAusgabe(1, $ex->getSQL());
        $HER->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $HER->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($HER->Form instanceof awisFormular) {
        $HER->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>