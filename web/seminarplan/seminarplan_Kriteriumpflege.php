<?php
/**
 * Bearbeitung der R�ckf�hrungen durch die Filiale bzw. Zentrale
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200809291144
 */

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Maske;

try 
{		
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SKK','%');
	$TextKonserven[]=array('SEP','%');
	//$TextKonserven[]=array('SRG','%');
	//$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	//$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	//$TextKonserven[]=array('Wort','lbl_DSZurueck');
	//$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	//$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');	
	//$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	//$TextKonserven[]=array('Fehler','err_keineRechte');
	//$TextKonserven[]=array('Fehler','err_keineDaten');
	//$TextKonserven[]=array('Wort','Status');
	//$TextKonserven[]=array('Wort','AlleAnzeigen');
	//$TextKonserven[]=array('Wort','Abschliessen');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	
	$script = "<script type='text/javascript'>
		  
			function base64_encode(data) {
			  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
			  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
			    ac = 0,
			    enc = '',
			    tmp_arr = [];
			
			  if (!data) {
			    return data;
			  }
			
			  data = unescape(encodeURIComponent(data));
			
			  do {
			    // pack three octets into four hexets
			    o1 = data.charCodeAt(i++);
			    o2 = data.charCodeAt(i++);
			    o3 = data.charCodeAt(i++);
			
			    bits = o1 << 16 | o2 << 8 | o3;
			
			    h1 = bits >> 18 & 0x3f;
			    h2 = bits >> 12 & 0x3f;
			    h3 = bits >> 6 & 0x3f;
			    h4 = bits & 0x3f;
			
			    // use hexets to index into b64, and append result to encoded string
			    tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
			  } while (i < data.length);
			
			  enc = tmp_arr.join('');
			
			  var r = data.length % 3;
			
			  return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
			}
			
			function urlencode(str) {

  				str = (str + '').toString();

  				return encodeURIComponent(str)
    			.replace(/!/g, '%21')
    			.replace(/'/g, '%27')
  			    .replace(/\(/g, '%28')
                .replace(/\)/g, '%29')
                .replace(/\*/g, '%2A')
                .replace(/%20/g, '+');
			}
			
			function urlanpassen (linkObjekt) {
		    
			if (linkObjekt.getAttribute) 
			{
				urlNeu = '/berichte/drucken.php?XRE=44&ID=';
			    urlZusatz = 'Sep_Seminar='+ urlencode(document.getElementById('sucSKK_SEMINAR').value);
				urlZusatz = urlZusatz + '&Sep_Von='+urlencode(document.getElementById('sucSKK_VON').value);
				urlZusatz = urlZusatz + '&Sep_Bis='+urlencode(document.getElementById('sucSKK_BIS').value);
				urlZusatz = urlZusatz + '&Sep_Frage='+urlencode(document.getElementsByName('sucSEF_KEY')[0].value);
				
				if(document.getElementsByName('sucNurBearbeitung')[0].checked)
				{
					Bearb=1;
				}
				else
				{
					Bearb=0;
				}
				
				urlZusatz = urlZusatz + '&Sep_Bearb='+urlencode(Bearb);
				urlNeu = urlNeu+base64_encode(urlZusatz);
				
				linkObjekt.setAttribute('href', urlNeu);		      
		    }
		  }
	</script>";
	
	echo $script;
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();		
	if(isset($_REQUEST['txtMaske']) and $_REQUEST['txtMaske'] == 'Liste')
	{
		$Maske = 'Liste';
	}
	elseif((isset($_REQUEST['txtMaske']) and $_REQUEST['txtMaske'] == 'Kriterium') or isset($_GET['SSort']))
	{
		$Maske = 'Kriterium';
	}
	else 
	{
		$Maske = 'Suche';
	}
	
	
	$FeldBreiten['SEP_SEMINAR']=100;
	$FeldBreiten['SKK_FRAGE']=360;
	$FeldBreiten['SEE_FEEDBACK']=150;
	$FeldBreiten['SKK_ANTWORT']=370;
	$FeldBreiten['SKK_KRITSET']=110;
	$FeldBreiten['SKK_KRITERIUM1']=110;
	$FeldBreiten['SKK_KRITERIUM2']=300;
	$FeldBreiten['SKK_KRITERIUM3']=150;
	$FeldBreiten['SKK_VON']=95;
	$FeldBreiten['SKK_BIS']=95;
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht4205 = $AWISBenutzer->HatDasRecht(4205);
	if($Recht4205==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{	
		$Param['SKK_SEMINAR']=$_POST['sucSKK_SEMINAR'];	
		$Param['SKK_VON']=$_POST['sucSKK_VON'];	
		$Param['SKK_BIS']=$_POST['sucSKK_BIS'];	
		$Param['SEF_KEY']=$_POST['sucSEF_KEY'];
		
		$Param['BEARB'] = (isset($_POST['sucNurBearbeitung'])?'on':'');
		$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
		
		$Param['BLOCK']= 1;
		$Param['ORDERBY']='';
	
		$AWISBenutzer->ParameterSchreiben("SKKSuche",serialize($Param));
		$Maske = 'Liste';	
		$AWIS_KEY1 = '';
		$_REQUEST['txtSEZ_KEY'] = '';
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen("SKKSuche"));
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$Param);
	
	if(isset($_GET['SEZ_KEY']))
	{
		$AWIS_KEY1 = $_GET['SEZ_KEY'];
		$Maske = 'Liste';
	}
	
	if(isset($_REQUEST['SKK_KEY']) and $_REQUEST['SKK_KEY'] != '')
	{
		$AWIS_KEY2 = $_REQUEST['SKK_KEY'];
	}
	
	if(isset($_REQUEST['txtSEZ_KEY']) and $_REQUEST['txtSEZ_KEY'] != '')
	{
		$AWIS_KEY1 = $_REQUEST['txtSEZ_KEY'];
		$Maske = 'Kriterium';
	}
	
	
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./seminarplan_speichern.php');
		if(isset($_POST['txtSKK_KEY']))
		{
			
			$Maske = 'Kriterium';
			$AWIS_KEY1 = $_POST['txtSEZ_KEY'];
		}
		else 
		{
			$Maske = 'Liste';
			$AWIS_KEY1 = '';
		}
	}
	
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./seminarplan_loeschen.php');
		$Maske = 'Kriterium';
	}
	
	if(isset($_GET['Seite']) and $_GET['Seite'] == 'Kriterium')
	{
		$Maske = 'Kriterium';
		if(isset($_POST['txtSEPKey']))
		{
			$AWIS_KEY1 = $_POST['txtSEPKey'];
		}
	}

	
	$Form->DebugAusgabe(1,$AWIS_KEY1);
	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$Form->Formular_Start();
	
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./seminarplan_Main.php?cmdAktion=Kriterium>");
	
	//********************************************************
	// Suchmaske
	//********************************************************
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_SEMINAR'].':',180);
	$Form->Erstelle_TextFeld('*SKK_SEMINAR',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SKK_SEMINAR']:''),50,300,true);
	$AWISCursorPosition='sucSKK_SEMINAR';
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_VON'].':',180);
	$Form->Erstelle_TextFeld('*SKK_VON',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SKK_VON']:''),20,200,true,'','','','D');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_BIS'].':',180);
	$Form->Erstelle_TextFeld('*SKK_BIS',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SKK_BIS']:''),20,200,true,'','','','D');
	$Form->ZeileEnde();
	
	$Sprache = $AWISBenutzer->BenutzerSprache();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_FRAGE'].':',180);
	$SQL = 'select sef_key,func_textkonserve(sef_bezeichnung,sek_textkonserve,\''.$Sprache.'\') from seminarfragen '.
		   'inner join seminarkatalog on sef_sek_id = sek_key '.
		   'where sef_sef_id <> 0 and sef_status = \'A\''.
		   'order by sef_sortierung';
	$Form->Erstelle_SelectFeld('*SEF_KEY',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SEF_KEY']:''),250,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_BEARB'].':',180);
	$Form->Erstelle_Checkbox('*NurBearbeitung',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['BEARB']:''),50,true,'on');
	$Form->ZeileEnde();
	
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?'on':''),10,true,'on');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/icon_lupe.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W','',18,18);	
	$LinkPDF = '/berichte/drucken.php?XRE=44&ID=';
	$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF.'" onmouseover="urlanpassen(this);"', '/bilder/icon_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'],'','',18,18);
	//$Form->Schaltflaeche('script', 'cmdPdf', 'onmouseover="urlanpassen(this);', '/bilder/icon_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'],'','',18,18);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->SchreibeHTMLCode('<hr noshade width="1200" size="1" align="left" color="grey">');
	$Form->ZeileEnde();
	if($Maske == 'Liste' or $Maske == 'Kriterium')
	{
		if((isset($AWIS_KEY1) and $AWIS_KEY1 == '' and !isset($_GET['SSort']) and $Maske == 'Liste'))
		{
			if(!isset($_GET['Sort']))
			{
				if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
				{
					$ORDERBY = $Param['ORDERBY'];
				}
				else
				{
					$ORDERBY = ' sep_seminar,sef_sortierung';
				}
			}
			else
			{
				$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
			}
	
			
			$SQL = 'select sez_key,sep_seminar,sep_von,sep_bis,see_bezeichnung,func_textkonserve(sef_bezeichnung,sek_textkonserve,\''.$Sprache.'\') as sef_bezeichnung,sez_wert ';
			if($AWIS_KEY1<=0)
			{
					$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
			}
			$SQL .=	' from seminarzuord ' .
					'inner join seminarfeedback on sez_see_key = see_key '.
					'inner join seminarplan on see_sep_key = sep_key '.
					'inner join seminarfragen on sez_sef_id = sef_key '.
					'inner join seminarkatalog on sef_sek_id = sek_key '.
					'where sef_sef_id <> 0 and sef_status = \'A\''.
					'and sef_datenquelle is null '.
					'and sep_bis < sysdate - 56 ';					
			
			if($Bedingung!='')
			{
				$SQL .= ' AND ' . substr($Bedingung,4);
			}
			
			
			$SQL .= ' ORDER BY '.$ORDERBY;
					
			$Block = 1;
			if(isset($_REQUEST['Block']) and !isset($_POST['cmdSuche_x']) and !isset($_POST['cmdSpeichern_x']))
			{
				$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			}
			elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='' and !isset($_POST['cmdSuche_x']) and !isset($_POST['cmdSpeichern_x']))
			{
				$Block = intval($Param['BLOCK']);
			}
			else 
			{
				$Block  = 1;
			}
			
			
			$Form->DebugAusgabe(1,$SQL);
			
			//************************************************
			// Zeilen begrenzen
			//************************************************
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
			/*
			$BlockDB = $Form->Format('N0',($MaxDS / $ZeilenProSeite+1));
			
			var_dump($BlockDB,$Block,$MaxDS,$ZeilenProSeite);
			
			if($Block >= $BlockDB)
			{
				$Block = $BlockDB-1;
			}
			*/
			//*****************************************************************
			// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
			//*****************************************************************
			if($AWIS_KEY1<=0)
			{
				$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
			}
			
			$rsSKK = $DB->RecordSetOeffnen($SQL);
			
			//*****************************************************************
			// Aktuelle Parameter sichern
			//*****************************************************************
			//$Param['ORDERBY']=$ORDERBY;
			//$Param['KEY']=$AWIS_KEY1;
			$Param['BLOCK']=$Block;
			$Param['ORDERBY']=$ORDERBY;
			
			$AWISBenutzer->ParameterSchreiben("SKKSuche",serialize($Param));
			
			if ($Param['SEF_KEY'] != 7 and $Param['SEF_KEY'] != 16 and $Param['SEF_KEY'] != 17 and $Param['SEF_KEY'] != 0)
			{
				$Form->Hinweistext('Diese Frage ben�tigt kein Kriterium!', 1);
			}
			elseif($rsSKK->AnzahlDatensaetze() > 0)
			{
				$Form->ZeileStart();
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=SEP_SEMINAR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_SEMINAR'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_SEMINAR'],$FeldBreiten['SEP_SEMINAR'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=SEE_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEE_BEZEICHNUNG'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_FEEDBACK'],$FeldBreiten['SEE_FEEDBACK'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=SEP_VON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_VON'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_VON'],$FeldBreiten['SKK_VON'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=SEP_BIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_BIS'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_BIS'],$FeldBreiten['SKK_BIS'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=func_textkonserve(sef_bezeichnung,sek_textkonserve,\'DE\')'.((isset($_GET['Sort']) AND ($_GET['Sort']=='func_textkonserve(sef_bezeichnung,sek_textkonserve,\'DE\')'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_FRAGE'],$FeldBreiten['SKK_FRAGE'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):''.'&txtMaske=Liste');
				$Link .= '&Sort=SEZ_WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEZ_WERT'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_ANTWORT'],$FeldBreiten['SKK_ANTWORT'],'',$Link);
				$Form->ZeileEnde();
				
				$SKKZeile=0;
				
				while(!$rsSKK->EOF())
				{
					$Form->ZeileStart();
					$Link = './seminarplan_Main.php?cmdAktion=Kriterium&SEZ_KEY='.$rsSKK->FeldInhalt('SEZ_KEY');
					$Form->Erstelle_ListenFeld('SEP_SEMINAR',$rsSKK->FeldInhalt('SEP_SEMINAR'),0,$FeldBreiten['SEP_SEMINAR'],false,($SKKZeile%2),'',$Link,'T');
					$Form->Erstelle_ListenFeld('SEP_FEEDBACK',$rsSKK->FeldInhalt('SEE_BEZEICHNUNG'),0,$FeldBreiten['SEE_FEEDBACK'],false,($SKKZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('SEP_VON',$rsSKK->FeldInhalt('SEP_VON'),0,$FeldBreiten['SKK_VON'],false,($SKKZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('SEP_BIS',$rsSKK->FeldInhalt('SEP_BIS'),0,$FeldBreiten['SKK_BIS'],false,($SKKZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('SEF_BEZEICHNUNG',$rsSKK->FeldInhalt('SEF_BEZEICHNUNG'),0,$FeldBreiten['SKK_FRAGE'],false,($SKKZeile%2),'','','T');
					$Form->Erstelle_ListenFeld('SEZ_WERT',$rsSKK->FeldInhalt('SEZ_WERT'),0,$FeldBreiten['SKK_ANTWORT'],false,($SKKZeile%2),'','');
					$Form->ZeileEnde();
				
					$rsSKK->DSWeiter();
					$SKKZeile++;
				}
				$Form->ZeileStart();
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium&txtMaske=Liste';
				$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
				$Form->Erstelle_HiddenFeld('Maske', 'Liste');
				$Form->ZeileEnde();
				$Param['BLOCK']=$Block;
				$AWISBenutzer->ParameterSchreiben("SKKSuche",serialize($Param));
			}
			else 
			{
				$Form->Hinweistext($AWISSprachKonserven['Wort']['KeineDatenVorhanden'], 1);
			
			}
		}
		else 
		{
			if(($Maske == 'Kriterium' or (isset($AWIS_KEY1) and $AWIS_KEY1 != '')) and $AWIS_KEY2 == '')
			{	
				$SQL = 'select sez_key,sep_seminar,sep_von,sep_bis,see_bezeichnung,func_textkonserve(sef_bezeichnung,sek_textkonserve,\''.$Sprache.'\') as sef_bezeichnung,sez_wert,sez_abgeschl '; 
				$SQL .=	' from seminarzuord '.
						'inner join seminarfeedback on sez_see_key = see_key '.
						'inner join seminarplan on see_sep_key = sep_key '.
						'inner join seminarfragen on sez_sef_id = sef_key '.
						'inner join seminarkatalog on sef_sek_id = sek_key '.
						'where sef_sef_id <> 0 and sef_status = \'A\''.
						'and sef_datenquelle is null ';
				
				
					
				if($Bedingung!='')
				{
					$SQL .= ' AND ' . substr($Bedingung,4);
				}
				
				$SQL .= 'order by sef_sortierung';
					
					
					
				$rsSEZ = $DB->RecordSetOeffnen($SQL);
				
				
				if(!isset($_GET['SSort']))
				{
					if(isset($Param['ORDERBY2']) AND $Param['ORDERBY2']!='')
					{
						$ORDERBY = $Param['ORDERBY2'];
					}
					else
					{
						$ORDERBY = 'skk_Kriterium1';
					}
				}
				else
				{
					$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['SSort']);
				}
				
				$SQL2 = 'select skk_key, '.
						'case when skk_kriterium1 is null then \'\' else func_textkonserve(\'KRITERIUM1\',\'SKK\',\''.$Sprache.'\',skk_kriterium1 ) end as skk_kriterium1, '.
						'case when skk_kriterium2 is null then \'\' else func_textkonserve(\'KRITERIUM2\',\'SKK\',\''.$Sprache.'\',skk_kriterium2 ) end as skk_kriterium2, '.
						'case when skk_kriterium3 is null then \'\' else func_textkonserve(\'KRITERIUM3\',\'SKK\',\''.$Sprache.'\',skk_kriterium3 ) end as skk_kriterium3, '.
						' skk_user,skk_userdat ';
				$SQL2 .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
				$SQL2 .= ' from seminarkriterium '.
				 		 'where skk_sez_id= '.$AWIS_KEY1;
				
				//************************************************
				// Aktuellen Datenblock festlegen
				//************************************************
				$Block2 = 1;
				if(isset($_REQUEST['Block']))
				{
					$Block2=$Form->Format('N0',$_REQUEST['Block'],false);
				}
				elseif(isset($Param['BLOCK2']) AND $Param['BLOCK2']!='' and $Maske == 'Kriterium')
				{
					$Block2 = intval($Param['BLOCK2']);
				}
				else 
				{
					$Block2 = 1;
				}
				
				
				//************************************************
				// Zeilen begrenzen
				//************************************************
				$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
				$StartZeile = (($Block2-1)*$ZeilenProSeite)+1;
				$MaxDS = $DB->ErmittleZeilenAnzahl($SQL2);
				
			
				
				//*****************************************************************
				// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
				//*****************************************************************
				$SQL2 = 'SELECT * FROM ('.$SQL2.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
				
				
				$Form->DebugAusgabe(1,$SQL2);
				$rsSKK = $DB->RecordSetOeffnen($SQL2);
				
				$Param['ORDERBY2'] = $ORDERBY;
				
				$AWISBenutzer->ParameterSchreiben("SKKSuche",serialize($Param));
				
				// Infozeile zusammenbauen
				$Felder = array();
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./seminarplan_Main.php?cmdAktion=Kriterium&txtMaske=Liste".(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSKK->FeldInhalt('SKK_USER'));
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSKK->FeldInhalt('SKK_USERDAT'));
				$Form->InfoZeile($Felder,'');
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_SEMINAR'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_SEMINAR',$rsSEZ->FeldInhalt('SEP_SEMINAR'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_VON'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_VON',$Form->Format('D',$rsSEZ->FeldInhalt('SEP_VON')),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_BIS'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_BIS',$Form->Format('D',$rsSEZ->FeldInhalt('SEP_BIS')),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_FEEDBACK'].':',180);
				$Form->Erstelle_TextFeld('SKK_FEEDBACK',$rsSEZ->FeldInhalt('SEE_BEZEICHNUNG'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_FRAGE'].':',180);
				$Form->Erstelle_TextFeld('SKK_FRAGE',$rsSEZ->FeldInhalt('SEF_BEZEICHNUNG'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_ANTWORT'].':',180);
				$Form->Erstelle_TextFeld('SKK_ANTWORT',$rsSEZ->FeldInhalt('SEZ_WERT'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->Trennzeile('T');
				$Form->Trennzeile('O');
				
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('new','./seminarplan_Main.php?cmdAktion=Kriterium&SKK_KEY=-1&txtMaske=Kriterium&SEZ_KEY='.$AWIS_KEY1,'g');
				$Form->Erstelle_ListeIcons($Icons,38,-2);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&SSort=SKK_KRITERIUM1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SKK_KRITERIUM1'))?'~':'').'&SEZ_KEY='.$AWIS_KEY1;
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_KRITERIUM1'],$FeldBreiten['SKK_KRITERIUM1'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&SSort=SKK_KRITERIUM2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SKK_KRITERIUM2'))?'~':'').'&SEZ_KEY='.$AWIS_KEY1;
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_KRITERIUM2'],$FeldBreiten['SKK_KRITERIUM2'],'',$Link);
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&SSort=SKK_KRITERIUM3'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SKK_KRITERIUM3'))?'~':'').'&SEZ_KEY='.$AWIS_KEY1;
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SKK']['SKK_KRITERIUM3'],$FeldBreiten['SKK_KRITERIUM3'],'',$Link);
				$Form->ZeileEnde();
				
				
				$DS = 0;
				while(!$rsSKK->EOF())
				{
					$Form->ZeileStart();	
					$Icons = array();
					$Icons[] = array('edit','./seminarplan_Main.php?cmdAktion=Kriterium&txtMaske=Kriterium&SEZ_KEY='.$AWIS_KEY1.'&SKK_KEY='.$rsSKK->FeldInhalt('SKK_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
					$Icons[] = array('delete','./seminarplan_Main.php?cmdAktion=Kriterium&SEZ_KEY='.$AWIS_KEY1.'&Del='.$rsSKK->FeldInhalt('SKK_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'&Seite=Kriterium');
					$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
					$Form->Erstelle_ListenFeld('SKK_KRITERIUM1',$rsSKK->FeldInhalt('SKK_KRITERIUM1'),0,$FeldBreiten['SKK_KRITERIUM1'],false,($DS%2),'','','T');
					$Form->Erstelle_ListenFeld('SKK_KRITERIUM2',$rsSKK->FeldInhalt('SKK_KRITERIUM2'),0,$FeldBreiten['SKK_KRITERIUM2'],false,($DS%2),'','','T');
					$Form->Erstelle_ListenFeld('SKK_KRITERIUM3',$rsSKK->FeldInhalt('SKK_KRITERIUM3'),0,$FeldBreiten['SKK_KRITERIUM3'],false,($DS%2),'','','T');
					$Form->ZeileEnde();
					$rsSKK->DSWeiter();
					$DS++;
				}
				$Link = './seminarplan_Main.php?cmdAktion=Kriterium&txtMaske=Kriterium&SEZ_KEY='.$AWIS_KEY1;
				$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block2,'');
				$Form->Erstelle_HiddenFeld('Maske', 'Kriterium');
				$Form->Erstelle_HiddenFeld('SEZ_KEY', $AWIS_KEY1);
				$Form->ZeileEnde();
				$Param['BLOCK2']=$Block2;
				$AWISBenutzer->ParameterSchreiben("SKKSuche",serialize($Param));
				$Form->Trennzeile('T');
				
				$Form->ZeileStart();
				$Daten = explode('|',$AWISSprachKonserven['SKK']['ABSCHLUSS']);
				$Form->Erstelle_SelectFeld('SKK_ABSCHLUSS',$rsSEZ->FeldInhalt('SEZ_ABGESCHL'),150,true,'','','','','',$Daten);
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$Form->Erstelle_HiddenFeld('SEZ_KEY', $AWIS_KEY1);
			}
			else 
			{
				$SQL3 = 'select sez_key,sep_seminar,sep_von,sep_bis,see_bezeichnung,func_textkonserve(sef_bezeichnung,sek_textkonserve,\''.$Sprache.'\') as sef_bezeichnung,sez_wert,sez_abgeschl ';
				$SQL3 .=	' from seminarzuord '.
						'inner join seminarfeedback on sez_see_key = see_key '.
						'inner join seminarplan on see_sep_key = sep_key '.
						'inner join seminarfragen on sez_sef_id = sef_key '.
						'inner join seminarkatalog on sef_sek_id = sek_key '.
						'where sef_sef_id <> 0 and sef_status = \'A\''.
						'and sef_datenquelle is null ';
				
				
					
				if($Bedingung!='')
				{
					$SQL3 .= ' AND ' . substr($Bedingung,4);
				}
				
				$SQL3 .= 'order by sef_sortierung';
					
					
					
				$rsSEZ = $DB->RecordSetOeffnen($SQL3);
				
				
				$SQL4 = 'select * from seminarkriterium '.
						'where skk_key= '.$AWIS_KEY2;
				
				$rsSKK = $DB->RecordSetOeffnen($SQL4);
				
				// Infozeile zusammenbauen
				$Felder = array();
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href='./seminarplan_Main.php?cmdAktion=Kriterium&txtMaske=Kriterium&SEZ_KEY=".$_REQUEST['SEZ_KEY'].(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'')."' accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSKK->FeldInhalt('SKK_USER'));
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSKK->FeldInhalt('SKK_USERDAT'));
				$Form->InfoZeile($Felder,'');
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_SEMINAR'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_SEMINAR',$rsSEZ->FeldInhalt('SEP_SEMINAR'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_VON'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_VON',$Form->Format('D',$rsSEZ->FeldInhalt('SEP_VON')),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_BIS'].':',180);
				//$Form->Erstelle_TextLabel($rsSEZ->FeldInhalt('SEP_SEMINAR'),500);
				$Form->Erstelle_TextFeld('SKK_BIS',$Form->Format('D',$rsSEZ->FeldInhalt('SEP_BIS')),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_FEEDBACK'].':',180);
				$Form->Erstelle_TextFeld('SKK_FEEDBACK',$rsSEZ->FeldInhalt('SEE_BEZEICHNUNG'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_FRAGE'].':',180);
				$Form->Erstelle_TextFeld('SKK_FRAGE',$rsSEZ->FeldInhalt('SEF_BEZEICHNUNG'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_ANTWORT'].':',180);
				$Form->Erstelle_TextFeld('SKK_ANTWORT',$rsSEZ->FeldInhalt('SEZ_WERT'),50,500,false);
				$Form->ZeileEnde();
				
				$Form->Trennzeile('T');
				$Form->Trennzeile('O');
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_KRITERIUM1'].':',180);
				$Daten = explode('|',$AWISSprachKonserven['SKK']['KRITERIUM1']);
				$Form->Erstelle_SelectFeld('SKK_KRITERIUM1',$rsSKK->FeldInhalt('SKK_KRITERIUM1'),250,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_KRITERIUM2'].':',180);
				$Daten = explode('|',$AWISSprachKonserven['SKK']['KRITERIUM2']);
				$Form->Erstelle_SelectFeld('SKK_KRITERIUM2',$rsSKK->FeldInhalt('SKK_KRITERIUM2'),250,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['SKK']['SKK_KRITERIUM3'].':',180);
				$Daten = explode('|',$AWISSprachKonserven['SKK']['KRITERIUM3']);
				$Form->Erstelle_SelectFeld('SKK_KRITERIUM3',$rsSKK->FeldInhalt('SKK_KRITERIUM3'),250,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				$Form->Trennzeile('O');
				
				$Form->Erstelle_HiddenFeld('SKK_KEY', $AWIS_KEY2);
				$Form->Erstelle_HiddenFeld('SEZ_KEY', $AWIS_KEY1);
				
			}
		}
		
	}
	
$Form->Formular_Ende();
	
		
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();	
		
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		
		if(isset($AWIS_KEY1) and $AWIS_KEY1 != '')
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}
		$Form->SchaltflaechenEnde();
			
		$Form->SchreibeHTMLCode('</form>');

	
		$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031126");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031127");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if(isset($Param['SKK_SEMINAR']) AND $Param['SKK_SEMINAR']!='')
	{
		$Bedingung .= 'AND SEP_SEMINAR ' . $DB->LikeOderIst($Param['SKK_SEMINAR']) . ' ';
	}

	if(isset($Param['SKK_VON']) AND $Param['SKK_VON']!='')
	{
		$Bedingung .= ' AND SEP_VON >= '.$DB->FeldInhaltFormat('D',$Param['SKK_VON'],false);
	}

	if(isset($Param['SKK_BIS']) AND $Param['SKK_BIS']!='')
	{
		$Bedingung .= ' AND SEP_BIS <= '.$DB->FeldInhaltFormat('D',$Param['SKK_BIS'],false);
	}
	if(isset($Param['SEF_KEY']) AND $Param['SEF_KEY']!=0)
	{
		//$Bedingung .= 'AND UPPER(SEP_REGION) ' . $DB->LikeOderIst($Param['SEP_REGION'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'AND SEF_KEY =' . $DB->FeldInhaltFormat('NO',$Param['SEF_KEY']) . ' ';
	}
	if(isset($AWIS_KEY1) AND $AWIS_KEY1 != '')
	{
		//$Bedingung .= 'AND UPPER(SEP_REGION) ' . $DB->LikeOderIst($Param['SEP_REGION'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'AND SEZ_KEY =' . $DB->FeldInhaltFormat('NO',$AWIS_KEY1) . ' ';
	}
	if(isset($Param['BEARB']) AND $Param['BEARB'] != '')
	{
		//$Bedingung .= 'AND UPPER(SEP_REGION) ' . $DB->LikeOderIst($Param['SEP_REGION'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= 'AND SEZ_ABGESCHL = 0 ';
	}
	return $Bedingung;
}



?>