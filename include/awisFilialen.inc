<?php
/**
 * Klasse mit Methoden rund um eine Filiale
 *
 * @author  Sacha Kerres
 * @version 20100801
 */
require_once('awisDatenbank.inc');

class awisFilialen
{
	/**
	 * Zum Rechtepr�fen: LESEN
	 *
	 */
	const FILIAL_INFO_RECHT_LESEN = 1;
	/**
	 * Zum Rechtepr�fen: Schreiben
	 *
	 */
	const FILIAL_INFO_RECHT_BEARBEITEN = 2;

	/**
	 * ATU Nummer f�r die Reifenwechsel - Dienstleistung
	 *
	 */
	const DLP_REIFENWECHSEL = '1EI440';
	/**
	 * Fil_Gruppe f�r die Franchiser
	 *
	 */
	const FRANCH_GRUPPE = '5';
	/**
	 * Datenbank
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * Recordset mit den Filialdaten
	 *
	 * @var awisRecordset
	 */
	private $_rsFIL = null;

	/**
	 * Die aktuell geladene Filiale
	 *
	 * @var int
	 */
	private $_AktuelleFilID = 0;
	

	/**
	 * Liste aller Filialinfos
	 *
	 * @var array
	 */
	private $_FilialInfos=array();

	/**
	 * Liste aller geladener Filialinfotypen
	 *
	 * @var array
	 */
	private $_FilialInfosTypen=array();

	/**
	 * Intialisierung der Klasse
	 *
	 * @param int $FILID
	 */
	public function __construct($FILID=0)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$this->_AWISBenutzer = awisBenutzer::Init();

		$this->_AktuelleFilID = $FILID;

		if($this->_AktuelleFilID!==0)
		{
			$this->_LeseFiliale($FILID);

			$BindeVariablen=array();
			$BindeVariablen['var_N0_fil_id']=$this->_AktuelleFilID;
			$BindeVariablen['var_T_fif_status']='A';

			$SQL ='SELECT FIF_FIT_ID,FIF_IMQ_ID,FIF_WERT, FIF_KEY';
			$SQL .= ' , FilialinfosTypen2.*';
			$SQL .= ' FROM  Filialinfos ';
			$SQL .= ' INNER JOIN FilialinfosTypen2 ON FIF_FIT_ID = FIT_ID';
			$SQL .= ' WHERE FIF_FIL_ID = :var_N0_fil_id';
			$SQL .= ' AND FIT_STATUS = :var_T_fif_status';

			$rsFIF = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
			while(!$rsFIF->EOF())
			{
				$this->_FilialInfos[$rsFIF->FeldInhalt('FIF_FIT_ID')][self::FILIALINFO_FELD_WERT][$rsFIF->FeldInhalt('FIF_IMQ_ID')]=$rsFIF->FeldInhalt('FIF_WERT');
				$this->_FilialInfos[$rsFIF->FeldInhalt('FIF_FIT_ID')][self::FILIALINFO_FELD_KEY][$rsFIF->FeldInhalt('FIF_IMQ_ID')]=$rsFIF->FeldInhalt('FIF_KEY');
				$this->_FilialInfos[$rsFIF->FeldInhalt('FIF_FIT_ID')][self::FILIALINFO_FELD_USER][$rsFIF->FeldInhalt('FIF_IMQ_ID')]=$rsFIF->FeldInhalt('FIF_USER');
				$this->_FilialInfos[$rsFIF->FeldInhalt('FIF_FIT_ID')][self::FILIALINFO_FELD_USERDAT][$rsFIF->FeldInhalt('FIF_IMQ_ID')]=$rsFIF->FeldInhalt('FIF_USERDAT');

				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_FTG_ID']=$rsFIF->FeldInhalt('FIT_FTG_ID');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_XRC_ID']=$rsFIF->FeldInhalt('FIT_XRC_ID');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_STUFEANZEIGE']=$rsFIF->FeldInhalt('FIT_STUFEANZEIGE');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_STUFEBEARBEITEN']=$rsFIF->FeldInhalt('FIT_STUFEBEARBEITEN');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_SORTIERUNG']=$rsFIF->FeldInhalt('FIT_SORTIERUNG');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_FORMAT']=$rsFIF->FeldInhalt('FIT_FORMAT');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_DATENQUELLE']=$rsFIF->FeldInhalt('FIT_DATENQUELLE');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_ZEICHEN']=$rsFIF->FeldInhalt('FIT_ZEICHEN');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_BREITE']=$rsFIF->FeldInhalt('FIT_BREITE');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_SUCHFELD']=$rsFIF->FeldInhalt('FIT_SUCHFELD');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_READONLY']=$rsFIF->FeldInhalt('FIT_READONLY');
				$this->_FilialInfosTypen[$rsFIF->FeldInhalt('FIF_FIT_ID')]['FIT_IMQ_ID']=$rsFIF->FeldInhalt('FIT_IMQ_ID');

				$rsFIF->DSWeiter();
			}
			unset ($rsFIF);
		}
	}

	/**
	 * Liefert die aktuelle Filial-ID
	 *
	 * @return int
	 */
	public function FilialID($FilialID = 0)
	{
		if($FilialID!==0)
		{
			$this->_LeseFiliale($FilialID);
		}
		return $this->_AktuelleFilID;
	}

	/**
	 * Liefert ein Array mit den aktuellen Betreuern der Filiale
	 *
	 * @param int [optional] $FilialID
	 * @param string [optional] $Zeitpunkt
	 * @param int	[optional] $Eindeutig 	Liefert eindeutige Zuordnungen zu einer Rolle
	 * @return array
	 */
	public function BetreuerListe($FilialID = 0, $Zeitpunkt = '', $Eindeutig = true)
	{
		if($FilialID==0)
		{
			$FilialID=$this->_AktuelleFilID;
		}
		if($Zeitpunkt == '')
		{
			$Zeitpunkt = date('d.m.Y'); //SYSDATE';
		}
		else
		{
			//$Zeitpunkt = $this->_DB->FeldInhaltFormat('D',date('d.m.Y',strtotime($Zeitpunkt)));
		}
		$Erg = array();

		$SQL = 'SELECT KON_KEY, KON_NAME1, KON_NAME2, FER_KEY, FER_BEZEICHNUNG, FER_BEMERKUNG, FER_GUELTIGAB, FER_GUELTIGBIS';
		$SQL .= ' , FER_FRB_KEY, FIL_ID, FEZ_FEB_KEY, FRZ_BEMERKUNG, FRZ_GUELTIGAB,FEB_KEY, FEB_EBENE, FIL_LAN_WWSKENN';
		$SQL .= ' FROM Filialen';
		$SQL .= ' LEFT OUTER JOIN Filialebenenrollenzuordnungen ON FRZ_XXX_KEY = FIL_ID AND FRZ_XTN_KUERZEL = \'FIL\' AND FRZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FRZ_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
		$SQL .= ' LEFT OUTER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';	// Kontakt k�nnte gel�scht sein!
		$SQL .= ' LEFT OUTER JOIN FilialEbenenRollen ON FRZ_FER_KEY = FER_KEY AND FER_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FER_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
		$SQL .= ' LEFT OUTER JOIN FilialEbenenZuordnungen ON FEZ_FIL_ID = FIL_ID AND FEZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FEZ_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
		$SQL .= ' LEFT OUTER JOIN FilialEbenen ON FEZ_FEB_KEY = FEB_KEY AND FEB_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FEB_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
		$SQL .= ' WHERE FIL_ID = '.$this->_DB->WertSetzen('FER','N0',$FilialID);
		$SQL .= ' AND FER_ANZEIGEN = 1';			// Nur die sichtbaren Rollen!
        $SQL .= ' AND not (FIL_LAN_WWSKENN = \'BRD\' and FER_KEY = 23 ) '; //In Deutschland keinen RL mehr anzeigen
		$SQL .= ' ORDER BY FER_BEZEICHNUNG, FEB_EBENE DESC';

		$NaechsteEbene='';
		$LetzteRolle = '';
		$LetzteRolleEbene = 0;
		$RollenEbenen = array();

		$rsFER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FER'));

        $FIL_LAN_WWSKENN = $rsFER->FeldInhalt('FIL_LAN_WWSKENN');
		while(!$rsFER->EOF())
		{
		    if($rsFER->FeldInhalt('FER_BEZEICHNUNG')!=$LetzteRolle)
		    {
		        $LetzteRolle = $rsFER->FeldInhalt('FER_BEZEICHNUNG');
		        $LetzteRolleEbene = $rsFER->FeldInhalt('FEB_EBENE');
		    }
		    
			// 28.02.2012 => Nur �bernehmen, wenn die angezeigt werden sollen
//			if($rsFER->FeldInhalt('FER_KEY')!='' AND $rsFER->FeldInhalt('FER_ANZEIGEN')==1)
			if($rsFER->FeldInhalt('FER_KEY')!='')
			{
				//$Erg[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->Datensatz();
				if($Eindeutig==1)
				{
					$Erg[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->Datensatz();
				}
				else
				{
				    if($LetzteRolleEbene != $rsFER->FeldInhalt('FEB_EBENE'))
				    {
				        break;
				    }
					$Erg[]=$rsFER->Datensatz();
					// Merken, dass auf dieser Ebene schon eine Rolle exisitiert
					$RollenEbenen[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->FeldInhalt('FEB_EBENE');
				}
			}
			$NaechsteEbene = $rsFER->FeldInhalt('FEZ_FEB_KEY');

			$rsFER->DSWeiter();
		}
		// Alle Ebenen durchlaufen
		while($NaechsteEbene!='')
		{
			$SQL = 'SELECT KON_KEY, KON_NAME1, KON_NAME2, FER_KEY, FER_BEZEICHNUNG, FER_BEMERKUNG, FER_GUELTIGAB, FER_GUELTIGBIS';
			$SQL .= ' , FER_FRB_KEY, FEZ_FIL_ID AS FIL_ID, FEB_FEB_KEY, FRZ_BEMERKUNG, FRZ_GUELTIGAB, FEB_EBENE';
			$SQL .= ' FROM Filialebenen';
//			$SQL .= ' LEFT OUTER JOIN Filialebenenrollenzuordnungen ON FRZ_XXX_KEY = FEB_KEY AND FRZ_XTN_KUERZEL = \'FEB\'';
			$SQL .= ' LEFT OUTER JOIN Filialebenenrollenzuordnungen ON FRZ_XXX_KEY = FEB_KEY AND FRZ_XTN_KUERZEL = \'FEB\' AND FRZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FRZ_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
			$SQL .= ' LEFT OUTER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';	// Kontakt k�nnte gel�scht sein!
			$SQL .= ' LEFT OUTER JOIN FilialEbenenRollen ON FRZ_FER_KEY = FER_KEY AND FER_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FER_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
			$SQL .= ' LEFT OUTER JOIN Filialebenenzuordnungen ON FEZ_FEB_KEY = FEB_KEY AND FEZ_FIL_ID = '.$this->_DB->WertSetzen('FER','N0',$FilialID).' AND FEZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FEZ_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
			$SQL .= ' WHERE FEB_KEY = '.$this->_DB->WertSetzen('FER','N0',$NaechsteEbene);    //:var_N0_feb_key';
			$SQL .= ' AND FEB_GUELTIGAB <= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt).' AND FEB_GUELTIGBIS >= '.$this->_DB->WertSetzen('FER','D',$Zeitpunkt);
            $SQL .= ' AND FER_ANZEIGEN = 1';			// Nur die sichtbaren Rollen!
            if($FIL_LAN_WWSKENN == 'BRD'){
                $SQL .= ' AND FER_KEY <> 23  '; //In Deutschland keinen RL mehr anzeigen
            }
            $SQL .= ' ORDER BY FER_BEZEICHNUNG';
            
			$rsFER = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FER'));
			
			if($rsFER->EOF())
			{
				break;
			}
			while(!$rsFER->EOF())
			{
				if($rsFER->FeldInhalt('FER_KEY')!='' AND !isset($Erg[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]))
				{
					//$Erg[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->Datensatz();

					if($Eindeutig==1)
					{
						$Erg[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->Datensatz();
					}
					else
					{
					    if(!isset($RollenEbenen[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]) OR $RollenEbenen[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]<=$rsFER->FeldInhalt('FEB_EBENE'))
					    {
                            $Erg[]=$rsFER->Datensatz();
                            $RollenEbenen[$rsFER->FeldInhalt('FER_BEZEICHNUNG')]=$rsFER->FeldInhalt('FEB_EBENE');
					    }
					}
				}
				$NaechsteEbene = $rsFER->FeldInhalt('FEB_FEB_KEY');

				$rsFER->DSWeiter();
			}
		}

		ksort($Erg);

		return $Erg;
	}


	/**
	 * Liefert die Ebenen, in der die Filiale angesiedelt ist
	 *
	 * @param int $FilialID
	 * @param string $Zeitpunkt
	 * @return array()
	 */
	public function Zuordnungen($FilialID = 0, $Zeitpunkt='')
	{
		if($FilialID==0)
		{
			$FilialID=$this->_AktuelleFilID;
		}
		if($Zeitpunkt == '')
		{
			$Zeitpunkt = 'SYSDATE';
		}
		else
		{
			$Zeitpunkt = awis_FeldInhaltFormat('DO',$Zeitpunkt);
		}
		$Erg = array();

		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id']=$FilialID;
		
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG, FEB_BEMERKUNG, FEB_FEB_KEY, FEB_EBENE';
		$SQL .= ' FROM FilialebenenZuordnungen ';
		$SQL .= ' INNER JOIN Filialebenen ON FEZ_FEB_KEY = FEB_KEY  AND FEB_GUELTIGAB <= '.$Zeitpunkt.' AND FEB_GUELTIGBIS >= '.$Zeitpunkt;
		$SQL .= ' WHERE FEZ_FIL_ID = :var_N0_fil_id';
		$SQL .= ' AND FEZ_GUELTIGAB <= '.$Zeitpunkt.' AND FEZ_GUELTIGBIS >= '.$Zeitpunkt;

		$rsFEZ = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if(!$rsFEZ->EOF())
		{
			$Erg[] = $rsFEZ->Datensatz();

			while($rsFEZ->FeldInhalt('FEB_FEB_KEY')!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_feb_key']=$rsFEZ->FeldInhalt('FEB_FEB_KEY');
		
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG,  FEB_BEMERKUNG, FEB_FEB_KEY, FEB_EBENE';
				$SQL .= ' FROM Filialebenen ';
				$SQL .= ' WHERE FEB_KEY = :var_N0_feb_key';
				$SQL .= ' AND FEB_GUELTIGAB <= '.$Zeitpunkt.' AND FEB_GUELTIGBIS >= '.$Zeitpunkt;

				$rsFEZ = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
				if(!$rsFEZ->EOF())
				{
					$Erg[] = $rsFEZ->Datensatz();
				}
			}

		}

		return $Erg;
	}


	public function FilialenEinerEbene($FEB_KEY, $Zeitpunkt='')
	{
		if($Zeitpunkt == '' OR $Zeitpunkt == 'SYSDATE')
		{
			$Zeitpunkt = 'SYSDATE';
		}
		else
		{
			$Zeitpunkt = awis_FeldInhaltFormat('DO',$Zeitpunkt);
		}
		$Erg = array();

		$BindeVariablen=array();
		$BindeVariablen['var_N0_feb_key']=$FEB_KEY;
				
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG, FEB_BEMERKUNG, FEB_FEB_KEY, FEB_EBENE, FEZ_FIL_ID';
		$SQL .= ' FROM Filialebenen ';
		$SQL .= ' LEFT OUTER JOIN FilialebenenZuordnungen ON FEZ_FEB_KEY = FEB_KEY';
		$SQL .= ' WHERE FEB_KEY = :var_N0_feb_key';
		$SQL .= ' AND trunc(FEZ_GUELTIGAB) <= trunc('.$Zeitpunkt.') AND trunc(FEZ_GUELTIGBIS) >= trunc('.$Zeitpunkt;
		$SQL .= ') AND trunc(FEB_GUELTIGAB) <= ('.$Zeitpunkt.') AND (FEB_GUELTIGBIS) >= ('.$Zeitpunkt.')';

		$rsFEB = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		while(!$rsFEB->EOF())
		{
			$Erg[] = $rsFEB->Datensatz();
			$rsFEB->DSWeiter();
		}

		$BindeVariablen=array();
		$BindeVariablen['var_N0_feb_key']=$FEB_KEY;
		
		$SQL = 'SELECT FEB_KEY FROM FilialEbenen';
		$SQL .= ' WHERE FEB_FEB_KEY = :var_N0_feb_key';
		$SQL .= ' AND FEB_GUELTIGAB <= '.$Zeitpunkt.' AND FEB_GUELTIGBIS >= '.$Zeitpunkt;
		$rsFEB = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

		while(!$rsFEB->EOF())
		{
			$Erg = array_merge($Erg, $this->FilialenEinerEbene($rsFEB->FeldInhalt('FEB_KEY'),$Zeitpunkt));

			$rsFEB->DSWeiter();
		}

		return $Erg;
	}


	const FILIALINFO_FELD_WERT = 'Wert';
	const FILIALINFO_FELD_KEY = 'Key';
	const FILIALINFO_FELD_USER = 'User';
	const FILIALINFO_FELD_USERDAT = 'Userdat';
	/**
	 * Liest f�r eine Filiale eine Information aus
	 *
	 * @param mixed $Info
	 * @param int $ImportQuelle
	 * @param string Feld, das geliefert werden soll
	 */
	public function FilialInfo($Info, $ImportQuelle=0, $Feld=self::FILIALINFO_FELD_WERT)
	{
		if(is_numeric($Info))
		{
			if(!isset($this->_FilialInfos[$Info]))
			{
				return false;
			}

			foreach($this->_FilialInfos[$Info][$Feld] AS $Quellen=>$Wert)
			{
//echo $Info.'##'.$Quellen.'**'.$this->_DB->FeldInhaltFormat('N0',$this->_FilialInfosTypen[$Info]['FIT_IMQ_ID'],false).'++';
				if($ImportQuelle==0 AND ($Quellen==$this->_DB->FeldInhaltFormat('N0',$this->_FilialInfosTypen[$Info]['FIT_IMQ_ID'],false)))
				{
					return $Wert;
				}
				elseif($Quellen==$ImportQuelle)
				{
					return $Wert;
				}
				elseif($ImportQuelle==0 AND $this->_DB->FeldInhaltFormat('N0',$this->_FilialInfosTypen[$Info]['FIT_IMQ_ID'],false)==0)
				{
					return $Wert;
				}
			}
		}
		else 		// Feld abfragen
		{
			$Wert = $this->_rsFIL->FeldInhalt($Info);
			return $Wert;
		}

		return false;
	}

	/**
	 * Liefert Infos �ber einen Filialinfotyp
	 *
	 * @param int $Info
	 * @return array
	 */
	public function FilialInfoTyp($Info)
	{
		if(!isset($this->_FilialInfosTypen[$Info]))
		{
			return false;
		}

		return $this->_FilialInfosTypen[$Info];
	}

	/**
	 * Liefert das Recht des aktuellen Benutzers f�r einen InfoTyp zur�ck
	 */
	public function FiliaInfoTypRecht($Info, $RechteArt=self::FILIAL_INFO_RECHT_LESEN)
	{
		if(!isset($this->_FilialInfosTypen[$Info]))
		{
			return false;
		}

		if($this->_FilialInfosTypen[$Info]['FIT_XRC_ID']=='')
		{
			return ($RechteArt==self::FILIAL_INFO_RECHT_LESEN?true:false);
		}
		else
		{
			$Recht = $this->_AWISBenutzer->HatDasRecht($this->_FilialInfosTypen[$Info]['FIT_XRC_ID']);
			$Feld = ($RechteArt==self::FILIAL_INFO_RECHT_LESEN?'FIT_STUFEANZEIGE':'FIT_STUFEBEARBEITEN');
			if(($Recht&$this->_FilialInfosTypen[$Info][$Feld])==$this->_FilialInfosTypen[$Info][$Feld])
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Sucht in einem Text nach Textkonserven ($$NAME~FKT$$) und ersetzt diese durch den Wert.
	 * Als Funktionen sind zul�ssig (L=lower, U=upper)
	 *
	 * @param string $Text
	 */
	public function ErsetzeFilalInfos($Text)
	{
		$Text = str_replace('~~DP~~',':',$Text);

		$Pos = strpos($Text,'$$');
		while($Pos!==false)
		{
			$Ende = strpos($Text,'$$',$Pos+2);
			if($Ende !== false)
			{
				$Variable = substr($Text,$Pos,$Ende-$Pos+2);
				$Felder = substr($Text,$Pos+2,$Ende-$Pos-2);
				$Felder = explode('~',$Felder);
				$Wert = $this->FilialInfo($Felder[0]);

				for($i=0;$i<strlen($Felder[1]);$i++)
				{
					switch ($Felder[1][$i])
					{
						case 'L':
							$Wert = strtolower($Wert);
							break;
						case 'U':
							$Wert = strtoupper($Wert);
							break;
						case 'P':		// Strpad
							// P0L4
							$Zeichen = $Felder[1][++$i];
							$Ausrichtung = $Felder[1][++$i];
							$Laenge = $Felder[1][++$i];
							$Wert = str_pad($Wert,$Laenge,$Zeichen,($Ausrichtung=='L'?STR_PAD_LEFT:STR_PAD_RIGHT));
							break;
					}
				}

				$Text = str_replace($Variable, $Wert, $Text);
				$Pos = strpos($Text,'$$');
			}
			else
			{
				$Pos=false;
			}
		}

		return $Text;
	}


	/**
	 * Liefert ein Array mit Feiertagen, die in diesem Monat anfallen
	 *
	 * @param string $DatumVom
	 * @param string $DatumBis
	 * @return array
	 */
	public function Feiertage($DatumVom='', $DatumBis='')
	{
		$Erg = array();

		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id']=$this->FilialID();
		$BindeVariablen['var_T_lan_code']=$this->FilialInfo('LAN_CODE');
		$BindeVariablen['var_N0_bul_id']=$this->FilialInfo(65,8);
		
		$SQL = 'SELECT *';
		$SQL .= ' FROM Feiertage';
		$SQL .= ' WHERE (XFT_FIL_ID = :var_N0_fil_id';
		$SQL .= ' OR XFT_LAN_CODE = :var_T_lan_code';
		$SQL .= ' OR XFT_BUL_ID = :var_N0_bul_id';
		$SQL .= ') AND (';
		$SQL .= ' XFT_TAG >= '.$this->_DB->FeldInhaltFormat('D',($DatumVom==''?date('d.m.Y'):$DatumVom),false);
		$SQL .= ' AND XFT_TAG <= '.$this->_DB->FeldInhaltFormat('D',($DatumBis==''?date('d.m.Y'):$DatumBis),false);
		$SQL .= ')';
		$SQL .= ' ORDER BY XFT_TAG';
		
		$rsXFT = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
		$Ausnahmen = explode(',',$rsXFT->FeldInhalt('XFT_FILAUSNAHME'));
	
		while(!$rsXFT->EOF())
		{
			if(array_search($this->FilialID(), $Ausnahmen)===false)
			{
				$Erg[$rsXFT->FeldInhalt('XFT_KEY')]['Datum']=awisFormular::Format('D',$rsXFT->FeldInhalt('XFT_TAG'));
				$Erg[$rsXFT->FeldInhalt('XFT_KEY')]['Bezeichnung']=$rsXFT->FeldInhalt('XFT_BEZEICHNUNG');
			}
			$rsXFT->DSWeiter();
		}

		return $Erg;
	}

	/**
	 * Entfernungen - liefert ein Array mit Entfernungsangaben zwischen 2 Filialen
	 * @param int $FilVon
	 * @param int $FilNach
	 * @return array ('von','nach','entfernung','zeit','kosten')
	 */
	public function FilialEntfernung($FilVon, $FilNach)
	{
		$Erg = array('von'=>'', 'nach'=>'', 'entfernung'=>-1, 'zeit'=>-1, 'kosten'=>-1);

		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id_von']=$FilVon;
		$BindeVariablen['var_N0_fil_id_nach']=$FilNach;

		$SQL = 'SELECT FEG_ENTFERNUNG, FEG_FAHRZEIT, FEG_KOSTEN';
		$SQL .= ' FROM FILIALENENTFERNUNGEN';
		$SQL .= ' WHERE (FEG_FIL_ID_VON = :var_N0_fil_id_von';
		$SQL .= ' OR FEG_FIL_ID_NACH = :var_N0_fil_id_von)';
		$SQL .= ' AND (FEG_FIL_ID_VON = :var_N0_fil_id_nach';
		$SQL .= ' OR FEG_FIL_ID_NACH = :var_N0_fil_id_nach)';

		$rsDaten = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if(!$rsDaten->EOF())
		{
			$Erg['von']=$FilVon;
			$Erg['nach']=$FilNach;
			$Erg['entfernung']=$rsDaten->FeldInhalt('FEG_ENTFERNUNG');
			$Erg['zeit']=$rsDaten->FeldInhalt('FEG_FAHRZEIT');
			$Erg['kosten']=$rsDaten->FeldInhalt('FEG_KOSTEN');
		}

		return $Erg;
	}

	/**
	 * Daten f�r eine Filiale einlesen
	 *
	 */
	private function _LeseFiliale($FilID)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_T_dpf_dpr_nummer']=self::DLP_REIFENWECHSEL;
		$BindeVariablen['var_N0_fil_id']=$FilID;

		$SQL = 'SELECT Filialen.* ';
		$SQL .= ', CASE WHEN FIL_LAGERKZ=\'L\' THEN \'Werl\' ELSE \'Weiden\' END AS FIL_LAGER';
		$SQL .= ', LAN_CODE, LAN_LAND';
		$SQL .= ',(SELECT DPF_VK FROM dienstleistungspreisefilialen where dpf_dpr_nummer = :var_T_dpf_dpr_nummer and dpf_fil_id = FIL_ID) AS DPF_REIFENWECHEL';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';

		$SQL .= ' WHERE FIL_ID = :var_N0_fil_id';


		$this->_rsFIL = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

		$this->_AktuelleFilID = $this->_rsFIL->FeldInhalt('FIL_ID');
	}
	
	/**
	 * Liefert zur�ck ob eine Filiale den Filialbestand sehen darf oder nicht.
	 * M�glichkeit optional mit Franchiser oder ohne.
	 *
	 * @param int $FilID
	 * @param int $FilFranch
     *
	 * @return boolean
	 */
	public function AnzFilBestand($FilID,$FilFranch=false)
	{
		$this->_DB->SetzeBindevariable('FIL','var_T_FRANCH',self::FRANCH_GRUPPE);
		$this->_DB->SetzeBindevariable('FIL','var_N0_fil_id',$FilID);
	
		$SQL = 'SELECT LAN_CODE ';
		$SQL .= 'FROM Filialen ';
		$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		$SQL .= ' WHERE FIL_ID = :var_N0_fil_id';
		$SQL .= ' AND (LAN_CODE != \'DE\'';
	
		if (!$FilFranch)
		{
			$SQL .= ' OR FIL_GRUPPE = :var_T_FRANCH';
		}
			
		$SQL .= ')';

		$rsLAN = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FIL'));
		if ($rsLAN->AnzahlDatensaetze() == 1)
		{
			return true;
		}
		else 
		{
			return false;
		}
		
		/*
		$SQL = 'SELECT LAN_CODE ';
		$SQL .= 'FROM Filialen ';
		$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		$SQL .= ' WHERE FIL_ID = '.$this->_DB->WertSetzen('FIL','N0', $FilID);
		$SQL .= ' AND (LAN_CODE != '.$this->_DB->WertSetzen('FIL','T', 'DE');
		
		if (!$FilFranch)
		{
			$SQL .= ' OR FIL_GRUPPE = '.$this->_DB->WertSetzen('FIL', 'T', self::FRANCH_GRUPPE);
		}
			
		$SQL .= ')';
		
		$rsLAN = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FIL'));
		*/
		
	}
	
	/**
	 * Liefert die n�chsten Lacktage f�r eine Filiale
	 * @param int $FILID
	 * @param int $Anzahl
	 */
	public function NaechsteLacktage($FILID, $Anzahl=1)
	{
	    $SQL = 'SELECT FIF_WERT';
	    $SQL .= ' FROM FILIALINFOS';
	    $SQL .= ' WHERE FIF_FIL_ID = '.$this->_DB->WertSetzen('FIF', 'N0', $FILID);
	    $SQL .= ' AND FIF_FIT_ID = '.$this->_DB->WertSetzen('FIF', 'N0', 851);
	    
	    $rsFIF = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FIF'));
	   
	    //PG 20151104
	    //�nderung ab OR: Bei Auswahl unregelm��ig o. Kein SR, kommt eine 0. 
	    if($rsFIF->EOF() or $rsFIF->FeldInhalt('FIF_WERT') == '0')
	    {
	        return false;
	    }
	    
	    $StartTag = date('d.m.Y');
	    $StartWochenTag = (int)date('w');
	    
	    $NaechsterTag = strtotime($StartTag);
	    
	    $Tage = array();
	    $AnzTage = 0;
	    $LackTage = array();

	    // Wochentage f�r die Lacktage bestimmen
	    for($Tag=0;$Tag<=7;$Tag++)
	    {
	        if((int)$rsFIF->FeldInhalt('FIF_WERT')&(pow(2,$Tag)))
	        {
	            $LackTage[$Tag+1]='';
	        }
	    }
	    
  
	    // Jetzt die n�chsten Tage suchen
	    while($AnzTage<$Anzahl+2)
	    {
	        
    	    for($Tag=$StartWochenTag;$Tag<=7;$Tag++)
    	    {
    	        $WochenTag = date('w',$NaechsterTag);        // Sonntag = 0
                
                if(isset($LackTage[$WochenTag]))
                {
                    // Pr�fen, ob ein Feiertag vorliegt
                    $FeierTage = $this->Feiertage(date('d.m.Y',$NaechsterTag),date('d.m.Y',$NaechsterTag));
                    while(!empty($FeierTage))
                    {
                        $NaechsterTag = strtotime(date('d.m.Y',$NaechsterTag).' + 1 week');
                        $FeierTage = $this->Feiertage(date('d.m.Y',$NaechsterTag),date('d.m.Y',$NaechsterTag));
                    }
                    
                    $Tage[$AnzTage++] = $NaechsterTag;
                }

                $NaechsterTag = $NaechsterTag+(60*60*24);
    	    }
    	    
    	    $StartWochenTag=0;
	    }
	     
	    sort($Tage);
	    return array_slice($Tage,0,$Anzahl);
	}
	
}
?>