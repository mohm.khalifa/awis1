<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

//Werden ben�tigt f�r EAN
require_once('register.inc.php');
require_once('db.inc.php');
require_once('sicherheit.inc.php');

require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('RFK','RFK%');
	$TextKonserven[]=array('RFK','RueckfuehrungNichtAbgeschlossen');
	$TextKonserven[]=array('RFK','RueckfuehrungKeineDifferenzen');
	$TextKonserven[]=array('RFK','lst_RFK_BOXART');
	$TextKonserven[]=array('RFF','RFF%');
	$TextKonserven[]=array('ASP','ASP_BEZEICHNUNG');
	$TextKonserven[]=array('Ausdruck','txtUeberschriftR�ckf�hrungs%');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	$TextKonserven[]=array('Wort','Rueckfuehrungstyp');
	$TextKonserven[]=array('ADK','%');		
	$TextKonserven[]=array('ADS','%');		
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Wort','Absender');
	$TextKonserven[]=array('Wort','Empfaenger');
	$TextKonserven[]=array('Wort','Datum');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht3920 = $AWISBenutzer->HatDasRecht(3920);
	if($Recht3920==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');
	
	$DruckArt = 1;		// Standard: R�ckf�hrungsliste
	
	if(isset($_GET['DruckArt']) AND $_GET['DruckArt']==1)
	{	
		$DruckArt=1;								// R�ckf�hrungsliste drucken
		$Ausgabe='R�ckf�hrungsliste';
	}
	elseif(isset($_GET['DruckArt']) AND $_GET['DruckArt']==2)
	{
		$DruckArt=2;								// R�ckf�hrungslieferschein drucken
		$Ausgabe='R�ckf�hrungslieferschein';
	}
	elseif(isset($_GET['DruckArt']) AND $_GET['DruckArt']==3)
	{
		$DruckArt=3;								// R�ckf�hrungsdifferenzen drucken
		$Ausgabe='R�ckf�hrungsdifferenzen';
	}
	
	if(isset($_GET['RFK_KEY']))
	{
		$AWIS_KEY1 = floatval($_GET['RFK_KEY']);
	}
		
	$Spalte = 20;
	$Zeile = 20;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,$AWISSprachKonserven['Ausdruck']['txtUeberschrift'.$Ausgabe]);
	
	$Zeile=_Erzeuge_Seite_Rueckfuehrung();
	
	$DB->SetzeBindevariable('RFK','var_N0_rfk_key','0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
	$DB->SetzeBindevariable('RFK','var_T_sprache',$AWISBenutzer->BenutzerSprache(),awisDatenbank::VAR_TYP_TEXT);
	
	$SQL = 'SELECT RFK_KEY, RFF_AST_ATUNR, FUNC_ASP_BEZEICHNUNG(rff_ast_atunr,:var_T_sprache) AS ASP_BEZEICHNUNG,';
	$SQL .= ' RFF_MENGEFILIALE, RFF_MENGEZENTRALE, RFF_DATUMSCANFILIALE, RFS_STATUS, RFF_RUECKLIEFKZ';
	$SQL .= ' FROM RueckFuehrungsfilialmengenKopf';
	$SQL .= ' LEFT OUTER JOIN RueckFuehrungsFilialmengenpos ON rff_rfk_key = rfk_key';
	$SQL .= ' LEFT OUTER JOIN RueckFuehrungsStatus ON RFS_KEY = RFK_RFS_KEY';
	$SQL .= ' WHERE RFK_KEY = :var_N0_rfk_key';
	
	if ($DruckArt == 3)
	{
		$SQL .= ' AND RFF_MENGEFILIALE <> RFF_MENGEZENTRALE';
	}	
	
	$SQL .= ' ORDER BY RFF_AST_ATUNR';
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsRFK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
	$rsRFKZeilen = $rsRFK->AnzahlDatensaetze();
	
	if($DruckArt == 3 and $rsRFKZeilen == 0)
	{
		$Form->Hinweistext($AWISSprachKonserven['RFK']['RueckfuehrungKeineDifferenzen']);
		die();
	}	
	
	if(($DruckArt == 2 and $DB->FeldInhaltFormat('Z', $rsRFK->FeldInhalt('RFS_STATUS'))<31) or 	
	   ($DruckArt == 3 and $DB->FeldInhaltFormat('Z', $rsRFK->FeldInhalt('RFS_STATUS'))<32))
	{
		$Form->Hinweistext($AWISSprachKonserven['RFK']['RueckfuehrungNichtAbgeschlossen']);
		die();
	}	
			
	for($RFKZeile=0;$RFKZeile<$rsRFKZeilen;$RFKZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-30)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Rueckfuehrung();
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);
	
		if ($DruckArt == 2 and ($rsRFK->FeldInhalt('RFF_RUECKLIEFKZ') == 'L' or $rsRFK->FeldInhalt('RFF_RUECKLIEFKZ') == 'S'))
		{			
			$Ausdruck->_pdf->SetFont('Arial','B',8);			
		}
		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->Cell(15,6,$rsRFK->FeldInhalt('RFF_AST_ATUNR'),1,0,'L',0);		
		
		$Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->Cell(90,6,$rsRFK->FeldInhalt('ASP_BEZEICHNUNG'),1,0,'L',0);
		
		if ($DruckArt == 1 or $DruckArt == 2)
		{
			$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
			$Ausdruck->_pdf->Cell(20,6,$Form->Format('D', $rsRFK->FeldInhalt('RFF_DATUMSCANFILIALE')),1,0,'L',0);
			
			$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
			$Ausdruck->_pdf->Cell(26,6,$rsRFK->FeldInhalt('RFF_MENGEFILIALE'),1,0,'L',0);							
			
			if ($DruckArt == 2)
			{
				$Ausdruck->_pdf->SetXY($Spalte+151,$Zeile);
				$Ausdruck->_pdf->Cell(25,6,$rsRFK->FeldInhalt('RFF_RUECKLIEFKZ'),1,0,'L',0);							
			}
		}		
						
		elseif ($DruckArt == 3)
		{
			$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
			$Ausdruck->_pdf->Cell(30,6,$rsRFK->FeldInhalt('RFF_MENGEFILIALE'),1,0,'L',0);							
			
			$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
			$Ausdruck->_pdf->Cell(30,6,$rsRFK->FeldInhalt('RFF_MENGEZENTRALE'),1,0,'L',0);							
		}
		
		if ($DruckArt == 2 and ($rsRFK->FeldInhalt('RFF_RUECKLIEFKZ') == 'L' or $rsRFK->FeldInhalt('RFF_RUECKLIEFKZ') == 'S'))
		{			
			$Ausdruck->_pdf->SetFont('Arial','',8);			
		}
		
		$Zeile+=6;
		
		$rsRFK->DSWeiter();		
	}
	
	//Bei R�ckf�hrungslieferschein pr�fen, ob Gasflaschen mit vorhanden sind
	if ($DruckArt==2)
	{
		//Pr�fen, ob Filiale das Recht hat, das Bef�rderungspapier (Gasflaschen zu drucken)
		if($AWISBenutzer->HatDasRecht(4302)!=0)
		{
			$DB->SetzeBindevariable('ADR','var_N0_adk_rfk_key','0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
			$SQL = 'SELECT * FROM ADRKOPF WHERE ADK_RFK_KEY = :var_N0_adk_rfk_key';
			$rsADK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ADR'));

			//Lieferschein - Bef�rderungspapier erzeugen
			if ($rsADK->AnzahlDatensaetze()>0)
			{																	
				$AWIS_KEY2=$rsADK->FeldInhalt('ADK_KEY');	
					
				//F�r Fahrer
				$Spalte = 20;
				$Zeile = 20;															
				$Zeile=_Erzeuge_Seite_Lieferschein(1);													
				$Zeile=_Erzeuge_Block_Lieferschein($Zeile+3, 1);
			}

			//Pr�fen, ob ein LQ-Bef�rderungspapier erzeugt werden muss,
            $SQL  ='select';
            $SQL .='     a.RFF_AST_ATUNR,';
            $SQL .='     a.RFF_MENGEFILIALE,';
            $SQL .='     d.AST_BEZEICHNUNGWW,';
            $SQL .='     c.ASI_WERT as RFF_GEWICHTBRUTTO';
            $SQL .=' from';
            $SQL .='     RUECKFUEHRUNGSFILIALMENGENPOS a';
            $SQL .='     inner join ARTIKELSTAMMINFOS B on';
            $SQL .='         a.RFF_AST_ATUNR = B.ASI_AST_ATUNR';
            $SQL .='     and';
            $SQL .='         B.ASI_AIT_ID = 192 and asi_wert = \'J\' ';
            $SQL .='     left join ARTIKELSTAMMINFOS c on';
            $SQL .='         a.RFF_AST_ATUNR = c.ASI_AST_ATUNR';
            $SQL .='     and';
            $SQL .='         c.ASI_AIT_ID = 182';
            $SQL .= '    inner join ARTIKELSTAMM d on';
            $SQL .= '        d.AST_ATUNR = RFF_AST_ATUNR ';
            $SQL .= ' where ';
            $SQL .= ' RFF_RFK_KEY = '.$DB->WertSetzen('RFF','Z',$AWIS_KEY1);

            $rsLQ = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFF'));

            if($rsLQ->AnzahlDatensaetze()>0){
                _Erzeuge_LQBefoerderungspapier($rsLQ);
            }
		}
	}	
	$Ausdruck->Anzeigen();

}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


/**
*
* Funktion erzeugt die Standard-Ansicht der PDF (Kopf, Fuss, Seite, usw.)
*
* @name _Erzeuge_Seite_Rueckfuehrung
* @version 0.1
* @author Thomas Riedl
* @return int (Zeile)
*
*/
function _Erzeuge_Seite_Rueckfuehrung()
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;
	global $Form;
	global $Ausdruck;
	global $Ausgabe;
	global $DruckArt;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 20;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);

	if ($Seite == 1)
	{
		// �berschrift
		$DB->SetzeBindevariable('RFK','var_N0_rfk_key','0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
		
		$SQL = 'SELECT RFK_KEY, RFK_FIL_ID, RFK_RUECKFUEHRUNGSNR, RFK_BEMERKUNGFILIALE, RFK_BOXART, RFK_BOXEAN';
		$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
		$SQL.= ' WHERE RFK_KEY=:var_N0_rfk_key';
		$rsPDFKopf = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
		$rsPDFKopfZeilen = $rsPDFKopf->AnzahlDatensaetze();
		
		$BoxText = explode("|",$AWISSprachKonserven['RFK']['lst_RFK_BOXART']);
		
		foreach ($BoxText as $Box)
		{
			$Artbox = explode("~",$Box);
			if (intval($Artbox[0])==intval($rsPDFKopf->FeldInhalt('RFK_BOXART')))
			{
				$Gitterbox = $Artbox[1];
			}				
		}
				
		if($rsPDFKopfZeilen>0)
		{
			$Ausdruck->_pdf->SetFont('Arial','B',14);			
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);			
			$Ausdruck->_pdf->Cell(180,6,$AWISSprachKonserven['Ausdruck']['txtUeberschrift'.$Ausgabe],0,0,'L',0);
			$Zeile+=15;			
			$Ausdruck->_pdf->SetFont('Arial','',10);
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['RFK']['RFK_FIL_ID'],0,0,'L',0);
			$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);
			$Ausdruck->_pdf->Cell(40,6,$rsPDFKopf->FeldInhalt('RFK_FIL_ID'),0,0,'L',0);
			$Zeile+=6;
			
			if ($DruckArt == 2 or $DruckArt == 3)
			{
				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
				$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['RFK']['RFK_RUECKFUEHRUNGSNR'],0,0,'L',0);
				$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);	
				$Ausdruck->_pdf->Cell(30,6,$rsPDFKopf->FeldInhalt('RFK_RUECKFUEHRUNGSNR'),0,0,'L',0);
				
				if ($DruckArt == 2)
				{			
					$Ausdruck->_pdf->Image(Erzeuge_EAN13(substr($rsPDFKopf->FeldInhalt('RFK_RUECKFUEHRUNGSNR'),0,12),1),$Spalte+75,$Zeile-8,'','','png');	
					$Ausdruck->_pdf->SetXY($Spalte+78,$Zeile+2);
					$Ausdruck->_pdf->SetFont('Arial','B',8);
					$Ausdruck->_pdf->SetCharSpacing(1,0);
					$Ausdruck->_pdf->Cell(30,6,$rsPDFKopf->FeldInhalt('RFK_RUECKFUEHRUNGSNR'),0,0,'L',0);	
					$Ausdruck->_pdf->SetFont('Arial','',10);
					$Ausdruck->_pdf->SetCharSpacing(0,0);
				}
				$Zeile+=6;			
				
				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
				$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['RFK']['RFK_BEMERKUNGFILIALE'],0,0,'L',0);
				$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);	
				$Ausdruck->_pdf->Cell(30,6,$rsPDFKopf->FeldInhalt('RFK_BEMERKUNGFILIALE'),0,0,'L',0);				
				$Zeile+=6;							
				
				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
				$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['RFK']['RFK_BOXART'],0,0,'L',0);
				$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);	
				$Ausdruck->_pdf->Cell(30,6,$Gitterbox,0,0,'L',0);				
				$Zeile+=6;							
				
				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
				$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['RFK']['RFK_BOXEAN'],0,0,'L',0);
				$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);	
				$Ausdruck->_pdf->Cell(30,6,$rsPDFKopf->FeldInhalt('RFK_BOXEAN'),0,0,'L',0);				
				$Zeile+=6;			
			}
			
		}
		else{
			$Zeile+=42;
		}
	}
	else 
	{
		$Zeile+=15;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(15,6,$AWISSprachKonserven['RFF']['RFF_AST_ATUNR'],1,0,'L',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);	
	$Ausdruck->_pdf->Cell(90,6,$AWISSprachKonserven['ASP']['ASP_BEZEICHNUNG'],1,0,'L',1);
			
	if ($DruckArt == 1 or $DruckArt == 2)
	{
		$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->Cell(20,6,$AWISSprachKonserven['RFF']['RFF_DATUMSCANFILIALE'],1,0,'L',1);
		
		$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
		$Ausdruck->_pdf->Cell(26,6,$AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'],1,0,'L',1);
		
		if ($DruckArt == 2)
		{
			$Ausdruck->_pdf->SetXY($Spalte+151,$Zeile);
			$Ausdruck->_pdf->Cell(25,6,$AWISSprachKonserven['Wort']['Rueckfuehrungstyp'],1,0,'L',1);
		}
	}
				
	elseif($DruckArt == 3)
	{
		$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->Cell(30,6,$AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'],1,0,'L',1);
		
		$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
		$Ausdruck->_pdf->Cell(30,6,$AWISSprachKonserven['RFF']['RFF_MENGEZENTRALE'],1,0,'L',1);
	}
	
	$Zeile+=6;
	
	return $Zeile;
}

function _Erzeuge_LQBefoerderungspapier(awisRecordset $rsLQ){

    global $AWIS_KEY2;
    global $AWISSprachKonserven;
    global $Spalte;
    global $Seite;
    global $Ausdruck;
    global $Form;

    $AWISBenutzer = awisBenutzer::Init();
    $Gesamtgewicht = 0.00;
    $Gesamtmenge = 0.00;
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $TextKonserven[]=array('ADK','%');
    $TextKonserven[]=array('ADS','%');
    $AWISSprachKonservenADS = $Form->LadeTexte($TextKonserven);

    $Ausdruck->NeueSeite(0,1);
    $Seite++;
    $Ausdruck->_pdf->SetFillColor(210,210,210);
    $Zeile = 10;

   
    $Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);

    $DB->SetzeBindevariable('ADR','var_N0_adk_key','0'.$AWIS_KEY2,awisDatenbank::VAR_TYP_GANZEZAHL);
    $SQL = 'SELECT * FROM ADRKOPF WHERE ADK_KEY=:var_N0_adk_key';
    $rsADKKopf = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ADR'));

    $Ausdruck->_pdf->SetFont('Arial','B',14);
    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(40,6,$AWISSprachKonservenADS['ADS']['LQ_PDF_UEBERSCHRIFT'],0,0,'L',0);
    $Zeile+=10;

    $Ausdruck->_pdf->SetFont('Arial','',10);

    $Zeile+=10;
    $Ausdruck->_pdf->setXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(90,8,$AWISSprachKonserven['Wort']['Datum'] . ': '. date('d.m.Y'),0,0,'L',0);
    $Ausdruck->_pdf->Cell(20,8,$AWISSprachKonservenADS['ADS']['LQ_PDF_KFZ'] . ': '. $AWISSprachKonservenADS['ADS']['LQ_PDF_WENU'] . ' _______',0,0,'L',0);


    $Zeile+=10;

        $DB->SetzeBindevariable('FIL','var_N0_fil_id',$rsADKKopf->FeldInhalt('ADK_FIL_ID'),awisDatenbank::VAR_TYP_GANZEZAHL);

        $SQL = 'SELECT FIL_ID, FIL_STRASSE, FIL_PLZ, FIL_ORT FROM FILIALEN WHERE FIL_ID = :var_N0_fil_id';
        $rsFil = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL',false));

        $SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=912 AND FIF_FIL_ID = :var_N0_fil_id';
        $rsFilinfoName1 = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL',false));

        $SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=913 AND FIF_FIL_ID = :var_N0_fil_id';
        $rsFilinfoName2 = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL'));

        //Weiden
        if ($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='N')
        {
            $Empf_Name = 'ESTATO';
            $Empf_Form = 'Umweltservice GmbH';
            $Empf_Strasse = 'Dr.-Kilian-Str. 11';
            $Empf_Ort = '92637 Weiden';
            $Empf_Telefon = 'Telefon (0961) 306-0';
        }
        //Werl
        elseif (($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='L'))
        {
            $Empf_Name = 'ESTATO';
            $Empf_Form = 'Umweltservice GmbH';
            $Empf_Strasse = 'Hansering 2';
            $Empf_Ort = '59457 Werl';
            $Empf_Telefon = 'Telefon (02922) 807-4400';
        }

        //Absender
        $Ausdruck->_pdf->SetFont('Arial','',12);
        $Ausdruck->_pdf->setXY($Spalte,$Zeile);
        $Ausdruck->_pdf->cell(22,6,$AWISSprachKonservenADS['ADS']['Absender'].':',0,0,'L',0);
        $Ausdruck->_pdf->cell(50,6,$rsFilinfoName1->FeldInhalt('FIF_WERT'),0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+22,$Zeile);
        $Ausdruck->_pdf->cell(50,6,$rsFilinfoName2->FeldInhalt('FIF_WERT'),0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+22,$Zeile);
        $Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_STRASSE'),0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+22,$Zeile);
        $Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_PLZ').' '.$rsFil->FeldInhalt('FIL_ORT'),0,0,'L',0);
        $Zeile+=5;

        //Empf�nger
        $Zeile-=20;
        $Ausdruck->_pdf->SetXY($Spalte+90,$Zeile);
        $Ausdruck->_pdf->cell(25,6,$AWISSprachKonservenADS['ADS']['Empfaenger'].':',0,0,'L',0);
        $Ausdruck->_pdf->cell(50,6,$Empf_Name,0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
        $Ausdruck->_pdf->cell(60,6,$Empf_Form,0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
        $Ausdruck->_pdf->cell(50,6,$Empf_Strasse,0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
        $Ausdruck->_pdf->cell(50,6,$Empf_Ort,0,0,'L',0);
        $Zeile+=5;

        $Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
        $Ausdruck->_pdf->cell(30,6,$Empf_Telefon,0,0,'L',0);
        $Zeile+=15;

    // Listen�berschrift

    $Ausdruck->_pdf->SetFont('Arial','B',8);
    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(15,6,$AWISSprachKonserven['RFF']['RFF_AST_ATUNR'],1,0,'L',1);

    $Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);
    $Ausdruck->_pdf->Cell(90,6,$AWISSprachKonserven['ASP']['ASP_BEZEICHNUNG'],1,0,'L',1);


    $Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
    $Ausdruck->_pdf->Cell(20,6,$AWISSprachKonserven['Wort']['Menge'],1,0,'L',1);

    $Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
    $Ausdruck->_pdf->Cell(20,6,$AWISSprachKonserven['ADS']['LG_PDF_BRUTTOINKG'],1,0,'L',1);


    $Ausdruck->_pdf->SetXY($Spalte+145,$Zeile);
    $Ausdruck->_pdf->Cell(25,6,$AWISSprachKonserven['ADS']['LQ_LANG'],1,0,'L',1);

    $Zeile+=6;
    $Ausdruck->_pdf->SetFont('Arial','',8);
    while(!$rsLQ->EOF()){

        $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
        $Ausdruck->_pdf->Cell(15,6,$rsLQ->FeldInhalt('RFF_AST_ATUNR'),1,0,'L',0);

        $Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);
        $Ausdruck->_pdf->Cell(90,6,$rsLQ->FeldInhalt('AST_BEZEICHNUNGWW'),1,0,'L',0);


        $Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
        $Ausdruck->_pdf->Cell(20,6,$rsLQ->FeldInhalt('RFF_MENGEFILIALE'),1,0,'L',0);
        $Menge = str_replace(',','.',$rsLQ->FeldInhalt('RFF_MENGEFILIALE','N2'));
        $Gesamtmenge += $Menge;


        $Gewicht = $Menge * $rsLQ->FeldInhalt('RFF_GEWICHTBRUTTO','N2');
        $Gesamtgewicht += $Gewicht;
        $Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
        $Ausdruck->_pdf->Cell(20,6,str_replace(',','.',$rsLQ->FeldInhalt('RFF_MENGEFILIALE','N2'))*str_replace(',','.',$rsLQ->FeldInhalt('RFF_GEWICHTBRUTTO','N2')),1,0,'L',0);


        $Ausdruck->_pdf->SetXY($Spalte+145,$Zeile);
        $Ausdruck->_pdf->Cell(25,6,'LQ',1,0,'C',0);

        $rsLQ->DSWeiter();
        $Zeile+=6;

        if($Zeile >= $Ausdruck->SeitenHoehe()-50){
            $Ausdruck->NeueSeite();
            $Zeile = 20;
        }

    }

    $Ausdruck->_pdf->SetFont('Arial','B',8);
    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(105,6,'',1,0,'L',1);


    $Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
    $Ausdruck->_pdf->Cell(20,6,$Gesamtmenge,1,0,'L',1);

    $Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
    $Ausdruck->_pdf->Cell(20,6,$Gesamtgewicht,1,0,'L',1);

    $Ausdruck->_pdf->SetXY($Spalte+145,$Zeile);
    $Ausdruck->_pdf->Cell(25,6,'',1,0,'L',1);

    //Fu�zeile
    $Zeile=$Zeile+25;
    $Spalte+=20;
    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+55,$Zeile);
    $Ausdruck->_pdf->SetXY($Spalte+13,$Zeile);
    $Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Unterschrift_Absender'],0,0,'L',0);

    $Ausdruck->_pdf->SetXY($Spalte+60,$Zeile);
    $Ausdruck->_pdf->Line($Spalte+60,$Zeile,$Spalte+115,$Zeile);
    $Ausdruck->_pdf->SetXY($Spalte+73,$Zeile);
    $Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Unterschrift_Fahrer'],0,0,'L',0);

    $Zeile += 20;


    return $Zeile;
}

function _Erzeuge_Seite_Lieferschein($LieferscheinSeite)
{
	global $AWIS_KEY2;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$AWISBenutzer = awisBenutzer::Init();
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
			
	$TextKonserven[]=array('ADK','%');	
	$TextKonserven[]=array('ADS','%');	
	$AWISSprachKonservenADS = $Form->LadeTexte($TextKonserven);
	
	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	$DB->SetzeBindevariable('ADR','var_N0_adk_key','0'.$AWIS_KEY2,awisDatenbank::VAR_TYP_GANZEZAHL);
	$SQL = 'SELECT * FROM ADRKOPF WHERE ADK_KEY=:var_N0_adk_key';		
	$rsADKKopf = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ADR'));							
									
	$Ausdruck->_pdf->SetFont('Arial','B',18);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonservenADS['ADK']['txt_Lieferschein'],0,0,'L',0);				
	$Zeile+=10;		
	
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
	$Ausdruck->_pdf->cell(60,5,$AWISSprachKonserven['ADK']['txt_Uebernahmescheinnummer'].':',0,0,'L',0);
	$EAN='1'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).date('y').date('m').str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);	
	
	$EAN_CLASS = new BarcodeEncode_EAN13();
		
	$EAN_SAVE='';
	if ($EAN_CLASS->Validate($EAN))
	{
		$EAN_CODE = $EAN_CLASS->Enc($EAN);
		$EAN_SAVE = $EAN_CODE->iData;
	}
			
	$Ausdruck->_pdf->cell(30,5,$EAN_SAVE,0,0,'L',0);		
	
	$EAN2='2'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).date('y').date('m').str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);		
	if ($EAN_CLASS->Validate($EAN2))
	{
		$EAN_CODE = $EAN_CLASS->Enc($EAN2);
		$EAN_SAVE = $EAN_CODE->iData;
	}
			
	$Zeile+=10;				
	$Ausdruck->_pdf->Image(Erzeuge_EAN13($EAN2,2),$Spalte+125,$Zeile-8,50,10,'png');	
	$Ausdruck->_pdf->SetXY($Spalte+150,$Zeile+2);
	$Ausdruck->_pdf->SetFont('Courier','B',9);
	$Ausdruck->_pdf->Cell(30,6,$EAN_SAVE,0,0,'L',0);
	$Ausdruck->_pdf->SetFont('Arial','',10);

	$Zeile+=10;	
		
	if ($LieferscheinSeite==1)
	{	
		$DB->SetzeBindevariable('FIL','var_N0_fil_id',$rsADKKopf->FeldInhalt('ADK_FIL_ID'),awisDatenbank::VAR_TYP_GANZEZAHL);
		
		$SQL = 'SELECT FIL_ID, FIL_STRASSE, FIL_PLZ, FIL_ORT FROM FILIALEN WHERE FIL_ID = :var_N0_fil_id';
		$rsFil = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL',false));
		
		$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=912 AND FIF_FIL_ID = :var_N0_fil_id';
		$rsFilinfoName1 = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL',false));
		
		$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=913 AND FIF_FIL_ID = :var_N0_fil_id';
		$rsFilinfoName2 = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL'));
		
		//Weiden
		if ($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='N')
		{
			$Empf_Name = 'ESTATO';
			$Empf_Form = 'Umweltservice GmbH';
			$Empf_Strasse = 'Dr.-Kilian-Str. 11';
			$Empf_Ort = '92637 Weiden';
			$Empf_Telefon = 'Telefon (0961) 306-0';
		}
		//Werl
		elseif (($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='L'))
		{
			$Empf_Name = 'ESTATO';
			$Empf_Form = 'Umweltservice GmbH';
			$Empf_Strasse = 'Hansering 2';
			$Empf_Ort = '59457 Werl';
			$Empf_Telefon = 'Telefon (02922) 807-4400';			
		}
		
		//Absender
		$Ausdruck->_pdf->SetFont('Arial','',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->cell(22,6,$AWISSprachKonservenADS['ADS']['Absender'].':',0,0,'L',0);						
		$Ausdruck->_pdf->cell(50,6,$rsFilinfoName1->FeldInhalt('FIF_WERT'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFilinfoName2->FeldInhalt('FIF_WERT'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_STRASSE'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_PLZ').' '.$rsFil->FeldInhalt('FIL_ORT'),0,0,'L',0);						
		$Zeile+=5;		
		
		//Empf�nger
		$Zeile-=20;
		$Ausdruck->_pdf->SetXY($Spalte+90,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$AWISSprachKonservenADS['ADS']['Empfaenger'].':',0,0,'L',0);						
		$Ausdruck->_pdf->cell(50,6,$Empf_Name,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(60,6,$Empf_Form,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$Empf_Strasse,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$Empf_Ort,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(30,6,$Empf_Telefon,0,0,'L',0);						
		$Zeile+=5;			
	}

	return $Zeile;
}


function _Erzeuge_Block_Lieferschein($Zeile, $Block)
{
	global $AWIS_KEY2;	
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$AWISBenutzer = awisBenutzer::Init();
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
			
	$TextKonserven[]=array('ADS','%');	
	$AWISSprachKonservenADS = $Form->LadeTexte($TextKonserven);
	
	$Zeilebeginn=$Zeile;
						
	$Zeile=$Zeile+3;		
									
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
	$Ausdruck->_pdf->cell(10,6,$AWISSprachKonservenADS['ADS']['Nummer'],0,0,'L',0);							
	
	$Ausdruck->_pdf->setXY($Spalte+10,$Zeile);
	$Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Bezeichnung'],0,0,'C',0);		
		
	$Ausdruck->_pdf->setXY($Spalte+65,$Zeile);
	$Ausdruck->_pdf->cell(25,6,$AWISSprachKonservenADS['ADS']['Nettogewicht'],0,0,'C',0);		
		
	$Ausdruck->_pdf->setXY($Spalte+90,$Zeile);
	$Ausdruck->_pdf->cell(20,6,$AWISSprachKonservenADS['ADS']['Anzahl'],0,0,'C',0);		
	
	$Ausdruck->_pdf->setXY($Spalte+110,$Zeile);
	$Ausdruck->_pdf->cell(30,6,$AWISSprachKonservenADS['ADS']['Gesamtgewicht'],0,0,'C',0);																	
	
	$Ausdruck->_pdf->setXY($Spalte+140,$Zeile);
	$Ausdruck->_pdf->cell(20,6,$AWISSprachKonservenADS['ADS']['Faktor'],0,0,'C',0);																	
	
	$Ausdruck->_pdf->setXY($Spalte+160,$Zeile);
	$Ausdruck->_pdf->cell(20,6,$AWISSprachKonservenADS['ADS']['Punkte'],0,0,'C',0);																	
							
	//Positionen zu Block 1		
	$DB->SetzeBindevariable('ADR','var_N0_adp_adk_key',$AWIS_KEY2,awisDatenbank::VAR_TYP_GANZEZAHL);
	
	$SQL = 'SELECT ADRST.*, ADRPOS.*';		
	$SQL .= ' FROM ADRSTAMM ADRST ';	
	$SQL .= ' INNER JOIN ADRPOS ON ADS_KEY = ADP_ADS_KEY AND ADP_ADK_KEY = :var_N0_adp_adk_key';	
	$SQL .= ' WHERE ADS_ADRART=3';		
	$SQL .= ' ORDER BY ADS_ADRNR';		
	
	$rsADP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ADR'));
	
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
	$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);	
	
	$Zeilebeginn=$Zeile;	
	$GesamtPunkte=0;
	
	while (!$rsADP->EOF())
	{
		$Zeile+=6;
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
		
		$Ausdruck->_pdf->cell(100,6,$AWISSprachKonservenADS['ADS']['Zusatztext_Leergase'],0,0,'L',0);
		
		$Zeile+=6;
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
		
		$Ausgabe = $rsADP->FeldInhalt('ADS_EWCBEZ');
		if ($AWISBenutzer->BenutzerSprache()=='IT' AND $rsADP->FeldInhalt('ADS_EWCBEZIT') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_EWCBEZIT');
		}
		elseif ($AWISBenutzer->BenutzerSprache()=='CZ' AND $rsADP->FeldInhalt('ADS_EWCBEZCZ') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_EWCBEZCZ');
		}
		elseif ($AWISBenutzer->BenutzerSprache()=='NL' AND $rsADP->FeldInhalt('ADS_EWCBEZNL') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_EWCBEZNL');
		}		
		
		// 20140325-CA: ADR-Text muss auf Bef�rderungspapier gro� geschrieben werden.
		$Ausdruck->_pdf->cell(100,6,strtoupper($Ausgabe),0,0,'L',0);			
		//$Ausdruck->_pdf->cell(100,6,$rsADP->FeldInhalt('ADS_EWCBEZ'),0,0,'L',0);			
		
		$Zeile+=6;
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);		
		$Ausdruck->_pdf->cell(10,6,$rsADP->FeldInhalt('ADS_ADRNR'),0,0,'L',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);		
		
		$Ausgabe = $rsADP->FeldInhalt('ADS_LAGABEZ');
		if ($AWISBenutzer->BenutzerSprache()=='IT' AND $rsADP->FeldInhalt('ADS_LAGABEZIT') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_LAGABEZIT');
		}
		elseif ($AWISBenutzer->BenutzerSprache()=='CZ' AND $rsADP->FeldInhalt('ADS_LAGABEZCZ') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_LAGABEZCZ');
		}
		elseif ($AWISBenutzer->BenutzerSprache()=='NL' AND $rsADP->FeldInhalt('ADS_LAGABEZNL') != '')
		{
			$Ausgabe = $rsADP->FeldInhalt('ADS_LAGABEZNL');
		}		
		
		$Ausdruck->_pdf->cell(100,6,$Ausgabe,0,0,'L',0);		
		//$Ausdruck->_pdf->cell(55,6,$rsADP->FeldInhalt('ADS_LAGABEZ'),0,0,'L',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+65,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$rsADP->FeldInhalt('ADS_ZULADGEW'). ' kg',0,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+90,$Zeile);
		$Ausdruck->_pdf->cell(20,6,$rsADP->FeldInhalt('ADP_MENGE').' '.$rsADP->FeldInhalt('ADS_BEHTYP'),0,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+110,$Zeile);
		//$Ausdruck->_pdf->cell(30,6,$rsADP->FeldInhalt('ADP_MENGE') * $rsADP->FeldInhalt('ZULAD_GEW').' '.$rsADP->FeldInhalt('EWC_CODE'),0,0,'R',0);			
		$Ausdruck->_pdf->cell(30,6,$rsADP->FeldInhalt('ADP_GEWICHT').' '.$rsADP->FeldInhalt('ADS_EINHEIT'),0,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+140,$Zeile);
		$Ausdruck->_pdf->cell(20,6,$rsADP->FeldInhalt('ADS_FAKTOR'),0,0,'R',0);			
		
		$Punkte = $rsADP->FeldInhalt('ADP_GEWICHT')*$rsADP->FeldInhalt('ADS_FAKTOR');
		
		$Ausdruck->_pdf->SetXY($Spalte+160,$Zeile);
		$Ausdruck->_pdf->cell(20,6,$rsADP->FeldInhalt('ADP_GEWICHT')*$rsADP->FeldInhalt('ADS_FAKTOR'),0,0,'R',0);			
		
		$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);
		
		$GesamtPunkte+=$Punkte;
		
		$rsADP->DSWeiter();		
	}		
	
	$Zeile+=12;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(110,6,'',0,0,'R',0);			
	$Ausdruck->_pdf->cell(50,6,$AWISSprachKonservenADS['ADS']['Anzahl_Gesamtpunkte'].':',1,0,'L',0);			
	
	$Ausdruck->_pdf->SetXY($Spalte+160,$Zeile);
	$Ausdruck->_pdf->cell(20,6,$GesamtPunkte,1,0,'R',0);					
	
	$Zeile+=3;
	$Ausdruck->_pdf->Line($Spalte,$Zeilebeginn,$Spalte,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte+180,$Zeilebeginn,$Spalte+180,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);	
	
	//Block 5
	$SQL = 'SELECT * FROM ADRTEXTE WHERE ADX_ADRART = 3 ORDER BY ADX_ADRBLOCK, ADX_ADRSORT';
	$rsADR = $DB->RecordSetOeffnen($SQL);
	$rsADRZeilen = $rsADR->AnzahlDatensaetze();
	$rsADR = $rsADR->Tabelle();
	
	$Zeile+=10;
	
	if ($rsADRZeilen>0)
	{			
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
						
		$Zeilebeginn=$Zeile;
				
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);		
		$Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Hinweise'].':',0,0,'L',0);
		$Zeile=$Zeile+10;
		
		for($ADRZeile=0;$ADRZeile<$rsADRZeilen;$ADRZeile++)
		{			
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			
			$Ausgabe = $rsADR["ADX_ADRBEZ"][$ADRZeile];
			if ($AWISBenutzer->BenutzerSprache()=='IT' AND $rsADR["ADX_ADRBEZIT"][$ADRZeile] != '')
			{
				$Ausgabe = $rsADR["ADX_ADRBEZIT"][$ADRZeile];
			}
			elseif ($AWISBenutzer->BenutzerSprache()=='CZ' AND $rsADR["ADX_ADRBEZCZ"][$ADRZeile] != '')
			{
				$Ausgabe = $rsADR["ADX_ADRBEZCZ"][$ADRZeile];
			}
			elseif ($AWISBenutzer->BenutzerSprache()=='NL' AND $rsADR["ADX_ADRBEZNL"][$ADRZeile] != '')
			{
				$Ausgabe = $rsADR["ADX_ADRBEZNL"][$ADRZeile];
			}
			//$Ausdruck->_pdf->cell(180,6,$rsADR["ADX_ADRBEZ"][$ADRZeile],0,0,'L',0);
			$Ausdruck->_pdf->cell(180,6,$Ausgabe,0,0,'L',0);
			$Zeile+=5;
			if ($rsADR["ADX_ADRBLOCK"][$ADRZeile]!=$rsADR["ADX_ADRBLOCK"][$ADRZeile+1])
			{
				$Zeile+=2;
			}			
		}
		
		$Zeile=$Zeile+3;		
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);		
	}
	
	//Fu�zeile
	$Zeile=$Zeile+15;		
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+55,$Zeile);
	//$Ausdruck->_pdf->cell(55,6,'Pol.Kennzeichen des LKW',0,0,'L',0);
	$Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Unterschrift_Absender'],0,0,'L',0);
	
	$Ausdruck->_pdf->SetXY($Spalte+60,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+60,$Zeile,$Spalte+115,$Zeile);
	$Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Unterschrift_Fahrer'],0,0,'L',0);
	
	$Ausdruck->_pdf->SetXY($Spalte+120,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+120,$Zeile,$Spalte+175,$Zeile);
	//$Ausdruck->_pdf->cell(55,6,'Unterschrift Filialleiter',0,0,'L',0);		
	$Ausdruck->_pdf->cell(55,6,$AWISSprachKonservenADS['ADS']['Unterschrift_Empfaenger'],0,0,'L',0);		
	
	$Ausdruck->_pdf->Line($Spalte,$Zeilebeginn,$Spalte,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte+180,$Zeilebeginn,$Spalte+180,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);						
		
	return $Zeile;
}


/**
*
* Funktion speichert ein Bild mit EAN-Code
*
* @author Christian Argauer
* @param  char EAN13-Nummer 12-stellig
* @return Datei-Name
*
*/
function Erzeuge_EAN13($EAN_NR,$Art)
{	
	$DateiName = awis_UserExportDateiName('.png');
	if ($Art==2)
	{
		$DateiName = awis_UserExportDateiName('2.png');
	}
	$symbology = BarcodeFactory::Create (ENCODING_EAN13);
	$barcode = BackendFactory ::Create('IMAGE', $symbology);
	$barcode->SetScale(1);
	$barcode->SetMargins(10,0,0,1);
	$barcode->SetColor('black','white');
	$barcode->NoText(true);
	$barcode->SetHeight(30);
	$barcode->Stroke($EAN_NR,$DateiName);
	return $DateiName;
}

?>