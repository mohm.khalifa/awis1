<?php
require_once 'textkonserven_funktionen.inc';

try
{
    $XTX = new textkonserven_funktionen();
    $XTX->RechteMeldung(0);//Anzeigen Recht

    if(isset($_POST['cmdSpeichern_x'])){
        require_once 'textkonserven_speichern.php';
    }

	echo "<form name=frmSuche method=post action=textkonserven_Main.php?cmdAktion=Textkonserven>";

	$XTX->Form->Formular_Start();

	$XTX->Form->ZeileStart();
	$XTX->Form->Erstelle_TextLabel($XTX->AWISSprachKonserven['XTX']['XTX_BEREICH'].':',60);
    $XTX->Form->Erstelle_MehrfachSelectFeld('*XTX_BEREICH',isset($_POST['sucXTX_BEREICH'])?$_POST['sucXTX_BEREICH']:[],300,true,$XTX->TextkonservenSelectfeldSQL());


    $XTX->Form->Erstelle_TextLabel($XTX->AWISSprachKonserven['XTX']['XTX_SPRACHEN'].':',70);
    $XTX->Form->Erstelle_MehrfachSelectFeld('*XTX_SPRACHEN',isset($_POST['sucXTX_SPRACHEN'])?$_POST['sucXTX_SPRACHEN']:[],'300:300',true,'','','','','',$XTX->VerfuegbareSprachenArray(),'','',[],'','AWIS','','',3);

    $XTX->Form->Erstelle_TextLabel($XTX->AWISSprachKonserven['XTX']['XTX_SYSTEMINFOS'].':',90);
    $XTX->Form->Erstelle_Checkbox('XTX_SYSTEMINFOS','off',50,true);

    $XTX->Form->Erstelle_TextLabel($XTX->AWISSprachKonserven['XTX']['XTX_DBLCLICK'].':',90);
    $Optionen = explode('|',$XTX->AWISSprachKonserven['XTX']['XTX_LST_DBLCLICK']);
    $XTX->Form->Erstelle_SelectFeld('XTX_DBLCLICK','',200,true,'','',1,'','',$Optionen);


    $XTX->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $XTX->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $XTX->Form->ZeileEnde();

    $XTX->Form->ZeileStart();
    $XTX->Form->Trennzeile('L');
    $XTX->Form->ZeileEnde();

    if(isset($_POST['sucXTX_BEREICH'])){
        require_once 'textkonserven_Textkonserven_Liste.php';
    }


    $XTX->Form->Formular_Ende();

	$XTX->Form->SchaltflaechenStart();
	$XTX->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$XTX->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$XTX->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $XTX->AWISSprachKonserven['Wort']['lbl_speichern'], 'W');
	$XTX->Form->SchaltflaechenEnde();

	$XTX->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($XTX->Form instanceof awisFormular)
	{
		$XTX->Form->DebugAusgabe(1, $ex->getSQL());
		$XTX->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$XTX->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($XTX->Form instanceof awisFormular)
	{
		$XTX->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>