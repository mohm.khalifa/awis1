<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200811241645
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('LVB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');	
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4100 = $AWISBenutzer->HatDasRecht(4100);
	if($Recht4100==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		//$Param['PBM_XBN_KEY']=$Form->Format('N0',$_POST['txtPBM_XBN_KEY'],true);
		$Param['LVB_AUFTR_ID']=$Form->Format('N0',$_POST['sucLVB_AUFTR_ID'],true);
		$Param['LVB_ART_ID']=$Form->Format('T',$_POST['sucLVB_ART_ID'],true);
		$Param['LVB_ID_VS']=$Form->Format('N0',$_POST['sucLVB_ID_VS'],true);	

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_LVB',serialize($Param));
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LVB'));
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY LVB_ART_ID ASC';
	}	
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$SQL = 'SELECT DISTINCT LVSLBSKOMM.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM LVSLBSKOMM';

	$Bedingung=_BedingungErstellen($Param);

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	$SQL .= $ORDERBY;

	
	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_LVB',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('LVB',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	$Form->DebugAusgabe(1,$SQL);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	
	$rsLVB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('LVB'));
	$AWISBenutzer->ParameterSchreiben('Formular_LVB',serialize($Param));

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./behaelterabfrage_Main.php?cmdAktion=Details>");

	//********************************************************
	// Daten anzeigen
	//********************************************************
	if($rsLVB->EOF())
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsLVB->AnzahlDatensaetze()>=1)						// Liste anzeigen
	{

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LVB_AUFTR_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LVB_AUFTR_ID'))?'~':'');		
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVB']['LVB_AUFTR_ID'],100,'',$Link);		
		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LVB_ART_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LVB_ART_ID'))?'~':'');		
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVB']['LVB_ART_ID'],150,'',$Link);		
		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LVB_ID_VS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LVB_ID_VS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVB']['LVB_ID_VS'],200,'',$Link);		
		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LVB_VE_IST_ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LVB_VE_IST_ANZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVB']['LVB_VE_IST_ANZ'],100,'',$Link);		
		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LVB_USERDAT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LVB_USERDAT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LVB']['LVB_USERDAT'],100,'',$Link);		
		$Form->ZeileEnde();
		$DS=0;

		while(!$rsLVB->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('LVB_AUFTR_ID',$rsLVB->FeldInhalt('LVB_AUFTR_ID'),0,100,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('LVB_ART_ID',$rsLVB->FeldInhalt('LVB_ART_ID'),0,150,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('LVB_ID_VS',$rsLVB->FeldInhalt('LVB_ID_VS'),0,200,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('LVB_VE_IST_ANZ',$rsLVB->FeldInhalt('LVB_VE_IST_ANZ'),0,100,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('LVB_USERDAT',$rsLVB->FeldInhalt('LVB_USERDAT'),0,100,false,($DS%2),'','','D');
			$Form->ZeileEnde();

			$rsLVB->DSWeiter();
		}

		$Link = './behaelterabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();

	}
	
	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '/filialtaetigkeiten/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=behaelterabfrage&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');	

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200811241438");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200811241438");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung für die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	
	$Bedingung = '';

	// Einschränken nach Filiale
	if(isset($Param['LVB_AUFTR_ID']) AND $Param['LVB_AUFTR_ID']!='')
	{
		$DB->SetzeBindevariable('LVB','var_N0_lvb_auftr_id',$DB->FeldInhaltFormat('N0',$Param['LVB_AUFTR_ID']),awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND LVB_AUFTR_ID = :var_N0_lvb_auftr_id ';
	}

	// Einschränken nach Artikelnummer
	if(isset($Param['LVB_ART_ID']) AND $Param['LVB_ART_ID']!='')
	{
		$Bedingung .= ' AND LVB_ART_ID '.$DB->LikeOderIst($Param['LVB_ART_ID'],1,'LVB') . ' ';
	}
	
	// Einschränken nach Artikelnummer
	if(isset($Param['LVB_ID_VS']) AND $Param['LVB_ID_VS']!='')
	{
		$DB->SetzeBindevariable('LVB','var_N0_lvb_id_vs',$DB->FeldInhaltFormat('N0',$Param['LVB_ID_VS']),awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND LVB_ID_VS = :var_N0_lvb_id_vs ';
	}
	
	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}

?>