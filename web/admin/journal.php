<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("ATU_Header.php");	// Kopfzeile
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>

<?php
$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

echo"<h1>Nachtverarbeitungen der verschiedenen Datenbanken</h1>";

if(($RechteStufe&4)!=4)
{
	awisEreignis(3,1000,'Journal',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
	die("Keine ausreichenden Rechte!");
}

$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

echo '<form name=frmSuche method=get action=./journalbild.php>';

echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';
echo '<td colspan=3><select accesskey=d name="txtDB">';

for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
	echo '<option value="">Lokaler Server</option>';
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '">' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
echo '</select></td>';
echo '</tr>';
echo '<tr>';
echo '<td id=FeldBez><u>S</u>tartzeit</td>';
//echo '<td><input accesskey=s tabindex=25 size=20 type=text name=startzeit value="' . date("Y-m-d", time() - 86400) . ' 12:00"></td>';
echo '<td><input accesskey=s tabindex=25 size=20 type=text name=startzeit value="' . date("Y-m-d H:00", time() - 82800) . '"></td>';
echo '<td id=FeldBez><u>E</u>ndzeit</td>';
//echo '<td><input accesskey=e tabindex=27 size=20 type=text name=endzeit value="' . date("Y-m-d") . ' 12:00"></td>';
echo '<td><input accesskey=e tabindex=27 size=20 type=text name=endzeit value="' . date("Y-m-d H:00", time() + 3600) . '"></td>';
echo '</tr>';
echo '<tr>';
echo '<td id=FeldBez><u>L</u>aufzeit</td>';
echo '<td colspan=3><input accesskey=l tabindex=30 size=5 type=text name=laufzeit value="2"></td>';
echo '</tr>';
echo '</table>';

print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

echo '</form>';
print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='../admin/index.php';>";

awislogoff($con);

/*
if (!empty($_GET["db"]))
{
	if (strtolower($_GET["db"]) != "awis3")
	{
		$db = "?db=" . strtolower($_GET["db"]);
	}
}

// include("./journalbild.php");
echo '<img src="./journalbild.php" alt="" />';
// include("/admin/journalbild.php?db=awis26");
*/
/*
<a href=./journalbild.php?db=awis3 title="&Uuml;bersicht auf Awis3">Awis3</a><br>
<a href=./journalbild.php?db=awis7 title="&Uuml;bersicht auf Awis7">Awis7</a><br>
<a href=./journalbild.php?db=awis25 title="&Uuml;bersicht auf Awis25">Awis25</a><br>
<a href=./journalbild.php?db=awis26 title="&Uuml;bersicht auf Awis26">Awis26</a><br>
<a href=./journalbild.php?db=awis39 title="&Uuml;bersicht auf Awis26">Awis39</a><br>
*/
?>
</body>
</html>
