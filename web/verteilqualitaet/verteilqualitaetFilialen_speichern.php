<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('WEB','WEB_%');
$TextKonserven[]=array('WBE','WBE_%');
$TextKonserven[]=array('WER','WER_%');
$TextKonserven[]=array('Wort','lbl_weiter');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$Felder = $Form->NameInArray($_POST, 'txtWEB_',1,1);
	$Felder = explode(';',$Felder);

	$Auswahl = $Form->NameInArray($_POST,'txtWeb_',1,1);
	$Auswahl = explode(';',$Auswahl);
	
	for($i=0;$i<count($Auswahl);$i=$i+2)
	{
		$PersNr = substr($Auswahl[$i],15,6);
		$WebKey = substr($Auswahl[$i],21,2);
		
		$SQL = 'Select WER_PERSNR,WER_WEB_KEY FROM WERBEEINTRAEGE WHERE WER_PERSNR='.$DB->FeldInhaltFormat('T',$PersNr);
		$SQL .= ' AND WER_WEB_KEY='.$DB->FeldInhaltFormat('NO',$WebKey);
		
		$rsVSA = $DB->RecordSetOeffnen($SQL);
		
		if($rsVSA->AnzahlDatensaetze() == 0)
		{
		   $SQL = "INSERT INTO WERBEEINTRAEGE(WER_PERSNR,WER_SELWERT1,WER_SELWERT2,WER_WEB_KEY,WER_USER,Wer_Userdat) VALUES (";	
		   $SQL .= ' ' . $DB->FeldInhaltFormat('T',$PersNr,true);
		   $SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST[$Auswahl[$i]],true);
		   $i++;
		   $SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST[$Auswahl[$i]],true);
		   $SQL .= ',' . $DB->FeldInhaltFormat('N0',$WebKey,true);
		   $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		   $SQL .= ',SYSDATE';
		   $SQL .= ')';
		   
                   $i--;	

		   if($DB->Ausfuehren($SQL)===false)
		   {
			die();
		   }
		}
		else
		{
		        $SQL = 'UPDATE WERBEEINTRAEGE SET WER_SELWERT1='.$DB->FeldInhaltFormat('N0',$_POST[$Auswahl[$i]],true);
			$i++;
			$SQL .= ' ,WER_SELWERT2='. $DB->FeldInhaltFormat('N0',$_POST[$Auswahl[$i]],true);
			$SQL .= ' WHERE WER_PERSNR='. $DB->FeldInhaltFormat('T',$PersNr);
			$SQL .= ' AND WER_WEB_KEY='.$DB->FeldInhaltFormat('N0',$WebKey,true);
			
			$i--;
			
			if($DB->Ausfuehren($SQL)===false)
			{
			  die();
			}
			
		}
	}
	
	
	
	
	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);	
}
catch (Exception $ex)
{
	
}
?>