<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 16.09.2016
 * Time: 18:45
 */
require_once 'awisBenutzer.inc';
require_once 'awisSlackProtokoll.php';
require_once __DIR__ . "/vendor/autoload.php";

class awisSlack
{
    /**
     * Text der Nachricht
     *
     * @var string
     */
    protected $_Text = '';

    /**
     * Betreff der Nachricht
     *
     * @var string
     */
    protected $_Betreff = '';

    /**
     * Link des Betreffs
     *
     * @var string
     */
    protected $_BetreffLink = '';

    /**
     * Absender der Nachricht
     *
     * @var
     */
    protected $_Absender;

    /**
     * Empf�nger der Nachricht
     *
     * @var array
     */
    protected $_Empfaenger = array();

    /**
     * Webhook f. Ausgehende Nachrichten
     *
     * @var array
     */
    protected $_Webhook = array();

    /**
     * AwisBenutzerObj
     *
     * @var awisBenutzer
     */
    protected $_AWISBenutzer;

    /**
     * Priorit�t der Nachricht
     *
     * @var
     */
    protected $_Prioritaet;

    /**
     * Farbe des Strichs
     *
     * @var
     */
    protected $_Farbe;

    /**
     * Icon des Absenders
     *
     * @var
     */
    protected $_Icon;

    /**
     * SlackClient
     *
     * @var
     */
    protected $_SlackClient;

    /**
     * awisSlackProtokoll
     *
     * @var awisSlackProtokoll
     */
    protected $_AWISSlackProtokoll;

    function __construct(awisBenutzer $AWISBenutzer)
    {
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_AWISSlackProtokoll = new awisSlackProtokoll($this->_AWISBenutzer);

        $this->setzeWebhook(array('TYP' => 'XPP', 'Wert' => 'Slack_AWIS_Default_Hook'));
        $this->Empfaenger(array('TYP' => 'XPP', 'Wert' => 'Slack_AWIS_Default_Empfaenger'));
        $this->Absender('AWIS');
    }


    /**
     * Setzt die Webhook f�r ausgehende Nachrichten
     *
     * @param $Webhook
     */
    public function setzeWebhook($Webhook)
    {
        if (is_array($Webhook) and count($Webhook) > 0) {
            switch ($Webhook['TYP']) {
                case 'XPP':
                    $this->_Webhook = $this->_AWISBenutzer->ParameterLesen($Webhook['Wert']);
                    break;
                default:
                    break;
            }
        } else {
            $this->_Webhook = $Webhook;
        }
    }

    /**
     * Setzt den Text
     *
     * @param $Text
     */
    public function Text($Text)
    {

        $this->_Text = utf8_encode($Text);
    }


    /**
     * Setzt den Betreff
     *
     * @param $Betreff
     */
    public function Betreff($Betreff, $Link = '')
    {
        $this->_Betreff = utf8_encode($Betreff);
        $this->_BetreffLink = $Link;
    }

    /**
     * Setzt den Empf�nger
     *
     * @param $Empaenger String o. Array()
     */
    public function Empfaenger($Empaenger)
    {
        $this->_Empfaenger = array();
        //2 Dimensionales Arrays mit ['TYP'] bekommen eine Extrabehandlung
        if (isset($Empaenger['TYP'])) {
            switch ($Empaenger['TYP']) {
                case 'XPP':
                    $Empaenger = $this->_AWISBenutzer->ParameterLesen($Empaenger['Wert'], true);
                    break;
                default:
                    break;
            }
        }

        //Empf�nger auswerten:
        if (!is_array($Empaenger) and strpos($Empaenger, ';') !== false) { //Mit Semikolon getrennte Empf�nger m�ssen zu einem Array werden
            $this->_Empfaenger = explode(';', str_replace(' ', '', $Empaenger));
        } elseif (!is_array($Empaenger)) { //Einzelner als String �bergebener Empf�nger wird zu einem Array
            $this->_Empfaenger = array($Empaenger);
        } else { //Empf�ngerarray kann ein array bleiben
            $this->_Empfaenger = $Empaenger;
        }
    }

    /**
     * Setzt einen Pseudoabsender
     *
     * @param string $Absender
     */
    public function Absender($Absender = 'AWIS')
    {
        $this->_Absender = $Absender;
    }

    const Benachrichtigung_Fehler = 3;
    const Benachrichtigung_Warnung = 2;
    const Benachrichtigung_Ok = 1;

    /**
     * Setzt den Benachrichtigungstypen
     *
     * @param int $BenachrichtigungsTyp (const PRIO_XXX)
     * @return int
     */
    public function BenachrichtigungsTyp($BenachrichtigungsTyp = '')
    {
        switch ($BenachrichtigungsTyp) {
            case $this::Benachrichtigung_Fehler:
                $this->Farbe('#ff0000');
                $this->Icon(':exclamation:');
                break;
            case $this::Benachrichtigung_Warnung:
                $this->Farbe('#ffff00');
                $this->Icon(':warning:');
                break;
            case $this::Benachrichtigung_Ok:
                $this->Farbe('#66ff33');
                $this->Icon(':white_check_mark:');
                break;
            default:
                break;
        }
        $this->_Prioritaet = $BenachrichtigungsTyp;
    }

    /**
     * Setzt die Farbe des Striches bei einer Slacknachricht
     *
     * @param $HEXRGBCode
     */
    public function Farbe($HEXRGBCode)
    {
        $this->_Farbe = $HEXRGBCode;
    }

    /**
     * Baut und versendet die Nachricht
     *
     * @return bool
     */
    public function Senden()
    {
        //Client mit gesetzter URL Initialisieren;
        $this->_SlackClient = new Maknz\Slack\Client($this->_Webhook);
        $this->_SlackClient->setDefaultUsername($this->_Absender);
        $this->_SlackClient->setDefaultIcon($this->_Icon);
        $return = false;

        //Parameter f�r $Param
        $Params = array();
        $Params['text'] = '';
        $Attachments = array();

        if ($this->_Betreff != '') {
            $Attachments['title'] = $this->_Betreff;
            $Attachments['text'] = $this->_Text;
            // $Params['text'] = $this->_Absender; //Pseudo Absender suggerieren

        } else {
            $Params['text'] = $this->_Text;
        }

        if ($this->_BetreffLink != '') {
            $Attachments['title_link'] = $this->_BetreffLink;
        }

        if ($this->_Farbe != '') {
            $Attachments['color'] = $this->_Farbe;
        }

        //Empf�nger wurden durch Empfaenger() zum Array
        foreach ($this->_Empfaenger as $Empfaenger) {
            $this->_SlackClient->to($Empfaenger);
            $this->_SlackClient->setDefaultChannel($Empfaenger);
            $this->_senden($Params, $Attachments);
        }

        return $return;
    }

    /**
     * Versendet die Nachricht wirklich
     *
     * @param        $Params
     * @param string $Attachments
     */
    private function _senden($Params, $Attachments = '')
    {
        //1. Nachricht erstellen
        $Message = $this->_SlackClient->createMessage();

        //2. GGf. "Anh�nge" anh�ngen
        if ($Attachments != '') {
            $Message->Attach($Attachments);
        }
        //3. Senden
        try {
            $Message->send($Params['text']);
        } catch (Exception $e) {
            $this->_handleError($e);
        }
    }

    /**
     * Behandelt die aufgetretenen Fehler
     *
     * @param Exception $Exception
     */
    private function _handleError(Exception $Exception)
    {
        switch ($Exception->getCode()) {
            case 404:
                if (strpos($Exception->getMessage(), 'channel_not_found') !== false) {
                    echo 'Empf�nger nicht gefunden. Empf�nger: ';
                    var_dump($this->_Empfaenger);
                }

                break;
        }
    }

    /**
     * Setzt das Icon
     *
     * @param $Icon
     */
    public function Icon($Icon)
    {
        $this->_Icon = $Icon;
    }


}