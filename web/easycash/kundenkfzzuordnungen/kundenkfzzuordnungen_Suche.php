<?php
//global $AWIS_KEY1;
//global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KundenKfzZuordnungen','%');
	$TextKonserven[]=array('Liste','lst_KKZ_STATUS');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Werkzeuge = new awisWerkzeuge();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13000 = $AWISBenutzer->HatDasRecht(13000);
	if($Recht13000==0)
	{
	    awisEreignis(3,1000,'KundenKfzZuordnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	echo "<form name=frmSuche method=post action=./kundenkfzzuordnungen_Main.php?cmdAktion=Details>";

	/**********************************************
	* Eingabemaske
	***********************************************/
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_INFOFELD_1'].':',190);
	$Form->Erstelle_TextFeld('*KKZ_INFOFELD_1','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_VERTRAGSNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*KKZ_VERTRAGSNUMMER','',20,200,true,'','','','Z');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_REFERENZFELD'].':',190);
	$Form->Erstelle_TextFeld('*KKZ_REFERENZFELD','',20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_LIEFERANT'].':',190);
	$SQL = 'SELECT DISTINCT KKZ_LIEFERANT as KEY, KKZ_LIEFERANT as NAME';
	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
		$SQL .= ' FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG@COM_DE.ATU.DE k';
	}
	else 
	{
		$SQL .= ' FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG@COM_TEST_DE.ATU.DE k';
	}	
	$SQL .= ' ORDER BY KKZ_LIEFERANT';
	$Form->Erstelle_SelectFeld('*KKZ_LIEFERANT','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();	
	
	$Form->ZeileStart();	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_STATUS'].':',190);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_KKZ_STATUS']);
	$Form->Erstelle_SelectFeld('*KKZ_STATUS','',200,true,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
	$Form->ZeileEnde();	

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();


	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201010181451");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201010181452");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>