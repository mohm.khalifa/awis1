<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$script = "<script type='text/javascript'>
	
	function EinAusklappen(frame) {
	
	 	if(document.getElementById('FRAME'+frame).style.display == 'block') 
    	{ 
    	     document.getElementById('FRAME'+frame).style.display = 'none'; 
			 if(frame != 5)
			 { 
				document.getElementById('UEB'+frame).style.borderBottom = '';
			 }
			 document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_plus.png';                 
	    }
		else 
		{
			  document.getElementById('FRAME'+frame).style.display = 'block';	
			  if(frame != 5)
			  { 
			  	document.getElementById('UEB'+frame).style.borderBottom = '1px solid black';
			  }
			  document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_minus.png'; 
		}
		
	}
	
	</script>";
	
	echo $script;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','lbl_suche');

	$AWISBenutzer = awisBenutzer::Init();
		
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Frames = unserialize($AWISBenutzer->ParameterLesen('Schad_Frame'));
	
	$Frame1 = $Frames[0] == 1? true:false;
	$Frame2 = $Frames[1] == 1? true:false;
	$Frame3 = $Frames[2] == 1? true:false;
	$Frame4 = $Frames[3] == 1? true:false;
	$Frame5 = $Frames[4] == 1? true:false;
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht39003 = $AWISBenutzer->HatDasRecht(39003);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES'));
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Schad_Schnellsuche');
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'BEARBEITUNGSNR';
	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if($Recht39003==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['txtBES_FIL_ID'],true);
		}
		else
		{
			$Param['BES_DATUMVON']=       isset($_POST['sucBES_DATUMVON'])?$Form->Format('T',$_POST['sucBES_DATUMVON'],true):'';
			$Param['BES_DATUMBIS']=       isset($_POST['sucBES_DATUMBIS'])?$Form->Format('T',$_POST['sucBES_DATUMBIS'],true):'';
			$Param['BES_KUNDE']=          isset($_POST['sucBES_KUNDE'])?$Form->Format('T',$_POST['sucBES_KUNDE'],true):'';
			$Param['BES_STRASSE']=        isset($_POST['sucBES_STRASSE'])?$Form->Format('T',$_POST['sucBES_STRASSE'],true):'';
			$Param['BES_PLZ']=            isset($_POST['sucBES_PLZ'])?$Form->Format('T',$_POST['sucBES_PLZ'],true):'';
			$Param['BES_ORT']=            isset($_POST['sucBES_ORT'])?$Form->Format('T',$_POST['sucBES_ORT'],true):'';
			$Param['BES_TELEFON']=        isset($_POST['sucBES_TELEFON'])?$Form->Format('T',$_POST['sucBES_TELEFON'],true):'';
			$Param['BES_EMAIL']=          isset($_POST['sucBES_EMAIL'])?$Form->Format('T',$_POST['sucBES_EMAIL'],true):'';
			$Param['BES_ATUCARD']=        isset($_POST['sucBES_ATUCARD'])?$Form->Format('T',$_POST['sucBES_ATUCARD'],true):'';
			$Param['BES_SACHBEARBEITER']= isset($_POST['sucBES_SACHBEARBEITER'])?$Form->Format('T',$_POST['sucBES_SACHBEARBEITER'],true):'';
			if(isset($_POST['sucBES_FIL_ID']))
			{
				$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['sucBES_FIL_ID'],true);
				if($Param['BES_FIL_ID'] == 0)
				{
					$Param['BES_FIL_ID'] = '';
				}
			}
			else
			{
				$Param['BES_FIL_ID']=0;
			}
		}
		$Param['BES_BEARBEITUNG'] = 	 isset($_POST['sucBES_BEARBEITUNG'])?$Form->Format('T',$_POST['sucBES_BEARBEITUNG'],true):'';
		$Param['BES_KFZ'] = 		     isset($_POST['sucBES_KFZ'])?$Form->Format('T',$_POST['sucBES_KFZ'],true):'';
		$Param['BES_VORGANG'] = 	     isset($_POST['sucBES_VORGANG'])?$Form->Format('T',$_POST['sucBES_VORGANG'],true):'';
		$Param['BES_AART']=			     isset($_POST['sucBES_AART'])?$Form->Format('N0',$_POST['sucBES_AART'],true):'';
		$Param['SPEICHERN']=		     isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	
	if(isset($_GET['BEARBEITUNGSNR']))
	{
		$AWIS_KEY1 = $_GET['BEARBEITUNGSNR'];
	}
	if((isset($_POST['txtBEARBEITUNGSNR']) and $Fehler > 0) or (isset($_POST['cmdHinzufuegen_x'])))
	{
		$AWIS_KEY1 = $_POST['txtBEARBEITUNGSNR'];
	}

	
	$Bedingung = '';
	$SQL = 'select BEARBNRNEU, ART, to_clob(FEHLERBESCHREIBUNG) AS REKL_TEXT, FILNR, VERURS_FILIALE, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ';
	$SQL .= "DIAGNOSE_WERKSTATTLEITER, DIAGNOSE_TKDL,REKLASCHEINNR,REKLASCHEINVAX,EINGABEAM,DATUMEREIGNIS,BEARBEITUNGSNRFIL,GEWICHTUNG,FABRIKAT,TYP,KFZTYP,ERSTZULASSUNG,KM,BEZEICHNUNG,MECHANIKER,AUFTRAGSDATUM,DATUMREGULIERUNG,BONNRREGULIERUNG,FORDERUNG,WEITERGELEITET_HERSTELLER, ";
	$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, GROSSKDNR, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM,BESICHTIGT,KBANR,KAUFDATUM,BONNR,LIEFERANT,LIEFARTNR,VORGANGSNR,FGLIEFERANT,FGMARKE,ZUSATZINFO,FEHLERBESCHREIBUNG,BEMINTERN,ANWALTSSACHE,WEITERGELEITETVERSICHERUNG, ";
	$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL, EINGABEDURCH_FILPERSNR, ATUNR, WERT,SBNAME,ORTEREIGNIS,ANTRAGART,AKTENZEICHENRA,RAFORDERUNG,STANDBEIGERICHT,AKTENZEITENGERICHT, ";
	$SQL .='row_number() OVER (';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= "SELECT * ";
	$SQL .= "FROM SCHAEDEN_NEW ";
	$SQL .= "INNER JOIN ANTRAGART AA ON ANTRAGART = AA.ID ";
	$SQL .= "LEFT JOIN SACHBEARBEITER SB ON BEARBEITER = SB.ID) ";
	if($AWIS_KEY1 != '')
	{
		$Bedingung .= ' AND bearbeitungsnr='.$AWIS_KEY1;
	}
		
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$Bedingung.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";				
	    $Bedingung.= " AND ANTRAGART in (10,3,18,17)";		
	}
	
	$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	
	
	$rsErgebnis = $DBSCHAD->RecordSetOeffnen($SQL);	
	$Form->SchreibeHTMLCode('<form name="frmBESDetails" action="./schaden_Main.php?cmdAktion=Details" method=POST  enctype="multipart/form-data">');
	$Form->Formular_Start();
	if($AWIS_KEY1 == '')
	{
		$AWIS_KEY1 = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
	}
	if (($rsErgebnis->AnzahlDatensaetze() > 1))
	{
		$DS = 0;	// f�r Hintergrundfarbumschaltung
			$Gesamtbreite = '';
			// Spaltenbreiten f�r Listenansicht
			$FeldBreiten = array();
			$FeldBreiten['BEARBEITUNG'] = 150;
			$FeldBreiten['AART'] = 100;
			$FeldBreiten['KUNDE'] = 120;
			$FeldBreiten['STRASSE'] = 100;
			$FeldBreiten['PLZ'] = 100;
			$FeldBreiten['ORT'] = 100;
			
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
			{
				$FeldBreiten['FILNR'] = 100;
				$FeldBreiten['TELEFON'] = 100;
				$FeldBreiten['EMAIL'] = 100;
				$FeldBreiten['CARD'] = 100;
			}
			
			foreach ($FeldBreiten as $value)
			{
				$Gesamtbreite += $value;
			}

			
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
			//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
			// �berschrift der Listenansicht mit Sortierungslink: Filial;
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=BEARBEITUNGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEARBEITUNGSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEARB'], $FeldBreiten['BEARBEITUNG'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WERT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AART'], $FeldBreiten['AART'], '', $Link);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
			{
				// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=FILNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FILNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_FILNR'], $FeldBreiten['FILNR'], '', $Link);
			}
			// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=KUNDENNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KUNDENNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KUNDE'], $FeldBreiten['KUNDE'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=STRASSE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='STRASSE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_STRASSE'], $FeldBreiten['STRASSE'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PLZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_PLZ'], $FeldBreiten['PLZ'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ORT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ORT'], $FeldBreiten['ORT'], '', $Link);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
			{
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=TELEFON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='TELFON'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_TELEFON'], $FeldBreiten['TELEFON'], '', $Link);
				// �berschrift der Listenansicht mit Sortierungslink: Nachname
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=EMAIL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='EMAIL'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EMAIL'], $FeldBreiten['EMAIL'], '', $Link);
				// �berschrift der Listenansicht mit Sortierungslink: Nachname
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=PAN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PAN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_CARD'], $FeldBreiten['CARD'], '', $Link);
			}
			$Form->ZeileEnde();
			
			while(! $rsErgebnis->EOF())
			{
				$Form->ZeileStart();
				
				
				if($rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') <> '')
				{
					$Hintergrund = 3;
				}
				else
				{
					$Hintergrund = ($DS%2);
				}
				
				$Link = './schaden_Main.php?cmdAktion=Details&BEARBEITUNGSNR=0'.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$TTT = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
				$Form->Erstelle_ListenFeld('BES_BEARBEITUNG', $rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['BEARBEITUNG'], false, $Hintergrund, '',$Link, 'T', 'L', $TTT);
				$TTT =  $rsErgebnis->FeldInhalt('WERT');
				$Form->Erstelle_ListenFeld('BES_ART',$rsErgebnis->FeldInhalt('WERT'), 0, $FeldBreiten['AART'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
				{
					$TTT =  $rsErgebnis->FeldInhalt('FILNR');
					$Form->Erstelle_ListenFeld('BES_FILNR',$rsErgebnis->FeldInhalt('FILNR'), 0, $FeldBreiten['FILNR'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				}
				$TTT = $rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME');
				$Form->Erstelle_ListenFeld('BES_KUNDE',$rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME'), 0, $FeldBreiten['KUNDE'], false, $Hintergrund,'','', 'T', 'L', $TTT);
				$TTT = $rsErgebnis->FeldInhalt('STRASSE');
				$Form->Erstelle_ListenFeld('BES_STRASSE',$rsErgebnis->FeldInhalt('STRASSE'), 0, $FeldBreiten['STRASSE'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				$TTT = $rsErgebnis->FeldInhalt('PLZ');
				$Form->Erstelle_ListenFeld('BES_PLZ',$rsErgebnis->FeldInhalt('PLZ'), 0, $FeldBreiten['PLZ'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				$TTT = $rsErgebnis->FeldInhalt('ORT');
				$Form->Erstelle_ListenFeld('BES_ORT',$rsErgebnis->FeldInhalt('ORT'), 0, $FeldBreiten['ORT'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
				{
					$TTT =  $rsErgebnis->FeldInhalt('TELEFON');
					$Form->Erstelle_ListenFeld('BES_TELEFON',$rsErgebnis->FeldInhalt('TELEFON'), 0, $FeldBreiten['TELEFON'], false, $Hintergrund, '','', 'T', 'L', $TTT);
					$TTT =  $rsErgebnis->FeldInhalt('EMAIL');
					$Form->Erstelle_ListenFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('EMAIL'), 0, $FeldBreiten['EMAIL'], false, $Hintergrund, '','', 'T', 'L', $TTT);
					$TTT =  $rsErgebnis->FeldInhalt('PAN');
					$Form->Erstelle_ListenFeld('BES_CARDNR',$rsErgebnis->FeldInhalt('PAN'), 0, $FeldBreiten['EMAIL'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				}
				
				$Form->ZeileEnde();
				$DS++;
				$rsErgebnis->DSWeiter();
			}
			
			$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsErgebnis->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
			$Link = './schaden_Main.php?cmdAktion=Details';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
			$Form->ZeileEnde();
	}
	else if (($rsErgebnis->AnzahlDatensaetze() == 1))
	{
		$SQL2 = 'select * from(';
		$SQL2 .=  'select historie_kennung,historie_datum ';
		$SQL2 .= 'from historie';
		$SQL2 .= ' where historie_bearbnr ='.$AWIS_KEY1;
		$SQL2 .= ' order by historie_datum desc)';
		$SQL2 .= ' where rownum = 1';
		$rsHist = $DBSCHAD->RecordSetOeffnen($SQL2);
		
		

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schaden_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_KENNUNG'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_DATUM'));
		
		$Form->InfoZeile($Felder,'');
		$Form->ZeileStart();
		
		
		
		$Form->ZeileEnde();
	   
        $Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_ALLGMEIN'],true, 10);


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BEARBNR',$rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 10, 150,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'] . ':',30);
		$Form->AuswahlBox('BES_FIL', 'box1','', 'BES_FIL', '', 60,2,isset($_POST['sucBES_FIL'])?$_POST['sucBES_FIL']:$rsErgebnis->FeldInhalt('FILNR'),'T',true,'','','',50,'','','',0);
		
		
		
		$Inhalt = '';
		$Fehler = '';
		$FIL_ID = '';
		ob_start();
		
		
		if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_FIL']) or $_POST['sucBES_FIL'] == ''))
		{
		
			$FIL_ID = '0';
			$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
		}
		else
		{
			if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_FIL']!= '')
			{
				$FIL_ID= intval($_POST['sucBES_FIL']);
				if($_POST['sucBES_FIL'] == '')
				{
					$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
				}
			}
			else
			{
				if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_FIL'] == '')
				{
					$FIL_ID = '0';
					$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
				}
				else
				{
					$FIL_ID = intval($rsErgebnis->FeldInhalt('FILNR'));
				}
			}
			 
			if($FIL_ID == '0' and $Fehler == '')
			{
				$Fehler == 'keine g�ltige Fililale';
			}
		}
		
		$SQL  = ' Select * from( ';
		$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
		$SQL .= ' where fil_gruppe IS NOT NULL OR';
		$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
		$SQL .= ' union';
		$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
		$SQL .= ' FROM schaddev09.pseudofilialen)';
		$SQL .= ' where fil_id =0'.$FIL_ID;
		
		$rsFIL = $DB->RecordSetOeffnen($SQL);
		 
		if($rsFIL->AnzahlDatensaetze() == 0 and $Fehler == '')
		{
			$Fehler = 'keine g�ltige Fililale';
		}
		
		
		
		
		
		$Form->Erstelle_TextFeld('BES_FILILAE',$Fehler == '' ? $rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,499, false, '', '', '', 'T', 'L','','',50);
	
		$Inhalt = ob_get_contents();
		ob_end_clean();
		
		$Form->AuswahlBoxHuelle('box1','AuswahlListe','',$Inhalt);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ORTEREIGNIS'] . ':',101);
		$Form->Erstelle_TextFeld('BES_ORTEREIGNIS',$rsErgebnis->FeldInhalt('ORTEREIGNIS'), 10, 150,true, '','', '','', 'L');
		$Form->ZeileEnde();
		

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ARCHIVNR'] . ':',130);
		$Form->Erstelle_TextFeld('BES_ARCHIVNR',$rsErgebnis->FeldInhalt('VORGANGSNR'), 10, 150,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'] . ':',30);
		$Form->AuswahlBox('BES_VUFIL', 'box2','', 'BES_FIL', '', 60,2,isset($_POST['sucBES_VUFIL'])?$_POST['sucBES_VUFIL']:$rsErgebnis->FeldInhalt('VERURS_FILIALE'),'T',true,'','','',50,'','','',0);
		
		
		ob_start();
		
		
		if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_VUFIL']) or $_POST['sucBES_VUFIL'] == ''))
		{
		
			$VUFIL_ID = '0';
			$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
		}
		else
		{
			if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_VUFIL']!= '')
			{
				$VUFIL_ID= intval($_POST['sucBES_VUFIL']);
				if($_POST['sucBES_VUFIL'] == '')
				{
					$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
				}
			}
			else
			{
				if(isset($_POST['sucBES_VUFIL']) and $_POST['sucBES_VUFIL'] == '')
				{
					$VUFIL_ID = '0';
					$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
				}
				else
				{
					$VUFIL_ID = intval($rsErgebnis->FeldInhalt('VERURS_FILIALE'));
				}
			}
		
			if($FIL_ID == '0' and $Fehler == '')
			{
				$Fehler == 'keine g�ltige Fililale';
			}
		}
		
		$SQL  = ' Select * from( ';
		$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
		$SQL .= ' where fil_gruppe IS NOT NULL OR';
		$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
		$SQL .= ' union';
		$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
		$SQL .= ' FROM schaddev09.pseudofilialen)';
		$SQL .= ' where fil_id =0'.$VUFIL_ID;
		
		$rsFIL = $DB->RecordSetOeffnen($SQL);
			
		if($rsFIL->AnzahlDatensaetze() == 0 and $Fehler == '')
		{
			$Fehler = 'keine g�ltige Fililale';
		}
		
		$SQL2  = 'select * from v_filialebenenrollen_aktuell';
		$SQL2  .= ' inner join kontakte on xx1_kon_key = kon_key';
		$SQL2  .= ' where xx1_fil_id='.$VUFIL_ID;
		$SQL2  .= ' and xx1_fer_key = 25';
		
		$rsGBL = $DB->RecordSetOeffnen($SQL2);
		
		//position:absolute;left:388pt; top:213pt;
		
		if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
		{
			$Top = '18pt';
			$Left = '-185pt';
		}
		else 
		{
			$Top = '18pt';
			$Left = '-185pt';
		}
		$Form->Erstelle_TextFeld('BES_VUFIL_NAME',$Fehler == '' ? $rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,250, false, '', '', '', 'T', 'L','','',50);
		$Form->Erstelle_TextFeld('BES_GBL',$Fehler == '' ? $rsGBL->FeldInhalt('KON_NAME1'):$Fehler, 40,250, false,'position:relative;left:'.$Left.'; top:'.$Top.';', '', '', 'T', 'L','','',50);
		
		$Inhalt = ob_get_contents();
		ob_end_clean();
		
		$Form->AuswahlBoxHuelle('box2','AuswahlListe','',$Inhalt);
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGANGPER'] . ':',100);
		$SQL2 ='select ID,MITTEL from eingangper ';
		$Form->Erstelle_SelectFeld('BES_EINGANGPER',$rsErgebnis->FeldInhalt('EINGANGPER'),120,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RSCHEINNR'] . ':',130);
		$Form->Erstelle_TextFeld('BES_RSCHEINNR',$rsErgebnis->FeldInhalt('REKLASCHEINNR'), 10, 190,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GBL'] . ':',300);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGABEAM'] . ':',100);
		$Form->Erstelle_TextFeld('BES_EINGABEAM',$rsErgebnis->FeldInhalt('EINGABEAM'), 10, 150,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGANGPER'] . ':',100);
		$SQL2 ='select ID,SBNAME from SACHBEARBEITER ';
		$Form->Erstelle_SelectFeld('BES_BEARBEITER',$rsErgebnis->FeldInhalt('SACHBEARBEITER'),110,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RVAXNR'] . ':',130);
		$Form->Erstelle_TextFeld('BES_RVAXNR',$rsErgebnis->FeldInhalt('REKLASCHEINVAX'), 10, 150,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANTRAGART'] . ':',90);
		$SQL2 ='select ID,WERT from ANTRAGART ';
		$Form->Erstelle_SelectFeld('BES_AART',$rsErgebnis->FeldInhalt('ANTRAGART'),250,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GESCHLAM'] . ':',100);
		$Form->Erstelle_TextFeld('BES_GESCHLAM',$rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM'), 10, 150,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GESCHLAM'] . ':',100);
		$Form->Erstelle_TextFeld('BES_SBDAT',$rsErgebnis->FeldInhalt('DATUMEREIGNIS'), 10, 120,true, '','', '','D', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARBNRFIL'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BEARBNRFIL',$rsErgebnis->FeldInhalt('BEARBEITUNGSNRFIL'), 29, 279,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GEW'] . ':',50);
		$SQL3 ='select ID,WERT from GEWICHTUNG ';
		$Form->Erstelle_SelectFeld('BES_GEWICHTUNG',$rsErgebnis->FeldInhalt('GEWICHTUNG'),161,true,$SQL3,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$SQL4 ='select * from WIEDERVORLAGEN_NEW  ';
	    $SQL4 .= 'where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
	    $SQL3 .= ' and rownum = 1';
	    $SQL4 .= ' order by WV_DATUM asc';
	    $rsWV = $DBSCHAD->RecordSetOeffnen($SQL4);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVAM'] . ':',100);
		$Form->Erstelle_TextFeld('BES_WVAM',$Form->Format('D',$rsWV->FeldInhalt('WV_DATUM')), 10, 150,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BESICHTIGT'] . ':',100);
		$Form->Erstelle_TextFeld('BES_BESICHTIGT',$rsErgebnis->FeldInhalt('BESICHTIGT'), 10, 110,true, '','', '','', 'L');
		$Form->ZeileEnde();	
		$Form->FormularBereichInhaltEnde();
		
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_KUNDEKFZ'],false, 10);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANREDE'] . ':',130);
		$SQL5 ='select ID,NAME from anrede ';
		$Form->Erstelle_SelectFeld('BES_ANREDE',$rsErgebnis->FeldInhalt('GESCHLECHT'),380,true,$SQL5,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FABRIKAT'] . ':',110);
		$Form->Erstelle_TextFeld('BES_FABRIKAT',$rsErgebnis->FeldInhalt('FABRIKAT'), 73, 100,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_NAME'] . ':',130);
		$Form->Erstelle_TextFeld('BES_NAME',$rsErgebnis->FeldInhalt('KUNDENNAME'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_MODELL'] . ':',110);
		$Form->Erstelle_TextFeld('BES_MODELL',$rsErgebnis->FeldInhalt('TYP'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORNAME'] . ':',130);
		$Form->Erstelle_TextFeld('BES_VORNAME',$rsErgebnis->FeldInhalt('VORNAME'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TYP'] . ':',110);
		$Form->Erstelle_TextFeld('BES_TYP',$rsErgebnis->FeldInhalt('KFZTYP'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STRASSE'] . ':',130);
		$Form->Erstelle_TextFeld('BES_STRASSE',$rsErgebnis->FeldInhalt('STRASSE'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KBA'] . ':',110);
		$Form->Erstelle_TextFeld('BES_KBA',$rsErgebnis->FeldInhalt('KBANR'), 73, 190,true, '','', '','', 'L','','',15);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PLZORT'] . ':',130);
		$Form->Erstelle_TextFeld('BES_PLZ',$rsErgebnis->FeldInhalt('PLZ'), 3, 55,true, '','', '','', 'L');
		$Form->Erstelle_TextFeld('BES_ORT',$rsErgebnis->FeldInhalt('ORT'), 25, 325,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EZ'] . ':',110);
		$Form->Erstelle_TextFeld('BES_EZ',$rsErgebnis->FeldInhalt('ERSTZULASSUNG'),10, 190,true, '','', '','D', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON'] . ':',130);
		$Form->Erstelle_TextFeld('BES_TELEFON',$rsErgebnis->FeldInhalt('TELEFON'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KM'] . ':',110);
		$Form->Erstelle_TextFeld('BES_KM',$rsErgebnis->FeldInhalt('KM'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON2'] . ':',130);
		$Form->Erstelle_TextFeld('BES_TELEFON2',$rsErgebnis->FeldInhalt('TELEFON2'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KFZKZ'] . ':',110);
		$Form->Erstelle_TextFeld('BES_KENNZ',$rsErgebnis->FeldInhalt('KFZ_KENNZ'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EMAIL'] . ':',130);
		$Form->Erstelle_TextFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('EMAIL'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FAX'] . ':',110);
		$Form->Erstelle_TextFeld('BES_FAX',$rsErgebnis->FeldInhalt('FAX'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GROSSKDNR'] . ':',130);
		$Form->Erstelle_TextFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('GROSSKDNR'), 25, 380,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ATUCARDNR'] . ':',110);
		$Form->Erstelle_TextFeld('BES_FAX',$rsErgebnis->FeldInhalt('PAN'), 73, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_AUFTRAG'],false, 10);
	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUFTRAG'] . ':',130);
		$Form->Erstelle_TextFeld('BES_AUFTRAG',$rsErgebnis->FeldInhalt('BEZEICHNUNG'), 155, 200,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNG'] . ':',130);
		$SQL6 ='select KENNUNG,KENNUNG_BEZEICHNUNG from KENNUNGEN ';
		$Form->Erstelle_SelectFeld('BES_KENNUNG',$rsErgebnis->FeldInhalt('KENNUNG'),348,true,$SQL6,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_MECHANIKER'] . ':',80);
		$Form->Erstelle_TextFeld('BES_MECHANIKER',$rsErgebnis->FeldInhalt('MECHANIKER'), 20, 165,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_UAUFTRAG'] . ':',120);
		$SQL7 ='select ART_ID,AUFTRAGSART_ATU from AUFTRAGSARTEN ';
		$Form->Erstelle_SelectFeld('BES_AUFTRAGSART_NEU',$rsErgebnis->FeldInhalt('AUFTRAGSART_ATU_NEU'),350,true,$SQL7,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUFTRAGSDATUM'] . ':',130);
		$Form->Erstelle_TextFeld('BES_AUFTRAGSDATUM',$rsErgebnis->FeldInhalt('AUFTRAGSDATUM'), 10, 165,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KAUFDATUM'] . ':',100);
		$Form->Erstelle_TextFeld('BES_KAUFDATUM',$rsErgebnis->FeldInhalt('KAUFDATUM'), 10, 220,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUWA'] . ':',100);
		$Form->Erstelle_TextFeld('BES_VUWA',$rsErgebnis->FeldInhalt('WANR'), 10, 163,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BONNR'] . ':',100);
		$Form->Erstelle_TextFeld('BES_BONNR',$rsErgebnis->FeldInhalt('BONNR'), 11, 165,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUATUNR'] . ':',130);
		$Form->Erstelle_TextFeld('BES_AUFTRAGSDATUM',$rsErgebnis->FeldInhalt('ATUNR'), 10, 135,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_LIEFERANT'] . ':',130);
		
		
		$Form->AuswahlBox('BES_LIEFERANT', 'box3','', 'BES_LIEF', '', 60,2,isset($_POST['sucBES_LIEFERANT'])?$_POST['sucBES_LIEFERANT']:$rsErgebnis->FeldInhalt('LIEFERANT'),'T',true,'','','',50,'','','',0);
		
		$Inhalt = '';
		$Fehler = '';
		$LIE_NR = '';
		ob_start();
		
		
		if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_LIEFERANT']) or $_POST['sucBES_LIEFERANT'] == ''))
		{
			$LIE_NR = '0';
			$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
		}
		else
		{
			if(isset($_POST['sucBES_LIEFERANT']) and $_POST['sucBES_LIEFERANT']!= '')
			{
				$LIE_NR= intval($_POST['sucBES_LIEFERANT']);
				if($_POST['sucBES_LIEFERANT'] == '')
				{
					$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
				}
			}
			else
			{
				if(isset($_POST['sucBES_LIEFERANT']) and $_POST['sucBES_LIEFERANT'] == '')
				{
					$LIE_NR = '0';
					$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
				}
				else
				{
					$LIE_NR = intval($rsErgebnis->FeldInhalt('LIEFERANT'));
				}
			}
		
			if($LIE_NR == '0' and $Fehler == '')
			{
				$Fehler == 'kein g�ltiger Lieferant';
			}
		}
		
		$SQL  = ' Select * from LIEFERANTEN WHERE LIE_NR=\''.$rsErgebnis->FeldInhalt('LIEFERANT').'\'';
		
		$rsLIE = $DB->RecordSetOeffnen($SQL);
			
		if($rsLIE->AnzahlDatensaetze() == 0 and $Fehler == '')
		{
			$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
		}
		$Form->Erstelle_TextFeld('BES_LIENAME',$Fehler == '' ? $rsLIE->FeldInhalt('LIE_NAME1'):$Fehler, 200,423, false, '', '', '', 'T', 'L','','',50);
		
		$Inhalt = ob_get_contents();
		ob_end_clean();
		
		$Form->AuswahlBoxHuelle('box3','AuswahlListe','',$Inhalt);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_LIEFARTNR'] . ':',100);
		$Form->Erstelle_TextFeld('BES_LIEFARTNR',$rsErgebnis->FeldInhalt('LIEFARTNR'), 11, 135,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DATUMREG'] . ':',130);
		$Form->Erstelle_TextFeld('BES_DATUMREG',$rsErgebnis->FeldInhalt('DATUMREGULIERUNG'), 10, 142,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BONREG'] . ':',123);
		$Form->Erstelle_TextFeld('BES_KAUFDATUM',$rsErgebnis->FeldInhalt('BONNRREGULIERUNG'), 10, 483,true, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERKAEUFER'] . ':',100);
		$Form->Erstelle_TextFeld('BES_VERKAEUFER',$rsErgebnis->FeldInhalt('VERK�UFER'), 11, 163,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		
		
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_FEHLERDIAG'],false, 10);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WORANARB'] . ':',130);
		$SQL7 ='select ID,GRUND from SCHADENSGRUND ';
		$Form->Erstelle_SelectFeld('BES_WORANARB',$rsErgebnis->FeldInhalt('SCHADENSGRUND'),220,true,$SQL7,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WASBESCH'] . ':',130);
		$SQL8 ='select BID,SCHLAGWORT from BESCHAEDIGT ';
		$Form->Erstelle_SelectFeld('BES_WASBESCH',$rsErgebnis->FeldInhalt('BID'),260,true,$SQL8,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUSFALLURSACHE'] . ':',63);
		$SQL9 ='select ID,WERT from AUSFALLURSACHE ';
		$Form->Erstelle_SelectFeld('BES_AUSFALLURSACHE',$rsErgebnis->FeldInhalt('AUSFALLURSACHE'),348,true,$SQL9,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FGLIEFERANT'] . ':',130);
		$SQL10 ='select ID,WERT from FGLIEFERANT ';
		$Form->Erstelle_SelectFeld('BES_FGLIEFERANT',$rsErgebnis->FeldInhalt('FGLIEFERANT'),220,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FGMARKE'] . ':',130);
		$SQL10 ='select ID,WERT from FGMARKE ';
		$Form->Erstelle_SelectFeld('BES_FGMARKE',$rsErgebnis->FeldInhalt('FGMARKE'),173,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ZUSATZINFO'] . ':',100);
		$Form->Erstelle_TextFeld('BES_ZUSATZINFO',$rsErgebnis->FeldInhalt('ZUSATZINFO'), 50, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FEHLER'] . ':',130);
		$Form->Erstelle_TextFeld('BES_FEHLER',$rsErgebnis->FeldInhalt('FEHLERBESCHREIBUNG'), 156, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DIAGWL'] . ':',130);
		$Form->Erstelle_TextFeld('BES_DIAGWL',$rsErgebnis->FeldInhalt('DIAGNOSE_WERKSTATTLEITER'), 156, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BESPMA'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BESPMA',$rsErgebnis->FeldInhalt('DIAGNOSE_TKDL'), 156, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEMINTERN'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BEMINTERN',$rsErgebnis->FeldInhalt('BEMINTERN'), 156, 190,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KONKUNDE'] . ':',130);
		$SQL10 ='select ID,WERT from KONTAKT_KUNDE ';
		$Form->Erstelle_SelectFeld('BES_KONKUNDE',$rsErgebnis->FeldInhalt('KONTAKT_KUNDE'),220,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TERMIN1'] . ':',130);
		$Form->Erstelle_TextFeld('BES_TERMIN1',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_1'), 10, 175,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WER1'] . ':',92);
		$Form->Erstelle_TextFeld('BES_WER1',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_WER_1'), 51, 120,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GBLINFO'] . ':',130);
		$Daten = explode('|',$AWISSprachKonserven['BES']['BES_LST_JANEIN']);
		$Form->Erstelle_SelectFeld('BES_GBLINFO',$rsErgebnis->FeldInhalt('INFO_AN_GBL'),220,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TERMIN2'] . ':',130);
		$Form->Erstelle_TextFeld('BES_TERMIN2',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_2'), 10, 175,true, '','', '','D', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WER2'] . ':',92);
		$Form->Erstelle_TextFeld('BES_WER2',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_WER_2'), 51, 120,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STAND'].':', 130, '');
		$SQL2 ='select STANDID,STANDBEM from STAND where ORT=\'ATU\' and veraltet = 0 order by standbem ';
		$Form->Erstelle_SelectFeld('BES_STAND',$rsErgebnis->FeldInhalt('STAND'),110,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();
		
		$Form->FormularBereichInhaltStart($AWISSprachKonserven['BES']['UEBERSCHRIFT_FORDERUNG'],true, 10);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WHERSTELLER'] . ':',130);
		$Form->Erstelle_TextFeld('BES_WHERSTELLER',$rsErgebnis->FeldInhalt('WEITERGELEITET_HERSTELLER'), 10, 223,true, '','', '','D', 'L');
		$SQL11='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL11.=' and bid=1';
		$rsHerst = $DBSCHAD->RecordSetOeffnen($SQL11);
		if($rsHerst->FeldInhalt('BETRAG') == '')
		{
			$HBetrag = 0;
		}
		else
		{
			$HBetrag = $rsHerst->FeldInhalt('BETRAG');
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRHER'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BETRHER',$Form->Format('N2',$HBetrag).'&euro;', 25, 250,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANWALT'] . ':',80);
		$Form->Erstelle_Checkbox('CHK_AKTIV',$rsErgebnis->FeldInhalt('ANWALTSSACHE'),65,true,-1);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AZ'] . ':',100);
		$Form->Erstelle_TextFeld('AZ',$rsErgebnis->FeldInhalt('AKTENZEICHENRA'), 11, 165,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVERS'] . ':',130);
		$Form->Erstelle_TextFeld('BES_WVERS',$rsErgebnis->FeldInhalt('WEITERGELEITETVERSICHERUNG'), 10, 223,true, '','', '','D', 'L');
		$SQL12='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL12.=' and bid=2';
		$rsVERS = $DBSCHAD->RecordSetOeffnen($SQL12);
		if($rsVERS->FeldInhalt('BETRAG') == '')
		{
			$VBetrag = 0;
		}
		else
		{
			$VBetrag = $rsVERS->FeldInhalt('BETRAG');
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRVERS'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BETRVERS',$Form->Format('N2',$VBetrag).'&euro;', 25, 395,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RAFORD'] . ':',100);
		$Form->Erstelle_TextFeld('BES_BONNR',$Form->Format('N2',$rsErgebnis->FeldInhalt('RAFORDERUNG')).'&euro;', 11, 165,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$SQL13='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL13.=' and bid=6';
		$rsVORK = $DBSCHAD->RecordSetOeffnen($SQL13);
		if($rsVORK->FeldInhalt('BETRAG') == '')
		{
			$VORBetrag = 0;
		}
		else
		{
			$VORBetrag = $rsVORK->FeldInhalt('BETRAG');
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORKATU'] . ':',130);
		$Form->Erstelle_TextFeld('BES_VORKATU',$Form->Format('N2',$VORBetrag).'&euro;', 25, 223,false, '','', '','', 'L');
		$SQL14='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL14.=' and bid=3';
		$rsVORK = $DBSCHAD->RecordSetOeffnen($SQL14);
		if($rsVORK->FeldInhalt('BETRAG') == '')
		{
			$BETBetrag = 0;
		}
		else
		{
			$BETBetrag = $rsVORK->FeldInhalt('BETRAG');
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRAGATU'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BETRAGATU',$Form->Format('N2',$BETBetrag).'&euro;', 25, 395,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GERICHT'].':', 100, '');
		$SQL15 ='select STANDID,STANDBEM from STAND where ORT=\'Gericht\' and veraltet = 0 order by STANDBEM ';
		$Form->Erstelle_SelectFeld('BES_GERICHT',$rsErgebnis->FeldInhalt('STANDBEIGERICHT'),110,true,$SQL15,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FORDERUNG'] . ':',130);
		$Form->Erstelle_TextFeld('BES_FORDERUNG',$Form->Format('N2',$rsErgebnis->FeldInhalt('FORDERUNG')).'&euro;', 25, 223,true, '','', '','', 'L');	
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SUMSCHAD'] . ':',130);
		$Form->Erstelle_TextFeld('BES_BETRAGATU',$Form->Format('N2',$HBetrag+$VBetrag+$BETBetrag).'&euro;', 25, 395,false, '','', '','', 'L');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AZGERICHT'] . ':',100);
		$Form->Erstelle_TextFeld('BES_AZGERICHT',$rsErgebnis->FeldInhalt('AKTENZEITENGERICHT'), 11, 165,true, '','', '','', 'L');
		$Form->ZeileEnde();
		$Form->FormularBereichInhaltEnde();

		
		$Form->FormularBereichEnde();
		
	
		
	}
	else 
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	//$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	if($SchnellSucheFeld and $rsErgebnis->AnzahlDatensaetze() == 1)
	{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheBearb'].':',150,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheBearb']);
		$Form->Erstelle_TextFeld('*BES_BEARBEITUNG','',10,0,true,'','','','T','L','','',10,'','f');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheKFZ'].':',45,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheKFZ']);
		$Form->Erstelle_TextFeld('*BES_KFZ','',15,0,true,'','','','T','L','','',15,'','f');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheKUNDE'].':',60,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheKUNDE']);
		$Form->Erstelle_TextFeld('*BES_KUNDE','',30,0,true,'','','','T','L','','',30,'','f');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	}
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_BES',serialize($Param));
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	$Recht39003 = $AWISBenutzer->HatDasRecht(39003);
	
	
	if(isset($Param['BES_FIL_ID']) AND $Param['BES_FIL_ID']!='' AND $Param['BES_FIL_ID']!=0)
	{
		$Bedingung .= 'AND FILNR ' . $DB->LikeOderIst($Param['BES_FIL_ID']) . ' ';
	}
	
	if(isset($Param['BES_BEARBEITUNG']) AND $Param['BES_BEARBEITUNG']!='')
	{
		$Bedingung .= 'AND BEARBEITUNGSNR ' . $DB->LikeOderIst($Param['BES_BEARBEITUNG']) . ' ';
	}
	if(isset($Param['BES_VORGANG']))
	{
		if($Param['BES_VORGANG']=='O')
		{
			$Bedingung .= " AND (AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";
		}
		elseif($Param['BES_VORGANG']=='G')
		{
			$Bedingung .= " AND AKTEGESCHLOSSENAM is not null";
		}
	}
	if(isset($Param['BES_AART']) AND $Param['BES_AART']!='' AND $Param['BES_AART'] != '0')
	{
		$Bedingung .= ' AND ANTRAGART ' . $DB->LikeOderIst($Param['BES_AART']) . ' ';
	}
	
	if (isset($Param['BES_DATUMVON']) AND (!empty($Param['BES_DATUMVON'])))
	{
		$Bedingung .= ' AND (eingabeam >= ' . $DB->FeldInhaltFormat('D', $Param['BES_DATUMVON']) . ')';
	}
	 
	if (isset($Param['BES_DATUMBIS']) AND (!empty($Param['BES_DATUMBIS'])))
	{
		$Bedingung .= ' AND (eingabeam <= ' . $DB->FeldInhaltFormat('D', $Param['BES_DATUMBIS']) . ')';
	}
	
	if(isset($Param['BES_KUNDE']) AND $Param['BES_KUNDE']!='')
	{
		$NameList ='';
		$Namen = explode(' ',$Param['BES_KUNDE']);
		
			foreach ($Namen as $Name)
			{
				if($Name != '')
				{
				
					$Bedingung .= 'AND (UPPER(KUNDENNAME)' . $DB->LikeOderIst($Name,1) . ' ';
					$Bedingung .= 'OR UPPER(VORNAME)' . $DB->LikeOderIst($Name,1) . ') ';
				}
			}
		
		
	}
	if(isset($Param['BES_STRASSE']) AND $Param['BES_STRASSE']!='')
	{
		$Bedingung .= 'AND UPPER(STRASSE) ' . $DB->LikeOderIst($Param['BES_STRASSE'],1) . ' ';
	}
	if(isset($Param['BES_PLZ']) AND $Param['BES_PLZ']!='')
	{
		$Bedingung .= 'AND UPPER(PLZ) ' . $DB->LikeOderIst($Param['BES_PLZ'],1) . ' ';
	}
	if(isset($Param['BES_ORT']) AND $Param['BES_ORT']!='')
	{
		$Bedingung .= 'AND UPPER(ORT) ' . $DB->LikeOderIst($Param['BES_ORT'],1) . ' ';
	}
	if(isset($Param['BES_TELEFON']) AND $Param['BES_TELEFON']!='')
	{
		$Bedingung .= 'AND UPPER(TELEFON) ' . $DB->LikeOderIst($Param['BES_TELEFON'],1) . ' ';
	}
	if(isset($Param['BES_EMAIL']) AND $Param['BES_EMAIL']!='')
	{
		$Bedingung .= 'AND UPPER(EMAIL) ' . $DB->LikeOderIst($Param['BES_EMAIL'],1) . ' ';
	}
	if(isset($Param['BES_CARD']) AND $Param['BES_CARD']!='')
	{
		$Bedingung .= 'AND UPPER(PAN) ' . $DB->LikeOderIst($Param['BES_CARD'],1) . ' ';
	}
	if(isset($Param['BES_SACHBEARBEITER']) AND $Param['BES_SACHBEARBEITER']!='')
	{
		$Bedingung .= 'AND UPPER(SBNAME) ' . $DB->LikeOderIst($Param['BES_SACHBEARBEITER'],1) . ' ';
	}
	if (isset($Param['BES_KFZ']) AND (! empty($Param['BES_KFZ'])))
	{
		$Bedingung .= ' AND (UPPER(lartnrkomp(KFZ_KENNZ)) like UPPER(\'%\'||lartnrkomp(\'' . $Param['BES_KFZ'] . '\')||\'%\'))';
	}
	
	return $Bedingung;
}

?>