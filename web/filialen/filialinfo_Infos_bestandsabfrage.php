<?php
//********************************************************************************************
// Bestandsabfrage
//********************************************************************************************
//
// Dieses Modul ermöglicht die Anzeige der Bestandsabfragen von den Filialen
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 20.07.2007
//********************************************************************************************
// Änderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
require_once("awis_forms.inc.php");
require_once("db.inc.php");

global $con;
global $awisRSZeilen;
global $AWISSprache;
global $AWISBenutzer;

$TXT_Baustein = array();
$TXT_Baustein[]=array('BAF','BAF_%');
$TXT_Baustein[]=array('Wort','KeineBestandsabfragen');
$TXT_Baustein[]=array('Liste','lst_LIK_STATUS');
$TXT_Baustein[]=array('Wort','KeineLieferkontrollen');
$TXT_Baustein[]=array('Wort','Abgeschlossen');
$TXT_Baustein[]=array('Wort','NichtAbgeschlossen');
$TXT_Baustein[]=array('Wort','Uploaddatei');
$TXT_Baustein[]=array('Wort','KeineBerechtigungBestandsabfragen');
$TXT_Baustein[]=array('Wort','PDFErzeugen');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];

$RechteStufe = awisBenutzerRecht($con,3601);	// MDE Bestandsabfrage
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");          
}

// Beschränkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
//var_dump($UserFilialen); 

if($UserFilialen!='' AND intval($FIL_ID) <> intval($UserFilialen))
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBerechtigungBestandsabfragen'],1);
	die;
}

$SQL = 'SELECT BAF_AST_ATUNR, AST_BEZEICHNUNGWW, BAF_DATUMSCAN, BAF_MENGESCAN, FIB_BESTAND, RUF_REGUNDFACHNR, AST_VK' ; 
$SQL .= ' FROM BESTANDSABFRAGEN';
$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = BAF_AST_ATUNR';
$SQL .= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = BAF_AST_ATUNR AND FIB_FIL_ID = BAF_FIL_ID';
$SQL .= ' LEFT JOIN REGALUNDFACHNUMMER ON RUF_AST_ATUNR = BAF_AST_ATUNR AND RUF_FIL_ID = BAF_FIL_ID AND RUF_AKTIV = 1';
$SQL .= ' WHERE BAF_FIL_ID=0'.intval($FIL_ID);
$SQL .= ' ORDER BY BAF_AST_ATUNR ASC';

awis_Debug(1,$SQL);

$rsBAF=awisOpenRecordset($con,$SQL);
$rsBAFZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

if($rsBAFZeilen==0)
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBestandsabfragen'],1);
}

else 
{	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['BAF']['BAF_DATUMSCAN'],150);
	awis_FORM_Erstelle_TextFeld('BAF_DATUMSCAN',$rsBAF['BAF_DATUMSCAN'][0],0,100,false,'','','','D');
	awis_FORM_ZeileEnde();
	
	//$PDF_Link='./filialinfo_Infos_bestandsabfrage_pdf.php?BAF_DATUMSCAN='.$rsBAF['BAF_DATUMSCAN'][0].'&BAF_FIL_ID='.$FIL_ID; 
	$PDF_Link='./filialinfo_Infos_bestandsabfrage_pdf.php?BAF_FIL_ID='.$FIL_ID; 
	echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
	
	awis_FORM_Trennzeile();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_AST_ATUNR'],80);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_AST_BEZEICHNUNG'],180);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_FILIALBESTAND'],120);
	
	if (($RechteStufe & 2) == 2)
	{
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_MENGESCAN'],120);
	}
	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_VK'],100);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['BAF']['BAF_REGALUNDFACHNUMMER'],300);	
	awis_FORM_ZeileEnde();	
	
	for($BAFZeile=0;$BAFZeile<$rsBAFZeilen;$BAFZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsBAF['BAF_AST_ATUNR'][$BAFZeile].'&cmdAktion=ArtikelInfos';
		awis_FORM_Erstelle_ListenFeld('BAF_AST_ATUNR',$rsBAF['BAF_AST_ATUNR'][$BAFZeile],10,80,false,($BAFZeile%2),'',$Link,'T','L');
		awis_FORM_Erstelle_ListenFeld('BAF_AST_BEZEICHNUNG',substr($rsBAF['AST_BEZEICHNUNGWW'][$BAFZeile],0,15),10,180,false,($BAFZeile%2),'','','T','L');
		awis_FORM_Erstelle_ListenFeld('BAF_FILIALBESTAND',$rsBAF['FIB_BESTAND'][$BAFZeile],10,120,false,($BAFZeile%2),'','','N','L');
		
		if (($RechteStufe & 2) == 2)
		{
			awis_FORM_Erstelle_ListenFeld('BAF_MENGESCAN',$rsBAF['BAF_MENGESCAN'][$BAFZeile],10,120,false,($BAFZeile%2),'','','N','L');
		}
		
		awis_FORM_Erstelle_ListenFeld('BAF_VK]',$rsBAF['AST_VK'][$BAFZeile],10,100,false,($BAFZeile%2),'','','N','L');
		awis_FORM_Erstelle_ListenFeld('BAF_REGALUNDFACHNUMMER',$rsBAF['RUF_REGUNDFACHNR'][$BAFZeile],10,300,false,($BAFZeile%2),'','','T','L');
		awis_FORM_ZeileEnde();
	}
}
awis_FORM_FormularEnde();
?>