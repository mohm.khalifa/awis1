<?php
	//global $HTTP_GET_VARS;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $con;
	global $AWISBenutzer;

	$Rechte_LAR = awisBenutzerRecht($con,402);
	$Recht400 = awisBenutzerRecht($con,400);

	// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	//$SQL = "SELECT * ";
	//$SQL .= " FROM AWIS.LieferantenArtikel, AWIS.Lieferanten, AWIS.ArtikelPruefPersonal, AWIS.TeileInfos WHERE APP_ID(+) = LAR_APP_ID AND LIE_NR(+) = LAR_LIE_NR AND TEI_KEY2 = LAR_KEY AND TEI_KEY1=0" . $Artikel[0] . "";
	
	$SQL="SELECT AA.LAR_KEY, AA.LAR_LARTNR, AA.LAR_LIE_NR, AA.LAR_ALTERNATIVARTIKEL, AA.LAR_ALTELIEFNR, ";
	$SQL.="AA.LAR_BEMERKUNGEN, AA.LAR_AUSLAUFARTIKEL, AA.LAR_PROBLEMARTIKEL, AA.LAR_PROBLEMBESCHREIBUNG, ";
	$SQL.="AA.LAR_GEPRUEFT, AA.LAR_APP_ID, AA.LAR_MUSTER, AA.LAR_BEZWW, AA.LAR_BEKANNTWW, AA.LAR_IMQ_ID, ";    
	$SQL.="EE.ALI_WERT AS LAR_REKLAMATIONSKENNUNG, AA.LAR_SUCH_LARTNR, AA.LAR_USER, AA.LAR_USERDAT, AA.LAR_LARTID, BB.*, CC.*, DD.* ";
    $SQL.="FROM AWIS.LIEFERANTENARTIKEL AA, AWIS.LIEFERANTEN BB, AWIS.ARTIKELPRUEFPERSONAL CC, AWIS.TEILEINFOS DD, ";
    $SQL.="(SELECT * FROM AWIS.ASTLARINFOS WHERE ALI_ALT_ID=2 AND ALI_AST_KEY=0" . $Artikel[0] . ") EE ";
   	$SQL.="WHERE CC.APP_ID(+) = AA.LAR_APP_ID AND BB.LIE_NR(+) = AA.LAR_LIE_NR AND DD.TEI_KEY2 = AA.LAR_KEY ";
    $SQL.="AND EE.ALI_LAR_KEY(+)=DD.TEI_KEY2 AND DD.TEI_KEY1 = 0" . $Artikel[0] . "";

	//$SQL .= " AND LAR_REKLAMATIONSKENNUNG IS NULL";
	awis_Debug(1,($Recht400&128)==128);
	
	if(!($Recht400&128)==128)
	{
		awis_Debug(1,"Hallo");
		//$SQL .= " AND LAR_IMQ_ID=2";
		$SQL .= " AND TEI_USER='WWS'";
	}
	//TR 05.11.07 Ausblenden von Lieferantenartikel mit einer Reklamationskennung
	If (!($Recht400&2048)==2048) 
	{
		$SQL.=" AND LAR_REKLAMATIONSKENNUNG is null"; 
	}
	
	if(!isset($_GET['LARSort']))
	{
		$SQL .= " ORDER BY LIE_NR, LAR_LARTNR";
	}
	else
	{
		$SQL .= " ORDER BY " . $_GET['LARSort'] . ", LAR_LARTNR";
	}
awis_Debug(1,$SQL);

	$rsLiefArt = awisOpenRecordset($con, $SQL);
	if($rsLiefArt==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsLiefartZeilen=$awisRSZeilen;

	$MitReklInfo = awis_BenutzerParameter($con,"ReklamationsInfoLieferanten" , $AWISBenutzer->BenutzerName() );

	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr><td id=FeldBez></td>";
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LARTNR&Key=" . $Artikel[0] . " onmouseover=\"window.status='Sortieren.';return true;\" onmouseout=\"window.status='';return true;\"><u>L</u>iefArtNr</a></td>";
	if($MitReklInfo)
	{
		print "<td width=40 id=FeldBez>Info</td>";
	}
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LIE_NR&Key=" . $Artikel[0] . " onmouseover=\"window.status='Sortieren.';return true;\" onmouseout=\"window.status='';return true;\">LiefNr</a></td>";
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LIE_NAME1&Key=" . $Artikel[0] . " onmouseover=\"window.status='Sortieren.';return true;\" onmouseout=\"window.status='';return true;\">Lieferant</a></td>";
	print '<td id=FeldBez>Teileinfo</td>';
//	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=APP_NAME&Key=" . $Artikel[0] . ">Pr�fer</a></td>";

	print "<td id=FeldBez>Datum</td>";
	print "<td id=FeldBez>Zuord.</td>";
	print "<td id=FeldBez>Rekl.</td>";
	print "</tr>";

			// Hinzuf�gen von Datens�tzen
	if(($Rechte_LAR & 2)==2)
	{
		print "<tr>";

		// Lieferantenartikel
		print "<td></td>";
		print "<td><input accesskey=l name=txtLAR_LARTNR_0></td>";
		if($MitReklInfo)
		{
			print "<td>&nbsp;</td>";
		}

		print "<td><input name=txtLAR_LIE_NR size=6 onchange=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value=''> oder </td>";
		print "<td><select name=txtLAR_LIE_NR2 onchange=document.getElementsByName(\"txtLAR_LIE_NR\")[0].value=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value;>";

		$rsLieferanten = awisOpenRecordset($con, "SELECT * FROM AWIS.Lieferanten ORDER BY LIE_Name1");
		$rsLieferantenZeilen = $awisRSZeilen;
		print "<option value=0>Bitte w�hlen...</option>";

		for($LiefNr=0;$LiefNr<$rsLieferantenZeilen;$LiefNr++)
		{
			print "<option value=" . $rsLieferanten["LIE_NR"][$LiefNr] . ">" . $rsLieferanten["LIE_NAME1"][$LiefNr] . "</option>";
		}
		unset($rsLieferanten);

		print "</select></td>";


		print "<td>/</td>";			// Pr�fer
		print "<td>/</td>";
		print "<td>/</td>";
		print "<td>/</td>";			// Reklamation

		print "</tr>";

	}

	for($LiefArt=0;$LiefArt<$rsLiefartZeilen;$LiefArt++)
	{
		print "<tr>";

		$ZweitArtikel = +1;
			// Zwei gleiche Artikel, Nur die Benutzer sind in der falschen Reihenfolge
		if(isset($rsLiefArt["LAR_LARTNR"][$LiefArt+1]) AND $rsLiefArt["LAR_LARTNR"][$LiefArt]==$rsLiefArt["LAR_LARTNR"][$LiefArt+1] AND
			$rsLiefArt['LAR_LIE_NR'][$LiefArt]==$rsLiefArt['LAR_LIE_NR'][$LiefArt+1] AND
			$rsLiefArt['TEI_USER'][$LiefArt]=='WWS')
		{
			$LiefArt++;
			$ZweitArtikel = -1;
		}
	

		if(($Rechte_LAR & 2)==2 AND strtoupper($rsLiefArt["TEI_USER"][$LiefArt])!='WWS')
		{
			print "<td><input name=cmdLARLoeschen_" . $rsLiefArt["TEI_KEY"][$LiefArt] . " type=image src=/bilder/muelleimer.png title=\"Zuordnung l�schen\" onmouseover=\"window.status='Zuordnung l�schen.';return true;\" onmouseout=\"window.status='';return true;\"></td>";
		}
		else
		{
			print "<td></td>";
		}

			// 1. Spalte -> LieferantenArtikelNummer
		echo "<td nowrap ><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt] . " onmouseover=\"window.status='Lieferantenartikel anzeigen.';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >" . $rsLiefArt["LAR_LARTNR"][$LiefArt] . "</a>";
		if(!isset($_GET['LAS']))		// Anzeige der LieferantenArtikelSets - Einzelteile
		{
			$SQL = "SELECT DISTINCT T1.TEI_SUCH1";
			$SQL .= " FROM TeileInfos T1 ";
			$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
			$SQL .= " AND T1.TEI_SUCH2 " . awisLIKEoderIST($rsLiefArt['LAR_LARTNR'][$LiefArt],false,false,true,false);

			$rsLAS = awisOpenRecordset($con, $SQL);
			$rsLASZeilen = $awisRSZeilen;
			if($rsLASZeilen>0)
			{
				echo " (<a title='Lieferantenset aus " . $rsLASZeilen . " Teilen' href=./artikel_Main.php?Key=" . $Artikel[0] . '&cmdAktion=ArtikelInfos&LAS=Ja>' . $rsLASZeilen . ' Teile</a>)';
			}
		}
		echo '</td>';



		if($MitReklInfo)
		{
			print "<td align=center valign=center>";
			$DateiName = realpath('../dokumente/lieferanten/reklamationen') . '/lief_rekl_' . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . '.pdf';
			if(file_exists($DateiName))
			{
				print "<a href=../dokumente/lieferanten/reklamationen/lief_rekl_" . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . ".pdf><img border=0 src=/bilder/pdf.png title=Anleitung onmouseover=\"window.status='Reklamation.';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Reklamation';return true;\"></a>";
			}
			print "</td>";
		}
			// 2. Spalte -> LieferantenNummer
		echo "<td><a href=/stammdaten/lieferanten.php?ASTKEY=" . $Artikel[0] . "&Zurueck=artikel_LIEF&LIENR=" . $rsLiefArt["LAR_LIE_NR"][$LiefArt] . " onmouseover=\"window.status='Lieferantendaten anzeigen.';return true;\" onmouseout=\"window.status='';return true;\">" . $rsLiefArt["LAR_LIE_NR"][$LiefArt] . "</a></td>";

			// 3. Spalte -> LieferantenName
		if($rsLiefArt["LIE_NAME1"][$LiefArt]=='')
		{
			echo '<td>::unbekannt::</td>';
		}
		else
		{
			echo '<td>' . $rsLiefArt["LIE_NAME1"][$LiefArt] . '</td>';
		}
			// 3a. Spalte

		$Anzeige ='';
		if($rsLiefArt["LAR_ALTERNATIVARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige = ', Akt';
		}
		if($rsLiefArt["LAR_AUSLAUFARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige .= ', Ausl';
		}
		if($rsLiefArt["LAR_ALTELIEFNR"][$LiefArt]!=0)
		{
			$Anzeige .= ', Alt';
		}
		if($rsLiefArt["LAR_PROBLEMARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige .= ', Prob';
		}
		if($rsLiefArt["LAR_GEPRUEFT"][$LiefArt]!=0)
		{
			$Anzeige .= ', Gepr';
		}
		if($rsLiefArt["LAR_MUSTER"][$LiefArt]!=0)
		{
			$Anzeige .= ', M';
		}
		if($rsLiefArt["LAR_BEMERKUNGEN"][$LiefArt].''!='')
		{
			$Anzeige .= ', B';
		}
		echo "<td>" . substr($Anzeige,2) . "</td>";

			// 4. Spalte -> Pr�fer
//		echo "<td>" . substr($rsLiefArt["APP_NAME"][$LiefArt],0,5) . "</td>";


			// 4a -> Letzte �nderung
		if($rsLiefArt["TEI_USER"][$LiefArt]!='WWS')
		{
			echo "<td>" . $rsLiefArt["TEI_USERDAT"][$LiefArt] . "</td>";
		}
		else
		{
			echo "<td>&nbsp;</td>";
		}

			// 5. Spalte -> Zuordnung (mit Link)
		echo "<td>";
				// Zuordnung aus Crossing
		echo "<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt] . " onmouseover=\"window.status='Lieferantenartikel anzeigen.';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status=''>";
		echo $rsLiefArt["TEI_USER"][$LiefArt] . "</a>";

				// Zuordnung aus WWS
		if(isset($rsLiefArt["LAR_LARTNR"][$LiefArt+$ZweitArtikel]) AND $rsLiefArt["LAR_LARTNR"][$LiefArt]==$rsLiefArt["LAR_LARTNR"][$LiefArt+$ZweitArtikel] AND
			$rsLiefArt['LAR_LIE_NR'][$LiefArt]==$rsLiefArt['LAR_LIE_NR'][$LiefArt+$ZweitArtikel])
		{
			echo "/<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt+$ZweitArtikel] . " onmouseover=\"window.status='Lieferantenartikel anzeigen.';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >";
			echo $rsLiefArt["TEI_USER"][$LiefArt+$ZweitArtikel] . "</a>";
			if($ZweitArtikel ==1)
			{
				$LiefArt++;
			}
		}
		echo "</td>";



			// 6. Spalte
		print "<td>" . ($rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]==''?'- -':"<font color=#FF0000>" . $rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]) . "</font></td>";

		print "</tr>";

		// Gebrauchsnummern anzeigen
		if(isset($_GET["GNr"]) AND $_GET["GNr"]=='Ja')
		{
			$rsGNr = awisOpenRecordset($con, "SELECT * FROM AWIS.Gebrauchsnummern, AWIS.TEILEINFOS WHERE GNR_KEY = TEI_KEY2	 AND TEI_KEY1 = " . $rsLiefArt["LAR_KEY"][$LiefArt]);
awis_Debug(1,$SQL);			
			$rsGNRZeilen = $awisRSZeilen;

			if($rsGNRZeilen>0)
			{
 				print "<tr><td></td><td colspan=4>";
				print "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
				for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
				{
					print "<td>Gebrauchsnummer:</td>";
					print "<td><input name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
					print "</td>";
					print "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
					print "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
					print "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
				}
				print "</tr></table>";
				print "</td></tr>";
			}
			else
			{
				print "\n<!--Keine Gebrauchsnummern gefunden-->\n";
			}

			unset($rsGNr);
		}

		if(isset($_GET['LAS']) AND $_GET['LAS']=='Ja')	// Lieferantenartikelsets anzeigen
		{
			// Doppelte Artikel nicht mehr anzeigen. Kann sein, dass dies durch Nachfolgeartikel wirklich mehrfach erfasst ist.
			$SQL = 'SELECT DISTINCT LAR_LARTNR, AST_KEY, AST_ATUNR, LAR_BEZWW';
			$SQL .= " FROM TeileInfos T1 INNER JOIN LieferantenArtikel ON T1.TEI_WERT1 = LAR_LARTNR";
			$SQL .= " LEFT OUTER JOIN TeileInfos T2 ON T1.TEI_SUCH1=T2.TEI_SUCH2 ";
			$SQL .= " LEFT OUTER JOIN ArtikelStamm ON T2.TEI_KEY1 = AST_KEY ";
			$SQL .= " WHERE (T1.TEI_ITY_ID1='LAR' AND T1.TEI_ITY_ID2 = 'LAS') ";
			$SQL .= " AND T1.TEI_SUCH2 " . awisLIKEoderIST($rsLiefArt['LAR_LARTNR'][$LiefArt],false,false,true,false);

			$rsLAS = awisOpenRecordset($con, $SQL);

			print "<tr><td></td><td colspan=7>";
			print "<table border=1 width=500><tr>";
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 class=FeldBez>LiefArt-Nr</td>';
				echo '<td width=100 class=FeldBez>Bezeichnung</td>';
				echo '<td width=100 class=FeldBez>ATU-Nr</td>';
				echo '</tr>';
			}
			for($LASZeile=0;$LASZeile<$awisRSZeilen;$LASZeile++)
			{
				echo '<tr>';
				echo '<td>' . $rsLAS['LAR_LARTNR'][$LASZeile] . '</td>';
				echo '<td>' . $rsLAS['LAR_BEZWW'][$LASZeile] . '</td>';
				echo '<td><a href=./artikel_Main.php?Key=' . $rsLAS['AST_KEY'][$LASZeile] . '&cmdAktion=ArtikelInfos>' . $rsLAS['AST_ATUNR'][$LASZeile] . '</a></td>';
				echo '</tr>';
			}
			print "</tr></table>";
			print "</td></tr>";
		}
	}

	print "</table>";

	unset($rsLiefArt);

?>