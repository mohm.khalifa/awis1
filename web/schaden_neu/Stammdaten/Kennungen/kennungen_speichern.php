<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	if (isset($_GET['button']))
	{
		if(isset($_GET['VERALTET']))
		{
			$Aktiv = '';
			if($_GET['VERALTET'] == '1')
			{
				$Aktiv = 0;
			}
			else
			{
				$Aktiv = 1;
			}
			$SQL = "UPDATE KENNUNGEN ";
			$SQL .="SET VERALTET =".$Aktiv;
			$SQL .= ',  KENN_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ',  KENN_USERDAT=SYSDATE';
			$SQL .=" WHERE KENN_KEY = " .$_GET['button'];
			$DBSCHAD->Ausfuehren($SQL);
	
			$DBSCHAD->Ausfuehren($SQL);
		}
		elseif(isset($_GET['FILIALEN']))
		{
			$Sichtbar = '';
			if($_GET['FILIALEN'] == '1')
			{
				$Sichtbar = 0;
			}
			else
			{
				$Sichtbar = 1;
			}
			$SQL = "UPDATE KENNUNGEN ";
			$SQL .="SET SICHTBAR_FILIALEN =".$Sichtbar;
			$SQL .= ',  KENN_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ',  KENN_USERDAT=SYSDATE';
			$SQL .=" WHERE KENN_KEY = " .$_GET['button'];
			$DBSCHAD->Ausfuehren($SQL);
			 
			$DBSCHAD->Ausfuehren($SQL);
		}
		elseif(isset($_GET['AUSWERTUNG']))
		{
			$Auswertung = '';
			if($_GET['AUSWERTUNG'] == '1')
			{
				$Auswertung = 0;
			}
			else
			{
				$Auswertung = 1;
			}
			$SQL = "UPDATE KENNUNGEN ";
			$SQL .="SET AKTIV_FUER_AUSWERTUNG =".$Auswertung;
			$SQL .= ',  KENN_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ',  KENN_USERDAT=SYSDATE';
			$SQL .=" WHERE KENN_KEY = " .$_GET['button'];
			$DBSCHAD->Ausfuehren($SQL);

			$DBSCHAD->Ausfuehren($SQL);
		}
	}	
	
	if ($AWIS_KEY1 == '-1') //Neuanlage
	{
		$SQL ='insert into KENNUNGEN ';
	    $SQL .=' (KENNUNG,KENNUNG_BEZEICHNUNG,VERALTET,SICHTBAR_FILIALEN,KENN_USER, AKTIV_FUER_AUSWERTUNG,KENN_USERDAT)';
	    $SQL .=' values';
	    $SQL .='('.$DBSCHAD->WertSetzen('SDK','T',mb_strtoupper($_POST['txtBES_KENNUNG']));
	    $SQL .=','.$DBSCHAD->WertSetzen('SDK','T',$_POST['txtBES_KENNUNGSBEZEICHNUNG']);
	  
	    if(isset($_POST['txtCHK_AKTIV']))
	    {
	        $SQL .= ',0';
	    }
	    else
	    {
	        $SQL .= ',1';
	    }
	    if(isset($_POST['txtCHK_FILIALEN']))
	    {
	    	$SQL .= ',1';
	    }
	    else
	    {
	    	$SQL .= ',0';
	    }
	    $SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
		if(isset($_POST['txtCHK_AUSWERTUNG']))
		{
			$SQL .= ',1';
		}
		else
		{
			$SQL .= ',0';
		}
	    $SQL .=',sysdate)';
	    	
	    $DBSCHAD->Ausfuehren($SQL,'',true,$DBSCHAD->Bindevariablen('SDK'));
	    $AWIS_KEY1 = '';
	    $SpeichernOK = true;
	    $Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
	}
	elseif($AWIS_KEY1 > '0')
	{		
			$SQL ='update KENNUNGEN ';
			//$SQL .= 'set KENNUNG='.$DB->FeldInhaltFormat('T',$_POST['txtBES_KENNUNG']);
			$SQL .= ' set KENNUNG_BEZEICHNUNG='.$DB->FeldInhaltFormat('T',$_POST['txtBES_KENNUNGSBEZEICHNUNG']);
				
			if(isset($_POST['txtCHK_AKTIV']))
			{
				$SQL .= ', VERALTET=0';
			}
			else
			{
				$SQL .= ', VERALTET=1';
			}
			if(isset($_POST['txtCHK_FILIALEN']))
			{
				$SQL .= ', SICHTBAR_FILIALEN=1';
			}
			else
			{
				$SQL .= ', SICHTBAR_FILIALEN=0';
			}
			if(isset($_POST['txtCHK_AUSWERTUNG']))
			{
				$SQL .= ', AKTIV_FUER_AUSWERTUNG=1';
			}
			else
			{
				$SQL .= ', AKTIV_FUER_AUSWERTUNG=0';
			}

			$SQL .= ', KENN_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', KENN_USERDAT=SYSDATE';
			$SQL .=' where KENN_KEY='.$AWIS_KEY1;
			$DBSCHAD->Ausfuehren($SQL,'',true);		
			$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			$SpeichernOK = true;
	}
}
catch (awisException $ex)
{
    
    if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.UNIQUE_KENNUNG) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
        $Fehler = 1;
        $Hinweis = 'Die Kennung befindet sich schon in der Datenbank.';
       
        $Form->Hinweistext($Hinweis);
    }
    else
    {
        $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
        $Form->DebugAusgabe(1,$ex->getSQL());
    }


	
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
