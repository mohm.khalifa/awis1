<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php

    global $AWISCursorPosition;
    global $CAG;

    require_once 'carglass_funktionen.php';

    try {
    $CAG = new carglass_funktionen();


    echo "<link rel=stylesheet type=text/css href='" . $CAG->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $CAG->AWISSprachKonserven['TITEL']['tit_CAG'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$CAG->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(75000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$CAG->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($CAG->Form instanceof awisFormular) {
        $CAG->Form->DebugAusgabe(1, $ex->getMessage());
        $CAG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "202009161520");
    } else {
        $CAG->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($CAG->Form instanceof awisFormular) {
        $CAG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "202009161521");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>