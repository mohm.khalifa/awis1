<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;


try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    if($BildschirmBreite<1024) {
    }
    elseif($BildschirmBreite<1280) {
    }
    else {
    }

    $TextKonserven = array();
    $TextKonserven[]=array('WEB','*');
    $TextKonserven[]=array('WBE','*');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','lbl_drucken');
    $TextKonserven[]=array('Wort','lbl_Hilfe');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');

    $Form = new awisFormular();	

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht14000 = $AWISBenutzer->HatDasRecht(14000);

    if($Recht14000 == 0)
    {
	$Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $flagNeuanlage = false; 
    
    /*
     * SK, 08.05.2012, wurde durch direkte Links ersetzt
    if(isset($_GET['ExportWEB_KEY']))
    {
	include('verteilqualitaet_Export.php');
	
	$AWIS_KEY1 = $_GET['ExportWEB_KEY'];	
    }
    if(isset($_GET['ExportAdressenWEB_KEY']))
    {
       include('verteilqualitaet_AdressenExport.php');	

       $AWIS_KEY1 = $_GET['ExportAdressenWEB_KEY'];	
    }
    */
    //isset($_GET['ExportAdressenWEB_KEY']))
    if(isset($_GET['EMailWEB_KEY']))
    {
      include('verteilqualitaet_EMail.php');	

      $AWIS_KEY1 = $_GET['EMailWEB_KEY'];
    }
    if(isset($_POST['cmdEMAILSendenOK'])) { 
	include('verteilqualitaet_EMail.php');	

        //$AWIS_KEY1 = $_POST['txtEMailWEB_KEY'];
    }
    if(isset($_POST['cmdEMAILAbbrechen']))
    {
      $AWIS_KEY1 = $_POST['txtEMailWEB_KEY'];
    }
    if(isset($_POST['cmdSuche_x'])) {

        $Param['WEB_WERBEMEDIUM']=$_POST['sucWERBEMEDIUM'];
        $Param['WEB_DATVON']=$_POST['sucDATVON'];
        $Param['WEB_DATBIS']=$_POST['sucDATBIS'];
        
	if(isset($_POST['txtNurFehlerhafte']))
        {
            $Param['NurFehlerhafte']=$_POST['txtNurFehlerhafte'];
        }

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Param['KEY']=0;
	
        $AWISBenutzer->ParameterSchreiben("WEBSuche",serialize($Param));
    }
    if(isset($_POST['cmdDSZurueck_x'])) // vorheriger DS
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('WEBSuche'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND WEB_KEY < '.floatval($_POST['txtWEB_KEY']);

        $SQL = 'SELECT * FROM (SELECT WEB_KEY ';
	if($Param['WEB_DATVON'] != "" && $Param['WEB_DATBIS'] != "") 
	{  
		$SQL .= ' From Werbemedium aa inner join Werbemittelzuordnung bb on aa.WEB_KEY = WBE_WEB_KEY ';
	}
	else
	{
		$SQL .= ' From Werbemedium aa '; 
	}
        $SQL .= 'WHERE '.substr($Bedingung,4).' ';
        $SQL .= 'ORDER BY WEB_KEY DESC) WHERE ROWNUM = 1';

        $rsVSA = $DB->RecordSetOeffnen($SQL);
        if($rsVSA->FeldInhalt('WEB_KEY') != '') {
            $AWIS_KEY1 = $rsVSA->FeldInhalt('WEB_KEY');
        }
        else {
            $AWIS_KEY1 = $_POST['txtWEB_KEY'];
        }
    }
    elseif(isset($_POST['cmdDSWeiter_x'])) // nächster DS
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('WEBSuche'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND WEB_KEY > '.floatval($_POST['txtWEB_KEY']);

        $SQL = 'SELECT * FROM (SELECT WEB_KEY ';
	if($Param['WEB_DATVON'] != "" && $Param['WEB_DATBIS'] != "") 
	{  
		$SQL .= ' From Werbemedium aa inner join Werbemittelzuordnung bb on aa.WEB_KEY = WBE_WEB_KEY ';
	}
	else
	{
		$SQL .= ' From Werbemedium aa '; 
	}
        $SQL .= 'WHERE '.substr($Bedingung,4).' ';
        $SQL .= 'ORDER BY WEB_KEY) WHERE ROWNUM = 1';

	$rsVSA = $DB->RecordSetOeffnen($SQL);
        if($rsVSA->FeldInhalt('WEB_KEY') != '')
        {
	    $AWIS_KEY1 = $rsVSA->FeldInhalt('WEB_KEY');
	    //echo "AWIS_KEY".$AWIS_KEY1;
        }
        else
        {
	    $AWIS_KEY1 = $_POST['txtWEB_KEY'];
        }
        //echo "HALLO-AWIS_KEY1 = ".$AWIS_KEY1;
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
	include('verteilqualitaet_speichern.php');
    }
     elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
            include './verteilqualitaet_loeschen.php';
        }
        elseif(isset($_POST['cmdLoeschenAbbrechen'])) {

            //$Form->DebugAusgabe(1,$_REQUEST);
            $AWIS_KEY1 = $_REQUEST['txtWEB_KEY'];
    } 
    elseif(isset($_GET['WEB_KEY'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['WEB_KEY']);
        $AWISBenutzer->ParameterSchreiben("AktuellerWEB",$_GET['WEB_KEY']);
    }
    elseif(isset($_GET['WBE_WEB_KEY']))
    {
	$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['WBE_WEB_KEY']);
        $AWISBenutzer->ParameterSchreiben("AktuellerWEB",$_GET['WBE_WEB_KEY']);
    }
    elseif(isset($_POST['cmdDSNeu_x']))
    {
	$flagNeuanlage = true;
    }
    else
    {
        if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block'])) 
        {
            //$AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerWEB");
        }
    }	
    $Param = unserialize($AWISBenutzer->ParameterLesen("WEBSuche"));

if($flagNeuanlage == true)
{
   $Form->Formular_Start();
   
   $Form->SchreibeHTMLCode('<form name=frmVerteilqualitaetDetails action=./verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
	
   $Form->Erstelle_HiddenFeld('WEB_KEY', $AWIS_KEY1);
	
   $Form->ZeileStart();
   $Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WEB_WERBEMEDIUM'].':',150);
   $Form->Erstelle_TextFeld('WEB_WERBEMEDIUM','',40,100,true);
   $Form->ZeileEnde();

   $Form->Trennzeile('O');

   if ($AWIS_KEY1 ==0) {
      $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
      $SubReg = new awisRegister(14001);
      $SubReg->ZeichneRegister($RegisterSeite);
    }
    
    $Form->Formular_Ende();
}
else
{
    $Bedingung = _BedingungErstellen($Param);
    
    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if(!isset($_GET['Sort'])) {
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='') {
            $ORDERBY = $Param['ORDERBY'];
        }
        else {
            $ORDERBY = ' WEB_KEY';
        }
    }
    else {
        $ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
    }

    $SQL ='Select aa.WEB_KEY,Aa.Web_Werbemedium ';
    
    if($AWIS_KEY1 <= 0) {
        $SQL .= ',row_number() over (order by '.$ORDERBY.') AS ZeilenNr ';
    }
    if($Param['WEB_DATVON'] != "" && $Param['WEB_DATBIS'] != "") 
    {  
     $SQL .= ' From Werbemedium aa inner join Werbemittelzuordnung bb on aa.WEB_KEY = WBE_WEB_KEY ';
    }
    else
    {
       $SQL .= ' From Werbemedium aa '; 
    }
    if($Bedingung!='') {
        $SQL .= ' WHERE'.substr($Bedingung,4);
    }
    $SQL .= ' GROUP BY aa.WEB_KEY, aa.WEB_WERBEMEDIUM ORDER BY '.$ORDERBY ;
   
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if(isset($_REQUEST['Block'])) {
        $Block=$Form->Format('N0',$_REQUEST['Block'],false);
    }
    elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='') {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    //echo $SQL;

    //*****************************************************************
    // Nicht einschränken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if($AWIS_KEY1<=0) {
        $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
    }

    //echo $SQL;
    $rsVSA = $DB->RecordSetOeffnen($SQL);

    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $Param['ORDERBY']=$ORDERBY;
    $Param['KEY']=$AWIS_KEY1;
    $Param['BLOCK']=$Block;
    $AWISBenutzer->ParameterSchreiben("VVSSuche",serialize($Param));

    $Form->SchreibeHTMLCode('<form name=frmVerteilqualitaetDetails action=./verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

    //********************************************************
    // Daten anzeigen
    //********************************************************
    
    if(isset($_GET['WBE_WEB_KEY']))
    {
	$Form->Formular_Start();

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./verteilqualitaet_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVSA->FeldInhalt('VVS_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVSA->FeldInhalt('VVS_USERDAT'));
        $Form->InfoZeile($Felder,'');

        $AWIS_KEY1 = $rsVSA->FeldInhalt('WEB_KEY');
        $AWISBenutzer->ParameterSchreiben("AktuellerWEB",$AWIS_KEY1);

        $EditRecht = $Recht14000&2;
        //$EditRecht = true;
	
        $Form->Erstelle_HiddenFeld('WEB_KEY', $AWIS_KEY1);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WEB_KEY'].':',150);
        $Form->Erstelle_TextFeld('WEB_KEY',$rsVSA->FeldInhalt('WEB_KEY'),20,50,false);
        $Form->ZeileEnde();

        $Form->Trennzeile('L');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WEB_WERBEMEDIUM'].':',150);
        $Form->Erstelle_TextFeld('WEB_WERBEMEDIUM',$rsVSA->FeldInhalt('WEB_WERBEMEDIUM'),40,100,true);
	$Form->ZeileEnde();

        $Form->Trennzeile('O');

        //Erstelle Unterregister
        if ($AWIS_KEY1 !=0) {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
            $SubReg = new awisRegister(14001);
            $SubReg->ZeichneRegister($RegisterSeite);
        }	
   }
    else
    {
   
    if($rsVSA->EOF() )		// Keine Meldung bei neuen Datensätzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
    }
    elseif(($rsVSA->AnzahlDatensaetze()>1) or (isset($_GET['Liste'])))	// Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        $Link = './verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=WEB_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WEB_KEY'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_KEY'],130,'',$Link);
        $Link = './verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=WEB_WERBEMEDIUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WEB_WERBEMEDIUM'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_WERBEMEDIUM'],200,'',$Link);
        $Link = './verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=WBE_DATVON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WEB_STATUS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_STATUS'],180,'',$Link);
        
	
	//$Link = './verteilqualitaet_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        //$Link .= '&Sort=WBE_DATBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WBE_DATBIS'))?'~':'');
        //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATBIS'],180,'',$Link);
        
	$Form->ZeileEnde();

        $WEBZeile=0;

	$Datum = date('d.m.Y');

        while(!$rsVSA->EOF()) {
            $Form->ZeileStart();
            $Link = './verteilqualitaet_Main.php?cmdAktion=Details&WEB_KEY='.$rsVSA->FeldInhalt('WEB_KEY').'';
            $Form->Erstelle_ListenFeld('WEB_KEY',$rsVSA->FeldInhalt('WEB_KEY'),0,130,false,($WEBZeile%2),'',$Link,'T');
            $Form->Erstelle_ListenFeld('WEB_WERBEMEDIUM',$rsVSA->FeldInhalt('WEB_WERBEMEDIUM'),0,200,false,($WEBZeile%2),'','','T');
	    	
	    $SQL = 'Select WBE_WEB_KEY FROM WERBEMITTELZUORDNUNG WHERE WBE_DATBIS >='.$DB->FeldInhaltFormat('DU',$Datum);
	    $SQL .= ' AND WBE_WEB_KEY='.$rsVSA->FeldInhalt('WEB_KEY');

	    $rsStatus = $DB->RecordSetOeffnen($SQL);
            if($rsStatus->AnzahlDatensaetze() > 0)
	    {
		$Form->Erstelle_ListenFeld('WEB_STATUS','OFFEN',0,180,false,($WEBZeile%2),'color:red','','T');
	    }
	    else
	    {
	       $Form->Erstelle_ListenFeld('WEB_STATUS','Erledigt',0,180,false,($WEBZeile%2),'color:green','','T');
	    }
	    

	   
	    //$Form->Erstelle_ListenFeld('WBE_DATVON',$rsWEB->FeldInhalt('WBE_DATVON'),0,180,false,($WEBZeile%2),'','','DU');
            //$Form->Erstelle_ListenFeld('WBE_DATBIS',$rsWEB->FeldInhalt('WBE_DATBIS'),0,180,false,($WEBZeile%2),'','','DU');
            //Status einblenden
	    $Form->ZeileEnde();

            $rsVSA->DSWeiter();
            $WEBZeile++;
        }

        $Link = './verteilqualitaet_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

        $Form->Formular_Ende();
    }
    elseif(isset($_GET['WEB_KEY']) OR $AWIS_KEY1 != 0 OR $rsVSA->AnzahlDatensaetze() == 1) {
    //Nur ein Datensatz gefunden
	
        $Form->Formular_Start();

	// Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./verteilqualitaet_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVSA->FeldInhalt('VVS_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVSA->FeldInhalt('VVS_USERDAT'));
        $Form->InfoZeile($Felder,'');

        $AWIS_KEY1 = $rsVSA->FeldInhalt('WEB_KEY');
        $AWISBenutzer->ParameterSchreiben("AktuellerWEB",$AWIS_KEY1);

        $EditRecht = $Recht14000&2;
        //$EditRecht = true;
	
        $Form->Erstelle_HiddenFeld('WEB_KEY', $AWIS_KEY1);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WEB_KEY'].':',150);
        $Form->Erstelle_TextFeld('WEB_KEY',$rsVSA->FeldInhalt('WEB_KEY'),20,50,false);
        $Form->ZeileEnde();

        $Form->Trennzeile('L');

	$Export = array();
	$EMAIL = array();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WEB_WERBEMEDIUM'].':',150);
	if(($Recht14000&2)==2)
        {
	   $Export[0] = array('paket','./verteilqualitaet_AdressenExport.php?ExportAdressenWEB_KEY='.$rsVSA->FeldInhalt('WEB_KEY'));
	}
	$Export[1] = array('report','./verteilqualitaet_Export.php?ExportWEB_KEY='.$rsVSA->FeldInhalt('WEB_KEY'));
	$Form->Erstelle_ListeIcons($Export,40,-1);
	$Form->Erstelle_TextFeld('WEB_WERBEMEDIUM',$rsVSA->FeldInhalt('WEB_WERBEMEDIUM'),360,360,false);
	 if(($Recht14000&2)==2)
        {
	   $Form->Erstelle_TextFeld('','EMAIL AN FILIALEN SENDEN:',220,300,false);
	   $EMAIL[0] = array('warnung','./verteilqualitaet_Main.php?cmdAktion=Details&EMailWEB_KEY='.$rsVSA->FeldInhalt('WEB_KEY'));
	}
	$Form->Erstelle_ListeIcons($EMAIL,40,-1);
	
	
	$Form->ZeileEnde();

        $Form->Trennzeile('O');

        //Erstelle Unterregister
        if ($AWIS_KEY1 !=0) {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
            $SubReg = new awisRegister(14001);
            $SubReg->ZeichneRegister($RegisterSeite);
        }
    }
    }
}
    //***************************************
    // Schaltflächen für dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    
  if($flagNeuanlage == false)
  {  
    if(($rsVSA->AnzahlDatensaetze()==1) )
    {
        if(($Recht14000&2)==2)
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
    }
   }
   else
   {
      $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S'); 
   }
  

  if($flagNeuanlage == false)
  {
    if(($rsVSA->AnzahlDatensaetze()==1) && $flagNeuanlage == false)
    {
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
        $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
    }
  }
  else
  {
    	
  }
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {

        //$Form->Fehler _Anzeigen('INTERN',$ex->getMessage('MELDEN'),6,"200905131735");
    }
    else
    {
	echo 'allg. Fehler:'.$ex->getMessage();
    }
}

function _BedingungErstellen($ArrParam)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if(floatval($AWIS_KEY1) != 0) {
            $Bedingung.= ' AND WEB_KEY = '.$AWIS_KEY1;
            return $Bedingung;
    }

    //var_dump($ArrParam);	

    if(isset($ArrParam['WEB_WERBEMEDIUM']) AND $ArrParam['WEB_WERBEMEDIUM'] != '')
    {
        $Bedingung = " AND web_werbemedium = ".$DB->FeldInhaltFormat('T',$ArrParam['WEB_WERBEMEDIUM'],true);
    }
    if(isset($ArrParam['WEB_DATVON']) AND $ArrParam['WEB_DATVON'] != '')
    {
        $Bedingung .= " AND WBE_DATVON >= ".$DB->FeldInhaltFormat('DU',$ArrParam['WEB_DATVON'],true);
    }
    if(isset($ArrParam['WEB_DATBIS']) AND $ArrParam['WEB_DATBIS'] != '')
    {
        $Bedingung .= " AND WBE_DATBIS <= ".$DB->FeldInhaltFormat('DU',$ArrParam['WEB_DATBIS'],true)." ";
    }
    /* 
    if(isset($ArrParam['VVS_VORGANGNR']) AND $ArrParam['VVS_VORGANGNR'] != '')
    {
        $Bedingung .= " AND vvs_vorgangnr =  ".$DB->FeldInhaltFormat('T',$ArrParam['VVS_VORGANGNR'],true)." ";
    }
    if(isset($ArrParam['VVS_SCHADENKZ']) AND $ArrParam['VVS_SCHADENKZ'] != '')
    {
        $Bedingung .= " AND vvs_schadenkz =  ".$DB->FeldInhaltFormat('T',$ArrParam['VVS_SCHADENKZ'],true)." ";
    }
    if(isset($ArrParam['NurFehlerhafte']) AND $ArrParam['NurFehlerhafte'] != '')
    {
        $Bedingung .= " AND vvs_ven_key <> 0 AND vvs_vcn_key <> 112 AND vvs_freigabe = 0";
    }
    */

    //echo $Bedingung;

    return $Bedingung;
}
?>