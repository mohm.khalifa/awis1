<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $Funktionen;
require_once('filialkompetenzen_funktionen.php');

try
{
    $Funktionen = new filialkompetenzen_funktionen();
    $Funktionen->AWISBenutzer = awisBenutzer::Init();
    $Version = 3;
    echo "<link rel=stylesheet type=text/css href=" . $Funktionen->AWISBenutzer->CSSDatei($Version) .">";
}
catch (Exception $ex)
{
    die($ex->getMessage());
}

echo '<title>'.$Funktionen->AWISSprachKonserven['TITEL']['tit_FILIALKOMPETENZEN'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader$Version.inc");	// Kopfzeile
try
{
    $Form = new awisFormular();
    if($Funktionen->AWISBenutzer->HatDasRecht(660)==0)
    {
        $Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"201208161436");
        die();
    }
    $Register = new awisRegister(660);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:'Suche'));

    $Form->SetzeCursor($Funktionen->AWISCursorPosition);
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201208161435");
    }
    else
    {
        echo 'AWIS: '.$ex->getMessage();
    }
}
?>
</body>
</html>