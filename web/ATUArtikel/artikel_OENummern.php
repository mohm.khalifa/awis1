<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $AWISBenutzer;

	$HERRechte = awisBenutzerRecht($con, 602);		// F�r den Link zu den Herstellern
	$OENRechte = awisBenutzerRecht($con,403);	
	if($OENRechte==0)
	{
		print "<hr><span class=HinweisText>Keine ausreichenden Rechte.</span>";
		awisLogoff($con);
		die();
	}
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	
	$SQL = "SELECT * FROM AWIS.OENummern, AWIS.TeileInfos, AWIS.Hersteller WHERE HER_ID = OEN_HER_ID AND OEN_KEY=TEI_KEY2 AND TEI_KEY1=0" . $Artikel[0];

	$rsOEN = awisOpenRecordset($con, $SQL);
	if($rsOEN==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsOENZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr>";
	print "<td width=30 id=FeldBez>&nbsp;</td>";
	print "<td id=FeldBez>OE-Nummer</td>";
	print "<td id=FeldBez>Hersteller</td>";
	print "<td id=FeldBez>Muster</td>";	
	print "<td id=FeldBez>Bemerkung</td>";	
	print "<td id=FeldBez>Ersatz-OE</td>";	
	print "<td id=FeldBez>Preis</td>";
	print "<td id=FeldBez>Preis vom</td>";
	print "<td id=FeldBez>Preis Quelle</td>";
	print "</tr>";
	
	
	if(($OENRechte & 2)==2)
	{
		print "<tr>";
		
		print "<td><input type=hidden name=txtOEN_KEY_0></td>";
		
		print "<td><input name=txtOEN_NUMMER size=20></td>";
		print "<td><select name=txtOEN_HER_ID>\n\r";

		$rsHER = awisOpenRecordset($con, "SELECT HER_ID, HER_BEZEICHNUNG FROM AWIS.Hersteller WHERE HER_TYP=1 ORDER BY HER_BEZEICHNUNG");
		for($HerZeile=0;$HerZeile<$awisRSZeilen;$HerZeile++)
		{
			print "\n\r<option ";
			if($HerZeile==0)
			{
				print " selected ";
			}
			print " value=" . $rsHER["HER_ID"][$HerZeile] . ">"  . $rsHER["HER_BEZEICHNUNG"][$HerZeile] . "</option>";
		}
		
		print "</select></td>";

		print "</tr>";
	}

	for($OEZeile=0;$OEZeile<$rsOENZeilen;$OEZeile++)
	{
		print "<tr>";

		if(($OENRechte & 2)==2)
		{
			print "<td><input name=cmdOENLoeschen_" . $rsOEN["OEN_KEY"][$OEZeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
		}
		else
		{
			print "<td></td>";
		}
		
		print "<td><a href=./artikel_Main.php?cmdAktion=OENummern&Seite=Preise&OENKEY=" . $rsOEN["OEN_KEY"][$OEZeile] . ">";
		print $rsOEN["OEN_NUMMER"][$OEZeile] . "</a></td>";

		if($HERRechte>0)		
		{
			echo "<td><a href=/stammdaten/hersteller.php?HERID=" . $rsOEN["HER_ID"][$OEZeile] . '&Zurueck=' . str_replace('&','~~9~~',$_SERVER['REQUEST_URI']) . ">";
			echo $rsOEN["HER_BEZEICHNUNG"][$OEZeile] . "</a></td>";
		}
		else
		{
			echo '<td>' . $rsOEN["HER_BEZEICHNUNG"][$OEZeile] . "</td>";
		}

		print "<td>" . ($rsOEN["OEN_MUSTER"][$OEZeile]==0?"Nein":"Ja") . "</td>";
		print "<td>" . $rsOEN["OEN_BEMERKUNG"][$OEZeile] . "</td>";

		$SQL = "SELECT * FROM AWIS.OENummernPreise, OENUMMERNPREISIMPORTQUELLEN WHERE OEP_OIQ_NR = OIQ_NR AND OEP_SUCHNUMMER " . awisLIKEoderIST($rsOEN["OEN_NUMMER"][$OEZeile],false,false,false,2) . " AND OEP_HER_ID=" . $rsOEN["HER_ID"][$OEZeile] . " ORDER BY TO_DATE(OIQ_STAND,'dd.mm.yy') DESC";
		$rsOEP = awisOpenRecordset($con, $SQL);

		if($awisRSZeilen>0)
		{
			echo "<td>";
			
				// Nach irgendwann-einmal ErsatzOENr suchen und einen Hinweis ausgeben!
			if($rsOEP["OEP_ERSATZNR"][0]=='')
			{
				for($i=1;$i<$awisRSZeilen;$i++)
				{
					if($rsOEP["OEP_ERSATZNR"][$i])
					{
						echo '*';
						break;
					}
				}
			}
			else
			{
				echo '<a href=./artikel_Main.php?cmdAktion=OENummern&Seite=Preise&OEN_NUMMER=' . $rsOEP['OEP_ERSATZNR'][0] . '&HER_ID=' . $rsOEN["HER_ID"][$OEZeile] . '>' . $rsOEP["OEP_ERSATZNR"][0] . '</a>';
			}
			echo "</td>";
			echo "<td>" . awis_format($rsOEP["OEP_PREIS"][0],"Currency") . "</td>";
			echo "<td>" . $rsOEP['OIQ_STAND'][0] . "</td>";
			echo "<td>" . $rsOEP['OIQ_QUELLE'][0] . "</td>";
		}
		else
		{
			print "<td>n.v.</td><td>n.v.</td><td>n.v.</td><td>n.v.</td>";
		}
		unset($rsOEP);
		
		print "</tr>";	
	}	// Alle Zeilen ausgeben
		
	print "</table>";
	
	unset($rsLiefArt);
?>