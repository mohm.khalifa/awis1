<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonservenLoeschen = $Form->LadeTexte($TextKonserven);
    
	$Tabelle= '';
	
	$Key=$_POST['txtZMM_KEY'];
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{

	    $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./zahlungsmittelmatrix_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
	    
	    $Form->Erstelle_HiddenFeld('ZMM_KEY', $Key); //Damit er beim nächsten mal Posten wieder mitkommt.
	    
	
		$SQL = 'SELECT *';
		$SQL .= ' FROM ZAHLUNGSMITTELMATRIX ';
		$SQL .= ' WHERE ZMM_KEY=0'.$Key;

		$rsDaten = $DB->RecordsetOeffnen($SQL);

		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('ZMM','ZMM_ZAMINR'),$rsDaten->FeldInhalt('ZMM_ZAMINR'));
		$Felder[]=array($Form->LadeTextBaustein('ZMM','ZMM_ZAMIBEZEICHNUNG'),$rsDaten->FeldInhalt('ZMM_ZAMIBEZEICHNUNG'));
		$Felder[]=array($Form->LadeTextBaustein('ZMM','ZMM_WERTIGKEIT'),$rsDaten->FeldInhalt('ZMM_WERTIGKEIT'));
		$Felder[]=array($Form->LadeTextBaustein('ZMM','ZMM_GUELTIG_VON'),$rsDaten->FeldInhalt('ZMM_GUELTIG_VON'));
		$Felder[]=array($Form->LadeTextBaustein('ZMM','ZMM_GUELTIG_BIS'),$rsDaten->FeldInhalt('ZMM_GUELTIG_BIS'));
		
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonservenLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();
		
		foreach($Felder AS $Feld)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($Feld[0].':',200);
		    $Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		    $Form->ZeileEnde();
		}
		
		$Form->Trennzeile();
		
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$AWISSprachKonservenLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$AWISSprachKonservenLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
		
		$Form->SchreibeHTMLCode('</form>');
		
		$Form->Formular_Ende();
		
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = 'DELETE FROM zahlungsmittelmatrix where zmm_key = ' .$Key ;		

		//var_dump ($SQL);
		if($DB->Ausfuehren($SQL)===false)
		{
			throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
		}
		
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>