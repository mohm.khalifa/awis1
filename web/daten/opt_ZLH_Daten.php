<?php
//**************************************************************
// Liefert die Hersteller f�r einen Lieferantenartikel
// 
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['LIE_NR']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT COALESCE(ZHK_CODE,ZLH_TECDOCID) AS KEY, COALESCE(ZHK_CODE,ZLH_KUERZEL) || \' - \' || ZLH_BEZEICHNUNG AS ANZEIGE ';
$SQL .= ' FROM ZUKAUFLIEFERANTENHERSTELLER';
$SQL .= ' LEFT OUTER JOIN ZUKAUFHERSTELLERCODES ON ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_GET['LIE_NR'],false);
$SQL .= ' WHERE ZHK_KEY IS NOT NULL OR (ZHK_KEY IS NULL AND ZLH_TECDOCID IS NOT NULL)';
$SQL .= ' ORDER BY ZLH_KUERZEL, ZLH_BEZEICHNUNG';

$rsPEB = $DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsPEB->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE')===false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsPEB->FeldInhalt('KEY').'">'.$rsPEB->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsPEB->FeldInhalt('KEY').'#+#'.$rsPEB->FeldInhalt('ANZEIGE').'';
	}

	$rsPEB->DSWeiter();
}
?>