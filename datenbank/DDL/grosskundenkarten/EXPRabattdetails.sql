/***************************************************************************
* Tabellendefinitionen : EXPRabattdetails
* Erstellt am : 14.12.2016 16:08:00
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE TABLE EXPRabattdetails(
ERA_KEY NUMBER(10,0) CONSTRAINT CNN_ERA_KEY NOT NULL,
ERA_BEZEICHNUNG VARCHAR2(50),
ERA_WG VARCHAR2(50),
CONSTRAINT PK_ERA_KEY PRIMARY KEY(ERA_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : EXPRabattdetails
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

COMMENT ON COLUMN EXPRabattdetails.ERA_KEY IS 'KEY';
COMMENT ON COLUMN EXPRabattdetails.ERA_Bezeichnung IS 'Bezeichnung';
COMMENT ON COLUMN EXPRabattdetails.ERA_WG IS 'Warengruppen';


/***************************************************************************
* Indexdefinitionen : EXPRabattdetails
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Sequenz erstellen : EXPRabattdetails
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE SEQUENCE SEQ_ERA_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trg_seq_ERA_KEY
     BEFORE INSERT ON EXPRabattdetails
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.ERA_KEY := seq_ERA_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 14.12.2016 16:08:00
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='ERA';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('ERA', 'EXPRabattdetails', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='ERA' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('ERA','Tabelle', 'EXPRabattdetails', 'EXPRabattdetails')
;


/***************************************************************************
* Ende des Skripts EXPRabattdetails
****************************************************************************
* Fertig am : 14.12.2016 16:08:00
***************************************************************************/

COMMIT;
