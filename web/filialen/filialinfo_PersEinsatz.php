<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<title>Awis - Filialinformationen</title>
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<?
require_once("register.inc.php");
require_once("db.inc.php"); // DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName) . ">";
?>
</head>

<body>
<?
// Variablen
global $awisDBFehler;
global $awisRSZeilen;
global $HTTP_REFERER;

include ("ATU_Header.php"); // Kopfzeile

$con = awisLogon();

$RechteKontakte = awisBenutzerRecht($con, 150);
$RechteFilialInfo = awisBenutzerRecht($con, 100);
$RechteStufeMitarbeiter = awisBenutzerRecht($con, 1301);
$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$AWISBenutzer->BenutzerName(),'','','');
   die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$SQL = 'SELECT MitarbeiterEinsaetze.*, Filialen.FIL_BEZ, Mitarbeiter.*, Personal.*, ';
$SQL .= ' MitarbeiterEinsaetzeArten.*, TZVon.XTZ_TAGESZEIT AS ZeitVon, ';
$SQL .= ' TZBis.XTZ_TAGESZEIT AS ZeitBis';
$SQL .= ' FROM MitarbeiterEinsaetze INNER JOIN Mitarbeiter ON MEI_MIT_KEY = MIT_KEY';
$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
$SQL .= ' INNER JOIN TagesZeiten TZVon ON MEI_VonXTZ_ID = TZVon.XTZ_ID';
$SQL .= ' INNER JOIN TagesZeiten TZBis ON MEI_BisXTZ_ID = TZBis.XTZ_ID';
$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';

$Bedingung = '';
$Bedingung .= " AND MIT_PER_NR='" . $_REQUEST['PER_NR'] . "'";
$Bedingung .= " AND MEI_TAG>=TO_DATE('" . date('d.m.Y',mktime(0,0,0,date('m'),date('d')-1,date('Y'))) . "','DD.MM.RRRR')";
$Bedingung .= " AND MEI_TAG<=TO_DATE('" . date('d.m.Y',mktime(0,0,0,date('m'),date('d')+1,date('Y'))) . "','DD.MM.RRRR')";

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

$SQL .= ' ORDER BY MIT_BEZEICHNUNG, MEI_Tag, MEI_VonXTZ_ID';

awis_Debug(1,$SQL);

$rsMEI = awisOpenRecordset($con, $SQL);
$rsMEIZeilen = $awisRSZeilen;
$AnzeigeTag = '';

if ($rsMEIZeilen==0)
{
	echo "<br><span class=HinweisText>Es wurden keine Daten gefunden.</span><p>";
	echo "<img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='".$HTTP_REFERER.(strpos($HTTP_REFERER,'cmdAktion=Filialinfos')===false?"?cmdAktion=Filialinfos&":'').(strpos($HTTP_REFERER,'FILID')===false?"&FILID=".$_REQUEST['FILID']:'') ."'>";
	die();
}
else 
{
	/*********************************************************************
	* Daten anzeigen
	*********************************************************************/
	echo '<table class=DatenTabelle border=1 width=100%>';
	
	echo "<tr><td colspan=4 align=left><span class=DatenFeldGross>Schichtplan f�r " . $rsMEI["MIT_BEZEICHNUNG"][0] . "</span></td>";
	echo "<td align=right><img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='".$HTTP_REFERER.(strpos($HTTP_REFERER,'cmdAktion=Filialinfos')===false?"?cmdAktion=Filialinfos&":'').(strpos($HTTP_REFERER,'FILID')===false?"&FILID=".$_REQUEST['FILID']:'') ."'></td></tr>";
	
	echo '<tr><td Class=FeldBez colspan=9>Gefundene Eintr�ge</td></tr>';
	
	echo '<tr>';
	echo '<td Class=FeldBez>Tag</td>';
	echo '<td Class=FeldBez>von</td>';
	echo '<td Class=FeldBez>bis</td>';
	echo '<td Class=FeldBez>Ort</td>';
	echo '<td Class=FeldBez>Bemerkung</td>';
	echo '</tr>';
	
	for($MeiZeile=0;$MeiZeile<$rsMEIZeilen;$MeiZeile++)
	{
		echo '<tr>';
		
		
	//		echo '<td>';
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';
		
		if($AnzeigeTag != str_replace('-','.',$rsMEI['MEI_TAG'][$MeiZeile]))
		{
			$AnzeigeTag = str_replace('-','.',$rsMEI['MEI_TAG'][$MeiZeile]);
			echo $AnzeigeTag  . '</td>';
		}
		
		
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' align=right>' . $rsMEI['ZEITVON'][$MeiZeile] . '</td>';	
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' align=right>' . $rsMEI['ZEITBIS'][$MeiZeile] . '</td>';	
	
		if($rsMEI['MEI_MEA_KEY'][$MeiZeile]==8)		// Filale oder
		{
			echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .' >' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '&nbsp;';
			
			if($RechteFilialInfo>0)
			{
				echo '<a href=/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsMEI['MEI_FIL_ID'][$MeiZeile] . '>' . $rsMEI['MEI_FIL_ID'][$MeiZeile] . '</a> (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';	
			}
			else
			{
				echo $rsMEI['MEI_FIL_ID'][$MeiZeile] . ' (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';	
			}
			echo '</td>';
		}
		else
		{
			echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '</td>';	
		}
		
	
		echo '<td ' . (($MeiZeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . $rsMEI['MEI_BEMERKUNG'][$MeiZeile] . '</td>';	
		
		echo '</tr>';
	}
	
	
	echo '</table>';
	
	/*********************************************************************
	* Ende Daten anzeigen
	*********************************************************************/
}
echo '</form>';

awisLogoff($con);
?>
