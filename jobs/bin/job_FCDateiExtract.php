<?php
require_once('awisFCDateiExtract.inc');

try {

    $Benutzer = '';
    $DebugLevel = 0;
    $MD5 = true;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--ignore':
                $Ignore = $argv[++$i];
                switch ($Ignore) {
                    case 'md5':
                        $MD5 = false;
                }
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --ignore  <Wert>                   Welche pr�fungen sollen deaktiviert werden: md5, .' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $FCExtract = new awisFCDateiExtract($Benutzer, $DebugLevel);
    $FCExtract->MD5Check = $MD5;
    $FCExtract->DebugLevel($DebugLevel);
    $FCExtract->extrahiereFCDatei();
    $FCExtract->ImportFirmKFZ();
    $FCExtract->ImportFleetDaten();
    $FCExtract->ExportFirmKFZ();

} catch (Exception $ex) {
    echo PHP_EOL . 'FCDateiExtract-Import: Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'FCDateiExtract-Fehler', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>