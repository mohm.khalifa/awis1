<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;


try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	
		
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_QMK'));
	
	
	if($Recht34000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Form->Formular_Start();	
	
	$FeldBreiten = array();
	$FeldBreiten['Schluessel'] = 150;
	$FeldBreiten['Lang'] = 200;
	$FeldBreiten['Aktiv'] = 50;
	$FeldBreiten['Gueltigab'] = 100;
	$FeldBreiten['Gueltigbis'] = 100;
	$Param['BLOCK']=1;

	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'qmk_key';
	}
	if ((isset($_GET['Sort']) and $_GET['Sort'] != '') and !isset($_GET['QMK_KEY']))
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	
	$SQL  = ' Select qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat,qmg_gueltigab,qmg_gueltigbis, ';
    $SQL .='row_number() OVER (';
	// sonst pauschal nach Pr�fdatum
	$SQL .= ' order by ' . $Param['ORDER']; 
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat,to_char(qmkriterien_minmax(\'min\',gueltigkeiten.qmk_key),\'DD.MM.YYYY\') as qmg_gueltigab, ';
	$SQL .= 'case when qmkriterien_minmax(\'min\',gueltigkeiten.qmk_key) is null then null else to_char(qmkriterien_minmax(\'max\',gueltigkeiten.qmk_key),\'DD.MM.YYYY\') end as qmg_gueltigbis ';
	$SQL .= ' from(select * from qmkriterien) gueltigkeiten';
	//$SQL .= 'left join QMKRITERIENGUELTIG ON QMK_KEY = QMG_QMK_KEY and trunc(qmg_gueltigbis) >= trunc(sysdate) ';
	//$SQL .= ' AND TRUNC(qmg_gueltigab) <= TRUNC(sysdate)';
	$Bedingung = '';
	if(isset($_GET['QMK_KEY']))
	{
		$Bedingung .= 'and qmk_key=\''.$_GET['QMK_KEY'].'\'';
	}
	if($Bedingung <> '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	//$SQL .= 'group by qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat';
	$SQL .= ')';
	
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	
	}
	else
	{
		$Block = $Param['BLOCK'];
	}

	
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsQMK = $DB->RecordSetOeffnen($SQL);
	

	if(!isset($_GET['QMK_KEY']))
	{
		if(isset($_POST['cmdSpeichern_x']) and $Fehler == 0)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['SPEICHERN']);
			$Form->ZeileEnde();
			$Form->Trennzeile('O');
		}
		
		$Form->ZeileStart('width:100%;');
		$Icons = array();
		$Icons[] = array('new','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&QMK_KEY=-1','g');
		$Form->Erstelle_ListeIcons($Icons,38,0);
		
		$Link= '';
		// �berschrift der Listenansicht mit Sortierungslink: Filiale
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMK_KEY'], $FeldBreiten['Schluessel'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_LANG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_LANG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMK_LANG'], $FeldBreiten['Lang'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_AKTIV,QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_AKTIV,QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMK_AKTIV'], $FeldBreiten['Aktiv'], '', '');
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMK_GUELTIGAB'], $FeldBreiten['Gueltigab'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMK_GUELTIGBIS'], $FeldBreiten['Gueltigbis'], '', $Link);
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsQMK->EOF())
		{
			
			$Form->ZeileStart('width:100%');
			$Icons = array();
				//$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FFA_KEY='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
				$Icons[] = array('edit','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&QMK_KEY='.$rsQMK->FeldInhalt('QMK_KEY').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'').'&Unterseite=Gueltigkeiten');
				$Icons[] = array('delete','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Del='.$rsQMK->FeldInhalt('QMK_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$TTT = $rsQMK->FeldInhalt('QMK_KEY');
			$Form->Erstelle_ListenFeld('QMK_SCHLUESSEL', $rsQMK->FeldInhalt('QMK_KEY'), 0, $FeldBreiten['Schluessel'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMK_LANG');
			$Form->Erstelle_ListenFeld('QMK_LANG',$rsQMK->FeldInhalt('QMK_LANG'), 0, $FeldBreiten['Lang'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMK_AKTIV');
			$Form->Erstelle_ListenFeld('QMK_AKTIV',$rsQMK->FeldInhalt('QMK_AKTIV') == 1 ? 'Ja' : 'Nein', 0, $FeldBreiten['Aktiv'], false, ($DS%2), '','', 'T', 'C', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMK_GUELTIGAB');
			$Form->Erstelle_ListenFeld('QMK_GUELTIGAB',$Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGAB')), 0, $FeldBreiten['Gueltigab'], false, ($DS%2), '','', 'T', 'C', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMK_GUELTIGBIS');
			$Form->Erstelle_ListenFeld('QMK_GUELTIGBIS',$Form->Format('D',$rsQMK->FeldInhalt('QMG_GUELTIGBIS')), 0, $FeldBreiten['Gueltigbis'], false, ($DS%2), '','', 'T', 'C', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsQMK->DSWeiter();
		}
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else 
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Kriterien accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMK_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMK_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		
		if(($Fehler&1)==1)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_SCHLUESSEL']);
			$Form->ZeileEnde();
		}
		if(($Fehler&2)==2)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_LANG']);
			$Form->ZeileEnde();
		}
		if(($Fehler&4)==4)
		{
			$Form->ZeileStart();
			$Form->Hinweistext('Der einzuf�gende Datensatz befindet sich schon in der Datenbank');
			$Form->ZeileEnde();
		}
		if(($Fehler&8)==8)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KRITGUEAB']);
			$Form->ZeileEnde();
		}
		if(($Fehler&16)==16)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KRITGUEBIS']);
			$Form->ZeileEnde();
		}
		if(($Fehler&32)==32)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KRITGUEBISVER']);
			$Form->ZeileEnde();
		}
		
		if(($Fehler&64)==64)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KRITGUEBISKLEIN']);
			$Form->ZeileEnde();
		}
		
		
		
		if($Fehler > 0)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_NSPEICHERN']);
			$Form->ZeileEnde();
			$Form->Trennzeile('O');
		}
		else if(isset($_POST['cmdSpeichern_x']) and $Fehler == 0)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['SPEICHERN']);
			$Form->ZeileEnde();
			$Form->Trennzeile('O');
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_KEY'] . ':',140);
		$Form->Erstelle_TextFeld('QMK_SCHLUESSEL',!isset($_POST['cmdSpeichern_x'])? $rsQMK->FeldInhalt('QMK_KEY') : $_POST['txtQMK_SCHLUESSEL'], 40,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_LANG'] . ':',140);
		$Form->Erstelle_TextFeld('QMK_LANG',!isset($_POST['cmdSpeichern_x'])?$rsQMK->FeldInhalt('QMK_LANG'): $_POST['txtQMK_LANG'], 40,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_AKTIV'] . ':',140);
		if (($rsQMK->AnzahlDatensaetze() == 0))
		{
			$Form->Erstelle_Checkbox('CHK_AKTIV',!isset($_POST['cmdSpeichern_x']) or isset($_POST['txtCHK_AKTIV']) ? 1 : 0,40,true,1);
		}
		else 
		{
			$Form->Erstelle_Checkbox('CHK_AKTIV',!isset($_POST['cmdSpeichern_x']) ? $rsQMK->FeldInhalt('QMK_AKTIV') : isset($_POST['txtCHK_AKTIV']) ? 1 : 0,40,true,1);
		}
		$Form->ZeileEnde();
		
		if(isset($_GET['QMK_KEY']))
		{
			$Form->Erstelle_HiddenFeld('QMK_KEY',$_GET['QMK_KEY']);
		}
		
		if($_GET['QMK_KEY'] != '-1')
		{
			$RegisterSeite = (isset($_GET['Unterseite'])?$_GET['Unterseite']:(isset($_POST['Unterseite'])?$_POST['Unterseite']:'Gueltigkeiten'));
			$RegStamm = new awisRegister(34020);
			$RegStamm->ZeichneRegister('Gueltigkeiten');
		}
		
		

	
		
		
	}
	//$Form->SchreibeHTMLCode('</form>');
	$Form->Formular_Ende();
	$AWISBenutzer->ParameterSchreiben('Formular_QMK',serialize($Param));
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>