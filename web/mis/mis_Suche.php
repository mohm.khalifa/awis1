<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('MIS','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Datum%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht24000 = $AWISBenutzer->HatDasRecht(24000);
	if($Recht24000==0)
	{
	    awisEreignis(3,1000,'MIS',$AWISBenutzer->BenutzerName(),'','','');
	    $Form->SchreibeHTMLCode("<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>");
		$Form->SchreibeHTMLCode("<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>");
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./mis_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MIS']['txt_KFZKennzeichen'].':',190);
	$Form->Erstelle_TextFeld('*KFZKennzeichen','',20,200,true);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201210081346");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201210081347");
	}
	else
	{
		$Form->SchreibeHTMLCode('allg. Fehler:'.$ex->getMessage());
	}
}
?>