<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $SK;

$Schaltflaechenposition = $SK->AWISBenutzer->ParameterLesen('Schaltflaechen_Position');

$SK->ladebalkenStart();

if($Schaltflaechenposition == 1){
    _Schaltflaechen($SK);
}

$GespeicherteFelder = $SK->Param['GespeicherteFelder'];
if (isset($_POST['txtIstZustand'])) {
    $SK->Param['IstZustand'] = 1;
}
$SQLFelder = array();

//Gespeicherte Felder sind unsortiert im Feld, diese m�ssen nun in eine geordnete Struktur gebracht werden(nach Feld gruppiert).
foreach ($GespeicherteFelder as $Key => $Feld) {

    $Feldtyp = substr($Feld['Feld'], 0, 3);
    $Feldname = substr($Feld['Feld'], 3);
    $Operator = $Feld['Operator'];
    $Wert = $Feld['Wert'];

    $SQLFelder[$Feldname][] = array('Wert' => $Wert, 'Operator' => $Operator);
}

$WHERE = '';
$DebugInfo = '';
foreach ($SQLFelder as $Feldname => $FeldArray) {
    $WHERE .= ' AND (';
    $DebugInfo .= ' AND (';
    $SubWhere = '';
    foreach ($FeldArray as $EinzelFeld) {
        if (strpos($EinzelFeld['Operator'], 'like') !== false) {
            $EinzelFeld['Wert'] = str_replace('*', '%', mb_strtoupper($EinzelFeld['Wert']));
        }
        if($EinzelFeld['Operator']=='<>' or $EinzelFeld['Operator']=='not like'){
            $Verknuepfung = ' AND ';
        }else{
            $Verknuepfung = ' OR ';
        }
        $SubWhere .= $Verknuepfung . $Feldname . ' ' . $EinzelFeld['Operator'] . ' ' . $SK->DB->WertSetzen('SMK', 'T', $EinzelFeld['Wert']);
        $DebugInfo .= $Verknuepfung . $Feldname . ' ' .$EinzelFeld['Operator'] . ' ' . $EinzelFeld['Wert'];
    }
    $SubWhere = substr($SubWhere, 4);

    $WHERE .= $SubWhere;
    $WHERE .= ')';
    $DebugInfo .= ')';
}

$WHERE = substr($WHERE, 4);
$WHERE = ' WHERE ' . $WHERE;

$SQL = 'Select * From (';
$SQL .= 'select a.LAR_KEY, a.LAR_LARTNR, a.LAR_LIE_NR as LIE_NR, b.TEI_KEY, C.AST_ATUNR, C.AST_BEZEICHNUNG, D.WUG_KEY, d.WUG_BEZEICHNUNG, E.WGR_ID, e.WGR_BEZEICHNUNG, c.AST_KENNUNG ';
if (isset($SK->Param['IstZustand'])) {
    $SQL .= ' ,coalesce((SELECT TII_WERT FROM TEILEINFOSINFOS WHERE TII_ITY_KEY = 318 AND TII_TEI_KEY = TEI_KEY AND ROWNUM=1),\'0\') as TII_SK_SPERRE';
} else {
    $SQL .= ',0 as TII_SK_SPERRE';
}
$SQL .= ' from LIEFERANTENARTIKEL a';
$SQL .= ' inner join TEILEINFOS B';
$SQL .= ' on a.LAR_KEY = B.TEI_KEY2 and B.TEI_ITY_ID2 = \'LAR\'';
$SQL .= ' inner join ARTIKELSTAMM C';
$SQL .= ' on C.AST_KEY = B.TEI_KEY1 and B.TEI_ITY_ID1 = \'AST\'';
$SQL .= ' inner join WARENUNTERGRUPPEN D ';
$SQL .= ' on D.WUG_KEY = C.AST_WUG_KEY';
$SQL .= ' inner join WARENGRUPPEN E';
$SQL .= ' on E.WGR_ID = D.WUG_WGR_ID';
$SQL .= ' )';
$SQL .= $WHERE;

//Um den Server nicht zu sprengen, muss bei einer gewissen Anzahl an Datens�tze die Anzeige + Selektion limitiert werden.
//Es wird dann bei ALLEN Datens�tzen (auch diese, die nicht sichtbar sind) das H�ckchen gesetzt.
$Anz = $SK->DB->ErmittleZeilenAnzahl($SQL, $SK->DB->Bindevariablen('SMK', false));

$MaxAnz = 500;
$ZuVieleDS = false;
$SK->Form->Formular_Start();

if ($Anz > $MaxAnz) { //Wenn der SQL zuviele Datens�tze liefert

    //1. SQL merken, um auf einfachsten weg beim speichern ermittelt wird, welche Datens�tze in Frage kommen
    //SQL und Bindevariablen merken
    $SK->Param['SQL'] = $SQL;
    $SK->Param['Bindevariablen'] = $SK->DB->Bindevariablen('SMK', false);
    $ZuVieleDS = true;

    //2. Subselect herumbauen, um den SQL einzuschr�nken
    $SQL = 'select * from (' . $SQL . ') where rownum <= ' . $MaxAnz;

    //3. Hinweis Anzeigen
    $SK->Form->ZeileStart();
    $Hinweis = $SK->Form->LadeTextBaustein('Hinweis', 'Hinweis_SK_ZuVieleDS', $SK->AWISBenutzer->BenutzerSprache());
    $Hinweis = str_replace('#AnzDS#', $MaxAnz, $Hinweis);
    $SK->Form->Hinweistext($Hinweis, awisFormular::HINWEISTEXT_WARNUNG);
    $SK->Form->ZeileEnde();
}
$SK->Param['ZuVieleDS'] = $ZuVieleDS;
$rsSMK = $SK->DB->RecordSetOeffnen($SQL, $SK->DB->Bindevariablen('SMK'));
$SK->Form->DebugAusgabe(1,$SK->DB->LetzterSQL());

$FeldBreiten = array();
$FeldBreiten['TII_SK_SPERRE'] = 30;
$FeldBreiten['LAR_LARTNR'] = 100;
$FeldBreiten['LIE_NR'] = 100;
$FeldBreiten['AST_ATUNR'] = 100;
$FeldBreiten['AST_BEZEICHNUNG'] = 100;
$FeldBreiten['WUG_BEZEICHNUNG'] = 300;
$FeldBreiten['WGR_BEZEICHNUNG'] = 300;

echo '<script> ' . PHP_EOL;
echo ' //Alle anchecken.. ' . PHP_EOL;
echo '$( document ).ready(function() { ' . PHP_EOL;
echo '  $("#txtchkAlle").change(function() { ' . PHP_EOL;
echo '      $(".chkGruppe").prop(\'checked\',  $("#txtchkAlle").prop(\'checked\')); ' . PHP_EOL;
echo '  });  ' . PHP_EOL;
echo '});  ' . PHP_EOL;
echo '</script> ' . PHP_EOL;

$SK->Form->ZeileStart();
$SK->Form->Erstelle_Textarea('Where','Verkn�pfung ' . PHP_EOL . $DebugInfo,300,30,1,true,'','','','disabled');
$SK->Form->ZeileEnde();

$SK->Form->ZeileStart();

$SK->Form->Erstelle_ListenCheckbox('chkAlle', 1, $FeldBreiten['TII_SK_SPERRE'], true, 1, '', '', '', '', -2);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['LAR']['LAR_LARTNR'], $FeldBreiten['LAR_LARTNR']);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['LIE']['LIE_NR'], $FeldBreiten['LIE_NR']);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['AST']['AST_ATUNR'], $FeldBreiten['AST_ATUNR']);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['AST']['AST_BEZEICHNUNG'], $FeldBreiten['AST_BEZEICHNUNG']);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['WUG']['WUG_BEZEICHNUNG'], $FeldBreiten['WUG_BEZEICHNUNG']);
$SK->Form->Erstelle_Liste_Ueberschrift($SK->AWISSprachKonserven['WGR']['WGR_BEZEICHNUNG'], $FeldBreiten['WGR_BEZEICHNUNG']);
$SK->Form->ZeileEnde();

$DS = 0;
while (!$rsSMK->EOF()) {
    $DS++;
    $HG = $DS % 2;
    $SK->Form->ZeileStart();
    $SK->Form->Erstelle_ListenCheckbox('TII_SK_SPERRE_' . $rsSMK->FeldInhalt('TEI_KEY'), $rsSMK->FeldInhalt('TII_SK_SPERRE'), $FeldBreiten['TII_SK_SPERRE'], !$ZuVieleDS, 1, '', '',
        '', '', $HG, true, '', 'chkGruppe');
    $SK->Form->Erstelle_ListenFeld('LAR_LARTNR', $rsSMK->FeldInhalt('LAR_LARTNR'), 100, $FeldBreiten['LAR_LARTNR'], false, $HG);
    $SK->Form->Erstelle_ListenFeld('LIE_NR', $rsSMK->FeldInhalt('LIE_NR'), 100, $FeldBreiten['LIE_NR'], false, $HG);
    $SK->Form->Erstelle_ListenFeld('AST_ATUNR', $rsSMK->FeldInhalt('AST_ATUNR'), 100, $FeldBreiten['AST_ATUNR'], false, $HG);
    $SK->Form->Erstelle_ListenFeld('AST_BEZEICHNUNG', $rsSMK->FeldInhalt('AST_BEZEICHNUNG'), 100, $FeldBreiten['AST_BEZEICHNUNG'], false, $HG);
    $SK->Form->Erstelle_ListenFeld('WUG_BEZEICHNUNG', $rsSMK->FeldInhalt('WUG_BEZEICHNUNG'), 100, $FeldBreiten['WUG_BEZEICHNUNG'], false, $HG);
    $SK->Form->Erstelle_ListenFeld('WGR_BEZEICHNUNG', $rsSMK->FeldInhalt('WGR_BEZEICHNUNG'), 100, $FeldBreiten['WGR_BEZEICHNUNG'], false, $HG);
    $SK->Form->ZeileEnde();

    $rsSMK->DSWeiter();
}

$SK->Form->Formular_Ende();

$SK->ladebalkenEnde();
echo '</div>';

if($Schaltflaechenposition == 0){
    _Schaltflaechen($SK);
}

$SK->Form->SchreibeHTMLCode('</form>');

function _Schaltflaechen($SK){
    $SK->Form->SchaltflaechenStart();
    $SK->Form->Schaltflaeche('image', 'cmdStep_1', '', '/bilder/cmd_dszurueck.png', $SK->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $SK->Form->Schaltflaeche('image', 'cmdStep_3', '', '/bilder/cmd_dsweiter.png', $SK->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');

    $SK->Form->SchaltflaechenEnde();

}

?>