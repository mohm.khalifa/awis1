<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZAP','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10011 = $AWISBenutzer->HatDasRecht(10011);		// Artikelstamm
	if(($Recht10011)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZAP_GUELTIGAB DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT Zukaufartikellieferantenpreise.*, IMQ_IMPORTQUELLE';
	$SQL .= ', (SELECT COUNT(*) FROM Zukaufbestellungen WHERE ZUB_ZAP_KEY = ZAP_KEY) AS Verwendung';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufartikellieferantenpreise ';
	$SQL .= ' INNER JOIN Importquellen ON ZAP_IMQ_ID = IMQ_ID';
	$SQL .= ' WHERE ZAP_ZAL_KEY=0'.$AWIS_KEY2;

	if(isset($_GET['ZAP_KEY']))
	{
		$SQL .= ' AND ZAP_KEY = '.$DB->FeldInhaltFormat('N0',$_GET['ZAP_KEY']);
	}


	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUKKAEUFE',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$Form->DebugAusgabe(1,$SQL);
	$rsZAP = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if(!isset($_GET['ZAP_KEY']) AND $rsZAP->AnzahlDatensaetze()>=0)
	{
		$Form->ZeileStart();

		$Icons = array();
		if(($Recht10011&4))
		{
			$Icons[] = array('new','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise&ZAP_KEY=0&ZLA_KEY='.$AWIS_KEY1.'&Edit='.$AWIS_KEY2,'g',$AWISSprachKonserven['ZAP']['wrd_PreisHinzufuegen']);
			//$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		$Form->Erstelle_ListeIcons($Icons,38,-1);

		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZAP_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_GUELTIGAB'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_GUELTIGAB'],100,'',$Link);
		$Link .= '&SSort=ZAP_EINHEITENPROVK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_EINHEITENPROVK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_EINHEITENPROVK'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZAP_VKPR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_VKPR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_VKPR'],170,'',$Link);
		if(($Recht10011&16)==16)
		{
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&SSort=ZAP_EKPR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_EKPR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_EKPR'],100,'',$Link);
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&SSort=ZAP_RABATT1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_RABATT1'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_RABATT1'],100,'',$Link);
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&SSort=ZAP_RABATT2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_RABATT2'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_RABATT2'],100,'',$Link);
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&SSort=ZAP_EK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAP_EK'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_EK'],100,'',$Link);
		}
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_USER'],200,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['AnzBestellungen'],150,'',$Link);
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZAP->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(($Recht10011&6))	// �ndernrecht
			{
				$Link='./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise&ZAP_KEY='.$rsZAP->FeldInhalt('ZAP_KEY').'&ZLA_KEY='.$AWIS_KEY1.'&Edit='.$AWIS_KEY2;

				$Icons[] = array('edit','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise&ZAP_KEY='.$rsZAP->FeldInhalt('ZAP_KEY').'&ZLA_KEY='.$AWIS_KEY1.'&Edit='.$AWIS_KEY2);
				if($rsZAP->FeldInhalt('ZAP_IMQ_ID')==4)		// WWS Daten k�nnen nicht gel�scht werden
				{
					$Icons[] = array('delete','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise&Del='.$rsZAP->FeldInhalt('ZAP_KEY').'&Key='.$AWIS_KEY1.'&Edit='.$AWIS_KEY2);
				}
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('ZAP_GUELTIGAB',$rsZAP->FeldInhalt('ZAP_GUELTIGAB'),0,100,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('ZAP_EINHEITENPROVK',$rsZAP->FeldInhalt('ZAP_EINHEITENPROVK'),0,100,false,($DS%2),'','','Z','L');
			$Form->Erstelle_ListenFeld('ZAP_VKPR',$rsZAP->FeldInhalt('ZAP_VKPR'),0,170,false,($DS%2),'','','N2','L');
			if(($Recht10011&16)==16)
			{
				$Form->Erstelle_ListenFeld('ZAP_EKPR',$rsZAP->FeldInhalt('ZAP_EKPR'),0,100,false,($DS%2),'','','N2','L');
				$Form->Erstelle_ListenFeld('ZAP_RABATT1',$rsZAP->FeldInhalt('ZAP_RABATT1'),0,100,false,($DS%2),'','','N2','L');
				$Form->Erstelle_ListenFeld('ZAP_RABATT2',$rsZAP->FeldInhalt('ZAP_RABATT2'),0,100,false,($DS%2),'','','N2','L');
				$Form->Erstelle_ListenFeld('ZAP_EK',$rsZAP->FeldInhalt('ZAP_EK'),0,100,false,($DS%2),'','','N2','L');
			}
			$Form->Erstelle_ListenFeld('ZAP_USER',$rsZAP->FeldInhalt('ZAP_USER'),0,200,false,($DS%2),'','','T','L',$Form->Format('DU',$rsZAP->FeldInhalt('ZAP_USERDAT')));
			$Form->Erstelle_ListenFeld('ZAP_EK',$rsZAP->FeldInhalt('VERWENDUNG'),0,150,false,($DS%2),'','','N0','L');
			$Form->ZeileEnde();

			$rsZAP->DSWeiter();
			$DS++;
		}

		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	}
	elseif(isset($_GET['ZAP_KEY']))
	{
		$EditRecht = ($Recht10011&6);

		if($rsZAP->FeldInhalt('VERWENDUNG')>0)
		{
			$EditRecht=false;
		}
		$Form->Erstelle_HiddenFeld('ZAP_KEY',$rsZAP->FeldInhalt('ZAP_KEY'));
		$Form->Erstelle_HiddenFeld('ZAP_ZAL_KEY',$AWIS_KEY2);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&Unterseite=Preise&ZLA_KEY=".$AWIS_KEY1."&Edit=".$AWIS_KEY2." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZAP->FeldInhalt('ZAP_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZAP->FeldInhalt('ZAP_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_GUELTIGAB'].':',150);
		$Form->Erstelle_TextFeld('ZAP_GUELTIGAB',$rsZAP->FeldInhalt('ZAP_GUELTIGAB'),10,250,$EditRecht,'','','','D','L','',date('d.m.Y'));
		if($EditRecht)
		{
			$AWISCursorPosition='txtZAP_GUELTIGAB';
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_VKPR'].':',150);
		$Form->Erstelle_TextFeld('ZAP_VKPR',$rsZAP->FeldInhalt('ZAP_VKPR'),10,175,$EditRecht,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_MWS_ID'].':',150);
		$SQL = 'SELECT MWS_ID, MWS_LAN_CODE || \' - \' || MWS_BEZEICHNUNG FROM MEHRWERTSTEUERSAETZE';
		$SQL .= ' ORDER BY MWS_LAN_CODE, MWS_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('ZAP_MWS_ID',$rsZAP->FeldInhalt('ZAP_MWS_ID'),360,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		if($rsZAP->FeldInhalt('ZAP_KEY')!=0)
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_VKBRUTTO'].':',150);
			$Form->Erstelle_TextFeld('ZAP_VKBRUTTO',$rsZAP->FeldInhalt('ZAP_VKBRUTTO'),10,175,$EditRecht,'','','','N2','L');
		}
		$Form->ZeileEnde();

		if(($Recht10011&16)!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_EKPR'].':',150);
			$Form->Erstelle_TextFeld('ZAP_EKPR',$rsZAP->FeldInhalt('ZAP_EKPR'),10,175,$EditRecht,'','','','N2');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_RABATT1'].':',150);
			$Form->Erstelle_TextFeld('ZAP_RABATT1',$rsZAP->FeldInhalt('ZAP_RABATT1'),10,105,$EditRecht,'','','','N2','L','','0');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_RABATT2'].':',150);
			$Form->Erstelle_TextFeld('ZAP_RABATT2',$rsZAP->FeldInhalt('ZAP_RABATT2'),10,105,$EditRecht,'','','','N2','L','','0');
			if($rsZAP->FeldInhalt('ZAP_KEY')!=0)
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_EK'].':',150);
				$Form->Erstelle_TextFeld('ZAP_EK',$rsZAP->FeldInhalt('ZAP_EK'),10,175,$EditRecht,'','','','N2','L');
			}
			$Form->ZeileEnde();
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_ALTTEILWERT'].':',150);
		$Form->Erstelle_TextFeld('ZAP_ALTTEILWERT',$rsZAP->FeldInhalt('ZAP_ALTTEILWERT'),10,175,$EditRecht,'','','','N2','L','','0,00');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_EINHEITENPROVK'].':',150);
		$Form->Erstelle_TextFeld('ZAP_EINHEITENPROVK',$rsZAP->FeldInhalt('ZAP_EINHEITENPROVK'),10,175,$EditRecht,'','','','N0','L','',1);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_PREISBINDUNGVON'].':',150);
		$Form->Erstelle_TextFeld('ZAP_PREISBINDUNGVON',$rsZAP->FeldInhalt('ZAP_PREISBINDUNGVON'),10,175,$EditRecht,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_PREISBINDUNGBIS'].':',150);
		$Form->Erstelle_TextFeld('ZAP_PREISBINDUNGBIS',$rsZAP->FeldInhalt('ZAP_PREISBINDUNGBIS'),10,175,$EditRecht,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_LAN_CODE'].':',150);
		$SQL = 'SELECT LAN_CODE, LAN_LAND';
		$SQL .= ' FROM Laender ORDER BY LAN_LAND';
		$Form->Erstelle_SelectFeld('ZAP_LAN_CODE',$rsZAP->FeldInhalt('ZAP_LAN_CODE'),500,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'DE');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_SPERRE'].':',150);
		$KateogrieAlt = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('ZAP_SPERRE',$rsZAP->FeldInhalt('ZAP_SPERRE'),175,$EditRecht,'','',0,'','',$KateogrieAlt);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_SPERRGRUND'].':',150);
		$Form->Erstelle_TextFeld('ZAP_SPERRGRUND',$rsZAP->FeldInhalt('ZAP_SPERRGRUND'),50,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		if($rsZAP->FeldInhalt('ZAP_KEY')!='')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAP']['ZAP_IMQ_ID'].':',150);
			$Form->Erstelle_TextFeld('*IMQ_IMPORTQUELLE',$rsZAP->FeldInhalt('IMQ_IMPORTQUELLE'),50,500,false,'','','','T');
			$Form->ZeileEnde();
		}

	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181311");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181309");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>