<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KNZ','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('CAD','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','CSVErzeugen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht31000 = $AWISBenutzer->HatDasRecht(31000);
	
	if($Recht31000==0) //wenn kein recht 
	{
	    awisEreignis(3,1000,'Kartenzahlungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Kartenzahlungen'));

	$Block = 1;
	//********************************************************
	// Parameter von Suche 
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		
		$Param['KNZ_FILNR'] = $_POST['sucKNZ_FILNR'];
		$Param['KNZ_DATUM_VON'] = $_POST['sucKNZ_DATUM_VON'];
		$Param['KNZ_DATUM_BIS'] = $_POST['sucKNZ_DATUM_BIS'];
		
		$Param['KNZ_BETRAG'] = $_POST['sucKNZ_BETRAG'];
		$Param['KNZ_ART'] = $_POST['sucKNZ_ART'];
		//$Param['KNZ_BLZ'] = $_POST['sucKNZ_BLZ'];
		$Param['KNZ_KTONR'] = $_POST['sucKNZ_KTONR'];
		$Param['KNZ_TERMINALID'] = $_POST['sucKNZ_TERMINALID'];
		
		$Param['KNZ_CHECKED'] = '';

		$AWISBenutzer->ParameterSchreiben('Formular_Kartenzahlungen',serialize($Param));
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($_GET['Sort'])) //Wenn man sortiert darf f�r die Checkboxen kein Parameter geschrieben werden.
		{
			$Param['KNZ_CHECKED'] = '';
		
			foreach ($_POST as $key => $value)                              // dann alle POST-�bergaben durchlaufen
			{
				if (strpos($key, 'txtKNZ_CHK_') === 0)                      // wenn an 1. Position (0. Index), 'txtKNZ_CHK' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
				{
					if(isset($Param['KNZ_CHECKED']) AND strlen($Param['KNZ_CHECKED']) != 0 ) //2. und folgende Checkboxenkeys
					{ 
						$Param['KNZ_CHECKED'] = $Param['KNZ_CHECKED'] . ';' . str_replace('txtKNZ_CHK_','',$key);
					}
					else
					{
						$Param['KNZ_CHECKED'] = str_replace('txtKNZ_CHK_','',$key); //Erster Checkboxenkey
					}	
				}		
			}
		}
		
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			
			if(!isset($Param['ORDER']) AND $Param['ORDER'] = '')
			{
				if($_GET['Sort'] != 'KNZ_CHECKED' || $_GET['Sort']!= 'KNZ_CHECKED~')
				{
					$Param['ORDER']=$_GET['Sort'];
				}
				else
				{
					$Param['ORDER']='KNZ_DATUM DESC';
				}
			}
		
			$AWISBenutzer->ParameterSchreiben('Formular_Kartenzahlungen',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}
	
	//var_dump($_POST);

	//*********************************************************
	//* Sortierung
	//*********************************************************

	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '. $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY KNZ_DATUM DESC';
			$Param['ORDER']='KNZ_DATUM DESC';
		}
	}
	else
	{
		if(str_replace('~','',$_GET['Sort']) != 'KNZ_CHECKED')
		{
			$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		}
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL = 'SELECT knz.*, fil.fil_bez, ';
	$SQL .= 'row_number() OVER (';
	$SQL .= ' '.$ORDERBY;
	$SQL .= ' ) AS ZeilenNr ' ;
	$SQL .= 'FROM KARTENZAHLUNGEN knz
 			INNER JOIN FILIALEN fil
 			ON fil.FIL_ID = knz.KNZ_FILNR';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	// Zeilen begrenzen
	//$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$MaxDSAnzahl = 200;
	$StartZeile = (($Block - 1) * $MaxDSAnzahl) + 1;
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $MaxDSAnzahl);     
	
	$rsKNZ = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('KNZ', false));
	
	$AWISBenutzer->ParameterSchreiben('Formular_Kartenzahlungen',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmkartenzahlungen action=./kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST enctype="multipart/form-data">';

	$DetailAnsicht = false;
	
	$FeldBreiten = array();
	$FeldBreiten['Plus'] = 37;
	$FeldBreiten['FilNr'] = 70;
	$FeldBreiten['FilBez'] = 180;
	$FeldBreiten['Datum'] = 180;
	$FeldBreiten['Art'] = 70;
	$FeldBreiten['Betrag'] = 70;
	$FeldBreiten['TerminalID'] = 120;
	
	if(($Recht31000&2) == 2)
	{
		$FeldBreiten['TraceNR'] = 120;
		$FeldBreiten['BLZ'] = 110;
		$FeldBreiten['KTONR'] = 120;
	}
	
	$Gesamtbreite = 0;
	foreach ($FeldBreiten as $value) //Gesamtbreite f�r Fu�zeile
	{
		$Gesamtbreite += $value;
	}

	if($rsKNZ->AnzahlDatensaetze() > 0)
	{
		$Form->Formular_Start();
			
		if((($Recht31000&1) == 1 AND ($Recht31000&2) != 2 )) //Standartansicht Kopfzeile
		{
			$Form->ZeileStart();
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_CHECKED'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_CHECKED'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift('+',$FeldBreiten['Plus'] - 4,'font-weight:bolder', $Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_FILNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_FILNR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_FILNR'].':',$FeldBreiten['FilNr'],'',$Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=FIL_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_BEZ'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_FILIALE'].':',$FeldBreiten['FilBez'],'',$Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_DATUM'].':',$FeldBreiten['Datum'],'',$Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_ART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_ART'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_ART'].':',$FeldBreiten['Art'],'',$Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_BETRAG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_BETRAG'].':',$FeldBreiten['Betrag'],'',$Link);
				
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_TERMINALID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_TERMINALID'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',$FeldBreiten['TerminalID'],'',$Link);
			$Form->ZeileEnde();

		}
		elseif(($Recht31000&2 == 2)) //Erweiterte Ansicht Kopfzeile
		{
			$Form->ZeileStart();
		
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_CHECKED'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_CHECKED'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift('+',$FeldBreiten['Plus'] - 4,'font-weight:bolder', $Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_FILNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_FILNR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_FILNR'].':',$FeldBreiten['FilNr'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=FIL_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIL_BEZ'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_FILIALE'].':',$FeldBreiten['FilBez'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_DATUM'].':',$FeldBreiten['Datum'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_ART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_ART'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_ART'].':',$FeldBreiten['Art'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_BETRAG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_BETRAG'].':',$FeldBreiten['Betrag'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_TERMINALID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_TERMINALID'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',$FeldBreiten['TerminalID'],'',$Link);
		
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_BLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_BLZ'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_BLZ'].':',$FeldBreiten['BLZ'],'',$Link);
			
			$Link = './kartenzahlungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=KNZ_KTONR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_KTONR'))?'~':'');			
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_KTONR'].':',$FeldBreiten['KTONR'],'',$Link);
			$Form->ZeileEnde();
		}
		
		if($rsKNZ->AnzahlDatensaetze() > 12) //Scrolleiste
		{
			$Form->SchreibeHTMLCode('<div id=scrKNZ style="width: 100%; height: 450px;  overflow-y: scroll;">');
		}
		
		$DS=0;
		$Teilsumme=0.00;
		$Gesamtsumme=0.00;
		$AnzahlDSAuswahl = 0;
		
		//Benutzerparamter auslesen, in ein Array Schreiben, damit sp�ter die Checkboxen vorbelegt werden k�nnen.
		$AusgewaehlteDS = explode(';',$Param['KNZ_CHECKED']);
		
		
		while(!$rsKNZ->EOF())
		{	
			$checked = false; 
			if(isset($_GET['Sort']) AND $_GET['Sort']=='KNZ_CHECKED')
			{
				$checked = true;
			}
			
			foreach($AusgewaehlteDS as $key => $value)
			{
				if ($rsKNZ->FeldInhalt('KNZ_KEY') == $value)
				{
					$checked = true;
					$Teilsumme += str_replace(',','.',$rsKNZ->FeldInhalt('KNZ_BETRAG'));
					$AnzahlDSAuswahl += 1; //F�r Fu�zeile
				}	
				
			}
			
			if((($Recht31000&1)==1 AND ($Recht31000&2)!=2)) //Normale Ansicht Listenfelder
			{
				$Form->ZeileStart();
				
				$Form->Erstelle_Checkbox('KNZ_CHK_' . $rsKNZ->FeldInhalt('KNZ_KEY'), $checked, $FeldBreiten['Plus'],true);
				$Form->Erstelle_ListenFeld('!KNZ_FILNR',$rsKNZ->FeldInhalt('KNZ_FILNR'),4,$FeldBreiten['FilNr']);
				$Form->Erstelle_ListenFeld('!KNZ_FILIALE',$rsKNZ->FeldInhalt('FIL_BEZ'),24,$FeldBreiten['FilBez']);
				$Form->Erstelle_ListenFeld('!KNZ_DATUM',$rsKNZ->FeldInhalt('KNZ_DATUM'),30,$FeldBreiten['Datum']);
				$Form->Erstelle_ListenFeld('!KNZ_ART',$rsKNZ->FeldInhalt('KNZ_ART'),40,$FeldBreiten['Art']);
				$Form->Erstelle_ListenFeld('!KNZ_BETRAG',$rsKNZ->FeldInhalt('KNZ_BETRAG'),10,$FeldBreiten['Betrag']);
				$Form->Erstelle_ListenFeld('!KNZ_TERMINALID',$rsKNZ->FeldInhalt('KNZ_TERMINALID'),20,$FeldBreiten['TerminalID']);
			
				$Form->ZeileEnde();
				
				$Gesamtsumme += str_replace(',','.',$rsKNZ->FeldInhalt('KNZ_BETRAG'));
			}
			elseif ($Recht31000&2 ==2) //Erweiterte Ansicht Listenfelder 
			{
				$Form->ZeileStart();
				
				$Form->Erstelle_Checkbox('KNZ_CHK_' . $rsKNZ->FeldInhalt('KNZ_KEY'), $checked, $FeldBreiten['Plus'],true);
				$Form->Erstelle_ListenFeld('!KNZ_FILNR',$rsKNZ->FeldInhalt('KNZ_FILNR'),4,$FeldBreiten['FilNr']);
				$Form->Erstelle_ListenFeld('!KNZ_FILIALE',$rsKNZ->FeldInhalt('FIL_BEZ'),24,$FeldBreiten['FilBez']);
				$Form->Erstelle_ListenFeld('!KNZ_DATUM',$rsKNZ->FeldInhalt('KNZ_DATUM'),30,$FeldBreiten['Datum']);
				$Form->Erstelle_ListenFeld('!KNZ_ART',$rsKNZ->FeldInhalt('KNZ_ART'),40,$FeldBreiten['Art']);
				$Form->Erstelle_ListenFeld('!KNZ_BETRAG',$rsKNZ->FeldInhalt('KNZ_BETRAG'),10,$FeldBreiten['Betrag']);
				$Form->Erstelle_ListenFeld('!KNZ_TERMINALID',$rsKNZ->FeldInhalt('KNZ_TERMINALID'),20,$FeldBreiten['TerminalID']);
				//$Form->Erstelle_ListenFeld('!KNZ_TRACENR',$rsKNZ->FeldInhalt('KNZ_TRACENR'),20,$FeldBreiten['TraceNR']);
				$Form->Erstelle_ListenFeld('!KNZ_BLZ',$rsKNZ->FeldInhalt('KNZ_BLZ'),20,$FeldBreiten['BLZ']);
				$Form->Erstelle_ListenFeld('!KNZ_KTONR',$rsKNZ->FeldInhalt('KNZ_KTONR'),20,$FeldBreiten['KTONR']);
	
				$Form->ZeileEnde();
				
				$Gesamtsumme += str_replace(',','.',$rsKNZ->FeldInhalt('KNZ_BETRAG'));
				
			}
			$rsKNZ->DSWeiter();
			$DS++;
		}
		
		if($rsKNZ->AnzahlDatensaetze() > 12)
		{
			$Form->SchreibeHTMLCode('</div>') ;
		}
		
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('KNZ', false));
		$DSGesvorAkt = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$Form->ZeileStart();
		//$Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsKNZ->AnzahlDatensaetze()). $AWISSprachKonserven['KNZ']['KNZ_DSvon']. $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe') .$AWISSprachKonserven['KNZ']['KNZ_DSMAX']. $AnzahlDSAuswahl . ' Datens�tze addiert: ' . $Teilsumme . ' Euro. ', $Gesamtbreite, 'font-weight:bolder;');
		$Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsKNZ->AnzahlDatensaetze()). $AWISSprachKonserven['KNZ']['KNZ_DSvon']. '200' .$AWISSprachKonserven['KNZ']['KNZ_DSMAX']. $AnzahlDSAuswahl . ' Datens�tze addiert: ' . $Teilsumme . ' Euro. ', $Gesamtbreite, 'font-weight:bolder;');
		$Form->ZeileEnde();
				
		$Link = './kartenzahlungen_Main.php?cmdAktion=Details';
	
		$Form->Formular_Ende();
	}
	else 
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}			
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmd_teilsumme', '/kartenzahlungen_Main.php', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	
	if(($Recht31000&3) == 3)
	{
		$LinkPDF = '/berichte/drucken.php?XRE=45&ID='.base64_encode('&KNZ_KEY=' . $Param['KNZ_CHECKED'].
																	'&KNZ_FILNR=' . $Param['KNZ_FILNR'] . 
																	'&KNZ_DATUM_VON='. $Param['KNZ_DATUM_VON'] .
																	'&KNZ_DATUM_BIS='. $Param['KNZ_DATUM_BIS'] .
																	'&KNZ_BETRAG=' .$Param['KNZ_BETRAG'].
																	'&KNZ_ART='. $Param['KNZ_ART'] .
																//	'&KNZ_BLZ=' . $Param['KNZ_BLZ'].
																	'&KNZ_KTONR=' . $Param['KNZ_KTONR']);
		$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);

		/*
		$LinkCSV = './kartenzahlungen_csv.php?ID='.base64_encode('&KNZ_KEY=' . $Param['KNZ_CHECKED'].
				'&KNZ_FILNR=' . $Param['KNZ_FILNR'] .
				'&KNZ_DATUM_VON='. $Param['KNZ_DATUM_VON'] .
				'&KNZ_DATUM_BIS='. $Param['KNZ_DATUM_BIS'] .
				'&KNZ_BETRAG=' .$Param['KNZ_BETRAG'].
				'&KNZ_ART='. $Param['KNZ_ART'] .
				//'&KNZ_BLZ=' . $Param['KNZ_BLZ'].
				'&KNZ_KTONR=' . $Param['KNZ_KTONR']);
		$Form->Schaltflaeche('href', 'cmdCSV', $LinkCSV, '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['Wort']['CSVErzeugen']);
		*/
	}
	
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151651");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151652");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWIS_KEY2;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';


	if(isset($Param['KNZ_FILNR']) AND $Param['KNZ_FILNR']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_N0_knz_filnr', $Param['KNZ_FILNR'], awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND KNZ_FILNR = :var_N0_knz_filnr';
	}
	
	if(isset($Param['KNZ_DATUM_VON']) OR isset($Param['KNZ_DATUM_BIS']))
	{
		if(isset($Param['KNZ_DATUM_BIS']) AND $Param['KNZ_DATUM_BIS'] != '')
		{
			$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_von', $Param['KNZ_DATUM_VON'], awisDatenbank::VAR_TYP_DATUM);
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) >= :var_D_knz_datum_von';

			$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_bis', $Param['KNZ_DATUM_BIS'], awisDatenbank::VAR_TYP_DATUM);
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) <= :var_D_knz_datum_bis';
		}
		else
		{
			if($Param['KNZ_DATUM_VON'] != '')
			{
				$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_von', $Param['KNZ_DATUM_VON'], awisDatenbank::VAR_TYP_DATUM);
				$Bedingung .= ' AND TRUNC(KNZ_DATUM) = :var_D_knz_datum_von';
			}
		}
	}
	
	if(isset($Param['KNZ_BETRAG']) AND $Param['KNZ_BETRAG']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_knz_betrag', $Param['KNZ_BETRAG'], awisDatenbank::VAR_TYP_UNBEKANNT);
		$Bedingung .= ' AND KNZ_BETRAG = :var_knz_betrag';
	}
	
	if(isset($Param['KNZ_ART']) AND $Param['KNZ_ART']!='' AND $Param['KNZ_ART']!=1)
	{
		$DB->SetzeBindevariable('KNZ', 'var_T_knz_art', $Param['KNZ_ART'], awisDatenbank::VAR_TYP_TEXT);
		$Bedingung .= ' AND KNZ_ART = :var_T_knz_art';
	}
	
	/*
	if(isset($Param['KNZ_BLZ']) AND $Param['KNZ_BLZ']!='')
	{
		$Bedingung .= ' AND KNZ_BLZ = ' . $DB->FeldInhaltFormat('T',$Param['KNZ_BLZ']);
	}
	*/
	
	if(isset($Param['KNZ_KTONR']) AND $Param['KNZ_KTONR']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_T_knz_ktonr', $Param['KNZ_KTONR'].'$', awisDatenbank::VAR_TYP_TEXT);
		$Bedingung .= ' AND REGEXP_LIKE(KNZ_KTONR, :var_T_knz_ktonr)';
	}
	
	if(isset($Param['KNZ_TERMINALID']) AND $Param['KNZ_TERMINALID']!='')
	{
	    $DB->SetzeBindevariable('KNZ', 'var_T_KNZ_TERMINALID', $Param['KNZ_TERMINALID'].'$', awisDatenbank::VAR_TYP_TEXT);
	    $Bedingung .= ' AND REGEXP_LIKE(KNZ_TERMINALID, :var_T_KNZ_TERMINALID)';
	}

	return $Bedingung;
}



?>