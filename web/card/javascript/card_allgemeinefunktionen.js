/* Liest den Wert aus einem Datumsfeld, gibt ihn an eine Pr�ffunktion weiter und
gibt einen Wahrheitswert zur�ck. Als Parameter erh�lt die Funktion den Feldnamen
des zu untersuchenden Feldes */

function datumsfeld(feldname)
{
	if(datumspruefung(document.getElementsByName(feldname)[0].value))
		return true;
	else
		return false;
	
}


// pr�ft ob es sich um ein g�ltiges Datum handelt


function gueltigesdatum(datum)
{
	schalter=true;
	
	arrmonate=new Array(31,28,31,30,31,30,31,31,30,31,30,31);
	tag=parseInt(datum.slice(0,2),10);
	monat=parseInt(datum.slice(3,5),10);
	jahr=parseInt(datum.slice(6),10);
	if(jahr>30)		//Umwandlung in vierstelliges Jahr
		jahr+=1900;
	else jahr+=2000;
		
	if(monat<1 || monat>12)
	{
		schalter=false;
	}
	else
	{
		if(jahr%4==0 && (!(jahr%100==0)||jahr%400==0))	// Schaltjahrespr�fung
		arrmonate[1]=29;
		
		if(tag<1 || tag>arrmonate[(monat-1)])
		{
			schalter=false;
		}
	}
	
	return schalter;
}

/* pr�ft ob ein Datum in der Form TT.MM.JJ vorliegt und dieses auch g�ltig ist*/

function datumspruefung(objekt)
{
	var datum = objekt.value;
	var ausdruck=/^\d\d\.\d\d\.\d\d$/;
	if(ausdruck.test(datum)&&gueltigesdatum(datum))
	{
		return true;
	}
	else
	{
		if(datum=='')
			alert(unescape("Geben Sie bitte ein g%FCltiges Datum ein!"));
		else
			alert(unescape("\""+datum+"\" ist kein g%FCltiges Datum!"));
			
		objekt.select();
		return false;
	}
}

/* pr�ft ob ein Feld leer ist, als Parameter wird der Feldname �bergeben */

function leerpruef(feldname)
{
	var s="document.hauptformular."+feldname;
	var s1=s+".value";
	var s2=s+".focus()";
	if(eval(s1)=="")
	{
		alert("Bitte Eintrag vornehmen!");
		eval(s2);
		return false;
	}
	else
	return true;
}

/* prueft ob ein Eintrag in einer Liste gew�hlt worden ist */

function auswahlliste(listenname)
{
	var s="document.hauptformular."+listenname;
	var s1=s+".selectedIndex";
	var s2=s+".focus()";
	if(eval(s1)==0||eval(s1)=="")
	{
		alert("Bitte Eintrag ausw�hlen!");
		eval(s2);
		return false;
	}
	else
	return true;
}

/* prueft ob ein zweites Datum hinter dem ersten Datum liegt und gibt den Text
der Variablen text aus
Parameter: Datumsfeld1,Datumsfeld 2,Text */

function spaeterdatum(datum1,datum2,text)
{
	var sd1="document.hauptformular."+datum1;
	var sd2="document.hauptformular."+datum2;
	var wsd1=eval(sd1+".value");
	var wsd2=eval(sd2+".value");
	var fsd2=sd2+".focus()";
	var ssd2=sd2+".select()";
	
	var arrd1=wsd1.split(".");
	var arrd2=wsd2.split(".");
	
	dat1=new Date(parseInt(arrd1[2]),(parseInt(arrd1[1])-1),parseInt(arrd1[0]));
	dat2=new Date(parseInt(arrd2[2]),(parseInt(arrd2[1])-1),parseInt(arrd2[0]));
	
	if(dat1<=dat2)
	return true;
	else
	{
		alert(text);
		eval(fsd2);
		eval(ssd2);
		return false;
	}
}

//Ersetzt einfach Hochkommas durch doppelte Hochkommas

function hochkommasersetzen(wort)
{
	while(wort.match(/'/))
		wort=wort.replace(/'/,"\"");
	
	return wort;
}

/* Pr�ft ob es sich beim �bergebenen Wert um eine Ganzzahl handelt, gibt true oder false zur�ck */
function ganzzahl(feld)
{
	s="document.hauptformular."+feld;
	wert=eval(s+".value");
	if(wert.search(/^[0-9]*$/))
		return true;
	else
		return false;
}

// �berpr�ft ob der �bergebene Wert eine Dezimalzahl ist und gibt true oder false zur�ck

function dezimalzahl(feld)
{
	s="document.hauptformular."+feld;
	wert=eval(s+".value");
	if(wert.search(/^[0-9]*(,[0-9]*)?$/))
		return true;
	else
		return false;
}

// Pr�ft Betragseingabe und gibt entsprechende Fehlermeldungen aus: Gibt true oder false zur�ck
// Parameter: objekt = Objekt des betroffenen Textfeldes, stellen = Anzahl Vorkommastellen, welche der Betrag h�chstens haben darf

function betragspruef(objekt,stellen)
{
	var betrag = objekt.value;
	
	while(betrag.match(/\s/))
		betrag=betrag.replace(/\s/,"");	//Leerzeichen entfernen
	
	
	if(betrag.search(/^[-]?([0-9]{1,3}(\.[0-9]{3})*|[0-9]+)(,[0-9]{0,2})?(�)?$/)>-1) //Es d�rfen nur Zahlen, ev. Tausenderpunkt, genau ein Komma sowie ein �-Zeichen eingegeben werden
	{
		//Pr�fen ob zu viele Vorkommastellen
		
		var negativ = 0;
		var intZahl = parseInt(datenbankbetrag(betrag));
		
		if(intZahl < 0)
			negativ = 1;
		
		
		if(intZahl.toString().length>stellen+negativ)
		{
			alert(unescape("Der Betrag hat zu viele Vorkommastellen!\nErlaubt sind maximal "+stellen+" Vorkommastellen!"));
			objekt.select();
			return false;
		}
		else
			return true;
	}
	else
	{
		alert(unescape("Bitte korrekten Betrag in der Form\n99,99 oder 99,99 � eingeben!"));
		objekt.select();
		return false;
	}
}

// Wandelt einen Betrag in der Form 1.234,56 � in die Form 12345.56 um, damit dieser in Datenbank eingetragen werden kann

function datenbankbetrag(betrag)
{
	while(betrag.match(/\./))
			betrag=betrag.replace(/\./,"");	//. (Tausendertrennzeichen) entfernen
	betrag=betrag.replace(/,/,".");			//Ersetzt das Komma durch einen Punkt
	betrag=parseFloat(betrag);
	return betrag;
}

//Liefert den Wert eines Feldes, Argumente: Objekt, feldname

function feldwert(obj,feldname)
{
    var rueckgabe;
    var objekt=obj.getElementsByName(feldname)[0];
    var feldart=objekt.type; 
    
    if(feldart=="select-one"&&!objekt.disabled)
    {
        rueckgabe=escape(objekt.options[objekt.selectedIndex].value);
    }
    else if(feldart=="text"||feldart=="hidden")
        {
            rueckgabe=objekt.value;
        }
    return rueckgabe;
}

// Wird ausgef�hrt, wenn Fehlermeldung ausgegeben werden soll

function fehlermeldung(meldetext,fokus)
{
	alert(unescape(meldetext));
}

// Liefert bei �bergabe eines Listenfeldobjektes den aktuellen Wert (text) dieses Feldes
// durch optionale �bergabe von wert (z.B. "value") kann auch der Wert (<option value="...">) des Eintrags ermittelt werden

function listeneintrag(objekt,wert)
{
	if(typeof(wert)=='undefined')
	{
		var wert="text";
	}
	
	return eval("objekt.options[objekt.selectedIndex]."+wert);
}

//Graut alle Felder mit Ausnahme des ausl�senden Feldes im aktuellen Fenster aus, welche den Parameter name="grau" haben

function aenderung(funktionsname)			//Wird aufgerufen, wenn Taste in �nderbaren Textfeldern gedr�ckt worden ist
{											//Als Parameter kann optional ein Funktionsname �bergeben werden, welcher die lokal
											//zu definierende Funktion mit diesem Namen aufruft									
	var intTastenwert = window.event.keyCode;	//ermittelt den Unicode, der gedr�ckten Taste
	
	if(intTastenwert!=9)	//Wenn nicht Tabulator-Taste
	{
		//Alle Felder mit name="grau" mit Ausnahme des aktuellen Feldes werden disabled gesetzt
		var obj=document.getElementsByName("grau");
						
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].id!=window.event.srcElement.id)
			{
				obj[i].disabled = true;
			}
		}
		
		if(typeof(funktionsname)!='undefined')	//Funktion aufrufen, deren Funktionsname �bergeben worden ist
		{
			try{
			eval(funktionsname+"();");
			}catch(e){}
		}
	}			
}


//Graut alle Felder der Zeilen mit Ausnahme der Felder der ausl�senden Zeile im aktuellen Fenster aus, deren Felder den Parameter name="grau" haben
//Zur Ermittlung der aktuellen Zeile m�ssen die Felder einen Parameter id= "3-stelliges K�rzel (z.B. bet) sowie Datensatzid (z.B. 45678)" haben:
//z.B. id="bet45678"

function aenderungzeile(funktionsname)		//Wird aufgerufen, wenn Taste in �nderbaren Textfeldern gedr�ckt worden ist
{											//Als Parameter kann optional ein Funktionsname �bergeben werden, welcher die lokal
											//zu definierende Funktion mit diesem Namen aufruft									
	var intTastenwert = window.event.keyCode;	//ermittelt den Unicode, der gedr�ckten Taste
	
	if(intTastenwert!=9)	//Wenn nicht Tabulator-Taste
	{
		//Alle Felder mit name="grau" mit Ausnahme der aktuellen Zeile werden disabled gesetzt
		var obj=document.getElementsByName("grau");
						
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].id.slice(3)!=window.event.srcElement.id.slice(3))
			{
				obj[i].disabled = true;
			}
		}
		
		//Alle L�schbuttons werden ausgeblendet
		obj=document.getElementsByName("loeschen");
		
		for(i=0;i<obj.length;i++)
		{
			obj[i].style.display = "none";
		}
		
		
		if(typeof(funktionsname)!='undefined')	//Funktion aufrufen, deren Funktionsname �bergeben worden ist
		{
			try{
			eval(funktionsname+"();");
			}catch(e){}
		}
	}			
}

//�ffnet bei einem Doppelklick auf ein Feld ein Zoomfenster mit dem Inhalt dieses Feldes
function zoom()
{
	var obj = window.event.srcElement;
	
	try{
	
	if(obj.getAttribute("type")=="text"||obj.nodeName=="TEXTAREA")
	{
		var lesen = 0;
			
		if(obj.disabled || obj.getAttribute("readonly"))		//ermittelt ob das Textfeld disabled oder readonly gesetzt ist
			lesen = 1;
			
		var feldlaenge = obj.getAttribute("maxlength");
		
		if(feldlaenge==null)	//Wenn Attribut maxlength nicht gesetzt ist, so wird automatisch eine Feldlaenge von 1000 angenommen
			feldlaenge=1000;
		
		var rueck = showModalDialog("zoom.php?strFeldinhalt="+escape(obj.value)+"&intFeldlaenge="+feldlaenge+"&intLesen="+lesen,"","dialogHeight:300px;dialogWidth:400px;scroll:0;status:0;help:0;");
		
		if(!lesen && obj.value!=rueck)
		{
			obj.value = rueck;
		
			//Ermittelt die Funktion, welche �ber onkeyup aufgerufen wird, und ruft diese auf
			var anfang = String(obj.getAttribute("onkeyup")).slice(23);
			var ende = anfang.substr(0,anfang.length-2);
			eval(ende);
		}
	}
	}catch(e){}
}

//Einfache Hochkommas werden durch doppelte einfache Hochkommas gequotet

function hochkommasquoten(wort)
{
	var neuwert="";

	for(i=0;i<wort.length;i++)
	{
		neuwert+=wort.charAt(i).replace(/'/,"''"); //einfaches ' durch doppeltes '' ersetzen
	}
	return neuwert;
}

//Bereitet einen Text f�r einen Eintrag in die Datenbank vor

function textdbformat(zeichenkette)
{
	return hochkommasquoten(zeichenkette);
}

//Pr�ft ob es sich bei der �bergebenen Zahl um einen ganzzahligen Wert handelt

function intpruef(intZahl)
{
	if(intZahl.search(/^[0-9]+$/)>-1)
		return true;
	else
		return false;
}

//Pr�ft ob es sich um eine korrekte Rechnungsnr handelt

function rnrpruef(strRnr)
{
	if(strRnr.search(/^[0-9]{6}[AM]{1}$/)>-1 || strRnr.search(/^[0-9]{6} [0-9]{9}A$/)>-1 || 
	(strRnr.search(/^[0-9]{4}\/[0-9]{6}$/)>-1&&parseInt(strRnr.slice(0,2),10)<=12&&parseInt(strRnr.slice(0,2),10)>=1)) 
		return true;
	else
		return false;
}

//Blendet versteckte (hidden) Elemente mit dem Attribut name="neuzeile" ein

function neuezeileein()
{
	var obj = document.getElementsByName("neuzeile");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.visibility = "visible";
	}
}

//Blendet Elemente mit dem Attribut name="neuzeile" aus

function neuezeileaus()
{
	var obj = document.getElementsByName("neuzeile");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.visibility = "hidden";
	}
}

//Wandelt einen Double-Wert in einen Eurobetrag um (z.B. 15.3 in 15,30 �)

function eurobetrag(dblBetrag)
{
	var strVorzeichen = '';
	
	if(dblBetrag.search(/-/)>-1)
	{
		dblBetrag = dblBetrag.replace(/-/,'');
		strVorzeichen = '-'
	}
	
	var arrDblBetrag = dblBetrag.split(".");
	var strVorKomma = arrDblBetrag[0];
	var strNachKomma = arrDblBetrag[1];
	
	
	if(strVorKomma==''||typeof(strVorKomma)=='undefined')
		var intLangVorKomma = 0;
	else
		var intLangVorKomma = strVorKomma.length;
	
	if(strNachKomma==''||typeof(strNachKomma)=='undefined')
		var intLangNachKomma = 0;
	else
		var intLangNachKomma =	strNachKomma.length;
	
	if(intLangVorKomma==0)
	{
		var strVorKommaNeu = '0';
	}
	else
	{
		var strVorKommaNeu = '';
		for(i=intLangVorKomma;i>0;i-=3)
		{
			strVorKommaNeu = strVorKomma.substring(i-3,i)+"."+strVorKommaNeu;
		}
		
		strVorKommaNeu = strVorKommaNeu.slice(0,strVorKommaNeu.length-1);
	}
	
	var strNachKommaNeu = strNachKomma;
	
	if(intLangNachKomma==0)
		strNachKommaNeu = '00';
	if(intLangNachKomma==1)
		strNachKommaNeu = strNachKommaNeu+'0';	
	
	return strVorzeichen+strVorKommaNeu+","+strNachKommaNeu+" �";
}

//Schneidet Nachkommastellen von dblBetrag ab intStellen+1 Nachkommastellen weg (z.B. nachkommaweg(19.259,2) gibt 19.25 zur�ck) und gibt den Wert zur�ck

function nachkommaweg(dblBetrag,intStellen)
{
	var intFaktor=Math.pow(10,intStellen);
	return Math.floor(dblBetrag*intFaktor)/intFaktor;
}
