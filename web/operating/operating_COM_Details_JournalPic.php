<?php
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("jpgraph/jpgraph.php");
require_once("jpgraph/jpgraph_gantt.php");

global $comcon;
global $AWISBenutzer;
$awisRSZeilen='';

$con = awisLogon();//@OCILogon("awis","IlOuLua",awisDBServer());
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Verbindung zur Datenbank</font></h2>");
}

if (!empty($_REQUEST["startzeit"]))
{
	$startzeit = $_REQUEST["startzeit"];
}
else
{
	$startzeit = date("d.m.Y H:i",strtotime("-1 day"));
}
if (!empty($_REQUEST["endzeit"]))
{
	$endzeit = $_REQUEST["endzeit"];
}
else
{
	$endzeit = date("d.m.Y H:i");
}
/*if (!empty($_REQUEST["laufzeit"]))
{
	$laufzeit = $_REQUEST["laufzeit"];
}*/


$comcon = awisLogonComCluster('DE',false);


	$sql = "select decode(jou_argument, null, jou_funktion, jou_argument) BEZ, ";
	$sql .= "to_char(jou_start, 'DD.MM.YYYY HH24:MI:SS') Anfang, to_char(jou_ende, 'DD.MM.YYYY HH24:MI:SS') Ende, ";
	$sql .= "round((jou_ende - jou_start) * 3600 * 24, 0) Dauer, JOU_FEHLERCODE, JOU_ANZSTATEMENT from journal ";
	$sql .= "where jou_start >= to_date('" . $startzeit . "', 'DD.MM.YYYY HH24:MI') ";
	$sql .= "and jou_start <= to_date('" . $endzeit . "', 'DD.MM.YYYY HH24:MI') ";
	$sql .= "order by jou_start";

	$rsJouDaten = awisOpenRecordset($comcon, $sql);
	$rsJouDatenZeilen = $awisRSZeilen;

	awisLogoff($comcon);			


$gJpgBrandTiming=true;			// Laufzeit mit ausgeben
$graph = new GanttGraph();
$graph->scale->SetDateLocale('de_DE');
$graph->SetMarginColor('blue:1.7');
$graph->SetColor('white');
$graph->SetDateRange($startzeit,$endzeit);

$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->scale->hour->SetBackgroundColor('lightyellow:1.5');
$graph->scale->hour->SetFont(FF_FONT1);
$graph->scale->day->SetBackgroundColor('lightyellow:1.5');
$graph->scale->day->SetFont(FF_FONT1,FS_BOLD);

$graph->title->Set("Zeitlicher Ablauf der Importe als Image-Map");
$graph->title->SetColor('white');
$graph->subtitle->Set("Alle Jobs von " . $startzeit . " bis " . $endzeit);
$graph->subtitle->SetColor('white');
//$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);

$graph->ShowHeaders(GANTT_HDAY | GANTT_HHOUR);

// $graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
// $graph->scale->week->SetFont(FF_FONT1);
$graph->scale->hour->SetIntervall(1);

$graph->scale->hour->SetStyle(HOURSTYLE_HM24);
$graph->scale->day->SetStyle(DAYSTYLE_SHORTDAYDATE3);
$graph->scale->UseWeekendBackground(false);

for($DS=0;$DS<$rsJouDatenZeilen;$DS++)
{
	$bar = new GanttBar($DS,$rsJouDaten["BEZ"][$DS],$rsJouDaten["ANFANG"][$DS],$rsJouDaten["ENDE"][$DS],$rsJouDaten["DAUER"][$DS] . " (" . $rsJouDaten["JOU_ANZSTATEMENT"][$DS] . ")",10);
	$bar->SetPattern(BAND_RDIAG,"yellow");
	$bar->SetFillColor("red");
//	$bar->progress->Set(0.5);
	if ($rsJouDaten["JOU_FEHLERCODE"][$DS] == "0")
	{
		$bar->SetCSIMTarget('/operating/operating_COM_Details_Journal.php','OK ' . substr($rsJouDaten["ANFANG"][$DS], 11) . ' - ' . substr($rsJouDaten["ENDE"][$DS], 11));
		$bar->title->SetCSIMTarget('/operating/operating_COM_Details_Journal.php','OK ' . substr($rsJouDaten["ANFANG"][$DS], 11) . ' - ' . substr($rsJouDaten["ENDE"][$DS], 11));
//		$bar->caption->SetColor('darkgreen');
		
	}
	else
	{
		$bar->SetCSIMTarget('/operating/operating_COM_Details_Journal.php','Fehler ' . substr($rsJouDaten["ANFANG"][$DS], 11) . ' - ' . substr($rsJouDaten["ENDE"][$DS], 11));
		$bar->title->SetCSIMTarget('/operating/operating_COM_Details_Journal.php','Fehler ' . substr($rsJouDaten["ANFANG"][$DS], 11) . ' - ' . substr($rsJouDaten["ENDE"][$DS], 11));
		$bar->caption->SetColor('red');
/*		$bar->rightMark->Show();
		$bar->rightMark->title->Set("?");
		$bar->rightMark->SetType(MARK_FILLEDCIRCLE);
		$bar->rightMark->SetWidth(10);
		$bar->rightMark->SetColor("red");
		$bar->rightMark->SetFillColor("red");
//		$bar->rightMark->title->SetFont(FF_ARIAL,FS_BOLD,12);
		$bar->rightMark->title->SetColor("white");
*/
	}
//	$bar->SetShadow(true,"gray");
	$graph->Add($bar);
}

   // Setup a horizontal grid
$graph->hgrid->Show();
//$graph->hgrid-> line->SetColor('lightblue' );
//$graph->hgrid->line->Show(false);
$graph->hgrid->SetRowFillColor( 'darkblue@0.95');

$graph->StrokeCSIM(basename(__FILE__));

?>
