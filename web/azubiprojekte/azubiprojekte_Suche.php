<?php
global $Funktionen;

$Funktionen->RechteMeldung(2);

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./azubiprojekte_Main.php?cmdAktion=Details>");
$Funktionen->Form->Formular_Start();

$LabelBreite = 200;
$FeldBreite = 200;

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_STUFE'].":",$LabelBreite);
$SQL = 'SELECT  AZB_STUFE, AZB_BEREICHNAME';
$SQL .= ' FROM AZUBIPROJEKTEBEREICHE';
$SQL .= ' ORDER BY AZB_STUFE';
$Funktionen->Form->Erstelle_SelectFeld('*AZB_STUFE', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['AZT_AZB_STUFE']:''), $FeldBreite, true, $SQL, "~".$Funktionen->AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','width: 500px');
$Funktionen->Form->ZeileEnde();
$Funktionen->AWISCursorPosition = "sucAZB_STUFE";

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel('', 1000);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_PERSNR'].":", $LabelBreite);
$Funktionen->Form->AuswahlBox('AZT_PERSNR', 'box1', '', 'AZT_Daten', 'txtAZT_FILNR_HIDDEN', $FeldBreite, 200, (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['AZT_PERSNR']:''), 'T', true, '', '', '', '6', '', '', '', 0,'','pattern = "[0-9*%]{1,6}"');
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_FILNR'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*AZT_FILNR', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['AZT_FILNR']:''), 200, $FeldBreite, true,'','','','T','L','','',4,'','','pattern = "[0-9*%]{1,4}"');
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_VORNAME'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*AZT_VORNAME', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['AZT_VORNAME']:''), 200, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_NAME'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*AZT_NAME', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['AZT_NAME']:''), 200, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Funktionen->Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?'on':''),50,true,'on');
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->Formular_Ende();

$Funktionen->Form->SchaltflaechenStart();

$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Funktionen->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_suche'], 'W');

$Funktionen->Form->SchaltflaechenEnde();

$Funktionen->Form->SchreibeHTMLCode("</form>");