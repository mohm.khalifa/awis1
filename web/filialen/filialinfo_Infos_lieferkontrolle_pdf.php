<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?
//********************************************************************************************
// Lieferkontrolle
//********************************************************************************************
//
// Dieses Modul erstellt eine PDF - Datei f�r die Lieferkontrollen der Filialen
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 23.07.2007
//********************************************************************************************
// �nderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
global $AWISBenutzer;

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

?>
</head>

<body>
<?
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3600);	// MDE Lieferkontrolle
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Lieferkontrolle-PDF', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_GET);


if ($_REQUEST['LKK_LIEFERSCHEINNR']!='' && $_REQUEST['LKK_FIL_ID']!= '' ) // && ($RechteStufe&16)==16)
{
	$FILIALE = intval($_REQUEST['LKK_FIL_ID']);
	
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	$SQL = 'SELECT *';
	$SQL .=' FROM Lieferkontrollen LIK';
	$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = LIK_AST_ATUNR';
	$SQL .=' WHERE LIK_FIL_ID=0'.intval($_REQUEST['LKK_FIL_ID']);
	$SQL .= ' AND LIK_LieferscheinNr=0'.intval($_REQUEST['LKK_LIEFERSCHEINNR']);
//	$SQL .= ' AND LIK_MENGESOLL <> (LIK_MENGEIST+LIK_MENGEKORREKTUR)';
	$SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
		
	awis_Debug(1,$SQL);
	
	$rsLIK = awisOpenRecordset($con,$SQL);
	$rsLIKZeilen = $awisRSZeilen;
	
	if($rsLIKZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten gefunden!</span></center>");
	}
	else 
	{
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
	
		define('FPDF_FONTPATH','font/');
		$pdf = new fpdi('p','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		$ATULogo = $pdf->ImportPage(1);				
		
		$Zeile=0;
		$Seitenrand=15;
		$Seite = 1;
		
		NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$FILIALE,$Seite);
				
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;
		
		$LSNR = $rsLIK['LIK_LIEFERSCHEINNR'][0];
		$Kommissionierung = $rsLIK['LIK_DATUMKOMM'][0];
		$Lager = $rsLIK['LIK_QUELLE'][0];
		
		$Y_Wert=$Y_Wert+5;
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(40,6,"Lieferschein-Nr:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+40,$Y_Wert);
		$pdf->cell(40,6,$LSNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(40,6,"Kommissionierung:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+40,$Y_Wert);
		$pdf->cell(40,6,$Kommissionierung,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(40,6,"Lager:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+40,$Y_Wert);
		$pdf->cell(40,6,$Lager,0,0,'L',0);
				
		$Y_Wert=$Y_Wert+10;		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(15,12,"ATUNR",0,0,'L',1);		
		$pdf->line($LinkerRand,$Y_Wert,$LinkerRand,$Y_Wert+12);
		
		$pdf->setXY($LinkerRand+15,$Y_Wert);
		$pdf->cell(90,12,"Artikelbezeichnung",0,0,'L',1);
		$pdf->line($LinkerRand+15,$Y_Wert,$LinkerRand+15,$Y_Wert+12);
		
		$pdf->setXY($LinkerRand+105,$Y_Wert);
		$pdf->cell(22,12,"Scandatum",0,0,'L',1);
		$pdf->line($LinkerRand+105,$Y_Wert,$LinkerRand+105,$Y_Wert+12);
				
		$pdf->setXY($LinkerRand+127,$Y_Wert);
		$pdf->cell(15,6,"Menge",0,0,'L',1);				
		
		$pdf->setXY($LinkerRand+142,$Y_Wert);
		$pdf->cell(15,6,"Menge",0,0,'L',1);		
		
		$pdf->setXY($LinkerRand+157,$Y_Wert);
		$pdf->cell(15,6,"Ges.",0,0,'L',1);				
		
		$pdf->setXY($LinkerRand+172,$Y_Wert);
		$pdf->cell(15,6,"Ges.",0,0,'L',1);
				
		$Y_Wert=$Y_Wert+6;
		$pdf->setXY($LinkerRand+127,$Y_Wert);
		$pdf->cell(15,6,"Soll",0,0,'L',1);		
		$pdf->line($LinkerRand+127,$Y_Wert-6,$LinkerRand+127,$Y_Wert+6);
		
		$pdf->setXY($LinkerRand+142,$Y_Wert);
		$pdf->cell(15,6,"Scan",0,0,'L',1);		
		$pdf->line($LinkerRand+142,$Y_Wert-6,$LinkerRand+142,$Y_Wert+6);
		
		$pdf->setXY($LinkerRand+157,$Y_Wert);
		$pdf->cell(15,6,"Soll",0,0,'L',1);		
		$pdf->line($LinkerRand+157,$Y_Wert-6,$LinkerRand+157,$Y_Wert+6);
		
		$pdf->setXY($LinkerRand+172,$Y_Wert);
		$pdf->cell(15,6,"Ist",0,0,'L',1);				
		$pdf->line($LinkerRand+172,$Y_Wert-6,$LinkerRand+172,$Y_Wert+6);
		$pdf->line($LinkerRand+187,$Y_Wert-6,$LinkerRand+187,$Y_Wert+6);
		$pdf->line($LinkerRand,$Y_Wert-6,$LinkerRand+187,$Y_Wert-6);
		
//		$pdf->setXY($LinkerRand+120,$Y_Wert);
//		$pdf->cell(30,6,"Korrekturmenge",1,0,'L',1);		
				
		
		for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
		{
			if ($Y_Wert > 270)
			{
				$Y_Wert=16;
				$Y_Wert=$Y_Wert+10;
				$Seite = $Seite + 1;
				
				NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$FILIALE,$Seite);
				
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(15,12,"ATUNR",0,0,'L',1);		
				$pdf->line($LinkerRand,$Y_Wert,$LinkerRand,$Y_Wert+12);
				
				$pdf->setXY($LinkerRand+15,$Y_Wert);
				$pdf->cell(90,12,"Artikelbezeichnung",0,0,'L',1);
				$pdf->line($LinkerRand+15,$Y_Wert,$LinkerRand+15,$Y_Wert+12);
				
				$pdf->setXY($LinkerRand+105,$Y_Wert);
				$pdf->cell(22,12,"Scandatum",0,0,'L',1);
				$pdf->line($LinkerRand+105,$Y_Wert,$LinkerRand+105,$Y_Wert+12);
						
				$pdf->setXY($LinkerRand+127,$Y_Wert);
				$pdf->cell(15,6,"Menge",0,0,'L',1);				
				
				$pdf->setXY($LinkerRand+142,$Y_Wert);
				$pdf->cell(15,6,"Menge",0,0,'L',1);		
				
				$pdf->setXY($LinkerRand+157,$Y_Wert);
				$pdf->cell(15,6,"Ges.",0,0,'L',1);				
				
				$pdf->setXY($LinkerRand+172,$Y_Wert);
				$pdf->cell(15,6,"Ges.",0,0,'L',1);
						
				$Y_Wert=$Y_Wert+6;
				$pdf->setXY($LinkerRand+127,$Y_Wert);
				$pdf->cell(15,6,"Soll",0,0,'L',1);		
				$pdf->line($LinkerRand+127,$Y_Wert-6,$LinkerRand+127,$Y_Wert+6);
				
				$pdf->setXY($LinkerRand+142,$Y_Wert);
				$pdf->cell(15,6,"Scan",0,0,'L',1);		
				$pdf->line($LinkerRand+142,$Y_Wert-6,$LinkerRand+142,$Y_Wert+6);
				
				$pdf->setXY($LinkerRand+157,$Y_Wert);
				$pdf->cell(15,6,"Soll",0,0,'L',1);		
				$pdf->line($LinkerRand+157,$Y_Wert-6,$LinkerRand+157,$Y_Wert+6);
				
				$pdf->setXY($LinkerRand+172,$Y_Wert);
				$pdf->cell(15,6,"Ist",0,0,'L',1);				
				$pdf->line($LinkerRand+172,$Y_Wert-6,$LinkerRand+172,$Y_Wert+6);
				$pdf->line($LinkerRand+187,$Y_Wert-6,$LinkerRand+187,$Y_Wert+6);
				$pdf->line($LinkerRand,$Y_Wert-6,$LinkerRand+187,$Y_Wert-6);								
			}
			
			$ATUNR=$rsLIK['LIK_AST_ATUNR'][$LIKZeile];
			$SCANDATUM=$rsLIK['LIK_DATUMSCAN'][$LIKZeile];
			$SOLLMENGE=$rsLIK['LIK_MENGESOLL'][$LIKZeile];
			$SCANMENGE=$rsLIK['LIK_MENGEIST'][$LIKZeile];
			$SOLLBESTAND=$rsLIK['LIK_SOLLBESTAND'][$LIKZeile];
			$ISTBESTAND=$rsLIK['LIK_ISTBESTAND'][$LIKZeile];
			$BEZEICHNUNG=$rsLIK['AST_BEZEICHNUNGWW'][$LIKZeile];
			//$KORREKTURMENGE=$rsLIK['LIK_MENGEKORREKTUR'][$LIKZeile];
			
			$Y_Wert=$Y_Wert+6;
			$pdf->SetFont('Arial','',9);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(15,6,$ATUNR,1,0,'L',0);	
			
			$pdf->setXY($LinkerRand+15,$Y_Wert);
			$pdf->cell(90,6,$BEZEICHNUNG,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+105,$Y_Wert);
			$pdf->cell(22,6,$SCANDATUM,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+127,$Y_Wert);
			$pdf->cell(15,6,$SOLLMENGE,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+142,$Y_Wert);
			$pdf->cell(15,6,$SCANMENGE,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+157,$Y_Wert);
			$pdf->cell(15,6,$SOLLBESTAND,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+172,$Y_Wert);
			$pdf->cell(15,6,$ISTBESTAND,1,0,'L',0);
			
//			$pdf->setXY($LinkerRand+120,$Y_Wert);
//			$pdf->cell(30,6,$KORREKTURMENGE,1,0,'L',0);
		}		
	}	
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
	echo "<hr><a href=./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Lieferkontrolle><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Thomas Riedl
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool �berschrift
* @param string Datum
* @param int Seite
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$Ueberschrift,$FILIALE,$Seite)
{
	static $Seite;
	
	$pdf->addpage();									// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+160,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->Cell(180,3,'Seite '. $Seite,0,0,'C',0);
	//$pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
		
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"Differenzliste von Filiale: ".$FILIALE,0,0,'C',0);
	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	$Zeile = 22;
}	
	
?>
</body>
</html>
