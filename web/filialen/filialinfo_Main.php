<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN2152">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?
/**
 * filialinfo_Main.php
 *
 * Zentrale Seite fuer die Filiolinformationen
 *
 * @author Sacha Kerres
 * @version 20080212
 * @package FILIALEN
 * @uses register.inc.php
 * @uses db.inc.php
 * @uses sicherheit.inc.php
 * @uses awis_forms.inc.php
 *
 */
require_once("register.inc.php");
require_once("db.inc.php"); // DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
global $AWISBenutzer;
global $AWISSprache;
global $SpeichernButton;

$con = awislogon();

// Sprung zur neuen Seite, falls das Recht vorhanden ist
// SK, 28.10.2009
$RechtNeu = awisBenutzerRecht($con,5000);
if($RechtNeu>0 AND ($RechtNeu&256)==0)			// dekativiert, damit man was testen kann
{
	$QS =$_SERVER['QUERY_STRING'];
	$QS = str_replace('cmdAktion=Filialinfos','cmdAktion=Details',$QS);
	header('Location: https://'.$_SERVER['HTTP_HOST'].'/filialinfos/filialinfos_Main.php?'.$QS);
}

if(isset($_GET['Sprache']))
{
	$AWISSprache=$_GET['Sprache'];
}
else
{
	$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Filialen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo "<title>Awis - ".$AWISSprachKonserven['TITEL']['tit_Filialen']."</title>";
echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) . ">";
echo "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";
?>
</head>

<body>
<?
include ("ATU_Header.php"); // Kopfzeile

$con = awislogon();

if(awisBenutzerRecht($con,100)==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     awisFORM_Meldung(1,$AWISSprachKonserven['Fehler']['err_keineRechte']);
     die();
}

if($con == FALSE)
{
	awisFORM_Meldung(1,$AWISSprachKonserven['Fehler']['err_keineDatenbank']);
	die();
}

if(isset($_POST['FILID']))
{
	$FILID=$_POST['FILID'];
}
elseif(isset($_POST['FIL_ID']))
{
	$FILID=$_POST['FIL_ID'];
}
else
{
	$FILID=0;
}

/**
 * Daten speichern
 */

if(isset($_POST["cmdSpeichern_x"]))
{ // Nur wenn Speichernknopf aktiv ist

	if((awisBenutzerRecht($con, 110) & 2) == 2)
	{ // Speichern Filialpreise

		if(isset($_POST['txtStundenSatz_old']) AND $_POST['txtStundenSatz_old'] != $_POST['txtStundenSatz'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,200,0" . abs(str_replace(",", ".", $_POST["txtStundenSatz"])) . ",'" . $AWISBenutzer->BenutzerName() . "'); END;";
    	    awisExecute($con, $SQL);
		}
    }

	$DatenBearbeitung = awisBenutzerRecht($con, 118);

    // Speicherung f�r regionalen Reifenrabatt hinzugef�gt 21.09.2007 Wacker (IBM)

    if(($DatenBearbeitung & 512)==512)  //Regionaler Reifenrabatt
	{ // Speichern Regionaler Reifenrabatt

		if(isset($_POST['txtReifenrabatt_old']) AND $_POST['txtReifenrabatt_old'] != $_POST['txtReifenrabatt'])
		{
	        //$SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,204,0" . abs(str_replace(",", ".", $_POST["txtReifenrabatt"])) . ",'" . $AWISBenutzer->BenutzerName() . "'); END;";
    	    $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,204,'" . $_POST["txtReifenrabatt"] . "','" . $AWISBenutzer->BenutzerName() . "'); END;";
	        awisExecute($con, $SQL);
		}
    }

	if(($DatenBearbeitung & 1)==1)		// TÜV-Termine
	{
		if(isset($_POST['txtTUEVMontag_old']) AND $_POST['txtTUEVMontag_old'] != $_POST['txtTUEVMontag'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,41,'" . $_POST["txtTUEVMontag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTUEVDienstag_old']) AND $_POST['txtTUEVDienstag_old'] != $_POST['txtTUEVDienstag'])
		{
		    $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,42,'" . $_POST["txtTUEVDienstag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
        	awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTUEVMittwoch_old']) AND $_POST['txtTUEVMittwoch_old'] != $_POST['txtTUEVMittwoch'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,43,'" . $_POST["txtTUEVMittwoch"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
	        awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTUEVDonnerstag_old']) AND $_POST['txtTUEVDonnerstag_old'] != $_POST['txtTUEVDonnerstag'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,44,'" . $_POST["txtTUEVDonnerstag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
	        awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTUEVFreitag_old']) AND $_POST['txtTUEVFreitag_old'] != $_POST['txtTUEVFreitag'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,45,'" . $_POST["txtTUEVFreitag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
	        awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTUEVSamstag_old']) AND $_POST['txtTUEVSamstag_old'] != $_POST['txtTUEVSamstag'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,46,'" . $_POST["txtTUEVSamstag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
	        awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 2)==2)		// Wegbeschreibung
	{
		if(isset($_POST["txtWegBeschreibung"]) AND $_POST["txtWegBeschreibung"] <> $_POST["txtWegBeschreibung_old"])
		{
        	$SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,47,'" . $_POST["txtWegBeschreibung"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
			awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 2)==2)		// Wegbeschreibung
	{
		if(isset($_POST["txtAllgInfos"]) AND $_POST["txtAllgInfos"] <> $_POST["txtAllgInfos_old"])
		{
        	$SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,800,'" . $_POST["txtAllgInfos"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
			awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 4)==4)		// Anlieferungstag
	{
		if(isset($_POST['txtAnlieferungstag_old']) AND $_POST['txtAnlieferungstag_old'] != $_POST['txtAnlieferungstag'])

		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,49,'" . $_POST["txtAnlieferungstag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 8)==8)		// Kommissioniertag
	{
		if(isset($_POST['txtKommissioniertag_old']) AND $_POST['txtKommissioniertag_old'] != $_POST['txtKommissioniertag'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,48,'" . $_POST["txtKommissioniertag"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 16)==16)		// Gebiet (ehem. Gebietsleiter) und TKDL
	{
		if(isset($_POST['txtGebLeiter_old']) AND $_POST['txtGebLeiter_old']!=$_POST['txtGebLeiter'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,21,'" . $_POST["txtGebLeiter"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
	        awisExecute($con, $SQL);
		}
		if(isset($_POST['txtTKDL_old']) AND $_POST['txtTKDL_old']!=$_POST['txtTKDL'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,28,'" . $_POST["txtTKDL"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);

		}
	}

	if(($DatenBearbeitung & 32)==32)		// Regionalzentrum
	{
		if(isset($_POST['txtVGK_ID_old']) AND intval($_POST['txtVGK_ID_old'])!=intval($_POST['txtVGK_ID']))		// wg. 001 und 1 !!!
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,26,'" . $_POST["txtVGK_ID"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);
		}
	}


	if(($DatenBearbeitung & 64)==64)		// AU/HU Aktionen
	{
		if(isset($_POST['txtAUHU_old']) AND $_POST['txtAUHU_old']!=$_POST['txtAUHU'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,203,'" . $_POST["txtAUHU"] . "','". $AWISBenutzer->BenutzerName() ."'); END;";
    	    awisExecute($con, $SQL);
		}
	}

	if(($DatenBearbeitung & 128)==128)		// Ladenkonzept 2011
	{
	    if(isset($_POST['txtLadenkonzept_old']) AND $_POST['txtLadenkonzept_old'] != $_POST['txtLadenkonzept'])
		{
	        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,36,'" . $_POST["txtLadenkonzept"] . "','" . $AWISBenutzer->BenutzerName() . "'); END;";
		    awisExecute($con, $SQL);
		}
	}

		//*************************************
		// TUEV-Bemerkungen
		//*************************************
	$FeldName = awis_NameInArray($_POST, "txtPRB_BEMERKUNG_");
	if($FeldName != '')
	{
		for($i=0;$i<99;$i++)
		{
			if(!isset($_POST["txtPRB_BEMERKUNG_$i"]))
			{
				break;
			}
			$FTUParm = explode("~", $_POST["txtPRA_ID_$i"]);

			$SQL = "SELECT * FROM PruefBemerkungen ";
			$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
			$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
			$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];

			$rsTest = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen > 0)
			{
				If($_POST["txtPRB_BEMERKUNG_$i"]=='')
				{
					$SQL = 'DELETE FROM PruefBemerkungen ';
					$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
					$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
					$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];
				}
				else
				{
					$SQL = "UPDATE PruefBemerkungen SET PRB_BEMERKUNG='" . $_POST["txtPRB_BEMERKUNG_$i"] . "'";
					$SQL .= ", PRB_USER = '" . $AWISBenutzer->BenutzerName() . "'";
					$SQL .= ", PRB_USERDAT = SYSDATE";
					$SQL .= " WHERE PRB_FIL_ID=0" . $FTUParm[0];
					$SQL .= " AND PRB_PRG_ID=0" . $FTUParm[1];
					$SQL .= " AND PRB_PRA_ID=0" . $FTUParm[2];
				}
			}
			elseIf($_POST["txtPRB_BEMERKUNG_$i"]!='')			// Neu hinzufügen
			{
				$SQL = "INSERT INTO PruefBemerkungen (PRB_FIL_ID, PRB_PRG_ID, PRB_PRA_ID, PRB_BEMERKUNG, PRB_USER, PRB_USERDAT)";
   				$SQL .= " VALUES(";
				$SQL .= " " . $FTUParm[0] . "";
				$SQL .= " ," . $FTUParm[1] . "";
				$SQL .= " ," . $FTUParm[2] . "";
				$SQL .= ", '" . $_POST["txtPRB_BEMERKUNG_$i"] . "'";
				$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
				$SQL .= ", SYSDATE)";
			}
			awisExecute($con, $SQL);
		}
	}
} // Ende Speichern


	// Auswahl löschen und leere Maske anzeigen
if(isset($_GET["Reset"]))
{
	awis_BenutzerParameterSpeichern($con, "FilialSuche", $AWISBenutzer->BenutzerName(), '');
    $cmdAktion = '';
}

/**
 * Daten anzeigen
 */

echo '<form name=frmFilialInfo method=post enctype="multipart/form-data" action=./filialinfo_Main.php';
$Param = '';
if(isset($_GET['cmdAktion']))
{
	$Param.='&cmdAktion='.$_GET['cmdAktion'];
}
if(isset($_GET['Seite'])) 		// Für die Unterregister
{
	$Param.='&Seite='.$_GET['Seite'];
}
if(isset($_GET['Unterseite'])) 		// Für die Unterregister
{
	$Param.='&Unterseite='.$_GET['Unterseite'];
}
if(isset($_GET['LFDNR'])) 		// Für die Unterregister
{
	$Param.='&LFDNR='.$_GET['LFDNR'];
}
//.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').'';

if(isset($_GET['Block']))		// Für Blockweise darstellung
{
	$Param.='&Block='.$_GET['Block'];
}
if($Param!='')
{
	echo '?'.substr($Param,1);
}
echo ">";

$SpeichernButton = False;
//awis_Debug(1,$_POST,$_GET);

$RegisterSeite = (isset($_GET['cmdAktion'])?$_GET['cmdAktion']:'');
if(isset($_POST['cmdSuche_x']))
{
	$RegisterSeite='Filialinfos';
}

awis_RegisterErstellen(1, $con, $RegisterSeite);


if(isset($_GET['Zurueck']))
{
		// Sonderzeichen werden ersetzt!
		// ~~1~~ = &

	echo "<br><hr><img title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='".str_replace('~~1~~','&',$_GET['Zurueck'])."';>";
}
else
{
	echo "<br><hr><img title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
}

if($SpeichernButton)
{
     print " <input type=image accesskey=S title='".$AWISSprachKonserven['Wort']['lbl_speichern']."' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./filialinfo_Main.php?" . $_SERVER['QUERY_STRING'] . "&Speichern=True'>";
}

echo "&nbsp;<input type=image title='".$AWISSprachKonserven['Wort']['lbl_hilfe']."' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=filialen&HilfeBereich=" . $cmdAktion . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

echo "</form>";

// include "debug_info.php";
awislogoff($con);
?>
</body>
</html>