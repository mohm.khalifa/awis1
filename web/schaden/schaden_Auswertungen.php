<?php
require_once 'awisDatenbank.inc';
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;


//ini_set('max_execution_time','120');

$con = awislogon();
$DBServer = awisDBServer();
$conSchad = awisAdminLogon($DBServer, "schaddev09", "schad");
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con, 3301);
if($RechteStufe==0)
{
   awisEreignis(3,3301,'Beschwerdeverwaltung Auswertungen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(!isset($_POST['txtFIL_ID']))
{
	echo '<form name=frmAuswertungen method=post action=./schaden_Main.php?cmdAktion=Auswertungen>';
	
	//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
	$Fil_User=substr($AWISBenutzer->BenutzerName(),0,4);
	$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
	$Fil_Nr=0;
	
	// Beschr�nkung auf eine Filiale?
	if (strtoupper($Fil_User) == 'FIL-')
	{
		$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);
	}
	elseif ($UserFilialen != '' or $UserFilialen != '0')
	{
		$Fil_Nr=$UserFilialen;
	}
	
	echo '<table border=0 width=300>';
	/******* Filiale *******/	
	echo '<tr>';
	echo '<td>Filiale</td>';
	if ($Fil_Nr != 0)
	{
		echo '<td>'.$Fil_Nr .'</td>';		
		echo '<td><input type=hidden name=txtFIL_ID value=' .intval($Fil_Nr) .'></td>';
	}
	else 
	{
		echo '<td><input name=txtFIL_ID size=5 tabindex=1 value=""></td>';
	}
	echo '</tr>';
	
	/******* Datum *******/

	echo '<tr>';
	echo '<td>Datum vom</td>';
	echo '<td><input name=txtDatumVom size=9 tabindex=2 value=' . date('01.01.Y') . '>';
	echo '</tr><tr><td>bis<td><input name=txtDatumBis size=9 tabindex=3 value=' . date('31.12.Y') . '></td>';
	echo '</tr>';	
	echo '</table>';
	
	echo '<hr>'	;	
	
	/************* Auswertungen *************/
	/*
	echo '<tr>';
	echo '<td>Auswertung</td><td>';
	echo '<select name=txtListe tabindex=4>';
	
	if(($RechteStufe&1)==1)
	{
		echo '<option value=1>Kompakt</option>';
	}
	
	if(($RechteStufe&2)==2)
	{
		echo '<option value=2>Detail</option>';
	}
	
	echo '</select></td></tr>';	
	*/	

	echo "<br>&nbsp;<input tabindex=5 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

	echo '</form>';
}
else			// Listen anzeigen
{
	if(!isset($_POST['txtFIL_ID']) or $_POST['txtFIL_ID']=='')
	{		
		die("<center><span class=HinweisText>Es wurde keine Filiale angegeben!</span></center>");		
	}
	
	/*
	if($_POST['txtListe']=='1')
	{
		include "./schaden_Auswertungen_Kompakt.php";	
	}	
	
	elseif($_POST['txtListe']=='2')
	{
		include "./schaden_Auswertungen_Detail.php";	
	}	
	*/
	
	
	include "./schaden_Auswertungen_Detail.php";	
	
	$Werkzeuge = new awisWerkzeuge();
	
//	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	//{
	//	$DateiPfad = '/daten/webdaten/export/schaden/';
	//}
	//else
//	{
		$DateiPfad = '/daten/web/export/schaden/';
	//}
	
	$DateiName = 'schaden'. time().'.pdf';
	//$DateiName = time().'.pdf';
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/schaden/' . $DateiNameLink['basename'];
	//$Ausdruck->_pdf->saveas($DateiName);
	
	
	
	//L�sche alte Dateien raus.
	if ($dh = opendir($DateiPfad)) {
		while (($file = readdir($dh)) !== false) {
			//Standardordner ignorieren und nur Dateien die �lter als einen Tag sind l�schen
			$DateiDatum = filemtime($DateiPfad . $file);
			//var_dump (date('d.m.Y H:i:s',filemtime($DateiPfad . $file)));
			//var_dump (date('d.m.Y H:i:s',time()-86400));
			if (($file!="." AND $file !=".." )AND ( $DateiDatum <= time()- 86400))
			{
				//Files vom Server entfernen
				unlink("".$DateiPfad."".$file."");
			}
		}
		//ge�ffnetes Verzeichnis wieder schlie�en
		closedir($dh);
	}
	
	
	$Ausdruck->_pdf->Output($DateiPfad.$DateiName,'F');
	
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";
	
//	var_dump($_POST);
	
	
	echo "<br><br><a href=./schaden_Main.php?cmdAktion=Auswertungen>Neue Auswertung</a>";

}

awislogoff($conSchad);
awislogoff($con);
?>