<?php
//**************************************************************
// Liefert die Daten f�r den Serienbrief des CRM
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['CAD_KEY']))
{
	die('##PARAM##');
}

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

if($_GET['Info']=='Rahmen')
{
	$SQL = "SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_ORT, CAP_ANREDE, CAP_NACHNAME, CAP_VORNAME ";
	$SQL .= ' , CAD_KEY';
	$SQL .= " FROM CRMAdressen ";
	$SQL .= ' INNER JOIN CRMAKQUISESTAND ON CAD_KEY = CAQ_CAD_KEY';
	$SQL .= ' LEFT OUTER JOIN CRMANSPRECHPARTNER ON CAQ_CAP_KEY = CAP_KEY ';
	$SQL .= ' WHERE CAD_KEY = 0'.$DB->FeldInhaltFormat('N0',$_GET['CAD_KEY'],false);
}
elseif($_GET['Info']=='')
{
	$SQL = "";
	$SQL .= " FROM";
	$SQL .= ' WHERE ';
	$SQL .= ' ORDER BY ';
}

$rsFEB=$DB->RecordSetOeffnen($SQL);
ob_clean();
while(!$rsFEB->EOF())
{
	for($i=1;$i<$rsFEB->AnzahlSpalten();$i++)
	{
		echo $rsFEB->FeldInfo($i,'Name');
		echo '=';
		echo $rsFEB->FeldInhalt($i);

		echo "~";
	}

	$rsFEB->DSWeiter();
}
?>