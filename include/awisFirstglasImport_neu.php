<?php
require_once ('awisDatenbank.inc');
require_once ('awisFirstglasFunktionen.php');
require_once ('awisBenutzer.inc');

ini_set ( 'max_execution_time', 0 );

date_default_timezone_set ( 'Europe/Berlin' );
// setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'deu_deu');

error_reporting ( "E_ALL" );

class FirstGlasImport_neu
{
	private static $instance = NULL;
	private $_Funktionen;
	private $_DB;
	protected $_AWISBenutzer;
	protected $pfadAuftrag = '/daten/daten/versicherungen/firstglas/Auftrag/'; // OK
	protected $pfadKassendaten = '/daten/daten/versicherungen/firstglas/Kasse/'; // OK
	protected $pfadGlasKopf = '/daten/daten/pccommon/glas/stamm/';
	protected $pfadGlasStamm = '/daten/daten/pccommon/glas/stamm/';
	protected $aktAuftragsdateien = array ();
	private $ber_Kopfsatz = array ();
	protected $ber_Positionssatz = array ();
	protected $ber_Stornosatz = array ();
	private $unb_Kopfsatz = array ();
	private $unb_Positionssatz = array ();
	private $unb_Stornosatz = array ();
	private $ber_temp_Positionssatz = array ();
	private $temp_fehlerPositionssatz = array ();
	protected $fehlerPositionssatz = array ();
	
	protected $letzteAuftragsdatei = '';
	protected $aktDatumAuftragsdatei;
	protected $aktDatumKassendatei;
	protected $differenzTage;
	protected $differenzTageKassendaten;
	protected $letzteKassendatenDatei = '';
	private $fikFiles = array (); // Fiktive Auftragsdateinamen
	private $fikFilesKassendaten = array ();
	
	// Pruefe Duplikate wird nur gesetzt falls in der Tabelle "FGDUPLIKATE" EINTRAEGE VORHANDEN SIND
	// ->wird auf FileName bezogen Beispiel: 20090707.txt --> Auftragsdatei!!
	protected $pruefeDuplikate = false;
	protected $aktDateiBereitImport;
	protected $pruefen;
	protected $aktZeile;
	protected $aktZeilennummer;
	
	protected $aktKopfnummer;
	protected $aktPosnummer;
	protected $aktStornonummer;
	
	protected $posZeilen;
	private $_VorgangNr;
	protected $XDI_KEY = '';
	protected $countDuplikate = 0;
	protected $countDuplikatsEintrag = 0;
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');
	private $Werkzeug;
	public function __construct()
	{
		
		$this->_AWISBenutzer = awisBenutzer::Init ();
		$this->_DB = awisDatenbank::NeueVerbindung ( 'AWIS' );
		$this->_DB->Oeffnen ();
		$this->_Funktionen = new FirstglasFunktionen ();
		$this->pruefen = new FirstglasFunktionen ();
		$this->Werkzeug = new awisWerkzeuge();

	}
	
	public function getInstance()
	{
		if (self::$instance === NULL)
		{
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function __clone()
	{
	}
	
	public function setpfadAuftrag($pfadAuftrag)
	{
		$this->pfadAuftrag = '';
		$this->pfadAuftrag = $pfadAuftrag;
	}
	
	// Funktion zum Setzen des ImportKeys
	
	public function setXDIKEY($XDIKEY)
	{
		$this->XDI_KEY = '';
		$this->XDI_KEY = $XDIKEY;
	}
	
	public function getXDIKEY()
	{
		return $this->XDI_KEY;
	}
	
	public function getpfadAuftrag()
	{
		return $this->pfadAuftrag;
	}
	
	public function starteImport($JobID)
	{
		$this->_Funktionen->SchreibeStartZeit ( "runImport" );
		
 		$this->ImportGlasKopf ();
 		$this->ImportGlasStamm ();
		
 		//auskommentiert,da alles in eine Funktion zusammengefasst
		//$this->aktuelleAuftragsDateien ();
		//$this->letzteImportierteAuftragsdatei ();
		
		$this->ImportAuftragsdateien();
//		die("Verarbeitung abgebrochen - nur Auftragsdaten");

		//$this->aktuelleKassendaten ();
		$this->safeKassendaten();
		$this->speichereKassendatenStorno ();
		
		$this->_Funktionen->SchreibeEndeZeit ( 'runImport', $JobID );
	}
	/**
	 * Schreibt die Daten der Datei glas_kopf.res (0000010051207)
	 * in die Tabelle FGINZUGANG
	 */
	public function ImportGlasKopf()
	{
		echo 'Start Funktion: ImportGlasKopf'.PHP_EOL;
		
		$this->_Funktionen->SchreibeModulStatus ( "runImport", "Start IMPORT GLASKOPF" );
	
		$SQL = "TRUNCATE TABLE FGINZUGANG";
	
		$this->_DB->Ausfuehren($SQL, '', true);
	
		// Liest die Datei in ein Array
		$daten = file ( $this->pfadGlasKopf . 'glas_kopf.res' );
		$MaxDaten = count ( $daten );
		$Blockgroesse = 420;
	
		for($i = 0; $i <= $MaxDaten; $i += $Blockgroesse)
		{
			// schneidet Anzahl Elemente ($Blockgroesse) aus und fuegt es in ein anderes Array ($ar1) ein
			$ar1 = array_slice ( $daten, $i, $Blockgroesse );
				
			$Vorspann = '\',\'' . $this->_AWISBenutzer->BenutzerName () . '\',SYSDATE FROM DUAL UNION SELECT \'';
			// verbindet Array ($ar1, bsp: 0000010051207) mit $Vorspann
			$Daten = implode ( $Vorspann, $ar1 );
			// entfernt Zeilenumbrueche fuer INSERT
			$Daten = str_replace ( " \r\n", '', $Daten );
				
			$SQL = "INSERT INTO FGINZUGANG (FGI_VORGANGNR,FGI_USER,FGI_USERDAT)";
			$SQL .= " SELECT '";
			$SQL .= $Daten;
			$SQL .= '\',\'' . $this->_AWISBenutzer->BenutzerName () . '\',SYSDATE FROM DUAL';
				
			$this->_DB->Ausfuehren($SQL, '', true);
	
		}
	
		$this->_Funktionen->SchreibeModulStatus ( "runImport", "ENDE IMPORT GLASKOPF" );
		
		echo 'Ende Funktion: ImportGlasKopf'.PHP_EOL;
	}
	/**
	 * Schreibt die Daten der Datei glas_stamm.res (7A0F7Z)
	 * in die Tabelle FGLASSTAMM
	 * -> inhaltlich identisch mit ImportGlasKopf()
	 */
	public function ImportGlasStamm()
	{
		echo 'Start Funktion: ImportGlasStamm'.PHP_EOL;
		$this->_Funktionen->SchreibeModulStatus ( "runImport", "Start IMPORT GLASSTAMM" );
	
		$SQL = "TRUNCATE TABLE FGLASSTAMM";
	
		$this->_DB->Ausfuehren($SQL, '', true);
	
		$daten = file ( $this->pfadGlasStamm . 'glas_stamm.res' );
	
		$MaxDaten = count ( $daten );
		$Blockgroesse = 420;
		for($i = 0; $i < $MaxDaten; $i += $Blockgroesse)
		{
			$ar1 = array_slice ( $daten, $i, $Blockgroesse );
				
			$Vorspann = '\',\'' . $this->_AWISBenutzer->BenutzerName () . '\',SYSDATE FROM DUAL UNION SELECT \'';
			$Daten = implode ( $Vorspann, $ar1 );
			$Daten = str_replace ( " \r\n", '', $Daten );
				
			$SQL = "INSERT INTO FGLASSTAMM (FGL_ARTNR,FGL_USER,FGL_USERDAT)";
			$SQL .= " SELECT '";
			$SQL .= $Daten;
			$SQL .= '\',\'' . $this->_AWISBenutzer->BenutzerName () . '\',SYSDATE FROM DUAL';
				
			$this->_DB->Ausfuehren($SQL, '', true);
	
		}
	
		$this->_Funktionen->SchreibeModulStatus ( "runImport", "ENDE IMPORT GLASSTAMM" );
		echo 'Ende Funktion: ImportGlasStamm'.PHP_EOL;
	}

	public function ImportAuftragsdateien()
	{
		$DateiName = '';
	
		$SQL = "Select * from IMPORTPROTOKOLL WHERE XDI_BEREICH='FGA' ORDER BY XDI_KEY DESC";
		$rsZuletztImportiert = $this->_DB->RecordSetOeffnen ( $SQL );
	
		$DateiName = $rsZuletztImportiert->FeldInhalt ( 'XDI_DATEINAME' );
	
		$pos = strrpos ( $rsZuletztImportiert->FeldInhalt ( 'XDI_DATEINAME' ), '/' );
		
		//letzteAuftragsdatei = die zuletzt importierte Datei (->aus Importprotokoll)
		$this->letzteAuftragsdatei = substr ( $DateiName, $pos + 1, 8 );
				
		
		$checkFile = '';
		$element = 0;
	
		//liest das Verzeichnis aus, in dem die aktuellen Auftragsdateien liegen
		if ($handle = opendir ( $this->pfadAuftrag ))
		{
			while ( false !== ($file = readdir ( $handle )) )
			{
				if(is_file( $this->pfadAuftrag.$file))
				{
					$checkFile = '';
					$checkFile = substr ( $file, 0, 3 );
						
					if ($file != '.' || $file != '..')
					{
						if ($checkFile != 'imp' and $checkFile != 'err')
						{
							$Dat = str_replace('.txt', '', $file);
							
							//errechnet die Tage zwischen der vorhandenen Auftragsdatei und der zuletzt importierten Datei
							$pruef_Dat = (strtotime ( $this->letzteAuftragsdatei) - strtotime($Dat))/86400;

							//prueft ob die Datei aelter ist, als die zuletzt importierte Datei
							if($pruef_Dat > 0)
							{
								$Err = 'Die Datei unter: ' . $this->pfadAuftrag.$file . ' ist aelter als die zuletzt importierte Datei ('.$this->letzteAuftragsdatei.')';
																
								$this->Werkzeug->EMail($this->_EMAIL_Infos,'ERROR - AUTOGLASIMPORT',$Err,2,'','shuttle@de.atu.eu');
																
								rename($this->pfadAuftrag.$file, $this->pfadAuftrag.'/Fehler/err_'.$file);
								//ToDo: verschiebe in Errorverzeichnis
								
							}
							else {
								
								echo $Dat . ' kann importiert werden'.PHP_EOL;
								$this->aktAuftragsdateien [$element] = $file;
								$element ++;
							}
						}
					}
				}
		
			}
		}
		date_default_timezone_set ( 'UTC' );
		
		$this->aktDatumAuftragsdatei = date ( 'Ymd' );
		
		//errechnet die Differenztage zwischen dem heutigen Datum und der zuletzt importierten Datei (aus Importprotokoll)
		$this->differenzTage = strtotime ( $this->aktDatumAuftragsdatei ) - strtotime ( $this->letzteAuftragsdatei );
		$this->differenzTage = $this->differenzTage / 86400;
		$this->differenzTage = round ( $this->differenzTage - 1);
		
		
		//durchlaeuft die Differenztage in einer Schleife und schreibt alle Tage
		//in Form der Auftragsdateien in das Array "fikFiles"
		//z. B. letzteAuftragsdatei: 20160328
		//      heutiges Datum:      20160401
		//	    Differenz:  3 --> 4 Tage - 1 Tag (weil Auftragsdateien immer einen Tag versetzt kommen)		
		
		
		for($element = 0; $element < $this->differenzTage; $element ++)
		{
			$this->letzteAuftragsdatei = strtotime ( $this->letzteAuftragsdatei ) + 86400;
			$this->letzteAuftragsdatei = date ( 'Ymd', $this->letzteAuftragsdatei );
			
			//var_dump($this->letzteAuftragsdatei);
			//im Array ist dann folgendes enthalten:
			//string(8) "20160329" string(8) "20160330" string(8) "20160331"
			
			$this->fikFiles [$element] = $this->letzteAuftragsdatei;
		}
		
		
		
		for($zaehler = 0; $zaehler < count ( $this->fikFiles ); $zaehler ++)
		{				
			unset ( $this->ber_Kopfsatz );
			unset ( $this->ber_Positionssatz );
			unset ( $this->ber_Stornosatz );
			unset ( $this->ber_temp_Positionssatz );
			unset ( $this->fehlerPositionssatz );
			unset ( $this->temp_fehlerPositionssatz );
			unset ( $this->unb_Kopfsatz );
			unset ( $this->unb_Positionssatz );
			unset ( $this->unb_Stornosatz );
			
			for($element = 0; $element < count ( $this->aktAuftragsdateien ); $element ++)
			{
				//prueft ob alle Dateien fortlaufend sind bzw ob die Dateien im Verzeichnis vorhanden sind
				//wenn nicht wird eine WARNING-Mail an Shuttle geschrieben
				if(file_exists($this->pfadAuftrag.$this->fikFiles[$zaehler].'.txt') or file_exists($this->pfadAuftrag.'imp_'.$this->fikFiles[$zaehler].'.txt') or file_exists($this->pfadAuftrag.'err_'.$this->fikFiles[$zaehler].'.txt'))
				{
					unset ( $this->ber_Kopfsatz );
					unset ( $this->ber_Positionssatz );
					unset ( $this->ber_Stornosatz );
					unset ( $this->ber_temp_Positionssatz );
					unset ( $this->fehlerPositionssatz );
					unset ( $this->temp_fehlerPositionssatz );
					unset ( $this->unb_Kopfsatz );
					unset ( $this->unb_Positionssatz );
					unset ( $this->unb_Stornosatz );
					
					$this->ber_Kopfsatz = array ();
					$this->ber_Positionssatz = array ();
					$this->ber_Stornosatz = array ();
					$this->ber_temp_Positionssatz = array ();
					$this->fehlerPositionssatz = array ();
					$this->temp_fehlerPositionssatz = array ();
					$this->unb_Kopfsatz = array ();
					$this->unb_Positionssatz = array ();
					$this->unb_Stornosatz = array ();
					
					$this->aktDateiBereitImport = '';
					$cutExtension = '';
					$cutExtension = substr ( $this->aktAuftragsdateien [$element], 0, 8 );
					
					if ($cutExtension == $this->fikFiles [$zaehler])
					{
						$this->aktDateiBereitImport = $this->aktAuftragsdateien [$element];
						
						$BindeVariablen = array ();
						$BindeVariablen ['var_T_fdp_dateiname'] = $this->aktDateiBereitImport;
						
						$SQL = 'Select * from FGDUPLIKATE WHERE FDP_DATEINAME=:var_T_fdp_dateiname';
						
						$rsCheckDuplikate = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
						
						if ($rsCheckDuplikate->AnzahlDatensaetze () > 0)
						{
							$this->pruefeDuplikate = true;
						} else
						{
							// Wenn noch kein Eintrag in der Duplikatstabelle vorhanden ist!!!
							$this->pruefeDuplikate = false;
						}
						
						$BindeVariablen = array ();
						$BindeVariablen ['var_T_xdi_bereich'] = 'FGA';
						
						$SQL = "Select * from IMPORTPROTOKOLL WHERE XDI_BEREICH=:var_T_xdi_bereich ORDER BY XDI_KEY DESC";
						
						$rsCheck = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
						
						
						if ($rsCheck->FeldInhalt ( 'XDI_BEMERKUNG' ) != 0)
						{
							
							$SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
							$SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', 'FGA', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->pfadAuftrag . $this->aktDateiBereitImport, false );
							$SQL .= ',SYSDATE';
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '0', false );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';
																	
							$this->_DB->Ausfuehren($SQL, '', true);
							
							$BindeVariablen = array ();
							$BindeVariablen ['var_T_xdi_bereich'] = 'FGA';
							
							$SQL = "Select XDI_KEY,XDI_BEREICH,XDI_DATEINAME,XDI_DATUM ,XDI_MD5,XDI_BEMERKUNG,XDI_USER,XDI_USERDAT ";
							$SQL .= "FROM IMPORTPROTOKOLL WHERE XDI_BEREICH=:var_T_xdi_bereich AND XDI_DATUM = (Select MAX(XDI_DATUM) FROM IMPORTPROTOKOLL)";
							
							$rsXDI = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
							$this->setXDIKEY ( $rsXDI->FeldInhalt ( 'XDI_KEY' ) );
							
							$this->readInArray ();
							
							$SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
							$SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', 'FGA', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->pfadAuftrag . $this->aktDateiBereitImport, false );
							$SQL .= ',SYSDATE';
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '1', false );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL, '', true);
							
							// Wenn importiert benenne File um!!!
							rename ( $this->pfadAuftrag . $this->aktDateiBereitImport, $this->pfadAuftrag . "/Backup/"."imp_$this->aktDateiBereitImport" );
							//rename ( $this->pfadAuftrag . $this->aktDateiBereitImport, $this->pfadAuftrag . "imp_".$this->aktDateiBereitImport );
						} 
					}

				
				}
				else 
				{
					$warn = 'Folgende Datei fehlt in der Vearbeitung: '.$this->fikFiles [$zaehler];
					$this->Werkzeug->EMail($this->_EMAIL_Infos,'WARNING - AUTOGLASIMPORT',$warn,2,'','shuttle@de.atu.eu');
					
				}
				
				
			}
		}
		
	}
	
	private function readInArray()
	{	
		$this->aktKopfnummer = 0;
		$this->aktPosnummer = 0;
		$this->aktStornonummer = 0;
		
		$fd = null;
		
		if (($fd = fopen ( $this->pfadAuftrag . $this->aktDateiBereitImport, 'r' )) === false)
		{
			echo 'keine datei da' . $this->aktDateiBereitImport.PHP_EOL;
			die();
			// echo $this->error_msg = "Fehler beim oeffnen der Datei";
		} 
		else
		{
			$this->aktZeilennummer = 1;
			
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Start Import - Kopfdaten" );
			
			while ( ! feof ( $fd ) )
			{
				$this->aktZeile = fgets ( $fd );
				
				if (substr ( $this->aktZeile, 0, 1 ) == 'K')
				{
					$this->unb_Kopfsatz = explode ( ';', $this->aktZeile );
					
					$this->aktKopfnummer ++;
					$this->pruefeKopfArray ();
				}
			}
			// 
			rewind($fd);
			
			
			
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Ende Import - Kopfdaten" );
			
			$this->posZeilen = 0;
			
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Start Import - Positionsdaten" );
				
			$this->aktZeilennummer = 1;
			
			while ( ! feof ( $fd ) )
			{
				$this->aktZeile = fgets ( $fd );
				
				if (substr ( $this->aktZeile, 0, 1 ) == 'P')
				{
					
					$this->unb_Positionssatz [$this->posZeilen] = explode ( ';', $this->aktZeile );
					$this->posZeilen ++;
					$this->aktPosnummer ++;
				}
			
			}
			$this->pruefePosArray ();
			
			rewind($fd);
						
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Ende Import - Positionsdaten" );

			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Start Import - Stornodaten" );
			
			while ( ! feof ( $fd ) )
			{
				$this->aktZeile = fgets ( $fd );

				if (substr ( $this->aktZeile, 0, 1 ) == 'S')
				{					
					$this->unb_Stornosatz = explode ( ';', $this->aktZeile );
					
					$this->pruefeStornoArray ();
					
					$this->aktStornonummer ++;
				}
			}
			//}
			fclose ( $fd );
			//$fd = null;
			
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "Ende Import - Stornodaten" );
			
		}
			
	}
	
	private function pruefeKopfArray()
	{
		$fehler = false;
		$gewKFZKennz = '';
		$this->pruefen = new FirstglasFunktionen ();
		$gewKFZKennz = $this->pruefen->wandleKFZKennzeichen ( $this->unb_Kopfsatz [4] );
		$this->unb_Kopfsatz [4] = $gewKFZKennz;
		
		$pruefeVorgangsNummer = false;
		$pruefeVorgangsNummer = $this->pruefen->pruefeVorgangNr ( $this->unb_Kopfsatz [2] );
		
		if ($pruefeVorgangsNummer == false)
		{
			$fehler = true;
		} 
				
		// Checken SB - Kopfdaten
		$SB = ctype_digit ( $this->unb_Kopfsatz [9] );
		$SB = $this->pruefen->pruefeSB ( $this->unb_Kopfsatz [9] );
		$this->unb_Kopfsatz [9] = $SB;
		
		if ($fehler == false)
		{
			for($element = 0; $element < count ( $this->unb_Kopfsatz ); $element ++)
			{
				$this->ber_Kopfsatz [$element] = $this->unb_Kopfsatz [$element];
			}
			array_splice ( $this->unb_Kopfsatz, 0 );
			$this->speichereKopfsatz ();
		} 
		else
		{
			$ImportFile = $this->getXDIKEY ();
			
			$SQL = "INSERT INTO FGKOPFDATENFEHLER (FKF_KENNUNG,FKF_FILID,FKF_VORGANGNR,";
			$SQL .= "FKF_KFZBEZ,FKF_KFZKENNZ,FKF_KBANR,FKF_FAHRGESTELLNR,FKF_VERSICHERUNG,";
			$SQL .= "FKF_VERSSCHEINNR,FKF_SB,FKF_MONTAGEDATUM,FKF_ZEITSTEMPEL,FKF_STEUERSATZ,";
			$SQL .= "FKF_KUNDENNAME,FKF_IMP_KEY,FKF_USER,FKF_USERDAT) VALUES (";
			$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [0], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $this->unb_Kopfsatz [1], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [2], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [3], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [4], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [5], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [6], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [7], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [8], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->unb_Kopfsatz [9], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->unb_Kopfsatz [10], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->unb_Kopfsatz [11], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->unb_Kopfsatz [12], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->unb_Kopfsatz [13], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N', $ImportFile, false );
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			$this->_DB->Ausfuehren($SQL, '', true);
			
			array_splice ( $this->unb_Kopfsatz, 0 );
			
			$fehler = false;
		
		}
	}
	
	private function pruefeStornoArray()
	{
		$SB = ctype_digit ( $this->unb_Stornosatz [9] );
		$SB = $this->pruefen->pruefeSB ( $this->unb_Stornosatz [9] );
		$this->unb_Stornosatz [9] = $SB;
		echo $SB.PHP_EOL;

		for($element = 0; $element < count ( $this->unb_Stornosatz ); $element ++)
		{
			$this->ber_Stornosatz [$element] = $this->unb_Stornosatz [$element];
		}
		
		array_splice ( $this->unb_Stornosatz, 0 );
		$this->speichereStornosatz ();
		
	}
	
	private function pruefePosArray()
	{
		$Pruefe_Anzahl = false;
		$Pruef_Array_Anzahl = Array ();
		$Pruefe_Alle_Anzahl = false;
		$Pruef_Array_Oempreis = Array ();
		$Pruefe_Alle_Oempreis = false;
		$Pruef_Zaehler = 0;
		
		$pruef_Anzahl = false;
		$pruef_Oempreis = false;
		$Pruefe_Oempreis = false;
		
		$count = 0;
		$count_True = 0;
		$count_Fehler = 0;
		
		for($i = 0; $i < count ( $this->unb_Positionssatz ); $i ++)
		{
			unset ( $this->temp_fehlerPositionssatz );
			$this->temp_fehlerPositionssatz = array ();
			
			$Pruefe_Anzahl = $this->pruefen->checkNumber ( $this->unb_Positionssatz [$i] [4] );
			$Pruefe_Oempreis = $this->pruefen->checkNumber ( $this->unb_Positionssatz [$i] [6] );
			
			if ($Pruefe_Anzahl == true && $Pruefe_Oempreis == true)
			{
				$count_True ++;
			} 
			else
			{
				// Schreibe VorgangsNr in Fehlerarray mit Zeitstempel
				$count_Fehler ++;
				
				// Kopiere Fehlerhaften DS
				for($y = 0, $z = 0; $y < count ( $this->unb_Positionssatz [$i] ); $y ++, $z ++)
				{
					$this->temp_fehlerPositionssatz [$z] = $this->unb_Positionssatz [$i] [$y];
				}
				
					$r = count ( $this->fehlerPositionssatz );
					
					for($y = 0; $y < count ( $this->temp_fehlerPositionssatz ); $y ++)
					{
						$this->fehlerPositionssatz [$r] [$y] = $this->temp_fehlerPositionssatz [$y];
					}
			}
			$count ++;
		}
		
		$anz = 0;
		$anz = $this->getAnzahlFehlerhaftePositionsdaten ();
		
		if ($anz == 0)
		{
			for($i = 0; $i < count ( $this->unb_Positionssatz ); $i ++)
			{
				for($y = 0; $y < count ( $this->unb_Positionssatz [$i] ); $y ++)
				{
					$this->ber_Positionssatz [$i] [$y] = $this->unb_Positionssatz [$i] [$y];
				}
			}
		} 
		else
		{
			$this->bereinigePositionsdaten ();
		}
		$LAUFZEIT = time () - $STARTZEIT;
		$this->speicherePossatz ();
	}
	
	public function bereinigePositionsdaten()
	{
		unset ( $this->temp_fehlerPositionssatz );
		$this->temp_fehlerPositionssatz = array ();
				
		for($i = 0; $i < count ( $this->unb_Positionssatz ); $i ++)
		{
			$flag_Fehler = false;
			
			for($z = 0; $z < count ( $this->fehlerPositionssatz ); $z ++)
			{
				if ($this->unb_Positionssatz [$i] [1] == $this->fehlerPositionssatz [$z] [1] && $this->unb_Positionssatz [$i] [8] == $this->fehlerPositionssatz [$z] [8])
				{
					$flag_Fehler = true;
					
					$anzTempPositionssatz = count ( $this->temp_fehlerPositionssatz );
					
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [0] = $this->unb_Positionssatz [$i] [0];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [1] = $this->unb_Positionssatz [$i] [1];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [2] = $this->unb_Positionssatz [$i] [2];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [3] = $this->unb_Positionssatz [$i] [3];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [4] = $this->unb_Positionssatz [$i] [4];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [5] = $this->unb_Positionssatz [$i] [5];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [6] = $this->unb_Positionssatz [$i] [6];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [7] = $this->unb_Positionssatz [$i] [7];
					$this->temp_fehlerPositionssatz [$anzTempPositionssatz] [8] = $this->unb_Positionssatz [$i] [8];
					
					$i ++;
					$z --;
				
				} 
				else
				{
					if ($flag_Fehler != true)
					{
						$element = count ( $this->ber_Positionssatz );
						
						for($x = 0; $x < count ( $this->unb_Positionssatz [$i] ); $x ++)
						{
							$this->ber_Positionssatz [$element] [$x] = $this->unb_Positionssatz [$i] [$x];
						}
					
					}
				}
			
			}
		}
		
		array_splice ( $this->fehlerPositionssatz, 0 );
		
		for($i = 0; $i < count ( $this->temp_fehlerPositionssatz ); $i ++)
		{
			for($y = 0; $y < count ( $this->temp_fehlerPositionssatz [$i] ); $y ++)
			{
				$this->fehlerPositionssatz [$i] [$y] = $this->temp_fehlerPositionssatz [$i] [$y];
			}
		}
	}
	
	public function getAnzahlFehlerhaftePositionsdaten()
	{
		$anz = count ( $this->fehlerPositionssatz );
		
		return $anz;
	}
	
	private function speichereStornosatz()
	{
		$ImportFile = $this->getXDIKEY ();
		
		$BindeVariablen = array ();
		$BindeVariablen ['var_T_fgs_vorgangnr'] = $this->ber_Stornosatz [2];
		
		$SQL = "Select * from FGSTORNODATEN";
		$SQL .= " WHERE FGS_VORGANGNR=:var_T_fgs_vorgangnr";
		
		$rsStornoDaten = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
		if ($rsStornoDaten->FeldInhalt ( 'FGS_VORGANGNR' ) == '')
		{
			$SQL = "INSERT INTO FGSTORNODATEN (FGS_KENNUNG,FGS_FILIALNR,FGS_VORGANGNR,";
			$SQL .= "FGS_KFZBEZ,FGS_KFZKENNZ,FGS_KBANR,FGS_FAHRGESTELLNR,FGS_VERSICHERUNG,";
			$SQL .= "FGS_VERSSCHEINNR,FGS_SB,FGS_MONTAGEDATUM,FGS_ZEITSTEMPEL,FGS_STEUERSATZ,";
			$SQL .= "FGS_AKTIV,FGS_FGK_KEY,FGS_IMP_KEY,FGS_USER,";
			$SQL .= "FGS_USERDAT) VALUES (";
			$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [0], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Stornosatz [1], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [2], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [3], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [4], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [5], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [6], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [7], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [8], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Stornosatz [9], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Stornosatz [10], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Stornosatz [11], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Stornosatz [12], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 1, false );
			$SQL .= ',null';
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $ImportFile, false );
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			echo $SQL.PHP_EOL;
			
			$this->_DB->Ausfuehren($SQL, '', true);
			
			array_splice ( $this->ber_Stornosatz, 0 );
		} 
		else
		{
			$ImportFile = $this->getXDIKEY ();
			
			if ($rsStornoDaten->FeldInhalt ( 'FGS_VORGANGNR' ) == $this->ber_Stornosatz [2])
			{
				$wand_Datum = strtotime ( $this->ber_Stornosatz [11] );
				$wand_Datum = date ( 'd.m.Y H:i:s', $wand_Datum );
				
				if ($rsStornoDaten->FeldInhalt ( 'FGS_ZEITSTEMPEL' ) == $wand_Datum)
				{
				} 
				else
				{
					if ($rsStornoDaten->FeldInhalt ( 'FGS_ZEITSTEMPEL' ) <= $wand_Datum)
					{
						
						$SQLCHECKHISTORY = 'Select * from FGSTORNODATEN_HIST WHERE FSH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Stornosatz [11], false );
						
						echo $SQL.PHP_EOL;
						
						$rsCheck_History = $this->_DB->RecordSetOeffnen ( $SQLCHECKHISTORY );
						
						if ($rsCheck_History->AnzahlDatensaetze () == 0)
						{
							
							$BindeVariablen = array ();
							$BindeVariablen ['var_T_fgs_vorgangnr'] = $this->ber_Stornosatz [2];
							
							$SQL = 'INSERT INTO FGSTORNODATEN_HIST SELECT * FROM FGSTORNODATEN WHERE';
							$SQL .= ' FGS_VORGANGNR=:var_T_fgs_vorgangnr';

							$this->_DB->Ausfuehren($SQL, '', true, $BindeVariablen);
						}
						
						$BindeVariablen = array ();
						$BindeVariablen ['var_T_fgs_vorgangnr'] = $this->ber_Stornosatz [2];
						
						$SQL = "Select * from FGSTORNODATEN";
						$SQL .= " WHERE FGS_VORGANGNR=:var_T_fgs_vorgangnr";
						
						echo $SQL.PHP_EOL;
						
						$rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
						
						$SQL = 'UPDATE FGSTORNODATEN SET ';
						$SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [0], false );
						$SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Stornosatz [1], false );
						$SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [2], false );
						$SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [3], false );
						$SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [4], false );
						$SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [5], false );
						$SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [6], false );
						$SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [7], false );
						$SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [8], false );
						$SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Stornosatz [9], false );
						$SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Stornosatz [10], false );
						$SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Stornosatz [11], false );
						$SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Stornosatz [12], false );
						$SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat ( 'NO', 1, false );
						$SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopfDatenUpdate->FeldInhalt ( 'FGS_FGK_KEY' ), true );
						$SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $ImportFile, true );
						$SQL .= ',FGS_USER=\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
						$SQL .= ',FGS_USERDAT=sysdate';
						$SQL .= ' WHERE FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Stornosatz [2], false );
						
						echo $SQL.PHP_EOL;
						
						$this->_DB->Ausfuehren($SQL, '', true);
					
					}
				}
			}
		}
	}
	
	private function speichereKopfsatz()
	{
		$ImportFile = $this->getXDIKEY ();
		
		$BindeVariablen = array ();
		$BindeVariablen ['var_T_fgk_vorgangnr'] = $this->ber_Kopfsatz [2];
		
		$SQL = "Select * from FGKOPFDATEN";
		$SQL .= " WHERE FGK_VORGANGNR=:var_T_fgk_vorgangnr";

		$rsKopfDaten = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
			
		
		if ($rsKopfDaten->FeldInhalt ( 'FGK_VORGANGNR' ) == '')
		{
			$SQL = "INSERT INTO FGKOPFDATEN (FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
			$SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
			$SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
			$SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_FGN_KEY,FGK_FCN_KEY,FGK_USER,FGK_USERDAT,";
			$SQL .= "FGK_SCHADENNR,FGK_SB_RABATT,FGK_REGELWERK_KEY, FGK_VERSNR,";
			$SQL .= "FGK_SB_UNGEKUERZT,FGK_SB_RABATT_VERS,FGK_BETRAG_NETTO,FGK_MWST,FGK_BETRAG_BRUTTO,FGK_AW) VALUES (";
			$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [0], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Kopfsatz [1], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [3], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [4], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [5], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [6], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [7], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [8], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [9], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [10], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [11], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [12], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [13], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N0', $ImportFile, false );
			$SQL .= ',null';
			$SQL .= ',null';
			$SQL .= ',null';
			$SQL .= ',SYSDATE';
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [14], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [15], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [16], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N0', $this->ber_Kopfsatz [17], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [18], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [19], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [20], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [21], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [22], false );
            $SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N0', $this->ber_Kopfsatz [23], false );
			$SQL .= ')';
			
			$this->_DB->Ausfuehren($SQL, '', true);
			
			array_splice ( $this->ber_Kopfsatz, 0 );
		
		} 
		else
		{
			if ($rsKopfDaten->FeldInhalt ( 'FGK_VORGANGNR' ) == $this->ber_Kopfsatz [2])
			{
				
				$wand_Datum = strtotime ( $this->ber_Kopfsatz [11] );
				// $wand_Datum = date('d.m.Y H:i:s',$wand_Datum);
				
				if (strtotime ( $rsKopfDaten->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) ) == $wand_Datum)
				{
				} 
				else
				{
					if (strtotime ( $rsKopfDaten->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) ) < $wand_Datum)
					{
						
						$this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [11], false );
						$this->ber_Kopfsatz [2];
						
						$SQLCHECKHISTORY = 'Select * from FGKOPFDATEN_HIST WHERE FKH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [11], false );
						
						$rsCheck_History = $this->_DB->RecordSetOeffnen ( $SQLCHECKHISTORY );
						
						if ($rsCheck_History->AnzahlDatensaetze () == 0)
						{
							
							$BindeVariablen = array ();
							$BindeVariablen ['var_T_fgk_vorgangnr'] = $this->ber_Kopfsatz [2];
							
							$SQL = 'INSERT INTO FGKOPFDATEN_HIST SELECT * FROM FGKOPFDATEN WHERE ';
							$SQL .= 'FGK_VORGANGNR=:var_T_fgk_vorgangnr';
							
							$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
							
							$SQL = 'Select * from FGKOPFDATEN_HIST WHERE ';
							$SQL .= ' FKH_VORGANGNR= ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
							$SQL .= ' AND FKH_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKopfDaten->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) );
							
							$rsKey = $this->_DB->RecordSetOeffnen ( $SQL );
							
							$SQL = 'Select * from FGPOSITIONSDATEN WHERE';
							$SQL .= ' FGP_VORGANGNR= ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
							$SQL .= ' AND FGP_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKopfDaten->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) );
							
							$rsPosition = $this->_DB->RecordSetOeffnen ( $SQL );
							
							while ( ! $rsPosition->EOF () )
							{
								$SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
								$SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
								$SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT,FPH_DIFFORGPGLASMREGEL,FPH_DIFFORGPALLGREGEL,FPH_VERCLIENT,FPH_GLASMREGEL) VALUES(';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_KENNUNG' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_VORGANGNR' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_ARTNR' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_ARTBEZ' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsPosition->FeldInhalt ( 'FGP_ANZAHL' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_EINHEIT' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsPosition->FeldInhalt ( 'FGP_OEMPREIS' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsPosition->FeldInhalt ( 'FGP_EKNETTO' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $rsPosition->FeldInhalt ( 'FGP_ZEITSTEMPEL' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKey->FeldInhalt ( 'FKH_KEY' ), false );
								$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
								$SQL .= ',SYSDATE';
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsPosition->FeldInhalt ( 'FGP_DIFFORGPGLASMREGEL' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsPosition->FeldInhalt ( 'FGP_DIFFORGPALLGREGEL' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsPosition->FeldInhalt ( 'FGP_VERCLIENT' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsPosition->FeldInhalt ( 'FGP_GLASMREGEL' ), false );
								$SQL .= ')';
								
								$this->_DB->Ausfuehren($SQL, '', true);
								
								$SQL = 'Delete from FGPOSITIONSDATEN WHERE';
								$SQL .= ' FGP_VORGANGNR= ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
								$SQL .= '  AND FGP_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKopfDaten->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) );
								
								$this->_DB->Ausfuehren($SQL, '', true);
								
								$rsPosition->DSWeiter ();
							}
						
						}
						
						$ImportFile = $this->getXDIKEY ();
						
						$BindeVariablen = array ();
						$BindeVariablen ['var_T_fgk_vorgangnr'] = $this->ber_Kopfsatz [2];
						
						$SQL = "Select * from FGKOPFDATEN";
						$SQL .= " WHERE FGK_VORGANGNR=:var_T_fgk_vorgangnr";
						
						$rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
						
						$SQL = 'UPDATE FGKOPFDATEN SET ';
						$SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [0], false );
						$SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Kopfsatz [1], false );
						$SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
						$SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [3], false );
						$SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [4], false );
						$SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [5], false );
						$SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [6], false );
						$SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [7], false );
						$SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [8], false );
						$SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [9], false );
						$SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [10], false );
						$SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Kopfsatz [11], false );
						$SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [12], false );
						$SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [13], false );
						$SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $ImportFile, true );
						$SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKopfDatenUpdate->FeldInhalt ( 'FGK_ZEITSTEMPEL_KASSIERT' ), true );
						$SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopfDatenUpdate->FeldInhalt ( 'FGK_FGN_KEY' ), true );
						$SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopfDatenUpdate->FeldInhalt ( 'FGK_FCN_KEY' ), true );
						$SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat ( 'T', $rsKopfDatenUpdate->FeldInhalt ( 'FGK_FREIGABEGRUND' ), true );
						$SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKopfDatenUpdate->FeldInhalt ( 'FGK_DATUMZUGANG' ), true );
						//$SQL .= ',FGK_USER=\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
						$SQL .= ',FGK_USERDAT=sysdate';
						$SQL .= ',FGK_SCHADENNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [14], false );
						$SQL .= ',FGK_SB_RABATT=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [15], false );
						$SQL .= ',FGK_REGELWERK_KEY=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [16], false );
						$SQL .= ',FGK_VERSNR=' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Kopfsatz [17], false );
						$SQL .= ',FGK_SB_UNGEKUERZT=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [18], false );
						$SQL .= ',FGK_SB_RABATT_VERS=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [19], false );
						$SQL .= ',FGK_BETRAG_NETTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [20], false );
						$SQL .= ',FGK_MWST=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [21], false );
						$SQL .= ',FGK_BETRAG_BRUTTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Kopfsatz [22], false );
						$SQL .= ' WHERE FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Kopfsatz [2], false );
						
						$this->_DB->Ausfuehren($SQL, '', true);
					}
				}
			}
		}
	}
	
	public function speicherePossatz()
	{
		$this->countDuplikate = 0;
		$this->countDuplikatsEintrag = 0;
		
		$wandDatumPosition = '';
		$wandDatumPositionHist = '';
		
		$count_Pos = 0;
		$count_Hist = 0;
		
		$ImportFile = $this->getXDIKEY ();
				
		for($y = 0; $y < count ( $this->ber_Positionssatz ); $y ++)
		{
			$SQL = 'Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
			$SQL .= ' AND FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );

			$wandDatumPosition = $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
			$wandDatumPosition = substr ( $wandDatumPosition, 9, 19 );
			
			$rsKopf = $this->_DB->RecordSetOeffnen ( $SQL );
			
			if ($rsKopf->AnzahlDatensaetze () == 1)
			{
				if ($rsKopf->FeldInhalt ( 'FGK_VORGANGNR' ) == $this->ber_Positionssatz [$y] [1] && $rsKopf->FeldInhalt ( 'FGK_ZEITSTEMPEL' ) == $wandDatumPosition)
				{
					$SQL = 'Select * from FGPOSITIONSDATEN WHERE ';
					$SQL .= ' FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
					$SQL .= ' AND FGP_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2] );
					$SQL .= ' AND FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6] );
					$SQL .= ' AND FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7] );
					$SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );

					$checkVorhandenePositionsdaten = $this->_DB->RecordSetOeffnen ( $SQL );
					
					// Wenn bereits die gleichen Positionsdaten vorhanden sind fuehre kein INSERT durch
					
					if ($checkVorhandenePositionsdaten->AnzahlDatensaetze () == 0)
					{
						$SQL = "INSERT INTO FGPOSITIONSDATEN (FGP_KENNUNG,FGP_VORGANGNR,";
						$SQL .= "FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,FGP_OEMPREIS,";
						$SQL .= "FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,";
						$SQL .= "FGP_USER,FGP_USERDAT,FGP_DIFFORGPGLASMREGEL,FGP_DIFFORGPALLGREGEL,FGP_VERCLIENT,FGP_GLASMREGEL) VALUES (";
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [4], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopf->FeldInhalt ( 'FGK_KEY' ), false );
						$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
						$SQL .= ',SYSDATE';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [9], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [10], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [11], false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Positionssatz [$y] [12], false );
						$SQL .= ')';
						
						echo 'insert in posdaten: '.$SQL.PHP_EOL;
						$this->_DB->Ausfuehren($SQL, '', true);
						
						$count_Pos ++;
					
					} 
					else
					{
						
						if ($this->pruefeDuplikate == false)
						{
							$SQL = 'Select * from FGPOSITIONSDATEN WHERE';
							$SQL .= ' FGP_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2] );
							$SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
							$SQL .= ' AND FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
							
							$rsSummeDuplikate = $this->_DB->RecordSetOeffnen ( $SQL );
							
							if ($rsSummeDuplikate->AnzahlDatensaetze () > 0)
							{
								$Anzahl = 0;
								$anzFGPOS = 0;
								$anzDuplikat = 0;
								
								$anzFGPOS = $rsSummeDuplikate->FeldInhalt ( 'FGP_ANZAHL' );
								$anzDuplikat = $this->ber_Positionssatz [$y] [4];
								$Anzahl = $anzFGPOS + $anzDuplikat;
								
								$SQL = 'UPDATE FGPOSITIONSDATEN SET ';
								$SQL .= ' FGP_KENNUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
								$SQL .= ',FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
								$SQL .= ',FGP_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
								$SQL .= ',FGP_ARTBEZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
								$SQL .= ',FGP_ANZAHL=' . $this->_DB->FeldInhaltFormat ( 'N2', $Anzahl, false );
								$SQL .= ',FGP_EINHEIT=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
								$SQL .= ',FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
								$SQL .= ',FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
								$SQL .= ',FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
								$SQL .= ',FGP_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat ( 'N2', '0', false );
								$SQL .= ',FGP_FGK_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopf->FeldInhalt ( 'FGK_KEY' ), false );
								$SQL .= ',FGP_USER=\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
								$SQL .= ',FGP_USERDAT=sysdate';
								$SQL .= ',FGP_DIFFORGPGLASMREGEL=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [9], false );
								$SQL .= ',FGP_DIFFORGPALLGREGEL=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [10], false );
								$SQL .= ',FGP_VERCLIENT=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [11], false );
								$SQL .= ',FGP_GLASMREGEL=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [12], false );
								$SQL .= ' WHERE FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
								$SQL .= ' AND FGP_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
								$SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );

								$this->_DB->Ausfuehren($SQL, '', true);
								
								// Speichere Duplikat in der Tabelle FGDUPLIKATE
								// ---------------------------------------------
								
								$SQL = "INSERT INTO FGDUPLIKATE (FDP_KENNUNG,FDP_VORGANGNR,";
								$SQL .= "FDP_ARTNR,FDP_ARTBEZ,FDP_ANZAHL,FDP_EINHEIT,FDP_OEMPREIS,";
								$SQL .= "FDP_EKNETTO,FDP_ZEITSTEMPEL,FDP_FREMDWAEHRUNG,FDP_FGK_KEY,";
								$SQL .= "FDP_DATEINAME,FDP_USER,FDP_USERDAT) VALUES (";
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [4], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopf->FeldInhalt ( 'FGK_KEY' ), false );
								$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->aktDateiBereitImport, false );
								$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';
								
								$this->_DB->Ausfuehren($SQL, '', true);
								
								$this->countDuplikate ++;
							}
						}
					}
				}
			} 
			else
			{
				
				$wandDatumPositionHist = $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
				$wandDatumPositionHist = substr ( $wandDatumPositionHist, 9, 19 );
				
				$SQL = 'Select * from FGKOPFDATEN_HIST WHERE FKH_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
				$SQL .= ' AND FKH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
				
				$rsHistory = $this->_DB->RecordSetOeffnen ( $SQL );
				
				if ($rsHistory->AnzahlDatensaetze () == 1)
				{
					if ($rsHistory->FeldInhalt ( 'FKH_VORGANGNR' ) == $this->ber_Positionssatz [$y] [1] && $rsHistory->FeldInhalt ( 'FKH_ZEITSTEMPEL' ) == $wandDatumPositionHist)
					{
						$SQL = 'Select * from FGPOSITIONSDATEN_HIST WHERE ';
						$SQL .= ' FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
						$SQL .= ' AND FPH_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2] );
						$SQL .= ' AND FPH_OEMPREIS=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6] );
						$SQL .= ' AND FPH_EKNETTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7] );
						$SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
						
						$checkVorhandenePositionsdatenHist = $this->_DB->RecordSetOeffnen ( $SQL );
						
						if ($checkVorhandenePositionsdatenHist->AnzahlDatensaetze () == 0)
						{
							$SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
							$SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
							$SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT,FPH_DIFFORGPGLASMREGEL,FPH_DIFFORGPALLGREGEL,FPH_VERCLIENT,FPH_GLASMREGEL) VALUES(';
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [4], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsHistory->FeldInhalt ( 'FKH_KEY' ), false );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [9], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [10], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [11], false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [12], false );
							$SQL .= ')';

							$this->_DB->Ausfuehren($SQL, '', true);
							
							$count_Hist ++;
						
						} 
						else
						{
							// Berechnung DUPLIKATE fuer die History - Tabellen
							// Erster Durchlauf
							
							if ($this->pruefeDuplikate == false)
							{
								$SQL = 'Select * from FGPOSITIONSDATEN_HIST WHERE';
								$SQL .= ' FPH_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2] );
								$SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8] );
								$SQL .= ' AND FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1] );
							
								$rsSummeDuplikate = $this->_DB->RecordSetOeffnen ( $SQL );
								
								if ($rsSummeDuplikate->AnzahlDatensaetze () > 0)
								{
									$Anzahl = 0;
									$anzFGPOS = 0;
									$anzDuplikat = 0;
									
									$anzFGPOS = $rsSummeDuplikate->FeldInhalt ( 'FPH_ANZAHL' );
									$anzDuplikat = $this->ber_Positionssatz [$y] [4];
									$Anzahl = $anzFGPOS + $anzDuplikat;
									
									$SQL = 'UPDATE FGPOSITIONSDATEN_HIST SET ';
									$SQL .= ' FPH_KENNUNG=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
									$SQL .= ',FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
									$SQL .= ',FPH_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
									$SQL .= ',FPH_ARTBEZ=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
									$SQL .= ',FPH_ANZAHL=' . $this->_DB->FeldInhaltFormat ( 'N2', $Anzahl, false );
									$SQL .= ',FPH_EINHEIT=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
									$SQL .= ',FPH_OEMPREIS=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
									$SQL .= ',FPH_EKNETTO=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
									$SQL .= ',FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
									$SQL .= ',FPH_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat ( 'N2', '0', false );
									$SQL .= ',FPH_FKH_KEY=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsHistory->FeldInhalt ( 'FKH_KEY' ), false );
									$SQL .= ',FPH_USER=\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
									$SQL .= ',FPH_USERDAT=sysdate';
									$SQL .= ',FPH_DIFFORGPGLASMREGEL=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [9], false );
									$SQL .= ',FPH_DIFFORGPALLGREGEL=' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [10], false );
									$SQL .= ',FPH_VERCLIENT=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [11], false );
									$SQL .= ',FPH_GLASMREGEL=' . $this->_DB->FeldInhaltFormat ( 'NO', $this->ber_Positionssatz [$y] [12], false );
									$SQL .= ' WHERE FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
									$SQL .= ' AND FPH_ARTNR=' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
									$SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
									
									$this->_DB->Ausfuehren($SQL, '', true);
									
									// Speichere Duplikat in der Tabelle FGDUPLIKATE
									// ---------------------------------------------
									
									$SQL = "INSERT INTO FGDUPLIKATE (FDP_KENNUNG,FDP_VORGANGNR,";
									$SQL .= "FDP_ARTNR,FDP_ARTBEZ,FDP_ANZAHL,FDP_EINHEIT,FDP_OEMPREIS,";
									$SQL .= "FDP_EKNETTO,FDP_ZEITSTEMPEL,FDP_FREMDWAEHRUNG,FDP_FGK_KEY,";
									$SQL .= "FDP_DATEINAME,FDP_USER,FDP_USERDAT) VALUES (";
									$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [0], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [1], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [2], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [3], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [4], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->ber_Positionssatz [$y] [5], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [6], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->ber_Positionssatz [$y] [7], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->ber_Positionssatz [$y] [8], false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKopf->FeldInhalt ( 'FGK_KEY' ), false );
									$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->aktDateiBereitImport, false );
									$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
									$SQL .= ',SYSDATE';
									$SQL .= ')';
									
									$this->_DB->Ausfuehren($SQL, '', true);
									
									$this->countDuplikate ++;
								
								}
							
							}
						}
					}
				
				}
			}
		}
		
		// ------------------------------------------------------------------------------
		// -----------------------Ueberpruefe DuplikatsCount-----------------------------
		// ------------------------------------------------------------------------------
		
		if ($this->countDuplikate == 0)
		{
			$SQL = "INSERT INTO FGDUPLIKATE (FDP_KENNUNG,FDP_VORGANGNR,";
			$SQL .= "FDP_ARTNR,FDP_ARTBEZ,FDP_ANZAHL,FDP_EINHEIT,FDP_OEMPREIS,";
			$SQL .= "FDP_EKNETTO,FDP_FREMDWAEHRUNG,";
			$SQL .= "FDP_DATEINAME,FDP_USER,FDP_USERDAT) VALUES (";
			$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', 'P', false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '0', false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', 0, false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', 'DUMMY', false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', 0, false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->aktDateiBereitImport, false );
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			$this->_DB->Ausfuehren($SQL, '', true);
		}
		$this->speichereFehlerPositionssatz ();
		
	} // Ende der Funktion
	
	public function speichereFehlerPositionssatz()
	{
		for($i = 0; $i < count ( $this->fehlerPositionssatz ); $i ++)
		{
			$SQL = "INSERT INTO FGPOSITIONSDATENFEHLER (FPF_KENNUNG,FPF_VORGANGNR,";
			$SQL .= "FPF_ARTNR,FPF_ARTBEZ,FPF_ANZAHL,FPF_EINHEIT,FPF_OEMPREIS,";
			$SQL .= "FPF_EKNETTO,FPF_ZEITSTEMPEL,";
			$SQL .= "FPF_USER,FPF_USERDAT) VALUES (";
			$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [0], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [1], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [2], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [3], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->fehlerPositionssatz [$i] [4], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [5], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->fehlerPositionssatz [$i] [6], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $this->fehlerPositionssatz [$i] [7], false );
			$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'DU', $this->fehlerPositionssatz [$i] [8], false );
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			$this->_DB->Ausfuehren($SQL, '', true);
			
			$BindeVariablen = array ();
			$BindeVariablen ['var_T_fgk_vorgangnr'] = $this->fehlerPositionssatz [$i] [1];
			
			$SQL = "Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=:var_T_fgk_vorgangnr";
			
			$rsKopfDS = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
			
			if ($rsKopfDS->AnzahlDatensaetze () > 0)
			{
				$SQL = "INSERT INTO FGKOPFDATENFEHLER (FKF_KENNUNG,FKF_FILID,FKF_VORGANGNR,";
				$SQL .= " FKF_KFZBEZ,FKF_KFZKENNZ,FKF_KBANR,FKF_FAHRGESTELLNR,FKF_VERSICHERUNG,";
				$SQL .= " FKF_VERSSCHEINNR,FKF_SB,FKF_MONTAGEDATUM,FKF_ZEITSTEMPEL,FKF_STEUERSATZ,";
				$SQL .= " FKF_KUNDENNAME,FKF_IMP_KEY,FKF_USER,FKF_USERDAT) Select ";
				$SQL .= " FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
				$SQL .= " FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
				$SQL .= " FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
				$SQL .= " FGK_KUNDENNAME,FGK_IMP_KEY,FGK_USER,FGK_USERDAT from FGKOPFDATEN";
				$SQL .= " WHERE FGK_VORGANGNR=" . $this->_DB->FeldInhaltFormat ( 'T', $this->fehlerPositionssatz [$i] [1], false );

				$this->_DB->Ausfuehren($SQL, '', true);
				
				$BindeVariablen = array ();
				$BindeVariablen ['var_T_fgk_vorgangnr'] = $this->fehlerPositionssatz [$i] [1];
				
				$SQL = "Delete from FGKOPFDATEN WHERE FGK_VORGANGNR=:var_T_fgk_vorgangnr";
				
				$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
			}
		}
		
		$SQL = 'Select * from FGKOPFDATENFEHLER';
		
		$rsKopfFehler = $this->_DB->RecordSetOeffnen ( $SQL );
		
		while ( ! $rsKopfFehler->EOF () )
		{
			$SQL = "INSERT INTO FGPOSITIONSDATENFEHLER (FPF_KENNUNG,FPF_VORGANGNR,";
			$SQL .= " FPF_ARTNR,FPF_ARTBEZ,FPF_ANZAHL,FPF_EINHEIT,FPF_OEMPREIS,FPF_EKNETTO,FPF_ZEITSTEMPEL,";
			$SQL .= " FPF_USER,FPF_USERDAT) Select ";
			$SQL .= " FGP_KENNUNG,FGP_VORGANGNR,";
			$SQL .= " FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,FGP_OEMPREIS,FGP_EKNETTO,FGP_ZEITSTEMPEL,";
			$SQL .= " FGP_USER,FGP_USERDAT from FGPOSITIONSDATEN";
			$SQL .= " WHERE FGP_VORGANGNR=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKopfFehler->FeldInhalt ( 'FKF_VORGANGNR' ) );
			
			$this->_DB->Ausfuehren($SQL, '', true);
			
			$BindeVariablen = array ();
			$BindeVariablen ['var_T_fgp_vorgangnr'] = $rsKopfFehler->FeldInhalt ( 'FKF_VORGANGNR' );
			
			$SQL = 'Delete from FGPOSITIONSDATEN WHERE FGP_VORGANGNR=:var_T_fgp_vorgangnr';
			
			$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
			
			$rsKopfFehler->DSWeiter ();
		}
	}

	public function speichereKassendatenStorno()
	{
		// Stornierte DS
		$SQL = 'Select KENNUNG, FIL_ID,DATUM, ZEIT, BSA, VORGANGS_NUMMER, substr(AEM_NR,1,20) AS AEM_NR, ARTNR, MENGE, UMSATZ,KFZ_KZ from dwh.FIRSTGLASS_STORNIERT@DWH';
		
		$rsKassendatenStorno = $this->_DB->RecordSetOeffnen ( $SQL );
		
		While ( ! $rsKassendatenStorno->EOF () )
		{
			$BindeVariablen = array ();
			$BindeVariablen ['var_T_fks_firstglassnr'] = $rsKassendatenStorno->FeldInhalt ( 'AEM_NR' );
			
			$SQL = "Select FKS_DATUMSTORNIERT,FKS_FIRSTGLASSNR";
			$SQL .= " from FKASSENDATEN_STORNIERT "; // WHERE FKS_DATUMSTORNIERT=".$this->_DB->FeldInhaltFormat('D', $rsKassendatenStorno->FeldInhalt('DATUM'));
			$SQL .= " WHERE FKS_FIRSTGLASSNR=:var_T_fks_firstglassnr";
			
			$rsPruefeKassendaten = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
			
			if ($rsPruefeKassendaten->AnzahlDatensaetze () == 0)
			{
				$SQL = "INSERT INTO FKASSENDATEN_STORNIERT (FKS_DATUMSTORNIERT,FKS_FIRSTGLASSNR";
				$SQL .= ") VALUES (";
				$SQL .= '' . $this->_DB->FeldInhaltFormat ( 'DU', $rsKassendatenStorno->FeldInhalt ( 'DATUM' ), false );
				$SQL .= ', ' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendatenStorno->FeldInhalt ( 'AEM_NR' ), false );
				$SQL .= ')';
				
				$this->_DB->Ausfuehren($SQL, '', true);
			
			}
			$rsKassendatenStorno->DSWeiter ();
		}
	}
	
	public function safeKassendaten()
	{	
		
		$ImportFile = $this->getXDIKEY ();
		// Kassendaten speichern
		
		$Datum = '';
		$DatumKassendaten = '';
		
		$SQL = 'Select max(FGK_ZEITSTEMPEL) AS DATETIME FROM FGKOPFDATEN';
		$rsControl = $this->_DB->RecordSetOeffnen ( $SQL );
		
		$MAXFILE = $rsControl->FeldInhalt ( 'DATETIME' );
		
		$Datum = substr ( $MAXFILE, 0, 10 );
	
		$this->aktDatumKassendaten = $Datum;
	
		$BindeVariablen = array ();
		$BindeVariablen ['var_T_xdi_bereich'] = 'FGK';
		
		$SQL = "Select * from IMPORTPROTOKOLL WHERE XDI_BEREICH=:var_T_xdi_bereich ORDER BY XDI_KEY DESC";
		
		$rsCheck = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
		
		if ($rsCheck->FeldInhalt ( 'XDI_BEMERKUNG' ) != 0)
		{
			$DatumKassendaten = $rsCheck->FeldInhalt ( 'XDI_DATEINAME' );
			$DatumKassendaten = substr ( $DatumKassendaten, 53, 60 );
			$DatumKasse = substr ( $DatumKassendaten, 0, 6 );
			$Tag = substr ( $DatumKassendaten, 4, 2 );
			$Monat = substr ( $DatumKassendaten, 2, 2 );
			$Jahr = substr ( $DatumKassendaten, 0, 2 );
			$wandDatum = $Tag . '.' . $Monat . '.20' . $Jahr;
		
			$this->KassendatenDatei = $wandDatum;
			
			$this->berechneDifferenzTageKassendaten ();
			
			$this->aktDatumKassendaten = $DatumKassendaten;
			
			for($i = 0; $i < count ( $this->fikFilesKassendaten ); $i ++)
			{
			 	$Dateiname = "";
				$Dateiname = substr ( $this->fikFilesKassendaten [$i], 2, 8 );
								
				$SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
				$SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
				$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', 'FGK', false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->pfadKassendaten . 'estermann' . $Dateiname . '.txt', false );
				$SQL .= ',SYSDATE';
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '0', false );
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				$this->_DB->Ausfuehren($SQL, '', true);
				
				// ---------------------Kassendaten--------------------------------
				
				$DatumKassendaten = $this->fikFilesKassendaten [$i];
				$DatumKasse = substr ( $DatumKassendaten, 0, 6 );
				$Tag = substr ( $DatumKassendaten, 6, 2 );
				$Monat = substr ( $DatumKassendaten, 4, 2 );
				$Jahr = substr ( $DatumKassendaten, 0, 4 );
				$wandDatum = $Tag . '.' . $Monat . '.' . $Jahr;
				
				$this->_Funktionen->SchreibeModulStatus ( "runImport", "IMPORT KASSENDATEN" );
				
				echo$wandDatum;
				
				$SQL = "Select KENNUNG,FIL_ID,DATUM,ZEIT,BSA,VORGANGS_NUMMER,AEM_NR,ARTNR,MENGE,UMSATZ,KFZ_KZ,AEM_VERS_KZ,VERS_NR from dwh.FIRSTGLASS_KASSENDATEN@DWH";
				$SQL .= " left join FKASSENDATEN ON FIL_ID = FKA_FILID AND VORGANGS_NUMMER = FKA_WANR ";
				$SQL .= " WHERE FKA_WANR IS NULL AND DATUM >=" . $this->_DB->FeldInhaltFormat ( 'DU', '17.08.09 00:00:00' ) . " AND DATUM <=" . $this->_DB->FeldInhaltFormat ( 'DU', $wandDatum );
				
				$rsKassendaten = $this->_DB->RecordSetOeffnen ( $SQL );
				
				While ( ! $rsKassendaten->EOF () )
				{
					$SQL = "Select FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ";
					$SQL .= " from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendaten->FeldInhalt ( 'FIL_ID' ) );
					$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsKassendaten->FeldInhalt ( 'DATUM' ) );
					$SQL .= " AND FKA_UHRZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'ZEIT' ) );
					$SQL .= " AND FKA_BSA=" . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'BSA' ) );
					$SQL .= " AND FKA_MENGE=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendaten->FeldInhalt ( 'MENGE' ) );
					$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendaten->FeldInhalt ( 'UMSATZ' ) );
					
					$rsPruefeKassendaten = $this->_DB->RecordSetOeffnen ( $SQL );
					
					if ($rsPruefeKassendaten->AnzahlDatensaetze () == 0)
					{
						$SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
						$SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
						$SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_USER,FKA_USERDAT,FKA_AEM_VERS_KZ,FKA_VERS_NR";
						$SQL .= ") VALUES (";
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'KENNUNG' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendaten->FeldInhalt ( 'FIL_ID' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'D', $rsKassendaten->FeldInhalt ( 'DATUM' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'ZEIT' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'BSA' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'VORGANGS_NUMMER' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'AEM_NR' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'ARTNR' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendaten->FeldInhalt ( 'MENGE' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsKassendaten->FeldInhalt ( 'UMSATZ' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'KFZ_KZ' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 'DATEN AUS DWH', false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 0, True );
						$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
						$SQL .= ',SYSDATE';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsKassendaten->FeldInhalt ( 'AEM_VERS_KZ' ), false );
						$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsKassendaten->FeldInhalt ( 'VERS_NR' ), false );
						$SQL .= ')';
						
						$this->_DB->Ausfuehren($SQL, '', true);
						
						$BindeVariablen = array ();
						$BindeVariablen ['var_N0_fil_id'] = $rsKassendaten->FeldInhalt ( 'FIL_ID' );
						$BindeVariablen ['var_N0_vorgangs_nummer'] = $rsKassendaten->FeldInhalt ( 'VORGANGS_NUMMER' );
						$BindeVariablen ['var_T_aem_nr'] = $rsKassendaten->FeldInhalt ( 'AEM_NR' );
						$BindeVariablen ['var_T_kfz_kz'] = $rsKassendaten->FeldInhalt ( 'KFZ_KZ' );
						
						$SQL = 'INSERT INTO DWH.WA_GLAS@DWH(FILNR, WANR, VORGANGSNR, KFZ_KENNZ)';
						$SQL .= ' VALUES (:var_N0_fil_id, :var_N0_vorgangs_nummer, :var_T_aem_nr, :var_T_kfz_kz)';
						
						$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
					
					} 
					else
					{
						$BindeVariablen = array ();
						$BindeVariablen ['var_T_vorgangsnr'] = $rsKassendaten->FeldInhalt ( 'AEM_NR' );
						
						$SQL = 'Select * from DWH.WA_GLAS@DWH WHERE VORGANGSNR=:var_T_vorgangsnr';
						
						$rspruefeWAGLAS = $this->_DB->RecordSetOeffnen ( $SQL, $BindeVariablen );
						
						if ($rspruefeWAGLAS->AnzahlDatensaetze () == 0)
						{
							$BindeVariablen = array ();
							$BindeVariablen ['var_N0_fil_id'] = $rsKassendaten->FeldInhalt ( 'FIL_ID' );
							$BindeVariablen ['var_N0_vorgangs_nummer'] = $rsKassendaten->FeldInhalt ( 'VORGANGS_NUMMER' );
							$BindeVariablen ['var_T_aem_nr'] = $rsKassendaten->FeldInhalt ( 'AEM_NR' );
							$BindeVariablen ['var_T_kfz_kz'] = $rsKassendaten->FeldInhalt ( 'KFZ_KZ' );
							
							$SQL = 'INSERT INTO DWH.WA_GLAS@DWH(FILNR, WANR, VORGANGSNR, KFZ_KENNZ)';
							$SQL .= ' VALUES (:var_N0_fil_id, :var_N0_vorgangs_nummer, :var_T_aem_nr, :var_T_kfz_kz)';
							
							$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
						} 
						else
						{
							$BindeVariablen = array ();
							$BindeVariablen ['var_N0_fil_id'] = $rsKassendaten->FeldInhalt ( 'FIL_ID' );
							$BindeVariablen ['var_N0_vorgangs_nummer'] = $rsKassendaten->FeldInhalt ( 'VORGANGS_NUMMER' );
							$BindeVariablen ['var_T_aem_nr'] = $rsKassendaten->FeldInhalt ( 'AEM_NR' );
							$BindeVariablen ['var_T_kfz_kz'] = $rsKassendaten->FeldInhalt ( 'KFZ_KZ' );
							
							$SQL = 'UPDATE DWH.WA_GLAS@DWH SET ';
							$SQL .= ' FILNR=:var_N0_fil_id';
							$SQL .= ',WANR=:var_N0_vorgangs_nummer';
							$SQL .= ',VORGANGSNR=:var_T_aem_nr';
							$SQL .= ',KFZ_KENNZ=:var_T_kfz_kz';
							$SQL .= ' WHERE VORGANGSNR=:var_T_aem_nr';
							
							$this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
						}
					}			
					$rsKassendaten->DSWeiter ();
				}
				
				$SQL = "INSERT INTO IMPORTPROTOKOLL (XDI_BEREICH,XDI_DATEINAME,XDI_DATUM,";
				$SQL .= "XDI_BEMERKUNG,XDI_USER,XDI_USERDAT) VALUES (";
				$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', 'FGK', false );
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $this->pfadKassendaten . 'estermann' . $Dateiname . '.txt', false );
				$SQL .= ',SYSDATE';
				$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '1', false );
				$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				$this->_DB->Ausfuehren($SQL, '', true);
			
			}
			
			$this->_Funktionen->SchreibeModulStatus ( "runImport", "IMPORT KassendatenStorno" );
			
			// ----------------------------------------------------------------------------------------
			// -----------------------------Rueckfuehrungen--------------------------------------------
			// ----------------------------------------------------------------------------------------
			
			for($i = 0; $i < count ( $this->fikFilesKassendaten ); $i ++)
			{
				$SQL = "Select KENNUNG, FIL_ID,DATUM, ZEIT, BSA, VORGANGS_NUMMER, substr(AEM_NR,1,20) AS AEM_NR, ARTIKELNUMMER, MENGE, UMSATZ from dwh.FIRSTGLASS_KASSENDATENSTORNO@DWH ";
				$SQL .= " WHERE DATUM >=" . $this->_DB->FeldInhaltFormat ( 'DU', '17.08.09' ) . " AND DATUM <=" . $this->_DB->FeldInhaltFormat ( 'DU', $this->fikFilesKassendaten [$i] );
				
				$rsRueckFuehrung = $this->_DB->RecordSetOeffnen ( $SQL );
				
				//echo "Start KasseStorno";
				//echo "\n";
				
				while ( ! $rsRueckFuehrung->EOF () )
				{
					
					$SQL = "Select FKA_KENNUNG,FKA_FILID,FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ";
					$SQL .= " from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ) );
					$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ) );
					$SQL .= " AND FKA_UHRZEIT=" . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'ZEIT' ) );
					$SQL .= " AND FKA_BSA=" . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'BSA' ) );
					$SQL .= " AND FKA_MENGE=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'MENGE' ) );
					$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' ) );
					
					$rsPruefeRueckFuehrung = $this->_DB->RecordSetOeffnen ( $SQL );
					
					if ($rsPruefeRueckFuehrung->AnzahlDatensaetze () == 0)
					{
						
						if ($rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ) == null || $rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ) == '')
						{
							$Betrag = $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' );
							$Betrag = str_replace ( '-', '', $Betrag );
							
							$SQL = "Select * from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ) );
							$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ) );
							$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $Betrag );
							
							$rsAEMNR = $this->_DB->RecordSetOeffnen ( $SQL );
							
							$SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
							$SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
							$SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_USER,FKA_USERDAT";
							$SQL .= ") VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'KENNUNG' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'ZEIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'BSA' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'VORGANGS_NUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_AEMNR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'ARTIKELNUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'MENGE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 0, false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 0, True );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL, '', true );
							
							$SQL = 'INSERT INTO DWH.WA_GLAS@DWH(FILNR, WANR, VORGANGSNR, KFZ_KENNZ';
							
							$SQL .= ') VALUES (';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'VORGANGS_NUMMER' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_AEMNR' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'KFZ_KZ' ), false ) . ' ';
							
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL, '', true );
						
						} 
						else
						{
							// Wenn die AEM-Nummer nicht leer ist
							
							// Pruefen
							$Betrag = $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' );
							$Betrag = str_replace ( '-', '', $Betrag );
							
							$SQL = "Select * from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ) );
							$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ) );
							$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $Betrag );
							
							$rsAEMNR = $this->_DB->RecordSetOeffnen ( $SQL );
							
							$SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
							$SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
							$SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_USER,FKA_USERDAT";
							$SQL .= ") VALUES (";
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'KENNUNG' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'ZEIT' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'BSA' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'VORGANGS_NUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'ARTIKELNUMMER' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'MENGE' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'N2', $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' ), false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'T', '', false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 0, false );
							$SQL .= ',' . $this->_DB->FeldInhaltFormat ( 'NO', 0, True );
							$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName () . '\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL, '', true);
							
							$SQL = 'INSERT INTO DWH.WA_GLAS@DWH(FILNR, WANR, VORGANGSNR, KFZ_KENNZ';
							$SQL .= ') VALUES (';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'VORGANGS_NUMMER' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ), false ) . ', ';
							$SQL .= $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'KFZ_KZ' ), false ) . ' ';
							$SQL .= ')';
							
							$this->_DB->Ausfuehren($SQL, '', true);							
						}
					} 
					else
					{
						
						$SQL = 'Select * from DWH.WA_GLAS@DWH WHERE WANR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsRueckFuehrung->FeldInhalt ( 'VORGANGS_NUMMER' ), false );
						
						$rspruefeWAGLAS = $this->_DB->RecordSetOeffnen ( $SQL );
						
						if ($rspruefeWAGLAS->AnzahlDatensaetze () == 0)
						{
						
						} else
						{
							if ($rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ) == null || $rsRueckFuehrung->FeldInhalt ( 'AEM_NR' ) == '')
							{
								$Betrag = $rsRueckFuehrung->FeldInhalt ( 'UMSATZ' );
								$Betrag = str_replace ( '-', '', $Betrag );
								
								$SQL = "Select * from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ) );
								$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ) );
								$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', $Betrag );
								
								$rsAEMNR = $this->_DB->RecordSetOeffnen ( $SQL );
								
								$SQL = 'UPDATE DWH.WA_GLAS@DWH SET ';
								$SQL .= ' FILNR=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsAEMNR->FeldInhalt ( 'FKA_FILID' ), false );
								$SQL .= ',WANR=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsAEMNR->FeldInhalt ( 'FKA_WANR' ), false );
								$SQL .= ',VORGANGSNR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_AEMNR' ), false );
								$SQL .= ' WHERE WANR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_WANR' ), false );
								
								$this->_DB->Ausfuehren($SQL, '', true);
								
								$SQL = "Select * from FKASSENDATEN WHERE FKA_FILID=" . $this->_DB->FeldInhaltFormat ( 'NO', $rsRueckFuehrung->FeldInhalt ( 'FIL_ID' ) );
								$SQL .= " AND FKA_DATUM=" . $this->_DB->FeldInhaltFormat ( 'D', $rsRueckFuehrung->FeldInhalt ( 'DATUM' ) );
								$SQL .= " AND FKA_BETRAG=" . $this->_DB->FeldInhaltFormat ( 'N2', '-' . $Betrag );
								
								$rsSTORNONR = $this->_DB->RecordSetOeffnen ( $SQL );
								
								$SQL = 'UPDATE DWH.WA_GLAS@DWH SET ';
								$SQL .= ' FILNR=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsSTORNONR->FeldInhalt ( 'FKA_FILID' ), false );
								$SQL .= ',WANR=' . $this->_DB->FeldInhaltFormat ( 'NO', $rsSTORNONR->FeldInhalt ( 'FKA_WANR' ), false );
								$SQL .= ',VORGANGSNR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_AEMNR' ), false );
								$SQL .= ' WHERE WANR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsSTORNONR->FeldInhalt ( 'FKA_WANR' ), false );
								
								$this->_DB->Ausfuehren($SQL, '', true);
								
								// Kassendaten updaten
								// --------------------------------------------------
								$SQL = 'UPDATE FKASSENDATEN SET';
								$SQL .= ' FKA_AEMNR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsAEMNR->FeldInhalt ( 'FKA_AEMNR' ), false );
								$SQL .= ' WHERE FKA_WANR=' . $this->_DB->FeldInhaltFormat ( 'T', $rsSTORNONR->FeldInhalt ( 'FKA_WANR' ), false );
								
								$this->_DB->Ausfuehren($SQL, '', true);	
							}
						}
					}
					$rsRueckFuehrung->DSWeiter ();
				} // Ende While
				echo "Ende 1";
				echo "\n";
				
			} // Ende FOR
			echo "Ende 2";
			echo "\n";
		
		}
		echo "Ende 3";
		echo "\n";
	
	
		$this->_Funktionen->SchreibeModulStatus ( "runImport", "ENDE IMPORT KassendatenStorno" );
	}

	private function berechneDifferenzTageKassendaten()
	{
		date_default_timezone_set ( 'UTC' );
		
		unset ( $this->fikFilesKassendaten );

		$this->differenzTageKassendaten = strtotime ( $this->aktDatumKassendaten ) - strtotime ( $this->KassendatenDatei );
		$this->differenzTageKassendaten = $this->differenzTageKassendaten / 86400;
		$this->differenzTageKassendaten = round ( $this->differenzTageKassendaten );
		
		// Erzeuge Files
		for($element = 0; $element < $this->differenzTageKassendaten; $element ++)
		{
			$this->KassendatenDatei = strtotime ( $this->KassendatenDatei ) + 86400;
			$this->KassendatenDatei = date ( 'Ymd', $this->KassendatenDatei );
			$this->fikFilesKassendaten [$element] = $this->KassendatenDatei;
		}
	}

} // Ende der Klasse
?>
