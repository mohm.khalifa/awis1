<?php
/**
 * Suchmaske f�r Kartenzahlungen
 *
 * @author Patrick Gebhardt
 * @copyright ATU
 * @version 201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS'); 
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KNZ','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht31000=$AWISBenutzer->HatDasRecht(31000);
	if($Recht31000==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./kartenzahlungen_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Kartenzahlungen'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();
	
	$Form->ZeileStart(); 
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_FILNR'].':',190);
	$Form->Erstelle_TextFeld('*KNZ_FILNR',($Param['SPEICHERN']=='on'?$Param['KNZ_FILNR']:''),25,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_DATUM'].' von:',190);
	$Form->Erstelle_TextFeld('*KNZ_DATUM_VON',($Param['SPEICHERN']=='on'?$Param['KNZ_DATUM_VON']:''),25,230,true,'','','','D','L');
	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_DATUM'].' bis:',90);
	$Form->Erstelle_TextFeld('*KNZ_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['KNZ_DATUM_BIS']:''),25,230,true,'','','','D','L');
	//$Form->Erstelle_TextFeld(string $Name, string $Wert,    nt $Zeichen, int $Breite, boolean $AendernRecht, string $Style, string $InputStyle, string $Link, string $Format, string $Ausrichtung, string $ToolTippText, mixed $DefaultWert, int $MaxZeichen, string $Java, $Tastenkuerzel);
	$Form->ZeileEnde();
	//Hiddentextfelder f�r Suchparameter
	/*	
	$Form->ZeileStart();

	$Form->Erstelle_HiddenFeld('*KNZ_BETRAG',($Param['SPEICHERN']=='on'?$Param['KNZ_BETRAG']:''));
	$Form->Erstelle_HiddenFeld('*KNZ_ART',($Param['SPEICHERN']=='on'?$Param['KNZ_ART']:''));
	$Form->Erstelle_HiddenFeld('*KNZ_BLZ',($Param['SPEICHERN']=='on'?$Param['KNZ_BLZ']:''));
	$Form->Erstelle_HiddenFeld('*KNZ_KTONR',($Param['SPEICHERN']=='on'?$Param['KNZ_KTONR']:''));

	$Form->ZeileEnde();
*/	
	if (($Recht31000&2)==2) //Erweiterte Ansicht
	{

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_BETRAG'].':',190);
		$Form->Erstelle_TextFeld('*KNZ_BETRAG',($Param['SPEICHERN']=='on'?$Param['KNZ_BETRAG']:''),25,200,true);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_ART'].':',190);
		$Form->Erstelle_SelectFeld('*KNZ_ART', ($Param['SPEICHERN']=='on'?$Param['KNZ_ART']:''), 200, true, 'SELECT KNZ_ART AS ID, KNZ_ART FROM(SELECT DISTINCT KNZ_ART AS KNZ_ART FROM KARTENZAHLUNGEN ORDER BY 1)','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2);
		
		$AWISCursorPosition='sucKNZ_ART';
		$Form->ZeileEnde();
		
		/*$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_BLZ'].':',190);
		$Form->Erstelle_TextFeld('*KNZ_BLZ',($Param['SPEICHERN']=='on'?$Param['KNZ_BLZ']:''),25,200,true);
		$Form->ZeileEnde();
		*/
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_KTONR'].':',190);
		$Form->Erstelle_TextFeld('*KNZ_KTONR',($Param['SPEICHERN']=='on'?$Param['KNZ_KTONR']:''),25,200,true);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',190);
		$Form->Erstelle_TextFeld('*KNZ_TERMINALID',($Param['SPEICHERN']=='on'?$Param['KNZ_TERMINALID']:''),25,200,true);
		$Form->ZeileEnde();
		
	}
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');


	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>