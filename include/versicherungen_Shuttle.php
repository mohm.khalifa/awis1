﻿<?php

/****************************************************************************
 *                                                                          *                                        *
 *                          SHUTTLESCRIPT V1.0                              *
 *
 * @author                                                                  Stefan Oppl                             *
 *                                                                          *
 * @todo                                                                    Versicherungsdaten verarbeiten            *
 *                                                                          *
 *                                                                          *
 *                                                                          ***************************************************************************/

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('versicherungen_Verarbeitung.php');
require_once('awisVersicherungenFunktionen.php');
require_once('awisVersicherungenImport.inc');
require_once('awisAutoglas_DateiTransfer.inc');
require_once("awisJobs.inc");


class VersicherungenShuttle
{
    private $_DebugLevel;
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_VersImport;
    private $_VersVerarbeitung;
    private $_JobID;
    private $_DWHGeladen = true;
    private $_FGJob = false;
    private $_awisJobs;
    private $_startJob = false;

    // Steuervariablen
    private $_checkDWH = true;

    private $_checkFG = true;

    private $_loadDWHViews = true;
    private $_awisLevel = '';
    private $_Werkzeug = '';

    private $_EMAIL_Infos = array('shuttle@de.atu.eu');

    function __construct($DebugLevel = 0, $Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_DebugLevel = $DebugLevel;
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_Funktionen = new VersicherungenFunktionen();
        $this->_VersImport = new VersicherungenImport($DebugLevel, $Benutzer);
        $this->_VersVerarbeitung = new VersicherungenVerarbeiten($DebugLevel, $Benutzer);

        $this->_Werkzeug = new awisWerkzeuge();
        $this->_awisLevel = $this->_Werkzeug->awisLevel();

        $this->_awisJobs = new awisJobs($Benutzer);
    }

    public function Shuttle()
    {
        try
        {
            $SQL = 'Select VJI_KEY,VJI_JOBID,VJI_DATUMJOBID from VERSJOBID ORDER BY VJI_KEY';
            $rsJOB = $this->_DB->RecordSetOeffnen($SQL);

            $this->_JobID = $rsJOB->FeldInhalt('VJI_JOBID');
            
            $this->debugAusgabe("Versicherungs-Verarbeitung wird gestartet", 10);

            if($this->_Funktionen->PruefeAnstehend('LadeNeueDaten', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('LadeNeueDaten');
                $this->_VersImport->LadeNeueDaten();
                $this->_Funktionen->SchreibeEndeZeit('LadeNeueDaten', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('SperreVoraengeMitUmsatz0', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('SperreVoraengeMitUmsatz0');
                $this->_VersVerarbeitung->SperreVoraengeMitUmsatz0();
                $this->_Funktionen->SchreibeEndeZeit('SperreVoraengeMitUmsatz0', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefeDubletten', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefeDubletten');
                $this->_VersVerarbeitung->PruefeDubletten();
                $this->_Funktionen->SchreibeEndeZeit('PruefeDubletten', $this->_JobID);
            }
            //Setzt alle Ruecknahmen / Umbuchung auf den Status 120
            if($this->_Funktionen->PruefeAnstehend('SetzteStatusUmgebucht', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('SetzteStatusUmgebucht');
                $this->_VersVerarbeitung->SetzteStatusUmgebucht();
                $this->_Funktionen->SchreibeEndeZeit('SetzteStatusUmgebucht', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefFalscheSKZ', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefFalscheSKZ');
                $this->_VersVerarbeitung->PruefFalscheSKZ();
                $this->_Funktionen->SchreibeEndeZeit('PruefFalscheSKZ', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefeUndBearbeiteSKZ_K', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefeUndBearbeiteSKZ_K');
                $this->_VersVerarbeitung->PruefeUndBearbeiteSKZ_K();
                $this->_Funktionen->SchreibeEndeZeit('PruefeUndBearbeiteSKZ_K', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefSKZ_E', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefSKZ_E');
                $this->_VersVerarbeitung->PruefSKZ_E();
                $this->_Funktionen->SchreibeEndeZeit('PruefSKZ_E', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefSKZ_S', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefSKZ_S');
                $this->_VersVerarbeitung->PruefSKZ_S();
                $this->_Funktionen->SchreibeEndeZeit('PruefSKZ_S', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('SetzeSKZ_T', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('SetzeSKZ_T');
                $this->_VersVerarbeitung->SetzeSKZ_T();
                $this->_Funktionen->SchreibeEndeZeit('SetzeSKZ_T', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('PruefSKZ_T', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('PruefSKZ_T');
                $this->_VersVerarbeitung->PruefSKZ_T();
                $this->_Funktionen->SchreibeEndeZeit('PruefSKZ_T', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('SperreSKZ_T', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('SperreSKZ_T');
                $this->_VersVerarbeitung->SperreSKZ_T();
                $this->_Funktionen->SchreibeEndeZeit('SperreSKZ_T', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('SetzeSKZ_P', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('SetzeSKZ_P');
                $this->_VersVerarbeitung->SetzeSKZ_P();
                $this->_Funktionen->SchreibeEndeZeit('SetzeSKZ_P', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('BearbSKZ_T', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('BearbSKZ_T');
                $this->_VersVerarbeitung->BearbSKZ_T();
                $this->_Funktionen->SchreibeEndeZeit('BearbSKZ_T', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('BearbSKZ_P', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('BearbSKZ_P');
                $this->_VersVerarbeitung->BearbSKZ_P();
                $this->_Funktionen->SchreibeEndeZeit('BearbSKZ_P', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('BearbSKZ_E', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('BearbSKZ_E');
                $this->_VersVerarbeitung->BearbSKZ_E();
                $this->_Funktionen->SchreibeEndeZeit('BearbSKZ_E', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('BearbKennzeichen', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('BearbKennzeichen');
                $this->_VersVerarbeitung->BearbKennzeichen();
                $this->_Funktionen->SchreibeEndeZeit('BearbKennzeichen', $this->_JobID);
            }
            //Vorgangspruefung auf doppelten Rechnungsversand => Vorgang sperren
            if($this->_Funktionen->PruefeAnstehend('ExportiereVorgaengeFuerWebservice', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('ExportiereVorgaengeFuerWebservice');
                $this->_VersVerarbeitung->ExportiereVorgaengeFuerWebservice();
                $this->_Funktionen->SchreibeEndeZeit('ExportiereVorgaengeFuerWebservice', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('ExportiereVorgaenge', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('ExportiereVorgaenge');
                $this->_VersVerarbeitung->ExportiereVorgaenge();
                $this->_Funktionen->SchreibeEndeZeit('ExportiereVorgaenge', $this->_JobID);
            }
            if($this->_Funktionen->PruefeAnstehend('RueckschreibenDWH', 'Versicherung'))
            {
                $this->_Funktionen->SchreibeStartZeit('RueckschreibenDWH');

                if($this->_Werkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV or $this->_Werkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE)
                {
                    $this->_VersVerarbeitung->RueckschreibenDWH();
                }
                $this->_Funktionen->SchreibeEndeZeit('RueckschreibenDWH', $this->_JobID);
            }
            $this->debugAusgabe("Versicherungs-Verarbeitung beendet", 10);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherungen_shuttle: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo $this->_DB->LetzterSQL();

            $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'WARNING - Versicherungen_Shuttle', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function debugAusgabe($Text = '', $Level = 500)
    {
        if($this->_DebugLevel >= $Level)
        {
            echo date('d.m.y H:i:s ') . $Text . PHP_EOL;
        }
    }
}

?>