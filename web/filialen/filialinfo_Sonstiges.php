<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 116);
 if($RechteStufe==0)
 {
    awisEreignis(3,1000,'Filialen: Sonstiges',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
 }

 
$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ";

$FILID = (isset($_REQUEST['FIL_ID'])?$_REQUEST['FIL_ID']:'');

// Alle Filialen anzeigen
If(!isset($_GET["Liste"]) AND $FILID == '')
{
	$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
	if(substr($foo,0,7)=="FIL_ID=")
	{
		$FILID=substr($foo,7,99);
	}
}
// Bedingung ausf�llen -> ID �bernehmen

if($FILID != '')
{
    $SQL .= " AND ( FIL_ID=0" . $FILID . ")";
}
else
{
 	$Params = explode(";", awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName()));
    if($Params[0] != '')
    {
        $Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0], 0);
    }
     if($Params[1] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_BEZ) " . awisLIKEoderIST("%" . $Params[1] . "%", 1, 0, 1);
    }
     if($Params[2] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_ORT) " . awisLIKEoderIST('%'.$Params[2].'%', 1, 0, 1);
    }
    if($Params[3] != '')
    {
        $Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3], 0);
	}
	if($Params[5]!='')
	{
		$Bedingung .= "AND FIL_ID IN (SELECT DISTINCT PER_FIL_ID FROM PERSONAL WHERE SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $Params[5]. "%",1,0,1) . ")";		
	}
    
    if($Bedingung != '')
    {
        $SQL .= " AND ( " . substr($Bedingung, 4) . ")";
    }
}

$rsFiliale = awisOpenRecordset($con, $SQL);
$rsFilialeAnz = $awisRSZeilen;

If($rsFilialeAnz > 1) 
{ // Liste gefunden -> Liste anzeigen
 	print "<table border=0 width=100%>";
    for($i = 0;$i < $rsFilialeAnz;$i++)
    {
         print "<tr><td><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Sonstiges&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
         print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
         print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>" . $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td><tr>";
    }
    print "</table>";
}
elseIf($rsFilialeAnz == 0) 
{
	print "<p class=HinweisText>Es konnte keine Informationen f�r die gesuchte Filiale gefunden werden</p>";
}
else 
{ // Eine Filiale
	     /**
         * Daten anzeigen
         */

		 	// Aktuelle Filiale speichern
	awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=" . $rsFiliale["FIL_ID"][0]);	

/***************************************
*	Inventurdaten
*****************************************/        
        // Daten f�r die Infoseite sammeln
	$SQL = "SELECT  filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, FIT_ID, FIF_Wert
				FROM filialinfostypen, filialinfostypengruppen, FilialInfos
				 WHERE  filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id
				 	AND	FIF_FIT_ID = FIT_ID
					AND FIT_FTG_ID = 6
	 			    AND FIF_FIL_ID = " . $rsFiliale["FIL_ID"][0] . "
				 ORDER BY FTG_ID, fit_sortierung, FIF_UserDat DESC
			";	// Nur Telefonnummern
	$rsFilialSonstiges = awisOpenRecordset($con, $SQL);
	$rsFilialSonstigesAnz = $awisRSZeilen;

     // �berschrift schreiben
	print "<table width=100% border=0><tr><td align=left>";
    print "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
    print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Sonstiges&Liste=True';></td></tr></table>";
    print "<hr>";

	print "<table width=100%>";

    for($i=0;$i<$rsFilialSonstigesAnz;$i++)
	{
		print "<tr>";
	
		print "<td width=200 " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIT_INFORMATION"][$i] . "</td>";		
		if($rsFilialSonstiges["FIT_ID"][$i]==91)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . ($rsFilialSonstiges["FIF_WERT"][$i]==0?"nein":"ja") . "</td>";		
		}
		else
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIF_WERT"][$i] . "</td>";		
		}
		print "</tr>";
	}
	unset($rsFilialSonstigesAnz);

	print "</table>";
	print "<hr>";
	print "<table border=0>";

/***************************************
*	Soll-Mitarbeiter
*****************************************/        
	
	$SQL = "SELECT  filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, FIT_ID, FIF_Wert, FIF_USERDAT
				FROM filialinfostypen, filialinfostypengruppen, FilialInfos
				 WHERE  filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id
				 	AND	FIF_FIT_ID = FIT_ID
					AND FIT_FTG_ID = 11 AND FIF_IMQ_ID = 8
	 			    AND FIF_FIL_ID = " . $rsFiliale["FIL_ID"][0] . "
				 ORDER BY FIF_UserDat DESC, FIF_FIT_ID
			";
	$rsFilialSonstiges = awisOpenRecordset($con, $SQL);
	$rsFilialSonstigesAnz = $awisRSZeilen;

	$Zeile=1;
    for($i=0;$i<$rsFilialSonstigesAnz;$i++)
	{
		print "<tr>";
		print "<td width=300" . (($Zeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIT_INFORMATION"][$i] ." (" . $rsFilialSonstiges["FIF_USERDAT"][$i] . ")</td>";		
		print "<td width=80" . (($Zeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIF_WERT"][$i] . "</td>";		
		++$i;	// N�chste Zeile daneben setzen
		print "<td width=250" . (($Zeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIT_INFORMATION"][$i] . "</td>";		
		print "<td width=80" . (($Zeile%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIF_WERT"][$i] . "</td>";		

		$Zeile++;

		print "</tr>";
	}

	print "</table><table border=0 width=100%><tr><td colspan=99><hr></td></tr>";
	
	$SQL = "SELECT  filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, FIT_ID, FIF_Wert, FIF_USERDAT, 
			   PER_NACHNAME, PER_VORNAME
			FROM filialinfostypen, filialinfostypengruppen, FilialInfos, Personal
			WHERE  filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id
			 	AND	FIF_FIT_ID = FIT_ID
				AND FIF_WERT = PER_Nr(+)
				AND FIT_FTG_ID = 10
	 		    AND FIF_FIL_ID = " . $rsFiliale["FIL_ID"][0] . "
			ORDER BY FTG_ID, fit_sortierung, FIF_UserDat DESC
			";	// Nur Telefonnummern
	$rsFilialSonstiges = awisOpenRecordset($con, $SQL);
	$rsFilialSonstigesAnz = $awisRSZeilen;
	
    for($i=0;$i<$rsFilialSonstigesAnz;$i++)
	{
		print "<tr>";
	
		print "<td width=200 " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIT_INFORMATION"][$i] . "</td>";		
		if($rsFilialSonstiges["PER_NACHNAME"][$i]!="")
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["PER_NACHNAME"][$i] . ", " . $rsFilialSonstiges["PER_VORNAME"][$i] . "</td>";		
		}
		else
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialSonstiges["FIF_WERT"][$i] . "</td>";		
		}
		print "</tr>";
	}


    print "</table>";
    
	print "</tr></table>";

}	// Auswahl oder Details

?>