<?php
global $AWIS_KEY1;

require_once('jpgraph/jpgraph_barcode.php');
require_once('register.inc.php');

$TextKonserven=array();
$TextKonserven[]=array('RFK','RFK_%');
$TextKonserven[]=array('RFF','RFF_%');
$TextKonserven[]=array('RFK','WirklichAbschliessen');
$TextKonserven[]=array('RFK','WirklichAbschliessenFiliale');
$TextKonserven[]=array('RFK','RueckfuehrungAbgeschlossenFiliale');
$TextKonserven[]=array('RFK','RueckfuehrungAbgeschlossenZentrale');
$TextKonserven[]=array('RFK','HinweisNegativbestand');
$TextKonserven[]=array('RFK','HinweisNegativbestandZentrale');
$TextKonserven[]=array('RFK','HinweisBestandsdifferenzen');
$TextKonserven[]=array('RFK','HinweisNegativbestandArtikel');
$TextKonserven[]=array('RFF','HinweisDifferenzenRueckfuehrung');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Abbrechen');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','Rueckfuehrungsmenge');
$TextKonserven[]=array('Wort','Filialbestand');
$TextKonserven[]=array('Fehler','%');
$TextKonserven[]=array('Fehler','err_FehlerAbschluss');
$TextKonserven[]=array('Fehler','err_EingabeGroesserNull');
$TextKonserven[]=array('Fehler','err_RueckfuehrungAbschliessen');
$TextKonserven[]=array('Wort','lbl_weiter');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$TXT_Abschliessen = $Form->LadeTexte($TextKonserven);

$Art= '';
$Hinweis = '';
$Form->DebugAusgabe(1,$_POST);

if (isset($_POST['cmd_VerbuchenZentrale_x']) or isset($_POST['cmd_VerbuchenZentrale2_x']))
{	
	$AWIS_KEY1=$_POST['txtRFK_KEY'];
	$Form->DebugAusgabe(1,$_POST);
	
	$DB->SetzeBindevariable('RFF','var_N0_rff_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
	$DB->SetzeBindevariable('RFF','var_T_rff_user', $AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);

	if (isset($_POST['cmd_VerbuchenZentrale_x']))
	{
		$DB->SetzeBindevariable('RFF','var_T_rff_bemerkungzentrale', 'Verschrottung',awisDatenbank::VAR_TYP_TEXT);		
	}
	else 
	{
		$DB->SetzeBindevariable('RFF','var_T_rff_bemerkungzentrale', 'Sortiments- und Lagerbereinigung',awisDatenbank::VAR_TYP_TEXT);		
	}
	
	$SQL = 'UPDATE RUECKFUEHRUNGSFILIALMENGENPOS ';
	$SQL.= 'SET RFF_MENGEZENTRALE = RFF_MENGEFILIALE ';
	$SQL.= ', RFF_BEMERKUNGZENTRALE=:var_T_rff_bemerkungzentrale';
	$SQL.= ', RFF_USER=:var_T_rff_user';
	$SQL.= ', RFF_USERDAT=SYSDATE';
	$SQL .= ' WHERE RFF_RFK_key=:var_N0_rff_rfk_key';

	$Form->DebugAusgabe(1,$SQL);
	
	if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'))===false)
	{
		awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
	}	
}

if(isset($_POST['cmdAbschliessenFilialeOK']))
{
	$Art = 'Filiale';
	$Key=$_POST['txtRFK_KEY'];
	$RFKKey=$_POST['txtRFK_KEY'];

	$AWIS_KEY1 = $Key;

	// Daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	$HinweisAbschliessen=false;
	
	$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
			
	$SQL = 'SELECT RFK_PERSNRFILIALE, RFK_BOXART, RFK_BOXEAN FROM RueckFuehrungsFilialmengenKopf WHERE RFK_key=:var_N0_rfk_key';
	
	$rsRFK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));				
	
	$Pflichtfelder = array('RFK_PERSNRFILIALE','RFK_BOXART');
	
	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if(($Pflichtfeld == 'RFK_BOXART' AND $rsRFK->FeldInhalt($Pflichtfeld) == '-1') or $rsRFK->FeldInhalt($Pflichtfeld) == '')	// Name muss angegeben werden
		{
			$Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFK'][$Pflichtfeld].'<br>';
			$HinweisAbschliessen=true;
		}
		
		if($Pflichtfeld == 'RFK_PERSNRFILIALE' AND ($rsRFK->FeldInhalt($Pflichtfeld) == '0' or $rsRFK->FeldInhalt($Pflichtfeld) == ''))	// Personalnummer-Filiale muss angegeben werden
		{
			$Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFK'][$Pflichtfeld].'<br>';
			$HinweisAbschliessen=true;
		}
	}

	//Pr�fen ob eine Menge < als 0 ist
	$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	
	$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE FROM RueckFuehrungsFilialmengenPos WHERE RFF_MENGEFILIALE <= 0 AND RFF_RFK_key=:var_N0_rfk_key';
	
	$rsRFF = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
					
	if ($rsRFF->AnzahlDatensaetze()>0)
	{
		while(!$rsRFF->EOF())
		{
		    $Fehler .= $TXT_Abschliessen['RFF']['RFF_AST_ATUNR'].' '.$rsRFF->FeldInhalt('RFF_AST_ATUNR').': '.$TXT_Abschliessen['Fehler']['err_EingabeGroesserNull'].'<br>';
			//$Fehler .= 'ATUNR: '.$rsRFF->FeldInhalt('RFF_AST_ATUNR') .'. Menge muss gr��er als 0 sein.<br>';
			$rsRFF->DSWeiter();
		}
	}
	 //Schau, ob ein W Artikel mit auf der R�ckf�hrung ist. Diese m�ssen immer gesondert zur�ckgef�hrt werden. 
	$SQLWPruefung = '
	        select * from(
            SELECT distinct nvl(upper(a.RUECKLIEFKZ),\'bla\') as RUECKLIEFKZ FROM RueckFuehrungsFilialmengenPos b
            left join 
            V_ARTIKEL_RUECKLIEFKZ a
            on a.AST_ATUNR = B.RFF_AST_ATUNR and a.GUELTIG=0
            WHERE RFF_RFK_key=' . $_POST['txtRFK_KEY'] . ')';
 	

	
	$RSWPruefung = $DB->RecordSetOeffnen($SQLWPruefung);
	if(($RSWPruefung->AnzahlDatensaetze())>1)
	{
    	while(!$RSWPruefung->EOF())
    	{
    	    $RSWPruefung->FeldInhalt('RUECKLIEFKZ');
    	    if ($RSWPruefung->FeldInhalt('RUECKLIEFKZ') == 'W')
    	    {
    	       $Fehler .= $TXT_Abschliessen['Fehler']['err_RueckfuehrungWArtikelAbschliesen'];  
    	    }
    	    
    	    $RSWPruefung->DSWeiter();
    	}
	}
	// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		$Form->ZeileStart();
		if ($HinweisAbschliessen==true)
		{
			$Form->Hinweistext($TXT_Abschliessen['Fehler']['err_FehlerAbschluss'].'<br>'.$Fehler);
		}
		else 
		{
			$Form->Hinweistext($Fehler);
		}		
		$Form->ZeileEnde();
		$Speichern=false;				
		
		$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();				
		die();				
		
		//die('<span class=HinweisText>'.$TXT_Abschliessen['Fehler']['err_FehlerAbschluss'].'<br>'.$Fehler.'</span>');
		//$Form->Hinweistext($TXT_Abschliessen['Fehler']['err_FehlerAbschluss'].'<br>'.$Fehler.'<br>');
		//$Speichern = false;
	}
	else 
	{
		$Speichern = true;
	}

	if ($Speichern == true)
	{
		$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
			
		$SQL = 'SELECT RFK_FIL_ID, RFF_AST_ATUNR, RFF_MENGEFILIALE';
		$SQL .= ' FROM RueckFuehrungsFilialmengenKopf';
		$SQL .= ' LEFT JOIN RueckFuehrungsFilialmengenPos on RFF_RFK_KEY = RFK_KEY';
		$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
	
		$rsRFF = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));				
			
		$Hinweis = '';
		$Negativbestand = array();
		
		while(!$rsRFF->EOF())
		{
			$DB->SetzeBindevariable('FBA', 'var_N0_fil_id', $rsRFF->FeldInhalt('RFK_FIL_ID'), awisDatenbank::VAR_TYP_GANZEZAHL);
			$DB->SetzeBindevariable('FBA', 'var_T_ast_atunr', $rsRFF->FeldInhalt('RFF_AST_ATUNR'), awisDatenbank::VAR_TYP_TEXT);
				
			$SQL = 'SELECT BESTAND FROM V_Filialbestand_Aktuell ';
			$SQL .= ' WHERE FIL_ID = :var_N0_fil_id';
			$SQL .= ' AND AST_ATUNR = :var_T_ast_atunr';
			
			$rsFIB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FBA'));

			if (($rsFIB->AnzahlDatensaetze()==0 and $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'))>0) or
				($rsFIB->AnzahlDatensaetze()>0 and $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEFILIALE'))))
			//if ($rsFIB->AnzahlDatensaetze() == 0 or $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEFILIALE')))
			{				
				$Hinweis .= $TXT_Abschliessen['RFF']['RFF_AST_ATUNR'].': '.$rsRFF->FeldInhalt('RFF_AST_ATUNR'). '   '. $TXT_Abschliessen['Wort']['Rueckfuehrungsmenge'].':'.$rsRFF->FeldInhalt('RFF_MENGEFILIALE').'  '.$TXT_Abschliessen['Wort']['Filialbestand'].':'.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').'<br>';				
				$Negativbestand[$rsRFF->FeldInhalt('RFF_AST_ATUNR')] = $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND'));					
			}
									
			$rsRFF->DSWeiter();			
		}		
		
		$BoxText = explode("|",$AWISSprachKonserven['RFK']['lst_RFK_BOXART']);
		
		foreach ($BoxText as $Box)
		{
			$Artbox = explode("~",$Box);
			if (intval($Artbox[0])==intval($_POST['txtRFK_BOXART']))
			{
				$Gitterbox = $Artbox[1];
			}				
		}
		
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_FIL_ID'),$_POST['txtRFK_FIL_ID']);
		//$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_BOXART'),$_POST['txtRFK_BOXART']);
		$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_BOXART'),$Gitterbox);
		$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_BOXEAN'),$_POST['txtRFK_BOXEAN']);
		$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_PERSNRFILIALE'),$_POST['txtRFK_PERSNRFILIALE']);
	}
	
}
elseif(isset($_POST['cmdAbschliessenZentraleOK']))
{
	$Art = 'Zentrale';
	$Key=$_POST['txtRFK_KEY'];
	$RFKKey=$_POST['txtRFK_KEY'];
		
	$AWIS_KEY1 = $Key;	
	/*
	// Daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	
	$SQL = 'SELECT distinct RFK_KEY, RFF_AST_ATUNR, RFF_LIE_NR, RFS_STATUS, RUECKLIEFKZ';
	$SQL .= ' FROM RueckFuehrungsFilialMengenKopf';
	$SQL .= ' LEFT JOIN RueckFuehrungsFilialMengenPos ON RFK_KEY = RFF_RFK_KEY';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSSTATUS RFS ON RFK_RFS_KEY = RFS_KEY';
	$SQL .= ' LEFT JOIN V_ARTIKEL_RUECKLIEFKZ ON RFF_AST_ATUNR = AST_ATUNR AND GUELTIG=0';
	$SQL .= ' WHERE RFK_KEY=0'.$AWIS_KEY1;
	$SQL .= ' AND RUECKLIEFKZ = \'L\'';
	
	$Form->DebugAusgabe(1,$SQL);
	
	$rsRFF = $DB->RecordSetOeffnen($SQL);								
		
	while(!$rsRFF->EOF())
	{
		$Pflichtfelder = array('RFF_LIE_NR');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($rsRFF->FeldInhalt($Pflichtfeld) == '')	// Lieferant muss angegeben werden
			{
				$Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFF'][$Pflichtfeld].' '.$TXT_Abschliessen['RFF']['RFF_AST_ATUNR'].': '.$rsRFF->FeldInhalt('RFF_AST_ATUNR').'<br>';
			}
		}
		$rsRFF->DSWeiter();												
	}
	
	if($Fehler!='')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($Fehler);
		$Form->ZeileEnde();
		$Speichern=false;				
		
		$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');					
		$Form->SchaltflaechenEnde();				
		die();		
	}		
	*/
	$Fehler = '';
	$PflichtFehler=false;
	
	$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	
	$SQL = 'SELECT RFK_PERSNRZENTRALE, RFK_DATUMABSCHLUSSFIL FROM RueckFuehrungsFilialmengenKopf WHERE RFK_key=:var_N0_rfk_key';
	
	$rsRFK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
	
	$Pflichtfelder = array('RFK_PERSNRZENTRALE');

	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if($rsRFK->FeldInhalt($Pflichtfeld) == '')	// Pflichtfeld darf nicht leer sein
		{
			$PflichtFehler=true;
			$Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFK'][$Pflichtfeld].'<br>';
		}
		
		if($Pflichtfeld == 'RFK_PERSNRZENTRALE' AND $rsRFK->FeldInhalt($Pflichtfeld) == '0')	// Personalnummer-Filiale muss angegeben werden
		{
			$Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFK'][$Pflichtfeld].'<br>';
			$HinweisAbschliessen=true;
		}
	}
	
	$AktDatum=date("d.m.Y");
	
	if ($Form->Format('D',$AktDatum) == $Form->Format('D',$rsRFK->FeldInhalt('RFK_DATUMABSCHLUSSFIL')))
	{
		$Fehler.= $TXT_Abschliessen['Fehler']['err_RueckfuehrungAbschliessen'];		
	}
	// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{		
		$Form->ZeileStart();
		if ($PflichtFehler==true)
		{
			$Form->Hinweistext($TXT_Abschliessen['Fehler']['err_FehlerAbschluss'].'<br>'.$Fehler);
		}
		else 
		{
			$Form->Hinweistext($Fehler);
		}
		//$Form->Hinweistext($Fehler);
		$Form->ZeileEnde();
		$Speichern=false;				
		
		$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();				
		die();	
		
		//die('<span class=HinweisText>Um die R�ckf�hrung abzuschliessen, m�ssen vorher erst folgende Felder ausgef�llt werden<br>'.$Fehler.'</span>');
		//$Form->Hinweistext($TXT_Abschliessen['Fehler']['err_FehlerAbschluss'].'<br>'.$Fehler).'<br>';
		//$Speichern = false;
	}
	else 
	{
		$Speichern = true;
	}

	if ($Speichern == true)
	{		
		$AnzahlDifferenzen=0;
		
		$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	
//		$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE';
//		$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENPOS';
//		$SQL.= ' WHERE RFF_RFK_KEY = :var_N0_rfk_key';
//		$SQL.= ' AND RFF_MENGEFILIALE <> RFF_MENGEZENTRALE';
		
		$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE FROM(';
		$SQL.= ' SELECT RFF_AST_ATUNR, SUM(RFF_MENGEFILIALE) AS RFF_MENGEFILIALE,';
		$SQL.= ' SUM(RFF_MENGEZENTRALE) AS RFF_MENGEZENTRALE';
		$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENPOS';
		$SQL.= ' WHERE RFF_RFK_KEY = :var_N0_rfk_key';
		$SQL.= ' GROUP BY RFF_AST_ATUNR)';
		$SQL.= ' WHERE RFF_MENGEFILIALE <> RFF_MENGEZENTRALE';
		
		$rsRFFP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
						
		if ($rsRFFP->AnzahlDatensaetze() > 0)
		{
			$AnzahlDifferenzen = $rsRFFP->AnzahlDatensaetze();			
		}
		
		$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
		
//		$SQL = 'SELECT RFK_FIL_ID, RFF_KEY, RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE, RFF_BEMERKUNGZENTRALE';//, FIB_BESTAND - RFF_MENGEFILIALE AS DIFFERENZ';
//		$SQL .= ' FROM RueckFuehrungsFilialmengenKopf';
//		$SQL .= ' LEFT JOIN RueckFuehrungsFilialmengenPos on RFF_RFK_KEY = RFK_KEY';
//		$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
		
		$SQL = 'SELECT RFK_FIL_ID, RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE, RFF_BEMERKUNGZENTRALE FROM(';
		$SQL .= ' SELECT RFK_FIL_ID, RFF_AST_ATUNR, SUM(RFF_MENGEFILIALE) AS RFF_MENGEFILIALE,';
		$SQL .= ' SUM(RFF_MENGEZENTRALE) AS RFF_MENGEZENTRALE,';
		$SQL .= ' DECODE(COUNT(TRIM(RFF_BEMERKUNGZENTRALE)),0,NULL,1) AS RFF_BEMERKUNGZENTRALE';
		$SQL .= ' FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
		$SQL .= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENPOS ON RFF_RFK_KEY = RFK_KEY';
		$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
		$SQL .= ' GROUP BY RFK_FIL_ID,  RFF_AST_ATUNR)';
	
		$rsRFF = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
			
		$Hinweis = '';
		$Negativbestand = array();
		
		while(!$rsRFF->EOF())
		{
			$DB->SetzeBindevariable('FIB', 'var_N0_fil_id', $rsRFF->FeldInhalt('RFK_FIL_ID'), awisDatenbank::VAR_TYP_GANZEZAHL);
			$DB->SetzeBindevariable('FIB', 'var_T_ast_atunr', $rsRFF->FeldInhalt('RFF_AST_ATUNR'), awisDatenbank::VAR_TYP_TEXT);
			//TODO: W raus von der pr�fung
			
			$SQLWPruefung = '
	        select upper(RUECKLIEFKZ) as RUECKLIEFKZ from
            V_ARTIKEL_RUECKLIEFKZ a
            WHERE GUELTIG = 0 and AST_ATUNR = \'' . $rsRFF->FeldInhalt('RFF_AST_ATUNR').'\'';
			//echo $SQLWPruefung;
			$RSWPruefung = $DB->RecordSetOeffnen($SQLWPruefung);
			
			while(!$RSWPruefung->EOF())
			{
			    if ($RSWPruefung->FeldInhalt('RUECKLIEFKZ')!='W') //Wenn nicht W, dann ganz normal behandeln und Fehler schmei�en
			    {
			        $SQL = 'SELECT (NVL(FIB_BESTAND,0)) as BESTAND';
			        $SQL.= ' FROM FILIALBESTAND';
			        $SQL.= ' WHERE FIB_AST_ATUNR = :var_T_ast_atunr';
			        $SQL.= ' AND FIB_FIL_ID = :var_N0_fil_id';
			        	
			        $rsFIB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIB'));
			        	
			        if ((($rsFIB->AnzahlDatensaetze()==0 and $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))>0) or
			            ($rsFIB->AnzahlDatensaetze()>0 and $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE')))) AND $rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE')=='')
			            //if (($rsFIB->AnzahlDatensaetze() == 0 or $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))) AND $rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE')=='')
			        {
			            // Wurden Fehler entdeckt? => Speichern abbrechen
			            $Pflichtfelder = array('RFF_BEMERKUNGZENTRALE');
			            foreach($Pflichtfelder AS $Pflichtfeld)
			            {
			                if($rsRFK->FeldInhalt($Pflichtfeld) == '')	// Name muss angegeben werden
			                {
			                    $Fehler .= $TXT_Abschliessen['Fehler']['err_KeinWert'].' '.$TXT_Abschliessen['RFF'][$Pflichtfeld].' '.$TXT_Abschliessen['RFF']['RFF_AST_ATUNR'].': '.$rsRFF->FeldInhalt('RFF_AST_ATUNR').'<br>';
			                }
			            }
			        
			            if($Fehler!='')
			            {
			                $Form->ZeileStart();
			                $Form->Hinweistext($TXT_Abschliessen['RFK']['HinweisNegativbestandArtikel'].'<br>'.$Fehler);
			                //$Form->Hinweistext($Fehler);
			                $Form->ZeileEnde();
			                $Speichern=false;
			                	
			                $Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
			                $Form->SchaltflaechenStart();
			                $Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
			                $Form->SchaltflaechenEnde();
			                die();
			        
			                //die('<span class=HinweisText>Negativbestand bei Artikel '.$rsRFF->FeldInhalt('RFF_AST_ATUNR').' '.$Fehler.'</span>');
			            }
			            else
			            {
			                $Speichern = true;
			            }
			            	
			        }
			        elseif((($rsFIB->AnzahlDatensaetze()==0 and $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))>0) or
			            ($rsFIB->AnzahlDatensaetze()>0 and $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE')))) AND $rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE')!='')
			            //elseif(($rsFIB->AnzahlDatensaetze() == 0 or $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND')) < $DB->FeldInhaltFormat('Z',$rsRFF->FeldInhalt('RFF_MENGEZENTRALE'))) AND $rsRFF->FeldInhalt('RFF_BEMERKUNGZENTRALE')!='')
			        {
			            $Hinweis .= $TXT_Abschliessen['RFF']['RFF_AST_ATUNR'].': '.$rsRFF->FeldInhalt('RFF_AST_ATUNR'). '   '. $TXT_Abschliessen['Wort']['Rueckfuehrungsmenge'].':'.$rsRFF->FeldInhalt('RFF_MENGEZENTRALE').'  '.$TXT_Abschliessen['Wort']['Filialbestand'].':'.($rsFIB->AnzahlDatensaetze()>0?$rsFIB->FeldInhalt('BESTAND'):'0').'<br>';
			            $Negativbestand[$rsRFF->FeldInhalt('RFF_AST_ATUNR')] = $DB->FeldInhaltFormat('Z',$rsFIB->FeldInhalt('BESTAND'));
			        }
			        
			        
			    }  
			 $RSWPruefung->DSWeiter();
			}
				
			$rsRFF->DSWeiter();
		}		
	}

	
	$Felder=array();
	//$Form->DebugAusgabe(1,$_POST);
	$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_FIL_ID'),$_POST['txtRFK_FIL_ID']);
	$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_PERSNRZENTRALE'),$_POST['txtRFK_PERSNRZENTRALE']);
}
elseif(isset($_POST['cmdAbschliessenOK']))	// Abschluss durchf�hren
{
	$AWIS_KEY1 = $_POST['txtKey'];
	
	$SQL = '';
	switch ($_POST['txtArt'])
	{
		case 'Filiale':	

			//Pr�fen, ob die R�ckf�hrung evtl. schon abgeschlossen ist
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL = 'SELECT RFS_STATUS';
			$SQL .= ' FROM RueckFuehrungsFilialmengenkopf';
			$SQL .= ' LEFT JOIN RueckFuehrungsStatus on RFS_KEY = RFK_RFS_KEY';
			$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
						
			$rsRFK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
			
			if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS'))>=31)
			{										
				$Form->ZeileStart();
				$Form->Hinweistext($TXT_Abschliessen['RFK']['RueckfuehrungAbgeschlossenFiliale']);
				$Form->ZeileEnde();
				
				$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();	
			}
						
			// Filialbestand updaten, wenn abgeschlossen wird (Filialbestand < R�ckf�hrungsmenge)
			$Felder = explode(';',awis_NameInArray($_POST,'txtNegativ',1,1));
			
			$Form->DebugAusgabe(1,$Felder);
			
			foreach($Felder AS $Feld)
			{	
				if(isset($_POST[$Feld]))
				{
					$Form->ZeileStart();
					$Form->DebugAusgabe(1,$Feld);
					$Form->DebugAusgabe(1,$_POST[$Feld]);
					$Form->ZeileEnde();
					
					$DB->SetzeBindevariable('RFF','var_N0_rff_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFF','var_N0_rff_bestandabschlfil', $_POST[$Feld],awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFF','var_T_rff_user', $AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFF','var_T_rff_ast_atunr', substr($Feld,11,6),awisDatenbank::VAR_TYP_TEXT);
					
					$SQL = 'UPDATE RueckFuehrungsFilialmengenPos';
					$SQL.= ' SET RFF_BESTANDABSCHLFIL=:var_N0_rff_bestandabschlfil';
					$SQL .= ', RFF_user=:var_T_rff_user';
					$SQL .= ', RFF_userdat=sysdate';
					$SQL .= ' WHERE RFF_RFK_key=:var_N0_rff_rfk_key';
					$SQL .= ' AND RFF_AST_ATUNR = :var_T_rff_ast_atunr';
					
					$Form->DebugAusgabe(1,$SQL);
					
					if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'))===false)
					{
						awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
					}
				}
			}
			
			$DB->SetzeBindevariable('RFF','var_N0_rff_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
			
			//R�ckf�hrungskennzeichen mit speichern
			$SQL = 'UPDATE RueckFuehrungsFilialmengenpos';
			$SQL.= ' SET RFF_RUECKLIEFKZ = NVL((select nvl(rueckliefkz,\' \') as rueckliefkz from v_artikel_rueckliefkz where ast_atunr = rff_ast_atunr and gueltig=0),\' \')';
			$SQL.= ' WHERE RFF_RFK_key=:var_N0_rff_rfk_key';					
			
			$Form->DebugAusgabe(1,$SQL);
			
			if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'))===false)
			{
				awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
			}

			
			//R�ckf�hrungsnummer zusammenbauen
			$RNRVORLAUF = '99';
			
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$rsRFK = $DB->RecordSetOeffnen('SELECT RFK_FIL_ID, RFK_LFDNR FROM RueckFuehrungsFilialmengenKopf WHERE RFK_key=:var_N0_rfk_key',$DB->Bindevariablen('RFK'));			
			$FIL_ID=$rsRFK->FeldInhalt('RFK_FIL_ID');
			$LFDNR=$rsRFK->FeldInhalt('RFK_LFDNR');
			
			$RNRFILIALE = str_pad($FIL_ID,4,'0',STR_PAD_LEFT);
			$RNRLFDNR = str_pad($LFDNR,6,'0',STR_PAD_LEFT);			
			
			$RNR = $RNRVORLAUF.''.$RNRFILIALE.''.$RNRLFDNR;
			
			$EAN_CLASS = new BarcodeEncode_EAN13();
			
			if ($EAN_CLASS->Validate($RNR))
			{
				$EAN_CODE = $EAN_CLASS->Enc($RNR);
				$EAN_SAVE = $EAN_CODE->iData;
			}
			//$Form->DebugAusgabe(1,$EAN_CODE,$EAN_SAVE);

			$DB->SetzeBindevariable('RFK','var_N0_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
			$DB->SetzeBindevariable('RFK','var_T_rfk_rueckfuehrungsnr', $EAN_SAVE,awisDatenbank::VAR_TYP_TEXT);
			$DB->SetzeBindevariable('RFK','var_T_rfk_user', $AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);
			
			$SQL = 'UPDATE RueckFuehrungsFilialMengenKopf';
			$SQL .= ' SET RFK_RFS_KEY = (select RFS_KEY FROM RueckFuehrungsStatus where RFS_STATUS = 31)';			
			$SQL .= ', RFK_RUECKFUEHRUNGSNR=:var_T_rfk_rueckfuehrungsnr';
			$SQL .= ', RFK_datumabschlussfil=sysdate';
			$SQL .= ', RFK_user=:var_T_rfk_user';
			$SQL .= ', RFK_userdat=sysdate';
			$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
			
			if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFK'))===false)
			{
				awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
			}
			//$Form->DebugAusgabe(1,$SQL);
			
			//Pr�fen ob Filiale eine AX-Filiale ist
			$DB->SetzeBindevariable('FIF', 'var_N0_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);
			$DB->SetzeBindevariable('FIF', 'var_N0_fif_fit_id', intval(910), awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL = 'SELECT FIF_WERT AS AXFILIALE FROM FILIALINFOS WHERE FIF_FIL_ID=:var_N0_fil_id AND FIF_FIT_ID=:var_N0_fif_fit_id';
			$Form->DebugAusgabe(1,$SQL);
			
			$rsAXFiliale = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIF'));						
			
			//Wenn Filiale auf AX umgestellt ist, Datei erzeugen
			if ($rsAXFiliale->FeldInhalt('AXFILIALE')=='J')
			{
				$SQL='BEGIN P_MDE.PROC_CREA_RUECK_AX_EXPORT ( '.$FIL_ID.', '.$EAN_SAVE.'); COMMIT; END;';				
	
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim ausf�hren der Filialtransitsetzung',200907280956,$SQL,2);
				}							
								
			}			
			//Wenn Filiale nicht auf AX umgestellt ist, dann Transit direkt in der Filiale bef�llen
			else 
			{
				$SQL='BEGIN P_MDE.PROC_CREA_FILIAL_TRANSIT ( '.$FIL_ID.', '.$EAN_SAVE.'); COMMIT; END;';
	
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim ausf�hren der Filialtransitsetzung',200810281615,$SQL,2);
				}							
			}
									
			
			//Pr�fen, ob Filiale das Recht hat, eine Abfallentsorgung f�r Gasflaschen anzulegen			
			//Schweiz nicht!
			$DB->SetzeBindevariable('FIL', 'var_N0_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$rsFilLand = $DB->RecordSetOeffnen('SELECT FIL_LAN_WWSKENN FROM FILIALEN WHERE FIL_ID=:var_N0_fil_id',$DB->Bindevariablen('FIL'));						
			
			// 22.05.2013 OP: Lt. Hr. Vollath T. werden die Gasflaschen in der Schweiz mit einer anderen ATUNR
			// Rueckgefuehrt. Die Gasflaschen sind mit einem Papier in der Fahrzeug-Begleitmappe abgedeckt.
			// Daher braucht auf dem Lieferschein nichts zusaetzliches angedruckt werden.
			if($AWISBenutzer->HatDasRecht(4302)!=0 AND $rsFilLand->FeldInhalt('FIL_LAN_WWSKENN') != 'SUI')
			{			
				//Pr�fen, ob Gase in der R�ckf�hrung vorhanden sind
				$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
				$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_USER, RFF_USERDAT';
				$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENPOS';
				$SQL.= ' INNER JOIN ADRSTAMM ON RFF_AST_ATUNR = ADS_AST_ATUNR AND ADS_ADRART = 3';
				$SQL.= ' WHERE RFF_RFK_KEY = :var_N0_rfk_key';			
				
				$rsGase = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
                
				//Wenn Gase mit in der R�ckf�hrung vorhanden sind, dann Eintrag in ADR - Tabellen
				if ($rsGase->AnzahlDatensaetze()>0)
				{
					// Pr�fe ob ADR-Eintrag f�r RFK_KEY (AWIS_KEY1) vorhanden und hole ggf. vorhandene ADK_VORGANGNR
					$DB->SetzeBindevariable('ADK', 'var_N0_adk_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
					
					$rsVorgangnrADR = $DB->RecordSetOeffnen('SELECT ADK_VORGANGNR FROM ADRKOPF WHERE ADK_RFK_KEY=:var_N0_adk_rfk_key',$DB->Bindevariablen('ADK'));					
					
					if ($rsVorgangnrADR->AnzahlDatensaetze()>0)
					{
						$Vorgangnr = $rsVorgangnrADR->FeldInhalt('ADK_VORGANGNR');
					}
					else
					{
						// Erstelle neue ADK_VORGANGNR f�r Filiale
						$DB->SetzeBindevariable('FIL', 'var_N0_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);

                        $rsVorgangnr = $DB->RecordSetOeffnen('SELECT ADV_ID as MAXVORGANGNR FROM ADRVORGANG WHERE ADV_FILID=:var_N0_fil_id',$DB->Bindevariablen('FIL'));
                        $Vorgangnr = $DB->FeldInhaltFormat('Z',$rsVorgangnr->FeldInhalt('MAXVORGANGNR'))+1;

                        if($Vorgangnr>1) {
                            $rsVorgangnr = $DB->RecordSetOeffnen('UPDATE ADRVORGANG set ADV_ID='. $Vorgangnr .' WHERE ADV_FILID = '. $DB->FeldInhaltFormat('Z',$FIL_ID));
                        } else {
                            $rsVorgangnr = $DB->RecordSetOeffnen('INSERT INTO ADRVORGANG (ADV_ID, ADV_FILID) VALUES ('. $Vorgangnr .', '. $DB->FeldInhaltFormat('Z',$FIL_ID).')');
                        }
					}
							
					$DB->SetzeBindevariable('FIL', 'var_N0_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);
					
					$rsFilLager = $DB->RecordSetOeffnen('SELECT UPPER(FIL_LAGERKZ) AS LAGER FROM FILIALEN WHERE FIL_ID=:var_N0_fil_id',$DB->Bindevariablen('FIL'));
					$FilLager = $rsFilLager->FeldInhalt('LAGER');
						
					$Entsorgernr = '';
										
					if ($FilLager == 'N')
					{
						$Entsorgernr = 'I363W1003';
					}
					elseif($FilLager == 'L')
					{
						$Entsorgernr = 'E97497404';
					}
					
					$DB->SetzeBindevariable('FIF', 'var_N0_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('FIF', 'var_N0_fif_fit_id', intval(911), awisDatenbank::VAR_TYP_GANZEZAHL);
					
					$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=:var_N0_fif_fit_id AND FIF_FIL_ID=:var_N0_fil_id';
					$rsFilinfoADR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIF'));
					
					$Fehler = '';
					
					$DB->SetzeBindevariable('ADK', 'var_N0_adk_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('ADK', 'var_N0_adk_fil_id', $FIL_ID, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('ADK', 'var_N0_adk_vorgangnr', $Vorgangnr, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('ADK', 'var_N0_adk_art', 3, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_status', 'A', awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_erzeugernr', $rsFilinfoADR->FeldInhalt('FIF_WERT'), awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_befoerderernr', 'I36T0250', awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_entsorgernr', $Entsorgernr, awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_lagerkz', $FilLager, awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('ADK', 'var_T_adk_user', $AWISBenutzer->BenutzerName() , awisDatenbank::VAR_TYP_TEXT);
					
					$SQL = 'MERGE INTO AWIS.ADRKOPF D USING ';
					$SQL .='(SELECT :var_N0_adk_rfk_key AS ADK_RFK_KEY FROM DUAL) S ';
					$SQL .='ON (D.ADK_RFK_KEY = S.ADK_RFK_KEY) ';
					$SQL .='WHEN MATCHED THEN UPDATE SET ';
					$SQL .='D.ADK_FIL_ID = :var_N0_adk_fil_id, ';
					$SQL .='D.ADK_ART = :var_N0_adk_art, ';
					$SQL .='D.ADK_VORGANGNR = :var_N0_adk_vorgangnr, ';
					$SQL .='D.ADK_STATUS = :var_T_adk_status, ';
					$SQL .='D.ADK_DATUMABSCHLUSS = SYSDATE, ';
					$SQL .='D.ADK_ERZEUGERNR = :var_T_adk_erzeugernr, ';
					$SQL .='D.ADK_BEFOERDERERNR = :var_T_adk_befoerderernr, ';
					$SQL .='D.ADK_ENTSORGERNR = :var_T_adk_entsorgernr, ';
					$SQL .='D.ADK_LAGERKZ = :var_T_adk_lagerkz, ';
					$SQL .='D.ADK_USER = :var_T_adk_user, ';
					$SQL .='D.ADK_USERDAT = SYSDATE ';
					$SQL .='WHEN NOT MATCHED THEN INSERT ( ';
					$SQL .='D.ADK_FIL_ID, D.ADK_ART, D.ADK_VORGANGNR, D.ADK_STATUS, D.ADK_DATUMERSTELLUNG, ';
					$SQL .='D.ADK_DATUMABSCHLUSS, D.ADK_ERZEUGERNR, D.ADK_BEFOERDERERNR, D.ADK_ENTSORGERNR, ';
					$SQL .='D.ADK_LAGERKZ, D.ADK_RFK_KEY, D.ADK_USER, D.ADK_USERDAT) ';
					$SQL .='VALUES ( ';
					$SQL .=':var_N0_adk_fil_id, :var_N0_adk_art, :var_N0_adk_vorgangnr, :var_T_adk_status, SYSDATE, ';
					$SQL .='SYSDATE, :var_T_adk_erzeugernr, :var_T_adk_befoerderernr, :var_T_adk_entsorgernr, ';
					$SQL .=':var_T_adk_lagerkz, :var_N0_adk_rfk_key, :var_T_adk_user, SYSDATE)';
					
					if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('ADK'))===false)
					{
						awisErrorMailLink('adr_speichern.php',1,$awisDBError['message'],'200910050904');
						die();
					}

					$DB->SetzeBindevariable('ADK', 'var_N0_adk_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
					
					$SQL='SELECT ADK_KEY FROM ADRKOPF WHERE ADK_RFK_KEY=:var_N0_adk_rfk_key';
					$rsKey = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ADK'));
					
					$ADKKey=$rsKey->FeldInhalt('ADK_KEY');			
					
					//Positionen eintragen
					while(!$rsGase->EOF())
					{
						//ADS_KEY ermitteln					
						$DB->SetzeBindevariable('ADR', 'var_T_ast_atunr', $rsGase->FeldInhalt('RFF_AST_ATUNR'), awisDatenbank::VAR_TYP_TEXT);
						
						$SQL = 'SELECT ADS_KEY, ADS_ZULADGEW FROM ADRSTAMM WHERE ADS_AST_ATUNR=:var_T_ast_atunr';					
						$rsADR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ADR'));	
						
						$DB->SetzeBindevariable('ADP', 'var_N0_adp_adk_key', $ADKKey, awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('ADP', 'var_N0_adp_ads_key', $rsADR->FeldInhalt('ADS_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('ADP', 'var_N0_adp_menge', $rsGase->FeldInhalt('RFF_MENGEFILIALE'), awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('ADP', 'var_N0_adp_gewicht', $rsADR->FeldInhalt('ADS_ZULADGEW')*$rsGase->FeldInhalt('RFF_MENGEFILIALE'), awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('ADP', 'var_N0_adp_user', $AWISBenutzer->BenutzerName(), awisDatenbank::VAR_TYP_TEXT);
						
						$SQL = 'MERGE INTO AWIS.ADRPOS D USING ( ';
						$SQL .='SELECT :var_N0_adp_adk_key AS ADP_ADK_KEY, :var_N0_adp_ads_key AS ADP_ADS_KEY FROM DUAL) S ';
						$SQL .='ON (D.ADP_ADK_KEY = S.ADP_ADK_KEY AND D.ADP_ADS_KEY = S.ADP_ADS_KEY) ';
						$SQL .='WHEN MATCHED THEN UPDATE SET ';
						$SQL .='D.ADP_MENGE = :var_N0_adp_menge, ';
						$SQL .='D.ADP_GEWICHT = :var_N0_adp_gewicht, ';
						$SQL .='D.ADP_USER = :var_N0_adp_user, ';
						$SQL .='D.ADP_USERDAT = SYSDATE ';
						$SQL .='WHEN NOT MATCHED THEN INSERT ( ';
						$SQL .='D.ADP_ADK_KEY, D.ADP_ADS_KEY, ';
						$SQL .='D.ADP_MENGE, D.ADP_GEWICHT, D.ADP_USER, D.ADP_USERDAT) ';
						$SQL .='VALUES ( ';
						$SQL .=':var_N0_adp_adk_key, :var_N0_adp_ads_key, ';
						$SQL .=':var_N0_adp_menge, :var_N0_adp_gewicht, :var_N0_adp_user, SYSDATE) ';
						
						if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('ADP'))===false)
						{					
							throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),200909161231,$SQL,2);					
						}
						
						$rsGase->DSWeiter();
					}				
				}
			}											
			break;
		case 'Zentrale':
			
			//Pr�fen, ob die R�ckf�hrung evtl. schon abgeschlossen ist
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL = 'SELECT RFS_STATUS';
			$SQL .= ' FROM RueckFuehrungsFilialmengenkopf';
			$SQL .= ' LEFT JOIN RueckFuehrungsStatus on RFS_KEY = RFK_RFS_KEY';
			$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
						
			$rsRFK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
			
			if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS'))>=32)
			{										
				$Form->ZeileStart();
				$Form->Hinweistext($TXT_Abschliessen['RFK']['RueckfuehrungAbgeschlossenZentrale']);
				$Form->ZeileEnde();
				
				$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();					
			}
			
			// Filialbestand updaten, wenn abgeschlossen wird (Filialbestand < R�ckf�hrungsmengezentrale)
			$Felder = explode(';',awis_NameInArray($_POST,'txtNegativ',1,1));
			
			$Form->DebugAusgabe(1,$Felder);
			
			foreach($Felder AS $Feld)
			{	
				if(isset($_POST[$Feld]))
				{
					$Form->ZeileStart();
					$Form->DebugAusgabe(1,$Feld);
					$Form->DebugAusgabe(1,$_POST[$Feld]);
					$Form->ZeileEnde();
					
					$DB->SetzeBindevariable('RFF','var_N0_rff_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFF','var_N0_rff_bestandabschlzen', $_POST[$Feld],awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFF','var_T_rff_user', $AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFF','var_T_rff_ast_atunr', substr($Feld,11,6),awisDatenbank::VAR_TYP_TEXT);
					
					$SQL = 'UPDATE RueckFuehrungsFilialmengenPos';
					$SQL.= ' SET RFF_BESTANDABSCHLZEN=:var_N0_rff_bestandabschlzen';
					$SQL .= ', RFF_user=:var_T_rff_user';
					$SQL .= ', RFF_userdat=sysdate';
					$SQL .= ' WHERE RFF_RFK_key=:var_N0_rff_rfk_key';
					$SQL .= ' AND RFF_AST_ATUNR = :var_T_rff_ast_atunr';
					
					$Form->DebugAusgabe(1,$SQL);
					
					if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'))===false)
					{
						awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
					}
				}
			}
			
			$DB->SetzeBindevariable('RFK','var_N0_rfk_key', '0'.$AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
			$DB->SetzeBindevariable('RFK','var_T_rfk_user', $AWISBenutzer->BenutzerName(),awisDatenbank::VAR_TYP_TEXT);
			
			$SQL = 'UPDATE RueckFuehrungsFilialMengenKopf';
			$SQL .= ' SET RFK_RFS_KEY = (select RFS_KEY FROM RueckFuehrungsStatus where RFS_STATUS = 32)';			
			$SQL .= ', RFK_datumabschlusszen=sysdate';
			$SQL .= ', RFK_user=:var_T_rfk_user';
			$SQL .= ', RFK_userdat=sysdate';
			$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
			//$Form->DebugAusgabe(1,$SQL);		
			
			if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFK'))===false)
			{
				awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
			}
			
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$rsRFK = $DB->RecordSetOeffnen('SELECT RFK_FIL_ID, RFK_RUECKFUEHRUNGSNR FROM RUECKFUEHRUNGSFILIALMENGENKOPF WHERE RFK_KEY=:var_N0_rfk_key',$DB->Bindevariablen('RFK'));
			$FIL_ID=$rsRFK->FeldInhalt('RFK_FIL_ID');
			$RUECKFUEHRUNGSNR=$rsRFK->FeldInhalt('RFK_RUECKFUEHRUNGSNR');
			
			$SQL='BEGIN P_MDE.PROC_CREA_RUECK_WWS_EXPORT ( '.$FIL_ID.', '.$RUECKFUEHRUNGSNR.'); COMMIT; END;';

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim ausf�hren der Filialtranitsetzung',200810281616,$SQL,2);
			}
			
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
//			$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE';
//			$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
//			$SQL.= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENPOS ON RFF_RFK_KEY = RFK_KEY';
//			$SQL.= ' WHERE RFK_KEY = :var_N0_rfk_key';	
//			$SQL.= ' AND RFF_MENGEZENTRALE <> RFF_MENGEFILIALE';
			
			$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE FROM(';
			$SQL.= ' SELECT RFF_AST_ATUNR, SUM(RFF_MENGEFILIALE) AS RFF_MENGEFILIALE, SUM(RFF_MENGEZENTRALE) AS RFF_MENGEZENTRALE';
			$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
			$SQL.= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENPOS ON RFF_RFK_KEY = RFK_KEY';
			$SQL.= ' WHERE RFK_KEY = :var_N0_rfk_key';
			$SQL.= ' GROUP BY RFF_AST_ATUNR)';
			$SQL.= ' WHERE RFF_MENGEZENTRALE <> RFF_MENGEFILIALE';

			$rsDifferenzen = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFK'));
			$rsDifferenzenAnzahl = $rsDifferenzen->AnzahlDatensaetze();
			
			if ($rsDifferenzenAnzahl > 0)
			{
				$Form->DebugAusgabe(1,$rsDifferenzenAnzahl);
				
				$SQL='SELECT P_MDE.FUNC_MAIL_FIL_RF_DIFFERENZ ( '.$FIL_ID.', '.$RUECKFUEHRUNGSNR.') as RUECKGABE FROM DUAL';
				
				$rsMail = $DB->RecordSetOeffnen($SQL);
				
				if($rsMail->FeldInhalt('RUECKGABE') == '1')
				{
					throw new awisException('Fehler beim ausf�hren der Mailversendung von Differenzen',200810291300,$SQL,2);
				}
			}
			
			break;
		default:
			break;
	}

	//if($SQL !='')
	//{
	//	if($DB->Ausfuehren($SQL)===false)
	//	{
	//		awisErrorMailLink('rueckfuehrungsbearbeitung_abschliessen_1',1,$awisDBError['messages'],'');
	//	}
	//}
}

if($Art!='' and $Speichern == true)
{
	$Form->SchreibeHTMLCode('<form name=frmAbschliessen action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
	$Form->Formular_Start();
	
	if ($Art == 'Filiale')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['RFK']['WirklichAbschliessenFiliale']);
		$Form->ZeileEnde();	
	}
	else 
	{
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['RFK']['WirklichAbschliessen']);
		$Form->ZeileEnde();	
	}	

	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}
	
	if($Hinweis!='')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['RFK']['HinweisNegativbestand'].'<br>'.$Hinweis);
		$Form->ZeileEnde();
		
		if ($Art == 'Filiale')
		{
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_Abschliessen['RFK']['HinweisBestandsdifferenzen']);
			$Form->ZeileEnde();
		}
	}
	
	if ($Art=='Zentrale' and $AnzahlDifferenzen > 0)
	{
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['RFF']['HinweisDifferenzenRueckfuehrung'].$AnzahlDifferenzen);		
		$Form->ZeileEnde();		
	}
	
	for ($x = 0; $x < sizeof($Negativbestand); ++$x)
	{		
	     $Form->Erstelle_HiddenFeld('Negativ_'.key($Negativbestand),current($Negativbestand));
	     next($Negativbestand);	     
	}
	
	$Form->Erstelle_HiddenFeld('RFK_Key',$RFKKey);
	$Form->Erstelle_HiddenFeld('Art',$Art);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdAbschliessenOK','','',$TXT_Abschliessen['Wort']['Ja'],'','');
	$Form->Schaltflaeche('submit','cmdAbschliessenAbbrechen','','',$TXT_Abschliessen['Wort']['Nein'],'');	
	$Form->ZeileEnde();
	
	

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>