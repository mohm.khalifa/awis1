<?php
//**************************************************************
// Liefert die Daten f�r das Verbundfeld in CRM Details
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


if(!isset($_GET['Abgeglichen']))
{
	die('##PARAM##');
}
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// SQL f�r Dropdown mit UNION, 1. SQL => �berschriftenzeile; 2. SQL => Daten
$SQL =  'select ' .
		'\'ID\' as BELEGNR, ' .        // 1. Spalte (siehe Kommentar f�r 1. Spalte nach UNION)
		'regexp_replace(rpad(\'Belegnummer\', 15,\'�\'),\'�\',\'�\') || \' | \' || \'Belegdatum\' || \' | \' || \'Anz.\'as WERT  , ' .   // 2. alle �berschriften-Teile verkn�pft
		'to_date(\'01.01.1970\', \'DD.MM.YYYY\') as order_datum, 0 as order_anz ' .     // Spalte 3 (hier dummy), damit gleiche Spaltenanzahl der beiden SQLs
		'from dual ' .
		'union ' .
		'select ' .
		'tgd.tgd_belegnr AS BELEGNR,' .            // ganze Datenmenge (weil kein KEY wegen Gruppenfunktion) => $delim f�r nachtr�gliches exploden, um zur�ck auf die Daten zu kommen
		'regexp_replace(rpad(tgd.tgd_belegnr, 15,\'�\'),\'�\',\'�\') || \' | \' || to_char(tgd.tgd_belegdatum, \'DD.MM.YYYY\') || \' | \' ||lpad(count(*), 4, \'0\') AS WERT , ' .     // ganze Datenmenge zur Ansicht
		'tgd.tgd_belegdatum as order_datum, count(*) as order_anz ' .       // mitgezogen, um nach diesen gruppieren zu k�nnen
		'from tuevgesellschaftendaten tgd ';
if ($_GET['Abgeglichen'] or $_GET['FilterBezahlt'])
{
	$SQL .= 'inner join tuevabgleich tag on tag.tag_tgd_key = tgd.tgd_key ';
}

$Bedingung = '';
if (isset($_GET['Abgeglichen']) && $_GET['Abgeglichen']==1)
{
	$Bedingung .= 'and tgd.tgd_abgeglichen = 1 ';
	$Bedingung .= 'and tag_userdat >= (sysdate - 366)';
}
else
{
	$Bedingung .= 'and tgd.tgd_abgeglichen = 0 ';
}


if($_GET['FILTER']!='')
{
	$Bedingung .= 'and tgd.tgd_belegnr'.$DB->LikeOderIst(strtoupper($_GET['FILTER']).'*');
}



/*
 if ($FilterBezahlt)
 {
$Bedingung .= 'and tag.tag_bezahlt = 0 ';
}
*/
if ($Bedingung != '')
{
	$SQL .= ' WHERE ' . substr($Bedingung, 4);
}

$SQL .=     'group by tgd.tgd_belegnr, tgd.tgd_belegdatum ' .
		'order by order_datum, order_anz';


$rsTAG=$DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
$DS=0;
if(isset($_GET['ZUSATZ'])!='')
{
	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else 
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}
while(!$rsTAG->EOF())
{
	if(++$DS>200)
	{
		break;
	}		// AND $rsTAG->FeldInhalt('BELEGNR')==$_GET['WERT'])
	
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsTAG->FeldInhalt('BELEGNR').'|'.$rsTAG->FeldInhalt('ORDER_DATUM').'">'.$rsTAG->FeldInhalt('WERT').'</option>';
	}
	else 
	{
		echo '#+#'.$rsTAG->FeldInhalt('BELEGNR').'|'.$rsTAG->FeldInhalt('ORDER_DATUM').'#+#'.$rsTAG->FeldInhalt('WERT').'';
	}
	
	$rsTAG->DSWeiter();
}
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else 
	{
		echo '#+##+#...';
	}
}
?>