<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $meldung;

$TextKonserven=array();
$TextKonserven[]=array('HRK','%');
$TextKonserven[]=array('BEW','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_DatumEndeVorAnfang');
$TextKonserven[]=array('Fehler','err_DatumAbstandTage');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');


$TXT_Speichern = $Form->LadeTexte($TextKonserven);
$Werkzeuge = new awisWerkzeuge();

$Recht40004 = $AWISBenutzer->HatDasRecht(40004);


try
{
	if(isset($_GET['Seite']) and $AWIS_KEY1 != '-1')
	{
		switch($_GET['Seite'])
		{
			case 'Konto':
				if($_POST['txtZKEY'] <> -1)
				{
					$vzweckleer = 0;
					$zileer = 0;
					$UpdateFelder = $Form->NameInArray($_POST, 'txtBES_',1,1);
					$UpdateFelder = explode(';',$UpdateFelder);
					$FeldListe = '';
					$Anzahl = 0;
					foreach($UpdateFelder AS $Inhalt)
					{
						$Feld = substr($Inhalt,3);
						if($Feld == 'BES_VERWENDUNGSZWECK')
						{
							$vzweckleer = 1;
						}
						if($Feld == 'BES_ZUSATZINFO')
						{
							$zileer = 1;
						}
						$Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
						if((isset($_POST['old'.$Feld]) or $Praefix == 'suc')and isset($_POST[$Praefix.$Feld])) //Sucfelder haben kein old
						{
							$WertNeu=$_POST[$Praefix.$Feld]!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST[$Praefix.$Feld]:'';
							$WertAlt=$Praefix=='txt'?$_POST['old'.$Feld]:'';

							if ($WertAlt != $WertNeu)
							{
								$Anzahl ++;
							}
						}
					}


					$SQL="begin :meldung:= zahl_vers.aend_zahlungen_vers_new(".$Anzahl.",'".$_POST['txtZKEY']."','".$_POST['txtBEARBNR']."','".($_POST['txtBES_BUCHUNGSART']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_BUCHUNGSART']:'')."','".str_replace(',','.',$_POST['txtBES_BETRAG'])."','".$_POST['txtBES_VERWENDUNGSZWECK']."','".$_POST['txtBES_DATUM']."','".$_POST['txtVS_ID']."','".$_POST['txtBES_VERSNR']."','".($_POST['txtBES_ART']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_ART']:'')."','".($_POST['txtBES_STATUS']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_STATUS']:'')."',".$vzweckleer.",'".$_POST['txtBES_ZUSATZINFO']."',".$zileer;
					$SQL.= ",'".$AWISBenutzer->BenutzerName()."',sysdate);end;";
					//$SQL="begin :meldung :=zahl_vers.aend_zahlungen_vers_new(1,'113560','000010004','','76.79','','','','','','',0,'',0);end;";

				}
				else
				{
					$SQL="begin :meldung :=zahl_vers.zahlungen_vers('".$_POST['txtBEARBNR']."','".($_POST['txtBES_BUCHUNGSART']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_BUCHUNGSART']:'')."',".$DBSCHAD->FeldInhaltFormat('N2',str_replace(',','.',$_POST['txtBES_BETRAG'])).",'".$_POST['txtBES_VERWENDUNGSZWECK']."','".$_POST['txtBES_DATUM']."','".$_POST['txtBES_ZUSATZINFO']."','".$AWISBenutzer->BenutzerName()."',sysdate";


					if($_POST['txtBES_BUCHUNGSART'] == 2)
					{
						$SQL.=",'".$_POST['txtBES_VERSNR']."','".($_POST['txtBES_ART']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_ART']:'')."','".($_POST['txtBES_STATUS']!=$TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBES_STATUS']:'')."'";
					}

					$SQL.=");end;";
				}
				if($SQL != '')
				{
                    if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
                    {
                        $verbindung=oci_connect("schaddev09","schad","AWISPROD_SH");
                    }
					elseif($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING)
                    {
                        $verbindung=oci_connect("schaddev09","schad","AWISSTAG_SH");
                    }else{
                        $verbindung=oci_connect("schaddev09","schad","AWISENTWICK_SH");
                    }
					$anweisung=ociparse($verbindung,$SQL);
					ocibindbyname($anweisung,":meldung",$meldung,1000);
					$nls=ociparse($verbindung,"ALTER SESSION SET NLS_DATE_FORMAT = 'DD.MM.YYYY HH24:MI:SS'");
					ociexecute($nls);
					ociexecute($anweisung);
					ocifreestatement($anweisung);
				}
				break;
			case 'Info':
				if($_POST['txtID'] == -1)
				{
					$SQL ='insert into BEARBEITUNGSSTAND_NEW ';
					$SQL .=' (BEARBNRNEU,DATUM,BEMERKUNGEN,ZAHLUNGSART,BNUSER,EINTRAG,BETRAG,BNUSERDAT,ID)';
					$SQL .=' values';
					$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtBEARBNRNEU']);
					$SQL .=','.$DB->FeldInhaltFormat('D',$_POST['txtBES_DATUM']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_ZAHLUNGSART']);
					$SQL .=',\'\'';
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=','.$DB->FeldInhaltFormat('N2',$_POST['txtBES_BETRAG']);
					$SQL .=',sysdate,SEQ_BEARBST_NEW_ID.nextval)';
					$DBSCHAD->Ausfuehren($SQL,'',true);

					$meldung = "Datensatz neu angelegt!";
				}
				else
				{
					$SQL = 'UPDATE BEARBEITUNGSSTAND_NEW SET';
					$SQL .= ' DATUM='.$DB->FeldInhaltFormat('D',$_POST['txtBES_DATUM']);
					$SQL .= ', BEMERKUNGEN='.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
					$SQL .= ', BETRAG='.$DB->FeldInhaltFormat('N2',$_POST['txtBES_BETRAG']);
					$SQL .= ', ZAHLUNGSART='.$DB->FeldInhaltFormat('T',$_POST['txtBES_ZAHLUNGSART']);
					$SQL .= ', EINTRAG=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', BNUSERDAT=sysdate';
					$SQL .= ' WHERE ID=' . $_POST['txtID'] . '';
					$DBSCHAD->Ausfuehren($SQL);

					$meldung = "Datensatz editiert!";

				}
				break;
			case 'WV':
				if($_POST['txtID'] == -1)
				{
					$SQL ='insert into WIEDERVORLAGEN_NEW ';
					$SQL .='(wv_id,wv_datum,bearbeitungsnr,wv_grund,wv_an,wv_user,wv_userdat)';
					$SQL .=' values ';
					$SQL .='(wv_id_wiedervorlagen_new.nextval';
					$SQL .=','.$DB->FeldInhaltFormat('D',$_POST['txtBES_DATUM']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtSCHADENSNR']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_WVGRUND']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_WVAN']);
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=',sysdate)';
					$DBSCHAD->Ausfuehren($SQL,'',true);

					$meldung = "Datensatz neu angelegt!";
				}
				else
				{
					$SQL = 'UPDATE WIEDERVORLAGEN_NEW SET';
					$SQL .= ' WV_DATUM='.$DB->FeldInhaltFormat('D',$_POST['txtBES_DATUM']);
					$SQL .= ', WV_AN='.$DB->FeldInhaltFormat('T',$_POST['txtBES_WVAN']);
					$SQL .= ', WV_GRUND='.$DB->FeldInhaltFormat('T',$_POST['txtBES_WVGRUND']);
					$SQL .= ', WV_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', WV_USERDAT=sysdate';
					$SQL .= ' WHERE WV_ID=' . $_POST['txtID'] . '';
					$DBSCHAD->Ausfuehren($SQL);

					$meldung = "Datensatz editiert!";

				}
				break;
			case 'Haupt':


				$SQL3 = 'UPDATE SCHAEDEN_NEW SET ';

				$SQL3.= 'KUNDENNAME='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_NAME']);
				$SQL3.= ',VORNAME='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_VORNAME']);
				$SQL3.= ',FILNR='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['sucBES_FIL']);
				$SQL3.= ',VERURS_FILIALE='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['sucBES_VUFIL']);
				$SQL3.= ',ANTRAGART='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_AART']);
				$SQL3.= ',BEARBEITER='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_BEARBEITER']);
				$SQL3.= ',ORTEREIGNIS='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ORTEREIGNIS']);
				$SQL3.= ',VORGANGSNR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ARCHIVNR']);
				$SQL3.= ',EINGANGPER='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_EINGANGPER']);
				$SQL3.= ',REKLASCHEINNR='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_RSCHEINNR']);
				$SQL3.= ',EINGABEAM='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_EINGABEAM']);
				$SQL3.= ',REKLASCHEINVAX='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_RVAXNR']);
				$SQL3.= ',AKTEGESCHLOSSENAM='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_GESCHLAM']);
				$SQL3.= ',DATUMEREIGNIS='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_SBDAT']);
				$SQL3.= ',GEWICHTUNG='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GEWICHTUNG']);
				$SQL3.= ',BESICHTIGT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BESICHTIGT']);
				$SQL3.= ',GESCHLECHT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ANREDE']);
				$SQL3.= ',FABRIKAT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FABRIKAT']);
				$SQL3.= ',TYP='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_MODELL']);
				$SQL3.= ',KFZTYP='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TYP']);
				$SQL3.= ',STRASSE='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_STRASSE']);
				$SQL3.= ',KBANR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KBA']);
				$SQL3.= ',PLZ='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_PLZ']);
				$SQL3.= ',ORT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ORT']);
				$SQL3.= ',ERSTZULASSUNG='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_EZ']);
				$SQL3.= ',TELEFON='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TELEFON']);
				$SQL3.= ',KM='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_KM']);
				$SQL3.= ',TELEFON2='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TELEFON2']);
				$SQL3.= ',KFZ_KENNZ='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KENNZ']);
				$SQL3.= ',EMAIL='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_EMAIL']);
				$SQL3.= ',FAX='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FAX']);
				$SQL3.= ',GROSSKDNR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['sucBES_GROSSKDNR_D']);
				$SQL3.= ',PAN='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_PAN']);
				$SQL3.= ',BEZEICHNUNG='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AUFTRAG']);
				$SQL3.= ',KENNUNG='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KENNUNG']);
				$SQL3.= ',MECHANIKER='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_MECHANIKER']);
				$SQL3.= ',AUFTRAGSART_ATU_NEU='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_AUFTRAGSART_NEU']);
				$SQL3.= ',AUFTRAGSDATUM='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_AUFTRAGSDATUM']);
				$SQL3.= ',KAUFDATUM='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_KAUFDATUM']);
				$SQL3.= ',WANR='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_VUWA']);
				$SQL3.= ',BONNR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BONNR']);
				$SQL3.= ',ATUNR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ATUNR']);
				$SQL3.= ',LIEFERANT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['sucBES_LIEFERANT']);
				$SQL3.= ',LIEFARTNR='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_LIEFARTNR']);
				$SQL3.= ',DATUMREGULIERUNG='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_DATUMREG']);
				$SQL3.= ',BONNRREGULIERUNG='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BONNRREG']);
				$SQL3.= ',VERK�UFER='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_VERKAEUFER']);
				$SQL3.= ',SCHADENSGRUND='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_WORANARB']);
				$SQL3.= ',BID='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_WASBESCH']);
				$SQL3.= ',AUSFALLURSACHE='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AUSFALLURSACHE']);
				$SQL3.= ',FGLIEFERANT='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_FGLIEFERANT']);
				$SQL3.= ',FGMARKE='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_FGMARKE']);
				$SQL3.= ',ZUSATZINFO='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ZUSATZINFO']);
				$SQL3.= ',FEHLERBESCHREIBUNG='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FEHLER']);
				$SQL3.= ',DIAGNOSE_WERKSTATTLEITER='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_DIAGWL']);
				$SQL3.= ',DIAGNOSE_TKDL='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BESPMA']);
				$SQL3.= ',BEMINTERN='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BEMINTERN']);
				$SQL3.= ',KONTAKT_KUNDE='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_KONKUNDE']);
				$SQL3.= ',TERMIN_KUNDE_1='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_TERMIN1']);
				$SQL3.= ',TERMIN_KUNDE_WER_1='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_WER1']);
				$SQL3.= ',INFO_AN_GBL='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GBLINFO']);
				$SQL3.= ',TERMIN_KUNDE_2='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_TERMIN2']);
				$SQL3.= ',TERMIN_KUNDE_WER_2='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_WER2']);
				$SQL3.= ',STAND='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_STAND']);
				$SQL3.= ',WEITERGELEITET_HERSTELLER='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_WHERSTELLER']);


				if(isset($_POST['txtCHK_AKTIV']))
				{
					$SQL3 .= ',ANWALTSSACHE=-1';
				}
				else
				{
					$SQL3 .= ',ANWALTSSACHE=0';
				}

				$SQL3.= ',AKTENZEICHENRA='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AZ']);
				$SQL3.= ',WEITERGELEITETVERSICHERUNG='.$DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_WVERS']);
				$SQL3.= ',RAFORDERUNG='.$DBSCHAD->FeldInhaltFormat('N2',preg_replace('![^0-9,^,]!', '',$_POST['txtBES_RAFORDERUNG']));
				$SQL3.= ',STANDBEIGERICHT='.$DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GERICHT']);
				$SQL3.= ',FORDERUNG='.$DBSCHAD->FeldInhaltFormat('N2',preg_replace('![^0-9,^,]!', '',$_POST['txtBES_FORDERUNG']));
				$SQL3.= ',AKTENZEITENGERICHT='.$DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AZGERICHT']);



				if(($Recht40004&2)==2) {
					if (isset($_POST['txtCHK_ANZEIGEFIL'])) {
						$SQL3 .= ',ANZEIGEFIL=1';
					} else {
						$SQL3 .= ',ANZEIGEFIL=0';
					}
				}
				else{
					if ($_POST['txtANZEIGEFIL'] == '1') {
						$SQL3 .= ',ANZEIGEFIL=1';
					} else {
						$SQL3 .= ',ANZEIGEFIL=0';
					}
				}


				$SQL3.= ' where bearbeitungsnr =\''.$AWIS_KEY1.'\'';


				if($_POST['sucBES_FIL'] != $_POST['txtoldBES_FIL'])
				{

					$User=$AWISBenutzer->BenutzerName();
					$Fil = $_POST['sucBES_FIL'];
					$Bearbnr = $_POST['txtBEARBNRNEU'];

					if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
					{
						$verbindung=oci_connect("schaddev09","schad","AWISPROD_SH");
					}
					elseif($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING)
					{
						$verbindung=oci_connect("schaddev09","schad","AWISSTAG_SH");
					}else{
                        $verbindung=oci_connect("schaddev09","schad","AWISENTWICK_SH");
					}
						$anweisung = str_replace("'","''",$SQL3);
						$anweisung=ociparse($verbindung,"begin :neuefilnr := beschwerden.filialwechsel('$anweisung',$Fil,'$Bearbnr','$User','S');end;");


						$nls=ociparse($verbindung,"ALTER SESSION SET NLS_DATE_FORMAT = 'DD.MM.YYYY HH24:MI:SS'");
						ociexecute($nls);
						ocibindbyname($anweisung,":neuefilnr",$meldung,250);
						ociexecute($anweisung);
						ocifreestatement($anweisung);

						$SQL7 = 'DELETE FROM HISTORIE WHERE HISTORIE_BEARBNR='.$AWIS_KEY1; // Historiendatensatz der alten Belegnummer l�schen (letzte laufende Nummer kann wieder verwendet werden)
						$DBSCHAD->Ausfuehren($SQL7);

						$AWIS_KEY1 = $meldung;

						$SQL5 = 'insert into HISTORIE (HISTORIE_BEARBNR,HISTORIE_KENNUNG,HISTORIE_DATUM,HISTORIE_AART)';
						$SQL5 .= 'values(';
						$SQL5 .= $AWIS_KEY1.',';
						$SQL5 .='\''.$AWISBenutzer->BenutzerName().'\',';
						$SQL5 .='sysdate,';
						$SQL5 .='1)';

						$DBSCHAD->Ausfuehren($SQL5,'',true);

						$meldung = 'Datensatz auf neue Bearbeitungsnummer umgebucht.';



					}
					else
					{
						$DBSCHAD->Ausfuehren($SQL3);

                        $SQL5 = 'insert into HISTORIE (HISTORIE_BEARBNR,HISTORIE_KENNUNG,HISTORIE_DATUM,HISTORIE_AART)';
                        $SQL5 .= 'values(';
                        $SQL5 .= $AWIS_KEY1.',';
                        $SQL5 .='\''.$AWISBenutzer->BenutzerName().'\',';
                        $SQL5 .='sysdate,';
                        $SQL5 .='0)';

                        $DBSCHAD->Ausfuehren($SQL5,'',true);

						$meldung = "Datensatz editiert!";
					}


					if($_POST['txtBES_BEARBEITER'] != $_POST['oldBES_BEARBEITER'])
					{
						$SQL = 'select (select SBNAME from SACHBEARBEITER where ID='.$_POST['oldBES_BEARBEITER'].') as old,(select SBNAME from SACHBEARBEITER where ID='.$_POST['txtBES_BEARBEITER'].') as new from dual';

						$rsSB = $DBSCHAD->RecordSetOeffnen($SQL);

						$SQL = 'UPDATE WIEDERVORLAGEN_NEW SET';
						$SQL .= ' WV_AN='.$DB->FeldInhaltFormat('T',$rsSB->FeldInhalt('NEW'));
						$SQL .= ', WV_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', WV_USERDAT=sysdate';
						$SQL .= ' WHERE BEARBEITUNGSNR=' . $AWIS_KEY1 . '';
						$SQL .= ' AND WV_AN =\'' . $rsSB->FeldInhalt('OLD') . '\'';

						$DBSCHAD->Ausfuehren($SQL);

						$Form->Hinweistext('WV f�r neuen Sachbearbeiter wurden umgesetzt.');

					}

				break;
		}
	}
	else if($AWIS_KEY1 == '-1')
	{
		$SQL = 'select max(lfdnr) as lfdnr from schaeden_new ';
		$SQL .=  'where schadensjahr='.$_POST['txtBES_SCHADENSJAHR'];
		$SQL .= ' and filnr='.$_POST['sucBES_FIL'];

		$rsLFDNR = $DBSCHAD->RecordSetOeffnen($SQL);

		$LFDNR = $rsLFDNR->FeldInhalt('LFDNR')+1;
		$SCHADENSNR = substr($_POST['txtBES_SCHADENSJAHR'],2,2).sprintf("%03s",trim($_POST['sucBES_FIL'])).sprintf("%04s",$rsLFDNR->FeldInhalt('LFDNR')+1);


		$SQL3='insert into SCHAEDEN_NEW (LFDNR,BEARBEITUNGSNR,SCHADENSJAHR,KUNDENNAME,VORNAME,FILNR,VERURS_FILIALE,ANTRAGART,BEARBEITER,ORTEREIGNIS,VORGANGSNR,EINGANGPER,REKLASCHEINNR,EINGABEAM,REKLASCHEINVAX,AKTEGESCHLOSSENAM,DATUMEREIGNIS,GEWICHTUNG,BESICHTIGT,GESCHLECHT,FABRIKAT,TYP,KFZTYP,STRASSE,KBANR,PLZ,ORT,ERSTZULASSUNG,TELEFON,KM,TELEFON2,KFZ_KENNZ,EMAIL,FAX,GROSSKDNR,PAN,BEZEICHNUNG,KENNUNG,MECHANIKER,AUFTRAGSART_ATU_NEU,AUFTRAGSDATUM,KAUFDATUM,WANR,BONNR,ATUNR,LIEFERANT,LIEFARTNR,DATUMREGULIERUNG,BONNRREGULIERUNG,VERK�UFER,SCHADENSGRUND,BID,AUSFALLURSACHE,FGLIEFERANT,FGMARKE,ZUSATZINFO,FEHLERBESCHREIBUNG,DIAGNOSE_WERKSTATTLEITER,DIAGNOSE_TKDL,BEMINTERN,KONTAKT_KUNDE,TERMIN_KUNDE_1,TERMIN_KUNDE_WER_1,INFO_AN_GBL,TERMIN_KUNDE_2,TERMIN_KUNDE_WER_2,STAND,WEITERGELEITET_HERSTELLER,ANWALTSSACHE,AKTENZEICHENRA,WEITERGELEITETVERSICHERUNG,RAFORDERUNG,STANDBEIGERICHT,FORDERUNG,AKTENZEITENGERICHT,ANZEIGEFIL)';
		$SQL3.= 'values(';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$LFDNR).',';
		//$SQL3.= $SEQ.',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$SCHADENSNR).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_SCHADENSJAHR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_NAME']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_VORNAME']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['sucBES_FIL']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['sucBES_VUFIL']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_AART']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_BEARBEITER']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ORTEREIGNIS']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ARCHIVNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_EINGANGPER']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_RSCHEINNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_EINGABEAM']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_RVAXNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_GESCHLAM']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_SBDAT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GEWICHTUNG']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BESICHTIGT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ANREDE']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FABRIKAT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_MODELL']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TYP']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_STRASSE']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KBA']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_PLZ']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ORT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_EZ']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TELEFON']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_KM']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_TELEFON2']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KENNZ']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_EMAIL']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FAX']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['sucBES_GROSSKDNR_D']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_PAN']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AUFTRAG']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_KENNUNG']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_MECHANIKER']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_AUFTRAGSART_NEU']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_AUFTRAGSDATUM']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_KAUFDATUM']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_VUWA']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BONNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ATUNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['sucBES_LIEFERANT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_LIEFARTNR']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_DATUMREG']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BONNRREG']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_VERKAEUFER']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_WORANARB']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_WASBESCH']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AUSFALLURSACHE']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_FGLIEFERANT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_FGMARKE']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_ZUSATZINFO']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_FEHLER']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_DIAGWL']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BESPMA']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_BEMINTERN']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_KONKUNDE']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_TERMIN1']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_WER1']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GBLINFO']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_TERMIN2']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_WER2']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_STAND']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_WHERSTELLER']).',';

		if(isset($_POST['txtCHK_AKTIV']))
		{
			$SQL3 .= '-1,';
		}
		else
		{
			$SQL3 .= '0,';
		}

		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AZ']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('D',$_POST['txtBES_WVERS']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N2',preg_replace('![^0-9,^,]!', '',$_POST['txtBES_RAFORDERUNG'])).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N0',$_POST['txtBES_GERICHT']).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('N2',preg_replace('![^0-9,^,]!', '',$_POST['txtBES_FORDERUNG'])).',';
		$SQL3.= $DBSCHAD->FeldInhaltFormat('T',$_POST['txtBES_AZGERICHT']).',';

		if(($Recht40004&2)==2) {
			if (isset($_POST['txtCHK_ANZEIGEFIL'])) {
				$SQL3 .= '1)';
			} else {
				$SQL3 .= '0)';
			}
		}
		else{
			if ($_POST['txtANZEIGEFIL'] == '1') {
				$SQL3 .= '1)';
			} else {
				$SQL3 .= '0)';
			}
		}



		$DBSCHAD->Ausfuehren($SQL3,'',true);

		$SQL2 ="select BEARBNRNEU_SCHAEDEN_NEW.currval as SEQ from dual";
		$rsSEQ = $DBSCHAD->RecordSetOeffnen($SQL2);

		$SEQ = $rsSEQ->FeldInhalt('SEQ');


		$SQL4 ='select * from schaeden_new where bearbnrneu=\''.$SEQ.'\'';
		$rsDaten = $DBSCHAD->RecordSetOeffnen($SQL4);

		$AWIS_KEY1 = $rsDaten->FeldInhalt('BEARBEITUNGSNR');
		$meldung = "Datensatz neu angelegt!";

		$SQL5 = 'insert into HISTORIE (HISTORIE_BEARBNR,HISTORIE_KENNUNG,HISTORIE_DATUM,HISTORIE_AART)';
		$SQL5 .= 'values(';
		$SQL5 .= $SCHADENSNR.',';
		$SQL5 .='\''.$AWISBenutzer->BenutzerName().'\',';
		$SQL5 .='sysdate,';
		$SQL5 .='1)';

		$DBSCHAD->Ausfuehren($SQL5,'',true);

		if($_POST['txtBES_GESCHLAM'] == '')
		{
			$SQL6="begin PACKAGE_WVEINTRAGEN.wveintragen('".$SCHADENSNR."','".$AWISBenutzer->BenutzerName()."');end;";

			if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) {
				$verbindung=oci_connect("schaddev09","schad","AWISPROD_SH");
			}elseif($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING){
                $verbindung=oci_connect("schaddev09","schad","AWISSTAG_SH");
			}
			else{
				$verbindung=oci_connect("schaddev09","schad","AWISENTWICK_SH");
			}

			$anweisung=ociparse($verbindung,$SQL6);
			$nls=ociparse($verbindung,"ALTER SESSION SET NLS_DATE_FORMAT = 'DD.MM.YYYY HH24:MI:SS'");
			ociexecute($nls);
			ociexecute($anweisung);
			ocifreestatement($anweisung);
		}
	}
}
catch (awisException $ex)
{

	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_QMZ_ZUORD) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Form->Hinweistext('Der einzuf�gende Datensatz befindet sich schon in der Datenbank');
	}
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_QMK_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 4;
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
