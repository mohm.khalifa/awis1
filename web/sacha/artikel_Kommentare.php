<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $HTTP_POST_VARS;

	
/********************************************************************************
* Daten Speichern, Hinzuf�gen und L�schen
********************************************************************************/
	if(isset($HTTP_POST_VARS['cmdSpeichern_x']) OR awis_NameInArray($HTTP_POST_VARS,"cmdKOMMLoeschen_")!='')
	{
		foreach($HTTP_POST_VARS as $Eintrag => $EintragsWert)
		{
			if(substr($Eintrag,0,13) == "txtKOMM_WERT_")
			{
				if($EintragsWert . '' != $HTTP_POST_VARS['txtKOMM_ALT_WERT_' . substr($Eintrag,13)] . '')
				{
					$SQL = "UPDATE AWIS.ArtikelstammInfos ";
					$SQL .= " SET ASI_WERT='" . $EintragsWert . "'";
					$SQL .= " , ASI_USER='" . $_SERVER['PHP_AUTH_USER'] . "', ASI_USERDAT=SYSDATE";
					$SQL .= " WHERE ASI_KEY=0" . substr($Eintrag,13);
					
					$Erg = awisExecute($con, $SQL);
					if($Erg===FALSE)
					{
						awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
					}
				}
			}
		    if(substr($Eintrag,0,16) == "cmdKOMMLoeschen_" AND substr($Eintrag,strlen($Eintrag)-1,1)=='x')
			{
				$SQL = "DELETE FROM AWIS.ArtikelstammInfos WHERE ASI_KEY=0" . intval(substr($Eintrag,16));
				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}

			if($Eintrag == "txtKOMM_NEUWERT_110" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $HTTP_POST_VARS["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 110,";							
				$SQL .= " '" . $_SERVER['PHP_AUTH_USER'] . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
			if($Eintrag == "txtKOMM_NEUWERT_111" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $HTTP_POST_VARS["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 111,";							
				$SQL .= " '" . $_SERVER['PHP_AUTH_USER'] . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
			if($Eintrag == "txtKOMM_NEUWERT_112" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $HTTP_POST_VARS["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 112,";							
				$SQL .= " '" . $_SERVER['PHP_AUTH_USER'] . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
		}
	}	


/*****************************************************************************************************
* Daten anzeigen
*****************************************************************************************************/
	
	$BearbeitungsStufe = awisBenutzerRecht($con, 401) ;
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $_SERVER['PHP_AUTH_USER']));
	
	$SQL = "SELECT * FROM AWIS.ArtikelstammInfos, AWIS.ArtikelstammInfosTypen ";
	$SQL .= " WHERE ASI_AIT_ID = AIT_ID AND ASI_AST_ATUNR='" . $Artikel[1] . "'";
	$SQL .= " AND ASI_AIT_ID IN(110,111,112) ORDER BY ASI_AIT_ID, ASI_USERDAT DESC";
//print "<hr>$SQL<hr>";

	$rsKommentare = awisOpenRecordset($con, $SQL);
	if($rsKommentare==FALSE)
	{
		awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
	}
	$rsKommentareZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=100% border=1>";
	print "<cols><col width=20></col><col width=*></col><col width=200></col><col width=100></col></cols>";

	$AktuelleID = 0;		// F�r den Blocktrenner	
	for($Zeile=0;$Zeile<$rsKommentareZeilen;$Zeile++)
	{
		if($AktuelleID!=$rsKommentare["AIT_ID"][$Zeile])
		{
			$AktuelleID=$rsKommentare["AIT_ID"][$Zeile];
			print "<tr><td id=FeldBez></td>";
			print "<td id=FeldBez>" . $rsKommentare['AIT_INFORMATION'][$Zeile] . "</td>";
			print "<td id=FeldBez>Ersteller</td>";	
			print "<td id=FeldBez>Datum</td>";	
		}

		print "<tr>";
		
		if(($BearbeitungsStufe & 32)==32)			// Recht 401
		{
			print "<td><input name=cmdKOMMLoeschen_" . $rsKommentare["ASI_KEY"][$Zeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
			print "<td><input size=80 name=txtKOMM_WERT_" . $rsKommentare["ASI_KEY"][$Zeile] . " type=text value='" . $rsKommentare["ASI_WERT"][$Zeile] . "'>";
			print "<input size=80 name=txtKOMM_ALT_WERT_" . $rsKommentare["ASI_KEY"][$Zeile] . " type=hidden value='" . $rsKommentare["ASI_WERT"][$Zeile] . "'></td>";
		}
		else
		{
			print "<td>&nbsp;</td>";
			print "<td>" . $rsKommentare["ASI_WERT"][$Zeile] . "</a></td>";
		}
		print "<td>" . $rsKommentare["ASI_USER"][$Zeile] . "</a></td>";
		print "<td>" . $rsKommentare["ASI_USERDAT"][$Zeile] . "</a></td>";

		print "</tr>";

		// Neue Zuordnung erlaubt?
	}

	if(($BearbeitungsStufe & 32)==32)			// Recht 401
	{
		print "<tr><td id=FeldBez></td>";
		print "<td id=FeldBez>Neuer Crossing Kommentar</td>";
		print "<td id=FeldBez>Ersteller</td>";	
		print "<td id=FeldBez>Datum</td></tr>";	
	
		print "<tr>";
		print "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		print "<td><input name=txtKOMM_NEUWERT_110 size=80>";
		print "<input type=hidden name=txtKOMM_TYP value=110></td>";
		print "<td>" . $_SERVER['PHP_AUTH_USER'] . "</td>";
		print "<td>" . date('d.m.Y') . "</td>";
		print "<td></td>";
		print "<td></td>";
		print "</tr>";
	}

	if(($BearbeitungsStufe & 64)==64)			// Recht 401
	{
		print "<tr><td id=FeldBez></td>";
		print "<td id=FeldBez>Neuer Einkauf Kommentar</td>";
		print "<td id=FeldBez>Ersteller</td>";	
		print "<td id=FeldBez>Datum</td></tr>";	
	
		print "<tr>";
		print "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		print "<td><input name=txtKOMM_NEUWERT_111 size=80>";
		print "<input type=hidden name=txtKOMM_TYP value=111></td>";
		print "<td>" . $_SERVER['PHP_AUTH_USER'] . "</td>";
		print "<td>" . date('d.m.Y') . "</td>";
		print "<td></td>";
		print "<td></td>";
		print "</tr>";
	}

	if(($BearbeitungsStufe & 128)==128)			// Recht 401
	{
		print "<tr><td id=FeldBez></td>";
		print "<td id=FeldBez>Neuer TKD Kommentar</td>";
		print "<td id=FeldBez>Ersteller</td>";	
		print "<td id=FeldBez>Datum</td></tr>";	
	
		print "<tr>";
		print "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		print "<td><input name=txtKOMM_NEUWERT_112 size=80>";
		print "<input type=hidden name=txtKOMM_TYP value=112></td>";
		print "<td>" . $_SERVER['PHP_AUTH_USER'] . "</td>";
		print "<td>" . date('d.m.Y') . "</td>";
		print "<td></td>";
		print "<td></td>";
		print "</tr>";
	}
	
	print "</table>";

	unset($rsKommentare);
?>