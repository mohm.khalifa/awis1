UPDATE textkonserven
SET
  xtx_text_de = 'Suche',
  xtx_text_cz = 'Suche',
  xtx_text_nl = 'Suche',
  xtx_text_it = 'Suche',
  xtx_text_fr = 'Suche',
  xtx_text_ch = 'Suche',
  xtx_text_at = 'Suche',
  xtx_userdat = SYSDATE
WHERE
  xtx_kennung = 'reg_590000'
  OR
  xtx_kennung = 'ttt_590000';