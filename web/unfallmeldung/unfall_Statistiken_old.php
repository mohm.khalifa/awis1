<?php
require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $StatParam;
global $Seite;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
    $TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort', 'AlleAnzeigen');    
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21030 = $AWISBenutzer->HatDasRecht(21030);
	if($Recht21030 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}

    // Seite-GET-Parameter abholen, um im <form>-Tag zu verwenden, dass bei "Weiter"-Klick immer der richtige Sub-Reiter ge�ffnet wird
    $Seite = (isset($_GET['Seite']) ? $_GET['Seite']: 'Bereich');

	/**********************************************
	* Benutzerparameter
	***********************************************/	
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_UNF_STAT')) < 8))
	{
        $StatParam['STAT_UNF_FilNr'] = '';
        $StatParam['STAT_UNF_DatumVon'] = '';
        $StatParam['STAT_UNF_DatumBis'] = '';        
        $StatParam['STAT_UNF_BereichId'] = 0;
        $StatParam['STAT_SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_UNF_STAT", serialize($StatParam));
	}
    else
    {
	    $StatParam = unserialize($AWISBenutzer->ParameterLesen('Formular_UNF_STAT'));
        if (isset($_POST['cmdSuche_x']))
        {
            $StatParam['STAT_UNF_FilNr'] = $_POST['sucUNF_FILNR'];
            $StatParam['STAT_UNF_DatumVon'] = $_POST['sucUNF_DATUMVON'];
            $StatParam['STAT_UNF_DatumBis'] = $_POST['sucUNF_DATUMBIS'];
            $StatParam['STAT_UNF_BereichId'] = $_POST['sucUNF_BEREICHID'];
            $StatParam['STAT_SPEICHERN']= (isset($_POST['sucUNF_AUSWAHLSPEICHERN'])? 'on': '');
            $AWISBenutzer->ParameterSchreiben("Formular_UNF_STAT", serialize($StatParam));
        }        
       	elseif (! isset($StatParam['STAT_SPEICHERN']))
    	{
    		$StatParam['STAT_SPEICHERN']= 'off';
            $StatParam['STAT_UNF_FilNr'] = '';
            $StatParam['STAT_UNF_DatumVon'] = '';
            $StatParam['STAT_UNF_DatumBis'] = '';        
            $StatParam['STAT_UNF_BereichId'] = 0;
            $StatParam['STAT_SPEICHERN']= 'off';
    	    $AWISBenutzer->ParameterSchreiben("Formular_UNF_STAT", serialize($StatParam));    		
    	}
	}        
    
    $Form->SchreibeHTMLCode('<form name=frmUnfallDetails action=./unfall_Main.php?cmdAktion=Statistiken&Seite='. $Seite . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();

    
    // Zeile: Beschriftungs und Suchfeld: FilNr
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_FILNR', ($StatParam['STAT_SPEICHERN'] == 'on'? $StatParam['STAT_UNF_FilNr']: ''), 4, 120, true, '', '', '', 'T', 'L');
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Suchauswahlfeld: Datum von
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DATUMVON', ($StatParam['STAT_SPEICHERN'] == 'on'? $StatParam['STAT_UNF_DatumVon']: ''), 8, 120, true, '', '', '', 'D', 'L');
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Suchauswahlfeld: Datum bis
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DATUMBIS', ($StatParam['STAT_SPEICHERN'] == 'on'? $StatParam['STAT_UNF_DatumBis']: ''), 8, 120, true, '', '', '', 'D', 'L');
    $Form->ZeileEnde();    
    // Zeile: Beschriftungs und Suchfeld: Bereich
    $Form->ZeileStart();
    $SQL =  'select uft_bereichid, uft_information ' . 
            'from unfallmeldunginfotypen ' .
            'where uft_status = 1 ' .
            'and uft_ueberschrift = 1 ' .
            'order by uft_information';
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['STATBEREICH'] . ':', 200);
    $Form->Erstelle_SelectFeld('*UNF_BEREICHID', ($StatParam['STAT_SPEICHERN'] == 'on'? $StatParam['STAT_UNF_BereichId']: ''), 160, true, $SQL, $AWISSprachKonserven['Wort']['AlleAnzeigen']);
    $AWISCursorPosition = 'sucUNF_BEREICHID';
    $Form->ZeileEnde();
    
    // Zeile: Beschriftungs und Checkbox: Auswahl speichern
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['AUSWAHLSPEICHERN'] . ':', 200);
    $Form->Erstelle_Checkbox('*UNF_AUSWAHLSPEICHERN', ($StatParam['STAT_SPEICHERN'] == 'on'? 'on': ''), 20 , true, 'on');
    $Form->ZeileEnde();

	$Form->Formular_Ende();	    
	
	$Register = new awisRegister(21030);
	$Register->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

	if(($Recht21030&1) == 1)
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}        
    
	$Form->SchaltflaechenEnde();		
    $Form->SchreibeHTMLCode('</form>');
    
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>