<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschenRueckfuehrung');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'RFK';
	$Key=$_POST['txtRFK_KEY'];
	$RFKKey=$_POST['txtRFK_KEY'];

	$AWIS_KEY1 = $Key;

	$Felder=array();
	$Felder[]=array($Form->LadeTextBaustein('RFK','RFK_FIL_ID'),$_POST['txtRFK_FIL_ID']);	
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Positionen':
			$Tabelle = 'RFF';
			$Key=$_GET['Del'];

			$DB->SetzeBindevariable('RFF', 'var_N0_rff_key', '0'.$Key, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE, RFF_KEY, RFF_RFK_KEY';
			$SQL .= ' FROM RueckFuehrungsFilialmengenpos ';
			$SQL .= ' WHERE RFF_KEY=:var_N0_rff_key';

			$rsDaten = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('RFF'));

			$RFKKey = $rsDaten->FeldInhalt('RFF_RFK_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('RFF','RFF_AST_ATUNR'),$rsDaten->FeldInhalt('RFF_AST_ATUNR'));
			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle'])
	{
		case 'RFK':
			$DB->SetzeBindevariable('KEY', 'var_N0_rfk_key', '0'.$_POST['txtKey'], awisDatenbank::VAR_TYP_GANZEZAHL);
			$SQL = 'DELETE FROM RueckFuehrungsfilialmengenKopf WHERE rfk_key=:var_N0_rfk_key';
			$AWIS_KEY1=0;
			break;
		case 'RFF':
			$DB->SetzeBindevariable('KEY', 'var_N0_rff_key', '0'.$_POST['txtKey'], awisDatenbank::VAR_TYP_GANZEZAHL);
			$SQL = 'DELETE FROM RueckFuehrungsFilialmengenPos WHERE rff_key=:var_N0_rff_key';
			$AWIS_KEY1=$_POST['txtRFKKey'];
			break;
		default:
			break;
	}

	if($SQL !='')
	{
		if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('KEY'))===false)
		{
			awisErrorMailLink('rueckfuehrungsbearbeitung_loeschen_1',1,$awisDBError['messages'],'');
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

	$Form->Formular_Start();
	$Form->ZeileStart();
	
	if ($Tabelle == 'RFK')
	{
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschenRueckfuehrung']);
	}
	else 
	{
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
	}
	$Form->ZeileEnde();

	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}

	$Form->Erstelle_HiddenFeld('RFKKey',$RFKKey);
	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
	$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
	$Form->ZeileEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>