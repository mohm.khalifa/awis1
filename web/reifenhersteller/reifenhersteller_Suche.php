<?php 
/**
 * Suchmaske f�r die Auswahl Reifenhersteller
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20130624
 *
 */
 
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven = array();
	$TextKonserven[]=array('REH','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht15400=$AWISBenutzer->HatDasRecht(15400);		
	if ($Recht15400 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    $Form->PruefeDatum('01.10.2015');
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./reifenhersteller_Main.php?cmdAktion=Details>");	

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_REH'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	$Form->Formular_Start();
	
	$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'].':',150);
	$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*REH_KEY',($Param['SPEICHERN']=='on'?$Param['REH_KEY']:'0'),'200:180',true,$SQL,'-1~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);	
	$Form->ZeileEnde();
	
	$Form->Trennzeile('O');
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',150);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht15400&4) == 4)		
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306240948");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201306240949");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>