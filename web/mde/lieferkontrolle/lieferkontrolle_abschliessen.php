<?php
global $AWIS_KEY1;

require_once('jpgraph/jpgraph_barcode.php');
require_once('register.inc.php');

$TextKonserven=array();
$TextKonserven[]=array('LIK','LIK_%');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Abbrechen');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','Filialbestand');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FehlerAbschluss');
$TextKonserven[]=array('Fehler','err_EingabeGroesserNull');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('LIK','HinweisDifferenzen');
$TextKonserven[]=array('LIK','HinweisAbgeschlossen');
$TextKonserven[]=array('LIK','WirklichAbschliessen');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$TXT_Abschliessen = $Form->LadeTexte($TextKonserven);

$Art= '';
$Hinweis = '';
//$Form->DebugAusgabe(1,$_POST);
if(isset($_POST['cmd_Abschliessen_x']))
{	
	$Key=$_POST['txtLKK_KEY'];
	$LKKKey=$_POST['txtLKK_KEY'];
	
	$AWIS_KEY1=$Key;
	
	$Art = 'Filiale';
	
	//Pr�fen, ob die Lieferung evtl. schon abgeschlossen ist
	$SQL = 'SELECT lkk_status';	
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .=' WHERE LKK_KEY=0'.intval($AWIS_KEY1);	
				
	$rsLKK = $DB->RecordSetOeffnen($SQL);
	
	if ($rsLKK->FeldInhalt('LKK_STATUS')=='A' or $rsLKK->FeldInhalt('LKK_STATUS')=='Z')
	{										
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_Abschliessen['LIK']['HinweisAbgeschlossen']);
		$Form->ZeileEnde();
		
		$Link='./lieferkontrolle_Main.php?cmdAktion=Details&LKK_KEY='.$AWIS_KEY1;
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdAbbrechen',$Link,'/bilder/cmd_weiter.png', $TXT_Abschliessen['Wort']['lbl_weiter'], 'W');
		$Form->SchaltflaechenEnde();				
		die();	
	}
		
	$SQL = 'SELECT *';
	$SQL .=' FROM Lieferkontrollen LIK';
	$SQL .=' WHERE LIK_LKK_KEY=0'.intval($_POST['txtLKK_KEY']);	
	$SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
	$SQL .= ' AND LIK_STATUS < 12';
	$SQL .= ' AND (LIK_STATUSLIEFERUNG is null or LIK_STATUSLIEFERUNG <> 1) ';
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
	
	$rsLIK=$DB->RecordSetOeffnen($SQL);
	//$Form->DebugAusgabe(1,$rsLIK->AnzahlDatensaetze(),$SQL);
	
	$Hinweis='';
	if ($rsLIK->AnzahlDatensaetze()>0)
	{
		$Hinweis = $TXT_Abschliessen['LIK']['HinweisDifferenzen'].'&nbsp;'.$rsLIK->AnzahlDatensaetze().'<br>';
	}				
	
}
elseif(isset($_POST['cmdAbschliessenOK']))	// Abschluss durchf�hren
{	
	$AWIS_KEY1 = $_POST['txtKey'];
	
	$SQL = '';
	switch ($_POST['txtArt'])
	{
		case 'Filiale':	
			
			//Kopf auf Abgeschlossen setzen
			$SQL = 'UPDATE Lieferkontrollenkopf';
			$SQL .= ' SET LKK_Status=\'A\'';
			$SQL .= ', LKK_user=\''.$AWISBenutzer->BenutzerName().'\'';			
			$SQL .= ' , LKK_UserDat=SYSDATE';
			$SQL .= ' WHERE LKK_KEY=0'.$AWIS_KEY1;			
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('lieferkontrolle_abschliessen_1',1,$awisDBError['messages'],'');
			}	
								
			$SQL = 'UPDATE Lieferkontrollen';
			$SQL .= ' SET LIK_Status=12';
			$SQL .= ', LIK_user=\''.$AWISBenutzer->BenutzerName().'\'';			
			$SQL .= ' , LIK_UserDat=SYSDATE';
			$SQL .= ' WHERE LIK_Status < 10 AND LIK_LKK_KEY=0'.$AWIS_KEY1;		
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('lieferkontrolle_abschliessen_1',1,$awisDBError['messages'],'');
			}
						
			$SQL = 'UPDATE Lieferkontrollen';
			$SQL .= ' SET LIK_StatusLieferung=1';
			$SQL .= ' , LIK_DatumAbschluss=SYSDATE';
			$SQL .= ', LIK_user=\''.$AWISBenutzer->BenutzerName().'\'';						
			$SQL .= ' , LIK_UserDat=SYSDATE';
			$SQL .= ' WHERE LIK_LKK_KEY='.$AWIS_KEY1;			
			
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('lieferkontrolle_abschliessen_1',1,$awisDBError['messages'],'');
			}																		
	}	
}

if($Art!='')
{
	$Form->SchreibeHTMLCode('<form name=frmAbschliessen action=./lieferkontrolle_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Hinweistext($TXT_Abschliessen['LIK']['WirklichAbschliessen']);
	$Form->ZeileEnde();			
	
	if($Hinweis!='')
	{
		$Form->ZeileStart();
		$Form->Hinweistext($Hinweis);
		$Form->ZeileEnde();				
	}	
	
	$Form->Erstelle_HiddenFeld('LKK_Key',$LKKKey);
	$Form->Erstelle_HiddenFeld('Art',$Art);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdAbschliessenOK','','',$TXT_Abschliessen['Wort']['Ja'],'','');
	$Form->Schaltflaeche('submit','cmdAbschliessenAbbrechen','','',$TXT_Abschliessen['Wort']['Nein'],'');	
	$Form->ZeileEnde();
	
	

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>