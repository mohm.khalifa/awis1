<?php

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[] = array('FGK', '%');
$TextKonserven[] = array('FKH', '%');
$TextKonserven[] = array('FKA', '%');
$TextKonserven[] = array('FGP', '%');
$TextKonserven[] = array('FPH', '%');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 20;

$SQL = 'Select FKASSENDATEN.*,FGK_VORGANGNR from FKASSENDATEN ';
$SQL .= ' LEFT JOIN FGKOPFDATEN ON FKA_AEMNR = FGK_VORGANGNR';
$SQL .= ' WHERE FGK_VORGANGNR IS NULL';
$SQL .= " AND fka_datum >= to_date('01.07.2013','DD.MM.YYYY') ";
$SQL .= ' ORDER BY fka_datum';

$rsKassendaten = $DB->RecordSetOeffnen($SQL);

$Ausdruck = new awisAusdruck('L', 'A4', array('BriefpapierATU_DE_Seite_2_quer.pdf'));
$Ausdruck->NeueSeite(0, 1);

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial', 'B', 10);

$Zeile = 30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_KENNUNG'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(10, 6, $AWISSprachKonserven['FKA']['FKA_FILID'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(40, 6, substr($AWISSprachKonserven['FKA']['FKA_DATUM'], 0, 10), 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['FKA']['FKA_UHRZEIT'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(10, 6, $AWISSprachKonserven['FKA']['FKA_BSA'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_WANR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_AEMNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_ATUNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_MENGE'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['FKA']['FKA_BETRAG'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(25, 6, $AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(33, 6, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);

$count = 0;

while (!$rsKassendaten->EOF())
{

    $Zeile += 6;

    if ($count < 20)
    {

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $rsKassendaten->FeldInhalt('FKA_KENNUNG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $rsKassendaten->FeldInhalt('FKA_FILID'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, $rsKassendaten->FeldInhalt('FKA_DATUM'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsKassendaten->FeldInhalt('FKA_UHRZEIT'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $rsKassendaten->FeldInhalt('FKA_BSA'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_WANR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsKassendaten->FeldInhalt('FKA_AEMNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_ATUNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_MENGE'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsKassendaten->FeldInhalt('FKA_BETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $rsKassendaten->FeldInhalt('FKA_KFZKENNZ'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(33, 6, $rsKassendaten->FeldInhalt('FGK_VORGANGNR'), 'LTRB', 0, 'L', 0);
        //$Ausdruck->_pdf->cell(10, 6, $count, 'LTRB', 0, 'L', 0);
    }
    else
    {
        $Zeile = 5;
        $count = 0;
        $Ausdruck->_pdf->AddPage();

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_KENNUNG'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $AWISSprachKonserven['FKA']['FKA_FILID'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, substr($AWISSprachKonserven['FKA']['FKA_DATUM'], 0, 10), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['FKA']['FKA_UHRZEIT'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $AWISSprachKonserven['FKA']['FKA_BSA'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_WANR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_AEMNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_ATUNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $AWISSprachKonserven['FKA']['FKA_MENGE'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $AWISSprachKonserven['FKA']['FKA_BETRAG'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(33, 6, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);

        $Zeile += 6;

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $rsKassendaten->FeldInhalt('FKA_KENNUNG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $rsKassendaten->FeldInhalt('FKA_FILID'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, $rsKassendaten->FeldInhalt('FKA_DATUM'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsKassendaten->FeldInhalt('FKA_UHRZEIT'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(10, 6, $rsKassendaten->FeldInhalt('FKA_BSA'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_WANR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsKassendaten->FeldInhalt('FKA_AEMNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_ATUNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 6, $rsKassendaten->FeldInhalt('FKA_MENGE'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsKassendaten->FeldInhalt('FKA_BETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $rsKassendaten->FeldInhalt('FKA_KFZKENNZ'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(33, 6, $rsKassendaten->FeldInhalt('FGK_VORGANGNR'), 'LTRB', 0, 'L', 0);
        //$Ausdruck->_pdf->cell(10, 6, $count, 'LTRB', 0, 'L', 0);
    }

    $count++;
    $rsKassendaten->DSWeiter();
}



$Ausdruck->Anzeigen();
?>