<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $SpeichernButton; // Anzeige eines Speichern-Knopfs
global $con;
global $awisRSZeilen;
global $awisRSInfo;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 113);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Filialen: Telefonnummern',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// Variablen belegen
$Bedingung='';

if(($RechteStufe & 16)==16)
{
	$ALARM_SEHEN = "J";
}
else
{
	$ALARM_SEHEN = "N";
}

if(($RechteStufe & 2)==2)
{
	$SQL = "SELECT * FROM FILIALEN WHERE ";
}
else
{
	$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL AND ";
}

$FILID = (isset($_REQUEST['FIL_ID'])?$_REQUEST['FIL_ID']:'');

// Alle Filialen anzeigen
If(!isset($_GET["Liste"]) AND $FILID == '')
{
	$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
	if(substr($foo,0,7)=="FIL_ID=")
	{
		$FILID=substr($foo,7,99);
	}
}
// Bedingung ausf�llen -> ID �bernehmen

if($FILID != '')
{
    $Bedingung .= " AND ( FIL_ID=0" . $FILID . ")";
}
else
{
 	$Params = explode(";", awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName()));

    if($Params[0] != '')
    {
        $Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0], 0);
    }
     if($Params[1] != '')
    {
    	$Bedingung .= "AND SUCHWORT(FIL_BEZ) " . awisLIKEoderIST("*" . $Params[1]. "*", 1,0,1);
    }
     if($Params[2] != '')
    {
    	$Bedingung .= "AND SUCHWORT(FIL_ORT) " . awisLIKEoderIST('%'.$Params[2].'%', 1,0,1);
    }
    if($Params[3] != '')
    {
        $Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3], 0);
	}
	if($Params[5]!='')
	{
		$Bedingung .= "AND FIL_ID IN (SELECT DISTINCT PER_FIL_ID FROM PERSONAL WHERE SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $Params[5]. "%",1,0,1) . ")";
	}

}

if($Bedingung != '')
{
    $SQL .= " ( " . substr($Bedingung, 4) . ")";
}

$rsFiliale = awisOpenRecordset($con, $SQL);
$rsFilialeAnz = $awisRSZeilen;

If($rsFilialeAnz > 1)
{ // Liste gefunden -> Liste anzeigen
 	print "<table border=0 width=100%>";
    for($i = 0;$i < $rsFilialeAnz;$i++)
    {
         print "<tr><td><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Telefon&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
         print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
         print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>" . $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td><tr>";
    }
    print "</table>";
}
elseIf($rsFilialeAnz == 0)
{
	print "<p class=HinweisText>Es konnte keine Telefonnummern f�r die gesuchte Filiale gefunden werden</p>";
}
else
{ // Eine Filiale

	if(isset($_POST["cmdSpeichern_x"]))
	{
		foreach($_POST as $Eintrag => $EintragWert)
		{
			if(strstr($Eintrag,"txtFeld_")!='')
			{
				$Nr = substr($Eintrag,8);
				if($EintragWert=='')
				{
					$SQL = "DELETE FROM FILIALINFOS WHERE FIF_FIL_ID=" . $_POST["FIL_ID"] . " AND FIF_FIT_ID=" . $Nr . "";
				}
				else
				{
					$SQL = "BEGIN AWIS.AENDERN_FILIALINFOS(" . $_POST["FIL_ID"] . ",$Nr,'" . $EintragWert . "','" . $AWISBenutzer->BenutzerName() . "'); END;";
				}
		        awisExecute($con, $SQL);
//		print "<hr>$SQL<hr>";
			}
		}
		print "<hr>";
	}

	// Aktuelle Filiale speichern
	awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=" . $rsFiliale["FIL_ID"][0]);
        /**
         * Daten anzeigen
         */

        // Daten f�r die Infoseite sammeln

	$SQL = "SELECT  filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, FIT_ID, FIT_IMQ_ID, FIT_RECHTEBIT,
			   FIT_ANZZEILEN, FIT_ANZSPALTEN, FIT_WERTE
				FROM filialinfostypen, filialinfostypengruppen
				 WHERE  filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id
					AND FTG_ID IN (20,21,22,23,24,25)";	// Nur Telefonnummern
	if ($ALARM_SEHEN == "N")
	{
		$SQL .= " AND FIT_ID <> 13";
	}
	$SQL .= " ORDER BY FTG_ID, fit_sortierung";

	$rsFilialInfo = awisOpenRecordset($con, $SQL);
	$rsFilialInfoAnz = $awisRSZeilen;


     // �berschrift schreiben
	print "<input type=hidden name=FIL_ID value=" . $rsFiliale["FIL_ID"][0] . ">"; // F�r Submit
	print "<table width=100% border=0><tr><td align=left>";
    print "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
    print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Telefon&Liste=True';></td></tr></table>";
    print "<hr>";

	print "<input type=hidden name=cmdAktion value=Telefon>";		// Zum Speichern

	print "<table width=100%>";
	$LetzteGruppe = '';
    for($i=0;$i<$rsFilialInfoAnz;$i++)
	{
		print "<tr>";

		If($LetzteGruppe!=$rsFilialInfo["FTG_GRUPPE"][$i])
		{
			print "<td colspan=2 width=180 id=FeldBez>" . $rsFilialInfo["FTG_GRUPPE"][$i] . "</td></tr><tr>";
			$LetzteGruppe=$rsFilialInfo["FTG_GRUPPE"][$i];
		}
		print "<td width=180 " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialInfo["FIT_INFORMATION"][$i] . "</td>";
//awis_Debug(1,"SELECT FIF_WERT, FIF_KEY, FIF_IMQ_ID FROM FilialInfos, ImportQuellen WHERE FIF_IMQ_ID = IMQ_ID AND FIF_FIL_ID=" . $rsFiliale["FIL_ID"][0] . " AND FIF_FIT_ID=" . $rsFilialInfo["FIT_ID"][$i] . " ORDER BY IMQ_PRIORITAET");
		$rsFilialInfoDaten = awisOpenRecordset($con, "SELECT FIF_WERT, FIF_KEY, FIF_IMQ_ID FROM FilialInfos, ImportQuellen WHERE FIF_IMQ_ID = IMQ_ID AND FIF_FIL_ID=" . $rsFiliale["FIL_ID"][0] . " AND FIF_FIT_ID=" . $rsFilialInfo["FIT_ID"][$i] . " ORDER BY IMQ_PRIORITAET");

			// Manuelle Eintr�ge �ndern
		for($RechteBit=4;$RechteBit<=8192;$RechteBit<<=1)
		{
			if((($rsFilialInfo["FIT_RECHTEBIT"][$i]&$RechteBit)==$RechteBit) AND (($RechteStufe&$RechteBit)==$RechteBit))
			{
				if($rsFilialInfo["FIT_WERTE"][$i]!='')
				{
					echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
					echo '<select name=txtFeld_' . $rsFilialInfo["FIT_ID"][$i] . '>';
					echo "<option value=''>::Bitte w�hlen::</option>";

					if(substr($rsFilialInfo["FIT_WERTE"][$i],0,1)=='#')		// Werteliste
					{
						$WertListe = explode('#',substr($rsFilialInfo["FIT_WERTE"][$i],1));
						foreach($WertListe As $Eintrag)
						{
							if(strpos($Eintrag,';')===false)			// Keine Wert->Anzeige Kombination
							{
								echo '<option ';
								if(isset($rsFilialInfoDaten["FIF_WERT"][0]) AND $rsFilialInfoDaten["FIF_WERT"][0] == $Eintrag)
								{
									echo ' selected ';
								}
								echo ' value='. $Eintrag . '>' . $Eintrag. '</option>';
							}
							else										// Wert und Anzeige in zwei Feldern
							{
								$EintragListe = explode(';',$Eintrag);
								echo '<option ';
								if($rsFilialInfoDaten["FIF_WERT"][0] == $EintragListe[0])
								{
									echo ' selected ';
								}

								echo ' value='. $EintragListe[0] . '>' . $EintragListe[1] . '</option>';
							}

						}
					}
					else 		// SQL Anweisung
					{
						$rsEintragsWert = awisOpenRecordset($con, $rsFilialInfo["FIT_WERTE"][$i]);
						for($EintragsWertZeile=0;$EintragsWertZeile<$awisRSZeilen;$EintragsWertZeile++)
						{
							if($awisRSSpalten==1)			// Keine Wert->Anzeige Kombination
							{
								echo '<option ';
								if($rsFilialInfoDaten["FIF_WERT"][0] == $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile])
								{
									echo ' selected ';
								}
								echo ' value='. $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '>' . $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '</option>';
							}
							else										// Wert und Anzeige in zwei Feldern
							{
								echo '<option ';
								if($rsFilialInfoDaten["FIF_WERT"][0] == $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile])
								{
									echo ' selected ';
								}
								echo ' value='. $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '>' . $rsEintragsWert[$awisRSInfo[2]['Name']][$EintragsWertZeile] . '</option>';
							}
						}
					}
					echo '</select>';

					echo '</td>';
				}
				else
				{
					if(isset($rsFilialInfo["FIT_ANZZEILEN"][$i]) AND $rsFilialInfo["FIT_ANZZEILEN"][$i]==1)
					{
						print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><input type=text size=". $rsFilialInfo["FIT_ANZSPALTEN"][$i] . " name=txtFeld_" . $rsFilialInfo["FIT_ID"][$i] . " value=" . (isset($rsFilialInfoDaten["FIF_WERT"][0])?$rsFilialInfoDaten["FIF_WERT"][0]:'') . "></td>";
					}
					else
					{
						print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><textarea rows=". $rsFilialInfo["FIT_ANZZEILEN"][$i] . " cols=". $rsFilialInfo["FIT_ANZSPALTEN"][$i] . " name=txtFeld_" . $rsFilialInfo["FIT_ID"][$i] . ">" . (isset($rsFilialInfoDaten["FIF_WERT"][0])?$rsFilialInfoDaten["FIF_WERT"][0]:'') . "</textarea></td>";
					}
				}
				$SpeichernButton = True;
				break;
			}
		}

		if($RechteBit>8192)
		{
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
			print $rsFilialInfoDaten["FIF_WERT"][0];
			print "</td>";
		}

		unset($rsFilialInfoDaten);

		print "</tr>";
	}
    print "</table>";
}	// Auswahl oder Details
?>