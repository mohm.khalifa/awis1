<?php

require_once 'awisBerichte.inc';

class ber_Airbagetiketten
    extends awisBerichte
{
    public function init(array $Parameter)
    {
        $ABL_KEYs = implode(', ', $_POST['txtDruckKey']);
        $SQL = 'SELECT ABG_KEY, ABG_BESTELLNR, ABG_FILIALE, ABG_DATUM, ABG_MODELLNR, ABG_MENGE, ABG_BEZEICHNUNG, ABG_AUTO, ABG_XBN_KEY FROM AIRBAGS
                WHERE ABG_KEY IN (' . $ABL_KEYs . ')';

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ABG', true));

        $this->_DB->TransaktionBegin();
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        global $ABG;
        $this->SetTitle('Etiketten');
        $this->SetSubject('');
        $this->SetCreator('AWIS');
        $this->SetAuthor($ABG->AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);

        while (!$this->_rec->EOF()) {
            $i = 1;
            while ($i <= $this->_rec->FeldInhalt('ABG_MENGE', 'Z')) {
                $this->AddPage();
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesse']);

                //Hersteller
                $this->_Zeile = $this->_Parameter['SeiteOben'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell(200, $this->_Parameter['ZeilenHoehe'], $ABG->AWISSprachKonserven['ABG']['ABG_HERSTELLER'], 0, 0, 'L');

                //Typ
                $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell(200, $this->_Parameter['ZeilenHoehe'], $ABG->AWISSprachKonserven['ABG']['ABG_TYP'], 0, 0, 'L');

                //Bestellnr
                $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell(200, $this->_Parameter['ZeilenHoehe'], $ABG->AWISSprachKonserven['ABG']['ABG_BESTELLNR'] . ': ' . $this->_rec->FeldInhalt('ABG_BESTELLNR', 'T'), 0, 0, 'L');

                //Modellnr
                $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell(200, $this->_Parameter['ZeilenHoehe'], $ABG->AWISSprachKonserven['ABG']['ABG_MODELLNR'] . ': ' . $this->_rec->FeldInhalt('ABG_MODELLNR', 'T'), 0, 0, 'L');

                //Auto
                $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell(200, $this->_Parameter['ZeilenHoehe'], $ABG->AWISSprachKonserven['ABG']['ABG_AUTO'] . ': ' . $this->_rec->FeldInhalt('ABG_AUTO', 'T'), 0, 0, 'L');

                $i++;
            }
            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************
        $this->_DB->TransaktionCommit();
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L');
    }
}