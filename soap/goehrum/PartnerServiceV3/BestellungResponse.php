<?php

class BestellungResponse
{

    /**
     * @var PartnerBestellBestaetigungV3 $BestellungResult
     */
    protected $BestellungResult = null;

    /**
     * @param PartnerBestellBestaetigungV3 $BestellungResult
     */
    public function __construct($BestellungResult)
    {
      $this->BestellungResult = $BestellungResult;
    }

    /**
     * @return PartnerBestellBestaetigungV3
     */
    public function getBestellungResult()
    {
      return $this->BestellungResult;
    }

    /**
     * @param PartnerBestellBestaetigungV3 $BestellungResult
     * @return BestellungResponse
     */
    public function setBestellungResult($BestellungResult)
    {
      $this->BestellungResult = $BestellungResult;
      return $this;
    }

}
