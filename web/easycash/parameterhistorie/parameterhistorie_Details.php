<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
   
try
{
    $Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PRH','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','CSVErzeugen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht33000 = $AWISBenutzer->HatDasRecht(33000);
	
    if($Recht33000==0) //wenn kein recht 
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

/*	if (awisWerkzeuge::awisLevel()=='PROD')
	{
	    $RemoteDB='@COM_DE.ATU.DE';
	    $RemoteConn='COM_DE';
	}
	else
	{
	    $RemoteDB='@COM_TEST_DE.ATU.DE';
	    $RemoteConn='COM_TEST_DE';
	}
*/	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Parameterhistorie'));

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	    $Param = array();
		$Param['PRH_DATUM_VON'] = $_POST['sucPRH_DATUM_VON'];
		$Param['PRH_DATUM_BIS'] = $_POST['sucPRH_DATUM_BIS'];
		$Param['PRH_TABELLE'] = $_POST['sucPRH_TABELLE'];
		$Param['PRH_ID'] = $_POST['sucPRH_ID'];
		$Param['PRH_AKTION'] = $_POST['sucPRH_AKTION'];
		$Param['PRH_DETAIL'] = $_POST['sucPRH_DETAIL'];
		$Param['PRH_BEARBEITER'] = $_POST['sucPRH_BEARBEITER'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		
		$AWISBenutzer->ParameterSchreiben('Formular_Parameterhistorie',serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
	    if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			
			if(!isset($Param['ORDER']) AND $Param['ORDER'] = '')
			{
				$Param['ORDER']='DATUM DESC';	
			}
			$AWISBenutzer->ParameterSchreiben('Formular_Parameterhistorie',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}
		$AWIS_KEY1=$Param['KEY'];
	}

    //*********************************************************
	//* Sortierung
	//*********************************************************

	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
		    echo"Hallo";
			$ORDERBY = 'ORDER BY '. $Param['ORDER'];
		}
		else
		{
		    echo"TEST";
			$ORDERBY = ' ORDER BY DATUM DESC';
			$Param['ORDER']='DATUM DESC';
		}
	}
	else
	{	
	    echo"BLUBB";
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$SQL = 'SELECT cond.DATUM, cond.ID, cond.TABLE_NAME, cond.ACTION, cond.UPDATEDBY ';
	$SQL .= ',row_number() OVER (';
	$SQL .= ' '.$ORDERBY;
	$SQL .= ' ) AS ZeilenNr ' ;
	$SQL .= 'FROM ZZZ_TEST_COND_HISTORIE cond ';
	
	//	$SQL = 'Select ID,DATUM,TABLE_NAME,ACTION,UPDATEDBY from zzz_test_cond_historie where id = 100103';
	
	if($Bedingung!='')
	{
	    $SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	$Form->DebugAusgabe(1,$SQL);
	
    //************************************************
	// Aktuellen Datenblock festlegen
	//************************************************
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
	    $Block=$Form->Format('N0',$_REQUEST['Block'],false);
	    $Param['BLOCK']=$Block;
	    $AWISBenutzer->ParameterSchreiben('Formular_Parameterhistorie', serialize($Param));
	}
	elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
	{
	    $Block = $Param['BLOCK'];
	}

	// Zeilen begrenzen
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $MaxDSAnzahl) + 1;
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$MaxDSAnzahl);
	
	$SQL .= $ORDERBY;
	
	$rsPRH = $DB->RecordsetOeffnen($SQL);
	$Param['BLOCK'] = $Block;
	
	$AWISBenutzer->ParameterSchreiben('Formular_Parameterhistorie',serialize($Param));
	

    //********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmparameterhistorie action=./parameterhistorie_Main.php?cmdAktion=Details method=POST>';
	
	$DetailAnsicht = false;
	
	$FeldBreiten = array();
	$FeldBreiten['Zeile'] = 50;
	$FeldBreiten['Datum'] = 90;
	$FeldBreiten['Tabelle'] = 200;
	$FeldBreiten['ID'] = 70;
	$FeldBreiten['Aktion'] = 220;
	$FeldBreiten['Detail'] = 200;
	$FeldBreiten['Bearbeiter'] = 150;
    
	if($rsPRH->EOF())		// Keine Meldung bei neuen Datens�tzen!
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsPRH->AnzahlDatensaetze()>=1)						// Liste anzeigen
	{
	    $Form->Formular_Start();
	    $Form->ZeileStart('font-size:11pt');
	
	    $Form->ZeileStart();
	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=PRH_ZEILE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_ZEILE'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_ZEILE'].':',$FeldBreiten['Zeile'],'',$Link);

	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_DATUM'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_DATUM'].':',$FeldBreiten['Datum'],'',$Link);

	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=TABLE_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_TABELLE'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_TABELLE'].':',$FeldBreiten['Tabelle'],'',$Link);

	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_ID'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_ID'].':',$FeldBreiten['ID'],'',$Link);

	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=ACTION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_AKTION'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_AKTION'].':',$FeldBreiten['Aktion'],'',$Link);

	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=DETAIL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_DETAIL'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_DETAIL'].':',$FeldBreiten['Detail'],'',$Link);
	    
	    $Link = './parameterhistorie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
	    $Link .= '&Sort=UPDATEDBY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PRH_BEARBEITER'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRH']['PRH_BEARBEITER'].':',$FeldBreiten['Bearbeiter'],'',$Link);
	    $Form->ZeileEnde();
	    
	    if($rsPRH->AnzahlDatensaetze() > 12) //Scrolleiste
	    {
	        $Form->SchreibeHTMLCode('<div id=scrKNZ style="width: 100%; height: 450px;  overflow-y: scroll;">');
	    }
	    
	    $DS=0;
        
	    while(!$rsPRH->EOF())
	    {
            $Form->ZeileStart('font-size:11pt');
            $Form->Erstelle_ListenFeld('!PRH_ZEILE',$rsPRH->FeldInhalt('ZEILENNR'),4,$FeldBreiten['Zeile'],false,($DS%2),'','','Z');
            $Form->Erstelle_ListenFeld('!PRH_DATUM',$rsPRH->FeldInhalt('DATUM'),4,$FeldBreiten['Datum'],false,($DS%2),'','','D');
            $Form->Erstelle_ListenFeld('!PRH_TABELLE',$rsPRH->FeldInhalt('TABLE_NAME'),24,$FeldBreiten['Tabelle'],false,($DS%2),'','','T');
            $Form->Erstelle_ListenFeld('!PRH_ID',$rsPRH->FeldInhalt('ID'),30,$FeldBreiten['ID'],false,($DS%2),'','','Z');
            $Form->Erstelle_ListenFeld('!PRH_AKTION',$rsPRH->FeldInhalt('ACTION'),220,$FeldBreiten['Aktion'],false,($DS%2),'','','T');
            $Form->Erstelle_ListenFeld('!PRH_DETAIL',$rsPRH->FeldInhalt('DETAIL'),200,$FeldBreiten['Detail'],false,($DS%2),'','','T');
            $Form->Erstelle_ListenFeld('!PRH_BEARBEITER',$rsPRH->FeldInhalt('UPDATEDBY'),20,$FeldBreiten['Bearbeiter'],false,($DS%2),'','','T');
            $Form->ZeileEnde();
	        
	        $rsPRH->DSWeiter();
	        $DS++;
	    }
	    
	    if($rsPRH->AnzahlDatensaetze() > 12)
		{
			$Form->SchreibeHTMLCode('</div>') ;
		}
	    
	    $Link = './parameterhistorie_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$MaxDSAnzahl,$Link,$Block,'');
		$Form->Formular_Ende();

	}			// Eine einzelne Adresse
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	
	if(($Recht33000&4) == 4)
	{
	    /*
		$LinkPDF = '/berichte/drucken.php?XRE=45&ID='.base64_encode('&KNZ_KEY=' . $Param['KNZ_CHECKED'].
																	'&KNZ_FILNR=' . $Param['KNZ_FILNR'] . 
																	'&KNZ_DATUM_VON='. $Param['KNZ_DATUM_VON'] .
																	'&KNZ_DATUM_BIS='. $Param['KNZ_DATUM_BIS'] .
																	'&KNZ_BETRAG=' .$Param['KNZ_BETRAG'].
																	'&KNZ_ART='. $Param['KNZ_ART'] .
																//	'&KNZ_BLZ=' . $Param['KNZ_BLZ'].
																	'&KNZ_KTONR=' . $Param['KNZ_KTONR']);
		$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
        */
	 
		$LinkCSV = './parameterhistorie_csv.php?XRE=45&ID='.base64_encode('&PRH_DATUM_VON=' . $Param['PRH_DATUM_VON'] .
				'&PRH_DATUM_BIS='.$Param['PRH_DATUM_BIS'].
		        '&PRH_TABELLE='. $Param['PRH_TABELLE'] .
				'&PRH_ID='. $Param['PRH_ID'] .
				'&PRH_AKTION=' .$Param['PRH_AKTION'].
//				'&PRH_DETAIL='. $Param['PRH_DETAIL'] .
				'&PRH_BEARBEITER=' . $Param['PRH_BEARBEITER']);
		$Form->Schaltflaeche('href', 'cmdCSV', $LinkCSV, '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['Wort']['CSVErzeugen']);
		
	}
	
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151651");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151652");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWIS_KEY2;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
    
	if(isset($Param['PRH_DATUM_VON']) OR isset($Param['PRH_DATUM_BIS']))
	{
	    if(isset($Param['PRH_DATUM_BIS']) AND $Param['PRH_DATUM_BIS'] != '')
	    {
	        $Bedingung .= ' AND TRUNC(cond.DATUM) >= ' .$DB->FeldInhaltFormat('D',$Param['PRH_DATUM_VON']) . '';
	
	        $Bedingung .= ' AND TRUNC(cond.DATUM) <= ' .$DB->FeldInhaltFormat('D',$Param['PRH_DATUM_BIS']) . '';
	    }
	    else
	    {
	        if($Param['PRH_DATUM_VON'] != '')
	        {
	            $Bedingung .= ' AND TRUNC(cond.DATUM) = ' .$DB->FeldInhaltFormat('D',$Param['PRH_DATUM_VON']) . '';
	        }
	    }
	}
	
	if(isset($Param['PRH_TABELLE']) AND $Param['PRH_TABELLE']!='')
	{
		$Bedingung .= ' AND TABLE_NAME = '. $DB->FeldInhaltFormat('T',$Param['PRH_TABELLE']) . ' ';
	}
	
	if(isset($Param['PRH_ID']) AND $Param['PRH_ID']!='')
	{
		$Bedingung .= ' AND UPPER(ID) ' .$DB->LIKEoderIST('%'.$Param['PRH_ID'].'%',awisDatenbank::AWIS_LIKE_UPPER) . '';
	}
	
	if(isset($Param['PRH_AKTION']) AND $Param['PRH_AKTION']!='')
	{
		$Bedingung .= ' AND UPPER(cond.ACTION) ' .$DB->LIKEoderIST('%'.$Param['PRH_AKTION'].'%',awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['PRH_DETAIL']) AND $Param['PRH_DETAIL']!='')
	{
		$DB->SetzeBindevariable('PRH', 'var_T_prh_detail', $Param['PRH_DETAIL'], awisDatenbank::AWIS_LIKE_UPPER);
		$Bedingung .= ' AND DETAIL like :var_T_prh_detail';
	}
    
	if(isset($Param['PRH_BEARBEITER']) AND $Param['PRH_BEARBEITER']!='')
	{
	    $Bedingung .= ' AND Upper(UPDATEDBY)' .$DB->LIKEoderIST('%'.$Param['PRH_BEARBEITER'].'%',awisDatenbank::AWIS_LIKE_UPPER) . '';
	}
	
	return $Bedingung;
}

?>