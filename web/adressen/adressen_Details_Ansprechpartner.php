<?php
global $AWISBenutzer;
global $awisRSZeilen;
global $AWISSprache;			
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try 
{ 
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','NeueKontaktart');
    $TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('KOT','KOT_TYP');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    
    $Recht3000 = $AWISBenutzer->HatDasRecht(3000);
    if($Recht3000==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
    }
    
    $EditModus = ($Recht3000&6);
    
    $SQL = 'SELECT *';
    $SQL .= ' FROM AdressenAnsprechpartner';
    $SQL .= ' WHERE AAP_ADR_KEY=0'.$AWIS_KEY1;
    
    if(isset($_GET['AAPKEY']))
    {
        $AWIS_KEY2 = $_GET['AAPKEY'];
    }
    elseif(isset($_POST['txtAAP_KEY']))
    {
    	$AWIS_KEY2='';
    }
    if(isset($_GET['AAP_KEY']))
    {
        $SQL .= ' AND AAP_KEY=0'.intval($_GET['AAP_KEY']);
    }
    if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
    {
        $SQL .= ' AND AAP_KEY=0'.intval($AWIS_KEY2);
    }
    if(!isset($_GET['AAP_Sort']))
    {
        $SQL .= ' ORDER BY AAP_Nachname, AAP_Vorname';
    }
    else
    {
        $SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['AAP_Sort']);
    }
    
    $rsAAP = $DB->RecordSetOeffnen($SQL);
    $rsAAPZeilen = $rsAAP->AnzahlDatensaetze();
    
    if(!isset($AWIS_KEY2) or $AWIS_KEY2=='' or isset($_GET['APListe']))				// Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();
    
        if($EditModus>0)
        {
            $Icons[] = array('new','./adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner&AAPKEY=0');
            $Form->Erstelle_ListeIcons($Icons,54,-1);
        }
    
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
        $Link .= '&AAPSort=AAP_NACHNAME'.((isset($_GET['AAP_Sort']) AND ($_GET['AAP_Sort']=='AAP_NACHNAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AAP']['AAP_NACHNAME'].', '.$AWISSprachKonserven['AAP']['AAP_VORNAME'],350,'',$Link);
    
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
        $Link .= '&AAPSort=AAP_POSITION'.((isset($_GET['AA_PSort']) AND ($_GET['AAP_Sort']=='AAP_POSITION'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AAP']['AAP_POSITION'],350,'',$Link);
        
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner';
        $Link .= '&AAPSort=AAP_TELEFON'.((isset($_GET['AAP_Sort']) AND ($_GET['AAP_Sort']=='AAP_TELEFON'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AAP']['AAP_TELEFON'],250,'',$Link);
    
        $Form->ZeileEnde();
    
        for($AAPZeile=0;$AAPZeile<$rsAAPZeilen;$AAPZeile++)
        {
            $Form->ZeileStart();
            $Icons = array();
    
            if($EditModus>0)	// Ändernrecht
            {
                 $Icons[] = array('edit','./adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner&AAPKEY='.$rsAAP->FeldInhalt('AAP_KEY'));
			     $Icons[] = array('delete','./adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Del='.$rsAAP->FeldInhalt('AAP_KEY'));
            }
            if($rsAAP->FeldInhalt('AAP_EMAIL')!='')
            {
			     $Icons[] = array('mail','mailto:'.$rsAAP->FeldInhalt('AAP_EMAIL'));
            }
            
            $Form->Erstelle_ListeIcons($Icons,54,($AAPZeile%2));
            $Form->Erstelle_ListenFeld('AAP_NACHNAME',$rsAAP->FeldInhalt('AAP_NACHNAME').', '.$rsAAP->FeldInhalt('AAP_VORNAME'),20,350,false,($AAPZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('AAP_POSITION',$rsAAP->FeldInhalt('AAP_POSITION'),20,350,false,($AAPZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('AAP_TELEFON',$rsAAP->FeldInhalt('AAP_TELEFON'),20,250,false,($AAPZeile%2),'','','T');
    		$Form->ZeileEnde();
    		
    		$rsAAP->DSWeiter();
        }
    
    	$Form->Formular_Ende();
    	
    }
    else 		// Einer oder keiner
    {

	   $Form->Formular_Start();
	   $Form->Erstelle_HiddenFeld('AAP_KEY',$AWIS_KEY2);

       // Infozeile zusammenbauen
	   $Felder = array();
	   $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Details&Seite=Ansprechpartner&APListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	   $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAAP->FeldInhalt('AAP_KEY')!= ''?$rsAAP->FeldInhalt('AAP_USER'):''));
	   $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAAP->FeldInhalt('AAP_KEY')!=''?$rsAAP->FeldInhalt('AAP_USERDAT'):''));
       $Form->InfoZeile($Felder,'');
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_ANREDE'].':',150);
	   $Liste=explode('|',$AWISSprachKonserven['AAP']['lst_AAP_ANREDE']);                                          
	   $Form->Erstelle_SelectFeld('AAP_ANREDE',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_ANREDE'):'',200,$EditModus,'','','','',1,$Liste);

       if($EditModus>0)
       {
    	   $CursorFeld='txtAAP_ANREDE';
       }
       
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_TITEL'].':',150);
       $Form->Erstelle_TextFeld('AAP_TITEL',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_TITEL'):'',20,200,$EditModus);
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_NACHNAME'].':',150);
       $Form->Erstelle_TextFeld('AAP_NACHNAME',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_NACHNAME'):'',20,200,$EditModus);
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_VORNAME'].':',150);
       $Form->Erstelle_TextFeld('AAP_VORNAME',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_VORNAME'):'',20,200,$EditModus);
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_TELEFON'].':',150);
       $Form->Erstelle_TextFeld('AAP_TELEFON',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_TELEFON'):'',20,200,$EditModus);
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_TELEFAX'].':',150);
       $Form->Erstelle_TextFeld('AAP_TELEFAX',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_TELEFAX'):'',50,500,$EditModus);
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_MOBILTELEFON'].':',150);
       $Form->Erstelle_TextFeld('AAP_MOBILTELEFON',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_MOBILTELEFON'):'',20,200,$EditModus);
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_EMAIL'].':',150);
       $Link = ($rsAAP->FeldInhalt('AAP_KEY')!='' AND $rsAAP->FeldInhalt('AAP_EMAIL')!='')?'mailto:'.$rsAAP->FeldInhalt('AAP_EMAIL'):'';
       $Form->Erstelle_TextFeld('AAP_EMAIL',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_EMAIL'):'',50,500,$EditModus,'','',$Link,'T');
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_ABTEILUNG'].':',150);
       $Form->Erstelle_TextFeld('AAP_ABTEILUNG',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_ABTEILUNG'):'',20,200,$EditModus);
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_POSITION'].':',150);
       $Form->Erstelle_TextFeld('AAP_POSITION',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_POSITION'):'',50,500,$EditModus);
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_AKTIONEN'].':',150);
	   $Liste = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
       $Form->Erstelle_SelectFeld('AAP_AKTIONEN',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_AKTIONEN'):'',200,$EditModus,'','','','0','',$Liste);
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_DATUMVOM'].':',150);
       $Form->Erstelle_TextFeld('AAP_DATUMVOM',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_DATUMVOM'):'',10,200,$EditModus,'','','','D');
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_DATUMBIS'].':',150);
       $Form->Erstelle_TextFeld('AAP_DATUMBIS',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_DATUMBIS'):'',10,200,$EditModus,'','','','D');
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_GEBURTSDATUM'].':',150);
	   $Form->Erstelle_TextFeld('AAP_GEBURTSDATUM',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_GEBURTSDATUM'):'',10,200,$EditModus,'','','','D');
       $Form->ZeileEnde();
    
       $Form->ZeileStart();
       $Form->Erstelle_TextLabel($AWISSprachKonserven['AAP']['AAP_BEMERKUNG'].':',150);
       $Form->Erstelle_Textarea('AAP_BEMERKUNG',$AWIS_KEY2!=0?$rsAAP->FeldInhalt('AAP_BEMERKUNG'):'',300,150,2,$EditModus);
	   $Form->ZeileEnde();
    
       if ($rsAAP->FeldInhalt('AAP_KEY')!='')
	   {
		  $Form->Trennzeile();
    
		  //Neue Abfrage, liefert nur Datensätze der aktuellen Adresse
		  $SQL = 'SELECT *';
		  $SQL .= ' FROM AdressenInfoTypen';
		  $SQL .= ' LEFT OUTER JOIN AdressenInformationen ON ADT_KEY = ADI_ADT_KEY AND ADI_XXX_KEY='.$AWIS_KEY2;
		  $SQL .= ' INNER JOIN AdressenAnsprechpartner on AAP_KEY=' . $AWIS_KEY2;
		  $SQL .= ' INNER JOIN Adressen on ADR_KEY = AAP_ADR_KEY';
		  $SQL .= ' AND Adressen.ADR_AZG_KEY = ADT_AZG_KEY';
		  $SQL .= ' WHERE BITAND(ADT_VERWENDUNG,2)=2 AND ADT_STATUS=\'A\'';
		  $SQL .= ' ORDER BY ADT_SORTIERUNG, ADT_INFORMATION';
		  
    	  $rsADI = $DB->RecordSetOeffnen($SQL);
    	  $rsADIZeilen = $rsADI->AnzahlDatensaetze();
    
    	  $LetzterBereich = '';
    	  for($ADIZeile=0;$ADIZeile<$rsADIZeilen;$ADIZeile++)
    		  {
    		      $Form->ZeileStart();
			      $Form->Erstelle_TextLabel($rsADI->FeldInhalt('ADT_INFORMATION').':',200);
    			  if($rsADI->FeldInhalt('ADT_DATENQUELLE')!='')
			      {
    			     switch(substr($rsADI->FeldInhalt('ADT_DATENQUELLE'),0,3))
    			     {
    			         case 'TXT':
    			             $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
    			             $Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
    			             $Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
    			             $Form->Erstelle_SelectFeld('API_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
    			         break;
					     case 'SQL':
    					     $Felder = explode(':',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
    					     $Form->Erstelle_SelectFeld('API_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'',$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
						 break;
    					 default:
    						$Form->Erstelle_TextFeld('API_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'),'','',$rsADI->FeldInhalt('ADT_DATENQUELLE'));
    					 break;
    				  }
			     }
    			 else
			     {
                    $Form->Erstelle_TextFeld('API_WERT_'.$rsADI->FeldInhalt('ADT_KEY').'_'.$rsADI->FeldInhalt('ADI_KEY'),$rsADI->FeldInhalt('ADI_WERT'),$rsADI->FeldInhalt('ADT_ZEICHEN'),$rsADI->FeldInhalt('ADT_BREITE'),$EditModus,'','','',$rsADI->FeldInhalt('ADT_FORMAT'));
    		     }
    		     $Form->ZeileEnde();
    		     $rsADI->DSWeiter();
            }
        }		
    	$Form->Formular_Ende();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>