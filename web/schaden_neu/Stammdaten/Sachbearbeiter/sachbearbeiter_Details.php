<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	
	$script = "<script type='text/javascript'>
	
	function Postfach(obj) {
		
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkWert == '0')
    	       {
					document.getElementById('Sach').style.display = 'block';
					document.getElementById('Sammel').style.display = 'none';
					document.getElementsByName('cmdSpeichern')[0].style.display = 'block';
					document.getElementsByName('txtART')[0].value = checkWert; 
					document.getElementsByName('txtBES_NAME')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_NAME')[0].setAttribute('required','');
					document.getElementsByName('txtBES_VORNAME')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_VORNAME')[0].setAttribute('required','');
					document.getElementsByName('txtBES_KUERZEL')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_KUERZEL')[0].setAttribute('required','');
					document.getElementsByName('txtBES_GUELTIG')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_GUELTIG')[0].setAttribute('required','');
					document.getElementsByName('txtBES_AWISUSR')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_AWISUSR')[0].setAttribute('required','');
			
					document.getElementsByName('txtBES_NAME_S')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_NAME_S')[0].removeAttribute('required');
					document.getElementsByName('txtBES_KUERZEL_S')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_KUERZEL_S')[0].removeAttribute('required');
					document.getElementsByName('txtBES_GUELTIG_S')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_GUELTIG_S')[0].removeAttribute('required');
			   }
    	       else if(checkWert == '1')
    	       {
					document.getElementById('Sammel').style.display = 'block';
					document.getElementById('Sach').style.display = 'none';
					document.getElementsByName('cmdSpeichern')[0].style.display = 'block'; 
					document.getElementsByName('txtART')[0].value = checkWert; 
					document.getElementsByName('txtBES_NAME')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_NAME')[0].removeAttribute('required');
					document.getElementsByName('txtBES_VORNAME')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_VORNAME')[0].removeAttribute('required');
					document.getElementsByName('txtBES_KUERZEL')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_KUERZEL')[0].removeAttribute('required');
					document.getElementsByName('txtBES_GUELTIG')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_GUELTIG')[0].removeAttribute('required');
					document.getElementsByName('txtBES_AWISUSR')[0].className = 'InputText'; 
				    document.getElementsByName('txtBES_AWISUSR')[0].removeAttribute('required');
					
					document.getElementsByName('txtBES_NAME_S')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_NAME_S')[0].setAttribute('required','');
					document.getElementsByName('txtBES_KUERZEL_S')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_KUERZEL_S')[0].setAttribute('required','');
					document.getElementsByName('txtBES_GUELTIG_S')[0].className = 'InputTextPflicht'; 
					document.getElementsByName('txtBES_GUELTIG_S')[0].setAttribute('required','');
			   }
			   else
			   {
			   		document.getElementById('Sach').style.display = 'none';
					document.getElementById('Sammel').style.display = 'none';
					document.getElementsByName('cmdSpeichern')[0].style.display = 'none'; 
			   }
		}
	
	</script>";
	
	echo $script;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40008 = $AWISBenutzer->HatDasRecht(40008);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SBB'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;	
	
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	
	if (isset($_GET['button']))
	{
		if(isset($_GET['aktiv'])) {
			$Aktiv = '';
			if($_GET['aktiv'] == '0')
			{
				$Aktiv = 1;
			}
			else
			{
				$Aktiv = 0;
			}
			$SQL = "UPDATE SACHBEARBEITER ";
			$SQL .= "SET AUSGESCHIEDEN =" . $Aktiv;
			$SQL .= ',  SBUSER=\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',  SBUSERDAT=SYSDATE';
			$SQL .= " WHERE ID = " . $_GET['button'];
			$DBSCHAD->Ausfuehren($SQL);
		}
		if(isset($_GET['aktivFil'])) {
			$AktivFil = '';
			if($_GET['aktivFil'] == '0')
			{
				$AktivFil = 1;
			}
			else
			{
				$AktivFil = 0;
			}
			$SQL = "UPDATE SACHBEARBEITER ";
			$SQL .= "SET SBAKTIVFIL =" . $AktivFil;
			$SQL .= ',  SBUSER=\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',  SBUSERDAT=SYSDATE';
			$SQL .= " WHERE ID = " . $_GET['button'];
			$DBSCHAD->Ausfuehren($SQL);
		}
	}
	
	elseif($FelderDel != '' or isset($_POST['cmdLoeschenOK']))
	{
		include('./sachbearbeiter_loeschen.php');
	}
	
	if($FelderNew != '')
	{
		include('./sachbearbeiter_speichern.php');
	}
	
    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'SBNAME';
 	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40008==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_GET['ID']))
	{
		$AWIS_KEY1 = $_GET['ID'];
	}
	if(isset($_POST['txtID']) and !isset($_POST['cmdSpeichern_x']) or (isset($_POST['cmdHinzufuegen_x'])))
	{
		$AWIS_KEY1 = $_POST['txtID'];
	}
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['BES_SBNAME'] = 	$Form->Format('T',$_POST['sucBES_SBNAME'],true);
		$Param['BES_SBVORNAME']= $Form->Format('T',$_POST['sucBES_SBVORNAME'],true);
		$Param['BES_AKTIV']= $Form->Format('N0',$_POST['sucBES_AKTIV'],true);
		$Param['BES_SAMMEL']=isset($_POST['sucBES_SAMMEL'])?1:0;
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./sachbearbeiter_speichern.php');
		
		if($Fehler != 0)
		{
			if($Fehler == 2)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGEEMPTY']);
				$Form->ZeileEnde();
			}
		}
	}

	/** Test-SQL Eskalationen **/
	
	$SQL = ' select ID,SBNAME,SBVORNAME,SBKUERZEL,AUSGESCHIEDEN,SCHADENBESCHWERDEN,AWIS_USER,SBUSER,SBUSERDAT,SBART,SBAKTIVFIL,row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= 'select ID,SBKUERZEL,SBNAME,SBVORNAME,SCHADENBESCHWERDEN,AUSGESCHIEDEN,AWIS_USER,SBUSER,SBUSERDAT,SBART,SBAKTIVFIL ';
	$SQL .=	' from sachbearbeiter) ';

	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '')
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '')
	{
		if($NeuAnlage == 1 or isset($_GET['HRT_KEY']) or $AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE ID='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND ID='.$AWIS_KEY1;
		}
		
	
	}
	


	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	if(!isset($_GET['ID']) and $AWIS_KEY1 == '')
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsSBB = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['Name'] = 250;
	$FeldBreiten['Vorname'] = 250;
	$FeldBreiten['Kuerzel'] = 100;
	$FeldBreiten['AKTIV'] = 20;
	$FeldBreiten['AKTIVFIL'] = 20;
	$FeldBreiten['GUELTIG'] = 100;
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 3.2);
	}
	
	$Form->SchreibeHTMLCode('<form name="frmSBBDetails" action="./sachbearbeiter_Main.php?cmdAktion=Details" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsSBB->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
		//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
		// �berschrift der Listenansicht mit Sortierungslink: Filiale
		$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=SBNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SBNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_SBNAME'], $FeldBreiten['Name'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=SBVORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SBVORNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_SBVORNAME'], $FeldBreiten['Vorname'], '',$Link);
		// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
		$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=SBKUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SBKUERZEL'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_SBKUERZEL'], $FeldBreiten['Kuerzel'], '', $Link);
		$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=SCHADENBESCHWERDEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SCHADENBESCHWERDEN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_GUELTIG'], $FeldBreiten['GUELTIG'], '', $Link);
		if (($Recht40008&2) == 2)
		{
			$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=AUSGESCHIEDEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AUSGESCHIEDEN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIV'];
			$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['AKTIV'], '',$Link,$TTT);
		}
		if (($Recht40008&2) == 2)
		{
			$Link = './sachbearbeiter_Main.php?cmdAktion=Details&Sort=SBAKTIVFIL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SBAKTIVFIL'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIVFIL'];
			$Form->Erstelle_Liste_Ueberschrift('F', $FeldBreiten['AKTIVFIL'], '',$Link,$TTT);
		}
		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsSBB->EOF())
		{
			$Form->ZeileStart();
			if (($Recht40008&2) == 2)
			{
				$Link = './sachbearbeiter_Main.php?cmdAktion=Details&ID='.$rsSBB->FeldInhalt('ID') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			}
			else 
			{
				$Link = '';
			}
			$TTT = $rsSBB->FeldInhalt('SBNAME');
			$Form->Erstelle_ListenFeld('BES_NAME', $rsSBB->FeldInhalt('SBNAME'), 0, $FeldBreiten['Name'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$TTT =  $rsSBB->FeldInhalt('SBVORNAME');
			$Form->Erstelle_ListenFeld('BES_VORNAME',$rsSBB->FeldInhalt('SBVORNAME'), 0, $FeldBreiten['Vorname'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsSBB->FeldInhalt('SBKUERZEL');
			$Form->Erstelle_ListenFeld('BES_KUERZEL',$rsSBB->FeldInhalt('SBKUERZEL'), 0, $FeldBreiten['Kuerzel'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsSBB->FeldInhalt('SCHADENBESCHWERDEN');
			$Form->Erstelle_ListenFeld('BES_GUELTIG',$rsSBB->FeldInhalt('SCHADENBESCHWERDEN'), 0, $FeldBreiten['GUELTIG'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$LinkAktiv = '';
			if (($Recht40008&2) == 2)
			{
				$LinkAktiv =  './sachbearbeiter_Main.php?cmdAktion=Details&button='.$rsSBB->FeldInhalt('ID').'&aktiv='.$rsSBB->FeldInhalt('AUSGESCHIEDEN');
				if ($rsSBB->FeldInhalt('AUSGESCHIEDEN') == '0')
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				else
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['AKTIV'] , ($DS%2));
				$IconsArray = '';
			}
			if (($Recht40008&2) == 2)
			{
				$LinkAktiv =  './sachbearbeiter_Main.php?cmdAktion=Details&button='.$rsSBB->FeldInhalt('ID').'&aktivFil='.$rsSBB->FeldInhalt('SBAKTIVFIL');
				if ($rsSBB->FeldInhalt('SBAKTIVFIL') == '1')
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_AktivFil']);
				}
				else
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_InaktivFil']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['AKTIVFIL'] , ($DS%2));
				$IconsArray = '';
			}
			
			/** Neue History erst mit VERSION 2
			 * 
			 */
			
			/*
			if(($Recht34000&8)==8)
			{
				$SQL  = ' Select * from QMHISTORY '; 
				$SQL .= ' where QMH_QMP_KEY='.$rsKDI->FeldInhalt('QMP_KEY');
				$SQL .= ' order by QMH_USERDAT';
				
				$rsQMH = $DB->RecordSetOeffnen($SQL);
				$Bild = '';
				
				if($rsQMH->AnzahlDatensaetze() > 0)
				{
					$Bild = '/bilder/attach.png';
					$Form->Erstelle_ListenBild('script','QMZ_HIST','onclick="ladeDiv('.$rsKDI->FeldInhalt('QMP_KEY').');"',$Bild,'',($DS%2),'',16,18,$FeldBreiten['Hist'],'C');
				}
				else 
				{
					$Form->Erstelle_ListenFeld('QMZ_HIST','', 0, $FeldBreiten['Hist'], false, ($DS%2), '','', 'T', 'C','');
				}				
			}
			*/
			$Form->ZeileEnde();
			$DS++;
			$rsSBB->DSWeiter();
		}
		
		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBB->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './sachbearbeiter_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
		
		//$Link = './sachbearbeiter_Main.php?cmdAktion=Details';
		//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif(($rsSBB->AnzahlDatensaetze() == 1) or $AWIS_KEY1 == '-1')
	{
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./sachbearbeiter_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBB->FeldInhalt('SBUSER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBB->FeldInhalt('SBUSERDAT'));
			$Form->InfoZeile($Felder,'');

		
		if($rsSBB->FeldInhalt('SBART') == '')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ART'] . ':',140);
			$Daten = explode('|',$AWISSprachKonserven['BES']['SBA_LST_ART']);
			$Form->Erstelle_SelectFeld('BES_ART',(isset($_REQUEST['txtBES_ART']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_ART']:$rsSBB->FeldInhalt('SBART'),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="Postfach(this)";');
			$Form->ZeileEnde();
			
		}
		
		
		if($rsSBB->FeldInhalt('SBART') == '0')
		{
			$Form->ZeileStart('display:block','Sach"ID="Sach');
			$Require = '!';
		}
		else 
		{
			$Form->ZeileStart('display:none','Sach"ID="Sach');
			$Require = '';
		}
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBNAME'] . ':',140);
		$Form->Erstelle_TextFeld($Require.'BES_NAME',(isset($_REQUEST['txtBES_NAME']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_NAME']:$rsSBB->FeldInhalt('SBNAME'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		if($rsSBB->FeldInhalt('SBART') == '0')
		{
		  $AWISCursorPosition = 'txtBES_NAME';
		}
		else if($rsSBB->FeldInhalt('SBART') == '1')
		{
		    $AWISCursorPosition = 'txtBES_NAME_S';
		}
		else 
		{
		    $AWISCursorPosition = 'txtBES_ART';
		}
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBVORNAME'] . ':',140);
		$Form->Erstelle_TextFeld($Require.'BES_VORNAME',(isset($_REQUEST['txtBES_VORNAME']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_VORNAME']:$rsSBB->FeldInhalt('SBVORNAME'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBKUERZEL'] . ':',140);
		$Form->Erstelle_TextFeld($Require.'BES_KUERZEL',(isset($_REQUEST['txtBES_KUERZEL']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_KUERZEL']:$rsSBB->FeldInhalt('SBKUERZEL'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GUELTIG'] . ':',140);
		$Daten = explode('|',$AWISSprachKonserven['BES']['SBA_LST_GUELTIG']);
		$Form->Erstelle_SelectFeld($Require.'BES_GUELTIG',(isset($_REQUEST['txtBES_GUELTIG']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_GUELTIG']:$rsSBB->FeldInhalt('SCHADENBESCHWERDEN'),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		
		$SQL2 = 'select xbn_key,lower(xbl_login) as AWIS_USER from benutzer ';
		$SQL2 .= 'inner join benutzerlogins on xbn_key = xbl_xbn_key ';
		$SQL2 .= 'where xbn_kon_key is not null and xbn_kon_key != 0 and xbn_status = \'A\' ';
		$SQL2 .= 'order by AWIS_USER';

		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AWISUSR'] . ':',140);
		$Form->Erstelle_SelectFeld($Require.'BES_AWISUSR',(isset($_REQUEST['txtBES_AWISUSR']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_AWISUSR']:$rsSBB->FeldInhalt('AWIS_USER'),120,true,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','AWIS');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV',(isset($_REQUEST['txtCHK_AKTIV']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AKTIV']:$rsSBB->FeldInhalt('AUSGESCHIEDEN'),40,true,0);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVFIL'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIVFIL',(isset($_REQUEST['txtCHK_AKTIVFIL']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AKTIVFIL']:$rsSBB->FeldInhalt('SBAKTIVFIL'),40,true,1);
		$Form->ZeileEnde();

		$FeldBreiten = array();
		$FeldBreiten['Blank'] = 20;
		$FeldBreiten['Firmierung'] = 130;
		
		if($AWIS_KEY1 != '-1')
		{
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Filiale
			//$Form->Schaltflaeche('image', 'cmdHinzufuegen','', '/bilder/icon_add.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'W','',13,13);
			
			if($AWIS_KEY1 == '')
			{
				$AWIS_KEY1 = $rsSBB->FeldInhalt('ID');
			}
			
			$SQL ='select spz.id zuordid,sb.id sbid,sb.sbname from sachbearbeiterzuord spz ';
			$SQL .='left join sachbearbeiter sb on spz.sp_key = sb.id';
			$SQL .=' where spz.sb_key = '.$AWIS_KEY1;
			$SQL .=' and sb.ausgeschieden = 0';
			$SQL .=' order by sbname';
			
			$rsZuord = $DBSCHAD->RecordSetOeffnen($SQL);
			
			$SQL2 ='select sb.id,sb.sbname from sachbearbeiter sb ';
			$SQL2 .='left join  sachbearbeiterzuord spz ON spz.sp_key = sb.id';
			$SQL2 .=' and spz.sb_key ='.$AWIS_KEY1;
			$SQL2 .=' where sbart = 1';
			$SQL2 .=' and spz.sb_key is null';
			$SQL2 .=' and sb.ausgeschieden = 0';
			$SQL2 .=' order by sbname';
			
			
			
			$rsList = $DBSCHAD->RecordSetOeffnen($SQL2);
			
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
			//$Link = './taetigkeiten_Main.php?cmdAktion=Details&HRT_KEY='.$AWIS_KEY1.'&Sort2=FIRM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIRM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_SAMMEL'], $FeldBreiten['Firmierung'],'', '');
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
			
			//$Form->Erstelle_HiddenFeld('Seite','Taetigkeiten');
				
			while(! $rsZuord->EOF())
			{
				$Form->ZeileStart();
				$Icons = array();
				//$Icons[] = array('delete','"./taetigkeiten_Main.php?cmdAktion=Details&Del='.$rsZuord->FeldInhalt('HTZ_KEY').'&HRT_KEY='.$AWIS_KEY1.'" onmouseover="urlanpassen(this);"');
				$Icons[] = array('delete','POST~'.$rsZuord->FeldInhalt('ZUORDID'));
				$Form->Erstelle_ListeIcons($Icons,20,($DS%2));
				$TTT = $rsZuord->FeldInhalt('FIRM');
				$Form->Erstelle_ListenFeld('BES_ARTS',$rsZuord->FeldInhalt('SBNAME'), 0, $FeldBreiten['Firmierung'], false, ($DS%2),'','', 'T', 'L', $TTT);
				$Form->ZeileEnde();
				$DS++;
				$rsZuord->DSWeiter();
			}
				
			if($rsList->AnzahlDatensaetze() > 0)
			{
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('add','POST~'.$AWIS_KEY1);
				$Form->Erstelle_ListeIcons($Icons,20,($DS%2));
				$Form->Erstelle_SelectFeld('BES_ART',isset($_REQUEST['txtHRT_FIRM'])?$_REQUEST['txtHRT_FIRM']:'0','300:135',true,$SQL2,'','','','','','','',array(),'font-size:14px;height:22px','SCHAD');
				$Form->ZeileEnde();
			}
			
			
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		$Form->ZeileEnde();
		
		if($rsSBB->FeldInhalt('SBART') == '1')
		{
			$Form->ZeileStart('display:block','Sammel"ID="Sammel');
			$Require = '!';
		}
		else
		{
			$Form->ZeileStart('display:none','Sammel"ID="Sammel');
			$Require = '';
		}
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBNAME'] . ':',140);
		$Form->Erstelle_TextFeld($Require.'BES_NAME_S',(isset($_REQUEST['txtBES_NAME']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_NAME']:$rsSBB->FeldInhalt('SBNAME'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBVORNAME'] . ':',140);
		$Form->Erstelle_TextFeld('BES_VORNAME_S',(isset($_REQUEST['txtBES_VORNAME']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_VORNAME']:$rsSBB->FeldInhalt('SBVORNAME'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SBKUERZEL'] . ':',140);
		$Form->Erstelle_TextFeld($Require.'BES_KUERZEL_S',(isset($_REQUEST['txtBES_KUERZEL']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_KUERZEL']:$rsSBB->FeldInhalt('SBKUERZEL'), 30,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GUELTIG'] . ':',140);
		$Daten = explode('|',$AWISSprachKonserven['BES']['SBA_LST_GUELTIG']);
		$Form->Erstelle_SelectFeld($Require.'BES_GUELTIG_S',(isset($_REQUEST['txtBES_GUELTIG']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtBES_GUELTIG']:$rsSBB->FeldInhalt('SCHADENBESCHWERDEN'),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV_S',(isset($_REQUEST['txtCHK_AKTIV']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AKTIV']:$rsSBB->FeldInhalt('AUSGESCHIEDEN'),40,true,0);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVFIL'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIVFIL_S',(isset($_REQUEST['txtCHK_AKTIVFIL']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AKTIVFIL']:$rsSBB->FeldInhalt('SBAKTIVFIL'),40,true,1);
		$Form->ZeileEnde();
		
		$Form->ZeileEnde();		
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
	$Form->Erstelle_HiddenFeld('ID',$AWIS_KEY1);
	$Form->Erstelle_HiddenFeld('ART',$rsSBB->FeldInhalt('SBART'));
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht40008&4)==4 and ($AWIS_KEY1 != '-1' and $rsSBB->AnzahlDatensaetze() != 1))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht40008&2)==2 and ($AWIS_KEY1 == '-1' or $rsSBB->AnzahlDatensaetze() == 1))
	{
		if($AWIS_KEY1 == '-1')
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern"style="display:none"', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}
		else 
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SCHAD_SBB',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['BES_SBNAME']) AND $Param['BES_SBNAME']!='')
	{
		$Bedingung .= 'AND UPPER(SBNAME) ' . $DB->LikeOderIst($Param['BES_SBNAME'], awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['BES_SBVORNAME']) AND $Param['BES_SBVORNAME']!='')
	{
		$Bedingung .= 'AND UPPER(SBVORNAME) ' . $DB->LikeOderIst($Param['BES_SBVORNAME'], awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['BES_AKTIV']) AND $Param['BES_AKTIV']!='')
	{
		$Bedingung .= 'AND AUSGESCHIEDEN ' . $DB->LikeOderIst($Param['BES_AKTIV']) . ' ';
	}
	if(isset($Param['BES_SAMMEL']) AND $Param['BES_SAMMEL']==1)
	{
		$Bedingung .= ' and  SBART = 1'; 
	}
	else 
	{
		$Bedingung .= ' and  SBART = 0';
	}
	
	return $Bedingung;
}

?>