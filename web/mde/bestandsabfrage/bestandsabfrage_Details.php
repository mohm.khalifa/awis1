<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BAF','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','vom');
	$TextKonserven[]=array('FIL','FIL_ID');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$RechteStufe = $AWISBenutzer->HatDasRecht(3601);
	if($RechteStufe==0)
	{
	    awisEreignis(3,1000,'BAF',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$DetailAnsicht=false;
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	//********************************************************
	// Daten suchen
	//********************************************************

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);	
	$FilZugriffListe=explode(',',$FilZugriff);
	$FIL_ID = (isset($_POST['txtFIL_ID'])?$_POST['txtFIL_ID']:'');
	
	if($FIL_ID=='')
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Bestandsabfrage'));
		$FIL_ID=$Param['FIL_ID'];
	}
	else
	{
		$Param['FIL_ID']=$FIL_ID;
		$AWISBenutzer->ParameterSchreiben('Formular_Bestandsabfrage',serialize($Param));
	}

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmBestandsabfrage action=./bestandsabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST>');

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',150);
	
	if(count($FilZugriffListe)>1)
	{		
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('FIL_ID',$FIL_ID,150,true,$SQL);
		$AWISCursorPosition='txtFIL_ID';
		
	}
	elseif($FilZugriff!='')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('FIL_ID',$FIL_ID);
		$Form->Erstelle_TextLabel($FIL_ID,150);
		$Param['FIL_ID']=$FIL_ID;
		$AWISBenutzer->ParameterSchreiben('Formular_Bestandsabfrage',serialize($Param));		
	}
	else
	{		
		$Form->Erstelle_TextFeld('FIL_ID',$FIL_ID,4,180,true,'','','','T','L','','',10);
		$AWISCursorPosition='txtFIL_ID';
	}
	$Form->ZeileEnde();
	$Form->Formular_Ende();
	
	$Reg = new awisRegister(3625);
	$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));			

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>