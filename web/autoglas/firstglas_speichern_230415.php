<?php

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('FGK', 'FGK_*');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('FKH', '*');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_Hilfe');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Wort', 'Umbuchung');
try
{

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    if (isset($_POST['txtcheckUmbuchung']) == 'on')
    {
        if ($_POST['txtFUM_KEY'] != $_POST['oldFUM_KEY'])
        {
            $SQL = "Select FUM_VORGANGSNR FROM FGUMBUCHUNG WHERE FUM_VORGANGSNR=" . $DB->FeldInhaltFormat('T', $_POST['txtFGP_VORGANGNR']);

            $rsCheck = $DB->RecordSetOeffnen($SQL);

            if ($rsCheck->AnzahlDatensaetze() == 0)
            {
                $SQL = "INSERT INTO FGUMBUCHUNG (FUM_VORGANGSNR,FUM_BGT_KEY,";
                $SQL .= "FUM_USER,FUM_USERDAT";
                $SQL .= ") VALUES (";
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtFGP_VORGANGNR'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('NO', $_POST['txtFUM_KEY'], false);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                $DB->Ausfuehren($SQL);
            }           
            else
            {
                $SQL = 'UPDATE FGUMBUCHUNG SET ';
                $SQL .= ' FUM_BGT_KEY=' . $DB->FeldInhaltFormat('T', $_POST['txtFUM_KEY'], false);
                $SQL .= ',FUM_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FUM_USERDAT=sysdate';
                $SQL .= ' WHERE FUM_VORGANGSNR=' . $DB->FeldInhaltFormat('T', $_POST['txtFGP_VORGANGNR'], false);

                $DB->Ausfuehren($SQL);
            }
        }
        else
        {
            
        }
    }
    elseif ($_POST['oldcheckUmbuchung'] == 'on')
    {
        $SQL = "Delete from FGUMBUCHUNG WHERE FUM_VORGANGSNR=" . $DB->FeldInhaltFormat('T', $_POST['txtFGP_VORGANGNR']);
        var_dump($_POST);
        $DB->Ausfuehren($SQL);
    }

    $SpeichereKassendaten = false;

    if (isset($_POST['txtFKA_NR']) == -1)
    {
        //$Form->DebugAusgabe(1,$_REQUEST);
        //Pruefe Eingegebene Werte(alle Felder m�ssen angegeben werden
        //-------------------------------------------------------------
        $SQL = "Select * from FKASSENDATEN WHERE";
        $SQL .= " FKA_FILID=" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_FILID'], false);
        $SQL .= " AND FKA_DATUM=" . $DB->FeldInhaltFormat('D', $_POST['txtFKA_DATUM'], false);
        $SQL .= " AND FKA_UHRZEIT=" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_UHRZEIT'], false);
        $SQL .= " AND FKA_BSA=" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_BSA'], false);
        $SQL .= " AND FKA_WANR=" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_WANR'], false);
        $SQL .= " AND FKA_AEMNR=" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_AEMNR'], false);

        $rsDuplikat = $DB->RecordSetOeffnen($SQL);

        if ($rsDuplikat->AnzahlDatensaetze() == 0)
        {
            $DATUMSGRENZE = "17.08.2009";

            $SpeichereKassendaten = true;

            $Felder = $Form->NameInArray($_POST, 'txtFKA_', 1, 1);

            if ($Felder != '')
            {
                $Felder = explode(';', $Felder);
                $TextKonserven[] = array('FKA', 'FKA_%');
                $TXT_Speichern = $Form->LadeTexte($TextKonserven);

                $Fehler = '';
                $Pflichtfelder = array('FKA_KENNUNG', 'FKA_FILID', 'FKA_AEMNR', 'FKA_DATUM', 'FKA_UHRZEIT', 'FKA_BSA', 'FKA_WANR', 'FKA_ATUNR', 'FKA_MENGE', 'FKA_BETRAG', 'FKA_KFZKENNZ');
                foreach ($Pflichtfelder AS $Pflichtfeld)
                {
                    if ($_POST['txt' . $Pflichtfeld] == '') // Name muss angegeben werden
                    {
                        $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['FKA'][$Pflichtfeld] . '<br>';
                    }
                }

                if ($Fehler != '')
                {
                    die('<span class=HinweisText>' . $Fehler . '</span>');
                }
                $FeldListe = '';
                $SQL = '';


                //Datumsabfrage
                //-------------------------------------------------------------
                $EingegebenesDatum = $Form->Format('D', $_POST['txtFKA_DATUM']);


                if (strtotime($EingegebenesDatum) < strtotime($DATUMSGRENZE))
                {

                    $SQL = "INSERT INTO FKASSENDATEN (FKA_KENNUNG,FKA_FILID,";
                    $SQL .= "FKA_DATUM,FKA_UHRZEIT,FKA_BSA,FKA_WANR,FKA_AEMNR,";
                    $SQL .= "FKA_ATUNR,FKA_MENGE,FKA_BETRAG,FKA_KFZKENNZ,FKA_BEMERKUNG,FKA_IMP_KEY,FKA_STATUS,FKA_USER,FKA_USERDAT";
                    $SQL .= ") VALUES (";
                    $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_KENNUNG'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('NO', $_POST['txtFKA_FILID'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('D', $_POST['txtFKA_DATUM'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_UHRZEIT'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_BSA'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_WANR'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_AEMNR'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_ATUNR'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('N2', $_POST['txtFKA_MENGE'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('N2', $_POST['txtFKA_BETRAG'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_KFZKENNZ'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_BEMERKUNG'], false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('NO', 0, false);
                    $SQL .= ',' . $DB->FeldInhaltFormat('NO', 0, True);
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    $DB->Ausfuehren($SQL);
                }
                else
                {
                    die('<span class=HinweisText>Datum ' . $_POST['txtFKA_DATUM'] . ' hat die erlaubte Grenze ueberschritten (Bitte unter 5641 oder 5553 melden)</span>');
                }
            }
        }
        else
        {
            die('<span class=HinweisText>Datensatz ist bereits vorhanden!!!</span>');
        }
    }
    else
    {
        
    }

    if (isset($_POST['txtFGK_KEY']) && $SpeichereKassendaten == false)
    {
        $Fehler = '';
        $AWIS_KEY1 = $_POST['txtFGK_KEY'];
        $Felder = $Form->NameInArray($_POST, 'txtFGK_', 1, 1);


        if ($Felder != '')
        {
            $Felder = explode(';', $Felder);
            $TextKonserven[] = array('FGK', 'FGK_%');
            $TXT_Speichern = $Form->LadeTexte($TextKonserven);
            //echo $TXT_Speichern;
        }

        //Wenn Feld direkt an Zugang uebertragen werden soll!
        //Feld "Sieben wurde gesetzt"
        //Vergleiche WERTE
        $checkStorno = false;
        $checkHackenEntfernen = false;
        $checkZugang = false;

        if ($_POST['txtFeldSpeichern7'] == $_POST['oldFeldSpeichern7'])
        {
            $checkZugang = false;
        }
        else
        {
            $checkZugang = true;
        }


        if ($_POST['txtFeldSpeichern1'] == $_POST['oldFeldSpeichern1'])
        {
            $checkStorno = false;
        }
        else
        {
            //Unterscheidung
            $checkStorno = true;
        }

        if ($_POST['txtFeldSpeichern11'] == $_POST['oldFeldSpeichern11'])
        {
            $checkHackenEntfernen = false;
        }
        else
        {
            $checkHackenEntfernen = true;
        }


        $SQL = "UPDATE FGKOPFDATEN SET ";
        $SQL .= "FGK_NOTIZ='" . $_POST['txtFGK_NOTIZ'] . "' ";
        $SQL .= "WHERE FGK_KEY=" . $AWIS_KEY1;

        $DB->Ausfuehren($SQL);

        if ($checkZugang == true && $checkStorno == false && $checkHackenEntfernen == false)
        {
            $Pflichtfelder = array('FGK_FREIGABEGRUND');
            foreach ($Pflichtfelder AS $Pflichtfeld)
            {
                if ($_POST['txt' . $Pflichtfeld] == '') // Freigabegrund muss bei manueller Freischaltung eingegeben werden!!!
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['FGK'][$Pflichtfeld] . '<br>';
                }
            }

            if ($Fehler != '')
            {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }
            $FeldListe = '';
            $SQL = '';



            if (floatval($_POST['txtFGK_KEY']) == 0)
            {
                
            }
            else
            {
                //Ueberpruefen der Werte die geupdatet werden sollen
                //--------------------------------------------------

                $FehlerListe = array();
                $UpdateFelder = '';

                $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY=' . $DB->FeldInhaltFormat('NO', $_POST['txtFGK_KEY'], false);

                $rsKopfdaten = $DB->RecordSetOeffnen($SQL);

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KENNUNG"), false);
                $SQL .= ',FGK_FILID=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt("FGK_FILID"), false);
                $SQL .= ',FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VORGANGNR"), false);
                $SQL .= ',FGK_KFZBEZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZBEZ"), false);
                $SQL .= ',FGK_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZKENNZ"), false);
                $SQL .= ',FGK_KBANR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KBANR"), false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_FAHRGESTELLNR"), false);
                $SQL .= ',FGK_VERSICHERUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSICHERUNG"), false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSSCHEINNR"), false);
                $SQL .= ',FGK_SB=' . $DB->FeldInhaltFormat('N', $rsKopfdaten->FeldInhalt("FGK_SB"), false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt("FGK_MONTAGEDATUM"), false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $DB->FeldInhaltFormat('DU', $rsKopfdaten->FeldInhalt("FGK_ZEITSTEMPEL"), false);
                $SQL .= ',FGK_STEUERSATZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_STEUERSATZ"), false);
                $SQL .= ',FGK_KUNDENNAME=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KUNDENNAME"), false);
                $SQL .= ',FGK_IMP_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_IMP_KEY'), true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), true);
                $SQL .= ',FGK_FGN_KEY=' . $DB->FeldInhaltFormat('NO', 7, true);
                $SQL .= ',FGK_FCN_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_FCN_KEY'), true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $DB->FeldInhaltFormat('T', $_POST['txtFGK_FREIGABEGRUND'], true);
                $SQL .= ',FGK_DATUMZUGANG=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt('FGK_DATUMZUGANG'), true);
                $SQL .= ',FGK_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

                $DB->Ausfuehren($SQL);

                $SQL = " SELECT *";
                $SQL.= " FROM versvorgangsstatus";
                $SQL.= " WHERE vvs_vorgangnr = " . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);
                $SQL.= " AND vvs_ven_key = 6 ";
                $SQL.= " AND vvs_vcn_key = 100";
                $SQL.= " AND vvs_freigabe = 0";
                $SQL.= " AND vvs_schadenkz = 'E'";

                //echo $SQL;
                $rsPruefe = $DB->RecordSetOeffnen($SQL);


                echo "<br>";
                echo "AnzahLDS:" . $rsPruefe->AnzahlDatensaetze();
                echo "<br>";

                if ($rsPruefe->AnzahlDatensaetze() == 1)
                {

                    $SQL = "UPDATE versvorgangsstatus";
                    $SQL .= " SET vvs_freigabe = 1,";
                    $SQL .= " vvs_freigabegrund = " . $DB->FeldInhaltFormat('T', $_POST['txtFGK_FREIGABEGRUND'], true);
                    $SQL .= " WHERE vvs_vorgangnr = " . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);
                    $SQL .= " AND vvs_ven_key = 6";
                    $SQL .= " AND vvs_vcn_key = 100";
                    $SQL .= " AND vvs_freigabe = 0";
                    $SQL .= " AND vvs_schadenkz = 'E'";

                    echo $SQL;

                    $DB->Ausfuehren($SQL);
                }
            }
        }
        else if ($checkZugang == false && $checkStorno == false && $checkHackenEntfernen == true)
        {
            $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY=' . $DB->FeldInhaltFormat('NO', $_POST['txtFGK_KEY'], false);

            $rsKopfdaten = $DB->RecordSetOeffnen($SQL);

            $SQL = 'UPDATE FGKOPFDATEN SET ';
            $SQL .= ' FGK_KENNUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KENNUNG"), false);
            $SQL .= ',FGK_FILID=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt("FGK_FILID"), false);
            $SQL .= ',FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VORGANGNR"), false);
            $SQL .= ',FGK_KFZBEZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZBEZ"), false);
            $SQL .= ',FGK_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZKENNZ"), false);
            $SQL .= ',FGK_KBANR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KBANR"), false);
            $SQL .= ',FGK_FAHRGESTELLNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_FAHRGESTELLNR"), false);
            $SQL .= ',FGK_VERSICHERUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSICHERUNG"), false);
            $SQL .= ',FGK_VERSSCHEINNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSSCHEINNR"), false);
            $SQL .= ',FGK_SB=' . $DB->FeldInhaltFormat('N', $rsKopfdaten->FeldInhalt("FGK_SB"), false);
            $SQL .= ',FGK_MONTAGEDATUM=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt("FGK_MONTAGEDATUM"), false);
            $SQL .= ',FGK_ZEITSTEMPEL=' . $DB->FeldInhaltFormat('DU', $rsKopfdaten->FeldInhalt("FGK_ZEITSTEMPEL"), false);
            $SQL .= ',FGK_STEUERSATZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_STEUERSATZ"), false);
            $SQL .= ',FGK_KUNDENNAME=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KUNDENNAME"), false);
            $SQL .= ',FGK_IMP_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_IMP_KEY'), true);
            $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), true);
            $SQL .= ',FGK_FGN_KEY=' . $DB->FeldInhaltFormat('NO', null, true);
            $SQL .= ',FGK_FCN_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_FCN_KEY'), true);
            $SQL .= ',FGK_FREIGABEGRUND=' . $DB->FeldInhaltFormat('T', '', true);
            $SQL .= ',FGK_DATUMZUGANG=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt('FGK_DATUMZUGANG'), true);
            $SQL .= ',FGK_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',FGK_USERDAT=sysdate';
            $SQL .= ' WHERE FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

            $DB->Ausfuehren($SQL);
            //Storno deaktivieren
        }
        else if ($checkZugang == false && $checkStorno == true && $checkHackenEntfernen == false)
        {
            //Deaktivieren oder Aktivieren

            if ($_POST['txtFeldSpeichern1'] == 0)
            {

                //Setze Kopfdatensatz auf null zur�ck
                //Setze Aktiv - Kennzeichen auf '0'

                $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY=' . $DB->FeldInhaltFormat('NO', $_POST['txtFGK_KEY'], false);

                $rsKopfdaten = $DB->RecordSetOeffnen($SQL);

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KENNUNG"), false);
                $SQL .= ',FGK_FILID=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt("FGK_FILID"), false);
                $SQL .= ',FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VORGANGNR"), false);
                $SQL .= ',FGK_KFZBEZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZBEZ"), false);
                $SQL .= ',FGK_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZKENNZ"), false);
                $SQL .= ',FGK_KBANR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KBANR"), false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_FAHRGESTELLNR"), false);
                $SQL .= ',FGK_VERSICHERUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSICHERUNG"), false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSSCHEINNR"), false);
                $SQL .= ',FGK_SB=' . $DB->FeldInhaltFormat('N', $rsKopfdaten->FeldInhalt("FGK_SB"), false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt("FGK_MONTAGEDATUM"), false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $DB->FeldInhaltFormat('DU', $rsKopfdaten->FeldInhalt("FGK_ZEITSTEMPEL"), false);
                $SQL .= ',FGK_STEUERSATZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_STEUERSATZ"), false);
                $SQL .= ',FGK_KUNDENNAME=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KUNDENNAME"), false);
                $SQL .= ',FGK_IMP_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_IMP_KEY'), true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), true);
                $SQL .= ',FGK_FGN_KEY=' . $DB->FeldInhaltFormat('NO', null, true);
                $SQL .= ',FGK_FCN_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_FCN_KEY'), true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_FREIGABEGRUND'), true);
                $SQL .= ',FGK_DATUMZUGANG=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt('FGK_DATUMZUGANG'), true);
                $SQL .= ',FGK_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

                $DB->Ausfuehren($SQL);

                //Stornosatz deaktivieren

                $SQL = ' Select FGKOPFDATEN.*,FGSTORNODATEN.* from FGKOPFDATEN INNER JOIN';
                $SQL .= ' FGSTORNODATEN ON FGK_VORGANGNR = FGS_VORGANGNR AND FGK_FILID = FGS_FILIALNR';
                $SQL .= ' WHERE FGK_VORGANGNR =' . $rsKopfdaten->FeldInhalt('FGK_VORGANGNR');

                $rsStornodaten = $DB->RecordSetOeffnen($SQL);

                $SQL = 'UPDATE FGSTORNODATEN SET ';
                $SQL .= ' FGS_KENNUNG =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KENNUNG"), false);
                $SQL .= ',FGS_FILIALNR =' . $DB->FeldInhaltFormat('NO', $rsStornodaten->FeldInhalt("FGS_FILIALNR"), false);
                $SQL .= ',FGS_VORGANGNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VORGANGNR"), false);
                $SQL .= ',FGS_KFZBEZ =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KFZBEZ"), false);
                $SQL .= ',FGS_KFZKENNZ =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KFZKENNZ"), false);
                $SQL .= ',FGS_KBANR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KBANR"), false);
                $SQL .= ',FGS_FAHRGESTELLNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_FAHRGESTELLNR"), false);
                $SQL .= ',FGS_VERSICHERUNG =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VERSICHERUNG"), false);
                $SQL .= ',FGS_VERSSCHEINNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VERSSCHEINNR"), false);
                $SQL .= ',FGS_SB =' . $DB->FeldInhaltFormat('N2', $rsStornodaten->FeldInhalt("FGS_SB"), false);
                $SQL .= ',FGS_MONTAGEDATUM =' . $DB->FeldInhaltFormat('DU', $rsStornodaten->FeldInhalt("FGS_MONTAGEDATUM"), false);
                $SQL .= ',FGS_ZEITSTEMPEL =' . $DB->FeldInhaltFormat('DU', $rsStornodaten->FeldInhalt("FGS_ZEITSTEMPEL"), false);
                $SQL .= ',FGS_STEUERSATZ =' . $DB->FeldInhaltFormat('N2', $rsStornodaten->FeldInhalt("FGS_STEUERSATZ"), false);
                $SQL .= ',FGS_AKTIV =' . $DB->FeldInhaltFormat('NO', 0, false);
                $SQL .= ',FGS_FGK_KEY =' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_KEY'), true);
                $SQL .= ',FGS_IMP_KEY =' . $DB->FeldInhaltFormat('NO', $rsStornodaten->FeldInhalt('FGS_ZEITSTEMPEL_KASSIERT'), true);
                $SQL .= ',FGS_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FGS_USERDAT=sysdate';
                $SQL .= ' WHERE FGS_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

                $DB->Ausfuehren($SQL);
            }
            else
            {
                //Setze Aktiv - Kennzeichen auf 1
                //Falls Kassendaten vorhanden sind setze Status 2

                $SQL = 'Select FGKOPFDATEN.* from FGKOPFDATEN WHERE FGK_KEY=' . $DB->FeldInhaltFormat('NO', $_POST['txtFGK_KEY'], false);
                $rsKopfdaten = $DB->RecordSetOeffnen($SQL);

                $SQL = 'UPDATE FGKOPFDATEN SET ';
                $SQL .= ' FGK_KENNUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KENNUNG"), false);
                $SQL .= ',FGK_FILID=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt("FGK_FILID"), false);
                $SQL .= ',FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VORGANGNR"), false);
                $SQL .= ',FGK_KFZBEZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZBEZ"), false);
                $SQL .= ',FGK_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KFZKENNZ"), false);
                $SQL .= ',FGK_KBANR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KBANR"), false);
                $SQL .= ',FGK_FAHRGESTELLNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_FAHRGESTELLNR"), false);
                $SQL .= ',FGK_VERSICHERUNG=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSICHERUNG"), false);
                $SQL .= ',FGK_VERSSCHEINNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_VERSSCHEINNR"), false);
                $SQL .= ',FGK_SB=' . $DB->FeldInhaltFormat('N', $rsKopfdaten->FeldInhalt("FGK_SB"), false);
                $SQL .= ',FGK_MONTAGEDATUM=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt("FGK_MONTAGEDATUM"), false);
                $SQL .= ',FGK_ZEITSTEMPEL=' . $DB->FeldInhaltFormat('DU', $rsKopfdaten->FeldInhalt("FGK_ZEITSTEMPEL"), false);
                $SQL .= ',FGK_STEUERSATZ=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_STEUERSATZ"), false);
                $SQL .= ',FGK_KUNDENNAME=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt("FGK_KUNDENNAME"), false);
                $SQL .= ',FGK_IMP_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_IMP_KEY'), true);
                $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), true);
                $SQL .= ',FGK_FGN_KEY=' . $DB->FeldInhaltFormat('NO', 1, true);
                $SQL .= ',FGK_FCN_KEY=' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_FCN_KEY'), true);
                $SQL .= ',FGK_FREIGABEGRUND=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_FREIGABEGRUND'), true);
                $SQL .= ',FGK_DATUMZUGANG=' . $DB->FeldInhaltFormat('D', $rsKopfdaten->FeldInhalt('FGK_DATUMZUGANG'), true);
                $SQL .= ',FGK_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FGK_USERDAT=sysdate';
                $SQL .= ' WHERE FGK_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

                $DB->Ausfuehren($SQL);

                //Stornosatz deaktivieren

                $SQL = ' Select FGKOPFDATEN.*,FGSTORNODATEN.* from FGKOPFDATEN INNER JOIN';
                $SQL .= ' FGSTORNODATEN ON FGK_VORGANGNR = FGS_VORGANGNR AND FGK_FILID = FGS_FILIALNR';
                $SQL .= ' WHERE FGK_VORGANGNR =' . $rsKopfdaten->FeldInhalt('FGK_VORGANGNR');

                $rsStornodaten = $DB->RecordSetOeffnen($SQL);

                $SQL = 'UPDATE FGSTORNODATEN SET ';
                $SQL .= ' FGS_KENNUNG =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KENNUNG"), false);
                $SQL .= ',FGS_FILIALNR =' . $DB->FeldInhaltFormat('NO', $rsStornodaten->FeldInhalt("FGS_FILIALNR"), false);
                $SQL .= ',FGS_VORGANGNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VORGANGNR"), false);
                $SQL .= ',FGS_KFZBEZ =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KFZBEZ"), false);
                $SQL .= ',FGS_KFZKENNZ =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KFZKENNZ"), false);
                $SQL .= ',FGS_KBANR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_KBANR"), false);
                $SQL .= ',FGS_FAHRGESTELLNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_FAHRGESTELLNR"), false);
                $SQL .= ',FGS_VERSICHERUNG =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VERSICHERUNG"), false);
                $SQL .= ',FGS_VERSSCHEINNR =' . $DB->FeldInhaltFormat('T', $rsStornodaten->FeldInhalt("FGS_VERSSCHEINNR"), false);
                $SQL .= ',FGS_SB =' . $DB->FeldInhaltFormat('N2', $rsStornodaten->FeldInhalt("FGS_SB"), false);
                $SQL .= ',FGS_MONTAGEDATUM =' . $DB->FeldInhaltFormat('DU', $rsStornodaten->FeldInhalt("FGS_MONTAGEDATUM"), false);
                $SQL .= ',FGS_ZEITSTEMPEL =' . $DB->FeldInhaltFormat('DU', $rsStornodaten->FeldInhalt("FGS_ZEITSTEMPEL"), false);
                $SQL .= ',FGS_STEUERSATZ =' . $DB->FeldInhaltFormat('N2', $rsStornodaten->FeldInhalt("FGS_STEUERSATZ"), false);
                $SQL .= ',FGS_AKTIV =' . $DB->FeldInhaltFormat('NO', 1, false);
                $SQL .= ',FGS_FGK_KEY =' . $DB->FeldInhaltFormat('NO', $rsKopfdaten->FeldInhalt('FGK_KEY'), true);
                $SQL .= ',FGS_IMP_KEY =' . $DB->FeldInhaltFormat('NO', $rsStornodaten->FeldInhalt('FGS_ZEITSTEMPEL_KASSIERT'), true);
                $SQL .= ',FGS_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',FGS_USERDAT=sysdate';
                $SQL .= ' WHERE FGS_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), false);

                $DB->Ausfuehren($SQL);
            }
        }
    }
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>
