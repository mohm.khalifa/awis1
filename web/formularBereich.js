//PrecireFormaularBereich
    
//Wir generieren die PrecireFormularBereich Funktion 
var PrecireFormularBereich = PrecireFormularBereich || {};
PrecireFormularBereich.Accordion = function ( panelSelectorsObj ) { 
 
//Erzeugen die Panels
    this.panels = []; 
    this.panelSelectors = panelSelectorsObj; 
    this.accordionPanels = document.querySelectorAll( this.panelSelectors['heading'] ); 

    for (i = 0; i < this.accordionPanels.length; i++) {
        this.makePanel( this.accordionPanels[i], i );
    }
};

//Wir generieren die Panels Funktion
PrecireFormularBereich.Accordion.prototype = {
    resetPanels: function () {
        this.panels.forEach( function ( v ) {
            v.unselect();
        });
    },
    makePanel: function ( panelElement, index ) {
        var panel = new PrecireFormularBereich.AccordionPanel( panelElement, this, index );
        this.panels.push( panel );
    }
};


PrecireFormularBereich.AccordionPanel = function ( headingEl, panelHolder, index ) {
    
    var self = this;

    this.panelHolder = panelHolder;
    this.index = index;
    this.headingEl = headingEl; 
    this.contentEl = headingEl.nextElementSibling;
    this.isSelected = false;

    this.setupAccessibility();

    this.headingEl.addEventListener( "click", function () {
        
        if (self.headingEl.getAttribute("tabindex") == 1){
        	self.headingEl.setAttribute( 'tabindex', '0' );
            self.unselect(); 
            
        }
        else {
        	self.headingEl.setAttribute( 'tabindex', '1' );
            self.select(); 
        }

    });

    return this;
};

//Erzeugen die Funktion f�r AccordionPanel (Attribute f�r <a>-tag, Select und Unselect Funktionen)
PrecireFormularBereich.AccordionPanel.prototype = {

    setupAccessibility: function(){
        this.headingEl.setAttribute( 'role', 'tab' );
        this.headingEl.setAttribute( 'id', 'accordionHeading_' + this.index );
        this.headingEl.setAttribute( 'aria-controls', 'accordionContent_' + this.index );
        //this.headingEl.setAttribute( 'tabindex', '0' );
        this.contentEl.setAttribute( 'id', 'accordionContent_' + this.index );
        this.contentEl.setAttribute( 'aria-labelledby', 'accordionHeading_' + this.index );
        this.contentEl.setAttribute( 'role', 'tabpanel' );
        //this.contentEl.setAttribute( 'tabindex', '0' );
    },
    select: function () {
        var self = this;
        this.isSelected = true;
        this.headingEl.addClass('active');
        this.contentEl.addClass('active');
        setTimeout(function(){
            self.contentEl.focus();
        }, 500);
        
    },
    unselect: function () {
        this.isSelected = false;
        this.headingEl.removeClass('active');
        this.contentEl.removeClass('active');    

    }
};

PrecireFormularBereich.init = function () {
    this.accordionContainer = new PrecireFormularBereich.Accordion({
        heading:    '.accordion-panel__heading',
        content:    '.accordion-panel__content'
    }); 
    //this.accordionContainer.panels[this.index].select();
};

window.onload = function () {
    PrecireFormularBereich.init();
};


//Beigabe und L�sch Klasse Funktion
HTMLElement.prototype.addClass = function ( className ) {
    if (this.classList){
        this.classList.add( className );
    }else{
        this.className += ' ' + className;
    }
}

HTMLElement.prototype.removeClass = function (className) {
    if (this.classList){
      this.classList.remove(className);
    }else{
      this.className = this.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
}
	