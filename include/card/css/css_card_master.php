<?php

require_once("card/card_db.inc.php");			// DB

//Farben: 

//blau: #3399ff, #1133ff
//gelb: #ffff00
//grau: #eeeee0, #111111, #eeeeee, #666677, #ffffee, #c0c0c0, #808080, #777777, , #f4f4e8
//gr�n: #006a7d, #00ff00


$strTabelleaussenfarbe="#999999";
$intTabellenaussenRahmenbreite="2px";

$grau="#eeeee0";
//$grau="green";		//Auskommentieren

$intAufloesung=benutzerparameter('BildschirmBreite');

global $intSchriftgroesse;

switch($intAufloesung)
{
	case 800:	$intSchriftgroesse=12;			//Angabe in px
				$intTabellenrahmenbreite=1;		//Angabe in px
				$strTabellenrahmenfarbe="red";
					$strTabellenrahmenfarbe=$grau;		//Auskommentieren
				$strRahmenfarbe="yellow";
					$strRahmenfarbe=$grau;				//Auskommentieren
				$intRahmenbreite=1;				//Angabe in px
				$intZelleinrueckung=0;			//Angabe in px
				$strZellhintergrund=$grau;
				break;
				
	case 1024:	$intSchriftgroesse=16;			//Angabe in px
				$intTabellenrahmenbreite=1;		//Angabe in px
				$strTabellenrahmenfarbe="red";
					$strTabellenrahmenfarbe=$grau;		//Auskommentieren
				$strRahmenfarbe="yellow";
					$strRahmenfarbe=$grau;				//Auskommentieren
				$intRahmenbreite=1;				//Angabe in px
				$intZelleinrueckung=0;			//Angabe in px
				$strZellhintergrund=$grau;
				break;
				
	default:	;
}

//$intSchriftgroesse wird in card_allgparam.inc.php festgelegt

switch($intSchriftgroesse)
{
	case 12:	$intElementhoehe=20;
				$intZellhoehe=22;
				break;
	case 16:	$intElementhoehe=24;
				$intZellhoehe=26;
				break;
	default:	$intElementhoehe=23;
				$intZellhoehe=25;
}


?>

<style type="text/css">
<!--
	body,textarea,input,button,select		{	font-family:Verdana,Helvetica,Arial;
												font-size:<?php echo $intSchriftgroesse ?>px; }
	
	body									{	margin:0px;
												background-color:<?php echo $grau;?>; }

	table 									{	font-size:<?php echo $intSchriftgroesse ?>px;
												border-style:solid;
												border-color:<?php echo $strTabellenrahmenfarbe; ?>;
												border-width:<?php echo $intTabellenrahmenbreite; ?>px; }
												
	tr										{ 	height:<?php echo $intZellhoehe; ?>px; }
												
	td 										{ 	padding-left:<?php echo $intZelleinrueckung; ?>;
												background-color:<?php echo $strZellhintergrund; ?>;
												border:solid;
												border-color:<?php echo $strRahmenfarbe; ?>;
												border-width:<?php echo $intRahmenbreite; ?>px; }
																					
	/* Es muss jeweils ein Attribut class im td-Tag angegeben werden z.B. <td class="text"> */
	
	td.text,td.textarea			 			{ 	background-color:<?php echo $strRahmenfarbe; ?>;
												border-color:<?php echo $strRahmenfarbe; ?>;
												border-left-width:<?php echo $intRahmenbreite; ?>px;
												border-right-width:<?php echo $intRahmenbreite; ?>px;
												border-top-width:<?php echo $intRahmenbreite-1; ?>px;
												border-bottom-width:<?php echo $intRahmenbreite-1; ?>px;
												vertical-align:middle; }
	
	td.select,td.button,td.submit,td.img	{	border-color:<?php echo $strRahmenfarbe; ?>;
												vertical-align:middle; }
	
	input,button							{	height:<?php echo $intElementhoehe; ?>; }
		
	.quadrat								{	height:<?php echo $intElementhoehe; ?>;
												width:<?php echo $intElementhoehe; ?>;
												/*cursor:hand;*/ }
												
	#tabelleaussen							{	border-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-width:<?php echo $intTabellenaussenRahmenbreite;?>;	}
												
												
	td.unten								{	border-bottom-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-bottom-width:1px; }
						
	td.oben									{	border-top-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-top-width:1px; }
						
	td.obenunten							{	border-top-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-bottom-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-top-width:1px;
												border-bottom-width:1px; }					
						


	td.links								{	border-left-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-left-width:1px;	}
						
	td.rechts								{	border-right-color:<?php echo $strTabelleaussenfarbe; ?>;
												border-right-width:1px;	}
}

	
	
	<?php
	//css-Dateien f�r entsprechende Seite einf�gen (z.B. css_cardhaupt.php f�r cardhaupt.php
	
	$strDatei = "card/css/css_".substr(strrchr($PHP_SELF,'/'),1);
		
	if(file_exists(realpath("/daten/include/".$strDatei)))
	{
		require_once($strDatei);
	}
	
	?>
-->
</style>
