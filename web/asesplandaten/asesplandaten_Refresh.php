<?php
global $APD;
try
{

    if(isset($_POST['cmdWeiter_x'])){
        $APD->Param['SPEICHERN'] = isset($_POST['txtAuswahlSpeichern'])?'on':'off';
        $APD->Param['APD_DATUM_VON'] = $_POST['txtAPD_DATUM_VON'];
        $APD->Param['APD_DATUM_BIS'] = $_POST['txtAPD_DATUM_BIS'];
        $APD->Param['APD_CLIENT_NO'] = $_POST['txtAPD_CLIENT_NO'];
        $APD->Param['APD_PERS_NR'] = isset($_POST['txtAPD_PERS_NR'])?$_POST['txtAPD_PERS_NR']:'';
        $APD->Param['APD_FIL_ID'] = isset($_POST['txtAPD_FIL_ID'])?$_POST['txtAPD_FIL_ID']:[];

        include("asesplandaten_speichern.php");
    }

    $Speichern = isset($APD->Param['SPEICHERN']) && $APD->Param['SPEICHERN'] == 'on'?true:false;

    $APD->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./asesplandaten_Main.php?cmdAktion=Plandatenrefresh>";

	$APD->Form->Formular_Start();

    $APD->Form->ZeileStart();
    $APD->Form->Hinweistext($APD->AWISSprachKonserven['APD']['APD_INFOTEXT'], 6, 'width: auto');
    $APD->Form->ZeileEnde();

    $APD->Form->ZeileStart();
    $APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['Wort']['Land'].':',190);
    $SQL = 'select LAN_ASES_CLIENTNO, LAN_LAND from Laender where LAN_ASES_CLIENTNO is not null';
    $APD->Form->Erstelle_SelectFeld('APD_CLIENT_NO',($Speichern?$APD->Param['APD_CLIENT_NO']:''),200,true,$SQL);
    $APD->Form->ZeileEnde();

    $APD->Form->AuswahlBox('APD_CLIENT','Filbox','','APD_Filialen','txtAPD_CLIENT_NO',10,10,'','T',true,'','','display: none',3,'','','',0);
    $APD->Form->AuswahlBoxHuelle('Filbox');


    // Auswahl kann gespeichert werden
    $APD->Form->ZeileStart();
    $APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $APD->Form->Erstelle_Checkbox('AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $APD->Form->ZeileEnde();

	$APD->Form->Formular_Ende();

	$APD->Form->SchaltflaechenStart();
	$APD->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$APD->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$APD->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $APD->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	$APD->Form->SchaltflaechenEnde();

	$APD->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($APD->Form instanceof awisFormular)
	{
		$APD->Form->DebugAusgabe(1, $ex->getSQL());
		$APD->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$APD->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($APD->Form instanceof awisFormular)
	{
		$APD->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>