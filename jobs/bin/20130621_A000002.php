<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

/*
 * Artikel neu einspielen
*/
try 
{
	//$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130621_Staging_Artikelneueinspielung.csv';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20130621_Produktiv_Artikelneueinspielung.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$Zeile = "AST_ARTNR;ART;VORGANG;SQL;\n";
	fputs($fp,$Zeile);

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// alle DS aus temp. Tabelle laden
	$SQL = 'SELECT * ';
	$SQL .= 'FROM XXX_ARTIKELNEUEINSPIELUNG ';
	$SQL .= 'WHERE erfolgreich IS NULL ';
	$SQL .= 'OR erfolgreich = 0';
	
	$rsQuellDat = $DB->RecordSetOeffnen($SQL);

	$DB->TransaktionBegin();
	
	while (!$rsQuellDat->EOF())
	{
		$SQL = 'SELECT ast_key, ast_atunr';
		$SQL .= ' FROM artikelstamm';
		$SQL .= ' WHERE ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
		//echo $SQL.'<br>';		

		$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
		$Zeile .= ';SELECT';
		$Zeile .= ';Artikelstamm laden';
		$Zeile .= ';' .$SQL ."\n";
		fputs($fp,$Zeile);
		
		$rsArtikelstamm = $DB->RecordSetOeffnen($SQL);
		
		if($rsArtikelstamm->EOF())
		{
			$SQL = 'SELECT WUG_KEY FROM warenuntergruppen';
			$SQL .= ' WHERE WUG_WGR_ID = '.$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('WGR_ID'),false);
			$SQL .= ' AND WUG_ID = '.$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('WUG_ID'),false);
				
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';WUG KEY laden';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);

			$rsWUG=$DB->RecordSetOeffnen($SQL);
				
			//neue ATUNR anlegen
			$SQL = 'INSERT INTO Artikelstamm (';
			$SQL .= 'AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNG';
			$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
			$SQL .= ') VALUES (';
			$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
			$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('AST_BEZEICHNUNG'),false) .',';
			$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_KENNUNGVORSCHLAG'),false) .',';
			$SQL .= $DB->FeldInhaltFormat('Z',$rsWUG->FeldInhalt('WUG_KEY'),false) .',';
			$SQL .= '4,';
			$SQL .= '\'AWIS\',';
			$SQL .= 'SYSDATE)';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';INSERT';
			$Zeile .= ';Artikelstamm anlegen';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$DB->Ausfuehren($SQL,'',true);			

			$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Artikelstamm angelegt AST_KEY=' .$AWIS_KEY1;
			$Zeile .= ";\n";
			fputs($fp,$Zeile);
			
			// Feld Verpackungsmenge AIT_ID = 60 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos ';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 60 ';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsASI=$DB->RecordSetOeffnen($SQL);
			
			if($rsASI->EOF())
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ') values (';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '60,';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_60'),false) .',';
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';					
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INSERT';
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos';
				$SQL .= ' SET asi_wert = ' .$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_60'),false) .',';
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 60';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';UPDATE';
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			// Feld Einkaufsmeldung AIT_ID = 100 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos ';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 100 ';
			//echo $SQL.'<br>';					
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);

			$rsASI=$DB->RecordSetOeffnen($SQL);
				
			if($rsASI->EOF())
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ") values (";
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '100,';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_100'),false) .',';
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';					
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INSERT';
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos';
				$SQL .= ' SET asi_wert = ' .$DB->FeldInhaltFormat('T',$rsQuellDat->FeldInhalt('ASI_WERT_100'),false);
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 100';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';UPDATE';
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			// Feld Einkaufsmeldung durch AIT_ID = 101 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr';
			$SQL .= ' FROM artikelstamminfos';
			$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$SQL .= ' AND asi_ait_id = 101';
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Pr�fung ARTIKELSTAMMINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsASI=$DB->RecordSetOeffnen($SQL);
			
			if($rsASI->EOF())
			{
				$SQL = 'INSERT INTO artikelstamminfos (';
				$SQL .= 'asi_ast_atunr,';
				$SQL .= 'asi_ait_id,';
				$SQL .= 'asi_wert,';
				$SQL .= 'asi_readonly,';
				$SQL .= 'asi_imq_id,';
				$SQL .= 'asi_user,';
				$SQL .= 'asi_userdat';
				$SQL .= ') values (';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '101,';
				$SQL .= "'1',";
				$SQL .= '1,';
				$SQL .= '4,';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE)';
				//echo $SQL.'<br>';

				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INSERT';
				$Zeile .= ';Insert ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);					
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos ';
				$SQL .= " SET asi_wert = '1'";
				$SQL .= ' WHERE asi_ast_atunr = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$SQL .= ' AND asi_ait_id = 101';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';UPDATE';
				$Zeile .= ';Update ARTIKELSTAMMINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			
			//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
			$SQL = 'SELECT * ';
			$SQL .= ' FROM LIEFERANTENARTIKEL';
			$SQL .= ' WHERE (LAR_SUCH_LARTNR) ' .$DB->LIKEoderIST($rsQuellDat->Feldinhalt('LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
			$SQL .= ' AND (TO_NUMBER(LAR_LIE_NR)) = ' .$DB->FeldInhaltFormat('Z',$rsQuellDat->Feldinhalt('LAR_LIE_NR'));
			//echo $SQL.'<br>';
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Pr�fung LIEFERANTENARTIKEL';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsLIEFERANTENARTIKEL = $DB->RecordSetOeffnen($SQL);
			
			if ($rsLIEFERANTENARTIKEL->EOF())
			{
				//neue LiefArtNr anlegen
				$SQL = 'INSERT INTO Lieferantenartikel (';
				$SQL .= 'LAR_LARTNR,';
				$SQL .= 'LAR_LIE_NR,';
				$SQL .= 'LAR_BEKANNTWW,';
				$SQL .= 'LAR_IMQ_ID,';
				$SQL .= 'LAR_SUCH_LARTNR,';
				$SQL .= 'LAR_USER,';
				$SQL .= 'LAR_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false) .',';
				$SQL .= $DB->FeldInhaltFormat('T',$rsQuellDat->Feldinhalt('LAR_LIE_NR'),false) .',';
				$SQL .= '0,';
				$SQL .= '4,';		// Importquelle
				$SQL .= 'asciiwort(' . $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false).'),';
				$SQL .= '\'schmidberger\',';
				$SQL .= 'SYSDATE';
				$SQL .= ')';
				//echo $SQL.'<br>';

				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INSERT';
				$Zeile .= ';Insert Lieferantenartikel';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
				
				$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 
			{
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';UPDATE';
				$Zeile .= ';Lieferantenartikel bereits vorhanden';
				$Zeile .= ";\n";
				fputs($fp,$Zeile);

				$AWIS_KEY2=$rsLIEFERANTENARTIKEL->FeldInhalt('LAR_KEY');
			}
			
			//Pr�fen, ob es bereits einen Teileinfo Satz gibt
			
			$SQL = 'SELECT tei_key';
			$SQL .= ' FROM teileinfos ';
			$SQL .= ' WHERE tei_ity_id1 = \'AST\'';
			$SQL .= ' AND tei_key1 = ' .$AWIS_KEY1;
			$SQL .= ' AND tei_ity_id2 = \'LAR\'';
			$SQL .= ' AND tei_key2 = ' .$AWIS_KEY2;
			//echo $SQL.'<br>';
			
			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';SELECT';
			$Zeile .= ';Pr�fung TEILEINFOS';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			
			$rsTEILEINFOS = $DB->RecordSetOeffnen($SQL);
			
			if ($rsTEILEINFOS->EOF())
			{
				$SQL = 'INSERT INTO TeileInfos (';
				$SQL .= 'TEI_ITY_ID1,';
				$SQL .= 'TEI_KEY1,';
				$SQL .= 'TEI_SUCH1,';
				$SQL .= 'TEI_WERT1,';
				$SQL .= 'TEI_ITY_ID2,';
				$SQL .= 'TEI_KEY2,';
				$SQL .= 'TEI_SUCH2,';
				$SQL .= 'TEI_WERT2,';
				$SQL .= 'TEI_USER,';
				$SQL .= 'TEI_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= '\'AST\',';
				$SQL .= $AWIS_KEY1 .',';
				$SQL .= 'suchwort(\''.$rsQuellDat->FeldInhalt('AST_ATUNR').'\'),';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false) .',';
				$SQL .= '\'LAR\',';
				$SQL .= $AWIS_KEY2 .',';
				$SQL .= 'asciiwort(' . $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false).'),';
				$SQL .= $DB->FeldInhaltFormat('TU',$rsQuellDat->Feldinhalt('LAR_LARTNR'),false) .',';
				$SQL .= '\'schmidberger\',';			
				$SQL .= 'SYSDATE';
				$SQL .= ')';
				//echo $SQL.'<br>';
				
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INSERT';
				$Zeile .= ';Insert TEILEINFOS';
				$Zeile .= ';' .$SQL ."\n";
				fputs($fp,$Zeile);
				
				$DB->Ausfuehren($SQL,'',true);
			}
			else
			{
				$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
				$Zeile .= ';INFO';
				$Zeile .= ';TEILEINFOS bereits vorhanden';
				$Zeile .= ";\n";
				fputs($fp,$Zeile);
			}	
						
			//Datensatz als erfolgreich markieren
			$SQL = 'UPDATE XXX_ARTIKELNEUEINSPIELUNG';
			$SQL .= ' SET erfolgreich = 1 ';
			$SQL .= ' WHERE AST_ATUNR = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';UPDATE';
			$Zeile .= ';Datensatz als erfolgreich markieren';
			$Zeile .= ';' .$SQL ."\n";
			fputs($fp,$Zeile);
			fputs($fp,"\n");
			
			$DB->Ausfuehren($SQL,'',true);
		}
		else 
		{
			// Artikel bereits vorhanden
			$SQL = 'UPDATE XXX_ARTIKELNEUEINSPIELUNG';
			$SQL .= ' SET erfolgreich = 1 ';
			$SQL .= ' WHERE AST_ATUNR = ' .$DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			//echo $SQL.'<br>';

			$Zeile = $DB->FeldInhaltFormat('TU',$rsQuellDat->FeldInhalt('AST_ATUNR'),false);
			$Zeile .= ';INFO';
			$Zeile .= ';Artikelstamm bereits vorhanden';
			$Zeile .= ";\n";
			fputs($fp,$Zeile);
			fputs($fp,"\n");
			
			$DB->Ausfuehren($SQL,'',true);
		}
		
		$rsQuellDat->DSWeiter();

	}// end of while

	$DB->TransaktionCommit();
	//$DB->TransaktionRollback(); //TEST

	fclose($fp);
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}



?>