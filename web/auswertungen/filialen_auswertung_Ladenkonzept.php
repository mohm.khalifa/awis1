<?php
// Variablen
global $_POST;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}


print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=Ladenkonzept>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	$SQL = 'SELECT Filialen.FIL_ID, Filialen.FIL_BEZ, Filialen.FIL_LAGERKZ, (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=36) AS Ladenkonzept';
	$SQL .= ' ,  (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=34 AND FIF_IMQ_ID=32) AS Eroeffnung, LAN_Code';
	$SQL .= ' FROM Filialen INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' ORDER BY Filialen.FIL_ID';

	$rsAusstattung = awisOpenRecordset($con, $SQL);
	$rsAusstattungZeilen = $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );
	
	fputs($fd, "Filiale;Bezeichnung;Land;Eröffnungsdatum;Ladenkonzept 2011 ab;Lager\n");
	for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
	{
		$LadenKonzeptDatum=$rsAusstattung['LADENKONZEPT'][$DSNr];
		if($LadenKonzeptDatum=='')
		{
			if($rsAusstattung['EROEFFNUNG'][$DSNr])
			{
				$Datum = explode('.',$rsAusstattung['EROEFFNUNG'][$DSNr]);
				if(mktime(0,0,0,$Datum[1],$Datum[0],$Datum[2]) > mktime(0,0,0,1,1,2006))
				{
					$LadenKonzeptDatum = date('d.m.Y',mktime(0,0,0,$Datum[1],$Datum[0],$Datum[2]));
				}
			}
		}
		
		fputs($fd, $rsAusstattung['FIL_ID'][$DSNr] . ';' . $rsAusstattung['FIL_BEZ'][$DSNr] . ';' . $rsAusstattung['LAN_CODE'][$DSNr] . ';' .  $rsAusstattung['EROEFFNUNG'][$DSNr] . ';' . $LadenKonzeptDatum . ';' . $rsAusstattung['FIL_LAGERKZ'][$DSNr] . "\n");
	}
	fclose($fd);
	
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei öffnen</a>';
}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Alle Filialen mit Ladenkonzept 2011.</li>';
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

