<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVA','%');
	$TextKonserven[]=array('SVA','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('CAD','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','SVA_%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19001 = $AWISBenutzer->HatDasRecht(19001);
	if($Recht19001==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ServiceArtikel'));
	
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./serviceleistungen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./serviceleistungen_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['SVA_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['SVA_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}


	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = ' ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY SVA_AST_ATUNR DESC';
			$Param['ORDER']='SVA_AST_ATUNR DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************

	$Bedingung = '';
	
	if($AWIS_KEY1>0)
	{
		$Bedingung .= ' AND SVA_KEY = '.$AWIS_KEY1;
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$Bedingung .= ' AND SVA_KEY = -1';
	}
	
	$SQL = 'SELECT SVCARTIKEL.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM SVCARTIKEL';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
//	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikel',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;


	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsSVA = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikel',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmServiceArtikel action=./serviceleistungen_Main.php?cmdAktion=Artikel method=POST enctype="multipart/form-data">';

	if($rsSVA->AnzahlDatensaetze()>1 AND !isset($_GET['SVA_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVA_AST_ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVA_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVA']['SVA_AST_ATUNR'],130,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVA_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVA']['SVA_BEZEICHNUNG'],250,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsSVA->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './serviceleistungen_Main.php?cmdAktion=Artikel&SVA_KEY=0'.$rsSVA->FeldInhalt('SVA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('SVA_AST_ATUNR',$rsSVA->FeldInhalt('SVA_AST_ATUNR'),0,130,false,($DS%2),'',$Link,'T','L',$rsSVA->FeldInhalt('SVA_LIEFERSCHEINNR'));
			$Form->Erstelle_ListenFeld('SVA_BEZEICHNUNG',$rsSVA->FeldInhalt('SVA_BEZEICHNUNG'),0,250,false,($DS%2),'','','T','L');

			$Form->ZeileEnde();

			$rsSVA->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsSVA->FeldInhalt('SVA_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikel',serialize($Param));
        $EditModus = ($Recht19001&6);

		$Form->Erstelle_HiddenFeld('SVA_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Artikel&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVA->FeldInhalt('SVA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVA->FeldInhalt('SVA_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht19001&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht19001&6);
		}

		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_BEZEICHNUNG'].':',190);
		$Form->Erstelle_TextFeld('!SVA_BEZEICHNUNG',$rsSVA->FeldInhalt('SVA_BEZEICHNUNG'),40,200,$EditRecht,'','','','T');
		$AWISCursorPosition='txtSVA_BEZEICHNUNG';
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_AST_ATUNR'].':',190);
		$Form->Erstelle_TextFeld('!SVA_AST_ATUNR',$rsSVA->FeldInhalt('SVA_AST_ATUNR'),10,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_SYSTEMKENNUNG'].':',190);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVA_SYSTEMKENNUNG']);
		$Form->Erstelle_SelectFeld('!SVA_SYSTEMKENNUNG',$rsSVA->FeldInhalt('SVA_SYSTEMKENNUNG'),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_DATENART'].':',190);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVA_DATENART']);
		$Form->Erstelle_SelectFeld('!SVA_DATENART',$rsSVA->FeldInhalt('SVA_DATENART'),"300:290",$EditModus,'',$OptionBitteWaehlen,'','','',$ListenDaten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_STATUS'].':',190);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVA_STATUS']);
		$Form->Erstelle_SelectFeld('!SVA_STATUS',$rsSVA->FeldInhalt('SVA_STATUS'),"300:290",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVA']['SVA_BEMERKUNG'].':',190);
		$Form->Erstelle_TextArea('SVA_BEMERKUNG',$rsSVA->FeldInhalt('SVA_BEMERKUNG'),800,99,5,$EditModus);
		$Form->ZeileEnde();
				
		
		$Form->Formular_Ende();

		if($AWIS_KEY1>0)
		{
			$Reg = new awisRegister(19040);
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht19001&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht19001&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	if(($Recht19001&8)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}
		
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
	$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikel',serialize($Param));
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>