<?php

require_once 'awisBerichte.inc';
require_once 'awisBarcode.inc';
require_once 'awisDatenbank.inc';

require_once("register.inc.php");
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

class ber_Grosskundenkarten_BAR_UNBAR
    extends awisBerichte
{

    var $angle = 0;

    private $_TBL_GROSSKUNDENKARTEN = "";
    private $_TBL_GROSSKUNDEST = "";

    private $_AusgabeArt;

    private $_AWISWerkzeuge;

    public $Speicherort = '';

    public function init(array $Parameter)
    {
        $this->_AWISWerkzeuge = new awisWerkzeuge();

        if($this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK or $this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING){
            $this->_TBL_GROSSKUNDENKARTEN = " TESTGROSSKUNDENKARTEN ";
            $this->_TBL_GROSSKUNDEST = " TESTGROSSKUNDEST ";

            $this->_AusgabeArt = 1; //Anzeige im Browser -> nur zum Testen
        } else {
            $this->_TBL_GROSSKUNDENKARTEN = " EXPERIAN_KOM.EXPERIAN_GROSSKUNDENKARTEN@COM_DE.ATU.DE ";
            $this->_TBL_GROSSKUNDEST = " EXPGW.ATU_GROSSKUNDEST@COM_DE.ATU.DE ";

            $this->_AusgabeArt = 1; //Download der Datei
        }

        $EAN = '';
        if(isset($_REQUEST["txtKUNDEN"])){ //Dirket mit nem Request gekommen?
            if (is_array($_REQUEST["txtKUNDEN"])) {
                $KdNr = implode(',', $_REQUEST["txtKUNDEN"]);
            } else {
                $KdNr = $_REQUEST["txtKUNDEN"];
            }
        }elseif(isset($Parameter['KDNR'])){ //Oder Klasse aufgerufen
            $KdNr = $Parameter['KDNR'];
            $EAN = $Parameter['EAN'];
        }else{
            throw new Exception('Kann keine KDNR ermitteln', '201908261429');
        }

        //Daten laden
        $SQL = 'SELECT DISTINCT KND.EAN_NR, KND.KUNDEN_NR, KND.KARTEN_NR, KND.NAME1, KND.NAME2, KND.NAME3, KND.GUELTIGKEIT, ERZ_KEY, KND.EAN_NR, LAND.GKB_STAAT, LAND.ZAHL_ART FROM '.$this->_TBL_GROSSKUNDENKARTEN.' KND
                LEFT JOIN EXPRABATTZUORDNUNGEN ON ERZ_KUNDEN_NR = KND.KUNDEN_NR 
                LEFT JOIN '.$this->_TBL_GROSSKUNDEST.' LAND ON KND.KUNDEN_NR = LAND.KUNDEN_NR 
                WHERE KND.KUNDEN_NR IN (' . $KdNr . ') AND KND.GEDRUCKT = 1';
        if($EAN){
            $SQL .= ' AND EAN_NR = ' . $this->_DB->WertSetzen('KND','T',$EAN);
        }


        $this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KND'));

        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('ERM', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

        $this->_BerichtsVorlage1 = '/daten/web/dokumente/vorlagen/' .'Grosskunden_1.png';
        $this->_BerichtsVorlage2 = '/daten/web/dokumente/vorlagen/' .'Grosskundenausweis_HG_Seite2.png';
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        $this->SetTitle('Grosskundenkarten');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $Barcode = new awisBarcode($this);

        $this->AddFont('Arial', 'B', 'ariblk.php'); //Arial (Bold) mit Arial Black ersetzen (aufpassen: Zeichenabstand breiter als bei Arial)

        while (!$this->_rec->EOF()) {
            if($this->_rec->FeldInhalt('ZAHL_ART') == 'G'){ // G = Barzahler
                $Bar = true;
            } else { // K = Unbar
                $Bar = false;
            }

            $this->NeueSeite();
            $this->Image($this->_BerichtsVorlage1,0,0,-400,-400);
            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetTextColor(255, 255, 255);

            //Überschrift
            $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->_Zeile);
            $this->SetFont($this->_Parameter['Schriftart'], 'BI', $this->_Parameter['TextGroesseUeberschrift']);
            $this->Cell($this->_Parameter['FeldBreiteUeberschrift'], 0, '', 0, 0, 'L');

            //Daten
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseDaten']);
            $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandUeberschrift']);
            $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME1'), 0, 0, 'L');
            $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
            $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME2'), 0, 0, 'L');
            $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
            $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME3'), 0, 0, 'L');

            //Ecke unten rechts: Kundennummer + Gültigkeit
            $this->SetXY($this->_Parameter['AbstandKundennummer'], $this->_Parameter['ZeilenAbstandKundennummer']);
            $this->Cell($this->_Parameter['FeldBreiteKdNr'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KD_NR'], 0, 0, 'R');
            $this->Cell($this->_Parameter['FeldBreiteKdNr'], 0, $this->_rec->FeldInhalt('KUNDEN_NR'), 0, 0, 'L');

            $this->SetX($this->_Parameter['AbstandGueltigBis']);
            $gueltigkeit = substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 0, 2) . '/' . substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 2, 2);
            $this->Cell($this->_Parameter['FeldBreiteGueltigBis'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_GUELTIG_BIS'], 0, 0, 'R');
            $this->Cell($this->_Parameter['FeldBreiteGueltigBis'], 0, $gueltigkeit, 0, 0, 'L');

            //Rückseite
            $this->NeueSeite();
            $this->_Zeile = $this->_Parameter['SeiteObenRueckseite'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->Image($this->_BerichtsVorlage2,0,0,-300,-300);

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseInfos']);

            if($Bar) {  //Bei Bar: Alles zum Rabatt anzeigen
                $this->_Zeile += $this->_Parameter['RabattTextX'];
                $this->SetXY($this->_Zeile,$this->_Spalte);
                $Text =  $this->_AWISSprachkonserven['ERM']['ERM_PDF_RABATT_1'] . ' '. $this->_AWISSprachkonserven['ERM']['ERM_PDF_RABATT_2'];
                $this->MultiCell($this->_Parameter['RabattTextW'], $this->_Parameter['RabattTextH'], $Text, 0, 'C');
                $this->SetXY($this->_Zeile+$this->_Parameter['AbstandKartennummerBAR'],$this->_Spalte+$this->_Parameter['ZeilenAbstandKartennummer']);

                $this->SetX($this->_Parameter['LinkerRand']+$this->_Parameter['AbstandKartennummerBAR']);
                $this->Cell($this->_Parameter['FeldBreiteKartennummer'], 0, $this->_AWISSprachkonserven['ERM']['ERM_KARTEN_NR'], 0, 0, 'R');
            } else {    //Bei Unbar: Kein Rabattstext anzeigen -> Karten-Nummer in der Mitte
                $this->SetXY($this->_Zeile+$this->_Parameter['AbstandKartennummerUNBAR'],$this->_Spalte+$this->_Parameter['ZeilenAbstandKartennummer']);
                $this->Cell($this->_Parameter['FeldBreiteKartennummer'], 0, $this->_AWISSprachkonserven['ERM']['ERM_KARTEN_NR'], 0, 0, 'R');
            }

            $this->SetFillColor(255,255,255);
            $this->SetXY($this->_Parameter['LinkerRand'],$this->_Parameter['EAN_BereichY']);
            $this->Cell($this->_Parameter['EAN_BereichW'],$this->_Parameter['EAN_BereichH'],'',0,0,0,true);
            //Barcode hinschreiben
            $this->SetFillColor(0, 0, 0);
            $this->SetTextColor(0,0,0);


            $AbstandBarcodeZuEANBereich = ($this->_Parameter['EAN_BereichH']-$this->_Parameter['BarcodeKarteH'])/2;

            if($Bar){
                $XBarcode = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBarcodeBAR1'];
            }else{
                $XBarcode = ($this->_Parameter['EAN_BereichW'])-($this->_Parameter['BarcodeBARW']*2);
            }
            $this->SetXY($XBarcode, $this->_Parameter['EAN_BereichY']+$AbstandBarcodeZuEANBereich);
            $XVorher = $this->GetX();
            $Barcode->EAN13($this->x, $this->y, substr($this->_rec->FeldInhalt('EAN_NR'),0,12), $this->_Parameter['BarcodeKarteH'], $this->_Parameter['BarcodeKarteW'],false);
            $this->SetXY($XVorher,$this->GetY()+$this->_Parameter['BarcodeKarteH']+$this->_Parameter['AbstandBarcodeEAN']);

            $this->Cell($this->_Parameter['EAN_NrW'],$this->_Parameter['EAN_NrH'],$this->_rec->FeldInhalt('EAN_NR'),0,0,'C');


            if($Bar){
                $this->SetXY($this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBarcodeBAR2'], $this->_Parameter['EAN_BereichY']+$AbstandBarcodeZuEANBereich);
                $EAN = $this->_AWISSprachkonserven['ERM']['ERM_PDF_RABATT_EAN'];
                $Barcode->EAN13($this->x, $this->y, substr($EAN,0,-1), $this->_Parameter['BarcodeKarteH'], $this->_Parameter['BarcodeKarteW'],false);
                $this->SetXY($XBarcode+$AbstandBarcodeZuEANBereich+($this->_Parameter['EAN_BereichW']/2),$this->GetY()+$this->_Parameter['BarcodeKarteH']+$this->_Parameter['AbstandBarcodeEAN']);
                $this->Cell($this->_Parameter['EAN_NrW'],$this->_Parameter['EAN_NrH'],$EAN,0,0,'C');
            }

            $this->SetXY($this->_Parameter['LinkerRand']-1,$this->_Parameter['DisclaimerY']);
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseDisclaimer']);
            $this->SetTextColor(255,255,255);
            $this->MultiCell($this->_Parameter['DisclaimerW'],$this->_Parameter['DisclaimerH'],$this->_AWISSprachkonserven['ERM']['ERM_PDF_DISCLAIMER'],0,'L');

            $this->SetY($this->_Parameter['DisclaimerY']);
            $this->SetX($this->_Parameter['LinkerRand']+$this->_Parameter['DisclaimerW']+$this->_Parameter['AbstandDisclaimerLogo']);
            if($this->_rec->FeldInhalt('GKB_STAAT') == 4 ) { //Deutschland -> GKB_STAAT = 4
                $this->Image(str_replace('#LAND#','de',$this->_AWISSprachkonserven['ERM']['ERM_PDF_LOGO_PFAD']),null,null,$this->_Parameter['LogoW'],$this->_Parameter['LogoH']);
            } else { //Österreich -> GKB_STAAT = 38 + Alles andere, da bei fehlendem Wert wir auf Nummer sicher gehen
                $this->Image(str_replace('#LAND#','at',$this->_AWISSprachkonserven['ERM']['ERM_PDF_LOGO_PFAD']),null,null,$this->_Parameter['LogoW'],$this->_Parameter['LogoH']);
            }
            $this->setY($this->GetY()+2);
            $this->SetX($this->_Parameter['LinkerRand']+$this->_Parameter['DisclaimerW']+$this->_Parameter['AbstandDisclaimerLogo']-1);
            if($this->_rec->FeldInhalt('GKB_STAAT') == 4 ) { //Deutschland -> GKB_STAAT = 4
                $this->MultiCell($this->_Parameter['AnschriftW'],$this->_Parameter['AnschriftH'],$this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT'],0,'L');
            } else { //Österreich -> GKB_STAAT = 38 + Alles andere, da bei fehlendem Wert wir auf Nummer sicher gehen
                $this->MultiCell($this->_Parameter['AnschriftW'],$this->_Parameter['AnschriftH'],$this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT_AT'],0,'L');
            }

            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************

        if($this->Speicherort != '') {
            $this->Output($this->Speicherort, 'F');
        } else {

            switch ($this->_AusgabeArt) {
                case 1:            // PDF im extra Fenster -> Download
                    $this->Output($this->_BerichtsName . '.pdf', 'D');
                    break;
                case 2:            // Standard-Stream
                    $this->Output($this->_BerichtsName . '.pdf', 'I');
                    break;
                case 3:            // Als String zurückliefern
                    return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                    break;
                case 4:                // Als Datei speichern
                    $this->Output($this->_Dateiname, 'F');
                    break;
            }
        }
    }

    public function setSpeicherort($Speicherort = ''){
        if($Speicherort !=''){
            $this->Speicherort = $Speicherort;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L', [85.60, 53.98]);
    }

    function _endpage()
    {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

}