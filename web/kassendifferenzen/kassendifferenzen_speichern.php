<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
    if(isset($_POST['txtEdit']))
    {
       //Datensatz editiert..  Updaten und in Hist wegschreiben

        $SQL = 'UPDATE Kassendifferenzen';
        $SQL .= ' SET KDI_FILID ='.$DB->WertSetzen('KDI','N0',$_POST['txtKDI_FILID'],false).'';
        $SQL .= ', KDI_DATUM='.$DB->WertSetzen('KDI','D',$_POST['txtKDI_DATUM']).'';
        if ($_POST['txtKDI_KATEGORIE'] <> $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
        {
            $SQL .= ', KDI_KATEGORIE='.$DB->WertSetzen('KDI','N0',$_POST['txtKDI_KATEGORIE']).'';
        }
        $SQL .= ', KDI_BETRAG=' .   $DB->WertSetzen('KDI','N2',(($_POST['txtKDI_VORZEICHEN'] == '-')?'-':'').str_replace(',', '.', $_POST['txtKDI_BETRAG']));
        $SQL .= ', KDI_BEGRUENDUNG='.$DB->WertSetzen('KDI','T',$_POST['txtKDI_BEGRUENDUNG']).'';
        $SQL .= ', KDI_PERSNR='.$DB->WertSetzen('KDI','T',$_POST['txtKDI_PERSNR']).' ';
        $SQL .= 'WHERE KDI_KEY=' . $DB->WertSetzen('KDI','N0',$_POST['txtKDI_KEY']).'';
        
        
       
        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('KDI'));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());
        $SQL ='INSERT';
        $SQL .=' INTO kassendifferenzen_hist';
        $SQL .=' (';
        $SQL .=' KDH_FILID,';
        $SQL .=' KDH_DATUM,';
        $SQL .=' KDH_KATEGORIE,';
        $SQL .=' KDH_BETRAG,';
        $SQL .=' KDH_BEGRUENDUNG,';
        $SQL .=' KDH_PERSNR,';
        $SQL .=' KDH_USER,';
        $SQL .=' KDH_USERDAT';
        $SQL .=' )';
        $SQL .=' VALUES';
        $SQL .=' (';
        $SQL .=' ' .$DB->WertSetzen('KDH','N0',$_POST['oldKDI_FILID']) . ',';
        $SQL .=' ' .$DB->WertSetzen('KDH','D',$_POST['oldKDI_DATUM']) . ',';
        $SQL .=' ' .$DB->WertSetzen('KDH','N0',$_POST['txtoldKDI_KategorieHidden']) . ',';
        $SQL .= $DB->WertSetzen('KDH','W',str_replace(',','.',$_POST['txtKDI_BETRAG_HIST'])) . ',';
        $SQL .=' ' .$DB->WertSetzen('KDH','T',$_POST['oldKDI_BEGRUENDUNG']) . ',';
        $SQL .=' ' .$DB->WertSetzen('KDH','T',$_POST['oldKDI_PERSNR']) . ',';
        $SQL .=' ' .$DB->WertSetzen('KDH','T',$AWISBenutzer->BenutzerName()) . ',';
        $SQL .=' sysdate';
        $SQL .=' )';

        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('KDH'));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());
        
    }
    else 
    {
        //neuen Datensatz hinzufügen
        
        $SQL ='INSERT';
        $SQL .=' INTO Kassendifferenzen';
        $SQL .=' (';
        $SQL .=' KDI_FILID,';
        $SQL .=' KDI_DATUM,';
        $SQL .=' KDI_KATEGORIE,';
        $SQL .=' KDI_BETRAG,';
        $SQL .=' KDI_BEGRUENDUNG,';
        $SQL .=' KDI_PERSNR,';
        $SQL .=' KDI_USER,';
        $SQL .=' KDI_USERDAT';
        $SQL .=' )';
        $SQL .=' VALUES';
        $SQL .=' (';
        $SQL .=' '.$DB->WertSetzen('KDI','N0',$_POST['txtKDI_FILID']) .',';
        $SQL .=' '.$DB->WertSetzen('KDI','D',$_POST['txtKDI_DATUM']) .',';
        $SQL .=' '.$DB->WertSetzen('KDI','N0',$_POST['txtKDI_KATEGORIE']) .',';
        $SQL .=  $DB->WertSetzen('KDI','W',(($_POST['txtKDI_VORZEICHEN'] == '-')?'-':'').str_replace(',' , '.' ,$_POST['txtKDI_BETRAG'])) . ',' ;
        $SQL .=' '.$DB->WertSetzen('KDI','T',$_POST['txtKDI_BEGRUENDUNG']) .',';
        $SQL .=' '.$DB->WertSetzen('KDI','T',$_POST['txtKDI_PERSNR']) .',';
        $SQL .=' '. $DB->WertSetzen('KDI','T',$AWISBenutzer->BenutzerName()) . ',';
        $SQL .=' sysdate';
        $SQL .=' )';
        
        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('KDI'));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());
        
    }

	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>