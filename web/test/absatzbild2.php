<?php
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_line.php");

$con = @OCILogon("awis","IlOuLua","AWIS3");

$ATUNR = "";

if (empty($_GET["atunr"]))
{
	$ATUNR = 'FU6071';
}
else
{
	$ATUNR = strtoupper($_GET["atunr"]);
}


if($con)
{
	$sql = "select ABS_FIL_MONABS1, ABS_FIL_MONABS2, ABS_FIL_MONABS3, ABS_FIL_MONABS4, ABS_FIL_MONABS5, ABS_FIL_MONABS6, ABS_FIL_MONABS7, ABS_FIL_MONABS8, ";
   	$sql .= "ABS_FIL_MONABS9, ABS_FIL_MONABS10, ABS_FIL_MONABS11, ABS_FIL_MONABS12 from absatz ";
	$sql .= "where abs_atunr = '" . $ATUNR . "' ";
	$sql .= "and abs_jahr = 2004";

	$rsDaten = awisOpenRecordset($con, $sql);
	$rsDatenZeilen = $awisRSZeilen;

}
if($con)
{
	$sql = "select round(avg(a.ABS_FIL_MONABS1)) M1, round(avg(a.ABS_FIL_MONABS2)) M2, round(avg(a.ABS_FIL_MONABS3)) M3, round(avg(a.ABS_FIL_MONABS4)) M4, round(avg(a.ABS_FIL_MONABS5)) M5, round(avg(a.ABS_FIL_MONABS6)) M6, ";
	$sql .= "round(avg(a.ABS_FIL_MONABS7)) M7, round(avg(a.ABS_FIL_MONABS8)) M8, round(avg(a.ABS_FIL_MONABS9)) M9, round(avg(a.ABS_FIL_MONABS10)) M10, round(avg(a.ABS_FIL_MONABS11)) M11, round(avg(a.ABS_FIL_MONABS12)) M12 ";
	$sql .= "from absatz a, absatz b ";
	$sql .= "where a.abs_wgr_id = b.abs_wgr_id ";
	$sql .= "and a.abs_wug_id = b.abs_wug_id ";
	$sql .= "and a.abs_jahr = 2004 ";
	$sql .= "and b.abs_jahr = 2004 ";
	$sql .= "and b.abs_atunr = '" . $ATUNR . "' ";
	
	
	$rsDaten2 = awisOpenRecordset($con, $sql);
	$rsDatenZeilen2 = $awisRSZeilen;

}

$ydata = array($rsDaten["ABS_FIL_MONABS1"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS2"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS3"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS4"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS5"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS6"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS7"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS8"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS9"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS10"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS11"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS12"][0]);

$ydata2 = array($rsDaten2["M1"][0]);
array_push($ydata2,$rsDaten2["M2"][0]);
array_push($ydata2,$rsDaten2["M3"][0]);
array_push($ydata2,$rsDaten2["M4"][0]);
array_push($ydata2,$rsDaten2["M5"][0]);
array_push($ydata2,$rsDaten2["M6"][0]);
array_push($ydata2,$rsDaten2["M7"][0]);
array_push($ydata2,$rsDaten2["M8"][0]);
array_push($ydata2,$rsDaten2["M9"][0]);
array_push($ydata2,$rsDaten2["M10"][0]);
array_push($ydata2,$rsDaten2["M11"][0]);
array_push($ydata2,$rsDaten2["M12"][0]);


// Create the graph. These two calls are always required
$graph = new Graph(400,200,"auto");
// $graph = new Graph();
$graph->SetScale("textlin");
$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->title->SetColor("white");

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot2=new LinePlot($ydata2);

// Add the plot to the graph
$graph->Add($lineplot);
$graph->Add($lineplot2);

$graph->img->SetMargin(40,100,20,40);
$graph->title->Set($ATUNR);
//$graph->xaxis->title->Set("Monate");
//$graph->yaxis->title->Set("Menge");

$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

// Use months as X-labels
$gDateLocale = new DateLocale();
$gDateLocale->Set('de_DE');
$graph->xaxis->SetTickLabels($gDateLocale->GetShortMonth());

$lineplot->SetColor("red");
$lineplot->SetWeight(2);

$lineplot2->SetColor("blue");
$lineplot2->SetWeight(2);

// Set the legends for the plots
$lineplot->SetLegend('2004');
$lineplot2->SetLegend("Schnitt");

// Adjust the legend position
$graph->legend->Pos(0.05,0.5,"right","center");
$graph->legend->SetShadow(0);

//$graph->yaxis->SetColor("red");
$graph->yaxis->SetWeight(2);
$graph->SetShadow();

// Display the graph
$graph->Stroke();
?>


