<?php
//**************************************************************
// Liefert die Benutzer f�r das Personaleinsatzpl�ne Formular
// je nach ausgew�hltem PersEinsatz Bereich
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['PEB_KEY']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Rechte des Benutzers ermitteln
$SQL = 'SELECT PBR_PBZ_KEY ';
$SQL .= ' FROM PerseinsbereicheRechteVergabe';
$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
// Pr�fen G�ltigkeiten der Rechtevergabe Personaleins�tze
$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
$SQL .= ' AND PBM_PEB_KEY = 0'.floatval($_GET['PEB_KEY']);
$SQL .= ' AND XBN_STATUS = \'A\'';
$rsPBR = $DB->RecordSetOeffnen($SQL);
$RechtPEI = 0;
while(!$rsPBR->EOF())
{
	$RechtPEI |= pow(2,$rsPBR->FeldInhalt('PBR_PBZ_KEY')-1);
	$rsPBR->DSWeiter();
}

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT xbn_key, xbn_name || coalesce(\' \'||xbn_vorname,\'\') AS Anwender';
$SQL .= ' FROM Perseinsbereichemitglieder ';
$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
$SQL .= ' WHERE PBM_PEB_KEY = 0'.floatval($_GET['PEB_KEY']);
if(($RechtPEI&16)==0)
{
	$SQL .= ' AND PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
}
$SQL .= ' ORDER BY xbn_name,xbn_vorname';

$rsPEB = $DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsPEB->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsPEB->FeldInhalt('XBN_KEY').'">'.$rsPEB->FeldInhalt('ANWENDER').'</option>';
	}
	else
	{
		echo '#+#'.$rsPEB->FeldInhalt('XBN_KEY').'#+#'.$rsPEB->FeldInhalt('ANWENDER').'';
	}

	$rsPEB->DSWeiter();
}
?>