<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>
<body>
<?php

$Rechtestufe = awisBenutzerRecht($con, 3300);
		//  1=Suchen
		//	2=�ndern
		//	4=Hinzuf�gen
		//	8=L�schen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schadens-DB',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$PDF_Link='';
$txt_Bearb_ID=$_REQUEST['txt_Bearb_ID_Save'];
$ART=$_REQUEST['ART'];

awis_Debug(1,$_REQUEST);

if(isset($_POST['cmdSpeichern_x'])) 
{	
	if ($_POST['txtSQL_Anweisung']=='INSERT')
	{
		$Params[0]=$_POST['txtFIL_ID'];
		$Params[1]=$_POST['txtKundenname'];
		$Params[2]=$_POST['txtKundenvorname'];
		$Params[3]=$_POST['txtStrasse'];
		$Params[4]=$_POST['txtPLZ'];
		$Params[5]=$_POST['txtOrt'];
		$Params[6]=$_POST['txtTelefon'];
		$Params[7]=$_POST['txtTelefon2'];
		$Params[8]=$_POST['txtFax'];
		$Params[9]=$_POST['txtEMail'];
		$Params[10]=$_POST['txtPAN'];
		$Params[11]=$_POST['txtKFZ_Kennz'];
		$Params[12]=$_POST['txtRekl_Text'];
		$Params[13]=$_POST['txtEingang'];
		$Params[14]=$_POST['txtWA_NR'];
		$Params[15]=$_POST['txtSachbearbeiter'];
		$Params[16]=$_POST['txtAnrede'];
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER'], implode(';',$Params));
		
		awis_Debug(1,$Params);
		
		$SQL='';
		
		if ($ART=='S')
		{
			if (strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");
			}
			
			$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE FILNR=".$_POST['txtFIL_ID']." AND SCHADENSJAHR='".date("Y")."'";
			
			$rsResult = awisOpenRecordset($con,$SQL_LFDNR);
			$rsResultZeilen = $awisRSZeilen;
		
			if($rsResultZeilen==0)		// Keine Daten
			{
				die("<center><span class=HinweisText>Datensatz konnte nicht gespeichert werden!</span></center>");
			}
			else
			{
					$LFDNR=str_pad($rsResult['LFNR'][0], 4, "0", STR_PAD_LEFT);
			}
			
			$BEARBNR_NEU=date("y").str_pad($_POST['txtFIL_ID'], 3, "0", STR_PAD_LEFT).$LFDNR;
			$SCHADJAHR=date("Y");
			
			awis_Debug(1,$LFDNR,$BEARBNR_NEU,$SCHADJAHR);
				
			$SQL='INSERT INTO SCHAEDEN_NEW@SCHAD.ATU.DE (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,EINGABEAM,EINGABEDURCH)';
			$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $_POST['txtFIL_ID'] . ',' . $_POST['txtFIL_ID'] . ','.$_POST['txtAnrede'].',\''.$_POST['txtKundenname'].'\',\''.$_POST['txtKundenvorname'].'\',\''.$_POST['txtStrasse'].'\',\''.$_POST['txtPLZ'].'\',\''.$_POST['txtOrt'].'\',\''.$_POST['txtTelefon'].'\',\''.$_POST['txtTelefon2'].'\',\''.$_POST['txtFax'].'\',\''.$_POST['txtEMail'].'\',\''.$_POST['txtPAN'].'\',\''.$_POST['txtKFZ_Kennz'].'\',\''.$_POST['txtRekl_Text'].'\','.$_POST['txtEingang'].','.($_POST['txtWA_NR']!=''?$_POST['txtWA_NR']:'NULL').','.$SCHADJAHR.','.$_POST['txtSachbearbeiter'].',\''."OFFEN".'\',SYSDATE,\''.$_SERVER['PHP_AUTH_USER'].'\')';
		}
		
		if ($ART=='B')
		{
			
		}

		awis_Debug(1,$SQL);
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
			awisLogoff($con);
			die();
		}
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER'],"");

		if ($_POST['txtSachbearbeiter']==303){
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE),\''.$BEARBNR_NEU.'\',\''."BackOffice".'\')';	
		}
		else {
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE)+1,\''.$BEARBNR_NEU.'\',\''."Filialen".'\')';	
		}
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Wiedervorlage konnte nicht erstellt werden. Zur�ck zur Eingabe</a><p>";
			awisLogoff($con);
			die();
		}
		
		$PDF_Link='./schaden_pdf.php?BEARB_NR='.$BEARBNR_NEU;
		$Hinzufuegen=TRUE;

	}
	elseif ($_POST['txtSQL_Anweisung']=='UPDATE')
	{
		
		//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
		//--->
	
		$SQL='';
		
		if ($ART=='S')
		{
			if (strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&BEARB_ID=".$txt_Bearb_ID."&ART=S'>Zur�ck zur Eingabe</a><p>";	
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");				
			}
			
			$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}else{
			$SQL="SELECT * FROM BESCHWERDE@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}
		
		$rsSchad_Save = awisOpenRecordset($con,$SQL);
		$rsSchad_SaveZeilen = $awisRSZeilen;
		
		$SQL='';
		$txtHinweis='';
		
		if($rsSchad_SaveZeilen==0)		// Keine Daten
		{
				awislogoff($con);
				die("<span class=HinweisText>Datensatz wurde nicht gefunden!</span>");
		}
		
		if($_POST['txtEingang'] != $_POST['txtEingang_old'])
		{
			if($rsSchad_Save['EINGANGPER'][0] != $_POST['txtEingang_old'])
			{
				$txtHinweis.=',EINGANGPER von '.$_POST['txtEingang_old']. ' auf '.$rsSchad_Save['EINGANGPER'][0];
			}
			else
			{
				$SQL.=',EINGANGPER='.$_POST['txtEingang'];
			}
		}
		
		if($_POST['txtKundenname'] != $_POST['txtKundenname_old'])
		{
			if($rsSchad_Save['KUNDENNAME'][0] != $_POST['txtKundenname_old'])
			{
				$txtHinweis.=',KUNDENNAME von '.$_POST['txtKundenname_old']. ' auf '.$rsSchad_Save['KUNDENNAME'][0];
			}
			else
			{
				$SQL.=',KUNDENNAME=\''.$_POST['txtKundenname'].'\'';
			}
		}
		
		if($_POST['txtKundenvorname'] != $_POST['txtKundenvorname_old'])
		{
			if($rsSchad_Save['VORNAME'][0] != $_POST['txtKundenvorname_old'])
			{
				$txtHinweis.=',VORNAME von '.$_POST['txtKundenvorname_old']. ' auf '.$rsSchad_Save['VORNAME'][0];
			}
			else
			{
				$SQL.=',VORNAME=\''.$_POST['txtKundenvorname'].'\'';
			}
		}
			
		if($_POST['txtStrasse'] != $_POST['txtStrasse_old'])
		{
			if($rsSchad_Save['STRASSE'][0] != $_POST['txtStrasse_old'])
			{
				$txtHinweis.=',STRASSE von '.$_POST['txtStrasse_old']. ' auf '.$rsSchad_Save['STRASSE'][0];
			}
			else
			{
				$SQL.=',STRASSE=\''.$_POST['txtStrasse'].'\'';
			}
		}
		
		if($_POST['txtPLZ'] != $_POST['txtPLZ_old'])
		{
			if($rsSchad_Save['PLZ'][0] != $_POST['txtPLZ_old'])
			{
				$txtHinweis.=',PLZ von '.$_POST['txtPLZ_old']. ' auf '.$rsSchad_Save['PLZ'][0];
			}
			else
			{
				$SQL.=',PLZ=\''.$_POST['txtPLZ'].'\'';
			}
		}
		
		if($_POST['txtOrt'] != $_POST['txtOrt_old'])
		{
			if($rsSchad_Save['ORT'][0] != $_POST['txtOrt_old'])
			{
				$txtHinweis.=',ORT von '.$_POST['txtOrt_old']. ' auf '.$rsSchad_Save['ORT'][0];
			}
			else
			{
				$SQL.=',ORT=\''.$_POST['txtOrt'].'\'';
			}
		}
		
		if($_POST['txtTelefon'] != $_POST['txtTelefon_old'])
		{
			if($rsSchad_Save['TELEFON'][0] != $_POST['txtTelefon_old'])
			{
				$txtHinweis.=',TELEFON von '.$_POST['txtTelefon_old']. ' auf '.$rsSchad_Save['TELEFON'][0];
			}
			else
			{
				$SQL.=',TELEFON=\''.$_POST['txtTelefon'].'\'';
			}
		}
	
		if($_POST['txtTelefon2'] != $_POST['txtTelefon2_old'])
		{
			if($rsSchad_Save['TELEFON2'][0] != $_POST['txtTelefon2_old'])
			{
				$txtHinweis.=',TELEFON2 von '.$_POST['txtTelefon2_old']. ' auf '.$rsSchad_Save['TELEFON2'][0];
			}
			else
			{
				$SQL.=',TELEFON2=\''.$_POST['txtTelefon2'].'\'';
			}
		}
		
		if($_POST['txtEMail'] != $_POST['txtEMail_old'])
		{
			if($rsSchad_Save['EMAIL'][0] != $_POST['txtEMail_old'])
			{
				$txtHinweis.=',EMAIL von '.$_POST['txtEMail_old']. ' auf '.$rsSchad_Save['EMAIL'][0];
			}
			else
			{
				$SQL.=',EMAIL=\''.$_POST['txtEMail'].'\'';
			}
		}
		
		if($_POST['txtPAN'] != $_POST['txtPAN_old'])
		{
			if($rsSchad_Save['PAN'][0] != $_POST['txtPAN_old'])
			{
				$txtHinweis.=',PAN von '.$_POST['txtPAN_old']. ' auf '.$rsSchad_Save['PAN'][0];
			}
			else
			{
				$SQL.=',PAN=\''.$_POST['txtPAN'].'\'';
			}
		}
		
		if ($ART=='S') // �nderungen welche die Tabelle der Sch�den betrifft
		{
			if($_POST['txtFax'] != $_POST['txtFax_old'])
			{
				if($rsSchad_Save['FAX'][0] != $_POST['txtFax_old'])
				{
					$txtHinweis.=',FAX von '.$_POST['txtFax_old']. ' auf '.$rsSchad_Save['FAX'][0];
				}
				else
				{
					$SQL.=',FAX=\''.$_POST['txtFax'].'\'';
				}
			}	
			
			if($_POST['txtKFZ_Kennz'] != $_POST['txtKFZ_Kennz_old'])
			{
				if($rsSchad_Save['KFZ_KENNZ'][0] != $_POST['txtKFZ_Kennz_old'])
				{
					$txtHinweis.=',KFZ_KENNZ von '.$_POST['txtKFZ_Kennz_old']. ' auf '.$rsSchad_Save['KFZ_KENNZ'][0];
				}
				else
				{
					$SQL.=',KFZ_KENNZ=\''.$_POST['txtKFZ_Kennz'].'\'';
				}
			}
			
			if($_POST['txtWANR'] != $_POST['txtWANR_old'])
			{
				if($rsSchad_Save['WANR'][0] != $_POST['txtWANR_old'])
				{
					$txtHinweis.=',WANR von '.$_POST['txtWANR_old']. ' auf '.$rsSchad_Save['WANR'][0];
				}
				else
				{
					$SQL.=',WANR=\''.($_POST['txtWANR']!=''?$_POST['txtWANR']:NULL).'\'';
				}
			}
			
			if($_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['FEHLERBESCHREIBUNG'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',FEHLERBESCHREIBUNG von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['FEHLERBESCHREIBUNG'][0];
				}
				else
				{
					$SQL.=',FEHLERBESCHREIBUNG=\''.$_POST['txtRekl_Text'].'\'';
				}
			}
			
			if($_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old'])
			{
				if($rsSchad_Save['BEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',BEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['BEARBEITER'][0];
				}
				else
				{
					$SQL.=',BEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
		}
		
		if ($ART=='B') // �nderungen welche die Tabelle der Bechwerden betrifft
		{
			if($_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old'])
			{
				if($rsSchad_Save['SACHBEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',SACHBEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['SACHBEARBEITER'][0];
				}
				else
				{
					$SQL.=',SACHBEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
			
			if($_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['BESCHWERDEGRUND'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',BESCHWERDEGRUND von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['BESCHWERDEGRUND'][0];
				}
				else
				{
					$SQL.=',BESCHWERDEGRUND='.$_POST['txtRekl_Text'];	
				}
			}
		}
			
		//<---
		
		// Update- Befehl
		
		if($txtHinweis=='' && $SQL!='')
		{
				
				$SQL.=',EINGABEAM=SYSDATE';
		
				if ($ART=='S')
				{
					$SQL.=',EINGABEDURCH=\''.$_SERVER['PHP_AUTH_USER'].'\'';
					$SQL="UPDATE SCHAEDEN_NEW@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				else 
				{
					$SQL="UPDATE BESCHWERDE@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				
				awis_Debug(1,$_REQUEST,$SQL);
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
		}
		elseif($txtHinweis!='')
		{
				echo $txtHinweis;
				awislogoff($con);
				
				die("<br><span class=HinweisText>Datensatz wurde von einen anderen Benutzer ge�ndert !</span>");
		}
	}
}
if ((isset($_POST['cmdHinzufuegen_x']) && ($Rechtestufe&4)==4) || (($Hinzufuegen==TRUE || $_REQUEST['cmdHinzufuegen']==TRUE) && ($Rechtestufe&4)==4))
{
	if($PDF_Link!='' && ($Rechtestufe&16)==16)
	{
		echo "<br>";
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>&nbsp;PDF-Datei erzeugen";
		echo "<p>";		
	}
	
	$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER']));
	
	if($_REQUEST['txtAuftragsart_NEU']!='')
	{
		$ART=$_REQUEST['txtAuftragsart_NEU'];
	}
	else {$ART='S';}
	
	awis_Debug(1,$Params);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='INSERT'>";
	
	echo "<table width=100% border=1>";
	echo "<tr>";
	echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	echo "</tr>";
	echo '<tr>';
	echo '<td  id=FeldBez>Eingang durch</td>';
	echo "<td><select name=txtEingang>";
		echo "<option value='NULL'>Bitte w�hlen...</option>";
		$rsEingang = awisOpenRecordset($con, 'SELECT id, initcap(mittel) as Eingang FROM EINGANGPER@SCHAD.ATU.DE ORDER BY 1');
		$rsEingangZeilen = $awisRSZeilen;
		
		if(empty($Params[13]))
		{
			for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
			{
				echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
			}
		}
		else 
		{
			for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
			{
				if ($Params[13]==$rsEingang['ID'][$EingangZeile])
				{
					echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
				}
				else
				{
					echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
				}
			}
		}
		echo '</select></td>';
		unset($rsEingang);
	echo '</tr>';
	echo '<tr>';
	echo '<td  id=FeldBez>Anrede</td>';
	echo "<td><select name=txtAnrede>";
		$rsAnrede = awisOpenRecordset($con, 'SELECT ID, NAME FROM ANREDE@SCHAD.ATU.DE ORDER BY 1');
		$rsAnredeZeilen = $awisRSZeilen;
		
		if(empty($Params[16]))
		{
			for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
			{
				echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
			}
		}
		else 
		{
			for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
			{
				if ($Params[16]==$rsAnrede['ID'][$AnredeZeile])
				{
					echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
				}
				else
				{
					echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
				}
			}
		}
		echo '</select></td>';
		unset($rsAnrede);
	echo '</tr>';
	echo '<tr>';
	echo '<td  id=FeldBez>Kundenname / -vorname</td>';
	echo "<td><input type=text name=txtKundenname value='".(empty($Params[1])?"":$Params[1]) ."' size=30>&nbsp;<input type=text name=txtKundenvorname value='".(empty($Params[2])?"":$Params[2]) ."' size=30></td>";
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>Strasse</td>';
	echo "<td><input type=text name=txtStrasse value='".(empty($Params[3])?"":$Params[3]) ."' size=30></td>";
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>PLZ / Ort</td>';
	echo "<td><input type=text name=txtPLZ value='".(empty($Params[4])?"":$Params[4]) ."' size=10>&nbsp;<input type=text name=txtOrt value='".(empty($Params[5])?"":$Params[5]) ."' size=30></td>";
	echo '</tr>';
	echo '<tr>';
	echo "<td id=FeldBez>Telefon / Telefon 2</td>";
	echo "<td><input type=text name=txtTelefon value='".(empty($Params[6])?"":$Params[6]) ."' size=30>&nbsp;<input type=text name=txtTelefon2 value='".(empty($Params[7])?"":$Params[7]) ."' size=30></td>";
	echo '</tr>';
	if ($ART=='S')
	{
		echo '<tr>';
		echo '<td id=FeldBez>Fax</td>';
		echo "<td><input type=text name=txtFax value='".(empty($Params[8])?"":$Params[8]) ."' size=30></td>";
		echo '</tr>';
	}
	echo '<tr>';
	echo '<td id=FeldBez>E-Mail</td>';
	echo "<td><input type=text name=txtEMail value='".(empty($Params[9])?"":$Params[9]) ."' size=30></td>";
	echo '</tr>';
	echo '<tr>';
	echo '<td id=FeldBez>ATU-Card-Nummer</td>'; 
	echo "<td><input type=text name=txtPAN value='".(empty($Params[10])?"":$Params[10]) ."' size=30></td>";
	echo '</tr>';
	
	if ($ART=='S')
	{
		echo '<tr>';
		echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
		echo "<td><input type=text name=txtKFZ_Kennz value='".(empty($Params[11])?"":$Params[11]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Filial-Nr</td>';
		echo "<td><select name=txtFIL_ID>";
			echo "<option value='0'>Bitte w�hlen...</option>";
			$rsFIL = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ, NVL2(FIL_ORTSTEIL,FIL_ORT ||'-'|| FIL_ORTSTEIL,FIL_ORT) AS ORT, FIL_STRASSE FROM V_FIL_VKL_GBL ORDER BY FIL_ORT");
			$rsFILZeilen = $awisRSZeilen;
			
			if(empty($Params[0]))
			{
				for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
				{
						echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . ' ' . $rsFIL['FIL_BEZ'][$FILZeile] . '</option>';
				}
			}
			else 
			{
				for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
				{
					if($Params[0]==$rsFIL['FIL_ID'][$FILZeile])
					{
						echo "<option value=" . $rsFIL['FIL_ID'][$FILZeile] . " selected='selected'>" . $rsFIL['ORT'][$FILZeile] . " | " . $rsFIL['FIL_STRASSE'][$FILZeile] . " | " . $rsFIL['FIL_ID'][$FILZeile] . " " . $rsFIL['FIL_BEZ'][$FILZeile] . "</option>";	
					}
					else 
					{
						echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . ' ' . $rsFIL['FIL_BEZ'][$FILZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsFIL);
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>WA-Nummer</td>';
		echo "<td><input type=text name=txtWA_NR value='".(empty($Params[14])?"":$Params[14]) ."' size=10></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Fehlerbeschreibung (max. 2500 Zeichen)</td>';
		echo "<td><textarea name=txtRekl_Text rows=7 cols=100>".(empty($Params[12])?"":$Params[12]) ."</textarea></td>";
		echo '</tr>';
	}
	else
	{
		echo '<tr>';
		echo '<td  id=FeldBez>Beschwerdegrund</td>';
		echo "<td><select name=txtRekl_Text>";
			echo '<option value=-1>Bitte w�hlen...</option>';
			$rsRekl_Text = awisOpenRecordset($con, 'SELECT ID, BESCHWERDEGRUND FROM BESCHWERDEGRUENDE@SCHAD.ATU.DE ORDER BY 1');
			$rsRekl_TextZeilen = $awisRSZeilen;
			for($Rekl_TextZeile=0;$Rekl_TextZeile<$rsRekl_TextZeilen;$Rekl_TextZeile++)
			{
					echo '<option value=' . $rsRekl_Text['ID'][$Rekl_TextZeile] . '>' . $rsRekl_Text['BESCHWERDEGRUND'][$Rekl_TextZeile] . '</option>';
			}
			echo '</select></td>';
			unset($rsRekl_Text);
		echo '</tr>';
		
	}
	
	echo '<tr>';
	echo '<td  id=FeldBez>Sachbearbeiter</td>';
	echo "<td><select name=txtSachbearbeiter>";
		echo '<option value=0>Bitte w�hlen...</option>';
		$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME,SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304) ORDER BY 1');
		$rsSBZeilen = $awisRSZeilen;
		
		if(empty($Params[15]))
		{
			for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
			{
					echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
			}
		}
		else 
		{
			for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
			{
				if($Params[15]==$rsSB['ID'][$SBZeile])
				{
					echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile] . "" . $rsSB['SBVORNAME'][$SBZeile] . "</option>";	
				}
				else 
				{
					echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
				}
			}
		}
		echo '</select></td>';
		unset($rsSB);
	echo '</tr>';
	
	echo "</table>";
	
	echo "</form>";
	
}
elseif ((isset($_REQUEST['BEARB_ID'])||$_POST['BEARB_ID']!='') && ! isset($_POST['cmdAnzeigen_x'])) //Anzeigen der Details f�r Bearbeitungsnummer 
{
	if ($_POST['BEARB_ID']!='')
	{
		$BEARB_ID=$_POST['BEARB_ID'];
		$ART=$_POST['ART'];
	}
	else 
	{
		$BEARB_ID=$_REQUEST['BEARB_ID'];
		$ART=$_REQUEST['ART'];
	}
	
	awis_Debug(1,$_REQUEST);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";
	echo "<table width=100% border=0><tr>";
	if (($Rechtestufe&2)==2)
	{
		echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}else
	{
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}
	echo "</tr>";
	echo "</table>";
	echo "<input type=hidden name=txt_Bearb_ID_Save  value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=BEARB_ID value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='UPDATE'>";
	
	//echo "<center><h3>HiFi-Reklamationsauftrag</h3></center>";
	
	if ($ART=='S')
	{
		$SQL = "SELECT ART, FEHLERBESCHREIBUNG AS REKL_TEXT, FILNR, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, KUNDENNAME, VORNAME, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, KFZ_KENNZ, EINGANGPER, WANR ";
		$SQL .= "FROM SCHAEDEN_NEW@SCHAD.ATU.DE ";
	}
	else 
	{
		$SQL = "SELECT ART, BESCHWERDEGRUND AS REKL_TEXT, FILNR, BEARBEITUNGSNR, SACHBEARBEITER, KUNDENNAME, VORNAME, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, EMAIL, PAN, EINGANGPER ";
		$SQL .= "FROM BESCHWERDE@SCHAD.ATU.DE ";
	}
		
	$SQL.="WHERE BEARBEITUNGSNR='". $BEARB_ID ."'";
	
	awis_Debug(1,$SQL);
	
	$rsSchad = awisOpenRecordset($con,$SQL,true,true);
	$rsSchadZeilen = $awisRSZeilen;	
	
	for($i=0;$i<$rsSchadZeilen;$i++)
	{
		echo "<table width=100% border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td  id=FeldBez>Auftragsart</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . ($rsSchad['ART'][$i]=='S'?'Schaden':'Beschwerde') . "</td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Bearbeitungs-Nr</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['BEARBEITUNGSNR'][$i] . "</td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Filial-Nr</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['FILNR'][$i] . "</td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Eingang durch</td>';
		echo "<td id=TabellenZeileGrau><select name=txtEingang>";
			$rsEingang = awisOpenRecordset($con, 'SELECT ID, INITCAP(MITTEL) AS EINGANG FROM EINGANGPER@SCHAD.ATU.DE');
			$rsEingangZeilen = $awisRSZeilen;
			
			echo "<option value='NULL' selected='selected'>:: Kein Angabe ::</option>";
			
			for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
			{
				if($rsEingang['ID'][$EingangZeile]==$rsSchad['EINGANGPER'][$i])
				{
						echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
				}
				else 
				{
					echo "<option value=" . $rsEingang['ID'][$EingangZeile] . ">" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
				}
			}
			
		echo "</select><input type=hidden name=txtEingang_old value=".$rsSchad['EINGANGPER'][$i] .">";
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Kundenname / -vorname</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=30 value='" . $rsSchad['KUNDENNAME'][$i] . "'><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";
		echo "&nbsp;";
		echo "<input type=text name=txtKundenvorname size=30 value='" . $rsSchad['VORNAME'][$i] . "'><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Strasse</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . $rsSchad['STRASSE'][$i] . "'><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>PLZ / Ort</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=10 value='" . $rsSchad['PLZ'][$i] . "'><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";
		echo '&nbsp;';
		echo "<input type=text name=txtOrt size=30 value='" . $rsSchad['ORT'][$i] . "'><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Telefon / Telefon 2</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=30 value='" . $rsSchad['TELEFON'][$i] . "'><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>&nbsp;<input type=text name=txtTelefon2 size=30 value=" . $rsSchad['TELEFON2'][$i] . "><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";
		echo '</tr>';
		if ($ART=='S'){
		echo '<tr>';
		echo '<td id=FeldBez>Fax</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . $rsSchad['FAX'][$i] . "'><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";
		echo '</tr>';
		}
		echo '<tr>';
		echo '<td id=FeldBez>E-Mail</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . $rsSchad['EMAIL'][$i] . "'><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>ATU-Card-Nummer</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . $rsSchad['PAN'][$i] . "'><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";
		echo '</tr>';
		if ($ART=='S')
		{
			echo '<tr>';
			echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . $rsSchad['KFZ_KENNZ'][$i] . "'><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
			echo '</tr>';
			echo '<tr>';
			echo '<td id=FeldBez>WA-Nummer</td>';
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . $rsSchad['WANR'][$i] . "'><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";
			echo '</tr>';
		}
		echo '<tr>';
		if ($ART=='S'){
			echo '<td id=FeldBez>Fehlerbeschreibung (max. 2500 Zeichen)</td>';
			echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><textarea name=txtRekl_Text rows=7 cols=100>" . $rsSchad['REKL_TEXT'][$i] . "</textarea><input type=hidden name=txtRekl_Text_old  value='" . $rsSchad['REKL_TEXT'][$i] . "'></td>";			
		}
		else{
			echo '<td id=FeldBez>Beschwerdegrund</td>';
			echo "<td id=TabellenZeileGrau><select name=txtRekl_Text>";
			$rsBG = awisOpenRecordset($con, 'SELECT ID, BESCHWERDEGRUND FROM BESCHWERDEGRUENDE@SCHAD.ATU.DE');
			$rsBGZeilen = $awisRSZeilen;
			
			echo "<option value='NULL' selected='selected'>:: Kein Angabe ::</option>";
			
			for($BGZeile=0;$BGZeile<$rsBGZeilen;$BGZeile++)
			{
				if($rsBG['ID'][$BGZeile]==$rsSchad['REKL_TEXT'][$i])
				{
					echo $rsBG['ID'][$BGZeile];
					echo $rsSchad['REKL_TEXT'][$i];
						echo "<option value=" . $rsBG['ID'][$BGZeile] . " selected='selected'>" . $rsBG['BESCHWERDEGRUND'][$BGZeile]. "</option>";
				}
				else 
				{
					echo "<option value=" . $rsBG['ID'][$BGZeile] . ">" . $rsBG['BESCHWERDEGRUND'][$BGZeile] . "</option>";
				}
			}
			
			echo "</select><input type=hidden name=txtRekl_Text_old value=".$rsSchad['REKL_TEXT'][$i] .">";
		}
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Sachbearbeiter</td>';
		echo "<td id=TabellenZeileGrau><select name=txtSachbearbeiter>";
			$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME, SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304)');
			$rsSBZeilen = $awisRSZeilen;
			
			echo "<option value='NULL' selected='selected'>:: Kein Angabe ::</option>";
			
			for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
			{
				if($rsSB['ID'][$SBZeile]==$rsSchad['SACHBEARBEITER'][$i])
				{
						echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
				}
				else 
				{
					echo "<option value=" . $rsSB['ID'][$SBZeile] . ">" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
				}
			}
			
		echo "</select><input type=hidden name=txtSachbearbeiter_old value=".$rsSchad['SACHBEARBEITER'][$i] .">";
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
	}	
	echo "</form>";
	
	
	if ($ART=='S' && ($Rechtestufe&16)==16)
	{
		echo "<form name=frmSchadenListe method=post action=./schaden_pdf.php>";
		echo "<input type=image align=right accesskey=P title='PDF erstellen (Alt+P)' src=/bilder/pdf_gross.png name=cmdProtokoll>";
		echo "<input type=hidden name=BEARB_NR value='".$BEARB_ID."'>";
		echo "</form>";
	}
	
}
else //Zweig f�r Listen-Aufbau nach Suche
{
		if(isset($_POST['cmdSuche_x']))			
		{
			$Params[0] = $_POST['txtAuftragsart'];
			$Params[1] = $_POST['txtBearbeitungsnummer'];
			$Params[2] = $_POST['txtFilialNr'];
			$Params[3] = $_POST['txtKundenname'];
			$Params[4] = $_POST['txtStrasse'];
			$Params[5] = $_POST['txtPLZ'];
			$Params[6] = $_POST['txtOrt'];
			$Params[7] = $_POST['txtTelefon'];
			$Params[8] = $_POST['txtEMail'];
			$Params[9] = $_POST['txtPAN'];
			$Params[10] = $_POST['txtSachbearbeiter'];
			
			
			awis_BenutzerParameterSpeichern($con, "Schaden-Suche", $_SERVER['PHP_AUTH_USER'], implode(';',$Params));
		}
		else
		{
				$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Suche", $_SERVER['PHP_AUTH_USER']));
		}
		
		awis_ZeitMessung(0);
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$_SERVER['PHP_AUTH_USER']);

		if ($Params[0]=='S')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
			$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND (TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='(TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
		}
		elseif ($Params[0]=='B')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " TELEFON AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
			$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		else
		{
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		
		
		if($Bedingung!='')
		{
			if ($Params[0]!='A')
			{
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
			else 
			{
				$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
				$SQL .= ' UNION';
				$SQL .= " SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " TELEFON AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
		}
		else
		{
				//$SQL .= ' WHERE ROWNUM<=' . $AwisBenuPara;
				echo "<br>";
				die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		
		$SQL .= ' ORDER BY FILNR, BEARBEITUNGSNR';
		
		awis_Debug(1,$SQL);
		
		$rsResult = awisOpenRecordset($con,$SQL);
		$rsResultZeilen = $awisRSZeilen;
		
		if($rsResultZeilen==0)		// Keine Daten
		{
			//echo 'Es wurde kein Eintrag gefunden.';
			echo "<br>";
			die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
		
			if(($Rechtestufe&16)==16)
			{
				echo "<td id=FeldBez></td>";
			}
			echo "<td id=FeldBez>Auftragsart</td>";
			echo "<td id=FeldBez>Bearbeitungs-Nr</td>";
			echo "<td id=FeldBez>Filial-Nr</td>";
			echo "<td id=FeldBez>Kunde</td>";
			echo "<td id=FeldBez>Strasse</td>";
			echo "<td id=FeldBez>PLZ</td>";
			echo "<td id=FeldBez>Ort</td>";
			echo "<td id=FeldBez>Telefon</td>";
			echo "<td id=FeldBez>E-Mail</td>";
			echo "<td id=FeldBez>ATU-Card-Nummer</td>";
			echo "<td id=FeldBez>Sachbearbeiter</td>";
			echo "</tr>";
			
			for($i=0;$i<$rsResultZeilen;$i++)
			{
				echo '<tr>';
				if(($Rechtestufe&16)==16 && $rsResult['ART'][$i]=='S')
				{
					echo "<td id=TabellenZeileGrau><a href=./schaden_pdf.php?BEARB_NR=" . $rsResult['BEARBEITUNGSNR'][$i] . "><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>";
				}else {
					echo "<td></td>";
				}
				echo '<td id=TabellenZeileGrau>' . ($rsResult['ART'][$i]=='S'?'Schaden':'Beschwerde') . '</td>';
				echo '<td id=TabellenZeileGrau><a href=./schaden_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsResult['BEARBEITUNGSNR'][$i] . '&ART=' . $rsResult['ART'][$i] . '>' . $rsResult['BEARBEITUNGSNR'][$i] . '</a></td>';
				echo '<td id=TabellenZeileGrau><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsResult['FILNR'][$i] . '>' . $rsResult['FILNR'][$i] .  '</a></td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['NAME'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['STRASSE'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PLZ'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['ORT'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['TELNR'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['EMAIL'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PAN'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['SACHBEARBEITER'][$i] . '</td>';
				echo '</tr>';
			}
				
			print "</table>";
			
			if($i<$AwisBenuPara)
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden!';
			}
			else
			{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
}

?>
</body>
</html>

