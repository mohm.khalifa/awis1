<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once 'awisOeffnungszeitenImport.php';

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('FZP','%');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Wort','Dateiname');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_weiter');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen

    $Form->Formular_Start();
    $Form->SchreibeHTMLCode('<form name=frmfilialinfo action=./filialinfos_Main.php?cmdAktion=Oeffnungszeiten method=POST enctype="multipart/form-data">');

    if (isset($_FILES['FZP_IMPORTFILE']['tmp_name']) and $_FILES['FZP_IMPORTFILE']['tmp_name'] != '') {
        $Import = new awisOeffnungszeitenImport();

        try{
            $Protokoll = $Import->ImportiereDatei($_FILES['FZP_IMPORTFILE']['tmp_name']);

            if(is_array($Protokoll)){
                $Form->ZeileStart();
                $Form->Hinweistext($AWISSprachKonserven['FZP']['FZP_SPEICHERN_OK'],awisFormular::HINWEISTEXT_OK);
                $Form->ZeileEnde();

                $Form->FormularBereichStart();
                $Form->FormularBereichInhaltStart('Protokoll',false);

                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift('Zeile',250);
                $Form->Erstelle_Liste_Ueberschrift('Meldung',400);
                $Form->ZeileEnde();

                $i = 0;
                foreach ($Protokoll as $Eintrag){

                    $Form->ZeileStart();
                    $Form->Erstelle_ListenFeld('Zeile',$Eintrag['Zeile'],25,250,false,$i%2);

                    if(isset($AWISSprachKonserven['FZP']['FZP_STATUS_'.$Eintrag['Meldung']])){
                        $Meldung = $AWISSprachKonserven['FZP']['FZP_STATUS_'.$Eintrag['Meldung']];
                    }else{
                        $Meldung = $AWISSprachKonserven['FZP']['FZP_STATUS_UNBEKANNT'];
                    }

                    $Form->Erstelle_ListenFeld('Meldung',$Meldung,10,400,false,$i%2);
                    $Form->ZeileEnde();
                }

                $Form->FormularBereichInhaltEnde();
                $Form->FormularBereichEnde();

            }

        }catch (Exception $e){

            $Form->ZeileStart();
            $Form->Hinweistext($e->getPrevious()->getMessage(),awisFormular::HINWEISTEXT_OK);
            $Form->ZeileEnde();
        }

    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Öffnungszeitenimport',170,'font-weight: bold; font-size:15px');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Musterdatei: ', 280);
    $Form->Erstelle_TextLabel('Download', 280,'','#./oeffnungszeiten_template.csv');
    $Form->ZeileEnde();


    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'], 280);
    $Form->Erstelle_DateiUpload('FZP_IMPORTFILE', 600, 30, 2000000,'','.pdf');
    $Form->ZeileEnde();


    $Form->Formular_Ende();

    //***************************************
    // Schaltflächen für dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
