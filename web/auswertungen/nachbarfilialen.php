<html>
<head>
    <link rel="SHORTCUT ICON" href="../favicon.ico">
    <title>Nachbarfilialen</title>
</head>

<body>
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();

echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) .">";
include ("awisHeader3.inc");	// Kopfzeile

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FIL','FIL_ANZNACHBARN');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);

$Form->Formular_Start();
echo '<form name="frmNachbarfilialen" method="post">';

$Form->FormularBereichStart();
$Form->FormularBereichInhaltStart('Nachbarfilialen',true);
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ANZNACHBARN'] . ': ',150);
$Form->Erstelle_TextFeld('AnzahlNachbarn',isset($_POST['txtAnzahlNachbarn'])?$_POST['txtAnzahlNachbarn']:'5',3,30,true);
$Form->ZeileEnde();

$Form->FormularBereichInhaltEnde();
$Form->FormularBereichEnde();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href','cmdZurueck','/auswertungen/auswertungen_Main.php','/bilder/cmd_zurueck.png');
$Form->Schaltflaeche('href','cmdExport','./nachbarfilialen_downloaden.php?AnzahlNachbarn=5','/bilder/cmd_export_xls.png','','','',27,27);

$Form->SchaltflaechenEnde();

echo '</form>';
$Form->Formular_Ende();
	

?>
<script type="application/javascript">
    $('#txtAnzahlNachbarn').change(function () {
       $('#cmdExport').attr('href','./nachbarfilialen_downloaden.php?AnzahlNachbarn='+$('#txtAnzahlNachbarn').val());
    });

</script>
</body>
</html>