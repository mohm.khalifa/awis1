<?php
global $con;
global $Recht3800;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FEB','FEB_%');
$TextKonserven[]=array('FEB','tttFEB_%');
$TextKonserven[]=array('FEB','lstFEB_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','GehoertZu');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3800 = awisBenutzerRecht($con,3800,$AWISBenutzer->BenutzerName());
$Recht3802 = awisBenutzerRecht($con,3802,$AWISBenutzer->BenutzerName());
if($Recht3800==0)
{
    awisEreignis(3,1000,'FEB',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
$Param = array_fill(0,10,'');
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = array_fill(0,10,'');
	$Param['FEB_BEZEICHNUNG']= $_POST['sucFEB_BEZEICHNUNG'];
	$Param['FEB_EBENE']= $_POST['sucFEB_EBENE'];
	$Param['AKTIVE']= $_POST['sucAKTIVE'];
	$Param['FEB_FILIALE']= $_POST['sucFEB_FILIALE'];
	$Param['KON_NAME']= $_POST['sucKON_NAME'];

	awis_BenutzerParameterSpeichern($con, "FEBSuche" , $AWISBenutzer->BenutzerName() , implode(';',$Param));
	awis_BenutzerParameterSpeichern($con, "AktuellerFEB" , $AWISBenutzer->BenutzerName() , '');
}
elseif(isset($_POST['cmdDSZurueck_x']))
{
	$Param = awis_BenutzerParameter($con, 'FEBSuche', $AWISBenutzer->BenutzerName());
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND FEB_BEZEICHNUNG < '.awis_FeldInhaltFormat('T',$_POST['txtFEB_BEZEICHNUNG']);
	
	$SQL = 'SELECT * FROM ';
	$SQL .= '(  SELECT FEB_KEY FROM FilialEbenen';
	$SQL .= '   WHERE '.substr($Bedingung,4);
	$SQL .= '   ORDER BY FEB_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsFEB = awisOpenRecordset($con, $SQL);
	if(isset($rsFEB['FEB_KEY'][0]))
	{
		$AWIS_KEY1=$rsFEB['FEB_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtFEB_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$Param = awis_BenutzerParameter($con, 'FEBSuche', $AWISBenutzer->BenutzerName());
	$Bedingung = _BedingungErstellen($Param);
	$Bedingung .= ' AND FEB_BEZEICHNUNG > '.awis_FeldInhaltFormat('T',$_POST['txtFEB_BEZEICHNUNG']);
	
	$SQL = 'SELECT FEB_KEY FROM (SELECT FEB_KEY FROM FilialEbenen';
	$SQL .= ' WHERE '.substr($Bedingung,4);
	$SQL .= ' ORDER BY FEB_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';

	$rsFEB = awisOpenRecordset($con, $SQL);
	if(isset($rsFEB['FEB_KEY'][0]))
	{
		$AWIS_KEY1=$rsFEB['FEB_KEY'][0];
	}
	else 
	{
		$AWIS_KEY1 = $_POST['txtFEB_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_GET['DelFRV']) or isset($_POST['cmdLoeschenOK']))
{
	include('./filialebenen_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "FEBSuche" , $AWISBenutzer->BenutzerName());
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./filialebenen_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerFEB" , $AWISBenutzer->BenutzerName());
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$AWIS_KEY1 = -1;
}
elseif(isset($_GET['FEB_KEY']))
{
	$AWIS_KEY1 = floatval($_GET['FEB_KEY']);		// Nur den Key speiechern
}
elseif(isset($_POST['txtFEB_KEY']))
{
	$AWIS_KEY1 = floatval($_POST['txtFEB_KEY']);		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	if(!isset($_GET['Liste']))
	{
		$AWIS_KEY1 = awis_BenutzerParameter($con, "AktuellerFEB" , $AWISBenutzer->BenutzerName());
	}
	else 
	{
		awis_BenutzerParameterSpeichern($con, "AktuellerFEB" , $AWISBenutzer->BenutzerName(),'');
	}

	if($AWIS_KEY1=='')
	{
		$Param = explode(';',awis_BenutzerParameter($con, 'FEBSuche', $AWISBenutzer->BenutzerName()));
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = _BedingungErstellen($Param);

$SQL = 'SELECT FilialEbenen.* ';
$SQL .= ' , (SELECT FEB_BEZEICHNUNG FROM FilialEbenen FE WHERE FE.FEB_KEY = FILIALEBENEN.FEB_FEB_KEY) AS BEZEICHNUNG';
$SQL .= ' FROM FilialEbenen';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY FEB_EBENE, FEB_SORTIERUNG, FEB_BEZEICHNUNG';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}
//awis_Debug(1,$SQL);
// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
$rsFEB = awisOpenRecordset($con, $SQL);
$rsFEBZeilen = $awisRSZeilen;
//********************************************************
// Daten anzeigen
//********************************************************
if($rsFEBZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>';
}
elseif($rsFEBZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FEB_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FEB_BEZEICHNUNG'))?'~':'');
	awis_FORM_ZeileEnde();

	if($rsFEBZeilen>$MaxDSAnzahl)
	{
//		awis_FORM_Hinweistext('Ausgabe begrenzt auf '.$MaxDSAnzahl.'. Insgesamt '.$rsFEBZeilen.' Zeilen gefunden.');
//		$rsFEBZeilen=$MaxDSAnzahl;
	}

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsFEBZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsFEBZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./filialebenen_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	awis_FORM_ZeileStart();
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FEB_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FEB_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEB']['FEB_BEZEICHNUNG'],300,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FEB_EBENE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FEB_EBENE'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEB']['FEB_EBENE'],100,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['GehoertZu'],300,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FEB_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FEB_GUELTIGAB'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEB']['FEB_GUELTIGAB'],120,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=FEB_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FEB_GUELTIGBIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEB']['FEB_GUELTIGBIS'],120,'',$Link);
	awis_FORM_ZeileEnde();
	
	for($FEBZeile=$StartZeile;$FEBZeile<$rsFEBZeilen and $FEBZeile<$StartZeile+$MaxDSAnzahl;$FEBZeile++)
	{	
		$FeldStyle = '';
		if(awis_PruefeDatum($rsFEB['FEB_GUELTIGAB'][$FEBZeile],false,true)>time()
			or awis_PruefeDatum($rsFEB['FEB_GUELTIGBIS'][$FEBZeile],false,true)<time())
		{
			$FeldStyle='font-style:italic;color:#FF2020';
		}
		awis_FORM_ZeileStart();
		$Link = './filialebenen_Main.php?cmdAktion=Details&FEB_KEY='.$rsFEB['FEB_KEY'][$FEBZeile].'';
		awis_FORM_Erstelle_ListenFeld('FEB_BEZEICHNUNG',$rsFEB['FEB_BEZEICHNUNG'][$FEBZeile],0,300,false,($FEBZeile%2),$FeldStyle,$Link,'T','L',$rsFEB['FEB_BEMERKUNG'][$FEBZeile]);
		awis_FORM_Erstelle_ListenFeld('FEB_EBENE',$rsFEB['FEB_EBENE'][$FEBZeile],0,100,false,($FEBZeile%2),$FeldStyle,'','N0');
		awis_FORM_Erstelle_ListenFeld('BEZEICHNUNG',($rsFEB['BEZEICHNUNG'][$FEBZeile]==''?'--':$rsFEB['BEZEICHNUNG'][$FEBZeile]),0,300,false,($FEBZeile%2),$FeldStyle);
		awis_FORM_Erstelle_ListenFeld('FEB_GUELTIGAB',($rsFEB['FEB_GUELTIGAB'][$FEBZeile]==''?'--':$rsFEB['FEB_GUELTIGAB'][$FEBZeile]),0,120,false,($FEBZeile%2),$FeldStyle);
		awis_FORM_Erstelle_ListenFeld('FEB_GUELTIGBIS',($rsFEB['FEB_GUELTIGBIS'][$FEBZeile]==''?'--':$rsFEB['FEB_GUELTIGBIS'][$FEBZeile]),0,120,false,($FEBZeile%2),$FeldStyle);
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();

	awis_FORM_SchaltflaechenStart();
	if(($Recht3800&16)==16)
	{
		awis_FORM_Schaltflaeche('href', 'cmdDrucken', './filialebenen_drucken_uebersicht.php', '/bilder/drucker.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
	}	
	awis_FORM_SchaltflaechenEnde();
	
}			// Ein einzelner Datensatz
else
{
	echo '<form name=frmFilialebenen action=./filialebenen_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['FRV_KEY'])?'&FRZ_KEY='.$_GET['FRZ_KEY']:'').''.' method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsFEB['FEB_KEY'][0])?$rsFEB['FEB_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerFEB" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);
	echo '<input type=hidden name=txtFEB_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFEB['FEB_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFEB['FEB_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	//$EditRecht=(($Recht3800&2)!=0);
	$EditRecht=false;
	if (($Recht3800&4)==4)
	{
		$EditRecht=true;
	}

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FEB_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsFEB['FEB_BEZEICHNUNG'][0]),50,350,$EditRecht,'','','','T','L');
	$CursorFeld='txtFEB_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FEB_BEMERKUNG',($AWIS_KEY1===0?'':$rsFEB['FEB_BEMERKUNG'][0]),120,500,$EditRecht,'','','','T','L');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_EBENE'].':',150);
	awis_FORM_Erstelle_TextFeld('FEB_EBENE',($AWIS_KEY1===0?'':$rsFEB['FEB_EBENE'][0]),3,50,$EditRecht,'','','','N0','L',$AWISSprachKonserven['FEB']['tttFEB_EBENE']);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_SORTIERUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('FEB_SORTIERUNG',($AWIS_KEY1===0?'':$rsFEB['FEB_SORTIERUNG'][0]),3,50,$EditRecht,'','','','N0','L',$AWISSprachKonserven['FEB']['tttFEB_SORTIERUNG'],50);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_GUELTIGAB'].':',150);
	awis_FORM_Erstelle_TextFeld('FEB_GUELTIGAB',($AWIS_KEY1===0?'':$rsFEB['FEB_GUELTIGAB'][0]),10,120,$EditRecht,'','','','D','L','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_GUELTIGBIS'].':',120);
	awis_FORM_Erstelle_TextFeld('FEB_GUELTIGBIS',($AWIS_KEY1===0?'':$rsFEB['FEB_GUELTIGBIS'][0]),10,150,$EditRecht,'','','','D','L','',awis_PruefeDatum('31.12.2030'));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_ANWENDUNG'].':',150);
	$Texte = explode("|",$AWISSprachKonserven['FEB']['lstFEB_ANWENDUNG']);
	awis_FORM_Erstelle_SelectFeld('FEB_ANWENDUNG',($AWIS_KEY1===0?'':$rsFEB['FEB_ANWENDUNG'][0]),100,$EditRecht,$con,'','','','','',$Texte,'');
	awis_FORM_ZeileEnde();

	if ($EditRecht == true)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_FEB_KEY'].':',150);
		$AktuelleDaten = ($AWIS_KEY1===0?'':array($rsFEB['FEB_FEB_KEY'][0].'~'.$rsFEB['BEZEICHNUNG'][0]));
		awis_FORM_Erstelle_SelectFeld('FEB_FEB_KEY',($AWIS_KEY1===0?'':$rsFEB['FEB_FEB_KEY'][0]),100,$EditRecht,$con,'***FEB_Daten;txtFEB_FEB_KEY;FEB_EBENE=*txtFEB_EBENE&WERT='.(!isset($rsFEB['FEB_FEB_KEY'][0])?'':$rsFEB['FEB_FEB_KEY'][0]).'','','','','',$AktuelleDaten,'','','','');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();

	if(!isset($_POST['cmdDSNeu_x']))
	{
		if ($Recht3802!=0)
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Rollenzuordnungen'));		
		}
		else 
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Filialzuordnungen'));		
		}
		
		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';
		awis_RegisterErstellen(3802, $con, $RegisterSeite);
	}

	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3800&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht3800&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht3800&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}	
	if(($Recht3800&16)==16)
	{
		awis_FORM_Schaltflaeche('href', 'cmdDrucken', './filialebenen_drucken_uebersicht.php?FEB_KEY=0'.$rsFEB['FEB_KEY'][0].'&FEB_EBENE=0'.$rsFEB['FEB_EBENE'][0], '/bilder/drucker.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
	}	
	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsFEB, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	
	$Bedingung = '';

	if($AWIS_KEY1 <> 0)
	{
		$Bedingung = 'AND FEB_KEY = '.$AWIS_KEY1;
		return $Bedingung;
	}

	if(isset($Param['FEB_BEZEICHNUNG']) and $Param['FEB_BEZEICHNUNG']!='')		// Name
	{
		$Bedingung .= 'AND FEB_Bezeichnung  ' . awisLIKEoderIST($Param['FEB_BEZEICHNUNG']) . ' ';
	}

	if(isset($Param['FEB_EBENE']) and $Param['FEB_EBENE']!='')		// Ebene
	{
		$Bedingung .= 'AND FEB_Ebene = ' . awis_FeldInhaltFormat('N0',$Param['FEB_EBENE']) . ' ';
	}	

	if(isset($Param['AKTIVE']) AND $Param['AKTIVE']!='')		// Aktive?
	{
		if(awis_FeldInhaltFormat('N0',$Param['AKTIVE'])==1)
		{
			$Bedingung .= 'AND trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
		}
		else 
		{
			$Bedingung .= 'AND (trunc(FEB_GUELTIGAB) >= trunc(SYSDATE) OR trunc(FEB_GUELTIGBIS) <= trunc(SYSDATE))';
		}
	}
	
	if(isset($Param['FEB_FILIALE']) and $Param['FEB_FILIALE']!='')		// Name
	{
		$Bedingung .= 'AND FEB_KEY in (SELECT FEZ_FEB_KEY FROM FILIALEBENENZUORDNUNGEN WHERE FEZ_FIL_ID = ' . awis_FeldInhaltFormat('N0',$Param['FEB_FILIALE']) . ' AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) >= trunc(SYSDATE)) ';
	}

	// Suche nach einem Kontaktnamen in einem Gebiet
	// -> Soll es erleichtern, einen Mitarbeiter zu finden, ob er zugeordnet ist
	if(isset($Param['KON_NAME']) and $Param['KON_NAME']!='')		// Name
	{
		$Bedingung .= 'AND (FEB_KEY in (SELECT FRZ_XXX_KEY 
				FROM FILIALEBENENROLLENZUORDNUNGEN
				INNER JOIN KONTAKTE ON FRZ_KON_KEY = KON_KEY 
				WHERE FRZ_XTN_KUERZEL =  \'FEB\' 
				AND UPPER(KON_NAME1) ' . awisLIKEoderIST($Param['KON_NAME'].'%',true,false) . ' AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)) ';
		$Bedingung .= 'OR  FEB_KEY in (SELECT FEZ_FEB_KEY 
				FROM FILIALEBENENROLLENZUORDNUNGEN
				INNER JOIN FILIALEBENENZUORDNUNGEN ON FRZ_XXX_KEY = FEZ_FIL_ID AND FRZ_XTN_KUERZEL =  \'FIL\' AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) >= trunc(SYSDATE) 
				INNER JOIN KONTAKTE ON FRZ_KON_KEY = KON_KEY 
				WHERE UPPER(KON_NAME1) ' . awisLIKEoderIST($Param['KON_NAME'].'%',true,false) . ' AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)) ';
		$Bedingung .= ')';
	}

	return $Bedingung;
}