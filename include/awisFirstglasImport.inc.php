<?php

/******************************************************************************
 *
 *                          IMPORT SCRIPT V1.0
 *                          @author Christian Beierl
 *                       
 *                      (IMPORTLOGS'S & FEHLERROUTINEN)
 *
 ******************************************************************************/

require_once('awisDatenbank.inc');
require_once('awisFirstglasFunktionen.php');
require_once('awisBenutzer.inc');

class FirstGlasImport
{



	/**
	 * Testpfad
	 *
	 * @var string
	 */
	protected $pfadFile = '../../../daten/web/firstglas/Daten/';
    /**
     * Testpfad GlasKopf
	 *
	 * @var string
	 */
	protected $pfadGlasKopf = '../../../daten/web/firstglas/Daten/Kopf/';

	/**
     * Letztes File das importiert wurde
	 * @var string
	 *
	 */
    protected $lastKassendaten = '';

    /**
     *
     * @var string
     */
    protected $lastFile = '';

    /**
     * Liste mit zuladenenDateien
     * @var array
     */
    protected $aktFile = array();

    /**
     *
     * @var array
     */
	protected $fikFiles = array();

    protected $aktDate;
	protected $aktFileReadyToImport;
	protected $processingFlag = 0;
	protected $diffDays;
	protected $error_msg;

    protected $pruefen;

	/**
	 * Datenbank
	 *
	 * @var awisDatenbank
	 */
	private $_DB;

	protected $unb_Kopfsatz = array();
	protected $unb_Positionssatz = array();
    private   $_Anzahl_DS_Positionssatz;
	protected $unb_Stornosatz = array();

	protected $ber_Kopfsatz = array();
	protected $ber_temp_Positionssatz = array();

    //Bereinigtes Positionssatz - Array
    protected $temp_fehlerPositionssatz = array();
    protected $fehlerPositionssatz = array();

    protected $ber_Positionssatz = array();
	protected $ber_Stornosatz = array();

	protected $aktZeile;
	protected $aktZeilennummer;
	protected $posZeilen;
	protected $stornoZeilen;
    /**
     * VorgangsNummer
     * @var String
     */
    private $_VorgangNr;

	/**
	 * Aktueller Benutzer
	 *
	 * @var awisBenutzer
	 */
	protected $_AWISBenutzer;
    protected $checkPossatz;
    
    
	//Konstruktur
	function __construct()
	{
	    $this->_AWISBenutzer = awisBenutzer::Init();
        $this->openDBConnection();
	}

	public function setPfadFile($pfadFile)
	{
        
	    $this->pfadFile = '';
	    $this->pfadFile = $pfadFile;
	}

	public function getPfadFile()
	{
	    return $this->pfadFile;
        
    }

	public function runImport()
	{

	}

	private function checkImportFiles()
	{

	    for($element=0;$element<count($this->aktFile);$element++)
	    {
	        $this->aktFileReadyToImport = '';
	        $cutExtension = '';
	        $cutExtension = substr($this->aktFile[$element],0,8);

	        if ($cutExtension == $this->fikFiles[0])
	        {
	            //Aktuelles File holen
	            //----------------------------------------------
	            $this->aktFileReadyToImport = $this->aktFile[$element];
	            $this->readInArray();
	        }
	        else
	        {
	            echo "N�chstes Element nicht vorhanden";
	        }
	    }


	}

	private function readInArray()
	{
	        if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false)
	        {
	            echo $this->error_msg = "Fehler beim �ffnen der Datei";
	        }
	        else
	        {
	            //Anfang Zeilennummer auf 1 setzen
	            $this->aktZeilennummer = 1;

	            while(! feof($fd))
	            {
	                $this->aktZeile = fgets($fd);

	                if(substr($this->aktZeile,0,1) == 'K')
	                {
	                  $this->unb_Kopfsatz = explode(';',$this->aktZeile);

	                  $this->checkKopfArray();
	                }
	            }
	            fclose($fd);

	        //Lese komplette Positionsdaten und Stornodaten ein!
	        //Filezeiger zur�cksetzen

	        $fd = null;
	        $this->posZeilen =0;
	        
	        if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false)
	        {
	            echo $this->error_msg = "Fehler beim �ffnen der Datei";
	        }
	        else
	        {
	            //Anfang Zeilennummer auf 1 setzen
	            $this->aktZeilennummer = 1;

	            while(! feof($fd))
	            {
	                $this->aktZeile = fgets($fd);

	                if(substr($this->aktZeile,0,1) == 'P')
	                {
	                  $this->unb_Positionssatz[$this->posZeilen] = explode(';',$this->aktZeile);
	                  $this->posZeilen++;

                    }

                    //elseif(substr($this->aktZeile,0,1) == 'S')
	                //{
	                //$this->unb_Stornsatz[$this->stornoZeilen] = explode(';',$this->aktZeile);
	                //var_dump($this->Stornosatz);
	                //}
	           }
               $this->checkPosArray();
	           //var_dump($this->unb_Positionssatz);
            }
	        fclose($fd);

            $fd = null;

            if(($fd = fopen($this->pfadFile.$this->aktFileReadyToImport,'r')) === false)
	        {
	            echo $this->error_msg = "Fehler beim �ffnen der Datei";
	        }
	        else
	        {
	            //Anfang Zeilennummer auf 1 setzen
	            //$this->aktZeilennummer = 1;

	            while(! feof($fd))
	            {
	                $this->aktZeile = fgets($fd);

	                if(substr($this->aktZeile,0,1) == 'S')
	                {
	                  $this->unb_Stornosatz = explode(';',$this->aktZeile);

                      //CHECK STORNO - ARRAY
	                 $this->checkStornoArray();
	                }
	            }
            }
           fclose($fd);

           //Bei Exception Fehlernummer in ARRAY schreiben

	        }//Else Zweig
	        //var_dump($this->unb_Stornosatz);
	}

	//Kopfdatens�tze �berpr�fen!!!!!
	//------------------------------

	private function checkKopfArray()
	{
	    $fehler = false;
	    //Bereinigung KFZ-KENNZEICHEN ARRAY POSITION[4]
	    $gewKFZKennz = '';
	    $this->pruefen = new FirstglasFunktionen();
	    $gewKFZKennz = $this->pruefen->wandleKFZKennzeichen($this->unb_Kopfsatz[4]);
	    $this->unb_Kopfsatz[4] = $gewKFZKennz;

	    //Ueberpruefen der VorgangsNummer
	    $pruefeVorgangsNummer = false;
	    $pruefeVorgangsNummer = $this->pruefen->pruefeVorgangNr($this->unb_Kopfsatz[2]);

	    if($pruefeVorgangsNummer == true)
	    {

	    }
	    else
	    {
	        $fehler = true;
	        //Schreibe falschen Vorgang in die Fehlertabelle
	    }

	    //Checken SB - Kopfdaten
	    $SB = ctype_digit($this->unb_Kopfsatz[9]);
	    $SB = $this->pruefen->pruefeSB($this->unb_Kopfsatz[9]);
	    $this->unb_Kopfsatz[9] = $SB;


	    if($fehler == false)
	    {
	        $this->copyToCleanKopfArray();
	        //Array bereinigen
	        array_splice($this->unb_Kopfsatz,0);
	        //Schreibt alle �berpr�ften Kopfs�tze in das Array ber_Kopfsatz
	        var_dump($this->ber_Kopfsatz);

	        //Aufruf nach Check - Speichere Kopfsatzdaten
	        $this->safeKopfsatz();
	    }
	    else
	    {
	        //Fehler in Tab "FGKOPFDATENFEHLER" aufnehmen

            $SQL = "INSERT INTO FGKOPFDATENFEHLER (FKF_KENNUNG,FKF_FILID,FKF_VORGANGNR,";
	        $SQL .= "FKF_KFZBEZ,FKF_KFZKENNZ,FKF_KBANR,FKF_FAHRGESTELLNR,FKF_VERSICHERUNG,";
	        $SQL .= "FKF_VERSSCHEINNR,FKF_SB,FKF_MONTAGEDATUM,FKF_ZEITSTEMPEL,FKF_STEUERSATZ,";
	        $SQL .= "FKF_KUNDENNAME,FKF_IMP_KEY,FKF_USER,FKF_USERDAT) VALUES (";
	        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[0],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->unb_Kopfsatz[1],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[2],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[3],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[4],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[5],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[6],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[7],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[8],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->unb_Kopfsatz[9],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$this->unb_Kopfsatz[10],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->unb_Kopfsatz[11],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[12],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->unb_Kopfsatz[13],false);
			$SQL .= ',null';
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

	        //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--
	        //echo $SQL;

	        if($this->_DB->Ausfuehren($SQL)===false)
			{
				//throw new awisException('',,$SQL,2);
			}

             //Bereinige Array
             //----------------------------------
             array_splice($this->unb_Kopfsatz,0);

             $fehler = false;

        }
	}//------ Ende checkKopfArray


    //Ueberpruefen des StornoArray's
    //------------------------------

    private function checkStornoArray()
    {
     //Pruefen ob bei den Stornodaten das FELD "SB" korrekt ist
     //-------------------------------------------------------

     //var_dump($this->unb_Stornosatz);
     
     $SB = ctype_digit($this->unb_Stornosatz[9]);
 	 $SB = $this->pruefen->pruefeSB($this->unb_Stornosatz[9]);
	 $this->unb_Stornosatz[9] = $SB;
     

    $this->copyToCleanStornoArray();
	        //Array bereinigen
	 array_splice($this->unb_Stornosatz,0);
	        //Schreibt alle �berpr�ften Kopfs�tze in das Array ber_Kopfsatz
	 //var_dump($this->unb_Stornosatz);

	//Aufruf nach Check - Speichere Kopfsatzdaten
	//Speicher Stornodatensatz
    $this->safeStornosatz();
    }

   
   private function checkPosArray()
	{
        //-------Fehler verbleiben in den unbereinigten Array-----
        //-------Schreibe Daten in das bereinigte Array-----------

       $Pruefe_Anzahl = false;
       $Pruef_Array_Anzahl = Array();
       $Pruefe_Alle_Anzahl = false;
       $Pruef_Array_Oempreis = Array();
       $Pruefe_Alle_Oempreis = false;
       $Pruef_Zaehler = 0;

       $pruef_Anzahl = false;
       $pruef_Oempreis = false;

       $Pruefe_Oempreis = false;

       //------CHECKE DS - GRUPPE------
       //------------------------------

        for($i=0;$i<count($this->unb_Positionssatz);$i++)
        {

            //echo 'Variable_i:'.$i;

            for($y=0;$y<count($this->unb_Positionssatz[$i]);$y++)
            {
                $Pruefe_Anzahl = false;
                $Pruefe_Oempreis = false;
                $pruef_Oempreis = false;
                $pruef_Anzahl = false;


                if ($i == 0)
                {
                    //------Erste VorgangsNr auslesen
                    if($y == 2)
                    {
                        $this->_VorgangNr = $this->unb_Positionssatz[$i][1];
                        
                    }
                }
                    
                    if ($y == 2)
                    {
                        if($this->unb_Positionssatz[$i][1] == $this->_VorgangNr)
                        {
                            //echo "POSITIONSSATZNR:".$this->unb_Positionssatz[$i][1];
                            //echo "AKTVORGANGNR:".$this->_VorgangNr;
                            //Solange alter Vorgang �berpr�fe diesen ist Gruppe korrekt
                            //Speichere ihn in das bereinigte Array bei Fehlerschreibe in in das Fehlerarray
                            $Pruefe_Anzahl = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][4]);
                            $Pruef_Array_Anzahl[$Pruef_Zaehler] = $Pruefe_Anzahl;
                            var_dump($Pruef_Array_Anzahl);
                            $Pruefe_Oempreis = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][6]);
                            var_dump($Pruefe_Alle_Oempreis);
                            $Pruef_Array_Oempreis[$Pruef_Zaehler] = $Pruefe_Oempreis;

                            //Zaehle zusammenh�ngenge Datens�tze
                            $Pruef_Zaehler++;
                            //var_dump($Pruef_Array_Anzahl);
                            
                            $index = count($this->unb_Positionssatz);
                            $index = $index - 1;

                            if ($i == $index)
                            {
                                //echo "Letztes Element erreicht";

                                //echo "<br>";
                                //echo "Zaehler:".$Pruef_Zaehler;
                                //echo "<br>";

                            $count = 1;
                            $count_Oempreis = 1;

                            for ($j=0;$j<$Pruef_Zaehler;$j++)
                            {
                                //echo "<br>";
                                $Pruef_Array_Anzahl[$j];
                                //echo "<br>";

                                $pruef_Anzahl = false;

                                $pruef_Anzahl = $Pruef_Array_Anzahl[$j];
                                $pruef_Oempreis = $Pruef_Array_Oempreis[$j];

                                //echo "<br>";
                                //echo "Pruefwert_Anzahl:".$pruef_Anzahl;
                                //echo "<br>";

                                //echo "<br>";
                                //echo "Pruefwert_OEMPREIS:".$pruef_Oempreis;
                                //echo "<br>";

                                if($pruef_Anzahl == true)
                                {
                                    //echo "<br>";
                                    //echo "Zaehlerstand:".$count;
                                    //echo "<br>";
                                    //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count++;

                                    if($count == $Pruef_Zaehler)
                                    {
                                        $Pruefe_Alle_Anzahl = true;
                                        //echo "PRUEFE_ALLE".$Pruefe_Alle_Anzahl;
                                    }
                                }

                                if($pruef_Oempreis == true)
                                {
                                    //echo "<br>";
                                    //echo "Zaehlerstand:".$count_Oempreis;
                                    //echo "<br>";
                                    //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count_Oempreis++;

                                    if($count_Oempreis == $Pruef_Zaehler)
                                    {
                                        $Pruefe_Alle_Oempreis = true;
                                        //echo "PRUEFE_ALLE_OEMPREIS".$Pruefe_Alle_Oempreis;
                                    }
                                }
                            }
                                //SCHROTT

                                if ($Pruefe_Alle_Anzahl == true && $Pruefe_Alle_Oempreis == true)
                                {
                                    //------------------------------------------
                                    //Dann kopiere Datensatzgruppe
                                    //------------------------------------------
                                    $f = ($i - $Pruef_Zaehler);
                                    $f++;

                                    //echo "WERT VON F:".$f;
                                    //echo "<br>";

                                    //echo "<br>";
                                    array_splice($this->ber_temp_Positionssatz,0);

                                    for ($r=0;$r<$Pruef_Zaehler;$r++)
                                    {
                                        //echo "Wert von f in letzter Schleife".$f;
                                        //echo "Wert i".$i;
                                        //echo "Wert f".$f;

                                        for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++)
                                        {
                                            //bereinigte Gruppe zwischenspeichern
                                            //-----------------------------------
                                            //echo "Wert r".$r;
                                            $this->ber_temp_Positionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                        }

                                        $f++;
                                    }
                                }
                                    //bereinigte Gruppe an Array ber_Positionssatz anf�gen
                                    //------------------------------------------------------------------------
                                    //echo "<br>";
                                    //var_dump($this->ber_temp_Positionssatz);
                                    //echo "<br>";
                                    $this->copyGruppeToCleanArray();

                                    array_splice($this->ber_temp_Positionssatz,0);
                                    $Pruefe_Anzahl = false;
                                    array_splice($Pruef_Array_Anzahl,0);
                                    //var_dump($Pruef_Array_Anzahl);
                                    $Pruefe_Alle_Anzahl = false;
                                    array_splice($Pruef_Array_Oempreis,0);
                                    //var_dump($Pruefe_Alle_Oempreis);
                                    $Pruefe_Alle_Oempreis = false;
                                    $Pruef_Zaehler = 0;
                                    $count=0;
                                    $count_Oempreis=0;
                                    //------------------------------------------
                        }
                        }
                        else
                        {
                            //Neuer DS
                            //echo "<br>";
                            //echo "Zaehler:".$Pruef_Zaehler;
                            //echo "<br>";

                            $count = 1;
                            $count_Oempreis = 1;

                            for ($j=0;$j<$Pruef_Zaehler;$j++)
                            {
                                //echo "<br>";
                                //echo $Pruef_Array_Anzahl[$j];
                                //echo "<br>";

                                $pruef_Anzahl = false;

                                $pruef_Anzahl = $Pruef_Array_Anzahl[$j];
                                $pruef_Oempreis = $Pruef_Array_Oempreis[$j];

                                //echo "<br>";
                                //echo "Pruefwert_Anzahl:".$pruef_Anzahl;
                                //echo "<br>";

                                //echo "<br>";
                                //echo "Pruefwert_OEMPREIS:".$pruef_Oempreis;
                                //echo "<br>";

                                if($pruef_Anzahl == true)
                                {
                                    //echo "<br>";
                                    //echo "Zaehlerstand:".$count;
                                    //echo "<br>";
                                    //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count++;

                                    if($count == $Pruef_Zaehler)
                                    {
                                        $Pruefe_Alle_Anzahl = true;
                                        //echo "PRUEFE_ALLE".$Pruefe_Alle_Anzahl;
                                    }
                                }

                                if($pruef_Oempreis == true)
                                {
                                    //echo "<br>";
                                    //echo "Zaehlerstand:".$count_Oempreis;
                                    //echo "<br>";
                                    //echo "Pruef_Zaehler:".$Pruef_Zaehler;
                                    $count_Oempreis++;

                                    if($count_Oempreis == $Pruef_Zaehler)
                                    {
                                        $Pruefe_Alle_Oempreis = true;
                                        //echo "PRUEFE_ALLE_OEMPREIS".$Pruefe_Alle_Oempreis;
                                    }
                                }

                                if ($Pruefe_Alle_Anzahl == true && $Pruefe_Alle_Oempreis == true)
                                {
                                    echo "<br>";
                                    echo "DS IST RICHTIG";
                                    echo "<br>";
                                    //------------------------------------------
                                    //Dann kopiere Datensatzgruppe
                                    //------------------------------------------
                                    $f = ($i - $Pruef_Zaehler);
                                    //$f++;
                                    //echo "<br>";

                                    //echo "<br>";
                                    for ($r=0;$r<$Pruef_Zaehler;$r++)
                                    {
                                        //echo "Wert von f".$f;
                                        //echo "Wert i".$i;
                                        //echo "Wert f".$f;

                                        for($z=0;$z<count($this->unb_Positionssatz[$r]);$z++)
                                        {
                                            //bereinigte Gruppe zwischenspeichern
                                            //-----------------------------------
                                            //echo "Wert r".$r;
                                            $this->ber_temp_Positionssatz[$r][$z] = $this->unb_Positionssatz[$f][$z];
                                        }

                                        $f++;
                                    }

                                    //bereinigte Gruppe an Array ber_Positionssatz anf�gen
                                    //------------------------------------------------------------------------
                                    //echo "<br>";
                                    //var_dump($this->ber_temp_Positionssatz);
                                    //echo "<br>";
                                    $this->copyGruppeToCleanArray();
                                    array_splice($this->ber_temp_Positionssatz,0);
                                    $Pruefe_Anzahl = false;
                                    array_splice($Pruef_Array_Anzahl,0);
                                    //var_dump($Pruef_Array_Anzahl);
                                    $Pruefe_Alle_Anzahl = false;
                                    array_splice($Pruef_Array_Oempreis,0);
                                    //var_dump($Pruefe_Alle_Oempreis);
                                    $Pruefe_Alle_Oempreis = false;
                                    $Pruef_Zaehler = 0;
                                    $count=0;
                                    $count_Oempreis=0;
                                    //var_dump($this->unb_Positionssatz);
                                }
                                else
                                {
                                    $control = $j;
                                    $control++;
                                    if($control == $Pruef_Zaehler)
                                    {

                                    if ($Pruefe_Alle_Anzahl == false || $Pruefe_Alle_Oempreis == false)
                                    {
                                        echo "<br>";
                                        echo "Pruefzaehler:".$Pruef_Zaehler;
                                        echo "<br>";

                                        
                                          echo "WERT VON I:".$i;
                                          echo $this->unb_Positionssatz[$i][0];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][1];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][2];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][3];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][4];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][5];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][6];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][7];
                                          echo "<br>";
                                          echo $this->unb_Positionssatz[$i][8];

                                          echo "<br>";
                                          echo "Pruefzaehler-ENDE:".$Pruef_Zaehler;
                                          echo "<br>";

                                          echo "DS FALSCH ABER SOWAS VON FALSCH";
                                        }
                                    }

                                    //---Schreibe falsche DS in FEHLERTABELLE
                                    //------------------------------------------


                                }
                            }

                            $this->_VorgangNr = $this->unb_Positionssatz[$i][1];

                            $i--;

                            //echo "<br>";
                            //echo "Neuer DS";
                            //echo "<br>";
                        }
                    }
            }

        }
    }

    private function copyGruppeToCleanArray()
    {
        //Letzte Stelle ermitteln
        //echo "CHECKCHECK_---------------------------------------------------------------------------------";
        //var_dump($this->ber_temp_Positionssatz);
        //echo "CHECKCHECK_---------------------------------------------------------------------------------";

        for ($i=0;$i<count($this->ber_temp_Positionssatz);$i++)
        {
            $r = count($this->ber_Positionssatz);

            for($y=0;$y<count($this->ber_temp_Positionssatz[$i]);$y++)
            {
                $this->ber_Positionssatz[$r][$y] = $this->ber_temp_Positionssatz[$i][$y];

            }
        }
        //var_dump($this->ber_Positionssatz);
    }



    public function copyGruppeFehlerToCleanArray()
    {
        //Letzte Stelle ermitteln
        //echo "CHECKCHECK_---------------------------------------------------------------------------------";
        //var_dump($this->ber_temp_Positionssatz);
        //echo "CHECKCHECK_---------------------------------------------------------------------------------";

        for ($i=0;$i<count($this->temp_fehlerPositionssatz);$i++)
        {
            $r = count($this->fehlerPositionssatz);

            for($y=0;$y<count($this->temp_fehlerPositionssatz[$i]);$y++)
            {
                $this->fehlerPositionssatz[$r][$y] = $this->temp_fehlerPositionssatz[$i][$y];
            }
        }
    }

    public function showPosFehlerArray()
    {
        echo "FEHLERARRAY*******************************************************<br>";
        var_dump($this->fehlerPositionssatz);
        echo "FEHLERARRAY-------------------------------------------------------<br>";
    }

     public function showCleanPosArray()
    {
        //echo "BEREINIGTES ARRAY--------------------------------------------------------------";
        var_dump($this->ber_Positionssatz);
        //echo "ENDE BEREINIGTES ARRAY---------------------------------------------------------";
    }


    private function copyToCleanKopfArray()
	{
	    for($element=0;$element<count($this->unb_Kopfsatz);$element++)
	    {
	        $this->ber_Kopfsatz[$element] = $this->unb_Kopfsatz[$element];
	    }
	    //var_dump($this->ber_Kopfsatz);
	}

    private function copyToCleanStornoArray()
	{
	    for($element=0;$element<count($this->unb_Stornosatz);$element++)
	    {
	        $this->ber_Stornosatz[$element] = $this->unb_Stornosatz[$element];
	    }
        //var_dump($this->unb_Stornsatz);
	}




	private function safeStornosatz()
	{
	  //Ueberpr�fe ob bereits eine gleiche Vorgangsnummer vorhanden ist!

	    $SQL =  "Select * from FGSTORNODATEN";
	    $SQL .= " WHERE FGS_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
        //echo $SQL;

	    $rsStornoDaten = $this->_DB->RecordSetOeffnen($SQL);
	    if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == '')
	    {
	        echo "<br>";
	        $SQL = "INSERT INTO FGSTORNODATEN (FGS_KENNUNG,FGS_FILIALNR,FGS_VORGANGNR,";
	        $SQL .= "FGS_KFZBEZ,FGS_KFZKENNZ,FGS_KBANR,FGS_FAHRGESTELLNR,FGS_VERSICHERUNG,";
	        $SQL .= "FGS_VERSSCHEINNR,FGS_SB,FGS_MONTAGEDATUM,FGS_ZEITSTEMPEL,FGS_STEUERSATZ,";
            $SQL .= "FGS_AKTIV,FGS_FGK_KEY,FGS_IMP_KEY,FGS_USER,";
            $SQL .= "FGS_USERDAT) VALUES (";
	        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[0],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[1],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[3],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[4],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[5],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[6],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[7],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[8],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->ber_Stornosatz[9],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[10],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[11],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[12],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[13],false);
			$SQL .= ',null';
			$SQL .= ',null';
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

	        //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--
	        //echo $SQL;

	        if($this->_DB->Ausfuehren($SQL)===false)
			{
				//throw new awisException('',,$SQL,2);
			}

	        //echo "<br>";
	        //echo "Kein Vorgang gefunden";
	        array_splice($this->ber_Stornosatz,0);
	        //echo "<br>";
	        var_dump($this->ber_Stornosatz);

	    }
	    else
	    {
	        if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == $this->ber_Stornosatz[2])
	        {
	            //echo "<br>";
	            //echo "VORGANGSNR stimmt �berein";
	            //echo "<br>";

	            //Zeitstempel wandeln wegen der �berpr�fung des Datum's
	            //------------------------------------------------------------------

	            $wand_Datum = strtotime($this->ber_Stornosatz[11]);
	            $wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

	            if($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL') == $wand_Datum)
	            {
	                //Wenn �bereinstimmung
	                //echo "<br>";
	                //echo "Zeitstempel stimmt �berein - kein UPDATE";
	                //echo "<br>";
	            }
	            else
	            {
	               if($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL') <= $this->ber_Stornosatz[11])
	               {
	                    //Alte Daten in HISTORY - Tabelle verschieben
	                    //-------------------------------------------
	                    $SQL = 'INSERT INTO FGSTORNODATEN_HIST SELECT * FROM FGSTORNODATEN WHERE';
	                    $SQL .= ' FGS_VORGANGNR='.$this->ber_Stornosatz[2];
                        $SQL .= 'AND FGS_KEY='.$rsStornoDaten->FeldInhalt('FGS_KEY');

                        echo $SQL;

	                    if($this->_DB->Ausfuehren($SQL)===false)
	                    {
	                        //throw new awisException('',,$SQL,2);
	                    }

                        //--------------------�NDERN--------------------------------
	                    //----------------------------------------------------------
	                    //---------------UPDATE Kopfdatensatz -- DS-----------------
	                    //----------------------------------------------------------
	                    $SQL =  "Select * from FGSTORNODATEN";
	                    $SQL .= " WHERE FGS_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);

	                    $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

	                    //var_dump($rsKopfDatenUpdate);

	                    $SQL = 'UPDATE FGSTORNODATEN SET ';
	                    $SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[0],false);
	                    $SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[1],false);
	                    $SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);
	                    $SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[3],false);
	                    $SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[4],false);
	                    $SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[5],false);
	                    $SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[6],false);
	                    $SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[7],false);
	                    $SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[8],false);
	                    $SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[9],false);
	                    $SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[10],false);
	                    $SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Stornosatz[11],false);
	                    $SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[12],false);
	                    $SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Stornosatz[13],false);
	                    $SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGS_FGK_KEY'),true);
                        $SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGS_IMP_KEY'),true);
                        $SQL .= ',FGS_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
	                    $SQL .= ',FGS_USERDAT=sysdate';
	                    $SQL .= ' WHERE FGS_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$this->ber_Stornosatz[2],false);

                        if($this->_DB->Ausfuehren($SQL)===false)
	                    {
	                        //throw new awisException('',,$SQL,2);
	                    }

	                    //Selektiere Positionsdaten -> POS_HISTORY
	                    //echo "<br>";
	                    //echo "Zeitstempel unterscheidet sich - Update notwendig";
	                    //echo "<br>";
	               }
	            }
	        }

	    }
	}


private function safeKopfsatz()
	{
	  //Ueberpr�fe ob bereits eine gleiche Vorgangsnummer vorhanden ist!

	    $SQL =  "Select * from FGKOPFDATEN";
	    $SQL .= " WHERE FGK_VORGANGNR=".$this->ber_Kopfsatz[2];


	    $rsKopfDaten = $this->_DB->RecordSetOeffnen($SQL);
	    if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == '')
	    {
	        echo "<br>";
	        $SQL = "INSERT INTO FGKOPFDATEN (FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
	        $SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
	        $SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
	        $SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_FGN_KEY,FGK_FCN_KEY,FGK_USER,FGK_USERDAT) VALUES (";
	        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[0],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kopfsatz[1],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[3],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[4],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[5],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[6],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[7],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[8],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('N',$this->ber_Kopfsatz[9],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('D',$this->ber_Kopfsatz[10],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[12],false);
			$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[13],false);
			$SQL .= ',null';
			$SQL .= ',null';
			$SQL .= ',null';
			$SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

	        //--Funktionsaufruf - Lade die dazugeh�rigen Positionsdaten--
	        //echo $SQL;

	        if($this->_DB->Ausfuehren($SQL)===false)
			{
				//throw new awisException('',,$SQL,2);
			}

	        //echo "<br>";
	        //echo "Kein Vorgang gefunden";
	        array_splice($this->ber_Kopfsatz,0);
	        //echo "<br>";
	        var_dump($this->ber_Kopfsatz);

	    }
	    else
	    {
	        if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == $this->ber_Kopfsatz[2])
	        {
	            echo "<br>";
	            echo "VORGANGSNR stimmt �berein";
	            echo "<br>";

	            //Zeitstempel wandeln wegen der �berpr�fung des Datum's
	            //------------------------------------------------------------------

	            $wand_Datum = strtotime($this->ber_Kopfsatz[11]);
	            $wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

	            if($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL') == $wand_Datum)
	            {
	                //Wenn �bereinstimmung
	                echo "<br>";
	                echo "Zeitstempel stimmt �berein - kein UPDATE";
	                echo "<br>";
	            }
	            else
	            {
	               if($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL') <= $this->ber_Kopfsatz[11])
	               {
	                    //Alte Daten in HISTORY - Tabelle verschieben
	                    //-------------------------------------------
	                    $SQL = 'INSERT INTO FGKOPFDATEN_HIST SELECT * FROM FGKOPFDATEN WHERE';
	                    $SQL .= 'FGK_VORGANGNR='.$this->ber_Kopfsatz[2];
                        //$SQL .= 'AND FGK_KEY='.$rsKopf->FeldInhalt('FGK_KEY');

	                    if($this->_DB->Ausfuehren($SQL)===false)
	                    {
	                        //throw new awisException('',,$SQL,2);
	                    }

                        //--------------------�NDERN--------------------------------
	                    //----------------------------------------------------------
	                    //---------------UPDATE Kopfdatensatz -- DS-----------------
	                    //----------------------------------------------------------
	                    $SQL =  "Select * from FGKOPFDATEN";
	                    $SQL .= " WHERE FGK_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);

	                    $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

	                    //var_dump($rsKopfDatenUpdate);

	                    $SQL = 'UPDATE FGKOPFDATEN SET ';
	                    $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[0],false);
	                    $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Kopfsatz[1],false);
	                    $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);
	                    $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[3],false);
	                    $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[4],false);
	                    $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[5],false);
	                    $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[6],false);
	                    $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[7],false);
	                    $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[8],false);
	                    $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N',$this->ber_Kopfsatz[9],false);
	                    $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('D',$this->ber_Kopfsatz[10],false);
	                    $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Kopfsatz[11],false);
	                    $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[12],false);
	                    $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[13],false);
	                    $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_IMP_KEY'),true);
                        $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),true);
                        $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_FGN_KEY'),true);
                        $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsKopfDatenUpdate->FeldInhalt('FGK_FCN_KEY'),true);
                        $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T',$rsKopfDatenUpdate->FeldInhalt('FGK_FREIGABEGRUND'),true);
                        $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('D',$rsKopfDatenUpdate->FeldInhalt('FGK_DATUMZUGANG'),true);
                        $SQL .= ',FGK_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
	                    $SQL .= ',FGK_USERDAT=sysdate';
	                    $SQL .= ' WHERE FGK_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$this->ber_Kopfsatz[2],false);

	                    if($this->_DB->Ausfuehren($SQL)===false)
	                    {
	                        //throw new awisException('',,$SQL,2);
	                    }

	                    //Selektiere Positionsdaten -> POS_HISTORY
	                    //echo "<br>";
	                    //echo "Zeitstempel unterscheidet sich - Update notwendig";
	                    //echo "<br>";
	               }
	            }
	        }

	    }
	}


  public function safePossatz()
  {
       //Sp�ter - selektiere nur die DS die noch keinen Status gesetzt haben
       //var_dump($rsKopf);
       $SQL = "Select * from FGKOPFDATEN";
       $rsKopf = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsKopf->EOF())
        {
			$Vorgang_NR = $rsKopf->FeldInhalt('FGK_VORGANGNR');
           	$KOPF_ID = $rsKopf->FeldInhalt('FGK_KEY');
//echo '<br>KOPF:'.$KOPF_ID;
//echo '<br>AnzPosSatz:'.count($this->ber_Positionssatz);
            for ($y=0;$y<count($this->ber_Positionssatz);$y++)
            {
//echo '<br>y:'.$y;
//echo '<br>PosSatz:'.$this->ber_Positionssatz[$y][1];

                if($this->ber_Positionssatz[$y][1] == $Vorgang_NR)
                {
                 	$SQL = "Select * from FGPOSITIONSDATEN WHERE FGP_VORGANGNR=".$this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][1],false);
                 	$rsPos = $this->_DB->RecordSetOeffnen($SQL);
                 	if($rsPos->EOF())
                 	{
                 		echo '<br>Keine Daten gefunden';
                 	}
//echo '<br>rsPOS'.$rsPos->FeldInhalt('FGP_KEY');
//echo '<br>Anz'.$rsPos->AnzahlDatensaetze();
                     //-----------------------------------------------------
                     //----------Wurde kein Datensatz gefunden
                     //-----------------------------------------------------
                     if($rsPos->FeldInhalt('FGP_VORGANGNR') == '')
                     {
                       for($h=0;$h<count($this->ber_Positionssatz);$h++)
                       {
                         if($this->ber_Positionssatz[$h][1] == $Vorgang_NR)
                         {
                             $SQL = "INSERT INTO FGPOSITIONSDATEN (FGP_KENNUNG,FGP_VORGANGNR,";
                             $SQL .= "FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,FGP_OEMPREIS,";
                             $SQL .= "FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,";
                             $SQL .= "FGP_USER,FGP_USERDAT) VALUES (";
                             $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][0],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][1],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][2],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][3],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$h][4],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$h][5],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][6],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$h][7],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$h][8],false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',0,false);
                             $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopf->FeldInhalt('FGK_KEY'),false);
                             $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                             $SQL .= ',SYSDATE';
                             $SQL .= ')';

                             if($this->_DB->Ausfuehren($SQL)===false)
                             {
                             }

                         }
                      }
                      break;
                   }
                   else
                   {
                     while(!$rsPos->EOF())
                     {
                           if(($rsPos->FeldInhalt('FGP_VORGANGNR') == $this->ber_Positionssatz[$y][1]) AND ($rsPos->FeldInhalt('FGP_ARTNR') == $this->ber_Positionssatz[$y][2]))
                           {
                              $wand_Datum = awisFormular::PruefeDatum($this->ber_Positionssatz[$y][8],1,1,1);
                              $wand_Datum = strtotime($this->ber_Positionssatz[$y][8]);
                    	          //$wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

                              if(($rsPos->FeldInhalt('FGP_VORGANGNR') == $this->ber_Positionssatz[$y][1]) AND ($rsPos->FeldInhalt('FGP_ARTNR') == $this->ber_Positionssatz[$y][2]))
                              {
                                    echo "VORGANGNR STIMMT �BEREIN----------------------------------------";
                                    echo "<br>";
                                    echo "<br>Zeitstempel:".$rsPos->FeldInhalt('FGP_ZEITSTEMPEL');
                                    echo "<br>Zeitstempel:".awisFormular::PruefeDatum($rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),1,1,1);
                                    echo "<br>ZETI:".$wand_Datum;
                                    echo "<br>ZETI:".$this->ber_Positionssatz[$y][8];
                                    echo "<br>";

                                    if(awisFormular::PruefeDatum($rsPos->FeldInhalt('FGP_ZEITSTEMPEL'),1,1,1) < $wand_Datum)
                                    {
                                        //echo "UPDATE************************************************************************";

                                     //----------------------------------------------
                                     //Verschiebe DS in die History Tabelle
                                     //----------------------------------------------
                                        $SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST SELECT * FROM FGPOSITIONSDATEN WHERE';
                                        $SQL .= ' FGP_VORGANGNR='.$rsPos->FeldInhalt('FGP_VORGANGNR');
                                        $SQL .= ' AND FGP_KEY = ' . $rsPos->FeldInhalt('FGP_KEY');

                                        //echo $SQL;

                                        if($this->_DB->Ausfuehren($SQL)===false)
                                        {
                                        }
                                     //----------------------------------------------
                                     //F�hre Update aus
                                     //----------------------------------------------
                                        $SQL = 'UPDATE FGPOSITIONSDATEN SET ';
                                        $SQL .= ' FGP_KENNUNG=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][0],false);
                                        $SQL .= ',FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][1],false);
                                        $SQL .= ',FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$y][2],false);
                                        $SQL .= ',FGP_ARTBEZ=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][3],false);
                                        $SQL .= ',FGP_ANZAHL=' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$y][4],false);
                                        $SQL .= ',FGP_EINHEIT=' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][5],false);
                                        $SQL .= ',FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$y][6],false);
                                        $SQL .= ',FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$y][7],false);
                                        $SQL .= ',FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$y][8],false);
                                        $SQL .= ',FGP_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat('N2','0',false);
                                        $SQL .= ',FGP_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO',$rsPos->FeldInhalt('FGP_FGK_KEY'),false);
                                        $SQL .= ',FGP_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                                        $SQL .= ',FGP_USERDAT=sysdate';
                                        $SQL .= ' WHERE FGP_VORGANGNR='. $this->_DB->FeldInhaltFormat('T',$rsPos->FeldInhalt('FGP_VORGANGNR'),false);
                                        $SQL .= ' AND FGP_ARTNR='.$this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$y][2],false);
                                        if($this->_DB->Ausfuehren($SQL)===false)
                                        {
                                        }
                                    }
                                    else
                                    {
//echo '<br>   N�chster Datensatz';
										$rsPos->DSWeiter();
//echo '<br>DS-Nr:'.$rsPos->DSNummer();
                                    }
                               }

                             }

                        if(!$rsPos->EOF())
                        {
                        	$rsPos->DSWeiter();
                        }
//echo '<br>DS-Nr:'.$rsPos->DSNummer();
                      }//Ende While

                    }//ENDE ELSE
                }
                else
                {
//echo '<br>Andere Vorgangsnummer';
                }
             }//Ende FOR
//echo '<br><hr>N�chster Kopffsatz';
          $rsKopf->DSWeiter();
        }

} //Ende der Funktion


public function safeFehlerPositionssatz()
{
    //Schreibe Fehlerhafte Positionssaetze weg!!!

    for ($i=0;$i<count($this->fehlerPositionssatz);$i++)
    {
        for($y=0;$y<count($this->fehlerPositionssatz[$i]);$y++)
        {
                $SQL = "INSERT INTO FGPOSITIONSDATEN (FPF_KENNUNG,FPF_VORGANGNR,";
                $SQL .= "FPF_ARTNR,FPF_ARTBEZ,FPF_ANZAHL,FPF_EINHEIT,FPF_OEMPREIS,";
                $SQL .= "FPF_EKNETTO,FPF_ZEITSTEMPEL,FPF_FREMDWAEHRUNG,FPF_FGK_KEY,";
                $SQL .= "FPF_USER,FPF_USERDAT) VALUES (";
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$i][0],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$i][1],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$i][2],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$i][3],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->ber_Positionssatz[$i][4],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$i][5],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$i][6],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->ber_Positionssatz[$i][7],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->ber_Positionssatz[$i][8],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',0,false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$rsKopf->FeldInhalt('FGK_KEY'),false);
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($this->_DB->Ausfuehren($SQL)===false)
                {
                }
            
                //Schreibe dazugeh�rigen Kopfsatz weg
                $SQL = 'INSERT INTO FGKOPFDATENFEHLER Select * from FGKOPFDATEN WHERE FGK_KENNUNG='. $this->_DB->FeldInhaltFormat('T',$this->ber_Positionssatz[$i][1],false);

                if($this->_DB->Ausfuehren($SQL)===false)
                {
                }
          }
    }




}


public function ImportGlasKopf()
{
    //L�sche Tabelle FGINZUGANG
    $SQL = "TRUNCATE TABLE FGINZUGANG";

    if($this->_DB->Ausfuehren($SQL)===false)
    {
    }

    if(($fd = fopen($this->pfadGlasKopf.'glas_kopf.res','r')) === false)
	{
        echo $this->error_msg = "Fehler beim �ffnen der Datei";
	}
	else
	{
	    //Anfang Zeilennummer auf 1 setzen
	    //$this->aktZeilennummer = 1;
        $nr = 0;

        while(!feof($fd))
	    {
	      $this->aktZeile = fgets($fd);
         
          $SQL = "INSERT INTO FGINZUGANG (FGI_VORGANGNR,FGI_USER,FGI_USERDAT) VALUES (";
          $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$this->aktZeile,false);
          $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
          $SQL .= ',SYSDATE';
          $SQL .= ')';

          if($this->_DB->Ausfuehren($SQL)===false)
          {
          }


        }
     fclose($fd);
   }
   
}




    public function getAktFile()
	{
	    //Aktuelles Verzeichnis durchlaufen
	    $checkFile = '';
	    //Anz der Dateien im Verzeichnis
	    $element = 0;
	    $FileNR = 1;

	    if ($handle = opendir($this->pfadFile))
	    {
	        while(false !== ($file = readdir($handle)))
	        {
	           $checkFile = '';

	            //echo $file.PHP_EOL;
	            $checkFile = substr($file,0,3);

	            if ($file == '.' || $file == '..')
	            {

	            }
	            else
	            {
	                if($checkFile == 'imp')
	                {
	                    //echo "<br>";
	                    //echo $checkFile.PHP_EOL;
	                    //echo "File wurde bereits importiert".PHP_EOL;
	                }
	                else
	                {
	                    //echo "<br>";
	                    //echo $checkFile.PHP_EOL;
	                    //echo "File wurde noch nicht importiert".PHP_EOL;
	                    //echo "Datei die importiert werden soll: ";

	                    $this->aktFile[$element] = $file;
	                    $element++;
	                    //echo "</br>";
	                }
	                //echo $FileNR++;
	            }

	        }

	        var_dump($this->aktFile);
	    }
	     //-------------------------------------------------------------------------
	     //Sortiere Array - Aufsteigend!
	     //-------------------------------------------------------------------------




	    //Array mit den aktuellen zu importierenden Daten
	    //echo $this->aktFile[0];
	}

	public function openDBConnection()
	{
	    try
	    {
	    	$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
	    	$this->_DB->Oeffnen();
	    	$_AWISBenutzer = awisBenutzer::Init();
	        echo "<link rel=stylesheet type=text/css href=" . $_AWISBenutzer->CSSDatei() .">";
	    }
	    catch (Exception $ex)
	    {
	        die($ex->getMessage());
	    }
	}



	public function getLastFileImportedDate()
	{
	    //Tabelle "Importlog" aus den AWIS wird am Donnerstag ge�ndert!!!
	    //holt sich die letzt importierte Datei aus der der Tabelle "Importlog"
	    //bei der das VerarbeitungsFlag "True" war!

	    //$test = '20090408.txt';
	    $test = '20090331.txt';

	    $this->lastFile = substr($test,0,8);
	    echo $this->lastFile;

	    //Aufruf DiffDays
	    //-----------------------------------
	    $this->diffDays();
	}


	private function diffDays()
	{
	    $this->aktDate = date('Ymd');
	    //echo "<br>";
	    //echo $this->aktDate;
	    //echo $lastdayMonth = strtotime("last day month");
	    $this->diffDays = strtotime($this->aktDate) - strtotime($this->lastFile);
	    $this->diffDays = $this->diffDays / 86400;
	    //echo "<br>";
	    $this->diffDays = round($this->diffDays);

	    //Erzeuge Files
	    for ($element = 0;$element < $this->diffDays;$element++)
	    {
	        echo "<br>";
	        $this->lastFile = strtotime($this->lastFile) + 86400;
	        $this->lastFile = date('Ymd',$this->lastFile);
	        echo $this->lastFile;
	        $this->fikFiles[$element] = $this->lastFile;
	    }

	    //Array aufsteigend sortieren
	    //echo "<br>";
	    //var_dump($this->fikFiles);

	    $this->checkImportFiles();

	}


} // Ende der Klasse
?>