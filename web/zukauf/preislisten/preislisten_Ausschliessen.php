<?php
global $ZPL;
global $Aus;
require_once 'preislisten_Ausschliessen_funktionen.inc';

$Aus = new preislisten_Ausschliessen_funktionen();
if(isset($_POST['cmdSpeichern_x'])){
    if(isset($ZPL->Param['ZUVIELEDS']) and $ZPL->Param['ZUVIELEDS'] ==true){ //Mehr Datensätze gefunden als maximal erlaubt sind? Dann update alle die im SQL geliefert wurden
        $ZPL->DBi->BindevariablensatzArray2BindevariablenSatz($ZPL->Param['Bindevariablen'],'DBI');
        $SQL = 'UPDATE ZUKAUFIMPORT SET SPERRE =' . $ZPL->DBi->WertSetzen('DBI','Z',$ZPL->Param['txtchkAlle']) ;
        $SQL .= ' WHERE IMP_KEY in (select IMP_KEY from(' . $ZPL->Param['SQL'] . '))';
        $ZPL->DBi->Ausfuehren($SQL,'',true,$ZPL->DBi->Bindevariablen('DBI'));
    }else{
        foreach ($_POST as $key => $value) {
            $Name = explode('_',$key);
            @$IMP_KEY = $Name[1];
            $Name = $Name[0];
            if ($Name == 'oldSPERRE') {
               if(isset($_POST['txt'.substr($Name,3).'_'.$IMP_KEY])){
                    $Wert = 1;
                }else{
                   $Wert = 0;
               }
                $SQL = 'UPDATE ZUKAUFIMPORT SET SPERRE =' . $ZPL->DBi->WertSetzen('DBI','Z',$Wert) ;
                $SQL .= ' where IMP_KEY = ' . $ZPL->DBi->WertSetzen('DBI','Z',$IMP_KEY) ;

                $ZPL->DBi->Ausfuehren($SQL,'',true,$ZPL->DBi->Bindevariablen('DBI'));
            }
        }
    }
}
$Step = $ZPL->Form->NameInArray($_POST, 'cmdStep_', 0, 1);
if ($Step !== false) {
    $Step = explode('_', $Step)[1];
} else {
    $Step = 1;
}

if (file_exists("preislisten_Ausschliessen_Step$Step.php")) {
    include "preislisten_Ausschliessen_Step$Step.php";
} else {
    throw new Exception('Unerwarteter Zugriff.' . serialize($_REQUEST), '-201701171130');
}
die;
?>