<?php
global $AWIS_KEY2;                // UnfallMeldungenInfoTypen: UFT_KEY
global $FehlerAugetreten;        // Flag: Beim Speichern ist ein Fehler aufgetreten (z.B. Pflichtfeld nicht gef�llt)
global $ListeAnzeigen;            // Flag: Liste anzeigen (nicht Detailansicht)
global $Form;
require_once 'awisSeminarplanEinladungsemail.php';
try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $SET_MAIL_KEY = 0;
    $SET_STATUSEMAIL = 0;

    if (isset($_GET['SET_MAIL_KEY'])) {
        $SET_MAIL_KEY = $_GET['SET_MAIL_KEY'];
        $SET_STATUSEMAIL = $_GET['SET_STATUSEMAIL'];
    } elseif (isset($_POST['txtSET_MAIL_KEY'])) {
        $SET_MAIL_KEY = $_POST['txtSET_MAIL_KEY'];
        $SET_STATUSEMAIL = $_POST['txtSET_STATUSEMAIL'];
    }

    $Debug = 'SET_KEY = ' . $SET_MAIL_KEY . '<br> SET_STATUSEMAIL = ' . $SET_STATUSEMAIL;

    $Mail = new awisSeminarplanEinladungsemail($SET_MAIL_KEY);

    $Form->Erstelle_HiddenFeld('SET_MAIL_KEY', $SET_MAIL_KEY);
    $Form->Erstelle_HiddenFeld('SET_STATUSEMAIL', $SET_STATUSEMAIL);

    //Infozeile
    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./seminarplan_Main.php?cmdAktion=Details&SEP_KEY=" . $AWIS_KEY1 . "accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    $Form->InfoZeile($Felder, '');


    //Wenn der Status 0 ist, ist der Job �ber diesen Datensatz noch nicht gelaufen 
    //(h�ndisch zurckgesetzt)
    if ($SET_STATUSEMAIL == 0) {
        $Werkzeuge = new awisWerkzeuge();

        if (isset($_POST['txtSET_EMAIL'])) {
            $Empfaenger = $_POST['txtSET_EMAIL'];
            $EmpfaengerCC = 'academy.einladung@de.atu.eu';
            $Mail->LoescheAdressListe();
            $Mail->Adressliste($Empfaenger, 'TO');
            $Mail->Adressliste($EmpfaengerCC, 'CC');
        } else {
            $Empfaenger = implode(',', $Mail->getAdressliste('TO'));
        }


        $Form->DebugAusgabe(2, $Debug);
        $Edit = true;

        if ($Mail->checkEmail($Empfaenger) == false)//keine echte Email
        {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['SET']['SET_ERR_EMAIL']);
            $Form->ZeileEnde();
            $Fehler = true;
            $MailOK = false;
        } elseif (isset($_POST['txtSET_EMAIL'])) {
            $Mail->Senden();
            $Mail->ProtokolliereEmailversand(awisSeminarplanEinladungsemail::EMAIL_STATUS_MANUELL);
            $DB->Ausfuehren($SQL);

            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['SET']['SET_EMAILOK']);
            $Form->ZeileEnde();
            $MailOK = true;

            $Edit = false;
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_EMAIL'], 90);
        $Form->Erstelle_TextFeld('SET_EMAIL', $Empfaenger, 50, 150, $Edit);
        $Form->ZeileEnde();
    } elseif ($SET_STATUSEMAIL > 0) {//Wenn der Status gr��er als 0 ist, wurde die Mail bereits versendet.
        //Status wieder zur�cksetzen
        $SQL = 'UPDATE ';
        $SQL .= 'seminarteilnehmer ';
        $SQL .= 'SET ';
        $SQL .= 'SET_STATUSEMAIL           = 0,';
        $SQL .= ' SET_EMAILVERSANDZEITPUNKT = null';
        $SQL .= ' WHERE';
        $SQL .= ' set_key = ' . $SET_MAIL_KEY;

        if ($DB->Ausfuehren($SQL) === false) {
            awisErrorMailLink('seminarplan_emailManuell_2', 1, $awisDBError['messages'], '');
        } else {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['SET']['SET_EMAIL_ZURUECKGESETZT']);
            $Form->ZeileEnde();
        }
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
}
?>