<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LZG', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'AktuellesSortiment');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'KeineZuordnungGefunden');
    $TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('AST', 'AST_VK');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Param = $AWISBenutzer->ParameterLesen("Formular_LIELZG");

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht605 = $AWISBenutzer->HatDasRecht(605);
    if (($Recht605 & 128) == 0) {
        awisEreignis(3, 1000, 'LZG', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $DetailAnsicht = false;
    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./lieferanten_loeschen.php');
        if ($AWIS_KEY1 == 0) {
            $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIELZG'));
        } else {
            $Param['Key'] = $AWIS_KEY1;
        }
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./lieferanten_speichern.php');
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIELZG'));
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIELZG'));
    } elseif (isset($_GET['LZG_KEY'])) {
        $AWIS_KEY1 = $Form->Format('N0', $_GET['LZG_KEY']);
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIELZG'));
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIELZG'));

        if (!isset($Param['KEY'])) {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
            $AWISBenutzer->ParameterSchreiben('Formular_LIELZG', serialize($Param));
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['KEY'] = 0;
        }
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY LZG_KEY';
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    $SQL = 'SELECT LIEFERANTENAPZUGRIFFSGRP.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM LIEFERANTENAPZUGRIFFSGRP';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $Form->DebugAusgabe(1, $SQL);
    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
            $AWISBenutzer->ParameterSchreiben('Formular_LIELZG', serialize($Param));
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $rsLZG = $DB->RecordsetOeffnen($SQL);

    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmZukaufartikel action=./lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'') . ' method=POST>';

    if ($rsLZG->EOF() AND !isset($_POST['cmdDSNeu_x']))        // Keine Meldung bei neuen Datens�tzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsLZG->AnzahlDatensaetze() > 1)                        // Liste anzeigen
    {
        $DetailAnsicht = false;
        $Form->Formular_Start();

        $Form->ZeileStart();
        $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP';
        $Link .= '&Sort=LZG_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LZG_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LZG']['LZG_BEZEICHNUNG'], 250, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP';
        $Link .= '&Sort=LZG_BEMERKUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LZG_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LZG']['LZG_BEMERKUNG'], 400, '', $Link);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsLZG->EOF()) {
            $Form->ZeileStart();
            $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&LZG_KEY=0' . $rsLZG->FeldInhalt('LZG_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Form->Erstelle_ListenFeld('LZG_BEZEICHNUNG', $rsLZG->FeldInhalt('LZG_BEZEICHNUNG'), 0, 250, false, ($DS % 2), '', $Link, 'T', 'L',
                $rsLZG->FeldInhalt('ZLH_BEZEICHNUNG'));
            $Form->Erstelle_ListenFeld('LZG_BEMERKUNG', $rsLZG->FeldInhalt('LZG_BEMERKUNG'), 0, 400, false, ($DS % 2));
            $Form->ZeileEnde();

            $rsLZG->DSWeiter();
            $DS++;
        }

        $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $Form->DebugAusgabe(1, $Recht605);
        
        $DetailAnsicht = true;
        $AWIS_KEY1 = $rsLZG->FeldInhalt('LZG_KEY');

        $Param['KEY'] = $AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben('Formular_LIELZG', serialize($Param));

        $Form->Formular_Start();
        $Form->Erstelle_HiddenFeld('LZG_KEY', $rsLZG->FeldInhalt('LZG_KEY'));
        $Form->Erstelle_HiddenFeld('LZG_XBN_KEY', ($rsLZG->FeldInhalt('LZG_XBN_KEY') == ''?$AWISBenutzer->BenutzerID():$rsLZG->FeldInhalt('LZG_XBN_KEY')));

        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLZG->FeldInhalt('LIE_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLZG->FeldInhalt('LIE_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht605 & 128) != 0);

        // Bezeichnung
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LZG']['LZG_BEZEICHNUNG'] . ':', 200);
        $Form->Erstelle_TextFeld('LZG_BEZEICHNUNG', $rsLZG->FeldInhalt('LZG_BEZEICHNUNG'), 55, 370, $EditRecht);
        $AWISCursorPosition = 'txtLZG_BEZEICHNUNG';
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LZG']['LZG_BEMERKUNG'] . ':', 200);
        $Form->Erstelle_TextFeld('LZG_BEMERKUNG', $rsLZG->FeldInhalt('LZG_BEMERKUNG'), 55, 370, $EditRecht);
        $Form->ZeileEnde();

        $Form->Formular_Ende();

        if ($rsLZG->FeldInhalt('LZG_KEY') != '') {
            $Reg = new awisRegister(411);
            $Reg->ZeichneRegister(isset($_GET['Seite'])?$_GET['Seite']:'Mitarbeiter');
        }
    }

    
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../stammdaten_Main.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ((($Recht605 & 128) != 0)) {
        if ($DetailAnsicht) {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
            $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
        }
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    if ($AWISCursorPosition != '') {
        $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
        $Form->SchreibeHTMLCode("document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();");
        $Form->SchreibeHTMLCode('</Script>');
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if ($AWIS_KEY1 != 0) {
        $Bedingung .= ' AND LZG_KEY = ' . floatval($AWIS_KEY1);

        return $Bedingung;
    }

    $Bedingung .= ' AND LZG_XBN_KEY = ' . $AWISBenutzer->BenutzerID();

    $Param['WHERE'] = $Bedingung;

    return $Bedingung;
}

?>