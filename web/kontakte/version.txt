#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fr ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Kontakte
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT,CH

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.01;Erste Version;15.12.2009;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>

