<?php
global $awisRSInfo;
global $awisRSSpalten;
global $awisRSZeilen;
global $AWISBenutzer;

$con = awislogon();

/****************************************************************************
*
*    Abfrage zusammenstellen
*
****************************************************************************/

$SQL = "";
$params = "";			// Abfrageparameter

if(!isset($_POST['drpListe']))
{
	$params = explode(";",awis_BenutzerParameter($con, "ReifenAbsatz-Suche", $AWISBenutzer->BenutzerName()));
}
else
{
	$params = $_POST['drpListe'];
	$params .= ";";		// . $_POST['optLager']; 		// Gibt es nicht mehr
	$params .= ";"; 	// . $_POST['optZeitraum'];	// Gibt es nicht mehr
	$params .= ";" . $_POST['txtKennung'];
	$params .= ";" . $_POST['txtAusgabeListe'];
	$params .= ";" . $_POST['txtGruppen'];
	awis_BenutzerParameterSpeichern($con, "ReifenAbsatz-Suche", $AWISBenutzer->BenutzerName(), $params);
	$params = explode(";",$params);
}

if($params[0]=="Alle")
{
	$SQL = "SELECT DISTINCT ";
}
else 
{
	$SQL = "SELECT ";	
}

// Auswahlliste laden

if($params[4] == 'ATUStandard')
{
	if($_POST['txtGruppen']=='keine')
	{
		$SQL .= '*';
	}
	else
	{
		$SQLFelder = 'ATUNR, SUCH1, GESBESTD, GESBESTD_N, GESBESTD_L';
		$SQL .= $SQLFelder;
	}
}
else
{
	$rsAAF = awisOpenRecordset($con, "SELECT AAF_FELDLISTE FROM AbsatzAuswertungsFelder WHERE AAF_BEZEICHNUNG='" . $params[4] . "' AND AAF_XBN_KEY=0" . awisBenutzerID());
	$SQLFelder = $rsAAF['AAF_FELDLISTE'][0];

		// Bei Gruppierungen muss das Suchfeld vorhanden sein
	if(!isset($_POST['txtGruppen']) or $_POST['txtGruppen']!='keine')
	{
		if($SQLFelder=='')
		{
			$SQLFelder = '*';
		}
		elseIf(strstr($SQLFelder,'SUCH1')===FALSE)
		{
			$SQLFelder = "SUCH1," . $SQLFelder;
		}
	}

	$SQL .= $SQLFelder;
}

$SQL .= " FROM v_Absatz_Saison, Steuerdateien ";
$SQL .= " WHERE STE_ATUNR = ATUNR ";

If($params[0]<>"Alle")
{
    $SQL .= " AND STE_NAME='" . $params[0] . "'";		// Steuerdatei
}

   						// Nicht bei Gruppierungen
//If($params[3]!='' AND $_POST['txtGruppen']=='keine')
{
    If($params[3]!='')		// Alle Kennungen
	{
		if($params[3]=='_')	// Alle leeren Kennungen
		{
			$SQL .= " AND KENNUNG IS NULL";
		}
		else				// Eine Liste mit Kennungen
		{
			$SQL .= " AND (KENNUNG IN ('" . strtoupper(implode("','",explode(",",$params[3]))) . "')";
			If(strstr($params[3],"_")!="")
			{
				$SQL .= " OR KENNUNG IS NULL";
			}
			$SQL .= ")";
		}
	}
}

if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='keine')			// Ohne Gruppierungen
{
	if($params[0]<>"Alle")
	{
		$SQL .= " ORDER BY STE_Key";
	}
	else 
	{
		$SQL .= " ORDER BY ATUNR";
	}
}
else												// Gruppierungen
{
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='osumme3')
	{
		$SQL .= " AND (WG='02' AND SUCH1 LIKE 'W%')";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='osumme2')
	{
		$SQL .= " AND (WG='01' AND SUCH1 LIKE 'S%')";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='offgesamt')
	{
		$SQL .= " AND WG='23'";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='offsommer')
	{
		$SQL .= " AND (WG='23' AND SUCH1 LIKE 'S%')";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='offwinter')
	{
		$SQL .= " AND (WG='23' AND SUCH1 LIKE 'W%')";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='osumme3_offwinter')
	{
		$SQL .= " AND (WG IN ('02','23') AND SUCH1 LIKE 'W%')";
	}
	if(isset($_POST['txtGruppen']) AND $_POST['txtGruppen']=='osumme2_offsommer')
	{
		$SQL .= " AND (WG IN ('01','23') AND SUCH1 LIKE 'S%')";
	}
	
	
	if($SQLFelder!=='*')
	{
		$SQL .= " GROUP BY " .$SQLFelder;
	}
	$SQL .= " ORDER BY SUCH1,ATUNR";
}
awis_debug(1,$SQL);

$rsListe = awisOpenRecordset($con, $SQL);
$rsListeZeilen = $awisRSZeilen;

$FeldListe = array_keys($rsListe);

// Felder f�r Zwischen und Gesamtsummen und das Ausgabeformat
$ZwSumme=array();	// Vorbelegen, falls keine Zahlen ausgew�hlt wurden
$GesSumme=array();
for($i=1;$i<=$awisRSSpalten;$i++)
{
	if($awisRSInfo[$i]['Typ']=='NUMBER')
	{
		$ZwSumme[$awisRSInfo[$i]['Name']] = 0;
		$GesSumme[$awisRSInfo[$i]['Name']] = 0;
	}
}
awis_Debug(1,$SQL);
/*******************************************************
* �berschrift
*******************************************************/

if(isset($_POST['txtFormat']) AND $_POST['txtFormat']=='csv')
{
	$DateiName = awis_UserExportDateiName('.csv');
    $fp = fopen($DateiName,"w+");

	for($i=0;$i<sizeof($FeldListe);$i++)
	{
		$FeldBez[$i] = $FeldListe[$i];
	}
	$FeldBez = awis_FeldNamenListe($con, $FeldBez, $SatzNr = 2);

    for($i=0; $i<count($FeldListe); $i++)
    {
        fputs($fp,$FeldBez[$FeldListe[$i]][0] . ";");
    }
    fputs($fp,chr(13) . chr(10));
}
else
{
    print "<table id=DatenTabelle border=1><tr>";

	for($i=0;$i<sizeof($FeldListe);$i++)
	{
		$FeldBez[$i] = $FeldListe[$i];
	}
	$FeldBez = awis_FeldNamenListe($con, $FeldBez, $SatzNr = 2);
    for($i=0; $i<count($FeldListe); $i++)
    {
        print "<td id=FeldBez>" . $FeldBez[$FeldListe[$i]][0] . "</td>";
    }
    print "</tr>";
}

/******************************************************
* Daten anzeigen
*******************************************************/
//echo $rsListeZeilen;
$LetzteGruppe = '';
for($i=0;$i<=$rsListeZeilen; $i++)
{
    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
	{
		print "<tr>";
	}
			//*****************************
			// Zwischensumme?
			//*****************************
	if((!isset($_POST['txtGruppen']) or $_POST['txtGruppen']!='keine') AND $LetzteGruppe!=$rsListe['SUCH1'][$i])
	{
		if($LetzteGruppe!='')		// Neue Gruppe?
		{
	        for($j=0; $j<count($FeldListe); $j++)
	        {
				if(array_key_exists($FeldListe[$j],$ZwSumme))
				{
				    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
					{
						echo '<td align=right id=TabellenZeileGrau><b>' . $ZwSumme[$FeldListe[$j]] . '</b></td>';
					}
					else
					{
						fputs($fp, "" . $ZwSumme[$FeldListe[$j]] . ';');
					}

					$GesSumme[$FeldListe[$j]]+=$ZwSumme[$FeldListe[$j]];
					$ZwSumme[$FeldListe[$j]]=0;
				}
				else
				{
					if($FeldListe[$j]=="SUCH1")
					{
					    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
						{
							echo '<td id=TabellenZeileGrau><b>Summe</b></td>';
						}
						else
						{
							fputs($fp, "Summe;");
						}
					}
					else
					{
					    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
						{
							echo '<td id=TabellenZeileGrau>&nbsp;</td>';
					}
						else
						{
							fputs($fp, ";");
						}
					}
				}
			}
		    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
			{
				echo '</tr><tr>';
			}
			else
			{
				fputs($fp, chr(13) . chr(10));
			}
		}
		$LetzteGruppe = $rsListe['SUCH1'][$i];

		if($i==$rsListeZeilen)		// Letzte Zeile erreicht: Letzte Zwischensumme und Gesamtsumme
		{
	        for($j=0; $j<count($FeldListe); $j++)
	        {
				if(array_key_exists($FeldListe[$j],$ZwSumme))
				{
				    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
					{
						echo '<td align=right id=TabellenZeileGrau><b>' . $GesSumme[$FeldListe[$j]] . '</b></td>';
					}
					else
					{
						fputs($fp, "" . $GesSumme[$FeldListe[$j]] . ';');
					}
				}
				else
				{
					if($FeldListe[$j]=="SUCH1")
					{
					    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
						{
							echo '<td id=TabellenZeileGrau><b>Gesamtumme</b></td>';
						}
						else
						{
							fputs($fp, "Gesamtsumme;");
						}
					}
					else
					{
					    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
						{
							echo '<td id=TabellenZeileGrau>&nbsp;</td>';
						}
						else
						{
							fputs($fp, ";");
						}
					}
				}
			}
			if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
			{
				print "</tr>";
			}
			else
			{
				fputs($fp, chr(13) . chr(10));
			}
			break;
		}
	}

	// Daten ausgeben
    for($j=0; $j<count($FeldListe); $j++)
    {
		if(array_key_exists($FeldListe[$j],$ZwSumme))		// Zahlenfelder
		{
		    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
			{
		           print "<td align=right id=TabellenZeileWeiss>" . (isset($rsListe[$FeldListe[$j]][$i])?$rsListe[$FeldListe[$j]][$i]:'') . "</td>";
			}
			else
			{
				fputs($fp, $rsListe[$FeldListe[$j]][$i] . ";");
			}
			$ZwSumme[$FeldListe[$j]] += str_replace(',','.',(isset($rsListe[$FeldListe[$j]][$i])?$rsListe[$FeldListe[$j]][$i]:'0'));
		}
		else		// Textfelder
		{
		    if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
			{
		           print "<td id=TabellenZeileWeiss>" . (isset($rsListe[$FeldListe[$j]][$i])?$rsListe[$FeldListe[$j]][$i]:'') . "</td>";
			}
			else
			{
				fputs($fp, (isset($rsListe[$FeldListe[$j]][$i])?$rsListe[$FeldListe[$j]][$i]:'') . ";");
			}
		}
	}
	if(!isset($_POST['txtFormat']) or $_POST['txtFormat']!='csv')
	{
	      print "</tr>";
	}
	else
	{
		fputs($fp, chr(13) . chr(10));
	}
}		// Daten ausgeben

if(isset($_POST['txtFormat']) AND $_POST['txtFormat']=='csv')
{
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . " onMouseover=\"window.status='Rechtsklick zum Speichern der Datei.';return true;\" onMouseOut=\"window.status='AWIS';return true;\">Datei �ffnen</a>";

	echo "<br><br><a href=./absatz_main.php?cmdAktion=Liste onMouseover=\"window.status='Daten neu abfragen, und im Browser anzeigen.';return true;\" onMouseOut=\"window.status='AWIS';return true;\">Daten im Browser anzeigen.</a>";

    fclose($fp);
}
else
{
    print "</table>";
}

awislogoff($con);
?>
