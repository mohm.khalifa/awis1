<?php
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con,2001);
if($RechteStufe==0)
{
	awisLogoff($con);
	die();
}

$comcon = awisLogonComCluster('DE',false);

if(isset($_POST['cmdSave_x']))
{
	$DSListe = explode(";",awis_NameInArray($_POST,'txtPFP_WERT',1));

	foreach($DSListe as $DSZeile)
	{
		$FeldDS = substr($DSZeile,12);
		$Felder = '';
		$Fehler='';
		$FeldListe = array('PFP_WERT','PFP_BEMERKUNG');

		$rsPFP = awisOpenRecordset($comcon,'SELECT * FROM EXPERIAN_ATU.PLP_PREISLISTENPARAMETER WHERE PFP_KEY=0'.$FeldDS);
		foreach($FeldListe as $Feld)
		{
			//$FeldName = substr(awis_NameInArray($_POST,'txt'.$Feld.'_'.$FeldDS,0),3);

			if($_POST['txt'.$Feld.'_'.$FeldDS] != $_POST['old'.$Feld.'_'.$FeldDS])
			{
				if($_POST['old'.$Feld.'_'.$FeldDS] != $rsPFP[$Feld][0])
				{
					$Fehler  .= '<br>' . substr($Feld,3) . ' von \'<i>' . $_POST['old'.$Feld.'_'.$FeldDS] . '</i>\' auf \'<i>' . $rsPFP[''.$Feld.''][0] . '</i>\'';
				}
				else
				{
					$Felder .= ", ".$Feld."='" . $_POST['txt'.$Feld.'_'.$FeldDS] . "'";
				}
			}
		}

		if($Felder!='' and $Fehler=='')
		{
			$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPARAMETER SET ' . substr($Felder,1);
			$SQL .= ', PFP_User=\'' . $AWISBenutzer->BenutzerName() . '\', PFP_USERDAT=SYSDATE';
			$SQL .= ' WHERE PFP_KEY = 0'.$FeldDS;

			if(!awisExecute($comcon,$SQL))
			{
				awisErrorMailLink('17081200-helpdesk_Benutzer.php', 2, $awisDBFehler['message']);
			}
		}
	}
}


//********************************************************************
// Daten anzeigen
//********************************************************************

echo '<form name=frmParameter action=./preislisten_Main.php?cmdAktion=Parameter method=POST>';

$SQL = 'SELECT ID_FPLB,BEZEICHNUNG';
$SQL .= ' FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB';
$SQL .= ' WHERE STATUS=1 ORDER BY ID_FPLB';

$rsEPT = awisOpenRecordset($comcon,$SQL);
$rsEPTZeilen = $awisRSZeilen;

//awis_Debug(1,$SQL,$rsEPT);

echo '<h2>Preislisten Parameter</h2>';
echo '<table border=0 >';

for($EPTZeile=0;$EPTZeile<$rsEPTZeilen;$EPTZeile++)
{

	echo '<tr><td colspan=3 class=FeldBez>' . $rsEPT['ID_FPLB'][$EPTZeile] . ' - ' . $rsEPT['BEZEICHNUNG'][$EPTZeile] . '</td></tr>';

	$SQL = 'SELECT * ';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPARAMETER';
	$SQL .= ' WHERE PFP_ID_FPLB=0' . $rsEPT['ID_FPLB'][$EPTZeile];

	$rsPLP = awisOpenRecordset($comcon,$SQL);
	$rsPLPZeilen = $awisRSZeilen;

	for($PFPZeile=0;$PFPZeile<$rsPLPZeilen;$PFPZeile++)
	{
		echo '<tr>';
		echo '<td>'.$rsPLP['PFP_PARAMETER'][$PFPZeile].':&nbsp;</td>';
		echo '<td>'.(($RechteStufe&2)==0?$rsPLP['PFP_WERT'][$PFPZeile]:'<input type=text size=30 name=txtPFP_WERT_' . $rsPLP['PFP_KEY'][$PFPZeile] . ' value=\'' . $rsPLP['PFP_WERT'][$PFPZeile] . '\'><input type=hidden size=80 name=oldPFP_WERT_' . $rsPLP['PFP_KEY'][$PFPZeile] . ' value=\'' . $rsPLP['PFP_WERT'][$PFPZeile] . '\'>');
		echo '<td>'.(($RechteStufe&2)==0?$rsPLP['PFP_BEMERKUNG'][$PFPZeile]:'<input type=text size=50 name=txtPFP_BEMERKUNG_' . $rsPLP['PFP_KEY'][$PFPZeile] . ' value=\'' . $rsPLP['PFP_BEMERKUNG'][$PFPZeile] . '\'><input type=hidden size=80 name=oldPFP_BEMERKUNG_' . $rsPLP['PFP_KEY'][$PFPZeile] . ' value=\'' . $rsPLP['PFP_BEMERKUNG'][$PFPZeile] . '\'>');
		echo '</tr>';
	}

	echo '<tr><td colspan=3 height=4 bgcolor=#AAAABB></td></tr>';

}
echo '</table>';



echo "<hr>&nbsp;<input type=image accesskey=s title='Parameter speichern (Alt+s)' src=/bilder/diskette.png name=cmdSave>";


echo '</form>';


awisLogoff($comcon);

?>