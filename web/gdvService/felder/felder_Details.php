<?php
require_once 'felder_funktionen.php';
/**
 * Details Stammdatenpflege GDV-felder
 *
 * @author    Patrick Gebhardt
 * @copyright ATU Auto Teile Unger
 * @version   201607131457
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;

try {
    $script = "<script type='text/javascript'>
	function pflichtAus() {
		document.getElementsByName('txtGDN_NACHRICHTENTYPEN')[0].required = false;  // required aus 	
		document.getElementsByName('txtGDN_SATZARTEN')[0].required = false;  // required aus 	
		document.getElementsByName('txtGDF_KEY')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_INDEX')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_FELDTYP')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_FELDTYPWERT')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_FELDTYPWERT2')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_FELDTYPWERT3')[0].required = false;  // required aus 
		document.getElementsByName('txtGDV_FELDTYPWERT4')[0].required = false;  // required aus 
	}
	</script>";

    echo $script;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('GDV', '%');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Liste', 'GDV_FELDTYPEN');
    $TextKonserven[] = array('Liste', 'GDV_INVOICE');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht44003 = $AWISBenutzer->HatDasRecht(44004);
    $Param = array();
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_FE'));
    $Fehler = false;

    $FelderNew = $Form->NameInArray($_POST, 'ico_add', 1, 1);
    $FelderDel = $Form->NameInArray($_POST, 'ico_delete', 1, 1);

    if (!isset($Param['ORDER']) or $Param['ORDER'] == '') {
        $Param['ORDER'] = 'VVE_BEZEICHNUNG';
    }
    if (isset($_GET['Sort']) and $_GET['Sort'] != '') {        // wenn GET-Sort, dann nach diesen Feld sortieren
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    }
    if (isset($_REQUEST['Block']) and $_REQUEST['Block'] != '' and !isset($_REQUEST['txtListe'])) {
        $Param['BLOCK'] = $_REQUEST['Block'];
    }
    if ($Recht44003 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (isset($_GET['VVW_VERSNR'])) {
        $AWIS_KEY1 = $_GET['VVW_VERSNR'];
    } elseif (isset($_POST['txtVVW_VERSNR'])) {
        $AWIS_KEY1 = $_POST['txtVVW_VERSNR'];
    }
    else {
        $AWIS_KEY1 = '';
    }

    if (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = '-1';
    }
    if (isset($_POST['cmdSuche_x'])) {
        $Param['VMV_VERSICHERUNG'] = $Form->Format('N0', $_POST['sucVMV_VERSICHERUNG'], true);
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
        $Param['BLOCK'] = 1;
    }
    if ($FelderNew != '') {
        include('./felder_speichern.php');
    }
    if ($FelderDel != '' or isset($_POST['cmdLoeschenOK']) or isset($_POST['cmdLoeschen_x'])) {
        include('./felder_loeschen.php');
    }

    /* Daten beschaffen */
    $SQL = 'SELECT';
    $SQL .= '   VVE_VERSNR,VVE_BEZEICHNUNG,VVW_ZUSATZWEG,VVW_RELEASE,VVW_USER,VVW_USERDAT, ';
    $SQL .= '   row_number() OVER (';
    $SQL .= ' order by ' . $Param['ORDER'] . ' )  AS ZeilenNr';
    $SQL .= ' FROM';
    $SQL .= '   (';
    $SQL .= 'SELECT ';
    $SQL .= 'VVE_VERSNR,VVE_BEZEICHNUNG,VVW_ZUSATZWEG,VVW_RELEASE,VVW_USER,VVW_USERDAT ';
    $SQL .= ' FROM ';
    $SQL .= 'versversandweg ';
    $SQL .= ' inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';
    $SQL .= ' where VVW_RELEASE is not null';
    $SQL .= '   )daten';

    if ((isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '') and !isset($_POST['cmdSpeichern_x']) and !isset($_POST['cmdLoeschenOK']) and !isset($_POST['cmdLoeschen_x'])) {
        $Bedingung = '';
        $Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }
    }

    if ($AWIS_KEY1 != '' and !isset($_POST['cmdSpeichern_x'])) {
        if (!isset($_POST['cmdSuche_x'])) {
            $SQL .= ' WHERE VVE_VERSNR=' . $AWIS_KEY1;
        } else {
            $SQL .= ' AND VVE_VERSNR=' . $AWIS_KEY1;
        }
    }

    if (isset($_POST['sucVNT_VERSICHERUNG']) and isset($_POST['cmdSuche_x'])) {
        $AWIS_KEY1 = $_POST['sucVNT_VERSICHERUNG'];
    }
    $MaxDS = 1;
    $ZeilenProSeite = 1;
    $Block = 1;
    // Zum Blättern in den Daten
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    } else {
        $Block = $Param['BLOCK'];
    }
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    //AWIS_KEY1 ist leer, wenn man über die Suche kommt. Wenn gespeichert wird(AWIS_KEY1 ist dann nicht leer), kommt man wieder in die Liste, deswegen auch die Blockparameter hernehmen.
    if ($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x']))) {
        $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
    }
    $Form->DebugAusgabe(1, $SQL);
    $rsVVW = $DB->RecordSetOeffnen($SQL);
    // Spaltenbreiten für Listenansicht
    $FeldBreiten = array();
    $FeldBreiten['BES_AUFTRAGSART'] = 500;
    $FeldBreiten['BES_VERALTET'] = 20;

    $count = 0;
    $Gesamtbreite = '';
    foreach ($FeldBreiten as $value) {
        $Gesamtbreite += $value;
        $count++;
    }
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false) {
        $Gesamtbreite += ($count * 4);
    }
    $Gesamtbreite += 20;

    $Form->SchreibeHTMLCode('<form name="frmVVWDetails" action="./felder_Main.php?cmdAktion=Details' . ' " method=POST  enctype="multipart/form-data">');
    $Form->Formular_Start();

    $AnzVers = $rsVVW->AnzahlDatensaetze();

    if (($AnzVers > 1)) //Versicherungsliste
    {
        $FeldBreiten = array();
        $FeldBreiten['Blank'] = 38;
        $FeldBreiten['VERSICHERUNG'] = 350;
        $count = 0;
        $Gesamtbreite = '';
        foreach ($FeldBreiten as $value) {
            $Gesamtbreite += $value;
            $count++;
        }

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');

        $Link = '';

        $Link = './felder_Main.php?cmdAktion=Details&Sort=VVE_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVE_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'], $FeldBreiten['VERSICHERUNG'], '', $Link);
        $Form->ZeileEnde();
        $DS = 0;
        while (!$rsVVW->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            $Icons[] = array('edit', './felder_Main.php?cmdAktion=Details&VVW_VERSNR=' . $rsVVW->FeldInhalt('VVE_VERSNR'));
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));
            $TTT = $rsVVW->FeldInhalt('VVE_BEZEICHNUNG');
            $Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVVW->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VERSICHERUNG'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
            $Form->ZeileEnde();
            $DS++;
            $rsVVW->DSWeiter();
        }

        $DSGesvorAkt = ($Block - 1) * $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['SummeAnzeigeDS'] . ' / ' . $AWISSprachKonserven['GDV']['SummeGesamtDS'] . ': ' . $Form->Format('N0',
                $rsVVW->AnzahlDatensaetze() == 0?$DSGesvorAkt:$DSGesvorAkt + 1) . ' - ' . $Form->Format('N0', $DSGesvorAkt + $DS) . ' / ' . $Form->Format('N0', $MaxDS),
            $Gesamtbreite, 'font-weight:bolder;');
        $Link = './felder_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        $Form->ZeileEnde();
    } elseif ($AnzVers = 1) { //Versicherungsdetailansicht

        //Ausgew�hlte Versicherung merken.

        //Felder SQL
        $SQL2  = 'SELECT VSF_KEY ,';
        $SQL2 .= '   VSF_VERSNR ,';
        $SQL2 .= '   VSF_VERSWELTFELD ,';
        $SQL2 .= '   VSF_GDVPFAD ,';
        $SQL2 .= '   VSF_KONSTANT ,';
        $SQL2 .= '   VSF_FORMAT ,';
        $SQL2 .= '   VSF_USER ,';
        $SQL2 .= '   VSF_USERDAT ,';
        $SQL2 .= '   VSF_INDEX ,';
        $SQL2 .='   row_number() OVER (';
        $SQL2 .= ' order by 1 )  AS ZeilenNr';
        $SQL2 .= ' FROM VERSFELDER';
        $SQL2 .= ' WHERE VSF_VERSNR = ' . $DB->WertSetzen('VSF', 'N0', $rsVVW->FeldInhalt('VVE_VERSNR'));


        if(isset($_REQUEST['Block'])) {
            $Block2 = $_REQUEST['Block'];
        }
        else {
            $Block2 = 1;
        }
        $StartZeile = (($Block2 - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL2,$DB->Bindevariablen('VSF', false));

        $SQL2 = "SELECT * FROM ($SQL2) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);

        $rsVSF = $DB->RecordSetOeffnen($SQL2, $DB->Bindevariablen('VSF', false));
        $Form->DebugAusgabe(1, 'SQL FelderDetails: ' . $DB->LetzterSQL());

        $AWIS_KEY1 = $rsVVW->FeldInhalt('VVE_VERSNR');

        $FeldBreiten = array();
        $FeldBreiten['Blank'] = 20;
        $FeldBreiten['Versweltfeld'] = 350;
        $FeldBreiten['GDVPfad'] = 350;
        $FeldBreiten['Konstant'] = 350;
        $FeldBreiten['Format'] = 100;
        $FeldBreiten['Index'] = 60;

        $Gesamtbreite = '';
        foreach ($FeldBreiten as $value) {
            $Gesamtbreite += $value;
            $count++;
        }

        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./felder_Main.php?cmdAktion=Details  accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVVW->FeldInhalt('VVW_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVVW->FeldInhalt('VVW_USERDAT'));
        $Form->InfoZeile($Felder, '');


        $Form->ZeileStart();
        $Form->Trennzeile('O');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
        $Form->Erstelle_SelectFeld('!VVW_VERSICHERUNG',$rsVVW->FeldInhalt('VVE_VERSNR'),400,false,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Trennzeile('O');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VSF_VERSWELTFELD'], $FeldBreiten['Versweltfeld'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VSF_GDVPFAD'], $FeldBreiten['GDVPfad'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VSF_KONSTANT'], $FeldBreiten['Konstant'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VSF_FORMAT'], $FeldBreiten['Format'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VSF_INDEX'], $FeldBreiten['Index'], '', '');
        $Form->ZeileEnde();
        $DS = 0;    // f�r Hintergrundfarbumschaltung

        while (!$rsVSF->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            $Icons[] = array('delete', 'POST~' . $rsVSF->FeldInhalt('VSF_KEY'));
            $Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2), '', '', ' onclick = "pflichtAus();"');
            $TTT = $rsVSF->FeldInhalt('VSF_VERSWELTFELD');
            $Form->Erstelle_ListenFeld('VSF_VERSWELTFELD',
                $rsVSF->FeldInhalt('VSF_VERSWELTFELD'), 0, $FeldBreiten['Versweltfeld'], false,
                ($DS % 2), '', '', 'T', 'L', $TTT);
            $TTT = $rsVSF->FeldInhalt('VSF_GDVPFAD');
            $Form->Erstelle_ListenFeld('VSF_GDVPFAD', $rsVSF->FeldInhalt('VSF_GDVPFAD'), 0, $FeldBreiten['GDVPfad'],
                false, ($DS % 2), '', '', 'T', 'L', $TTT);
            $TTT = $rsVSF->FeldInhalt('VSF_KONSTANT');
            $Form->Erstelle_ListenFeld('VSF_KONSTANT', $rsVSF->FeldInhalt('VSF_KONSTANT'), 0, $FeldBreiten['Konstant'],
                false, ($DS % 2), '', '', 'T', 'L', $TTT);
            $TTT = $rsVSF->FeldInhalt('VSF_FORMAT');
            $Form->Erstelle_ListenFeld('VSF_FORMAT', $rsVSF->FeldInhalt('VSF_FORMAT'), 0, $FeldBreiten['Format'],
                false, ($DS % 2), '', '', 'T', 'L', $TTT);
            $TTT = $rsVSF->FeldInhalt('VSF_INDEX');
            $Form->Erstelle_ListenFeld('VSF_INDEX', $rsVSF->FeldInhalt('VSF_INDEX'), 0, $FeldBreiten['Index'],
                false, ($DS % 2), '', '', 'T', 'L', $TTT);
            $Form->ZeileEnde();
            $DS++;
            $rsVSF->DSWeiter();
        }

        $DSGesvorAkt = ($Block2 - 1) * $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['SummeAnzeigeDS'] . ' / ' . $AWISSprachKonserven['GDV']['SummeGesamtDS'] . ': ' . $Form->Format('N0',
                $rsVSF->AnzahlDatensaetze() == 0?$DSGesvorAkt:$DSGesvorAkt + 1) . ' - ' . $Form->Format('N0', $DSGesvorAkt + $DS) . ' / ' . $Form->Format('N0', $MaxDS),
            $Gesamtbreite, 'font-weight:bolder;');
        $Link = './felder_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsVVW->FeldInhalt('VVE_VERSNR').'&txtListe=2';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link,$Block2, '');
        $Form->ZeileEnde();

        $Form->Erstelle_HiddenFeld('VVW_VERSNR',$rsVVW->FeldInhalt('VVE_VERSNR'));
        $Form->Erstelle_HiddenFeld('Liste',2);

            $Felder = new felder_funktionen();
            $Form->ZeileStart();
            $Form->Trennzeile('L');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDV_PFAD'], 200, 'font-size:15px; font-weight: bolder');
            $Form->ZeileEnde();

            $SQLNachrichtentypen = $Felder->holeFelderDetailDaten($Felder::TYP_NACHRICHTENTYP, $Felder::RETURNTYP_SQL);
            $Wert = '';
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDN_BEZEICHNUNG'] . ':', 180);
            $Form->Erstelle_SelectFeld('!GDN_NACHRICHTENTYPEN',$Wert, '320:320', true, $SQLNachrichtentypen, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '',
                '', '', $DB->Bindevariablen('VVW', true), 'font-size:14px;height:22px');

            $Form->ZeileEnde();


            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'Satzarten_box');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDS_SATZART'] . ':', 160);
            $Form->Erstelle_SelectFeld('GDN_SATZARTEN',1000, '350:320', true,
                '***VSF_Daten;txtGDN_NACHRICHTENTYPEN;txtGDN_NACHRICHTENTYPEN=*txtGDN_NACHRICHTENTYPEN&TYP=Satzarten&ZUSATZ=~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] . '&VVE_VERSNR=' . $AWIS_KEY1,
                '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','' , '', '', 'none;');
            $Form->ZeileEnde();

            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'Felder_box');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VMV_FELD'] . ':', 160);
            $Form->Erstelle_SelectFeld('GDF_KEY', '', '350:320', true,
                '***VSF_Daten;txtGDN_NACHRICHTENTYPEN;txtGDN_NACHRICHTENTYPEN =*txtGDN_NACHRICHTENTYPEN&TYP=Felder&txtGDN_SATZARTEN=*txtGDN_SATZARTEN&ZUSATZ=~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] . '&VVE_VERSNR=' . $AWIS_KEY1,
                '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', 'none;');
            $Form->ZeileEnde();

            $Form->Erstelle_HiddenFeld('RELEASE',$rsVVW->FeldInhalt('VVW_RELEASE'));

            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'Feldpfad_box');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDV_FELDPFAD']. ':', 180);
            $Form->Erstelle_TextFeld('VSF_GDVPFAD', $Wert, 50, 318, true, '', '', '', 'T', 'L', '', '', '', '', '', 'disabled');
            $Form->Erstelle_HiddenFeld('GDVPFAD','');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Trennzeile('O');
            $Form->ZeileEnde();

            //Feldoptionen:
            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'Feldoptionen_box');
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDV_WERT'], 200, 'font-size:15px; font-weight: bolder');
            $Form->ZeileEnde();

            $Wert = '';
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VSF_FORMAT'] . ':', 180);
            $Form->Erstelle_TextFeld('GDV_FORMAT', $Wert, 160,321, true);
            $Form->ZeileEnde();

            $Wert = '0';
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Fehler != false ? '!':''.$AWISSprachKonserven['GDV']['VSF_INDEX'] . ':', 180);
            $Form->Erstelle_TextFeld('!GDV_INDEX',$Wert, 160,321, true);
            $Form->ZeileEnde();

            $Wert = '';
            $Liste = explode('|', $AWISSprachKonserven['Liste']['GDV_FELDTYPEN']);
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDV_FELDTYP']. ':', 180);
            $Form->Erstelle_SelectFeld('GDV_FELDTYP', $Wert, '320:320', true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Liste, '', '',
                $DB->Bindevariablen('VVW', true), 'font-size:14px;height:22px');
            $Form->ZeileEnde();
            $Form->ZeileEnde();


            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'FeldTypWert_box');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDV_FELDTYPWERT'] . ':', 180);
            $Form->Erstelle_TextFeld('GDV_FELDTYPWERT', $Wert, 160, 316, true, '', '', '', 'T', 'L', '', '', '', '', '','disabled');
            $Form->Erstelle_HiddenFeld('FELDTYPWERT','');
            $Form->Schaltflaeche($Form::BILD_TYP_SCRIPT, 'cmdFELDTYPWERT', ' ', '/bilder/cmd_export_xls.png', '', '', '', '20', 20);

            $Icons = array();
            $Icons[] = array('add', 'POST~' . $AWIS_KEY1);
            $Form->Erstelle_ListeIcons($Icons,20,-1,'','','id="FeldSubmit"');

            echo '<div id="FeldTypWertOptionen" style="display:none; position:absolute; border:5px outset #81bef7;border-radius:5px; z-index:3; 
            background-color:#81bef7; top:10px; left:50px; width:' . 700 . 'px;">';
            echo '</div>';
            $Form->ZeileEnde();
            $Wert = '';
            $Form->ZeileStart($Felder->setzeDisplay($Wert), 'FeldTypWert_box2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VSF_KONSTANT'] . ':', 180);
            $Form->Erstelle_TextFeld('GDV_FELDTYPWERT2', $Wert, 160,321, true);
            $Icons = array();
            $Icons[] = array('add', 'POST~' . $AWIS_KEY1);
        $Form->Erstelle_ListeIcons($Icons,20,-1,'','','id="FeldSubmit2"');
        $Form->ZeileEnde();


        $Wert = '';
        $Form->ZeileStart($Felder->setzeDisplay($Wert), 'FeldTypWert_box3');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VSF_FUNKTION'] . ':', 180);
        $Form->Erstelle_TextFeld('GDV_FELDTYPWERT3', $Wert, 160,321, true);
        $Icons = array();
        $Icons[] = array('add', 'POST~' . $AWIS_KEY1);
        $Form->Erstelle_ListeIcons($Icons,20,-1,'','','id="FeldSubmit3"');
        $Form->ZeileEnde();


        $Wert = '';
        $Form->ZeileStart($Felder->setzeDisplay($Wert), 'FeldTypWert_box4');
        $Liste2 = explode('|', $AWISSprachKonserven['Liste']['GDV_INVOICE']);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VSF_INVOICE']  . ':', 180);
        $Form->Erstelle_SelectFeld('GDV_FELDTYPWERT4', $Wert, '320:320', true, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Liste2, '', '', $DB->Bindevariablen('VVW', true), 'font-size:14px;height:22px');
        $Icons = array();
        $Icons[] = array('add', 'POST~' . $AWIS_KEY1);
        $Form->Erstelle_ListeIcons($Icons,20,-1,'','','id="FeldSubmit4"');
        $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Trennzeile('O');
            $Form->ZeileEnde();
    }
    else { //Keine Versicherung gefunden
        $Form->ZeileStart();
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
        $Form->ZeileEnde();
    }

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/gdvService/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if ($AWIS_KEY1 == ' - 1')//Speichern wenn und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
    {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode(' </form> ');

    $AWISBenutzer->ParameterSchreiben('Formular_GDV_FE', serialize($Param));
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS - Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {

    if ($Form instanceof awisFormular) {

        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg . Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $DB;
    global $Form;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Bedingung = '';

    if (isset($Param['VMV_VERSICHERUNG']) AND ($Param['VMV_VERSICHERUNG'] != '0' and $Param['VMV_VERSICHERUNG'] != '')) {
        $Bedingung .= ' AND VVE_VERSNR ' . $DB->LikeOderIst($Param['VMV_VERSICHERUNG']);
    }

    return $Bedingung;
}

?>