<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');


	$script = "<script type='text/javascript'>
	function wechsleVertrag(obj) {
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkWert == '1' && checkText != '::bitte w�hlen::') // bitte w�hlen
    	       {
				   document.getElementById('GAS').style.display = 'block';
				   document.getElementById('STROM').style.display = 'none';
				   document.getElementsByName('sucRAV_STROMLIEFERANT')[0].value = ''; 
	           }
    	       else if(checkWert == '0' && checkText != '::bitte w�hlen::') // Datenbank
    	        {
					document.getElementById('GAS').style.display = 'none';
					document.getElementById('STROM').style.display = 'block';
					document.getElementsByName('sucRAV_GASLIEFERANT')[0].value = ''; 
				}
    	       else // Aufgaben
    	        {
					document.getElementById('GAS').style.display = 'none';
					document.getElementById('STROM').style.display = 'none';
					document.getElementsByName('sucRAV_GASLIEFERANT')[0].value = ''; 
					document.getElementsByName('sucRAV_STROMLIEFERANT')[0].value = ''; 
    	        }
	
	}
	</script>";

	echo $script;


	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht630 = $AWISBenutzer->HatDasRecht(630);
	if($Recht630==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSR'));
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./rahmenvertraege_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSART'].':',190);
	$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_VERTRAGSART']);
	$Form->Erstelle_SelectFeld('*RAV_VERTRAGSART',($Param['SPEICHERN']=='on'?$Param['GSV_VERTRAGSART']:''),150,true,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="wechsleVertrag(this);"');
	$AWISCursorPosition='sucRAV_VERTAGSART';
	$Form->ZeileEnde();

	if(($Param['SPEICHERN'] != '' and $Param['GSV_VERTRAGSART'] == '1'))
	{
		$Form->ZeileStart('display:block','GAS"ID="GAS');
	}
	else
	{
		$Form->ZeileStart('display:none','GAS"ID="GAS');
	}

	$SQL =  'select gsl_key,coalesce(gsl_firma,gsl_suchname)';
	$SQL .= ' from gsvlieferanten';
	$SQL .= ' where GSL_LIEFERANTENART in (1,2)';
	$SQL .= ' and GSL_AKTIV = 1';
	$SQL .= ' order by gsl_firma asc';

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GASLIEFERANT'].':',190);
	$Form->Erstelle_SelectFeld('*RAV_GASLIEFERANT',($Param['SPEICHERN']=='on'?$Param['GSV_GASLIEFERANT']:''),150,true,$SQL,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	$Form->ZeileEnde();


	if(($Param['SPEICHERN'] != '' and $Param['GSV_VERTRAGSART'] == '0'))
	{
		$Form->ZeileStart('display:block','STROM"ID="STROM');
	}
	else
	{
		$Form->ZeileStart('display:none','STROM"ID="STROM');
	}

	$SQL =  'select gsl_key,coalesce(gsl_firma,gsl_suchname)';
	$SQL .= ' from gsvlieferanten';
	$SQL .= ' where GSL_LIEFERANTENART in (0,2)';
	$SQL .= ' and GSL_AKTIV = 1';
	$SQL .= ' order by gsl_firma asc';

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STROMLIEFERANT'].':',190);
	$Form->Erstelle_SelectFeld('*RAV_STROMLIEFERANT',($Param['SPEICHERN']=='on'?$Param['GSV_STROMLIEFERANT']:''),150,true,$SQL,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',150);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');

	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201431");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201432");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>