<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PAA','%');
	$TextKonserven[]=array('TTT','PAA_%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('DPR','DPR_BEZEICHNUNG');
	$TextKonserven[]=array('PFG','PFG_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AlleSetzen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8003= $AWISBenutzer->HatDasRecht(8003);		// Herstelle
	if(($Recht8003&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY PAA_SORTIERUNG, PAA_AST_ATUNR DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$SortFeld = str_replace('~',' ',$SortFeld);
			$OrderBy .= ', '.$SortFeld.(substr($_GET['SSort'],-1,1)=='~'?' DESC':'');
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.substr($OrderBy,2));
	}
	$SQL = 'SELECT Preisabfragendatenartikel .*, PFG_BEZEICHNUNG';
	$SQL .= ', COALESCE(AST_BEZEICHNUNGWW,DPR_BEZEICHNUNG) AS AST_BEZEICHNUNGWW';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Preisabfragendatenartikel ';
	$SQL .= ' LEFT OUTER JOIN Preisabfragengruppen ON PAA_PFG_KEY= PFG_KEY';
	$SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = PAA_AST_ATUNR ';
	$SQL .= ' LEFT OUTER JOIN (SELECT DISTINCT DPR_NUMMER, DPR_BEZEICHNUNG FROM Dienstleistungspreise ) DL ON DPR_NUMMER = PAA_AST_ATUNR ';
	$SQL .= ' WHERE PAA_PAP_KEY = 0'.$AWIS_KEY1;
	if(isset($_GET['PAA_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['PAA_KEY'],false);
	}

	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND PAA_KEY = 0'.$AWIS_KEY2;
	}

	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

	$SQL .= $ORDERBY;

	$rsPAA = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['PAA_PFG_KEY']=$AWIS_KEY1;
	$EditModus=(($Recht8003&2)!=0);
	
	
	if(isset($_GET['PAA_KEY']))
	{
		$Form->Formular_Start();
		
		$Form->Erstelle_HiddenFeld('PAA_KEY', $AWIS_KEY2);
		
		$Form->ZeileStart();
					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel&PAAListe accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAA->FeldInhalt('PAA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAA->FeldInhalt('PAA_USERDAT'));
		$Form->InfoZeile($Felder,'');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_AST_ATUNR'].':', 160);
		$Form->Erstelle_TextFeld('PAA_AST_ATUNR', $rsPAA->FeldInhalt('PAA_AST_ATUNR'), 10, 150, $EditModus, 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':', 160);
		$Form->Erstelle_TextFeld('#AST_BEZEICHNUNGWW', $rsPAA->FeldInhalt('AST_BEZEICHNUNGWW'), 0, 350, false);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_GRUPPE'].':', 160,'','',$AWISSprachKonserven['TTT']['PAA_GRUPPE']);
		$Form->Erstelle_TextFeld('PAA_GRUPPE', $rsPAA->FeldInhalt('PAA_GRUPPE'), 10, 150, $EditModus, 'T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_HAUPTGRUPPE'].':', 160,'','',$AWISSprachKonserven['TTT']['PAA_HAUPTGRUPPE']);
		$Form->Erstelle_TextFeld('PAA_HAUPTGRUPPE', $rsPAA->FeldInhalt('PAA_HAUPTGRUPPE'), 10, 150, $EditModus, 'T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_SORTIERUNG'].':', 160,'','',$AWISSprachKonserven['TTT']['PAA_SORTIERUNG']);
		$Form->Erstelle_TextFeld('PAA_SORTIERUNG', $rsPAA->FeldInhalt('PAA_SORTIERUNG'), 10, 150, $EditModus, 'T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_DLNR'].':', 160);
		$Form->Erstelle_TextFeld('PAA_DLNR', $rsPAA->FeldInhalt('PAA_DLNR'), 10, 150, $EditModus, 'T');
		if($rsPAA->FeldInhalt('PAA_KOMPLETTNR')!='')
		{
			$SQL = 'SELECT DPR_BEZEICHNUNG';
			$SQL .= ' FROM Dienstleistungspreise WHERE DPR_NUMMER = :var_ast_atunr';
			$BindeVariablen = array();
			$BindeVariablen['var_ast_atunr']=$rsPAA->FeldInhalt('PAA_KOMPLETTNR');
			$rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['DPR_BEZEICHNUNG'].':', 160);
			$Form->Erstelle_TextFeld('#DPR_BEZEICHNUNG', $rsAST->FeldInhalt('DPR_BEZEICHNUNG'), 0, 350, false);
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_KOMPLETTNR'].':', 160);
		$Form->Erstelle_TextFeld('PAA_KOMPLETTNR', $rsPAA->FeldInhalt('PAA_KOMPLETTNR'), 10, 150, $EditModus, 'T');
		if($rsPAA->FeldInhalt('PAA_KOMPLETTNR')!='')
		{
			$SQL = 'SELECT AST_BEZEICHNUNGWW ';
			$SQL .= ' FROM ARTIKELSTAMM WHERE ast_atunr = :var_ast_atunr';
			$SQL .= ' UNION ';
			$SQL .= 'SELECT DPR_BEZEICHNUNG';
			$SQL .= ' FROM Dienstleistungspreise WHERE DPR_NUMMER = :var_ast_atunr';
			$BindeVariablen = array();
			$BindeVariablen['var_ast_atunr']=$rsPAA->FeldInhalt('PAA_KOMPLETTNR');
			$rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':', 160);
			$Form->Erstelle_TextFeld('#AST_BEZEICHNUNGWW', $rsAST->FeldInhalt('AST_BEZEICHNUNGWW'), 0, 350, false);
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAA']['PAA_PFG_KEY'].':', 160);
		$SQL = 'SELECT PFG_KEY, PFG_BEZEICHNUNG';
		$SQL .= ' FROM Preisabfragengruppen';
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('PAA_PFG_KEY', $rsPAA->FeldInhalt('PAA_PFG_KEY'),'200:190', $EditModus, $SQL);
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
	}
	else	// Liste
	{
		$Form->Formular_Start();
		$DS=0;
		$Form->ZeileStart();
		
		if((intval($Recht8003)&4)!=0)
		{
			$Icons[] = array('new','./preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel&PAA_KEY=-1');
			$Form->Erstelle_ListeIcons($Icons,36,-1);
  		}
		
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PAA_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PAA_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAA']['PAA_AST_ATUNR'],90,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=AST_BEZEICHNUNGWW'.((isset($_GET['SSort']) AND ($_GET['SSort']=='AST_BEZEICHNUNGWW'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],350,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PAA_GRUPPE;PAA_HAUPTGRUPPE;PAA_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PAA_GRUPPE;PAA_HAUPTGRUPPE;PAA_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAA']['PAA_GRUPPE'],100,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PAA_HAUPTGRUPPE;PAA_GRUPPE;PAA_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PAA_HAUPTGRUPPE;PAA_GRUPPE;PAA_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAA']['PAA_HAUPTGRUPPE'],100,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PAA_DLNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PAA_DLNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAA']['PAA_DLNR'],140,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PAA_KOMPLETTNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PAA_KOMPLETTNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAA']['PAA_KOMPLETTNR'],120,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=PFG_BEZEICHNUNG;PAA_HAUPTGRUPPE;PAA_GRUPPE;PAA_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='PFG_BEZEICHNUNG;PAA_HAUPTGRUPPE;PAA_GRUPPE;PAA_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFG']['PFG_BEZEICHNUNG'],230,'',$Link);
		$Form->ZeileEnde();
	
		while(!$rsPAA->EOF())
		{
			$Form->ZeileStart();
			$Icons=array();
			if(intval($Recht8003&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel&PAA_KEY='.$rsPAA->FeldInhalt('PAA_KEY'));
			}
			if(intval($Recht8003&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel&Del='.$rsPAA->FeldInhalt('PAA_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,36,($DS%2));
			
			
			$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel&PAA_KEY='.$rsPAA->FeldInhalt('PAA_KEY');
			$Form->Erstelle_ListenFeld('#PAA_AST_ATUNR',$rsPAA->FeldInhalt('PAA_AST_ATUNR'),10,90,false,($DS%2),'',$Link);
			$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$rsPAA->FeldInhalt('PAA_AST_ATUNR');
			$Form->Erstelle_ListenFeld('#AST_BEZEICHNUNGWW',$rsPAA->FeldInhalt('AST_BEZEICHNUNGWW'),10,350,false,($DS%2),'',$Link);
			$Form->Erstelle_ListenFeld('#PAA_GRUPPE',$rsPAA->FeldInhalt('PAA_GRUPPE'),10,100,false,($DS%2));
			$Form->Erstelle_ListenFeld('#PAA_HAUPTGRUPPE',$rsPAA->FeldInhalt('PAA_HAUPTGRUPPE'),10,100,false,($DS%2));
			$Form->Erstelle_ListenFeld('#PAA_DLNR',$rsPAA->FeldInhalt('PAA_DLNR'),10,140,false,($DS%2));
			$Form->Erstelle_ListenFeld('#PAA_KOMPLETTNR',$rsPAA->FeldInhalt('PAA_KOMPLETTNR'),10,120,false,($DS%2));
			$Form->Erstelle_ListenFeld('#PFG_BEZEICHNUNG',$rsPAA->FeldInhalt('PFG_BEZEICHNUNG'),10,230,false,($DS%2));
			$Form->ZeileEnde();
			$rsPAA->DSWeiter();
			$DS++;
		}
		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete&Seite=Artikel';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	
		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201105311624");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201105311623");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>