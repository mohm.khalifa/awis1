<script language="JavaScript">


//Ermittelt den aktuellen Dateinamen
	
	<?php $strDateiname = str_replace("_haupt.php","",str_replace("/card/rls/","",$PHP_SELF)); ?>
		
	//Liest den Querystring aus dem Hiddenfeld "querystring" in der Navigationsleiste aus
	
	function querystringlesen()
	{
		return parent.<?php echo $strDateiname; ?>_navigation.document.getElementById("querystring").value;
	}
	
	//Erweitert den Querystring des Hiddenfeldes "querystring" in der Navigationsleiste um den String "strErweiterung"
	
	function querystringerweitern(strErweiterung)
	{
		var objQuerystring = parent.<?php echo $strDateiname; ?>_navigation.document.getElementById("querystring");
		
		objQuerystring.value = objQuerystring.value+strErweiterung;
	}
	
	//Verk�rzt den Querystring des Hiddenfeldes "querystring" in der Navigationsleiste um den String "strVerkuerzung"
	
	function querystringverkuerzen(strVerkuerzung)
	{
		var objQuerystring = parent.<?php echo $strDateiname; ?>_navigation.document.getElementById("querystring");
		
		objQuerystring.value = objQuerystring.value.replace(eval("/"+strVerkuerzung+"/"),"");
	}

</script>

<?php

//Standardwert von $intDialog setzen, wenn Variable nicht gesetzt worden ist

if(!isset($intDialog))
	$intDialog = 1;
	
$intAnzDs = anzahlds($strDb,$strSql);		//Anzahl der Datens�tze ermitteln

if($intAnzDs)	//Wenn Datens�tze vorhanden sind
{
	$arrOrderBy = explode("order by",$strSql);
	
	$strOrderBy = "order by ".trim(array_pop($arrOrderBy));
	
	$intEndeZaehler = floor(($intAnzDs-1)/$intScrollanzahl);
	
	if(!isset($intZaehler))		//Wird einmal am Anfang durchlaufen
	{
		if(isset($strBlock)&&$strBlock!='')		//Berechnet den intZaehler (welcher Block), wenn bestimmter Datensatz
		{										//darin enthalten sein soll ($strBlock (Abfragekriterium) muss gesetzt sein!)
			if($strDb=="ora")
			{
				$strSqlBlock = "select xxxrn from (select xxx.*,rownum xxxrn from (".$strSql.") xxx) where ".$strBlock;
				$arrBlock = abfrage("ora",$strSqlBlock);
				$intBlock = $arrBlock["trefferarray"]["XXXRN"][0];
				$intZaehler = floor(($intBlock-1)/$intScrollanzahl);
			}
		}
		else
		{
			$intZaehler=0;
		}
	}
	
	if($intZaehler&&(($intZaehler*$intScrollanzahl)>=$intAnzDs))	//Z�hler anpassen wenn beim L�schen Blockgrenze unterschritten wird
	{
		$intZaehler -= 1;
	}
	
	if($strDb=="ora")
	{
		$strSqlScroll="select * from (select * from (".$strSql.") where ROWNUM<=".($intZaehler+1)*$intScrollanzahl." 
		minus select * from (".$strSql.") where ROWNUM<=".($intZaehler)*$intScrollanzahl.") ".$strOrderBy;
	}
	elseif($strDb=="db2")
	{
		$arrHilf=explode("from",$strSql);
		
		$strSqlScroll="select * from (".trim($arrHilf[0]).",row_number() over() as rn from (".$strSql.")xxx)yyy 
		where rn between ".($intZaehler*$intScrollanzahl+1)." and ".(($intZaehler+1)*$intScrollanzahl);
	}
		
	$intAnfang = $intZaehler*$intScrollanzahl+1;
	
	if($intZaehler==$intEndeZaehler)
		$intEnde = $intAnzDs;
	else
		$intEnde = ($intZaehler+1)*$intScrollanzahl;
	
	//$strPosition: String welcher Datensatzvon - Datensatzbis / Anzahl Datens�tze gesamt entspricht
		
	if($intAnfang == $intEnde)
		$strPosition = $intAnfang."/".$intAnzDs;
	else
		$strPosition = $intAnfang."-".$intEnde."/".$intAnzDs;
	
	?>
	
	
	<script language="JavaScript">	
	
	var aktzaehler = <?php echo $intZaehler; ?>;
	
	function blaettern(richtung)	//Wird bei Klick auf Bl�tterbuttons in Navigationsleiste aufgerufen
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Dialogfenster bl�ttern");	//Zeitmessung
		<?php } ?>
				
		var typ = typeof(richtung);
		if(typ =='number')
		{
			var zaehler = <?php echo $intZaehler; ?>+richtung;
		}
		
		if(zaehler<0)
			zaehler=0;
			
		if(zaehler><?php echo $intEndeZaehler; ?>)
			zaehler=<?php echo $intEndeZaehler; ?>;
		
		if(typ =='string')
		{
			if(richtung=='anfang')
			{
				zaehler = 0;
			}
			if(richtung=='ende')
			{
				zaehler = <?php echo  $intEndeZaehler; ?>;
			}
		}
			
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&intZaehler="+zaehler;
	}
	
	//Datens�tze werden aufgrund der Kundennr sowie des aktuellen Intervallz�hlers erneut eingelesen
		
	function aktualisieren()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Dialogfenster aktualisieren");	//Zeitmessung
		<?php } ?>
			
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&intZaehler=<?php echo $intZaehler; ?>";
	}
	
	//Dialogfenster schlie�en
	
	function fensterzu()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Dialogfenster schlie�en");	//Zeitmessung
		<?php } ?>
				
		parent.self.close();
	}
		
	</script>
<?php
}		//Ende if($intAnzDs)
else
{
	$strPosition="0/0";

	$strSqlScroll=$strSql;
	?>
	<script language="JavaScript">
		function blaettern()
		{
			alert(unescape("Es sind keine Datens%E4tze zum Bl%E4ttern vorhanden!"));
		}
		function aktualisieren()
		{
			blaettern();
		}
		
		//Dialogfenster schlie�en
	
		function fensterzu()
		{
			<?php
			if($intZeitmessung)
			{
				echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Dialogfenster schlie�en");	//Zeitmessung
			<?php } ?>
					
			parent.self.close();
		}
	</script>
	<?php
}
?>
