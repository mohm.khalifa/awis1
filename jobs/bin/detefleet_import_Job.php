<?php

/**
 * Job importiert Daten (von DeTeFleet/ATU-Card) matcht ATUNR zu
 * Materialnummern von DeTeFleet erstellt eine Preisliste und
 * stellt diese dem COM-Server zur Verf�gung.
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * @author 01.05.2011 Thomas Riedl
 * @version 1.0
 * �nderungen:
 * 26.03.2013 SO : Nach Erstellung der Preisliste wird eine Zuordnungs-
 *                 datei fuer EDIPlan exportiert. Inhalt MatNr/ATUNr
 * ***********************************************************************
 */
require_once('detefleet_Import.php');
require_once('detefleet_Extrahierung.php');
require_once('detefleet_Matching.php');
require_once('detefleet_Export.php');
require_once('awisDatenbank.inc');
require_once('detefleet_Auswertung.php');
require_once('detefleet_funktionen.php');

try {

	$Benutzer = '';
	$DebugLevel = 0;
	$zeit = true;

	for ($i = 1; $i < $argc; $i++) {
		switch (strtolower($argv[$i])) {
			case '--benutzer':
				$Benutzer = $argv[++$i];
				break;
			case '--debuglevel':
				$DebugLevel = $argv[++$i];
				break;
			default:
				echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
			case '--help':
				echo $argv[0] . PHP_EOL;
				echo 'Optionen:' . PHP_EOL;
				echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
				echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
				echo '  --ignore  <Wert>                   Welche pr�fungen sollen deaktiviert werden: zeit .' . PHP_EOL;
				echo PHP_EOL;
				die();
		}
	}

	$Werkzeug = new awisWerkzeuge();
	$Job = new detefleet_Import();
	$funktionen = new detefleet_funktionen();
	$funktionen->DebugLevel($DebugLevel);

	$awisLevel = $Werkzeug->awisLevel();
	if($awisLevel == '****') {
		throw new awisException('awisLevel unbekannt', 201208300827);
	}

	// Empfaenger fuer die Mails festlegen
	if($awisLevel == 'ENTW') {
		$empfaengerIntern = array('shuttle@de.atu.eu');
		$empfaenger = array('shuttle@de.atu.eu');
	}
	elseif($awisLevel == 'STAG') {
		$empfaengerIntern = array('shuttle@de.atu.eu');
		$empfaenger = array('shuttle@de.atu.eu');
	}
	elseif($awisLevel == 'PROD') {
		$empfaengerIntern = array('shuttle@de.atu.eu');
		$empfaenger = array('Zentrale-Abrechnung@de.atu.eu','shuttle@de.atu.eu');
	}
	elseif($awisLevel == 'SHUT') {
		$empfaengerIntern = array('shuttle@de.atu.eu');
		$empfaenger = array('Zentrale-Abrechnung@de.atu.eu','shuttle@de.atu.eu');
	}

	$funktionen->debugAusgabe('pruefe ob Preisliste erstellt werden muss', 10);
	// pruefen ob eine neue Preisliste erzeugt werden muss
	$Job->pruefeStart();
	$funktionen->debugAusgabe('Pruefung abgeschlossen', 10);

	// neue Preisliste muss  erzeugt werden
	if ($Job->getFlagImport() == true) {
		$funktionen->debugAusgabe('Preisliste erstellen gestartet', 10);
		$Werkzeug->EMail($empfaengerIntern,'DeTeFleet beginnt', 'Beginne DeTeFleet',2,'','awis@de.atu.eu');

		// Import starten
		$funktionen->debugAusgabe('Start Import der Grunddatei', 10);
		$Job->starteImport();
		$funktionen->debugAusgabe('Ende Import der Grunddatei', 10);

		/*
		 * Extrahierung starten (Teile der Artikelbezeichnung werden
		 * in verschiedene Spalten extrahiert)
		 */
		$funktionen->debugAusgabe('Start Extrahierung', 10);
		$Extrahierung = new detefleet_Extrahierung($Job->getFlagImport(),$Job->getPreislisteID());
		$Extrahierung->extrahierung();
		$funktionen->debugAusgabe('Ende Extrahierung', 10);

		// Matching MatNr -> ATUNR starten
		$funktionen->debugAusgabe('Start Matching', 10);
		$Matching = new detefleet_Matching();
		$Matching->setPreisListe($Job->getPreislisteID());
		$Matching->starteMatching();
		$funktionen->debugAusgabe('Start Matching R1', 10);
		$Matching->starteMatchingRX('R1');
		$funktionen->debugAusgabe('Start Matching R2', 10);
		$Matching->starteMatchingRX('R2');
		$funktionen->debugAusgabe('Start Anreicherung', 10);
		$Matching->starteAnreicherung();

		// Preisliste exportieren
		$funktionen->debugAusgabe('Start Export', 10);
		$Export = new detefleet_Export();
		$Export->setPreisListe($Job->getPreislisteID());

		// Preisliste auf Duplikate pruefen
		$funktionen->debugAusgabe('Start CheckDuplikate', 10);
		$Duplikate = $Export->checkDuplikate();
		$funktionen->debugAusgabe('Ende CheckDuplikate', 10);

		// Alles in Ordnung, Preisliste wurde erstellt
		if ($Duplikate == false) {
			$Export->export();
			$funktionen->debugAusgabe('Ende Export', 10);
			$funktionen->debugAusgabe('Start Auswertung', 10);
			$Auswertung = new deteFleet_Auswertung();
			$Auswertung->exportMatZuordnung();
			$Auswertung->exportMatZuordnung2();
			$Auswertung->exportMatZuordnungEdiPlan();
			$funktionen->debugAusgabe('Ende Auswertung', 10);

			$Werkzeug->EMail($empfaenger,'OK PREISLISTE DETEFLEET - DATUM '.date('Ymd'), 'DeTeFleet - Preisliste wurde erstellt',2,'','awis@de.atu.eu');
			$funktionen->debugAusgabe('Preisliste erfolgreich erstellt', 10);
		}
		/* Duplikate gefunden - ERROR */
		else {
			$Werkzeug->EMail($empfaenger,'ERROR PREISLISTE DETEFLEET - DATUM '.date('Ymd'), 'DeTeFleet - Preisliste wurde nicht erstellt. Duplikate vorhanden!',2,'','awis@de.atu.eu');
			$funktionen->debugAusgabe('Preisliste nicht erstellt - DUPLIKATE', 10);
		}
	}
	/* Preisliste darf nicht erzeugt werden */
	else {
		$Werkzeug->EMail($empfaengerIntern,'DeTeFleet', 'DeTeFleet darf nicht laufen',2,'','awis@de.atu.eu');
		$funktionen->debugAusgabe('Preisliste nicht erstellt - darf nicht laufen', 10);
	}

} catch (Exception $ex) {
	echo PHP_EOL . 'BSWImport: Fehler: ' . $ex->getMessage() . PHP_EOL;

	$AWISWerkzeug = new awisWerkzeuge();
	$AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'BSWImport-Fehler', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

	echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
	echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
	echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
	echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>
