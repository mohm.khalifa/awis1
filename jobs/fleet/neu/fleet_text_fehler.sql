set lines 500;
set pagesize 0;
set heading off;
set feedback off;
set verify off;
set term off;
set trimspool on;
spool fleet_text_fehler.txt
select
KENNZEICHEN || ';' ||
ANREDE_ID || ';' ||
NAME || ';' ||
NAME2 || ';' ||
STAAT_ID || ';' ||
STRASSE || ';' ||
HAUSNUMMER || ';' ||
PLZ || ';' ||
ORT || ';' ||
FAHRER || ';' ||
ALTERFAHRER || ';' ||
FAHRGESTELLNR || ';' ||
TO_CHAR(ERSTZULASSUNG,'YYYY-MM-DD HH24:MI:SS') || ';' ||
FLOTTENTYP 
from
skFLEET_TEXT_FEHLER;
exit;

