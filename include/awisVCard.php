<?php

class awisVCard{

    public function getVCard($Vorname, $Nachname, $Title, $Organistation, $Abteilung, $JobTitle, $Tel, $Handy, $Mail){
        $VCard  ='BEGIN:VCARD' . PHP_EOL;
        $VCard .='VERSION:4.0'. PHP_EOL;


        $N = $Nachname . ';' . $Vorname . ';;' . $Title;
        $VCard .='N:'.$N. PHP_EOL;

        $FN = $Title!=''?$Title . ' ' : '';
        $FN .= $Vorname . ' ';
        $FN .= $Nachname;
        $VCard .='FN:'.$FN. PHP_EOL;
        if($Organistation){
            $VCard .='ORG:'. $Organistation. PHP_EOL;
        }
        if($Abteilung){
            $VCard .='ROLE:'.$Abteilung.' '.$JobTitle. PHP_EOL;
        }

        if($JobTitle){
            $VCard .='TITLE:'.$JobTitle. PHP_EOL;
        }

        if($Tel){
            $VCard .='TEL;TYPE=work,voice:'.$Tel. PHP_EOL;
        }
        if($Handy){
            $VCard .='TEL;TYPE=mobile,CELL,voice:'.$Handy. PHP_EOL;
        }

        if($Mail){
            $VCard .='EMAIL:'.$Mail. PHP_EOL;
        }


        $VCard .='REV:'.date('c'). PHP_EOL;
        $VCard .='END:VCARD'. PHP_EOL;

        return $VCard;
    }

}