<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
$con = awisLogon();

echo '<h2>AWIS - Personaleins�tze</h2>';

$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe>0)
{
	echo '<a name=Einleitung></a><h3>Einleitung</h3>';
	echo 'Unter dem Bereich <i>Personaleins�tze</i> werden die Aktivit�ten der Mitarbeiter (die im Vertrieb angesiedelt sind)
		eingetragen und ausgewertet.<br><br><hr>';

	echo '<a name=Suche></a><h3>Suchmaske</h3>';

	echo 'In der Suchmaske kann festgelegt werden, nach welchen Kriterien die Eins�tze angezeigt werden.
		Die Kriterien werden auf Wunsch zwischengespeichert und bleiben damit auch dann erhalten, wenn
		dieser Programmpunkt verlassen wird.
		<br><br>
		Folgende Kriterien k�nnen ausgew�hlt werden 
		<ul>
			<li><i><b>Mitarbeiter</b></i><br>
			Es kann aus der Liste ein Mitarbeiter oder <i>::alle::</i> gew�hlt werden. Es stehen dabei nur
			die innerhalb dieses Programmabschnitts definierten Mitarbeiter zur Verf�gung.</li> 
			<li><i><b>T�tigkeit</b></i><br>
			Eine T�tigkiet beschreibt eine Aufgabe, die ein Mitarbeiter erf�llt. Es kann eine einzelne oder 
			<i>::alle::</i> gew�hlt werden.</li>
			<li><i><b>Datum</b></i><br>
			Hier kann ein Datumsbereich festgelegt werden. Das Datum muss im Format TT.MM oder TT.MM.JJ oder TT.MM.JJJJ
			eingegeben werden.</li>
			<li><i><b>Filiale</b></i><br>
			Bei diesem Feld kann eine Filialnummer eingegeben werden.
			</li>
			<li><i><b>Regionalzentrum</b></i><br>
			Je nach Benutzerrechte k�nnen Sie hier ein Regionalzentrum ausw�hlen. Wenn Sie <i>::alle::</i> ausw�hlen,
			werden alle Regionalzentren ausgew�hlt, f�r die Sie eine Berechtigung besitzen.
			</li>
			<li><i><b>Kategorie</b></i><br>
			Hier k�nnen die einzelnen Kategorieren (d.h. T�tigkeiten) der Mitarbeiter ausgew�hlt werden.
			</li>
		</ul><br>';
	echo 'Folgende Schaltfl�chen stehen zur Verf�gung:
		<ul><img src=/bilder/eingabe_ok.png> Suche starten (Eingabetaste)</ul>
		<ul><img src=/bilder/radierer.png> Suchmaske l�schen</ul>
		<ul><img src=/bilder/hilfe.png> Diese Hilfe anzeigen (ALT+H)</ul>
		<ul><img src=/bilder/zurueck.png> Zur�ck zum Hauptmen�</ul>
		';
	echo '<hr>';
	echo '<a name=Einsaetze></a><h3>Eins�tze</h3>';
	
	echo 'Bei den Eins�tzen werden alle Eins�tze der Mitarebeiter angezeigt, die den in der Suchmaske festgelegten
		Kriterien entsprechen.<br>
		 '	;
	if(($RechteStufe&2)==2)		// Neue Hinzuf�gen
	{
		echo 'Am oberen Bildschirmrand sehen Sie eine Eingabezeile, in der ein neuer Datensatz hinzugef�gt werden kann.<br>
			Nachdem alle Felder ausgef�llt wurden, kann der Datensatz �ber die Schaltfl�che <img src=/bilder/diskette.png> oder 
			die Tastenkombintion ALT+S gespeichert werden.<br>';
	}
	
	echo 'In der angezeigten Liste <i>Gefundene Eintr�ge</i> werden alle gefundenen Datens�tze angezeigt.<br>';
	if(($RechteStufe&4)==4)		// �ndern
	{
		echo 'Mit Hilfe der Schaltfl�che <img src=/bilder/aendern.png> oder der Tastenkombination ALT+A k�nnen die Datens�tze 
		ge�ndert werden.';
	}
	if(($RechteStufe&16)!=16)
	{
		echo ' Sie k�nnen jedoch nur Datens�tze �ndern, die den aktuellen oder den vorhergehenden Tag betreffen.<br>';
	}

	if(($RechteStufe&8)==8)		// L�schen
	{
		echo 'Mit Hilfe der Schaltfl�che <img src=/bilder/muelleimer.png> oder der Tastenkombination ALT+X k�nnen Sie Datens�tze 
		l�schen. Es erscheint beim L�schen noch einmal eine Sicherheitsabfrage.';
	}
	if(($RechteStufe&16)!=16)
	{
		echo ' Sie k�nnen jedoch nur Datens�tze l�schen, die den aktuellen oder den vorhergehenden Tag betreffen.<br>';
	}
	
	echo '<br><br>In der Liste werden die Handy- und Festnetztelefonnummern angezeigt (sofern verf�gbar). Durch Anklicken einer Kurzwahl
		kann der entsprechende Kontakteintrag aus dem Telefonbuch ge�ffnet werden. Wenn die Schaltfl�chen <img src=/bilder/handy.png>
		oder <img src=/bilder/telefon.png> mit der Maus angefahren werden, so wird in der Statuszeile die Telefonnummer zur Kurzwahl
		angezeigt.<br>	';
		
	echo '<br>�ber die Schaltfl�che <img src=/bilder/drucker.png> oder der Tastenkombination ALT+P kann die aktuell angezeigte
		Liste als PDF Datei erstellt und anschlie�end gedruckt oder gespeichert werden.';
}

/***************************
* Mitarbeiter
***************************/
$RechteStufe=awisBenutzerRecht($con, 1301);
if($RechteStufe>0)
{
	echo '<a name=Mitarbeiter></a><h3>Mitarbeiter</h3>';

	
	echo 'In diesem Register k�nnen Mitarbeiter angelegt und verwaltet werden.<br>';
	if(($RechteStufe&4)==4)
	{
		echo 'In der ersten Zeile k�nnen neue Mitarbeiter angelegt werden. Wenn alle Felder ausgef�llt wurden,
			kann der Datensatz �ber die Schaltfl�che <img src=/bilder/diskette.png> oder 
			die Tastenkombintion ALT+S gespeichert werden.<br>';
	}

	
}


if(awisBenutzerRecht($con, 1302)>0)
{
	echo '<a name=Probleme></a><h3>Noch zu kl�ren</h3>';

}

if(awisBenutzerRecht($con, 1305)>0)
{
	echo '<a name=Gebiete></a><h3>Gebiete</h3>';

}


if(awisBenutzerRecht($con, 1304)>0)
{
	echo '<a name=Auswertungen></a><h3>Auswertungen</h3>';

}

    awislogoff($con);
?>