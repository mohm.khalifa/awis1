#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fuer ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=EUrechnungen
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.01;Erste Version;11.09.2016;Patrick Gebhardt

