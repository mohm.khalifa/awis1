<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

$TextKonserven = array();
$TextKonserven[]=array('XBT','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);
	if($Recht19000==0)
	{
	    awisEreignis(3,1000,'SVCVEREINBARUNGENINFOS',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->Formular_Start();

	$EditModus=(($Recht19000&6)!=0);
	if($EditModus)
	{
		$AWISCursorPosition='';
	}

	$SQL = 'SELECT *';
	$SQL .= ' FROM Informationstypen';
	$SQL .= ' LEFT OUTER JOIN SVCVEREINBARUNGENINFOS ON ITY_KEY = SVI_ITY_KEY AND SVI_SVB_KEY='.$AWIS_KEY1;
	$SQL .= ' WHERE ITY_STATUS<>\'I\' AND ITY_XTN_KUERZEL = \'SVB\' AND ITY_BEREICH = \'Servicevereinbarung\'';
	$SQL .= ' ORDER BY ITY_BEREICH, ITY_SORTIERUNG';
	$rsLIN = $DB->RecordSetOeffnen($SQL);

	$LetzterBereich = '';
	while(!$rsLIN->EOF())
	{
		$ToolTipp = ($rsLIN->FeldInhalt('SVI_USER')==''?'':$rsLIN->FeldInhalt('SVI_USER').'/'.$rsLIN->FeldInhalt('SVI_USERDAT'));

		$Form->ZeileStart();
		$Info = $Form->LadeTextBaustein('Infofeld','ity_lbl_'.$rsLIN->FeldInhalt('ITY_KEY'));
		$InfoTTT = $Form->LadeTextBaustein('Infofeld','ity_ttt_'.$rsLIN->FeldInhalt('ITY_KEY'));
		$Form->Erstelle_TextLabel($Info.':',200,'','',$InfoTTT);
		$EditModus=($AWISBenutzer->HatDasRecht($rsLIN->FeldInhalt('ITY_XRC_ID'))&$rsLIN->FeldInhalt('ITY_RECHTESTUFEBEARBEITEN'))==$rsLIN->FeldInhalt('ITY_RECHTESTUFEBEARBEITEN');
		if($rsLIN->FeldInhalt('ITY_DATENQUELLE')!='')
		{
			switch(substr($rsLIN->FeldInhalt('ITY_DATENQUELLE'),0,3))
			{
				case 'TXT':
					$Felder = explode(':',$rsLIN->FeldInhalt('ITY_DATENQUELLE'));
					$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
					$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
					$Form->Erstelle_SelectFeld('SVI_WERT_'.$rsLIN->FeldInhalt('ITY_KEY').'_'.$rsLIN->FeldInhalt('SVI_KEY').'_'.$rsLIN->FeldInhalt('ITY_FORMAT'),$rsLIN->FeldInhalt('SVI_WERT'),$rsLIN->FeldInhalt('ITY_BREITE'),$EditModus,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'',$ToolTipp);
					break;
				case 'SQL':
					$Felder = explode(':',$rsLIN->FeldInhalt('ITY_DATENQUELLE'));
					$Form->Erstelle_SelectFeld('SVI_WERT_'.$rsLIN->FeldInhalt('ITY_KEY').'_'.$rsLIN->FeldInhalt('SVI_KEY').'_'.$rsLIN->FeldInhalt('ITY_FORMAT'),$rsLIN->FeldInhalt('SVI_WERT'),$rsLIN->FeldInhalt('ITY_BREITE'),$EditModus,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','',$ToolTipp);
					break;
				default:
					$Form->Erstelle_TextFeld('SVI_WERT_'.$rsLIN->FeldInhalt('ITY_KEY').'_'.$rsLIN->FeldInhalt('SVI_KEY').'_'.$rsLIN->FeldInhalt('ITY_FORMAT'),$rsLIN->FeldInhalt('SVI_WERT'),$rsLIN->FeldInhalt('ITY_ZEICHEN'),$rsLIN->FeldInhalt('ITY_BREITE'),$EditModus,'','','',$rsLIN->FeldInhalt('ITY_FORMAT'),'',$ToolTipp,$rsLIN->FeldInhalt('ITY_DATENQUELLE'));
					break;
			}
		}
		else
		{
			$Form->Erstelle_TextFeld('SVI_WERT_'.$rsLIN->FeldInhalt('ITY_KEY').'_'.$rsLIN->FeldInhalt('SVI_KEY').'_'.$rsLIN->FeldInhalt('ITY_FORMAT'),$rsLIN->FeldInhalt('SVI_WERT'),$rsLIN->FeldInhalt('ITY_ZEICHEN'),$rsLIN->FeldInhalt('ITY_BREITE'),$EditModus,'','','',$rsLIN->FeldInhalt('ITY_FORMAT'),'',$ToolTipp);
		}
		if($AWISCursorPosition=='')
		{
			$AWISCursorPosition = 'txtSVI_WERT_'.$rsLIN->FeldInhalt('ITY_KEY').'_'.$rsLIN->FeldInhalt('SVI_KEY').'_'.$rsLIN->FeldInhalt('ITY_FORMAT');
		}

		$Form->ZeileEnde();
		$rsLIN->DSWeiter();
	}

	$Form->Formular_Ende();
}
catch (Exception $ex)
{
	if(is_object($Form))
	{
		$Form->Formular_Start();
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'WIEDERHOLEN',0);
		$Form->Formular_Ende();
	}
}
?>