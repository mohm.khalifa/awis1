<?php

class PartnerserviceV3 extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'Bestandsauskunft' => 'Bestandsauskunft',
      'SuchAnfrage' => 'SuchAnfrage',
      'Serialize' => 'Serialize',
      'BestandsabfrageV3' => 'BestandsabfrageV3',
      'BestandV3' => 'BestandV3',
      'ArrayOfBestandsabfrageV3' => 'ArrayOfBestandsabfrageV3',
      'SuchAnfrageArtikel' => 'SuchAnfrageArtikel',
      'ArrayOfSuchAnfrageArtikel' => 'ArrayOfSuchAnfrageArtikel',
      'BestandsauskunftResponse' => 'BestandsauskunftResponse',
      'ArrayOfBestandV3' => 'ArrayOfBestandV3',
      'Bestellung' => 'Bestellung',
      'PartnerBestellungV3' => 'PartnerBestellungV3',
      'ArrayOfBestellPosition' => 'ArrayOfBestellPosition',
      'BestellPosition' => 'BestellPosition',
      'OrderLieferadresse' => 'OrderLieferadresse',
      'BestellungResponse' => 'BestellungResponse',
      'PartnerBestellBestaetigungV3' => 'PartnerBestellBestaetigungV3',
      'ArrayOfBestaetigungPosition' => 'ArrayOfBestaetigungPosition',
      'BestaetigungPosition' => 'BestaetigungPosition',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);

      parent::__construct($wsdl, $options);
    }

    /**
     * @param Bestandsauskunft $parameters
     * @return BestandsauskunftResponse
     */
    public function Bestandsauskunft(Bestandsauskunft $parameters)
    {
      return $this->__soapCall('Bestandsauskunft', array($parameters));
    }

    /**
     * @param Bestellung $parameters
     * @return BestellungResponse
     */
    public function Bestellung(Bestellung $parameters)
    {
      return $this->__soapCall('Bestellung', array($parameters));
    }

}
