<?php
require_once 'awisZukaufpreislisten.inc';

try
{
    $Benutzer = 'awis_jobs';
    $DebugLevel = 0;
    $MD5 = 1;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--md5':
                            $MD5 = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
		            echo ' Optionen:'.PHP_EOL;
		            echo ' --benutzer <Login>                 Anmeldename fuer den Job.'.PHP_EOL;
		            echo ' --debuglevel <Nr>                  Zu verwendender DebugLevel fuer Ausgaben.'.PHP_EOL;
		            echo ' --md5 <{0,1}>                            MD5 ignorieren?'.PHP_EOL;
                            die();
            }
    }

    $Imp= new awisZukaufpreislisten(false,'awis_jobs');
    $Imp->MD5Check =$MD5;
    $Imp->DebugLevel($DebugLevel);
    $Imp->ImportiereDatei();

}
catch (Exception $ex)
{
        echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
        echo 'SQL:'.$ex->getSQL().PHP_EOL;
}
?>

