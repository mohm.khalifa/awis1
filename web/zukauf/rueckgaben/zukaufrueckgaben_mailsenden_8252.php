<?php
/**
 * �bermittlung der Daten an WERTHENBACH
 */
require_once 'awisMailer.inc';

try
{
//	$Form->DebugAusgabe(1,$_POST,$_GET);
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//pr�fen wegen Datumsformatierung
	$SQL = 'SELECT DISTINCT zukauflieferscheine.*, COALESCE(LAN_CODE,\'DE\') AS FIL_LAN_CODE, LVK_EMAIL';
	$SQL .= ', ZLH_BEZEICHNUNG, LKD_KUNDENNR, ZRU_MENGE';
	$SQL .= ' FROM zukauflieferscheine';
	$SQL .= ' INNER JOIN zukaufrueckgaben ON ZRU_ZUL_KEY = ZUL_KEY';
	$SQL .= ' LEFT OUTER JOIN Filialen ON ZUL_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' LEFT OUTER JOIN FILIALENLIEFERANTEN ON FLI_FIL_ID = FIL_ID AND FLI_LIE_NR = ZUL_LIE_NR AND FLI_GUELTIGAB <= ZUL_DATUM AND FLI_GUELTIGBIS >= ZUL_DATUM';
	$SQL .= ' LEFT OUTER JOIN LIEFERANTENVERKAUFSHAEUSER ON LVK_KEY = FLI_LVK_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZUL_ZLH_KEY = ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN LIEFERANTENKUNDENNUMMERN ON LKD_LIE_NR = ZUL_LIE_NR AND LKD_FIL_ID = ZUL_FIL_ID';
	$SQL .= ' WHERE zru_key = :var_N0_ZRU_KEY';
	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_ZRU_KEY']=$DB->FeldInhaltFormat('N0',$_GET['ZRU_KEY'],false);
	$rsZUL = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	
	if($rsZUL->EOF())
	{
	    die('ARTIKEL NICHT GEFUNDEN');
	}
	// Parameter
	
	$TextKonserven[]=array('ZUR','Mail%');
	$AWISMailKonserven = $Form->LadeTexte($TextKonserven);
	
	$EmpfaengerMail = $rsZUL->FeldInhalt('LVK_EMAIL');
	if($EmpfaengerMail=='')
	{
		
	}
	
	$Absender = str_pad($rsZUL->FeldInhalt('ZUL_FIL_ID'),4,'0',STR_PAD_LEFT).'@'.$rsZUL->FeldInhalt('FIL_LAN_CODE').'.atu.eu';
	$Betreff = $AWISMailKonserven['ZRU']['MailBetreff'];
	$Text = $AWISMailKonserven['ZRU']['MailBody'];
	$Text = str_replace('$FILIALE$',$rsZUL->FeldInhalt('ZUL_FIL_ID'),$Text);
	$Text = str_replace('$ARTIKEL$',$rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'),$Text);
	$Text = str_replace('$ARTIKELNR$',$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER'),$Text);
	$Text = str_replace('$MENGE$',$rsZUL->FeldInhalt('ZRU_MENGE'),$Text);
	$Text = str_replace('$HERSTELLERID$',$rsZUL->FeldInhalt('ZUL_HERSTELLER'),$Text);
	$Text = str_replace('$HERSTELLER$',$rsZUL->FeldInhalt('ZLH_BEZEICHNUNG'),$Text);
	$Text = str_replace('$LIEFERSCHEIN$',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'),$Text);
	$Text = str_replace('$EXTERNEID$',$rsZUL->FeldInhalt('ZUL_EXTERNEID'),$Text);
	$Text = str_replace('$LIEFERDATUM$',$Form->Format('D',$rsZUL->FeldInhalt('ZUL_DATUM')),$Text);
	$Text = str_replace('$WANR$',$rsZUL->FeldInhalt('ZUL_WANR'),$Text);
	$Text = str_replace('$KUNDENNR$',$rsZUL->FeldInhalt('LKD_KUNDENNR'),$Text);
	
	$Text = str_replace('$EMPFAENGER_MAIL$',$rsZUL->FeldInhalt('LVK_EMAIL'),$Text);
	
	//$Text = str_replace('$WANR$',$rsZUL->FeldI)
	$AWISWerkzeug = new awisWerkzeuge();
	
	switch($AWISWerkzeug->awisLevel())
	{
	    case awisWerkzeuge::AWIS_LEVEL_ENTWICK:
	    case awisWerkzeuge::AWIS_LEVEL_UNBEKANNT:
	        $Empfaenger = 'shuttle@de.atu.eu';
	        break;
	    case awisWerkzeuge::AWIS_LEVEL_STAGING:
	        $Empfaenger = 'zukauf@de.atu.eu';
	        break;
	    case awisWerkzeuge::AWIS_LEVEL_PRODUKTIV:
	        $Empfaenger = $rsZUL->FeldInhalt('LVK_EMAIL');
	        break;
	}
	
	$Mail->DebugLevel(0);
	$Mail->Absender($Absender);
	$Mail->AdressListe(awisMailer::TYP_TO,$Empfaenger,false,false);
	$Mail->AdressListe(awisMailer::TYP_CC,'zukauf@de.atu.eu',false,false);
	$Mail->AdressListe(awisMailer::TYP_CC,'shuttle@de.atu.eu',false,false);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text);
	
	$Mail->MailSenden();
}
catch(Exception $ex)
{
	$Form->Fehler_Anzeigen('INTERN', $ex->getMessage(),'MELDEN',6,'201101240837');
}