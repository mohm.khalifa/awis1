<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $AWISBenutzer;
global $AWISSprache;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$con=awislogon();

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Operating_AWIS');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_AWIS');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_AWIS'].'</title>';
?>
</head>
<body>
<?php
include ("ATU_Header.php");	// Kopfzeile

$RechteStufe = awisBenutzerRecht($con,3120);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'Operating-AWIS',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_debug(1,$_REQUEST);

$cmdAktion='';

if(isset($_GET['cmdAktion']))
{
	$cmdAktion = $_GET['cmdAktion'];
}

/************************************************
* Daten anzeigen
************************************************/

awis_RegisterErstellen(3120, $con, $cmdAktion);

	print "<br><hr><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

	awislogoff($con);

?>
</body>
</html>

