#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Dienstleistungspreise
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
2.00.00;Neues Framework verwendet und alle L�nder aufgenommen;06.08.2012;<a href=mailto:henry.ott@ds.atu.eu>Henry Ott</a>
1.00.05;Gesperrte Dienstleistungen werden ROT dargestellt;05.11.2007;<a href=mailto:christian.argauer@de.atu.eu>Christian Argauer</a>
1.00.04;Suche auch mit Umlauten und Sonderzeichen;23.03.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.03;Suche auch nach Nummer m�glich;11.03.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.02;Sortierung eingebaut;27.02.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.01;Erste Version;15.01.2004;<a href=mailto:Entwick>Sacha Kerres</a>

