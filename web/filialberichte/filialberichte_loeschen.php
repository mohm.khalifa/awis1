<?php

global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('FLB','%');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

        $Form->Formular_Start();

        if(isset($_GET['Del']))
		{
		$Tabelle = 'FLB';
		$Key=$_GET['Del'];

                $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

                $SQL = "Select Flb_Key, Flb_Datum, FLB_FIL_ID, FLB_BEMERKUNG, FLB_PRUEFER, FLB_PBE_KEY, FLB_PEZ_STATUS, FLB_USER, Flb_Userdat ";
                $SQL .= "FROM FILIALBERICHTE WHERE FLB_KEY=".$_GET['Del'];

                $rsDel = $DB->RecordSetOeffnen($SQL);

                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_KEY'],60,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_FIL_ID'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_DATUM'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_BEMERKUNG'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FLB']['FLB_PRUEFER'],200,'');
                $Form->ZeileEnde();

                $FGKZeile=0;
                $DS = 0;

                While(!$rsDel->EOF())
                {
		    		$Form->ZeileStart();	
                    $Form->Erstelle_ListenFeld('FLB_KEY',$rsDel->FeldInhalt('FLB_KEY'),0,60,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FLB_FIL_ID',$rsDel->FeldInhalt('FLB_FIL_ID'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FLB_DATUM',$rsDel->FeldInhalt('FLB_DATUM'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FLB_BEMERKUNG',$rsDel->FeldInhalt('FLB_BEMERKUNG'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FLB_PRUEFER',$rsDel->FeldInhalt('FLB_PRUEFER'),0,200,false,($FGKZeile%2),'','');
                    $Form->ZeileEnde();

                    $FGKZeile++;
                    $rsDel->DSWeiter();
                }

                $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./filialberichte_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

        $Form->Erstelle_HiddenFeld('FLB_KEY', $_REQUEST['Del']);
        $Form->Erstelle_HiddenFeld('FLB_KEY', $Key);
		
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
                
        	$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
        elseif($_REQUEST['cmdLoeschenOK'])
        {
	    	$SQL = "SELECT * from FILIALBERICHTE WHERE FLB_KEY=".$DB->FeldInhaltFormat('NO', $_POST['txtFLB_KEY']);
            
            $rsDelDaten = $DB->RecordSetOeffnen($SQL);

            $SQL = "Delete from FILIALBERICHTE WHERE FLB_KEY=".$DB->FeldInhaltFormat('NO',  $_POST['txtFLB_KEY']);

            if($DB->Ausfuehren($SQL)===false)
            {
            }
	     
            $SQL='Delete from PERSONALEINSBERICHTSZUORD WHERE PEZ_XTN_KUERZEL = \'FLB\' AND PEZ_XXX_KEY='.$DB->FeldInhaltFormat('NO',$_POST['txtFLB_KEY']);
            
        	if($DB->Ausfuehren($SQL)===false)
            {
            }
            
            
	     $Fehler = 'Datensatz der Filiale '.$rsDelDaten->FeldInhalt('FLB_FIL_ID').' wurde gel�scht';
			   
	     if($Fehler!='')
	     {
		echo('<span class=HinweisText>'.$Fehler.'</span>');
	     } 
	     
	      $AWIS_KEY1 = 0;
	     
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>