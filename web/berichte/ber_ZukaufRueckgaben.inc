<?php
require_once 'awisBerichte.inc';
require_once 'tcpdf.php';

class ber_ZukaufRueckgaben
	extends awisBerichte
{
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{

		//*****************************************
		// Daten f�r den Bericht laden
		//*****************************************
		$SQL = 'SELECT Zukaufrueckgaben.*, Zukauflieferscheine.*';
		$SQL .= ' , LIE_NAME1, LIE_NAME2, ZLH_BEZEICHNUNG';
		$SQL .= ', FIL_BEZ, FIL_STRASSE, FIL_ORT, LKD_KUNDENNR';
		$SQL .= ' FROM Zukaufrueckgaben';
		$SQL .= ' INNER JOIN Zukauflieferscheine ON ZRU_ZUL_KEY = ZUL_KEY';
		$SQL .= ' INNER JOIN Filialen ON ZUL_FIL_ID = FIL_ID';
		$SQL .= ' INNER JOIN FilialenLieferanten ON FLI_FIL_ID = FIL_ID AND FLI_LIE_NR = ZUL_LIE_NR AND FLI_GUELTIGAB <= ZUL_DATUM AND FLI_GUELTIGBIS >= ZUL_DATUM';
		$SQL .= ' INNER JOIN LIEFERANTENKUNDENNUMMERN ON LKD_FIL_ID = FIL_ID AND LKD_LIE_NR = ZUL_LIE_NR';
		$SQL .= ' INNER JOIN Lieferanten ON ZUL_LIE_NR = LIE_NR';
		$SQL .= ' LEFT OUTER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZUL_ZLH_KEY = ZLH_KEY';
		
		// Alle Parameter auswerten
		// Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
		//			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
		$Bedingung = '';
		if(isset($Parameter['ZRU_KEY']))
		{
			$Param = explode('~',$Parameter['ZRU_KEY']);
			$Bedingung .= " and ZRU_KEY ".self::_VergleichsOperatorPruefen($Param[0])." ".$this->_DB->FeldInhaltFormat('N0',$Param[1],false);
		}
		
		if($Bedingung!='')
		{
			$SQL .= ' WHERE '.substr($Bedingung,4);
		}
		$SQL .= ' ORDER BY ZRU_KEY';

		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
		
		//*****************************************
		// Alle Textbausteine laden
		//*****************************************
		$TextKonserven[]=array('ZRU','*');
		$TextKonserven[]=array('ZUL','*');
		$TextKonserven[]=array('FIL','FIL_BEZ');
		$TextKonserven[]=array('FIL','FIL_ORT');
		$TextKonserven[]=array('LKD','LKD_KUNDENNR');
		$TextKonserven[]=array('ZLH','ZLH_BEZEICHNUNG');
		$TextKonserven[]=array('LIE','LIE_NAME1');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eiegntliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins(0,0,0);
		$this->AliasNbPages();
		
		
		// Barcode initalisieren
		$this->_Barcode = new awisBarcode($this);
		$this->NeueSeite();

		// Einzelne Felder drucken
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']*1.3);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe']*1.3,$this->_Form->Format('T',$this->_AWISSprachkonserven['LIE']['LIE_NAME1']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']*1.3);
		$this->Cell(80,$this->_Parameter['Zeilenhoehe']*1.3,$this->_Form->Format('T',$this->_rec->FeldInhalt('LIE_NAME1').' '.$this->_rec->FeldInhalt('LIE_NAME2')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*1.3;
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZUL']['ZUL_LIEFERSCHEINNR']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('L',$this->_rec->FeldInhalt('ZUL_LIEFERSCHEINNR')),0,0,'L');
		// Barcode
		$this->_Barcode->Code128(self::SeitenBreite()-50,$this->_Zeile,$this->_rec->FeldInhalt('ZUL_LIEFERSCHEINNR'),30,5);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZUL']['ZUL_DATUM']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$this->_rec->FeldInhalt('ZUL_DATUM')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZUL']['ZUL_WANR']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('L',$this->_rec->FeldInhalt('ZUL_WANR')),0,0,'L');
		// Barcode
		$this->_Barcode->Code128(self::SeitenBreite()-50,$this->_Zeile,$this->_rec->FeldInhalt('ZUL_WANR'),30,5);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZUL']['ZUL_ARTIKELNUMMER']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ZUL_ARTIKELNUMMER')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZLH']['ZLH_BEZEICHNUNG']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ZLH_BEZEICHNUNG').' ('.$this->_rec->FeldInhalt('ZUL_HERSTELLER').')'),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		// Trennlinie
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->Line($this->_Spalte,$this->_Zeile,self::SeitenBreite()-self::RECHTERRAND,$this->_Zeile);
		$this->_Zeile += 2;

		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['FIL']['FIL_BEZ']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('FIL_BEZ')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];

		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['FIL']['FIL_ORT']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('FIL_STRASSE').', '.$this->_rec->FeldInhalt('FIL_ORT')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['LKD']['LKD_KUNDENNR']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('LKD_KUNDENNR')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZRU']['ZRU_DATUM']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D',$this->_rec->FeldInhalt('ZRU_DATUM')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZRU']['ZRU_MENGE']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('NxT',$this->_rec->FeldInhalt('ZRU_MENGE')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Schriftgroesse']);
		$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['ZRU']['ZRU_BEMERKUNG']).':',0,0,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Schriftgroesse']);
		$this->MultiCell(90,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('ZRU_BEMERKUNG')),0,'L');
		$this->_Zeile = $this->GetY();
		
		//*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
	}
	

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,4,$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(($this->PageNo()==1?1:2));		// Erste oder zweite Seite importieren
			$this->useTemplate($pdf_vorlage);
		}
		
		$Zeile = $this->_Parameter['Kopfzeile'.($this->PageNo()==1?'1':'2').'-Oben'];
		$Spalte = $this->_Spalte;

		// Berichtsparameter anzeigen
		$this->SetXY($Spalte,$Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Ueberschrift-Groesse']);
		$this->Cell(30,4,$this->_Form->Format('TD',$this->_AWISSprachkonserven['ZRU']['ber_ZukaufRueckgaben']),0,0,'L','');

		$Zeile+=5;

		// Trennlinie
		$this->SetXY($Spalte,$Zeile);
		$this->Line($this->_Spalte,$Zeile,self::SeitenBreite()-self::RECHTERRAND,$Zeile);
		$Zeile+=2;
	}	
}
?>