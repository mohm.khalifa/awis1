<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff

$RechteStufe = awisBenutzerRecht($con, 2);
if(($RechteStufe&1)!=1)
{
	// Kein LOG, da normal in der �bersicht...
    awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

/*****************************************************
* Daten Speichern
*****************************************************/

if(isset($_POST['cmdSpeichern_x']))
{
//var_dump($_POST);
	foreach($_POST as $Eintrag=>$Wert)
	{
		if(substr($Eintrag,0,14)=='txtXRS_XRC_ID_')
		{
			$IDBit=substr($Eintrag,14);

			if($IDBit=='X')		// Neuer Datensatz
			{
				if($_POST['txtXRS_BESCHREIBUNG_X']!='')
				{
					$BindeVariablen=array();
					$BindeVariablen['var_N0_xrs_xrc_id']=$_POST['txtXRS_XRC_ID_X'];
					$BindeVariablen['var_N0_xrs_bit']=$_POST['txtXRS_BIT_X'];
					$BindeVariablen['var_T_xrs_beschreibung']=$_POST['txtXRS_BESCHREIBUNG_X'];
					
					$SQL = "INSERT INTO RechteStufen(XRS_XRC_ID, XRS_BIT, XRS_BESCHREIBUNG)";
					$SQL .= " VALUES(:var_N0_xrs_xrc_id,:var_N0_xrs_bit,:var_T_xrs_beschreibung)";
					$Erg=awisExecute($con, $SQL, true, false, 0, $BindeVariablen);
					if($Erg==FALSE)
					{
						awisErrorMailLink("rechte_RechteStufen.php", 2, $awisDBFehler['message'],$SQL);
					}
				}
			}
			elseif($_POST['txtXRS_BESCHREIBUNG_' . $IDBit] != $_POST['txtXRS_BESCHREIBUNG_' . $IDBit . '_OLD'])
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_xrs_xrc_id']=$_POST['txtXRS_XRC_ID_' . $IDBit];
				$BindeVariablen['var_N0_xrs_bit']=$_POST['txtXRS_BIT_' . $IDBit];
				$BindeVariablen['var_T_xrs_beschreibung']=$_POST['txtXRS_BESCHREIBUNG_' . $IDBit];
				
				$SQL = "UPDATE RechteStufen SET XRS_BESCHREIBUNG=:var_T_xrs_beschreibung";
				$SQL .= " WHERE XRS_XRC_ID=:var_N0_xrs_xrc_id";
				$SQL .= " AND XRS_BIT=:var_N0_xrs_bit";

				$Erg=awisExecute($con, $SQL, true, false, 0 ,$BindeVariablen);
				if($Erg==FALSE)
				{
					awisErrorMailLink("rechte_RechteStufen.php", 2, $awisDBFehler['message'],$SQL);
				}
			}
		}
	}
}		// Speichern

foreach($_POST as $Eintrag=>$Wert)
{
	if(substr($Eintrag,0,14)=='cmdDSLoeschen_')
	{
		$IDBit=substr($Eintrag,14);
		$IDBit=substr($IDBit,0,strpos($IDBit,'_'));
	
		$BindeVariablen=array();
		$BindeVariablen['var_N0_xrs_xrc_id']=$_POST['txtXRS_XRC_ID_' . $IDBit];
		$BindeVariablen['var_N0_xrs_bit']=$IDBit;

		$SQL = "DELETE FROM RechteStufen WHERE XRS_XRC_ID=:var_N0_xrs_xrc_id AND XRS_BIT=:var_N0_xrs_bit";

		$Erg=awisExecute($con, $SQL, true, false, 0, $BindeVariablen);
		if($Erg==FALSE)
		{
			awisErrorMailLink("rechte_RechteStufen.php", 2, $awisDBFehler['message'],$SQL);
		}

		break;
	}
}

awis_Debug(1,$_POST,$_GET);
/*****************************************************
* Daten anzeigen
*****************************************************/
$AWIS_KEY1 = (isset($_POST['txtXRC_ID'])?$_POST['txtXRC_ID']:(isset($_GET['XRC_ID'])?$_GET['XRC_ID']:0));

echo '<form name=frmRechteStufen method=post action=./rechte_Main.php?cmdAktion=RechteStufen>';
echo '<table border=0 width=100%>';

echo '<tr><td>Recht</td>';
echo '<td>';
echo "<select name=txtXRC_ID onchange='document.frmRechte.submit();'>";

$rsXRC = awisOpenRecordset($con, "SELECT * FROM rechte ORDER BY XRC_RECHT");
for($i=0;$i<$awisRSZeilen;$i++)
{
	echo '<option ';
	if($AWIS_KEY1 == $rsXRC['XRC_ID'][$i])
	{
		echo 'selected';
	}
	echo ' value=' . $rsXRC['XRC_ID'][$i] . '>' . $rsXRC['XRC_RECHT'][$i] . ' (' . $rsXRC['XRC_ID'][$i] . ')</option>';
}
unset($rsXRC);
echo '</select></td></tr>';
echo '</table>';
echo '<hr>';

echo '<table border=1 width=100%>';
echo '<colspec><col width=40></col><col width=60></col><col width=60></col><col width=*></col></colspec>';
echo '<tr><td id=FeldBez></td><td id=FeldBez>Bit</td><td id=FeldBez>Bitwert</td><td id=FeldBez>Beschreibung</td></tr>';
// Daten anzeigen
if(isset($_POST['txtXRC_ID']) OR isset($_GET['XRC_ID']))		// ID angegeben
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_xrs_xrc_id']=$AWIS_KEY1;
	
	$rsXRS = awisOpenRecordset($con,'SELECT * FROM RechteStufen WHERE XRS_XRC_ID=:var_N0_xrs_xrc_id ORDER BY XRS_BIT', true, false, $BindeVariablen);
	$MaxBit=-1;
	for($i=0;$i<$awisRSZeilen;$i++)
	{
		echo '<tr>';
		if(($RechteStufe&8)==8)
		{
			echo "<td><input type=image alt=\"L�schen (Alt+x)\" src=/bilder/Muelleimer_gross.png name=cmdDSLoeschen_" . $rsXRS['XRS_BIT'][$i] . "  accesskey=x onclick=location.href='./rechte_Main.php?" . $_SERVER['QUERY_STRING'] . "&Loeschen=True'></td>";
		}
		else
		{
			echo '<td></td>';
		}
		echo '<td><input type=hidden name=txtXRS_XRC_ID_'. $rsXRS['XRS_BIT'][$i] . ' value=' . $AWIS_KEY1 . '>';
		echo "<input type=hidden name=txtXRS_BIT_" .  $rsXRS['XRS_BIT'][$i] . " size=3 value=" .  $rsXRS['XRS_BIT'][$i] . ">" .  $rsXRS['XRS_BIT'][$i] . "</td>";
		echo '<td>' . pow(2,$rsXRS['XRS_BIT'][$i]) . '&nbsp;<input type=checkbox name=txtWahl_' . $rsXRS['XRS_BIT'][$i] . ' onclick=RechneWerte(' . $rsXRS['XRS_BIT'][$i] . ',' . pow(2,$rsXRS['XRS_BIT'][$i]) . ');></td>';
		if(($RechteStufe&4)==4)
		{
			echo '<td><input name=txtXRS_BESCHREIBUNG_'. $rsXRS['XRS_BIT'][$i] . " size=50 value='" . $rsXRS['XRS_BESCHREIBUNG'][$i] . "'>";
		}
		else
		{
			echo '<td>' . $rsXRS['XRS_BESCHREIBUNG'][$i] . '</td>';
		}
		echo '<input name=txtXRS_BESCHREIBUNG_'. $rsXRS['XRS_BIT'][$i] . "_OLD type=hidden size=50 value='" . $rsXRS['XRS_BESCHREIBUNG'][$i] . "'></td>";

		$MaxBit=$rsXRS['XRS_BIT'][$i];
	}

	if(($RechteStufe&2)==2)
	{
		echo '<tr><td></td><td><input type=hidden name=txtXRS_XRC_ID_X value=' . $AWIS_KEY1 . '>';
		echo "<input name=txtXRS_BIT_X size=3 value=" .  ++$MaxBit . "></td>";
		echo '<td>' . pow(2,$MaxBit) . '</td>';
		echo '<td><input name=txtXRS_BESCHREIBUNG_X size=50></td>';

		echo '</tr>';
	}
	echo '<tr><td colspan=2><font size=2>Rechtesumme</font></td><td><input readonly size=3 name=txtWahl_X value=0></td><td></td></tr>';

	unset($rsXRS);
}
echo '</table>';

if(($RechteStufe&2)==2 OR ($RechteStufe&4)==4)
{
	print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
}

echo '</form>';
awisLogoff($con);

?>



<SCRIPT LANGUAGE=JSCRIPT type="text/javascript">
// Zum Berechnen einer Summe aus den einzelnen Werte
function RechneWerte(Bit, BitWert)
{
	if(document.getElementsByName('txtWahl_'+Bit)[0].checked==true)
	{
		document.getElementsByName('txtWahl_X')[0].value=parseInt(document.getElementsByName('txtWahl_X')[0].value) + parseInt(BitWert);
	}
	else
	{
		document.getElementsByName('txtWahl_X')[0].value=parseInt(document.getElementsByName('txtWahl_X')[0].value) - parseInt(BitWert);
	}
}

</SCRIPT>