<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1200);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Feiertage',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

print "<h1 id=SeitenTitel>&Uuml;bersicht &uuml;ber die gesetzlichen Feiertage in Deutschland</h1>";
print '<table border=0 width=100%>';
print "<tr><td align=center>";
print "<img border=0 src=/bilder/feiertage_de_uebersicht_".date('Y').".png >";
print "</td></tr>";
print "<tr><td align=center>";
print '<a href="http://www.schnelle-online.info/Schulferien.html" target="_blank">Schulferien anzeigen</a>';
print "</td></tr>";
echo '</table>';
print 'Quelle: <a href="http://www.schnelle-online.info" target="_blank">http://www.schnelle-online.info</a>';
print '<table border=0 width=100%>';
print "<tr><td align=center>";
print "<img border=0 src=/bilder/feiertage_de.png >";
print "</td></tr>";
echo '</table>';
print 'Quelle: <a href="http://www.bmi.bund.de/SharedDocs/Downloads/DE/Lexikon/feiertage_de.html" target="_blank">http://www.bmi.bund.de/SharedDocs/Downloads/DE/Lexikon/feiertage_de.html</a>';
print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='infos_Main.php';>";
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

