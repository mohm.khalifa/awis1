<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$Daten = file_get_contents('/daten/daten/portal/infos.xml');
if($Daten!==false)
{
    $XML = new awisSimpleXMLElement($Daten);
    if($XML!==false)
    {
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        
        $SQL = 'DELETE ';
        $SQL .= ' FROM PORTALNEWSLETTERINFOS';

        $DB->Ausfuehren($SQL);

        foreach($XML->Beitraege->Beitrag AS $Beitrag)
        {
            echo 'ID:'.$Beitrag->id.PHP_EOL;

            $SQL = 'INSERT INTO PORTALNEWSLETTERINFOS';
            $SQL .= '(PNI_BEREICH,PNI_IMPORTID,PNI_TITEL,PNI_TEXT,PNI_XML,PNI_USER,PNI_USERDAT)';
            $SQL .= ' VALUES (';
            $SQL .= ' '.$DB->FeldInhaltFormat('T','Beitrag');
            $SQL .= ','.$DB->FeldInhaltFormat('T', (string)$Beitrag->id);
            $SQL .= ','.$DB->FeldInhaltFormat('T',trim(urldecode((string)$Beitrag->title)));
            $SQL .= ','.$DB->FeldInhaltFormat('T',trim(urldecode((string)$Beitrag->text)));

            $Infos = new SimpleXMLElement('<Infos/>');
            $Infos->addChild('Zugriffe',$Beitrag->hits);

            $SQL .= ','.$DB->FeldInhaltFormat('T',$Infos->asXML());
            $SQL .= ', \'Import\'';
            $SQL .= ', SYSDATE)';
            
            $DB->Ausfuehren($SQL);
        }
        foreach($XML->Umfragen->Frage AS $Frage)
        {
            echo 'ID:'.$Frage->id.PHP_EOL;

            $SQL = 'INSERT INTO PORTALNEWSLETTERINFOS';
            $SQL .= '(PNI_BEREICH,PNI_IMPORTID,PNI_TITEL,PNI_TEXT,PNI_XML,PNI_USER,PNI_USERDAT)';
            $SQL .= ' VALUES (';
            $SQL .= ' '.$DB->FeldInhaltFormat('T','Frage');
            $SQL .= ','.$DB->FeldInhaltFormat('T', (string)$Frage->id);
            $SQL .= ','.$DB->FeldInhaltFormat('T',urldecode((string)$Frage->title));
            $SQL .= ','.$DB->FeldInhaltFormat('T',urldecode((string)$Frage->text));
            $Infos = new SimpleXMLElement('<Infos/>');
            $Infos->addChild('Stimmen',$Frage->hits);
            $Infos->addChild('Gesamtstimmen',$Frage->voters);

            $SQL .= ','.$DB->FeldInhaltFormat('T',$Infos->asXML());
            $SQL .= ', \'Import\'';
            $SQL .= ', SYSDATE)';

            $DB->Ausfuehren($SQL);
        }
        foreach($XML->Forumsbeitraege->Beitrag AS $Beitrag)
        {
            echo 'ID:'.$Beitrag->id.PHP_EOL;

            $SQL = 'INSERT INTO PORTALNEWSLETTERINFOS';
            $SQL .= '(PNI_BEREICH,PNI_IMPORTID,PNI_TITEL,PNI_TEXT,PNI_XML,PNI_USER,PNI_USERDAT)';
            $SQL .= ' VALUES (';
            $SQL .= ' '.$DB->FeldInhaltFormat('T','Forum');
            $SQL .= ','.$DB->FeldInhaltFormat('T', (string)$Beitrag->id);
            $SQL .= ','.$DB->FeldInhaltFormat('T',urldecode((string)$Beitrag->title));
            $SQL .= ','.$DB->FeldInhaltFormat('T',urldecode((string)$Beitrag->text));
            $Infos = new SimpleXMLElement('<Infos/>');
            $Infos->addChild('Zugriffe',$Beitrag->num_views);
            $Infos->addChild('Antworten',$Beitrag->num_replies);
            $Infos->addChild('LetzterEintrag',date('d.m.Y H:i',(string)$Beitrag->posted));

            $SQL .= ','.$DB->FeldInhaltFormat('T',$Infos->asXML());
            $SQL .= ', \'Import\'';
            $SQL .= ', SYSDATE)';

            $DB->Ausfuehren($SQL);
        }
    }
}
?>
