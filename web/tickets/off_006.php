<?php
/**
 * Formular: 06-personalisiertes Postfach Filialmitarbeiter
 * @author Tobias Sch�ffler
 *
 */


$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'TTT_*');
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());
$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'lst_*');
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'txt_*');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextFormular = $Form->LadeTexte($TextKonserven);

//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$SQL = ' select ADE_MAIL, ADE_SURNAME || COALESCE(\', \'||ADE_GIVENNAME,\'\') AS KON_NAME from ACTIVEDIRECTORYEXPORT ';

$Block='100';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_BENACHRICHTIGUNG'], 299,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BENACHRICHTIGUNG']);
$Form->Erstelle_MehrfachSelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn',isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn']:array(),'450:450',true,$SQL,'','','','','');
$Form->ZeileEnde();

//**************************************************
// Personalisiertes Postfach
//**************************************************

$Daten = explode('|',$TextFormular['OTF_FRM_'.$FormularKennung]['lst_PersPostfach']);

$Block='110';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALISIERT'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_PERPOSTFACH']);
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-PersPostfach',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PersPostfach'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PersPostfach']:''),'130:120',true,'','~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','',$Daten);
$Form->ZeileEnde();

$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Daten = explode('|',$TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion']);
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTION'], 225, '', '', '');
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Funktion',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion']:''),'130:118',true,'','~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),'','','','BE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONSONSTIGES'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Freitext', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']:''), 50, 550, true, '', '', '', 'T','','','','','','','','BE');
$Form->ZeileEnde();
$Block='115';
$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']:''), 10, 80, true, '', '', '', 'T','','','','','','','','BE');
$Form->ZeileEnde();

//Hole Vertriebsleiter; Omnitracker braucht keine Personalnummer sondern nur Vor- und Nachname
$SQL = "select kon_name1 || COALESCE(', '|| kon_name2,'') as kon_name_1, kon_name1 || COALESCE(', '|| kon_name2,'') as kon_name_1 from kontakte";
$SQL .= " where kon_key in (";
$SQL .= "    select frz_kon_key from FILIALEBENENROLLENZUORDNUNGEN";
$SQL .= "    where frz_xtn_kuerzel = 'FEB'";
$SQL .= "    and FRZ_FER_KEY = 25";
$SQL .= "    and sysdate <= frz_gueltigbis)";
$SQL .= " and kon_status = 'A'";
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_VERTRIEBSLEITER'], 225, '', '', '');
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Vertriebsleiter'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Vertriebsleiter']:''),'230:230',true,$SQL,'~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','AWIS','','BE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLE'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']:''), 15, 500, true, '', '', '', 'T','','','',0,'','','','BE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLENDATEI'], 225, '', '', '');
$Form->Erstelle_Bild('href','*LINK','/dokumentanzeigen.php?bereich=itanforderungen&dateiname=KST-Liste_untereinander&erweiterung=xlsx','../bilder/icon_excel.png');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');
$Block='110';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALE'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Filiale', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']:''), 15, 500, true, '', '', '', 'T','','','',4,'','','','BE');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');

echo '<script type="text/javascript">'.PHP_EOL;
echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;
echo '</script>'.PHP_EOL;

$Block = '120';
$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALEALT'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt']:''), 30, 250, true, '', '', '', 'T','','','',4,'','','','AE');
$Form->Erstelle_TextLabel('&nbsp;', 150, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALENEU'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu']:''), 30, 250, true, '', '', '', 'T','','','',4,'','','','AE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONALT'], 225, '', '', '');
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FunktionAlt'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FunktionAlt']:''),'130:118',true,'','~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),'','','','AE');
$Form->Erstelle_TextLabel('&nbsp;', 270, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONNEU'], 225, '', '', '');
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FunktionNeu'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-FunktionNeu']:''),'130:118',true,'','~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),'','','','AE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONSONSTIGESALT'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextAlt']:''), 50, 400, true, '', '', '', 'T','','','','','','','','AE');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONSONSTIGESNEU'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FreitextNeu']:''), 50, 550, true, '', '', '', 'T','','','','','','','','AE');
$Form->ZeileEnde();
$Block='125';
$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']:''), 10, 80, true, '', '', '', 'T','','','','','','','','AE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLENEU'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu']:''), 15, 500, true, '', '', '', 'T','','','',0,'','','','AE');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLENDATEI'], 225, '', '', '');
$Form->Erstelle_Bild('href','*LINK','/dokumentanzeigen.php?bereich=itanforderungen&dateiname=KST-Liste_untereinander&erweiterung=xlsx','../bilder/icon_excel.png');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');
$Form->SchreibeHTMLCode('</div>');
$Block = '120';
echo '<script type="text/javascript">'.PHP_EOL;
echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;
echo '</script>'.PHP_EOL;

$Block = '130';
$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Daten = explode('|',$TextFormular['OTF_FRM_'.$FormularKennung]['lst_Funktion']);
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTION'], 225, '', '', '');
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Funktion',(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion']:''),'130:118',true,'','~'.$TextFormular['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),'','','','LO');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FUNKTIONSONSTIGES'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Freitext', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext']:''), 50, 550, true, '', '', '', 'T','','','','','','','','LO');
$Form->ZeileEnde();
$Block='135';
$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_PERSONALNUMMER'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer']:''), 10, 80, true, '', '', '', 'T','','','','','','','','LO');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLE'], 225, '', '', '');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle']:''), 15, 500, true, '', '', '', 'T','','','',0,'','','','LO');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_KOSTENSTELLENDATEI'], 225, '', '', '');
$Form->Erstelle_Bild('href','*LINK','/dokumentanzeigen.php?bereich=itanforderungen&dateiname=KST-Liste_untereinander&erweiterung=xlsx','../bilder/icon_excel.png');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');
$Block = '130';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_FILIALE'], 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Filiale', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale']:''), 15, 500, true, '', '', '', 'T','','','',4,'','','','LO');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');

echo '<script type="text/javascript">'.PHP_EOL;
echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;
$Block = '110';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-PersPostfach").change(function(){'.PHP_EOL;
echo 'var angefordert = $(this).val();'.PHP_EOL;
echo 'if(angefordert == "")'.PHP_EOL; //nichts ausgew�hlt
echo '{'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
$Block = '120';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
$Block = '130';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '} else if(angefordert == 0)'.PHP_EOL; //beantragen
echo '{'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".LO").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".LO").attr("required", false)'.PHP_EOL;
echo '$(".LO").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".LO >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".LO >input").attr("required", false)'.PHP_EOL;
echo '$(".LO >input").removeClass("InputTextPflicht")'.PHP_EOL;
$Block= '110';
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").change(function(){'.PHP_EOL;
$Block= '115';
echo 'var funktion = $(this).val();'.PHP_EOL;
echo 'if(funktion == "0") {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Vertriebsleiter").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
$Block = '120';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".AE").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".AE").attr("required", false)'.PHP_EOL;
echo '$(".AE").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".AE >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".AE >input").attr("required", false)'.PHP_EOL;
echo '$(".AE >input").removeClass("InputTextPflicht")'.PHP_EOL;
echo '} else if(angefordert == 1)'.PHP_EOL; //�ndern
echo '{'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").change(function(){'.PHP_EOL;
echo 'var funktionalt = $(this).val();'.PHP_EOL;
echo 'var funktionneu = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").val();'.PHP_EOL;
$Block = '125';
echo 'if(funktionalt == "0" || funktionneu == "0") {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
$Block = '120';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").change(function(){'.PHP_EOL;
echo 'var funktionneu = $(this).val();'.PHP_EOL;
echo 'var funktionalt = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").val();'.PHP_EOL;
$Block= '125';
echo 'if(funktionalt == "0" || funktionneu == "0") {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-KostenstelleNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
$Block = '110';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".BE >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".BE").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".BE >input").attr("required", false)'.PHP_EOL;
echo '$(".BE >input").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".BE").attr("required", false)'.PHP_EOL;
echo '$(".BE").removeClass("InputTextPflicht")'.PHP_EOL;
$Block = '130';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".LO").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".LO").attr("required", false)'.PHP_EOL;
echo '$(".LO").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".LO >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".LO >input").attr("required", false)'.PHP_EOL;
echo '$(".LO >input").removeClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL; //l�schen
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").change(function(){'.PHP_EOL;
$Block= '135';
echo 'var funktion = $(this).val();'.PHP_EOL;
echo 'if(funktion == "0") {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '$(".Block'.$Block.'").show();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Personalnummer").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle").removeClass("InputTextPflicht")'.PHP_EOL;
$Block= '130';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Filiale").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '110';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".BE >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".BE").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".BE >input").attr("required", false)'.PHP_EOL;
echo '$(".BE >input").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".BE").attr("required", false)'.PHP_EOL;
echo '$(".BE").removeClass("InputTextPflicht")'.PHP_EOL;
$Block = '120';
echo '$(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '$(".Block'.$Block.'").hide();'.PHP_EOL;
echo '$(".AE").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".AE").attr("required", false)'.PHP_EOL;
echo '$(".AE").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$(".AE >input").each(function(i, obj) { $(obj).val(""); });'.PHP_EOL;
echo '$(".AE >input").attr("required", false)'.PHP_EOL;
echo '$(".AE >input").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;;
$Block = '110';
echo '$( document).ready(function() { $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-PersPostfach").change(); });';
echo '</script>'.PHP_EOL;

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion").change(function(){'.PHP_EOL;
echo 'var funktion = $(this).val();'.PHP_EOL;
echo 'if(funktion == 4)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Freitext").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;
echo '</script>'.PHP_EOL;

$Block = '120';
echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").keyup(function(){'.PHP_EOL;
echo 'var filalt = $(this).val();'.PHP_EOL;
echo 'var filneu = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").val();';
echo 'if(filalt != "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo 'if(filneu == "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;
echo '</script>'.PHP_EOL;

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").keyup(function(){'.PHP_EOL;
echo 'var filneu = $(this).val();'.PHP_EOL;
echo 'var filalt = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").val();';
echo 'if(filneu != "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo 'if(filalt == "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeAlt").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FilialeNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;
echo '</script>'.PHP_EOL;

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").change(function(){'.PHP_EOL;
echo 'var funkalt = $(this).val();'.PHP_EOL;
echo 'var funkneu = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").val();';
echo 'if(funkalt != "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo 'if(funkneu == "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;
echo '</script>'.PHP_EOL;

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").change(function(){'.PHP_EOL;
echo 'var funkneu = $(this).val();'.PHP_EOL;
echo 'var funkalt = $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").val();';
echo 'if(funkneu != "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").addClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").attr("required", true)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").addClass("InputTextPflicht")'.PHP_EOL;
echo '} else {'.PHP_EOL;
echo 'if(funkalt == "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionAlt").removeClass("InputTextPflicht")'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").attr("required", false)'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FunktionNeu").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '})'.PHP_EOL;
echo '</script>'.PHP_EOL;


//**************************************************
// Bemerkungen
//**************************************************
$Block='900';

$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_ALLGEMEIN'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEMERKUNGEN_ALLG']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']:''), 600, 60, 3, true);
$Form->ZeileEnde();
?>