<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	if(isset($_POST['txtQMM_KEY']))
	{
		if($_POST['txtQMM_KEY'] <> -1)
		{
			$SQL ='update qmmassnahmen set qmm_bezeichnung ='.$DB->FeldInhaltFormat('T',$_POST['txtQMM_BESCHREIBUNG']);
			if(!$Form->NameInArray($_POST,'txtCHK_QMM_AKTIV',1))
			{
				$SQL .= ', QMM_AKTIV=0';
			}
			else
			{
				$SQL .= ', QMM_AKTIV=1';
			}
			$SQL .= ', QMM_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', QMM_USERDAT=SYSDATE';
			$SQL .=' where qmm_key='.$DB->FeldInhaltFormat('T',$_POST['txtQMM_KEY']);
			$DB->Ausfuehren($SQL,'',true);
		}
		else
		{
			$SQL ='insert into qmmassnahmen ';
			$SQL .=' (qmm_bezeichnung,qmm_aktiv,qmm_user,qmm_userdat)';
			$SQL .=' values';
			$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtQMM_BESCHREIBUNG']);
			if(!$Form->NameInArray($_POST,'txtCHK_QMM_AKTIV',1))
			{
				$SQL .= ',0';
			}
			else
			{
				$SQL .= ',1';
			}
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
		}
	}
	
	
	else if(isset($_POST['txtQMG_KEY']))
	{
		if($_POST['txtQMG_KEY'] <> -1)
		{
			$SQL ='update qmkriteriengueltig set qmg_gueltigab ='.$DB->FeldInhaltFormat('D',$_POST['txtQMG_GUELTIGAB']);
			$SQL .=', qmg_gueltigbis ='.$DB->FeldInhaltFormat('D',$_POST['txtQMG_GUELTIGBIS']);
			$SQL .= ', QMG_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', QMG_USERDAT=SYSDATE';
			$SQL .=' where qmg_key='.$DB->FeldInhaltFormat('T',$_POST['txtQMG_KEY']);
			$DB->Ausfuehren($SQL,'',true);
		}
		else 
		{
			$SQL ='insert into qmkriteriengueltig ';
			$SQL .=' (qmg_qmk_key,qmg_gueltigab,qmg_gueltigbis,qmg_user,qmg_userdat)';
			$SQL .=' values';
			$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtQMK_SCHLUESSEL']);
			$SQL .=','.$DB->FeldInhaltFormat('D',$_POST['txtQMG_GUELTIGAB']);
			$SQL .=','.$DB->FeldInhaltFormat('D',$_POST['txtQMG_GUELTIGBIS']);
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
		}
	}
	
	else if(isset($_POST['txtQMK_KEY']))
	{
		
		if($_POST['txtQMK_KEY'] <> -1)
		{
			$SQL ='update qmkriterien set qmk_key ='.$DB->FeldInhaltFormat('T',substr($_POST['txtQMK_SCHLUESSEL'],0,50));
			$SQL .=', qmk_lang ='.$DB->FeldInhaltFormat('T',substr($_POST['txtQMK_LANG'],0,50));
			if(!$Form->NameInArray($_POST,'txtCHK_AKTIV',1))
			{
				$SQL .= ', QMK_AKTIV=0';
			}
			else 
			{
				$SQL .= ', QMK_AKTIV=1';
			}
			$SQL .= ', QMK_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', QMK_USERDAT=SYSDATE';
			$SQL .=' where qmk_key='.$DB->FeldInhaltFormat('T',$_POST['txtQMK_KEY']);
			$DB->Ausfuehren($SQL,'',true);
		}
		else 
		{
			$SQL ='insert into qmkriterien';
			$SQL .=' (qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat)';
			$SQL .=' values';
			$SQL .='('.$DB->FeldInhaltFormat('T',substr($_POST['txtQMK_SCHLUESSEL'],0,50)); 
			$SQL .=','.$DB->FeldInhaltFormat('T',substr($_POST['txtQMK_LANG'],0,50)); 
			if(!$Form->NameInArray($_POST,'txtCHK_AKTIV',1))
			{
				$SQL .= ',0';
			}
			else
			{
				$SQL .= ',1';
			}
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
			
			$SQL ='insert into qmkriteriengueltig ';
			$SQL .=' (qmg_qmk_key,qmg_gueltigab,qmg_gueltigbis,qmg_user,qmg_userdat)';
			$SQL .=' values';
			$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtQMK_SCHLUESSEL']);
			$SQL .=',to_char(sysdate,\'DD.MM.YYYY\')';
			$SQL .=',\'31.12.2030\'';
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
			
		}
	}
	else 
	{
		if(($_POST['txtQMS_FOLGESTATUS'] != '' and $_POST['txtQMS_FOLGESTATUS'] != 0) and !isset($_POST['cmdHinzufuegen_x']))  // Wenn der Status noch auf offen ist und nicht
		{																													   // Massnahme hinzuf�gen Button gedr�ckt	
			if($_POST['txtQMM_STATUS'] == 1) // Massnahmen nochmals pr�fen ob mittlerweile kurzfristig
			{
				$SQL2 ='update qmmassnahmenzuord set qmz_status = 13,qmz_userdat = sysdate ';
				$SQL2 .=' where qmz_key in (';
				$SQL2 .=' select qmz_key from qmmassnahmenzuord where qmz_qmp_key ='.$_POST['txtQMP_KEY']; 
				$SQL2 .='and trunc(qmz_frist) < trunc(sysdate) + 4)';
				$DB->Ausfuehren($SQL2,'',true);
			}
			
			
			/** Eingaben f�r Massnahmenplan speichern.
			 *  Wenn auf Massnahme versandt dann Status umsetzen.
		     **/
			$SQL = 'UPDATE QMPLAENE';
			$SQL .= ' SET QMP_FESTSTELLUNG ='.$DB->FeldInhaltFormat('T',substr($_POST['txtFESTSTELLUNG'],0,4000));
			$SQL .= ', QMP_PERSNR=\''.$_POST['txtQMM_Persnr'].'\'';
			if($_POST['txtQMM_STATUS'] == 1)
			{
				$SQL .= ', QMP_STATUS=(select qms_key from qmstatus where qms_folgestatus = 0 and qms_massnahme = 0)';
			}
			$SQL .= ', QMP_MASSNAHMEDAT=SYSDATE';
			$SQL .= ', QMP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', QMP_USERDAT=SYSDATE';
			$SQL .= ' WHERE QMP_KEY='.$_POST['txtQMP_KEY'];
			$DB->Ausfuehren($SQL,'',true);
			
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['SPEICHERN']);
			$Form->ZeileEnde();
	
			if($_POST['txtQMM_STATUS'] == 1)
			{
				
				$SQL2 ='select * from qmplaene '; //Plan holen der auf 'Ma�nahmen versandt' gesetzt wurde
				$SQL2 .=' inner join qmkriterien on qmp_qmk_key = qmk_key';
				$SQL2 .=' where qmp_key ='.$_POST['txtQMP_KEY'];
					
				$rsFill = $DB->RecordSetOeffnen($SQL2);
					
				$SQL ='SELECT';
				$SQL .='   CASE';
				$SQL .='     WHEN EMAIL_GBL IS NULL';
				$SQL .='     THEN \''.$AWISSprachKonserven['QMM']['FEHLER_MAIL'].'\'';
				$SQL .='     ELSE EMAIL_GBL';
				$SQL .='   END';
				$SQL .=' AS';
				$SQL .='   EMAIL_GBL,';
				$SQL .='     CASE';
				$SQL .='       WHEN EMAIL_GBL IS NULL';
				$SQL .='       THEN \''.$AWISSprachKonserven['QMM']['MAIL_BETREFF'].'\'';
				$SQL .='       WHEN ANZKURZ > 0 THEN (SELECT mvt_betreff FROM mailversandtexte WHERE mvt_key = 15)';
				$SQL .='       ELSE (SELECT mvt_betreff FROM mailversandtexte WHERE mvt_key = 14)';
				$SQL .='     END';
				$SQL .=' AS';
				$SQL .='   BETREFF_GBL,';
				$SQL .='    CASE';
				$SQL .='       WHEN ANZKURZ > 0 THEN (SELECT mvt_text FROM mailversandtexte WHERE mvt_key = 15)'; // Wenn Kurzfristige Massnahmen vorhanden in Text erw�hnen.
				$SQL .='       ELSE (SELECT mvt_text FROM mailversandtexte WHERE mvt_key = 14)'; // Sonst normalen Text anzeigen.
				$SQL .='     END';
				$SQL .='   EMAILTEXT_GBL';
				$SQL .='  FROM';
				$SQL .='   (SELECT';
				$SQL .='     (SELECT kko_wert';
				$SQL .='     FROM kontaktekommunikation';
				$SQL .='     WHERE kko_kon_key = rolle.kon_keygbl';
				$SQL .='     AND kko_kot_key   = 7';
				$SQL .='     AND rownum        = 1';
				$SQL .='     ) AS email_gbl,';
				$SQL .='     ( SELECT count(*) as ANZKURZ FROM qmmassnahmenzuord ';
				$SQL .='   WHERE qmz_qmp_key ='.$_POST['txtQMP_KEY'];
				$SQL .='   and qmz_status';
				$SQL .='   in (';
				$SQL .='   SELECT qms_key';
				$SQL .='   FROM qmstatus';
				$SQL .='   WHERE qms_massnahme = 1';
				$SQL .='   AND qms_folgestatus = 0';
				$SQL .='   AND (qms_frist     IS NOT NULL';
				$SQL .='   AND qms_frist       > 0))) as ANZKURZ';
				$SQL .='   FROM';
				$SQL .='     (SELECT filialrolle('.$rsFill->FeldInhalt('QMP_FIL_ID').',25,sysdate,\'kon_key\') AS kon_keygbl';
				$SQL .='     FROM dual';
				$SQL .='     ) rolle';
				$SQL .='   )';
					
				$rsMail = $DB->RecordSetOeffnen($SQL); // Email Daten f�r Meldung GBL 'Neuer Massnahmenplan'
				$HinweisText = array();                           
				$Mail = new awisMailer($DB, $AWISBenutzer);          // Mailer-Klasse instanzieren
				$Absender = 'noreply@de.atu.eu';                     // Absender der Mails 
				$Empfaenger = $rsMail->FeldInhalt('EMAIL_GBL');      // Empf�nger der Mails => Zust�ndiger GBL f�r Filiale des Plans
				$EmpfaengerCC = '';                     
				$EmpfaengerBCC = '';
				
				$Betreff = $rsMail->FeldInhalt('BETREFF_GBL'); //Betreff Massnahme versandt
				
				$Text = '';
				
				$SQL3 ='SELECT qms_frist,qms_key'; // Status f�r Kurzfristig holen
				$SQL3 .='   FROM qmstatus';
				$SQL3 .='   WHERE qms_massnahme = 1';
				$SQL3 .='   AND qms_folgestatus = 0';
				$SQL3 .='   AND (qms_frist     IS NOT NULL';
				$SQL3 .='   AND qms_frist       > 0)';
				
				$rsFrist = $DB->RecordSetOeffnen($SQL3);
				
				$SQL4 = 'select * from qmmassnahmenzuord';
				$SQL4 .= ' inner join qmmassnahmen on qmz_qmm_key = qmm_key'; 
				$SQL4 .= ' where qmz_qmp_key ='.$_POST['txtQMP_KEY'];
				$rsTab = $DB->RecordSetOeffnen($SQL4);
				
				/** Tabelle mit den Massnahmen f�r den Kopf malen.
				 *  Wenn Status gleich Kurzfristig dann Massnahme in Tabelle rot ansonsten schwarz
				 **/
				$Tabelle = '';
				$Tabelle = '<table border="1"><tr><th>Massnahme</th><th>Frist</th>';
				while(! $rsTab->EOF())
				{
					$Tabelle .= '<tr><td>';
					if($rsTab->FeldInhalt('QMZ_STATUS') == $rsFrist->FeldInhalt('QMS_KEY'))
					{
						$Tabelle .= '<font color="#ff0000">';
					}
					$Tabelle .=	$rsTab->FeldInhalt('QMM_BEZEICHNUNG');
					if($rsTab->FeldInhalt('QMZ_STATUS') == $rsFrist->FeldInhalt('QMS_KEY'))
					{
						$Tabelle .= '</font>';
					}		
					$Tabelle .= '</td>';
		    		$Tabelle .=	'<td>';
		    		if($rsTab->FeldInhalt('QMZ_STATUS') == $rsFrist->FeldInhalt('QMS_KEY'))
					{
						$Tabelle .= '<font color="#ff0000">';
					}
					$Tabelle .= $Form->Format('D',$rsTab->FeldInhalt('QMZ_FRIST'));
					if($rsTab->FeldInhalt('QMZ_STATUS') == $rsFrist->FeldInhalt('QMS_KEY'))
					{
						$Tabelle .= '</font>';
					}		
		    		$Tabelle .=	'</td></tr>';
		    		$rsTab->DSWeiter();
				}	
	  			$Tabelle .='</table>';
					
				/**
				 * Platzhalter im Emailtext ersezten
				 * Fil_ID
				 * Kriterium des Ma�nahmenplans
				 * Frist des Massnahmenplans vom Importdatum + Zeit bis zur Status�nderung
				 * Oben erstellte Tabelle mit Massnahmen unter Text einf�gen	
				 **/	
	  			$Text = str_replace('#FILIALE#','<b>'.$rsFill->FeldInhalt('QMP_FIL_ID').'</b>',$rsMail->FeldInhalt('EMAILTEXT_GBL'));
				$Text = str_replace('#KRITERIUM#','<b>\''.$rsFill->FeldInhalt('QMK_LANG').'\'</b>',$Text);
				$Text = str_replace('#FRIST#','<b>'.$rsFrist->FeldInhalt('QMS_FRIST').'</b>',$Text);
				$Text = str_replace('#TABELLE_MASS#',$Tabelle,$Text);
				
				$Mail->AnhaengeLoeschen();
				$Mail->LoescheAdressListe();
				$Mail->DebugLevel(0);
				$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
				$Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
				$Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
				$Mail->Absender($Absender);
				$Mail->Betreff($Betreff);
				$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
				$Mail->SetzeBezug('QMM',$_POST['txtQMP_KEY']); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
				$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen  
				$Mail->MailInWarteschlange();
				
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['QMM']['EMAIL']);
				$Form->ZeileEnde();
					
			}
			
			
		}
		elseif(($_POST['txtQMS_FOLGESTATUS'] == 0) and !isset($_POST['cmdHinzufuegen_x'])) // Wenn 'Ma�nahme versandt' Button f�r GBL Haken benutzen
		{
			$SQL  = 'select qmz_key from qmmassnahmenzuord ';  // Alle Massnahmen f�r ausgew�hlten Kopf holen
			$SQL .= ' inner join qmstatus on qmz_status = qms_key';
			$SQL .= ' where qmz_qmp_key ='.$_POST['txtQMP_KEY'];
			$SQL .= ' and qms_folgestatus is not null'; 
			
			$rsZuord = $DB->RecordSetOeffnen($SQL);
			while(! $rsZuord->EOF())
			{
					$SQL2 = 'UPDATE qmmassnahmenzuord';
					$SQL2 .= ' SET QMZ_USERDAT=SYSDATE';
					$SQL2 .= ', QMZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				    $SQL2 .= ', QMZ_ERFUELLT='.$_POST['txtCMB_Erfuellt_'.$rsZuord->FeldInhalt('QMZ_KEY')]; // Wenn Haken f�r Massnahme gesetzt dann Status in Tabelle auf 1 setzen	
					if(isset($_POST['txtQMZ_MASSNAHME_'.$rsZuord->FeldInhalt('QMZ_KEY')]))
					{
						$SQL2 .= ', QMZ_QMM_KEY='.$_POST['txtQMZ_MASSNAHME_'.$rsZuord->FeldInhalt('QMZ_KEY')]; 
					}
					if(isset($_POST['txtQMZ_BEMERKUNG_'.$rsZuord->FeldInhalt('QMZ_KEY')]))
					{
						$SQL2 .= ', QMZ_BEMERKUNG='.$DB->FeldInhaltFormat('T',$_POST['txtQMZ_BEMERKUNG_'.$rsZuord->FeldInhalt('QMZ_KEY')]);
					}
					$SQL2 .= ' WHERE QMZ_KEY='.$rsZuord->FeldInhalt('QMZ_KEY');
					$DB->Ausfuehren($SQL2,'',true);
					$rsZuord->DSWeiter();
					
			}
			
			if(isset($_POST['txtFESTSTELLUNG']))
			{
				/** Eingaben f�r Massnahmenplan speichern.
				 *  Wenn auf Massnahme versandt dann Status umsetzen.
				 **/
				$SQL = 'UPDATE QMPLAENE';
				$SQL .= ' SET QMP_FESTSTELLUNG ='.$DB->FeldInhaltFormat('T',substr($_POST['txtFESTSTELLUNG'],0,4000));
				$SQL .= ', QMP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', QMP_USERDAT=SYSDATE';
				$SQL .= ' WHERE QMP_KEY='.$_POST['txtQMP_KEY'];
				$DB->Ausfuehren($SQL,'',true);
			}
			
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['SPEICHERN']);
			$Form->ZeileEnde();
		}
		else // �brig bleibt der + Button f�r die Massnahmen
		{
			$SQL ='insert into qmmassnahmenzuord';
			$SQL .=' (qmz_qmp_key,qmz_qmm_key,qmz_status,qmz_frist,qmz_bemerkung,qmz_user,qmz_userdat)';
			$SQL .=' values';
			$SQL .='('.$_POST['txtQMP_KEY']; // Key vom Kopf mitnehmen
			$SQL .=','.$_POST['txtQMM_MASSNAHME']; // Ausgew�hlte Massnahme aus der Maske eintragen.
			if((strtotime($_POST['txtQMM_FRIST']) < strtotime(date('d.m.Y',time()+345600))))
			{
				$SQL .=',13'; // Wenn Frist nicht l�nger als vier Tage dann 'Kurzfristig'
			}
			else 
			{
				$SQL .=',10'; // Sonst 'Ma�nahme offen'
			}
			$SQL .=',\''.$Form->Format('D',$_POST['txtQMM_FRIST']).'\''; // Frist aus Maske eintragen
			$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtQMZ_BESCHREIBUNG']); // Beschreibungstext aus der Maske eintragen.
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
		
		}
	}
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_QMZ_ZUORD) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Form->Hinweistext('Der einzuf�gende Datensatz befindet sich schon in der Datenbank');
	}
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_QMK_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 4;
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
