<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');

/*
 * Anforderung A00002 - Lieferantenartikel durch neue Lieferantenartikel(nummern) ersetzen
 * Verh�ltnis LAR_LARTNR_ALT zu LAR_LARTNR_NEU = 1 zu 1
 * 
 * Festlegungen in Absprache mit dem Anforderer:
 * Alte Lieferantenartikel, wenn vorhanden, werden immer auf Auslauf gesetzt
 * Neue Lieferantenartikel werden, wenn alte vorhanden, neu angelegt
 * Wenn alte Teileinfoverkn�pfung AST/LAR(alt) vorhanden, wird AST/LAR(neu) neu angelegt
 * Nichtvorhandene (alte) Lieferantenartikelnummern werden �bersprungen
 * Nichtvorhandene Teileinfos AST/LAR(alt) werden �bersprungen
 * 
*/

function Protokoll($fp, $ProtArray)
{
	$zeile  = $ProtArray['LAR_LARTNR_ALT'] .';';
	$zeile .= $ProtArray['AST_ATUNR'] .';';
	$zeile .= $ProtArray['LAR_LARTNR_NEU'] .';';
	$zeile .= $ProtArray['KATEGORIE'] .';';
	$zeile .= $ProtArray['AKTION'] .';';
	$zeile .= $ProtArray['SQL'] .";\n";
	fputs($fp,$zeile);
	
	return 0;
}

try
{
	$Anforderung = 'A00002';
	$Anforderer = 'Schmidberger';
	$QuellTabelle = 'XXX_'.$Anforderung;
	$Lieferant = '1274';
	$Bemerkung = "Neue OJD-Nummer lt. Datei ATU New OJD Refrences.xlsx 17.02.12  SN";

	//$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20120224 Staging PROTOKOLL.csv';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20120224 Produktiv PROTOKOLL.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$zeile = "LAR_LARTNR_ALT;AST_ATUNR;LAR_LARTNR_NEU;KATEGORIE;AKTION;SQL;\n";
	fputs($fp,$zeile);
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	// alle zu ersetzenden LARTNR aus temp. Tabelle laden
	$SQL = 'SELECT * FROM '.$QuellTabelle;
	$SQL .= ' WHERE erfolgreich IS NULL';
//	$SQL .= ' OR erfolgreich = 0';
	
	$rsQuelle = $DB->RecordSetOeffnen($SQL);
	
	$DB->TransaktionBegin();
	
	while (!$rsQuelle->EOF())
	{
		$ProtokollArray = array();
		$ProtokollArray['LAR_LARTNR_ALT'] = $rsQuelle->Feldinhalt('LAR_LARTNR_ALT');
		$ProtokollArray['AST_ATUNR'] = $rsQuelle->Feldinhalt('AST_ATUNR');
		$ProtokollArray['LAR_LARTNR_NEU'] = $rsQuelle->Feldinhalt('LAR_LARTNR_NEU');
		$ProtokollArray['KATEGORIE'] = '';
		$ProtokollArray['AKTION'] = '';
		$ProtokollArray['SQL'] = '';
		
		//Gibt es Eintr�ge im Lieferantenartikel f�r LAR_LARTNR_ALT und Lieferant xy
		$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
		$SQL .= ' WHERE LAR_SUCH_LARTNR ' .$DB->LIKEoderIST($rsQuelle->Feldinhalt('LAR_LARTNR_ALT'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
		$SQL .= ' AND LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$Lieferant, false);

		$ProtokollArray['AKTION'] = 'Laden Lieferantenartikel alte Nummer';
		$ProtokollArray['SQL'] = $SQL; 		
		$ProtokollArray['KATEGORIE'] = 'INFO'; 		
		Protokoll($fp, $ProtokollArray);
		
		$rsLAR_ALT = $DB->RecordSetOeffnen($SQL);
		
		if (!$rsLAR_ALT->EOF())
		{
			while (!$rsLAR_ALT->EOF())
			{
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				if (($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN') == '') or is_null($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN')))
				{
					$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$Bemerkung, false) .',';
				}
				else if (stripos($rsLAR_ALT->Feldinhalt('LAR_BEMERKUNGEN'),$Bemerkung) === false)
				{
					$SQL .= ' LAR_BEMERKUNGEN = LAR_BEMERKUNGEN || \' \' || '.$DB->FeldInhaltFormat('T',$Bemerkung, false) .',';
				}
				$SQL .= ' LAR_AUSLAUFARTIKEL=1';
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsLAR_ALT->Feldinhalt('LAR_KEY'));
				
				$ProtokollArray['AKTION'] = 'Datensatz gefunden, Update Lieferantenartikel Auslauf und Bemerkung';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UPDATE'; 		
				Protokoll($fp, $ProtokollArray);
				
				$DB->Ausfuehren($SQL,'',true);
				
				//undo Datensatz protokollieren
				$SQL = 'UPDATE LIEFERANTENARTIKEL SET';
				$SQL .= ' LAR_BEMERKUNGEN = ' .$DB->FeldInhaltFormat('T',$rsLAR_ALT->Feldinhalt('LAR_BEMERKUNG'), false) .',';
				$SQL .= ' LAR_AUSLAUFARTIKEL = ' .$DB->FeldInhaltFormat('Z',$rsLAR_ALT->Feldinhalt('LAR_AUSLAUFARTIKEL'), false) .',';
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsLAR_ALT->Feldinhalt('LAR_KEY'));

				$ProtokollArray['AKTION'] = 'Undo Update Lieferantenartikel Auslauf und Bemerkung';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
				Protokoll($fp, $ProtokollArray);
				
				//Gibt es Eintr�ge in Teileinfos f�r AST/LAR ALTE LAR_LARTNR
				$SQL = 'SELECT * FROM TEILEINFOS';
				$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND';
				$SQL .= ' TEI_ITY_ID2 = \'LAR\' AND';
				$SQL .= ' TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsLAR_ALT->Feldinhalt('LAR_KEY'));

				$ProtokollArray['AKTION'] = 'Laden Teileinfos Kombination AST/LAR alte Nummer';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'INFO'; 		
				Protokoll($fp, $ProtokollArray);
				
				$rsTEI_ALT = $DB->RecordSetOeffnen($SQL);
				
				if (!$rsTEI_ALT->EOF()) //Mind. ein alter Teileinfosatz vorhanden
				{
					while (!$rsTEI_ALT->EOF())
					{
						$ProtokollArray['AKTION'] = 'Gefunden Teileinfos Kombination AST/LAR alte Nummer';
						$ProtokollArray['AKTION'] .= ' TEI_KEY=' .$rsTEI_ALT->FeldInhalt('TEI_KEY');
						$ProtokollArray['SQL'] = '';
						$ProtokollArray['KATEGORIE'] = 'INFO'; 		
						Protokoll($fp, $ProtokollArray);
						
						//Gibt es einen Eintrag im Lieferantenartikel f�r LAR_LARTNR_NEU und Lieferant xy
							
						$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
						$SQL .= ' WHERE LAR_SUCH_LARTNR ' .$DB->LIKEoderIST($rsQuelle->Feldinhalt('LAR_LARTNR_NEU'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
						$SQL .= ' AND LAR_LIE_NR = '.$DB->FeldInhaltFormat('T',$Lieferant, false);

						$ProtokollArray['AKTION'] = 'Laden Lieferantenartikel neue Nummer';
						$ProtokollArray['SQL'] = $SQL; 		
						$ProtokollArray['KATEGORIE'] = 'INFO'; 		
						Protokoll($fp, $ProtokollArray);
						
						$rsLAR_NEU = $DB->RecordSetOeffnen($SQL);
						
                        if($rsLAR_NEU->EOF()) //Neuen DS anlegen
                        {                           
							$SQLINS = 'INSERT INTO LIEFERANTENARTIKEL (';
							$SQLINS .= 'LAR_LARTNR,';
							$SQLINS .= 'LAR_LIE_NR,';
							$SQLINS .= 'LAR_BEMERKUNGEN,';
							$SQLINS .= 'LAR_BEKANNTWW,';
							$SQLINS .= 'LAR_IMQ_ID,';
							$SQLINS .= 'LAR_SUCH_LARTNR,';
							$SQLINS .= 'LAR_USER,';
							$SQLINS .= 'LAR_USERDAT';
							$SQLINS .= ') VALUES (';
							$SQLINS .= $DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_LARTNR_NEU'),false) .',';
							$SQLINS .= $DB->FeldInhaltFormat('T',$Lieferant,false) .',';
							$SQLINS .= $DB->FeldInhaltFormat('T',$Bemerkung,false) .',';
							$SQLINS .= '0,';
							$SQLINS .= '4,';
							$SQLINS .= 'asciiwort(' .$DB->FeldInhaltFormat('TU',$rsQuelle->Feldinhalt('LAR_LARTNR_NEU'),false) .'),';
							$SQLINS .= $DB->FeldInhaltFormat('T',$Anforderer,false) .',';
							$SQLINS .= 'SYSDATE';
							$SQLINS .= ')';
								
							$ProtokollArray['AKTION'] = 'Datensatz nicht gefunden, Insert Lieferantenartikel neue Nummer';
							$ProtokollArray['SQL'] = $SQLINS; 		
							$ProtokollArray['KATEGORIE'] = 'INSERT'; 		
							Protokoll($fp, $ProtokollArray);
							
							$DB->Ausfuehren($SQLINS,'',true);

							$ProtokollArray['AKTION'] = 'Laden Lieferantenartikel neue Nummer nochmal';
							$ProtokollArray['SQL'] = $SQL; 		
							$ProtokollArray['KATEGORIE'] = 'INFO'; 		
							Protokoll($fp, $ProtokollArray);
								
							//nach Insert jetzt nochmal den LiefDS f�r weitere Bearbeitung laden
							$rsLAR_NEU = $DB->RecordSetOeffnen($SQL);
							
							if (!$rsLAR_NEU->EOF())
							{
								$SQL  = 'DELETE FROM LIEFERANTENARTIKEL';
								$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsLAR_NEU->Feldinhalt('LAR_KEY'));
								$ProtokollArray['AKTION'] = 'Undo Insert Lieferantenartikel neue Nummer';
								$ProtokollArray['SQL'] = $SQL; 		
								$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
								Protokoll($fp, $ProtokollArray);
							}
							
						} //endif

						while (!$rsLAR_NEU->EOF())
						{
                           	//gibt es einen Teileinfosatz f�r Kombination alter AST/neuer LAR?
							$SQL = 'SELECT * FROM TEILEINFOS ';
							$SQL .= 'WHERE TEI_ITY_ID1 = \'AST\' AND ';
							$SQL .= 'TEI_ITY_ID2 = \'LAR\' AND ';
							$SQL .= 'TEI_KEY1 = ' .$DB->FeldInhaltFormat('Z',$rsTEI_ALT->FeldInhalt('TEI_KEY1')) .' AND ';
							$SQL .= 'TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsLAR_NEU->Feldinhalt('LAR_KEY'));
								
							$ProtokollArray['AKTION'] = 'Laden Teileinfos Kombination AST/LAR neue Nummer';
							$ProtokollArray['SQL'] = $SQL; 		
							$ProtokollArray['KATEGORIE'] = 'INFO'; 		
							Protokoll($fp, $ProtokollArray);
							
							$rsTEI_NEU = $DB->RecordSetOeffnen($SQL);

							if ($rsTEI_NEU->EOF()) //kein TEI vorhanden -> neuen anlegen
							{
								$SQL = 'INSERT INTO TEILEINFOS (';
								$SQL .= 'TEI_ITY_ID1,';
								$SQL .= 'TEI_KEY1,';
								$SQL .= 'TEI_SUCH1,';
								$SQL .= 'TEI_WERT1,';
								$SQL .= 'TEI_ITY_ID2,';
								$SQL .= 'TEI_KEY2,';
								$SQL .= 'TEI_SUCH2,';
								$SQL .= 'TEI_WERT2,';
								$SQL .= 'TEI_USER,';
								$SQL .= 'TEI_USERDAT';
								$SQL .= ') VALUES (';
								// AST Teil = alte Werte �bernehmen
								$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_ITY_ID1')).',';
								$SQL .= $DB->FeldInhaltFormat('Z',$rsTEI_ALT->Feldinhalt('TEI_KEY1')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_SUCH1')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_WERT1')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$rsTEI_ALT->Feldinhalt('TEI_ITY_ID2')).',';
								// LAR Teil = neue Werte bef�llen
								$SQL .= $DB->FeldInhaltFormat('Z',$rsLAR_NEU->Feldinhalt('LAR_KEY')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$rsLAR_NEU->Feldinhalt('LAR_SUCH_LARTNR')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$rsLAR_NEU->Feldinhalt('LAR_LARTNR')).',';
								$SQL .= $DB->FeldInhaltFormat('T',$Anforderer,false) .',';
								$SQL .= 'SYSDATE';
								$SQL .= ')';

								$ProtokollArray['AKTION'] = 'Datensatz nicht gefunden, Insert Teileinfos Kombination AST/LAR neue Nummer';
								$ProtokollArray['SQL'] = $SQL; 		
								$ProtokollArray['KATEGORIE'] = 'INSERT'; 		
								Protokoll($fp, $ProtokollArray);
								
								$DB->Ausfuehren($SQL,'',true);
								
								$SQL = "SELECT seq_TEI_KEY.CurrVal AS KEY FROM DUAL";
								$rsSeqTEIKey = $DB->RecordSetOeffnen ($SQL);

								$SQL  = 'DELETE FROM TEILEINFOS';
								$SQL .= ' WHERE TEI_KEY = ' .$DB->FeldInhaltFormat('Z',$rsSeqTEIKey->FeldInhalt('KEY'));
								$ProtokollArray['AKTION'] = 'Undo Insert Teileinfos Kombination AST/LAR neue Nummer';
								$ProtokollArray['SQL'] = $SQL; 		
								$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
								Protokoll($fp, $ProtokollArray);
							}
							else
							{
								//nichts machen
								$ProtokollArray['AKTION'] = 'Gefunden Teileinfos Kombination AST/LAR neue Nummer, nichts machen';
								$ProtokollArray['SQL'] = '';
								$ProtokollArray['KATEGORIE'] = 'INFO'; 		
								Protokoll($fp, $ProtokollArray);
							} //endif

							$rsLAR_NEU->DSWeiter();
									
						} //end while
						
						$rsTEI_ALT->DSWeiter();
					} //end while
									
					$SQL = 'UPDATE ' .$QuellTabelle;
					$SQL .= ' SET erfolgreich = 1 ';
					$SQL .= ' WHERE LAR_LARTNR_ALT = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR_ALT'),false);
					$DB->Ausfuehren($SQL,'',true);

					$ProtokollArray['AKTION'] = 'Vorgang erfolgreich abgeschlossen';
					$ProtokollArray['SQL'] = $SQL; 		
					$ProtokollArray['KATEGORIE'] = 'OK'; 		
					Protokoll($fp, $ProtokollArray);
				}
				else
				{
					$ProtokollArray['AKTION'] = 'Fehler, Keinen Teileinfosatz gefunden';
					$ProtokollArray['SQL'] = $SQL; 		
					$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
					Protokoll($fp, $ProtokollArray);

					$SQL = 'UPDATE ' .$QuellTabelle;
					$SQL .= ' SET erfolgreich = 0 ';
					$SQL .= ' WHERE LAR_LARTNR_ALT = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR_ALT'),false);
					$DB->Ausfuehren($SQL,'',true);
				}									
				
				$rsLAR_ALT->DSWeiter();
			}
		}
		else
		{
			$ProtokollArray['AKTION'] = 'Fehler, Lieferantenartikel alte Nummer nicht gefunden';
			$ProtokollArray['SQL'] = $SQL; 
			$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
			Protokoll($fp, $ProtokollArray);

			$SQL = 'UPDATE ' .$QuellTabelle;
			$SQL .= ' SET erfolgreich = 0 ';
			$SQL .= ' WHERE LAR_LARTNR_ALT = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR_ALT'),false);
			$DB->Ausfuehren($SQL,'',true);
		}

		$rsQuelle->DSWeiter();
		
	} //end while

	//$DB->TransaktionRollback();
	$DB->TransaktionCommit();
	$DB->Schliessen();
	fclose($fp);
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}

?>
