<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $ZPL;
$DetailAnsicht = false;
$Speichern = false;


//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['ZPL_KEY'])) {
    $AWIS_KEY1 = $_GET['ZPL_KEY'];
} elseif (isset($_POST['txtZPL_KEY'])) {
    $AWIS_KEY1 = $_POST['txtZPL_KEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $AWIS_KEY1 = -1;
    $_POST = array(); //Falls in nem bestehenden Datensatz auf "Neu" geklickt wurde, muss der POST geleert werden, da diese dann in die Felder geschrieben werden
}
if($AWIS_KEY1!=''){
    $ZPL->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
}

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $ZPL->Param = array();
    $ZPL->Param['ZPL_BEZEICHNUNG'] = $_POST['sucZPL_BEZEICHNUNG'];
    $ZPL->Param['ZPL_LIE_NR'] = $_POST['sucZPL_LIE_NR'];

    $ZPL->Param['KEY'] = '';
    $ZPL->Param['WHERE'] = '';
    $ZPL->Param['ORDER'] = '';
    $ZPL->Param['BLOCK'] = 1;
    $ZPL->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./preislisten_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./preislisten_speichern.php');
}elseif(isset($_GET['Liste'])){
    //Liste anzeigen
    $AWIS_KEY1 = '';
}
else{ //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $ZPL->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($ZPL->Param['ORDER']) AND $ZPL->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $ZPL->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY ZPL_USERDAT DESC';
        $ZPL->Param['ORDER'] = ' ZPL_USERDAT DESC ';
    }
} else {
    $ZPL->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $ZPL->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $ZPL->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL  ='SELECT ';
$SQL .='  ZPL_KEY,';
$SQL .='   ZPL_LIE_NR,';
$SQL .='   ZPL_PFAD,';
$SQL .='   ZPL_BEZEICHNUNG,';
$SQL .='   ZPL_STATUS,';
$SQL .='   ZPL_USER,';
$SQL .='   ZPL_USERDAT,';
$SQL .= ' row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .=' FROM ZUKAUFPREISLISTEN ';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $ZPL->Form->Format('N0', $_REQUEST['Block'], false);
        $ZPL->Param['BLOCK'] = $Block;
    } elseif (isset($ZPL->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $ZPL->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $ZPL->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $ZPL->DBi->ErmittleZeilenAnzahl($SQL, $ZPL->DBi->Bindevariablen('ZPL', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $ZPL->DBi->WertSetzen('ZPL', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $ZPL->DBi->WertSetzen('ZPL', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsZPL = $ZPL->DBi->RecordsetOeffnen($SQL, $ZPL->DBi->Bindevariablen('ZPL', true));
$ZPL->Form->DebugAusgabe(1, $ZPL->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$ZPL->Form->Formular_Start();
echo '<form name=frmpreislisten action=./preislisten_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data" >';

if ($rsZPL->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $ZPL->Form->Hinweistext($ZPL->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsZPL->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['ZPL_LIE_NR'] = 150;
    $FeldBreiten['ZPL_BEZEICHNUNG'] = 150;
    $FeldBreiten['ZPL_STATUS'] = 150;
    
    $ZPL->Form->ZeileStart();
    $Link = './preislisten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=ZPL_LIE_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ZPL_LIE_NR'))?'~':'');
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['ZPL_LIE_NR'], $FeldBreiten['ZPL_LIE_NR'], '', $Link);

    $Link = './preislisten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=ZPL_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ZPL_BEZEICHNUNG'))?'~':'');
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['ZPL_BEZEICHNUNG'], $FeldBreiten['ZPL_BEZEICHNUNG'], '', $Link);

    $Link = './preislisten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=ZPL_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ZPL_STATUS'))?'~':'');
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['ZPL_STATUS'], $FeldBreiten['ZPL_STATUS'], '', $Link);

    $ZPL->Form->ZeileEnde();

    $DS = 0;
    while (!$rsZPL->EOF()) {
        $HG = ($DS % 2);
        $ZPL->Form->ZeileStart();
        $Link = './preislisten_Main.php?cmdAktion=Details&ZPL_KEY=0' . $rsZPL->FeldInhalt('ZPL_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $ZPL->Form->Erstelle_ListenFeld('ZPL_LIE_NR', $rsZPL->FeldInhalt('ZPL_LIE_NR'), 0, $FeldBreiten['ZPL_LIE_NR'], false, $HG, '', $Link, 'T', 'L',$rsZPL->FeldInhalt('ZPL_LIE_NR'));
        $ZPL->Form->Erstelle_ListenFeld('ZPL_BEZEICHNUNG', $rsZPL->FeldInhalt('ZPL_BEZEICHNUNG'), 0, $FeldBreiten['ZPL_BEZEICHNUNG'], false, $HG, '', '', 'T', 'L',$rsZPL->FeldInhalt('ZPL_BEZEICHNUNG'));
        $ZPL->Form->Erstelle_ListenFeld('ZPL_STATUS', $rsZPL->FeldInhalt('ZPL_STATUS'), 0, $FeldBreiten['ZPL_STATUS'], false, $HG, '', '', 'T', 'L',$rsZPL->FeldInhalt('ZPL_STATUS'));

        $ZPL->Form->ZeileEnde();

        $rsZPL->DSWeiter();
        $DS++;
    }

    $Link = './preislisten_Main.php?cmdAktion=Details';
    $ZPL->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    if($rsZPL->FeldInhalt('ZPL_KEY')!=''){
        $AWIS_KEY1 = $rsZPL->FeldInhalt('ZPL_KEY');
    }

    $Speichern = true;
    $ZPL->Form->Erstelle_HiddenFeld('ZPL_KEY', $AWIS_KEY1);

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./preislisten_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $ZPL->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsZPL->FeldInhalt('ZPL_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsZPL->FeldInhalt('ZPL_USERDAT'));
    $ZPL->Form->InfoZeile($Felder, '');

    $EditRecht = (($ZPL->Recht10070 & 2) != 0);

    if($AWIS_KEY1>0){
        $EditRecht = false;
    }



    //Wenn die Preisliste noch nicht verarbeitet ist, dann den Kopf ein wenig detailierter Anzeigen
    if($rsZPL->FeldInhalt('ZPL_STATUS')<=1){
        $ZPL->Form->ZeileStart();
        $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL']['ZPL_BEZEICHNUNG'] . ':', 170);
        $ZPL->Form->Erstelle_TextFeld('!ZPL_BEZEICHNUNG', $rsZPL->FeldOderPOST('ZPL_BEZEICHNUNG'), 15, 300, $EditRecht, '', '', '', 'T', '', '');
        $ZPL->Form->ZeileEnde();

        if($AWIS_KEY1==-1){
            $NeuerPfad = $ZPL->AWISBenutzer->ParameterLesen('ZukaufpreislistenPfad') .  substr(time(),2);
            mkdir($NeuerPfad);
        }else{
            $NeuerPfad = $rsZPL->FeldInhalt('ZPL_PFAD');
        }

        $ZPL->Form->ZeileStart();
        $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL']['ZPL_PFAD'] . ':', 170);
        $ZPL->Form->Erstelle_TextLabel($NeuerPfad,200);
        $ZPL->Form->Erstelle_HiddenFeld('ZPL_PFAD',$NeuerPfad);
        $ZPL->Form->ZeileEnde();


        $ZPL->Form->ZeileStart();
        $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL']['ZPL_LIE_NR'] . ':', 170);
        $ZPL->Form->Erstelle_TextFeld('!ZPL_LIE_NR', $rsZPL->FeldOderPOST('ZPL_LIE_NR'), 12, 150, $EditRecht, '', '', '', 'T', '', '');
        $ZPL->Form->ZeileEnde();

    }else{ //Verarbeitete Preisliste
        $ZPL->Form->Erstelle_TextLabel($rsZPL->FeldOderPOST('ZPL_BEZEICHNUNG'), 170,'font-weight: boldest');
        if($ZPL->Recht10071){
            $Register = new awisRegister(10071);
            $Register->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
        }
    }



    echo '<script>';
    echo 'function executeQuery() {'.PHP_EOL;
    echo '    ZPL_PROTOKOLL(this);'.PHP_EOL;
    echo '    setTimeout(executeQuery, 30000); // Nach dem ersten lauf, alle 30 Sekunden wieder aufrufen'.PHP_EOL;
    echo '}'.PHP_EOL;
    echo ''.PHP_EOL;
    echo '$(document).ready(function () {'.PHP_EOL;
    echo '    // Nach 30 Sekunden das erste mal aufrufen'.PHP_EOL;
    echo '    setTimeout(executeQuery, 30000);'.PHP_EOL;
    echo '});'.PHP_EOL;
    echo '</script>'.PHP_EOL;

    $ZPL->Form->FormularBereichStart();
    $ZPL->Form->FormularBereichInhaltStart('Protokoll',false,10,'ZPL_PROTOKOLL','box1',false,array('txtZPL_KEY'));
    $ZPL->Form->AuswahlBoxHuelle('box1');
    $ZPL->Form->FormularBereichInhaltEnde();
    $ZPL->Form->FormularBereichEnde();

}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$ZPL->Form->Formular_Ende();
$ZPL->Form->SchaltflaechenStart();

$ZPL->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $ZPL->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($ZPL->Recht10070 & 2) ==2 AND $Speichern) { //Speichern erlaubt + in einen Datensatz?
    $ZPL->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $ZPL->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}

if (($ZPL->Recht10070 & 4) == 4 AND $AWIS_KEY1!=-1) {// Hinzuf�gen erlaubt + nicht gerade in einer Neuanlage?
    $ZPL->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $ZPL->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

if (($ZPL->Recht10070 & 64) == 64 AND  $AWIS_KEY1>0) { //L�schen erlaubt + in einen Datensatz?
    $ZPL->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $ZPL->AWISSprachKonserven['Wort']['lbl_loeschen'], '');
}

$ZPL->Form->SchaltflaechenEnde();

$ZPL->Form->SchreibeHTMLCode('</form>');

?>