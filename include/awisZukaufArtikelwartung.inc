<?php
/**
 * Klasse f�r die Zukaufwartungen
*
* @author Sacha Kerres
* @version 201506
* @copyright ATU Auto Teile Unger
*
*/
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisMailer.inc');
require_once('awisProtokoll.inc');


class awisZukaufArtikelwartung
{
    /**
     * Datenbankverbindung
     *
     * @var awisDatenbank
     */
    protected $_DB = null;
    /**
     * AWIS Benutzer
     * @var awisBenutzer
     */
    protected $_AWISBenutzer = null;
    /**
     * Debugausgaben
     *
     * @var int
     */
    protected $_DebugLevel = 0;
    
    /**
     * Ziel-E-Mail bei Preisabweichungen
     *
     * @var string
     */
    protected $_EMAIL_PreisProblem = array('shuttle@de.atu.eu','zukauf@de.atu.eu');
    
    /**
     * Empf�nger f�r Fehler-Mails
     *
     * @var string
    */
    protected $_EMAIL_Fehler = array('shuttle@de.atu.eu','zukauf@de.atu.eu');
    /**
     * Empf�nger f�r Fehler-Mails
     *
     * @var string
    */
    protected $_EMAIL_Infos = array('shuttle@de.atu.eu','zukauf@de.atu.eu');
    /**
     * Empf�nger f�r Fehler-Mails f�r das Operating
     *
     * @var string
    */
    protected $_EMAIL_Operating = array('shuttle@de.atu.eu','zukauf@de.atu.eu','shuttle@de.atu.eu');
    
    /**
     * Stadard-Protokollverzeichnis fuer alle Prtokolle
     * @var string
    */
    protected $_ProtokollVerzeichnis = '/var/log/awis/';
    /**
     * Protokollobjekt
     * @var awisProtokoll
     */
    protected $_Protokoll = null;
    /**
     * Werkzeug
     * @var awisWerkzeuge
     */
    protected $_AWISWerkzeug= null;
    
    
    public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
    {
        $this->_DB = $DB;
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_AWISWerkzeug = new awisWerkzeuge();
        $this->_Protokoll = new awisProtokoll($this->_DB, $this->_AWISBenutzer);
    }

    /**
     * Liest oder setzt den Debuglevel im Objekt
     * @param int $NeuerLevel
     * @return number
     */
    public function DebugLevel($NeuerLevel = null)
    {
        if(!is_null($NeuerLevel))
        {
            $this->_DebugLevel = (int)$NeuerLevel;
        }
    
        return $this->_DebugLevel;
    }
    /**
     * Prueft verschiedene Stammdaten und passt Zuordnungen an
     */
    public function StammmdatenPruefungen()
    {
        $Analyse = '';
         
        $Analyse .= 'Auslaufartikel bearbeiten:<br>';
        $Analyse .= $this->_StammdatenPruefungen_Auslaufartikel();

        if($this->_AWISBenutzer->ParameterLesen('Zukauf: Zuordnung Sonderbestellartikel')==1)
        {
            $Analyse .= '<hr>';
            $Analyse .= 'Sonderbestellartikel bearbeiten:<br>';
            $Analyse .= $this->_StammdatenPruefungen_Sonderbestellartikel();
        }
        
        $Analyse .= '<hr>';
        $Analyse .= 'Folgende Sortimentszuordnungen wurden ge&auml;ndert:<br>';
        $Analyse .= $this->_SortimentSetzen();
        
        $WerkZeug = new awisWerkzeuge();

        $Empfaenger = 'shuttle@de.atu.eu';
        if($WerkZeug->awisLevel()==awisWerkzeuge::AWIS_LEVEL_SHUTTLE)
        {
            $Empfaenger = 'zukauf@de.atu.eu';
        }
            
        $Mail = new awisMailer($this->_DB, $this->_AWISBenutzer);
        $Mail->Betreff('ZUKAUF Artikelstamm Wartung ');
        $Mail->Absender('shuttle@de.atu.eu');
        $Mail->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
        $Mail->AdressListe(awisMailer::TYP_CC,'shuttle@de.atu.eu',awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
        $Mail->Text($Analyse, awisMailer::FORMAT_HTML, true);
        $Mail->MailInWarteschlange();
    }
    
    /**
     * Prueft Auslaufartikel bei Zuordnungen
     */
    private function _StammdatenPruefungen_Auslaufartikel()
    {
        try
        {
            $Erg = '';
            $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
            
            $SQL = 'select distinct zla_key, ZLA_ARTIKELNUMMER, zlh_bezeichnung, zla_ast_atunr, ast_kennung';
            $SQL .= ' from artikelstamm ';
            $SQL .= ' inner join zukaufartikel ON zla_ast_atunr = ast_atunr';
            $SQL .= ' inner join warenuntergruppen ON ast_wug_key = wug_key';
            $SQL .= ' inner join zukauflieferantenhersteller ON zla_zlh_key = zlh_key';
            $SQL .= ' WHERE ast_kennung = \'A\' AND zla_ast_atunr IS NOT NULL';
             
            $rsZLA = $this->_DB->RecordSetOeffnen($SQL);
            	
            $this->_DB->TransaktionBegin();
            $DS=0;
            
            while(!$rsZLA->EOF())
            {
                $Erg .= $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER').'/'.$rsZLA->FeldInhalt('ZLH_BEZEICHNUNG').': '.$rsZLA->FeldInhalt('ZLA_AST_ATUNR').'<br>';
                 
                $SQL = 'UPDATE ZUKAUFARTIKEL ';
                $SQL .= ' SET ZLA_AST_ATUNR = NULL';
                $SQL .= ' WHERE ZLA_KEY = '.$this->_DB->WertSetzen('ZLA', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZLA'));
    
                $SQL = 'INSERT INTO ZUKAUFANPASSUNGENPROTOKOLL';
                $SQL .= '(ZUP_XTN_KUERZEL,ZUP_XXX_KEY,ZUP_AKTION,ZUP_DATENALT,ZUP_FELD';
                $SQL .= ',ZUP_USER,ZUP_USERDAT)';
                $SQL .= ' VALUES (';
                $SQL .= $this->_DB->WertSetzen('ZUP', 'T', 'ZLA');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ATU-Artikel entfernt wegen Auslaufkennzeichen');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $rsZLA->FeldInhalt('ZLA_AST_ATUNR'));
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ZLA_AST_ATUNR');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $this->_AWISBenutzer->BenutzerName());
                $SQL .= ', SYSDATE)';
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUP'));

                $this->_Protokoll->KonsolenAusgabe('Artikel '.$rsZLA->FeldInhalt('ZLA_AST_ATUNR').' bei ZLA '.$rsZLA->FeldInhalt('ZLA_KEY').' wurde entfernt.', 5, awisProtokoll::AUSGABETYP_INFO);
                
                $rsZLA->DSWeiter();
                $DS++;
            }
            	
            $this->_DB->TransaktionCommit();
            
            $this->_Protokoll->KonsolenAusgabe($DS.' Artikel angepasst.', 2, awisProtokoll::AUSGABETYP_INFO);
            
        }
        catch(Exception $ex)
        {
            $this->_DB->TransaktionRollback();
            $Erg = 'FEHLER bei der Verabeitung:<br>'.$this->_DB->LetzterSQL();
            $Erg .= '<br>Fehler:'.$ex->getMessage();
        }
         
        return $Erg;
    }
    
    /**
     * Prueft Auslaufartikel bei Zuordnungen
     */
    private function _StammdatenPruefungen_Sonderbestellartikel()
    {
        try
        {
            $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
            $this->_Protokoll->KonsolenAusgabe('Sonderbestellartikel', 5, awisProtokoll::AUSGABETYP_INFO);
            
            $Erg = '';
            	
            /*
            $SQL = 'select distinct zla_key, ZLA_ARTIKELNUMMER, zlh_bezeichnung, ZLA_TAUSCHTEIL, zla_ast_atunr, ast_kennung';
            $SQL .= ' , (SELECT ZAI_ID FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4 AND ROWNUM = 1) AS ATUNR';
            $SQL .= ' , (SELECT COUNT(*) FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4) AS ANZATUNR';
            $SQL .= ' , (SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = AST_ATUNR AND ASI_AIT_ID = 190) AS ATW';
            $SQL .= ' from artikelstamm ';
            $SQL .= ' inner join zukaufartikel ON (SELECT ZAI_ID FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4) = ast_atunr';
            $SQL .= ' inner join warenuntergruppen ON ast_wug_key = wug_key';
            $SQL .= ' inner join zukauflieferantenhersteller ON zla_zlh_key = zlh_key';
            $SQL .= ' WHERE ast_kennung = \'S\' AND zla_ast_atunr IS NULL ';
            $SQL .= ' AND (SELECT ZAI_ID FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4) IS NOT NULL';
            */
            $SQL  ='select distinct zla_key, ZLA_ARTIKELNUMMER, zlh_bezeichnung, ZLA_TAUSCHTEIL, zla_ast_atunr, ast_kennung, ZAI_ID AS ATUNR,';
            $SQL .='        (SELECT COUNT(*) FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4) AS ANZATUNR,';
            $SQL .='        (SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = AST_ATUNR AND ASI_AIT_ID = 190) AS ATW';
            $SQL .=' from zukaufartikel';
            $SQL .=' inner join ZUKAUFLIEFERANTENARTIKELIDS ON zai_zla_key = zla_key AND zai_aid_nr = 4';
            $SQL .=' inner join artikelstamm ON ast_atunr = zai_id';
            $SQL .=' inner join warenuntergruppen ON ast_wug_key = wug_key';
            $SQL .=' inner join zukauflieferantenhersteller ON zla_zlh_key = zlh_key';
            $SQL .=' WHERE ast_kennung = \'S\' AND zla_ast_atunr IS NULL';
            
            $rsZLA = $this->_DB->RecordSetOeffnen($SQL);
            $this->_Protokoll->KonsolenAusgabe($this->_DB->LetzterSQL(), 990, awisProtokoll::AUSGABETYP_INFO);
            	
            $this->_DB->TransaktionBegin();
            $DS = 0;            	
            while(!$rsZLA->EOF())
            {
                if($rsZLA->FeldInhalt('ANZATUNR')==1)
                {
                    if($rsZLA->FeldInhalt('ATW')!='' AND $rsZLA->FeldInhalt('ZLA_TAUSCHTEIL')!=0 OR $rsZLA->FeldInhalt('ATW')=='' AND $rsZLA->FeldInhalt('ZLA_TAUSCHTEIL')==0)
                    {
                        $Erg .= $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER').'/'.$rsZLA->FeldInhalt('ZLH_BEZEICHNUNG').': '.$rsZLA->FeldInhalt('ATUNR').'<br>';
                         
                        $SQL = 'UPDATE ZUKAUFARTIKEL ';
                        $SQL .= ' SET ZLA_AST_ATUNR = '.$this->_DB->WertSetzen('ZLA', 'T', $rsZLA->FeldInhalt('ATUNR'));
                        $SQL .= ' WHERE ZLA_KEY = '.$this->_DB->WertSetzen('ZLA', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZLA'));
            
                        $SQL = 'INSERT INTO ZUKAUFANPASSUNGENPROTOKOLL';
                        $SQL .= '(ZUP_XTN_KUERZEL,ZUP_XXX_KEY,ZUP_AKTION,ZUP_DATENALT,ZUP_FELD';
                        $SQL .= ',ZUP_USER,ZUP_USERDAT)';
                        $SQL .= ' VALUES (';
                        $SQL .= $this->_DB->WertSetzen('ZUP', 'T', 'ZLA');
                        $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                        $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ATU-Artikel eingetragen wegen Sonderbestellkennzeichen');
                        $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $rsZLA->FeldInhalt('ZLA_AST_ATUNR'));
                        $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ZLA_AST_ATUNR');
                        $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $this->_AWISBenutzer->BenutzerName());
                        $SQL .= ', SYSDATE)';
                        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUP'));
        
                        $this->_Protokoll->KonsolenAusgabe('Artikel '.$rsZLA->FeldInhalt('ATUNR').' bei ZLA '.$rsZLA->FeldInhalt('ZLA_KEY').' wurde eingetragen.', 5, awisProtokoll::AUSGABETYP_INFO);
                        $DS++;
                    }
                    else 
                    {
                        $this->_Protokoll->KonsolenAusgabe('Artikel '.$rsZLA->FeldInhalt('ATUNR').' bei ZLA '.$rsZLA->FeldInhalt('ZLA_KEY').' wird nicht verwendet, da ATW unterschiedlich ist.', 5, awisProtokoll::AUSGABETYP_INFO);
                    }
                }
                else
                {
                    $this->_Protokoll->KonsolenAusgabe('Artikel '.$rsZLA->FeldInhalt('ATUNR').' bei ZLA '.$rsZLA->FeldInhalt('ZLA_KEY').' hat mehr als eine ATUNR hinterlegt. Artikel wird ignoriert. Bitte Entwicker benachrichtigen.', 5, awisProtokoll::AUSGABETYP_FEHLER);
                }
                $rsZLA->DSWeiter();
            }
            	
            $this->_DB->TransaktionCommit();
            
            $this->_Protokoll->KonsolenAusgabe($DS.' Artikel angepasst.', 2, awisProtokoll::AUSGABETYP_INFO);
        }
        catch(Exception $ex)
        {
            $this->_DB->TransaktionRollback();
            $Erg = 'FEHLER bei der Verabeitung:<br>'.$this->_DB->LetzterSQL();
            $Erg .= '<br>Fehler:'.$ex->getMessage();
            $this->_Protokoll->KonsolenAusgabe('Fehler: '.$this->_DB->LetzterSQL(), 990, awisProtokoll::AUSGABETYP_INFO);
        }
         
        return $Erg;
    }

    /**
     * Lieferscheine einer Rechnung zuordnen
     */
    public function LieferscheineZuordnen()
    {
        $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
        
        $SQL = 'SELECT ZUR_KEY, ZUR_WANR, ZUR_LIE_NR, ZUR_LIEFERSCHEINNUMMER';
        $SQL .= ' FROM ZUKAUFRECHNUNGEN';
        $SQL .= ' WHERE ZUR_ZUL_KEY IS NULL AND ZUR_LIEFERSCHEINNUMMER IS NOT NULL';
        $SQL .= ' AND ZUR_USERDAT > (SYSDATE - 365)';
        $SQL .= ' ORDER BY ZUR_KEY DESC';
        
        $rsZUR = $this->_DB->RecordSetOeffnen($SQL);

        $DS = 0;
        while(!$rsZUR->EOF())
        {
            $this->_Protokoll->KonsolenAusgabe('Pruefe '.$rsZUR->FeldInhalt('ZUR_KEY').' mit '.$rsZUR->FeldInhalt('ZUR_LIEFERSCHEINNUMMER'), 90, awisProtokoll::AUSGABETYP_INFO);
            
            $SQL = 'SELECT *';
            $SQL .= ' FROM ZUKAUFLIEFERSCHEINE';
            $SQL .= ' WHERE ZUL_LIE_NR = '.$this->_DB->WertSetzen('ZUL', 'T', $rsZUR->FeldInhalt('ZUR_LIE_NR'));
            //$SQL .= ' AND ZUL_WANR = '.$this->_DB->WertSetzen('ZUL', 'T', $rsZUR->FeldInhalt('ZUR_WANR'));
            $SQL .= ' AND ZUL_EXTERNEID = '.$this->_DB->WertSetzen('ZUL', 'T', $rsZUR->FeldInhalt('ZUR_LIEFERSCHEINNUMMER'));
            
            $rsZUL = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ZUL'));
            if(!$rsZUL->EOF())
            {
                $this->_Protokoll->KonsolenAusgabe('Lieferschein gefunden', 5, awisProtokoll::AUSGABETYP_INFO);

                $SQL = 'UPDATE ZUKAUFRECHNUNGEN';
                $SQL .= ' SET ZUR_ZUL_KEY = '.$this->_DB->WertSetzen('ZUL', 'N0', $rsZUL->FeldInhalt('ZUL_KEY'));
                $SQL .= ' , ZUR_USER = '.$this->_DB->WertSetzen('ZUL', 'T', 'Lieferscheinzuordnung');
                $SQL .= ' , ZUR_USERDAT = SYSDATE';
                $SQL .= ' WHERE ZUR_KEY = '.$this->_DB->WertSetzen('ZUL', 'N0', $rsZUR->FeldInhalt('ZUR_KEY'));
                
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUL'));
            }    
            
            $DS++;
            $rsZUR->DSWeiter();
        }
    }
    
    

    /**
     * Lieferscheine einer Rechnung zuordnen
     */
    public function BestellungenZuordnen()
    {
        $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
    
        $SQL = 'SELECT ZUL_KEY, ZUL_WANR, ZUL_LIE_NR, ZUL_BESTELLID';
        $SQL .= ' FROM ZUKAUFLIEFERSCHEINE';
        $SQL .= ' WHERE ZUL_ZUB_KEY IS NULL AND ZUL_BESTELLID IS NOT NULL';
        $SQL .= ' AND ZUL_USERDAT > (SYSDATE - 365)';
        $SQL .= ' ORDER BY ZUL_KEY DESC';
    
        $rsZUL = $this->_DB->RecordSetOeffnen($SQL);
    
        $DS = 0;
        while(!$rsZUL->EOF())
        {
            $this->_Protokoll->KonsolenAusgabe('Pruefe '.$rsZUL->FeldInhalt('ZUL_KEY').' mit '.$rsZUL->FeldInhalt('ZUL_BESTELLID'), 90, awisProtokoll::AUSGABETYP_INFO);
    
            $SQL = 'SELECT *';
            $SQL .= ' FROM ZUKAUFBESTELLUNGEN';
            $SQL .= ' WHERE ZUB_LIE_NR = '.$this->_DB->WertSetzen('ZUL', 'T', $rsZUL->FeldInhalt('ZUL_LIE_NR'));
            $SQL .= ' AND ZUB_EXTERNEID = '.$this->_DB->WertSetzen('ZUL', 'T', $rsZUL->FeldInhalt('ZUL_BESTELLID'));
    
            $rsZUB = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ZUL'));
            if(!$rsZUB->EOF())
            {
                $this->_Protokoll->KonsolenAusgabe('Bestellung gefunden', 5, awisProtokoll::AUSGABETYP_INFO);
    
                $SQL = 'UPDATE ZUKAUFLIEFERSCHEINE';
                $SQL .= ' SET ZUL_ZUB_KEY = '.$this->_DB->WertSetzen('ZUL', 'N0', $rsZUB->FeldInhalt('ZUB_KEY'));
                $SQL .= ' , ZUL_USER = '.$this->_DB->WertSetzen('ZUL', 'T', 'Bestellungenzuordnung');
                $SQL .= ' , ZUL_USERDAT = SYSDATE';
                $SQL .= ' WHERE ZUL_KEY = '.$this->_DB->WertSetzen('ZUL', 'N0', $rsZUL->FeldInhalt('ZUL_KEY'));
    
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUL'));
            }
            
            $DS++;
            $rsZUL->DSWeiter();
        }
    }
    
    /**
     * Ordnet automatich Lieferantenartikel eine ATU Nummer zu
     */
    public function ZuordnungATUArtikel()
    {
        $this->_Protokoll->Init($this->_DebugLevel,__METHOD__,__CLASS__);
        $this->_Protokoll->KonsolenAusgabe('Versuche nicht zugeordnete Artikel zu finden ('.$this->_DebugLevel.')...', 1, awisProtokoll::AUSGABETYP_INFO);
        
        try 
        {
            // Alle noch nicht zugeordneten Artikel suchen
            // wichtig, dass nur ein eindeutiger Treffer vorhanden sein darf
            $SQL = 'SELECT *';
            $SQL .= ' FROM (';
            $SQL .= ' SELECT zla_key, zla_artikelnummer, zhz_lie_nr, lie_name1, lar_key, TEI_SUCH1';
            $SQL .= ' , LAR_GEPRUEFT, LAR_PROBLEMARTIKEL, ast_kennung';
            $SQL .= ' , LISTAGG(TEI_USER,\',\') WITHIN GROUP (ORDER BY TEI_USER)';
            $SQL .= ' , COUNT(*) OVER (PARTITION BY zla_key) AS ANZ';
            $SQL .= ' FROM zukaufartikel';
            $SQL .= ' INNER JOIN zukauflieferantenhersteller ON zla_zlh_key = zlh_key';
            $SQL .= ' INNER JOIN zukaufherstellerzuordnungen ON zlh_key = zhz_zlh_key';
            $SQL .= ' INNER JOIN LIEFERANTENARTIkel on LAR_SUCH_LARTNR = asciiwort(zla_artikelnummer) AND lar_lie_nr = zhz_lie_nr';
            $SQL .= ' INNER JOIN teileinfos ON tei_key2 = lar_key and tei_ity_id2 = \'LAR\' AND tei_ity_id1 = \'AST\' AND TEI_USER <> \'Import\'';
            $SQL .= ' INNER JOIN Artikelstamm ON tei_key1 = ast_key';
            $SQL .= ' INNER JOIN lieferanten on lie_nr = lar_lie_nr';
            $SQL .= ' where zla_ast_atunr is null';
            $SQL .= ' and REGEXP_SUBSTR(TEI_SUCH1, \'[�@�].*\') IS NULL';
            $SQL .= ' AND NOT (TEI_USER = \'XXX\' AND LAR_GEPRUEFT IS NULL)';
            $SQL .= ' and LAR_REKLAMATIONSKENNUNG IS NULL';
            $SQL .= ' and (lie_name1 not like \'%DUMMY%\' AND lie_name1 NOT like \'%BRUTTO%\' AND lie_name1 NOT like \'%RESERVIERT%\' )';
            $SQL .= ' and lar_problemartikel = 0';
            $SQL .= ' AND CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = TEI_SUCH1) END IS NULL';
            $SQL .= ' AND (AST_WUG_KEY <> 1  AND AST_VK IS NOT NULL)';      // Gueltige ATU Artikel
            $SQL .= ' AND ((SELECT asciiwort(asi_wert) FROM artikelstamminfos where asi_ait_id = 41 and asi_ast_atunr = tei_such1) = asciiwort(zla_artikelnummer))';
            $SQL .= ' GROUP BY zla_key, zla_artikelnummer, AST_IMQ_ID, zhz_lie_nr, lie_name1, lar_key, TEI_SUCH1';
            $SQL .= ' , LAR_GEPRUEFT, LAR_PROBLEMARTIKEL, AST_KENNUNG, LAR_REKLAMATIONSKENNUNG';
            $SQL .= ' ) DATEN';
            $SQL .= ' WHERE ANZ = 1';
            
            $rsZLA = $this->_DB->RecordSetOeffnen($SQL);
            while(!$rsZLA->EOF())
            {
                $this->_DB->TransaktionBegin();
                
                $this->_Protokoll->KonsolenAusgabe('LAR '.$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER'), 5, awisProtokoll::AUSGABETYP_INFO);
                
                $SQL = 'SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS';
                $SQL .= ' WHERE ZAI_AID_NR = '.$this->_DB->WertSetzen('ZAI', 'N0', 4);        // ATU NUmmer ohne Preiseinfluss
                $SQL .= ' AND ZAI_ZLA_KEY = '.$this->_DB->WertSetzen('ZAI', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                $rsZAI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ZAI'));
                if($rsZAI->EOF())
                {
                    $SQL = 'INSERT INTO ZUKAUFLIEFERANTENARTIKELIDS(ZAI_ZLA_KEY,ZAI_AID_NR,ZAI_ID,ZAI_IMQ_ID';
                    $SQL .= ',ZAI_USER,ZAI_USERDAT) VALUES (';
                    $SQL .= $this->_DB->WertSetzen('ZAI', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'N0', 4);        // ATU NUmmer ohne Preiseinfluss
                    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'T', $rsZLA->FeldInhalt('TEI_SUCH1'));
                    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'N0', 4);        // Manuelle Importquelle
                    $SQL .= ', '.$this->_DB->WertSetzen('ZAI', 'T', $this->_AWISBenutzer->BenutzerName());
                    $SQL .= ', SYSDATE)';
                    $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZAI'));
                }
                elseif($rsZAI->FeldInhalt('ZAI_ID')!=$rsZLA->FeldInhalt('TEU_SUCH1'))
                {
                    $SQL = 'UPDATE ZUKAUFLIEFERANTENARTIKELIDS';
                    $SQL .= ' SET ZAI_ID = '.$this->_DB->WertSetzen('ZAI', 'T', $rsZLA->FeldInhalt('TEI_SUCH1'));
                    $SQL .= ', ZAI_USER=  '.$this->_DB->WertSetzen('ZAI', 'T', $this->_AWISBenutzer->BenutzerName());
                    $SQL .= ', ZAI_USERDAT = SYSDATE';
                    $SQL .= ' WHERE ZAI_KEY = '.$this->_DB->WertSetzen('ZAI', 'N0', $rsZAI->FeldInhalt('ZAI_KEY'));
                    
                    $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZAI'));
                }            
                $ZusatzHinweis = '';
                $ProtokollSchreiben = true;
                switch($rsZLA->FeldInhalt('AST_KENNUNG'))
                {
                    case 'A':       // Auslaufartikel
                    case 'E':       // Endeartikel
                        $ZusatzHinweis = ' ('.$rsZLA->FeldInhalt('TEI_SUCH1').', Artikelkennung '.$rsZLA->FeldInhalt('AST_KENNUNG').')';
                        if($rsZLA->FeldInhalt('ZLA_AST_ATUNR')=='')     // Wenn der schon leer war, kein Protokoll schreiben
                        {
                            $ProtokollSchreiben=false;
                        }
                        break;
                    default:
                        $SQL = 'UPDATE ZUKAUFARTIKEL ';
                        $SQL .= ' SET ZLA_AST_ATUNR = '.$this->_DB->WertSetzen('ZLA', 'T', $rsZLA->FeldInhalt('TEI_SUCH1'));
                        $SQL .= ' WHERE ZLA_KEY = '.$this->_DB->WertSetzen('ZLA', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZLA'));
                }
                
                $this->_Protokoll->KonsolenAusgabe('ATUNR '.$rsZLA->FeldInhalt('TEI_SUCH1'), 5, awisProtokoll::AUSGABETYP_INFO);

                if($ProtokollSchreiben)
                {                
                    $SQL = 'INSERT INTO ZUKAUFANPASSUNGENPROTOKOLL';
                    $SQL .= '(ZUP_XTN_KUERZEL,ZUP_XXX_KEY,ZUP_AKTION,ZUP_DATENNEU,ZUP_DATENALT,ZUP_FELD';
                    $SQL .= ',ZUP_USER,ZUP_USERDAT)';
                    $SQL .= ' VALUES (';
                    $SQL .= $this->_DB->WertSetzen('ZUP', 'T', 'ZLA');
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ATU-Artikel durch eindeutigen Lieferantenartikel'.$ZusatzHinweis);
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $rsZLA->FeldInhalt('TEI_SUCH1'),true);
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $rsZLA->FeldInhalt('ZLA_AST_ATUNR'),true);
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ZLA_AST_ATUNR');
                    $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $this->_AWISBenutzer->BenutzerName());
                    $SQL .= ', SYSDATE)';
                    $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUP'));
                }
                                
                $this->_DB->TransaktionCommit();
                
                $rsZLA->DSWeiter();
            }
        }
        catch(Exception $ex)
        {
            $this->_Protokoll->KonsolenAusgabe('FEHLER: '.$ex->getCode(), 0, awisProtokoll::AUSGABETYP_FEHLER);
            $this->_DB->TransaktionRollback();
        }
    }
    
    /**
     * Setzt das Sortiment f�r einen Zukaufartikel anhand des zugeordneten ATU Artikels
     */
    private function _SortimentSetzen()
    {
        try 
        {
            $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
            
            $DatenAenderungen = array();
            
            // Schritt 1: vorhandene pr�fen
            $SQL = 'SELECT ZLA_KEY, ZLA_ARTIKELNUMMER, ZLH_KUERZEL, ZLA_SORTIMENT, WUG_ID';
            $SQL .= ' FROM ZUKAUFARTIKEL';
            $SQL .= ' INNER JOIN ARTIKELSTAMM ON ZLA_AST_ATUNR = AST_ATUNR';
            $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLA_ZLH_KEY = ZLH_KEY';
            $SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON AST_WUG_KEY = WUG_KEY AND WUG_WGR_ID = \'03\'';
            $SQL .= ' WHERE COALESCE(SUBSTR(ZLA_SORTIMENT,4,3),\'***\') <> WUG_ID';
            $SQL .= ' AND ZLA_AID_NR = 5';
            $rsZLA = $this->_DB->RecordSetOeffnen($SQL);
            $DS=0;
            
            while(!$rsZLA->EOF())
            {
                $this->_DB->TransaktionBegin();
                
                $SQL = 'INSERT INTO ZUKAUFANPASSUNGENPROTOKOLL';
                $SQL .= '(ZUP_XTN_KUERZEL,ZUP_XXX_KEY,ZUP_AKTION,ZUP_DATENNEU,ZUP_DATENALT,ZUP_FELD';
                $SQL .= ',ZUP_USER,ZUP_USERDAT)';
                $SQL .= ' VALUES (';
                $SQL .= $this->_DB->WertSetzen('ZUP', 'T', 'ZLA');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'Sortiment wurde angepasst.');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ZUK'.$rsZLA->FeldInhalt('WUG_ID'),true);
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $rsZLA->FeldInhalt('ZLA_SORTIMENT'),true);
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', 'ZLA_SORTIMENT');
                $SQL .= ', '.$this->_DB->WertSetzen('ZUP', 'T', $this->_AWISBenutzer->BenutzerName());
                $SQL .= ', SYSDATE)';
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZUP'));
                
                $SQL = 'UPDATE ZUKAUFARTIKEL';
                $SQL .= ' SET ZLA_SORTIMENT = '.$this->_DB->WertSetzen('ZLA', 'T', 'ZUK'.$rsZLA->FeldInhalt('WUG_ID'));
                $SQL .= ', ZLA_USER = '.$this->_DB->WertSetzen('ZLA', 'T', $this->_AWISBenutzer->BenutzerName());
                $SQL .= ', ZLA_USERDAT = SYSDATE';
                $SQL .= ' WHERE ZLA_KEY = '.$this->_DB->WertSetzen('ZLA', 'N0', $rsZLA->FeldInhalt('ZLA_KEY'));
                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ZLA'));
    
                $this->_DB->TransaktionCommit();

                $DatenAenderungen[] = $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER').'/'.$rsZLA->FeldInhalt('ZLH_KUERZEL').' von '.$rsZLA->FeldInhalt('ZLA_SORTIMENT').' auf ZUK'.$rsZLA->FeldInhalt('WUG_ID');

                $this->_Protokoll->KonsolenAusgabe($rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER').'/'.$rsZLA->FeldInhalt('ZLH_KUERZEL').' von '.$rsZLA->FeldInhalt('ZLA_SORTIMENT').' auf ZUK'.$rsZLA->FeldInhalt('WUG_ID'), 9, awisProtokoll::AUSGABETYP_INFO);
                
                $rsZLA->DSWeiter();
                $DS++;
            }
        }
        catch(AWISException $ex)
        {
            $this->_Protokoll->KonsolenAusgabe('FEHLER: '.$ex->getCode(), 0, awisProtokoll::AUSGABETYP_FEHLER);
            $this->_Protokoll->KonsolenAusgabe('FEHLER: '.$ex->getMessage(), 0, awisProtokoll::AUSGABETYP_FEHLER);
            $this->_Protokoll->KonsolenAusgabe('FEHLER: '.$ex->getSQL(), 0, awisProtokoll::AUSGABETYP_FEHLER);
            $this->_DB->TransaktionRollback();
        }
        
        $this->_Protokoll->KonsolenAusgabe($DS.' Artikel wurden angepasst.', 5, awisProtokoll::AUSGABETYP_INFO);
        
        return implode('<br>',$DatenAenderungen);
    }
    
    
}