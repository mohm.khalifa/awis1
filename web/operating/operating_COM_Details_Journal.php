<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");
require_once("jpgraph/jpgraph.php");
require_once("jpgraph/jpgraph_gantt.php");

global $comcon;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_journal','OCJ_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Journal');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Journal');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Journal'].'</title>';
?>
</head>
<body>
<?php


$Recht3116 = awisBenutzerRecht($con,3116);
if($Recht3116==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

echo "<p><center><h1><b>".$AWISSprachKonserven['Operating']['PKT_Operating_COM_Journal']."</b></h1></center><p>";

if (!empty($_REQUEST["startzeit"]))
{
	$startzeit = $_REQUEST["startzeit"];
	echo '<input type="hidden" name="startzeit" value="'.$startzeit.'">';
}
else
{
	$startzeit = date("d.m.Y H:i",strtotime("-1 day"));
	echo '<input type="hidden" name="startzeit" value="'.$startzeit.'">';
}
if (!empty($_REQUEST["endzeit"]))
{
	$endzeit = $_REQUEST["endzeit"];
	echo '<input type="hidden" name="endzeit" value="'.$endzeit.'">';
}
else
{
	$endzeit = date("d.m.Y H:i");
	echo '<input type="hidden" name="endzeit" value="'.$endzeit.'">';
}
/*
if (!empty($_REQUEST["laufzeit"]))
{
	$laufzeit = $_REQUEST["laufzeit"];
}
*/

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Journal action=./operating_COM_Details_JournalPic.php method=POST>';

echo "Zeige Journal von <input type=text name='startzeit' size=18 value='".$startzeit."'> bis <input type=text name='endzeit' size=18 value='".$endzeit."'>";
echo '&nbsp;<input type=submit name="cmdAnzeigen" value="Anzeigen">';

echo '</form>';
?>
</body>
</html>
