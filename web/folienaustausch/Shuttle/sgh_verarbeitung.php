<?php

require_once('sgh_import.php');
require_once('sgh_abgleich.php');

try
{
	echo "Starte Import ...\n";
	$import = new SGHImport();
	$import->starteImport();
	
	echo "Starte Abgleich ...\n";
	$abgleich = new SGHAbgleich();
	$abgleich->starteAbgleich();
}
catch (Exception $ex)
{
        //echo PHP_EOL.'SGHImport: Fehler: '.$ex->getMessage().PHP_EOL;
        #$AWISInfo = new awisINFO();
        #$AWISInfo->EMail(array('henry.ott@de.atu.eu','stefan.oppl@de.atu.eu'),'SGH-Fehler','Fehler : '.$ex->getMessage(),3);
}    
?>
