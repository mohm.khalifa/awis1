<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_line.php");

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


for($i=0;$i<=53;$i++)
{
	$Daten['1742'][$i]=0;
	$Daten['8164'][$i]=0;
	$Daten['2415'][$i]=0;
	$Daten['0255'][$i]=0;
	$Daten['8219'][$i]=0;
	$Daten['9157'][$i]=0;
	$Daten['2620'][$i]=0;
	$Daten['8252'][$i]=0;
}

$Jahr = (isset($_GET['Jahr'])?$DB->FeldInhaltFormat('N0',$_GET['Jahr']):date('Y'));

$SQL = "SELECT KFZ_HERST.BEZ, to_char(ZUB_Bestelldatum,'WW') AS Woche, count(*) AS Anzahl";
$SQL .= ' FROM ZUKAUFBESTELLUNGEN';
$SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
$SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
$SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
$SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
$SQL .= ' WHERE ZUB_BESTELLDATUM >= TO_DATE(\'01.01.'.$Jahr.'\',\'DD.MM.YYYY\')';
$SQL .= ' AND ZUB_BESTELLDATUM < TO_DATE(\'01.01.'.($Jahr+1).'\',\'DD.MM.YYYY\')';
$SQL .= ' ';
$SQL .= " GROUP BY KFZ_HERST.BEZ, to_char(ZUB_Bestelldatum,'WW')";
$SQL .= ' ORDER BY 2 DESC';

$rsSTAT = $DB->RecordSetOeffnen($SQL);
$Woche = 0;
while(!$rsSTAT->EOF())
{
	$Daten[$rsSTAT->FeldInhalt('BEZ')][(int)$rsSTAT->FeldInhalt('WOCHE')-1]=$rsSTAT->FeldInhalt('ANZAHL');

	$rsSTAT->DSWeiter();
}

$graph = new Graph(1000,400,"auto");
$graph->SetScale("textlin");
$graph->SetShadow();
$graph->img->SetMargin(60,30,20,40);

$rsSTAT->DSErster();
$i = 0;
$p=array();
$SQL = "SELECT KFZ_HERST.BEZ, count(*) AS Anzahl";
$SQL .= ' FROM ZUKAUFBESTELLUNGEN';
$SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
$SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
$SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
$SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
$SQL .= ' WHERE ZUB_BESTELLDATUM >= TO_DATE(\'01.01.'.$Jahr.'\',\'DD.MM.YYYY\')';
$SQL .= ' AND ZUB_BESTELLDATUM < TO_DATE(\'01.01.'.($Jahr+1).'\',\'DD.MM.YYYY\')';
$SQL .= " GROUP BY KFZ_HERST.BEZ";
$SQL .= ' ORDER BY 2 DESC';

$rsSTAT = $DB->RecordSetOeffnen($SQL);

while(!$rsSTAT->EOF())
{
    $p[$i] = new LinePlot($Daten[$rsSTAT->FeldInhalt('BEZ')]);
    $p[$i]->SetLegend ($rsSTAT->FeldInhalt('BEZ'));
    if($i++>15)
    {
        break;
    }
    
    $rsSTAT->DSWeiter();
}

$graph->title->Set("Zuk�ufe nach Fahrzeugen ".$Jahr);
$graph->xaxis->title->Set("Fahrzeug");
$graph->yaxis->title->Set("Bestellpositionen");
$graph->yaxis->SetTitleMargin(40);

$ap = new AccLinePlot($p);

// Add the accumulated line plot to the graph
$graph->Add( $ap);

//$p1->SetFillColor ("blue");

$graph ->legend->Pos( 0.02,0.05,"right","top");

$graph->Stroke();
?>