<?php

class Bestellung
{

    /**
     * @var PartnerBestellungV3 $BestelldatenV3
     */
    protected $BestelldatenV3 = null;

    /**
     * @param PartnerBestellungV3 $BestelldatenV3
     */
    public function __construct($BestelldatenV3)
    {
      $this->BestelldatenV3 = $BestelldatenV3;
    }

    /**
     * @return PartnerBestellungV3
     */
    public function getBestelldatenV3()
    {
      return $this->BestelldatenV3;
    }

    /**
     * @param PartnerBestellungV3 $BestelldatenV3
     * @return Bestellung
     */
    public function setBestelldatenV3($BestelldatenV3)
    {
      $this->BestelldatenV3 = $BestelldatenV3;
      return $this;
    }

}
