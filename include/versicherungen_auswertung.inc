<?php

	require_once 'awisDatenbank.inc';
	require_once('awisBenutzer.inc');
	require_once 'awisMailer.inc';
	require_once 'awisFormular.inc';
	
	class versicherungen_auswertung
	{
				
		/**
		 * Datenbankobjekt
		 * @var object
		 */
		private $_DB = '';
		
		/**
		 * Exportpfad der Reportingdatei
		 * @var string
		 */
		private $_exportPfadReporting = '/daten/daten/versicherungen/archiv/reporting/Versicherung-Gesperrt/';
		
		/**
		 * Dateiname der Exportdatei
		 * @var string
		 */
		private $_exportDateiname = '';
		
		/**
		 * Pfad inkl. Dateiname des LogFiles
		 * @var string
		 */
		private $_logFile = '/daten/daten/versicherungen/archiv/reporting/Versicherung-Gesperrt/auswertung.log';
		
		/**
		 * Recordsetobjekt mit den Daten fuer das
		 * Reporting
		 * @var Recordset-Objekt
		 */
		private $_rsAuswertungsdaten = null;
		
		/**
		 * AWIS-Benutzerobjekt
		 * @var object
		 */
		private $_AWISBenutzer = null;
		
		/**
		 * AWIS-Werkzeugeobjekt
		 * @var object
		 */
		private $_AWISWerkzeuge = null;
		
		/**
		 * AWIS-Mailerobjekt
		 * @var object
		 */
		private $_Mailer = null;
		
		/**
		 * AWIS-Level
		 * @var string
		 */
		private $_awisLevel = '';
		
		
		
		/**
		 * Benoetigte Objekte erstellen, Verarbeitungsdatum speichern
		 * @param date $datumVerarbeitung
		 * @param string $benutzer Benutzername
		 */
		public function __construct()
		{
			$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
			$this->_AWISBenutzer = awisBenutzer::Init();
			$this->_AWISWerkzeuge = new awisWerkzeuge();
			$this->_Mailer = new awisMailer($this->_DB,$this->_AWISBenutzer);
			$this->_awisLevel = $this->_AWISWerkzeuge->awisLevel();
			 
		}
		
		public function erstelleReporting() {
			
			try
			{
				
				$fp = $this->oeffneDatei($this->_logFile, 'a');
				
				$this->schreibeLogFile('Selektiere Daten', $fp);
				$this->selektiereDaten();
				$this->schreibeLogFile('Exportiere Datei und versende sie per Mail', $fp);
				$this->erstelleExportdatei();
				$this->schreibeLogFile('Reporting erfolgreich erstellt', $fp);
			}
			catch(Exception $ex)
			{
				echo $ex->getMessage() . $ex->getCode();
				$this->schreibeLogFile('ERROR ' . $ex->getMessage() . ' ' . $ex->getCode(), $fp);
			}
			
			// LogFile schliessen
			fclose($fp);
			
		}
		
		/**
		 * Schreibt einen Eintrag in die LogFile
		 * @param string $logFileText
		 * @param resource $fp
		 */
		private function schreibeLogFile($logFileText,$fp) {
			try
			{
				$praefix = '[' . strftime('%Y-%m-%d %H:%M:%S') . ']: ';
				
				fputs($fp, $praefix . $logFileText . "\n");
			}
			catch(Exception $ex)
			{
				// Nur ausgeben wenn LogFile nicht geschrieben werden kann (siehe erstelleReporting())
				echo $ex->getMessage() . $ex->getCode();
			}
		}
		
		/**
		 * Selektiert die Daten bei denen zwischen dem Kassen- & VersRechBetrag
		 * eine Differenz > 0 besteht.
		 */
		private function selektiereDaten() 
		{
			try
			{

				$SQL  ='SELECT a.vvs_filid  				AS FilNr,';
				$SQL .='   a.vvs_wanr 						AS WA,';
				$SQL .='   concat(a.vvs_filid,a.vvs_wanr)   AS RENR,';
				$SQL .='   a.vvs_vorgangnr                  AS Vorgang_nr,';
				$SQL .='   ver.vve_bezeichnung              AS Versicherung,';
				$SQL .='   a.vvs_kfzkennz                   AS KFZ,';
				$SQL .='   regexp_replace(REGEXP_REPLACE(regexp_replace (a.vvs_freigabegrund,CHR(13),\'\'), CHR(10), \'\'),\',\') AS grund,';
				$SQL .='   CASE';
				$SQL .='     WHEN vvs_freigabe = 0';
				$SQL .='     THEN \'NEIN\'';
				$SQL .='     ELSE \'JA\'';
				$SQL .='   END                              AS Freigabe,';
				$SQL .='   b.umsatz_vers                    AS Umsatz_Vers,';
				$SQL .='   c.ven_bemerkung                  AS Fehlerstatus,';
				$SQL .='   d.vcn_bemerkung                  AS Status,';
				$SQL .='   SUM(e.fgp_anzahl*e.fgp_oempreis) AS Autoglas,';
				$SQL .='   bea.DATUM                        AS Kassendatum,';
				$SQL .='   g.fgk_freigabegrund              AS Notiz,';
				$SQL .='   a.VVS_DATUMIMPORT                AS DATUM_IMPORTIERT,';
				$SQL .='   a.VVS_DATUMEXPORT                AS DATUM_EXPORTIERT,';
				$SQL .='   a.VVS_DATUMERHALTEN              AS DATUM_ERHALTEN,';
				$SQL .='   a.VVS_DATUMGDVABGELEGT           AS DATUM_GDVABGELEGT';
				$SQL .=' FROM VERSVORGANGSSTATUS a';
				$SQL .=' INNER JOIN V_VERS_FORM_RECHNUNGSDATEN b';
				$SQL .=' ON a.vvs_wanr   = b.wanr';
				$SQL .=' AND a.vvs_filid = b.filid';
				$SQL .=' INNER JOIN VERSSTATUSNUMMERN c';
				$SQL .=' ON a.vvs_ven_key = c.ven_key';
				$SQL .=' INNER JOIN VERS_FORM_BEARBDATEN bea';
				$SQL .=' ON a.VVS_FILID        = bea.FILNR';
				$SQL .=' AND a.VVS_WANR        = bea.WANR';
				$SQL .=' AND bea.VERKAEUFERART = \'K\'';
				$SQL .=' AND bea.QUELLE        = \'D\'';
				$SQL .=' INNER JOIN VERSCHECKSTATUSNUMMERN d';
				$SQL .=' ON a.vvs_vcn_key = d.vcn_key';
				$SQL .=' LEFT JOIN fgpositionsdaten e';
				$SQL .=' ON a.vvs_vorgangnr = e.fgp_vorgangnr';
				$SQL .=' LEFT JOIN fkassendaten f';
				$SQL .=' ON a.vvs_wanr   = f.fka_wanr';
				$SQL .=' AND a.vvs_filid = f.fka_filid';
				$SQL .=' LEFT JOIN fgkopfdaten g';
				$SQL .=' ON a.vvs_vorgangnr = g.fgk_vorgangnr';
				$SQL .=' INNER JOIN versversicherungen ver';
				$SQL .=' ON a.vvs_versnr          = ver.vve_versnr';
				$SQL .=' WHERE a.vvs_vcn_key NOT IN (120,114,115,116,117,118,119,113,124)';
				$SQL .=' GROUP BY a.vvs_filid,';
				$SQL .='   a.vvs_wanr,';
				$SQL .='   a.vvs_vorgangnr,';
				$SQL .='   a.vvs_kfzkennz,';
				$SQL .='   a.vvs_freigabegrund,';
				$SQL .='   b.umsatz_vers,';
				$SQL .='   c.ven_bemerkung,';
				$SQL .='   d.vcn_bemerkung,';
				$SQL .='   a.vvs_freigabe,';
				$SQL .='   ver.vve_bezeichnung,';
				$SQL .='   bea.DATUM,';
				$SQL .='   g.fgk_freigabegrund,';
				$SQL .='   a.VVS_DATUMIMPORT,';
				$SQL .='   a.VVS_DATUMEXPORT,';
				$SQL .='   a.VVS_DATUMERHALTEN,';
				$SQL .='   a.VVS_DATUMGDVABGELEGT';
					
				$this->_rsAuswertungsdaten = $this->_DB->RecordsetOeffnen($SQL);
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(), $ex->getCode());
			}			
		}
		
		/**
		 * Oeffnet angegebene Datei im angegebenen Modus und loest im Fehlerfall eine
		 * Exception aus.
		 * @param string $pfad Pfad inkl. Dateiname der Datei die geoeffnet werden soll
		 * @param string $modus Modus mit dem die Datei geoeffnet werden soll
		 * @return resource FilePointer
		 */
		private function oeffneDatei($pfad,$modus) {
			$fp = fopen($pfad, $modus);
			
			if(!$fp)
			{
				throw new Exception('Datei konnte nicht geoeffnet werden','1408061439');	
			}
			else
			{
				return $fp;
			}			
		}
		
		/**
		 * Erzeugt die Reportingdatei. Wenn dies Erfolgreich ist wird die Mail-Methode
		 * aufgerufen.
		 * @param object (Recordset) $rsReportingdaten
		 */
		private function erstelleExportdatei() 
		{
			try
			{
			
				if(is_null($this->_rsAuswertungsdaten))
				{
					throw new Exception('es wurden keine Reportingdaten selektiert','1408061436');
				}
				else
				{
					// Dateinamen erstellen
					$this->_exportDateiname = 'Versicherung-Gesperrt_' . strftime('%Y%m%d_%H%M%S') . '.csv';
					
					$fp = $this->oeffneDatei($this->_exportPfadReporting . $this->_exportDateiname, 'a');
					
					$ueberschrift = 'FILNR;WA;RENR;VORGANG_NR;VERSICHERUNG;KFZ;GRUND;FREIGABE;UMSATZ_VERS;FEHLERSTATUS;STATUS;AUTOGLAS;KASSENDATUM;NOTIZ';
					
					fputs($fp, $ueberschrift . "\n");
					
					$datenZeile = '';
					
					while(!$this->_rsAuswertungsdaten->EOF())
					{
						$datenZeile = $this->_rsAuswertungsdaten->FeldInhalt('FILNR');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('WA');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('RENR');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('VORGANG_NR');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('VERSICHERUNG');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('KFZ');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('GRUND');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('FREIGABE');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('UMSATZ_VERS');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('FEHLERSTATUS');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('STATUS');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('AUTOGLAS');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('KASSENDATUM');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('NOTIZ');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('DATUM_IMPORTIERT');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('DATUM_EXPORTIERT');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('DATUM_ERHALTEN');
						$datenZeile .= ';' . $this->_rsAuswertungsdaten->FeldInhalt('DATUM_GDVABGELEGT');
						
						fputs($fp,$datenZeile . "\n");
						
						$datenZeile = '';
						
						$this->_rsAuswertungsdaten->DSWeiter();
					}
					
					fclose($fp);
				}
				
				// wenn bis hierher alles gut geht, dann kann Mail gesendet werden
				$this->sendeReportingMail();
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}
		
		/**
		 * Sendet die Reportingdatei per Mail an Verteiler
		 */
		private function sendeReportingMail() 
		{
			try
			{
				$SQL = 'SELECT mvt_betreff, mvt_text ';
				$SQL .= 'FROM mailversandtexte ';
				$SQL .= 'WHERE mvt_bereich = \'VER_AUSWERTUNG\'';
				
				$rsMVT = $this->_DB->RecordSetOeffnen($SQL);
				
				
				$this->_Mailer->SetzeVersandPrioritaet(10);
				$this->_Mailer->LoescheAdressListe();
				$this->_Mailer->AnhaengeLoeschen();
					
				$this->_Mailer->Absender('shuttle@de.atu.eu');
				
				$empfaenger = array();
				$empfaengerCC = array();
				
				if($this->_awisLevel == 'ENTW' || $this->_awisLevel == 'STAG')
				{
	//				$empfaengerCC[] = 'stefan.oppl@de.atu.eu';
					$empfaenger[] = 'tamara.bannert@de.atu.eu';
				}
				elseif($this->_awisLevel == 'PROD' || $this->_awisLevel == 'SHUT')
				{
					$empfaenger[] = 'sebastian@jarantowski.de';
					$empfaengerCC[] = 'tamara.scholz@de.atu.eu';
					$empfaengerCC[] = 'tamara.bannert@de.atu.eu';
						
				}
				else
				{
					throw new Exception('unbekannter AWIS-Level','1408071013');
				}
				
				foreach ($empfaenger as $empfaengerValue)
				{
					if(!$this->_Mailer->AdressListe(awisMailer::TYP_TO,$empfaengerValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
						throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_TO','1408071017');
					}
				}
				
				foreach ($empfaengerCC as $empfaengerCCValue)
				{
					if(!$this->_Mailer->AdressListe(awisMailer::TYP_CC,$empfaengerCCValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
					    throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_CC','1408071021');
					}
				}
					
				// nur Kuerzel fuer Bezug setzen. Einen Key gibt es nicht da in der Reportingdatei
				// mehrere Vorgaenge enthalten sein koennen.
				$this->_Mailer->SetzeBezug('VAB',0);
					
				$this->_Mailer->Anhaenge($this->_exportPfadReporting . $this->_exportDateiname, $this->_exportDateiname);
				$this->_Mailer->Betreff($rsMVT->FeldInhalt('MVT_BETREFF'));
				$this->_Mailer->Text($rsMVT->FeldInhalt('MVT_TEXT'),awisMailer::FORMAT_HTML, true);
					
				$this->_Mailer->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}		
	}

?>