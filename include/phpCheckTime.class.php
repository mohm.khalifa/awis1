<?php


class phpCheckTime
{

	var $init_time;
	var $the_time_elapsed;

	/*
	* Constructor
	*/
	function phpCheckTime() {
		$this->the_time_elapsed = 0;
		$this->init_time = 0;
	}
	
	/*
	* start Toma el tiempo inicial
	*/
	function start()
	{
		list($usec, $sec) = explode(" ",microtime());
		$this->init_time = (float)$usec + (float)$sec;
	}

	/*
	* stop Toma el tiempo actual y calcula tiempo transcurrido
	*/
	function stop()
	{		
		if ($this->init_time == 0) {
			$this->the_time_elapsed = 0;
		}
		else {
			list($usec, $sec) = explode(" ",microtime());
			$actual_time = (float)$usec + (float)$sec;
			$this->the_time_elapsed += $actual_time - $this->init_time;
			$this->init_time = 0;
		}
	}

	/*
	* resume Retoma el control del tiempo
	*/
	function resume()
	{
		list($usec, $sec) = explode(" ",microtime());
		$this->init_time = (float)$usec + (float)$sec;
	}
	
	/*
	* getTime Devuelve el tiempo transcurrido en total desde el primer start (sin contar los periodos stopped)
	*/
	function getTime()
	{
		if ($this->init_time == 0) {    // El cronometro est� parado - toma de tiempos en frio
			return $this->the_time_elapsed;
		}
		else {   // El cronometro sigue corriendo - toma de tiempos en caliente
			list($usec, $sec) = explode(" ",microtime());
			$actual_time = (float)$usec + (float)$sec;
			$last_time_elapsed = $actual_time - $this->init_time;
			return ($this->the_time_elapsed + $last_time_elapsed);
		}
	}

}
?>