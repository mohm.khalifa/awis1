<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');

global $awisRSZeilen;

//ini_set('max_execution_time','9000');

//$AWISBenutzer = new awisUser();
//print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Vorlagen = array('PDF_Logo_2008.pdf');

$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

$SQL = "select VUFIL, KENNUNG_BEZEICHNUNG, count(*) as ANZAHL, sum(Fallkosten) as SUMME_KOSTEN";
$SQL.= " from";
$SQL.= " (";
$SQL.= " SELECT SCHAEDEN_NEW.BEARBEITUNGSNR AS BearbNr, SCHAEDEN_NEW.BEARBEITUNGSNRFIL AS BearbNrFiliale, ";
$SQL.= " SCHAEDEN_NEW.EINGABEAM AS Eingabe_am, SCHAEDEN_NEW.KENNUNG AS Kennung, ";
$SQL.= " KENNUNGEN.KENNUNG_BEZEICHNUNG AS Kennung_Bezeichnung, SCHAEDEN_NEW.FEHLERBESCHREIBUNG AS Fehlerbeschreibung, ";
$SQL.= " SCHAEDEN_NEW.DIAGNOSE_WERKSTATTLEITER AS DiagnoseWL, SCHAEDEN_NEW.DIAGNOSE_TKDL AS BesprMassnahme, ";
//$SQL.= " --Anwaltsache.Anwaltsache, ";
$SQL.= " SCHAEDEN_NEW.FILNR AS BearbFil, ";
//$SQL.= " --[Filialliste_GBL+VKL_20080208_1].FIL_BEZ AS [Bearb-Fil_Name], ";
$SQL.= " SCHAEDEN_NEW.VERURS_FILIALE AS VUFil, ";
//$SQL.= " --[Filialliste_GBL+VKL_20080208].FIL_BEZ AS [VU-Fil_Name], ";
$SQL.= " Sum(ZAHLUNGEN.BETRAG) AS Fallkosten, STAND.STANDBEM AS BearbStatus, ";
$SQL.= " AUFTRAGSARTEN.AUFTRAGSART_ATU AS urspr_Auftrag, ";
$SQL.= " SCHADENSGRUND.GRUND AS woran_gearbeitet, BESCHAEDIGT.SCHLAGWORT AS was_beschaedigt, ";
$SQL.= " AUSFALLURSACHE.WERT AS Ausfallursache, SCHAEDEN_NEW.KFZ_KENNZ AS Kfz_Kennz, ";
$SQL.= " SCHAEDEN_NEW.KUNDENNAME AS KDName, ";
//$SQL.= " --Antragsart_Bezeichungen_QM.Antragart_Bezeichung, ";
$SQL.= " SCHAEDEN_NEW.ATUNR AS ATUNr";
$SQL.= " FROM SCHAEDEN_NEW ";
$SQL.= " LEFT JOIN AUSFALLURSACHE ON SCHAEDEN_NEW.AUSFALLURSACHE = AUSFALLURSACHE.ID";
//$SQL.= " --LEFT JOIN Anwaltsache ON SCHAEDEN_NEW.ANWALTSACHE = Anwaltsache.Rechtstreit_ID";
//$SQL.= " --LEFT JOIN [Filialliste_GBL+VKL_20080208] AS [Filialliste_GBL+VKL_20080208_1] ON SCHAEDEN_NEW.FILNR = [Filialliste_GBL+VKL_20080208_1].FIL_ID) ";
//$SQL.= " --LEFT JOIN [Filialliste_GBL+VKL_20080208] ON SCHAEDEN_NEW.VERURS_FILIALE = [Filialliste_GBL+VKL_20080208].FIL_ID) ";
$SQL.= " LEFT JOIN ZAHLUNGEN ON SCHAEDEN_NEW.BEARBEITUNGSNR = ZAHLUNGEN.BEARBEITUNGSNR";
$SQL.= " LEFT JOIN STAND ON SCHAEDEN_NEW.STAND = STAND.STANDID";
$SQL.= " LEFT JOIN AUFTRAGSARTEN ON SCHAEDEN_NEW.AUFTRAGSART_ATU_NEU = AUFTRAGSARTEN.ART_ID";
$SQL.= " LEFT JOIN SCHADENSGRUND ON SCHAEDEN_NEW.SCHADENSGRUND = SCHADENSGRUND.ID";
$SQL.= " LEFT JOIN BESCHAEDIGT ON SCHAEDEN_NEW.BID = BESCHAEDIGT.BID";
$SQL.= " LEFT JOIN KENNUNGEN ON SCHAEDEN_NEW.KENNUNG = KENNUNGEN.KENNUNG";
//$SQL.= " --LEFT JOIN Antragsart_Bezeichungen_QM ON SCHAEDEN_NEW.ANTRAGART = Antragsart_Bezeichungen_QM.Antragart_ID";
$SQL.= " WHERE SCHAEDEN_NEW.KENNUNG Not Like 'V%' ";
$SQL.= " AND SCHAEDEN_NEW.KENNUNG <> 'Z-STORNO' AND SCHAEDEN_NEW.KENNUNG <> 'Q-W-H-LUK'";
	      	
if($_POST['txtDatumVom']=='') //txtDatumVom
{
	$Datum = '01.01.2008';
}
else if (awis_PruefeDatum($_POST['txtDatumVom'],0,1) < awis_PruefeDatum('01.01.2008',0,1))
{
	$Datum = '01.01.2008';
}
else 
{
	$Datum = $_POST['txtDatumVom'];
}
	
$SQL .= " AND SCHAEDEN_NEW.EINGABEAM >= TO_DATE('" . $Datum . "','DD.MM.RRRR')";

if($_POST['txtDatumBis']=='') //txtDatumVom
{
	$Datumbis = date('d.m.Y');//'01.01.2008';
}

else//if($_POST['txtDatumBis']!='')		//txtDatumBis
{
	$Datumbis = $_POST['txtDatumBis'];	
}	
$SQL .= " AND SCHAEDEN_NEW.EINGABEAM <= TO_DATE('" . $Datumbis . "','DD.MM.RRRR')";

if($_POST['txtFIL_ID']!='')		//txtFIL_ID
{
	$SQL .= " AND SCHAEDEN_NEW.VERURS_FILIALE = ". $_POST['txtFIL_ID'] . "";		
}					
	
$SQL.= " GROUP BY SCHAEDEN_NEW.BEARBEITUNGSNR, SCHAEDEN_NEW.BEARBEITUNGSNRFIL, SCHAEDEN_NEW.EINGABEAM, SCHAEDEN_NEW.KENNUNG, ";
$SQL.= " KENNUNGEN.KENNUNG_BEZEICHNUNG, SCHAEDEN_NEW.FEHLERBESCHREIBUNG, SCHAEDEN_NEW.DIAGNOSE_WERKSTATTLEITER, ";
$SQL.= " SCHAEDEN_NEW.DIAGNOSE_TKDL, ";
//$SQL.= " --Anwaltsache.Anwaltsache, ";
$SQL.= " SCHAEDEN_NEW.FILNR, ";
//$SQL.= " --[Filialliste_GBL+VKL_20080208_1].FIL_BEZ, ";
$SQL.= " SCHAEDEN_NEW.VERURS_FILIALE, ";
//$SQL.= " --[Filialliste_GBL+VKL_20080208].FIL_BEZ, ";
$SQL.= " STAND.STANDBEM, AUFTRAGSARTEN.AUFTRAGSART_ATU, ";
$SQL.= " SCHADENSGRUND.GRUND, BESCHAEDIGT.SCHLAGWORT, AUSFALLURSACHE.WERT, SCHAEDEN_NEW.KFZ_KENNZ, ";
$SQL.= " SCHAEDEN_NEW.KUNDENNAME, ";
//$SQL.= " --Antragsart_Bezeichungen_QM.Antragart_Bezeichung, ";
$SQL.= " SCHAEDEN_NEW.ATUNR";				
$SQL.= " )";		
$SQL.= " GROUP BY VUFIL, KENNUNG_BEZEICHNUNG";
$SQL.= " ORDER BY VUFIL, KENNUNG_BEZEICHNUNG";

awis_Debug(1,$SQL);

$rsSchad = awisOpenRecordset($conSchad, $SQL);
$rsSchadZeilen = $awisRSZeilen;

$Spalte = 20;
$Zeile = 20;
$Ausdruck = null;

$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Schaden-Liste-Kompakt');

	$Ausdruck->NeueSeite(0,1);	
	$Ausdruck->_pdf->SetFillColor(210,210,210);

	$Zeile = 20;

	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);

	// �berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',14);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(180,6,'Schadenreport (Kompakt)',0,0,'L',0);
	
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile+15);
	$Ausdruck->_pdf->Cell(50,6,'Zeitraum: '. $Datum .' bis '. $Datumbis ,0,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+130,$Zeile+15);
	$Ausdruck->_pdf->Cell(50,6,'Erstellt am: '. date('d.m.Y'),0,0,'L',0);
	$Zeile+=15;
	
	$Ausdruck->_pdf->SetFont('Arial','B',10);
	
	$Zeile+=10;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(15,6,'VU-FIL',1,0,'C',1);
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+6);

	$Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);	
	$Ausdruck->_pdf->Cell(80,6,'Kennung',1,0,'C',1);
	$Ausdruck->_pdf->Line($Spalte+15,$Zeile,$Spalte+15,$Zeile+6);
	
	$Ausdruck->_pdf->SetXY($Spalte+95,$Zeile);
	$Ausdruck->_pdf->Cell(22,6,'Anzahl F�lle',1,0,'C',1);
	$Ausdruck->_pdf->Line($Spalte+95,$Zeile,$Spalte+95,$Zeile+6);
	
	$Ausdruck->_pdf->SetXY($Spalte+117,$Zeile);
	$Ausdruck->_pdf->Cell(30,6,'Fallkosten',1,0,'C',1);
	$Ausdruck->_pdf->Line($Spalte+117,$Zeile,$Spalte+117,$Zeile+6);							
	
	$Ausdruck->_pdf->SetXY($Spalte+147,$Zeile);
	$Ausdruck->_pdf->Cell(20,6,'Quote',1,0,'C',1);
	$Ausdruck->_pdf->Line($Spalte+147,$Zeile,$Spalte+147,$Zeile+6);	

	$Summe_Anzahl = 0;
	$Summe_Gesamt = 0;
	$Summe_Quote = 0;
	
	$Seite = 1;
	
	for($SchadZeile=0;$SchadZeile<$rsSchadZeilen;$SchadZeile++)
	{
		if ($Zeile > 260)
		{			
			$Ausdruck->_pdf->SetFont('Arial','',8);
			$Ausdruck->_pdf->SetXY($Spalte,280);
			$Ausdruck->_pdf->Cell(180,6,'Seite '. $Seite,0,0,'C',0);	
			
			$Zeile=16;
			$Zeile=$Zeile+10;
			$Seite = $Seite + 1;						
			
			$Ausdruck->NeueSeite(0,1);
			
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			//$pdf->setXY($LinkerRand,$Y_Wert);
			$Ausdruck->_pdf->SetFont('Arial','B',10);
			$Ausdruck->_pdf->Cell(15,6,'VU-FIL',1,0,'C',1);
			$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+6);
			
			$Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);
			$Ausdruck->_pdf->Cell(80,6,'Kennung',1,0,'C',1);
			$Ausdruck->_pdf->Line($Spalte+15,$Zeile,$Spalte+15,$Zeile+6);
			
			$Ausdruck->_pdf->SetXY($Spalte+95,$Zeile);
			$Ausdruck->_pdf->Cell(22,6,'Anzahl F�lle',1,0,'C',1);
			$Ausdruck->_pdf->Line($Spalte+95,$Zeile,$Spalte+95,$Zeile+6);
			
			$Ausdruck->_pdf->SetXY($Spalte+117,$Zeile);
			$Ausdruck->_pdf->Cell(30,6,'Fallkosten',1,0,'C',1);
			$Ausdruck->_pdf->Line($Spalte+117,$Zeile,$Spalte+117,$Zeile+6);							
			
			$Ausdruck->_pdf->SetXY($Spalte+147,$Zeile);
			$Ausdruck->_pdf->Cell(20,6,'Quote',1,0,'C',1);
			$Ausdruck->_pdf->Line($Spalte+147,$Zeile,$Spalte+147,$Zeile+6);							
		}
		
		$Zeile = $Zeile+6;
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(15,6,$rsSchad['VUFIL'][$SchadZeile],1,0,'L',0);
		
		$Ausdruck->_pdf->SetXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->Cell(80,6,$rsSchad['KENNUNG_BEZEICHNUNG'][$SchadZeile],1,0,'L',0);
		
		$Anzahl = intval($rsSchad['ANZAHL'][$SchadZeile]);
		$Ausdruck->_pdf->SetXY($Spalte+95,$Zeile);
		$Ausdruck->_pdf->Cell(22,6,$Anzahl,1,0,'R',0);
				
		$Summe = floatval(str_replace(",",".",$rsSchad['SUMME_KOSTEN'][$SchadZeile]));
		$Ausdruck->_pdf->SetXY($Spalte+117,$Zeile);
		$Ausdruck->_pdf->Cell(30,6,($Summe!=0?number_format($Summe,2,',','.').' �':''),1,0,'R',0);							
		
		$Summe = floatval(str_replace(",",".",$rsSchad['SUMME_KOSTEN'][$SchadZeile]));
		$Anzahl = intval($rsSchad['ANZAHL'][$SchadZeile]);
		
		$Quote = $Summe / $Anzahl;
		$Ausdruck->_pdf->SetXY($Spalte+147,$Zeile);
		$Ausdruck->_pdf->Cell(20,6,($Quote!=0?number_format($Quote,2,',','.').' �':''),1,0,'R',0);							
		
		$Summe_Anzahl = $Summe_Anzahl + $Anzahl;
		$Summe_Gesamt = $Summe_Gesamt + $Summe;
		$Summe_Quote = $Summe_Quote + $Quote;			
	}
	
	$Summe_Quote_2 = $Summe_Gesamt / $Summe_Anzahl;
	
	//Gesamtergebnis ausgeben
	$Zeile = $Zeile+6;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(95,6,'Gesamtergebnis:',1,0,'L',0);	
	
	$Ausdruck->_pdf->SetXY($Spalte+95,$Zeile);
	$Ausdruck->_pdf->Cell(22,6,$Summe_Anzahl,1,0,'R',0);
			
	$Ausdruck->_pdf->SetXY($Spalte+117,$Zeile);
	$Ausdruck->_pdf->Cell(30,6,($Summe_Gesamt!=0?number_format($Summe_Gesamt,2,',','.').' �':''),1,0,'R',0);							
		
	$Ausdruck->_pdf->SetXY($Spalte+147,$Zeile);
	$Ausdruck->_pdf->Cell(20,6,($Summe_Quote_2!=0?number_format($Summe_Quote_2,2,',','.').' �':''),1,0,'R',0);							

	
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY($Spalte,280);
	$Ausdruck->_pdf->Cell(180,6,'Seite '. $Seite,0,0,'C',0);	
	//$Ausdruck->Anzeigen();


?>