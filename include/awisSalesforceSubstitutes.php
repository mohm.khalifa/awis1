<?php

class awisSalesforceSubstitutes
{
    /**
     * @var awisDatenbank
     */
    private $_DB;

    function __construct($DB)
    {
        $this->_DB = $DB;
    }

    public function getSalesForce($SalesForces)
    {

        $Substitutes = [];

        foreach ($SalesForces as $SalesForce) {

            //Filialpersonal?
            if (in_array($SalesForce->RoleKey, [1, 2])) {
                $SQL = 'SELECT ';
                $SQL .= '   frz.*, ';
                $SQL .= '   fer_bezeichnung, ';
                $SQL .= '   kon_name1, ';
                $SQL .= '   kon_name2, ';
                $SQL .= '   kon_per_nr, ';
                $SQL .= '   xbl_login, ';
                $SQL .= '   xbn_key, ';
                $SQL .= '   (';
                $SQL .= '     SELECT ';
                $SQL .= '       kko_wert AS email_address ';
                $SQL .= '     FROM ';
                $SQL .= '       kontaktekommunikation ';
                $SQL .= '     WHERE ';
                $SQL .= '       kko_kot_key = 7 ';
                $SQL .= '       AND kko_kon_key = kon_key ';
                $SQL .= '       AND ROWNUM = 1';
                $SQL .= '   ) AS email_address ';
                $SQL .= ' FROM ';
                $SQL .= '   (';
                $SQL .= '     SELECT ';
                $SQL .= '       ok.* ';
                $SQL .= '     FROM ';
                $SQL .= '       filialebenenrollenzuordnungen nok ';
                $SQL .= '       LEFT JOIN filialebenenrollenzuordnungen ok ON nok.frz_xxx_key = ok.frz_xxx_key ';
                $SQL .= '       AND nok.frz_xtn_kuerzel = ok.frz_xtn_kuerzel ';
                $SQL .= '       AND ok.frz_fer_key IN (66) ';
                $SQL .= '     WHERE ';
                $SQL .= '       nok.frz_key = ' . $this->_DB->WertSetzen('FRZ', 'Z', $SalesForce->FRZ_KEY);
                $SQL .= '   ) frz ';
                $SQL .= '   INNER JOIN kontakte ON frz_kon_key = kon_key ';
                $SQL .= '   INNER JOIN benutzer ON xbn_kon_key = kon_key ';
                $SQL .= '   INNER JOIN benutzerlogins ON xbl_xbn_key = xbn_key ';
                $SQL .= '   INNER JOIN filialebenenrollen ON frz_fer_key = fer_key';

                $rsFRZ = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FRZ'));
            } else {
                //Ebenenpersonal
                $SQL = 'select a.*,';
                $SQL .= '   \'Vertreter\' as FER_BEZEICHNUNG, ';
                $SQL .= '   KON_NAME1, ';
                $SQL .= '   KON_NAME2, ';
                $SQL .= '   KON_PER_NR, ';
                $SQL .= '   XBL_LOGIN, ';
                $SQL .= '   XBN_KEY, ';
                $SQL .= ' XBN_KEY, ';
                $SQL .= '   (';
                $SQL .= '     select ';
                $SQL .= '       KKO_WERT as EMAIL_ADDRESS ';
                $SQL .= '     from ';
                $SQL .= '       KONTAKTEKOMMUNIKATION ';
                $SQL .= '     where ';
                $SQL .= '       KKO_KOT_KEY = 7 ';
                $SQL .= '       and KKO_KON_KEY = KON_KEY ';
                $SQL .= '       and rownum = 1';
                $SQL .= '   ) as EMAIL_ADDRESS ';
                $SQL .= ' from FILIALEBENENROLLENVERTRETER a';
                $SQL .= '  inner join KONTAKTE on FRV_KON_KEY = KON_KEY ';
                $SQL .= '   inner join BENUTZER on XBN_KON_KEY = KON_KEY ';
                $SQL .= '   inner join BENUTZERLOGINS on XBL_XBN_KEY = XBN_KEY ';
                $SQL .= ' where';
                $SQL .= ' a.FRV_DATUMVON <= sysdate ';
                $SQL .= ' and frv_datumbis >= sysdate';
                $SQL .= ' and frv_frz_key = ' . $this->_DB->WertSetzen('FRZ', 'Z', $SalesForce->FRZ_KEY);

                $rsFRZ = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FRZ'));
            }

            $Substitute = [];
            $Substitute['origin'] = $SalesForce;

            while (!$rsFRZ->EOF()) {
                $SalesForce = new stdClass();

                $SalesForce->key = $rsFRZ->FeldInhalt('XBN_KEY');
                $SalesForce->text = $rsFRZ->FeldInhalt('KON_NAME1') . ', ' . $rsFRZ->FeldInhalt('KON_NAME2') . '(' . $rsFRZ->FeldInhalt('FER_BEZEICHNUNG') . ')';
                $SalesForce->RoleKey = $rsFRZ->FeldInhalt('FRZ_FER_KEY');
                $SalesForce->Role = $rsFRZ->FeldInhalt('FER_BEZEICHNUNG');
                $SalesForce->FRZ_KEY = $rsFRZ->FeldInhalt('FRZ_KEY');
                $SalesForce->FirstName = $rsFRZ->FeldInhalt('KON_NAME1');
                $SalesForce->LastName = $rsFRZ->FeldInhalt('KON_NAME2');
                $SalesForce->EmployeeId = $rsFRZ->FeldInhalt('KON_PER_NR');
                $SalesForce->LoginName = $rsFRZ->FeldInhalt('XBL_LOGIN');
                $SalesForce->ValidFrom = $rsFRZ->FeldInhalt('FRZ_GUELTIGAB');
                $SalesForce->ValidTo = $rsFRZ->FeldInhalt('FRZ_GUELTIGBIS');
                $SalesForce->Notice = $rsFRZ->FeldInhalt('FRZ_BEMERKUNG');
                $SalesForce->EmailAddress = $rsFRZ->FeldInhalt('EMAIL_ADDRESS');

                $Substitute['substitutes'][] = $SalesForce;
                $rsFRZ->DSWeiter();
            }
            $Substitutes[] = $Substitute;
        }

        return $Substitutes;
    }

}