<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KDI','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht36000 = $AWISBenutzer->HatDasRecht(36000);
	
	$Fehler = false;
	
	$Param = array();
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Sort = 'KDI_Datum desc';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{
	  // wenn GET-Sort, dann nach diesen Feld sortieren
	 $Sort = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	if($Recht36000==0)
	{
		$Form->DebugAusgabe(1,"Hab keine Rechte" );
		$Form->Fehler_KeineRechte();
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		$Form->DebugAusgabe(1,"Bin in der Suche, weil ich mit cmdSuche gekommen bin" );

	    if (isset($_POST['txtKDI_FILID']) && $_POST['txtKDI_FILID'] != "") //bei Filialzugriff da Hiddentextfield.
	    {
	        $Param['KDI_FILID'] = $_POST['txtKDI_FILID'];
	    }
	    elseif(isset($_POST['sucKDI_FILID']) && $_POST['sucKDI_FILID'] != "") //bei regul�ren Zugriff das Textfeld benutzen. 
	    {
	       $Param['KDI_FILID'] = $_POST['sucKDI_FILID'];
	    }
	    else 
	    {
	        $Param['KDI_FILID'] = '';
	    }

	    $Param['KDI_DATUM_VON'] = $Form->Format('D',$_POST['sucKDI_DATUM_VON'],true);
	    $Param['KDI_DATUM_BIS'] = $Form->Format('T',$_POST['sucKDI_DATUM_BIS'],true);
	    $Param['KDI_KATEGORIE'] = $Form->Format('N0',$_POST['sucKDI_KATEGORIE']!=0?$_POST['sucKDI_KATEGORIE']:'',true);
	    $Param['KDI_HISTORISCH']=isset($_POST['sucHistorie'])?'on':'off';
	    $Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	    
	}
	elseif(isset($_GET['KDI_KEY'])) //Von Detail zu Unterdetail
	{
		$Form->DebugAusgabe(1,"Hab nen GET KDI_KEY" );
		 
		$AWIS_KEY1 = $_GET['KDI_KEY'];
	}
	elseif(isset($_POST['cmdSpeichern_x']) )
	{
	    //Feldpr�fungen vor dem speichern
	    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Kassendifferenzen'));
	    $Fehler = false;
	    if(!is_numeric($_POST['txtKDI_FILID']) OR $_POST['txtKDI_FILID'] == '' OR !isset($_POST['txtKDI_FILID']) )
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_FIL']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	    
	    $SQLCheckPersonal ='select Vorname ||\' \'|| name as pers_name from personal_komplett ';
	    $SQLCheckPersonal .=' where (datum_austritt >= sysdate or datum_austritt is null) AND persnr like \'' . $_POST['txtKDI_PERSNR'] .'\'';
	    
	    $RSCheckPersonal = $DB->RecordSetOeffnen($SQLCheckPersonal);

	    if($RSCheckPersonal->AnzahlDatensaetze()>0 == false)
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_PERSNR']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	    
	    if(!strtotime($_POST['txtKDI_DATUM']) OR $_POST['txtKDI_DATUM'] == '' OR !isset($_POST['txtKDI_DATUM']) OR $_POST['txtKDI_DATUM'] == '01.01.1970' )
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_DATUM']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	    
	    $Betrag = $_POST['txtKDI_BETRAG'];
	    $Betrag = str_replace(',', '.', $Betrag);
	    $Betrag = str_replace('', $AWISSprachKonserven['KDI']['KDI_EURO'], $Betrag);

	    if(!is_numeric($Betrag) or $Betrag == '' OR !isset($Betrag) )
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_BETRAG']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	    else 
	    {
	        $_POST['txtKDI_BETRAG'] = $Betrag;
	    }
	    
	    if($_POST['txtKDI_KATEGORIE'] == $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] OR $_POST['txtKDI_KATEGORIE'] == '' OR !isset($_POST['txtKDI_KATEGORIE']) )
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_KATEGORIE']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	    
	    if($_POST['txtKDI_BEGRUENDUNG'] == '' OR !isset($_POST['txtKDI_BEGRUENDUNG']) )
	    {
	        $Form->ZeileStart();
	        $Form->Hinweistext($AWISSprachKonserven['KDI']['KDI_ERR_BEGRUENDUNG']);
	        $Form->ZeileEnde();
	        $Fehler = true;
	    }
	     
	    $Form->DebugAusgabe(1,"Fehler: " . $Fehler);
	    
	    if($Fehler==False)
	    {
	    	$Form->DebugAusgabe(1,"Include speichern.php" );
	       include('./kassendifferenzen_speichern.php');
	       
	       $Form->Hinweistext("Daten wurden gespeichert");
	    }
	    else 
	    {
	        $AWIS_KEY1 = -1;
	    }
	    
	}
	else //nicht �ber die Suche gekommen.
	{
		$Form->DebugAusgabe(1,"Nicht �ber die Suche gekommen" );
	    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Kassendifferenzen'));
	    
	    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	    {
	    	$Form->DebugAusgabe(1,"Filialzugriff" );
	    	$Param['KDI_FILID'] = preg_replace('/^0+/', '', $FilZugriff);   
	    }
	}
	
	//Grund SELECT: Alle Derzeit G�ltigen Datens�tze
	$SQL =' SELECT KDI_KEY,';
    $SQL .='     KDI_FILID,';
    $SQL .='     KDI_DATUM,';
    $SQL .='     KDI_KATEGORIE,';
    $SQL .='     KDI_BETRAG,';
    $SQL .='     KDI_BEGRUENDUNG,';
    $SQL .='     KDI_PERSNR,';
    $SQL .='     KDI_USER,';
    $SQL .='     KDI_USERDAT,';
    $SQL .='     \'Normal\' AS tbl';
    $SQL .='   FROM Kassendifferenzen';
    
    if(isset($Param['KDI_HISTORISCH']) And $Param['KDI_HISTORISCH']  == 'on')
    {
        //Historische Daten sollen mit angezeigt werden
        
        //Subselect bauen f�r einschr�nkung
        $SQL = 'Select * from (' . $SQL;
        
        //Union Rumbauen und Subselect schlie�en. 
        //Die aliasse werden mit absicht so vergeben, damit man ganz normal drauf einschr�nken kann. 
        //im Feld tbl kann unterschieden werden, ob es ein Normaler DS ist, oder Hist
        $SQL .=' UNION ALL';
        $SQL .='   SELECT KDH_KEY    AS KDI_KEY,';
        $SQL .='     KDH_FILID       AS KDI_FILID,';
        $SQL .='     KDH_DATUM       as KDI_DATUM,';
        $SQL .='     KDH_KATEGORIE   AS KDI_KATEGORIE,';
        $SQL .='     KDH_BETRAG      AS KDI_BETRAG,';
        $SQL .='     KDH_BEGRUENDUNG AS KDI_BEGRUENDUNG,';
        $SQL .='     KDH_PERSNR      AS KDI_PERSNR,';
        $SQL .='     KDH_USER        AS KDI_USER,';
        $SQL .='     KDH_USERDAT     AS KDI_USERDAT,';
        $SQL .='     \'HIST\'          AS tbl';
        $SQL .='   FROM kassendifferenzen_hist';
        $SQL .='   )';
        
    }
    
	$Bedingung = '';

	if (isset($Param['KDI_FILID']) && $Param['KDI_FILID'] != ""  && is_numeric($Param['KDI_FILID'])) 
	{
	    $Bedingung .= 'AND KDI_FILID = ' . $Param['KDI_FILID'];
	}
	elseif(isset($Param['KDI_FILID']) AND !is_numeric($Param['KDI_FILID']) AND $Param['KDI_FILID'] != "" )
	{
	    $Bedingung .= 'AND KDI_FILID = 0' ;
	}
	
	if (isset($Param['KDI_DATUM_VON']) && $Param['KDI_DATUM_VON'] != "" )
	{
	    $Bedingung .= 'AND KDI_DATUM >= ' . '\'' . $Param['KDI_DATUM_VON'] . '\'';
	}
	
	if (isset($Param['KDI_DATUM_BIS']) && $Param['KDI_DATUM_BIS'] != "" )
	{
	    $Bedingung .= 'AND KDI_DATUM <= ' .  '\'' . $Param['KDI_DATUM_BIS'] . '\'';
	}
	
	if (isset($Param['KDI_KATEGORIE']) && $Param['KDI_KATEGORIE'] != "" )
	{
	    $Bedingung .= 'AND KDI_KATEGORIE = ' .  '\'' . $Param['KDI_KATEGORIE'] . '\'';
	}

    if($AWIS_KEY1 > 0){
        $Bedingung = ' AND KDI_KEY = 0'.$AWIS_KEY1;
    }

	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	
	if (isset($Sort))
	{
	    $SQL .= ' ORDER BY ' . $Sort;
	}

	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;

	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x']))
		{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
		}
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$Form->DebugAusgabe(1, $SQL);
	$rsHRK = $DB->RecordSetOeffnen($SQL);
	
	
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['FilID'] = 50;
	$FeldBreiten['Datum'] = 150;
	$FeldBreiten['Kategorie'] = 100;
	$FeldBreiten['Personalnummer'] = 80;
	$FeldBreiten['Betrag'] = 142;
	
	
	$Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./kassendifferenzen_Main.php?cmdAktion=Details>");
	$Form->Formular_Start();
	
	
	if($AWIS_KEY1 == '' and !isset($_POST['cmdDSNeu_x']))
	{
		if (($rsHRK->AnzahlDatensaetze() >= 1))
		{
		     //Listenansicht    
		    
			$Form->ZeileStart();
			$Link = './kassendifferenzen_Main.php?cmdAktion=Details&Sort=KDI_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KDI_FILID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KDI']['KDI_FILID'], $FeldBreiten['FilID'], '', $Link);
			
			$Link = './kassendifferenzen_Main.php?cmdAktion=Details&Sort=KDI_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KDI_DATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KDI']['KDI_DATUM'], $FeldBreiten['Datum'], '', $Link);
			
			$Link = './kassendifferenzen_Main.php?cmdAktion=Details&Sort=KDI_KATEGORIE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KDI_KATEGORIE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KDI']['KDI_KATEGORIE'], $FeldBreiten['Kategorie'], '', $Link);
				
			$Link = './kassendifferenzen_Main.php?cmdAktion=Details&Sort=KDI_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KDI_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KDI']['KDI_BETRAG'], $FeldBreiten['Betrag'], '', $Link);
						
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
			while(! $rsHRK->EOF())
			{
			    //Bei historischen Datensatz
			    if($rsHRK->FeldInhalt('TBL') == 'HIST')
			    {
			        $Link = './kassendifferenzen_Main.php?cmdAktion=Details&KDI_KEY=0'.$rsHRK->FeldInhalt('KDI_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . '&TBL=HIST';
			         $Hintergrund = 3;
			    }
                else 
                {
                    //Normaler DS
                    $Link = './kassendifferenzen_Main.php?cmdAktion=Details&KDI_KEY=0'.$rsHRK->FeldInhalt('KDI_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
                    $Hintergrund = $DS%2;
                }				
                
                $Form->ZeileStart();
                
				$TTT = $rsHRK->FeldInhalt('KDI_FILID');
				$Form->Erstelle_ListenFeld('KDI_FILID', $rsHRK->FeldInhalt('KDI_FILID'), 0, $FeldBreiten['FilID'], false, $Hintergrund, '', $Link, 'T', 'L', $TTT);
				
				$TTT = $rsHRK->FeldInhalt('KDI_DATUM');
				$Form->Erstelle_ListenFeld('KDI_DATUM', $rsHRK->FeldInhalt('KDI_DATUM'), 0, $FeldBreiten['Datum'], false, $Hintergrund, '', '', 'D', 'L', $TTT);
				
				
				$TTT = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_KATEGORIE']);
				$TTT = ($TTT[$rsHRK->FeldInhalt('KDI_KATEGORIE')-1]);
				$TTT = substr($TTT,strpos($TTT,'~')+1);
				$Form->Erstelle_ListenFeld('KDI_KATEGORIE', $rsHRK->FeldInhalt('KDI_KATEGORIE'), 0, $FeldBreiten['Kategorie'], false, $Hintergrund, '', '', 'T', 'L', $TTT);
				
				$TTT = $rsHRK->FeldInhalt('KDI_BETRAG');
				$Form->Erstelle_ListenFeld('KDI_BETRAG', $rsHRK->FeldInhalt('KDI_BETRAG').' '.$AWISSprachKonserven['KDI']['KDI_EURO'], 0, $FeldBreiten['Betrag'], false, $Hintergrund, '', '', 'T', 'R', $TTT);
		
				$Form->ZeileEnde();
				$DS++;
				$rsHRK->DSWeiter();
			}
			$Link = './kassendifferenzen_Main.php?cmdAktion=Details';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		}
		else 
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
	}
	elseif (isset($_POST['cmdDSNeu_x']) or $AWIS_KEY1 == -1)
	{
	    //neuen Datensatz hinzuf�gen
        $Aendernrecht = true;
	    
	    // Spaltenbreiten 
	    $FeldBreiten = array();
	    $FeldBreiten['FilID'] = 50;
	    $FeldBreiten['Datum'] = 20;
	    $FeldBreiten['Kategorie'] = 250;
	    $FeldBreiten['Personalnummer'] = 20;
	    $FeldBreiten['Betrag'] = 36;
	    $FeldBreiten['Labels'] = 160;
	    $FeldBreiten['Begruendung'] = 50;
		$FeldBreiten['Vorzeichen'] = 121;
	    
	    $Hist = false;
	    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	    {
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_FILID'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('*KDI_FILIDWERT', preg_replace('/^0+/', '', $FilZugriff),25,200,false);
	        $Form->Erstelle_HiddenFeld('KDI_FILID',preg_replace('/^0+/', '', $FilZugriff));
	        $Form->ZeileEnde();
	    }
	    else
	    {
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_FILID'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!KDI_FILID',isset($_POST['txtKDI_FILID'])?$_POST['txtKDI_FILID']:'',25,$FeldBreiten['FilID'],true);
	        $Form->ZeileEnde();
	         
	    }
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_PERSNR'].':',$FeldBreiten['Labels']);
	    $Form->Erstelle_TextFeld('!KDI_PERSNR',isset($_POST['txtKDI_PERSNR'])?$_POST['txtKDI_PERSNR']:'',$FeldBreiten['Personalnummer'],$FeldBreiten['Personalnummer']*10,true,'D','','','T');
	    $Form->ZeileEnde();
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_DATUM'].':',$FeldBreiten['Labels']);
	    $Form->Erstelle_TextFeld('!KDI_DATUM',isset($_POST['txtKDI_DATUM'])?$_POST['txtKDI_DATUM']:'',$FeldBreiten['Datum'],$FeldBreiten['Datum']*11,true,'D','','','D');
	    $Form->ZeileEnde();
	     
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_BETRAG'].':',$FeldBreiten['Labels']);
		$VorzeichenWerte = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_VORZEICHEN']);
		$Form->Erstelle_SelectFeld('!KDI_VORZEICHEN',isset($_POST['txtKDI_VORZEICHEN'])?$_POST['txtKDI_VORZEICHEN']:'',$FeldBreiten['Vorzeichen'],true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$VorzeichenWerte);
	    $Form->Erstelle_TextFeld('!KDI_BETRAG',isset($_POST['txtKDI_BETRAG'])?$_POST['txtKDI_BETRAG']:'',$FeldBreiten['Betrag'],$FeldBreiten['Betrag']*10,true,'D','','','T','L',$AWISSprachKonserven['KDI']['ttt_KDI_FORMAT'],'',0,'','','pattern="[0-9]+(\\,[0-9][0-9]?)?"');
	    $Form->ZeileEnde();
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_KATEGORIE'].':',$FeldBreiten['Labels']);
	    $KateogrieAlt = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_KATEGORIE']);
	    $Form->Erstelle_SelectFeld('!KDI_KATEGORIE',isset($_POST['txtKDI_KATEGORIE'])?$_POST['txtKDI_KATEGORIE']:'',380,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$KateogrieAlt);
	    $Form->ZeileEnde();
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_BEGRUENDUNG'].':',$FeldBreiten['Labels']);
	    $Form->Erstelle_TextArea('!KDI_BEGRUENDUNG',isset($_POST['txtKDI_BEGRUENDUNG'])?$_POST['txtKDI_BEGRUENDUNG']:'',100,50,5,true);
	    $Form->ZeileEnde();
	    

	}
	elseif (isset($_GET['KDI_KEY']))
	{
	    //�ndernrecht - Historische DS sind nicht �nderbar. 
	    if((($Recht36000&4)==4) AND !isset($_GET['TBL']))
	    {
	        $Aendernrecht = true; 
	    }
	    else 
	    {
	        $Aendernrecht = false; 
	    }
	    
	    if(isset($_GET['TBL']))
	    {
	       //Bei Historischen Datens�tze nur die Linke Spalte anzeigen, sodass DS nicht ge�ndert werden k�nnen.
	       $Hist = true;
	    }
	    else 
	    {
	        $Hist = false;
	    }
	    
	    //Feldbreiten 
	    $FeldBreiten = array();
	    $FeldBreiten['FilID'] = 4;
	    $FeldBreiten['Datum'] = 10;
	    $FeldBreiten['Betrag'] = 9;
	    $FeldBreiten['Begruendung'] = 30;
	    $FeldBreiten['Personalnummer'] = 10;
	    $FeldBreiten['Vorzeichen'] = 120;

	    
	    //Grund SELECT: Alle Derzeit G�ltigen Datens�tze
	    $SQL =' SELECT KDI_KEY,';
	    $SQL .='     KDI_FILID,';
	    $SQL .='     to_char(KDI_DATUM,\'DD.MM.RRRR\') as KDI_DATUM,';
	    $SQL .='     KDI_KATEGORIE,';
	    $SQL .='     KDI_BETRAG,';
	    $SQL .='     KDI_BEGRUENDUNG,';
	    $SQL .='     KDI_PERSNR,';
	    $SQL .='     KDI_USER,';
	    $SQL .='     KDI_USERDAT,';
	    $SQL .='     \'Normal\' AS tbl';
	    $SQL .='   FROM Kassendifferenzen';
	    
	    if(isset($_GET['TBL']))
	    {
	        //Historische Daten sollen mit angezeigt werden
	    
	        //Subselect bauen f�r einschr�nkung
	        $SQL = 'Select * from (' . $SQL;
	    
	        //Union Rumbauen und Subselect schlie�en.
	        //Die aliasse werden mit absicht so vergeben, damit man ganz normal drauf einschr�nken kann.
	        //im Feld tbl kann unterschieden werden, ob es ein Normaler DS ist, oder Hist
	        $SQL .=' UNION ALL';
	        $SQL .='   SELECT KDH_KEY    AS KDI_KEY,';
	        $SQL .='     KDH_FILID       AS KDI_FILID,';
	        $SQL .='     to_char(KDH_DATUM,\'DD.MM.RRRR\')       AS KDI_DATUM,';
	        $SQL .='     KDH_KATEGORIE   AS KDI_KATEGORIE,';
	        $SQL .='     KDH_BETRAG      AS KDI_BETRAG,';
	        $SQL .='     KDH_BEGRUENDUNG AS KDI_BEGRUENDUNG,';
	        $SQL .='     KDH_PERSNR      AS KDI_PERSNR,';
	        $SQL .='     KDH_USER        AS KDI_USER,';
	        $SQL .='     KDH_USERDAT     AS KDI_USERDAT,';
	        $SQL .='     \'HIST\'          AS tbl';
	        $SQL .='   FROM kassendifferenzen_hist';
	        $SQL .='   )';
	    }
	    //Einschr�nkung auf DS
		$SQL .= ' WHERE KDI_KEY='.$AWIS_KEY1;

		if(isset($_GET['TBL']))
		{
		  $SQL .= ' AND tbl=\'HIST\'';
		}
		
		$rsDetails = $DB->RecordSetOeffnen($SQL);
		
	
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kassendifferenzen_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsDetails->FeldInhalt('KDI_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsDetails->FeldInhalt('KDI_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		
		
		$Form->Erstelle_HiddenFeld('KDI_KEY', $AWIS_KEY1);
		$Form->Erstelle_HiddenFeld('Edit', $AWIS_KEY1);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',140);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_ORIGINAL'] . ':',250,'font-weight:bolder');
		if ($Hist == false and $Aendernrecht)
		{
		  $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_AENDERN'] . ':',250,'font-weight:bolder');
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_FILID'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('KDI_FILID'),250);
		if ($Hist == false and $Aendernrecht)
		{
		  $Form->Erstelle_TextFeld('!KDI_FILID',$Form->Format('T',$rsDetails->FeldInhalt('KDI_FILID')), $FeldBreiten['FilID'], $FeldBreiten['FilID']*10,$Aendernrecht, '','', '','', 'L');
		}
		  $Form->ZeileEnde();
		
        //Personalname Holen
		  
		$SQLPersName ='select Vorname ||\' \'|| name as pers_name from v_personal_komplett ';
		$SQLPersName .=' where (datum_austritt >= sysdate or datum_austritt is null) AND persnr = ' . $rsDetails->FeldInhalt('KDI_PERSNR');
        
		$rsPersName = $DB->RecordSetOeffnen($SQLPersName);
        

    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_PERSNR'] . ':',140);
    	$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('KDI_PERSNR'),70);
    	$Form->Erstelle_TextLabel($rsPersName->FeldInhalt('PERS_NAME'),180, 'font-weight:bolder');
    	if ($Hist == false and $Aendernrecht)
    	{
    	    $Form->Erstelle_TextFeld('!KDI_PERSNR',$Form->Format('T',$rsDetails->FeldInhalt('KDI_PERSNR')), $FeldBreiten['Personalnummer'], $FeldBreiten['Personalnummer']*10,$Aendernrecht, '','', '','', 'L');
    	}
    	$Form->ZeileEnde();
		  
		
    	$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_DATUM'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('KDI_DATUM'),250);
		if ($Hist == false and $Aendernrecht)
		{
		  $Form->Erstelle_TextFeld('!KDI_DATUM',$Form->Format('T',$rsDetails->FeldInhalt('KDI_DATUM')), $FeldBreiten['Datum'], $FeldBreiten['Datum']*11,$Aendernrecht, '','', '','D', 'L');
		}
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_BETRAG'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('KDI_BETRAG'),250);
		
		if ($Hist == false and $Aendernrecht)
		{
			$VorzeichenWerte = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_VORZEICHEN']);
			$Form->Erstelle_SelectFeld('!KDI_VORZEICHEN',($rsDetails->FeldInhalt('KDI_BETRAG')<0)?'-':'+',$FeldBreiten['Vorzeichen'],$Aendernrecht,'','','','','',$VorzeichenWerte);

			//Da im Textfeld das Minus vom Betrag entfernt wird, muss f�r den HIST Insert der alte Wert in einem Hiddenfeld gehalten werden!
			$Form->Erstelle_HiddenFeld('KDI_BETRAG_HIST',$Form->Format('W',$rsDetails->FeldInhalt('KDI_BETRAG')));
			$Form->Erstelle_TextFeld('!KDI_BETRAG',str_replace('-','',$Form->Format('T',$rsDetails->FeldInhalt('KDI_BETRAG'))), $FeldBreiten['Betrag'], $FeldBreiten['Betrag']*10,$Aendernrecht, '','', '','','L',$AWISSprachKonserven['KDI']['ttt_KDI_FORMAT'],'','','','','pattern="[0-9]+(\\,[0-9][0-9]?)?"');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_EURO'],'');
		}
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$KateogrieAlt = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_KATEGORIE']);
		$KateogrieAlt = ($KateogrieAlt[$rsDetails->FeldInhalt('KDI_KATEGORIE')-1]);
		$KateogrieAlt = substr($KateogrieAlt,strpos($KateogrieAlt,'~')+1);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_KATEGORIE'] . ':',140);
		$Form->Erstelle_TextLabel($KateogrieAlt,250);
		$Form->Erstelle_HiddenFeld('oldKDI_KategorieHidden', $rsDetails->FeldInhalt('KDI_KATEGORIE'));
		$Daten = explode('|',$AWISSprachKonserven['KDI']['LST_KDI_KATEGORIE']);
		if ($Hist == false and $Aendernrecht)
		{
		  $Form->Erstelle_SelectFeld('!KDI_KATEGORIE',$rsDetails->FeldInhalt('KDI_KATEGORIE'),'450:390',$Aendernrecht,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		}
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_BEGRUENDUNG'] . ':',140);
		$Form->Erstelle_TextArea('KDI_BEGRUENDUNG',$rsDetails->FeldInhalt('KDI_BEGRUENDUNG'),250,30,5,false);
		if ($Hist == false and $Aendernrecht)
		{
		  $Form->Erstelle_TextArea('!KDI_BEGRUENDUNG',$rsDetails->FeldInhalt('KDI_BEGRUENDUNG'),100,52,5,$Aendernrecht);
		}
		$Form->ZeileEnde();
		
		
	}
	
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht36000&2)  == 2 and !isset($_GET['KDI_KEY']) and !isset($_POST['cmdDSNeu_x']) or $Fehler = false)		// Hinzuf�gen erlaubt UND Nicht in Detailansicht eines DS
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	
	if ((isset($_GET['KDI_KEY']) or isset($_POST['cmdDSNeu_x']) or $Fehler == true)AND $Hist <> true AND (isset($Aendernrecht) AND ($Aendernrecht))) //Nur in der Unterdetailmaske speichern
	{
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	if(!isset($_GET['KDI_KEY']))
	{
	   $AWISBenutzer->ParameterSchreiben('Formular_Kassendifferenzen',serialize($Param));
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>