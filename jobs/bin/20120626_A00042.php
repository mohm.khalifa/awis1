<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');

/*
 * Anforderung A00042 Import der Aktionspreise T�V 
*/

try
{
	$Anforderung = 'A00042';
	$QuellTabelle = 'XXX_'.$Anforderung;
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$SQL = 'SELECT * FROM '.$QuellTabelle;
	
	$rsQuelle = $DB->RecordSetOeffnen($SQL);
	
	$DB->TransaktionBegin();
	
	while (!$rsQuelle->EOF())
	{
		$SQL  = 'MERGE INTO FILIALINFOS FIL_INFO ';
		$SQL .= ' USING (SELECT :var_N0_FIF_FIl_ID AS FIF_FIL_ID, :var_T_FIF_FIT_ID AS FIF_FIT_ID, :var_T_AKTIONSPREIS AS FIF_WERT FROM DUAL) IMP';
		$SQL .= ' ON (FIL_INFO.FIF_FIL_ID=IMP.FIF_FIL_ID AND';
		$SQL .= ' FIL_INFO.FIF_FIT_ID=IMP.FIF_FIT_ID)';
		$SQL .= 'WHEN  MATCHED THEN';
		$SQL .= ' UPDATE';
		$SQL .= ' SET FIL_INFO.FIF_WERT   = :var_T_AKTIONSPREIS,';
		$SQL .= ' FIL_INFO.FIF_USER       = \'Import\',';
		$SQL .= ' FIL_INFO.FIF_USERDAT    = SYSDATE';
		$SQL .= ' WHEN  NOT MATCHED THEN';
		$SQL .= ' INSERT';
		$SQL .= '(';
		$SQL .= 'FIL_INFO.FIF_FIL_ID,';
		$SQL .= 'FIL_INFO.FIF_FIT_ID,';
		$SQL .= 'FIL_INFO.FIF_WERT,';
		$SQL .= 'FIL_INFO.FIF_READONLY,';
		$SQL .= 'FIL_INFO.FIF_IMQ_ID,';
		$SQL .= 'FIL_INFO.FIF_USER,';
		$SQL .= 'FIL_INFO.FIF_USERDAT';
		$SQL .= ')';
		$SQL .= ' VALUES';
		$SQL .= '(';
		$SQL .= ':var_N0_FIF_FIl_ID,';
		$SQL .= ':var_T_FIF_FIT_ID,';
		$SQL .= ':var_T_AKTIONSPREIS,';
		$SQL .= '1,';
		$SQL .= '4,';
		$SQL .= '\'Import\',';
		$SQL .= 'SYSDATE';
		$SQL .= ') '; 

		// Aktion AU/HU FIT_ID=203
		$BindeVariablen = array();
		$BindeVariablen['var_N0_FIF_FIl_ID'] = $rsQuelle->FeldInhalt('FILID');
		$BindeVariablen['var_T_FIF_FIT_ID'] = '203';
		$BindeVariablen['var_T_AKTIONSPREIS'] = $rsQuelle->FeldInhalt('AUHU');
		$DB->Ausfuehren($SQL,'',true,$BindeVariablen);
		
		// Aktion AU/HU S�umniszuschlag FIT_ID=208
		$BindeVariablen = array();
		$BindeVariablen['var_N0_FIF_FIl_ID'] = $rsQuelle->FeldInhalt('FILID');
		$BindeVariablen['var_T_FIF_FIT_ID'] = '208';
		$BindeVariablen['var_T_AKTIONSPREIS'] = $rsQuelle->FeldInhalt('AUHU2');
		$DB->Ausfuehren($SQL,'',true,$BindeVariablen);

		$rsQuelle->DSWeiter();
		
	} //end while

	//$DB->TransaktionRollback();
	$DB->TransaktionCommit();
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
}

?>
