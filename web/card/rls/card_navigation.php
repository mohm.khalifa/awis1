<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

?>
<html>
<head>
	<title>Navigation Card</title>
	<?php include("card/css/css_card_master.php"); ?>
	
	<script language="JavaScript">
	
	//Datensatz wird aufgrund der Rechnungsnr erneut eingelesen
	function aktualisieren()
	{
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit("Datensatz aktualisieren");
		<?php } ?>
		
		try
		{
			var Rec_rnr = parent.card_haupt.document.getElementById("Rec_rnr").value;
			parent.card_haupt.location.href="card_haupt.php?strRec_rnr="+Rec_rnr;
		}
		catch(e)
		{
			//Objekt mit id="Rec_rnr" ist nicht vorhanden
		}
	}
	
	function rechnungsnrsuchen()
	{
		var obj = document.getElementById('strRec_rnr');
		var strRec_rnr = obj.value;
		
		var strAusdruck = /^[0-9]{2}(01|02|03|04|05|06|07|08|09|10|11|12)[0-9]{6}/;
		
		if((strRec_rnr.search(/^[0-9]{1,6}$/) == -1&&!strAusdruck.test(strRec_rnr))||strRec_rnr.length<6)
		{
			if(strRec_rnr.length>6)
				intZahl=obj.getAttribute('maxLength');
			else 
				intZahl=6;
				
			alert("Bitte korrekte Rechnungsnummer ("+intZahl+" Ziffern) angeben!");
			obj.value='';
			obj.focus();
			return false;
		}
		
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit('Rechnungs-Nr suchen');		//Zeitmessung
		<?php } ?>
					
		obj.value = '';
		obj.focus();
		
		top.card_haupt.location.href="card_haupt.php?strRec_rnr="+strRec_rnr+"&formRgnrGesendet=1";
		
		return false;
	}
	
	// Wird nicht mehr gebraucht: 27.10.04
	
	function rechnungsnrcheck()
	{
		//Rechnungsnr ist 6- oder 10-stellig (wenn JJMMzahlzahl)
		var obj = document.getElementById('strRec_rnr');
		var strRec_rnr = obj.value;
		
		var strAusdruck = /^[0-9]{2}(01|02|03|04|05|06|07|08|09|10|11|12)[0-9]{2}/;
		
		if(strAusdruck.test(strRec_rnr))
		{
			obj.setAttribute('maxLength',10);
		}
		else
		{
			if(strRec_rnr.length>6)
			{
				obj.value=strRec_rnr.substr(0,6);
				obj.setAttribute('maxLength',6);
			}
		}
	}
	
	
	function kundensuchen()
	{
		var obj = document.getElementById('strKundenname');
		var strKundenname = obj.value;
		
		if(strKundenname == '')
		{
			alert("Bitte Suchkriterium angeben!");
			obj.focus();
			return false;
		}
		
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit('Kunde suchen');  //Zeitmessung
		<?php } ?>
		
		var objGrossklein = document.getElementById("grossklein");
		var objTextanfang = document.getElementById("textanfang");
		
		var intGrossklein = 0;
		var intTextanfang = 0;
		
		if(objGrossklein.checked)
		{
			intGrossklein = 1;
			objGrossklein.checked = false;
		}
		
		if(objTextanfang.checked)
		{
			intTextanfang = 1;
			objTextanfang.checked = false;
		}
				
		obj.value = '';
		obj.focus();
		
		//Dialogfenster �ffnen
		
		var parameter = "strKundenname="+escape(strKundenname)+"&intGrossklein="+intGrossklein+"&intTextanfang="+intTextanfang;
		
		<?php echo dialog("kundensuche_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			top.card_haupt.location.href="card_haupt.php?strKunden_Nr="+rueckgabe;
		}
			
		return false;
	}
	
	function kundennrsuchen()
	{
		var obj = document.getElementById('strKundennr');
		var strKundennr = obj.value;
		
		if(strKundennr.search(/^[0-9]{8}$/) == -1)
		{
			alert("Bitte 8-stellige Zahl als Kundennummer angeben!");
			obj.value='';
			obj.focus();
			return false;
		}
		
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit('Kundennr suchen');  //Zeitmessung
		<?php } ?>
				
		obj.value = '';
		obj.focus();
		
		//Dialogfenster �ffnen
		
		var parameter = "intDialog=1&strKundennr="+strKundennr;
		
		<?php echo dialog("rechnungen_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			top.card_haupt.location.href="card_haupt.php?strRec_rnr="+rueckgabe;
		}
			
		return false;
	}
	
	function importieren()
	{
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit('Rechnungen importieren');  //Zeitmessung
		<?php } ?>
		top.card_haupt.location.href="card_haupt.php?intImport=1";
	}
		
	//Mahnungen aufbereiten
	
	function mahnungenaufbereiten()
	{
		parent.card_haupt.location.href='card_mahnungen_aufbereiten.php';
	}
	
	//pdf-Datei Zahlungsjournal erstellen
	
	function zahlungsjournal()
	{
		parent.card_haupt.location.href='card_zahlungsjournal.php';
	}
	
	//Sonderkonditionen Zahlungsziel
	
	function sonderkonditionen_zz()
	{
		<?php
		if($intZeitmessung)
		{
		?>
			parent.card_ladezeit.sendezeit("Sonderkonditionen Zahlungsziel anzeigen");	//Zeitmessung
		<?php } ?>
			
		var parameter = "intDialog=1";
	
		<?php echo dialog("sonderkonditionenzz_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			var intKazId=rueckgabe;
		}
	}
	
	//Liste offene Vorg�nge anzeigen
	
	function offene_vorgaenge()
	{
		parent.card_haupt.location.href='card_offene_vorgaenge.php';
	}
	
	function rgnrblaettern(strRichtung)
	{
		var objRnr = document.getElementById("aktrnr");
		var strRnr = objRnr.value;
		if(strRnr!='undefined'&&strRnr!='')
		{
			objRnr.value='';
			top.card_haupt.location.href="card_haupt.php?strRecBlaett_rnr="+strRnr+"&strRichtung="+strRichtung;
		}
	}
	
	</script>
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="100%" id="tabellenavigation">
<tr><td align="center">

	<table border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
		<td>Rechnungs-Nr:</td>
		<form onSubmit="return rechnungsnrsuchen();"">
		<input type="hidden" name="formRgnrGesendet" value="1">
		<td class="text">
		<input type="text" id="strRec_rnr" maxlength="12">
		</td>
		<td class="img"><img src="/bilder/eingabe_ok.png" class="quadrat" alt="Suche starten" onclick="rechnungsnrsuchen();"></td>
		</form>
		<td rowspan="2">&nbsp;</td>
		<form onSubmit="return kundensuchen();">
		<td>Kunde suchen:</td>
		<td class="text">
		<input type="text" id="strKundenname" maxlength="50">
		</td>
		<td class="img"><img src="/bilder/eingabe_ok.png" class="quadrat" alt="Suche starten" onclick="kundensuchen();"></td>
		</form>
		<td rowspan="2">&nbsp;</td>
		<td><img src="/bilder/hilfe.png" alt="Hilfe (Alt + H)" class="quadrat"><img src="/bilder/aktualisieren.png" alt="Inhalt aktualisieren" class="quadrat" onclick="aktualisieren();"><img src="/bilder/zurueck.png" alt="zur&uuml;ck" class="quadrat">
		</td>
		</tr>
		<form onSubmit="return kundennrsuchen();">
		<td>Kunden-Nr:</td>
		<td class="text">
		<input type="text" id="strKundennr" maxlength="8">
		</td>
		<td class="img"><img src="/bilder/eingabe_ok.png" class="quadrat" alt="Suche starten" onclick="kundennrsuchen();"></td>
		</form>
		
		
		
		
		<td>&nbsp;</td>
		<td>	
			<table border="0" cellpadding="0" cellspacing="0" align="center" class="leer">
			<tr class="leer">
			<td class="leer"><span title="Gro&szlig;-/Kleinschreibung beachten">G/K:</span></td>
			<td class="leer"><input type="checkbox" id="grossklein" title="Gro&szlig;-/Kleinschreibung beachten" onclick="document.getElementById('strKundenname').focus();"></td>
			<td class="leer"><span title="Nur am Textanfang suchen">&nbsp;TA:</span></td>
			<td class="leer"><input type="checkbox" id="textanfang" title="Nur am Textanfang suchen" onclick="document.getElementById('strKundenname').focus();"></td>
			</tr>
			</table>
		</td>
		<td>&nbsp;</td>
		<td><img src="/bilder/zur.png" alt="eine Rechnungs-Nr zur&uuml;ck" class="quadrat" onclick="rgnrblaettern('zur');"><img src="/bilder/vor.png" alt="eine Rechnungs-Nr vor" class="quadrat" onclick="rgnrblaettern('vor');"><input type="hidden" id="aktrnr"></td>
		</tr>
		</table>
		
		<table border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
		<td><button type="button" onclick="importieren();">Rechnungsimport</button></td>
		<td><button type="button" onclick="parent.card_haupt.location.href='card_kontoauszuege.php';">Kontoausz&uuml;ge</button></td>
		<td><button type="button" onclick="mahnungenaufbereiten();">Mahnungen</button></td>
		<td><button type="button" onclick="zahlungsjournal();">Zahlungsjournal</button></td>
		<td><button type="button" onclick="sonderkonditionen_zz();">SonderkondZZ</button></td>
		<td><button type="button" onclick="offene_vorgaenge();">Offene Vorg&auml;nge</button></td>
		<!--<td><button type="button" onclick="importieren();">Rechnungsimport</button></td>
		<td>&nbsp;</td>
		<td colspan="7">
		<button type="button" onclick="parent.card_haupt.location.href='card_kontoauszuege.php';">Kontoausz&uuml;ge</button>
		</td></tr>
		<tr>
		<td>
		<button type="button" onclick="mahnungenaufbereiten();">Mahnungen</button>
		<button type="button" onclick="zahlungsjournal();">Zahlungsjournal</button>
		</td>
		<td><button type="button" onclick="sonderkonditionen_zz();">SonderkondZZ</button></td>
		<td colspan="7"><button type="button" onclick="offene_vorgaenge();">Offene Vorg&auml;nge</button></td>-->
		</tr>
	</table>
</td></tr>
</table>
</body>
</html>
