<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN2152">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="Sacha Kerres, ATU Autoteile Unger">
<?php
/**
 *
 * benutzerverwaltung_Main.php
 *
 * Hauptmaske f�r die Benutzerverwaltung
 *
 * @author Sacha Kerres
 * @version 20080427
 *
 * @uses awis_forms.inc.php
 * @uses db.inc.php
 * @uses sicherheit.inc.php
 * @uses register.inc.php
 *
 * @copyright ATU Auto-Teile-Unger
 * @package Benutzerverwaltung
 *
 *  */
/*require_once("/daten/include/register.inc.php");
require_once("/daten/include/db.inc.php");		// DB-Befehle
require_once("/daten/include/sicherheit.inc.php");
require_once("/daten/include/awis_forms.inc.php");
*/
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;
try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	$SK_KEY=0;
	print "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TITEL','tit_Benutzerverwaltung');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_reset');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Benutzerverwaltung'].'</title>';
	?>
	</head>
	<body>
	<?php
	
	include ("/daten/include/awisHeader.inc");	// Kopfzeile

	// Rechte abfragen
	$Recht1 = $AWISBenutzer->HatDasRecht(1);
	if($Recht1==0)
	{
	    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/cmd_zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form = new awisFormular();

	$cmdAktion='';

	if(isset($_GET['cmdAktion']))
	{
		$cmdAktion = $_GET['cmdAktion'];
	}
	if($cmdAktion=='' AND isset($_POST['cmdSuche_x']))
	{
		$cmdAktion='Details';
	}

	if(isset($_POST['cmdXReset_x']))
	{
		awis_BenutzerParameterSpeichern($con, "ArtikelStammSuche", $AWISBenutzer->BenutzerName(),'');
		$cmdAktion='Suche';
	}

	/************************************************
	* Daten anzeigen
	************************************************/
	$Register = new awisRegister(100);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));
//$Form->DebugAusgabe(1,$_GET,$_POST);

	echo '</form>';

	/**********************************/
	// Cursor auf das erste Feld setzen
	//*********************************/
	if($AWISCursorPosition!='')
	{
		echo '<script type="text/javascript">';
		echo "document.getElementsByName('".$AWISCursorPosition."')[0].focus();";
		echo '</script>';
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}
?>
</body>
</html>