<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISSprache;
global $DB;
global $AWISBenutzer;

try 
{
    require_once ('awisDatenbank.inc');
    require_once('awisDokument.inc');
    
    $TextKonserven=array();
    $TextKonserven[]=array('Wort','WirklichLoeschen');
    $TextKonserven[]=array('Wort','Ja');
    $TextKonserven[]=array('Wort','Nein');
    $TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler','err_keineDaten');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();    
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Tabelle= '';
    
    if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Adresse
    {
        $Tabelle = 'ADR';
        $Key=$_POST['txtADR_KEY'];
    
        $Felder=array();
        $Felder[]=array($Form->LadeTextBaustein('ADR_NAME1','Adressen',$AWISSprache),$_POST['txtADR_NAME1']);
        $Felder[]=array($Form->LadeTextBaustein('ADR_NAME2','Adressen',$AWISSprache),$_POST['txtADR_NAME2']);
    }
    elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
    {
        switch($_GET['Seite'])
        {
        	case 'Infos':
        	    $Tabelle = 'ADI';
        	    $Key=$_GET['Del'];
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT ADI_BEMERKUNG,ADI_WERT,ADT_INFORMATION FROM AdressenInformationen INNER JOIN AdressenInfoTypen ON ADI_ADT_KEY = ADT_KEY WHERE ADI_KEY=0'.$Key);
        	    
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('ADT_INFORMATION','ADT',$AWISSprache),$rsDaten->FeldInhalt('ADT_INFORMATION'));
        	    $Felder[]=array($Form->LadeTextBaustein('ADI_WERT','ADI',$AWISSprache),$rsDaten->FeldInhalt('ADI_WERT'));
        	    $Felder[]=array($Form->LadeTextBaustein('ADI_BEMERKUNG','ADI',$AWISSprache),$rsDaten->FeldInhalt('ADI_BEMERKUNG'));
        	    
        	    break;
        	case 'Datentabelle':
        	    $Tabelle = 'XXX';
        	    $Key=$_GET['Del'];
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT ADI_BEMERKUNG,ADI_WERT,ADT_INFORMATION FROM AdressenInformationen INNER JOIN AdressenInfoTypen ON ADI_ADT_KEY = ADT_KEY WHERE ADI_DATENZEILE=0'.$Key);
        	    
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('ADT_INFORMATION','ADT',$AWISSprache),$rsDaten->FeldInhalt('ADT_INFORMATION'));
        	    $Felder[]=array($Form->LadeTextBaustein('ADI_WERT','ADI',$AWISSprache),$rsDaten->FeldInhalt('ADI_WERT'));
        	    
        	    break;
        	case 'Ansprechpartner':
        	    $Tabelle = 'AAP';
        	    $Key=$_GET['Del'];
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT AAP_NACHNAME, AAP_VORNAME FROM AdressenAnsprechpartner WHERE AAP_KEY=0'.$Key);
        	    
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('AAP_NACHNAME','AAP',$AWISSprache),$rsDaten->FeldInhalt('AAP_NACHNAME'));
        	    $Felder[]=array($Form->LadeTextBaustein('AAP_VORNAME','AAP',$AWISSprache),$rsDaten->FeldInhalt('AAP_VORNAME'));

        	    break;
        	case 'Dokumente':
        	    $Tabelle = 'DOC';
        	    $Key=$_GET['Del'];
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT DOC_DATUM, DOC_BEZEICHNUNG FROM Dokumente WHERE DOC_KEY=0'.$Key);
        	    
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('DOC_DATUM','DOC',$AWISSprache),$rsDaten->FeldInhalt('DOC_DATUM'));
        	    $Felder[]=array($Form->LadeTextBaustein('DOC_BEZEICHNUNG','DOC',$AWISSprache),$rsDaten->FeldInhalt('DOC_BEZEICHNUNG'));
        	    break;
        }
    }
    elseif(isset($_GET['DelADI']) AND !isset($_POST['cmdLoeschen_x']))
    {
        $Tabelle = 'ADI';
        $Key=$_GET['DelADI'];
        $rsDaten = $DB->RecordSetOeffnen('SELECT ADI_BEMERKUNG,ADI_WERT,ADT_INFORMATION FROM AdressenInformationen INNER JOIN AdressenInfoTypen ON ADI_ADT_KEY = ADT_KEY WHERE ADI_KEY=0'.$Key);
        
        $Felder=array();
        $Felder[]=array($Form->LadeTextBaustein('ADT_INFORMATION','ADT',$AWISSprache),$rsDaten->FeldInhalt('ADT_INFORMATION'));
        $Felder[]=array($Form->LadeTextBaustein('ADI_WERT','ADI',$AWISSprache),$rsDaten->FeldInhalt('ADI_WERT'));
        $Felder[]=array($Form->LadeTextBaustein('ADI_BEMERKUNG','ADI',$AWISSprache),$rsDaten->FeldInhalt('ADI_BEMERKUNG'));
    }
    elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) 
        {
        	case 'ADR':
        	    $SQL = 'DELETE FROM Adressen WHERE adr_key=0'.$_POST['txtKey'];
        	    $AWIS_KEY1=0;
        	    break;
        	case 'ADI':
        	    $SQL = 'DELETE FROM AdressenInformationen WHERE adi_key=0'.$_POST['txtKey'];
        	    $Param=unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
        	    $AWIS_KEY1=$Param['KEY'];
        	    break;
        	case 'XXX':
        	    $SQL = 'DELETE FROM AdressenInformationen WHERE adi_datenzeile=0'.$_POST['txtKey'];
        	    $Param=unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
        	    $AWIS_KEY1=$Param['KEY'];
        	    break;
        	case 'AAP':
        	    $SQL = 'DELETE FROM AdressenAnsprechpartner WHERE aap_key=0'.$_POST['txtKey'];
        	    $Param=unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
        	    $AWIS_KEY1=$Param['KEY'];
        	    break;
        	case 'DOC':
        	    $SQL = '';
        	    $DOC = new awisDokument($DB, $AWISBenutzer);
        	    $DOC->DokumentLoeschen($_POST['txtKey']);
        	    $Form->DebugAusgabe(1,$_POST);
        	    $AWIS_KEY1 = $AWISBenutzer->ParameterLesen('AktuelleAdresse');
        	default:
        	    break;
        }
    
        if($SQL !='')
        {
            if($DB->Ausfuehren($SQL)===false)
            {
                throw new awisException('Fehler beim Loeschen',201403240929,$SQL,awisException::AWIS_ERR_SYSTEM);
            }
        }
    }
    if($Tabelle!='')
    {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
        echo '<form name=frmLoeschen action=./adressen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
        echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';
    
        foreach($Felder AS $Feld)
        {
            echo '<br>'.$Feld[0].': '.$Feld[1];
        }
    
        echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
        echo '<input type=hidden name=txtKey value="'.$Key.'">';
        echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
        echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';
        echo '</form>';
        $DB->Schliessen();
        die();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>