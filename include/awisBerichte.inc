<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBarcode.inc';
require_once('fpdf.php');
require_once('fpdi.php');

define('FPDF_FONTPATH','/daten/include/font/');

/**
 * Klasse zum Erzeugen von PDF Dateien
 * @author kerres_s
 *
 */
abstract class awisBerichte
extends FPDI
{
	/**
	 * Zeilen f�r ein A4 Blatt
	 * @var int
	 */
	const ZEILENA4 = 290;
	/**
	 * Spalten f�r ein A4 Blatt
	 * @var int
	 */
	const SPALTENA4 = 210;
	/**
	 * Linker Rand f�r die PDFs
	 * @var int
	 */
	const LINKERRAND = 10;
	/**
	 * Rechter Rand f�r die PDFs
	 * @var int
	 */
	const RECHTERRAND = 10;
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	protected $_DB = null;
	/**
	 * Recordset mit den Daten
	 *
	 * @var awisRecordset
	 */
	protected $_rec = null;

	/**
	 * BerichtsID vom aktuellen Bericht
	 * @var int
	 */
	protected  $_BerichtsID = 0;
	/**
	 * Name des Berichts
	 *
	 * @var string
	 */
	protected $_BerichtsName='';

	/**
	 * Dateiname, wenn der Bericht als Datei abgelegt werden soll.
	 * @var string
	 */
	protected $_Dateiname='';
	/**
	 * Ausgabeformat des Berichts (A4)
	 *
	 * @var string
	 */
	protected $_BerichtsFormat='';

	/**
	 * Ausrichtung (P/L)
	 *
	 * @var unknown_type
	 */
	protected $_BerichtsAusrichtung='';

	/**
	 * Zu verwendende Vorlage f�r diesen Bericht
	 *
	 * @var string
	 */
	protected $_BerichtsVorlage='';

	/**
	 * Gibt an, ob ein eigener Kopf gedruckt werden muss
	 *
	 * @var int
	 */
	private $_BerichtsKopfDrucken = 0;

	/**
	 * Webtexte f�r die Ausgabe
	 *
	 * @var array
	 */
	protected $_AWISSprachkonserven = array();
	/**
	 * Aktueller Benutzer
	 *
	 * @var awisBenutzer
	 */
	protected $_AWISBenutzer= null;

	/**
	 * Linke Spalte f�r die Ausdrucke
	 *
	 * @var int
	 */
	protected $_Spalte = self::LINKERRAND;

	/**
	 * Aktuelle Zeile im Bericht
	 * @var int
	 */
	protected $_Zeile = 0;
	/**
	 * Unterer Rand f�r das Dokument
	 * @var int
	 */
	protected $_UntererRand = 20;

	/**
	 * Form-Objekt
	 * @var awisFormular
	 */
	protected $_Form = null;
	/**
	 * Alle Parameter f�r diesen Bericht
	 * @var array
	 */
	protected $_Parameter = array();
	/**
	 * Konstruktor
	 *
	 * Setzt den aktzuellen Benutzer
	 * @var awisBenutzer $AWISBenutzer
	 */
	public function __construct(awisBenutzer $AWISBenutzer, awisDatenbank $DB, $ReportID,$OBClean=true)
	{
		$this->_AWISBenutzer = $AWISBenutzer;
		$this->_DB = $DB;
		
		$this->_Form = new awisFormular();

		$SQL = 'SELECT * FROM Berichte WHERE xre_key = 0'.$ReportID;

		$rsXRE = $this->_DB->RecordSetOeffnen($SQL);
		if($rsXRE->EOF())
		{
			throw new Exception('Kein g�ltiger Bericht.',20080302114);
		}

		// Ist das notwendige Recht vorhanden?
		$Recht = $this->_AWISBenutzer->HatDasRecht($rsXRE->FeldInhalt('XRE_XRC_ID'));
		$RechtBit = ($Recht&intval($rsXRE->FeldInhalt('XRE_STUFE')));
		if($RechtBit != $rsXRE->FeldInhalt('XRE_STUFE'))
		{
			throw new Exception('Keine ausreichenden Rechte fuer '.$this->_AWISBenutzer->BenutzerName().': '.$rsXRE->FeldInhalt('XRE_STUFE').'-'.$RechtBit.'-'.$Recht.'-'.($Recht & $rsXRE->FeldInhalt('XRE_STUFE')).'-'.$rsXRE->FeldInhalt('XRE_STUFE'),201201251017);
		}

		// Standardwerte aus den Tabellen setzen
		$this->_BerichtsName=$rsXRE->FeldInhalt('XRE_BEZEICHNUNG');
		$this->_BerichtsFormat=$rsXRE->FeldInhalt('XRE_FORMAT');
		$this->_BerichtsAusrichtung=$rsXRE->FeldInhalt('XRE_AUSRICHTUNG');
		$this->_BerichtsID=$rsXRE->FeldInhalt('XRE_KEY');
		$this->_BerichtsVorlage='';
		if($rsXRE->FeldInhalt('XRE_VORLAGE')!='')
		{
			$this->_BerichtsVorlage='/daten/web/dokumente/vorlagen/'.$rsXRE->FeldInhalt('XRE_VORLAGE');
			if(file_exists($this->_BerichtsVorlage))
			{
				$this->setSourceFile($this->_BerichtsVorlage);
			}
			else
			{
				$this->_BerichtsVorlage='';
			}
		}
		

		//*************************
		// Parameter einlesen
		//*************************
		$SQL = 'SELECT xrp_parameter, xrp_wert';
		$SQL .= ' FROM berichteparameter ';
		$SQL .= ' WHERE xrp_xre_key = :var_N0_xrp_xre_key';
		$BindeVariablen = array();
		$BindeVariablen['var_N0_xrp_xre_key']=$ReportID;
		
		$rsXRP = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
		while(!$rsXRP->EOF())
		{
			$this->_Parameter[$rsXRP->FeldInhalt('XRP_PARAMETER')]=$rsXRP->FeldInhalt('XRP_WERT');
			$rsXRP->DSWeiter();
		}

		// PDF initialisieren
		parent::FPDI($this->_BerichtsAusrichtung,'mm',$this->_BerichtsFormat);
		$this->SetMargins(0, 0, 0);
		
		if($OBClean)
		{
		  @ob_clean();
		}
	}

	/**
	 * Liefert den aktuellen Berichtsnamen oder setzt einen neuen
	 * @param string [optional] $NeuerName
	 */
	public function BerichtsName($NeuerName=null)
	{
		if(!is_null($NeuerName))
		{
			$this->_BerichtsName = $NeuerName;
		}
		
		return $this->_BerichtsName;
	}

	/**
	 * Legt einen Dateinamen im Dateisystem fest
	 * @param string $DateiName
	 */
	public function DateiName($DateiName)
	{
		// TODO : Namen pr�fen und auch den Pfad, nur erlaubte Pfade definieren
		$this->_Dateiname = $DateiName;
	}

	/**
	 * Schreibt einen Text, der Sonderformatierungen (&lt;b&gt;, &lt;i&gt; bzw. &lt;/b&gt; und &lt;/i&gt;) enthalten darf. Zu beachten ist, dass <b>alle</b> R�nder sauber gesetzt sind. 
	 * Zeilenumbr�che werden mit \n automatisch ausgef�hrt. 
	 * @param string $FormatText			Zu druckender Text
	 * @param int $Zeilenhoehe				Zeilenhoehe
	 * @param int $Schriftgroesse			Schriftgr��e in Pixeln
	 * @param string $Schriftart			Zu verwendende Schriftart.
	 */
	public function SchreibeSonderText($FormatText,$Zeilenhoehe,$Schriftgroesse,$Schriftart)
	{
		$Format['B']=0;
		$Format['I']=0;
		$Format['U']=0;
		$FormatAenderung = false;
		$Puffer = '';
			
		for($Pos=0;$Pos<strlen($FormatText);$Pos++)
		{
			$FormatAenderung = false;
			switch(strtolower(substr($FormatText,$Pos,3)))
			{
				case '<b>':
					$Format['B']=1;
					$Pos+=2;
					$FormatAenderung=true;
					break;
				case '<i>':
					$Format['I']=1;
					$Pos+=2;
					$FormatAenderung=true;
					break;
				case '<u>':
					$Format['U']=1;
					$Pos+=2;
					$FormatAenderung=true;
					break;
			}
			switch(substr($FormatText,$Pos,4))
			{
				case '</b>':
					$Format['B']=0;
					$Pos+=3;
					$FormatAenderung=true;
					break;
				case '</i>':
					$Format['I']=0;
					$Pos+=3;
					$FormatAenderung=true;
					break;
				case '</u>':
					$Format['U']=0;
					$Pos+=3;
					$FormatAenderung=true;
					break;
			}
		
			if($FormatAenderung)
			{
				$this->Write($Zeilenhoehe,$Puffer);
				$Puffer = '';
				$FormatString = ($Format['B']?'B':'').($Format['I']?'I':'').($Format['U']?'U':'');
				$this->SetFont($Schriftart,$FormatString,$Schriftgroesse);
			}
			else
			{
				$Puffer .= $FormatText[$Pos];
			}
		}
	
		$this->Write($Zeilenhoehe,$Puffer);
		$this->SetFont($Schriftart,'',$Schriftgroesse);
	}
	
	/**
	 * Bericht vorbereiten
	 *
	 * @param int $Parameter	Parameter f�r die Datenermittlung
	 */
	abstract public function init(array $Parameter);

	/**
	 * Erzeugt Bericht und liefert den temp. Pfadnamen
	 *
	 * @param $AusgabeArt	Ausgabeart des PDFs (1=Anzeige)
	 */
	abstract public function ErzeugePDF($AusgabeArt=1);

	/**
	 * Vergleichsopeartor pruefen
	 *
	 * @param string $Operator
	 * @return string
	 */
	protected function _VergleichsOperatorPruefen($Operator)
	{
		if(array_search($Operator,array('=','>=','<=','<>','>','<'))===false)
		{
			return(' = ');
		}

		return $Operator;
	}

	/**
	 * Liefert die Spaltenbreite in mm
	 *
	 * @return int
	 */
	protected function SeitenBreite()
	{
		if($this->_BerichtsAusrichtung=='L')
		{
			return self::ZEILENA4;
		}
		else
		{
			return self::SPALTENA4;
		}
	}

	/**
	 * Liefert die Hohe der Seite in mm
	 *
	 * @return int
	 */
	protected function SeitenHoehe()
	{
		if($this->_BerichtsAusrichtung=='L')
		{
			return self::SPALTENA4;
		}
		else
		{
			return self::ZEILENA4;
		}
	}


}