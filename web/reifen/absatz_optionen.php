<?php 
global $awisDBFehler;
global $awisRSZeilen;
$con = awislogon();

/****************************************************************************
*
* Neue Parameter speichern
*
****************************************************************************/
//var_dump($_POST);

if(isset($_POST["cmdLoeschen_x"]) AND $_POST["cmdLoeschen_x"]!='')
{
	if($_POST['txtAktListe']!='::ATU Standard::')
	{
		if(!isset($_POST["cmdLoeschBestaetigung"]) AND !isset($_POST["cmdLoeschAbbruch"]))
		{
			echo "<form name=frmAAF method=post>";

			echo "<input type=hidden name=cmdLoeschen_x value=Ja>";
			echo "<input type=hidden name=txtAktListe value='" . $_POST['txtAktListe'] . "'";

			echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Liste [" . $_POST['txtAktListe'] . "] l�schen m�chten?</span><br><br>";
			echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
			
			echo "</form>";
			awisLogoff($con);
			die();
		}
		
		if(!isset($_POST["cmdLoeschAbbruch"]))
		{
			$SQL = "DELETE FROM AbsatzAuswertungsFelder WHERE AAF_Bezeichnung='" . $_POST['txtAktListe'] . "' AND AAF_XBN_KEY=0" . awisBenutzerID();
	
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("absatz_optionen.php", 2, $awisDBFehler['message']);
			}
		}
	}
}

If(isset($_POST['cmdSpeichern_x']))
{
	$FeldListe = "";
    $foo = array();
			// Sortieren
	if($_POST['txtSortieren']=='Ja')
	{
		foreach($_POST AS $Liste=>$Eintrag)
		{
			if($Liste=='chkAlle')
			{
				$FeldListe = "";
				break;
			}
			if(substr($Liste,0,3)=='chk')
			{
				$foo = array_merge($foo, array($Liste=>$Eintrag))				;
			}
		}
		asort($foo);
		$foo = implode(';',array_keys($foo));
		$foo = explode(';', $foo);
		for($Nr =0;$Nr<count($foo);$Nr++)
		{
			$FeldListe = $FeldListe . ", " . substr($foo[$Nr],3);	
		}
//var_dump($FeldListe);
	}
	else	// Speichern
	{	
		$chkListe = explode(';',awis_NameInArray($_POST,'chk',1));
		
		awis_Debug(1,$FeldListe);
		awis_Debug(1,$chkListe);
		
		foreach($chkListe AS $Liste)
		{
			if($Liste=='chkAlle')
			{
				$FeldListe = ", *";
				break;
			}
			if(substr($Liste,0,3)=='chk')
			{
				$Nr = '9999';
				if(isset($_POST['txt' . substr($Liste,3)]))
				{
					$Nr = sprintf('%04d','0000'.$_POST['txt' . substr($Liste,3)]);
				}
				
				$FeldListe = $FeldListe . ", " .  $Nr . substr($Liste,3);	
			}
		}
		
		// Liste umbauen, wg Sortierung
		$FeldListe = substr($FeldListe,2);
		$foo = explode(', ',$FeldListe);
		sort($foo);
		$FeldListe='';
		foreach ($foo as $Eintrag)
		{
			$FeldListe .= ', ' . substr($Eintrag,4);
		}
		$FeldListe=substr($FeldListe,2) ;//. ',';
	}

	if(substr($FeldListe,0,2)==', ')
	{
		$FeldListe = substr($FeldListe,2);
	}	
	
	awis_Debug(1,$FeldListe);
	
	// Neue Liste anlegen?
	if($_POST['txtNeueListe'] != '')
	{
		$SQL = 'INSERT INTO ABSATZAUSWERTUNGSFELDER(AAF_BEZEICHNUNG, AAF_XBN_KEY, AAF_FELDLISTE, AAF_SORTIERUNG, AAF_USER, AAF_USERDAT)';
		$SQL .= 'VALUES(';
		$SQL .= "'" . $_POST['txtNeueListe'] . "'";
		$SQL .= "," . awisBenutzerID() . "";
		$SQL .= ",'" . $FeldListe . "'";
		$SQL .= ",''";		// F�r sp�ter...
		$SQL .= ",'" . $_SERVER['PHP_AUTH_USER'] . "'";
		$SQL .= ",SYSDATE";
		$SQL .= ')';

		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("absatz_optionen.php", 2, $awisDBFehler['message']);
		}
	}
	else		// Vorhandene Speichern
	{
		$SQL = 'UPDATE ABSATZAUSWERTUNGSFELDER SET';
		$SQL .= " AAF_FELDLISTE = '" . $FeldListe . "'";
		$SQL .= ", AAF_SORTIERUNG = ''";	// F�r Sp�ter
		$SQL .= ",AAF_USER = '" . $_SERVER['PHP_AUTH_USER'] . "'";
		$SQL .= ",AAF_USERDAT=SYSDATE";
		$SQL .= " WHERE AAF_Bezeichnung = '" . $_POST['txtAktListe']  . "'";

		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("absatz_optionen.php", 2, $awisDBFehler['message']);
		}
	}
}


/*******************************************************************************
*
*  Daten anzeigen
*
*******************************************************************************/

echo '<font size=5>Liste konfigurieren</font>';

echo '<Form Name=frmSuchen method=post action=./absatz_main.php?cmdAktion=Optionen>';
echo '<table>';
echo '<tr><td valign=top>Neue Liste:</td><td><input name=txtNeueListe size=20></td><td>&nbsp;</td></tr>';

$aktParameter = '';
$rsAAF = awisOpenRecordset($con, "SELECT * FROM AbsatzAuswertungsFelder WHERE AAF_XBN_KEY=0" . awisBenutzerID() . " ORDER BY AAF_Bezeichnung");
if($awisRSZeilen>0)
{
	echo '<tr><td>Vorhandene Listen:</td><td valign=top>';
	echo '<select name=txtAktListe>';
	echo '<option>::Liste laden::</option>';
	for($Zeile=0;$Zeile<$awisRSZeilen;$Zeile++)
	{
		if($_POST['txtAktListe']!=$rsAAF['AAF_BEZEICHNUNG'][$Zeile])
		{
			echo '<option>';
		}
		else
		{
			echo '<option selected>';
			$aktParameter = ', ' . $rsAAF['AAF_FELDLISTE'][$Zeile] . ',';
		}
		
		echo $rsAAF['AAF_BEZEICHNUNG'][$Zeile] . '</option>';
	}
	echo '</select></td><td>';
	
	echo "<input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Liste laden (Alt+w)' name=cmdSuche value=Aktualisieren>";

	echo '</td></tr>';
}
//echo '<tr><td><input name=cmdAktion value=Speichern type=submit></td></tr></table><hr>';
echo '<hr></tr><tr><td colspan=9>';

//echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$_POST['cmdAktion']&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";


echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./absatz_Main.php?cmdAktion=ArtikelInfos&Seite=" . $_GET['cmdAktion'] . "&Speichern=True'>";

if(isset($_POST['txtAktListe']) AND $_POST['txtAktListe']!='' and !isset($_POST['cmdSortieren_x']))
{
	echo " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=" . $_GET['cmdAktion'] . "&Loeschen=True'>";
	echo " <input type=image accesskey=R alt='Sortierung (Alt+R)' src=/bilder/sortierien.png name=cmdSortieren onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=" . $_GET['cmdAktion'] . "&Sortieren=True'>";
}
echo '</td></tr></table><hr>';

				// Reihenfolge der Felder festlegen
if(isset($_POST['cmdSortieren_x']))
{
	echo "<b>Folgende Reihenfolge ist aktiviert:</b><br>";

	echo '<table width=300 border=1>';
	$Felder = explode(',',substr($aktParameter,1));
	for($Nr=0;$Nr<count($Felder);$Nr++)
	{
		if(trim($Felder[$Nr])!='')
		{
			$Felder[$Nr] = trim($Felder[$Nr]);
			echo "<tr><td>" . $Felder[$Nr] . "</td><td><input type=text name=chk" . $Felder[$Nr] . " value=" . (($Nr+1)*5) . "></td></tr>";
		}
	}
	echo '</table>';
	echo '<input type=hidden name=txtSortieren value=Ja>';
}
else			// Felder ausw�hlen
{
	echo "<b>Folgende Felder stehen zur Verf�gung:</b><br>";
	
	if($aktParameter=='')
	{
		$aktParameter=', *,';
	}
	$rsFelder = awisopenrecordset($con,"SELECT * FROM v_Absatz_Saison WHERE ATUNR=''");
	 
	echo "<table><tr><td>Alle Felder</td><td><input type=checkbox ";
	if($aktParameter==", *,")
	{
	    echo " checked ";
	}
	echo " name=chkAlle Value=on></td></tr>";
	
	$FeldListe = array_keys($rsFelder);
	
	sort($FeldListe);
	
	for($i=0;$i < count(array_keys($FeldListe));$i++)
	{
	    echo "<tr><td>" . $FeldListe[$i] . "</td><td><input type=checkbox ";
	    $pos = strpos($aktParameter, ", " . $FeldListe[$i] . ",");
	    if($pos!==FALSE)
	    {
	        echo " checked ";
	    }
	    echo " name=chk" . $FeldListe[$i] . ">";

	    	// Alte Sortierung, falls vorhanden
	    if($pos!==FALSE)
	    {
			echo '<input type=hidden name=txt' . $FeldListe[$i] . ' value=' . (preg_match_all('/,/',substr($aktParameter,0,$pos),$foo)+1) . ">";
	    }
	    
	    echo "</td></tr>";
	}
	
	echo "</table>";

}

echo "</form>";
awislogoff($con);
?>
