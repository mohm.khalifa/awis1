<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVL','%');
	$TextKonserven[]=array('SVL','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('CAD','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','SVL_%');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19002 = $AWISBenutzer->HatDasRecht(19002);
	if($Recht19002==0)
	{
	    awisEreignis(3,1000,'Zukaufrueckgaben',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ServiceProbleme'));
	
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./serviceleistungen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./serviceleistungen_speichern.php');
		$Param['KEY']='';
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['SVL_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['SVL_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}


	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = ' ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY SVL_DATUM DESC';
			$Param['ORDER']='SVL_DATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************

	$Bedingung = ' AND SVL_STATUS IN (1,2)';		// Offene und gehaltene
	
	if($AWIS_KEY1>0)
	{
		$Bedingung .= ' AND SVL_KEY = '.$AWIS_KEY1;
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$Bedingung .= ' AND SVL_KEY = -1';
	}
	
	$SQL = 'SELECT SVCLEISTUNGEN.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM SVCLEISTUNGEN';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
//	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		$Block = 1;
		// Zum Bl�ttern in den Daten
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		
		if($MaxDS <= $StartZeile)
		{
			$Block = 1;
			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		}
		
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	// Zeilen begrenzen
	$rsSVL = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmServiceArtikel action=./serviceleistungen_Main.php?cmdAktion=Problemfaelle method=POST enctype="multipart/form-data">';

	if($rsSVL->EOF())
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['KeineOffenenProbleme'], 800);
		$Form->ZeileEnde();
		$Form->Formular_Ende();
		
		$Param['BLOCK']=1;		// Wieder auf Block, falls eine falsche Positionierung vorlag
	}
	elseif(($rsSVL->AnzahlDatensaetze()>1 AND !isset($_GET['SVL_KEY']))	OR isset($_GET['Liste']))					// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_DATUM'],100,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_FIL_ID'],70,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_WANR'],100,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_AST_ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_AST_ATUNR'],100,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_BEZEICHNUNG'],430,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_STATUS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_STATUS'],130,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVL_KUNDENNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVL_KUNDENNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_KUNDENNR'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		$SVLStatusArtenTexte = explode('|',$AWISSprachKonserven['Liste']['SVL_STATUS']);
		foreach($SVLStatusArtenTexte AS $ArtText)
		{
			$Text = explode('~',$ArtText);
			$SVL_STATUS[$Text[0]]=$Text[1];
		}
		
		while(!$rsSVL->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle&SVL_KEY=0'.$rsSVL->FeldInhalt('SVL_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('SVL_DATUM',$rsSVL->FeldInhalt('SVL_DATUM'),0,100,false,($DS%2),'',$Link,'D','L');
			$Form->Erstelle_ListenFeld('SVL_FIL_ID',$rsSVL->FeldInhalt('SVL_FIL_ID'),0,70,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('SVL_WANR',$rsSVL->FeldInhalt('SVL_WANR'),0,100,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('SVL_AST_ATUNR',$rsSVL->FeldInhalt('SVL_AST_ATUNR'),0,100,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('SVL_BEZEICHNUNG',$rsSVL->FeldInhalt('SVL_BEZEICHNUNG'),0,430,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('SVL_STATUS',$SVL_STATUS[$rsSVL->FeldInhalt('SVL_STATUS')],0,130,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('SVL_KUNDENNR',$rsSVL->FeldInhalt('SVL_KUNDENNR'),0,100,false,($DS%2),'','','T','L');
			if($rsSVL->FeldInhalt('SVL_BEMERKUNGINTERN')!='')
			{
				$Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsSVL->FeldInhalt('SVL_BEMERKUNGINTERN'));
			}
				
			$Form->ZeileEnde();

			$rsSVL->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Problemfaelle';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsSVL->FeldInhalt('SVL_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ServiceProbleme',serialize($Param));
        $EditRecht = ($Recht19002&6);

		$Form->Erstelle_HiddenFeld('SVL_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Problemfaelle&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVL->FeldInhalt('SVL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVL->FeldInhalt('SVL_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_STATUS'].':',190);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVL_STATUS_EDIT']);		// Nicht alle Optionen zur Auswahl!
		$Form->Erstelle_SelectFeld('SVL_STATUS',$rsSVL->FeldInhalt('SVL_STATUS'),"200:190",$EditRecht,'','','A','','',$ListenDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_BEMERKUNGINTERN'].':',190);
		$Form->Erstelle_TextArea('SVL_BEMERKUNGINTERN',$rsSVL->FeldInhalt('SVL_BEMERKUNGINTERN'),800,99,5,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SUCHBEGRIFF'].':',190);
		$Form->Erstelle_TextFeld('*SUCHNAME','',20,180,$EditRecht,'','background:#50FF50');
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		$Form->Erstelle_SelectFeld('SVL_SVT_KEY',$rsSVL->FeldInhalt('SVL_SVT_KEY'),"320:280",$EditRecht,'***SVT_Daten;txtSVL_SVT_KEY;SUCHNAME=*sucSUCHNAME&Zusatz='.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','',array($OptionBitteWaehlen));
		$Form->ZeileEnde();
		
		$Form->Trennzeile();
		
		$EditRecht = false;
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_DATUM'].':',150);
		$Form->Erstelle_TextFeld('SVL_DATUM',$rsSVL->FeldInhalt('SVL_DATUM'),10,200,$EditRecht,'','','','D');
		$AWISCursorPosition=($EditRecht?'txtSVL_DATUM':$AWISCursorPosition);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_FIL_ID'].':',150);
		$Link = ($rsSVL->FeldInhalt('SVL_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsSVL->FeldInhalt('SVL_FIL_ID'));
		$Form->Erstelle_TextFeld('SVL_FIL_ID',$rsSVL->FeldInhalt('SVL_FIL_ID'),5,200,$EditRecht,'','','','N0');
		$Form->Erstelle_TextFeld('*FIL_BEZ',$rsSVL->FeldInhalt('FIL_BEZ'),0,200,false,'','',$Link);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_WANR'].':',150);
		$Form->Erstelle_TextFeld('SVL_WANR',$rsSVL->FeldInhalt('SVL_WANR'),15,200,$EditRecht,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_AST_ATUNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_AST_ATUNR',$rsSVL->FeldInhalt('SVL_AST_ATUNR'),15,250,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_BEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('SVL_BEZEICHNUNG',$rsSVL->FeldInhalt('SVL_BEZEICHNUNG'),80,450,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_MENGE'].':',150);
		$Form->Erstelle_TextFeld('SVL_MENGE',$rsSVL->FeldInhalt('SVL_MENGE'),15,200,$EditRecht,'','','','N0');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_VKPREIS'].':',150);
		$Form->Erstelle_TextFeld('SVL_VKPREIS',$rsSVL->FeldInhalt('SVL_VKPREIS'),6,250,$EditRecht,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_UMSATZ'].':',150);
		$Form->Erstelle_TextFeld('SVL_UMSATZ',$rsSVL->FeldInhalt('SVL_UMSATZ'),6,200,$EditRecht,'','','','N2');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KENNZEICHEN'].':',150);
		$Form->Erstelle_TextFeld('SVL_KENNZEICHEN',$rsSVL->FeldInhalt('SVL_KENNZEICHEN'),15,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KTYPNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_KTYPNR',$rsSVL->FeldInhalt('SVL_KTYPNR'),15,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KUNDENNAME1'].':',150);
		$Form->Erstelle_TextFeld('SVL_KUNDENNAME1',$rsSVL->FeldInhalt('SVL_KUNDENNAME1'),40,250,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KUNDENNAME2'].':',150);
		$Form->Erstelle_TextFeld('SVL_KUNDENNAME2',$rsSVL->FeldInhalt('SVL_KUNDENNAME2'),40,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_PERSNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_PERSNR',$rsSVL->FeldInhalt('SVL_PERSNR'),40,250,$EditRecht,'','','','T');
		$Form->Erstelle_TextFeld('*PRS_NAME',$rsSVL->FeldInhalt('PRS_NAME'),40,200,false,'','','','T');
		$Form->ZeileEnde();
		
		$Form->Trennzeile();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KENNUNG'].':',150);
		$Form->Erstelle_TextFeld('SVL_KENNUNG',$rsSVL->FeldInhalt('SVL_KENNUNG'),35,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_IMPORTZEITPUNKT'].':',150);
		$Form->Erstelle_TextFeld('SVL_IMPORTZEITPUNKT',$rsSVL->FeldInhalt('SVL_IMPORTZEITPUNKT'),20,250,$EditRecht,'','','','DU');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_MELDEZEITPUNKT'].':',150);
		$Form->Erstelle_TextFeld('SVL_MELDEZEITPUNKT',$rsSVL->FeldInhalt('SVL_MELDEZEITPUNKT'),20,200,$EditRecht,'','','','DU');
		$Form->ZeileEnde();
				
		$Form->Formular_Ende();
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht19002&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
	$AWISBenutzer->ParameterSchreiben('Formular_ServiceProbleme',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>