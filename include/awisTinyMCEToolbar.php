<?php

/**
 * Klasse zum erezugen einer TinyMCE-Toolbar (Schaltfl�chen wie FETT, Kursiv usw)
 */
class awisTinyMCEToolbar
{

    /**
     * Elemente f�r die Toolbar
     *
     * @var array
     */
    private $_Toolbar = array();

    /**
     * awisTinyMCEToolbar constructor.
     */
    function __construct()
    {
        $this->_Toolbar['plugins'] = array();
        $this->_Toolbar['buttons'] = array();
        $this->_Toolbar['additionals'] = array();
    }


    /**
     * Farben f�r die Colorpicker
     */
    const Farben = ' textcolor_map: [
                                    "000000", "Black",
                                    "C31525", "ATU",
                                    "993300", "Burnt orange",
                                    "333300", "Dark olive",
                                    "003300", "Dark green",
                                    "003366", "Dark azure",
                                    "000080", "Navy Blue",
                                    "333399", "Indigo",
                                    "333333", "Very dark gray",
                                    "800000", "Maroon",
                                    "FF6600", "Orange",
                                    "808000", "Olive",
                                    "008000", "Green",
                                    "008080", "Teal",
                                    "0000FF", "Blue",
                                    "666699", "Grayish blue",
                                    "808080", "Gray",
                                    "FF0000", "Red",
                                    "FF9900", "Amber",
                                    "99CC00", "Yellow green",
                                    "339966", "Sea green",
                                    "33CCCC", "Turquoise",
                                    "3366FF", "Royal blue",
                                    "800080", "Purple",
                                    "999999", "Medium gray",
                                    "FF00FF", "Magenta",
                                    "FFCC00", "Gold",
                                    "FFFF00", "Yellow",
                                    "00FF00", "Lime",
                                    "00FFFF", "Aqua",
                                    "00CCFF", "Sky blue",
                                    "FFFFFF", "White",
                                    "FF99CC", "Pink",
                                    "FFCC99", "Peach",
                                    "FFFF99", "Light yellow",
                                    "CCFFCC", "Pale green",
                                    "CCFFFF", "Pale cyan",
                                    "99CCFF", "Light sky blue",
                                    "CC99FF", "Plum"
                                  ]';

    /**
     * Schaltfl�che zum �ndern der Textfarbe
     */
    public function MitTextfarbAuswahl()
    {
        $this->_Toolbar['plugins'][] = 'textcolor';
        $this->_Toolbar['buttons'][] = 'forecolor';
        $this->_Toolbar['additionals'][] = self::Farben;
    }

    /**
     * Schaltfl�che zum �ndern der Hintergrundfarbe
     */
    public function MitHintergrundFarbauswahl()
    {
        $this->_Toolbar['plugins'][] = 'textcolor';
        $this->_Toolbar['buttons'][] = 'forecolor';
        $this->_Toolbar['additionals'][] = self::Farben;
    }

    /**
     * Laedt Link Plugin
     */
    public function MitLinkPlugin()
    {
        $this->_Toolbar['plugins'][] = 'link';
    }

    /**
     * Laedt Code Plugin
     */
    public function MitCodePlugin()
    {
        $this->_Toolbar['plugins'][] = 'code';
    }
    /**
     * Laedt Code Plugin
     */
    public function MitQuickbarsPlugin()
    {
        $this->_Toolbar['plugins'][] = 'quickbars';
    }

    /**
     * F�gt einen Strich zwischen die Schaltfl�chen (Zum gruppieren)
     */
    public function Spacer()
    {
        $this->_Toolbar['buttons'][] = ' | ';
    }

    /**
     * F�gt gewisse Standardbuttons hinzu
     */
    public function MitStandardFunktionen()
    {
        $this->_Toolbar['buttons'][] = 'undo redo | styleselect | bold italic | link image';
    }

    /**
     * Liefert das Toolbararray
     *
     * @return array Mehrdimensionales array: array('plugins'=>array(), 'buttons'=>array(), 'additionals'=>array)
     */
    public function getToolbar()
    {
        return $this->_Toolbar;
    }
}