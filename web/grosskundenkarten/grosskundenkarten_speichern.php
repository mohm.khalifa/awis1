<?php
global $Funktionen;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {

    if (isset($_GET['KARTE']) and $_GET['KARTE'] != '') {
        $SQL = 'UPDATE '.$Funktionen->holeKartenTabelle().' ';
        $SQL .= 'SET GEDRUCKT = 1 ';
        $SQL .= ', DRUCK_DATUM = null ';
        $SQL .= 'WHERE KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('KARTE', 'Z', $_GET['KUNDEN_NR']);
        $SQL .= ' AND KARTEN_NR = ' . $Funktionen->DB->WertSetzen('KARTE', 'Z', $_GET['KARTE']);

        $Funktionen->DB->Ausfuehren($SQL, '', true, $Funktionen->DB->Bindevariablen('KARTE'));
    }

    if ($_GET['cmdAktion'] == 'Modelle') {
        $datenLeer = true;
        for ($i = 1; $i <= 6; $i++) {
            if ((isset($_POST['txtERD_KEY_' . $i]) and $_POST['txtERD_KEY_' . $i] != $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen']) and (isset($_POST['txtERM_RABATTSATZ_' . $i]) and $_POST['txtERM_RABATTSATZ_' . $i] != $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen'])) {
                $datenLeer = false;
                break;
            }
        }

        if (!$datenLeer) {
            if (isset($_POST['txtERM_NR']) and $_POST['txtERM_NR'] < 0) {
                $SQL = 'SELECT MAX(ERM_NR) + 1 AS ID FROM EXPRABATTMODELLE';
                $ID = $Funktionen->DB->RecordSetOeffnen($SQL)->FeldInhalt('ID');
                if($ID == '') {
                    $ID = 1;
                }
            }

            if (isset($_POST['txtERM_NR']) and $_POST['txtERM_NR'] > 0) {
                $SQL = 'DELETE FROM EXPRABATTMODELLE WHERE ERM_NR = ' . $Funktionen->DB->WertSetzen('ERM', 'T', $_POST['txtERM_NR']);
                $Funktionen->DB->Ausfuehren($SQL, 'ERM', true, $Funktionen->DB->Bindevariablen('ERM', true));
                $ID = $_POST['txtERM_NR'];
            }

            for ($i = 1; $i <= 6; $i++) {
                if ((isset($_POST['txtERD_KEY_' . $i]) and $_POST['txtERD_KEY_' . $i] != $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen']) and (isset($_POST['txtERM_RABATTSATZ_' . $i]) and $_POST['txtERM_RABATTSATZ_' . $i] != $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen'])) {
                    $SQL = "INSERT INTO EXPRABATTMODELLE (ERM_NR, ERM_MODELLNAME , ERM_ERD_KEY, ERM_RABATTSATZ) VALUES (";
                    $SQL .= $Funktionen->DB->WertSetzen('ERM', 'T', $ID) . ", ";
                    $SQL .= $Funktionen->DB->WertSetzen('ERM', 'T', $_POST['txtERM_MODELLNAME']). ", ";
                    $SQL .= $Funktionen->DB->WertSetzen('ERM', 'T', $_POST['txtERD_KEY_' . $i]) . ", ";
                    $SQL .= $Funktionen->DB->WertSetzen('ERM', 'T', $_POST['txtERM_RABATTSATZ_' . $i]) . ")";

                    $Funktionen->DB->Ausfuehren($SQL, 'ERM', true, $Funktionen->DB->Bindevariablen('ERM', true));
                }
            }
        } else {
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachkonserven['ERM']['ERM_FEHLER_FELDER'], awisFormular::HINWEISTEXT_FEHLER);
        }
    } elseif ($_GET['cmdAktion'] == 'Zuordnung') {
        if ((isset($_POST['sucERM_NR']) and $_POST['sucERM_NR'] == $Funktionen->AWISSprachkonserven['Wort']['txt_BitteWaehlen']) or isset($_POST['cmdLoeschen_x'])) {
            $SQL = 'DELETE FROM EXPRABATTZUORDNUNGEN WHERE ERZ_KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucKUNDEN_NR']);
            $Funktionen->DB->Ausfuehren($SQL, '', true, $Funktionen->DB->Bindevariablen('ERZ', true));
        } elseif (isset($_POST['sucERM_NR']) and isset($_POST['sucKUNDEN_NR'])) {
            $SQL = 'SELECT ERZ_ERM_NR FROM EXPRABATTZUORDNUNGEN WHERE ERZ_KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucKUNDEN_NR']);
            $rsKunde = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('ERZ', true));

            if ($rsKunde->AnzahlDatensaetze() > 0) {
                $SQL = 'UPDATE EXPRABATTZUORDNUNGEN SET ERZ_ERM_NR = ' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucERM_NR']) . ' WHERE ERZ_KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucKUNDEN_NR']);
                $Funktionen->DB->Ausfuehren($SQL, '', true, $Funktionen->DB->Bindevariablen('ERZ', true));
            } else {
                $SQL = 'INSERT INTO EXPRABATTZUORDNUNGEN (ERZ_KUNDEN_NR, ERZ_ERM_NR) 
                        VALUES (' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucKUNDEN_NR']) . ', ' . $Funktionen->DB->WertSetzen('ERZ', 'TN', $_POST['sucERM_NR']) . ')';
                $Funktionen->DB->Ausfuehren($SQL, '', true, $Funktionen->DB->Bindevariablen('ERZ', true));
            }
        }
    }
} catch (awisException $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {

    if ($Funktionen->Form instanceof awisFormular) {

        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}