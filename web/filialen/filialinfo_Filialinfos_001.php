<?php
/**
* Filialinformations - Hauptseite
*
* @author Sacha Kerres
* @version 20080212
* @copyright ATU
* @todo Umbauen auf neues Framework
*
* */
require_once('awisFilialen.inc');
global $AWIS_KEY1;
global $con;
global $awisRSZeilen;
global $SpeichernButton;		// Anzeige eines Speichern-Knopfs
global $CursorFeld;
global $AWISBenutzer;			// Benutzer-Objekt
global $AWISSprache;			// Aktuelle Sprache
global $AWISSprachKonserven;
global $rsFiliale;

$TextKonserven[]=array('Wort','Wochentag_%');
$TextKonserven[]=array('Wort','lbl_%');
$TextKonserven[]=array('Wort','Anlieferungstag');
$TextKonserven[]=array('Wort','Achtung');
$TextKonserven[]=array('Wort','NeueAdresseAb');
$TextKonserven[]=array('Wort','OeffnungsZeiten');
$TextKonserven[]=array('Wort','MoBisFr');
$TextKonserven[]=array('Wort','Sa');
$TextKonserven[]=array('Wort','EroeffnungAm');
$TextKonserven[]=array('Wort','EroeffnetAm');
$TextKonserven[]=array('Wort','keinTermin');
$TextKonserven[]=array('Wort','Stundensatz');
$TextKonserven[]=array('Wort','UserDat');
$TextKonserven[]=array('Wort','Geschaeftsleiter%');
$TextKonserven[]=array('Wort','Werkstattleiter%');
$TextKonserven[]=array('Wort','keineAngabe');
$TextKonserven[]=array('Wort','lautPersonaleinsatzplan');
$TextKonserven[]=array('Wort','Werkstattleiter%');
$TextKonserven[]=array('Wort','TKDL');
$TextKonserven[]=array('Wort','UmzugAm');
$TextKonserven[]=array('Wort','UmgezogenAm');
$TextKonserven[]=array('Wort','unbekannt');
$TextKonserven[]=array('Wort','nichtzugeordnet');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('FIL','%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(awisBenutzerRecht($con,100)==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("Keine ausreichenden Rechte!");
}

$LinkRecht = awisBenutzerRecht($con,112);
$PersRecht = awisBenutzerRecht($con, 1300);			// Personaleinsatzplanung

$DatenBearbeitung = awisBenutzerRecht($con,118);	// Filialdaten-Bearbeitung
$RechteStufeTUEV = awisBenutzerRecht($con,111);	// TUEV-Infos
$RechteSchluessel = awisBenutzerRecht($con,1503);	// Schluessel-Vergaben
$RechteStufeFilialInfos = awisBenutzerRecht($con,119);	// Filialinfos-Anzeigen

if(!isset($rsFiliale["FIL_ID"][0]) OR $rsFiliale["FIL_ID"][0]=="")
{
	if (isset($_REQUEST['FIL_ID']) && $_REQUEST['FIL_ID']!='')
	{
		awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=0". $_REQUEST['FIL_ID']);
	}
	die("<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineDaten']."</span>");
}
$AWIS_KEY1 = $rsFiliale["FIL_ID"][0];
	// Waehrungskennzeichen f�r diese Filiale
$recLAN = awisOpenRecordset($con, "SELECT LAN_WAE_CODE FROM Laender WHERE LAN_WWSKENN='" . $rsFiliale['FIL_LAN_WWSKENN'][0] . "'");

$FILWaehrung = (!isset($recLAN['LAN_WAE_CODE'][0])?'???':$recLAN['LAN_WAE_CODE'][0]);
unset($recLAN);

/************************************************
*
* Daten anzeigen
*
* **********************************************/

// Aktuelle Filiale speichern
awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=0". $rsFiliale["FIL_ID"][0]);

 // Daten f?r die Infoseite sammeln
 $SQL = "SELECT filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
	       filialinfostypen.fit_information, filialinfostypen.fit_sortierung,
   			filialinfos.fif_wert, fit_id, FIF_KEY, FIF_IMQ_ID, FIF_FIL_ID, FIF_USER,  FIF_USERDAT
			FROM filialinfos, filialinfostypen, filialinfostypengruppen, importquellen
			 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
			 	AND importquellen.IMQ_ID = filialinfos.FIF_IMQ_ID
    			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
				AND FIF_FIL_ID=0" . $rsFiliale["FIL_ID"][0] . ")
			ORDER BY IMQ_PRIORITAET";

$rsFilialInfo = awisOpenRecordset($con, $SQL);

// ?berschrift schreiben
echo "<input type=hidden name=FILID value=" . $rsFiliale["FIL_ID"][0] . ">";	// F?r Submit
echo "<table width=100% border=0><tr><td align=left>";
echo "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
if(($RechteSchluessel&1)==1)
{
	echo "</td><td backcolor=#000000 align=right><img src=/bilder/sc_svergabe.png title='".$AWISSprachKonserven['Wort']['lbl_Schluessel']."' onclick=location.href='../schluessel/schluessel_Main.php?cmdAktion=Vergaben&txtFIL_ID=".$rsFiliale["FIL_ID"][0]."&Anzeigen=true';>&nbsp;<img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos&Liste=True';></td></tr></table>";
}
else
{
	echo "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."' onclick=location.href='./filialinfo_Main.php?cmdAktion=Filialinfos&Liste=True';></td></tr></table>";
}
echo "<hr>";

echo "<table width=100% id=DatenTabelle><tr>";		// Gro?er Rahmen mit drei Spalten

echo "<td valign=top width=24%><table border=0>";
echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_BEZ"][0] . "</span></td></tr>";
echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . $rsFiliale["FIL_STRASSE"][0] . "</span></td></tr>";
echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett>" . ($rsFiliale["LAN_CODE"][0]==''?'':$rsFiliale["LAN_CODE"][0].'-') . $rsFiliale["FIL_PLZ"][0] . " " . $rsFiliale["FIL_ORT"][0] . " " . $rsFiliale["FIL_ORTSTEIL"][0] . "</span></td></tr>";

	// Bundesland anzeigen
$Erg = SucheFilialInfo($rsFilialInfo, 65, "FIT_ID");
if($Erg>=0)
{
	$rsBl = awisOpenRecordset($con, "SELECT BUL_BUNDESLAND, BUL_ID FROM Bundeslaender WHERE BUL_ID=0" . number_format($rsFilialInfo["FIF_WERT"][$Erg],0) . "");
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormalFett> " . $rsBl["BUL_BUNDESLAND"][0] . "</span></td></tr>";
	unset($rsBl);
}

// Anzeigen, ob die Filiale eine Standardfiliale ist. TR 03.05.2007
if($rsFiliale["FIL_FILTYP"][0] == 'S')
{
	echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>".$AWISSprachKonserven['FIL']['Standardfiliale']."</span></td></tr>";
}
elseif($rsFiliale["FIL_FILTYP"][0] == 'L')
{
	echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>".$AWISSprachKonserven['FIL']['Lightfiliale']."</span></td></tr>";
}

// Anzeigen, ob die Filiale eine Kompetenzfiliale ist. TR 30.06.2008
if($rsFiliale["FIL_AUTOGAS"][0] == 'J')
{
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['Kompetenzfiliale']."</span></td></tr>";
}

// Anzeigen, ob die Filiale auf Axapta umgestellt ist. TR 01.07.2009
$Erg = SucheFilialInfo($rsFilialInfo, 910, "FIT_ID");		// Axapta-Filiale
if ($rsFilialInfo["FIF_WERT"][$Erg]=='J')
{	
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['Axaptafiliale']."</span></td></tr>";
}		

	// Neue Adresse pr�fen!

$Erg = SucheFilialInfo($rsFilialInfo, 104, "FIT_ID");
If($Erg <> -1 AND awis_format($rsFilialInfo["FIF_WERT"][$Erg],'SortDatum') > date("Ymt"))
{
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr><td><span Class=HinweisText>".$AWISSprachKonserven['Wort']['Achtung']."</span></td></tr>";
	echo "<tr><td><span Class=HinweisText>".$AWISSprachKonserven['Wort']['NeueAdresseAb']."" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
	echo "<tr><td>&nbsp;</td></tr>";

	$Erg = SucheFilialInfo($rsFilialInfo, 101, "FIT_ID");		// Neue Strasse
		echo "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
	$Erg = SucheFilialInfo($rsFilialInfo, 102, "FIT_ID");		// Neue Plz
		echo "<tr><td><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . " ";
	$Erg = SucheFilialInfo($rsFilialInfo, 103, "FIT_ID");		// Neuer Ort
		echo "" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";

	echo "<tr><td>&nbsp;</td></tr>";

}

echo "</table></td>";
	// Ende Filialadresse


	// Zweite Spalte
	echo "<td valign=top width=33%><table border=0>";


echo "<tr><td><table border=0>";
$Erg = SucheFilialInfo($rsFilialInfo, 3, "FIT_ID");		// Kundentelefon
if($Erg >= 0)
{
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
}

$Erg = SucheFilialInfo($rsFilialInfo, 2, "FIT_ID");		// Faxnummer
if($Erg >= 0)
{
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>" . $rsFilialInfo["FIT_INFORMATION"][$Erg] . " : </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
}
echo "</table>";

	// ?ffnungszeiten
echo "<tr><td>&nbsp;</td></tr><tr><td id=DatenFeld><span class=DatenFeldNormalFett>".$AWISSprachKonserven['Wort']['OeffnungsZeiten']."</span></td></tr>";

$Erg = SucheFilialInfo($rsFilialInfo, 121, "FIT_ID");		// ?ffnung Mo-Fr
	echo "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>".$AWISSprachKonserven['Wort']['MoBisFr'].": </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
$Erg = SucheFilialInfo($rsFilialInfo, 122, "FIT_ID");
	echo "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";

$Erg = SucheFilialInfo($rsFilialInfo, 123, "FIT_ID");		// �ffnung Sa
	echo "<tr><td id=DatenFeld><table border=0><tr><td width=70><span class=DatenFeldNormal>".$AWISSprachKonserven['Wort']['Sa'].": </span></td><td><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
$Erg = SucheFilialInfo($rsFilialInfo, 124, "FIT_ID");
	echo "-" . $rsFilialInfo["FIF_WERT"][$Erg]	. "</span></td></tr></table></td></tr>";

echo "<tr><td>&nbsp;</td></tr>";		// Leerzeile

$Erg = SucheFilialInfo($rsFilialInfo, 34, "FIT_ID", 32);		// Er?ffnungsdatum aus ACCESS
$AktDatum = awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert');

if(mktime(0,0,0,substr($rsFilialInfo["FIF_WERT"][$Erg],3,2),substr($rsFilialInfo["FIF_WERT"][$Erg],0,2),substr($rsFilialInfo["FIF_WERT"][$Erg],6,4))>time())
{
 	echo "<tr><td id=DatenFeld><span class=HinweisText>".$AWISSprachKonserven['Wort']['EroeffnungAm'].": </span><span class=HinweisText>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
}
else
{
 	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['Wort']['EroeffnetAm'].": </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
}

$Erg = SucheFilialInfo($rsFilialInfo, 104, "FIT_ID");		// Umzugsdatum
if($Erg>=0)
{
	$AktDatum = ($AktDatum<awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert')?awis_format($rsFilialInfo["FIF_WERT"][$Erg],'DatumsWert'):$AktDatum);
	$Datum = mktime(0,0,0,substr($rsFilialInfo["FIF_WERT"][$Erg],3,2),substr($rsFilialInfo["FIF_WERT"][$Erg],0,2), substr($rsFilialInfo["FIF_WERT"][$Erg],6));

	if($Datum>time())
	{
 		echo "<tr><td id=DatenFeld><span class=HinweisText>".$AWISSprachKonserven['Wort']['UmzugAm'].": </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
	}
	else
	{
 		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['Wort']['UmgezogenAm'].": </span><span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . '</span>';
	}
}

$Erg = SucheFilialInfo($rsFilialInfo, 36, "FIT_ID");		// Ladenkonzept 2011

if(($DatenBearbeitung & 128)==128)		// Ladenkonzept 2011
{
	$Ausgabe = isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'';
	echo '<tr><td id=DatenFeld>';
	echo "<input type=hidden name=txtLadenkonzept_old value=" . $Ausgabe . '>';
	echo '<span class=DatenFeldNormal>'.$AWISSprachKonserven['FIL']['LadenKonzept'].': </span>';

	$Ausgabe = (isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'');
	If($Ausgabe=='' AND $AktDatum > strtotime('01/01/2006'))
	{
		$Ausgabe = date('d.m.Y',$AktDatum);
		echo "<span class=DatenFeldNormalFett>" . $Ausgabe . "</span>";
	}
	else
	{
		echo "<span class=DatenFeldNormalFett><input accesskey=z type=text title='Letzte �nderung von: " . (isset($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  "/" . (isset($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=10 name=txtLadenkonzept value='" . $Ausgabe . "'></span>";
		echo "<input align=right type=hidden name=txtFIF_KEY_Ladenkonzept value='" . (isset($rsFilialInfo["FIF_KEY"][$Erg])?$rsFilialInfo["FIF_KEY"][$Erg]:'') . "'>";
		$SpeichernButton = True;
	}
	echo "</td></tr>";
}
else
{
	$Ausgabe = isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'';
	If($Ausgabe=='' AND $AktDatum > strtotime('08/01/2005'))
	{
		$Ausgabe = date('d.m.Y',$AktDatum);
	}
	elseIf($Ausgabe=='')		// Keine Infos
	{
		$Ausgabe = $AWISSprachKonserven['Wort']['keinTermin'];
	}
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Ladenkonzept 2011: </span><span class=DatenFeldNormalFett>" . $Ausgabe . "</span></td></tr>";
}

		//********************************
		// Stundensatz der Filialen und
		// Spezialaktion Radwechsel ohne Wuchten (seit 19.09.05, SP)
		//********************************
$RechteStufe = awisBenutzerRecht($con, 110);
if($RechteStufe>0)
{
	echo "<tr><td>&nbsp;</td></tr>";		// Leerzeile
	$Erg = SucheFilialInfo($rsFilialInfo, 200, "FIT_ID");		// Stundensatz
	if(($RechteStufe & 2)==2)
	{
		$Ausgabe = number_format(str_replace(",",".",(isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:0)),2,",","");
		echo '<tr><td id=DatenFeld>';
		echo '<input type=hidden name=txtStundenSatz_old value=' . $Ausgabe . '>';
		echo '<span class=DatenFeldNormal>'.$AWISSprachKonserven['Wort']['Stundensatz'].': </span>';
		echo "<span class=DatenFeldNormalFett><input accesskey=z  title='".$AWISSprachKonserven['Wort']['UserDat'].": " . (isset($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  "/" . (isset($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' type=text size='" . strlen($Ausgabe) . "' name=txtStundenSatz value='" . $Ausgabe . "'></span>" . $FILWaehrung;
		echo "<input align=right type=hidden name=txtFIF_KEY_StundenSatz value='" .  (isset($rsFilialInfo["FIF_KEY"][$Erg])?$rsFilialInfo["FIF_KEY"][$Erg]:'') . "'>";
		$SpeichernButton = True;
		echo "</td></tr>";
	}
	else
	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['Wort']['Stundensatz']." : </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",(isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'')),2,",","") . $FILWaehrung . "</span></td></tr>";
	}
	if (substr($rsFiliale["FIL_PLZ"][0], 0, 1) == 'A')
	{
		$DPRNummer = 'OEIN03';
	}
	else
	{
		$DPRNummer = '1EIN03';
	}
	$DPFPreisSQL = "Select DPF_VK from dienstleistungspreisefilialen where dpf_dpr_nummer = '" . $DPRNummer . "' and dpf_fil_id = " . $rsFiliale["FIL_ID"][0];
	$rsDPFPreisInfo = awisOpenRecordset($con, $DPFPreisSQL);
	//if ((isset($rsDPFPreisInfo["DPF_VK"][0]) AND $rsDPFPreisInfo["DPF_VK"][0] != 0) AND count($rsDPFPreisInfo) > 0)
	//Wenn VK=0 auch anzeigen TR 05.05.08
	if (isset($rsDPFPreisInfo["DPF_VK"][0]) AND count($rsDPFPreisInfo) > 0)
	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['PreisRadWechsel'].": </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",$rsDPFPreisInfo["DPF_VK"][0]),2,",","") . $FILWaehrung . "</span></td></tr>";
	}
	else
	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['PreisRadWechsel'].": </span><span class=DatenFeldNormalFett>". $AWISSprachKonserven['Wort']['unbekannt'] . "</span></td></tr>";
	}
}
		//********************************
		// Aktion AU/HU in den Filialen
		//********************************
$Erg = SucheFilialInfo($rsFilialInfo, 203, "FIT_ID");		// Aktion AU/HU
if(($DatenBearbeitung & 64)==64)
{
	echo '<td id=DatenFeld><span class=DatenFeldNormal>'.$AWISSprachKonserven['FIL']['PreisAktionAUHU'].': </span>';
	echo '<input type=hidden name=txtAUHU_old value=\'' . $Ausgabe . '\'>';
	echo "<input size=20 title='".$AWISSprachKonserven['Wort']['UserDat'].": " . (isset($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  "/" . (isset($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' name=txtAUHU value='" . (isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";
	$SpeichernButton = True;
}
else	// Keine Bearbeitung
{
	if(isset($rsFilialInfo["FIF_WERT"][$Erg]) AND (substr_count($rsFilialInfo["FIF_WERT"][$Erg], 'EURO') > 0))		// EURO in ? umbauen!
	{
		$Wert = str_replace('EURO','',$rsFilialInfo['FIF_WERT'][$Erg]);
		if(is_numeric($Wert))
		{
			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['PreisAktionAUHU'].": </span><span class=DatenFeldNormalFett>" . number_format(str_replace(",",".",$rsFilialInfo["FIF_WERT"][$Erg]),2,",","") . " ". $FILWaehrung . "</span></td></tr>";
		}
		else
		{
			$Wert = str_replace('EURO','&euro;',$rsFilialInfo['FIF_WERT'][$Erg]);
			echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['PreisAktionAUHU'].": </span><span class=DatenFeldNormalFett>" . $Wert . "</span></td></tr>";
		}
	}
	else
	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>".$AWISSprachKonserven['FIL']['PreisAktionAUHU'].": </span><span class=DatenFeldNormalFett>" . (isset($rsFilialInfo["FIF_WERT"][$Erg]) && $rsFilialInfo["FIF_WERT"][$Erg]!=''?$rsFilialInfo["FIF_WERT"][$Erg]:$AWISSprachKonserven['Wort']['unbekannt']) . " </span></td></tr>";
	}
}

echo "</table></td>";	// Ende zweite Spalte

 	// Dritte Spalte
echo "<td valign=top width=43%><table border=0>";

	// 1. Gesch�ftsleiter-ID
SchreibePersonenFeld($AWISSprachKonserven['Wort']['Geschaeftsleiter1'],200,74,$rsFilialInfo,$con,($LinkRecht>0));

	// 2. Gesch�ftsleiter-ID
SchreibePersonenFeld($AWISSprachKonserven['Wort']['Geschaeftsleiter2'],200,75,$rsFilialInfo,$con,($LinkRecht>0));

	// 1. Werkstattleiter
SchreibePersonenFeld($AWISSprachKonserven['Wort']['Werkstattleiter1'],200,76,$rsFilialInfo,$con,($LinkRecht>0));

	// Werkstattleiter2
SchreibePersonenFeld($AWISSprachKonserven['Wort']['Werkstattleiter2'],200,77,$rsFilialInfo,$con,($LinkRecht>0));


$Erg = SucheFilialInfo($rsFilialInfo, 26, "FIT_ID");		// Regionalzentrum
$RegZentrum = $rsFilialInfo["FIF_WERT"][$Erg];
$Erg = SucheFilialInfo($rsFilialInfo, 21, "FIT_ID");		// Gebiet
$Gebiet = (isset($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'');

//**********************************
// Int. Vertrieb
//**********************************

$Recht122 = awisBenutzerRecht($con, 122);
if($Recht122==0)
{
	if($rsFiliale['LAN_CODE'][0]!='DE')
	{
		$rsMIT = awisOpenRecordset($con, "SELECT MIT_PER_NR,MTA_BEZEICHNUNG,MIT_BEZEICHNUNG FROM Mitarbeiter INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID WHERE MIT_STATUS='A' AND MIT_MTA_ID IN (130)");
		$rsMITZeilen = $awisRSZeilen;
		for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
		{
		 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
			echo $rsMIT['MTA_BEZEICHNUNG'][$MITZeile] . ':</span></td><td><span class=DatenFeldNormalFett>';
			echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
			echo '</span></td></tr></table></td></tr>';
		}
	}
}

/**********************************
* VKL
**********************************/

if($Recht122==0)
{
	$rsMIT = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID WHERE MIT_STATUS='A' AND MIT_REZ_ID=0" . $RegZentrum . " AND MIT_MTA_ID IN (1,110)");
	$rsMITZeilen = $awisRSZeilen;
	for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
	{
		if($PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
		{
			$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
		}
	 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
		echo $Ausgabe . $rsMIT['MTA_BEZEICHNUNG'][$MITZeile] . ':'.($Ausgabe==''?'':'</a>') .' </span></td><td><span class=DatenFeldNormalFett>';
		echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
		echo '</span></td></tr></table></td></tr>';
	}
	unset($rsMIT);
}

/**********************************
* TKDL
**********************************/
if($Recht122==0)
{
	$Erg = SucheFilialInfo($rsFilialInfo, 28, "FIT_ID");		// manueller TKDL

	if($Erg > 0 AND $rsFilialInfo["FIF_WERT"][$Erg]!='')
	{
	 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
		echo "<input type=hidden name=txtTKDL_old value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'>";
		echo $AWISSprachKonserven['Wort']['TKDL'].':</a> </span></td><td><span class=DatenFeldNormalFett>';
		if(($DatenBearbeitung & 16)==16)
		{
			echo "<td><input size=20 name=txtTKDL value='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'></td>";
		}
		else	// Keine Bearbeitung
		{
			echo $rsFilialInfo["FIF_WERT"][$Erg];
		}
		echo '</span></td></tr></table></td></tr>';
	}
	else
	{
		$rsMIT = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter INNER JOIN VerkaufsGebiete ON MIT_KEY = VKG_TKDL_MIT_KEY WHERE MIT_REZ_ID=0" . $RegZentrum . " AND VKG_ID='". $Gebiet . "'");
		$rsMITZeilen = $awisRSZeilen;

		for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
		{
			if($rsMIT['VKG_ID'][0]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
			{
				$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
				//$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsMIT['MIT_PER_NR'][0]) . ">";
			}

		 	echo '<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>';
			echo $Ausgabe . $AWISSprachKonserven['Wort']['TKDL'].':</a> </span></td><td><span class=DatenFeldNormalFett>';


			if(($DatenBearbeitung & 16)==16)		// Bearbeitung m�glich
			{
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
				echo "<input type=hidden name=txtTKDL_old value='" . ($Erg!=-1?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
				echo "&nbsp;(<input size=20 name=txtTKDL value='" . ($Erg!=-1?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>)";
			}
			else		// Nur Anzeige
			{

				if ($rsFilialInfo["FIF_WERT"][$Erg]!='')	// Ist eine Alternative vorhanden? -> Ja -> Anzeigen
				{
					echo "" . ($Erg!=-1?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "";
				}
				else		// Nein -> Standard anzeigen
				{
					echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile];
				}
			}

			echo '</span></td></tr></table></td></tr>';
		}
		unset($rsMIT);
	}
}

	// Gebietskennung / Gebietsleiter
$Ausgabe='';
if($Recht122==0)		// Alte oder neue Anzeige?
{
	if(($DatenBearbeitung & 16)==16)
	{
		$SQL = "SELECT VKG_ID, MIT_Bezeichnung, PER_NR";
		$SQL .= ' FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY';
		$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
		$SQL .= " ORDER BY MIT_BEZEICHNUNG";

		$rsPersListe = awisOpenRecordset($con, $SQL);
		$rsPersListeZeilen = $awisRSZeilen;

		// TODO: Aktuellen Suchen
		for($j=0;$j<$rsPersListeZeilen;$j++)
		{
			if($rsPersListe['VKG_ID'][$j]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
			{
				$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][$j]) . ">";
	//				$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][$j]) . ">";
				break;
			}
		}
		echo "<tr><td id=DatenFeld>";
		echo '<input type=hidden name=txtGebLeiter_old value=' . $Gebiet . '>';
		echo "<table border=0><tr><td width=150><span class=DatenFeldNormal>$Ausgabe Gebietsleiter" . ($Ausgabe!=''?'</a>':'') . " :</span></td><td><span class=DatenFeldNormalFett>";
		echo "<select name=txtGebLeiter >";
		echo '<option value=0>'.$AWISSprachKonserven['Wort']['nichtzugeordnet'].'</option>';
		for($j=0;$j<$rsPersListeZeilen;$j++)
		{
			echo "<option ";
			if($rsPersListe['VKG_ID'][$j] == $Gebiet)
			{
				echo " selected ";
			}
			echo " value='" . $rsPersListe["VKG_ID"][$j] . "'>" . $rsPersListe["MIT_BEZEICHNUNG"][$j] . "</option>";
		}
		echo "</select>";
		echo "</span></td></tr>";

		$SpeichernButton = True;
	}
	else
	{
		// Gebietsleiter
		$SQL = "SELECT VKG_ID, MIT_Bezeichnung, PER_NR";
		$SQL .= ' FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY';
		$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
		$SQL .= " WHERE VKG_ID='" . $Gebiet . "'";
		$rsPersListe = awisOpenRecordset($con, $SQL);
		if(isset($rsPersListe['VKG_ID'][0]) AND $rsPersListe['VKG_ID'][0]==$Gebiet AND $PersRecht>0)		// Zugriff auf Personaleinsatz - Daten?
		{
			$Ausgabe = "<a href=./filialinfo_PersEinsatz.php?FILID=".$rsFiliale['FIL_ID'][0]."&PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][0]) . ">";
		//	$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsPersListe['PER_NR'][0]) . ">";
		}

		echo "<tr><td id=DatenFeld><table border=0><tr><td width=150><span class=DatenFeldNormal>$Ausgabe Gebietsleiter" . ($Ausgabe!=''?'</a>':'') . " :</span></td><td><span class=DatenFeldNormalFett>";
		if (isset($rsPersListe['MIT_BEZEICHNUNG'][0]))
		{
			echo $rsPersListe['MIT_BEZEICHNUNG'][0];
		}
		echo "</span></td></tr>";

	}

		//**************************************
		// Regionalzentrum
		//**************************************

	if(($DatenBearbeitung & 32)==32)
	{
		$rsREZ = awisOpenRecordset($con, "SELECT * FROM RegionalZentren ORDER BY REZ_BEZEICHNUNG");
		$rsREZZeilen = $awisRSZeilen;

		echo "<tr><td id=DatenFeldNormal width=150>";
		echo '<input type=hidden name=txtVGK_ID_old value=' . $RegZentrum . '>';
		echo "<span class=DatenFeldNormal>Regionalzentrum:</span></td><td>";
		echo "<select name=txtVGK_ID >";
		echo '<option value=0>'.$AWISSprachKonserven['Wort']['nichtzugeordnet'].'</option>';
		for($j=0;$j<$rsREZZeilen;$j++)
		{
			echo "<option ";
			if($rsREZ['REZ_ID'][$j] == $RegZentrum)
			{
				echo " selected ";
			}
			echo " value='" . $rsREZ["REZ_ID"][$j] . "'>" . $rsREZ["REZ_BEZEICHNUNG"][$j] . "</option>";
		}
		echo "</select>";
		echo "</span></td></tr>";

		$SpeichernButton = True;

	}
	else
	{
		if($RegZentrum!="")
		{
			$rsRegionalZentrum = awisOpenRecordset($con, "SELECT * FROM RegionalZentren WHERE REZ_ID=0". $RegZentrum ."");
			If($rsRegionalZentrum["REZ_BEZEICHNUNG"][0]!="")
			{
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span></td><td>";
				echo "<span class=DatenFeldNormalFett>" . $rsRegionalZentrum["REZ_BEZEICHNUNG"][0]  . "</span></td></tr>";
			}
			else
			{
				echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionalzentrum: </span></td><td>";
				echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
			}
		}
	}
}
else
{
	// wg. Bug in den Tabellendefinitionen!!
	echo "<tr><td id=DatenFeld><table border=0>";

	echo '<tr><td width=200>Zuordnung:</td><td>';

	$FilObj = new awisFilialen($AWIS_KEY1);
	$Zuordnungen = $FilObj->Zuordnungen();
	$Text = '';
	foreach($Zuordnungen AS $Zuordnung)
	{
		$Text .= ' - <a href="./filialinfo_Main.php?cmdAktion=Filialinfos&FEB_KEY='.$Zuordnung['FEB_KEY'].'" title="'.$Zuordnung['FEB_BEMERKUNG'].'">'.$Zuordnung['FEB_BEZEICHNUNG'].'</a>';
	}
	echo substr($Text,2);
	echo '</td></tr>';

}


echo "<tr><td>&nbsp</td></tr>";		// Leerzeile
echo '</table></td></tr>';

	// Tabelle f?r die Anlieferung, Kommissioniertag und Regionalzentrum
echo "<tr><td><table border=0>";

echo '<tr><td id=DatenFeld>';
//Anlieferung
echo '<span class=DatenFeldNormal>Anlieferung: </span></td><td>';
echo "<span class=DatenFeldNormalFett>";
if($rsFiliale['FIL_LAGERKZ'][0]=='L')
{
	echo '<a href=/dokumentanzeigen.php?dateiname=anlieferung_werl&erweiterung=pdf&bereich=sonstiges><img border=0 src=/bilder/icon_pdf.png></a>';
}
elseif($rsFiliale['FIL_LAGERKZ'][0]=='N')
{
	echo '<a href=/dokumentanzeigen.php?dateiname=anlieferung_weiden&erweiterung=pdf&bereich=sonstiges><img border=0 src=/bilder/icon_pdf.png></a>';
}
echo "</span></td></tr>";
//Kommissioniertag
echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Kommissioniertag: </span></td><td>";
echo "<span class=DatenFeldNormalFett>";
if($rsFiliale['FIL_LAGERKZ'][0]=='L')
{
	echo '<a href=/dokumentanzeigen.php?dateiname=kommissioniertag_werl&erweiterung=pdf&bereich=sonstiges><img border=0 src=/bilder/icon_pdf.png></a>';
}
elseif($rsFiliale['FIL_LAGERKZ'][0]=='N')
{
	echo '<a href=/dokumentanzeigen.php?dateiname=kommissioniertag_weiden&erweiterung=pdf&bereich=sonstiges><img border=0 src=/bilder/icon_pdf.png></a>';
}
echo "</span></td></tr>";

/*
$Erg = SucheFilialInfo($rsFilialInfo, 49, "FIT_ID");		// Anlieferung
echo '<tr><td id=DatenFeld>';
echo "<input type=hidden name=txtAnlieferungstag_old value='" . ($Erg==-1?'':$rsFilialInfo["FIF_WERT"][$Erg]) . "'>";
echo '<span class=DatenFeldNormal>'.$AWISSprachKonserven['Wort']['Anlieferungstag'].' : </span></td><td>';

if(($DatenBearbeitung & 4)==4)
{
	echo "<select name=txtAnlieferungstag size=1>";
	echo "<option value=''>".$AWISSprachKonserven['Wort']['txt_BitteWaehlen']."</option>";
	echo "<option value='Montag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Montag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Montag']."</Option>";
	echo "<option value='Dienstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Dienstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Dienstag']."</Option>";
	echo "<option value='Mittwoch' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Mittwoch'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Mittwoch']."</Option>";
	echo "<option value='Donnerstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Donnerstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Donnerstag']."</Option>";
	echo "<option value='Freitag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Freitag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Freitag']."</Option>";
	echo "<option value='Samstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Samstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Samstag']."</Option>";
	echo "</Select>";
	echo "<span class=DatenFeldNormalFett> aus " . awis_LagerText($rsFiliale["FIL_LAGERKZ"][0]);	// Bezeichnung laden
	echo "</span></td></tr>";

	$SpeichernButton = True;
}
else
{
	//Sprachkonserven auskommentiert, da sonst vom Serviccenter kein Datum eingegeben werden kann z.B. bei Feiertagen
	//TR 17.03.08
	//echo "<span class=DatenFeldNormalFett>" . $AWISSprachKonserven['Wort']['Wochentag_'.$rsFilialInfo["FIF_WERT"][$Erg]];
	echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg];
	echo " aus " . awis_LagerText($rsFiliale["FIL_LAGERKZ"][0]);	// Bezeichnung laden
	echo "</span></td></tr>";
}

$Erg = SucheFilialInfo($rsFilialInfo, 50, "FIT_ID");		// Lieferh�ufigkeit
if (!empty($rsFilialInfo["FIF_WERT"][$Erg]))
	{
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>&nbsp;</span></td><td>";
	echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
}

$Erg = SucheFilialInfo($rsFilialInfo, 48, "FIT_ID");		// Kommissioniertag
	echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Kommissioniertag : </span></td><td>";
if(($DatenBearbeitung & 8)==8)
{
	echo "<input type=hidden name=txtKommissioniertag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<select name=txtKommissioniertag size=1>";
	echo "<option value='Montag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Montag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Montag']."</Option>";
	echo "<option value='Dienstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Dienstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Dienstag']."</Option>";
	echo "<option value='Mittwoch' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Mittwoch'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Mittwoch']."</Option>";
	echo "<option value='Donnerstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Donnerstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Donnerstag']."</Option>";
	echo "<option value='Freitag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Freitag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Freitag']."</Option>";
	echo "<option value='Samstag' ". ($rsFilialInfo["FIF_WERT"][$Erg]=='Samstag'?"Selected":"") . ">".$AWISSprachKonserven['Wort']['Wochentag_Samstag']."</Option>";
	echo "</Select>";
	echo "</td></tr>";

	$SpeichernButton = True;
}
else
{
	//Sprachkonserven auskommentiert, da sonst vom Serviccenter kein Datum eingegeben werden kann z.B. bei Feiertagen
	//TR 17.03.08
	//echo "<span class=DatenFeldNormalFett>" . $AWISSprachKonserven['Wort']['Wochentag_'.$rsFilialInfo["FIF_WERT"][$Erg]] . "</span></td></tr>";
	echo "<span class=DatenFeldNormalFett>" . $rsFilialInfo["FIF_WERT"][$Erg] . "</span></td></tr>";
}

//echo "<tr><td colspan=2><b>Achtung Sonderkommissionierung in KW 40/41</b></td></tr>";
*/
echo "</table></td></tr>";




//**********************************************************************************
//
// Regionaler Reifenrabatt
//
//**********************************************************************************

// Feld f�r Regionalen Reifenrabatt hinzugef�gt 21.09.2007 Wacker (IBM)

// Tabelle f�r Reifenrabatt

if (($RechteStufeFilialInfos&1)==1)
{

	echo "<tr><td><table border=0>";



		$SQL = "select FIF.FIF_WERT";
		$SQL .= ' from FILIALINFOS FIF, FILIALEN FIL';
		$SQL .= ' where FIF.FIF_FIL_ID = FIL.FIL_ID';
		$SQL .= ' AND FIF.FIF_FIT_ID = 204';
		$SQL .= " AND FIF.FIF_FIL_ID=" . $rsFiliale["FIL_ID"][0] ." ";

		//awis_debug(1, $SQL);

		$rsReifen = awisOpenRecordset($con, $SQL);
		$Ausgabe = $rsReifen["FIF_WERT"][0];

	if(($DatenBearbeitung&512) == 512)
	{



		echo "<tr><td>&nbsp</td></tr>";		// Leerzeile

		echo '<tr><td id=DatenFeld>';
		echo '<input type=hidden name=txtReifenrabatt_old value=' . $Ausgabe . '>';
		echo '<span class=DatenFeldNormal>Regionaler Reifenrabatt in %: </span>';
		echo "<span class=DatenFeldNormalFett><input accesskey=z  title='".$AWISSprachKonserven['Wort']['UserDat'].": " . (isset($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  "/" . (isset($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' type=text size='" . strlen($Ausgabe) . "' name=txtReifenrabatt value='" . $Ausgabe . "'></span>";
		echo '<span class=DatenFeldNormal></span>';
		$SpeichernButton = True;
		echo "</td></tr>";


	}
	else
	{
		echo "<tr><td id=DatenFeld><span class=DatenFeldNormal>Regionaler Reifenrabatt in %:</span><span class=DatenFeldNormalFett>" . $Ausgabe . "</span><span class=DatenFeldNormal></span></td></tr>";
	}


	echo "</table></td></tr>";

}
//**********************************************************************************


echo "</table></td>";	// Ende dritte Spalte
echo "</tr></table>";	// Ende Datentabelle

// Unterregister

awis_RegisterErstellen(4,$con,(isset($_GET['Seite'])?$_GET['Seite']:'Allgemein'));



/************************************************************************************
*
* Hilfsfunktionen
*
************************************************************************************/
Function SucheFilialInfo($rsFiliaInfo, $InfoID, $Spalte, $ImpQuelle = 0)
{

	$i=0;
	while(isset($rsFiliaInfo[$Spalte][$i]))
	{
		if($ImpQuelle>0)
		{
			 if($rsFiliaInfo['FIF_IMQ_ID'][$i]==$ImpQuelle AND $rsFiliaInfo[$Spalte][$i]==$InfoID)
			 {
				return $i;
			 }
		}
		elseif($rsFiliaInfo[$Spalte][$i]==$InfoID)
		{
			return $i;
		}
		$i++;
	}
	return -1;
}

/************************************************************
*
* Schreibt eine Person aus der Filiale mit einem Link
*
*************************************************************/
Function SchreibePersonenFeld($Beschreibung, $Breite, $WertID, $rsFilialInfo, $con, $Link)
{
	global $awisRSZeilen;
	global $AWISSprachKonserven;

	$Ausgabe="";

	$SQL="SELECT FIL_LAN_WWSKENN FROM FILIALEN WHERE FIL_ID=0".$rsFilialInfo["FIF_FIL_ID"][0];
	$rsFilialLand=awisOpenRecordset($con,$SQL);

	if ($rsFilialLand["FIL_LAN_WWSKENN"][0] != 'BRD')
	{

		echo "<tr><td id=DatenFeld><table border=0><tr><td valign=top width=$Breite><span class=DatenFeldNormal>";

		$SQL="SELECT SAW_NAME, SAW_PER_NR FROM V_SONDERAUSWEISE_GL_WL_AKTIV WHERE SAW_FIL_ID=".$rsFilialInfo["FIF_FIL_ID"][0]." AND FIF_FIT_ID=".$WertID;
		$rsSAW_Filinfo=awisOpenRecordset($con,$SQL);


		if (isset($rsSAW_Filinfo["SAW_NAME"][0]))
		{
			$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsSAW_Filinfo["SAW_PER_NR"][0]) . ">";
			$Ausgabe.=$Beschreibung."</a>: </span></td><td><span class=DatenFeldNormalFett>";
			$Ausgabe.=$rsSAW_Filinfo["SAW_NAME"][0];
		}
		else
		{
			$Ausgabe .= $Beschreibung . ':</td><td><span class=DatenFeldNormal>'.$AWISSprachKonserven['Wort']['keineAngabe'];
		}
		echo "" . $Ausgabe . "</span></td></tr></table></td></tr>";
	}
	else
	{
	 	echo "<tr><td id=DatenFeld><table border=0><tr><td valign=top width=$Breite><span class=DatenFeldNormal>";

		$Erg = SucheFilialInfo($rsFilialInfo, $WertID, "FIT_ID");

		//Ausblenden von 2 Personen, da diese im Projekt neue Filialsysteme sind
		//Herr Freitag (PERNR 105356) und Herr Weber (PERNR 106873)
		//TR 31.01.08
		if($Erg!=-1 AND $rsFilialInfo["FIF_WERT"][$Erg]!='' AND $rsFilialInfo["FIF_WERT"][$Erg] != '105356' and $rsFilialInfo["FIF_WERT"][$Erg] != '106873')		// Ist ein Filial-Mitarbeiter vorhanden?
		{
			if($Link)
			{
					$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsFilialInfo["FIF_WERT"][$Erg]) . ">";
			}
			echo "$Ausgabe $Beschreibung" . ($Ausgabe!=''?'</a>':'') . ": </span></td><td><span class=DatenFeldNormalFett>";
			$Ausgabe=''	;

		//	if($rsFilialInfo["FIF_WERT"][$Erg]!="" AND $Link AND awisHatDasRecht(112)>0)
			{
		//		$Ausgabe = "<a href=./filialinfo_Schicht.php?PER_NR=" . str_replace(" ","%20",$rsFilialInfo["FIF_WERT"][$Erg]) . ">";
			}

			$rsPerson = awisOpenRecordset($con,"SELECT * FROM PERSONAL WHERE PER_NR='" . $rsFilialInfo["FIF_WERT"][$Erg] . "'");

			If($Ausgabe!="")
			{
				$Ausgabe .= $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0] . "</a>";
			}
			else
			{
				if($rsPerson["PER_NACHNAME"][0]!="")
				{
					$Ausgabe = $rsPerson["PER_NACHNAME"][0] . ", " . $rsPerson["PER_VORNAME"][0];
				}
			}
		}
		else
		{
			$Ausgabe .= $Beschreibung . ':</td><td><span class=DatenFeldNormal>'.$AWISSprachKonserven['Wort']['keineAngabe'];
		}

				// Weitere Mitarbeiter aus der Personaleinsatzpl�ne
		//Ausblenden von 2 Personen, da diese im Projekt neue Filialsysteme sind
		//Herr Freitag (PERNR 105356) und Herr Weber (PERNR 106873)
		//TR 31.01.08

		$SQL = "SELECT PER_Nr, PER_Nachname, Per_Vorname
					FROM PERSONAL, PERSONALEINSATZPLANUNG,
					(SELECT * FROM PERSONALTAETIGKEITEN WHERE PET_USER=(SELECT DISTINCT PER_USER FROM PERSONAL WHERE PER_FIL_ID=" . ($rsFilialInfo["FIF_FIL_ID"][0]==''?'0':$rsFilialInfo["FIF_FIL_ID"][0]) . "))
					 WHERE PER_Nr = PEP_PER_Nr AND PET_ID = PER_PET_ID
					  AND PEP_FIL_ID = " . ($rsFilialInfo["FIF_FIL_ID"][0]==''?'0':$rsFilialInfo["FIF_FIL_ID"][0]) . "
					  AND PER_AUSTRITT IS NULL
					  AND PER_NR <> 105356 AND PER_NR <> 106873
					  AND PET_TAETIGKEIT = '";			// Wieder auf die Zahl->Stefan Import
		$AusgabeAlternativ=0;
		switch($WertID)
		{
			case 74:				// 1. Gesch?ftsleiter
				$SQL.="1. Gesch�ftsleiter/in'";
				$AusgabeAlternativ=1;
				break;
			case 75:				// 2. Gesch?ftsleiter
				$SQL.="2. Gesch�ftsleiter/in'";
				$AusgabeAlternativ=1;
				break;
			case 76:				// 1. Werkstattleiter
				$SQL.="1. Werkstattleiter/in'";
				$AusgabeAlternativ=1;
				break;
			case 77:				// 2. Werkstattleiter
				$SQL.="2. Werkstattleiter/in'";
				$AusgabeAlternativ=1;
				break;
		}

		if($AusgabeAlternativ==1)
		{
			$SQL .= " AND PEP_JAHR = " . date("Y",time()) . " AND PEP_KALENDERWOCHE = " . date("W",time());

			$rsPEP = awisOpenRecordset($con,$SQL);

			if (!empty($rsPerson['PER_NR'][0]))
			{
				$PER_NR_ORIG=$rsPerson['PER_NR'][0];
			}
			else{
				$PER_NR_ORIG='';
			}

			for($pep=0;$pep<$awisRSZeilen;$pep++)
			{

				if($rsPEP['PER_NR'][$pep]!=$PER_NR_ORIG)
				{
					$Ausgabe .= '<br><a ' . ($Link?'href=./filialinfo_Main.php?cmdAktion=Personal':'') . ' name=Alternativ title=\''.$Beschreibung.' '.$AWISSprachKonserven['Wort']['lautPersonaleinsatzplan'].'\'>*</a><i>' . $rsPEP['PER_NACHNAME'][$pep] . ', ' . $rsPEP['PER_VORNAME'][$pep] . '</i>';
				}
			}
		}

		echo "" . $Ausgabe . "</span></td></tr></table></td></tr>";
	}
}
?>