<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_AbfallentsorgungAnlegen');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$Form->DebugAusgabe(1,$_POST);
	
	//$AWIS_KEY1=$_POST['txtADK_KEY'];
		
	if(isset($_POST['txtADK_KEY']))
	{
		$AWIS_KEY1=$_POST['txtADK_KEY'];
		
		$Felder = $Form->NameInArray($_POST, 'txtADK_',1,1);
					
		if($Felder!='')
		{			
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ADK','ADK_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
			$FeldListe='';
			$SQL = '';
						
			if(floatval($_POST['txtADK_KEY'])==0)		//Neue ADR-Abwicklung
			{
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';			
				$Pflichtfelder = array('ADK_FIL_ID');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='')	// Filiale muss angegeben werden
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ADK'][$Pflichtfeld].'<br>';
					}
				}
				
				$SQL = 'select ADK_KEY FROM ADRKOPF ';
				$SQL .= 'WHERE ADK_FIL_ID = '.$DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID'],true).' AND ADK_STATUS = \'O\'';
				$SQL .= 'AND ADK_ART = '.$DB->FeldInhaltFormat('Z',1,true);
			
				$rsADKFil = $DB->RecordSetOeffnen($SQL);
				if ($rsADKFil->AnzahlDatensaetze()>=1)
				{
						$Fehler .= $TXT_Speichern['Fehler']['err_AbfallentsorgungAnlegen'].' ('.$TXT_Speichern['ADK']['ADK_FIL_ID'].' '.$_POST['txtADK_FIL_ID'].')<br>';
				}				
								
				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();
					
					$Link='./adr_Main.php?cmdAktion=Suche';
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href','cmdAdr',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
					$Form->SchaltflaechenEnde();									

					$Speichern = false;					
					die();										
				}
				else 
				{
					$Speichern = true;
				}
				
				if($Speichern == true)
				{									
					$rsVorgangnr = $DB->RecordSetOeffnen('SELECT ADV_ID as MAXVORGANGNR FROM ADRVORGANG WHERE ADV_FILID='. $DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID']));
					$Vorgangnr = $DB->FeldInhaltFormat('Z',$rsVorgangnr->FeldInhalt('MAXVORGANGNR'))+1;

                    if($Vorgangnr>1)
                    {
                        $rsVorgangnr = $DB->RecordSetOeffnen('UPDATE ADRVORGANG set ADV_ID='. $Vorgangnr .' WHERE ADV_FILID = '. $DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID']));
                    }
                    else{
                        $rsVorgangnr = $DB->RecordSetOeffnen('INSERT INTO ADRVORGANG (ADV_ID, ADV_FILID) VALUES ('. $Vorgangnr .', '. $DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID']).')');
                    }


					$rsFilLager = $DB->RecordSetOeffnen('SELECT UPPER(FIL_LAGERKZ) AS LAGER FROM FILIALEN WHERE FIL_ID=0'.$DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID'],false));
					$FilLager = $rsFilLager->FeldInhalt('LAGER');
					
					$Entsorgernr = '';
					
					if ($FilLager == 'N')
					{
						$Entsorgernr = 'I363W1003';
					}
					elseif($FilLager == 'L')
					{
						$Entsorgernr = 'E97497404';
					}
					
					$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=911 AND FIF_FIL_ID =0'.$DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID'],false);
					$rsFilinfoADR = $DB->RecordSetOeffnen($SQL);
					
					$Fehler = '';
					$SQL = 'INSERT INTO ADRKopf';
					$SQL .= '(ADK_FIL_ID, ADK_VORGANGNR, ADK_ART, ADK_STATUS, ADK_DATUMERSTELLUNG';
					$SQL .= ',ADK_ERZEUGERNR, ADK_BEFOERDERERNR, ADK_ENTSORGERNR, ADK_LAGERKZ';					
					$SQL .= ',ADK_USER, ADK_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= '' . $DB->FeldInhaltFormat('Z',$_POST['txtADK_FIL_ID'],true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',$Vorgangnr,true);					
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',1);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T','O');
					$SQL .= ' ,SYSDATE';
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$rsFilinfoADR->FeldInhalt('FIF_WERT'),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T','I36T0250',true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$Entsorgernr,true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$FilLager,true);					
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
				
					if($DB->Ausfuehren($SQL)===false)
					{
						awisErrorMailLink('adr_speichern.php',1,$awisDBError['message'],'200909171407');
						die();
					}
					$SQL = 'SELECT seq_ADK_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderter ADRkopf
			{
				$Felder = explode(';',$Form->NameInArray($_POST, 'txtADK_',1,1));			
				$FehlerListe = array();
				$UpdateFelder = '';
				$Fehler = '';
				
				$rsADK = $DB->RecordSetOeffnen('SELECT * FROM ADRKopf WHERE ADK_key=' . $_POST['txtADK_KEY'] . '');
							
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						switch ($FeldName)
						{
							default:							
								$WertNeu=$DB->FeldInhaltFormat($rsADK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);	
						}					

						$WertAlt=$DB->FeldInhaltFormat($rsADK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsADK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsADK->FeldInhalt($FeldName),true);

						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
											
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
				
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsADK->FeldInhalt('ADK_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
						//$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE ADRKopf SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ADK_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ADK_userdat=sysdate';
					$SQL .= ' WHERE ADK_key=0' . $_POST['txtADK_KEY'] . '';
					$DB->Ausfuehren($SQL,'',true);
				}
			}
		}
	}
	
	
	
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtADP_MENGE_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{								
			$FeldTeile = explode('_',$Feld);
			
			$SQL = 'SELECT * FROM ADRPOS WHERE ADP_KEY = 0'.$FeldTeile[3];	
			$rsADP = $DB->RecordSetOeffnen($SQL);
			
			if(!$rsADP->EOF())
			{
				$FeldTeile[3]=$rsADP->FeldInhalt('ADP_KEY');
			}
	
			$SQL = '';
			if($FeldTeile[3]=='')		// Neu
			{
				$SQL = 'SELECT ADS_ZULADGEW FROM ADRSTAMM WHERE ADS_KEY= 0'.$FeldTeile[2];	
				$rsFelder = $DB->RecordSetOeffnen($SQL);																
				
				$SQL='';				
				if($DB->FeldInhaltFormat('N0',$_POST[$Feld],false)!='0')				
				{
					$SQL = 'INSERT INTO ADRPOS';
					$SQL .= '(ADP_ADK_KEY, ADP_ADS_KEY, ADP_MENGE, ADP_GEWICHT, ADP_USER,ADP_USERDAT)';					
					$SQL .= ' VALUES(';
					$SQL .= ' '.$AWIS_KEY1;
					$SQL .= ','.$FeldTeile[2];
					$SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST[$Feld],false);					
					$SQL .= ', '.$DB->FeldInhaltFormat('N2',$DB->FeldInhaltFormat('N2',$rsFelder->FeldInhalt('ADS_ZULADGEW'))*$_POST[$Feld],false);					
					$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', sysdate)';					
				}
			}
			else		// �ndern
			{
				$Speichern = false;

				$FeldName = 'ADP_MENGE';
				$WertNeu=$DB->FeldInhaltFormat($rsADP->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsADP->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr($Feld,3)],true);
				if($WertAlt!=$WertNeu)
				{
					$Speichern = true;
				}
								
				if($Speichern)
				{
					$SQL = 'SELECT ADS_ZULADGEW FROM ADRSTAMM WHERE ADS_KEY= 0'.$FeldTeile[2];
					$rsFelder = $DB->RecordSetOeffnen($SQL);
					$SQL='';

				    //ho, 20120906 Falls bereits abgeschlossen alte Positionsdaten speichern
				    if ($rsADK->FeldInhalt('ADK_STATUS')=='A')
				    {
				        if (is_numeric($WertNeu))
				        {
        					$SQL = 'INSERT INTO ADRPOS_H ';
        					$SQL .= '(ADH_ADP_KEY,ADH_MENGE_ALT,ADH_MENGE_NEU,ADH_GEWICHT_ALT,ADH_GEWICHT_NEU,ADH_USER_ALT,ADH_USERDAT_ALT,ADH_USER,ADH_USERDAT)';					
        					$SQL .= ' VALUES (';
        					$SQL .= $rsADP->FeldInhalt('ADP_KEY');
        					$SQL .= ','.$DB->FeldInhaltFormat('N0',$rsADP->FeldInhalt('ADP_MENGE'));
        					$SQL .= ','.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
        					$SQL .= ','.$DB->FeldInhaltFormat('N2',$rsADP->FeldInhalt('ADP_GEWICHT'));
        					$SQL .= ','.$DB->FeldInhaltFormat('N2',$DB->FeldInhaltFormat('N2', $rsFelder->FeldInhalt('ADS_ZULADGEW'))*$_POST[$Feld]);
        					$SQL .= ','.$DB->FeldInhaltFormat('T',$rsADP->FeldInhalt('ADP_USER'));
        					$SQL .= ','.$DB->FeldInhaltFormat('DU',$rsADP->FeldInhalt('ADP_USERDAT'));
    						$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
    						$SQL .= ',sysdate)';
    						//$Form->DebugAusgabe(1,$SQL);
        					$DB->Ausfuehren($SQL,'',true);

        					$SQL = 'UPDATE ADRPOS SET ';
    						$SQL .= ' ADP_MENGE= '.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
    						$SQL .= ', ADP_GEWICHT = '.$DB->FeldInhaltFormat('N2',$DB->FeldInhaltFormat('N2', $rsFelder->FeldInhalt('ADS_ZULADGEW'))*$_POST[$Feld]);					
    						$SQL .= ', ADP_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
    						$SQL .= ', ADP_USERDAT = sysdate';
    						$SQL .= ' WHERE ADP_KEY = 0'.$FeldTeile[3];		
				        }
				    }
					else
					{
    					if ($WertNeu != '0')
       					{
       					    $SQL = 'UPDATE ADRPOS SET ';
    						$SQL .= ' ADP_MENGE= '.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
    						$SQL .= ', ADP_GEWICHT = '.$DB->FeldInhaltFormat('N2',$DB->FeldInhaltFormat('N2', $rsFelder->FeldInhalt('ADS_ZULADGEW'))*$_POST[$Feld]);					
    						$SQL .= ', ADP_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
    						$SQL .= ', ADP_USERDAT = sysdate';
    						$SQL .= ' WHERE ADP_KEY = 0'.$FeldTeile[3];		
       					}
       					else 
       					{
        			        $SQL = ' DELETE FROM ADRPOS WHERE ADP_KEY = 0'.$FeldTeile[3];
    					}
					}
				}
			}

			if($SQL!='')
			{	
				//$Form->DebugAusgabe(1,$SQL);			
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}	

	//Stammdaten
	if(isset($_POST['txtADS_KEY']))
	{
		$AWIS_KEY2=$_POST['txtADS_KEY'];
		
		$Felder = $Form->NameInArray($_POST, 'txtADS_',1,1);
		
		if($Felder!='')
		{
			//$Form->DebugAusgabe(1,$Felder);	
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ADS','ADS_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
			$FeldListe='';
			$SQL = '';						
			
			if(floatval($_POST['txtADS_KEY'])==0)		//Neue ADR-Abwicklung
			{	
				//$Form->DebugAusgabe(1,$Felder);														
					$Fehler = '';
					$SQL = 'INSERT INTO ADRStamm';
					$SQL .= '(ADS_ADRART, ADS_ADRNR, ADS_LAGACODE, ADS_LAGABEZ, ADS_EWCCODE';
					$SQL .= ' ,ADS_EWCBEZ, ADS_BEHTYP, ADS_BEHGEW, ADS_ZULADGEW, ADS_FAKTOR, ADS_EINHEIT, ADS_AST_ATUNR, ADS_USER, ADS_USERDAT';									
					$SQL .= ')VALUES (';
					$SQL .= '' . $DB->FeldInhaltFormat('Z',$_POST['txtADS_ADRART'],true);					
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_ADRNR']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_LAGACODE']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_LAGABEZ']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_EWCCODE']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_EWCBEZ']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_BEHTYP']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('N2',$_POST['txtADS_BEHGEW']);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('N2',$_POST['txtADS_ZULADGEW']);					
					$SQL .= ' ,' . $DB->FeldInhaltFormat('N2',$_POST['txtADS_FAKTOR']);					
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_EINHEIT']);					
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADS_AST_ATUNR']);					
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
					//$Form->DebugAusgabe(1,$SQL);
					if($DB->Ausfuehren($SQL)===false)
					{
						awisErrorMailLink('adr_speichern.php',1,$awisDBError['message'],'200909171407');
						die();
					}
					$SQL = 'SELECT seq_ADS_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}			
			else 					// ge�nderte ADRkopf
			{
				$Felder = explode(';',$Form->NameInArray($_POST, 'txtADS_',1,1));			
				//$Form->DebugAusgabe(1,$Felder);			
				
				$FehlerListe = array();
				$UpdateFelder = '';
				$Fehler = '';
				
				$rsADS = $DB->RecordSetOeffnen('SELECT * FROM ADRStamm WHERE ADS_key=' . $_POST['txtADS_KEY'] . '');
				
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						switch ($FeldName)
						{
							default:							
								$WertNeu=$DB->FeldInhaltFormat($rsADS->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);	
						}					

						$WertAlt=$DB->FeldInhaltFormat($rsADS->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsADS->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsADS->FeldInhalt($FeldName),true);

						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
											
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
				
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsADS->FeldInhalt('ADS_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
						//$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE ADRStamm SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ADS_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ADS_userdat=sysdate';
					$SQL .= ' WHERE ADS_key=0' . $_POST['txtADS_KEY'] . '';
					//$Form->DebugAusgabe(1,$SQL);
					if($DB->Ausfuehren($SQL)===false)
					{
						$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200909171410');
						throw new awisException('Fehler beim Speichern',909171410,$SQL,2);
					}
				}
			}
		}
	}

	//Stammdaten
	if(isset($_POST['txtADX_KEY']))
	{
		$AWIS_KEY2=$_POST['txtADX_KEY'];
		
		$Felder = $Form->NameInArray($_POST, 'txtADX_',1,1);
		
		if($Felder!='')
		{
			//$Form->DebugAusgabe(1,$Felder);	
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ADX','ADX_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
			$FeldListe='';
			$SQL = '';						
			
			if(floatval($_POST['txtADX_KEY'])==0)		//Neue ADR-Abwicklung
			{
                $Block = $_POST['txtADX_ADRBLOCK'];
                $Sort = $_POST['txtADX_ADRSORT'];

                if($Block <= 10) {
                    if($Sort <= 5) {
                        $Fehler = '';
                        $SQL = 'INSERT INTO ADRTexte';
                        $SQL .= '(ADX_ADRART, ADX_ADRBLOCK, ADX_ADRSORT, ADX_ADRBEZ ';
                        $SQL .= ', ADX_USER, ADX_USERDAT';
                        $SQL .= ')VALUES (';
                        $SQL .= '' . $DB->FeldInhaltFormat('NO',$_POST['txtADX_ADRART']);
                        $SQL .= ' ,' . $DB->FeldInhaltFormat('NO',$_POST['txtADX_ADRBLOCK']);
                        $SQL .= ' ,' . $DB->FeldInhaltFormat('NO',$_POST['txtADX_ADRSORT']);
                        $SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtADX_ADRBEZ']);
                        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';
                        //$Form->DebugAusgabe(1,$SQL);
                        if($DB->Ausfuehren($SQL)===false)
                        {
                            awisErrorMailLink('adr_speichern.php',1,$awisDBError['message'],'200909171407');
                            die();
                        }
                        $SQL = 'SELECT seq_ADX_KEY.CurrVal AS KEY FROM DUAL';
                        $rsKey = $DB->RecordSetOeffnen($SQL);
                        $AWIS_KEY2=$rsKey->FeldInhalt('KEY');
                    } else {
                        $Form->Hinweistext('Datensatz nicht gespeichert. Maximal 5 Zeilen je Block erlaubt');
                    }
                } else {
                    $Form->Hinweistext('Datensatz nicht gespeichert. Maximal 10 Bl�cke erlaubt');
                }

			}			
			else 					// ge�nderte ADRkopf
			{
				$Felder = explode(';',$Form->NameInArray($_POST, 'txtADX_',1,1));			
				//$Form->DebugAusgabe(1,$Felder);			
				
				$FehlerListe = array();
				$UpdateFelder = '';
				$Fehler = '';
				
				$rsADX = $DB->RecordSetOeffnen('SELECT * FROM ADRTexte WHERE ADX_key=' . $_POST['txtADX_KEY'] . '');
				
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						switch ($FeldName)
						{
							default:							
								$WertNeu=$DB->FeldInhaltFormat($rsADX->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);	
						}					

						$WertAlt=$DB->FeldInhaltFormat($rsADX->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsADX->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsADX->FeldInhalt($FeldName),true);

						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
											
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
				
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsADX->FeldInhalt('ADX_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
						//$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE ADRTexte SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ADX_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ADX_userdat=sysdate';
					$SQL .= ' WHERE ADX_key=0' . $_POST['txtADX_KEY'] . '';
					//$Form->DebugAusgabe(1,$SQL);
					if($DB->Ausfuehren($SQL)===false)
					{
						$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200909171410');
						throw new awisException('Fehler beim Speichern',909171410,$SQL,2);
					}
				}
			}
		}
	}		
}	
	
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);	
}
catch (Exception $ex)
{
	
}
?>