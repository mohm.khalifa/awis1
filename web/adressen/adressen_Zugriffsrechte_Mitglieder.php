<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $con;
global $AWISBenutzer;
global $awisRSZeilen;
global $AWISSprache;			
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try 
{
    require_once('awisDatenbank.inc');
    require_once('awisFormular.inc');

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('AZG','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('AZM','AZM_%');
    $TextKonserven[]=array('XBN','XBN_VOLLERNAME');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht3001 = $AWISBenutzer->HatDasRecht(3001);
    
    if($Recht3001==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
        die();
    }
    $SQL = 'SELECT *';
    $SQL .= ' FROM AdressenZugriffsGrpMitglieder';
    $SQL .= ' INNER JOIN Benutzer ON AZM_XBN_KEY = XBN_KEY';
    $SQL .= ' WHERE AZM_AZG_KEY=0'.$AWIS_KEY1;
    
    if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
    {
    	$SQL .= ' AND AZM_KEY=0'.intval($AWIS_KEY2);
    }
    elseif(isset($_GET['AZMKEY']))
    {
    	$SQL .= ' AND AZM_KEY=0'.intval($_GET['AZMKEY']);
    }
    if(!isset($_GET['AZMSort']))
    {
    	$SQL .= ' ORDER BY XBN_VOLLERNAME';
    }
    else
    {
    	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['AZMSort']);
    }
    
    //********************************************************
    // Daten suchen
    //********************************************************    
    $rsAZM = $DB->RecordSetOeffnen($SQL);
    $rsAZMZeilen = $rsAZM->AnzahlDatensaetze();
   
    if($rsAZMZeilen >= 1 or isset($_GET['APListe']))					// Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();
    
        if($Recht3001>0)
        {
            $Icons[] = array('new','./adressen_Main.php?cmdAktion=Zugriffsrechte&Seite=Mitglieder&AZMKEY=0');
            $Form->Erstelle_ListeIcons($Icons,36,-1);
        }
    
        $Link = './adressen_Main.php?cmdAktion=Zugriffsrechte&Seite=Mitglieder';
        $Link .= '&AZMSort=XBN_VOLLERNAME'.((isset($_GET['AZMSort']) AND ($_GET['AZMSort']=='XBN_VOLLERNAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'],350,'',$Link);
        $Form->ZeileEnde();
    
        for($AZMZeile=0;$AZMZeile<$rsAZMZeilen;$AZMZeile++)
        {
            $Form->ZeileStart();
            $Icons = array();
        
            if($Recht3001>0)	// Ändernrecht
            {
    		    $Icons[] = array('delete','./adressen_Main.php?cmdAktion=Zugriffsrechte&Seite=Mitglieder&Del='.$rsAZM->FeldInhalt('AZM_KEY'));
            }
    
	        $Form->Erstelle_ListeIcons($Icons,36,($AZMZeile%2));
            $Form->Erstelle_ListenFeld('XBN_VOLLERNAME',$rsAZM->FeldInhalt('XBN_VOLLERNAME'),20,350,false,($AZMZeile%2),'','','T');
            $Form->ZeileEnde();
            $rsAZM->DSWeiter();
        }
        $Form->Formular_Ende();
    }
    else 		// Einer oder keiner
    {
        $Form->Formular_Start();
        echo '<input name=txtAZM_KEY type=hidden value=0'.($rsAZM->FeldInhalt('AZM_KEY')!=0?$rsAZM->FeldInhalt('AZM_KEY'):0) .'>';
        $AWIS_KEY2 = $rsAZM->FeldInhalt('AZM_KEY')!=0?$rsAZM->FeldInhalt('AZM_KEY'):0;
        
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Zugriffsrechte&Seite=Mitglieder&APListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAZM->FeldInhalt('AZM_KEY')!=0?$rsAZM->FeldInhalt('AZM_USER'):''));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAZM->FeldInhalt('AZM_KEY')!=0?$rsAZM->FeldInhalt('AZM_USERDAT'):''));
        $Form->InfoZeile($Felder,'');
    
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'].':',150);
        $SQL = 'SELECT XBN_KEY, XBN_VOLLERNAME';
        $SQL .= ' FROM Benutzer';
        $SQL .= ' WHERE UPPER(XBN_NAME) NOT LIKE \'FIL%\'';
        $SQL .= ' ORDER BY XBN_VOLLERNAME';
        $Form->Erstelle_SelectFeld('AZM_XBN_KEY',$rsAZM->FeldInhalt('AZM_XBN_KEY')!=''?$rsAZM->FeldInhalt('AZM_XBN_KEY'):'',200,$Recht3001,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();
    
        $Form->Formular_Ende();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>