<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZAL','%');
	$TextKonserven[]=array('ZAP','ZAP_GUELTIGAB');
	$TextKonserven[]=array('ZAP','ZAP_VKPR');
	$TextKonserven[]=array('ZAP','ZAP_EK');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10009 = $AWISBenutzer->HatDasRecht(10009);		// Artikelstamm
	if(($Recht10009&16)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZAL_LIE_NR DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZukaufArtikelLieferanten.*, LIE_NAME1, LIE_NAME2';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZukaufArtikelLieferanten ';
	$SQL .= ' INNER JOIN Lieferanten ON ZAL_LIE_NR = LIE_NR';
	$SQL .= ' WHERE ZAL_ZLA_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
		$SQL .= ' AND ZAL_KEY = 0'.$AWIS_KEY2;
	}
	elseif($AWIS_KEY2>0)
	{
		$SQL .= ' AND ZAL_KEY = 0'.$AWIS_KEY2;
	}


	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZAL = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if($rsZAL->AnzahlDatensaetze()>1 OR isset($_GET['SListe']) OR !isset($_GET['Edit']))
	{
		$Form->ZeileStart();
		if((intval($Recht10009)&6)!=0)
		{
			$Icons[] = array('new','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLA_KEY='.$AWIS_KEY1.'&Edit=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

  		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAL_LIE_NR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAL_LIE_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAL']['ZAL_LIE_NR'],100,'',$Link);
  		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=LIE_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],290,'',$Link);
  		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAL_BESTELLNUMMER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAL_BESTELLNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAL']['ZAL_BESTELLNUMMER'],150,'',$Link);
  		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAL_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAL_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAL']['ZAL_AST_ATUNR'],150,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_GUELTIGAB'],110,'','');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_VKPR'],150,'','');
		if(($Recht10009&32)==32)
		{
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAP']['ZAP_EK'],110,'','');
		}

		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZAL->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht10009&2)>0)	// �ndernrecht
			{
				$Icons[] = array('edit','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLA_KEY='.$AWIS_KEY1.'&Edit='.$rsZAL->FeldInhalt('ZAL_KEY'));
			}
			if(intval($Recht10009&4)>0)	// �ndernrecht
			{
				$Icons[] = array('delete','./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLA_KEY='.$AWIS_KEY1.'&Del='.$rsZAL->FeldInhalt('ZAL_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*ZAL_LIE_NR',$rsZAL->FeldInhalt('ZAL_LIE_NR'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*LIE_NAME1',substr($rsZAL->FeldInhalt('LIE_NAME1'),0,20),20,290,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAL_BESTELLNUMMER',$rsZAL->FeldInhalt('ZAL_BESTELLNUMMER'),20,150,false,($DS%2),'','','T');
			$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Details&AST_ATUNR='.$rsZAL->FeldInhalt('ZAL_AST_ATUNR');
			$Form->Erstelle_ListenFeld('*ZAL_AST_ATUNR',$rsZAL->FeldInhalt('ZAL_AST_ATUNR'),20,150,false,($DS%2),$Link,'','T');

			//pr�fen wegen Datumsformatierung
			$SQL = 'SELECT ZAP_VKPR, ZAP_EK, ZAP_GUELTIGAB ';
			$SQL .= ' FROM ZUKAUFARTIKELLIEFERANTENPREISE ';
			$SQL .= ' WHERE ZAP_ZAL_KEY='.$rsZAL->FeldInhalt('ZAL_KEY');
			$SQL .= ' AND ZAP_GUELTIGAB <= SYSDATE';
			$SQL .= ' ORDER BY ZAP_GUELTIGAB DESC';

			$rsZAP = $DB->RecordSetOeffnen($SQL);
			$Form->Erstelle_ListenFeld('*ZAP_GUELTIGAB',$rsZAP->FeldInhalt('ZAP_GUELTIGAB'),20,110,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('*ZAP_VKPR',$rsZAP->FeldInhalt('ZAP_VKPR'),20,150,false,($DS%2),'','','N2');
			if(($Recht10009&32)==32)
			{
				$Form->Erstelle_ListenFeld('*ZAP_EK',$rsZAP->FeldInhalt('ZAP_EK'),20,110,false,($DS%2),'','','N2');
			}

			$Form->ZeileEnde();

			$rsZAL->DSWeiter();
			$DS++;
		}


		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsZAL->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht10009&6);
		$AWIS_KEY2 = $rsZAL->FeldInhalt('ZAL_KEY');

		$Form->Erstelle_HiddenFeld('ZAL_KEY',$AWIS_KEY2);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufartikel_Main.php?cmdAktion=Details&Seite=Lieferanten&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZAL->FeldInhalt('ZAL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZAL->FeldInhalt('ZAL_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_LIE_NR'].':',200);
		$SQL = 'SELECT LIE_NR, LIE_NAME1';
		$SQL .= ' FROM Lieferanten';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' AND (NOT EXISTS(SELECT * FROM ZukaufArtikelLieferanten WHERE ZAL_ZLA_KEY = 0'.$AWIS_KEY1.' AND ZAL_LIE_NR=LIE_NR) OR LIE_NR='.$DB->FeldInhaltFormat('T',$rsZAL->FeldInhalt('ZAL_LIE_NR'),true).')';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZAL_LIE_NR',$rsZAL->FeldInhalt('ZAL_LIE_NR'),300,$EditModus,$SQL);
		if($EditModus)
		{
			$AWISCursorPosition='txtZAL_LIE_NR';
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZAL_BEZEICHNUNG',$rsZAL->FeldInhalt('ZAL_BEZEICHNUNG'),80,600,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Link = '../warengruppen/warengruppen_Main.php?cmdAktion=Details&WG1='.$rsZAL->FeldInhalt('ZAL_WG1').'&WG2='.$rsZAL->FeldInhalt('ZAL_WG2').'&WG3='.$rsZAL->FeldInhalt('ZAL_WG3').'';
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_WG1'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZAL_WG1',$rsZAL->FeldInhalt('ZAL_WG1'),5,80,$EditModus,'','','','T','L','','',20);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_WG2'].':',100);
		$Form->Erstelle_TextFeld('ZAL_WG2',$rsZAL->FeldInhalt('ZAL_WG2'),5,80,$EditModus,'','','','T','L','','',20);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_WG3'].':',100);
		$Form->Erstelle_TextFeld('ZAL_WG3',$rsZAL->FeldInhalt('ZAL_WG3'),5,80,$EditModus,'','','','T','L','','',20);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AktuellesSortiment'].':',200);
		$SQL = 'SELECT ZSZ_SORTIMENT FROM ZukaufSortimentZuordnungen';
		$SQL .= ' WHERE ZSZ_WG1='.$DB->FeldInhaltFormat('T',$rsZAL->FeldInhalt('ZAL_WG1'));
		$SQL .= ' AND ZSZ_WG2='.$DB->FeldInhaltFormat('T',$rsZAL->FeldInhalt('ZAL_WG2'));
		$SQL .= ' AND ZSZ_WG3='.$DB->FeldInhaltFormat('T',$rsZAL->FeldInhalt('ZAL_WG3'));
		$rsZSZ=$DB->RecordSetOeffnen($SQL);
		$Link = '../warengruppen/warengruppen_Main.php?cmdAktion=Details&WG1='.$rsZAL->FeldInhalt('ZAL_WG2').'&WG2='.$rsZAL->FeldInhalt('ZAL_WG2').'&WG3='.$rsZAL->FeldInhalt('ZAL_WG3').'&LIE_NR='.$rsZAL->FeldInhalt('ZAL_LIE_NR');
		$Form->Erstelle_TextFeld('#ZSZ_SORTIMENT',($rsZSZ->FeldInhalt('ZSZ_SORTIMENT')==''?$AWISSprachKonserven['Wort']['KeineZuordnungGefunden']:$rsZSZ->FeldInhalt('ZSZ_SORTIMENT')),10,300,false,'','',$Link);
		$Form->ZeileEnde();


			// Sonstige Daten
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_MENGENEINHEIT'].':',200);
		$Form->Erstelle_TextFeld('ZAL_MENGENEINHEIT',$rsZAL->FeldInhalt('ZAL_MENGENEINHEIT'),10,100,$EditModus,'','','','T','L','','St');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_EINHEITENPROVK'].':',170);
		$Form->Erstelle_TextFeld('ZAL_EINHEITENPROVK',$rsZAL->FeldInhalt('ZAL_EINHEITENPROVK'),10,100,$EditModus,'','','','T','L','',1,4);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_MWS_ID'].':',150);
		$SQL = 'SELECT MWS_ID, MWS_LAN_CODE || \' - \' || MWS_BEZEICHNUNG FROM MEHRWERTSTEUERSAETZE';
		$SQL .= ' ORDER BY MWS_LAN_CODE, MWS_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('ZAP_MWS_ID',$rsZAL->FeldInhalt('ZAL_MWS_ID'),360,$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BESTELLNUMMER'].':',200);
		$Form->Erstelle_TextFeld('ZAL_BESTELLNUMMER',$rsZAL->FeldInhalt('ZAL_BESTELLNUMMER'),30,100,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BEWERTUNG'].':',200);
		$Form->Erstelle_TextFeld('ZAL_BEWERTUNG',$rsZAL->FeldInhalt('ZAL_BEWERTUNG'),5,100,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_SPERRE'].':',200);
		$JaNein = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('ZAL_SPERRE',$rsZAL->FeldInhalt('ZAL_SPERRE'),100,$EditModus,'','','0','','',$JaNein);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_SPERRGRUND'].':',170);
		$Form->Erstelle_TextFeld('ZAL_SPERRGRUND',$rsZAL->FeldInhalt('ZAL_SPERRGRUND'),60,500,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BEMERKUNG'].':',200);
		$Form->Erstelle_TextFeld('ZAL_BEMERKUNG',$rsZAL->FeldInhalt('ZAL_BEMERKUNG'),100,600,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_CREADAT'].':',200);
		$Form->Erstelle_TextFeld('#ZAL_CREADAT',$rsZAL->FeldInhalt('ZAL_CREADAT'),60,500,false,'','','','DU');
		$Form->ZeileEnde();

/*			// Preisdaten
		if(($Recht10009&16)==16)
		{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_MWS_ID'].':',170);
		$SQL = 'SELECT MWS_ID, MWS_LAN_CODE || \' - \' || MWS_BEZEICHNUNG FROM MEHRWERTSTEUERSAETZE';
		$SQL .= ' ORDER BY MWS_LAN_CODE, MWS_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('ZAL_MWS_ID',$rsZAL->FeldInhalt('ZAL_MWS_ID'),200,$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_VK'].':',200);
			$Form->Erstelle_TextFeld('ZAL_VK',$rsZAL->FeldInhalt('ZAL_VK'),10,100,$EditModus,'','','','N2');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_EK'].':',170);
			$Form->Erstelle_TextFeld('ZAL_EK',$rsZAL->FeldInhalt('ZAL_EK'),10,100,$EditModus,'','','','N2');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_RABATT1'].':',170);
			$Form->Erstelle_TextFeld('ZAL_RABATT1',$rsZAL->FeldInhalt('ZAL_RABATT1'),10,100,$EditModus,'','','','N2');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_RABATT2'].':',170);
			$Form->Erstelle_TextFeld('ZAL_RABATT2',$rsZAL->FeldInhalt('ZAL_RABATT2'),10,100,$EditModus,'','','','N2');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_PREISDATUM'].':',200);
			$Form->Erstelle_TextFeld('ZAL_PREISDATUM',$rsZAL->FeldInhalt('ZAL_PREISDATUM'),10,100,$EditModus,'','','','D');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_WAEHRUNG'].':',170);
			$Form->Erstelle_TextFeld('ZAL_WAEHRUNG',$rsZAL->FeldInhalt('ZAL_WAEHRUNG'),10,100,$EditModus,'','','','T');
			$Form->ZeileEnde();
		}
*/

		$Form->Formular_Ende();

		if($rsZAL->FeldInhalt('ZAL_KEY')!='')
		{
			$Reg = new awisRegister(10005);
			$Reg->ZeichneRegister();
		}
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}



}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>