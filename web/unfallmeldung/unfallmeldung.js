$(document).ready(function () {
    verkuerzeEingabe();
    $('#txtUNF_VERBANDBUCH').change(function () {
        verkuerzeEingabe();
    });
});

function verkuerzeEingabe() {
    var langeEingabe = true;

    if($('#txtUNF_VERBANDBUCH').val() == 1){
        langeEingabe = false;
    }

    if(langeEingabe == true){
        $('.richtigerUnfall').css('display','inline');

    }else{
        $('.richtigerUnfall').css('display','none');
    }

}



var DatumVergangenheit = "nein";

var DatumHeute = new Date();
var DiesesJahr = DatumHeute.getYear();
var DieserMonat = DatumHeute.getMonth() + 1;
var DieserTag = DatumHeute.getDate();
var Fehler = false;


var EingabeDatum = new Date();
var EingabeMonatszahl, AnzahlMonatstage;
var Set = "1234567890.";



// Anzahl der Monatstage ermitteln
function Monatstage(Zahl, Jahr) {
    var TageJeMonat = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    // Schaltjahr ?
    if (Jahr % 4 == 0 && Jahr % 100 != 0 || Jahr % 400 == 0) {
        TageJeMonat[1] = 29;
        //alert("Schaltjahr");
    }
    else
    {
        //alert("Kein Schaltjahr");
    }

    AnzahlMonatstage = TageJeMonat[Zahl];
    return
}

function PruefeDatum(obj,hwt) {

    Eingabe = obj.value;
    Laenge = obj.value.length;
    Fehler = false;
    // Eingabefeld leer?
    if (Eingabe == "") {
        Fehler = true;
        //alert("leer")
    }

    // Eingabeformat OK ? (10 Zeichen, Trennzeichen ein Punkt)
    if (Laenge == 10 && Eingabe.substring(2, 3) == "." && Eingabe.substring(5, 6) == ".") {
        //alert("Format OK");
    } else {
        Fehler = true;
        //alert("Format Falsch");
    }

    // Zahlen? ::::::::::::::
    if (Laenge > 0) {
        for (i = 0; i <= Laenge; i++) {
            zeichen = Eingabe.charAt(i);
            if (Set.indexOf(zeichen) != -1) {
                //alert("Bis auf die Punkte alles Ziffern - OK")
            } else {
                Fehler = true;
                //alert("Eingetragener Datumswert ist ung�ltig! \nerlaubte Zeichen: Ziffern + Punkte");
            }
        }
    }

    EingabeTag = parseInt(Eingabe.substring(0, 2), 10);
    EingabeMonat = parseInt(Eingabe.substring(3, 5), 10);
    EingabeJahr = parseInt(Eingabe.substring(6, 10), 10);


    // g�ltige Monats und Jahreswerte?
    if (EingabeMonat >= 1 && EingabeMonat <= 12 && EingabeJahr >= 1900 && EingabeJahr <= 9999) {
        Monatstage(EingabeMonat - 1, EingabeJahr)
        //alert("Monats- und Jahreswerte OK")
    } else {
        Fehler = true;
        Falsch = "Fehler:";
        if (EingabeMonat < 1 || EingabeMonat > 12) Falsch = Falsch + " KalenderMonat";
        if (EingabeJahr < 1900 || EingabeJahr > 9999) Falsch = Falsch + " KalenderJahr";
        //alert("Eingetragene Datumswerte sind ung�ltig! \n" + Falsch);
        Fehler = true;
    }


    // g�ltiger Tageswert?
    if (EingabeTag >= 1 && EingabeTag <= AnzahlMonatstage) {
        EingabeDatum = new Date(EingabeJahr, EingabeMonat - 1, EingabeTag);
        // alert("Tageswert OK");
    } else {
        Falsch = "Fehler:";
        if (EingabeTag < 1 || EingabeTag > AnzahlMonatstage) Falsch = Falsch + " Kalendertag";
        //alert("Eingetragene Datumswerte sind ung�ltig! \n" + Falsch);
        Fehler = true;
    }

    if (Fehler == true) {
        document.getElementById(hwt).style.visibility = "visible";
    }
    else
    {
        document.getElementById(hwt).style.visibility = "hidden";
    }

}//Function ende


function PflichtEinblenden(obj,einBlendOption,BlendDiv) {
    //Param einBlendOption = Welche Option muss ausgew�hlt sein, um das Div einzublenden
    var checkWert = obj.options[obj.selectedIndex].value;
    // alert(checkWert);
    if(checkWert == einBlendOption) //Dann div einblenden
    {
        //alert("0"); //ja
        document.getElementById(BlendDiv).style.display = "inline";
    }
    else //ausblenden
    {
        document.getElementById(BlendDiv).style.display = "none";
    }


}//Function ende

function resetSelectFeld(ResetID)
{
    //document.getElementsByName(ResetID).selectedIndex = null;
    document.frmUnfallDetails.txtUNF_URSACHESPEZKEY.selectedIndex = 0;
}

