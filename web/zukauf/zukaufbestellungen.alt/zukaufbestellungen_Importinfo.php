<?php
global $con;
//global $Recht10003;
global $Recht10007;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZUB','ttt_ZUBSTATUS');
$TextKonserven[]=array('ZUB','ZUB_WANR');
$TextKonserven[]=array('ZUB','LetzterDatensatz%');
$TextKonserven[]=array('ZIL','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_ZIL_TYP');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('XJO','XJO_%');
$TextKonserven[]=array('Wort','DatumVom');
$TextKonserven[]=array('Wort','DatumBis');
$TextKonserven[]=array('Wort','HotlineInfo_PV');
$TextKonserven[]=array('Wort','HotlineInfo_TROST');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10007 = awisBenutzerRecht($con,10007,$AWISBenutzer->BenutzerName());
if(($Recht10007&1)==0)
{
    awisEreignis(3,1000,'ZLI',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

if(awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'")===false)
{
	echo '####';
}

$SQL = 'SELECT *';
$SQL .= ' FROM jobliste';
$SQL .= ' WHERE xjo_key IN (1,2,3)';
$SQL .= ' ORDER BY XJO_KEY';

$rsXJO = awisOpenRecordset($con, $SQL);
$rsXJOZeilen = $awisRSZeilen;


awis_FORM_FormularStart();

awis_FORM_ZeileStart();
$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_JOBBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_JOBBEZEICHNUNG'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_JOBBEZEICHNUNG'],300,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_LETZTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTERSTART'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTERSTART'],160,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_LETZTESENDE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTESENDE'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTESENDE'],160,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_NAECHSTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_NAECHSTERSTART'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_NAECHSTERSTART'],160,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_INTERVALL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_INTERVALL'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_INTERVALL'],80,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_LETZTERSTATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTERSTATUS'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTERSTATUS'],140,'',$Link);

$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
$Link .= '&Sort=XJO_LETZTEMELDUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTEMELDUNG'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTEMELDUNG'],150,'',$Link);

awis_FORM_ZeileEnde();


for($XJOZeile=0;$XJOZeile<$rsXJOZeilen;$XJOZeile++)
{
	$Status = '';
	awis_FORM_ZeileStart();

	$Link = '';
	awis_FORM_Erstelle_ListenFeld('XJO_JOBBEZEICHNUNG',$rsXJO['XJO_JOBBEZEICHNUNG'][$XJOZeile],0,300,false,($XJOZeile%2),'',$Link,'T');
	awis_FORM_Erstelle_ListenFeld('XJO_LETZTERSTART',$rsXJO['XJO_LETZTERSTART'][$XJOZeile],0,160,false,($XJOZeile%2),'',$Link,'DU');
	awis_FORM_Erstelle_ListenFeld('XJO_LETZTESENDE',$rsXJO['XJO_LETZTESENDE'][$XJOZeile],0,160,false,($XJOZeile%2),'',$Link,'DU');
	awis_FORM_Erstelle_ListenFeld('XJO_NAECHSTERSTART',$rsXJO['XJO_NAECHSTERSTART'][$XJOZeile],0,160,false,($XJOZeile%2),'',$Link,'DU');
	awis_FORM_Erstelle_ListenFeld('XJO_INTERVALL',$rsXJO['XJO_INTERVALL'][$XJOZeile],0,80,false,($XJOZeile%2),'',$Link,'Z');
	awis_FORM_Erstelle_ListenFeld('XJO_LETZTERSTATUS',$rsXJO['XJO_LETZTERSTATUS'][$XJOZeile],0,140,false,($XJOZeile%2),'',$Link,'Z');
	awis_FORM_Erstelle_ListenFeld('XJO_LETZTEMELDUNG',$rsXJO['XJO_LETZTEMELDUNG'][$XJOZeile],0,150,false,($XJOZeile%2),'',$Link,'Z');
	
	awis_FORM_ZeileEnde();
}

awis_FORM_Trennzeile();

$SQL = 'SELECT MAX(ZUB_BESTELLDATUM) AS DATUM';
$SQL .= ' FROM Zukaufbestellungen';
$SQL .= ' WHERE SUBSTR(ZUB_EXTERNEID,1,1)=\'P\'';
$rsInfo = awisOpenRecordset($con, $SQL);
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['LetzterDatensatzPV'].':',250);
awis_FORM_Erstelle_TextFeld('#PV',$rsInfo['DATUM'][0],18,180,false,'','','','DU');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['HotlineInfo_PV'].'',0);
awis_FORM_ZeileEnde();

$SQL = 'SELECT MAX(ZUB_BESTELLDATUM) AS DATUM';
$SQL .= ' FROM Zukaufbestellungen';
$SQL .= ' WHERE SUBSTR(ZUB_EXTERNEID,1,1)=\'T\'';
$rsInfo = awisOpenRecordset($con, $SQL);
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['LetzterDatensatzTROST'].':',250);
awis_FORM_Erstelle_TextFeld('#TROST',$rsInfo['DATUM'][0],18,180,false,'','','','DU');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['HotlineInfo_TROST'].'',0);
awis_FORM_ZeileEnde();



	
awis_FORM_Trennzeile();	

echo '<form name=frmLog action="./zukaufbestellungen_Main.php?cmdAktion=Importinfo" method=post>';


//*********************************************************************************************
// Protokoll anzeigen
//*********************************************************************************************

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Ueberschrift($AWISSprachKonserven['ZIL']['ZukaufImportLog']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',150);
awis_FORM_Erstelle_TextFeld('*DatumVom',(isset($_POST['sucDatumVom'])?$_POST['sucDatumVom']:date('d.m.Y')),18,160,true,'','','','DU');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',150);
awis_FORM_Erstelle_TextFeld('*DatumBis',(isset($_POST['sucDatumBis'])?$_POST['sucDatumBis']:date('d.m.Y H:i')),18,160,true,'','','','DU');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZIL']['ZIL_TYP'].':',150);
$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_ZIL_TYP']);
awis_FORM_Erstelle_SelectFeld('*ZIL_TYP',(isset($_POST['sucZIL_TYP'])?$_POST['sucZIL_TYP']:''),200,true,null,'','','',1,'',$Daten);

awis_FORM_ZeileEnde();

awis_FORM_SchaltflaechenStart();
awis_FORM_Schaltflaeche('image', 'cmdFilter', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
awis_FORM_SchaltflaechenEnde();

awis_FORM_Trennzeile();	

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
if(awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'")===false)
{
	echo '####';
}

$SQL = 'SELECT ZukaufImportLog.*, ZUB_KEY, ZUB_WANR';
$SQL .= ' FROM ZukaufImportLog';
$SQL .= ' INNER JOIN Zukaufbestellungen ON ZIL_EXTERNEID = ZUB_EXTERNEID';

$Bedingung = '';
if(isset($_POST['sucDatumVom']))
{
	$StartZeit = awis_PruefeDatum($_POST['sucDatumVom'],true);
	$Bedingung .= ' AND ZIL_ZEITPUNKT >= '.awis_FeldInhaltFormat('DU',$StartZeit);
}
else 
{
	$StartZeit = date('d.m.Y');
	$Bedingung .= ' AND ZIL_ZEITPUNKT >= '.awis_FeldInhaltFormat('DU',$StartZeit);
}

if(isset($_POST['sucDatumBis']))
{
	$EndeZeit = awis_PruefeDatum($_POST['sucDatumBis'],true);
	$Bedingung .= ' AND ZIL_ZEITPUNKT <= '.awis_FeldInhaltFormat('DU',$EndeZeit);
}

if(isset($_POST['sucZIL_TYP']) AND $_POST['sucZIL_TYP']!='')
{
	$Bedingung .= ' AND ZIL_TYP = '.awis_FeldInhaltFormat('T',$_POST['sucZIL_TYP']);
}

if($Bedingung != '')
{
	$SQL .= ' WHERE '. substr($Bedingung,4);
}
$SQL .= ' ORDER BY ZIL_ZEITPUNKT DESC';

$rsZIL = awisOpenRecordset($con, $SQL);
$rsZILZeilen = $awisRSZeilen;

	// Blockweise
$StartZeile=0;
if(isset($_GET['Block']))
{
	$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
}

	// Seitenweises bl�ttern
if($rsZILZeilen>$MaxDSAnzahl)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

	for($i=0;$i<($rsZILZeilen/$MaxDSAnzahl);$i++)
	{
		if($i!=($StartZeile/$MaxDSAnzahl))
		{
			$Text = '&nbsp;<a href=./zukaufbestellungen_Main.php?cmdAktion=Importinfo&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
		else
		{
			$Text = '&nbsp;'.($i+1).'';
			awis_FORM_Erstelle_TextLabel($Text,30,'');
		}
	}
	awis_FORM_ZeileEnde();
}

awis_FORM_ZeileStart();

$Link = '';
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_ZEITPUNKT'],160,'',$Link);
$Link = '';
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_TYP'],60,'',$Link);
$Link = '';
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_MELDUNG'],600,'',$Link);
$Link = '';
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_WANR'],160,'',$Link);
$Link = '';
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_BEARBEITUNGSSTATUS'],100,'',$Link);

awis_FORM_ZeileEnde();

for($ZILZeile=$StartZeile;$ZILZeile<$rsZILZeilen and $ZILZeile<$StartZeile+$MaxDSAnzahl;$ZILZeile++)
{
	awis_FORM_ZeileStart();

	awis_FORM_Erstelle_ListenFeld('ZIL_ZEITPUNKT',$rsZIL['ZIL_ZEITPUNKT'][$ZILZeile],10,160,false,($ZILZeile%2),'','','DU');
	awis_FORM_Erstelle_ListenFeld('ZIL_TYP',$rsZIL['ZIL_TYP'][$ZILZeile],10,60,false,($ZILZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('ZIL_MELDUNG',$rsZIL['ZIL_MELDUNG'][$ZILZeile],10,600,false,($ZILZeile%2),'','','T');
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZIL['ZUB_KEY'][$ZILZeile];
	awis_FORM_Erstelle_ListenFeld('ZUB_WANR',$rsZIL['ZUB_WANR'][$ZILZeile],10,160,false,($ZILZeile%2),'',$Link,'T');
	awis_FORM_Erstelle_ListenFeld('ZIL_BEARBEITUNGSSTATUS',$rsZIL['ZIL_BEARBEITUNGSSTATUS'][$ZILZeile],10,100,false,($ZILZeile%2),'','','T');
	
	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();
echo '</form>'; 

?>