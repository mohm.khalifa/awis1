<?php
/**
 * Suchmaske f�r eurechnungen
 *
 * @author    Patrick Gebhardt
 * @copyright ATU
 * @version   201110
 *
 *
 */
global $Funktionen;
global $AWISCursorPosition;

try {

    if ($Funktionen->Recht == 0) {
        $Funktionen->Form->Formular_Start();
        $Funktionen->Form->Formular_Ende();
        die();
    }

    $Funktionen->Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./grosskundenkarten_Main.php?cmdAktion=Zuordnung>");

    /**********************************************
     * * Eingabemaske
     ***********************************************/

    $Param = unserialize($Funktionen->AWISBenutzer->ParameterLesen('Formular_ERM'));

    if (!isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
    }

    $Funktionen->Form->Formular_Start();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERZ_KUNDEN_NR'] . ':', 190);
    $Funktionen->Form->Erstelle_TextFeld('*ERZ_KUNDEN_NR', (isset($Param['ERZ_KUNDEN_NR']) && $Param['SPEICHERN'] == 'on'?$Param['ERZ_KUNDEN_NR']:''), 15, 130, true, '', '', '', 'T');
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERM_NAME'] . ':', 190);
    $Funktionen->Form->Erstelle_TextFeld('*ERM_NAME', (isset($Param['ERM_NAME']) && $Param['SPEICHERN'] == 'on'?$Param['ERM_NAME']:''), 50, 130, true, '', '', '', 'T');
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['LBL_LAENDER'], 190);
    $Funktionen->Form->Erstelle_SelectFeld('*LAND', (isset($Param['LAND']) && $Param['SPEICHERN'] == 'on'?$Param['LAND']:''), 130, true, '', '', '', '', '', explode('|', $Funktionen->AWISSprachkonserven['ERM']['LST_LAENDER']));
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['LBL_KNDART'], 190);
    $Funktionen->Form->Erstelle_SelectFeld('*ART', (isset($Param['ART']) && $Param['SPEICHERN'] == 'on'?$Param['ART']:''), 130, true, '', '', '', '', '', explode('|', $Funktionen->AWISSprachkonserven['ERM']['LST_KNDART']));
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERM_DRUCKSTATUS'], 190);
    $Funktionen->Form->Erstelle_SelectFeld('*ERM_DRUCKSTATUS', (isset($Param['ART']) && $Param['SPEICHERN'] == 'on'?$Param['ERM_DRUCKSTATUS']:''), 130, true, '', '', '1', '', '', explode('|', $Funktionen->AWISSprachkonserven['ERM']['ERM_LST_DRUCKSTATUS']));
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['Wort']['AuswahlSpeichern'] . ':', 190);
    $Funktionen->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 30, true, 'on', '', $Funktionen->AWISSprachkonserven['Wort']['ttt_AuswahlSpeichern']);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Funktionen->Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');
    $Funktionen->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_suche'], 'W');

    if (($Funktionen->Recht & 2) == 2 and grosskundenkarten_funktionen::RABATTMODELLE_AKTIV)        // Hinzuf�gen erlaubt?
    {
        $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Funktionen->Form->SchaltflaechenEnde();

    if ($AWISCursorPosition != '') {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";
        echo '</Script>';
    }
} catch (awisException $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>