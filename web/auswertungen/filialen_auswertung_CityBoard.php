<?php
// Variablen
global $_POST;
global $_GET;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);

print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=Cityboard>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	$SQL = 'SELECT FIL_ID, FIL_BEZ, FIL_STRASSE, FIF_WERT FROM Filialen, FilialInfos ';
	$SQL .= ' WHERE FIL_ID = FIF_FIL_ID AND (FIF_FIT_ID IN (119) AND FIF_WERT > 0)';
	$SQL .= ' ORDER BY FIL_ID';
	
	$rsAusstattung = awisOpenRecordset($con, $SQL);
	$rsAusstattungZeilen = $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );
	
	fputs($fd, "Filiale;Bezeichnung;Strasse;Cityboard\n");
	for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
	{
		fputs($fd, $rsAusstattung['FIL_ID'][$DSNr] . ';' 
			. $rsAusstattung['FIL_BEZ'][$DSNr] . ';' 	
			. $rsAusstattung['FIL_STRASSE'][$DSNr] . ';' 	
			. awis_format($rsAusstattung['FIF_WERT'][$DSNr],'Standardzahl') . "\n");
	}
	fclose($fd);
	
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Alle Filialen mit Cityboard-Ausstattung gr��er als 0.</li></ul>';
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

