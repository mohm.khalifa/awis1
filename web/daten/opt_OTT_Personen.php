<?php
//**************************************************************
// Liefert die Kontakte, die eine Durchwahl haben
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisOT.inc');

if(!isset($_GET['KEY']))
{
	die('##PARAM##');
}
try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $BEN = awisBenutzer::Init();

    if(strlen($_GET['FILTER'])<3)
    {
    	$Form = new awisFormular();
    	$Fehler = $Form->LadeTextBaustein('Fehler', 'err_ZuWenigZeichenFilter',$BEN->BenutzerSprache());
    	$Fehler = str_replace('$1', 3, $Fehler);
    	
    	echo '#~#';		// Trennzeichen zum Kennzeichen der Daten
    	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
		{
        	echo '<option value="">'.$Fehler.'</option>';
		}
        else
        {
        	echo '#+##+#'.$Fehler.'';
		}
    	die();
    }
    
    $OT = new awisOT($BEN);
    $Daten = $OT->ObjektListe('Stammdaten\Personen','*AktivePersonen#StringVal~LastName~'.$_GET['FILTER'],array('Nachname','Telefon','Title'),999);

    if(isset($_GET['WERT']))
    {
    	echo $_GET['WERT'];
    }

    $DS=0;
    if(isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ']!='')
    {
    	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
    	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
    	{
    		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
    	}
    	else
    	{
    		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
    	}
    }


    echo '#~#';		// Trennzeichen zum Kennzeichen der Daten
    $DS=0;
    
    if(empty($Daten))
    {
    	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
    	{
    		echo '<option value="">::--::</option>';
    	}
    	else
    	{
    		echo '#+##+#::--::';
    	}
    }
    
    foreach($Daten AS $Person)
    {
        if(++$DS>200)
    	{
    		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
        	{
        		echo '<option value="">...</option>';
        	}
        	else
        	{
        		echo '#+##+#...';
        	}
        	break;
        }

        if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
    	{
    		echo '<option value="'.$Person['id'].'~'.$Person['Title'].'">'.$Person['LastName'].' '.$Person['Phone'].'</option>';
    	}
    	else
    	{
    		echo '#+#'.$Person['id'].'~'.$Person['Title'].'#+#'.$Person['LastName'].' '.$Person['Phone'].'';
    	}
    }
}
catch(Exception $ex)
{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
    	{
    		echo '<option value="0">::Zugriffsfehler::</option>';
    	}
    	else
    	{
    		echo '#+#0#+#::Zugriffsfehler::';
    	}
}
?>