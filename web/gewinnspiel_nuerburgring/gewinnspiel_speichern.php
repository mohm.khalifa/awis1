<?php
try {
    $TextKonserven = array();
    $TextKonserven[] = array('GWN', '*');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'Personalnummer');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $SQL = 'SELECT count(*) AS ANZAHL FROM GEWINNSPIELNUERBURGRING WHERE GWN_PERSNR = ' . $DB->WertSetzen('GWN', 'T', $_POST['sucGWN_PERSNR']);
    $rsCheck = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('GWN', true));

    $Form->ZeileStart();
    if ($rsCheck->FeldInhalt('ANZAHL') > 0) {
        $Form->Hinweistext($AWISSprachKonserven['GWN']['GWN_BEREITSTEILGENOMMEN'], awisFormular::HINWEISTEXT_HINWEIS);
    } elseif ($_POST['txtGWN_ANTWORT'] == $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] or $_POST['txtGWN_SHIRTGROESSE'] == $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Form->Hinweistext($AWISSprachKonserven['GWN']['GWN_LEEREFELDER'], awisFormular::HINWEISTEXT_WARNUNG);
    } else {
        $SQL = 'INSERT INTO GEWINNSPIELNUERBURGRING (GWN_ANTWORT, GWN_NAME, GWN_VORNAME, GWN_PERSNR, GWN_HERKUNFT, GWN_SHIRTGROESSE, GWN_TELNR, GWN_MAIL, GWN_USER, GWN_USERDAT) VALUES (';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_ANTWORT']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_NAME']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_VORNAME']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'Z', $_POST['sucGWN_PERSNR']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_HERKUNFT']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_SHIRTGROESSE']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_TELNR']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $_POST['txtGWN_MAIL']) . ', ';
        $SQL .= $DB->WertSetzen('GWN', 'T', $AWISBenutzer->BenutzerName()) . ', ';
        $SQL .= 'sysdate)';

        $DB->Ausfuehren($SQL, 'GWN', true, $DB->Bindevariablen('GWN', true));

        $Form->Hinweistext($AWISSprachKonserven['GWN']['GWN_ERFOLGREICHTEILGENOMMEN'], awisFormular::HINWEISTEXT_OK);
    }
    $Form->ZeileEnde();
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}

?>