<?php
require_once('/daten/include/awisZukauf_WUETSCHNER.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
	//$Job = new awisJobProtokoll(6, 'Starte Wuetschner - Import.');
/*
	$ZUK = new awisZukauf_WUETSCHNER((isset($argv[1])?(int)$argv[1]:0),0);			// Abfruf LIVE - ALT

        $Job->SchreibeLog(3,1,'Suche offene Bestellungen...');

	if($ZUK->LeseBestellungen('','01.11.2010',date('d.m.Y'),'','')>0)
	//if($ZUK->LeseBestellungen('','20.05.2009',date('d.m.Y'),'','')>0)
	{
        	$Job->SchreibeLog(3,1,'Speichere Bestellungen...');
		$ZUK->SpeichereBestellungen();
	}

*/
	
	$AWISUser = awisBenutzer::init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

	
	$ZUK = new awisZukauf_WUETSCHNER((isset($argv[1])?(int)$argv[1]:0),2);			// Abruf LIVE - NEU

  	$rsZUB = $DB->RecordSetOeffnen('SELECT MAX(ZUB_BESTELLDATUM) AS LETZTER FROM Zukaufbestellungen WHERE ZUB_LIE_NR = \'8164\'');
	// Datum zurueck gehen, um Nachzuegler zu bekommen
   	$Letzter = $Form->PruefeDatum($rsZUB->FeldInhalt('LETZTER'),0,0,1)-(60*60*28*2);

        //$Job->SchreibeLog(3,1,'Suche offene Bestellungen auf dem neuen Server...');

	//if($ZUK->LeseBestellungen('','13.02.2015','15.02.2015','','')>0)
	if($ZUK->LeseBestellungen('',date('d.m.Y',$Letzter),date('d.m.Y'),'','')>0)
	//if($ZUK->LeseBestellungen('','22.04.2013',date('d.m.Y'),'','')>0)
	{
        	//$Job->SchreibeLog(3,1,'Speichere Bestellungen...');
		$ZUK->SpeichereBestellungen();
	}

        //$Job->SchreibeLog(1,0,'Ende des Jobs.');
}
catch (Exception $ex)
{
        echo $ex->getMessage().PHP_EOL;
	//if(is_object($Job))
	{
	        //$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
	}
	if(trim($ex->getMessage())=='Gateway Error')
	{
		if(awisWerkzeuge::EMail(array('atufehler@wuetschner.com','zukauf@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Bestellschnittstelle','Der Server ist nicht erreichbar. Fehlermeldung war: Gateway Error.',3)===false)
		{
			echo '  --> Fehler bei EXTERN E-Mail';
		}
	}
	elseif(trim($ex->getMessage())=='Error Fetching http headers')
	{
		if(awisWerkzeuge::EMail(array('atufehler@wuetschner.com','zukauf@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Bestellschnittstelle','Der Server ist nicht erreichbar. Fehlermeldung war: Error Fetching http headers.',3)===false)
		{
			echo '  --> Fehler bei EXTERN E-Mail';
		}
	}

/*
		if(awisWerkzeuge::EMail('shuttle@de.atu.eu','Wuetschner-Bestellschnittstelle','Der Server ist nicht erreichbar',3,'shuttle@de.atu.eu'))
		{
			echo '  --> per E-Mail benachrichtigt.'.PHP_EOL;
		}
		else
		{
			echo '  --> FEHLER beim E-Mail Versand.'.PHP_EOL;
		}
*/
}
?>
