<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

$intBestAend=$intMahngebuehrenHauptBestDsAendern;		//Recht wird in card_rechte.inc.php gesetzt

if(isset($intMag_id))	//Wenn Neuanlage/�nderung
{
	if($intMag_id=='')	//Neuanlage
	{
		$strSqlSeq = "select seq_mag_id.nextval seq from dual";
		$arrSeq = abfrage("ora",$strSqlSeq);
		$intSeq = $arrSeq["trefferarray"]["SEQ"][0];
		
		$strSql="insert into mahngebuehren 
		(mag_id,mag_kunden_nr,mag_betrag,mag_datum,mag_text,mag_abrzeitraum,mag_aend_benutzer,mag_aend_datum,mag_loesch,
		mag_rnr,mag_kontoauszugid)
		values($intSeq,'$strKundennr',$dblMag_betrag,'$datMag_datum',
		$intMag_text,'$strAbrDatum','$strMag_Aend_Benutzer',sysdate,0,'$strMag_rnr',$intKazId)"; 
		abfrage("ora",$strSql);
		
		$strBlock = "id=".$intSeq;
		unset($intZaehler);
	}
	else				//�nderung	bzw. L�schen
	{
		$strSql="update mahngebuehren set ".
		(isset($dblMag_betrag)?"mag_betrag=".$dblMag_betrag.",":"").
		(isset($datMag_datum)?"mag_datum='".$datMag_datum."',":"").
		(isset($intMag_text)?"mag_text=".$intMag_text.",":"").
		"mag_aend_benutzer='".$strMag_Aend_Benutzer."',mag_aend_datum=sysdate";
		if($intMag_loesch)
		{
			$strSql.=",mag_loesch=".$intMag_loesch;
		}
		else
		{
			$strBlock = "id=".$intMag_id;
			unset($intZaehler);
		}
		
		$strSql.=" where mag_id=".$intMag_id;
		
		abfrage("ora",$strSql);
	}
}

//Firma ermitteln

$strSql='select 
		case when "Firma" is null then
			case when "Vorname" is null then
				"Name"
			else
				"Name" || \' \' || "Vorname"
			end
		else
			case when "Abteilung" is null then
				"Firma"
			else
				"Firma" || \' \' || "Abteilung"
			end
		end || \', \' || "Ort" as "Firma"
		from '.$tabAdressen.' where "Kunden_Nr"=\''.$strKundennr.'\'
		and "Adress_Art"=\'01\'';

$arrAdressen=abfrage("db2",$strSql);

if($arrAdressen["zeilen"])
	$strFirma=$arrAdressen["trefferarray"]["Firma"][0];

//Saldo Mahngeb�hren berechnen;

$strSql="select sum(nvl(a.mag_betrag,0)) betrag
from mahngebuehren a
where a.mag_kunden_nr='".$strKundennr."' and a.mag_abrzeitraum='".$strAbrDatum."' 
and a.mag_loesch=0";	
	
$arrSaldo=abfrage("ora",$strSql);

if($arrSaldo["zeilen"])
{
	$dblSaldoMahngeb = $arrSaldo["trefferarray"]["BETRAG"][0];
}
else
{
	$dblSaldoMahngeb = 0;
}

//Daten f�r Listenfeld

$strSql="select * from mahngebuehren_text order by mat_text";
$arrMahngText=abfrage("ora",$strSql);

$strSql="select * from mahngebuehren_text where mat_id>=3 order by mat_text";		//ohne Mahngeb�hr 1, Mahngeb�hr 2
$arrMahngTextNeu=abfrage("ora",$strSql);

?>
<html>
<head>
<title>Mahngeb&uuml;hren</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>





<?php

//Bestehende Datens�tze ermitteln

$strDb = "ora";

$strSql="select a.mag_id id, a.mag_kunden_nr kundennr, a.mag_betrag betrag,
a.mag_datum datum, a.mag_text mag_text,a.mag_rnr rnr,
a.mag_kontoauszugid kaz,b.mat_id
from mahngebuehren a,
mahngebuehren_text b 
where a.mag_kunden_nr='".$strKundennr."' and a.mag_abrzeitraum='".$strAbrDatum."' 
and a.mag_loesch=0 and a.mag_text=b.mat_id order by datum desc,id desc";

include("card/card_blaettern_haupt.inc.php");

$arrMahngebuehren=abfrage("ora",$strSqlScroll);

$arr = $arrMahngebuehren["trefferarray"];

?>

<script language="JavaScript">

<?php
if($intZeitmessung)
{
	echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>

function init()		//Aufruf �ber <body onload="init();">
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
	document.ondblclick = zoom;
	
	<?php	if($intSeq&&$intOhneKaz)		//Wenn neuer Datensatz vorerst ohne Kontoauszug angelegt werden soll
			{	?>
				document.getElementById("kaz"+<?php echo $intSeq;?>).click();		//Dadurch wird Fenster mahngebuehren ge�ffnet
	<?php	} ?>
}

function aenderungzeilerest()  //Wird aus Funktion aenderungzeile() in card_allgmeinefunktionen.js aufgerufen
{
	document.getElementById('neu').setAttribute("onclick","");		//Funktion onclick auf Button neuer Dastensatz ausschalten
	
	document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';		//Speicher-Button wird sichtbar
}


function send()
{
	self.location.href="framesetmahngebuehren.php";
}

function aendsend(parameter)
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Mahngeb�hren �ndern");	//Zeitmessung
	<?php } ?>
		
	var obj = window.event.srcElement;
	
	var buchid = obj.id.slice(3);
		
	var betobj = document.getElementById("bet"+buchid);
	var datobj = document.getElementById("dat"+buchid);
	var matobj = document.getElementById("mat"+buchid);
	var rnrobj = document.getElementById("rnr"+buchid);
	var kazobj = document.getElementById("kaz"+buchid);
		
	//Datumspr�fung
	if(!datumspruefung(datobj))
	{
		return;
	}
	
	//Betragspr�fung
	if(!betragspruef(betobj,8))
		return;
	
	//Pr�fung ob Auswahl in Liste Buchungsart erfolgt ist
	
	var buchungsart = listeneintrag(matobj,"value");
	
	if(buchungsart==-1)
	{
		alert(unescape("W%E4hlen Sie bitte einen Eintrag aus der Liste \"Text Mahngeb%FChren\"!"));
		matobj.focus();
		return;
	}
	
	//Pr�fung ob Rechnungs-Nr. eingetragen worden ist
	
	var rnr = rnrobj.value;
	
	//23.12.04 MB: Rechnungsnummer kann nicht ver�ndert werden, somit ist Pr�fung �berfl�ssig
	
	/*if(!rnrpruef(rnr))
	{
		alert(unescape("Geben Sie bitte eine g%FCltige Rechnungsnummer an!"));
		rnrobj.select();
		return;
	}*/
	
	//Pr�fung ob neue Rechnungs-Nr. mit einer der bestehenden Rechnungsnummern �bereinstimmt
	
	var obj=document.getElementsByName("grau");
	var schalter = true;
	
	for(i=0;i<obj.length&&schalter;i++)
	{
		if(obj[i].id.substr(0,3)=='rnr'&& rnr == document.getElementById(obj[i].id).value)
		{
			schalter = false;
		}
	}
	if(schalter)
	{
		alert("Geben Sie bitte eine Rechnungsnummer an,\nwelche in diesem Abrechnungszeitraum\nbereits verwendet worden ist");
		rnrobj.select();
		return;
	}
		
	//Pr�fung ob g�ltige Zahl in Feld Kontoauszug steht
	
	if(!intpruef(kazobj.value))
	{
		alert(unescape("Geben Sie bitte eine g%FCltige Kontoauszugs-Id an!"));
		kazobj.select();
		return;
	}
	
	var mag_betrag=datenbankbetrag(betobj.value);
	var mag_datum=datobj.value;
	var mag_text = listeneintrag(matobj,"value");
	var mag_rnr = rnrobj.value;
	var mag_kaz = kazobj.value;
	
	var weitereParam = "";
	
	if(parameter=="ohneKaz")		//Wenn Kontoauszug vorerst nicht eingetragen werden soll
	{
		var weitereParam = "&intOhneKaz=1";
	}
	
	location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&strMag_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&intMag_id="+buchid
	+"&dblMag_betrag="+mag_betrag+"&datMag_datum="+mag_datum+"&intMag_text="+mag_text+"&strMag_rnr="+mag_rnr+"&intKazId="+mag_kaz+weitereParam;
}

//Wird bei Klick auf Rechnungs-Nr. �bertragen aufgerufen

function uebertragen(strRnr)
{
	try{
		document.getElementById("rnr").value=strRnr;
		document.getElementById("bet").focus();		//Fokus auf Betragsfeld setzen
	} catch(e){}
}


//Wird aufgerufen wenn neuer Datensatz angef�gt werden soll

function neu()
{
	<?php
	if($intBestAend)
		echo "aenderungzeile('aenderungzeilerest');";	//nicht betroffene Felder ausgrauen
	else
		echo "document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';";		//Speicher-Button wird sichtbar
	?>
	
	//Link in Feld Rnr deaktivieren
	var obj=document.getElementsByName("grau");
	for(i=0;i<obj.length;i++)
	{
		if(obj[i].id.slice(3)!=window.event.srcElement.id.slice(3)&&(obj[i].id.substr(0,3)=='rnr'||obj[i].id.substr(0,3)=='kaz'))
		{
			obj[i].style.cursor='auto';
			obj[i].setAttribute('onclick',"");
		}		
	}
	
	//Button Rechnungs-Nr. �bertragen einblenden
	
	obj=document.getElementsByName("uebertrag");
		
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.display = "inline";
	}
	
	window.event.srcElement.style.display = "none";		//Button neu ausblenden
	
	document.getElementById("dat").style.display = "inline";
	
	var obj = document.getElementsByName("neuzeile");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.visibility = "visible";
	}
	
	document.getElementById("bet").focus();		//Fokus auf Betragsfeld setzen
}

function loeschen()
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Mahngeb�hren l�schen");	//Zeitmessung
	<?php } ?>

	var obj = window.event.srcElement;
	
	var buchid = obj.id.slice(3);
	
	var betobj = document.getElementById("bet"+buchid).value;
	var datobj = document.getElementById("dat"+buchid).value;
	var matobj = listeneintrag(document.getElementById("mat"+buchid));
	var rnrobj = document.getElementById("rnr"+buchid).value;
	
	var rueck = confirm(unescape("Soll der Datensatz\n\nDatum: "+datobj+"\nBetrag: "+betobj+"\nText: "+matobj+"\nRechnungs-Nr: "+rnrobj+"\n\nwirklich gel%F6scht werden?"));
	
	if(rueck)
	{
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&strMag_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&intMag_id="+buchid+"&intMag_loesch=1&intZaehler=<?php echo $intZaehler;?>";
	}
}

//Wird bei Klick auf das Feld mit der Rechnungsnr ausgef�hrt, d.h. �bermittelt die Rechnungsnr nach card_haupt.php
	
function auswahlrg(rgnr)
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Rechnungsnr aufgrund Mahngeb�hren");	//Zeitmessung
	<?php } ?>
	parent.window.returnValue = rgnr;
	parent.self.close();
}

//Wird bei Klick auf das Feld Kontoauszug ausgef�hrt

function kontoauszug()
{

	var AktObj = window.event.srcElement;
	var idAktObj = AktObj.id;
	var intKazId = AktObj.value;
	
	if(idAktObj == 'kaz')	//Wenn gerade neuer Datensatz angelegt wird
	{
		aendsend("ohneKaz");
		return;
	}
	
		
	var parameter = "intDialog=2&intKaa_tabelle=2";			// 2 entspricht der Tabelle Mahngeb�hren
		
	if(intKazId == 0)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("�bersicht Kontoausz�ge anzeigen");	//Zeitmessung
		<?php } ?>
				
		<?php echo dialog("kazuebersicht_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			intKazId = rueckgabe;
		}
	}
	else
	{
		parameter += "&intNeuSperre=1";
	}
	
	if(intKazId!=0)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Einzelner Kontoauszug anzeigen");	//Zeitmessung
		<?php } ?>
			
		var intMagId = idAktObj.slice(3);
	
		parameter += "&intKazId="+intKazId+"&intMagId="+intMagId;
		
		<?php echo dialog("kazeinzelsatz_frameset.php"); ?>
		
		aktualisieren();	//Fenster neu einlesen
	}
}

//�bertr�gt bei Doppelklick rnr in Feld des neu anzulegenden Datensatzes

function rnrtransfer()
{
	window.event.cancelBubble = true;
	
	var obj = window.event.srcElement;
	var rnr = obj.value;
	
	document.getElementById("rnr").value = rnr;
}


</script>

</head>
<body onload="init();" body style="overflow:hidden;">
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center" valign="top"><br>
Datens�tze: <span class="hpt_titel"><?php echo $strPosition; ?></span><br><br>
Name: <span class="hpt_titel"><?php echo $strFirma; ?></span><br>
Kundennr: <span class="hpt_titel"><?php echo $strKundennr;?></span><br>
Abrechnungszeitraum: <span class="hpt_titel"><?php echo $strAbrDatum;?></span><br>
Saldo: <span class="hpt_titel"><?php echo eurobetrag($dblSaldoMahngeb); ?></span><br><br>
	<table align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td class="hpt_titel">Datum:</td>
	<td class="hpt_titel">Betrag:</td>
	<td class="hpt_titel">Text Mahngeb&uuml;hren:</td>
	<td class="hpt_titel">Rechnungs-Nr:</td>
	<td class="hpt_titel">Kontoauszug:</td>
	<td class="quadrat">&nbsp;</td>
	</tr>
<?php
	
	$strRnrVergleich = $arr["RNR"][0];
	$intRnrSchalter = 1;

	for($i=0;$i<$arrMahngebuehren["zeilen"];$i++)
	{
		//Pr�fung ob die Rechnungsnummern f�r alle Zeilen identisch sind
		if($intRnrSchalter)
		{
			if($strRnrVergleich != $arr["RNR"][$i])
				$intRnrSchalter = 0;
		}
		
		$intMagId = $arr["ID"][$i];
		echo "<tr>";
		
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"dat".$intMagId.
		"\" class=\"hpt_dat\" maxlength=\"8\" value=\"".$arr["DATUM"][$i]."\"";
		if(!$intBestAend)
			echo " readOnly";
		else
			echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
		echo "></td>";
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"bet".$intMagId.
		"\" class=\"hpt_bet\" maxlength=\"16\" value=\"".eurobetrag($arr["BETRAG"][$i])."\"";
		if(!$intBestAend)
			echo " readOnly";
		else
			echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
		echo "></td><td>";
		if(!$intBestAend)
		{
			echo element(array(
			"type"=>"select",
			"class"=>"hpt_mat",
			"name"=>"grau",
			"id"=>"mat".$intMagId,
			"readOnly"=>"true",
			"optionsarray"=>$arrMahngText["trefferarray"],
			"optionstextfeld"=>"MAT_TEXT",
			"optionswertfeld"=>"MAT_ID",
			"optionsvorauswahlwert"=>$arrMahngebuehren["trefferarray"]["MAG_TEXT"][$i],
			"optionsvorauswahlfeld"=>"MAT_ID"
			));
		}
		else
		{
			echo element(array(
			"type"=>"select",
			"class"=>"hpt_mat",
			"name"=>"grau",
			"id"=>"mat".$intMagId,
			"onchange"=>"aenderungzeile('aenderungzeilerest');",
			"optionsarray"=>$arrMahngText["trefferarray"],
			"optionstextfeld"=>"MAT_TEXT",
			"optionswertfeld"=>"MAT_ID",
			"optionsvorauswahlwert"=>$arrMahngebuehren["trefferarray"]["MAG_TEXT"][$i],
			"optionsvorauswahlfeld"=>"MAT_ID"
			));
		}
		
		echo "</td>";
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"rnr".$intMagId.
		"\" class=\"hpt_rnr\" maxlength=\"18\" value=\"".$arr["RNR"][$i]."\" style=\"cursor: hand;\"
		onclick=\"auswahlrg('".$arr["RNR"][$i]."');\" onDblClick=\"rnrtransfer();\"";
		if(!$intBestAend)
			echo " readOnly";
		else
			echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
		echo "></td>";
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"kaz".$intMagId.
		"\" class=\"hpt_kaz\" value=\"".$arr["KAZ"][$i]."\" style=\"cursor: hand;\"
		onclick=\"kontoauszug();\" readOnly>";
		echo "</td>";
		if(!$intBestAend)
		{
			echo "<td>&nbsp;</td>";
		}
		else
		{
			echo "<td><input type=\"image\" src=\"/bilder/diskette.png\" accesskey=\"S\" class=\"speibut\" id=\"but".$arr["ID"][$i].
			"\" alt=\"speichern (Alt+S)\" onclick=\"aendsend();\">";
			echo "<input type=\"image\" src=\"/bilder/plus.png\" name=\"uebertrag\" accesskey=\"S\" class=\"rnrueberbut\" id=\"rnu".$arr["ID"][$i].
			"\" alt=\"Rechnungs-Nr. &uuml;bertragen (Alt+U)\" onclick=\"uebertragen('".$arr["RNR"][$i]."');\">";
			if($arrMahngebuehren["trefferarray"]["MAT_ID"][$i]!=1 && $arrMahngebuehren["trefferarray"]["MAT_ID"][$i]!=2)	//L�schbutton f�r Mahngeb�hr 1 / Mahngeb�hr 2 ausblenden
			{
				echo "<input type=\"image\" src=\"/bilder/muelleimer.png\" accesskey=\"L\" name=\"loeschen\" class=\"quadrat\" id=\"loe".$arr["ID"][$i].
				"\" alt=\"l&ouml;schen (Alt+L)\" onclick=\"loeschen();\"></td>";
			}
		}
		echo "</tr>";
	}
	
	//selecttabelle("ora",$hdlOraVerbindung,$strSql);
?>
	<tr>
	<td class="text" id="td_dat"><input type="image" src="/bilder/plus.png" id="neu" class="quadrat" alt="neuer Datensatz" onclick="neu();"><input type="text" class="hpt_dat" name="neuzeile" id="dat" maxlength="8" value="<?php echo date("d.m.y"); ?>"></td>
	<td class="text"><input type="text" class="hpt_bet" name="neuzeile" id="bet" maxlength="8"></td>
	<td>
	<?php
	echo element(array(
		"type"=>"select",
		"class"=>"hpt_mat",
		"name"=>"neuzeile",
		"id"=>"mat",
		"optionsarray"=>$arrMahngTextNeu["trefferarray"],
		"optionstextfeld"=>"MAT_TEXT",
		"optionswertfeld"=>"MAT_ID"
		));
	?>
	</td>
	<td class="text"><input type="text" class="hpt_rnr" name="neuzeile" id="rnr" maxlength="18"<?php
	if($intRnrSchalter)
	echo " value=\"".$strRnrVergleich."\" readonly";
	?>></td>
	<td class="text"><input type="text" class="hpt_kaz" name="neuzeile" id="kaz" maxlength="10" value="0" 
	onclick="kontoauszug();" style="cursor:hand;" readOnly></td>
	<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="speibut" name="neuzeile" id="but" alt="speichern (Alt+S)" onclick="aendsend();"></td>
	</tr>
	</table>
</td></tr></table>
<?php 
$intDialog = 1;
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>
</body>
</html>