<?php
/**
 * import_PepisZuordnungen.php
 * 
 * Mit diesem Job werden die Zuordnungen der Smart-Repair-Mitarbeiter aus Pepis ins AWIS eingelesen
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto-Teile-Unger
 * @version 20100504
 */
require_once('awisPepisImport.php');
require_once 'awisJobVerwaltung.inc';
require_once('awisDatenbank.inc');

try
{
	$Job = new awisJobProtokoll(15,'Starte Import.');
	
	$Pepis = new awisPepisImport('riedl_t');

	$Job->SchreibeLog(awisJobProtokoll::TYP_START,1,'Import beginnt.');		
	
	$Pepis->DebugLevel(10);
	
	//Pepiszuordnungen importieren
	$Pepis->ImportZuordnungen();				
	
	$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,1,'Import abgeschlossen.');		
}
catch (Exception $ex)
{
#	echo PHP_EOL.'Seminarplanimport: Fehler: '.$ex->getMessage().PHP_EOL;
	
	if(isset($Job))
	{
		awisWerkzeuge::EMail(array('thomas.riedl@de.atu.eu','stefan.oppl@de.atu.eu','christian.beierl@de.atu.eu'),'ERROR PEPISZUORDNUNGEN','',3,'','awis@de.atu.eu');		
		$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	}	
}
?>
