<?php
global $con;
global $awisRSZeilen;
global $CursorFeld;
global $AWISBenutzer;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $AWISBenutzer->BenutzerName());
$BSBreite = awis_BenutzerParameter($con, "BildschirmBreite" , $AWISBenutzer->BenutzerName());

$TextKonserven = array();
$TextKonserven[]=array('MPG','MPG_%');
$TextKonserven[]=array('MGT','MGT_GRUPPE');
$TextKonserven[]=array('Wort','km');
$TextKonserven[]=array('Liste','lst_JaNein');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010);

$MaxDSAnzahl=500;

$SQL = 'SELECT mitbewerbprodgruppen.*,MGT_GRUPPE';
$SQL .= ' FROM mitbewerbprodgruppen INNER JOIN mitbewerbprodgruppentypen ON mpg_mgt_id = mgt_id';
$SQL .= ' WHERE mpg_mbw_key=0'.$Param;
if(isset($_GET['Edit']))
{
	$SQL .= ' AND mpg_key=0'.$_GET['Edit'];
}
elseif(isset($_GET['Neu']))
{
	$SQL .= ' AND mpg_key=0';
}

$rsMPG = awisOpenRecordset($con, $SQL);
$rsMPGZeilen = $awisRSZeilen;


awis_FORM_FormularStart();

$SQL = 'SELECT * FROM MitbewerbProdGruppenTypen ORDER BY MGT_GRUPPE';
$rsMGT=awisOpenRecordset($con,$SQL);
$rsMGTZeilen = $awisRSZeilen;
$SQL = 'SELECT * FROM MitbewerbProdGruppen WHERE MPG_MBW_KEY=0'.$Param;
$rsMPG=awisOpenRecordset($con,$SQL);
$rsMPGZeilen=$awisRSZeilen;

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MPG']['MPG_MGT_ID'],300);
awis_FORM_ZeileEnde();

for($MGTZeile=0;$MGTZeile<$rsMGTZeilen;$MGTZeile++)
{
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($rsMGT['MGT_GRUPPE'][$MGTZeile],200);
	$Wert='';
	for($i=0;$i<$rsMPGZeilen;$i++)
	{
		if($rsMPG['MPG_MGT_ID'][$i]==$rsMGT['MGT_ID'][$MGTZeile])
		{
			$Wert='on';
			break;
		}
	}
	awis_FORM_Erstelle_Checkbox('MGTL_'.$rsMGT['MGT_ID'][$MGTZeile],$Wert,100,true,'on','');	
	
	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();
?>