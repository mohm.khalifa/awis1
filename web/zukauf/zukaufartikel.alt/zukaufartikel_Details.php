<?php
global $con;
global $Recht10001;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZLA','%');
$TextKonserven[]=array('ZLH','ZLH_BEZEICHNUNG');
$TextKonserven[]=array('ZLH','ZLH_KUERZEL');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','AktuellesSortiment');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('AST','AST_VK');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10001 = awisBenutzerRecht($con,10001,$AWISBenutzer->BenutzerName());
if($Recht10001==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = '';
	$Param .= ';'.$_POST['sucZLA_ARTIKELNUMMER'];
	$Param .= ';'.$_POST['sucZLA_BEZEICHNUNG'];
	$Param .= ';'.$_POST['sucZLA_ZLI_KEY'];
	$Param .= ';'.$_POST['sucZLA_ZLH_KEY'];
	$Param .= ';'.$_POST['sucZSZ_SORTIMENT'];
	$Param .= ';'.$_POST['sucZLA_AST_ATUNR'];
	$Param .= ';'.$_POST['sucNummer'];
	$Param .= ';'.$_POST['sucZLA_TAUSCHTEIL'];

	awis_BenutzerParameterSpeichern($con, "ZukaufArtikelSuche" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() , '');
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;;;;;;;';
	include('./zukaufartikel_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "ZukaufArtikelSuche" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		$Param = $AWIS_KEY1;
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;;';
	include('./zukaufartikel_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName());
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$Param = '-1;;;';
}
elseif(isset($_GET['ZLA_KEY']))
{
	$Param = ''.$_GET['ZLA_KEY'].';;;';		// Nur den Key speiechern
}
elseif(isset($_POST['txtZLA_KEY']))
{
	$Param = ''.$_POST['txtZLA_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$Param='';
	if(!isset($_GET['Liste']))
	{
		$Param = awis_BenutzerParameter($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() , '');
	}
	if($Param=='' OR $Param=='0')
	{
		$Param = awis_BenutzerParameter($con, 'ZukaufArtikelSuche', $AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')		// Key
{
	$Bedingung .= 'AND ZLA_Key = ' . intval($Param[0]) . ' ';
}
if(isset($Param[1]) AND $Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(ZLA_ARTIKELNUMMER) ' . awisLIKEoderIST($Param[1].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[2]) AND $Param[2]!='')
{
	$Bedingung .= 'AND (UPPER(ZLA_BEZEICHNUNG) ' . awisLIKEoderIST($Param[2].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[3]) AND $Param[3]>0)
{
	$Bedingung .= 'AND ZLA_ZLI_KEY = ' . intval($Param[3]) . ' ';
}
if(isset($Param[4]) AND $Param[4]>0)
{
	$Bedingung .= 'AND ZLA_ZLH_KEY = ' . intval($Param[4]) . ' ';
}
if(isset($Param[5]) AND $Param[5]>0)
{
	$Bedingung .= 'AND ZLA_ZSZ_KEY= ' . intval($Param[5]) . ' ';
}
if(isset($Param[6]) AND $Param[6]!='')
{
	$Bedingung .= 'AND (UPPER(ZLA_AST_ATUNR) ' . awisLIKEoderIST($Param[6].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[7]) AND $Param[7]!='')
{
	$Bedingung .= 'AND ZLA_KEY IN (SELECT ZAI_ZLA_KEY FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE UPPER(ZAI_ID) ' . awisLIKEoderIST($Param[7].'',1) . ')';
}
if(isset($Param[8]) AND $Param[8]>0)
{
	$Bedingung .= 'AND ZLA_TAUSCHTEIL = ' . intval($Param[8]) . ' ';
}

$SQL = 'SELECT *';
$SQL .= ' FROM Zukauflieferantenartikel';
$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY ZLA_ARTIKELNUMMER';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_Debug(1,$SQL);
$rsZLA = awisOpenRecordset($con, $SQL);
$rsZLAZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if($rsZLAZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>';
}
elseif($rsZLAZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');

	$Link .= '&Sort=ZLA_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLA_ARTIKELNUMMER'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'],200,'',$Link);
	$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],200,'',$Link);
	$Link .= '&Sort=ZLA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLA_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'],400,'',$Link);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZLAZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZLAZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufartikel_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($ZLAZeile=$StartZeile;$ZLAZeile<$rsZLAZeilen and $ZLAZeile<$StartZeile+$MaxDSAnzahl;$ZLAZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY=0'.$rsZLA['ZLA_KEY'][$ZLAZeile].(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
		awis_FORM_Erstelle_ListenFeld('ZLA_ARTIKELNUMMER',$rsZLA['ZLA_ARTIKELNUMMER'][$ZLAZeile],0,200,false,($ZLAZeile%2),'',$Link);
		$Link='';	// TODO: Sp�ter auf die Hersteller
		awis_FORM_Erstelle_ListenFeld('ZLH_KUERZEL',$rsZLA['ZLH_KUERZEL'][$ZLAZeile],0,200,false,($ZLAZeile%2),'',$Link,'T','L',$rsZLA['ZLH_BEZEICHNUNG'][$ZLAZeile]);
		awis_FORM_Erstelle_ListenFeld('ZLA_BEZEICHNUNG',$rsZLA['ZLA_BEZEICHNUNG'][$ZLAZeile],0,400,false,($ZLAZeile%2),'',$Link);
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmCRMAdressen action=./zukaufartikel_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsZLA['ZLA_KEY'][0])?$rsZLA['ZLA_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);

	echo '<input type=hidden name=txtZLA_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufartikel_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZLA['ZLA_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZLA['ZLA_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht10001&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=($Recht10001&6);
	}


	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLI_KEY'].':',200);
	if($EditRecht)
	{
		$CursorFeld='txtZLA_ZLI_KEY';
	}
	$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
	$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZLA_ZLI_KEY',($AWIS_KEY1===0?'':$rsZLA['ZLA_ZLI_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_ZeileEnde();

		// Nummer und Hersteller
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'].':',200);
	awis_FORM_Erstelle_TextFeld('ZLA_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZLA['ZLA_ARTIKELNUMMER'][0]),20,200,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZLA_ZLH_KEY',($AWIS_KEY1===0?'':$rsZLA['ZLA_ZLH_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_ZeileEnde();

		// Bezeichnung
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'].':',200);
	awis_FORM_Erstelle_TextFeld('ZLA_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsZLA['ZLA_BEZEICHNUNG'][0]),50,370,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_TAUSCHTEIL'].':',170);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('ZLA_TAUSCHTEIL',$rsZLA['ZLA_TAUSCHTEIL'][0],200,$EditRecht,$con,'','','','','',$Daten);
	awis_FORM_ZeileEnde();

		// Warengruppen
	awis_FORM_ZeileStart();
	$Link = '/zukauf/warengruppen/warengruppen_Main.php?cmdAktion=Details&WG1='.$rsZLA['ZLA_WG1'][0]."&WG2=".$rsZLA['ZLA_WG2'][0]."&WG3=".$rsZLA['ZLA_WG3'][0];
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_WG1'].':',200,'',$Link);
	awis_FORM_Erstelle_TextFeld('ZLA_WG1',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG1'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_WG2'].':',170);
	awis_FORM_Erstelle_TextFeld('ZLA_WG2',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG2'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_WG3'].':',170);
	awis_FORM_Erstelle_TextFeld('ZLA_WG3',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG3'][0]),10,100,$EditRecht);
	awis_FORM_ZeileEnde();

		// Zuordnung zu einer ATU Sortimentskennzeichnung
	if($AWIS_KEY1!==0 OR 1)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['AktuellesSortiment'].':',200);
		$SQL = 'SELECT ZSZ_SORTIMENT FROM ZukaufSortimentZuordnungen';
		$SQL .= ' WHERE ZSZ_WG1='.awisFeldFormat('T',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG1'][0]));
		$SQL .= ' AND ZSZ_WG2='.awisFeldFormat('T',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG2'][0]));
		$SQL .= ' AND ZSZ_WG3='.awisFeldFormat('T',($AWIS_KEY1===0?'':$rsZLA['ZLA_WG3'][0]));
		$rsZSZ=awisOpenRecordset($con,$SQL);
		awis_FORM_Erstelle_TextFeld('#ZSZ_SORTIMENT',($AWIS_KEY1===0?'':(!isset($rsZSZ['ZSZ_SORTIMENT'][0])?$AWISSprachKonserven['Wort']['KeineZuordnungGefunden']:$rsZSZ['ZSZ_SORTIMENT'][0])),10,300,false);
		awis_FORM_ZeileEnde();
	}

		// Sonstige Daten
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_MENGENEINHEIT'].':',200);
	awis_FORM_Erstelle_TextFeld('ZLA_MENGENEINHEIT',($AWIS_KEY1===0?'':$rsZLA['ZLA_MENGENEINHEIT'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_EINHEITENPROVK'].':',170);
	awis_FORM_Erstelle_TextFeld('ZLA_EINHEITENPROVK',($AWIS_KEY1===0?'':$rsZLA['ZLA_EINHEITENPROVK'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_MWS_ID'].':',170);
	$SQL = 'SELECT MWS_ID, MWS_BEZEICHNUNG FROM MEHRWERTSTEUERSAETZE';
	$SQL .= ' ORDER BY MWS_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZLA_MWS_ID',($AWIS_KEY1===0?'':$rsZLA['ZLA_MWS_ID'][0]),200,$EditRecht,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

		// Preisdaten
	if(($Recht10001&16)==16)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_VK'].':',200);
		awis_FORM_Erstelle_TextFeld('ZLA_VK',($AWIS_KEY1===0?'':$rsZLA['ZLA_VK'][0]),10,100,$EditRecht,'','','','N2');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_EK'].':',170);
		awis_FORM_Erstelle_TextFeld('ZLA_EK',($AWIS_KEY1===0?'':$rsZLA['ZLA_EK'][0]),10,100,$EditRecht,'','','','N2');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_RABATT1'].':',170);
		awis_FORM_Erstelle_TextFeld('ZLA_RABATT1',($AWIS_KEY1===0?'':$rsZLA['ZLA_RABATT1'][0]),10,100,$EditRecht,'','','','N2');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_RABATT2'].':',170);
		awis_FORM_Erstelle_TextFeld('ZLA_RABATT2',($AWIS_KEY1===0?'':$rsZLA['ZLA_RABATT2'][0]),10,100,$EditRecht,'','','','N2');
		awis_FORM_ZeileEnde();
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_PREISDATUM'].':',200);
		awis_FORM_Erstelle_TextFeld('ZLA_PREISDATUM',($AWIS_KEY1===0?'':$rsZLA['ZLA_PREISDATUM'][0]),10,100,$EditRecht,'','','','D');
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_WAEHRUNG'].':',170);
		awis_FORM_Erstelle_TextFeld('ZLA_WAEHRUNG',($AWIS_KEY1===0?'':$rsZLA['ZLA_WAEHRUNG'][0]),10,100,$EditRecht,'','','','T');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_GUELTIGAB'].':',200);
	awis_FORM_Erstelle_TextFeld('ZLA_GUELTIGAB',($AWIS_KEY1===0?'':$rsZLA['ZLA_GUELTIGAB'][0]),10,100,$EditRecht,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_GUELTIGBIS'].':',170);
	awis_FORM_Erstelle_TextFeld('ZLA_GUELTIGBIS',($AWIS_KEY1===0?'':$rsZLA['ZLA_GUELTIGBIS'][0]),10,100,$EditRecht,'','','','D');
	$StatusText = explode("|",$AWISSprachKonserven['Liste']['lst_AktivInaktiv']);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_STATUS'].':',170);
	awis_FORM_Erstelle_SelectFeld('ZLA_STATUS',($AWIS_KEY1===0?'':$rsZLA['ZLA_STATUS'][0]),200,$EditRecht,$con,'','','','','',$StatusText);
	awis_FORM_ZeileEnde();

	awis_FORM_Trennzeile('L');

	// ATU Nummer mit Preiswirkung
	awis_FORM_ZeileStart();
	$Link = '';
	if(isset($rsZLA['ZLA_AST_ATUNR'][0]) AND $rsZLA['ZLA_AST_ATUNR'][0]!='')
	{
		$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsZLA['ZLA_AST_ATUNR'][0].'&cmdAktion=ArtikelInfos';
	}
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'',$Link);
	awis_FORM_Erstelle_TextFeld('ZLA_AST_ATUNR',($AWIS_KEY1===0?'':$rsZLA['ZLA_AST_ATUNR'][0]),10,100,($Recht10001&64),'','','','T');
	if(isset($rsZLA['ZLA_AST_ATUNR'][0]) AND $rsZLA['ZLA_AST_ATUNR'][0]!='')
	{
		$SQL = 'SELECT AST_VK ';
		$SQL .= ' FROM Artikelstamm';
		$SQL .= ' WHERE AST_ATUNR = \''.$rsZLA['ZLA_AST_ATUNR'][0].'\'';
		$rsAST = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen>0)
		{
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',200);
			awis_FORM_Erstelle_TextFeld('*AST_VK',$rsAST['AST_VK'][0],10,100,false,'','','','N2');

			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_PRUEFSTATUS'].':',200);
			$Pruefung = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
			awis_FORM_Erstelle_SelectFeld('ZLA_PRUEFSTATUS',($AWIS_KEY1===0?'0':$rsZLA['ZLA_PRUEFSTATUS'][0]),200,$EditRecht,$con,'','','','','',$Pruefung);
		}
	}
	awis_FORM_ZeileEnde();

	// ATU Nummer ohne Preiswirkung
	$SQL = 'SELECT * FROM zukauflieferantenartikelids';
	$SQL .= ' WHERE zai_zla_key = 0'.$rsZLA['ZLA_KEY'][0];
	$SQL .= ' AND zai_aid_nr = 4';		// "weitere ATU Nummer!"
	$rsZAI = awisOpenRecordset($con,$SQL);

	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZUSATZATUNR'].':',200,'','');
	awis_FORM_Erstelle_TextFeld('ZAI_ID_'.(isset($rsZAI['ZAI_KEY'][0])?$rsZAI['ZAI_KEY'][0]:'0'),(isset($rsZAI['ZAI_ID'][0])?$rsZAI['ZAI_ID'][0]:''),10,100,($Recht10001&(2^7)),'','','','T');


	awis_FORM_FormularEnde();


	if(!isset($_POST['cmdDSNeu_x']))
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Kaeufe'));
		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(10002, $con, $RegisterSeite);
	}


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht10001&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht10001&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht10001&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}
	awis_FORM_SchaltflaechenEnde();


	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsZLA, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}