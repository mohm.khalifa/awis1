<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{

    $AWISBenutzer = awisBenutzer::Init('GEBHARDT_P');
    $Form = new awisFormular();
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$ExportFormat = 1;
	$Anzahl = 0;
  
	$DateiName = 'Personaleinsaetze-Berichte-GBL';
	  
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	 
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
	        break;
	    case 2:                 // Excel 2007
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
	        break;
	}
	 
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode('AWIS'));
	$XLSXObj->getProperties()->setTitle(utf8_encode('Personaleinsaetze'));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode('Personaleinsaetze'));
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode('Personaleinsaetze'));
	
	/*
	$SQLPEIKEY = 'SELECT
                  a.*,
                  VON.XTZ_TAGESZEIT AS von,
                  BIS.XTZ_TAGESZEIT AS bis
                FROM
                  personaleinsaetze a
                INNER JOIN
                  (
                    SELECT
                      a.pei_key,
                      a.pei_fil_id,
                      a.pei_datum,
                      MAX (a.PEI_BISXTZ_ID) OVER (PARTITION BY a.pei_fil_id,a.pei_datum) AS
                      MAXBIS
                    FROM
                      PERSONALEINSAETZE a
                    INNER JOIN
                      (
                        SELECT
                          MAX(pei_datum) AS pei_datum,
                          pei_fil_id
                        FROM
                          PERSONALEINSAETZE
                        WHERE
                          pei_pbe_key = 7
                        GROUP BY
                          pei_fil_id
                      )
                      b
                    ON
                      b.pei_fil_id  = a.pei_fil_id
                    AND b.pei_datum = a.pei_datum
                    WHERE
                      pei_pez_status = 1
                  )
                  b ON a.pei_key = b.pei_key
                AND b.maxbis     = a.pei_bisxtz_id
                INNER JOIN tageszeiten von
                ON
                  von.XTZ_ID = a.PEI_VONXTZ_ID
                INNER JOIN tageszeiten bis
                ON
                  BIS.XTZ_ID = a.PEI_BISXTZ_ID
         
	
	';
	
	*/
	
	$SQLPEIKEY = "
            	   select pei_key, pei_datum, pei_fil_id,
(select xtz_tageszeit from Tageszeiten where xtz_id = pei_vonxtz_id) as von,
(select xtz_tageszeit from Tageszeiten where xtz_id = pei_bisxtz_id) as bis

from(
SELECT DISTINCT
  first_value(pei_key) OVER (PARTITION BY pei_fil_id order by TRUNC(
  pei_datum) desc,PEI_BISXTZ_ID DESC) AS PEIKEY
FROM
  PERSONALEINSAETZE pei
WHERE
  pei_pez_status  = 1
AND pei_pbe_key = 7)
inner join PERSONALEINSAETZE
on peikey = pei_key
	    order by pei_fil_id
	    	    
	    ";
	
	//echo $SQLPEIKEY;
	$rsPEI = $DB->RecordSetOeffnen($SQLPEIKEY);
	
	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	
	//�berschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y H:i:s',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$ZeilenNr++;
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Filialnummer');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Filialname');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Gebiet');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Besuchdatum');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Zeit von');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Zeit bis');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;

	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Ampel');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$BerichtsFelderSQL = 'SELECT
                          *
                        FROM
                          PERSONALEINSBERICHTSFELDER
                        WHERE
                          pbf_pbe_key  = 7
                        AND pbf_status = \'A\'
                        AND pbf_format = \'Z\'
                        ORDER BY
                          PBF_SORTIERUNG';
	
	$rsBerichtsfelder = $DB->RecordSetOeffnen($BerichtsFelderSQL);
	
	//Zeilen�berschrift A1..... Aus der Datenbank holen
	while(!$rsBerichtsfelder->EOF())
	{
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',str_replace('LF_', '', $rsBerichtsfelder->FeldInhalt('PBF_BEZEICHNUNG')))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    $rsBerichtsfelder->DSWeiter();
	}
	
	
	$ZeilenNr++;
	$SpaltenNr=0;
	
	
	//Berichte Durchgehen, 
	while(!$rsPEI->EOF()) 
	{
	  
	    $SpaltenNr = 0;
	    $PEIKey = $rsPEI->FeldInhalt('PEI_KEY');//!=''?$rsPEI->FeldInhalt('PEI_KEY'):785348;
	    
	    $SQL = 'SELECT
                      case PEZ_WERT when \'0\' then \'2\' when \'1\' then \'1\' else \'0\' end as PEZ_WERT
                      FROM
                      Personaleinsberichtszuord a
                      INNER JOIN PERSONALEINSBERICHTSFELDER b
                       ON
                      a.PEZ_PBF_ID = B.PBF_ID
                      WHERE
                      PEZ_PEI_KEY   = '. $PEIKey . '
                      AND PEZ_PBF_ID IN (
                      SELECT
                      PBF_ID
                      FROM
                      PERSONALEINSBERICHTSFELDER
                      WHERE
                      pbf_pbe_key        = 7
                      AND PBF_DATENQUELLE IS NOT NULL
                      AND pbf_format       = \'Z\') order by PBF_SORTIERUNG';  	 
	    //echo $SQL;
	    $rsFelder = $DB->RecordSetOeffnen($SQL);
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsPEI->FeldInhalt('PEI_FIL_ID'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $SpaltenNr++;
        
        
        $SQLFilname = 'Select FIL_BEZ from V_FILIALEN where FIL_ID = ' . $rsPEI->FeldInhalt('PEI_FIL_ID');
        $rsFilname = $DB->RecordSetOeffnen($SQLFilname);
        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFilname->FeldInhalt('FIL_BEZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $SpaltenNr++;
	    
        
        //Zum Debugen den PEIKEY reinschreiben...
        //$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$PEIKey)),PHPExcel_Cell_DataType::TYPE_STRING);
        //$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        //$SpaltenNr++;
        
        $SQLGebiet = 'Select XX1_EBENE from V_Filialpfad where xx1_FIL_ID = ' . $rsPEI->FeldInhalt('PEI_FIL_ID');
        $rsGebiet = $DB->RecordSetOeffnen($SQLGebiet);
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsGebiet->FeldInhalt('XX1_EBENE'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $SpaltenNr++;
	     
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('D0',$rsPEI->FeldInhalt('PEI_DATUM'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsPEI->FeldInhalt('VON'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $SpaltenNr++;
	   
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsPEI->FeldInhalt('BIS'))),PHPExcel_Cell_DataType::TYPE_STRING);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $SpaltenNr++;
	    
	 
	    //Ampel berechnen. 
	    $SQLGesamt = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$PEIKey;
	    $SQLGesamt .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";
	    
	    $rsGesamt = $DB->RecordSetOeffnen($SQLGesamt);
	    
	    $Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');
	    
	    
	    $SQLFalsch = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$PEIKey;
	    $SQLFalsch .= "AND PEZ_WERT = '0'";
	    
	    $GesamtFalsch = $DB->RecordSetOeffnen($SQLFalsch);
	    $Falsch = $GesamtFalsch->FeldInhalt('ANZ');
	     

	    $Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100);
	    
	    if($Prozentsatz >= 89 && $Prozentsatz <= 100)
	    {
	        $Ampelwert = 'Gr�n'; 
	    }
        elseif($Prozentsatz >= 79 && $Prozentsatz <= 89)
        {
            $Ampelwert = 'Gelb';
        }
        elseif($Prozentsatz < 79)
        {
            $Ampelwert = 'Rot';
        }    
            
        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Ampelwert),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $SpaltenNr++;
	   //Wert der Felder A1.. 
	    
	    while(!$rsFelder->EOF())
	    {
	          
	      
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('Z',$rsFelder->FeldInhalt('PEZ_WERT'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
	      
	        $rsFelder->DSWeiter();
	    }
	    $ZeilenNr++;
	    $Anzahl++;
        $rsPEI->DSWeiter();
	}
	

	
	 for($S='A';$S<='BJ';$S++)
	 {
	   $XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	 }
	 
	
	
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment; filename="Personaleinsaetze.xls"');
	        $DateiEndung = '.xls';
	        break;
	    case 2:                 // Excel 2007
	         
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment; filename="Personaleinsaetze.xlsx"');
	        $DateiEndung = '.xlsx';
	        break;
	}
	 
	

	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
	    case 1:                 // Excel 5.0
	        $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
	        break;
	    case 2:                 // Excel 2007
	        $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
	        break;
	}
	//var_dump($Lagerkennung);
	$objWriter->save('/daten/daten/pccommon/Gebhardt/' . $DateiName . '.xls');
	$XLSXObj->disconnectWorksheets();
	$Anzahl++;
	$rsPEI->DSWeiter();
	
	
	
	
	$Absender = 'awisstag@de.atu.eu';
	$Betreff = "OK Personaleinsaetze-Export-Excel " . date('ymd');
	$Text = $Anzahl . ' Daten exportiert  >>/var/log/awis/Personaleinsaetze.log';
	$Empfaenger = 'shuttle@de.atu.eu';
	$Mail->AnhaengeLoeschen();
	$Mail->LoescheAdressListe();
	$Mail->DebugLevel(0);
	$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
	$Mail->Absender($Absender);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
	$Mail->SetzeBezug('TES',0); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
	$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
	$Mail->MailSenden();
}
catch(exception $ex)
{
	$Text = $ex->getMessage();
	
	
	$Absender = 'awisstag@de.atu.eu';
	$Betreff = "OK Personaleinsaetze-Export-Excel " . date('ymd');
	$Text = $Anzahl . ' Daten exportiert  >>/var/log/awis/Personaleinsaetze.log';
	$Empfaenger = 'shuttle@de.atu.eu';
	$Mail->AnhaengeLoeschen();
	$Mail->LoescheAdressListe();
	$Mail->DebugLevel(0);
	$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
	$Mail->Absender($Absender);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
	$Mail->SetzeBezug('TES',0); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
	$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
	$Mail->MailSenden();
}




?>