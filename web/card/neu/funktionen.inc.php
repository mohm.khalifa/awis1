<?php

// feste Parameter:
//
// alle:
//
// text 					(Beschriftung, Vorgabetext)
// hoehe
// breite
//
// Listenfeld (select):
//
// optionsarray				(Array f�r optionen)
// optionstextfeld			(Feldname des Optionarrays)
// optionsidfeld			(Feldname des Optionsarrays)
// optionsvorauwsahlwert	(Wert des Kriteriums f�r Vorauswahl)
// optionsvorauswahlfeld	(Feldname f�r Kriteriumsvergleich bei Vorauswahl)


function element($arrParam)		//�bergeben wird ein assoziatives Array mit den Attributen f�r das HTML-Element
{
	if(isset($arrParam["type"]))	//Pr�ft ob Parameter type �bergeben worden ist
	{
		$strType = $arrParam["type"];
		unset($arrParam["type"]);		//Element type aus Array l�schen
	}
	
	if(isset($arrParam["breite"]))	//Wenn der Parameter breite �bergeben worden ist
	{
		$arrParam["style"].="width:".$arrParam["breite"].";";
		unset($arrParam["breite"]);		//Element breite aus Array l�schen
	}
	
	if(isset($arrParam["hoehe"]))	//Wenn der Parameter hoehe �bergeben worden ist
	{
		$arrParam["style"].="height:".$arrParam["hoehe"].";";
		unset($arrParam["hoehe"]);		//Element hoehe aus Array l�schen
	}
	
	switch($strType)
	{
		case "button":		$strRueckgabe = button($arrParam);
		break;
		case "text":		$strRueckgabe = text($arrParam);
		break;
		case "textarea":	$strRueckgabe = textarea($arrParam);
		break;
		case "select":		$strRueckgabe = select($arrParam);
		break;
	}
		
	return $strRueckgabe;
}

function button($arrParam)	//erzeugt html-Code f�r einen Button
{
	$strAusgabe="<button type=\"button\"";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$strBeschriftung = $arrParam["text"];
		unset($arrParam["text"]);
	}
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">".$strBeschriftung."</button>";
	
	return $strAusgabe;
}

function text($arrParam)		//erzeugt html-Code f�r ein Textfeld
{
	$strAusgabe="<input type=\"text\"";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$arrParam["value"] = $arrParam["text"];
		unset($arrParam["text"]);
	}	
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">";
	
	return $strAusgabe;
}

function textarea($arrParam)	//erzeugt html-Code f�r eine Textarea
{
	$strAusgabe="<textarea";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$strText = $arrParam["text"];
		unset($arrParam["text"]);
	}	
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">".$strText."</textarea>";
	
	return $strAusgabe;
}

function select($arrParam)	//erzeugt html-Code f�r eine Auswahlliste
{
	$strAusgabe="<select";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		unset($arrParam["text"]);
	}
	
	if(isset($arrParam["optionsarray"]))	//Wenn der Parameter optionsarray �bergeben worden ist
	{
		$arrOptionsarray = $arrParam["optionsarray"];
		unset($arrParam["optionsarray"]);
	}	
	
	if(isset($arrParam["optionstextfeld"]))	//Wenn der Parameter optionstextfeld �bergeben worden ist
	{
		$strOptionstextfeld = $arrParam["optionstextfeld"];
		unset($arrParam["optionstextfeld"]);
	}	
	
	if(isset($arrParam["optionsidfeld"]))	//Wenn der Parameter optionsidfeld �bergeben worden ist
	{
		$strOptionsidfeld = $arrParam["optionsidfeld"];
		unset($arrParam["optionsidfeld"]);
	}
	
	if(isset($arrParam["optionsvorauswahlwert"]))	//Wenn der Parameter optionsvorauswahlwert �bergeben worden ist
	{
		$strOptionsvorauswahlwert = $arrParam["optionsvorauswahlwert"];
		unset($arrParam["optionsvorauswahlwert"]);
	}	
	
	if(isset($arrParam["optionsvorauswahlfeld"]))	//Wenn der Parameter optionsvorauswahlfeld �bergeben worden ist
	{
		$strOptionsvorauswahlfeld = $arrParam["optionsvorauswahlfeld"];
		unset($arrParam["optionsvorauswahlfeld"]);
	}		
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">";
	
	for($i=0;$i<count($arrOptionsarray[$strOptionstextfeld]);$i++)
	{
		$strAusgabe.="<option";
		
		if(isset($strOptionsidfeld))
		{
			$strAusgabe.=" id=\"".$arrOptionsarray[$strOptionsidfeld][$i]."\"";
		}
		
		if(isset($strOptionsvorauswahlwert)&&$arrOptionsarray[$strOptionsvorauswahlfeld][$i]==$strOptionsvorauswahlwert)
		{
			$strAusgabe.=" selected";
		}
		
		$strAusgabe.=">".$arrOptionsarray[$strOptionstextfeld][$i]."</option>";		
	}
	
	$strAusgabe.="</select>";
	
	return $strAusgabe;
}

// F�hrt ein �bergebenes Oracle-SQL-Statement aus und liefert zur�ck:
// select: 2-dimensionales Array in der Form $arrRueckgabe["Feldname"][Zeilennr]
// insert, update, delete: Anzahl betroffene Zeilen (1 oder 0)
// im Fehlerfall: Oracle-Fehlermeldung

function oracleabfrage($strSql)
{
	global $hdlOraVerbindung;		//Datenbank Verbindungs-Handle
	
	$strAbfragenart=substr(trim($strSql),0,strpos(trim($strSql)," "));		//ermittelt Abfragenart: select, insert, update oder delete
	
	$hdlAnweisung=@ociparse($hdlOraVerbindung,$strSql);	//Parsen der SQL-Anweisung
		
	if(!@ociexecute($hdlAnweisung))		//Im Fehlerfall wird das Skript abgebrochen und die Oracle-Fehlermeldung zur�ckgegeben
	{
		$arrFehler = @ocierror($hdlAnweisung);
		return $arrFehler["message"];
	}
	
	if(strtoupper($strAbfragenart)=='SELECT')
	{
		$intZeilen=@ocifetchstatement($hdlAnweisung,$arrSelect);
		$varRueckgabe=$arrSelect;									//R�ckgabe eines Arrays
	}
	else
	{
		$varRueckgabe=@ocirowcount($hdlAnweisung);					//R�ckgabe der Anzahl betroffener Zeilen
	}
	
	@ocifreestatement($hdlAnweisung);
	@ocicommit($hdlAnweisung);
		
	return $varRueckgabe;
}

// F�hrt ein �bergebenes Db2-SQL-Statement aus und liefert zur�ck:
// select: 2-dimensionales Array in der Form $arrRueckgabe["Feldname"][Zeilennr]
// insert, update, delete: Anzahl betroffene Zeilen (1 oder 0)
// im Fehlerfall: Skript wird abgebrochen und Fehlermeldung ausgegeben

function db2abfrage($strSql)
{
	global $hdlDb2Verbindung;		//Datenbank Verbindungs-Handle
		
	$strAbfragenart=substr(trim($strSql),0,strpos(trim($strSql)," "));		//ermittelt Abfragenart: select, insert, update oder delete
	
	$hdlAnweisung = @odbc_exec($hdlDb2Verbindung,$strSql) or ende("db2",odbc_errormsg());
				
	if(strtoupper($strAbfragenart)=='SELECT')
	{
		$arrSelect = array();
		$i = 0;
		
		while($arrSpalten=@odbc_fetch_array($hdlAnweisung))		//Aufbau eines 2-dimensionalen Arrays mit den Datensatztreffern
		{														//in der Form $arr["Feldname"][Zeilennr] = Wert der Zeilennr
			foreach($arrSpalten as $index => $wert)
			{
				if(!is_array($arrSelect[$index]))
				{
					$arrSelect[$index]=array();
				}
			
				$arrSelect[$index][$i]=$wert;
			}
			$i++;
		}
		$varRueckgabe=$arrSelect;									//R�ckgabe eines Arrays
	}
	else
	{
		$varRueckgabe = @odbc_num_rows($hdlAnweisung);				//R�ckgabe der Anzahl betroffener Zeilen
		@odbc_commit($hdlDb2Verbindung);
	}
	
	return $varRueckgabe;
}


function aufloesungermitteln($seite)
{
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";

	echo "<html>";
	echo "<head>";
	echo "<title>Objekt</title>";
	
	echo "<script language=\"JavaScript\">";
	
	echo "function bildschirmdaten()";
	echo "{";
		echo "location.href='".$seite."?aufl='+screen.width;";
	echo "}";
	echo "</script>";
		
	echo "</head>";

	echo "<body onload=\"bildschirmdaten();\"></body>";
}


//Wird aufgerufen, wenn ein Fehler aufgetreten ist

function ende($strFehlerart,$strFehlermeldung)
{
	
	die($strFehlerart." ".$strFehlermeldung);
}


?>