<?php
global $REX;
global $AWIS_KEY1;
try {
    if($AWIS_KEY1>0) { //gešnderter DS
        $SQL  ='UPDATE REIFENREKLAMATIONEN';
        $SQL .=' SET ';
        $SQL .='  REX_STATUS       = \'Z\'';
        $SQL .=' , REX_USER              = '. $REX->DB->WertSetzen('REX','T',$REX->AWISBenutzer->BenutzerName(),false);
        $SQL .=' , REX_USERDAT           = sysdate';
        $SQL .=' WHERE REX_KEY             = '. $REX->DB->WertSetzen('REX','N0',$AWIS_KEY1,false);

        $REX->DB->Ausfuehren($SQL, '', true, $REX->DB->Bindevariablen('REX'));
        $Meldung = $REX->AWISSprachKonserven['REX']['REX_FREIGABE_OK'];
    }
    if($Meldung){
        $REX->Form->Hinweistext($Meldung);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $REX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $REX->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $REX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>