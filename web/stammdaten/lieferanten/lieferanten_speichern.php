<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $TXT_Speichern;

$TextKonserven = array();
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');

try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $AWIS_KEY1 = (isset($_POST['txtLIE_NR'])?$_POST['txtLIE_NR']:'');

    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLIE_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '') {
        $AWIS_KEY1 = $_POST['txtLIE_NR'];

        $rsLIE = $DB->RecordSetOeffnen('SELECT * FROM Lieferanten WHERE LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1));
        if ($rsLIE->EOF()) {
            $PflichtfelderTest = array('LIE_NR', 'LIE_NAME1');
            $Pflichtfelder = array();
            $Fehler = '';
            foreach ($PflichtfelderTest as $Feld) {
                if (isset($_POST['txt' . $Feld])) {
                    $Pflichtfelder[] = $Feld;
                }
            }
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LIE'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }
            $SQL = 'INSERT INTO Lieferanten';
            $SQL .= '(LIE_NR,LIE_NAME1,LIE_NAME2,LIE_ANSPRECHP1,LIE_STRASSE,LIE_LAND,LIE_PLZ';
            $SQL .= ',LIE_ORT,LIE_TELEFON,LIE_TELEFAX,LIE_REKLNAME,LIE_REKLSTRASSE,LIE_REKLPLZ,LIE_REKLORT';
            $SQL .= ',LIE_BEKANNTWW,LIE_DUMMYKZ';
            $SQL .= ',LIE_USER,LIE_USERDAT';
            $SQL .= ')VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_NR'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_NAME1'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_NAME2'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_ANSPRECHP1'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_STRASSE'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_LAND'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_PLZ'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_ORT'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_TELEFON'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_TELEFAX'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_REKLNAME'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_REKLSTRASSE'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_REKLPLZ'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_REKLORT'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLIE_BEKANNTWW'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_DUMMYKZ'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL, '', true) === false) {
                throw new Exception('Fehler beim Speichern eines Lieferanten:' . $SQL, 201003161649);
            }
            $AWIS_KEY1 = $DB->FeldInhaltFormat('T', $_POST['txtLIE_NR'], true);
        } else {
            $PflichtfelderTest = array('LIE_NR', 'LIE_NAME1');
            $Pflichtfelder = array();
            $Fehler = '';
            foreach ($PflichtfelderTest as $Feld) {
                if (isset($_POST['txt' . $Feld])) {
                    $Pflichtfelder[] = $Feld;
                }
            }

            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LIE'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $FehlerListe = array();
            $UpdateFelder = '';

            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLIE->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLIE->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLIE->FeldInfo($FeldName, 'TypKZ'), $rsLIE->FeldInhalt($FeldName), true);

                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLIE->FeldInhalt('LIE_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Lieferanten SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LIE_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LIE_userdat=sysdate';
                $SQL .= ' WHERE LIE_NR=' . $DB->FeldInhaltFormat('T', $_POST['txtLIE_NR']) . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 201003161651, $SQL, 2);
                }
            }
        }
    }

    //***********************************************************************************
    //** Infos speichern
    //***********************************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLIN_WERT', 1, 1));

    if (count($Felder) > 0 AND $Felder[0] != '') {
        foreach ($Felder AS $Feld) {
            $SQL = '';
            $IDs = explode('_', $Feld);

            $rsLIN = $DB->RecordSetOeffnen('SELECT LIN_KEY, ITY_ANZAHL FROM LieferantenInfos INNER JOIN InformationsTypen ON LIN_ITY_KEY = ITY_KEY WHERE LIN_ITY_KEY = 0' . $IDs[2] . ' AND LIN_LIE_NR = ' . $DB->FeldInhaltFormat('T',
                    $AWIS_KEY1));

            if ($IDs[3] == '' AND $_POST[$Feld] !== '')            // Neuer Datensatz
            {
                $SQL = 'SELECT Count(*) FROM LieferantenInfos';
                $SQL .= ' WHERE LIN_LIE_NR=' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);
                $SQL .= ' AND LIN_ITY_KEY =' . $DB->FeldInhaltFormat('N0', $IDs[2]);
                $rsDaten = $DB->RecordSetOeffnen($SQL);

                if (!$rsLIN->EOF() AND $rsLIN->FeldInhalt('ITY_ANZAHL') <= $rsDaten->AnzahlDatensaetze()) {
                    $Form->Fehler_Anzeigen('ZuVieleOptionen', '', 'PRUEFEN', 5, '201003261103');
                } else {
                    $SQL = 'INSERT INTO LieferantenInfos';
                    $SQL .= '(LIN_WERT, LIN_ITY_KEY, LIN_LIE_NR, LIN_IMQ_ID, LIN_USER, LIN_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= ' ' . $DB->FeldInhaltFormat($IDs[4], $_POST[$Feld]);
                    $SQL .= ',' . $IDs[2];
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);
                    $SQL .= ',4';
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
                    $SQL .= ',sysdate';
                    $SQL .= ')';
                }
            } elseif ($IDs[3] != '' AND $_POST[$Feld] == '')        // L�schen
            {
                $SQL = 'DELETE from LieferantenInfos';
                $SQL .= ' WHERE LIN_KEY = 0' . $IDs[3] . ' AND LIN_LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);
            } elseif ($_POST[$Feld] != '')                        // �ndern
            {
                $WertAlt = $DB->FeldInhaltFormat($IDs[4], $_POST['old' . substr($Feld, 3)]);
                $WertNeu = $DB->FeldInhaltFormat($IDs[4], $_POST[$Feld]);
                if ($WertAlt != $WertNeu) {
                    $SQL = 'UPDATE LieferantenInfos ';
                    $SQL .= 'SET LIN_WERT= ' . $DB->FeldInhaltFormat($IDs[4], $_POST[$Feld]);
                    $SQL .= ',LIN_USER=' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
                    $SQL .= ',LIN_USERDAT=sysdate';
                    $SQL .= ' WHERE LIN_KEY = 0' . $IDs[3] . ' AND LIN_LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);
                }
            }
            if ($SQL != '') {
                if ($DB->Ausfuehren($SQL) === false) {
                    $Form->Fehler_Anzeigen('SpeicherFehler', 1, 'MELDEN', 5, '200809021421');
                }
            }
        }
    }

    //******************************************************************
    //* AP f�r Lieferanten
    //******************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAP_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '') {
        $AWIS_KEY2 = $_POST['txtLAP_KEY'];
        $TextKonserven[] = array('LAP', 'LAP_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);

        // Daten auf Vollst�ndigkeit pr�fen
        $Fehler = '';
        $Pflichtfelder = array('LAP_NACHNAME');
        foreach ($Pflichtfelder AS $Pflichtfeld) {
            if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LAP'][$Pflichtfeld] . '<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if ($Fehler != '') {
            die('<span class=HinweisText>' . $Fehler . '</span>');
        }

        $rsLAP = $DB->RecordSetOeffnen('SELECT * FROM LIEFERANTENAP WHERE LAP_KEY = ' . $DB->FeldInhaltFormat('N0', $AWIS_KEY2));
        if ($rsLAP->EOF()) {
            $SQL = 'INSERT INTO LIEFERANTENAP';
            $SQL .= '(LAP_LIE_NR,LAP_ANR_ID,LAP_NACHNAME,LAP_VORNAME,LAP_TAETIGKEIT,LAP_POSITION';
            $SQL .= ',LAP_TELEFON,LAP_FAXNUMMER,LAP_EMAIL,LAP_BEMERKUNG,LAP_LZG_KEY';
            $SQL .= ',LAP_USER,LAP_USERDAT';
            $SQL .= ')VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1, true);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAP_ANR_ID'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_NACHNAME'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_VORNAME'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_TAETIGKEIT'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_POSITION'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_TELEFON'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_FAXNUMMER'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_EMAIL'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAP_BEMERKUNG'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAP_LZG_KEY'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL, '', true) === false) {
                throw new Exception('Fehler beim Speichern einer Bestellung:' . $SQL, 200909151906);
            }
            $SQL = 'SELECT seq_LAP_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $AWIS_KEY2 = $rsKey->Feldinhalt('KEY');
        } else {
            $FehlerListe = array();
            $UpdateFelder = '';

            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLAP->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLAP->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLAP->FeldInfo($FeldName, 'TypKZ'), $rsLAP->FeldInhalt($FeldName), true);

                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLAP->FeldInhalt('LAP_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE LIEFERANTENAP SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LAP_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LAP_userdat=sysdate';
                $SQL .= ' WHERE LAP_key=0' . $_POST['txtLAP_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 808181117, $SQL, 2);
                }
            }
        }
    }

    //******************************************************************
    //* Gruppen AP f�r Lieferanten
    //******************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLZG_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '') {
        $AWIS_KEY1 = $_POST['txtLZG_KEY'];
        $TextKonserven[] = array('LZG', 'LZG_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);

        // Daten auf Vollst�ndigkeit pr�fen
        $Fehler = '';
        $Pflichtfelder = array('LZG_BEZEICHNUNG');
        foreach ($Pflichtfelder AS $Pflichtfeld) {
            if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LZG'][$Pflichtfeld] . '<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if ($Fehler != '') {
            die('<span class=HinweisText>' . $Fehler . '</span>');
        }

        $rsLZG = $DB->RecordSetOeffnen('SELECT * FROM LIEFERANTENAPZUGRIFFSGRP WHERE LZG_KEY = ' . $DB->FeldInhaltFormat('N0', $AWIS_KEY1));
        if ($rsLZG->EOF()) {
            $SQL = 'INSERT INTO LIEFERANTENAPZUGRIFFSGRP';
            $SQL .= '(LZG_XBN_KEY,LZG_BEZEICHNUNG,LZG_BEMERKUNG';
            $SQL .= ',LZG_USER,LZG_USERDAT';
            $SQL .= ')VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat('N0', $_POST['txtLZG_XBN_KEY'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLZG_BEZEICHNUNG'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLZG_BEMERKUNG'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL, '', true) === false) {
                throw new Exception('Fehler beim Speichern einer Bestellung:' . $SQL, 200909151906);
            }
            $SQL = 'SELECT seq_LZG_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $AWIS_KEY1 = $rsKey->Feldinhalt('KEY');
        } else {
            $FehlerListe = array();
            $UpdateFelder = '';

            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLZG->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLZG->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLZG->FeldInfo($FeldName, 'TypKZ'), $rsLZG->FeldInhalt($FeldName), true);

                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLZG->FeldInhalt('LZG_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE LIEFERANTENAPZUGRIFFSGRP SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LZG_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LZG_userdat=sysdate';
                $SQL .= ' WHERE LZG_key=0' . $_POST['txtLZG_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 808181117, $SQL, 2);
                }
            }
        }
    }

    //******************************************************************
    //* Gruppen f�r AP f�r Lieferanten
    //******************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLGM_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '') {
        $AWIS_KEY2 = $_POST['txtLGM_KEY'];
        $TextKonserven[] = array('LGM', 'LGM_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);

        // Daten auf Vollst�ndigkeit pr�fen
        $Fehler = '';
        $Pflichtfelder = array('LGM_XBN_KEY');
        foreach ($Pflichtfelder AS $Pflichtfeld) {
            if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LGM'][$Pflichtfeld] . '<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if ($Fehler != '') {
            die('<span class=HinweisText>' . $Fehler . '</span>');
        }

        $rsLGM = $DB->RecordSetOeffnen('SELECT * FROM LIEFERANTENAPZUGRIFFSGRPBEN WHERE LGM_KEY = ' . $DB->FeldInhaltFormat('N0', $_POST['txtLGM_KEY']));
        if ($rsLGM->EOF()) {
            $SQL = 'INSERT INTO LIEFERANTENAPZUGRIFFSGRPBEN';
            $SQL .= '(LGM_LZG_KEY, LGM_XBN_KEY,LGM_AENDERN';
            $SQL .= ',LGM_USER,LGM_USERDAT';
            $SQL .= ')VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat('N0', $_POST['txtLGM_LZG_KEY'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLGM_XBN_KEY'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLGM_AENDERN'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL, '', true) === false) {
                throw new Exception('Fehler beim Speichern' . $SQL, 200910201154);
            }
            $SQL = 'SELECT seq_LGM_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $AWIS_KEY2 = $rsKey->Feldinhalt('KEY');
        } else {
            $FehlerListe = array();
            $UpdateFelder = '';

            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLGM->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLGM->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLGM->FeldInfo($FeldName, 'TypKZ'), $rsLGM->FeldInhalt($FeldName), true);

                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLGM->FeldInhalt('LGM_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE LIEFERANTENAPZUGRIFFSGRPBEN SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LGM_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LGM_userdat=sysdate';
                $SQL .= ' WHERE LGM_key=0' . $_POST['txtLGM_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 808181117, $SQL, 2);
                }
            }
        }
    }

    //******************************************************************
    //* Verkaufsh�user f�r den externen Zukauf
    //******************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLVK_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '') {

        $AWIS_KEY2 = $_POST['txtLVK_KEY'];
        $TextKonserven[] = array('LVK', 'LVK_%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);

        // Daten auf Vollst�ndigkeit pr�fen
        $Fehler = '';
        $Pflichtfelder = array('LVK_NAME1', 'LVK_STRASSE', 'LVK_ORT', 'LVK_LAN_CODE');
        foreach ($Pflichtfelder AS $Pflichtfeld) {
            if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LVK'][$Pflichtfeld] . '<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if ($Fehler != '') {
            die('<span class=HinweisText>' . $Fehler . '</span>');
        }

        $rsLVK = $DB->RecordSetOeffnen('SELECT * FROM LIEFERANTENVERKAUFSHAEUSER WHERE LVK_KEY = ' . $DB->FeldInhaltFormat('N0', $AWIS_KEY2));
        if ($rsLVK->EOF()) {
            $SQL = 'INSERT INTO LIEFERANTENVERKAUFSHAEUSER';
            $SQL .= '(LVK_LIE_NR,LVK_KENNUNG,LVK_NAME1,LVK_NAME2,LVK_STRASSE,LVK_LAN_CODE';
            $SQL .= ',LVK_PLZ,LVK_ORT,LVK_ANSPRECHPARTNER,LVK_TELEFON,LVK_FAX,LVK_EMAIL';
            $SQL .= ',LVK_DATUMVOM,LVK_DATUMBIS,LVK_VKH_ID';
            $SQL .= ',LVK_USER,LVK_USERDAT';
            $SQL .= ')VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_LIE_NR'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_KENNUNG'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_NAME1'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_NAME2'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_STRASSE'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_LAN_CODE'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_PLZ'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_ORT'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_ANSPRECHPARTNER'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_TELEFON'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_FAX'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_EMAIL'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('D', $_POST['txtLVK_DATUMVOM'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('D', $_POST['txtLVK_DATUMBIS'], true);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLVK_VKH_ID'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            if ($DB->Ausfuehren($SQL, '', true) === false) {
                throw new Exception('Fehler beim Speichern einer Bestellung:' . $SQL, 200909151906);
            }
            $SQL = 'SELECT seq_LVK_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $AWIS_KEY2 = $rsKey->Feldinhalt('KEY');
        } else {
            $FehlerListe = array();
            $UpdateFelder = '';

            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLVK->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLVK->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLVK->FeldInfo($FeldName, 'TypKZ'), $rsLVK->FeldInhalt($FeldName), true);

                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLVK->FeldInhalt('LVK_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE LIEFERANTENVERKAUFSHAEUSER SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LVK_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LVK_userdat=sysdate';
                $SQL .= ' WHERE LVK_key=0' . $_POST['txtLVK_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 808181117, $SQL, 2);
                }
            }
        }

        $AWIS_KEY2 = 0;
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>