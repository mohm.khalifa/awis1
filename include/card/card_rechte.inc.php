<?php

require_once("sicherheit.inc.php");			// Rechtepr�fung AWIS
require_once("db.inc.php");					// DB AWIS


//Ermittelt die AWIS-Rechtestufe bei �bergabe einer Rechte-Id (Tabelle RECHTE)

function rechtestufe($intRechtNr)
{
	global $hdlOraVerbindung;
	
	if(!isset($hdlOraVerbindung))
		$hdlOraVerbindung=oraverbindung();
	
	return awisBenutzerRecht($hdlOraVerbindung,$intRechtNr);
}



//Einschr�nkungen



//Rechte

$intMahngebuehrenHauptBestDsAendern=1;		//Bestehende Datens�tze in mahngebuehren_haupt d�rfen statt nur gelesen
											//auch bearbeitet (ge�ndert, gel�scht) werden
											
$intCardIFrameBestDsAendern=1;				//Bestehende Datens�tze in card_iframe.php d�rfen statt nur gelesen
											//auch bearbeitet (ge�ndert, gel�scht) werden	
											
$intKazeinzelsatzHauptBestDsAendern=1;		//Bestehende Datens�tze in kazeinzelsatz_haupt d�rfen statt nur gelesen
											//auch bearbeitet (ge�ndert, gel�scht) werden					

$intSonderkonditionenZZBestDsAendern=1;		//Bestehende Datens�tze in sonderkonditionenzz_haupt d�rfen statt nur gelesen
											//auch bearbeitet (ge�ndert, gel�scht) werden					
										
											
?>