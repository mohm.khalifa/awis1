<?
/****************************************************************************************************
* 
* 	Artikel-Liste
* 
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Sept. 2003
* 
****************************************************************************************************/
// Variablen
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;

global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $RechteStufe;			// ATU Artikel: Basiszugriff

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

		// ATU Artikel: Bearbeitung
$BearbeitungsStufe = awisBenutzerRecht($con,401);

/**********************************************************************************************************
* Neuen ATU Artikel hinzuf�gen
**********************************************************************************************************/
if(($BearbeitungsStufe&4)==4 AND $HTTP_GET_VARS['Hinzufuegen']=='True')
{
		print "<form name=frmArtikel method=post action=./artikel_Main.php?cmdAktion=ArtikelInfos>";
		
		print "<table border=0 width=100%>";
		print "<tr><td><font size=5><b><i>Neuer ATU Artikel ";
		print "</i></b></font></td>";
		print "</tr></table>";
	
			// Zeile 1
		print "<table border=1 width=100% >";
		print "<colgroup><col width=150><col width=*></colgroup>";
		print "<tr>";
		print "<td id=FeldBez>ATU-Nr</td><td><input name=txtAST_ATUNR size=10></td>";

			// Zeile 2
/*		print "<table border=1 width=100% >";
		print "<colgroup><col width=150><col width=*></colgroup>";
		print "<tr><td id=FeldBez>Warengruppe</td><td>";
		echo '<select name=txtAST_WUG_KEY>';
		$rsWGR = awisOpenRecordset($con, "SELECT WUG_KEY, WUG_BEZEICHNUNG || ' - ' || WGR_BEZEICHNUNG AS BEZ FROM WARENGRUPPEN, WARENUNTERGRUPPEN WHERE WUG_WGR_ID = WGR_ID ORDER BY WUG_BEZEICHNUNG");
		for($zWGR=0;$zWGR<$awisRSZeilen;$zWGR++)
		{
			echo '<option value=' . $rsWGR['WUG_KEY'][$zWGR] . '>' . $rsWGR['BEZ'][$zWGR] . '</option>';
		}
		echo '</select>';
		print "</td></tr></table>";
*/
			// Zeile 3
		print "<table border=1 width=100% >";
		print "<colgroup><col width=200><col><col width=150><col></colgroup>";
		print "<td id=FeldBez>Alterna<u>t</u>ivbezeichnung</td>";		
		print "<td><input type=text accesskey=t size=85 name=txtAST_BEZEICHNUNG value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0] . "\">";
		print "<input type=hidden name=txtAST_BEZEICHNUNG_old value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0]. "\"></td>";
		print "<td id=FeldBez>KENN-Vorschlag:</td>";		
		print "<td><input type=text size=2 name=txtAST_KENNUNGVORSCHLAG value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\">";
		print "<input type=hidden name=txtAST_KENNUNGVORSCHLAG_old value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\"></td>";
		print "</td></tr></table>";

		print "<input type=hidden name=NeuSpeichern Value=True>";
		print "<input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&NeuerDS=True'>";
		print "</form>"	;

				// Cursor in das erste Feld setzen
		print "<Script Language=JavaScript>";
		print "document.getElementsByName(\"txtAST_ATUNR\")[0].focus();";
		print "</Script>";

		die();		// Bearbeitung hier beenden
}

if($HTTP_POST_VARS['NeuSpeichern']=='True' AND ($BearbeitungsStufe&4)==4)
{
	if(strtoupper($HTTP_POST_VARS['txtAST_ATUNR']) == '')
	{
		echo '<span class=HinweisText>Die ATU Nummer muss angegeben werden.</span>';
		die();
	}
	$rsArtikel = awisOpenRecordset($con, "SELECT AST_KEY, AST_ATUNR, AST_BEZEICHNUNGWW FROM AWIS.ARTIKELSTAMM WHERE AST_ATUNR='" . strtoupper($HTTP_POST_VARS['txtAST_ATUNR'] . "'"));
	if($awisRSZeilen>0)
	{
		echo '<span class=HinweisText>Der ATU Artikel ' . strtoupper($HTTP_POST_VARS['txtAST_ATUNR']) . ' ist bereits vorhanden</span>';
		echo '<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0' . $rsArtikel['AST_KEY'][0] . ">";
		echo $rsArtikel['AST_ATUNR'][0] . "</a>&nbsp;" . $rsArtikel['AST_BEZEICHNUNGWW'][0];
		die();
	}

	$SQL = "INSERT INTO AWIS.ArtikelStamm(";
	$SQL .= "AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG, AST_WUG_KEY, AST_VK, AST_IMQ_ID, AST_USER, AST_USERDAT)";
	$SQL .= "VALUES(";
	$SQL .= "'" . strtoupper($HTTP_POST_VARS['txtAST_ATUNR']) . "',";
	$SQL .= "'" . $HTTP_POST_VARS['txtAST_BEZEICHNUNG'] . "',";
	$SQL .= "'" . $HTTP_POST_VARS['txtAST_KENNUNGVORSCHLAG'] . "',";
//	$SQL .= "" . $HTTP_POST_VARS['txtAST_WUG_KEY'] . ",";
	$SQL .= "1,";
	$SQL .= "0.00,";
	$SQL .= "4,";
	$SQL .= "'" . $_SERVER['PHP_AUTH_USER'] . "',";
	$SQL .= "SYSDATE";
	$SQL .= ")";

	$Erg = awisExecute($con, $SQL);
	if($Erg==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}

	$rsAST = awisOpenRecordset($con, "SELECT AST_KEY FROM AWIS.ArtikelStamm where AST_ATUNR='" . strtoupper($HTTP_POST_VARS['txtAST_ATUNR']) . "'");
	awis_BenutzerParameterSpeichern($con, "AktuellerArtikel", $_SERVER['PHP_AUTH_USER'], $rsAST['AST_KEY'][0]);
}
/**********************************************************************************************************
* Ende HINZUF�GEN
**********************************************************************************************************/

/**********************************************************************************************************
* Artikel l�schen
**********************************************************************************************************/

if(awis_NameInArray($HTTP_POST_VARS, "cmdLoeschen_")!='')
{
	if(isset($HTTP_POST_VARS["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM AWIS.ArtikelStamm WHERE AST_KEY=0" . $HTTP_POST_VARS["txtAST_KEY"];
		$Erg = awisExecute($con, $SQL );
		if($Erg==FALSE)
		{
			awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
		}
		die('<br>Der Artikel wurde erfolgreich gel�scht<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True>Weiter</a>');
	}
	elseif($HTTP_POST_VARS["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmArtikel method=post>";

		print "<input type=hidden name=" . awis_NameInArray($HTTP_POST_VARS, "cmdLoeschen_") . ">";
		print "<input type=hidden name=txtAST_KEY value=" . $HTTP_POST_VARS["txtAST_KEY"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Artikel l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}
/**********************************************************************************************************
* ENDE Artikel l�schen
**********************************************************************************************************/

		// Aktualisieren gew�hlt?
if(isset($HTTP_POST_VARS['cmdSuche_x']))
{
	$Param = $HTTP_POST_VARS['txtAuswahlSpeichern'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_ATUNR'];
	$Param .= ";" . $HTTP_POST_VARS['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH'];
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_Exakt'];		// Exakte Suche
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
	$Param .= ";" . $HTTP_POST_VARS['txtKommentar'];			// Kommentar im Artikel
	$Param .= ";" . $HTTP_POST_VARS['txtTEI_SUCH_XXX'];			// Suche nach beliebiger Nummer
	
	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $_SERVER['PHP_AUTH_USER'] , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerArtikel", $_SERVER['PHP_AUTH_USER'], "");
	
	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $_SERVER['PHP_AUTH_USER']));
    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $_SERVER['PHP_AUTH_USER']));
}

	// SQL Anweisung erstellen
$SQL = "SELECT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG ";
$TabellenListe = " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN ";

if($HTTP_GET_VARS['Liste']=="True")		// Liste anzeigen
{
	$Artikel[0]='';
}
$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($HTTP_GET_VARS["Key"]) OR isset($HTTP_POST_VARS["txtAST_KEY"]) OR $Artikel[0]!='')
{
	if(isset($HTTP_POST_VARS["txtAST_KEY"]))
	{
		$ASTKEY = $HTTP_POST_VARS["txtAST_KEY"];
	}
	elseif(isset($HTTP_GET_VARS["Key"]))
	{
		$ASTKEY = $HTTP_GET_VARS["Key"];
	}
	else
	{
		$ASTKEY=$Artikel[0];
	}
	$Bedingung = " AND AST_KEY = " . $ASTKEY;


//**********************************************************
// Lieferantenartikelzuordnung l�schen
//**********************************************************
	if(awis_NameInArray($HTTP_POST_VARS, "cmdLARLoeschen_")!='' AND !isset($HTTP_POST_VARS["cmdLoeschAbbruch"]))
	{
		if(isset($HTTP_POST_VARS["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM TeileInfos WHERE TEI_KEY=0" . intval(substr(awis_NameInArray($HTTP_POST_VARS, "cmdLARLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			print "<form name=frmArtikel method=post>";

			print "<input type=hidden name=" . awis_NameInArray($HTTP_POST_VARS, "cmdLARLoeschen_") . ">";

			print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
			
			print "</form>";
			awisLogoff($con);
			die();			
		}	
	}

//****************************************************
// EAN Nummern Zuordnung l�schen
//****************************************************
	if(awis_NameInArray($HTTP_POST_VARS, "cmdEANLoeschen_")!='' AND !isset($HTTP_POST_VARS["cmdLoeschAbbruch"]))
	{
		if(isset($HTTP_POST_VARS["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM AWIS.TeileInfos WHERE TEI_KEY2=0" . intval(substr(awis_NameInArray($HTTP_POST_VARS, "cmdEANLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			print "<form name=frmArtikel method=post>";

			print "<input type=hidden name=" . awis_NameInArray($HTTP_POST_VARS, "cmdEANLoeschen_") . ">";

			print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
			
			print "</form>";
			die();			
		}	
	}

//****************************************************
// OE Nummern zuordnung l�schen
//****************************************************
	if(awis_NameInArray($HTTP_POST_VARS, "cmdOENLoeschen_")!='' AND !isset($HTTP_POST_VARS["cmdLoeschAbbruch"]))
	{
		if(isset($HTTP_POST_VARS["cmdLoeschBestaetigung"]))
		{
			$SQL = "DELETE FROM AWIS.TeileInfos WHERE TEI_KEY2=0" . intval(substr(awis_NameInArray($HTTP_POST_VARS, "cmdOENLoeschen_"),15));
			$Erg = awisExecute($con, $SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
			}
		}
		else
		{
			print "<form name=frmArtikel method=post>";

			print "<input type=hidden name=" . awis_NameInArray($HTTP_POST_VARS, "cmdOENLoeschen_") . ">";

			print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Zuordnung l�schen m�chten?</span><br><br>";
			print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
			print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
			
			print "</form>";
			awisLogoff($con);
			die();			
		}	
	}
	
//****************************************************
//
// Daten speichern
//
//****************************************************	
	
	if(awis_NameInArray($HTTP_POST_VARS, "cmdSpeichern")!='')
	{
		//******************************
		// Artikelstamm speichern
		//******************************
		$SAVESQL='';
		if(isset($HTTP_POST_VARS["txtAST_BEZEICHNUNG"]))	
		{
			if($HTTP_POST_VARS["txtAST_BEZEICHNUNG"] != $HTTP_POST_VARS["txtAST_BEZEICHNUNG_old"])
			{
				$SAVESQL .= ", AST_BEZEICHNUNG = '" . $HTTP_POST_VARS["txtAST_BEZEICHNUNG"] . "'";
			}			

			if($HTTP_POST_VARS["txtAST_KENNUNGVORSCHLAG"] != $HTTP_POST_VARS["txtAST_KENNUNGVORSCHLAG_old"])	
			{
				$SAVESQL .= ", AST_KENNUNGVORSCHLAG = '" . substr($HTTP_POST_VARS["txtAST_KENNUNGVORSCHLAG"],0,1) . "'";
			}

			$rsUpdateTest = awisOpenRecordset($con, "SELECT AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG, AST_USER, AST_UserDat FROM AWIS.ArtikelStamm WHERE AST_KEY=0" . $ASTKEY);
			if($HTTP_POST_VARS["txtAST_BEZEICHNUNG_old"] != $rsUpdateTest['AST_BEZEICHNUNG'][0] OR
				$HTTP_POST_VARS["txtAST_KENNUNGVORSCHLAG_old"] != $rsUpdateTest['AST_KENNUNGVORSCHLAG'][0])
			{
				echo '<span class=HinweisText>Die Daten wurden in der Zwischenzeit durch den Benutzer ' . $rsUpdateTest['AST_USER'][0] . ' ver�ndert.</span>';
				echo '<br><br>Bezeichnung: ' . $rsUpdateTest['AST_BEZEICHNUNG'][0] . '<br>';
				echo 'Kennvorschlag: ' . $rsUpdateTest['AST_KENNUNGVORSCHLAG'][0] . '<br>';
				echo '�nderungszeit:' . $rsUpdateTest['AST_USERDAT'][0] . '<br>';
				echo "<br><br><a href=./artikel_Main.php?Key=$ASTKEY&cmdAktion=ArtikelInfos>Bitte wiederholen Sie Ihre Eingabe.</a>";
				awisLogoff($con);
				die();
			}
	
			If($SAVESQL!='')
			{
				$SAVESQL = " UPDATE AWIS.ARTIKELSTAMM SET " . substr($SAVESQL,1) . " WHERE AST_KEY=" . $ASTKEY;
				$ExErg = awisExecute($con, $SAVESQL, FALSE);
				if($ExErg==FALSE)
				{
					awisErrorMailLink("artikel_Artikel", 3, $awisDBFehler['message']);
				}
			}
		}
		$SAVESQL='';

		/**************************************
		* Gebrauchsnummern speichern --> Nur zu VERSUCHSZWECKEN
		**************************************/
		if(isset($HTTP_POST_VARS["txtGNR_KEY"]))
		{
			print_r($HTTP_POST_VARS["txtGNR_KEY"]);
		}


		/***************************************
		* Meldung und Meldung durch
		****************************************/
		if(isset($HTTP_POST_VARS["txtMELDUNG"]))
		{
	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $HTTP_POST_VARS["txtMELDUNG_KEY"] . ",100,'" . $HTTP_POST_VARS["txtMELDUNG"] . "','" . $_SERVER['PHP_AUTH_USER'] . "', " . $HTTP_POST_VARS["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}

	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $HTTP_POST_VARS["txtMELDUNGDURCH_KEY"] . ",101,'" . $HTTP_POST_VARS["txtMELDUNGDURCH"] . "','" . $_SERVER['PHP_AUTH_USER'] . "', " . $HTTP_POST_VARS["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}
		}

		/***************************************
		* Kataloginfos
		****************************************/
		if(isset($HTTP_POST_VARS["txtMusterVorhanden"]))
		{
	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $HTTP_POST_VARS["txtMusterVorhanden_KEY"] . ",61,'" . $HTTP_POST_VARS["txtMusterVorhanden"] . "','" . $_SERVER['PHP_AUTH_USER'] . "', " . $HTTP_POST_VARS["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}
		}
		
		if(isset($HTTP_POST_VARS["txtVerpackungsmenge"]))			// Verpackungsmenge
		{
	        $SQL = "BEGIN AWIS.AENDERN_ARTIKELSTAMMINFOS(" . $HTTP_POST_VARS["txtVerpackungsmenge_KEY"] . ",60,'" . $HTTP_POST_VARS["txtVerpackungsmenge"] . "','" . $_SERVER['PHP_AUTH_USER'] . "', " . $HTTP_POST_VARS["txtAST_KEY"] . "); END;";
			$Erg = awisExecute($con, $SQL);
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
				die();
			}
		}
		
		/**************************************
		* LieferantenArtikel
		**************************************/
		
		if(isset($HTTP_POST_VARS["txtLAR_LARTNR_0"]) AND !isset($HTTP_POST_VARS['cmdSpeichernAbbruch']) AND $HTTP_POST_VARS["txtLAR_LIE_NR"]!='')
		{
			$rsLAR = awisOpenRecordset($con, "SELECT * FROM AWIS.LieferantenArtikel WHERE LAR_LARTNR='" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "' AND LAR_LIE_NR='" . $HTTP_POST_VARS["txtLAR_LIE_NR"] . "'");
			$rsLARZeilen = $awisRSZeilen;

			if($rsLARZeilen==0)				// Nicht gefunden
			{
				// Lieferant suchen
				$rsLIE = awisOpenRecordset($con, "SELECT * FROM AWIS.Lieferanten WHERE LIE_NR='" . $HTTP_POST_VARS["txtLAR_LIE_NR"] . "'");
				if($awisRSZeilen==0)
				{
					print "<span class=HinweisText>Der Lieferant " . $HTTP_POST_VARS["txtLAR_LIE_NR"] . " ist nicht vorhanden.</span>";
					print "<br><br>Bitte korrigieren Sie Ihre Eingabe.";
					print "<br><br><img src=/bilder/zurueck.png onclick=history.back();>";
				}
				else		// Lieferant vorhanden
				{
					if(isset($HTTP_POST_VARS["cmdSpeichernBestaetigung"]))
					{
						$SQL = "INSERT INTO AWIS.LieferantenArtikel(LAR_LARTNR, LAR_LIE_NR, LAR_IMQ_ID, LAR_USER, LAR_USERDAT, LAR_BEKANNTWW)";
						$SQL .= " VALUES (";
						$SQL .= "'" . $HTTP_POST_VARS['txtLAR_LARTNR_0'] . "',";
						$SQL .= "'" . $HTTP_POST_VARS["txtLAR_LIE_NR"] . "', ";
						$SQL .= "4, ";
						$SQL .= "'" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE, 0";
						$SQL .=" )";
						$Erg = awisExecute($con, $SQL);
						if($Erg==FALSE)
						{
							awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
							die();
						}

						$SQL = "SELECT LAR_KEY FROM AWIS.LieferantenArtikel WHERE LAR_LARTNR='";
						$SQL .= $HTTP_POST_VARS['txtLAR_LARTNR_0'] . "' AND LAR_LIE_NR='";
						$SQL .= $HTTP_POST_VARS["txtLAR_LIE_NR"] . "' ";
						$rsLAR = awisOpenRecordset($con, $SQL);
						if($rsLAR==FALSE)
						{
							awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
							die();
						}
						
						$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
						$SQL .= "VALUES('AST'," . $HTTP_POST_VARS["txtAST_KEY"] . ", 'LAR', " . $rsLAR['LAR_KEY'][0] .  ", ASCIIWORT('" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'), '" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'" ;
						$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
						$SQL .= ")";
						$Erg = awisExecute($con, $SQL);
						if($Erg==FALSE)
						{
							awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
							die();
						}
					}
					else	// Hinzuf�gen best�tigen
					{
						print "<form name=frmArtikel method=post>";
			
						print "<input type=hidden name=txtLAR_LARTNR_0>";
						print "<input type=hidden name=cmdSpeichern>";
						print "<input type=hidden name=txtAST_KEY value='" . $HTTP_POST_VARS["txtAST_KEY"] . "'>";
						print "<input type=hidden name=txtLAR_KEY value='" . $rsLAR['LAR_KEY'][0] . "'>";

						print "<table border=0>";
						print "<tr><td>Lieferantenartikelnummer</td><td><input type=text name=txtLAR_LARTNR_0 value=" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "></td></tr>";
						print "<tr><td>Lieferant</td><td><input type=text name=txtLAR_LIE_NR value=" . $HTTP_POST_VARS["txtLAR_LIE_NR"] . "></td></tr>";
						print "</table>";
			
						print "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie die die neuen Lieferantenartikel anlegen wollen?</span><br><br>";
						print "<input type=submit value=\"Ja, hinzuf�gen\" name=cmdSpeichernBestaetigung>";
						print "<input type=submit value=\"Nein, nicht hinzuf�gen\" name=cmdSpeichernAbbruch>";
						
						print "</form>";
						die();
					}	// Best�tigung?
				}		//Lieferant vorhanden?
			}
			else		// Lieferantenartikel vorhanden?
			{
				if(!isset($HTTP_POST_VARS["cmdSpeichernBestaetigung"]))
				{
					$SQL = "SELECT AST_ATUNR, TEI_KEY1 FROM AWIS.TeileInfos, AWIS.Artikelstamm";
					$SQL .= " WHERE TEI_ITY_ID2='LAR' AND TEI_KEY2 = " . $rsLAR["LAR_KEY"][0];
					$SQL .= " AND TEI_KEY1 = AST_KEY";
					
					$rsCheck = awisOpenRecordset($con, $SQL);
					if($awisRSZeilen>0)
					{
						print "<form name=frmArtikel method=post>";
			
						print "<input type=hidden name=txtLAR_LARTNR_0>";
						print "<input type=hidden name=cmdSpeichern>";
						print "<input type=hidden name=txtAST_KEY value='" . $HTTP_POST_VARS["txtAST_KEY"] . "'>";
						print "<input type=hidden name=txtLAR_KEY value='" . $rsLAR['LAR_KEY'][0] . "'>";

						print "<span class=HinweisText>Es existiert bereits eine Zuordnung f�r diesen Lieferantenartikel!</span>"	;
						print "<table border=0>";
						print "<tr><td>Lieferantenartikelnummer</td><td><input type=text name=txtLAR_LARTNR_0 value='" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'></td></tr>";
						print "<tr><td>Lieferant</td><td><input type=text name=txtLAR_LIE_NR value=" . $HTTP_POST_VARS["txtLAR_LIE_NR"] . "></td></tr>";
						print "<tr><td>ATU Artikel</td><td><input type=hidden readonly name=txtLAR_AST_ATUNR value=" . $rsCheck["AST_ATUNR"][0] . ">" . $rsCheck["AST_ATUNR"][0] . "</td></tr>";
						print "</table>";
			
						print "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie die die neuen Lieferantenartikel zu diesem Artikel zuweisen wollen?</span><br><br>";
	
						print "<input type=submit value=\"Ja, hinzuf�gen\" name=cmdSpeichernBestaetigung>";
						print "<input type=submit value=\"Nein, nicht zuweisen\" name=cmdSpeichernAbbruch>";
						
						print "</form>";
						die();
					}
					else
					{
						$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
						$SQL .= "VALUES('AST'," . $HTTP_POST_VARS["txtAST_KEY"] . ", 'LAR', " . $rsLAR["LAR_KEY"][0] .  ", ASCIIWORT('" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'), '" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'" ;
						$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
						$SQL .= ")";
					
						unset($rsCheck);
						awisExecute($con, $SQL);
						
					}
				}
							
				if(isset($HTTP_POST_VARS["cmdSpeichernBestaetigung"]))
				{
					$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
					$SQL .= "VALUES('AST'," . $HTTP_POST_VARS["txtAST_KEY"] . ", 'LAR', " . $rsLAR["LAR_KEY"][0] .  ", ASCIIWORT('" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'), '" . $HTTP_POST_VARS["txtLAR_LARTNR_0"] . "'" ;
					$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
					$SQL .= ")";
				
					awisExecute($con, $SQL);
				}
			}
		    unset($rsLAR);
			unset($rsLARZeilen);
		}

		/**************************************
		* EAN Nummer
		**************************************/
		
		if(isset($HTTP_POST_VARS["txtEAN_KEY_0"]))
		{
			$rsEAN = awisOpenRecordset($con, "SELECT * FROM AWIS.EANNummern WHERE EAN_NUMMER='" . $HTTP_POST_VARS["txtEAN_Nummer"] . "'");
			$rsEANZeilen = $awisRSZeilen;

			if($rsEANZeilen>0)
			{
				$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
				$SQL .= "VALUES('AST'," . $HTTP_POST_VARS["txtAST_KEY"] . ", 'EAN', " . $rsEAN["EAN_KEY"][0] .  ", ASCIIWORT('" . $HTTP_POST_VARS["txtEAN_Nummer"] . "'), '" . $HTTP_POST_VARS["txtEAN_Nummer"] . "'" ;
				$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
				$SQL .= ")";
			
				awisExecute($con, $SQL);
					
			}
			elseif($HTTP_POST_VARS["txtEAN_Nummer"]!='')
			{
				print "<span class=HinweisText>EAN Nummer " . $HTTP_POST_VARS["txtEAN_Nummer"] . "nicht gefunden</span>";
				
				print "<br><br>";

				die();
			}
		}
		
		/***************************************
		* OE Nummern
		****************************************/

		if(isset($HTTP_POST_VARS["txtOEN_KEY_0"]))
		{
			$rsOE = awisOpenRecordset($con, "SELECT * FROM AWIS.OENummern WHERE OEN_NUMMER='" . $HTTP_POST_VARS["txtOEN_NUMMER"] . "' AND OEN_HER_ID=0" . $HTTP_POST_VARS["txtOEN_HER_ID"]);
			$rsOEZeilen = $awisRSZeilen;

			if($rsOEZeilen>0)
			{
				$SQL = "INSERT INTO AWIS.TeileInfos(TEI_ITY_ID1, TEI_KEY1, TEI_ITY_ID2, TEI_KEY2, TEI_SUCH2, TEI_WERT2, TEI_USER, TEI_USERDAT)";
				$SQL .= "VALUES('AST'," . $HTTP_POST_VARS["txtAST_KEY"] . ", 'OEN', " . $rsOE["OEN_KEY"][0] .  ", ASCIIWORT('" . $HTTP_POST_VARS["txtOEN_NUMMER"] . "'), '" . $HTTP_POST_VARS["txtOEN_Nummer"] . "'" ;
				$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "', SYSDATE";
				$SQL .= ")";
			
				awisExecute($con, $SQL);
			}
			elseif($HTTP_POST_VARS["txtOEN_NUMMER"]!='')
			{
				print "<span class=HinweisText>OE Nummer " . $HTTP_POST_VARS["txtOEN_NUMMER"] . " f�r den Hersteller " . $HTTP_POST_VARS["txtOEN_HER_ID"] . " nicht gefunden</span>";
				
				print "<br><br>";

				die();
			}

		}
		
	}  // Ende Speichern von Daten

}
				//***********************************************************
elseif(!isset($HTTP_GET_VARS["Key"]) AND !isset($HTTP_GET_VARS["ATUNR"]))			// Artikel suchen (Liste anzeigen, oder einen)
				//***********************************************************
{
	$DSAnz=0;		// Insgesamt gelesene Zeilen

	if($HTTP_GET_VARS['Export']=='True')		// Export l�uft
	{
		print 'Daten werden vorbereitet...';
		flush();
	}
	else
	{
		if(($RechteStufe & 256) == 256)		// Export erlaubt
		{
			if($Param[11]=='')		// Nicht bei Suche nach beliebiger Nummer
			{
				print " <input border=0 type=image accesskey=X alt='Export (Alt+X)' src=/bilder/tabelle.png name=cmdExport onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Export=True&Liste=True'>";
			}
			@unlink(awis_UserExportDateiName('.csv'));
		}
		if(($BearbeitungsStufe&4)==4)		// Neuen Artikel hinzuf�gen
		{
			echo "<a accesskey=n href=./artikel_Main.php?cmdAktion=ArtikelInfos&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
		}

		// �berschrift
		print "<table  width=100% id=DatenTabelle border=1>";
	
		if($Param[1]!='')
		{
			print "\n<tr>";	
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>ATUNR</span></td>";
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>BEZEICHNUNG</span></td>";
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>WWS</span></td>";
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>Cross</span></td>";
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>VK</span></td>";
			print "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett>Bestand Lager</span></td>";
			print "\n<td id=FeldBez width=80><span class=DatenFeldNormalFett>Bestand Filialen</span></td>";
			print "\n<td id=FeldBez><span class=DatenFeldNormalFett>WG</span></td>";
			print "</tr>\n";
		}
	}

	$rsArtikel='';
	$rsArtikelZeilen=0;

	// Reine ATU-Artikel
    $Bedingung = '';
	if($Param[1]!='')			// ATU-Nummer
	{
		$Bedingung = " AND AST_ATUNR " . awisLIKEoderIST($Param[1], True, False, False);
	}
	if($Param[2]!='')			// Artikel-Bezeichnung
	{
		$Bedingung .= " AND ((AST_BEZEICHNUNGWW) " . awisLIKEoderIST($Param[2], True, False, false);
		$Bedingung .= " OR AST_BEZEICHNUNG " . awisLIKEoderIST($Param[2], True, False, false) . ")";
	}
	if($Param[10]!='')			// txtKommentar
	{
		$Bedingung .= " AND AST_ATUNR IN (SELECT ASI_AST_ATUNR FROM AWIS.ArtikelStammInfos where ASI_AIT_ID IN (110,111) AND UPPER(ASI_WERT) " . awisLIKEoderIST($Param[10],TRUE) . ")";
	}
	
	if($Bedingung!='')
	{
		$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'AST' AS FUNDSTELLE ";
		$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN ";

		$SQL = $SQL . " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID)" . $Bedingung;
		$rsArtikel = awisOpenRecordset($con, $SQL);
		if($rsArtikel==FALSE)
		{
			awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
			die();
		}
		$rsArtikelZeilen = $awisRSZeilen;
		$DSAnz += $awisRSZeilen;

		_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
		flush();
	}
	
	if($Param[3]!='')		// Sonstige Nummern
	{
		if($Param[4]!='')		// EAN-Nummern
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'EAN' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";

			$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND ((TEI_ITY_ID2 = 'EAN' ";
			$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, TRUE) . ")";
			$SQL .= " AND (TEI_KEY1 = AST_KEY))";		// Verkn�pfung zur TeileInfo
			$SQL .= "  ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
			}
			flush();
		}

		if($Param[5]!='')		// Gebrauchsnummern
		{
			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$Bedingung = " AND TEI_SUCH2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";
			}
			else
			{
				$Bedingung = " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}

			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'GNR' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND ((TEI_ITY_ID2 = 'EAN' ";
			$SQL .= " AND (TEI_KEY1 = AST_KEY))";		// Verkn�pfung zur TeileInfo
			$SQL .= $Bedingung . " ORDER BY AST_ATUNR";
			
			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
				flush();
			}
		}
		
		if($Param[6]!='')		// Lieferantenartikelnummer
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'LAR' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND ((TEI_ITY_ID2 = 'LAR' ";
			$SQL .= " AND (TEI_KEY1 = AST_KEY))";		// Verkn�pfung zur TeileInfo

			if($Param[7]!='on')
			{
				$Bedingung = " AND TEI_SUCH2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";
			}
			else
			{
				$Bedingung = " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}

			$SQL .= $Bedingung . " ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
				flush();
			}
		}

		if($Param[8]!='')		// Lieferantenartikelsets
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'LAS' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (TEI_KEY1 = AST_KEY) AND (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND (TEI_ITY_ID1='AST' AND TEI_KEY2 IN (SELECT TEI_KEY1 FROM TEILEINFOS WHERE (TEI_ITY_ID2 = 'LAS' ";

			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " AND UPPER(TEI_SUCH2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";
			}
			else
			{
				$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}
			$SQL .= ")) ORDER BY AST_ATUNR";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
				flush();
			}
		}	// Lieferantenartikelsets
		

		if($Param[9]!='')		// OE-Nummern
		{
			$SQL = "SELECT DISTINCT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'OEN' AS FUNDSTELLE ";
			$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN, AWIS.TeileInfos ";
			$SQL .= " WHERE (TEI_KEY1 = AST_KEY) AND ";
			$SQL .= " (AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID) AND (TEI_ITY_ID2 = 'OEN' ";

			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " AND UPPER(TEI_SUCH2) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . ")";
			}
			else
			{
				$SQL .= " AND TEI_WERT2 " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . ")";
			}
			$SQL .= "";

			if(connection_status()==0)		//Verbindung steht noch
			{
				$rsArtikel = awisOpenRecordset($con, $SQL);
				if($rsArtikel==FALSE)
				{
					awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
					die();
				}
				$rsArtikelZeilen = $awisRSZeilen;
				$DSAnz += $awisRSZeilen;
				_ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$HTTP_GET_VARS['Export']);
				flush();
			}
		}	// Lieferantenartikelsets

		if($Param[11]!='')		// Beliebige Nummer
		{
//			$DSAnz=0;
			// EAN Nummern
			echo '<table  width=500 id=DatenTabelle border=1>';
			$SQL = "SELECT * FROM EANNummern";
			if($Param[7]!='on')
			{
				$SQL .= " WHERE EAN_NUMMER " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			}
			else
			{
				$SQL .= " WHERE ASCIIWORTOE(EAN_NUMMER) " . awisLIKEoderIST($Param[3], True, TRUE, FALSE, 1) . "";
			}
			$rsSuche = awisOpenRecordset($con, $SQL);

			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>Typ</td></tr>';
			}

			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td><a href=./artikel_Main.php?cmdAktion=EANArtikel&EANKEY=' . $rsSuche['EAN_KEY'][$DSNr] . '>' . $rsSuche['EAN_NUMMER'][$DSNr] . '</a></td>';
				echo '<td width=150 >EAN-Nummer</td>';
				echo '<td width=200 >' . $rsSuche['EAN_TYP'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);
			
			// Gebrauchnsnummer
			$SQL = "SELECT * FROM GebrauchsNummern WHERE GNR_Nummer='" . $Param[3] . "'";

			// Lieferantenartikel-Nummer
			$SQL = "SELECT * FROM LieferantenArtikel";
			if($Param[7]!='on')
			{
				$SQL .= " WHERE LAR_LARTNR " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			}
			else
			{
				$SQL .= " WHERE ASCIIWORTOE(LAR_LARTNR) " . awisLIKEoderIST($Param[3], True, TRUE, FALSE, 1) . "";
			}
			$rsSuche = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>Lieferant</td></tr>';
			}
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td width=150 ><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=' . $rsSuche['LAR_KEY'][$DSNr] . '>' . $rsSuche['LAR_LARTNR'][$DSNr] . '</a></td>';
				echo '<td width=150 >Lief-Art</td>';
				echo '<td width=200 >' . $rsSuche['LAR_LIE_NR'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);

			
			
			// Lieferantenartikel-Sets
			$SQL = "SELECT * FROM LieferantenArtikelSets WHERE LAS_LARTNR='" . $Param[3] . "'";

			// OE-Nummern
			$SQL = "SELECT * FROM OENummern, Hersteller ";
			if($Param[7]!='on')			// NICHT Exakte suche
			{
				$SQL .= " WHERE UPPER(OEN_SUCHNUMMER) " . awisLIKEoderIST($Param[3], True, False, FALSE, 1) . "";
			}
			else
			{
				$SQL .= " WHERE OEN_NUMMER " . awisLIKEoderIST($Param[3], True, False, FALSE, 0) . "";
			}
			$SQL .= ' AND OEN_HER_ID = HER_ID(+)';
			
			$rsSuche = awisOpenRecordset($con, $SQL);
			if($awisRSZeilen>0)
			{
				echo '<tr><td width=150 id=FeldBez>Nummer</td><td width=150 id=FeldBez>Quelle</td><td width=200 id=FeldBez>???</td></tr>';
			}
			for($DSNr=0;$DSNr<$awisRSZeilen;$DSNr++)
			{
				echo '<tr>';
				echo '<td width=150 ><a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY=' . $rsSuche['OEN_KEY'][$DSNr] . '>' . $rsSuche['OEN_NUMMER'][$DSNr] . '</td>';
				echo '<td width=150 >OE-NUmmer</td>';
				echo '<td width=200 >' . $rsSuche['HER_BEZEICHNUNG'][$DSNr] . '</td>';
				echo '</tr>';
			}
			$DSAnz += $DSNr;
			unset($rsSuche);
		}
	}

			// Export ist aktiv -> Dateidownlad anzeigen	
	if($HTTP_GET_VARS['Export']=='True')
	{
		echo "<br><br><a accesskey=x onmouseover=\"status='�ffnen';return true;\" onmouseout=\"status='AWIS';return true;\" href=/export/" . $_SERVER['PHP_AUTH_USER'] . ".csv><img src=/bilder/dateioeffnen.png border=0 alt='�ffnen (Alt+X)'></a><br>";
		echo "<br><input type=image border=0 src=/bilder/NeueListe.png accesskey=t alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'>";
	}
	else
	{
		print "</table>";
	}

	
	print "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $DSAnz Artikel gefunden.</font>";
}
//*****************************************************
//
// Einzelner Datensatz -> Formular anzeigen
//
//*****************************************************

if($DSAnz==-1)  //$rsArtikelZeilen==1)	
{
	print "<form name=frmArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $_SERVER['PHP_AUTH_USER']));

	$SQL = "SELECT ARTIKELSTAMM.*, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG ";
	$SQL .= " FROM AWIS.ARTIKELSTAMM, AWIS.WARENUNTERGRUPPEN, AWIS.WARENGRUPPEN ";

	if(isset($HTTP_GET_VARS["ATUNR"]))	// ATUNR als Parameter angegeben
	{
		$SQL .= " WHERE AST_ATUNR='" . $HTTP_GET_VARS["ATUNR"] . "'";
	}
	else								// ASTKEY als Parameter angegeben
	{
		$SQL .= " WHERE AST_KEY=0" . $ASTKEY;
	}
	$SQL .= " AND AST_WUG_KEY = WUG_KEY AND WGR_ID = WUG_WGR_ID";
	
	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;
	if($rsArtikel===FALSE)
	{
		awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
		die();
	}

	$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID, ASI_KEY FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][0] . "'");
	$rsASIZeilen = $awisRSZeilen;

	if($rsASI===FALSE)
	{
		awisErrorMailLink("artikel_Artikel.php", 2, $awisDBFehler);
		die();
	}

	if($rsArtikelZeilen<1)			// Keine Daten gefunden
	{
		print "<span class=HinweisText>Mit den angegebenen Kriterien konnte leider kein Artikel gefunden werden.</span>";
	}
	else
	{	
		If(substr($rsArtikel['AST_ATUNR'][0],0,2)=='CA' OR substr($rsArtikel['AST_ATUNR'][0],0,2)=='CB')
		{
			$SuchArt = (substr($rsArtikel['AST_ATUNR'][0],0,2)=='CA'?'CB' . substr($rsArtikel['AST_ATUNR'][0],2):'CA' . substr($rsArtikel['AST_ATUNR'][0],2));
			$rsCACB=awisOpenRecordset($con, "SELECT AST_KEY, AST_ATUNR FROM AWIS.ArtikelStamm WHERE AST_ATUNR ='" . $SuchArt . "'");
		}

		print "<input type=hidden name=txtAST_ATUNR value='" . $rsArtikel['AST_ATUNR'][0] . "'>";
		print "<table border=0 width=100%>";
		print "<tr><td><font size=4><b>ATU Artikel " . ($rsArtikel['AST_BEKANNTWW'][0]==1?"<i>":"") . $rsArtikel['AST_ATUNR'][0];
		echo '</i></b>';
		if($rsCACB['AST_ATUNR'][0]!='')
		{
			echo ' (<a href=./artikel_Main.php?Key=' . $rsCACB['AST_KEY'][0] . '&cmdAktion=ArtikelInfos>' . $rsCACB['AST_ATUNR'][0] . '</a>)';
		}
		echo '</font></td>';
		print "<td width=100><font size=1>" . $rsArtikel["AST_USER"][0] ."<br>"  . $rsArtikel["AST_USERDAT"][0] . "</font></td>";
//		print "<td width=30 backcolor=#000000 align=right><img border=0 src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'></td>";
		print "</tr></table>";


			// Zeile 1

		print "<table border=1 width=100% >";
		print "<colgroup><col width=110><col width=100> <col width=90><col width=*> <col width=30><col width=30> <col width=30><col width=30> <col width=50><col width=40><col width=40></colgroup>";
		print "<tr>";
		print "<td align=right id=FeldBez>ATU-Nr</td><td id=FeldHervorgehoben><input type=hidden name=txtAST_KEY value=". $rsArtikel['AST_KEY'][0] . ">" . $rsArtikel['AST_ATUNR'][0] . "</td>";
		print "<td align=right id=FeldBez>Bez-WWS</td><td id=FeldHervorgehoben>" . ($rsArtikel['AST_BEZEICHNUNGWW'][0]==''?'- -':$rsArtikel['AST_BEZEICHNUNGWW'][0]) . "</td>";
		print "<td align=right id=FeldBez>WG</td><td id=FeldHervorgehoben>" . $rsArtikel['WGR_ID'][0] . "</td>";
		print "<td align=right id=FeldBez>UWG</td><td id=FeldHervorgehoben>" . $rsArtikel['WUG_ID'][0] . "</td>";
		print "<td align=right id=FeldBez>KENN1</td><td id=FeldHervorgehoben>" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "</td>";

		print "<td backcolor=#000000 align=right><img border=0 src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick='location.href=./artikel_Main.php?cmdAktion=ArtikelInfos&Liste=True'></td>";

		print "</tr>";	
		print "</table>";

			// Zeile 2

		print "<table border=1 width=100% >";
		print "<colgroup><col width=110><col width=*> <col width=90><col width=70> <col width=100><col width=200> </colgroup>";
		print "<tr>";

		$SQL = "SELECT DISTINCT KFZ_HERST.BEZ AS Hersteller FROM ARTIKEL_KTYPNR_AWST, KFZ_TYP, KFZ_MODELL, KFZ_HERST  ";
		$SQL .= " WHERE ARTIKEL_KTYPNR_AWST.KTYPNR = KFZ_TYP.KTYPNR AND KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR AND KFZ_HERST.KHERNR = KFZ_MODELL.KHERNR";
		$SQL .= " AND ATU_NR='" . $rsArtikel['AST_ATUNR'][0] . "'";
		
		$rsFahrzeuge = awisOpenRecordset($con, $SQL);
		$Fahrzeuge='';
		for($FzNr=0;$FzNr<$awisRSZeilen;$FzNr++)
		{
			$Fahrzeuge .= ', ' . substr($rsFahrzeuge['HERSTELLER'][$FzNr],0,4);
		}
		print "<td align=right id=FeldBez>Fahrzeuge</td><td id=FeldHervorgehoben>" . substr($Fahrzeuge,2) . "</td>";

		unset($rsFahrzeuge);
		
		print "<td align=right id=FeldBez>Hauptlief.</td><td id=FeldHervorgehoben><a href=/stammdaten/lieferanten.php?LIENR=" . $LiefNr . "&Zurueck=artikel_LIEF&ASTKEY=" . $rsArtikel["AST_KEY"][0] . ">" . awis_ArtikelInfo(40, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
		print "<td align=right id=FeldBez>Lief-Art-Nr.</td><td id=FeldHervorgehoben>" . awis_ArtikelInfo(41, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
		print "</tr>";	
		print "</table>";

			// Zeile 3
			
		print "<table border=1 width=100% >";
		print "<colgroup><col width=110><col width=*><col width=50><col width=80> </colgroup>";
		print "<tr>";
		if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Alternativbezeichung
		{
			print "<td align=right id=FeldBez>Bez.-Katalo<u>g</u></td>";		
			print "<td><input type=text accesskey=g size=90 maxlength=100 name=txtAST_BEZEICHNUNG value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0] . "\">";
			print "<input type=hidden name=txtAST_BEZEICHNUNG_old value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0]. "\"></td>";
		}
		else
		{
			print "<td align=right id=FeldBez>Bez.-Katalog</td><td>" . $rsArtikel['AST_BEZEICHNUNG'][0] . "</td>";
		}

		print "<td align=right id=FeldBez>VK</td><td id=FeldHervorgehoben>" . awis_format($rsArtikel['AST_VK'][0],"Currency") . "</td>";
		print "</tr>";	
		print "</table>";


		
			// Teil 2: Block aus drei Spalten
		print "<table border=1 width=100% >";
		print "<colgroup><col width=45%><col width=40%><col width=15%></colgroup>";
		print "<tr>";

			// Erste Spalte		
		echo '<td valign=top>';
			// Zeile A1
			
		print "<table border=1 width=100% >";
		print "<colgroup><col width=*><col width=200><col width=250><col width=100></colgroup>";
		print "<tr>";
		$Anz = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if($HTTP_GET_VARS['AltArt']=='Ja' OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			foreach($ASTs AS $Eintrag=>$Wert)
			{
				$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
			}
			print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ</a></td><td>" . substr($Erg,4) . "</td>";
		}
		else
		{
			print "<td id=FeldBez><a href=./artikel_Main.php?AltArt=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ</a></td><td>" . $Anz . "</td>";
		}

		if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Kennung
		{
			print "<td height=26 id=FeldBez>KENN-Vorschlag</td>";		
			print "<td><input type=text size=2 name=txtAST_KENNUNGVORSCHLAG value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\">";
			print "<input type=hidden name=txtAST_KENNUNGVORSCHLAG_old value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\"></td>";
		}
		else
		{
			print "<td height=26 id=FeldBez>KENN-Vorschlag</td><td>" . ($rsArtikel['AST_KENNUNGVORSCHLAG'][0]==''?'--':$rsArtikel['AST_KENNUNGVORSCHLAG'][0]) . "</td>";
		}
		print "</tr>";

				// Zeile A2
		print "<tr>";
			// Zusatzinfos: ZUBEH�R
		$Anz = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if($HTTP_GET_VARS['Zubehoer']=='Ja' OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			foreach($ASTs AS $Eintrag=>$Wert)
			{
				$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
			}
			print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r</a></td><td>" . substr($Erg,4) . "</td>";
		}
		else
		{
			print "<td id=FeldBez><a href=./artikel_Main.php?Zubehoer=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r</a></td><td>" . $Anz . "</td>";
		}

		print "<td height=26 id=FeldBez>Muster vorhanden</td><td>";
		$Wert = (awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT")==0?'Nein':'Ja');
		if(($BearbeitungsStufe & 16) == 16)		// Katalogfelder: Muster vorhanden
		{
			$Wert = awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT");
			print "<input name=txtMusterVorhanden_KEY type=hidden value=0" . awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_KEY") . ">";
			print "<select name=txtMusterVorhanden>";
			print "<option " . ($Wert!=0?'selected':'') . " value=-1>Ja</option>";
			print "<option " . ($Wert==0?'selected':'') . " value=0>Nein</option>";
			print "</select></td>";
		}
		else
		{
			print $Wert . "</td>";
		}
		print "</tr>";

				// Zeile A3
		print "<tr>";

		print "<td id=FeldBez>VPE</td><td>";
		print awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "</td>";

		$Mldg = awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
		if(($BearbeitungsStufe & 8) == 8)
		{
			print "<td height=26 id=FeldBez>an Ein<u>k</u>auf am</td><td><input size=6 accesskey=k type=text name=txtMELDUNG value=\"" . awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "\"><input type=hidden name=txtMELDUNG_KEY Value=0" . $Mldg . "></td>";
		}
		else		// Keine Bearbeitung erlaubt
		{
			print "<td height=26 id=FeldBez>an Einkauf am</td><td>" . ($Mldg==''?"n.v.":$Mldg) . "</td>";
		}
		
			
		echo '</tr></table>';

		echo '</td>';
			// Zweite Spalte
		echo '<td valign=top >';

				// Spalte B1		
		print "<table border=1 width=100% >";
		print "<colgroup><col width=25%><col width=25%><col width=25%><col width=25%></colgroup>";
		print "<tr>";
		print "<td colspan=2 align=center id=FeldBez><font size=2>Absatz</font></td>";
		print "<td colspan=2 align=center id=FeldBez><font size=2>Reklamationen</font></td>";
		echo '</tr>';
		echo '<tr>';
		$Wert = awis_ArtikelInfo(33, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		$Wert += awis_ArtikelInfo(34, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
		print "<td  id=FeldBez>Vorjahr</td><td id=FeldHervorgehoben>" . awis_format($Wert,'Standardzahl,0') . "</td>";
		$Wert = awis_ArtikelInfo(9999, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		print "<td id=FeldBez>Vorjahr</td><td id=FeldHervorgehoben>" . awis_format($Wert,'Standardzahl,0') . "</td>";
		echo '</tr>';
		echo '<tr>';
		
		$Wert = awis_ArtikelInfo(33, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		$Wert += awis_ArtikelInfo(34, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
		print "<td  id=FeldBez>Jahr</td><td id=FeldHervorgehoben>" . awis_format($Wert,'Standardzahl,0') . "</td>";
		
		$Wert = awis_ArtikelInfo(9999, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		print "<td id=FeldBez>Jahr</td><td id=FeldHervorgehoben>" . awis_format($Wert,'Standardzahl,0') . "</td>";

		echo '</tr>';
		echo '<tr>';
		print "<td height=26 id=FeldBez>durch</td><td colspan=3>";

		if(($BearbeitungsStufe & 8) == 8)
		{
			print "<input type=hidden name=txtMELDUNGDURCH_KEY Value=0" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_KEY") . ">";
			print "<select name=txtMELDUNGDURCH>";
			$rsAPP = awisOpenRecordset($con, "SELECT * FROM ArtikelPruefPersonal ORDER BY APP_NAME");
			$APP = awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
			for($APPNr=0;$APPNr<$awisRSZeilen;$APPNr++)
			{
				if($rsAPP["APP_ID"][$APPNr]==$APP OR $rsAPP["APP_AKTIV"][$APPNr]=='')
				{
					print "<option  ";
					if($APP == $rsAPP["APP_ID"][$APPNr])
					{
						print " selected ";
					}
					print " value=" . $rsAPP["APP_ID"][$APPNr] . ">" . $rsAPP["APP_NAME"][$APPNr] . "</option>";
				}
			}
			print "</select></td>";
		}
		else		// Keine Bearbeitung erlaubt
		{
			print "" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
		}
				
		echo '</tr></table>';
		
		echo '</td>';

			// Dritte Spalte
		echo '<td valign=top>';


		print "<table border=1 width=100% >";
		print "<colgroup><col width=50%><col width=50%></colgroup>";
		print "<tr>";
		print "<td colspan=2 align=center id=FeldBez><font size=2>Bestand</font></td></td>";

		print "</tr><tr>";
		print "<td id=FeldBez>Weiden</td><td  id=FeldHervorgehoben>" . awis_format(awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";

		print "</tr><tr>";
		print "<td id=FeldBez>Werl</td><td id=FeldHervorgehoben>" . awis_format(awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";

		print "</tr><tr>";
		print "<td id=FeldBez>Filialen</td><td id=FeldHervorgehoben>" . awis_format(awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";

		echo '</tr></table>';

		echo '</td>';
		
		print "</tr>";	
		print "</table>";
		
		
		
		
		

	
			// Zeile 1
/*		print "<table border=1 width=100% >";
		print "<colgroup><col width=85><col width=*><col width=110><col width=*><col width=100><col width=*><col width=100><col width=*></colgroup>";
		print "<tr>";
		print "<td id=FeldBez>ATU-Nr</td><td><input type=hidden name=txtAST_KEY value=". $rsArtikel['AST_KEY'][0] . ">" . $rsArtikel['AST_ATUNR'][0] . "</td>";
		print "<td id=FeldBez>Bezeichnung</td><td>" . ($rsArtikel['AST_BEZEICHNUNGWW'][0]==''?'- -':$rsArtikel['AST_BEZEICHNUNGWW'][0]) . "</td>";
		print "<td id=FeldBez>Warengruppe</td><td>" . $rsArtikel['WGR_ID'][0] . "/" . $rsArtikel['WGR_BEZEICHNUNG'][0] . "</td>";
		print "<td id=FeldBez>UWG</td><td>" . $rsArtikel['WUG_ID'][0] . "</td>";
		print "</tr>";	
		print "</table>";

			// Zeile1a
		print "<table border=1 width=100% >";
		print "<colgroup><col width=200><col><col width=150><col></colgroup>";
		print "<tr>";
	
		if(($BearbeitungsStufe & 2)==2)		// Bearbeitung der Alternativbezeichung
		{
			print "<td id=FeldBez>Alterna<u>t</u>ivbezeichnung</td>";		
			print "<td><input type=text accesskey=t size=85 name=txtAST_BEZEICHNUNG value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0] . "\">";
			print "<input type=hidden name=txtAST_BEZEICHNUNG_old value=\"" . $rsArtikel['AST_BEZEICHNUNG'][0]. "\"></td>";
	
			print "<td id=FeldBez>KENN-Vorschlag:</td>";		
			print "<td><input type=text size=2 name=txtAST_KENNUNGVORSCHLAG value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\">";
			print "<input type=hidden name=txtAST_KENNUNGVORSCHLAG_old value=\"" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "\"></td>";
		}
		else
		{
			print "<td id=FeldBez>Alternativbezeichnung</td><td>" . $rsArtikel['AST_BEZEICHNUNG'][0] . "</td>";
			print "<td id=FeldBez>KENN-Vorschlag</td><td>" . $rsArtikel['AST_KENNUNGVORSCHLAG'][0] . "</td>";
		}
		print "</tr>";
		print "</table>";

	
			// Zeile 2
		print "<table border=1 width=100% >";
		print "<colgroup><col width=55><col><col width=120><col><col width=180><col><col width=30><col></colgroup>";
		print "<tr>";
		print "<td id=FeldBez>Kenn1</td><td width=30>" . $rsArtikel['AST_KENNUNG'][0] . "</td>";
		
		$LiefNr = awis_ArtikelInfo(40, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
		print "<td id=FeldBez>Hauptlieferant</td><td ><a href=/stammdaten/lieferanten.php?LIENR=" . $LiefNr . "&Zurueck=artikel_LIEF&ASTKEY=" . $rsArtikel["AST_KEY"][0] . ">$LiefNr</td>";
		print "<td id=FeldBez>Lieferantenartikelnr</td><td>" . awis_ArtikelInfo(41, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
		print "<td id=FeldBez>VK</td><td>" . awis_format($rsArtikel['AST_VK'][0],"Currency") . "</td>";
		print "</tr>";	
		print "</table>";
	
			// Zeile 3
		print "<table border=1 width=100% >";
		print "<tr>";
		print "<td id=FeldBez>Bestand Weiden</td><td width=30>" . awis_format(awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";
		print "<td id=FeldBez>Bestand Werl</td><td width=30>" . awis_format(awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";
		print "<td id=FeldBez>Bestand Filialen</td><td width=30>" . awis_format(awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT"),"Standardzahl,0") . "</td>";
		$Bestand = awis_ArtikelInfo(30, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		$Bestand += awis_ArtikelInfo(31, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
		print "<td id=FeldBez>Lfd. Absatz</td><td>" . awis_format($Bestand,'Standardzahl,0') . "</td>";
		$Bestand = awis_ArtikelInfo(33, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Weiden
		$Bestand += awis_ArtikelInfo(34, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");		// Werl
		print "<td id=FeldBez>Absatz Vorjahr</td><td>" . awis_format($Bestand,'Standardzahl,0') . "</td>";
	
		print "</tr>";	
		print "</table>";
	
			// Zeile 4
		print "<table border=1 width=100% >";
		print "<colgroup><col width=200><col width=150><col width=100><col width=*></colgroup>";
		print "<tr>";
		if(($BearbeitungsStufe & 8) == 8)
		{
			print "<td id=FeldBez>Meldung an Ein<u>k</u>auf</td><td><input accesskey=k type=text name=txtMELDUNG value=\"" . awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "\"><input type=hidden name=txtMELDUNG_KEY Value=0" . awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_KEY") . "></td>";
			print "<td id=FeldBez>durch</td>";
			print "<input type=hidden name=txtMELDUNGDURCH_KEY Value=0" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_KEY") . ">";
			print "<td><select name=txtMELDUNGDURCH>";
			$rsAPP = awisOpenRecordset($con, "SELECT * FROM ArtikelPruefPersonal ORDER BY APP_NAME");
			$APP = awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
			for($APPNr=0;$APPNr<$awisRSZeilen;$APPNr++)
			{
				if($rsAPP["APP_ID"][$APPNr]==$APP OR $rsAPP["APP_AKTIV"][$APPNr]=='')
				{
					print "<option  ";
					if($APP == $rsAPP["APP_ID"][$APPNr])
					{
						print " selected ";
					}
					print " value=" . $rsAPP["APP_ID"][$APPNr] . ">" . $rsAPP["APP_NAME"][$APPNr] . "</option>";
				}
			}
			print "</select>";
		//	 value=\"" . awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "\"></td>";
		
		}
		else		// Keine Bearbeitung erlaubt
		{
			$Mldg = awis_ArtikelInfo(100, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT");
			print "<td id=FeldBez>Meldung an Einkauf</td><td>" . ($Mldg==''?"n.v.":$Mldg) . "</td>";
			print "<td id=FeldBez>durch</td>";
			print "<td>" . awis_ArtikelInfo(101, $rsASI, $rsArtikel["AST_ATUNR"][0], "ASI_WERT") . "</td>";
			
			unset($Mldg);
		}
		print "</tr>";	
	
		// Zeile 5
		print "<tr>";
	
		print "<td id=FeldBez>Muster vorhanden</td><td>";
		if(($BearbeitungsStufe & 16) == 16)		// Katalogfelder: Muster vorhanden
		{
			$Wert = awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT");
			print "<input name=txtMusterVorhanden_KEY type=hidden value=0" . awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_KEY") . ">";
			print "<select name=txtMusterVorhanden>";
			print "<option " . ($Wert!=0?'selected':'') . " value=-1>Ja</option>";
			print "<option " . ($Wert==0?'selected':'') . " value=0>Nein</option>";
			print "</select></td>";
		}
		else
		{
			$Wert = (awis_ArtikelInfo(61, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT")==0?'Nein':'Ja');
			print $Wert . "</td>";
		}
		
		print "<td id=FeldBez>Verpackungsmenge</td><td>";
		if(($BearbeitungsStufe & 16) == 16)		// Katalogfelder: Anzahl Teile
		{
			$Wert = awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT");
			print "<input name=txtVerpackungsmenge_KEY type=hidden value=0" . awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_KEY") . ">";
			print "<input type=text name=txtVerpackungsmenge size=5 value=$Wert>";
			print "</td>";
		}
		else
		{
			print awis_ArtikelInfo(60, $rsASI , $rsArtikel["AST_ATUNR"][0] , "ASI_WERT") . "</td>";
		}

			// Zusatzinfos: ALTERNATIVARTIKEL
		$Anz = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if($HTTP_GET_VARS['AltArt']=='Ja' OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(120, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			foreach($ASTs AS $Eintrag=>$Wert)
			{
				$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
			}
			print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ:</a></td><td>" . substr($Erg,4) . "</td>";
		}
		else
		{
			print "<td id=FeldBez><a href=./artikel_Main.php?AltArt=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Alternativ:</a></td><td>" . $Anz . "</td>";
		}

			// Zusatzinfos: ZUBEH�R
		$Anz = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "COUNT:ASI_WERT");
		if($HTTP_GET_VARS['Zubehoer']=='Ja' OR $Anz==1)
		{
			$Liste = awis_ArtikelInfo(121, $rsASI , $rsArtikel["AST_ATUNR"][0] , "LISTE:ASI_WERT");
			$ASTs = explode("<br>", $Liste);
			$Erg = '';
			foreach($ASTs AS $Eintrag=>$Wert)
			{
				$Erg .= '<br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $Wert . '>' . $Wert . '</a>';
			}
			print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r:</a></td><td>" . substr($Erg,4) . "</td>";
		}
		else
		{
			print "<td id=FeldBez><a href=./artikel_Main.php?Zubehoer=Ja&cmdAktion=ArtikelInfos&Key=0" . $rsArtikel['AST_KEY'][0] . ">Zubeh�r:</a></td><td>" . $Anz . "</td>";
		}

		print "</tr>";
	
		print "</table>";

		print "<hr>";
*/
	
			// Artikel als Parameter speichern, um sie in den Unterregistern verwenden zu k�nnen
		awis_BenutzerParameterSpeichern($con, "401", $_SERVER['PHP_AUTH_USER'], $rsArtikel["AST_KEY"][0] . ";" . $rsArtikel["AST_ATUNR"][0]);
		// Unterregister speichern
		$cmdAktion = ($HTTP_GET_VARS["Seite"]==''?"LieferantenArtikel":$HTTP_GET_VARS["Seite"]);

//		awis_RegisterErstellen(41, $con);

		if($BearbeitungsStufe>0)
		{
			print "<input type=hidden name=Speichern Value=True>";
			print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Speichern=True'>";

				// Manuelle Crossing, falls Berechtigung vorhanden
			if(($BearbeitungsStufe&8)==8 AND $rsArtikel["AST_IMQ_ID"][0]==4 AND $rsArtikel["AST_WUG_KEY"][0]==1)
			{
				print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . $rsArtikel["AST_KEY"][0] . "&Loeschen=True'>";
			}
			if(($BearbeitungsStufe&4)==4)		// Neuen Artikel hinzuf�gen
			{
				echo "<a accesskey=n href=./artikel_Main.php?cmdAktion=ArtikelInfos&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
			}

		
					// Cursor in das erste Feld setzen
			print "<Script Language=JavaScript>";
			print "document.getElementsByName(\"txtAST_BEZEICHNUNG\")[0].focus();";
			print "</Script>";
		}

	}	// Datensatz gefunden?
}
print "</form>";


/**************************************************************************
* 
* 	Schreibt ein Recordset in die Seite
* 
***************************************************************************/
function _ArtikelListeSchreiben($rsArtikel,$rsArtikelZeilen,$con,$Export)
{
	if($Export=='True')
	{
		if(file_exists(awis_UserExportDateiName('.csv')))
		{
			$fd = fopen(awis_UserExportDateiName('.csv'),'a+');	// Anh�ngen
		}
		else
		{
			$fd = fopen(awis_UserExportDateiName('.csv'),'w');		// Neu
			fputs($fd,"ATUNR;Bezeichnung WW;Bezeichnung Cross;VK;Lager-Best;Filial-Best;WGr;Funstelle\r\n");
		}
	
		for($i=0;$i<$rsArtikelZeilen;$i++)
		{
			fputs($fd,$rsArtikel["AST_ATUNR"][$i] . ';');
			fputs($fd,str_replace(';', ',', $rsArtikel["AST_BEZEICHNUNGWW"][$i]) . ';');
			fputs($fd,str_replace(';', ',', $rsArtikel["AST_BEZEICHNUNG"][$i]) . ';');
			fputs($fd,awis_format($rsArtikel["AST_VK"][$i],'Standardzahl') . ';');		// englisch wg. Excel
	
			$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AIT_ID IN (10,11,12) AND ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][$i] . "'");
			$LagerBest = awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT");
			$LagerBest += awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT");
	
			fputs($fd,$LagerBest . ';');
			fputs($fd,awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT") . ';');
			fputs($fd,$rsArtikel["WUG_WGR_ID"][$i] . ';');
			fputs($fd,$rsArtikel["FUNDSTELLE"][$i] );
			fputs($fd,"\r\n");
		}
			
		fclose($fd);
		echo '.';			// Anzeige
	}
	else
	{
		for($i=0;$i<$rsArtikelZeilen AND $i<1000;$i++)
		{
			print "<tr>";
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?Key=" . $rsArtikel["AST_KEY"][$i] . "&cmdAktion=ArtikelInfos>" . $rsArtikel["AST_ATUNR"][$i] . "</a> (" . $rsArtikel["FUNDSTELLE"][$i] . ") " . "</td>";
			print '<td ' . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>';
			echo ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?'<b></b>' . $rsArtikel["AST_BEZEICHNUNGWW"][$i].'<br>':'');
			echo ((($rsArtikel['AST_IMQ_ID'][$i]&4)==4)?"<b>Cross: </b><i>" .$rsArtikel["AST_BEZEICHNUNG"][$i] . "</i>":'') . '</a></td>';

			echo '<td ' . ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?'':'bgcolor=#FF0000') . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . ((($rsArtikel['AST_IMQ_ID'][$i]&2)==2)?'X':'') . '</td>';
			echo '<td ' . ((($rsArtikel['AST_IMQ_ID'][$i]&4)==4)?'':'bgcolor=#FF0000') . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .'>' . ((($rsArtikel['AST_IMQ_ID'][$i]&4)==4)?'X':'') . '</td>';

			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . number_format($rsArtikel["AST_VK"][$i],2,",",".") . "</a></td>";
		
			$rsASI = awisOpenRecordset($con,"SELECT ASI_AST_ATUNR, ASI_WERT, ASI_AIT_ID FROM AWIS.ARTIKELSTAMMINFOS WHERE ASI_AIT_ID IN (10,11,12) AND ASI_AST_ATUNR ='" . $rsArtikel["AST_ATUNR"][$i] . "'");
			$LagerBest = awis_ArtikelInfo(10, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT");
			$LagerBest += awis_ArtikelInfo(11, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT");
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $LagerBest . "</td>";
		
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . awis_ArtikelInfo(12, $rsASI, $rsArtikel["AST_ATUNR"][$i], "ASI_WERT") . "</td>";
		
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ." align=right>" . $rsArtikel["WUG_WGR_ID"][$i] . "</td>";
			
			print "</tr>";
		}
	}
	flush();
}

?>

