<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('V_VERS_FORM_RECHNUNGSDATEN', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }
    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    $SQL = 'SELECT v.kassedatum,v.kassezeit,v.umsatz_gesamt,v.umsatz_kunde,v.umsatz_vers,v.vorst_betrag, ';
    $SQL .= 'v.sb, v.zahlart, v.vorstkz, v.sbkz ';
    $SQL .= 'FROM v_vers_form_rechnungsdaten v, versvorgangsstatus vv ';
    $SQL .= 'WHERE v.filid = vv.vvs_filid AND ';
    $SQL .= 'v.wanr = vv.vvs_wanr AND ';
    $SQL .= 'vv.vvs_key = ' . $DB->WertSetzen('VVS', 'N0', $Param);

    $rsRechDat = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS'));

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenRechnungsdaten action=./versicherungen_Details.php?cmdAktion=Details&Seite=Rechnungsdaten' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $EditRecht = $Recht9500 & 2;

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['KASSEDATUM'] . ':', 150);
    $Form->Erstelle_TextFeld('KASSEDATUM', $rsRechDat->FeldInhalt('KASSEDATUM'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['KASSEZEIT'] . ':', 150);
    $Form->Erstelle_TextFeld('KASSEZEIT', $rsRechDat->FeldInhalt('KASSEZEIT'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['ZAHLART'] . ':', 150);
    $Form->Erstelle_TextFeld('ZAHLART', $rsRechDat->FeldInhalt('ZAHLART'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['UMSATZ_GESAMT'] . ':', 150);
    $Form->Erstelle_TextFeld('UMSATZ_GESAMT', $rsRechDat->FeldInhalt('UMSATZ_GESAMT'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['UMSATZ_KUNDE'] . ':', 150);
    $Form->Erstelle_TextFeld('UMSATZ_KUNDE', $rsRechDat->FeldInhalt('UMSATZ_KUNDE'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['UMSATZ_VERS'] . ':', 150);
    $Form->Erstelle_TextFeld('UMSATZ_VERS', $rsRechDat->FeldInhalt('UMSATZ_VERS'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['VORSTKZ'] . ':', 150);
    $Form->Erstelle_TextFeld('VORSTKZ', $rsRechDat->FeldInhalt('VORSTKZ'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['VORST_BETRAG'] . ':', 150);
    $Form->Erstelle_TextFeld('VORST_BETRAG', $rsRechDat->FeldInhalt('VORST_BETRAG'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['SBKZ'] . ':', 150);
    $Form->Erstelle_TextFeld('SBKZ', $rsRechDat->FeldInhalt('SBKZ'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_RECHNUNGSDATEN']['SB'] . ':', 150);
    $Form->Erstelle_TextFeld('SB', $rsRechDat->FeldInhalt('SB'), 360, 360, false);
    $Form->ZeileEnde();
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905132008");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
