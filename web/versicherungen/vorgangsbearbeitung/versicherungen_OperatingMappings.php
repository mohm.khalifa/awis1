<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('VDM', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT * ';
    $SQL .= 'FROM versmappingdwh ';
    $SQL .= 'ORDER BY vdm_system';

    if($DB->ErmittleZeilenAnzahl($SQL) >= 1)
    {
        $rsVDM = $DB->RecordSetOeffnen($SQL);

        $Form->Formular_Start();
        $Form->ZeileStart();

        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_SYSTEM'], 120, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_MAPPINGNAME'], 250, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_TABELLENNAME_ODS'], 250, '', false);
        //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_JOBLAUF'],120,'',false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_AKTIV'], 80, '', false);
        //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_PRIO'],80,'',false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_GELADEN'], 200, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VDM']['VDM_LADENJANEIN'], 150, '', false);

        $Form->ZeileEnde();

        $VDMZeile = 0;

        while(!$rsVDM->EOF())
        {
            // Status auswerten
            if($rsVDM->FeldInhalt('VDM_GELADEN') == 0)
            {
                $StatusGeladen = "wartet auf DWH-Load";
            }
            elseif($rsVDM->FeldInhalt('VDM_GELADEN') == 1)
            {
                $StatusGeladen = "Tabelle Geladen";
            }
            elseif($rsVDM->FeldInhalt('VDM_GELADEN') == 2)
            {
                $StatusGeladen = "mit Fehler geladen";
            }
            elseif($rsVDM->FeldInhalt('VDM_GELADEN') == 3)
            {
                $StatusGeladen = "nicht geladen (ausgehängt)";
            }
            elseif($rsVDM->FeldInhalt('VDM_GELADEN') == 4)
            {
                $StatusGeladen = "Status unbekannt";
            }

            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('VDM_SYSTEM', $rsVDM->FeldInhalt('VDM_SYSTEM'), 0, 120, false, ($VDMZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDM_MAPPINGNAME', $rsVDM->FeldInhalt('VDM_MAPPINGNAME'), 0, 250, false, ($VDMZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDM_MAPPINGNAME', $rsVDM->FeldInhalt('VDM_TABELLENNAME_ODS'), 0, 250, false, ($VDMZeile % 2), '', '', 'T');
            //$Form->Erstelle_ListenFeld('VDM_GELADEN',$rsVDM->FeldInhalt('VDM_JOBLAUF'),0,120,false,($VDMZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('VDM_GELADEN', $rsVDM->FeldInhalt('VDM_AKTIV'), 0, 80, false, ($VDMZeile % 2), '', '', 'T');
            //$Form->Erstelle_ListenFeld('VDM_GELADEN',$rsVDM->FeldInhalt('VDM_PRIO'),0,80,false,($VDMZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('VDM_GELADEN', $rsVDM->FeldInhalt('VDM_GELADEN') . ' ' . $StatusGeladen, 0, 200, false, ($VDMZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDM_LADENJANEIN', $rsVDM->FeldInhalt('VDM_LADENJANEIN'), 0, 150, false, ($VDMZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsVDM->DSWeiter();
            $VDMZeile++;
        }
    }
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906231637");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
