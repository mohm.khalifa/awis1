<?php
require_once('awisMailer.inc');
require_once ('awisDatenbank.inc');
require_once ('awisBenutzer.inc');


try {

    $Benutzer = '';
    $DebugLevel = 0;
    $MaxAlterTage = 14;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--maxaltertage':
                $MaxAlterTage = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --maxaltertage <Tage>              Wie alt d�rfen die Emails Maximal sein, um nicht historisiert zu werden. (Userdat < sysdate - $MaxAlterTage)' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $Benutzer = awisBenutzer::Init($Benutzer);
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();


    $Mailer = new awisMailer($DB,$Benutzer);
    $Mailer->MailversandHistorie($MaxAlterTage);



} catch (Exception $ex) {
    echo PHP_EOL . 'Mailhistorie Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'MOB-Datei-Fehler', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>