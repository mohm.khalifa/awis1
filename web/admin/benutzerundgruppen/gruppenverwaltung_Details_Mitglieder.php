<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XBG','%');
	$TextKonserven[]=array('XBN','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht25 = $AWISBenutzer->HatDasRecht(25);
	if($Recht25==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY XBN_NAME, XBN_VORNAME';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}


	// Daten ermitteln
	$SQL = 'SELECT xbn_key, xbn_name, xbn_vorname, xbn_status, XBG_XBB_KEY, XBG_XBN_KEY';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM benutzergruppen ';
	$SQL .= ' LEFT OUTER JOIN Benutzer ON XBG_xbn_key = xbn_key';
	$SQL .= ' WHERE XBG_XBB_key = 0'.$AWIS_KEY1;

	$AWIS_KEY2=0;
	if(isset($_GET['XBN_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['XBN_KEY']);
		$SQL .= ' AND XBG_XBN_KEY = '.$AWIS_KEY2;
	}

	$SQL .= $ORDERBY;


	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$rsXBG = $DB->RecordSetOeffnen($SQL);
	$Form->DebugAusgabe(1, $SQL);

	if($rsXBG->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht25)&6)!=0)
		{
			$Form->Erstelle_HiddenFeld('txtPBREdit','Ja');
		}
		if((intval($Recht25)&8)!=0)
		{
			$Icons[] = array('new','./gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY='.$AWIS_KEY1.'&Seite=Mitglieder&XBN_KEY=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

		$Link = './gruppenverwaltung_Main.php?cmdAktion=Details&Seite=Mitglieder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&XBB_KEY='.$AWIS_KEY1;
		$Link .= '&SSort=XBN_NAME'.((isset($_GET['SSort']) AND ($_GET['SSort']=='XBN_NAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_NAME'],350,'',$Link);
		$Form->ZeileEnde();

		$PEBZeile=0;
		$DS=0;
		while(!$rsXBG->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht25&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./gruppenverwaltung_Main.php?cmdAktion=Details&Seite=Mitglieder&Del='.$rsXBG->FeldInhalt('XBG_XBB_KEY').'~'.$rsXBG->FeldInhalt('XBG_XBN_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Link = './benutzerverwaltung_Main.php?cmdAktion=Details&XBN_KEY=0'.$rsXBG->FeldInhalt('XBN_KEY');
			$Form->Erstelle_ListenFeld('XBN_NAME',$rsXBG->FeldInhalt('XBN_NAME').' '.$rsXBG->FeldInhalt('XBN_VORNAME').($rsXBG->FeldInhalt('XBN_STATUS')==='I'?' -- INAKTIV --':''),0,350,false,($PEBZeile%2),'',$Link,'T');
			$Form->ZeileEnde();

			$rsXBG->DSWeiter();
			$PEBZeile++;
		}

		$Link = './gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY='.$AWIS_KEY1.''.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsXBG->FeldInhalt('XBG_KEY');

		$Form->Erstelle_HiddenFeld('XBG_KEY',$AWIS_KEY2);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./gruppenverwaltung_Main.php?cmdAktion=Details&XBGListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBG->FeldInhalt('XBG_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBG->FeldInhalt('XBG_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht25&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBG']['XBG_XBN_KEY'].':',150);
		$SQL = 'SELECT xbn_key, xbn_name || \' \' || xbn_vorname AS xbnname ';
		$SQL .= ' FROM Benutzer';
		$SQL .= ' WHERE NOT EXISTS (SELECT * FROM Benutzergruppen WHERE XBG_XBB_KEY = '.$AWIS_KEY1.' AND XBG_XBN_KEY = XBN_KEY)';
		$SQL .= ' ORDER BY xbn_name, xbn_vorname';

		$Form->Erstelle_SelectFeld('XBG_XBN_KEY',$rsXBG->FeldInhalt('XBG_XBN_KEY'),50,$EditRecht,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','');
		$AWISCursorPosition='txtXBG_XBN_KEY';
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200809111043);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>