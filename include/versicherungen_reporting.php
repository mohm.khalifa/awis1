<?php

	require_once 'awisDatenbank.inc';
	require_once 'awisMailer.inc';
	require_once 'awisFormular.inc';
	
	class versicherungen_reporting
	{
		
		/**
		 * Datumzeitstempel welches dem Reporting
		 * zugrunde liegen soll
		 * @var date
		 */
		private $_datetimeVerarbeitung = '';
		
		/**
		 * Datenbankobjekt
		 * @var object
		 */
		private $_objDB = '';
		
		/**
		 * Exportpfad der Reportingdatei
		 * @var string
		 */
		private $_exportPfadReporting = '/daten/daten/versicherungen/archiv/reporting/';
		
		/**
		 * Dateiname der Exportdatei
		 * @var string
		 */
		private $_exportDateiname = '';
		
		/**
		 * Pfad inkl. Dateiname des LogFiles
		 * @var string
		 */
		private $_logFile = '/daten/daten/versicherungen/archiv/reporting/report.log';
		
		/**
		 * Recordsetobjekt mit den Daten fuer das
		 * Reporting
		 * @var Recordset-Objekt
		 */
		private $_rsReportingdaten = null;
		
		/**
		 * AWIS-Benutzerobjekt
		 * @var object
		 */
		private $_objAWISBenutzer = null;
		
		/**
		 * AWIS-Werkzeugeobjekt
		 * @var object
		 */
		private $_objAWISWerkzeuge = null;
		
		/**
		 * AWIS-Mailerobjekt
		 * @var object
		 */
		private $_objMailer = null;
		
		/**
		 * AWIS-Level
		 * @var string
		 */
		private $_awisLevel = '';
		
		
		
		/**
		 * Benoetigte Objekte erstellen, Verarbeitungsdatum speichern
		 * @param date $datumVerarbeitung
		 * @param string $benutzer Benutzername
		 */
		public function __construct($datetimeVerarbeitung,$benutzer)
		{
			$this->_datetimeVerarbeitung = $datetimeVerarbeitung;
			$this->_objDB = awisDatenbank::NeueVerbindung('AWIS');
			$this->_objAWISBenutzer = awisBenutzer::Init($benutzer);
			$this->_objAWISWerkzeuge = new awisWerkzeuge();
			$this->_objMailer = new awisMailer($this->_objDB,$this->_objAWISBenutzer);
			$this->_awisLevel = $this->_objAWISWerkzeuge->awisLevel();
			 
		}
		
		public function erstelleReporting() {
			
			try
			{
				$fp = $this->oeffneDatei($this->_logFile, 'a');
				
				$this->schreibeLogFile('Selektiere Daten', $fp);
				$this->selektiereDaten();
				$this->schreibeLogFile('Exportiere Datei und versende sie per Mail', $fp);
				$this->erstelleExportdatei();
				$this->schreibeLogFile('Reporting erfolgreich erstellt', $fp);
			}
			catch(Exception $ex)
			{
				echo $ex->getMessage() . $ex->getCode();
				$this->schreibeLogFile('ERROR ' . $ex->getMessage() . ' ' . $ex->getCode(), $fp);
			}
			
			// LogFile schliessen
			fclose($fp);
			
		}
		
		/**
		 * Schreibt einen Eintrag in die LogFile
		 * @param string $logFileText
		 * @param resource $fp
		 */
		private function schreibeLogFile($logFileText,$fp) {
			try
			{
				$praefix = '[' . strftime('%Y-%m-%d %H:%M:%S') . ']: ';
				
				fputs($fp, $praefix . $logFileText . "\n");
			}
			catch(Exception $ex)
			{
				// Nur ausgeben wenn LogFile nicht geschrieben werden kann (siehe erstelleReporting())
				echo $ex->getMessage() . $ex->getCode();
			}
		}
		
		/**
		 * Selektiert die Daten bei denen zwischen dem Kassen- & VersRechBetrag
		 * eine Differenz > 0 besteht.
		 */
		private function selektiereDaten() 
		{
			try
			{
				$BindeVariablen=array();
				$BindeVariablen['var_DU_vab_userdat']=$this->_datetimeVerarbeitung;
				/*	
				$SQL = 'SELECT * FROM';
				$SQL .= ' (SELECT rech.vab_filid, rech.vab_wanr, kasse.umsatz_gesamt,vab_vorgangnr,';
				$SQL .= ' betrag_autoglas,rech.betrag_rechnung, umsatz_gesamt - rech.betrag_rechnung as diff';
				$SQL .= ' FROM';
				$SQL .= ' (SELECT filid,wanr,umsatz_gesamt';
				$SQL .= ' from v_vers_form_rechnungsdaten) kasse INNER JOIN';
				$SQL .= ' (SELECT vab_filid, vab_wanr, vab_vorgangnr,';
				$SQL .= ' round((sum(vab_anzahl * vab_betrag_rabattiert))*1.19,2) as betrag_rechnung,';
				$SQL .= ' round((sum(fgp_anzahl * fgp_oempreis))*1.19,2) as betrag_autoglas';
				$SQL .= ' FROM versausbuchung INNER JOIN fgpositionsdaten';
				$SQL .= ' ON vab_vorgangnr = fgp_vorgangnr AND vab_artnr = fgp_artnr';
				$SQL .= ' WHERE vab_userdat = to_date(:var_DU_vab_userdat,\'DD.MM.YYYY HH24:MI:SS\')';
				$SQL .= ' GROUP BY vab_filid, vab_wanr, vab_vorgangnr) rech';
				$SQL .= ' ON kasse.filid = rech.vab_filid AND kasse.wanr = rech.vab_wanr)';
				$SQL .= ' WHERE diff <> 0';
				*/
				
				$SQL  = 'SELECT *';
				$SQL .= ' FROM';
				$SQL .= '   (SELECT vfr.vers_bez,';
				$SQL .= '     rech.vab_filid,';
				$SQL .= '     rech.vab_wanr,';
				$SQL .= '     kasse.umsatz_gesamt,';
				$SQL .= '     vab_vorgangnr,';
				$SQL .= '     betrag_autoglas,';
				$SQL .= '     rech.betrag_rechnung,';
				$SQL .= '     umsatz_gesamt - rech.betrag_rechnung AS diff';
				$SQL .= '   FROM';
				$SQL .= '     (SELECT filid,wanr,umsatz_gesamt FROM v_vers_form_rechnungsdaten';
				$SQL .= '     ) kasse';
				$SQL .= '   INNER JOIN';
				$SQL .= '     (SELECT vab_filid,';
				$SQL .= '       vab_wanr,';
				$SQL .= '       vab_vorgangnr,';
				$SQL .= '       ROUND((SUM(vab_anzahl * vab_betrag_rabattiert))*1.19,2) AS betrag_rechnung,';
				$SQL .= '       ROUND((SUM(fgp_anzahl * fgp_oempreis))*1.19,2)          AS betrag_autoglas';
				$SQL .= '     FROM versausbuchung';
				$SQL .= '     INNER JOIN fgpositionsdaten';
				$SQL .= '     ON vab_vorgangnr = fgp_vorgangnr';
				$SQL .= '     AND vab_artnr    = fgp_artnr';
				$SQL .= '     WHERE vab_userdat = to_date(:var_DU_vab_userdat,\'DD.MM.YYYY HH24:MI:SS\')';
				$SQL .= '     GROUP BY vab_filid, vab_wanr, vab_vorgangnr';
				$SQL .= '     ) rech ON kasse.filid = rech.vab_filid';
				$SQL .= '   AND kasse.wanr          = rech.vab_wanr';
				$SQL .= '   INNER JOIN';
				$SQL .= '     (SELECT fil_id, wanr, vers_bez FROM v_vers_form_detail) vfr';
				$SQL .= '   ON vfr.fil_id = kasse.filid';
				$SQL .= '   AND vfr.wanr  = kasse.wanr)';
				$SQL .= ' WHERE diff <> 0';
				
				$this->_rsReportingdaten = $this->_objDB->RecordsetOeffnen($SQL,$BindeVariablen);
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(), $ex->getCode());
			}			
		}
		
		/**
		 * Oeffnet angegebene Datei im angegebenen Modus und loest im Fehlerfall eine
		 * Exception aus.
		 * @param string $pfad Pfad inkl. Dateiname der Datei die geoeffnet werden soll
		 * @param string $modus Modus mit dem die Datei geoeffnet werden soll
		 * @return resource FilePointer
		 */
		private function oeffneDatei($pfad,$modus) {
			$fp = fopen($pfad, $modus);
			
			if(!$fp)
			{
				throw new Exception('Datei konnte nicht geoeffnet werden','1408061439');	
			}
			else
			{
				return $fp;
			}			
		}
		
		/**
		 * Erzeugt die Reportingdatei. Wenn dies Erfolgreich ist wird die Mail-Methode
		 * aufgerufen.
		 * @param object (Recordset) $rsReportingdaten
		 */
		private function erstelleExportdatei() 
		{
			try
			{
			
				if(is_null($this->_rsReportingdaten))
				{
					throw new Exception('es wurden keine Reportingdaten selektiert','1408061436');
				}
				else
				{
					// Dateinamen erstellen
					$this->_exportDateiname = 'report_vers_diff_' . strftime('%Y%m%d_%H%M%S') . '.csv';
					
					$fp = $this->oeffneDatei($this->_exportPfadReporting . $this->_exportDateiname, 'a');
					
					$ueberschrift = 'VERSICHERUNG;FIL_ID;VORGANGNR;BETRAG_KASSE;AUTOGLAS_VORGANGNR;BETRAG_AUTOGLAS;BETRAG_RECHNUNG';
					
					fputs($fp, $ueberschrift.chr(13).chr(10));
					
					$datenZeile = '';
					
					while(!$this->_rsReportingdaten->EOF())
					{
						$datenZeile = $this->_rsReportingdaten->FeldInhalt('VERS_BEZ');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('VAB_FILID');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('VAB_WANR');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('UMSATZ_GESAMT');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('VAB_VORGANGNR');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('BETRAG_AUTOGLAS');
						$datenZeile .= ';' . $this->_rsReportingdaten->FeldInhalt('BETRAG_RECHNUNG');
						
						fputs($fp,$datenZeile.chr(13).chr(10));
						
						$datenZeile = '';
						
						$this->_rsReportingdaten->DSWeiter();
					}
					
					fclose($fp);
				}
				
				// wenn bis hierher alles gut geht, dann kann Mail gesendet werden
				$this->sendeReportingMail();
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}
		
		/**
		 * Sendet die Reportingdatei per Mail an Verteiler
		 */
		private function sendeReportingMail() 
		{
			try
			{
				$SQL = 'SELECT mvt_betreff, mvt_text ';
				$SQL .= 'FROM mailversandtexte ';
				$SQL .= 'WHERE mvt_bereich = \'VAB_REPORTING\'';
				
				$rsMVT = $this->_objDB->RecordSetOeffnen($SQL);
				
				
				$this->_objMailer->SetzeVersandPrioritaet(10);
				$this->_objMailer->LoescheAdressListe();
				$this->_objMailer->AnhaengeLoeschen();
					
				$this->_objMailer->Absender('shuttle@de.atu.eu');
				
				$empfaenger = array();
				$empfaengerCC = array();
				
				if($this->_awisLevel == 'ENTW' || $this->_awisLevel == 'STAG')
				{
					$empfaenger[] = 'stefan.oppl@de.atu.eu';
					$empfaengerCC[] = 'tamara.bannert@de.atu.eu';
				}
				elseif($this->_awisLevel == 'PROD' || $this->_awisLevel == 'SHUT')
				{
					$empfaenger[] = 'Versicherungskooperation@de.atu.eu';
					$empfaengerCC[] = 'tamara.bannert@de.atu.eu';
					$empfaengerCC[] = 'stefan.oppl@de.atu.eu';
				}
				else
				{
					throw new Exception('unbekannter AWIS-Level','1408071013');
				}
				
				foreach ($empfaenger as $empfaengerValue)
				{
					if(!$this->_objMailer->AdressListe(awisMailer::TYP_TO,$empfaengerValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
						throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_TO','1408071017');
					}
				}
				
				foreach ($empfaengerCC as $empfaengerCCValue)
				{
					if(!$this->_objMailer->AdressListe(awisMailer::TYP_CC,$empfaengerCCValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
					    throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_CC','1408071021');
					}
				}
					
				// nur Kuerzel fuer Bezug setzen. Einen Key gibt es nicht da in der Reportingdatei
				// mehrere Vorgaenge enthalten sein koennen.
				$this->_objMailer->SetzeBezug('VAB',0);
					
				$this->_objMailer->Anhaenge($this->_exportPfadReporting . $this->_exportDateiname, $this->_exportDateiname);
				$this->_objMailer->Betreff($rsMVT->FeldInhalt('MVT_BETREFF'));
				$this->_objMailer->Text($rsMVT->FeldInhalt('MVT_TEXT'),awisMailer::FORMAT_HTML, true);
					
				$this->_objMailer->MailInWarteschlange($this->_objAWISBenutzer->BenutzerName());
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}		
	}

?>