<?php

	require_once 'awisDatenbank.inc';
	require_once('awisBenutzer.inc');
	require_once 'awisMailer.inc';
	require_once 'awisFormular.inc';
	
	class autoglas_differenzliste
	{
				
		/**
		 * Datenbankobjekt
		 * @var object
		 */
		private $_DB = '';
		
		/**
		 * Exportpfad der Reportingdatei
		 * @var string
		 */
		
		private $_exportPfadDiffliste = '/daten/daten/versicherungen/archiv/reporting/Autoglas-Differenzen/';
		
		/**
		 * Dateiname der Exportdatei
		 * @var string
		 */
		private $_exportDateiname = '';
		
		/**
		 * Pfad inkl. Dateiname des LogFiles
		 * @var string
		 */
		
		private $_logFileAutoglas = '/daten/daten/versicherungen/archiv/reporting/Autoglas-Differenzen/diffliste.log';
		
		/**
		 * Recordsetobjekt mit den Daten fuer das
		 * Reporting
		 * @var Recordset-Objekt
		 */
		
		private $_rsAutoglasdifferenzen = null;
		
		/**
		 * AWIS-Benutzerobjekt
		 * @var object
		 */
		private $_AWISBenutzer = null;
		
		/**
		 * AWIS-Werkzeugeobjekt
		 * @var object
		 */
		private $_AWISWerkzeuge = null;
		
		
		/**
		 * AWIS-Mailerobjekt
		 * @var object
		 */
		private $_Mailer = null;
		
		/**
		 * AWIS-Level
		 * @var string
		 */
		private $_awisLevel = '';
		
		
		
		/**
		 * Benoetigte Objekte erstellen, Verarbeitungsdatum speichern
		 * @param date $datumVerarbeitung
		 * @param string $benutzer Benutzername
		 */
		public function __construct()
		{
			$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
			$this->_AWISBenutzer = awisBenutzer::Init();
			$this->_AWISWerkzeuge = new awisWerkzeuge();
			$this->_Mailer = new awisMailer($this->_DB,$this->_AWISBenutzer);
			$this->_awisLevel = $this->_AWISWerkzeuge->awisLevel();
			 
		}
		
		public function erstelleReporting() {
			
			try
			{
				//Versicherung-Sperrliste
				$fp = $this->oeffneDatei($this->_logFileAutoglas, 'a');
				
				$this->schreibeLogFile('Selektiere Daten', $fp);
				$this->selektiereDaten();
				$this->schreibeLogFile('Exportiere Datei und versende sie per Mail', $fp);
				$this->erstelleExportdatei();
				$this->schreibeLogFile('Reporting erfolgreich erstellt', $fp);
			}
			catch(Exception $ex)
			{
				echo $ex->getMessage() . $ex->getCode();
				$this->schreibeLogFile('ERROR ' . $ex->getMessage() . ' ' . $ex->getCode(), $fp);
			}
			
			// LogFile schliessen
			fclose($fp);
			
		}
		
		/**
		 * Schreibt einen Eintrag in die LogFile
		 * @param string $logFileText
		 * @param resource $fp
		 */
		private function schreibeLogFile($logFileText,$fp) {
			try
			{
				$praefix = '[' . strftime('%Y-%m-%d %H:%M:%S') . ']: ';
				
				fputs($fp, $praefix . $logFileText . "\n");
			}
			catch(Exception $ex)
			{
				// Nur ausgeben wenn LogFile nicht geschrieben werden kann (siehe erstelleReporting())
				echo $ex->getMessage() . $ex->getCode();
			}
		}
		/**
		 * Selektiert die Daten bei denen zwischen dem Kassen- & VersRechBetrag
		 * eine Differenz > 0 besteht.
		 */
		private function selektiereDaten() 
		{
			try
			{				
				//Autoglas Differenz-Liste
				$SQL  ='SELECT FFR_FILID, FFR_WANR, FFR_VORGANGNR, FFR_KFZKENNZ, VERS_BEZ,';
				$SQL .='           FFR_AEMBETRAG, FFR_KASSENBETRAG, FFR_DIFFERENZ,';
				$SQL .='           regexp_replace(REGEXP_REPLACE(regexp_replace (FFR_BEGRUENDUNG,CHR(13),\'\'), CHR(10), \'\'),\';\',\',\') AS FILBEGRUENDUNG, FFR_DIFFERENZDATUM,';
				$SQL .='           FGK_USER, USERDAT';
				$SQL .='           FROM (SELECT FGRUECKFUEHRUNG.*, fgkopfdaten.fgk_user, fkassendaten.*, vers_exp_kopf.VERS_BEZ,';
				$SQL .='           case when fgkopfdaten.fgk_user is not null then ';
				$SQL .='           fgkopfdaten.fgk_userdat ';
				$SQL .='           else null';
				$SQL .='           end as USERDAT';
				$SQL .='           FROM FGRUECKFUEHRUNG';
				$SQL .='           INNER JOIN FGKOPFDATEN';
				$SQL .='           ON FGKOPFDATEN.FGK_VORGANGNR = FGRUECKFUEHRUNG.FFR_VORGANGNR';
				$SQL .='           INNER JOIN FKASSENDATEN';
				$SQL .='           ON FKASSENDATEN.FKA_FILID = FGRUECKFUEHRUNG.FFR_FILID';
				$SQL .='           AND FKASSENDATEN.FKA_WANR = FGRUECKFUEHRUNG.FFR_WANR';
				$SQL .='           AND FKASSENDATEN.FKA_AEMNR = FGRUECKFUEHRUNG.FFR_VORGANGNR';
				$SQL .='           AND FKASSENDATEN.FKA_BETRAG = FGRUECKFUEHRUNG.FFR_KASSENBETRAG';
				$SQL .='           LEFT JOIN VERS_EXP_KOPF';
				$SQL .='           ON FGRUECKFUEHRUNG.FFR_FILID = VERS_EXP_KOPF.FILID';
				$SQL .='           AND FGRUECKFUEHRUNG.FFR_WANR = VERS_EXP_KOPF.WANR';
				$SQL .='           AND VERS_EXP_KOPF.QUELLE = \'D\'';
				$SQL .='           WHERE FFR_STATUS < 2) ffr';
												
				$this->_rsAutoglasdifferenzen = $this->_DB->RecordSetOeffnen($SQL);
				
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(), $ex->getCode());
			}			
		}
		
		/**
		 * Oeffnet angegebene Datei im angegebenen Modus und loest im Fehlerfall eine
		 * Exception aus.
		 * @param string $pfad Pfad inkl. Dateiname der Datei die geoeffnet werden soll
		 * @param string $modus Modus mit dem die Datei geoeffnet werden soll
		 * @return resource FilePointer
		 */
		private function oeffneDatei($pfad,$modus) {
			$fp = fopen($pfad, $modus);
			
			if(!$fp)
			{
				throw new Exception('Datei konnte nicht geoeffnet werden','1408061439');	
			}
			else
			{
				return $fp;
			}			
		}
		
		/**
		 * Erzeugt die Reportingdatei. Wenn dies Erfolgreich ist wird die Mail-Methode
		 * aufgerufen.
		 * @param object (Recordset) $rsReportingdaten
		 */
		private function erstelleExportdatei() 
		{
			try
			{
			
				if(is_null($this->_rsAutoglasdifferenzen))
				{
					throw new Exception('es wurden keine Reportingdaten selektiert','1408061436');
				}
				else
				{
					// Dateinamen erstellen
					$this->_exportDateiname = 'Autoglas-Differenzliste' . strftime('%Y%m%d_%H%M%S') . '.csv';
					
					$fp = $this->oeffneDatei($this->_exportPfadDiffliste . $this->_exportDateiname, 'a');
					
					$ueberschrift = 'FILID;WA;VORGANG_NR;KFZ-KZ;VERS_BEZ;BETRAG_AUTOGLAS;BETRAG_KASSE;DIFFERENZ;FIL_BEGRUENDUNG;DIFF_DATUM;USER;USERDAT';
					
					fputs($fp, $ueberschrift . "\n");
					
					$datenZeile = '';
					
					while(!$this->_rsAutoglasdifferenzen->EOF())
					{
						$datenZeile = $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_FILID');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_WANR');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_VORGANGNR');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_KFZKENNZ');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('VERS_BEZ');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_AEMBETRAG');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_KASSENBETRAG');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_DIFFERENZ');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FILBEGRUENDUNG');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FFR_DIFFERENZDATUM');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('FGK_USER');
						$datenZeile .= ';' . $this->_rsAutoglasdifferenzen->FeldInhalt('USERDAT');
						
						fputs($fp,$datenZeile . "\n");
						
						$datenZeile = '';
						
						$this->_rsAutoglasdifferenzen->DSWeiter();
					}
					
					fclose($fp);
				}
				
				// wenn bis hierher alles gut geht, dann kann Mail gesendet werden
				$this->sendeReportingMail();
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}
		
		/**
		 * Sendet die Reportingdatei per Mail an Verteiler
		 */
		private function sendeReportingMail() 
		{
			try
			{
				$SQL = 'SELECT mvt_betreff, mvt_text ';
				$SQL .= 'FROM mailversandtexte ';
				$SQL .= 'WHERE mvt_bereich = \'AUTOGLAS_DIFFLISTE\'';
				
				$rsMVT = $this->_DB->RecordSetOeffnen($SQL);
				
				
				$this->_Mailer->SetzeVersandPrioritaet(10);
				$this->_Mailer->LoescheAdressListe();
				$this->_Mailer->AnhaengeLoeschen();
					
				$this->_Mailer->Absender('shuttle@de.atu.eu');
				
				$empfaenger = array();
				$empfaengerCC = array();
				
				if($this->_awisLevel == 'ENTW' || $this->_awisLevel == 'STAG')
				{
					$empfaenger[] = 'tamara.bannert@de.atu.eu';
				}
				elseif($this->_awisLevel == 'PROD' || $this->_awisLevel == 'SHUT')
				{
                    $empfaenger[] = 'tamara.bannert@de.atu.eu';
				    $empfaengerCC[] = 'tamara.scholz@de.atu.eu';
				}
				else
				{
					throw new Exception('unbekannter AWIS-Level','1408071013');
				}
				
				foreach ($empfaenger as $empfaengerValue)
				{
					if(!$this->_Mailer->AdressListe(awisMailer::TYP_TO,$empfaengerValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
						throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_TO','1408071017');
					}
				}
				
				foreach ($empfaengerCC as $empfaengerCCValue)
				{
					if(!$this->_Mailer->AdressListe(awisMailer::TYP_CC,$empfaengerCCValue, awisMailer::PRUEFE_LOGIK, awisMailer::PRUEFAKTION_ERR))
					{
					    throw new Exception('fehlerhafte E-Mail-Adresse bei TYP_CC','1408071021');
					}
				}
					
				// nur Kuerzel fuer Bezug setzen. Einen Key gibt es nicht da in der Reportingdatei
				// mehrere Vorgaenge enthalten sein koennen.
				$this->_Mailer->SetzeBezug('VAB',0);
					
				$this->_Mailer->Anhaenge($this->_exportPfadDiffliste . $this->_exportDateiname, $this->_exportDateiname);
				$this->_Mailer->Betreff($rsMVT->FeldInhalt('MVT_BETREFF'));
				$this->_Mailer->Text($rsMVT->FeldInhalt('MVT_TEXT'),awisMailer::FORMAT_HTML, true);
					
				$this->_Mailer->MailInWarteschlange($this->_AWISBenutzer->BenutzerName());
				
						
			}
			catch(Exception $ex)
			{
				throw new Exception($ex->getMessage(),$ex->getCode());
			}
		}		
	}

?>