<?php
require_once('awisDatenbank.inc');
require_once('awisPreisabfragen.inc');

if(!isset($argv[1]))
{
    echo 'PAA_ID angeben.'.PHP_EOL;
    die();
}

$X = new awisPreisabfragen('skerres');

$X->DebugLevel(99);

$X->SetzeKlassifizierung(21,1);
$X->SetzeKlassifizierung(22,2);
$X->SetzeKlassifizierung(24,1);
$X->SetzeKlassifizierung(28,1);

$X->SetzeFilialEbene('D');

$X->SetzeRanking(1,3,5);

$X->ErstelleMitbewerberListe($argv[1]);

?>

