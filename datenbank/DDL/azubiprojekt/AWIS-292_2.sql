CREATE TABLE AZUBIPROJEKTEBEREICHE
(
      AZB_KEY NUMBER NOT NULL
    , AZB_STUFE NUMBER
    , AZB_BEREICHNAME VARCHAR2(75) NOT NULL
    , AZB_INFOTEXT VARCHAR2(500)
    , AZB_USER VARCHAR2(50)
    , AZB_USERDAT DATE DEFAULT SYSDATE
    , CONSTRAINT PK_AZB_KEY PRIMARY KEY
    (
        AZB_KEY
    )
    USING INDEX
    (
        CREATE UNIQUE INDEX AZUBIPROJEKTEBEREICHE_PK ON AZUBIPROJEKTEBEREICHE (AZB_KEY ASC)
        TABLESPACE INDX
    )
ENABLE
);

COMMENT ON COLUMN AZUBIPROJEKTEBEREICHE.AZB_STUFE IS 'Die jeweilige Stufe des Bereichs. Beginnt mit 1.';

COMMENT ON COLUMN AZUBIPROJEKTEBEREICHE.AZB_BEREICHNAME IS 'Name des Bereichs';

COMMENT ON COLUMN AZUBIPROJEKTEBEREICHE.AZB_INFOTEXT IS '(Info-)Freitext des Bereichs';

CREATE TRIGGER TRG_SEQ_AZB_KEY
BEFORE INSERT ON AZUBIPROJEKTEBEREICHE
FOR EACH ROW
    BEGIN
        <<COLUMN_SEQUENCES>>
        BEGIN
            IF INSERTING AND :NEW.AZB_KEY IS NULL THEN
                SELECT SEQ_AZB_KEY.NEXTVAL INTO :NEW.AZB_KEY FROM SYS.DUAL;
            END IF;
        END COLUMN_SEQUENCES;
    END;
/

CREATE TABLE AZUBIPROJEKTETEILNEHMER
(
  AZT_KEY NUMBER NOT NULL
, AZT_AZB_STUFE NUMBER
, AZT_PERSNR NUMBER NOT NULL
, AZT_FILNR NUMBER
, AZT_NAME VARCHAR2(50) NOT NULL
, AZT_VORNAME VARCHAR2(50) NOT NULL
, AZT_TSHIRT_GROESSE VARCHAR2(20)
, AZT_TEL_NR VARCHAR2(20)
, AZT_EMAIL VARCHAR2(50)
, AZT_USER VARCHAR2(50)
, AZT_USERDAT DATE DEFAULT SYSDATE
, CONSTRAINT PK_AZT_KEY PRIMARY KEY
  (
    AZT_KEY
  )
  USING INDEX
  (
      CREATE UNIQUE INDEX AZUBIPROJEKTETEILNEHMER_PK ON AZUBIPROJEKTETEILNEHMER (AZT_KEY ASC)
  )
  ENABLE
);

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_AZB_STUFE IS 'Stufe aus der Tabelle AZUBIPROJEKTEBEREICHE';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_PERSNR IS 'Personalnummer des Teilnehmers';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_FILNR IS 'Filialnummer';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_NAME IS '(Nach-)Name';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_VORNAME IS 'Vorname';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_TSHIRT_GROESSE IS 'T-Shirt-Gr��e';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_TEL_NR IS 'Telefonnummer';

COMMENT ON COLUMN AZUBIPROJEKTETEILNEHMER.AZT_EMAIL IS 'EMail-Adresse';

CREATE SEQUENCE SEQ_AZT_KEY;

CREATE TRIGGER TRG_SEQ_AZT_KEY
BEFORE INSERT ON AZUBIPROJEKTETEILNEHMER
FOR EACH ROW
    BEGIN
        <<COLUMN_SEQUENCES>>
        BEGIN
            IF INSERTING AND :NEW.AZT_KEY IS NULL THEN
                SELECT SEQ_AZT_KEY.NEXTVAL INTO :NEW.AZT_KEY FROM SYS.DUAL;
            END IF;
        END COLUMN_SEQUENCES;
    END;
/




INSERT INTO rechte ( xrc_id,xrc_recht ) VALUES (65000, 'Azubiprojekte');

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
    65000,
    0,
    'Anzeigen'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
    65000,
    1,
    'Suchen und Detail-Seite anzeigen f�r Fachabteilung'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
    65000,
    2,
    'Exportieren'
);

INSERT INTO tabellenkuerzel (
    xtn_kuerzel,
    xtn_tabellenname,
    xtn_datenquelle,
    xtn_version
) VALUES (
    'AZB',
    'AZUBIPROJEKTEBEREICHE',
    'ORACLE',
    1
);

INSERT INTO tabellenkuerzel (
    xtn_kuerzel,
    xtn_tabellenname,
    xtn_datenquelle,
    xtn_version
) VALUES (
    'AZT',
    'AZUBIPROJEKTETEILNEHMER',
    'ORACLE',
    1
);


INSERT INTO webmasken (
    xwm_id,
    xwm_bezeichnung,
    xwm_bemerkung,
    xwm_user,
    xwm_userdat,
    xwm_status
) VALUES (
    65000,
    'Azubiprojekte',
    'Azubiprojekte',
    'Meyer Andre',
    '10.10.2017',
    'A'
);



INSERT INTO webmaskenregister (
    xrg_id,
    xrg_xwm_id,
    xrg_bezeichnung,
    xrg_beschriftung,
    xrg_aktion,
    xrg_seite,
    xrg_bemerkung,
    xrg_xrc_id,
    xrg_rechtestufe,
    xrg_sortierung,
    xrg_neuehilfe
) VALUES (
    650000,
    65000,
    'Anmeldung',
    'Anmeldung',
    './azubiprojekte_Main.php',
    './azubiprojekte_Anmeldung.php',
    'Azubiprojekte Anmeldungsregister',
    65000,
    1,
    1,
    1
);

SET DEFINE OFF;
INSERT INTO webmaskenregister (
    xrg_id,
    xrg_xwm_id,
    xrg_bezeichnung,
    xrg_beschriftung,
    xrg_aktion,
    xrg_seite,
    xrg_bemerkung,
    xrg_xrc_id,
    xrg_rechtestufe,
    xrg_sortierung,
    xrg_neuehilfe
) VALUES (
    650001,
    65000,
    'Suche',
    'S<u>u</u>che',
    './azubiprojekte_Main.php?cmdAktion=Suche',
    './azubiprojekte_Suche.php',
    'Azubiprojekte Suchregister',
    65000,
    2,
    2,
    1
);

SET DEFINE OFF;
INSERT INTO webmaskenregister (
    xrg_id,
    xrg_xwm_id,
    xrg_bezeichnung,
    xrg_beschriftung,
    xrg_aktion,
    xrg_seite,
    xrg_bemerkung,
    xrg_xrc_id,
    xrg_rechtestufe,
    xrg_sortierung,
    xrg_neuehilfe
) VALUES (
    650002,
    65000,
    'Details',
    '<u>D</u>etails',
    './azubiprojekte_Main.php?cmdAktion=Details',
    './azubiprojekte_Details.php',
    'Azubiprojekte Detailregister',
    65000,
    2,
    3,
    1
);

INSERT INTO webmenuepunkte (
    xmp_key,
    xmp_xme_key,
    xmp_bezeichnung,
    xmp_xrc_id,
    xmp_stufe,
    xmp_gruppenid,
    xmp_sortierung,
    xmp_link,
    xmp_user,
    xmp_userdat,
    xmp_gueltigab,
    xmp_gueltigbis
) VALUES (
    234,
    1,
    'Azubiprojekte',
    65000,
    1,
    0,
    1,
    '/azubiprojekte/azubiprojekte_Main.php',
    'Andre Meyer',
    '10.10.2017',
    '10.10.2017',
    '31.12.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'reg_650000',
    'Register',
    'Register zu den Azubiprojekten',
    'Anmeldung',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'reg_650001',
    'Register',
    'Register zu den Azubiprojekten',
    'Suche',
    '10.10.2017'
);


INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'reg_650002',
    'Register',
    'Register zu den Azubiprojekten',
    'Details',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'MENUE_234_TEXT',
    'MENUE',
    'Menuepunkt zu den Azubiprojekten',
    'Azubiprojekte',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'MENUE_234_HINWEIS',
    'MENUE',
    'Menuepunkt zu den Azubiprojekten',
    'Azubiprojekte',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'tit_Azubiprojekte',
    'TITEL',
    'Titel zu den Azubiprojekten',
    'Azubiprojekte',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'ttt_650000',
    'Register',
    'Register',
    'Anmeldung',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'ttt_650001',
    'Register',
    'Register',
    'Suche',
    '10.10.2017'
);

INSERT INTO textkonserven (
    xtx_kennung,
    xtx_bereich,
    xtx_bemerkung,
    xtx_text_de,
    xtx_userdat
) VALUES (
    'ttt_650002',
    'Register',
    'Register',
    'Details',
    '10.10.2017'
);




SET DEFINE OFF;
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_INFOTEXT','AZT','Infotext des Projektes','Wir als Unternehmen m�chten unsere Auszubildenden Kfz-Mechatroniker mit einer Projektarbeit motivieren, f�rdern und fordern. F�r dieses Projekt wurde eine Corvette C3 Baujahr 1973 beschafft.<br>
<br>
<b>Ziel des Projektes:</b>                         Diese Corvette originalgetreu zu restaurieren und in eine A.T.U-rote Corvette zu verwandeln <br>
<b>Nutzen des Projektes:</b>                  Nutzung der Corvette zu Repr�sentationszwecken (z. B. auf Messen, Oldtimer-Treffen oder �hnliches)<br>
<b>Zielgruppe des Projektes:</b>          Dieses Projekt soll zusammen mit Auszubildenden durchgef�hrt werden. Hierzu gibt es verschiebende Themenbl�cke, in denen unterschiedliche Arten von Arbeiten durchgef�hrt werden.<br>
<br>
Hier k�nnen Sie sich f�r die Auslosung registrieren und mit etwas Gl�ck, d�rfen Sie am Projekt teilnehmen.<br>
<br>
<b>Ablauf:</b><br>
<br>
<ul>
<li><b>Anmeldung:</b> Einfach, schnell und unkompliziert im Portal AWIS unter dem Men�punkt: "Azubiprojekte" registrieren</li>
<li><b>Auswahl des Themenblocks:</b> Ein Anmeldung f�r mehrere Themenbl�cke ist hierbei m�glich</li>
<li><b>Anmeldebest�tigung:</b> Nach der Anmeldung erhalten Sie eine E-Mail an die Mailadresse der Filiale (optional falls angegeben, auch an die private Mailadresse)</li>
<li><b>Auslosung:</b> Die verbindlichen Teilnehmer werden <b>ca. vier Wochen vor Projektbeginn</b> ausgelost (die vorangegangene Anmeldebest�tigung stellt keinen Anspruch auf Teilnahme dar)</li>
<li><b>Bekanntgabe der Gewinner:</b> Diese werden direkt nach der Auslosung bekannt gegeben und per E-Mail �ber die erfolgreiche Teilnahme und genauere Details informiert</li>
</ul>','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_BEREICHSFRAGE','AZT','Frage neben Dropdown','Welcher Themenblock interessiert Sie?','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_FILNR','AZT','Filialnummer','Filialnummer','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_NAME','AZT','Name','Nachname','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_VORNAME','AZT','Vorname','Vorname','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_TSHIRT_GROESSE','AZT','TSHIRT GROESSE','T-Shirt-Gr��e','10.10.2017');
SET DEFINE OFF;
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_LST_TSHIRT_GROESSE','AZT','Tshirt Groessen im Dropdown','S~S|M~M|L~L|XL~XL|XXL~XXL','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_TEL_NR','AZT','Telefonnummer','Telefonnummer','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_EMAIL','AZT','E-Mail Adresse','E-Mail Adresse (optional, keine Filial-Mail-Adressen)','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_BESTAETIGUNG','AZT','Information Einverstaendnis','Ich habe meinen Ausbilder �ber die Teilnahme informiert und seine Einverst�ndnis erhalten  <input type="checkbox" required>','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_PERSNR','AZT','Personalnummer','Personalnummer (6-stellig)','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_ERFOLGREICHTEILGENOMMEN','AZT','Erfolgreich Teilgenommen','Ihr Anmeldung f�r den Themenblock #AZB_BEREICHNAME# war erfolgreich.','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_EMAIL_FALSCH','AZT','Eingegebene Mail nicht ok','Die von Ihnen eingegebene E-Mail-Adresse ist nicht g�ltig.','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_LEERFELDER','AZT','Dropdown leer','Bitte w�hlen Sie einen Themenblock und eine T-Shirt-Gr��e aus.','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_KEINEPERSNR','AZT','Personalnummer existiert nicht','Die eingegebene Personalnummer konnte nicht gefunden werden. Bitte �berpr�fen Sie ihre Eingabe.','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_FIL_FALSCH','AZT','Filialnummer existiert nicht','Die eingegebene Filialnummer konnte nicht gefunden werden. Bitte �berpr�fen Sie ihre Eingabe.','10.10.2017');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_BEREITSTEILGENOMMEN','AZT','Bereits Teilgenommen','Sie haben bereits teilgenommen. Sie k�nnen pro Bereich nur einmal teilnehmen.','26.10.2017');


INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de,xtx_userdat) VALUES ('AZT_STUFE','AZT','Das TextLabel bei der Suche f�r den Bereich','Themenbereich:','10.10.2017');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, xtx_userdat) values ('AZT_KEINE_DS','AZT','Text, wenn kein Datensatz gefunden wurde','Es wurde kein Datensatz zu Ihrer Suche gefunden.','10.10.2017');

INSERT INTO programmparameter (xpp_id,xpp_bezeichnung,xpp_default,xpp_ueberschreibbar,xpp_bemerkung,xpp_kodiert,xpp_datentyp) VALUES (230,'AZTSuche',';',255,'Suchparameter f�r die Azubiprojekte',0,'T');
INSERT INTO programmparameter (xpp_id,xpp_bezeichnung,xpp_default,xpp_ueberschreibbar,xpp_bemerkung,xpp_kodiert,xpp_datentyp) VALUES (231,'AZT_STANDART_EMPFAENGER','shuttle@de.atu.eu',255,'Standartempf�nger f�r die Azubiprojekte',0,'T');
INSERT INTO programmparameter (xpp_id,xpp_bezeichnung,xpp_default,xpp_ueberschreibbar,xpp_bemerkung,xpp_kodiert,xpp_datentyp) VALUES (232,'AZT_STANDART_ABSENDER','noreply@de.atu.eu',255,'Standartabsender f�r die Azubiprojekte',0,'T');




INSERT INTO AZUBIPROJEKTEBEREICHE (AZB_STUFE,AZB_BEREICHNAME,AZB_INFOTEXT,AZB_USER,AZB_USERDAT) values (1,'Motor/Antriebsstrang (08.01.2018 - 12.01.2018)','Hier erwartet Sie der Ausbau von Motor und Antriebstrang inkl. der technischen Bestandsaufnahme. <br>Zudem erfolgt die Demontage der kompletten Fahrzeugfront und Vorbereitung des Motorraums f�r die anschlie�ende Lackierung.<br>Sie befinden sich im 3. oder 4. Lehrjahr und m�chten ein Teil dieses Projekts werden, dann nutzen Sie Ihre Chance.<br>Die Teilnehmer werden ca. 4 Wochen vor Beginn ausgelost und entsprechend per E-Mail informiert. <br>Wir w�nschen Ihnen viel Erfolg.','meyer_a','25.10.2017');
INSERT INTO AZUBIPROJEKTEBEREICHE (AZB_STUFE,AZB_BEREICHNAME,AZB_INFOTEXT,AZB_USER,AZB_USERDAT) values (2,'Innenraum/Karosserie (22.01.2018 - 26.01.2018)','Hier erwartet Sie der Ausbau der kompletten Innenausstattung und Verglasung inkl. Bestandsaufnahme. <br>Zudem erfolgt die Demontage aller Karosserie-Anbauteile und Vorbereitung des Innenraums f�r die anschlie�ende Lackierung. <br>Sie befinden sich im 2., 3. oder 4. Lehrjahr und m�chten ein Teil dieses Projekts werden, dann nutzen Sie Ihre Chance. <br>Die Teilnehmer werden ca. 4 Wochen vor Beginn ausgelost und entsprechend per E-Mail informiert. <br>Wir w�nschen Ihnen viel Erfolg.','meyer_a','25.10.2017');

INSERT INTO mailversandtexte (mvt_bereich, mvt_sprache,mvt_betreff,mvt_text,mvt_gueltigab,mvt_user,mvt_userdat)
VALUES ('AZT_ANMELDEBESTAETIGUNG','DE','Anmeldebest�tigung: A.T.U - Azubiprojekt Corvette C3','<div style="font-family: Arial; font-size: 10pt">Sehr geehrter Herr/Frau #AZT_NACHNAME#,<br>
<br>
Sie haben sich soeben erfolgreich f�r das A.T.U � Azubiprojekt zur Restaurierung einer Corvette C3 f�r den Bereich #AZT_BEREICHNAME# registriert.<br>
Die ausgelosten Teilnehmer werden 4 Wochen vor Beginn der Einsatzwoche informiert.<br>
F�r eventuelle R�ckfragen k�nnen Sie sich gerne an Frau Jenny Fr�bel unter der Durchwahl: -5627 wenden.<br>
<br>
Wir w�nschen Ihnen viel Erfolg und dr�cken Ihnen die Daumen.</div>','01.01.1970','meyer_a','25.10.2017');





