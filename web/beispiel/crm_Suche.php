<?
global $CursorFeld;
global $AWISBenutzer;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3700=awisBenutzerRecht($con,3700);

echo "<br>";

echo "<form name=frmSuche method=post action=./crm_Main.php?cmdAktion=Details>";


	// Alle Parameter auslesen
	//	Es wird aber nicht alles eingeblendet!
$TelSuche = explode(";",awis_BenutzerParameter($con, "CRMSuche", $AWISBenutzer->BenutzerName()));

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheName'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchName','',20,200,true);
$CursorFeld='sucSuchName';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SuchePLZ'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchPLZ','',10,0,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheOrt'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchOrt','',10,200,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',150);
$SQL = 'SELECT KON_KEY, KON_NAME1 || \', \' || KON_Name2 AS Name FROM Kontakte';
$SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KON_KEY = KZA_KON_KEY';
$SQL.=' WHERE KZA_KAB_KEY=268';
awis_FORM_Erstelle_SelectFeld('*SucheBetreuer','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheStrasse'].':',150);
awis_FORM_Erstelle_TextFeld('*SucheStrasse','',30,280,true);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();

echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png name=cmdSuche value=\"Aktualisieren\">";
if(($Recht3700&4) == 4)		// Hinzufügen erlaubt?
{
	echo "&nbsp;<input type=image name=cmdDSNeu accesskey=n title='".$AWISSprachKonserven['Wort']['lbl_hinzufuegen']."' src=/bilder/plus.png name=cmdDSNeu>";// onclick=location.href='./crm_Main.php?cmdAktion=Liste&txtADRKey=-1&DSNeu=True'>";
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>