<?php

require_once 'awisBerichte.inc';

class ber_Palettenscheine extends awisBerichte {

    public function init(array $Parameter) {

        $SQL = 'select FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT ';
        $SQL .= 'from filialen ';
        $SQL .= 'where FIL_ID = ' . $this->_DB->WertSetzen('FIL','N0',$_POST['txtFIL_ID'],false,0);

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FIL',true));

        $TextKonserven[]=array('Wort', 'ANZAHL_PAL');
        $TextKonserven[]=array('SYSTEM','PROGNAME');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

    }

    public function ErzeugePDF($Ausgabeart = 1) {

        $this->SetTitle('Plalettenschein');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0,0,0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);

        // Feld Anzahl_Paletten *2 steuert wieviele Seiten gedruckt werden müssen.
        $i = 0;
        while($i < $_POST['txtPAL_ANZ']*2) {

            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Spalte = $this->_Parameter['LinkerRandUeberschrift'];

            $this->NeueSeite();

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseUeberschrift']);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRandUeberschrift'], $this->_Parameter['ZeilenhoeheUeberschrift'],
                str_pad($this->_rec->FeldInhalt('FIL_ID', 'T'), 3, 0, STR_PAD_LEFT), 0, 0, 'C');

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesse']);

            $this->_Zeile = $this->_Parameter['YPositionText'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'],
                $this->_rec->FeldInhalt('FIL_BEZ', 'T'), 0, 0, 'L');

            $this->_Zeile += $this->_Parameter['ZeilenHoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'],
                $this->_rec->FeldInhalt('FIL_STRASSE', 'T'), 0, 0, 'L');

            $this->_Zeile += $this->_Parameter['ZeilenHoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'],
                $this->_rec->FeldInhalt('FIL_PLZ', 'T') . '     ' . $this->_rec->FeldInhalt('FIL_ORT', 'T'), 0, 0, 'L');

            $this->_Zeile += $this->_Parameter['ZeilenHoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'],
                $this->_AWISSprachkonserven['Wort']['ANZAHL_PAL'] . ': ' . $_POST['txtPAL_ANZ'], 0, 0, 'L');

            $this->_Zeile += $this->_Parameter['ZeilenHoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $_POST['txtPAL_TEXT'], 0, 0,
                'L');
            $i++;
        }
        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************

        switch($Ausgabeart)
        {
            case 1:			// PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName.'.pdf','D');
                break;
            case 2:			// Standard-Stream
                $this->Output($this->_BerichtsName.'.pdf','I');
                break;
            case 3:			// Als String zurückliefern
                return($this->Output($this->_BerichtsName.'.pdf','S'));
                break;
            case 4:				// Als Datei speichern
                $this->Output($this->_Dateiname,'F');
                break;
        }

    }

    private function NeueSeite() {
        $this->AddPage('L');
    }
}