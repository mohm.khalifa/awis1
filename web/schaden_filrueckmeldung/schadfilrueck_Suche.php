<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
	$TextKonserven[]=array('BES','*');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht40015 = $AWISBenutzer->HatDasRecht(40015);

	if($Recht40015==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

		$Form->SchreibeHTMLCode('<form name="frmSBRsuche" action="./schadfilrueck_Main.php?cmdAktion=Details" method="POST"  >');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBR'));

		$Form->Formular_Start();

	
		/**
		 * Filialfeld
		 **/
		
		$SQL ='select feb_key,feb_bezeichnung from v_filialebenenrollen_aktuell';
		$SQL .=' inner join Filialebenen on xx1_feb_key = feb_key';

		if($AWISBenutzer->BenutzerKontaktKEY() != 0 or ($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='') // Wenn GL
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('SBR_FIL_ID', $FilZugriff, 1,150, false, '', '', '', 'T', 'L','');
				$Form->Erstelle_HiddenFeld('SBR_FIL_ID',$FilZugriff);
				$AWISCursorPosition = 'txtSBF_FIL_ID';
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'].':',200);
				$Form->Erstelle_TextFeld('*SBR_BEARBNR',($Param['SPEICHERN']=='on'?$Param['SBR_BEARB']:''),10,150,true,'','','','T','L','','',10);
				$Form->ZeileEnde();

				// Historische Vorg�nge der Filiale anzeigen
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORGANG'].':',200);
				$Daten = explode('|',$AWISSprachKonserven['BES']['lst_VORGANG']);
				$Form->Erstelle_SelectFeld('*BES_VORGANG',($Param['SPEICHERN']=='on'?$Param['BES_STATUS']:''),150,true,'','','O','','',$Daten);
				$Form->ZeileEnde();

				$SQL ='select ID,WERT from Antragart order by WERT';

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AART'] . ':',200);
				$Form->Erstelle_SelectFeld('*BES_AART',$Param['SPEICHERN']=='on'?$Param['BES_AART']:'',120,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','','','','',array(),'','SCHAD');
				$Form->ZeileEnde();

				// Auswahl kann gespeichert werden
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
				$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
				$Form->ZeileEnde();

			}
			elseif(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='' and ($Recht40015&4)==4) // Wenn Admin
			{
				$SQL .=' group by feb_key,feb_bezeichnung';
				$SQL .=' order by feb_bezeichnung';
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('*SBR_FIL_ID',($Param['SPEICHERN']=='on'?$Param['SBR_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
				$Form->ZeileEnde();
				$AWISCursorPosition = 'sucSBR_FIL_ID';

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'].':',200);
				$Form->Erstelle_TextFeld('*SBR_BEARBNR',($Param['SPEICHERN']=='on'?$Param['SBR_BEARB']:''),10,150,true,'','','','T','L','','',10);
				$Form->ZeileEnde();

				// Historische Vorg�nge der Filiale anzeigen
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORGANG'].':',200);
				$Daten = explode('|',$AWISSprachKonserven['BES']['lst_VORGANG']);
				$Form->Erstelle_SelectFeld('*BES_VORGANG',($Param['SPEICHERN']=='on'?$Param['BES_STATUS']:''),150,true,'','','O','','',$Daten);
				$Form->ZeileEnde();

				$SQL ='select ID,WERT from Antragart order by WERT';

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AART'] . ':',200);
				$Form->Erstelle_SelectFeld('*BES_AART',$Param['SPEICHERN']=='on'?$Param['BES_AART']:'',120,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','','','','',array(),'','SCHAD');
				$Form->ZeileEnde();

				// Auswahl kann gespeichert werden
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
				$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
				$Form->ZeileEnde();
			
			}
			elseif(($Recht40015&2)==2) // Wenn GBL oder RL oder RGZ
			{
				//Nur f�r GBL/RL/RGZ und ihre Vertreter 14.01.2015 ST
				$SQL .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
				$SQL .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (23,25,57,26,69,72))';
				$SQL .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
				$SQL .=' and trunc(frv_datumvon) <= trunc(sysdate)';
				$SQL .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57,26,69,72))';
				$SQL .=' group by feb_key,feb_bezeichnung';
				$SQL .=' order by feb_bezeichnung';
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',180);
			
				$Form->Erstelle_SelectFeld('*SBR_FIL_ID',$Param['SBR_FIL_ID'], 400, true, '***SBR_Daten;sucSBR_FIL_ID;Gebiet=*sucFIL_GEBIET&ZUSATZ='.$AWISSprachKonserven['Liste']['lst_ALLE_0'].'&KON='.$AWISBenutzer->BenutzerKontaktKEY(),(($Param['SBR_FIL_ID'] == '0' or $Param['SPEICHERN']=='')?$AWISSprachKonserven['Liste']['lst_ALLE_0']: $Param['SBR_FIL_ID'].'~'.$Param['SBR_FIL_ID']),'', '', '','','');
				//$Form->Erstelle_SelectFeld('*QMM_FIL_ID',($Param['SPEICHERN']=='on'?$Param['QMM_FIL_ID']:'0'),400,true,$SQL2,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'].':',200);
				$Form->Erstelle_TextFeld('*SBR_BEARBNR',($Param['SPEICHERN']=='on'?$Param['SBR_BEARB']:''),10,150,true,'','','','T','L','','',10);
				$Form->ZeileEnde();

				// Historische Vorg�nge der Filiale anzeigen
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORGANG'].':',200);
				$Daten = explode('|',$AWISSprachKonserven['BES']['lst_VORGANG']);
				$Form->Erstelle_SelectFeld('*BES_VORGANG',($Param['SPEICHERN']=='on'?$Param['BES_STATUS']:''),150,true,'','','O','','',$Daten);
				$Form->ZeileEnde();

				$SQL ='select ID,WERT from Antragart order by WERT';

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AART'] . ':',200);
				$Form->Erstelle_SelectFeld('*BES_AART',$Param['SPEICHERN']=='on'?$Param['BES_AART']:'',120,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','','','','',array(),'','SCHAD');
				$Form->ZeileEnde();

				// Auswahl kann gespeichert werden
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
				$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
				$Form->ZeileEnde();
				
			}

		}
		else 
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['BES']['ERR_KONTAKT']);
			$Form->ZeileEnde();
		}
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/schaden_neu/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if($AWISBenutzer->BenutzerKontaktKEY() != 0 or ($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
		$Form->SchreibeHTMLCode('</form>');
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>