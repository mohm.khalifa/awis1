<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class mailtexte_funktionen
{
    /**
     * @var awisFormular
     */
    public $Form;

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var awisBenutzer
     */
    public $AWISBenutzer;

    /**
     * Recht f�r die Maske
     *
     * @var int
     */
    public $Recht640;

    /**
     * AWISSprachKonserven
     *
     * @var array
     */
    public $AWISSprachKonserven;

    /**
     * CursorPosition der Seite
     *
     * @var String
     */
    public $AWISCursorPosition;

    /**
     * Parameter der Seite
     *
     * @var mixed
     */
    public $Param;

    /**
     * Datensatzkey
     *
     * @var string
     */
    public $AWIS_KEY1;

    /**
     * mailtexte_funktionen constructor.
     */
    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht640 = $this->AWISBenutzer->HatDasRecht(640);

        $this->Param = unserialize($this->AWISBenutzer->ParameterLesen('Formular_MVT'));

        $TextKonserven[] = array('MVT', '%');
        $TextKonserven[] = array('TITEL', 'tit_mailtexte');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Meldung', 'InsertOK');
        $TextKonserven[] = array('Meldung', 'UpdateOK');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
        $this->AWISBenutzer->ParameterSchreiben('Formular_MVT', serialize($this->Param));
    }

    /**
     * Pr�ft die Rechte
     *
     * @param int $Bit
     */
    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht640 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'MVT'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    /**
     * Erstellt dei Bedingung
     *
     * @return string
     */
    public function BedingungErstellen(){
        $return = '';

        if($this->AWIS_KEY1 != ''){
            $return .= ' AND MVT_KEY = ' . $this->DB->WertSetzen('MVT','T',$this->AWIS_KEY1);
        }
        return $return;
    }
}