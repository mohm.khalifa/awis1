<?php
require_once('awisArtikelstammImport.php');
require_once('awisDatenbank.inc');

try 
{
	$Artikel = new awisArtikelstammImport(0,'riedl_t');
	$Artikel->ImportArtikel();
	#$Artikel->ImportArtikelTest(0,'riedl_t');
		
}
catch (Exception $ex)
{	
	echo PHP_EOL.'awisArtikelstammImport: Fehler: '.$ex->getMessage().PHP_EOL;
	
	$AWISWerkzeug = new awisWerkzeuge();
	awisWerkzeuge::EMail('thomas.riedl@de.atu.eu','stefan.oppl@de.atu.eu','christian.beierl@de.atu.eu','Artikelstammimport-Fehler','Fehler :' .$ex->getMessage(),3,'','awis@de.atu.eu');
}
