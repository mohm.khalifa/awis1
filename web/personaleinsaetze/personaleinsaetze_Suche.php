<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20081006
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PEI','PEI_DATUM');
	$TextKonserven[]=array('PEI','PEI_PEA_KEY');
	$TextKonserven[]=array('PEI','PEI_FIL_ID');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('PBM','PBM_XBN_KEY');
	$TextKonserven[]=array('PEI','PEI_PEA_KEY');
	$TextKonserven[]=array('FER','FER_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','txt_OhneZuordnung');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Liste','lst_ALLE_0');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4500=$AWISBenutzer->HatDasRecht(4500);		// Rechte des Mitarbeiters

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./personaleinsaetze_Main.php?cmdAktion=Details>");

	// Rechte des Benutzers ermitteln
	$SQL = 'SELECT PBR_PBZ_KEY ';
	$SQL .= ' FROM PerseinsbereicheRechteVergabe';
	$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
	$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	//Perseins�tze Rechte pr�fen
	$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	$SQL .= ' AND XBN_STATUS = \'A\'';
	$rsPBR = $DB->RecordSetOeffnen($SQL);
	$RechtPEI = 0;
	while(!$rsPBR->EOF())
	{
		$RechtPEI |= pow(2,$rsPBR->FeldInhalt('PBR_PBZ_KEY')-1);
		$rsPBR->DSWeiter();
	}

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
		$Param['PBM_XBN_KEY']=$AWISBenutzer->BenutzerID();
	}

	$Form->Formular_Start();
	$SQL = "SELECT DISTINCT xbn_key, xbn_name || coalesce(' '||xbn_vorname,'') AS Anwender ";
	$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
	$SQL .= "       FROM Perseinsbereichemitglieder";
	$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
	$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
	$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
	$SQL .= " GROUP BY PEB_KEY) Bereiche";
//	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
	//Perseinsaetze Rechte pr�fen
	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key "; //TR 07.02.11
// 04.06.2009 SK
	//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key ";	
	//$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
	//$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';
	$SQL .= " INNER JOIN Benutzer ON pbm_xbn_key = xbn_key";
	$SQL .= ' ORDER BY 2';
	$Form->DebugAusgabe(1,$SQL);
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PBM']['PBM_XBN_KEY'].':',190);
	$Form->Erstelle_SelectFeld('PBM_XBN_KEY',($Param['SPEICHERN']=='on'?$Param['PBM_XBN_KEY']:$AWISBenutzer->BenutzerID()),100,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','',$AWISBenutzer->BenutzerID(),'');
	$AWISCursorPosition = 'txtPBM_XBN_KEY';
	$Form->ZeileEnde();

	// Datumsbereich festlegen
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*PEI_DATUM_VOM',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('d.m.Y'))),10);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',120);
	$Form->Erstelle_TextFeld('*PEI_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('d.m.Y'))),10);
	$Form->ZeileEnde();

	// Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_FIL_ID'].':',190);
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	}
	else
	{
		$Form->Erstelle_TextFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
	}
	$Form->ZeileEnde();


	//***********************************
	// Erweiterte Suche
	//***********************************

	// Personaleinsatzart
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_PEA_KEY'].':',190);
	$SQL = 'SELECT PEA_KEY, PEA_BEZEICHNUNG FROM PerseinsArten ORDER BY PEA_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*PEI_PEA_KEY',($Param['SPEICHERN']=='on'?$Param['PEI_PEA_KEY']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();

	// Rolle
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'].':',190);
	$SQL = 'SELECT FER_KEY, FER_BEZEICHNUNG FROM FilialEbenenRollen ORDER BY FER_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*FER_KEY',($Param['SPEICHERN']=='on'?$Param['FER_KEY']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();

	$Form->Trennzeile();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();


	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht4500&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	if(($Recht4500&64) == 64)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('href', 'cmdDSExp', './personaleinsaetze_export_lf_letzte_Excel.php', '/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_export'], 'N');
	}
	
	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200810061222");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"20081��61223");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>