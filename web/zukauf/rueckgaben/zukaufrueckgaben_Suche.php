<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUL','%');
	$TextKonserven[]=array('ZUB','ZUB_ZLH_KEY');
	$TextKonserven[]=array('AST','AST_ATUNR');
	$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Datum%');
	$TextKonserven[]=array('Wort','Ansicht');
	$TextKonserven[]=array('ZRU','def_NurOffene');
	$TextKonserven[]=array('Liste','ZUL_ENTLASTUNG');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10050 = $AWISBenutzer->HatDasRecht(10050);
	if($Recht10050==0)
	{
	    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	    $Form->SchreibeHTMLCode("<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>");
		$Form->SchreibeHTMLCode("<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>");
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./zukaufrueckgaben_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_FIL_ID'].':',190);
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('*ZUL_FIL_ID','',150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	}
	else
	{
		$Form->Erstelle_TextFeld('*ZUL_FIL_ID','',10,180,true,'','','','T','L','','',10);
	}
	$AWISCursorPosition='sucZUL_FIL_ID';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZUL_ARTIKELNUMMER','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*ZUL_ARTIKELBEZEICHNUNG','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIE_NR'].':',190);
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM Lieferanten';
	$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('*ZUL_LIE_NR','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*ZLH_KEY','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WANR'].':',190);
	$Form->Erstelle_TextFeld('*ZUL_WANR','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERSCHEINNR'].':',190);
	$Form->Erstelle_TextFeld('*ZUL_LIEFERSCHEINNR','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*DATUMVOM','',20,200,true);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('*DATUMBIS','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Ansicht'].':',190);
	$Gruende = explode("|",$AWISSprachKonserven['Liste']['ZUL_ENTLASTUNG']);
	$OptionOffene = '-1~'.$AWISSprachKonserven['ZRU']['def_NurOffene'];
	$Form->Erstelle_SelectFeld('*ZUL_ENTLASTUNG','',"300:290",true,'',$OptionOffene,'','','',$Gruende);
	$Form->ZeileEnde();


	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht10050&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		$Form->SchreibeHTMLCode('allg. Fehler:'.$ex->getMessage());
	}
}
?>