<?php

global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichSenden');
$TextKonserven[]=array('Wort','SendenOK');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try {

if(isset($_GET['EMailWEB_KEY']))
{
        $TXT_Senden = $Form->LadeTexte($TextKonserven);
	
	$Text = '';

        $SQL = " Select DISTINCT WBE_FILIALEN FROM WERBEMITTELZUORDNUNG WHERE WBE_WEB_KEY=".$DB->FeldInhaltFormat('NO',$_GET['EMailWEB_KEY']);
	$SQL .= " ORDER BY WBE_FILIALEN ASC";

        $rsDifferenzMails = $DB->RecordSetOeffnen($SQL);

	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./verteilqualitaet_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

	 $Form->Formular_Start();
	 $Form->ZeileStart();
	 $Form->Hinweistext($TXT_Senden['Wort']['WirklichSenden']);
	 $Form->ZeileEnde();

	 $PEIKey = '';

	 while(!$rsDifferenzMails->EOF())
	 {
	    $PEIKey  .= $rsDifferenzMails->FeldInhalt('WBE_FILIALEN');
	    $PEIKey .= ', '; 
	 
	    $rsDifferenzMails->DSWeiter();
	 }

	 $Form->ZeileStart();
	 $Form->Erstelle_TextFeld('WEB_FILIALEN',"Moechten Sie an folgende Filialen ".$PEIKey." eine Benachrichtigung senden",800,800,false);
	 $Form->ZeileEnde();

	 $Form->Erstelle_HiddenFeld('EMailWEB_KEY', $_GET['EMailWEB_KEY']);
          	
	  $Form->ZeileStart();
	  $Form->Schaltflaeche('submit','cmdEMAILSendenOK','','',$TXT_Senden['Wort']['Ja'],'');
	  $Form->Schaltflaeche('submit','cmdEMAILAbbrechen','','',$TXT_Senden['Wort']['Nein'],'');
                
          $Form->ZeileEnde();

	  $Form->SchreibeHTMLCode('</form>');

	  $Form->Formular_Ende();
}
elseif(isset($_POST['cmdEMAILSendenOK']))
{
	$this->_Werkzeug = new awisWerkzeuge();
	
	$TXT_Senden = $Form->LadeTexte($TextKonserven);
	
	$Text = '';

        $SQL = " Select DISTINCT WBE_FILIALEN FROM WERBEMITTELZUORDNUNG WHERE WBE_WEB_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtEMailWEB_KEY']);
	$SQL .= " ORDER BY WBE_FILIALEN ASC";

        $rsDifferenzMails = $DB->RecordSetOeffnen($SQL);

        $FIL_ID = '';
        $Laenge = '';

        $nr=0;
		
		while(!$rsDifferenzMails->EOF())
        {
	    $Text = "";	
	
            $FIL_ID = '';
            $FIL_ID = $rsDifferenzMails->FeldInhalt('WBE_FILIALEN');
            $Laenge = strlen($FIL_ID);

            if($Laenge == 1)
            {
               $FIL_ID = '000'.$FIL_ID;

            }
            elseif ($Laenge == 2)
            {
               $FIL_ID = '00'.$FIL_ID;
            }
            elseif($Laenge == 3)
            {
               $FIL_ID = '0'.$FIL_ID;
            }
	    
		$Text  = 'Liebe Kolleginnen und Kollegen,';
		$Text .= "\r \n";
		$Text .= "Im Zuge des Qualit�tsmanagements zur Beilagen-Streuung m�chten wir Sie bitten, die";
		$Text .= " Mitarbeiter-Befragung wie �blich im AWIS �ber den Punkt �Operative";
		$Text .= " Filialt�tigkeiten� zu bearbeiten.";
		$Text .= "\r \n";
		$Text .= "Die dort hinterlegte Abfrage kann innerhalb der n�chsten 3 Werktage von Ihnen";
		$Text .= " beliebig oft ge�ffnet und erg�nzt werden. Danach ist keine Eingabe mehr m�glich.";
		$Text .= "\r \n";
		$Text .= "Sollte die Beantwortung der �Erhalten-Frage� f�r einen Mitarbeiter aus bestimmten";
		$Text .= " Gr�nden (Urlaub, Krankheit etc.) nicht m�glich sein, so bitten wir Sie, die Antwort-";
		$Text .= " M�glichkeit bei �Bitte beantworten/Keine Angabe� zu belassen. \r \n";
		$Text .= "\r \n";
		$Text .= "Bei R�ckfragen k�nnen Sie sich gerne an Frau Barbara Gruber unter der Durchwahl 5611 ";
		$Text .= " wenden.\r \n";
		$Text .= "\r \n";
		$Text .= "Vielen Dank f�r Ihre Mithilfe und freundliche Gr��e\r \n";
		$Text .= "\r \n";
		$Text .= "Ihr Marketing-Team ";

        $nr++;
            
		// PRODUKTIV: 
            //$this->_Werkzeug->EMail($FIL_ID.'@de.atu.eu','Mitarbeiterbefragung zur Verteilqualit�t', $Text,3,'Marketing@de.atu.eu','Marketing@de.atu.eu');
                
        // TESTUMGEBUNG
            $this->_Werkzeug->EMail('Stefan.Oppl@de.atu.eu','Mitarbeiterbefragung zur Verteilqualit�t NO:'.$nr, $Text,3,'Marketing@de.atu.eu','Marketing@de.atu.eu');

	    $rsDifferenzMails->DSWeiter();
        }
	
	$Form->Formular_Start();
	$Form->ZeileStart();
	$Form->Hinweistext($TXT_Senden['Wort']['SendenOK']);
	$Form->ZeileEnde();
	 
	
	
	$AWIS_KEY1 = $_POST['txtEMailWEB_KEY'];
	
	
    
    }
}
catch (Exception $ex)
{
if($Form instanceof awisFormular)
{

        //$Form->Fehler _Anzeigen('INTERN',$ex->getMessage('MELDEN'),6,"200905131735");
}
else
{
	//echo 'allg. Fehler:'.$ex->getMessage();
}
}

?>