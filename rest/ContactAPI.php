<?php

try{
    require_once 'awisDatenbank.inc';
    require_once 'awisBenutzer.inc';
    require_once 'awisRest.inc';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Benutzer = awisBenutzer::Init('awis_jobs');

    $awisREST = new awisRest('RoleAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();

    $WHERE='';

    $SQL = 'SELECT * from (';
    $SQL .= 'SELECT SURNAME,GIVENNAME,EMAIL,TELEPHONE,DEPARTMENT, coalesce(XBL_XBN_KEY, XBN_KEY) as XBN_KEY';
    $SQL .= ' from (';
    $SQL .=' SELECT';
    $SQL .=' ADE_SURNAME as SURNAME,';
    $SQL .=' ADE_GIVENNAME as GIVENNAME,';
    $SQL .=' ADE_MAIL as EMAIL,';
    $SQL .=' ADE_TELEPHONENUMBER as TELEPHONE,';
    $SQL .=' ADE_DEPARTMENT as DEPARTMENT,';
    $SQL .=' ADE_CN, ';
    $SQL .=' NULL as XBN_KEY ';
    $SQL .=' FROM ACTIVEDIRECTORYEXPORT';

    $SQL .=' union';

    $SQL .=' select adv_surname as SURNAME,';
    $SQL .=' \'\' as GIVENNAME,';
    $SQL .=' ADV_MAIL AS EMAIL,';
    $SQL .=' \'\' as TELEPHONE,';
    $SQL .=' \'\' as DEPARTMENT,';
    $SQL .=' \'\' as ADE_CN,';
    $SQL .=' ADV_XBN_KEY as XBN_KEY';

    $SQL .=' from ACTIVEDIRECTORYVERTEILER';
    $SQL .=')';
    $SQL  .= " left join benutzerlogins ";
    $SQL  .= " on xbl_login = upper(ADE_CN) ";
    if(isset($Request['ONLYWITHXBN']['WERT']) and $Request['ONLYWITHXBN']['WERT'] == 1){
        $WHERE  .= " and xbn_key is not null ";
    }

    if(isset($Request['SEARCH_PHRASE']['WERT'])){
        $WHERE .= ' and  (lower(SURNAME) ' .$DB->LikeOderIst('%'.mb_strtolower($Request['SEARCH_PHRASE']['WERT']). '%',0,'API') . ' ';
        $WHERE .= ' or lower(GIVENNAME) ' . $DB->LikeOderIst('%'.mb_strtolower($Request['SEARCH_PHRASE']['WERT']).'%',0,'API').' ';
        $WHERE .= ' or lower(EMAIL) ' .$DB->LikeOderIst('%'.mb_strtolower($Request['SEARCH_PHRASE']['WERT']).'%',0,'API').' )';
    }

    if(isset($Request['XBN_KEY']['WERT'])){
        $WHERE .= ' and XBN_KEY = ' .$DB->WertSetzen('API','T',$Request['XBN_KEY']['WERT']).' ';
    }
    $SQL .= ") ";
    if($WHERE != ''){
        $SQL .= ' where '.substr($WHERE, 4);
    }

    $SQL .=' ORDER BY SURNAME';

    $rsKontakte = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('API'));



    $awisREST->sendeResponse($rsKontakte,awisRest::AntwortTyp_JSON);

}

catch(awisException $e){
    var_dump($e);
}catch (Exception $e){
    var_dump($e);
}