<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php

require_once('awisAusdruck.php');
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $DBServer;

clearstatcache();

include ("ATU_Header.php");

$DBServer = awisDBServer();

$conSchad = awisAdminLogon($DBServer, "schaddev09", "schad");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3300);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Schaden-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_REQUEST);

if ($_REQUEST['BEARB_NR']!='' && ($RechteStufe&64)==64)
{
	/***************************************
	* Hole Daten per SQL
	***************************************/		
	$SQL="SELECT SCH.BEARBEITUNGSNR, SCH.EINGABEAM, SCH.FILNR, SCH.GESCHLECHT, SCH.KFZ_KENNZ,";
	$SQL.=" SCH.KUNDENNAME, SCH.PLZ, SCH.ORT, SCH.STRASSE, SCH.TELEFON, SCH.TELEFON2,";
	$SQL.=" SCH.FAX, SCH.WANR, SCH.PAN, SCH.EMAIL, SCH.VORNAME, SCH.TERMIN_KUNDE_1, SCH.ATUNR, ";
	$SQL.=" SCH.TERMIN_KUNDE_2, SCH.TERMIN_KUNDE_WER_1, SCH.TERMIN_KUNDE_WER_2, SCH.EINGABEDURCH_FIL,";
	$SQL.=" SB.SBNAME as SACHBEARBEITER, EP.MITTEL as EINGANG, KK.WERT as KONTAKT,";
	$SQL.=" IGBL.WERT as INFOGBL, KEN.KENNUNG_BEZEICHNUNG as KENBEZEICHNUNG, AUF.AUFTRAGSART_ATU as AUFTRAGSART,";
	$SQL.=" SGR.GRUND as SCHADENSGRUND, BES.SCHLAGWORT as BESCHAEDIGT, AU.WERT as AUSFALLURSACHE, SCH.GROSSKDNR as GROSSKDNR,";
	$SQL.=" STA.BESCHREIBUNG as STAND, to_clob(SCH.FEHLERBESCHREIBUNG) as FEHLERBESCHREIBUNG";
	$SQL.=" FROM SCHAEDEN_NEW SCH";			
	$SQL.=" LEFT JOIN SACHBEARBEITER SB ON SB.ID = BEARBEITER";
	$SQL.=" LEFT JOIN EINGANGPER EP ON EP.ID = EINGANGPER";
	$SQL.=" LEFT JOIN KONTAKT_KUNDE KK ON KK.ID = KONTAKT_KUNDE";
	$SQL.=" LEFT JOIN INFO_AN_GBL IGBL ON IGBL.ID = INFO_AN_GBL";
	$SQL.=" LEFT JOIN KENNUNGEN KEN ON KEN.KENNUNG = SCH.KENNUNG";
	$SQL.=" LEFT JOIN AUFTRAGSARTEN AUF ON ART_ID = AUFTRAGSART_ATU_NEU";
	$SQL.=" LEFT JOIN SCHADENSGRUND SGR ON SGR.ID = SCHADENSGRUND";
	$SQL.=" LEFT JOIN BESCHAEDIGT BES ON BES.BID = SCH.BID";
	$SQL.=" LEFT JOIN AUSFALLURSACHE AU ON AU.ID = AUSFALLURSACHE";
	$SQL.=" LEFT JOIN STAND STA ON STANDID = STAND";
	$SQL.=" WHERE BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";	
	
	awis_Debug(1,$SQL);
	
	$rsResult = awisOpenRecordset($conSchad,$SQL);
	$rsResultZeilen = $awisRSZeilen;	

	if($rsResultZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten f�r Bearbeitungs-Nr.: ".$_REQUEST['BEARB_NR']." gefunden!</span></center>");
	}
	else
	{
		$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,$rsResult['BEARBEITUNGSNR'][0]);
		
		
		$BEARBEITER=$rsResult['SACHBEARBEITER'][0];
		$BEARBEITUNGSNR=$rsResult['BEARBEITUNGSNR'][0];
		$EINGABEAM=$rsResult['EINGABEAM'][0];
		$EINGANGPER=$rsResult['EINGANG'][0];
		$FEHLERBESCHREIBUNG=$rsResult['FEHLERBESCHREIBUNG'][0];
//		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',chr(13).chr(10),$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',' ',$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FILNR=$rsResult['FILNR'][0];
		$GESCHLECHT=$rsResult['GESCHLECHT'][0];
		$KFZ_KENNZ=$rsResult['KFZ_KENNZ'][0];
		$KUNDENNAME=$rsResult['KUNDENNAME'][0];
		$PLZ=$rsResult['PLZ'][0];
		$ORT=$rsResult['ORT'][0];
		$STRASSE=$rsResult['STRASSE'][0];
		$TELEFON=$rsResult['TELEFON'][0];
		$TELEFON2=$rsResult['TELEFON2'][0];
		$FAX=$rsResult['FAX'][0];
		$WANR=$rsResult['WANR'][0];
		$PAN=$rsResult['PAN'][0];
		$EMAIL=$rsResult['EMAIL'][0];
		$VORNAME=$rsResult['VORNAME'][0];
		$KONTAKT_KUNDE=$rsResult['KONTAKT'][0];
		$INFOANGBL=$rsResult['INFOGBL'][0];
		$KENNUNG=$rsResult['KENBEZEICHNUNG'][0];
		$AUFTRAGSART=$rsResult['AUFTRAGSART'][0];
		$SCHADENSGRUND=$rsResult['SCHADENSGRUND'][0];
		$BESCHAEDIGT=$rsResult['BESCHAEDIGT'][0];
		$AUSFALLURSACHE=$rsResult['AUSFALLURSACHE'][0];
		$TERMINKUNDE1=$rsResult['TERMIN_KUNDE_1'][0];
		$TERMINKUNDE2=$rsResult['TERMIN_KUNDE_2'][0];
		$TERMINKUNDEWER1=$rsResult['TERMIN_KUNDE_WER_1'][0];
		$TERMINKUNDEWER2=$rsResult['TERMIN_KUNDE_WER_2'][0];
		$STAND=$rsResult['STAND'][0];
		$EINGABEDURCH=$rsResult['EINGABEDURCH_FIL'][0];
		$ATUNR=$rsResult['ATUNR'][0];
		$GROSSKDNR=$rsResult['GROSSKDNR'][0];
	}	

		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
		
		$Zeile=0;
		$Seitenrand=15;
		
		NeueSeite($Zeile,$Seitenrand);
		
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;
				
		$Y_Wert=$Y_Wert+5;			
		
		$Y_Wert=$Y_Wert+18;
		
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->SetXY($LinkerRand,$Y_Wert);		
		$Ausdruck->_pdf->cell(130,6,"Kundendaten",1,0,'L',1);		
		
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->cell(50,6,"Bearbeitungsdaten",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+6;
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Name:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$KUNDENNAME,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"WA-Nr.:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$WANR,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->SetFont('Arial','B',9);
		$Ausdruck->_pdf->cell(35,6,"Bearb-Nr.:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+150,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$BEARBEITUNGSNR,0,0,'L',0);
		$Ausdruck->_pdf->SetFont('Arial','',9);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Vorname :",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$VORNAME,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Fz.-Kennz.:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$KFZ_KENNZ,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Sachbearb.:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+150,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$BEARBEITER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Stra�e :",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$STRASSE,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"ATU-Card:",0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Filial-Nr.:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+150,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$FILNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"PLZ / Ort :",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$PLZ." ".$ORT,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$PAN,0,0,'L',0);
	
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Erfasst am:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+150,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$EINGABEAM,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Tel. 1:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$TELEFON,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"FAX:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$FAX,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+130,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Eingang per:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+150,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$EINGANGPER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Tel. 2:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$TELEFON2,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Gro�-Kd-Nr:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$GROSSKDNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"E-Mail:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+16,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$EMAIL,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+6;
		
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(180,6,"Beschwerde / Kommentar",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+7;
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->MultiCell(180,4,$FEHLERBESCHREIBUNG,0,'N',0);
		
		//$bla=$pdf->getStringWidth($FEHLERBESCHREIBUNG);
		//$Hoehe=round($bla/46+0.4,0);
		//$Y_Wert=$Y_Wert+$Hoehe+6;
		
		$Ausdruck->_pdf->Ln();
		$Y_Wert=$Ausdruck->_pdf->GetY();
		
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(180,6,"Beschwerdebearbeitung / R�ckmeldung Filiale",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+12;
						
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Y_Wert=$Y_Wert-4;	
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Kontakt mit Kunde:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$KONTAKT_KUNDE,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Termin mit Kunden am:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$TERMINKUNDE1,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(45,6,"Wer:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(45,6,$TERMINKUNDEWER1,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Termin mit Kunden am:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$TERMINKUNDE2,0,0,'L',0);
		
		$Ausdruck->_pdf->setXY($LinkerRand+70,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Wer:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+88,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$TERMINKUNDEWER2,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Info an GBL:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$INFOANGBL,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Kennung:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$KENNUNG,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Urspr. Auftrag:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$AUFTRAGSART,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Woran gearbeitet:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$SCHADENSGRUND,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Was besch�digt:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$BESCHAEDIGT,0,0,'L',0);
		
		if ($AUSFALLURSACHE!='')
		{
			$Y_Wert=$Y_Wert+4;
		
			$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
			$Ausdruck->_pdf->cell(35,6,"Ausfallursache:",0,0,'L',0);
			$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
			$Ausdruck->_pdf->cell(35,6,$AUSFALLURSACHE,0,0,'L',0);
		}
		
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"verursachende ATU-NR:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$ATUNR,0,0,'L',0);
				
		$Y_Wert=$Y_Wert+4;
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,"Bearbeitungsstand:",0,0,'L',0);
		$Ausdruck->_pdf->setXY($LinkerRand+35,$Y_Wert);
		$Ausdruck->_pdf->cell(35,6,$STAND,0,0,'L',0);
			
		$Y_Wert=$Y_Wert+10;
		
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(180,6,"Diagnose / Ergebnis und Besprochene Ma�name",1,0,'L',1);		
		
		$SQL = "SELECT B.BEARBNRNEU, to_clob(B.BEMERKUNGEN) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW B";
		$SQL .= " WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW WHERE BEARBEITUNGSNR='". $BEARBEITUNGSNR ."') ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
		
		$rsSchadInfo = awisOpenRecordset($conSchad,$SQL,true,true);
		$rsSchadInfoZeilen = $awisRSZeilen;		
		
		$Y_Wert=$Y_Wert+7;
		$Ausdruck->_pdf->SetFont('Arial','B',9);
		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		
		$Ausdruck->_pdf->setX($LinkerRand);
		$Ausdruck->_pdf->cell(20,5,'Datum',1,0,'L',0);			
		$Ausdruck->_pdf->setX($LinkerRand+20);
		$Ausdruck->_pdf->cell(35,5,'Eintrag',1,0,'L',0);
		$Ausdruck->_pdf->setX($LinkerRand+55);
		$Ausdruck->_pdf->cell(15,5,'Betrag',1,0,'L',0);
		$Ausdruck->_pdf->setX($LinkerRand+70);
		$Ausdruck->_pdf->cell(20,5,'Zahlungsart',1,0,'L',0);
		$Ausdruck->_pdf->setX($LinkerRand+90);			
		$Ausdruck->_pdf->MultiCell(90,5,'Diagnose / Ergebnis',1);
		$Ausdruck->_pdf->Ln(1);		
		$Ausdruck->_pdf->SetFont('Arial','',9);
    					
		$Y_Wert2=$Y_Wert;
		
		$Zeile=0;
		$Seitenrand=15;
		
		NeueSeite($Zeile,$Seitenrand);
		for ($i=0;$i<$rsSchadInfoZeilen;$i++)
		{
			$bla=$Ausdruck->_pdf->getStringWidth($rsSchadInfo['BEMERKUNG'][$i]);
			$Hoehe=round($bla/46+0.4,0);
			$Y_Wert2=$Y_Wert2+$Hoehe;
			
			//if ($Y_Wert2 > 280)
			if ($Y_Wert2 > 275) //urspr�nglich 275
			{
				$Y_Wert=16;
				$Y_Wert=$Y_Wert+10;				
				
				$Zeile=0;
				$Seitenrand=15;
		
				NeueSeite($Zeile,$Seitenrand);
				$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);				
			}
						
			$Ausdruck->_pdf->setX($LinkerRand);
			$Ausdruck->_pdf->cell(20,5,$rsSchadInfo['DATUM'][$i],0,0,'L',0);			
			$Ausdruck->_pdf->setX($LinkerRand+20);
			$Ausdruck->_pdf->cell(35,5,$rsSchadInfo['EINTRAG'][$i],0,0,'L',0);
			$Ausdruck->_pdf->setX($LinkerRand+55);
			$Ausdruck->_pdf->cell(15,5,$rsSchadInfo['BETRAG'][$i],0,0,'L',0);
			$Ausdruck->_pdf->setX($LinkerRand+70);
			$Ausdruck->_pdf->cell(20,5,$rsSchadInfo['ZAHLUNGSART'][$i],0,0,'L',0);
			$Ausdruck->_pdf->setX($LinkerRand+90);			
			$Ausdruck->_pdf->MultiCell(90,5,$rsSchadInfo['BEMERKUNG'][$i],0);
			$Ausdruck->_pdf->Ln(1);								        	        	
			$Y_Wert2=$Ausdruck->_pdf->GetY();
		}
		
		$Ausdruck->Anzeigen();				
}	
	
/**
*
* Funktion erzeugt eine neue Seite
*
* @author Thomas Riedl
* @param  pointer Zeile
* @param  int LinkerRand
*
*/
function NeueSeite(&$Zeile,$LinkerRand)
{	
	global $Ausdruck;
	
	$Ausdruck->NeueSeite(0,1);
	
	$Ausdruck->_pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$Ausdruck->_pdf->setXY($LinkerRand,290);					// Cursor setzen
	$Ausdruck->_pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
		
	$Ausdruck->_pdf->setXY($LinkerRand,10);					// Cursor setzen
	$Ausdruck->_pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$Ausdruck->_pdf->SetFillColor(255,255,255);
	$Ausdruck->_pdf->cell(180,6,"ATU Qualit�tsmanagement - Beschwerdeprotokoll -",0,0,'C',0);
	
	
	// Ueberschrift setzen
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Ausdruck->_pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	
	$Zeile = 22;
}	
	
	
?>
</body>
</html>
