<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
$con = awisLogon();
?>

<title></title>
</head>

<body>

<h2>AWIS - Filialsuche</h2>

Mit diesem Fenster haben Sie die M�glichkeit, Filialen nach unterschiedlichen Kriterien zu suchen. 
Werden keine Kriterien angegeben, so werden alle Filialen im System angezeigt. 
<br><br>
Um nach Filialen zu suchen, f�llen Sie alle gew�nschten Suchfelder aus. Je mehr Felder Sie ausf�llen,
desto weniger Filialen werden gefunden, d.h. desto genauer ist die Trefferliste. Wird nur eine Filiale 
gefunden, so werden sofort die Details der Filiale angezeigt.
<br><br>
<table id=DatenTabelle border=1 width=95%>
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Bedeutung</td></tr>
<tr><td>Filial-Nummer</td><td>Eine eindeutige Filialnummer. Es k�nnen auch Jokerzeichen angegeben werden, z.B. 3*.</td></tr>
<tr><td>Filial-Name</td><td>Der Name bzw. die Bezeichnung der Filiale. Es wird nach allen Filialen gesucht, die die angegebenen Zeichen enthalten, d.h. es m�ssen keine Jokerzeichen angegeben werden.</td></tr>
<tr><td>Filial-Ort</td><td>Der Ort, in dem die Filiale steht. Es wird nach allen Orten gesucht. Es k�nnen Jokerzeichen angegeben werden.</td></tr>
<tr><td>Filial-Plz</td><td>Die Postleitzahl der Filiale. Es k�nnen Jokerzeichen verwendet werden, z.B. 93*.</td></tr>
<tr><td>Auswahl speichern</td><td>Dieses Feld beeinflu�t die Suchmaske selbst. Normalerweise wird die
							Suchmaske mit leeren Suchfeldern ge�ffnet. Wird diese Option aktiviert,
							so werden alle ausgef�llten Suchbegriffe gespeichert und alle Werte
							bei der n�chsten Suche automatisch in die Suchfelder eingetragen.</td></tr>
</table>

<hr>
<h2><a name=CCSortierung>Register�bersicht</h2>


<p>In der Filialinformation werden unterschiedliche Register angezeigt.</p>
<table border=1 width="95%" height="103">
  <tr>
    <td id=FeldBez width="27%" height="47">
      <p style="line-height: 100%"><font size="4">Register</font></td>
    <td id=FeldBez width="73%" height="47">
      <p style="line-height: 100%"><font size="4">Inhalt</font></td>
  </tr>

  <tr>
    <td width="27%" height="19" valign="top">
      <p style="line-height: 100%"><b><a name=Suche>Suche</a></b></td>
    <td width="73%" height="19">
      <p style="line-height: 100%">Hier k�nnen die Suchparameter eingegeben werden.</td>
  </tr>


  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%"><a name=Filialinfos><b>Filialinfos</b></a></td>
    <td width="73%" height="19">
      <p style="line-height: 100%">In diesem Register werden entweder alle Filialen angezeigt, die den
	  Suchkriterien entsprechen. Sollte nur eine Filiale gefunden werden, werden automatisch die Details f�r die Filiale angezeigt.
	  
<?php

	if(isset($_GET['Filialinfos_Details']))
	{
		echo '<table width=100% border=1><tr><td class=FeldBez>Details zu der Filialinformation</td><tr><td>
		<ul>
		
		<li>In der ersten Spalte wird Ihnen die Anschrift der Filiale ausgegeben. Sollte die Filiale umziehen,
		so wird die neue Adresse und der Umzugstermin ausgegeben.
		</li>
		<li>In der zweiten Spalte sehen Sie die Telefonnummern und die �ffnungszeiten der Filiale. Au�erdem k�nnen
		Sie sehen, wann die Filiale er�ffnet wurde. Sollte die Filiale bereits einmal umgezogen sein, so wird der 
		Umzustermin ebenfalls ausgegeben. Der Termin f�r die Umstellung auf das neue Ladenkonzept wird angegeben f�r
		alle Filialen, die vor dem 1.8.2005 er�ffnet wurden. Alle Filialen, die nach diesem Termin er�ffnet wurden oder
		umgezogen sind, werden bereits mit dem neuen Ladenkonzept er�ffnet. In diesem Fall ist das Datum mit dem 
		Er�ffnungs- bzw. Umzugsdatum identisch.<br>
		Den Abschluss in dieser Spalte bildet der Informationsblock mit den Stundens�tzen und der Aktion f�r AU/HU.
		</li>
		<li>Die dritte Spalte enth�lt die Informationen �ber die Gesch�fts- und Werkstattleiter. Sollten im PEPIS System
		andere / zus�tzliche Mitarbeiter hinterlegt sein, so werden diese mit einem * gekennzeichnet und ebenfalls ausgegeben.<br>
		Die Angaben �ber VKL, TKDL, Gebietsleiter und das Regionalzentrum werden direkt im AWIS gepflegt und hier angezeigt.
		Gleiches gilt auch f�r den Anlieferungs- und Kommisioniertag.
		
		
		</ul></td></tr></table>';
	}
	else 
	{
		echo '<br><a href=./filialen_Hilfe.php?Filialinfos_Details target=_self>Details</a>';
	}

?>
  </td></tr>

<?php
	if(awisBenutzerRecht($con, 113) > 0)
	{
		echo '';
		echo '<tr>';
		echo '<td width=27% height=19>';
		echo '<p style=line-height: 100%; margin: 10><a name=Telefon><b>Telefonnummern</b></a></td>';
		echo '<td width=73% height=19>';
		echo '<p style=line-height: 100%; margin: 10>';

		echo '
		In diesem Register werden zus�tzliche Telefonnummern angezeigt.		
		';

		echo '</td></tr>';
	}


	if(awisBenutzerRecht($con, 114) > 0)
	{
		echo '';
		echo '<tr>';
		echo '<td width=27% height=19>';
		echo '<p style=line-height: 100%; margin: 10><a name=Ausstattung><b>Ausstattung</b></a></td>';
		echo '<td width=73% height=19>';
		echo '<p style=line-height: 100%; margin: 10>';

		echo '
		Auf dieser Seite wird die Ausstattung in den Filialen angezeigt. Wenn ein Anwender die entsprechenden
		Rechte hat, k�nnen die Daten gepflegt werden. F�r die Werkstattausstattung k�nnen die Mengen pro
		Austattungsteil festgelegt werden.
		';

		echo '</td></tr>';
	}

	if(awisBenutzerRecht($con, 115) > 0)
	{
		echo '';
		echo '<tr>';
		echo '<td width=27% height=19>';
		echo '<p style=line-height: 100%; margin: 10><a name=Personal><b>Personal</b></a></td>';
		echo '<td width=73% height=19>';
		echo '<p style=line-height: 100%; margin: 10>';

		echo '
		Auf dieser Seite werden alle Mitarbeiter mit Ihren T�tigkeiten und dem <a href=./schichtplan_Hilfe.php>Schichtplan</a> angezeigt.
		';

		echo '</td></tr>';
	}

	if(awisBenutzerRecht($con, 116) > 0)
	{
		echo '';
		echo '<tr>';
		echo '<td width=27% height=19>';
		echo '<p style=line-height: 100%; margin: 10><a name=Sonstiges><b>Sonstiges</b></a></td>';
		echo '<td width=73% height=19>';
		echo '<p style=line-height: 100%; margin: 10>';

		echo '
		In diesem Register werden weitere Informationen zu den Filialen angezeigt.
		';

		echo '</td></tr>';
	}


	if(awisBenutzerRecht($con, 117) > 0)
	{
		echo '';
		echo '<tr>';
		echo '<td width=27% height=19>';
		echo '<p style=line-height: 100%; margin: 10><a name=Lieferungen><b>Lieferungen</b></a></td>';
		echo '<td width=73% height=19>';
		echo '<p style=line-height: 100%; margin: 10>';

		echo '
		In diesem Register werden weitere die Anlieferungen der Filialen angezeigt. In dem Auswahlfeld
		kann jeder Mitarbeiter eine Liste mit Filialnummern angeben, die angezeigt werden sollen. Dabei 
		kann die Eingabe folgenderma�en erfolgen:<br>
		1;2;5          Einzelne Filialen<br>
		1-10           Gruppe von Filialen<br>
		1-10;34;50-100 Kombination<bR>
		<b>Hinweis:</b> Die Liste darf keine Leerzeichen enthalten!
		';

		echo '</td></tr>';
	}


?>


  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%; margin: 10"><a name=Neueroeffnungen><b>Neuer�ffnungen</b></a></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Zeigt alle Filialen an, die in der n�chsten Zukunft er�ffnet werden. Sobald die Gebietskennung in den Filialen 
	  gepflegt wurde, wird die Filiale aus dieser Liste entfernt. Durch einen Klick auf die Links werden die Details der Filiale angezeigt.</td>
  </tr>

  <tr>
    <td width="27%" height="19">
      <p style="line-height: 100%; margin: 10"><a name=Umzuege><b>Umz�ge</b></a></td>
    <td width="73%" height="19">
      <p style="line-height: 100%; margin: 10">Zeigt alle Filialen an, bei denen in der n�chsten Zukunft ein Umzug ansteht. Sobald die neue Adresse bei der Filiale gepflegt wurde
	  wird die Filiale aus der Liste entfernt.</td>
  </tr>
</table>

<?php
    awislogoff($con);
?>

</body>

</html>
