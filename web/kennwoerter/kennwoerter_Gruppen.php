<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KZG','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('AST','AST_VK');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Param = unserialize($AWISBenutzer->ParameterLesen("Formular_KZG"));


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht9200 = $AWISBenutzer->HatDasRecht(9200);
	if(($Recht9200&16)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$DetailAnsicht=false;
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./kennwoerter_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KZG'));
		}
		else
		{
			$Param['Key'] = $AWIS_KEY1;
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./kennwoerter_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KZG'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KZG'));
	}
	elseif(isset($_GET['KZG_KEY']))
	{
		$AWIS_KEY1 = $Form->Format('N0',$_GET['KZG_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KZG'));
//		$Param['KEY'] = $AWIS_KEY1;
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KZG'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_KZG',serialize($Param));
		}
		else
		{
			$AWIS_KEY1=$Param['KEY'];
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

	//	$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY KZG_KEY';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT KENNWOERTERGRUPPEN.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM KENNWOERTERGRUPPEN';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL,$Param);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_KZG',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

//$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1,$SQL);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsKZG = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufartikel action=./kennwoerter_Main.php?cmdAktion=Gruppen'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

	if($rsKZG->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsKZG->AnzahlDatensaetze()>1 OR isset($_GET['Liste']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen';
		$Link .= '&Sort=KZG_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KZG_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KZG']['KZG_BEZEICHNUNG'],250,'',$Link);
		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen';
		$Link .= '&Sort=KZG_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KZG_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KZG']['KZG_BEMERKUNG'],400,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKZG->EOF())
		{
			$Form->ZeileStart();
			$Link = './kennwoerter_Main.php?cmdAktion=Gruppen&KZG_KEY=0'.$rsKZG->FeldInhalt('KZG_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('KZG_BEZEICHNUNG',$rsKZG->FeldInhalt('KZG_BEZEICHNUNG'),0,250,false,($DS%2),'',$Link,'T','L',$rsKZG->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Form->Erstelle_ListenFeld('KZG_BEMERKUNG',$rsKZG->FeldInhalt('KZG_BEMERKUNG'),0,400,false,($DS%2));
			$Form->ZeileEnde();

			$rsKZG->DSWeiter();
			$DS++;
		}

		$Link = './kennwoerter_Main.php?cmdAktion=Gruppen';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsKZG->FeldInhalt('KZG_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_KZG',serialize($Param));

		$Form->Formular_Start();
		$Form->Erstelle_HiddenFeld('KZG_KEY',$rsKZG->FeldInhalt('KZG_KEY'));
		$Form->Erstelle_HiddenFeld('KZG_XBN_KEY',($rsKZG->FeldInhalt('KZG_XBN_KEY')==''?$AWISBenutzer->BenutzerID():$rsKZG->FeldInhalt('KZG_XBN_KEY')));

		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kennwoerter_Main.php?cmdAktion=Gruppen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKZG->FeldInhalt('LIE_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKZG->FeldInhalt('LIE_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=($AWISBenutzer->BenutzerID()==$rsKZG->FeldInhalt('KZG_XBN_KEY') OR ($Recht9200&6));

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KZG']['KZG_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('KZG_BEZEICHNUNG',$rsKZG->FeldInhalt('KZG_BEZEICHNUNG'),55,370,$EditRecht);
		$AWISCursorPosition='txtKZG_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KZG']['KZG_BEMERKUNG'].':',200);
		$Form->Erstelle_TextFeld('KZG_BEMERKUNG',$rsKZG->FeldInhalt('KZG_BEMERKUNG'),55,370,$EditRecht);
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		if($rsKZG->FeldInhalt('KZG_KEY')!='')
		{
			$Reg = new awisRegister(9203);
			$Reg->ZeichneRegister(isset($_GET['Seite'])?$_GET['Seite']:'Mitglieder');
		}
	}

	//awis_Debug(1, $Param, $Bedingung, $rsKZG, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../stammdaten_Main.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht9200&16)!=0))
	{
		if($DetailAnsicht)
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
			$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
		}
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND KZG_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	$Bedingung .= ' AND KZG_XBN_KEY = '.$AWISBenutzer->BenutzerID();

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>