<?php
global $con;
global $Recht3010;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('MBW','MBW_%');
$TextKonserven[]=array('MBW','txtAnredenListe');
$TextKonserven[]=array('MBW','AnzAenderungen');
$TextKonserven[]=array('MBW','txtMitbewerberGroesse');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010,$_SERVER['PHP_AUTH_USER']);
$Recht3011 = awisBenutzerRecht($con,3011,$_SERVER['PHP_AUTH_USER']);

$SQL = 'SELECT *';
$SQL .= ' FROM Mitbewerber';
$SQL .= ' WHERE MBW_MBK_KEY = 1';

if(isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC',$_GET['Sort']);	
}
else 
{
	$SQL .= ' ORDER BY MBW_Name1, MBW_Name2, MBW_PLZ, MBW_STRASSE';
}

$rsMBW = awisOpenRecordset($con, $SQL);
$rsMBWZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
awis_FORM_FormularStart();	

awis_FORM_ZeileStart();
$Link = './mitbewerber_Main.php?cmdAktion=Neuanlagen';
$Link .= '&Sort=MBW_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_NAME1'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_NAME1'],300,'',$Link);
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_NAME2'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_STRASSE'],250);
$Link = './mitbewerber_Main.php?cmdAktion=Neuanlagen';
$Link .= '&Sort=MBW_PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_PLZ'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_PLZ'],100,'',$Link);
$Link = './mitbewerber_Main.php?cmdAktion=Neuanlagen';
$Link .= '&Sort=MBW_ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_ORT'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_ORT'],150,'',$Link);
$Link = './mitbewerber_Main.php?cmdAktion=Neuanlagen';
$Link .= '&Sort=MBW_USER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_USER'))?'~':'');
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_USER'],150,'',$Link);
awis_FORM_ZeileEnde();

for($MBWZeile=0;$MBWZeile<$rsMBWZeilen;$MBWZeile++)
{
	awis_FORM_ZeileStart();
	$Link = './mitbewerber_Main.php?cmdAktion=Details&&MBW_KEY='.$rsMBW['MBW_KEY'][$MBWZeile].'';
	awis_FORM_Erstelle_ListenFeld('MBW_NAME1',$rsMBW['MBW_NAME1'][$MBWZeile],0,300,false,($MBWZeile%2),'',$Link);
	awis_FORM_Erstelle_ListenFeld('MBW_NAME2',$rsMBW['MBW_NAME2'][$MBWZeile],0,200,false,($MBWZeile%2),'','');
	awis_FORM_Erstelle_ListenFeld('MBW_STRASSE',$rsMBW['MBW_STRASSE'][$MBWZeile].' '.$rsMBW['MBW_HAUSNUMMER'][$MBWZeile],0,250,false,($MBWZeile%2),'','');
	awis_FORM_Erstelle_ListenFeld('MBW_PLZ',$rsMBW['MBW_LAN_CODE'][$MBWZeile].'-'.$rsMBW['MBW_PLZ'][$MBWZeile],0,100,false,($MBWZeile%2),'','');
	awis_FORM_Erstelle_ListenFeld('MBW_ORT',$rsMBW['MBW_ORT'][$MBWZeile],0,150,false,($MBWZeile%2),'','');
	awis_FORM_Erstelle_ListenFeld('MBW_USER',$rsMBW['MBW_USER'][$MBWZeile],0,150,false,($MBWZeile%2),'','','T','L',$rsMBW['MBW_USERDAT'][$MBWZeile]);
	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();




?>