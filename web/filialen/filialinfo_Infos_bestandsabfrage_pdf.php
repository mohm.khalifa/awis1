<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?
//********************************************************************************************
// Lieferkontrolle
//********************************************************************************************
//
// Dieses Modul erstellt eine PDF - Datei zur Bestandsabfragen der Filialen
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 20.07.2007
//********************************************************************************************
// �nderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************

global $AWISBenutzer;
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3601);	// MDE Bestandsabfrage
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Bestandsabfrage-PDF', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");     
}

awis_Debug(1,$_GET);

//if ($_REQUEST['BAF_DATUMSCAN']!='' && $_REQUEST['BAF_FIL_ID']!= '' )
if ($_REQUEST['BAF_FIL_ID']!= '' )
{
	
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	$SQL = 'SELECT BAF_AST_ATUNR, AST_BEZEICHNUNGWW, BAF_DATUMSCAN, BAF_MENGESCAN, FIB_BESTAND, RUF_REGUNDFACHNR, AST_VK' ; 
	$SQL .= ' FROM BESTANDSABFRAGEN';
	$SQL .= ' INNER JOIN ARTIKELSTAMM ON AST_ATUNR = BAF_AST_ATUNR';
	$SQL .= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = BAF_AST_ATUNR AND FIB_FIL_ID = BAF_FIL_ID';
	$SQL .= ' LEFT JOIN REGALUNDFACHNUMMER ON RUF_AST_ATUNR = BAF_AST_ATUNR AND RUF_FIL_ID = BAF_FIL_ID AND RUF_AKTIV = 1';
	$SQL .= ' WHERE BAF_FIL_ID=0'.intval($_REQUEST['BAF_FIL_ID']);
//	$SQL .= ' AND BAF_DatumScan=\''.$_REQUEST['BAF_DATUMSCAN'].'\'';
	$SQL .= ' ORDER BY BAF_AST_ATUNR ASC';
	
	awis_Debug(1,$SQL);
	
	$rsBAF = awisOpenRecordset($con,$SQL);
	$rsBAFZeilen = $awisRSZeilen;
	
	if($rsBAFZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten gefunden!</span></center>");
	}
	else 
	{
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
	
		define('FPDF_FONTPATH','font/');
		$pdf = new fpdi('p','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		$ATULogo = $pdf->ImportPage(1);				
		
		$Zeile=0;
		$Seitenrand=15;
		$Seite=1;
		
		NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$rsBAF['BAF_DATUMSCAN'][0],$Seite);
				
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;
		
		$Y_Wert=$Y_Wert+10;
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(15,6,"ATUNR",1,0,'L',1);
		
		$pdf->setXY($LinkerRand+15,$Y_Wert);
		$pdf->cell(45,6,"Artikelbezeichnung",1,0,'L',1);
		
		$pdf->setXY($LinkerRand+55,$Y_Wert);
		$pdf->cell(25,6,"Filialbestand",1,0,'L',1);
		
		if (($RechteStufe & 2) == 2)
		{
			$pdf->setXY($LinkerRand+80,$Y_Wert);
			$pdf->cell(22,6,"Scanmenge",1,0,'L',1);
			
			$pdf->setXY($LinkerRand+102,$Y_Wert);
			$pdf->cell(20,6,"VK",1,0,'L',1);
		
			$pdf->setXY($LinkerRand+122,$Y_Wert);
			$pdf->cell(60,6,"Regalnummer der letzten Inventur",1,0,'L',1);
		}
		
		else 
		{
			$pdf->setXY($LinkerRand+80,$Y_Wert);
			$pdf->cell(20,6,"VK",1,0,'L',1);
		
			$pdf->setXY($LinkerRand+100,$Y_Wert);
			$pdf->cell(75,6,"Regalnummer der letzten Inventur",1,0,'L',1);
		}
		
		
						
		for($BAFZeile=0;$BAFZeile<$rsBAFZeilen;$BAFZeile++)
		{
			if ($Y_Wert > 270)
			{
				$Y_Wert=16;
				$Y_Wert=$Y_Wert+10;
				$Seite = $Seite + 1;
				
				NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$rsBAF['BAF_DATUMSCAN'][0],$Seite);
				
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(15,6,"ATUNR",1,0,'L',1);
				
				$pdf->setXY($LinkerRand+15,$Y_Wert);
				$pdf->cell(45,6,"Artikelbezeichnung",1,0,'L',1);
				
				$pdf->setXY($LinkerRand+55,$Y_Wert);
				$pdf->cell(25,6,"Filialbestand",1,0,'L',1);
				
				if (($RechteStufe & 2) == 2)
				{
					$pdf->setXY($LinkerRand+80,$Y_Wert);
					$pdf->cell(22,6,"Scanmenge",1,0,'L',1);
					
					$pdf->setXY($LinkerRand+102,$Y_Wert);
					$pdf->cell(20,6,"VK",1,0,'L',1);
				
					$pdf->setXY($LinkerRand+122,$Y_Wert);
					$pdf->cell(60,6,"Regalnummer der letzten Inventur",1,0,'L',1);
				}
				
				else 
				{				
					$pdf->setXY($LinkerRand+80,$Y_Wert);
					$pdf->cell(20,6,"VK",1,0,'L',1);
					
					$pdf->setXY($LinkerRand+100,$Y_Wert);
					$pdf->cell(75,6,"Regalnummer der letzten Inventur",1,0,'L',1);
				}
			}
			
			$ATUNR=$rsBAF['BAF_AST_ATUNR'][$BAFZeile];
			$BEZEICHNUNG=$rsBAF['AST_BEZEICHNUNGWW'][$BAFZeile];
			//$BESTAND=$rsBAF['FIB_BESTAND'][$BAFZeile];
			$BESTAND=number_format((float)$rsBAF['FIB_BESTAND'][$BAFZeile],0,',','.');
			$SCANMENGE=$rsBAF['BAF_MENGESCAN'][$BAFZeile];
			$REGUNDFACHNR=$rsBAF['RUF_REGUNDFACHNR'][$BAFZeile];
			$AST_VK=$rsBAF['AST_VK'][$BAFZeile];
			
			$Y_Wert=$Y_Wert+6;
			$pdf->SetFont('Arial','',9);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(15,6,$ATUNR,1,0,'L',0);	
			
			$pdf->setXY($LinkerRand+15,$Y_Wert);
			$pdf->cell(40,6,substr($BEZEICHNUNG,0,15),1,0,'L',0);
			
			$pdf->setXY($LinkerRand+55,$Y_Wert);
			$pdf->cell(25,6,$BESTAND,1,0,'L',0);
			
			if (($RechteStufe & 2) == 2)
			{
				$pdf->setXY($LinkerRand+80,$Y_Wert);
				$pdf->cell(22,6,$SCANMENGE,1,0,'L',0);
				
				$pdf->setXY($LinkerRand+102,$Y_Wert);
				$pdf->cell(20,6,awis_formatZahl($AST_VK,'Standardzahl'),1,0,'L',0);
				
				$pdf->setXY($LinkerRand+122,$Y_Wert);
				$pdf->cell(60,6,$REGUNDFACHNR,1,0,'L',0);				
			}
			
			else 
			{
				$pdf->setXY($LinkerRand+80,$Y_Wert);
				$pdf->cell(20,6,awis_formatZahl($AST_VK,'Standardzahl'),1,0,'L',0);
				
				$pdf->setXY($LinkerRand+100,$Y_Wert);
				$pdf->cell(75,6,$REGUNDFACHNR,1,0,'L',0);				
			}			
		}	
	}	
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
	echo "<hr><a href=./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Bestandsabfrage><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Thomas Riedl
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool �berschrift
* @param string Datum
* @param int Seite
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$Ueberschrift,$Datum,$Seite)
{
	static $Seite;
	
	$pdf->addpage();									// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+160,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->Cell(180,3,'Seite '. $Seite,0,0,'C',0);
	//$pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
	
	
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"Bestandsabfrage vom ".$Datum,0,0,'C',0);
	
	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	$Zeile = 22;
}

/*
*	@author Thomas Riedl
*	@param mixed $Ausdruck
*	@param string $Format
*	@param string $KommaZeichen
*	@return string
*	@version 1.0
*
*****************************************************************************************************/
function awis_formatZahl($Ausdruck, $Format, $KommaZeichen = ',')
{
	$Erg = '';
	$NK = 0;

	if(substr($Format,0,12)=='Standardzahl')
	{
		$NK = substr($Format,13,9);
		if($NK=='')
		{
			$NK=2;
		}
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,$NK,',','.') . '';
		//$Erg = str_replace("-","&minus;" , $Erg );
	}
	else
	{
		$Erg = $Ausdruck;
	}

	return $Erg;

}
	
?>
</body>
</html>
