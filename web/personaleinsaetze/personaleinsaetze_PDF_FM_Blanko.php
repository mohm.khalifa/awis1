<?php
require_once ('NeuePersBerichte.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$AWISBenutzer = awisBenutzer::Init();

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Form = new awisFormular();

$TextKonserven[] = array('IBG', '%');
$TextKonserven[] = array('CV', '%');
$TextKonserven[] = array('FM', '%');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


$SQL = 'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS ';
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY=' . $DB->FeldInhaltFormat('NO', $_GET['PEI_KEY']);

$rsBericht = $DB->RecordSetOeffnen($SQL);


$NeuePersBerichte = new NeuePersBerichte($DB,$AWISBenutzer,$AWISSprachKonserven,$rsBericht);
$NeuePersBerichte->HeaderDaten('Filialbesuch GBL Monatlich', 'Filialbericht GBL', '01.02.2019', '1.0',517,518,519);
$NeuePersBerichte->FooterDaten('Filialbesuch GBL Monatlich ' . date('m-Y'));
$NeuePersBerichte->NeueSeite();

$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A1'], 521);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A2'], 522);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A3'], 523);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A4'], 524);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_A5'], 609);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B1'], 526);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B2'], 527);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B3'], 528);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B4'], 529);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B5'], 530);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B6'], 531);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B7'], 532);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_B8'], 533);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C1'], 535);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C2'], 536);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C3'], 537);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C4'], 538);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_C5'], 539);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D1'], 541);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D2'], 542);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D3'], 543);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D4'], 544);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D5'], 546);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D6'], 547);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D7'], 548);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D8'], 549);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D9'], 550);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D10'], 551);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D11'], 552);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D12'], 553);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D13'], 554);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D14'], 555);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D15'], 556);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D16'], 557);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_D17'], 558);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_E'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_E1'], 560);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_E2'], 561);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_E3'], 562);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_F'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_F1'], 565);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_F2'], 566);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_F3'], 567);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_F4'], 568);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_G'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_G1'], 589);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_G2'], 590);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_H'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_H1'], 592);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_H2'], 593);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_H3'], 594);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_H4'], 595);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_I'], 0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_I1'], 605,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_I2'], 701,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_I3'], 702,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_J'], 0,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_J1'], 607,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FM']['FM_J2'], 608,false);


$NeuePersBerichte->Anzeigen();



?>