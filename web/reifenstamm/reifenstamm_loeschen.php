<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('RST','*');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try 
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form->DebugAusgabe(1,'l�schen:');
	$Form->DebugAusgabe(1,$_POST);
	
	$Tabelle= '';
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtRST_KEY']))
		{
			$Tabelle = 'RST';
			$Key=$_POST['txtRST_KEY'];
	
			$SQL = 'SELECT RST_KEY, RST_BEZEICHNUNG, RST_LIE_NR, RST_LARTNR';
			$SQL .= ' FROM REIFENSTAMM ';
			$SQL .= ' WHERE RST_KEY=0'.$Key;
			
			$rsDaten = $DB->RecordsetOeffnen($SQL);
			$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			$HauptKey = $rsDaten->FeldInhalt('RST_KEY');
				
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('RST','RST_LIE_NR'),$rsDaten->FeldInhalt('RST_LIE_NR'));
			$Felder[]=array($Form->LadeTextBaustein('RST','RST_LARTNR'),$rsDaten->FeldInhalt('RST_LARTNR'));
			$Felder[]=array($Form->LadeTextBaustein('RST','RST_BEZEICHNUNG'),$rsDaten->FeldInhalt('RST_BEZEICHNUNG'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Reifenstamm':
				$Tabelle = 'RST';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT RST_KEY, RST_BEZEICHNUNG, RST_LIE_NR, RST_LARTNR';
				$SQL .= ' FROM REIFENSTAMM ';
				$SQL .= ' WHERE RST_KEY=0'.$Key;
								
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RST_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_LIE_NR'),$rsDaten->FeldInhalt('RST_LIE_NR'));
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_LARTNR'),$rsDaten->FeldInhalt('RST_LARTNR'));
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_BEZEICHNUNG'),$rsDaten->FeldInhalt('RST_BEZEICHNUNG'));
				break;
			case 'Wiedervorlagen':
				$Tabelle = 'WVL';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT';
				$SQL .= ' RST_KEY, WVL_KEY, WVL_DATUM, WVL_TEXT, WVL_BEARBEITER';
				$SQL .= ' FROM WIEDERVORLAGEN ';
				$SQL .= ' INNER JOIN REIFENSTAMM ON RST_KEY=WVL_XXX_KEY';
				$SQL .= ' WHERE WVL_KEY=0'.$Key;
												
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RST_KEY');
				$SubKey=$rsDaten->FeldInhalt('WVL_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_DATUM'),$Form->Format('D', $rsDaten->FeldInhalt('WVL_DATUM')));
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_TEXT'),$Form->Format('T', $rsDaten->FeldInhalt('WVL_TEXT')));
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_BEARBEITER'),$Form->Format('T', $rsDaten->FeldInhalt('WVL_BEARBEITER')));
				break;
			case 'Artikel':
				$Tabelle = 'RRP';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT';
				$SQL .= ' RRP_KEY, RST_KEY, RST_BEZEICHNUNG, RST_BREITE, RST_QUERSCHNITT, RST_INNENDURCHMESSER, RRP_ATUNR, RRP_BEZEICHNUNGWW';
				$SQL .= ' FROM REIFENPFLEGE ';
				$SQL .= ' INNER JOIN REIFENSTAMM ON RST_KEY = RRP_RST_KEY ';
				$SQL .= ' WHERE RRP_KEY=0'.$Key;
												
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RST_KEY');
				$SubKey=$rsDaten->FeldInhalt('RRP_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_BEZEICHNUNG'),$Form->Format('T', $rsDaten->FeldInhalt('RST_BEZEICHNUNG')));
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_BREITE'),$Form->Format('T', $rsDaten->FeldInhalt('RST_BREITE')));
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_QUERSCHNITT'),$Form->Format('T', $rsDaten->FeldInhalt('RST_QUERSCHNITT')));
				$Felder[]=array($Form->LadeTextBaustein('RST','RST_INNENDURCHMESSER'),$Form->Format('T', $rsDaten->FeldInhalt('RST_INNENDURCHMESSER')));
				$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_ATUNR'),$Form->Format('T', $rsDaten->FeldInhalt('RRP_ATUNR')));
				$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_BEZEICHNUNGWW'),$Form->Format('T', $rsDaten->FeldInhalt('RRP_BEZEICHNUNGWW')));
				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'RST':
				$DB->TransaktionBegin();
				
				$SQL = 'DELETE FROM REIFENSTAMM WHERE RST_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
				$SQL = 'DELETE FROM WIEDERVORLAGEN ' ;
				$SQL .= ' WHERE WVL_XTN_KUERZEL=\'RST\'';
				$SQL .= ' AND WVL_XXX_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
				$SQL = 'UPDATE REIFENPFLEGE';
				$SQL .= ' SET RRP_RST_KEY=NULL';
				$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', RRP_USERDAT=SYSDATE';
				$SQL .= ' WHERE RRP_RST_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());

				$DB->TransaktionCommit();
				$AWIS_KEY1='';
				$AWIS_KEY2='';
				break;
						
			case 'WVL':
				$SQL = 'DELETE FROM WIEDERVORLAGEN WHERE WVL_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2='';
				break;
						
			case 'RRP':
				$SQL = 'UPDATE REIFENPFLEGE SET RRP_RST_KEY=NULL WHERE RRP_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2='';
				break;		
			default:
				break;
		}
	}
	
	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./reifenstamm_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
		$Form->Formular_Start();
		
		$Form->ZeileStart();
		switch ($Tabelle)
		{
			case 'RRP':
				$Form->Hinweistext($TXT_AdrLoeschen['RST']['txtZuordnungLoeschen']);
				break;
			default:
				$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
				break;
		}	
		$Form->ZeileEnde();
	
		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',200);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}
	
		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
		if(isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);
	
		$Form->Trennzeile();
	
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
	
		$Form->SchreibeHTMLCode('</form>');
	
		$Form->Formular_Ende();
	
		die();
	}
}
catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>